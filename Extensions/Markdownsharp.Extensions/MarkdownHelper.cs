﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dionysos;

namespace MarkdownSharp.Extensions
{
    public static class MarkdownHelper
    {
        public static MarkdownSharp.Markdown GetDefaultInstance()
        {
            MarkdownSharp.MarkdownOptions options = new MarkdownSharp.MarkdownOptions();
            options.AutoNewLines = true;            
            return new MarkdownSharp.Markdown(options);    
        }

        public static string AddMarkdown(string description)
        {
            if (description.IsNullOrWhiteSpace())
            {
                return string.Empty;
            }

            string toReturn = string.Empty;
            string[] lines = description.Split("\r\n", StringSplitOptions.None);

            for (int i = 0; i < lines.Length; i++)
            {
                string line = lines[i];

                if (line.EndsWith("  "))
                {
                    toReturn += line;
                }
                else if (line.EndsWith(" "))
                {
                    toReturn += line + " ";
                }
                else
                {
                    toReturn += line + "  ";
                }

                if (i + 1 < lines.Length)
                {
                    toReturn += "\r\n";
                }
            }

            return toReturn;
        }

        public static string RemoveMarkdown(string description)
        {
            string toReturn = string.Empty;

            string[] lines = description.Split("\r\n", StringSplitOptions.None);

            for (int i = 0; i < lines.Length; i++)
            {
                string line = lines[i];

                if (line.EndsWith("  "))
                {
                    toReturn += line.Substring(0, line.Length - 2);
                }
                else if (line.EndsWith(" "))
                {
                    toReturn += line.Substring(0, line.Length - 1);
                }
                else
                {
                    toReturn += line;
                }

                if (i + 1 < lines.Length)
                {
                    toReturn += "\r\n";
                }
            }

            return toReturn;
        }
    }
}
