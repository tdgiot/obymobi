﻿using Dionysos;
using Dionysos.Logging;
using Obymobi;
using Obymobi.Configuration;
using Obymobi.WebserviceTester;
using Obymobi.WebserviceTester.Commands;
using Obymobi.WebserviceTester.Logic;
using Obymobi.WebserviceTester.Testing;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using WebserviceTesterConsole.Logic;

namespace WebserviceTesterConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            // Set the data providers
            Dionysos.DataFactory.EntityCollectionFactory = new Dionysos.Data.LLBLGen.LLBLGenEntityCollectionFactory();
            Dionysos.DataFactory.EntityFactory = new Dionysos.Data.LLBLGen.LLBLGenEntityFactory();
            Dionysos.DataFactory.EntityUtil = new Dionysos.Data.LLBLGen.LLBLGenEntityUtil();

            // Initialize Console specific globals
            InitializeGlobals();

            // Initialize the shared Logic.WebserviceTester global
            Obymobi.WebserviceTester.Global.Initialize();
            Obymobi.WebserviceTester.Global.IsConsoleMode = true;            
            
            if (args.Length <= 0)
            {
                Logger.Log("No arguments provided to webservice tester'");
            }
            else 
            {                
                Logger.Log("Starting with arguments: '{0}'", StringUtilLight.CombineWithCommaSpace(args));
                
                try
                {
                    // Parse input to testruns            
                    new InputHandler(args).Handle();
                }
                catch (Exception ex)
                {
                    // Invalid input
                    Logger.Log("An exception occurred while handling the parameters: '{0}'", StringUtilLight.CombineWithCommaSpace(args));
                    Logger.Log("Message: {0}", ex.Message);
                    Logger.Log("Stacktrace: {0}", ex.StackTrace);                    
                }

                Console.ReadLine();
            }                                  
        }

        private static void InitializeGlobals()
        {                        
            // Set application info
            Dionysos.Global.ApplicationInfo = new Dionysos.ServiceProcess.ServiceInfo();
            Dionysos.Global.ApplicationInfo.ApplicationName = "WebserviceTester";
            Dionysos.Global.ApplicationInfo.ApplicationVersion = "1.2014051901";
            Dionysos.Global.ApplicationInfo.BasePath = Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location);
            Dionysos.GlobalLight.LoggingProvider = new AsyncLoggingProvider(Path.Combine(Dionysos.Global.ApplicationInfo.BasePath, "Logs"), "CloudStatus", 14, "WebserviceTester");

            // Configuration provider
            string applicationPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            var provider = new Dionysos.Configuration.XmlConfigurationProvider { ManualConfigFilePath = Path.Combine(applicationPath, "WebserviceTester.config") };
            Dionysos.Global.ConfigurationProvider = provider;
            Dionysos.Global.ConfigurationInfo.Add(new CraveCloudConfigInfo());
        }
    }
}
