﻿using Dionysos;
using Obymobi.WebserviceTester.Commands;
using Obymobi.WebserviceTester.Commands.TestCases;
using Obymobi.WebserviceTester.Commands.Testruns;
using Obymobi.WebserviceTester.Enums;
using Obymobi.WebserviceTester.Helpers;
using Obymobi.WebserviceTester.Interfaces;
using Obymobi.WebserviceTester.Logic;
using Obymobi.WebserviceTester.Managers;
using Obymobi.WebserviceTester.Testing;
using Obymobi.WebserviceTester.Testing.Testruns;
using Obymobi.WebserviceTester.Testing.Testruns.Crave;
using Obymobi.WebserviceTester.Testing.Testruns.Mobile;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace WebserviceTesterConsole.Logic
{
    public class InputHandler
    {
        #region Properties

        private string[] args { get; set; }
        private Dictionary<string, string> parameters { get; set; }

        #endregion

        #region Constructors

        public InputHandler(string[] args)
        {
            // Set arguments
            this.args = args;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Handles the given arguments
        /// </summary>
        public void Handle()
        {
            // Pair arguments
            this.parameters = DictionaryUtil.PairArguments(this.args);

            if (!this.parameters.ContainsKey("webservicetype"))
            {
                // No webservice type
                Logger.Log("Aborted: no webservice type has been specified as parameter");
            }            
            else if (!this.parameters.ContainsKey("webserviceversion"))
            {
                // No webservice version
                Logger.Log("Aborted: no webservice version has been specified as parameter");
            }            
            else if (!this.parameters.ContainsKey("testruns"))
            {
                // No test runs
                Logger.Log("Aborted: no testrun has been specified as parameter");
            }
            else
            {
                WebserviceType webserviceType = this.parameters["webservicetype"].TurnFirstToUpper().ToEnum<WebserviceType>();
                string webserviceVersion = this.parameters["webserviceversion"];
                
                // Try to initialize the webservice
                IWebservice webservice;
                if (!WebserviceHelper.TryInitializeWebservice(webserviceType, webserviceVersion, out webservice))
                {
                    // Failed to initialize the webservice based on the given type and version
                    Logger.Log("The webservice couldn't be initialized with the type '{0}' and version '{1}'", webserviceType.GetStringValue(), webserviceVersion);                    
                }
                else
                {
                    Logger.Log("Webservice successfully initialized!");

                    // Set the execution amount if there's one
                    int executionAmount = 1;
                    if (this.parameters.ContainsKey("executionamount"))
                    {
                        int.TryParse(this.parameters["executionamount"], out executionAmount);
                    }

                    // Set the company id if there's one
                    int companyId = -1;
                    if (this.parameters.ContainsKey("companyid"))
                    {
                        int.TryParse(this.parameters["companyid"], out companyId);
                    }

                    // Create the right testrun factory based on the webservice found
                    TestrunFactory testrunFactory = this.InitializeTestrunFactory(webservice);                    

                    // Split the testruns 
                    List<string> testrunNames = this.parameters["testruns"].Split(',').ToList();

                    List<Testrun> testruns = new List<Testrun>();
                    foreach (string testrunName in testrunNames)
                    {
                        Testrun testrun = null;

                        switch (testrunName.ToUpper())
                        { 
                            case TestrunOrderProductsForCompany.NAME:                                
                                testrun = testrunFactory.CreateTestrunOrderProductsForCompany(companyId);
                                break;
                            case TestrunGetMenuForAllCompanies.NAME:
                                testrun = testrunFactory.CreateTestrunGetMenuForAllCompanies();
                                break;
                        }

                        if (testrun != null) testruns.Add(testrun);
                    }

                    // Execute testruns
                    foreach (Testrun testrun in testruns)
                    {                                                
                        testrun.ExecutionAmount = executionAmount;
                        testrun.Run();
                    }
                }                
            }                      
        }

        /// <summary>
        /// Initializes a testrun factory based on the given webservice type
        /// </summary>
        /// <param name="webservice"></param>
        /// <returns></returns>
        private TestrunFactory InitializeTestrunFactory(IWebservice webservice)
        { 
            TestrunFactory factory = null;

            if (webservice is ICraveService)
                factory = new TestrunFactoryCrave((ICraveService)webservice);
            else if (webservice is IMobileservice)
                factory = new TestrunFactoryMobile((IMobileservice)webservice);

            return factory;
        }

        #endregion
    }
}
