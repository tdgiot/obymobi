﻿using Obymobi.WebserviceTester.Testing.TestCases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Obymobi.WebserviceTester.Testing.TestCases;
using Obymobi.WebserviceTester.Testing;
using Obymobi.WebserviceTester.Testing.TestCases.Mobile;
using Obymobi.WebserviceTester.Interfaces;
using Obymobi.WebserviceTester.WebServices.Params;
using Obymobi.WebserviceTester.Logic;

namespace WebserviceTesterConsole.Logic
{
    public class TestCaseParser
    {
        #region Properties

        private string[] arguments { get; set; }

        #endregion

        #region Constructors

        public TestCaseParser(string[] args)
        {
            this.arguments = args;
        }

        #endregion

        #region Methods

        public List<TestCaseBase> ParseToTestcases()
        {
            var testCases = new List<TestCaseBase>();

            // Divider the arguments in sections per test case
            List<string[]> argumentsPerTestcase = this.DividerArgumentsPerTestcase();

            foreach (string[] arguments in argumentsPerTestcase)
            {
                // Divide the arguments to key / value pairs
                Dictionary<string, string> keyValues = this.PairArguments(arguments);
                
                if (!keyValues.ContainsKey("testcase"))
                {
                    // Testcase missing
                }
                else if (!keyValues.ContainsKey("webserviceversion"))
                {
                    // Webservice version missing
                }
                else if (!keyValues.ContainsKey("webservicetype"))
                {
                    // Webservice type missing
                }
                else                                   
                {
                    // Create the test cases
                    TestCaseBase testcase = null;
                    try
                    {
                        if (keyValues["webservicetype"].Equals("mobileservice"))
                        {
                            testcase = this.CreateMobileTestcase(keyValues);
                        }
                        else if (keyValues["webservicetype"].Equals("craveservice"))
                        {
                            testcase = this.CreateCraveTestcase(keyValues);
                        }
                    }
                    catch (Exception ex)
                    {
                        Log.Error("ArgumentParser", "ParseToTestcases", "Error while parsing the arguments. '{0}'", ex.Message);
                    }

                    if (testcase != null)
                    {
                        testCases.Add(testcase);
                    }
                }                
            }

            return testCases;
        }

        private List<string[]> DividerArgumentsPerTestcase()
        {
            List<string[]> argumentsArrayPerTestcase = new List<string[]>();

            List<string> argumentsInTestcase = new List<string>();
            for (int i = 0; i < this.arguments.Length; i++)
            {
                string arg = this.arguments[i].ToLower();
                
                if (arg.Equals("-testcase"))
                {
                    // New test case found
                    if (argumentsInTestcase.Count > 0)
                    {
                        // Save the results for the previous arguments array
                        argumentsArrayPerTestcase.Add(argumentsInTestcase.ToArray());
                    }                    
                    // Initialize a new arguments collection
                    argumentsInTestcase = new List<string>();
                }

                argumentsInTestcase.Add(arg);
            }

            // Add the last test case if we have one
            if (argumentsInTestcase.Count > 0)
            {
                argumentsArrayPerTestcase.Add(argumentsInTestcase.ToArray());
            }

            return argumentsArrayPerTestcase;
        }

        private Dictionary<string, string> PairArguments(string[] arguments)
        {
            Dictionary<string, string> argumentsDictionary = new Dictionary<string, string>();

            int count = 1;
            for (int i = 0; i < arguments.Length; i++)
            {
                string key = arguments[i].Replace("-", "").ToLower();
                
                if (++i < arguments.Length)
                { 
                    string value = arguments[i].ToLower();
                    argumentsDictionary.Add(key, value);

                    Logger.Log("Arg pair {0}: {1} - {2}", count, key, value);
                    count++;
                }
            }    
        
            return argumentsDictionary;
        }

        private TestCaseBase CreateCraveTestcase(Dictionary<string, string> args)
        {
            if (!args.ContainsKey("clientid") && !args.ContainsKey("terminalid"))
            {
                return null;
            }

            // Get the webservice from arguments
            ICraveService webservice = this.GetCraveServiceFromArgs(args);

            // Convert the args to crave params
            CraveParams p = CraveParams.Create(args);

            TestCaseBase testcase = null;            
            switch (args["testcase"].ToUpper())
            {
                case Obymobi.WebserviceTester.Testing.TestCases.Crave.TestCaseGetClient.NAME:
                    testcase = new Obymobi.WebserviceTester.Testing.TestCases.Crave.TestCaseGetClient(webservice, p.Client, p.DeliverypointgroupId, p.DeliverypointId, p.ApplicationVersion, p.OsVersion, p.DeviceType);
                    break;
                case Obymobi.WebserviceTester.Testing.TestCases.Crave.TestCaseGetCompanies.NAME:
                    testcase = new Obymobi.WebserviceTester.Testing.TestCases.Crave.TestCaseGetCompanies(webservice, p.Client);
                    break;
                case Obymobi.WebserviceTester.Testing.TestCases.Crave.TestCaseGetCompany.NAME:
                    testcase = new Obymobi.WebserviceTester.Testing.TestCases.Crave.TestCaseGetCompany(webservice, p.Client);
                    break;
                case Obymobi.WebserviceTester.Testing.TestCases.Crave.TestCaseGetDeliverypoints.NAME:
                    testcase = new Obymobi.WebserviceTester.Testing.TestCases.Crave.TestCaseGetDeliverypoints(webservice, p.Client, p.DeliverypointgroupId);
                    break;
                case Obymobi.WebserviceTester.Testing.TestCases.Crave.TestCaseGetMenu.NAME:
                    testcase = new Obymobi.WebserviceTester.Testing.TestCases.Crave.TestCaseGetMenu(webservice, p.Client, p.DeliverypointgroupId);
                    break;
                case Obymobi.WebserviceTester.Testing.TestCases.Crave.TestCaseSaveOrder.NAME:
                    testcase = new Obymobi.WebserviceTester.Testing.TestCases.Crave.TestCaseSaveOrder(webservice, p.Client, p.CompanyId, p.WithMaxAlterations);
                    break;
            }
            return testcase;
        }

        private TestCaseBase CreateMobileTestcase(Dictionary<string, string> args)
        {
            // Get the webservice from the arguments
            IMobileService webservice = this.GetMobileServiceFromArgs(args);

            // Convert the args to mobile params
            MobileParams p = MobileParams.Create(args);

            TestCaseBase testcase = null;
            switch (args["testcase"].ToUpper())
            { 
                case Obymobi.WebserviceTester.Testing.TestCases.Mobile.TestCaseCreateCustomer.NAME:
                    testcase = new Obymobi.WebserviceTester.Testing.TestCases.Mobile.TestCaseCreateCustomer(webservice, p.EmailAddress, p.Phonenumber, p.Password, p.AnonymousAccount, p.DeviceType, p.DevicePlatform, p.DeviceModel, p.AnonymousEmail, p.AnonymousPassword, p.CustomerGuid, p.SocialmediaType);
                    break;
                case Obymobi.WebserviceTester.Testing.TestCases.Mobile.TestCaseGetCompany.NAME:
                    testcase = new Obymobi.WebserviceTester.Testing.TestCases.Mobile.TestCaseGetCompany(webservice, p.Customer, p.CompanyId, p.DeviceType, p.Tablet);
                    break;
                case Obymobi.WebserviceTester.Testing.TestCases.Mobile.TestCaseGetCompanyList.NAME:
                    testcase = new Obymobi.WebserviceTester.Testing.TestCases.Mobile.TestCaseGetCompanyList(webservice, p.Customer, p.DeviceType, p.Tablet, p.AccessCodes);
                    break;
                case Obymobi.WebserviceTester.Testing.TestCases.Mobile.TestCaseGetCustomer.NAME:
                    testcase = new Obymobi.WebserviceTester.Testing.TestCases.Mobile.TestCaseGetCustomer(webservice, p.Customer, p.Password, p.AnonymousEmail, p.AnonymousPassword, p.SocialmediaType);
                    break;
                case Obymobi.WebserviceTester.Testing.TestCases.Mobile.TestCaseGetMenu.NAME:
                    testcase = new Obymobi.WebserviceTester.Testing.TestCases.Mobile.TestCaseGetMenu(webservice, p.Customer, p.MenuId, p.DeviceType);
                    break;
                case Obymobi.WebserviceTester.Testing.TestCases.Mobile.TestCaseSaveOrder.NAME:
                    testcase = new Obymobi.WebserviceTester.Testing.TestCases.Mobile.TestCaseSaveOrder(webservice, p.Customer, p.CompanyId, p.WithMaxAlterations);
                    break;
            }
            return testcase;
        }

        private ICraveService GetCraveServiceFromArgs(Dictionary<string, string> args)
        { 
            ICraveService webservice = null;

            if (args.ContainsKey("webserviceversion"))
            {
                switch (args["webserviceversion"])
                {
                    case "2.1":
                        webservice = new Obymobi.WebserviceTester.WebServices._2._1.CraveService();
                        break;
                    case "6.0":
                        webservice = new Obymobi.WebserviceTester.WebServices._6._0.CraveService();
                        break;
                    case "7.0":
                        webservice = new Obymobi.WebserviceTester.WebServices._7._0.CraveService();
                        break;
                    case "9.0":
                        webservice = new Obymobi.WebserviceTester.WebServices._9._0.CraveService();
                        break;
                    case "10.0":
                        webservice = new Obymobi.WebserviceTester.WebServices._10._0.CraveService();
                        break;
                    case "11.0":
                        webservice = new Obymobi.WebserviceTester.WebServices._11._0.CraveService();
                        break;
                    case "12.0":
                        webservice = new Obymobi.WebserviceTester.WebServices._12._0.CraveService();
                        break;
                    case "13.0":
                        webservice = new Obymobi.WebserviceTester.WebServices._13._0.CraveService();
                        break;
                    case "14.0":
                        webservice = new Obymobi.WebserviceTester.WebServices._14._0.CraveService();
                        break;
                    case "15.0":
                        webservice = new Obymobi.WebserviceTester.WebServices._15._0.CraveService();
                        break;
                }                
            }

            return webservice;
        }

        private IMobileService GetMobileServiceFromArgs(Dictionary<string, string> args)
        {
            IMobileService webservice = null;

            if (args.ContainsKey("webserviceversion"))
            {
                switch (args["webserviceversion"])
                {
                    case "10.0":
                        webservice = new Obymobi.WebserviceTester.WebServices._10._0.MobileService();
                        break;
                    case "11.0":
                        webservice = new Obymobi.WebserviceTester.WebServices._11._0.MobileService();
                        break;
                    case "12.0":
                        webservice = new Obymobi.WebserviceTester.WebServices._12._0.MobileService();
                        break;
                    case "13.0":
                        webservice = new Obymobi.WebserviceTester.WebServices._13._0.MobileService();
                        break;
                    case "14.0":
                        webservice = new Obymobi.WebserviceTester.WebServices._14._0.MobileService();
                        break;
                    case "15.0":
                        webservice = new Obymobi.WebserviceTester.WebServices._15._0.MobileService();
                        break;
                }
            }

            return webservice;
        }
        
        #endregion
    }
}
