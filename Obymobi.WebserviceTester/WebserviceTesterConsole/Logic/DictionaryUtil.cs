﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebserviceTesterConsole.Logic
{
    public class DictionaryUtil
    {
        public static Dictionary<string, string> PairArguments(string[] arguments)
        {
            Dictionary<string, string> pairDictionary = new Dictionary<string, string>();

            for (int i = 0; i < arguments.Length; i++)
            {
                string key = arguments[i].Replace("-", "").ToLower();

                if (++i < arguments.Length)
                {
                    string value = arguments[i].ToLower();
                    pairDictionary.Add(key, value);                    
                }
            }

            return pairDictionary;
        }
    }
}
