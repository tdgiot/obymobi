﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Windows.Forms;
using Dionysos;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;
using WebserviceTester.Controls.Items;

namespace WebserviceTester.Controls
{
    public static class ComboxBoxUtil
    {
        public static void SelectedValue(this ComboBox cb, object value)
        {
            int index = -1;

            for (int i = 0; i < cb.Items.Count; i++)
            {
                object item = cb.Items[i];
                if (item is ComboboxItem)
                {
                    ComboboxItem comboboxItem = (ComboboxItem)item;
                    if (comboboxItem.Value == (int)value)
                    {
                        index = i;
                        break;
                    }
                }
                else if (item is ComboboxItemObject)
                {
                    ComboboxItemObject comboboxItemObj = (ComboboxItemObject)item;
                    if (comboboxItemObj.Value.Equals(value))
                    {
                        index = i;
                        break;
                    }
                }       
            }

            if (index > -1)
            {
                cb.SelectedIndex = index;
            }
        }

        public static int GetSelectedValue(this ComboBox cb)
        {
            return cb.SelectedIndex > -1 ? ((ComboboxItem)cb.Items[cb.SelectedIndex]).Value : -1;
        }

        public static void BindEnumToCb(this ComboBox cb, Type enumeration)
        {
            cb.Items.Clear();

            string[] names = Enum.GetNames(enumeration);
            Array values = Enum.GetValues(enumeration);
            
            for (int i = 0; i < names.Length; i++)
            {
                ComboboxItem item = new ComboboxItem();
                item.Text = EnumUtil.GetStringValue((Enum)values.GetValue(i));
                item.Value = Convert.ToInt32(values.GetValue(i));

                cb.Items.Add(item);
            }
        }

        public static void BindEntityCollectionToCb(this ComboBox cb, string entityName, EntityField valueField, EntityField textField, EntityField sortField)
        {
            cb.Items.Clear();
            
            IEntityCollection entities = (IEntityCollection)DataFactory.EntityCollectionFactory.GetEntityCollection(entityName);
            IEntity entityInfo = (IEntity)DataFactory.EntityFactory.GetEntity(entityName);

            //if (sortField == null)
                //sortField = textField;
            
            ExcludeIncludeFieldsList includedFields = new ExcludeIncludeFieldsList(false);
            // Get distinct non-null fields
            IEnumerable<int> fields = new int[]
            {
                textField.FieldIndex, valueField.FieldIndex, sortField.FieldIndex
            }.Distinct();

            if (fields.All(field => entityInfo.Fields[field] != null))
            {
                // All database fields, so no problem fetching only these
                foreach (int fieldIndex in fields)
                {
                    includedFields.Add(entityInfo.Fields[fieldIndex]);
                }
            }
            else
            {
                // Get DatabaseFieldsAttribute from all fields
                IEnumerable<PropertyInfo> propertyInfos = fields.Select(field => entityInfo.GetType().GetProperty(entityInfo.Fields[field].CurrentValue as string));
                IEnumerable<DatabaseFieldsAttribute> databaseFieldAttributes = propertyInfos.Select(propertyInfo => propertyInfo == null ? null : propertyInfo.GetCustomAttributes(typeof(DatabaseFieldsAttribute), false).Cast<DatabaseFieldsAttribute>().SingleOrDefault());
                if (databaseFieldAttributes.All(databaseFieldAttribute => databaseFieldAttribute != null))
                {
                    // All decorated with DatabaseFieldsAttribute, so fetch only specified database fields
                    foreach (DatabaseFieldsAttribute databaseFieldAttribute in databaseFieldAttributes)
                    {
                        foreach (string databaseField in databaseFieldAttribute.DatabaseFields)
                        {
                            includedFields.Add(entityInfo.Fields[databaseField]);
                        }
                    }
                }
            }

            entities.GetMulti(null, includedFields, null);            

            cb.BindEntityCollectionToCb(entities, valueField, textField, sortField);
        }

        public static void BindEntityCollectionToCb(this ComboBox cb, IEntityCollection entityCollection, EntityField valueField, EntityField textField, EntityField sortField)
        {
            entityCollection.Sort(sortField.FieldIndex, ListSortDirection.Ascending, null);

            foreach (IEntity entity in entityCollection)
            {
                ComboboxItem item = new ComboboxItem();
                
                bool displayFieldIsString = entity.Fields[textField.FieldIndex].CurrentValue is string;
                if (displayFieldIsString)
                    item.Text = (string)entity.Fields[textField.FieldIndex].CurrentValue;
                else
                    item.Text = entity.Fields[textField.FieldIndex].CurrentValue.ToString();

                item.Value = (int)entity.Fields[valueField.FieldIndex].CurrentValue;
                cb.Items.Add(item);
            }
        }
    }
}
