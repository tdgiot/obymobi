﻿using Obymobi;
using Obymobi.Configuration;
using Obymobi.WebserviceTester;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using System.Windows.Forms;
using Dionysos.Logging;

namespace WebserviceTester
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            // Set the data providers
            Dionysos.DataFactory.EntityCollectionFactory = new Dionysos.Data.LLBLGen.LLBLGenEntityCollectionFactory();
            Dionysos.DataFactory.EntityFactory = new Dionysos.Data.LLBLGen.LLBLGenEntityFactory();
            Dionysos.DataFactory.EntityUtil = new Dionysos.Data.LLBLGen.LLBLGenEntityUtil();

            // Initialize GUI specific globals
            InitializeGlobals();

            // Initialize the global from the shared Logic.WebserviceTester
            Obymobi.WebserviceTester.Global.Initialize();
            Obymobi.WebserviceTester.Global.IsConsoleMode = false;
            
            Application.Run(new FormMain());            
        }

        private static void InitializeGlobals()
        {
            // Set application info
            Dionysos.Global.ApplicationInfo = new Dionysos.ServiceProcess.ServiceInfo();
            Dionysos.Global.ApplicationInfo.ApplicationName = "WebserviceTester";
            Dionysos.Global.ApplicationInfo.ApplicationVersion = "1.2015042201";
            Dionysos.Global.ApplicationInfo.BasePath = Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location);
            Dionysos.GlobalLight.LoggingProvider = new AsyncLoggingProvider(Path.Combine(Dionysos.Global.ApplicationInfo.BasePath, "Logs"), "CloudStatus", 14, "WebserviceTester");

            // Set root directory
            Global.RootDirectory = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);

            // Configuration provider
            var provider = new Dionysos.Configuration.XmlConfigurationProvider { ManualConfigFilePath = Path.Combine(Global.RootDirectory, "WebserviceTester.config") };
            Dionysos.Global.ConfigurationProvider = provider;
            Dionysos.Global.ConfigurationInfo.Add(new CraveCloudConfigInfo());
        }
    }
}
