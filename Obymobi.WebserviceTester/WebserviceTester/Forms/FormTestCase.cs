﻿using System;
using System.Windows.Forms;
using Obymobi.WebserviceTester.Data.Models;

namespace WebserviceTester.Forms
{
    public partial class FormTestCase : Form
    {
        private TestRun TestRun;
        private TestCase TestCase;
        private Guid Identifier;

        public FormTestCase(TestRun testRun, Guid identifier)
        {
            InitializeComponent();
            this.TestRun = testRun;
            this.Identifier = identifier;
            this.TestCase = testRun.TestCases.Get(this.Identifier) != null ? testRun.TestCases.Get(this.Identifier).Clone() : new TestCase(this.Identifier);
            this.SetGui();
        }

        private void SetGui()
        {
            if (this.TestCase != null)
            {
                this.lblStarted.Text = this.TestCase.Started;
                this.lblProcessed.Text = this.TestCase.Processed;
                this.lblElapsed.Text = string.Format("{0} ms", this.TestCase.Elapsed);

                this.ucApiRequest.Update(this.TestCase.Request);
                this.ucApiResponse.Update(this.TestCase.Response);
            }
        }
    }
}
