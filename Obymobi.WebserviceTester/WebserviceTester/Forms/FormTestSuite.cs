﻿using Dionysos;
using Obymobi.WebserviceTester.Data.Managers;
using Obymobi.WebserviceTester.Data.Models;
using Obymobi.WebserviceTester.Enums;
using Obymobi.WebserviceTester.Extensions;
using System;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using WebserviceTester.Controls;

namespace WebserviceTester.Forms
{
    public partial class FormTestSuite : Form
    {
        private Guid Identifier;
        private TestSuiteCollection TestSuites;        
        private TestSuite TestSuite;

        public Action<Guid, TestSuite> ActionUpdateTestSuite;

        public FormTestSuite(TestSuiteCollection testSuites, Guid identifier)
        {
            InitializeComponent();
            this.TestSuites = testSuites;
            this.Identifier = identifier;
            this.TestSuite = this.TestSuites.Get(this.Identifier) != null ? this.TestSuites.Get(this.Identifier).Clone() : new TestSuite(identifier);
            this.LoadUserControls();
            this.SetGui();
        }        

        private void LoadUserControls()
        {            
            this.cbApiVersion.BindEnumToCb(typeof(ApiVersionType));
            this.cbPredefinedTestSuite.BindEnumToCb(typeof(TemplateApiRequestType));            
        }

        private void SetGui()
        {
            this.tbName.Text = this.TestSuite.Name;
            this.tbApiBaseUrl.Text = this.TestSuite.ApiUrl;
            this.cbApiVersion.SelectedValue(this.TestSuite.ApiVersionType);
            this.gvRequests.DataSource = this.CreateTableForRequests(this.TestSuite.Requests);
        }        

        private void Save()
        {
            if (this.IsValid())
            {
                this.TestSuite.Name = this.tbName.Text;
                this.TestSuite.ApiUrl = this.tbApiBaseUrl.Text;
                this.TestSuite.ApiVersionType = this.cbApiVersion.GetSelectedValue();

                this.ActionUpdateTestSuite?.Invoke(this.Identifier, this.TestSuite);
                this.Close();                
            }            
        }

        private DataTable CreateTableForRequests(ApiRequestCollection requests)
        {
            DataTable table = new DataTable();
            DataColumn clmTestcase = new DataColumn("Api Request");
            table.Columns.Add(clmTestcase);
            for (int i = 0; i < requests.Count; i++)
            {
                DataRow row = table.NewRow();
                row[clmTestcase] = requests[i].ApiMethodType.ToEnum<ApiMethodType>();
                table.Rows.Add(row);
            }
            return table;
        }

        private void UpdateApiRequest(ApiRequest request)
        {
            this.TestSuite.AddOrUpdateRequest(request);            
            this.SetGui();
        }

        private void DeleteApiRequest(ApiRequest request)
        {
            this.TestSuite.DeleteRequest(request);
            this.SetGui();
        }        

        private bool IsValid()
        {
            if (this.tbName.Text.IsNullOrWhiteSpace())
            {
                MessageBox.Show("Enter a name");
                return false;
            }
            else if (this.tbApiBaseUrl.Text.IsNullOrWhiteSpace())
            {
                MessageBox.Show("Enter an API base url");
                return false;
            }
            else if (this.cbApiVersion.SelectedIndex < 0)
            {
                MessageBox.Show("Select an API version");
                return false;
            }            
            return true;
        }

        private void btSave_Click(object sender, EventArgs e)
        {
            this.Save();
        }

        private void btNewRequest_Click(object sender, EventArgs e)
        {
            FormApiRequest form = new FormApiRequest(this.TestSuite, this.Identifier, Guid.NewGuid());
            form.ActionUpdateApiRequest = this.UpdateApiRequest;            
            form.Show();
        }        

        private void btEditRequest_Click(object sender, EventArgs e)
        {
            if (this.gvRequests.SelectedRows.Count > 0)
            {
                ApiRequest request = this.TestSuite.Requests[this.gvRequests.SelectedRows[0].Index];

                FormApiRequest form = new FormApiRequest(this.TestSuite, this.Identifier, request.Identifier);
                form.ActionUpdateApiRequest = this.UpdateApiRequest;
                form.Show();
            }            
        }

        private void btDeleteRequest_Click(object sender, EventArgs e)
        {
            if (this.gvRequests.SelectedRows.Count > 0)
            {
                ApiRequest request = this.TestSuite.Requests[this.gvRequests.SelectedRows[0].Index];
                this.DeleteApiRequest(request);
                this.SetGui();
            }
        }

        private void btCreateFromTemplate_Click(object sender, EventArgs e)
        {
            if (this.cbPredefinedTestSuite.SelectedIndex > -1)
            {
                this.TestSuite.Name = this.tbName.Text;
                this.TestSuite.ApiUrl = this.tbApiBaseUrl.Text;
                this.TestSuite.ApiVersionType = this.cbApiVersion.GetSelectedValue();

                TemplateApiRequestType type = this.cbPredefinedTestSuite.GetSelectedValue().ToEnum<TemplateApiRequestType>();
                switch (type)
                {
                    case TemplateApiRequestType.EmenuStartup:
                        FormTemplateEmenuStartup form = new FormTemplateEmenuStartup(this.TestSuites, this.Identifier);
                        form.ActionUpdateApiRequest = this.UpdateApiRequest;
                        form.Show();
                        break;
                }
            }
        }
    }
}