﻿using Obymobi.WebserviceTester.Data.Models;
using System;
using System.Windows.Forms;

namespace WebserviceTester.Forms
{
    public partial class FormApiRequest : Form
    {
        private TestSuite TestSuite;
        private Guid TestSuiteIdentifier;
        private Guid Identifier;

        public Action<ApiRequest> ActionUpdateApiRequest;

        public FormApiRequest(TestSuite testSuite, Guid testSuiteIdentifier, Guid identifier)
        {
            InitializeComponent();
            this.TestSuite = testSuite;
            this.TestSuiteIdentifier = testSuiteIdentifier;
            this.Identifier = identifier;
            this.SetGui();
        }

        private void SetGui()
        {
            this.ucApiRequest.Update(this.TestSuite.Requests.Get(this.Identifier));
        }        

        private void Save()
        {
            if (this.ucApiRequest.Save())
            {
                this.Close();
            }
        }

        private void btSave_Click(object sender, EventArgs e)
        {
            this.Save();
        }        
    }
}
