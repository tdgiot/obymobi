﻿using Obymobi.Data.EntityClasses;
using Obymobi.WebserviceTester.Data.Models;
using Obymobi.WebserviceTester.Enums;
using Obymobi.WebserviceTester.Extensions;
using Obymobi.WebserviceTester.Logic.Test.Testruns;
using System;
using System.Windows.Forms;

namespace WebserviceTester.Forms
{
    public partial class FormTemplateEmenuStartup : Form
    {
        private TestSuiteCollection TestSuites;
        private Guid Identifier;

        public Action<ApiRequest> ActionUpdateApiRequest;

        public FormTemplateEmenuStartup(TestSuiteCollection testSuites, Guid identifier)
        {
            InitializeComponent();
            this.TestSuites = testSuites;
            this.Identifier = identifier;
            this.ucSelectClient.SetGui();
        }

        private bool IsValid()
        {
            if (this.ucSelectClient.Client == null)
            {
                MessageBox.Show("No client selected");
                return false;
            }
            return true;
        }

        private void Add()
        {
            ClientEntity client = this.ucSelectClient.Client;

            ApiRequestCollection requests = TemplateApiRequestCreator.Create(TemplateApiRequestType.EmenuStartup);
            foreach (ApiRequest request in requests)
            {
                request.ApplyClient(client);
                this.ActionUpdateApiRequest?.Invoke(request);
            }
            this.Close();
        }        

        private void btAdd_Click(object sender, EventArgs e)
        {
            if (this.IsValid())
            {
                this.Add();
            }            
        }        
    }
}
