﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WebserviceTester.Forms
{
    public partial class FormRawResponse : Form
    {
        public FormRawResponse(string rawResponse)
        {
            InitializeComponent();
            this.tbRaw.Text = rawResponse;
        }
    }
}
