﻿namespace WebserviceTester.Forms
{
    partial class FormTestSuite
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.cbApiVersion = new System.Windows.Forms.ComboBox();
            this.btCreateFromTemplate = new System.Windows.Forms.Button();
            this.btDeleteTestcase = new System.Windows.Forms.Button();
            this.btEditTestcase = new System.Windows.Forms.Button();
            this.btNewTestcase = new System.Windows.Forms.Button();
            this.gvRequests = new System.Windows.Forms.DataGridView();
            this.cbPredefinedTestSuite = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.tbName = new System.Windows.Forms.TextBox();
            this.lblName = new System.Windows.Forms.Label();
            this.tbApiBaseUrl = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btSave = new System.Windows.Forms.Button();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gvRequests)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Location = new System.Drawing.Point(12, 12);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(514, 422);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.cbApiVersion);
            this.tabPage1.Controls.Add(this.btCreateFromTemplate);
            this.tabPage1.Controls.Add(this.btDeleteTestcase);
            this.tabPage1.Controls.Add(this.btEditTestcase);
            this.tabPage1.Controls.Add(this.btNewTestcase);
            this.tabPage1.Controls.Add(this.gvRequests);
            this.tabPage1.Controls.Add(this.cbPredefinedTestSuite);
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Controls.Add(this.tbName);
            this.tabPage1.Controls.Add(this.lblName);
            this.tabPage1.Controls.Add(this.tbApiBaseUrl);
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(506, 396);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Settings";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // cbApiVersion
            // 
            this.cbApiVersion.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbApiVersion.FormattingEnabled = true;
            this.cbApiVersion.Location = new System.Drawing.Point(363, 41);
            this.cbApiVersion.Name = "cbApiVersion";
            this.cbApiVersion.Size = new System.Drawing.Size(54, 21);
            this.cbApiVersion.TabIndex = 3;
            // 
            // btCreateFromTemplate
            // 
            this.btCreateFromTemplate.Location = new System.Drawing.Point(363, 88);
            this.btCreateFromTemplate.Name = "btCreateFromTemplate";
            this.btCreateFromTemplate.Size = new System.Drawing.Size(54, 21);
            this.btCreateFromTemplate.TabIndex = 5;
            this.btCreateFromTemplate.Text = "Add";
            this.btCreateFromTemplate.UseVisualStyleBackColor = true;
            this.btCreateFromTemplate.Click += new System.EventHandler(this.btCreateFromTemplate_Click);
            // 
            // btDeleteTestcase
            // 
            this.btDeleteTestcase.Location = new System.Drawing.Point(423, 170);
            this.btDeleteTestcase.Name = "btDeleteTestcase";
            this.btDeleteTestcase.Size = new System.Drawing.Size(75, 23);
            this.btDeleteTestcase.TabIndex = 8;
            this.btDeleteTestcase.Text = "Delete";
            this.btDeleteTestcase.UseVisualStyleBackColor = true;
            this.btDeleteTestcase.Click += new System.EventHandler(this.btDeleteRequest_Click);
            // 
            // btEditTestcase
            // 
            this.btEditTestcase.Location = new System.Drawing.Point(423, 141);
            this.btEditTestcase.Name = "btEditTestcase";
            this.btEditTestcase.Size = new System.Drawing.Size(75, 23);
            this.btEditTestcase.TabIndex = 7;
            this.btEditTestcase.Text = "Edit";
            this.btEditTestcase.UseVisualStyleBackColor = true;
            this.btEditTestcase.Click += new System.EventHandler(this.btEditRequest_Click);
            // 
            // btNewTestcase
            // 
            this.btNewTestcase.Location = new System.Drawing.Point(423, 112);
            this.btNewTestcase.Name = "btNewTestcase";
            this.btNewTestcase.Size = new System.Drawing.Size(75, 23);
            this.btNewTestcase.TabIndex = 6;
            this.btNewTestcase.Text = "New";
            this.btNewTestcase.UseVisualStyleBackColor = true;
            this.btNewTestcase.Click += new System.EventHandler(this.btNewRequest_Click);
            // 
            // gvRequests
            // 
            this.gvRequests.AllowUserToAddRows = false;
            this.gvRequests.AllowUserToDeleteRows = false;
            this.gvRequests.AllowUserToResizeColumns = false;
            this.gvRequests.AllowUserToResizeRows = false;
            this.gvRequests.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.gvRequests.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gvRequests.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.gvRequests.Location = new System.Drawing.Point(9, 112);
            this.gvRequests.Name = "gvRequests";
            this.gvRequests.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.gvRequests.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.gvRequests.Size = new System.Drawing.Size(408, 276);
            this.gvRequests.TabIndex = 9;
            //this.gvRequests.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.gvRequests_CellClick);
            // 
            // cbPredefinedTestSuite
            // 
            this.cbPredefinedTestSuite.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbPredefinedTestSuite.FormattingEnabled = true;
            this.cbPredefinedTestSuite.Location = new System.Drawing.Point(123, 88);
            this.cbPredefinedTestSuite.Name = "cbPredefinedTestSuite";
            this.cbPredefinedTestSuite.Size = new System.Drawing.Size(234, 21);
            this.cbPredefinedTestSuite.TabIndex = 4;
            //this.cbPredefinedTestSuite.SelectedIndexChanged += new System.EventHandler(this.cbPredefinedTestSuite_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 92);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(56, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "Templates";
            // 
            // tbName
            // 
            this.tbName.Location = new System.Drawing.Point(124, 15);
            this.tbName.Name = "tbName";
            this.tbName.Size = new System.Drawing.Size(233, 20);
            this.tbName.TabIndex = 1;
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.Location = new System.Drawing.Point(6, 18);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(35, 13);
            this.lblName.TabIndex = 2;
            this.lblName.Text = "Name";
            // 
            // tbApiBaseUrl
            // 
            this.tbApiBaseUrl.Location = new System.Drawing.Point(123, 41);
            this.tbApiBaseUrl.Name = "tbApiBaseUrl";
            this.tbApiBaseUrl.Size = new System.Drawing.Size(234, 20);
            this.tbApiBaseUrl.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 44);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(64, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "API base url";
            // 
            // btSave
            // 
            this.btSave.Location = new System.Drawing.Point(451, 440);
            this.btSave.Name = "btSave";
            this.btSave.Size = new System.Drawing.Size(75, 23);
            this.btSave.TabIndex = 9;
            this.btSave.Text = "Save";
            this.btSave.UseVisualStyleBackColor = true;
            this.btSave.Click += new System.EventHandler(this.btSave_Click);
            // 
            // FormTestSuite
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(535, 473);
            this.Controls.Add(this.btSave);
            this.Controls.Add(this.tabControl1);
            this.Name = "FormTestSuite";
            this.Text = "Test Suite";
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gvRequests)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Button btSave;
        private System.Windows.Forms.TextBox tbName;
        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.TextBox tbApiBaseUrl;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btCreateFromTemplate;
        private System.Windows.Forms.Button btDeleteTestcase;
        private System.Windows.Forms.Button btEditTestcase;
        private System.Windows.Forms.Button btNewTestcase;
        private System.Windows.Forms.DataGridView gvRequests;
        private System.Windows.Forms.ComboBox cbPredefinedTestSuite;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cbApiVersion;
    }
}