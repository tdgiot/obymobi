﻿using System;
using System.Windows.Forms;
using Obymobi.WebserviceTester.Data.Managers;
using Obymobi.WebserviceTester.Data.Models;
using Obymobi.WebserviceTester.Enums;

namespace WebserviceTester
{
    public partial class FormMain : System.Windows.Forms.Form
    {
        #region Fields

        private TestProvider Provider;

        #endregion

        #region Methods

        public FormMain()
        {
            InitializeComponent();

            this.Provider = new TestProvider();

            this.trFirst.ActionUpdateTestRun = this.Provider.UpdateTestRun;
            this.trFirst.ActionOnTestRunStarted = this.OnTestRunStarted;
            this.trFirst.ActionOnTestRunProcessed = this.OnTestRunProcessed;
            this.trFirst.ActionOnTestCaseStarted = this.OnTestCaseStarted;
            this.trFirst.ActionOnTestCaseProcessed = this.OnTestCaseProcessed;

            this.trSecond.ActionUpdateTestRun = this.Provider.UpdateTestRun;
            this.trSecond.ActionOnTestRunStarted = this.OnTestRunStarted;
            this.trSecond.ActionOnTestRunProcessed = this.OnTestRunProcessed;
            this.trSecond.ActionOnTestCaseStarted = this.OnTestCaseStarted;
            this.trSecond.ActionOnTestCaseProcessed = this.OnTestCaseProcessed;

            this.SetGui();
        }        

        private void SetGui()
        {
            this.ucTestSuite.Update(this.Provider.TestSuiteCollection);
            this.ucTestSuite.ActionUpdateTestSuite = this.UpdateTestSuite;
            this.ucTestSuite.ActionDeleteTestSuite = this.DeleteTestSuite;

            this.trFirst.Load(this.Provider.TestSuiteCollection, this.Provider.TestRunCollection);
            this.trSecond.Load(this.Provider.TestSuiteCollection, this.Provider.TestRunCollection);
        }

        public void UpdateTestSuite(Guid testSuiteIdentifier, TestSuite testSuite)
        {
            this.Provider.UpdateTestSuite(testSuiteIdentifier, testSuite);
            this.SetGui();
        }

        public void DeleteTestSuite(Guid identifier)
        {
            this.Provider.DeleteTestSuite(identifier);
            this.SetGui();
        }

        public void OnTestRunStarted(Guid testRunIdentifier)
        {
            TestRun testRun = this.Provider.TestRunCollection.Get(testRunIdentifier);
            this.WriteToLog("Started: {0}", testRun.Name);
            this.WriteToLog("On: {0}", testRun.Started);
        }

        public void OnTestRunProcessed(Guid testRunIdentifier)
        {
            TestRun testRun = this.Provider.TestRunCollection.Get(testRunIdentifier);
            this.WriteToLog("Processed: {0}", testRun.Name);            
            MessageBox.Show("Done");
        }

        public void OnTestCaseStarted(Guid testRunIdentifier, Guid testCaseGuid)
        {
            TestCase testCase = this.Provider.TestRunCollection.Get(testRunIdentifier).TestCases.Get(testCaseGuid);
            this.WriteToLog("Active testcase: {0}", testCase.Request.ApiMethodType.ToEnum<ApiMethodType>().ToString());
        }

        public void OnTestCaseProcessed(Guid testRunIdentifier, Guid testCaseGuid)
        {
            TestCase testCase = this.Provider.TestRunCollection.Get(testRunIdentifier).TestCases.Get(testCaseGuid);
            this.WriteToLog("Processed {0} in {1} ms", testCase.Response.ResultCode == 100 ? "successfully" : "failed", testCase.Elapsed);
        }

        private void WriteToLog(string contents, params object[] args)
        {
            if (this.tbOutput.InvokeRequired)
            {
                this.tbOutput.Invoke(new MethodInvoker(() => WriteToLog(contents, args)));
                return;
            }
            this.tbOutput.Text = string.Format(contents, args) + "\r\n" + this.tbOutput.Text;
        }

        #endregion

        #region Event Handlers                
        
        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Provider.SaveToDisk();
        }

        private void btCompare_Click(object sender, EventArgs e)
        {

        }

        #endregion        
    }
}
