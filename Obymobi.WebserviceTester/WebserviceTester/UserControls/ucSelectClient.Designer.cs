﻿namespace WebserviceTester.UserControls
{
    partial class ucSelectClient
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btCreate = new System.Windows.Forms.Button();
            this.cbDeliverypoint = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.cbDeliverypointgroup = new System.Windows.Forms.ComboBox();
            this.cbCompany = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.cbClient = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btCreate
            // 
            this.btCreate.Location = new System.Drawing.Point(319, 90);
            this.btCreate.Name = "btCreate";
            this.btCreate.Size = new System.Drawing.Size(51, 22);
            this.btCreate.TabIndex = 14;
            this.btCreate.Text = "Create";
            this.btCreate.UseVisualStyleBackColor = true;
            this.btCreate.Click += new System.EventHandler(this.btCreate_Click);
            // 
            // cbDeliverypoint
            // 
            this.cbDeliverypoint.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbDeliverypoint.FormattingEnabled = true;
            this.cbDeliverypoint.Location = new System.Drawing.Point(119, 64);
            this.cbDeliverypoint.Name = "cbDeliverypoint";
            this.cbDeliverypoint.Size = new System.Drawing.Size(251, 21);
            this.cbDeliverypoint.TabIndex = 22;
            this.cbDeliverypoint.SelectedIndexChanged += new System.EventHandler(this.cbDeliverypoint_SelectedIndexChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 67);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(68, 13);
            this.label3.TabIndex = 21;
            this.label3.Text = "Deliverypoint";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 40);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(95, 13);
            this.label1.TabIndex = 20;
            this.label1.Text = "Deliverypointgroup";
            // 
            // cbDeliverypointgroup
            // 
            this.cbDeliverypointgroup.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbDeliverypointgroup.FormattingEnabled = true;
            this.cbDeliverypointgroup.Location = new System.Drawing.Point(119, 37);
            this.cbDeliverypointgroup.Name = "cbDeliverypointgroup";
            this.cbDeliverypointgroup.Size = new System.Drawing.Size(251, 21);
            this.cbDeliverypointgroup.TabIndex = 19;
            this.cbDeliverypointgroup.SelectedIndexChanged += new System.EventHandler(this.cbDeliverypointgroup_SelectedIndexChanged);
            // 
            // cbCompany
            // 
            this.cbCompany.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbCompany.FormattingEnabled = true;
            this.cbCompany.Location = new System.Drawing.Point(119, 10);
            this.cbCompany.Name = "cbCompany";
            this.cbCompany.Size = new System.Drawing.Size(251, 21);
            this.cbCompany.TabIndex = 18;
            this.cbCompany.SelectedIndexChanged += new System.EventHandler(this.cbCompany_SelectedIndexChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(3, 13);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(51, 13);
            this.label5.TabIndex = 17;
            this.label5.Text = "Company";
            // 
            // cbClient
            // 
            this.cbClient.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbClient.FormattingEnabled = true;
            this.cbClient.Location = new System.Drawing.Point(119, 91);
            this.cbClient.Name = "cbClient";
            this.cbClient.Size = new System.Drawing.Size(194, 21);
            this.cbClient.TabIndex = 16;
            this.cbClient.SelectedIndexChanged += new System.EventHandler(this.cbClient_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 94);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(33, 13);
            this.label2.TabIndex = 15;
            this.label2.Text = "Client";
            // 
            // ucSelectClient
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.btCreate);
            this.Controls.Add(this.cbDeliverypoint);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cbDeliverypointgroup);
            this.Controls.Add(this.cbCompany);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.cbClient);
            this.Controls.Add(this.label2);
            this.Name = "ucSelectClient";
            this.Size = new System.Drawing.Size(377, 123);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btCreate;
        private System.Windows.Forms.ComboBox cbDeliverypoint;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cbDeliverypointgroup;
        private System.Windows.Forms.ComboBox cbCompany;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox cbClient;
        private System.Windows.Forms.Label label2;
    }
}
