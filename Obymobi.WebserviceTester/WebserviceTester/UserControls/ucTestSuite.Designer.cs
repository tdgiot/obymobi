﻿namespace WebserviceTester.UserControls
{
    partial class ucTestSuite
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblApiVersion = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lblCreated = new System.Windows.Forms.Label();
            this.lblBaseUrl = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.sfasdf = new System.Windows.Forms.Label();
            this.btDelete = new System.Windows.Forms.Button();
            this.btEdit = new System.Windows.Forms.Button();
            this.btNew = new System.Windows.Forms.Button();
            this.gvTestSuites = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.gvTestSuites)).BeginInit();
            this.SuspendLayout();
            // 
            // lblApiVersion
            // 
            this.lblApiVersion.AutoSize = true;
            this.lblApiVersion.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblApiVersion.Location = new System.Drawing.Point(514, 37);
            this.lblApiVersion.Name = "lblApiVersion";
            this.lblApiVersion.Size = new System.Drawing.Size(89, 13);
            this.lblApiVersion.TabIndex = 21;
            this.lblApiVersion.Text = "None selected";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(407, 37);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(61, 13);
            this.label3.TabIndex = 20;
            this.label3.Text = "API version";
            // 
            // lblCreated
            // 
            this.lblCreated.AutoSize = true;
            this.lblCreated.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCreated.Location = new System.Drawing.Point(514, 66);
            this.lblCreated.Name = "lblCreated";
            this.lblCreated.Size = new System.Drawing.Size(89, 13);
            this.lblCreated.TabIndex = 19;
            this.lblCreated.Text = "None selected";
            // 
            // lblBaseUrl
            // 
            this.lblBaseUrl.AutoSize = true;
            this.lblBaseUrl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBaseUrl.Location = new System.Drawing.Point(514, 8);
            this.lblBaseUrl.Name = "lblBaseUrl";
            this.lblBaseUrl.Size = new System.Drawing.Size(89, 13);
            this.lblBaseUrl.TabIndex = 18;
            this.lblBaseUrl.Text = "None selected";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(407, 8);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(67, 13);
            this.label2.TabIndex = 17;
            this.label2.Text = "API base url:";
            // 
            // sfasdf
            // 
            this.sfasdf.AutoSize = true;
            this.sfasdf.Location = new System.Drawing.Point(407, 66);
            this.sfasdf.Name = "sfasdf";
            this.sfasdf.Size = new System.Drawing.Size(47, 13);
            this.sfasdf.TabIndex = 16;
            this.sfasdf.Text = "Created:";
            // 
            // btDelete
            // 
            this.btDelete.Location = new System.Drawing.Point(299, 61);
            this.btDelete.Name = "btDelete";
            this.btDelete.Size = new System.Drawing.Size(75, 23);
            this.btDelete.TabIndex = 15;
            this.btDelete.Text = "Delete";
            this.btDelete.UseVisualStyleBackColor = true;
            this.btDelete.Click += new System.EventHandler(this.btDelete_Click);
            // 
            // btEdit
            // 
            this.btEdit.Location = new System.Drawing.Point(299, 32);
            this.btEdit.Name = "btEdit";
            this.btEdit.Size = new System.Drawing.Size(75, 23);
            this.btEdit.TabIndex = 14;
            this.btEdit.Text = "Edit";
            this.btEdit.UseVisualStyleBackColor = true;
            this.btEdit.Click += new System.EventHandler(this.btEdit_Click);
            // 
            // btNew
            // 
            this.btNew.Location = new System.Drawing.Point(299, 3);
            this.btNew.Name = "btNew";
            this.btNew.Size = new System.Drawing.Size(75, 23);
            this.btNew.TabIndex = 13;
            this.btNew.Text = "New";
            this.btNew.UseVisualStyleBackColor = true;
            this.btNew.Click += new System.EventHandler(this.btNew_Click);
            // 
            // gvTestSuites
            // 
            this.gvTestSuites.AllowUserToAddRows = false;
            this.gvTestSuites.AllowUserToDeleteRows = false;
            this.gvTestSuites.AllowUserToResizeColumns = false;
            this.gvTestSuites.AllowUserToResizeRows = false;
            this.gvTestSuites.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.gvTestSuites.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gvTestSuites.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.gvTestSuites.Location = new System.Drawing.Point(3, 3);
            this.gvTestSuites.Name = "gvTestSuites";
            this.gvTestSuites.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.gvTestSuites.Size = new System.Drawing.Size(290, 168);
            this.gvTestSuites.TabIndex = 12;
            this.gvTestSuites.Click += new System.EventHandler(this.gvTestSuites_Click);
            // 
            // ucTestSuite
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.lblApiVersion);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.lblCreated);
            this.Controls.Add(this.lblBaseUrl);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.sfasdf);
            this.Controls.Add(this.btDelete);
            this.Controls.Add(this.btEdit);
            this.Controls.Add(this.btNew);
            this.Controls.Add(this.gvTestSuites);
            this.Name = "ucTestSuite";
            this.Size = new System.Drawing.Size(611, 176);
            ((System.ComponentModel.ISupportInitialize)(this.gvTestSuites)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblApiVersion;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lblCreated;
        private System.Windows.Forms.Label lblBaseUrl;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label sfasdf;
        private System.Windows.Forms.Button btDelete;
        private System.Windows.Forms.Button btEdit;
        private System.Windows.Forms.Button btNew;
        private System.Windows.Forms.DataGridView gvTestSuites;
    }
}
