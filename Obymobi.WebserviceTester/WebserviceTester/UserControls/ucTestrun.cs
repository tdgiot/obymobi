﻿using System;
using System.Data;
using System.Windows.Forms;
using Obymobi.WebserviceTester.Enums;
using WebserviceTester.Controls.Items;
using Dionysos;
using Obymobi.WebserviceTester.Data.Models;
using Obymobi.WebserviceTester.Extensions;
using Obymobi.WebserviceTester.Logic.Test;
using WebserviceTester.Controls;
using WebserviceTester.Forms;

namespace WebserviceTester.UserControls
{
    public partial class ucTestrun : UserControl
    {
        private TestSuiteCollection TestSuites;
        private TestRunCollection TestRuns;

        public Action<Guid, TestRun> ActionUpdateTestRun;
        public Action<Guid> ActionOnTestRunStarted;
        public Action<Guid> ActionOnTestRunProcessed;
        public Action<Guid, Guid> ActionOnTestCaseStarted;
        public Action<Guid, Guid> ActionOnTestCaseProcessed;

        public ucTestrun()
        {
            InitializeComponent();        
        }

        public new void Load(TestSuiteCollection testSuites, TestRunCollection testRuns)
        {
            this.TestSuites = testSuites;
            this.TestRuns = testRuns;
            this.RenderTestSuites();
        }

        private void RenderTestSuites()
        {
            this.cbTestSuite.Items.Clear();
            foreach (TestSuite suite in this.TestSuites)
            {
                this.cbTestSuite.Items.Add(new ComboboxItemObject(suite.Name, suite.Identifier));
            }
        }

        private void RenderTestRuns(Guid testSuiteIdentifier)
        {
            this.cbTestRun.Items.Clear();
            foreach (TestRun testRun in this.TestRuns.GetByTestSuiteIdentifier(testSuiteIdentifier))
            {
                this.cbTestRun.Items.Add(new ComboboxItemObject(testRun.Created, testRun.Identifier));
            }
        }

        private void RenderTestcases(TestCaseCollection testCases)
        {
            if (this.gvTestcases.InvokeRequired)
            {
                this.gvTestcases.Invoke(new MethodInvoker(() => RenderTestcases(testCases)));
                return;
            }
            this.gvTestcases.DataSource = this.CreateTableForTestCases(testCases);
        }

        private void Run()
        {
            TestRun testRun = this.cbTestRun.SelectedIndex > -1 ? this.TestRuns[this.cbTestRun.SelectedIndex] : null;
            if (testRun == null)
            {
                TestSuite suite = this.TestSuites[this.cbTestSuite.SelectedIndex];
                testRun = suite.ToTestRun();
                this.ActionUpdateTestRun?.Invoke(testRun.Identifier, testRun);
                this.RenderTestRuns(suite.Identifier);
                this.cbTestRun.SelectedValue(testRun.Identifier);
            }
            
            TestrunExecutor executor = new TestrunExecutor(this.TestRuns, testRun.Identifier);
            executor.OnTestRunStarted = this.OnTestRunStarted;
            executor.OnTestRunProcessed = this.OnTestRunProcessed;
            executor.OnTestCaseStarted = this.OnTestCaseStarted;
            executor.OnTestCaseProcessed = this.OnTestCaseProcessed;
            executor.Execute();
        }

        public void OnTestRunStarted(Guid testRunGuid)
        {
            this.ActionOnTestRunStarted(testRunGuid);
        }

        public void OnTestRunProcessed(Guid testRunGuid)
        {
            this.ActionOnTestRunProcessed(testRunGuid);
        }

        public void OnTestCaseStarted(Guid testRunGuid, Guid testCaseGuid)
        {
            this.ActionOnTestCaseStarted(testRunGuid, testCaseGuid);
        }

        public void OnTestCaseProcessed(Guid testRunGuid, Guid testCaseGuid)
        {
            this.ActionOnTestCaseProcessed(testRunGuid, testCaseGuid);                        
            this.RenderTestcases(this.TestRuns.Get(testRunGuid).TestCases);
        }        

        private bool IsValid()
        {
            if (this.cbTestSuite.SelectedIndex < 0)
            {
                MessageBox.Show("Select a test suite");
                return false;
            }
            return true;
        }

        private DataTable CreateTableForTestCases(TestCaseCollection testCases)
        {
            DataTable table = new DataTable();        
            DataColumn clmTestcase = new DataColumn("Testcase");
            DataColumn clmResult = new DataColumn("Result");
            table.Columns.Add(clmTestcase);
            table.Columns.Add(clmResult);
            for (int i = 0; i < testCases.Count; i++)
            {
                DataRow row = table.NewRow();
                row[clmTestcase] = testCases[i].Request.ApiMethodType.ToEnum<ApiMethodType>();
                row[clmResult] = testCases[i].Processed.IsNullOrWhiteSpace() ? "" : testCases[i].Response.ResultCode == 100 ? "Success" : "Failure";
                table.Rows.Add(row);
            }
            return table;
        }                               

        private void cbTestSuite_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.cbTestSuite.SelectedIndex > -1)
            {
                TestSuite suite = this.TestSuites[this.cbTestSuite.SelectedIndex];                
                this.RenderTestRuns(suite.Identifier);
            }
        }

        private void cbTestRun_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.cbTestRun.SelectedIndex > -1)
            {
                ComboboxItemObject item = (ComboboxItemObject)this.cbTestRun.Items[this.cbTestRun.SelectedIndex];
                TestRun testRun = this.TestRuns.Get((Guid)item.Value);
                if (testRun != null)
                {
                    this.RenderTestcases(testRun.TestCases);
                }
            }
        }

        private void btRun_Click(object sender, EventArgs e)
        {
            if (this.IsValid())
            {
                this.Run();
            }
        }

        private void gvTestcases_DoubleClick(object sender, EventArgs e)
        {
            if (this.gvTestcases.SelectedRows.Count > 0)
            {
                TestRun testRun = this.cbTestRun.SelectedIndex > -1 ? this.TestRuns[this.cbTestRun.SelectedIndex] : null;
                if (testRun != null)
                {
                    TestCase testCase = testRun.TestCases[this.gvTestcases.SelectedRows[0].Index];

                    FormTestCase form = new FormTestCase(testRun, testCase.Identifier);
                    form.Show();
                }                
            }
        }        
    }
}
