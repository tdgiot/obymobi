﻿namespace WebserviceTester.UserControls
{
    partial class ucTestrun
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btRun = new System.Windows.Forms.Button();
            this.cbTestRun = new System.Windows.Forms.ComboBox();
            this.gvTestcases = new System.Windows.Forms.DataGridView();
            this.cbTestSuite = new System.Windows.Forms.ComboBox();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gvTestcases)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.btRun);
            this.groupBox1.Controls.Add(this.cbTestRun);
            this.groupBox1.Controls.Add(this.gvTestcases);
            this.groupBox1.Controls.Add(this.cbTestSuite);
            this.groupBox1.Location = new System.Drawing.Point(3, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(265, 321);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Testrun";
            // 
            // btRun
            // 
            this.btRun.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btRun.Location = new System.Drawing.Point(204, 57);
            this.btRun.Name = "btRun";
            this.btRun.Size = new System.Drawing.Size(55, 21);
            this.btRun.TabIndex = 3;
            this.btRun.Text = "Run";
            this.btRun.UseVisualStyleBackColor = true;
            this.btRun.Click += new System.EventHandler(this.btRun_Click);
            // 
            // cbTestRun
            // 
            this.cbTestRun.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cbTestRun.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbTestRun.FormattingEnabled = true;
            this.cbTestRun.Location = new System.Drawing.Point(6, 57);
            this.cbTestRun.Name = "cbTestRun";
            this.cbTestRun.Size = new System.Drawing.Size(192, 21);
            this.cbTestRun.TabIndex = 2;
            this.cbTestRun.SelectedIndexChanged += new System.EventHandler(this.cbTestRun_SelectedIndexChanged);
            // 
            // gvTestcases
            // 
            this.gvTestcases.AllowUserToAddRows = false;
            this.gvTestcases.AllowUserToDeleteRows = false;
            this.gvTestcases.AllowUserToResizeColumns = false;
            this.gvTestcases.AllowUserToResizeRows = false;
            this.gvTestcases.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gvTestcases.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.gvTestcases.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gvTestcases.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.gvTestcases.Location = new System.Drawing.Point(6, 84);
            this.gvTestcases.MultiSelect = false;
            this.gvTestcases.Name = "gvTestcases";
            this.gvTestcases.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.gvTestcases.Size = new System.Drawing.Size(253, 230);
            this.gvTestcases.TabIndex = 1;
            //this.gvTestcases.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.gvTestcases_CellDoubleClick);
            this.gvTestcases.DoubleClick += new System.EventHandler(this.gvTestcases_DoubleClick);
            // 
            // cbTestSuite
            // 
            this.cbTestSuite.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cbTestSuite.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbTestSuite.FormattingEnabled = true;
            this.cbTestSuite.Location = new System.Drawing.Point(6, 30);
            this.cbTestSuite.Name = "cbTestSuite";
            this.cbTestSuite.Size = new System.Drawing.Size(253, 21);
            this.cbTestSuite.TabIndex = 0;
            this.cbTestSuite.SelectedIndexChanged += new System.EventHandler(this.cbTestSuite_SelectedIndexChanged);
            // 
            // ucTestrun
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupBox1);
            this.Name = "ucTestrun";
            this.Size = new System.Drawing.Size(271, 328);
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gvTestcases)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DataGridView gvTestcases;
        private System.Windows.Forms.ComboBox cbTestSuite;
        private System.Windows.Forms.ComboBox cbTestRun;
        private System.Windows.Forms.Button btRun;
    }
}
