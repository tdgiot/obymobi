﻿using System;
using System.Windows.Forms;
using Obymobi.Data.HelperClasses;
using Obymobi.WebserviceTester.Data;
using Obymobi.WebserviceTester.Data.Models;
using Obymobi.WebserviceTester.Enums;
using WebserviceTester.Controls;

namespace WebserviceTester.UserControls
{
    public partial class ucApiRequest : UserControl
    {
        private Guid Identifier;
        private ApiRequest ApiRequest;

        public Action<ApiRequest> ActionUpdateApiRequest;

        public ucApiRequest()
        {
            InitializeComponent();            
        }

        public void Update(ApiRequest request)
        {
            this.Identifier = request.Identifier;
            this.ApiRequest = request;
            this.LoadUserControls();
            this.SetGui();
        }

        private void LoadUserControls()
        {
            this.cbApiMethod.BindEnumToCb(typeof(ApiMethodType));
            this.cbCompany.BindEntityCollectionToCb(DataManager.Instance.CompanyManager.GetCompanies(), CompanyFields.CompanyId, CompanyFields.Name, CompanyFields.Name);
        }

        private void SetGui()
        {
            if (this.ApiRequest != null)
            {
                this.cbApiMethod.SelectedIndex = this.cbApiMethod.FindString(this.ApiRequest.ApiMethodType.ToEnum<ApiMethodType>().ToString());
                this.cbCompany.SelectedValue(this.ApiRequest.CompanyId);
                this.cbClient.SelectedValue(this.ApiRequest.ClientId);
                this.cbTerminal.SelectedValue(this.ApiRequest.TerminalId);
                this.cbMenu.SelectedValue(this.ApiRequest.MenuId);
            }
        }

        private void LoadCompanySpecificData(int companyId)
        {
            this.cbMenu.BindEntityCollectionToCb(DataManager.Instance.MenuManager.GetMenusForCompany(companyId), MenuFields.MenuId, MenuFields.Name, MenuFields.Name);
            this.cbClient.BindEntityCollectionToCb(DataManager.Instance.ClientManager.GetClientsByCompanyId(companyId), ClientFields.ClientId, ClientFields.ClientId, ClientFields.ClientId);
            this.cbTerminal.BindEntityCollectionToCb(DataManager.Instance.TerminalManager.GetTerminalsByCompanyId(companyId), TerminalFields.TerminalId, TerminalFields.TerminalId, TerminalFields.TerminalId);
        }

        private bool IsValid()
        {
            if (this.cbApiMethod.SelectedIndex < 0)
            {
                MessageBox.Show("Select an API method");
                return false;
            }
            return true;
        }

        public bool Save()
        {
            if (this.IsValid())
            {
                ApiRequest request = new ApiRequest(this.cbApiMethod.GetSelectedValue().ToEnum<ApiMethodType>());
                request.Identifier = this.Identifier;
                request.CompanyId = this.cbCompany.GetSelectedValue();
                request.ClientId = this.cbClient.GetSelectedValue();
                request.TerminalId = this.cbTerminal.GetSelectedValue();
                request.MenuId = this.cbMenu.GetSelectedValue();

                this.ActionUpdateApiRequest?.Invoke(request);
                return true;
            }
            return false;
        }

        private void cbCompany_SelectedIndexChanged(object sender, EventArgs e)
        {
            int companyId = this.cbCompany.GetSelectedValue();
            if (companyId > 0)
            {
                this.LoadCompanySpecificData(companyId);
            }
        }
    }
}
