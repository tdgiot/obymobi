﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Obymobi.Data.EntityClasses;
using WebserviceTester.Controls;
using Obymobi.WebserviceTester.Data;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;

namespace WebserviceTester.UserControls
{
    public partial class ucSelectClient : UserControl
    {
        public ClientEntity Client { get; set; }

        public ucSelectClient()
        {
            InitializeComponent();            
        }

        public void SetGui()
        {
            this.LoadCompanies();
        }

        private void LoadCompanies()
        {
            this.cbCompany.BindEntityCollectionToCb(
                DataManager.Instance.CompanyManager.GetCompanies(),
                CompanyFields.CompanyId,
                CompanyFields.Name,
                CompanyFields.Name);
        }

        private void LoadDeliverypointgroups(int companyId)
        {
            this.cbDeliverypointgroup.BindEntityCollectionToCb(
                DataManager.Instance.DeliverypointgroupManager.GetDeliverypointgroupsByCompanyId(companyId),
                DeliverypointgroupFields.DeliverypointgroupId,
                DeliverypointgroupFields.Name,
                DeliverypointgroupFields.Name);
        }

        private void LoadDeliverypoints(int companyId, int deliverypointgroupId)
        {
            DeliverypointCollection deliverypoints = null;

            if (deliverypointgroupId > 0)
            {
                deliverypoints = DataManager.Instance.DeliverypointManager.GetDelliverypointsByDeliverypointgroupId(deliverypointgroupId);
            }
            else
            {
                deliverypoints = DataManager.Instance.DeliverypointManager.GetDeliverypointsByCompanyId(companyId);
            }

            this.cbDeliverypoint.BindEntityCollectionToCb(
                deliverypoints,
                DeliverypointFields.DeliverypointId,
                DeliverypointFields.Name,
                DeliverypointFields.Name);
        }

        private void LoadClients(int companyId, int deliverypointgroupId, int deliverypointId)
        {
            ClientCollection clients = null;
            if (deliverypointId > 0)
            {
                clients = DataManager.Instance.ClientManager.GetClientsByDeliverypointId(deliverypointId);
            }
            else if (deliverypointgroupId > 0)
            {
                clients = DataManager.Instance.ClientManager.GetClientsByDeliverypointgroupId(deliverypointgroupId);
            }
            else
            {
                clients = DataManager.Instance.ClientManager.GetClientsByCompanyId(companyId);
            }

            this.cbClient.BindEntityCollectionToCb(
                clients,
                ClientFields.ClientId,
                ClientFields.ClientId,
                ClientFields.ClientId);
        }

        private void cbCompany_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.cbDeliverypointgroup.Items.Clear();
            this.cbDeliverypoint.Items.Clear();
            this.cbClient.Items.Clear();

            int companyId = this.cbCompany.GetSelectedValue();
            if (companyId > 0)
            {
                this.LoadDeliverypointgroups(companyId);
            }
        }

        private void cbDeliverypointgroup_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.cbDeliverypoint.Items.Clear();
            this.cbClient.Items.Clear();

            int deliverypointgroupId = this.cbDeliverypointgroup.GetSelectedValue();
            if (deliverypointgroupId > 0)
            {
                this.LoadDeliverypoints(this.cbCompany.GetSelectedValue(), deliverypointgroupId);
                this.LoadClients(this.cbCompany.GetSelectedValue(), deliverypointgroupId, -1);
            }
        }

        private void cbDeliverypoint_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.cbClient.Items.Clear();

            int deliverypointId = this.cbDeliverypoint.GetSelectedValue();
            if (deliverypointId > 0)
            {
                this.LoadClients(this.cbCompany.GetSelectedValue(), this.cbDeliverypointgroup.GetSelectedValue(), deliverypointId);
            }
        }

        private void cbClient_SelectedIndexChanged(object sender, EventArgs e)
        {
            int clientId = this.cbClient.GetSelectedValue();
            if (clientId > 0)
            {
                ClientEntity client = DataManager.Instance.ClientManager.GetClientById(clientId);
                if (!client.IsNew)
                {
                    this.Client = client;
                }
            }
        }

        private void btCreate_Click(object sender, EventArgs e)
        {
            int deliverypointId = this.cbDeliverypoint.GetSelectedValue();
            if (deliverypointId <= 0)
            {
                MessageBox.Show("Select a deliverypoint first");
            }
            else
            {
                DataManager.Instance.ClientManager.CreateClient(this.cbCompany.GetSelectedValue(), this.cbDeliverypointgroup.GetSelectedValue(), deliverypointId);
                this.LoadClients(this.cbCompany.GetSelectedValue(), this.cbDeliverypointgroup.GetSelectedValue(), deliverypointId);
            }
        }
    }
}
