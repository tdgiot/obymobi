﻿using System;
using System.Windows.Forms;
using Obymobi.WebserviceTester.Data.Models;
using WebserviceTester.Forms;

namespace WebserviceTester.UserControls
{
    public partial class ucApiResponse : UserControl
    {
        private Guid Identifier;
        private ApiResponse ApiResponse;

        public ucApiResponse()
        {
            InitializeComponent();
        }

        public void Update(ApiResponse response)
        {
            this.Identifier = response.Identifier;
            this.ApiResponse = response;
            this.SetGui();            
        }

        private void SetGui()
        {
            if (this.ApiResponse != null)
            {
                this.tbResultCode.Text = this.ApiResponse.ResultCode.ToString();
                this.tbResultMessage.Text = this.ApiResponse.ResultMessage;
            }
        }

        private void btShowRaw_Click(object sender, EventArgs e)
        {
            if (this.ApiResponse != null)
            {
                FormRawResponse form = new FormRawResponse(this.ApiResponse.Xml);
                form.Show();
            }
        }
    }
}
