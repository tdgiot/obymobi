﻿using System;
using System.Data;
using System.Windows.Forms;
using Obymobi.WebserviceTester.Data.Models;
using Obymobi.WebserviceTester.Enums;
using WebserviceTester.Forms;

namespace WebserviceTester.UserControls
{
    public partial class ucTestSuite : UserControl
    {
        private TestSuiteCollection TestSuites;

        public Action<Guid, TestSuite> ActionUpdateTestSuite;
        public Action<Guid> ActionDeleteTestSuite;

        public ucTestSuite()
        {
            InitializeComponent();
        }

        public void Update(TestSuiteCollection testSuites)
        {
            this.TestSuites = testSuites;
            this.gvTestSuites.DataSource = this.CreateTableForTestSuite(this.TestSuites);
        }

        private DataTable CreateTableForTestSuite(TestSuiteCollection testSuites)
        {
            DataTable table = new DataTable();
            DataColumn clmName = new DataColumn("Name");
            table.Columns.Add(clmName);
            for (int i = 0; i < testSuites.Count; i++)
            {
                DataRow row = table.NewRow();
                row[clmName] = testSuites[i].Name;
                table.Rows.Add(row);
            }
            return table;
        }

        private void ShowTestSuiteForm(Guid testSuiteIdentifier)
        {
            FormTestSuite form = new FormTestSuite(this.TestSuites, testSuiteIdentifier);
            form.ActionUpdateTestSuite = this.ActionUpdateTestSuite;
            form.Show();
        }

        private void btNew_Click(object sender, EventArgs e)
        {
            this.ShowTestSuiteForm(Guid.NewGuid());
        }

        private void btEdit_Click(object sender, EventArgs e)
        {
            if (this.gvTestSuites.SelectedRows.Count > 0)
            {
                TestSuite suite = this.TestSuites[this.gvTestSuites.SelectedRows[0].Index];
                this.ShowTestSuiteForm(suite.Identifier);
            }
        }

        private void btDelete_Click(object sender, EventArgs e)
        {
            if (this.gvTestSuites.SelectedRows.Count > 0)
            {
                TestSuite suite = this.TestSuites[this.gvTestSuites.SelectedRows[0].Index];
                this.ActionDeleteTestSuite?.Invoke(suite.Identifier);                
            }
        }

        private void gvTestSuites_Click(object sender, EventArgs e)
        {
            if (this.gvTestSuites.SelectedRows.Count > 0)
            {
                TestSuite suite = this.TestSuites[this.gvTestSuites.SelectedRows[0].Index];

                this.lblBaseUrl.Text = suite.ApiUrl;
                this.lblApiVersion.Text = EnumUtil.GetStringValue((Enum)suite.ApiVersionType.ToEnum<ApiVersionType>());
                this.lblCreated.Text = suite.Created;
            }
        }
    }
}
