﻿using Obymobi.WebserviceTester.Data.Models;
using Obymobi.WebserviceTester.Logic.Api;

namespace Obymobi.WebserviceTester.Interfaces
{
    public interface ICraveService
    {
        void GetAdvertisements(TestCase t);
        void GetAnnouncements(TestCase t);
        void GetClient(TestCase t);
        void GetCompany(TestCase t);
        void GetDeliverypoints(TestCase t);
        void GetEntertainment(TestCase t);
        void GetMenu(TestCase t);
        void GetOrders(TestCase t);
        void GetPointOfInterest(TestCase t);
        void GetSite(TestCase t);
        void GetTerminal(TestCase t);
        void SaveOrder(TestCase t);
        void IsAlive(TestCase t);
    }
}
