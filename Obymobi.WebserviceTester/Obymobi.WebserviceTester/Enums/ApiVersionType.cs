﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dionysos;

namespace Obymobi.WebserviceTester.Enums
{
    public enum ApiVersionType : int
    {
        [StringValue("23.0")]
        Api23_0 = 1
    }
}
