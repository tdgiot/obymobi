﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dionysos;

namespace Obymobi.WebserviceTester.Enums
{
    public enum ApiMethodType : int
    {
        GetAdvertisement = 1,
        GetAnnouncements = 2,
        GetClient = 3,
        GetCompany = 4,
        GetDeliverypoints = 5,
        GetEntertainment = 6,
        GetMenu = 7,
        GetOrders = 8,
        GetPointOfInterest = 9,
        GetSite = 10,
        GetTerminal = 11,
        SaveOrder = 12,
        IsAlive = 13,
    }
}
