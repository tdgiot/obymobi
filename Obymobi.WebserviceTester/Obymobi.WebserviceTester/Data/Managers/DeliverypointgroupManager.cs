﻿using Obymobi.Data;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Obymobi.WebserviceTester.Data.Managers
{
    public class DeliverypointgroupManager
    {
        #region Constructors

        public DeliverypointgroupManager()
        {
            
        }

        #endregion

        #region Methods

        public DeliverypointgroupCollection GetDeliverypointgroupsByCompanyId(int companyId)
        {
            PredicateExpression filter = new PredicateExpression();
            filter.Add(DeliverypointgroupFields.CompanyId == companyId);

            IncludeFieldsList includeFieldsList = new IncludeFieldsList();
            includeFieldsList.Add(DeliverypointgroupFields.Name);

            DeliverypointgroupCollection deliverypointgroups = new DeliverypointgroupCollection();
            deliverypointgroups.GetMulti(filter, includeFieldsList, null);

            return deliverypointgroups;
        }        

        #endregion
    }
}