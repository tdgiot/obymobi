﻿using Obymobi.Data;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Obymobi.WebserviceTester.Data.Managers
{
    public class TerminalManager
    {
        #region Properties

        public TerminalCollection Terminals { get; set; }

        #endregion

        #region Constructors

        public TerminalManager()
        {
            
        }

        #endregion

        #region Methods

        public TerminalCollection GetTerminalsByCompanyId(int companyId)
        {
            PredicateExpression filter = new PredicateExpression();
            filter.Add(TerminalFields.CompanyId == companyId);
            filter.Add(DeviceFields.Identifier != string.Empty);

            RelationCollection relations = new RelationCollection();
            relations.Add(TerminalEntity.Relations.DeviceEntityUsingDeviceId);

            TerminalCollection terminals = new TerminalCollection();
            terminals.GetMulti(filter, relations);

            return terminals;
        }

        public TerminalEntity GetTerminalById(int terminalId)
        {
            return new TerminalEntity(terminalId);
        }

        #endregion
    }
}
