﻿using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Obymobi.WebserviceTester.Data.Managers
{
    public class CompanyManager
    {
        #region Properties

        #endregion

        #region Constructors

        public CompanyManager()
        {
            
        }

        #endregion

        #region Methods        

        public CompanyEntity GetCompanyById(int companyId)
        {
            return new CompanyEntity(companyId);
        }

        public CompanyCollection GetCompanies()
        {
            IncludeFieldsList includeFieldsList = new IncludeFieldsList();
            includeFieldsList.Add(CompanyFields.Name);

            CompanyCollection companies = new CompanyCollection();
            companies.GetMulti(null, includeFieldsList, 0);

            return companies;
        }

        #endregion
    }
}
