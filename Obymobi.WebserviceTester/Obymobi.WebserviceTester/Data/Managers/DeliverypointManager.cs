﻿using Obymobi.Data;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Obymobi.WebserviceTester.Data.Managers
{
    public class DeliverypointManager
    {
        #region Constructors

        public DeliverypointManager()
        {
            
        }

        #endregion

        #region Methods

        public DeliverypointCollection GetDeliverypointsByCompanyId(int companyId)
        {
            PredicateExpression filter = new PredicateExpression(DeliverypointFields.CompanyId == companyId);
            return this.GetDeliverypoints(filter);
        }

        public DeliverypointCollection GetDelliverypointsByDeliverypointgroupId(int deliverypointgroupId)
        {
            PredicateExpression filter = new PredicateExpression(DeliverypointFields.DeliverypointgroupId == deliverypointgroupId);
            return this.GetDeliverypoints(filter);
        }

        private DeliverypointCollection GetDeliverypoints(PredicateExpression subfilter)
        {
            PredicateExpression filter = new PredicateExpression();
            filter.Add(subfilter);

            IncludeFieldsList includeFieldsList = new IncludeFieldsList();
            includeFieldsList.Add(DeliverypointFields.Name);

            DeliverypointCollection deliverypoints = new DeliverypointCollection();
            deliverypoints.GetMulti(filter, includeFieldsList, null);

            return deliverypoints;
        }

        #endregion
    }
}