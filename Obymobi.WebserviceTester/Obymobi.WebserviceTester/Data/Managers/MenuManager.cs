﻿using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Obymobi.WebserviceTester.Data.Managers
{
    public class MenuManager
    {
        #region Properties

        #endregion

        #region Constructors

        public MenuManager()
        {
            
        }

        #endregion

        #region Methods                

        public MenuCollection GetMenusForCompany(int companyId)
        {
            PredicateExpression filter = new PredicateExpression(MenuFields.CompanyId == companyId);

            IncludeFieldsList includeFieldsList = new IncludeFieldsList();
            includeFieldsList.Add(MenuFields.Name);

            MenuCollection menus = new MenuCollection();
            menus.GetMulti(filter, includeFieldsList, 0);

            return menus;
        }

        public MenuCollection GetMenus()
        {
            IncludeFieldsList includeFieldsList = new IncludeFieldsList();
            includeFieldsList.Add(MenuFields.Name);

            MenuCollection menus = new MenuCollection();
            menus.GetMulti(null, includeFieldsList, 0);

            return menus;
        }

        #endregion
    }
}
