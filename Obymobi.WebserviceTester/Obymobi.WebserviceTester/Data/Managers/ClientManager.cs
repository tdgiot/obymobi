﻿using Obymobi.Data;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Obymobi.WebserviceTester.Data.Managers
{
    public class ClientManager
    {
        #region Properties

        public ClientCollection Clients { get; set; }

        #endregion

        #region Constructors

        public ClientManager()
        {
            this.Clients = new ClientCollection();
        }

        #endregion

        #region Methods

        public ClientCollection GetClientsByCompanyId(int companyId)
        {
            PredicateExpression filter = new PredicateExpression(ClientFields.CompanyId == companyId);
            return this.GetClients(filter);
        }

        public ClientCollection GetClientsByDeliverypointgroupId(int deliverypointgroupId)
        {
            PredicateExpression filter = new PredicateExpression(ClientFields.DeliverypointGroupId == deliverypointgroupId);            
            return this.GetClients(filter);
        }

        public ClientCollection GetClientsByDeliverypointId(int deliverypointId)
        {
            PredicateExpression filter = new PredicateExpression(ClientFields.DeliverypointId == deliverypointId);
            return this.GetClients(filter);
        }

        private ClientCollection GetClients(PredicateExpression subfilter)
        {
            PredicateExpression filter = new PredicateExpression(DeviceFields.Identifier != string.Empty);
            filter.Add(subfilter);

            RelationCollection relations = new RelationCollection();
            relations.Add(ClientEntity.Relations.DeviceEntityUsingDeviceId);

            ClientCollection clients = new ClientCollection();
            clients.GetMulti(filter, relations);

            return clients;
        }

        public ClientEntity GetClientById(int clientId)
        {
            if (clientId > 0)
            {
                return new ClientEntity(clientId);
            }
            return null;
        }

        public void CreateClient(int companyId, int deliverypointgroupId, int deliverypointId)
        {
            Random r = new Random();
            string mac = string.Format("xx:xx:xx:{0}:{1}:{2}", r.Next(10, 99), r.Next(10, 99), r.Next(10, 99));

            DeviceEntity device = new DeviceEntity();
            device.Identifier = mac;
            device.Type = DeviceType.SamsungP5110;
            device.DeviceModel = DeviceModel.Unknown;                
            device.Save();

            ClientEntity client = new ClientEntity();
            client.CompanyId = companyId;
            client.DeliverypointGroupId = deliverypointgroupId;
            client.DeliverypointId = deliverypointId;
            client.DeviceId = device.DeviceId;
            client.Save();
        }

        #endregion
    }
}