﻿using Obymobi.WebserviceTester.Data.Managers;
using Obymobi.WebserviceTester.Logic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Obymobi.WebserviceTester.Data
{
    public class DataManager
    {
        #region Properties

        private static DataManager instance;

        public CompanyManager CompanyManager { get; private set; }
        public TerminalManager TerminalManager { get; private set; }
        public ClientManager ClientManager { get; private set; }        
        public MenuManager MenuManager { get; private set; }
        public DeliverypointgroupManager DeliverypointgroupManager { get; private set; }
        public DeliverypointManager DeliverypointManager { get; private set; }

        public static DataManager Instance
        {
            get { return instance ?? (instance = new DataManager()); }
        }

        #endregion

        #region Constructors

        private DataManager()
        {
            try
            {
                this.CompanyManager = new CompanyManager();
                this.TerminalManager = new TerminalManager();
                this.ClientManager = new ClientManager();
                this.MenuManager = new MenuManager();                
                this.DeliverypointgroupManager = new DeliverypointgroupManager();
                this.DeliverypointManager = new DeliverypointManager();
            }
            catch (Exception e)
            {
                Logger.Log("Exception occured while trying to configure the data managers. {0}", e.Message);
            }
        }

        #endregion
    }
}
