﻿using Dionysos.Data.LLBLGen;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Obymobi.WebserviceTester.Data.Models
{
    public class TestRunCollection : List<TestRun>
    {
        public TestRunCollection()
        {

        }

        public TestRun Get(Guid identifier)
        {
            if (identifier != null)
            {
                return this.SingleOrDefault(x => x.Identifier == identifier);
            }
            return null;
        }

        public TestRunCollection GetByTestSuiteIdentifier(Guid identifier)
        {
            TestRunCollection testRuns = new TestRunCollection();
            testRuns.AddRange(this.Where(x => x.TestSuiteIdentifier == identifier));
            return testRuns;           
        }
    }
}
