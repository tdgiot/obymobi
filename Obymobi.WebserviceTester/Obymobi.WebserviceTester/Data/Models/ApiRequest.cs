﻿using Obymobi.Logic.HelperClasses;
using Obymobi.WebserviceTester.Enums;
using System;

namespace Obymobi.WebserviceTester.Data.Models
{
    public class ApiRequest
    {
        #region Constructors

        public ApiRequest()
        {
            this.SetDefaults();            
        }

        public ApiRequest(Guid identifier)
        {
            this.Identifier = identifier;
            this.SetDefaults();
        }

        public ApiRequest(ApiMethodType type)
        {
            this.SetDefaults();
            this.ApiMethodType = (int)type;
        }

        private void SetDefaults()
        {
            this.Identifier = Guid.NewGuid();
            this.CultureCode = string.Empty;
            this.ApplicationVersion = string.Empty;
            this.OsVersion = string.Empty;
            this.StatusMessage = string.Empty;
            this.Version = string.Empty;
            this.Xml = string.Empty;
        }

        #endregion

        #region Properties

        public Guid Identifier { get; set; }
        public int ApiMethodType { get; set; }
        public int ClientId { get; set; }
        public int TerminalId { get; set; }
        public int CompanyId { get; set; }
        public int DeliverypointgroupId { get; set; }
        public int DeliverypointId { get; set; }
        public int PointOfInterestId { get; set; }
        public int SiteId { get; set; }
        public int MenuId { get; set; }
        public string CultureCode { get; set; }
        public string ApplicationVersion { get; set; }
        public string OsVersion { get; set; }
        public int DeviceType { get; set; }
        public int DeviceModel { get; set; }
        public int StatusCode { get; set; }
        public string StatusMessage { get; set; }
        public string Version { get; set; }
        public bool IsConsole { get; set; }
        public bool IsMasterConsole { get; set; }
        public string Xml { get; set; }

        #endregion

        #region Methods

        public ApiRequest Clone()
        {
            return XmlHelper.Deserialize<ApiRequest>(XmlHelper.Serialize(this));
        }

        #endregion
    }
}
