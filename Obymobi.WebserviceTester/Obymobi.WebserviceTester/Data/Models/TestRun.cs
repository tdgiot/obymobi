﻿using Obymobi.Logic.HelperClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Obymobi.WebserviceTester.Data.Models
{
    public class TestRun
    {
        public TestRun()
        {
            this.Identifier = Guid.NewGuid();
            this.Created = DateTime.Now.ToString();
        }

        public Guid Identifier { get; set; }
        public Guid TestSuiteIdentifier { get; set; }
        public string Name { get; set; }
        public string Created { get; set; }
        public string Started { get; set; }
        public string Processed { get; set; }
        public long Elapsed { get; set; }
        public string ApiUrl { get; set; }
        public int ApiVersionType { get; set; }       
        public TestCaseCollection TestCases { get; set; }

        public TestRun Clone()
        {
            return XmlHelper.Deserialize<TestRun>(XmlHelper.Serialize(this));
        }
    }
}
