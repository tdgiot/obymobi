﻿using Obymobi.Logic.HelperClasses;
using Obymobi.WebserviceTester.Logic.Api;
using System;
using System.Globalization;

namespace Obymobi.WebserviceTester.Data.Models
{
    public class TestSuite
    {
        public TestSuite()
        {
            this.Identifier = Guid.NewGuid();
            this.Created = DateTime.Now.ToString(CultureInfo.InvariantCulture);
            this.Requests = new ApiRequestCollection();
        }

        public TestSuite(Guid identifier)
        {
            this.Identifier = identifier;
            this.Created = DateTime.Now.ToString(CultureInfo.InvariantCulture);
            this.Requests = new ApiRequestCollection();
        }

        public Guid Identifier { get; set; }
        public string Name { get; set; }
        public string Created { get; set; }
        public string ApiUrl { get; set; }
        public int ApiVersionType { get; set; }            
        public ApiRequestCollection Requests { get; set; }

        public TestSuite Clone()
        {
            return XmlHelper.Deserialize<TestSuite>(XmlHelper.Serialize(this));
        }
    }
}

