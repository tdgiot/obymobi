﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Obymobi.WebserviceTester.Data.Models
{
    public class TestSuiteCollection : List<TestSuite>
    {
        public TestSuiteCollection()
        {

        }

        public TestSuite Get(Guid identifier)
        {
            if (identifier != null)
            {
                return this.SingleOrDefault(x => x.Identifier == identifier);
            }
            return null;
        }
    }
}
