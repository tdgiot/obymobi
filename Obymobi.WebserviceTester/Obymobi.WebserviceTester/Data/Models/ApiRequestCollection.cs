﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Obymobi.WebserviceTester.Data.Models
{
    public class ApiRequestCollection : List<ApiRequest>
    {
        public ApiRequestCollection()
        {

        }

        public ApiRequest Get(Guid identifier)
        {
            if (identifier != null)
            {
                return this.SingleOrDefault(x => x.Identifier == identifier);
            }
            return null;
        }
    }
}
