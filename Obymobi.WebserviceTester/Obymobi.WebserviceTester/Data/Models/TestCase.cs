﻿using Obymobi.Logic.HelperClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Obymobi.WebserviceTester.Data.Models
{
    public class TestCase
    {
        #region Constructors

        public TestCase()
        {
            this.Identifier = Guid.NewGuid();
            this.Created = DateTime.Now.ToString();
            this.Response = ApiResponse.FailResponse();
        }

        public TestCase(Guid identifier)
        {
            this.Identifier = identifier;
            this.Created = DateTime.Now.ToString();            
            this.Response = ApiResponse.FailResponse();
        }

        public TestCase(ApiRequest request)
        {
            this.Request = request;
            this.Identifier = Guid.NewGuid();
            this.Created = DateTime.Now.ToString();
            this.Response = ApiResponse.FailResponse();
        }

        #endregion

        #region Properties

        public Guid Identifier { get; set; }
        public ApiRequest Request { get; set; }
        public ApiResponse Response { get; set; }
        public string Created { get; set; }
        public string Started { get; set; }
        public string Processed { get; set; }
        public long Elapsed { get; set; }

        #endregion

        #region Methods

        public TestCase Clone()
        {
            return XmlHelper.Deserialize<TestCase>(XmlHelper.Serialize(this));
        }

        #endregion
    }
}

