﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Obymobi.Logic.HelperClasses;

namespace Obymobi.WebserviceTester.Data.Models
{
    public class ApiResponse
    {
        #region Constructors        

        public ApiResponse()
        {

        }

        public ApiResponse(Guid identifier) : this(identifier, -1, string.Empty, string.Empty)
        {

        }        

        public ApiResponse(int resultCode, string resultMessage, string xml) : this(Guid.NewGuid(), resultCode, resultMessage, xml)
        {

        }

        public ApiResponse(Guid identifier, int resultCode, string resultMessage, string xml)
        {
            this.Identifier = identifier;
            this.ResultCode = resultCode;
            this.ResultMessage = resultMessage;
            this.Xml = xml;            
        }

        #endregion

        #region Static methods

        public static ApiResponse FailResponse()
        {
            return new ApiResponse(-1, string.Empty, string.Empty);
        }

        #endregion

        #region Properties

        public Guid Identifier { get; set; }
        public int ResultCode { get; set; }
        public string ResultMessage { get; set; }
        public string Xml { get; set; }

        #endregion

        #region Public methods

        public ApiResponse Clone()
        {
            return XmlHelper.Deserialize<ApiResponse>(XmlHelper.Serialize(this));
        }

        #endregion
    }
}
