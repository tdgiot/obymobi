﻿using Obymobi.WebserviceTester.Data.Models;
using Obymobi.WebserviceTester.Extensions;
using System;
using System.Collections.Generic;
using System.IO;

namespace Obymobi.WebserviceTester.Data.Managers
{
    public class TestProvider
    {
        #region Fields

        private const string DATA_DIRECTORY = "Data";
        private const string TESTSUITE_DIRECTORY = "Testsuites";
        private const string TESTRUN_DIRECTORY = "Testruns";
        
        private string testSuiteDir;
        private string testRunDir;

        #endregion

        #region Properties

        public TestSuiteCollection TestSuiteCollection { get; private set; }
        public TestRunCollection TestRunCollection { get; private set; }        

        #endregion

        #region Constructors

        public TestProvider()
        {
            this.CreateDirectories();

            this.TestSuiteCollection = new TestSuiteCollection();
            this.TestSuiteCollection.AddRange(this.LoadTestSuitesFromDisk());

            this.TestRunCollection = new TestRunCollection();
            this.TestRunCollection.AddRange(this.LoadTestRunsFromDisk());
        }

        #endregion

        #region Methods       

        private void CreateDirectories()
        {
            string dataDir = Path.Combine(Global.RootDirectory, TestProvider.DATA_DIRECTORY);
            this.testSuiteDir = Path.Combine(dataDir, TestProvider.TESTSUITE_DIRECTORY);
            this.testRunDir = Path.Combine(dataDir, TestProvider.TESTRUN_DIRECTORY);

            Directory.CreateDirectory(dataDir);
            Directory.CreateDirectory(this.testSuiteDir);
            Directory.CreateDirectory(this.testRunDir);
        } 

        private List<TestSuite> LoadTestSuitesFromDisk()
        {
            List<TestSuite> testSuites = new List<TestSuite>();
            string[] files = Directory.GetFiles(this.testSuiteDir, "*.xml");
            foreach (string file in files)
            {
                try
                {
                    testSuites.Add(Obymobi.Logic.HelperClasses.XmlHelper.Deserialize<TestSuite>(File.ReadAllText(file)));             
                }
                catch (Exception ex)
                {
                    Log.Error("TestManager", "LoadTestSuitesFromDisk", "Error: " + ex.Message);
                }                
            }
            return testSuites;
        }

        private List<TestRun> LoadTestRunsFromDisk()
        {
            List<TestRun> testRuns = new List<TestRun>();
            string[] files = Directory.GetFiles(this.testSuiteDir, "*.xml");
            foreach (string file in files)
            {
                try
                {
                    testRuns.Add(Obymobi.Logic.HelperClasses.XmlHelper.Deserialize<TestRun>(File.ReadAllText(file)));
                }
                catch (Exception ex)
                {
                    Log.Error("TestManager", "LoadTestRunsFromDisk", "Error: " + ex.Message);
                }
            }
            return testRuns;
        }

        public void UpdateTestSuite(Guid identifier, TestSuite testSuite)
        {
            TestSuite tmp = this.TestSuiteCollection.Get(identifier);
            if (tmp == null)
            {
                this.TestSuiteCollection.Add(testSuite);
            }
            else
            {
                tmp.Update(testSuite);                
            }            
        }

        public void DeleteTestSuite(Guid identifier)
        {
            TestSuite tmp = this.TestSuiteCollection.Get(identifier);
            if (tmp != null)
            {
                this.TestSuiteCollection.Remove(tmp);
            }
        }

        public void UpdateTestRun(Guid identifier, TestRun testRun)
        {
            TestRun tmp = this.TestRunCollection.Get(identifier);
            if (tmp == null)
            {
                this.TestRunCollection.Add(testRun);
            }
            else
            {
                tmp.Update(testRun);
            }
        }

        public void UpdateTestCase(Guid testRunIdentifier, Guid testCaseIdentifier, TestCase testCase)
        {
            TestCase tmp = this.TestRunCollection.Get(testRunIdentifier).TestCases.Get(testCaseIdentifier);
            if (tmp == null)
            {
                this.TestRunCollection.Get(testRunIdentifier).TestCases.Add(testCase);
            }
            else
            {
                tmp.Update(testCase);
            }
        }

        public void SaveToDisk()
        {
            try
            {
                Array.ForEach(Directory.GetFiles(this.testSuiteDir), File.Delete);
                foreach (TestSuite suite in this.TestSuiteCollection)
                {
                    string file = Path.Combine(this.testSuiteDir, suite.GetFilename());
                    File.WriteAllText(file, Obymobi.Logic.HelperClasses.XmlHelper.Serialize(suite));
                }

                Array.ForEach(Directory.GetFiles(this.testRunDir), File.Delete);
                foreach (TestRun run in this.TestRunCollection)
                {
                    string file = Path.Combine(this.testRunDir, run.GetFilename());
                    File.WriteAllText(file, Obymobi.Logic.HelperClasses.XmlHelper.Serialize(run));
                }
            }
            catch (Exception ex)
            {
                Log.Error("TestManager", "SaveToDisk", "Error: " + ex.Message);
            }
        }                              

        #endregion
    }
}