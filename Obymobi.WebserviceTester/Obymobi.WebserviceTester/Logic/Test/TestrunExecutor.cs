﻿using Obymobi.WebserviceTester.Data.Models;
using Obymobi.WebserviceTester.Enums;
using Obymobi.WebserviceTester.Interfaces;
using Obymobi.WebserviceTester.Logic.Api;
using System;
using System.Diagnostics;
using System.Threading.Tasks;

namespace Obymobi.WebserviceTester.Logic.Test
{
    public class TestrunExecutor
    {
        #region Fields

        private TestRunCollection TestRuns;
        private Guid Identifier;

        #endregion

        #region Constructors        

        public TestrunExecutor(TestRunCollection testRuns, Guid identifier)
        {
            this.TestRuns = testRuns;
            this.Identifier = identifier;
        }

        #endregion

        #region Public methods

        public void Execute()
        {
            Task.Factory.StartNew(() => Run());            
        }

        private void Run()
        {
            TestRun testRun = this.TestRuns.Get(this.Identifier);

            ICraveService craveService = ApiCreator.Create(testRun.ApiUrl, testRun.ApiVersionType.ToEnum<ApiVersionType>());

            testRun.Started = DateTime.Now.ToString();
            this.OnTestRunStarted?.Invoke(this.Identifier);

            Stopwatch watch = Stopwatch.StartNew();
            try
            {
                for (int i = 0; i < testRun.TestCases.Count; i++)
                {
                    this.OnTestCaseStarted?.Invoke(this.Identifier, testRun.TestCases[i].Identifier);

                    TestcaseExecutor.Execute(testRun.TestCases[i], craveService);

                    this.OnTestCaseProcessed?.Invoke(this.Identifier, testRun.TestCases[i].Identifier);
                }
            }
            catch (Exception ex)
            {
                Log.Error("TestrunExecutor", "Execute", "Error - " + ex.Message);
            }
            watch.Stop();

            testRun.Elapsed = watch.ElapsedMilliseconds;
            testRun.Processed = DateTime.Now.ToString();
            this.OnTestRunProcessed?.Invoke(this.Identifier);
        }

        #endregion

        #region Properties

        public Action<Guid> OnTestRunStarted { get; set; }
        public Action<Guid> OnTestRunProcessed { get; set; }
        public Action<Guid, Guid> OnTestCaseStarted { get; set; }
        public Action<Guid, Guid> OnTestCaseProcessed { get; set; }        

        #endregion
    }
}
