﻿using Obymobi.WebserviceTester.Data.Models;
using Obymobi.WebserviceTester.Interfaces;
using Obymobi.WebserviceTester.Logic.Api;
using Obymobi.WebserviceTester.Logic.Api.Methods;
using System;

namespace Obymobi.WebserviceTester.Logic.Test
{
    public static class TestcaseExecutor
    {
        #region Public methods        

        public static void Execute(TestCase testCase, ICraveService craveService)
        {
            ApiMethod method = ApiMethodHelper.Create(testCase.Request);
            method.CraveService = craveService;
            method.Execute(testCase);
        }     

        #endregion        
    }
}
