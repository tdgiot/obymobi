﻿using Obymobi.WebserviceTester.Data.Models;
using Obymobi.WebserviceTester.Enums;
using Obymobi.WebserviceTester.Logic.Test.Predefined;
using System;

namespace Obymobi.WebserviceTester.Logic.Test.Testruns
{
    public class TemplateApiRequestCreator
    {
        public static ApiRequestCollection Create(TemplateApiRequestType type)
        {
            switch (type)
            {
                case TemplateApiRequestType.EmenuStartup:
                    return new TemplateEmenuStartup();
                                        
                default:
                    throw new Exception("PredefinedTestrunCreator - Create - Unknown type: " + type);
            }
        }        
    }
}
