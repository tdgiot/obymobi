﻿using Obymobi.WebserviceTester.Data.Models;
using Obymobi.WebserviceTester.Logic.Api;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Obymobi.WebserviceTester.Logic.Test.Predefined
{
    public class TemplateEmenuStartup : ApiRequestCollection
    {
        public TemplateEmenuStartup()
        {
            this.Add(new ApiRequest(Enums.ApiMethodType.GetClient));
            this.Add(new ApiRequest(Enums.ApiMethodType.GetCompany));
            this.Add(new ApiRequest(Enums.ApiMethodType.GetDeliverypoints));
            this.Add(new ApiRequest(Enums.ApiMethodType.GetAdvertisement));
            this.Add(new ApiRequest(Enums.ApiMethodType.GetEntertainment));
            this.Add(new ApiRequest(Enums.ApiMethodType.GetMenu));
        }
    }
}
