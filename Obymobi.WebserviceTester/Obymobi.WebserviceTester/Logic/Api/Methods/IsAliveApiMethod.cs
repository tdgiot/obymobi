﻿using Obymobi.WebserviceTester.Data.Models;
using Obymobi.WebserviceTester.Interfaces;

namespace Obymobi.WebserviceTester.Logic.Api.Methods
{
    public class IsAliveApiMethod : ApiMethod
    {
        public override void Execute(TestCase testCase)
        {
            this.CraveService.IsAlive(testCase);
        }
    }
}
