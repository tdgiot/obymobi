﻿using Obymobi.WebserviceTester.Data.Models;
using Obymobi.WebserviceTester.Interfaces;

namespace Obymobi.WebserviceTester.Logic.Api.Methods
{
    public class GetEntertainmentApiMethod : ApiMethod
    {
        public override void Execute(TestCase testCase)
        {
            this.CraveService.GetEntertainment(testCase);
        }
    }
}
