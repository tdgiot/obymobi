﻿using Obymobi.WebserviceTester.Data.Models;
using Obymobi.WebserviceTester.Interfaces;

namespace Obymobi.WebserviceTester.Logic.Api.Methods
{
    public class GetPointOfInterestApiMethod : ApiMethod
    {
        public override void Execute(TestCase testCase)
        {
            this.CraveService.GetPointOfInterest(testCase);
        }
    }
}
