﻿using Obymobi.WebserviceTester.Data.Models;
using Obymobi.WebserviceTester.Interfaces;

namespace Obymobi.WebserviceTester.Logic.Api.Methods
{
    public class GetAnnouncementsApiMethod : ApiMethod
    {
        public override void Execute(TestCase testCase)
        {
            this.CraveService.GetAnnouncements(testCase);
        }
    }
}
