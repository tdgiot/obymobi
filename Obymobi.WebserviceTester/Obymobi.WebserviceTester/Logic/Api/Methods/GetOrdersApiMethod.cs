﻿using Obymobi.WebserviceTester.Data.Models;
using Obymobi.WebserviceTester.Interfaces;

namespace Obymobi.WebserviceTester.Logic.Api.Methods
{
    public class GetOrdersApiMethod : ApiMethod
    {
        public override void Execute(TestCase testCase)
        {
            this.CraveService.GetOrders(testCase);
        }
    }
}
