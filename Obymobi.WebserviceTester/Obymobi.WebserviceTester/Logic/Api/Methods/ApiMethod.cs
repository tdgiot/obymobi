﻿using Obymobi.WebserviceTester.Data.Models;
using Obymobi.WebserviceTester.Interfaces;

namespace Obymobi.WebserviceTester.Logic.Api.Methods
{
    public abstract class ApiMethod
    {
        #region Public methods

        public abstract void Execute(TestCase testCase);

        #endregion

        #region Properties

        public ICraveService CraveService { get; set; }

        #endregion
    }
}
