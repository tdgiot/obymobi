﻿using Dionysos;
using Obymobi.WebserviceTester.Data.Models;
using Obymobi.WebserviceTester.Enums;
using Obymobi.WebserviceTester.Interfaces;
using Obymobi.WebserviceTester.Logic.Test;
using System;

namespace Obymobi.WebserviceTester.Logic.Api
{
    public class ApiCreator
    {
        public enum Result
        {
            Success,
            Failure
        }

        public static ICraveService Create(string apiBaseUrl, ApiVersionType version)
        {
            if (!apiBaseUrl.EndsWith("/"))
                apiBaseUrl += "/";

            switch (version)
            {
                case ApiVersionType.Api23_0:
                    return new Version.Api23_0.CraveService(apiBaseUrl);

                default:
                    throw new Exception("ApiCreator - Create - Unable to create api, unknown version");
            }
        }

        public static Result Validate(string apiBaseUrl, ApiVersionType version)
        {
            Result result = Result.Failure;
            try
            {
                ICraveService craveService = ApiCreator.Create(apiBaseUrl, version);
                TestCase testCase = new TestCase(new ApiRequest(ApiMethodType.IsAlive));
                TestcaseExecutor.Execute(testCase, craveService);            
                if (testCase.Response.ResultCode == 100)
                {
                    result = Result.Success;
                }
            }
            catch (Exception ex)
            {
                Log.Error("ApiCreator", "Validate", "Error {0}", ex.Message);
            }
            return result;
        }
    }
}
