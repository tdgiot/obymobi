﻿using System;
using Dionysos;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.WebserviceTester.Data;
using Obymobi.WebserviceTester.Logic.Api;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System.Diagnostics;
using Obymobi.WebserviceTester.Data.Models;

namespace Obymobi.WebserviceTester.WebServices
{
    public static class ApiHelper
    {
        #region Public methods

        public static void ExecuteWithClient(int clientId, WebserviceCall webserviceCall)
        {
            ClientEntity client = DataManager.Instance.ClientManager.GetClientById(clientId);
            if (client != null)
            {
                ApiHelper.Execute(client.CompanyId, client.DeviceEntity.Identifier, webserviceCall);
            }
        }

        public static void ExecuteWithTerminal(int terminalId, WebserviceCall webserviceCall)
        {
            TerminalEntity terminal = DataManager.Instance.TerminalManager.GetTerminalById(terminalId);
            if (terminal != null)
            {
                ApiHelper.Execute(terminal.CompanyId, terminal.DeviceEntity.Identifier, webserviceCall);
            }            
        }

        public delegate TestCase WebserviceCall(long timestamp, string mac, string salt);

        #endregion

        #region Private methods

        private static void Execute(int companyId, string macAddress, WebserviceCall webserviceCall)
        {
            try
            {
                string salt = ApiHelper.GetCompanySalt(companyId);
                if (salt.IsNullOrWhiteSpace())
                {
                    // Failure
                }
                else if (macAddress.IsNullOrWhiteSpace())
                {
                    // Failure
                }
                else
                {
                    Stopwatch watch = Stopwatch.StartNew();
                    string started = DateTime.Now.ToString();

                    long timestamp = DateTime.UtcNow.ToUnixTime();
                    TestCase testCase = webserviceCall.Invoke(timestamp, macAddress, salt);                    

                    watch.Stop();

                    testCase.Started = started;
                    testCase.Processed = DateTime.Now.ToString();
                    testCase.Elapsed = watch.ElapsedMilliseconds;
                }
            }
            catch (Exception ex)
            {
                Log.Error("WebserviceHelper", "Execute", "Exception: {0}", ex.Message);
            }
        }        

        private static string GetCompanySalt(int companyId)
        {
            CompanyEntity companyEntity = new CompanyEntity();
            companyEntity.FetchUsingPK(companyId, null, null, new IncludeFieldsList(CompanyFields.Salt));

            return companyEntity.Salt;
        }        

        #endregion
    }
}

