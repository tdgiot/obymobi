using Obymobi.WebserviceTester.Interfaces;
using Obymobi.Security;
using Obymobi.WebserviceTester.WebServices;
using Obymobi.Logic.HelperClasses;
using System;
using Obymobi.WebserviceTester.Data.Models;

namespace Obymobi.WebserviceTester.Logic.Api.Version.Api23_0
{
    public class CraveService : ICraveService
    {
        #region Properties

        private CraveService23_0.CraveService webservice { get; set; }
        
        #endregion

        #region Constructors

        public CraveService(string apiBaseUrl)
        {            
            this.webservice = new CraveService23_0.CraveService();
            this.webservice.Url = string.Format("{0}23.0/craveservice.asmx", apiBaseUrl);
        }

        #endregion

        #region Methods

        public void GetClient(TestCase t)
        {
            ApiHelper.ExecuteWithClient(t.Request.ClientId, (timestamp, mac, salt) =>
            {
                string hash = Hasher.GetHashFromParameters(salt, timestamp, mac, t.Request.ClientId, mac, t.Request.DeliverypointgroupId, t.Request.DeliverypointId, t.Request.ApplicationVersion, t.Request.OsVersion, t.Request.DeviceType, t.Request.DeviceModel);
                var wsResult = this.webservice.GetClient(timestamp, mac, t.Request.ClientId, mac, t.Request.DeliverypointgroupId, t.Request.DeliverypointId, t.Request.ApplicationVersion, t.Request.OsVersion, t.Request.DeviceType, t.Request.DeviceModel, hash);
                t.Response = new ApiResponse(wsResult.ResultCode, wsResult.ResultMessage, XmlHelper.Serialize(wsResult.ModelCollection));
                return t;
            });
        }                

        public void GetCompany(TestCase t)
        {
            ApiHelper.ExecuteWithClient(t.Request.ClientId, (timestamp, mac, salt) =>
            {
                string hash = Hasher.GetHashFromParameters(salt, timestamp, mac, t.Request.CompanyId);
                var wsResult = this.webservice.GetCompany(timestamp, mac, t.Request.CompanyId, hash);                
                t.Response = new ApiResponse(wsResult.ResultCode, wsResult.ResultMessage, XmlHelper.Serialize(wsResult.ModelCollection));
                return t;
            });            
        }

        public void GetAdvertisements(TestCase t)
        {
            ApiHelper.ExecuteWithClient(t.Request.ClientId, (timestamp, mac, salt) =>
            {
                string hash = Hasher.GetHashFromParameters(salt, timestamp, mac);
                var wsResult = this.webservice.GetAdvertisements(timestamp, mac, hash);
                t.Response = new ApiResponse(wsResult.ResultCode, wsResult.ResultMessage, XmlHelper.Serialize(wsResult.ModelCollection));
                return t;
            });            
        }

        public void GetEntertainment(TestCase t)
        {
            ApiHelper.ExecuteWithClient(t.Request.ClientId, (timestamp, mac, salt) =>
            {
                string hash = Hasher.GetHashFromParameters(salt, timestamp, mac);
                var wsResult = this.webservice.GetEntertainment(timestamp, mac, hash);
                t.Response = new ApiResponse(wsResult.ResultCode, wsResult.ResultMessage, XmlHelper.Serialize(wsResult.ModelCollection));
                return t;
            });            
        }        

        public void GetMenu(TestCase t)
        {
            ApiHelper.ExecuteWithClient(t.Request.ClientId, (timestamp, mac, salt) =>
            {
                string hash = Hasher.GetHashFromParameters(salt, timestamp, mac, t.Request.DeliverypointgroupId);
                var wsResult = this.webservice.GetMenu(timestamp, mac, t.Request.DeliverypointgroupId, hash);
                t.Response = new ApiResponse(wsResult.ResultCode, wsResult.ResultMessage, XmlHelper.Serialize(wsResult.ModelCollection));
                return t;
            });            
        }                

        public void GetAnnouncements(TestCase t)
        {
            ApiHelper.ExecuteWithClient(t.Request.ClientId, (timestamp, mac, salt) =>
            {
                string hash = Hasher.GetHashFromParameters(salt, timestamp, mac);
                var wsResult = this.webservice.GetAnnouncements(timestamp, mac, hash);
                t.Response = new ApiResponse(wsResult.ResultCode, wsResult.ResultMessage, XmlHelper.Serialize(wsResult.ModelCollection));
                return t;
            });
        }

        public void GetDeliverypoints(TestCase t)
        {
            ApiHelper.ExecuteWithClient(t.Request.ClientId, (timestamp, mac, salt) =>
            {
                string hash = Hasher.GetHashFromParameters(salt, timestamp, mac, t.Request.DeliverypointgroupId);
                var wsResult = this.webservice.GetDeliverypoints(timestamp, mac, t.Request.DeliverypointgroupId, hash);
                t.Response = new ApiResponse(wsResult.ResultCode, wsResult.ResultMessage, XmlHelper.Serialize(wsResult.ModelCollection));
                return t;
            });            
        }

        public void GetTerminal(TestCase t)
        {
            ApiHelper.ExecuteWithTerminal(t.Request.TerminalId, (timestamp, mac, salt) =>
            {
                string hash = Hasher.GetHashFromParameters(salt, timestamp, mac, t.Request.TerminalId, t.Request.ApplicationVersion, t.Request.OsVersion, t.Request.DeviceType);
                var wsResult = this.webservice.GetTerminal(timestamp, mac, t.Request.TerminalId, t.Request.ApplicationVersion, t.Request.OsVersion, t.Request.DeviceType, hash);
                t.Response = new ApiResponse(wsResult.ResultCode, wsResult.ResultMessage, XmlHelper.Serialize(wsResult.ModelCollection));
                return t;
            });            
        }        

        public void GetOrders(TestCase t)
        {
            ApiHelper.ExecuteWithTerminal(t.Request.TerminalId, (timestamp, mac, salt) =>
            {
                string hash = Hasher.GetHashFromParameters(salt, timestamp, mac, t.Request.StatusCode, t.Request.StatusMessage, t.Request.Version, t.Request.IsConsole, t.Request.IsMasterConsole);
                var wsResult = this.webservice.GetOrders(timestamp, mac, t.Request.StatusCode, t.Request.StatusMessage, t.Request.Version, t.Request.IsConsole, t.Request.IsMasterConsole, hash);
                t.Response = new ApiResponse(wsResult.ResultCode, wsResult.ResultMessage, XmlHelper.Serialize(wsResult.ModelCollection));
                return t;
            });            
        }
        
        public void GetSite(TestCase t)
        {
            ApiHelper.ExecuteWithClient(t.Request.ClientId, (timestamp, mac, salt) =>
            {
                string hash = Hasher.GetHashFromParameters(salt, timestamp, mac, t.Request.SiteId, t.Request.DeviceType, t.Request.CultureCode);
                var wsResult = this.webservice.GetSite(timestamp, mac, t.Request.SiteId, t.Request.DeviceType, t.Request.CultureCode, hash);
                t.Response = new ApiResponse(wsResult.ResultCode, wsResult.ResultMessage, XmlHelper.Serialize(wsResult.ModelCollection));
                return t;
            });            
        }        

        public void GetPointOfInterest(TestCase t)
        {
            ApiHelper.ExecuteWithClient(t.Request.ClientId, (timestamp, mac, salt) =>
            {
                string hash = Hasher.GetHashFromParameters(salt, timestamp, mac, t.Request.PointOfInterestId, t.Request.DeviceType);
                var wsResult = this.webservice.GetPointOfInterest(timestamp, mac, t.Request.PointOfInterestId, t.Request.DeviceType, hash);
                t.Response = new ApiResponse(wsResult.ResultCode, wsResult.ResultMessage, XmlHelper.Serialize(wsResult.ModelCollection));
                return t;
            });            
        }

        public void SaveOrder(TestCase t)
        {
            ApiHelper.ExecuteWithClient(t.Request.ClientId, (timestamp, mac, salt) =>
            {
                string hash = Hasher.GetHashFromParameters(salt, timestamp, mac, t.Request.Xml);
                var wsResult = this.webservice.SaveOrder(timestamp, mac, t.Request.Xml, hash);
                t.Response = new ApiResponse(wsResult.ResultCode, wsResult.ResultMessage, XmlHelper.Serialize(wsResult.ModelCollection));
                return t;
            });            
        }

        public void IsAlive(TestCase t)
        {
            try
            {
                var wsResult = this.webservice.IsAlive();
                t.Response = new ApiResponse(100, "Success", string.Empty);
            }
            catch (Exception ex)
            {
                t.Response = new ApiResponse(999, ex.Message, string.Empty);
            }            
        }

        #endregion
    }
}
