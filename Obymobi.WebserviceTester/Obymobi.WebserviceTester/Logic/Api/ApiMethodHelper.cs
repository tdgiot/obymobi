﻿using Obymobi.WebserviceTester.Data.Models;
using Obymobi.WebserviceTester.Enums;
using Obymobi.WebserviceTester.Logic.Api.Methods;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Obymobi.WebserviceTester.Logic.Api
{
    public static class ApiMethodHelper
    {
        public static ApiMethod Create(ApiRequest request)
        {
            ApiMethodType type = request.ApiMethodType.ToEnum<ApiMethodType>();
            switch (type)
            {
                case ApiMethodType.GetAdvertisement:
                    return new GetAdvertisementsApiMethod();

                case ApiMethodType.GetAnnouncements:
                    return new GetAnnouncementsApiMethod();
                                                               
                case ApiMethodType.GetClient:
                    return new GetClientApiMethod();

                case ApiMethodType.GetCompany:
                    return new GetCompanyApiMethod();

                case ApiMethodType.GetDeliverypoints:
                    return new GetDeliverypointsApiMethod();

                case ApiMethodType.GetEntertainment:
                    return new GetEntertainmentApiMethod();

                case ApiMethodType.GetMenu:
                    return new GetMenuApiMethod();

                case ApiMethodType.GetOrders:
                    return new GetOrdersApiMethod();

                case ApiMethodType.GetPointOfInterest:
                    return new GetPointOfInterestApiMethod();

                case ApiMethodType.GetSite:
                    return new GetSiteApiMethod();

                case ApiMethodType.GetTerminal:
                    return new GetTerminalApiMethod();

                case ApiMethodType.SaveOrder:
                    return new SaveOrderApiMethod();

                case ApiMethodType.IsAlive:
                    return new IsAliveApiMethod();

                default:
                    throw new Exception("ApiMethodHelper - Create - Unknown ApiMethodType");
            }
        }
    }
}
