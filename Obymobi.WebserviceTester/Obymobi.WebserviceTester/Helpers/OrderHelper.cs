﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Obymobi.Data.EntityClasses;
using Dionysos;
using Obymobi.Enums;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;

namespace Obymobi.WebserviceTester.Helpers
{
    public class OrderHelper
    {
        public static Obymobi.Logic.Model.Mobile.Order CreateMobileOrderWithOrderItem(CustomerEntity customer, ProductEntity product, int companyId, int deliverypointId, AlterationitemCollection alterationitemCollection)
        {
            // Create order
            var order = new Obymobi.Logic.Model.Mobile.Order();
            order.Guid = GuidUtil.CreateGuid();
            order.CustomerId = customer.CustomerId;
            order.CompanyId = companyId;            
            order.DeliverypointId = deliverypointId;
            order.Type = (int)OrderType.Standard;
            order.Notes = "Order created by: Webservice Tester 2000";

            // Create order item
            Obymobi.Logic.Model.Mobile.Orderitem[] orderitems = new Obymobi.Logic.Model.Mobile.Orderitem[1];
            for (int i = 0; i < 1; i++)
            {
                var item = new Obymobi.Logic.Model.Mobile.Orderitem();
                item.ProductId = product.ProductId;
                item.ProductName = product.Name;
                item.Quantity = RandomUtil.Next(1, 50);
                item.ProductPriceIn = product.PriceIn ?? 0;
                item.Notes = "Orderitem created by: Webservice Tester 2000";

                // Check for alterations
                if (product.ProductAlterationCollection.Count > 0)
                {
                    EntityView<AlterationEntity> alterationsView = product.ProductAlterationCollection.AlterationCollection.DefaultView;

                    // Only use the alterations that are necessary (minoptions > 0)
                    alterationsView.Filter = new PredicateExpression(AlterationFields.MinOptions > 0);

                    var alterationitems = new List<Obymobi.Logic.Model.Mobile.Alterationitem>();
                    foreach (var alteration in alterationsView)
                    {
                        // Create alteration item
                        AlterationoptionEntity firstAlterationoption = alteration.AlterationitemCollection[0].AlterationoptionEntity;

                        var ai = new Obymobi.Logic.Model.Mobile.Alterationitem();
                        ai.AlterationitemId = alterationitemCollection.Single(option => option.AlterationId == alteration.AlterationId && option.AlterationoptionId == firstAlterationoption.AlterationoptionId).AlterationitemId;
                        ai.AlterationId = alteration.AlterationId;
                        ai.AlterationoptionId = firstAlterationoption.AlterationoptionId;

                        alterationitems.Add(ai);
                    }
                    item.Alterationitems = alterationitems.ToArray();
                }

                orderitems[0] = item;
            }
            order.Orderitems = orderitems;

            return order;
        }

        public static Obymobi.Logic.Model.Order CreateCraveOrderWithOrderItem(ClientEntity client, ProductEntity product, int companyId, int deliverypointId, AlterationitemCollection alterationitemCollection)
        {
            // Create order
            var order = new Obymobi.Logic.Model.Order();
            order.ClientId = client.ClientId;
            order.CompanyId = companyId;
            order.ClientMacAddress = client.MacAddress;
            order.DeliverypointId = deliverypointId;
            order.AgeVerificationType = 100;
            order.Type = (int)OrderType.Standard;
            order.Notes = "Order created by: Webservice Tester 2000";

            // Create order item
            Obymobi.Logic.Model.Orderitem[] orderitems = new Obymobi.Logic.Model.Orderitem[1];
            for (int i = 0; i < 1; i++)
            {
                var item = new Obymobi.Logic.Model.Orderitem();
                item.ProductId = product.ProductId;
                item.ProductName = product.Name;
                item.Quantity = RandomUtil.Next(1, 50);
                item.ProductPriceIn = product.PriceIn ?? 0;
                item.VatPercentage = 0;
                item.Notes = "Orderitem created by: Webservice Tester 2000";

                // Check for alterations
                if (product.ProductAlterationCollection.Count > 0)
                {
                    EntityView<AlterationEntity> alterationsView = product.ProductAlterationCollection.AlterationCollection.DefaultView;

                    // Only use the alterations that are necessary (minoptions > 0)
                    alterationsView.Filter = new PredicateExpression(AlterationFields.MinOptions > 0);

                    var alterationitems = new List<Obymobi.Logic.Model.Alterationitem>();
                    foreach (var alteration in alterationsView)
                    {
                        // Create alteration item
                        AlterationoptionEntity firstAlterationoption = alteration.AlterationitemCollection[0].AlterationoptionEntity;

                        var ai = new Obymobi.Logic.Model.Alterationitem();
                        ai.AlterationitemId = alterationitemCollection.Single(option => option.AlterationId == alteration.AlterationId && option.AlterationoptionId == firstAlterationoption.AlterationoptionId).AlterationitemId;
                        ai.AlterationId = alteration.AlterationId;
                        ai.AlterationoptionId = firstAlterationoption.AlterationoptionId;

                        alterationitems.Add(ai);
                    }
                    item.Alterationitems = alterationitems.ToArray();
                }

                orderitems[0] = item;
            }
            order.Orderitems = orderitems;

            return order;
        }
    }
}
