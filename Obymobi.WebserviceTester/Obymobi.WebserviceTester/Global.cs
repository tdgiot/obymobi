﻿using Dionysos;
using Dionysos.Logging;
using Obymobi.Configuration;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Logic.Status;
using Obymobi.WebserviceTester.CraveService2_1;
using Obymobi.WebserviceTester.Data;
using Obymobi.WebserviceTester.Helpers;
using Obymobi.WebserviceTester.Logic;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Obymobi.WebserviceTester
{
    public class Global
    {
        #region Properties

        public static string CraveService { get { return "craveservice.asmx"; } }
        public static string MobileService { get { return "mobileservice.asmx"; } }

        public static string RootDirectory { get; set; }
        public static string WebserviceUrl { get; set; }

        public static CloudStatus Status { get; set; }
        
        public static bool IsConsoleMode 
        { 
            get
            {
                return Logger.IsConsoleMode;
            }
            set
            {
                Logger.IsConsoleMode = value;
            }
        }
        
        #endregion

        #region Methods

        public static void Initialize()
        {
            // Initialize Assemblies
            InitializeAssemblies();

            // Initialize Globals
            InitializeGlobals();            
        }

        private static void InitializeAssemblies()
        {
            try
            {
                Dionysos.Global.AssemblyInfo.Add("Obymobi.Data.dll", "Data");
                Dionysos.Global.AssemblyInfo.Add("Obymobi.Logic.Legacy.dll", "Legacy");
            }
            catch (Exception e)
            {
                Log.Warning("Global", "InitializeAssemblies", string.Format("Unable to load assembly '{0}'", "Obymobi.Logic.Legacy.dll"));
            }
        }

        private static void InitializeGlobals()
        {
            // Initialize cloud status
            Global.Status = new CloudStatus(true);

            // Check with which cloud environment we should work.
            // Upgrade for backwards compability with the new CloudEnvironment & AlternativeWebserviceUrl
            if (ConfigurationManager.GetString(CraveCloudConfigConstants.CloudEnvironment) == "NOTSET")
            {
                Logger.Log("Upgrading to CloudEnvironment Configuration");

                // Old installation, upgrade!
                string webserviceUrl = ConfigurationManager.GetString(CraveOnSiteServerConfigConstants.WebserviceUrl1);

                if (webserviceUrl.Contains("app.", StringComparison.InvariantCultureIgnoreCase))
                    ConfigurationManager.SetValue(CraveCloudConfigConstants.CloudEnvironment, CloudEnvironment.ProductionPrimary.ToString());
                else if (webserviceUrl.Contains("dev.", StringComparison.InvariantCultureIgnoreCase))
                    ConfigurationManager.SetValue(CraveCloudConfigConstants.CloudEnvironment, CloudEnvironment.Development.ToString());
                else if (webserviceUrl.Contains("test.", StringComparison.InvariantCultureIgnoreCase))
                    ConfigurationManager.SetValue(CraveCloudConfigConstants.CloudEnvironment, CloudEnvironment.Test.ToString());
                else
                {
                    ConfigurationManager.SetValue(CraveCloudConfigConstants.CloudEnvironment, CloudEnvironment.Manual.ToString());
                    ConfigurationManager.SetValue(CraveCloudConfigConstants.ManualWebserviceBaseUrl, webserviceUrl);
                }

                Logger.Log("CloudEnvironment Configuration upgraded to: {0}", ConfigurationManager.GetString(CraveCloudConfigConstants.CloudEnvironment));
            }

            // Refresh the cloud status
            //Global.Status.Refresh();

            //if (Global.Status.IsWebserviceAvailable)
            //{                
                Global.WebserviceUrl = Global.Status.WebserviceBaseUrl;

                if (Global.WebserviceUrl.EndsWith("/"))
                    Global.WebserviceUrl = Global.WebserviceUrl.Substring(0, Global.WebserviceUrl.Length - 1);

                Logger.Log("Initialized webservice base url '{0}'...", Global.WebserviceUrl);
            //}
        }

        #endregion
    }
}
