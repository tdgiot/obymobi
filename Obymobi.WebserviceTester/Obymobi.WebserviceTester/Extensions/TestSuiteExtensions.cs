﻿using Obymobi.Data.EntityClasses;
using Obymobi.WebserviceTester.Data;
using Obymobi.WebserviceTester.Data.Models;
using Obymobi.WebserviceTester.Logic.Api;
using Obymobi.WebserviceTester.Logic.Test;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Obymobi.WebserviceTester.Extensions
{
    public static class TestSuiteExtensions
    {
        public static string GetFilename(this TestSuite testSuite)
        {
            return string.Format("{0}.xml", testSuite.Identifier);
        }

        public static void Update(this TestSuite testSuite, TestSuite updatedTestSuite)
        {
            testSuite.Name = updatedTestSuite.Name;
            testSuite.ApiUrl = updatedTestSuite.ApiUrl;
            testSuite.ApiVersionType = updatedTestSuite.ApiVersionType;
            testSuite.Requests = updatedTestSuite.Requests;
        }

        public static void AddOrUpdateRequest(this TestSuite testSuite, ApiRequest request)
        {
            ApiRequest tmp = testSuite.Requests.SingleOrDefault(x => x.Identifier == request.Identifier);
            if (tmp == null)
            {
                testSuite.Requests.Add(request);                
            }
            else
            {
                tmp.Update(request);
            }
        }        

        public static void DeleteRequest(this TestSuite testSuite, ApiRequest request)
        {
            ApiRequest tmp = testSuite.Requests.SingleOrDefault(x => x.Identifier == request.Identifier);
            if (tmp != null)
            {
                testSuite.Requests.Remove(tmp);                
            }            
        }        

        public static TestRun ToTestRun(this TestSuite testSuite)
        {
            TestRun testRun = new TestRun();
            testRun.TestSuiteIdentifier = testSuite.Identifier;
            testRun.Name = testSuite.Name;
            testRun.ApiUrl = testSuite.ApiUrl;
            testRun.ApiVersionType = testSuite.ApiVersionType;

            TestCaseCollection testCases = new TestCaseCollection();
            for (int i = 0; i < testSuite.Requests.Count; i++)
            {
                ApiRequest request = testSuite.Requests[i].Clone();
                testCases.Add(new TestCase(request));                
            }
            testRun.TestCases = testCases;

            return testRun;
        }
    }
}
