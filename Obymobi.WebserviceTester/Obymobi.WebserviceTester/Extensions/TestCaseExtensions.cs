﻿using Obymobi.WebserviceTester.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Obymobi.WebserviceTester.Extensions
{
    public static class TestCaseExtensions
    {
        public static void Update(this TestCase testCase, TestCase updatedTestCase)
        {
            testCase.Elapsed = updatedTestCase.Elapsed;
            testCase.Processed = updatedTestCase.Processed;
            testCase.Response = updatedTestCase.Response;
            testCase.Started = updatedTestCase.Started;
        }
    }
}
