﻿using Obymobi.Data.EntityClasses;
using Obymobi.WebserviceTester.Data;
using Obymobi.WebserviceTester.Data.Models;
using Obymobi.WebserviceTester.Logic.Api;
using Obymobi.WebserviceTester.Logic.Test;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Obymobi.WebserviceTester.Extensions
{
    public static class ApiRequestExtensions
    {
        public static void Update(this ApiRequest apiRequest, ApiRequest updatedRequest)
        {
            apiRequest.ApiMethodType = updatedRequest.ApiMethodType;
            apiRequest.ClientId = updatedRequest.ClientId;
            apiRequest.TerminalId = updatedRequest.TerminalId;
            apiRequest.CompanyId = updatedRequest.CompanyId;
            apiRequest.DeliverypointgroupId = updatedRequest.DeliverypointgroupId;
            apiRequest.DeliverypointId = updatedRequest.DeliverypointId;
            apiRequest.PointOfInterestId = updatedRequest.PointOfInterestId;
            apiRequest.SiteId = updatedRequest.SiteId;
            apiRequest.MenuId = updatedRequest.MenuId;
            apiRequest.CultureCode = updatedRequest.CultureCode;
            apiRequest.ApplicationVersion = updatedRequest.ApplicationVersion;
            apiRequest.OsVersion = updatedRequest.OsVersion;
            apiRequest.DeviceType = updatedRequest.DeviceType;
            apiRequest.DeviceModel = updatedRequest.DeviceModel;
            apiRequest.StatusCode = updatedRequest.StatusCode;
            apiRequest.StatusMessage = updatedRequest.StatusMessage;
            apiRequest.Version = updatedRequest.Version;
            apiRequest.IsConsole = updatedRequest.IsConsole;
            apiRequest.IsMasterConsole = updatedRequest.IsMasterConsole;
            apiRequest.Xml = updatedRequest.Xml;            
        }

        public static void ApplyClient(this ApiRequest apiRequest, ClientEntity client)
        {
            apiRequest.ClientId = client.ClientId;
            apiRequest.CompanyId = client.CompanyId;
            apiRequest.DeliverypointgroupId = client.DeliverypointGroupId ?? 0;
            apiRequest.DeliverypointId = client.DeliverypointId ?? 0;
            apiRequest.DeviceModel = (int)client.DeviceEntity.DeviceModel;
            apiRequest.DeviceType = (int)client.DeviceEntity.Type;
        }
    }
}
