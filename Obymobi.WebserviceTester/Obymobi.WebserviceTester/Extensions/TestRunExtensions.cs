﻿using Obymobi.Data.EntityClasses;
using Obymobi.WebserviceTester.Data;
using Obymobi.WebserviceTester.Data.Models;
using Obymobi.WebserviceTester.Logic.Api;
using Obymobi.WebserviceTester.Logic.Test;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Obymobi.WebserviceTester.Extensions
{
    public static class TestRunExtensions
    {
        public static string GetFilename(this TestRun testRun)
        {            
            return string.Format("{0}.xml", testRun.Identifier);
        }

        public static void Update(this TestRun testRun, TestRun updatedTestRun)
        {
            testRun.Name = updatedTestRun.Name;
            testRun.ApiUrl = updatedTestRun.ApiUrl;
            testRun.Processed = updatedTestRun.Processed;
            testRun.Elapsed = updatedTestRun.Elapsed;
            testRun.ApiVersionType = updatedTestRun.ApiVersionType;
            testRun.TestCases = updatedTestRun.TestCases;
        }        
    }
}
