﻿using Obymobi.Interfaces;

namespace Obymobi.Logic.Routing.Requests
{
    public class HandleRoutestepRequest : IHandleRoutestepRequest
    {
        public int OrderRoutestephandlerEntityId { get; set; }
    }
}
