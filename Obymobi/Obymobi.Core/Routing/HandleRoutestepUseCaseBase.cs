﻿using Obymobi.Interfaces;
using Obymobi.Logic.Routing.Requests;

namespace Obymobi.Logic.Routing.UseCases
{
    public abstract class HandleRoutestepUseCaseBase<TRequest> : IHandleRoutestepUseCase where TRequest : IHandleRoutestepRequest
    {
        public void HandleRoutestep(IHandleRoutestepRequest request)
        {
            this.HandleRoutestep((TRequest)request);
        }

        public abstract void HandleRoutestep(TRequest request);
    }
}
