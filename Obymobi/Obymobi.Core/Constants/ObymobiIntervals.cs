﻿namespace Obymobi.Constants
{
    /************************************************************************************************************
     * NOTE:
     * When you change values in this file, you also have to change the value in the Google Docs document
     * https://docs.google.com/a/crave-emenu.com/document/d/1hrd79Jh43bMxWxvBiAg6lhETmwBKQdxiUVgV5I6mxZI/edit#
     ************************************************************************************************************/
    public class ObymobiIntervals
    {
        public const int COMETSERVER_PINGPONG_INTERVAL = 10000;
        public const int COMETSERVER_PINGPONG_TIMEOUT = 10000;
        public const int COMETSERVER_STATSLOGGING_INTERVAL = 10000;
        public const int COMETSERVER_STATSLOGGING_TIMEOUT = 20000;
        public const int COMETSERVER_POKEINCLIENTLOG_INTERVAL = 30000;
        public const int COMETSERVER_NETMESSAGEPOLLER_INTERVAL = 15000;
        public const int COMETSERVER_NETMESSAGEPOLLER_TIMEOUT = 15000;
        public const int COMETSERVER_PONGER_INTERVAL = 15000;
        public const int COMETSERVER_PONGER_TIMEOUT = 60000;

        public const int SERVICES_DATABASEPOLLER_INTERVAL = 30000;
        public const int SERVICES_SUPPORTNOTIFICATIONPOLLER_INTERVAL = 7500;
        public const int SERVICES_MEDIATASKPOLLER_INTERVAL = 7500;
        public const int SERVICES_CLOUDTASKPOLLER_INTERVAL = 7500;
        public const int SERVICES_REPORTINGTASKPOLLER_INTERVAL = 7500;
        public const int SERVICES_COMPANYMANAGEMENTPOLLER_INTERVAL = 7500;
        public const int SERVICES_PERFORMANCEPOLLER_INTERVAL = 60000;
        public const int SERVICES_EXTERNALCONTENTPOLLER_INTERVAL = 60000;
        public const int SERVICES_WEATHERPOLLER_INTERVAL = 60000;
        public const int SERVICES_HOTSOSPOLLER_INTERVAL = 60000;
        public const int SERVICES_MESSAGEPOLLER_NORMAL_INTERVAL = 5000;
        public const int SERVICES_MESSAGEPOLLER_BATCH_INTERVAL = 15000;
        public const int SERVICES_GAMESESSIONREPORTPOLLER_INTERVAL = 60000;
        public const int SERVICES_SCHEDULEDCOMMANDTASKPOLLER_INTERVAL = 60000;
        public const int SERVICES_PMSTERMINALSTATUSPOLLER_INTERVAL = 15000;
        public const int SERVICES_ANALYTICSPROCESSINGTASKPOLLER_INTERVAL = 7500;
        public const int SERVICES_DEVICETOKENTASKPOLLER_INTERVAL = 15000;
        public const int SERVICES_UISCHEDULEPOLLER_INTERVAL = 15000;

        public const int MESSAGEHELPER_DELETEMESSAGE_THRESHOLD = 300000;

        public const int NOCSERVICE_NOCPOLLER_INTERVAL = 7500;
        public const int NOCSERVICE_SUPPORTPOOL_INTERVAL = 300000;

        public const int DEVICE_OFFLINE_THRESHOLD = 180000;
        public const int SUPPORT_NOTIFICATIONS_TERMINAL_OFFLINE_THRESHOLD = 900000;
        public const int SUPPORT_NOTIFICATIONS_COMET_CONNECTION_DOWN_THRESHOLD = 180000;
        public const int SUPPORT_NOTIFICATIONS_OTOUCHO_TERMINAL_OFFLINE_THRESHOLD = 180000;

        public const int CLIENT_WAKE_UP_TIME_THRESHOLD = 300000;

        public const int MEDIA_TASK_POLLER_DELETE_DELAY_DAYS = 30;

        public const int MEDIA_STORAGE_CACHE_IN_SECONDS = (24 * 60 * 60);
    }
}
