﻿namespace Obymobi.Constants
{
    public static class ApplicationCode
    {
        public const string Emenu = "CraveEmenuAndroidTablet";
        public const string Console = "CraveConsoleAndroidTablet";
        public const string Agent = "CraveAgentAndroid";
        public const string SupportTools = "CraveSupportToolsAndroid";
        public const string MessagingService = "CraveMessagingServiceAndroid";

        public const string OnsiteAgent = "CraveOnsiteAgent";
        public const string OnsiteServer = "CraveOnsiteServer";

        // Galaxy Tab 10.1 2
        public const string CraveOSSamsungP5110 = "CraveOsIceCreamP5110";
        // Sony Xperia Tablet Z
        public const string CraveOSSonySGP311 = "CraveOsSonySPG311";
        // Sony Xperia Tablet Z2
        public const string CraveOSSonySGP511 = "CraveOsSonySPG511";

        public const string CraveOsIntelAnzhen4 = "CraveOsIntelAnzhen4";
        public const string CraveOsTmini = "CraveOsRockchipRK312x";
        public const string CraveOsTminiV2 = "CraveOsRockchipRK312x_v2";
    }
}
