﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dionysos;
using System.Web;

namespace Obymobi
{
    /// <summary>
    /// Enum o
    /// </summary>
    public class WebApplicationPath
    {
        public enum WebApplicationPathResults : int
        {
            HttpContextIsNull = 200,
            HttpContextRequestIsNull = 201
        }

        /// <summary>
        /// Returns the path for the Console Application: 'console'
        /// </summary>
        public const string Console = "console";

        /// <summary>
        /// Returns the path for the Mobile Application: 'mobile'
        /// </summary>
        public const string Mobile = "mobile";

        /// <summary>
        /// Returns the path for the Management Application: 'management'
        /// </summary>
        public const string Management = "management";

        /// <summary>
        /// Returns the path for the Noc Application: 'noc'
        /// </summary>
        public const string Noc = "noc";

        /// <summary>
        /// Returns the path for the Messaging Application: 'messaging'
        /// </summary>
        public const string Messaging = "messaging";

        /// <summary>
        /// Returns the path for the Api Application: 'api'
        /// </summary>
        public const string Api = "api";

        /// <summary>
        /// Returns the Console url without a trailing slash (i.e. http://currentdomain.com/console)
        /// </summary>
        /// <returns></returns>
        public static string GetConsoleUrl()
        {
            WebApplicationPath.ValidateHttpContext();
            return HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority) + "/" + WebApplicationPath.Console;
        }

        /// <summary>
        /// Returns the Console Url + the path you want to append 
        /// </summary>
        /// <param name="pathToAppend">Path to append without starting slash (i.e. default.aspx), start slash will be added automatically</param>
        /// <returns></returns>
        public static string GetConsoleUrl(string pathToAppend)
        {
            return WebApplicationPath.GetConsoleUrl() + WebApplicationPath.AddSlash(pathToAppend);
        }

        /// <summary>
        /// Returns the Mobile url without a trailing slash (i.e. http://currentdomain.com/mobile)
        /// </summary>
        /// <returns></returns>
        public static string GetMobileUrl()
        {
            WebApplicationPath.ValidateHttpContext();
            return HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority) + "/" + WebApplicationPath.Mobile;
        }

        /// <summary>
        /// Returns the Mobile Url + the path you want to append 
        /// </summary>
        /// <param name="pathToAppend">Path to append without starting slash (i.e. default.aspx), start slash will be added automatically</param>
        /// <returns></returns>
        public static string GetMobileUrl(string pathToAppend)
        {
            return WebApplicationPath.GetMobileUrl() + WebApplicationPath.AddSlash(pathToAppend);
        }

        /// <summary>
        /// Returns the Management url without a trailing slash (i.e. http://currentdomain.com/management)
        /// </summary>
        /// <returns></returns>
        public static string GetManagementUrl()
        {
            WebApplicationPath.ValidateHttpContext();
            return HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority) + "/" + WebApplicationPath.Management;
        }

        /// <summary>
        /// Returns the Management Url + the path you want to append 
        /// </summary>
        /// <param name="pathToAppend">Path to append without starting slash (i.e. default.aspx), start slash will be added automatically</param>
        /// <returns></returns>
        public static string GetManagementUrl(string pathToAppend)
        {
            return WebApplicationPath.GetManagementUrl() + WebApplicationPath.AddSlash(pathToAppend);
        }

        /// <summary>
        /// Returns the Noc url without a trailing slash (i.e. http://currentdomain.com/noc)
        /// </summary>
        /// <returns></returns>
        public static string GetNocUrl()
        {
            WebApplicationPath.ValidateHttpContext();
            return HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority) + "/" + WebApplicationPath.Noc;
        }

        /// <summary>
        /// Returns the Noc Url + the path you want to append 
        /// </summary>
        /// <param name="pathToAppend">Path to append without starting slash (i.e. default.aspx), start slash will be added automatically</param>
        /// <returns></returns>
        public static string GetNocUrl(string pathToAppend)
        {
            return WebApplicationPath.GetNocUrl() + WebApplicationPath.AddSlash(pathToAppend);
        }

        /// <summary>
        /// Returns the Messaging url without a trailing slash (i.e. http://currentdomain.com/messaging)
        /// </summary>
        /// <returns></returns>
        public static string GetMessagingUrl()
        {
            WebApplicationPath.ValidateHttpContext();
            return HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority) + "/" + WebApplicationPath.Messaging;
        }

        /// <summary>
        /// Returns the Messaging Url + the path you want to append 
        /// </summary>
        /// <param name="pathToAppend">Path to append without starting slash (i.e. default.aspx), start slash will be added automatically</param>
        /// <returns></returns>
        public static string GetMessagingUrl(string pathToAppend)
        {
            return WebApplicationPath.GetMessagingUrl() + WebApplicationPath.AddSlash(pathToAppend);
        }

        /// <summary>
        /// Returns the Api url without a trailing slash (i.e. http://currentdomain.com/api)
        /// </summary>
        /// <returns></returns>
        public static string GetApiUrl()
        {
            WebApplicationPath.ValidateHttpContext();
            return HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority) + "/" + WebApplicationPath.Api;
        }

        /// <summary>
        /// Returns the Api Url + the path you want to append 
        /// </summary>
        /// <param name="pathToAppend">Path to append without starting slash (i.e. default.aspx), start slash will be added automatically</param>
        /// <returns></returns>
        public static string GetApiUrl(string pathToAppend)
        {
            return WebApplicationPath.GetApiUrl() + WebApplicationPath.AddSlash(pathToAppend);
        }

        private static string AddSlash(string path)
        {
            if (path.StartsWith("/"))
            {
                return path;
            }
            else
                return "/" + path;
        }

        private static void ValidateHttpContext()
        {
            if (HttpContext.Current == null)
                throw new ObymobiException(WebApplicationPathResults.HttpContextIsNull);
            else if (HttpContext.Current.Request == null)
                throw new ObymobiException(WebApplicationPathResults.HttpContextRequestIsNull);
        }

    }
}
