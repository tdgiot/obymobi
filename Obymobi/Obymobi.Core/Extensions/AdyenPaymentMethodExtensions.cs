﻿using System;
using System.Collections.Generic;
using Dionysos.Extensions;
using Obymobi.Attributes;
using Obymobi.Enums;

namespace Obymobi.Extensions
{
    public static class AdyenPaymentMethodExtensions
    {
        public static IEnumerable<AdyenPaymentMethodBrand> GetAdyenPaymentMethodBrands<TEnum>(this TEnum enumerator)
            where TEnum : Enum
        {
            AdyenPaymentMappingAttribute adyenPaymentMappingAttribute = enumerator.GetCustomAttributeFromEnumField<TEnum, AdyenPaymentMappingAttribute>();

            return adyenPaymentMappingAttribute.AdyenPaymentMethodBrands;
        }
    }
}
