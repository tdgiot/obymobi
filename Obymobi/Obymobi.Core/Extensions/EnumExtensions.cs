﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Obymobi.Extensions
{
	public static class EnumExtensions
	{
		/// <summary>
		///     Obtains a list of Enum values between the range of the from and till parameter.
		///     Note that till isn't included inside this collection.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="enumerator">The enum object which contains the values.</param>
		/// <param name="from">The starting number of the collection.</param>
		/// <param name="till">The ending number of the range which defines where to stop.</param>
		/// <returns>Collection of Enum values between the provided range.</returns>
		public static IReadOnlyList<T> GetRangeUntil<T>(this T enumerator, int from, int till)
			where T : Enum => Array.AsReadOnly(Enum.GetValues(enumerator.GetType()).Cast<int>().Where(e => e >= from && e < till).Cast<T>().ToArray());

		/// <summary>
		///     Validates if the enumerator value is matching with one of the provided values.
		/// </summary>
		/// <typeparam name="TEnum">The type of the enumerator that should be compared</typeparam>
		/// <param name="enumerator">The current enumerator value which should be compared</param>
		/// <param name="matchingEnums">The allowed values that should be validated</param>
		/// <returns></returns>
		public static bool IsIn<TEnum>(this TEnum enumerator, params TEnum[] matchingEnums)
			where TEnum : Enum, IComparable, IFormattable, IConvertible
		{
			Type enumType = typeof(TEnum);

			if (!enumType.IsEnum)
			{
				throw new ArgumentException("This object provided to this extension isn't an enumerator.");
			}

			return matchingEnums.Any(matchingEnum => enumerator.CompareTo(matchingEnum) == 0);
		}
	}
}
