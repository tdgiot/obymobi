﻿using Obymobi.Enums;

namespace Obymobi.Extensions
{
    public static class DeviceModelExtensions
    {
        public static bool IsT3(this DeviceModel? deviceModel)
        {
            return deviceModel != null && (deviceModel == DeviceModel.RockchipRK3566_10inch ||
                                           deviceModel == DeviceModel.RockchipRK3566_8inch);
        }
    }
}
