﻿using Obymobi.Enums;
using Obymobi.Interfaces;

namespace Obymobi.Extensions
{
    public static class ITagExtensions
    {
        public static TagType GetTagType(this ITag tag)
        {
            return tag.CompanyId.GetValueOrDefault() > 0 
                ? TagType.Company
                : TagType.System;
        }
    }
}
