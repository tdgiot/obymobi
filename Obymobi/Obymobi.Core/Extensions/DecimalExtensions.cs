﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dionysos;

namespace Obymobi.Extensions
{
    public static class DecimalExtensions
    {
        /// <summary>
        /// Formats the price based on the culture code.
        /// </summary>
        /// <param name="price">The price to format.</param>
        /// <param name="cultureCode">The code of the culture to use.</param>
        /// <returns>Formatted price.</returns>
        public static string Format(this decimal price, string cultureCode)
        {
            if (cultureCode.IsNullOrWhiteSpace())
            {
                return price.ToString("C");
            }

            Culture culture = (Culture)cultureCode;
            return price.ToString("C", culture.CultureInfo);
        }
    }
}
