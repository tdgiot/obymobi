﻿namespace CraveOnsiteAgent.Configuration
{
    public static class CraveOnSiteAgentConfigConstants
    {
        /// <summary>
        /// Base url of the webservice 1
        /// </summary>
        public const string WebserviceBaseUrl1 = "WebserviceBaseUrl1";

        /// <summary>
        /// Base url of the webservice 2
        /// </summary>
        public const string WebserviceBaseUrl2 = "WebserviceBaseUrl2";

        /// <summary>
        /// Base url of the webservice 3
        /// </summary>
        public const string WebserviceBaseUrl3 = "WebserviceBaseUrl3";

        /// <summary>
        /// Interval between the requests to the webservice
        /// </summary>
        public const string RequestInterval = "RequestInterval";

        /// <summary>
        /// Username of the company owner
        /// </summary>
        public const string Username = "Username";

        /// <summary>
        /// Password of the company owner
        /// </summary>
        public const string Password = "Password";

        /// <summary>
        /// Id of the company
        /// </summary>
        public const string CompanyId = "CompanyId";
    }
}
