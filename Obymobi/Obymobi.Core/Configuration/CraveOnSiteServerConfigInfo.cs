﻿using Dionysos.Configuration;
using System;
using Obymobi.Constants;

namespace Obymobi.Configuration
{
    public class CraveOnSiteServerConfigInfo : ConfigurationItemCollection
    {
        const string SECTION_NAME = "CraveOnSiteServer";

        public CraveOnSiteServerConfigInfo()
        {
            // General
            Add(new ConfigurationItem(SECTION_NAME, CraveOnSiteServerConfigConstants.LastUploadedLogs, "Last time that logs were uploaded", DateTime.MinValue, typeof(DateTime)));

            // Login
            Add(new ConfigurationItem(SECTION_NAME, CraveOnSiteServerConfigConstants.CompanyOwner, "Company username for clients to login to OSS", "", typeof(string)));
            Add(new ConfigurationItem(SECTION_NAME, CraveOnSiteServerConfigConstants.CompanyPassword, "Company password for clients to login to OSS", "", typeof(string)));
            Add(new ConfigurationItem(SECTION_NAME, CraveOnSiteServerConfigConstants.TerminalId, "Terminal ID", 0, typeof(int)));
            Add(new ConfigurationItem(SECTION_NAME, CraveOnSiteServerConfigConstants.CompanyId, "Company ID", 0, typeof(int)));
            Add(new ConfigurationItem(SECTION_NAME, CraveOnSiteServerConfigConstants.CustomerId, "Customer ID", 0, typeof(int)));
            Add(new ConfigurationItem(SECTION_NAME, CraveOnSiteServerConfigConstants.DeliverypointgroupId, "Deliverypointgroup ID", 0, typeof(int)));
            Add(new ConfigurationItem(SECTION_NAME, CraveOnSiteServerConfigConstants.MacAddress, "MacAddress", "", typeof(string)));
            Add(new ConfigurationItem(SECTION_NAME, CraveOnSiteServerConfigConstants.Salt, "Salt", "", typeof(string)));
            
            // Webservice
            Add(new ConfigurationItem(SECTION_NAME, CraveOnSiteServerConfigConstants.WebserviceUrl1, "Webservice 1 URL", ObymobiConstants.PrimaryWebserviceUrl, typeof(string)));
            Add(new ConfigurationItem(SECTION_NAME, CraveOnSiteServerConfigConstants.WebserviceUrl2, "Webservice 2 URL", ObymobiConstants.SecondaryWebserviceUrl, typeof(string)));
            Add(new ConfigurationItem(SECTION_NAME, CraveOnSiteServerConfigConstants.RequestInterval, "Request interval to webservice", 30, typeof(int)));

            Add(new ConfigurationItem(SECTION_NAME, CraveOnSiteServerConfigConstants.PosIntegrationInformationLastModifiedTicks, "Last moment the Pos integration information was modified", -1, typeof(long)));
            Add(new ConfigurationItem(SECTION_NAME, CraveOnSiteServerConfigConstants.CompanyIdLastModifiedTicks, "Last companyId used in the config", 0, typeof(int)));

            Add(new ConfigurationItem(SECTION_NAME, CraveOnSiteServerConfigConstants.ManualCdnRootContainer, "Manual Cdn Root Container", "", typeof(string)));
            Add(new ConfigurationItem(SECTION_NAME, CraveOnSiteServerConfigConstants.CheckInternetAvailability, "Check Internet availability", true, typeof(bool)));
            Add(new ConfigurationItem(SECTION_NAME, CraveOnSiteServerConfigConstants.UrlProtocol, "Url protocol (0 = Default, 1 = HTTP, 2 = HTTPS)", 2, typeof(int)));
        }
    }
}