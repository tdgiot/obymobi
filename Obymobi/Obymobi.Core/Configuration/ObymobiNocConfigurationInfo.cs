﻿using System;
using System.Collections.Generic;
using System.Text;
using Dionysos.Configuration;
using Obymobi.Constants;

namespace Obymobi.Configuration
{
    /// <summary>
    /// Configuration class for Obymobi Noc Service
    /// </summary>
    public class ObymobiNocConfigurationInfo : ConfigurationItemCollection
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the ObymobiNocService.Configuration.ObymobiNocConfigurationInfo class
        /// </summary>
        public ObymobiNocConfigurationInfo()
        {
            string sectionName = "ObymobiNoc";
            this.Add(new ConfigurationItem(sectionName, ObymobiNocConfigurationConstants.CheckInterval, "Interval in seconds between checks", 60, typeof(int)));
            this.Add(new ConfigurationItem(sectionName, ObymobiNocConfigurationConstants.MaxMinutesOfflineTerminals, "Maximum amount of minutes that terminals can be offline", 10, typeof(int)));
            this.Add(new ConfigurationItem(sectionName, ObymobiNocConfigurationConstants.MaxMinutesOrders, "Maximum amount of minutes to get the last orders for", 10, typeof(int)));
            this.Add(new ConfigurationItem(sectionName, ObymobiNocConfigurationConstants.AdministratorPhonenumbers, "Phonenumbers of the administrators (comma separated)", string.Empty, typeof(string)));
            this.Add(new ConfigurationItem(sectionName, ObymobiNocConfigurationConstants.AdministratorPhonenumbersUK, "Phonenumbers of the administrators (comma separated) in the UK", string.Empty, typeof(string)));
            this.Add(new ConfigurationItem(sectionName, ObymobiNocConfigurationConstants.CompanyIdsUK, "Ids of the companies (comma separated) in the UK", string.Empty, typeof(string)));
            this.Add(new ConfigurationItem(sectionName, ObymobiNocConfigurationConstants.AdministratorEmailAddresses, "E-mail addresses of the administrators (comma separated)", string.Empty, typeof(string)));
            this.Add(new ConfigurationItem(sectionName, ObymobiNocConfigurationConstants.TimeBeforeSendingSMS, "Timeout in minutes after the state of the Obymobi systems has changed before sending a SMS to the administrators", 5, typeof(int)));
            this.Add(new ConfigurationItem(sectionName, ObymobiNocConfigurationConstants.TimeBeforeSendingEmail, "Timeout in minutes after the state of the Obymobi systems has changed before sending a e-mail to the administrators", 1, typeof(int)));
            this.Add(new ConfigurationItem(sectionName, ObymobiNocConfigurationConstants.NocWebserviceUrl1, "Url of the Noc website 1", ObymobiConstants.PrimaryWebserviceUrl + "NocWebservice.asmx", typeof(string)));
            this.Add(new ConfigurationItem(sectionName, ObymobiNocConfigurationConstants.NocWebserviceUrl2, "Url of the Noc website 2", ObymobiConstants.SecondaryWebserviceUrl + "NocWebservice.asmx", typeof(string)));
            this.Add(new ConfigurationItem(sectionName, ObymobiNocConfigurationConstants.ObymobiWebsiteUrl, "Url of the Obymobi website", "http://www.obymobi.com/StatusCheck.aspx?noccheck=indeed", typeof(string)));
            this.Add(new ConfigurationItem(sectionName, ObymobiNocConfigurationConstants.MobileWebsiteUrl, "Url of the Obymobi mobile website", "http://i.obymobi.com/StatusCheck.aspx?noccheck=indeed", typeof(string)));
            this.Add(new ConfigurationItem(sectionName, ObymobiNocConfigurationConstants.OtouchoWebsiteUrl, "Url of the Otoucho website", "http://www.otoucho.com/StatusCheck.aspx?noccheck=indeed", typeof(string)));
            this.Add(new ConfigurationItem(sectionName, ObymobiNocConfigurationConstants.BusinessHoursStart, "Start of the business hours in minutes", 480, typeof(int)));
            this.Add(new ConfigurationItem(sectionName, ObymobiNocConfigurationConstants.BusinessHoursEnd, "End of the business hours in minutes", 1050, typeof(int)));
            this.Add(new ConfigurationItem(sectionName, ObymobiNocConfigurationConstants.TeamviewerPath, "Path to the Teamviewer executable", @"C:\Program Files (x86)\TeamViewer\Version5\TeamViewer.exe", typeof(string)));
        }

        #endregion
    }
}
