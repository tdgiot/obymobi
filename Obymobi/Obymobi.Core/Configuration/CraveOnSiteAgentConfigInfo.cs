﻿using Dionysos.Configuration;
using System;
using Obymobi.Constants;

namespace CraveOnsiteAgent.Configuration
{
    public class CraveOnSiteAgentConfigInfo : ConfigurationItemCollection
    {
        const string sectionName = "CraveOnSiteAgent";

        public CraveOnSiteAgentConfigInfo()
        {
            // Webservice
            Add(new ConfigurationItem(sectionName, CraveOnSiteAgentConfigConstants.WebserviceBaseUrl1, "Webservice base url 1", "https://app.crave-emenu.com/", typeof(string)));
            Add(new ConfigurationItem(sectionName, CraveOnSiteAgentConfigConstants.WebserviceBaseUrl2, "Webservice base url 2", "https://app.crave-emenu.com/", typeof(string)));
            Add(new ConfigurationItem(sectionName, CraveOnSiteAgentConfigConstants.WebserviceBaseUrl3, "Webservice base url 3", "https://app.crave-emenu.com/", typeof(string)));
            Add(new ConfigurationItem(sectionName, CraveOnSiteAgentConfigConstants.RequestInterval, "Interval between the webservice requests", 60, typeof(int)));
            Add(new ConfigurationItem(sectionName, CraveOnSiteAgentConfigConstants.Username, "Username of the company owner", string.Empty, typeof(string)));
            Add(new ConfigurationItem(sectionName, CraveOnSiteAgentConfigConstants.Password, "Password of the company owner", string.Empty, typeof(string)));
            Add(new ConfigurationItem(sectionName, CraveOnSiteAgentConfigConstants.CompanyId, "Id of the company", -1, typeof(int)));
        }
    }
}
