﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Obymobi.Configuration
{
    /// <summary>
    /// Configuration class which contains constants for the names of configuration items
    /// </summary>
    public class ObymobiNocConfigurationConstants
    {
        #region Fields

        /// <summary>
        /// Interval between the checks to the Obymobi webservice and terminals
        /// </summary>
        public const string CheckInterval = "CheckInterval";

        /// <summary>
        /// Maximum amount of minutes that terminals can be offline
        /// </summary>
        public const string MaxMinutesOfflineTerminals = "MaxMinutesOfflineTerminals";

        /// <summary>
        /// Maximum amount of minutes to get the last orders for
        /// </summary>
        public const string MaxMinutesOrders = "MaxMinutesOrders";

        /// <summary>
        /// The phonenumbers of the administrators (comma separated)
        /// </summary>
        public const string AdministratorPhonenumbers = "AdministratorPhonenumbers";

        /// <summary>
        /// The phonenumbers of the administrators (comma separated) in the UK
        /// </summary>
        public const string AdministratorPhonenumbersUK = "AdministratorPhonenumbersUK";

        /// <summary>
        /// The ids of the UK companies (comma separated)
        /// </summary>
        public const string CompanyIdsUK = "CompanyIdsUK";

        /// <summary>
        /// The e-mail addresses of the administrators (comma separated)
        /// </summary>
        public const string AdministratorEmailAddresses = "AdministratorEmailAddresses";

        /// <summary>
        /// Timeout in minutes after the state of the Obymobi systems has changed before sending a SMS to the administrators
        /// </summary>
        public const string TimeBeforeSendingSMS = "TimeBeforeSendingSMS";

        /// <summary>
        /// Timeout in minutes after the state of the Obymobi systems has changed before sending an e-mail to the administrators
        /// </summary>
        public const string TimeBeforeSendingEmail = "TimeBeforeSendingEmail";

        /// <summary>
        /// Url of the Noc webservice 1
        /// </summary>
        public const string NocWebserviceUrl1 = "NocWebserviceUrl1";

        /// <summary>
        /// Url of the Noc webservice 2
        /// </summary>
        public const string NocWebserviceUrl2 = "NocWebserviceUrl2";

        /// <summary>
        /// Url of the Noc webservice 3
        /// </summary>
        public const string NocWebserviceUrl3 = "NocWebserviceUrl3";

        /// <summary>
        /// Url of the Obymobi website
        /// </summary>
        public const string ObymobiWebsiteUrl = "ObymobiWebsiteUrl";

        /// <summary>
        /// Url of the Obymobi mobile website
        /// </summary>
        public const string MobileWebsiteUrl = "MobileWebsiteUrl";

        /// <summary>
        /// Url of the Otoucho website
        /// </summary>
        public const string OtouchoWebsiteUrl = "OtouchoWebsiteUrl";

        /// <summary>
        /// Start of the business hours in minutes
        /// </summary>
        public const string BusinessHoursStart = "BusinessHoursStart";

        /// <summary>
        /// End of the business hours in minutes
        /// </summary>
        public const string BusinessHoursEnd = "BusinessHoursEnd";

        /// <summary>
        /// Path to the Teamviewer executable
        /// </summary>
        public const string TeamviewerPath = "TeamviewerPath";

        #endregion
    }
}
