﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Obymobi
{
    /// <summary>
    /// Configuration class which contains constants for the names of configuration items
    /// </summary>
    public class ObymobiDataConfigConstants
    {
        #region Fields

        /// <summary>
        /// Latest Company Data change
        /// </summary>
        public const string CompanyDataLastChanged = "CompanyDataLastChanged";

        public const string UseNoLockHintOnSelects = "UseNoLockHintOnSelects";

        public const string DatabaseClearedForNonLiveUsage = "DatabaseClearedForNonLiveUsage";

        public const string AmazonUsersRemovedForNonLiveUsage = "AmazonUsersRemovedForNonLiveUsage";

        /// <summary>
        /// Last time a Company was removed from the Company List used by the Mobile Webservice (see CompanyValidator for more details)
        /// </summary>
        public const string LastItemRemovedFromMobileCompanyList = "LastItemRemovedFromMobileCompanyList";

        /// <summary>
        /// Last time an AccessCode was removed used by the Mobile Webservice
        /// </summary>
        public const string LastItemRemovedFromAccessCodes = "LastItemRemovedFromAccessCodes";

        #endregion
    }
}
