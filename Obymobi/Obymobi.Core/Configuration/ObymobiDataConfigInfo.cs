﻿using System;
using System.Collections.Generic;
using System.Text;
using Dionysos.Configuration;

namespace Obymobi
{
    /// <summary>
    /// Configuration for Obymobi
    /// </summary>
    public class ObymobiDataConfigInfo : ConfigurationItemCollection
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the ObymobiConfigInof class
        /// </summary>
        public ObymobiDataConfigInfo()
        {
            string sectionName = "Obymobi.Data";
            this.Add(new ConfigurationItem(sectionName, ObymobiDataConfigConstants.CompanyDataLastChanged, "Company Data last Changed", DateTime.MinValue, typeof(DateTime), false));
            this.Add(new ConfigurationItem(sectionName, ObymobiDataConfigConstants.UseNoLockHintOnSelects, "Use no lock hint to prevent db locks", true, typeof(bool), false));
            this.Add(new ConfigurationItem(sectionName, ObymobiDataConfigConstants.DatabaseClearedForNonLiveUsage, "Indicates if the database was cleared for non live usage", false, typeof(bool), false));
            this.Add(new ConfigurationItem(sectionName, ObymobiDataConfigConstants.LastItemRemovedFromMobileCompanyList, "Last time a Company was removed from the Company List used by the Mobile Webservice", new DateTime(2000, 1, 1), typeof(DateTime), false));
            this.Add(new ConfigurationItem(sectionName, ObymobiDataConfigConstants.LastItemRemovedFromAccessCodes, "Last time an AccessCode was removed used by the Mobile Webservice", new DateTime(2000, 1, 1), typeof(DateTime), false));
            this.Add(new ConfigurationItem(sectionName, ObymobiDataConfigConstants.AmazonUsersRemovedForNonLiveUsage, "Indicates if users on amazon were removed for an environment after a database had been restored", false, typeof(bool), false));            
        }

        #endregion
    }
}
