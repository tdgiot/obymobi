﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Obymobi
{
    /// <summary>
    /// Configuration class which contains constants for the names of configuration items
    /// </summary>
    public class ObymobiConfigConstants
    {
        #region Fields

        /// <summary>
        /// Cloud Environment
        /// GK - This setting indeed already existed in CraveCloudConfigConstants, but it's already used with 'Application Specific' in multiple places
        /// therefore I introduced this second one. This one should become the generic one to identify for which environment a database is intended
        /// in particular the connection string poller will use it.
        /// </summary>
        public const string CraveCloudEnvironment = "CraveCloudEnvironment";

        /// <summary>
        /// Relative Media Path to the application root
        /// </summary>
        public const string MediaPath = "MediaPath";

        /// <summary>
        /// Url of the page which handles the MiniTix requests
        /// </summary>
        public const string MiniTixReturnUrl = "MiniTixReturnUrl";

        /// <summary>
        /// Url of the page which handles the MiniTix errors
        /// </summary>
        public const string MiniTixErrorUrl = "MiniTixErrorUrl";

        /// <summary>
        /// Mobile Website Url incl. protocol and without trailing slash
        /// </summary>
        public const string MobileWebsiteUrl = "MobileWebsiteUrl";

        /// <summary>
        /// Url of the page which handles the MiniTix errors
        /// </summary>
        public const string ShowDemoMotd = "ShowDemoMotd";

        /// <summary>
        /// Url of the page which handles the MiniTix errors
        /// </summary>
        public const string ShowDemoUpdateAvailable = "ShowDemoUpdateAvailable";

        /// <summary>
        /// Url of the page which handles the MiniTix errors
        /// </summary>
        public const string ShowDemoUpdateRequired = "ShowDemoUpdateRequired";

        /// <summary>
        /// Requestlog Log Raw Request 
        /// </summary>
        public const string RequestlogLogRawRequestsOnSuccess = "RequestlogLogRawRequestsOnSuccess";

        /// <summary>
        /// Requestlog Log Result Bodies
        /// </summary>
        public const string RequestlogLogResultBodyOnSuccess = "RequestlogLogResultBodyOnSuccess";

        /// <summary>
        /// Requestlog Log SuccessfulCalls
        /// </summary>
        public const string RequestlogLogSuccessfulCalls = "RequestlogLogSuccessfulCalls";

        /// <summary>
        /// Deteremine to monitor database mirroring (otherwise only primary instance)
        /// </summary>
        public const string MonitorDatabaseMirroring = "MonitorDatabaseMirroring";

        /// <summary>
        /// Check internet connection
        /// </summary>
        public const string CheckInternetConnection = "CheckInternetConnection";

        public const string FallbackSupportpoolPhonenumbers = "FallbackSupportpoolPhonenumbers";

        public const string FallbackSupportpoolEmailAddresses = "FallbackSupportpoolEmailAddresses";

        public const string CompanyExportFolder = "CompanyExportFolder";

        public const string MinimumVersionMobileApp = "MinimumVersionMobileApp";

        public const string OfficeIpAddresses = "OfficeIpAddresses";

        #endregion
    }
}
