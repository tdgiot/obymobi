﻿using System;
using System.Collections.Generic;
using System.Text;
using Dionysos.Configuration;
using Obymobi.Constants;
using Obymobi.Enums;

namespace Obymobi
{
    /// <summary>
    /// Configuration for Obymobi
    /// </summary>
    public class ObymobiConfigInfo : ConfigurationItemCollection
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the ObymobiConfigInof class
        /// </summary>
        public ObymobiConfigInfo()
        {
            string sectionName = "Obymobi";
            this.Add(new ConfigurationItem(sectionName, ObymobiConfigConstants.CraveCloudEnvironment, "Cloud Environment: ProductionPrimary, ProductionSecondary, ProductionOverwrite, Test, Development, Demo or Manual", CloudEnvironment.Manual, typeof(string), false));
            this.Add(new ConfigurationItem(sectionName, ObymobiConfigConstants.MediaPath, "Relative path to the media folder", "~/Media", typeof(string)));
            this.Add(new ConfigurationItem(sectionName, ObymobiConfigConstants.MiniTixReturnUrl, "Url of the page which handles the MiniTix requests", "", typeof(string)));
            this.Add(new ConfigurationItem(sectionName, ObymobiConfigConstants.MiniTixErrorUrl, "Url of the page which handles the MiniTix errors", "", typeof(string)));
            this.Add(new ConfigurationItem(sectionName, ObymobiConfigConstants.ShowDemoMotd, "If true, show a demo Message of the Day ", false, typeof(bool)));
            this.Add(new ConfigurationItem(sectionName, ObymobiConfigConstants.ShowDemoUpdateAvailable, "If true show a demo Update Avaliable", false, typeof(bool)));
            this.Add(new ConfigurationItem(sectionName, ObymobiConfigConstants.ShowDemoUpdateRequired, "If true, show a demo Update Required", false, typeof(bool)));
            this.Add(new ConfigurationItem(sectionName, ObymobiConfigConstants.RequestlogLogRawRequestsOnSuccess, "Indicate if the Raw Request should be logged for successful calls", false, typeof(bool)));
            this.Add(new ConfigurationItem(sectionName, ObymobiConfigConstants.RequestlogLogResultBodyOnSuccess, "Indicate if the Result Body should be logged for successful calls", false, typeof(bool)));
            this.Add(new ConfigurationItem(sectionName, ObymobiConfigConstants.RequestlogLogSuccessfulCalls, "Indicate if successful calls should be logged", false, typeof(bool)));
            this.Add(new ConfigurationItem(sectionName, ObymobiConfigConstants.MobileWebsiteUrl, "Url of the mobile website", "http://i.obymobi.com", typeof(string)));
            this.Add(new ConfigurationItem(sectionName, ObymobiConfigConstants.MobileWebsiteUrl, "Url of the mobile website", "http://i.obymobi.com", typeof(string)));
            this.Add(new ConfigurationItem(sectionName, ObymobiConfigConstants.MonitorDatabaseMirroring, "Deteremine to monitor database mirroring (otherwise only primary instance)", true, typeof(bool)));
            this.Add(new ConfigurationItem(sectionName, ObymobiConfigConstants.CheckInternetConnection, "Deteremine if the internet connection should be checked.", true, typeof(bool)));
            this.Add(new ConfigurationItem(sectionName, ObymobiConfigConstants.FallbackSupportpoolPhonenumbers, "Phonenumbers to notify if a support pool is missing or incomplete.", ObymobiConstants.SupportpoolFallbackPhonenumbersCsv(), typeof(string), false));
            this.Add(new ConfigurationItem(sectionName, ObymobiConfigConstants.FallbackSupportpoolEmailAddresses, "E-mailaddresses to notify if a support pool is missing or incomplete.", ObymobiConstants.SupportpoolFallbackEmailaddressesCsv(), typeof(string), false));
            this.Add(new ConfigurationItem(sectionName, ObymobiConfigConstants.CompanyExportFolder, "Companies backups created by the MenuManager will be placed in this folder.", "C:/inetpub/exports", typeof(string)));
            this.Add(new ConfigurationItem(sectionName, ObymobiConfigConstants.MinimumVersionMobileApp, "Minimum version supported for the mobile App", "1.2013000000", typeof(string)));
            this.Add(new ConfigurationItem(sectionName, ObymobiConfigConstants.OfficeIpAddresses, "Comma separated list of public ip addresses of NL and UK offices", "83.98.229.10, 82.197.222.108, 90.152.2.44", typeof(string)));
        }

        #endregion
    }
}
