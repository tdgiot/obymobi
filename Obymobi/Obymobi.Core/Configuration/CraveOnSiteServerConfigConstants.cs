﻿namespace Obymobi.Configuration
{
    public static class CraveOnSiteServerConfigConstants
    {
        /// <summary>
        /// 
        /// </summary>
        public const string WebserviceUrl1 = "WebserviceURL1";

        /// <summary>
        /// 
        /// </summary>
        public const string WebserviceUrl2 = "WebserviceURL2";

        /// <summary>
        /// 
        /// </summary>
        public const string CompanyOwner = "CompanyOwner";

        /// <summary>
        /// 
        /// </summary>
        public const string CompanyPassword = "CompanyPassword";

        /// <summary>
        /// 
        /// </summary>
        public const string TerminalId = "TerminalId";

        /// <summary>
        /// 
        /// </summary>
        public const string CompanyId = "CompanyId";

        /// <summary>
        /// 
        /// </summary>
        public const string CustomerId = "CustomerId";

        /// <summary>
        /// 
        /// </summary>
        public const string MacAddress = "MacAddress";

        /// <summary>
        /// 
        /// </summary>
        public const string Salt = "Salt";

        /// <summary>
        /// Request interval
        /// </summary>
        public const string RequestInterval = "RequestInterval";

        /// <summary>
        /// Gets or sets the Ticks of the last moment the Company related PosIntegrationInformation was changed
        /// </summary>
        public const string PosIntegrationInformationLastModifiedTicks = "PosIntegrationInformationLastModifiedTicks";

        /// <summary>
        /// Gets or sets the CompanyId for which the LastModifiedTicks are registered
        /// </summary>
        public const string CompanyIdLastModifiedTicks = "CompanyIdLastModifiedTicks";

        /// <summary>
        /// Gets or sets the DeliverypointgroupId
        /// </summary>
        public const string DeliverypointgroupId = "DeliverypointgroupId";

        /// <summary>
        /// Gets or sets the LastUploadedLogs
        /// </summary>
        public const string LastUploadedLogs = "LastUploadedLogs";

        /// <summary>
        /// Gets or sets the LastUploadedLogs
        /// </summary>
        public const string ManualCdnRootContainer = "ManualCdnRootContainer";

        /// <summary>
        /// Gets or sets the flag that determines whether the internet availability check should be performed.
        /// </summary>
        public const string CheckInternetAvailability = "CheckInternetAvailability";

        /// <summary>
        /// Gets or sets the url protocol (HTTP/HTTPS).
        /// </summary>
        public const string UrlProtocol = "UrlProtocol";
    }
}
