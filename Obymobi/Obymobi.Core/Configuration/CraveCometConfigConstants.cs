﻿using Dionysos.Configuration;

namespace Obymobi.Configuration
{
    public class CraveCometConfigConstants : ConfigurationItemCollection
    {
        public const string LOGGING_LEVEL = "LoggingLevel";
        public const string LAST_COMET_MESSAGE_FROM_WEBSERVICE = "LastCometMessageFromWebservice";
        public const string USE_PONG_QUEUE = "UsePongQueue";
    }
}
