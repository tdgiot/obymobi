﻿using Dionysos;
using Dionysos.Configuration;
using System;

namespace Obymobi.Configuration
{
    public class CraveCometConfigInfo : ConfigurationItemCollection
    {
        const string SECTION_NAME = "CometConfig";

        public CraveCometConfigInfo()
        {
            Add(new ConfigurationItem(SECTION_NAME, CraveCometConfigConstants.LOGGING_LEVEL, "LoggingLevel", (int)LoggingLevel.Info, typeof(int), true));
            Add(new ConfigurationItem(SECTION_NAME, CraveCometConfigConstants.LAST_COMET_MESSAGE_FROM_WEBSERVICE, "Last pong received from webservice", DateTime.MinValue, typeof(DateTime), false));
            Add(new ConfigurationItem(SECTION_NAME, CraveCometConfigConstants.USE_PONG_QUEUE, "Use seperate consumer for handling pong messages", false, typeof(bool), true));
        }
    }
}
