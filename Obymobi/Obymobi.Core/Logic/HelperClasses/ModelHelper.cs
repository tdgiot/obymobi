﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using Dionysos;
using Obymobi.Logic.Model;

namespace Obymobi.Logic.HelperClasses
{
    public class ModelHelper
    {
        #region Fields

        static ModelHelper instance = null;

        private Assembly modelAssembly = null;
        private Dictionary<string, Type> models = new Dictionary<string, Type>();

        #endregion

        #region Constructors

        /// <summary>
        /// Constructs an instance of the ModelHelper class if the instance has not been initialized yet
        /// </summary>
        static ModelHelper()
        {
            if (instance == null)
            {
                // first create the instance
                instance = new ModelHelper();
                // Then init the instance (the init needs the instance to fill it)
                instance.Init();
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Initializes the static DataFactory instance
        /// </summary>
        private void Init()
        {
            this.GetModels();
        }

        private void GetModels()
        {
            // Get the model types
            this.models = new Dictionary<string, Type>();

            // Get the model assembly
            Type terminalType = typeof(Obymobi.Logic.Model.Terminal);
            this.modelAssembly = Assembly.GetAssembly(terminalType);

            // Get the model types
            foreach (Type type in this.modelAssembly.GetTypes())
            {
                if (type.Namespace == terminalType.Namespace)
                {
                    object instance = InstanceFactory.CreateInstance(modelAssembly, type);
                    if (instance != null && instance is ModelBase)
                        this.models.Add(type.Name, type);
                }
            }
        }

        #endregion

        #region Properties

        public static Dictionary<string, Type> Models
        {
            get
            {
                return instance.models;
            }
            set
            {
                instance.models = value;
            }
        }

        #endregion
    }
}
