﻿using Dionysos;
using Dionysos.Net.Mail;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Mail;
using Dionysos.Globalization;
using Dionysos.SMS;
using Dionysos.Security.Cryptography;

namespace Obymobi.Logic.HelperClasses
{
    public class SupportNotificationSendHelper : LoggingClassBase
    {        
        private static readonly TimeZoneInfo UkOfficeTimeZone = TimeZoneInformation.GetGMTStandardTime();
        private static readonly TimeZoneInfo NlOfficeTimeZone = TimeZoneInformation.GetWEuropeStandardTime();
        private readonly string emailSenderAddress, emailSenderName, smsSender;
        private readonly Object floodLock;
        private readonly Dictionary<string, List<DateTime>> floodCache;
        private readonly Dictionary<string, DateTime> floodAlerted;

        public SupportNotificationSendHelper(string emailSenderAddress, string emailSenderName, string smsSender)
        {
            this.floodLock = new Object();
            this.floodCache = new Dictionary<string, List<DateTime>>();
            this.floodAlerted = new Dictionary<string, DateTime>();

            this.emailSenderAddress = emailSenderAddress;
            this.emailSenderName = emailSenderName;
            this.smsSender = smsSender;

            if (StringUtil.IsNumeric(this.smsSender))
            {
                if (this.smsSender.Length > 18)
                    this.smsSender = this.smsSender.Substring(0, 18);
            }
            else if (this.smsSender.Length > 11)
                this.smsSender = this.smsSender.Substring(0, 11);
        }

        public bool IsFlooding(ref string message)
        {
            const int floodLimitPerTenMinutes = 5;
            bool toReturn = false;
            // This method will return true if we're flooding (same message more than 5 times in 10 minutes). 
            // To notify the user about that when the flooding is detected it will allow one last message with a prefix: 'Flooding detected'.
            string messageHash = Hasher.GetHash(message, HashType.MD5);
            lock(this.floodLock)
            {
                // Check if we already have a flood cache for this message
                if (floodCache.ContainsKey(messageHash))
                {
                    // Check how many were send the last 10 minutes
                    DateTime tenMinutesAgo = DateTime.Now.AddMinutes(-10);
                    List<DateTime> sentInLast10Minutes = this.floodCache[messageHash].Where(x => x > tenMinutesAgo).ToList();                    
                    
                    // If more than 5, it's flooding
                    if (sentInLast10Minutes.Count > floodLimitPerTenMinutes)
                    {
                        // Flood - alert once every 10 minutes
                        if (!this.floodAlerted.ContainsKey(messageHash) || this.floodAlerted[messageHash] < tenMinutesAgo)
                        {
                            message = "Flooding message: " + message;

                            // Record that we have alerted
                            if (!this.floodAlerted.ContainsKey(messageHash))
                                this.floodAlerted.Add(messageHash, DateTime.Now);
                            else
                                this.floodAlerted[messageHash] = DateTime.Now;
                        }
                        else
                        {
                            // We're flooding and we've already alerted, so we don't want to send anything.
                            toReturn = true;
                        }
                    }

                    // Set this selection as the new entry, which means we directly got rid of any entries
                    // older than 10 minutes and preventing memory usage by limiting to 10, enough to trigger
                    // the flood limit
                    if (sentInLast10Minutes.Count > (floodLimitPerTenMinutes + 1))
                        this.floodCache[messageHash] = sentInLast10Minutes.GetRange(0, floodLimitPerTenMinutes);
                    else
                        this.floodCache[messageHash] = sentInLast10Minutes;
                }
                else
                {
                    // No cache, yet, not flood, create the entry
                    floodCache.Add(messageHash, new List<DateTime>());
                }

                // Add this message to the floodcache
                floodCache[messageHash].Add(DateTime.Now);
            }

            return toReturn;
        }

        public bool SendSupportNotification(List<string> textRecipients, List<string> emailRecipients, string message, out string errorReport)
        {
            return SendSupportNotification(textRecipients, emailRecipients, null, message, out errorReport);
        }

        public bool SendSupportNotification(List<string> textRecipients, List<string> emailRecipients, string title, string message, out string errorReport, bool mailBodyHtml = false)
        {
            int succesfullSent = 0;
            errorReport = string.Empty;

            if (textRecipients == null)
                textRecipients = new List<string>(0);

            if (emailRecipients == null)
                emailRecipients = new List<string>(0);

            // Flood protection
            if (this.IsFlooding(ref message))
            {
                this.LogWarning("Flooding detected of: {0}", message);
                return true;
            }

            if (emailRecipients.Count <= 0 && textRecipients.Count <= 0)
            {
                errorReport += "SendSupportNotification - Failed. No phonenumber and email is set in the supportpool for this company or the system support pool.";
            }
            else
            {
                // Send E-mails            
                var mailSend = SendEmail(emailRecipients, message, title, mailBodyHtml, ref errorReport);
                if (mailSend)
                    succesfullSent++;

                // Send Texts            
                var textMessageSend = SendTextMessage(textRecipients, message, ref errorReport);
                if (textMessageSend)
                    succesfullSent++;
            }

            if (!errorReport.IsNullOrWhiteSpace())
            {
                this.LogError(errorReport);
            }

            return (succesfullSent > 0);
        }

        private bool SendEmail(List<string> emailRecipients, string message, string title, bool mailBodyHtml, ref string errorReport)
        {
            bool isSend = false;

            if (title.IsNullOrWhiteSpace())
                title = message;

            string lineBreaks = mailBodyHtml ? "<br/><br/>" : "\r\n";

            string dateTimeString = DateTime.Now.ToString("[dd-MM HH:mm:ss] ");
            DateTime utcNow = DateTime.UtcNow;
            DateTime ukTime = utcNow.UtcToLocalTime(UkOfficeTimeZone);
            DateTime nlTime = utcNow.UtcToLocalTime(NlOfficeTimeZone);
            string times = "UTC: {1:HH:mm:ss}{0}UK: {2:HH:mm:ss}{0}NL: {3:HH:mm:ss}".FormatSafe(lineBreaks, utcNow, ukTime, nlTime);
            const string disclaimer = "This email and any files transmitted with it are confidential and intended solely for the use of the individual or entity to whom they are addressed. If you have received this email in error please notify the system manager. This message contains confidential information and is intended only for the individual named. If you are not the named addressee you should not disseminate, distribute or copy this e-mail. Please notify the sender immediately by e-mail if you have received this e-mail by mistake and delete this e-mail from your system. If you are not the intended recipient you are notified that disclosing, copying, distributing or taking any action in reliance on the contents of this information is strictly prohibited.";
            string sender = "This message is sent from " + Environment.MachineName;

            MailAddress senderEmail = new MailAddress(this.emailSenderAddress, this.emailSenderName);
            foreach (var address in emailRecipients)
            {
                // Prepare variables to merge with
                try
                {
                    MailAddress recipientEmail = new MailAddress(address);

                    const int timeout = 30000;

                    var mailUtil = new MailUtilV2(senderEmail, recipientEmail, dateTimeString + title);
                    mailUtil.SmtpClient.Timeout = timeout;

                    message += lineBreaks + lineBreaks + times;
                    message += lineBreaks + lineBreaks + sender;
                    message += lineBreaks + lineBreaks + disclaimer;

                    if (mailBodyHtml)
                        mailUtil.BodyHtml = message;
                    else
                        mailUtil.BodyPlainText = message;

                    mailUtil.SendMail().Wait(timeout);

                    isSend = true;
                }
                catch (Exception ex)
                {
                    string errorText = string.Format("Email to '{0}' failed: {1} - {2}\r\n", address, message, ex.Message);
                    Debug.WriteLine("ERROR: E-mail not sent: " + errorText);
                    this.LogError("ERROR: E-mail not sent: " + errorText);
                    this.LogError("{0}", ex.GetAllMessages(true));
                    errorReport += errorText;
                }
            }

            return isSend;
        }

        private bool SendTextMessage(List<string> textRecipients, string message, ref string errorReport)
        {
            bool isSend = false;
            if (textRecipients.Count > 0)
            {
                var smsRequest = new MollieSMSRequest();

                smsRequest.Originator = this.smsSender;
                smsRequest.Recipients = String.Join(",", textRecipients.ToArray());

                // Restrict to 160 chars
                if (message.Length > 160)
                    smsRequest.Message = message.Substring(0, 156) + "...";
                else
                    smsRequest.Message = message;

                if (TestUtil.IsPcDeveloper)
                {
                    Debug.WriteLine("INFORMATION: Text to '{0}' with Body '{1}' not send, because it's a Developer PC.".FormatSafe(smsRequest.Recipients, message));
                    isSend = true;
                }
                else
                {
                    try
                    {
                        smsRequest.Send();
                        if (smsRequest.ResultEnum == MollieSMSRequest.MollieResult.Success)
                        {
                            isSend = true;
                        }
                        else
                        {
                            this.LogWarning("Sending SMS failed, result: '{0}', '{1}', '{2}'", smsRequest.ResultCode, smsRequest.ResultEnum, smsRequest.ResultMessage);
                        }
                    }
                    catch (Exception ex)
                    {
                        string errorText = string.Format("Sms to '{0}' failed: {1} - {2} - {3}\r\n", smsRequest.Recipients, smsRequest.ResultEnum, smsRequest.ResultMessage, ex.Message);
                        Debug.WriteLine("SendSupportNotification - ERROR: Sms not sent: " + errorText);
                        this.LogError("SendSupportNotification - ERROR: Sms not sent: " + errorText);
                        errorReport += errorText;
                    }
                }
            }

            return isSend;
        }

        public override string LogPrefix()
        {
            return "SupportNotificationSendHelper";
        }
    }
}
