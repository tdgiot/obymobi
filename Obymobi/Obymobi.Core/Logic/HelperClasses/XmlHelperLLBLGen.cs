﻿using SD.LLBLGen.Pro.ORMSupportClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace Obymobi.Logic.HelperClasses
{
    public class XmlHelperLLBLGen
    {
        /// <summary>
        /// Parses the entity from XML.
        /// </summary>
        /// <param name="xmlNode">The XML node.</param>
        /// <returns></returns>
        public static IEntity ParseEntityFromXml(XmlNode xmlNode)
        {
            IEntity entity = Dionysos.DataFactory.EntityFactory.GetEntity(xmlNode.Name) as IEntity;
            if (xmlNode.ChildNodes.Count > 0)
            {
                for (int j = 0; j < xmlNode.ChildNodes.Count; j++)
                {
                    XmlNode childNode = xmlNode.ChildNodes[j];
                    if (childNode.NodeType == XmlNodeType.Element)
                    {
                        IEntityField entityField = entity.Fields[childNode.Name];
                        if (entityField != null)
                        {
                            entity = XmlHelperLLBLGen.SetEntityFieldValue(entity, entityField, childNode.InnerXml);
                        }
                        else
                        {
                            if (childNode.ChildNodes.Count > 0)
                            {
                                IEntity subEntity = ParseEntityFromXml(childNode);

                                IEntityCollection entityCollection = Dionysos.Reflection.Member.InvokeProperty(entity, childNode.Name) as IEntityCollection;
                                if (entityCollection != null)
                                    entityCollection.Add(subEntity);
                            }
                        }
                    }
                }
            }

            return entity;
        }

        /// <summary>
        /// Sets the entity field value.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <param name="entityField">The entity field.</param>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        public static IEntity SetEntityFieldValue(IEntity entity, IEntityField entityField, string value)
        {
            string fieldName = entityField.Name;

            System.Type fieldType = entityField.ActualDotNetType;
            string fieldTypeString = fieldType.ToString();

            switch (fieldTypeString)
            {
                case "System.Boolean":
                    if (value == string.Empty)
                        entity.SetNewFieldValue(fieldName, false);
                    else
                        entity.SetNewFieldValue(fieldName, Boolean.Parse(value));
                    break;
                case "System.SByte":
                    if (value == string.Empty)
                        entity.SetNewFieldValue(fieldName, 0);
                    else
                        entity.SetNewFieldValue(fieldName, SByte.Parse(value));
                    break;
                case "System.Byte":
                    if (value == string.Empty)
                        entity.SetNewFieldValue(fieldName, 0);
                    else
                        entity.SetNewFieldValue(fieldName, Byte.Parse(value));
                    break;
                case "System.Int16":
                    if (value == string.Empty)
                        entity.SetNewFieldValue(fieldName, 0);
                    else
                        entity.SetNewFieldValue(fieldName, Int16.Parse(value));
                    break;
                case "System.UInt16":
                    if (value == string.Empty)
                        entity.SetNewFieldValue(fieldName, 0);
                    else
                        entity.SetNewFieldValue(fieldName, UInt16.Parse(value));
                    break;
                case "System.Int32":
                    if (value == string.Empty)
                        entity.SetNewFieldValue(fieldName, 0);
                    else
                        entity.SetNewFieldValue(fieldName, Int32.Parse(value));
                    break;
                case "System.UInt32":
                    if (value == string.Empty)
                        entity.SetNewFieldValue(fieldName, 0);
                    else
                        entity.SetNewFieldValue(fieldName, UInt32.Parse(value));
                    break;
                case "System.Int64":
                    if (value == string.Empty)
                        entity.SetNewFieldValue(fieldName, 0);
                    else
                        entity.SetNewFieldValue(fieldName, Int64.Parse(value));
                    break;
                case "System.UInt64":
                    if (value == string.Empty)
                        entity.SetNewFieldValue(fieldName, 0);
                    else
                        entity.SetNewFieldValue(fieldName, UInt64.Parse(value));
                    break;
                case "System.Char":
                    if (value == string.Empty)
                        entity.SetNewFieldValue(fieldName, '\0');
                    else
                        entity.SetNewFieldValue(fieldName, Char.Parse(value));
                    break;
                case "System.Single":
                    if (value == string.Empty)
                        entity.SetNewFieldValue(fieldName, 0.0f);
                    else
                        entity.SetNewFieldValue(fieldName, Single.Parse(value));
                    break;
                case "System.Double":
                    if (value == string.Empty)
                        entity.SetNewFieldValue(fieldName, 0.0d);
                    else
                        entity.SetNewFieldValue(fieldName, Double.Parse(value));
                    break;
                case "System.Decimal":
                    if (value == string.Empty)
                        entity.SetNewFieldValue(fieldName, 0.0m);
                    else
                        entity.SetNewFieldValue(fieldName, Decimal.Parse(value));
                    break;
                case "System.String":
                    if (value == string.Empty)
                        entity.SetNewFieldValue(fieldName, string.Empty);
                    else
                        entity.SetNewFieldValue(fieldName, value.Trim());
                    break;
                case "System.DateTime":
                    if (value == string.Empty)
                    {
                        // No value
                    }
                    else
                    {
                        DateTime result = DateTime.Parse(value);
                        if (result.Year < 1950)
                        {
                            // We do this because SQL only accepts after year 1750 or something, gives error otherwise
                            result = new DateTime(1900, 1, 1);
                        }
                        entity.SetNewFieldValue(fieldName, result);
                    }
                    break;
            }

            return entity;
        }
    }
}
