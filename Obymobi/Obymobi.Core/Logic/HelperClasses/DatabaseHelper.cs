﻿using Dionysos;
using Obymobi.Enums;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace Obymobi.Logic.HelperClasses
{
    public class DatabaseHelper
    {
        public static bool IsDatabaseReady(string connectionString)
        {
            return DatabaseHelper.GetDatabaseStatus(connectionString, false).Equals("ONLINE", StringComparison.OrdinalIgnoreCase);
        }

        public static bool IsDatabaseReady(string connectionString, out string status)
        {
            status = DatabaseHelper.GetDatabaseStatus(connectionString, true);
            return status.Equals("ONLINE", StringComparison.OrdinalIgnoreCase);
        }

        public static string GetDatabaseStatus(string connectionString, bool detailedStatus)
        {
            SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder(connectionString);

            return DatabaseHelper.GetDatabaseStatus(connectionString, builder.InitialCatalog, detailedStatus);
        }

        public static string GetDatabaseStatus(string connectionString, string databaseName, bool detailedStatus)
        {
            string detailedStatusText;
            string retval = GetDatabaseStatus(connectionString, databaseName, out detailedStatusText);

            if (detailedStatus)
                return detailedStatusText;
            else
                return retval;
        }

        public static CloudEnvironment GetEnvironmentOfDatabase(string connectionString, string databaseName = "")
        {
            CloudEnvironment toReturn = CloudEnvironment.Manual;
            SqlConnectionStringBuilder csb = new SqlConnectionStringBuilder(connectionString);
            csb.ConnectTimeout = 5;
            System.Data.SqlClient.SqlConnection sqlConnection = new System.Data.SqlClient.SqlConnection(csb.ToString());
            try
            {
                string queryText;
                if(databaseName.IsNullOrWhiteSpace())
                    databaseName = csb.InitialCatalog;

                sqlConnection.Open();

                // Nothing, good state!
                // Verify it's for real, by executing a query
                // Create the SQL Command and assign it it a string
                queryText = "USE " + databaseName;

                // Execute the SQL Command to use the database
                using(SqlCommand command = new System.Data.SqlClient.SqlCommand(queryText, sqlConnection))
                {
                    // Use ExecuteNonQuery() to return nothing
                    command.ExecuteNonQuery();
                }

                // Create the SQL Command and assign it it a string
                // GK Not a big fan of this, and thought of using LLBLGen classes, but those are not available in Obymobi.Core. 
                queryText = string.Format("SELECT top(1) [Value] FROM [Configuration] WHERE [Name] = '{0}' and [Section] = '{1}'", ObymobiConfigConstants.CraveCloudEnvironment, "Obymobi");

                // Execute the SQL Command to get the configuration value
                using (var command = new System.Data.SqlClient.SqlCommand(queryText, sqlConnection))
                {                    
                    object result = command.ExecuteScalar();
                    if (result == null || result == DBNull.Value)                    
                        throw new Exception(string.Format("CloudEnvironment is not yet configured in your database '{0}', \"{1}\" resulted in null.", databaseName, queryText));                    
                    
                    string cloudEnvironment = result.ToString();
                    if (!EnumUtil.TryParse(cloudEnvironment, out toReturn))
                        throw new Exception(string.Format("Could not parse '{0}' to a cloud environment - Ran query on database {1}: {2}", cloudEnvironment, databaseName, queryText));
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                sqlConnection.Close();
                if(sqlConnection != null)
                    sqlConnection.Dispose();
            }

            return toReturn;
        }

        public static string GetDatabaseStatus(string connectionString, string databaseName, out string detailedStatus, bool IsObymobiDatabase = true)
        {
            detailedStatus = "OFFLINE";
            string toReturn = "OFFLINE";
            SqlConnectionStringBuilder csb = new SqlConnectionStringBuilder(connectionString);
            csb.ConnectTimeout = 5;
            System.Data.SqlClient.SqlConnection sqlConnection = new System.Data.SqlClient.SqlConnection(csb.ToString());

            try
            {
                sqlConnection.Open();

                // 4. Create the SQL Command and assign it it a string
                string strSQLCommand = "SELECT DATABASEPROPERTYEX('" + databaseName + "', 'Status') DatabaseStatus_DATABASEPROPERTYEX";

                // 5. Execute the SQL Command
                System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand(strSQLCommand, sqlConnection);

                // 6. Use ExecuteScalar() to return the first result
                string status = command.ExecuteScalar().ToString();

                // ONLINE = Database is available for query. 
                // OFFLINE = Database was explicitly taken offline.
                // RESTORING = Database is being restored.
                // RECOVERING = Database is recovering and not yet ready for queries.
                // SUSPECT = Database did not recover.
                // EMERGENCY = Database is in an emergency, read-only state. Access is 
                toReturn = status;
                detailedStatus = status;
                if (!IsObymobiDatabase)
                { 
                    // Nothing to further check.
                }
                else if (status == "ONLINE")
                {
                    try
                    {
                        // Nothing, good state!
                        // Verify it's for real, by executing a query
                        // Create the SQL Command and assign it it a string
                        strSQLCommand = "USE " + databaseName;

                        // Execute the SQL Command
                        command = new System.Data.SqlClient.SqlCommand(strSQLCommand, sqlConnection);

                        // Use ExecuteNonQuery() to return nothing
                        command.ExecuteNonQuery();

                        // Create the SQL Command and assign it it a string
                        strSQLCommand = "SELECT COUNT(LanguageId) FROM Language";

                        // Execute the SQL Command
                        command = new System.Data.SqlClient.SqlCommand(strSQLCommand, sqlConnection);

                        // 6. Use ExecuteScalar() to return the first result
                        command.ExecuteScalar().ToString();
                    }
                    catch
                    {
                        detailedStatus += " (Not Queryable)";
                        toReturn = "OFFLINE";
                    }
                }
                else if (status == "RESTORING")
                {
                    // If mirroring get the correct mirroring state
                    strSQLCommand = "USE msdb EXEC sp_dbmmonitorresults '" + databaseName + "', 1, 1";

                    // 5. Execute the SQL Command
                    command = new System.Data.SqlClient.SqlCommand(strSQLCommand, sqlConnection);

                    SqlDataReader reader = command.ExecuteReader();
                    Byte state = 7;

                    string mirroringState = "UNKNOWN-MIRROR";
                    if (reader.Read())
                    {
                        state = reader.GetByte(2);
                        if (state < 7)
                        {
                            // 0 = Suspended
                            // 1 = Disconnected
                            // 2 = Synchronizing
                            // 3 = Pending Failover
                            // 4 = Synchronized
                            switch (state)
                            {
                                case 0:
                                    mirroringState = "SUSPENDED";
                                    break;
                                case 1:
                                    mirroringState = "DISCONNECTED";
                                    break;
                                case 3:
                                    mirroringState = "PENDING FAILOVER";
                                    break;
                                case 2:
                                case 4:
                                    mirroringState = "MIRRORING";
                                    break;
                            }
                        }
                    }
                    else
                    {
                        // All other states are just called 'OFFLINE'
                        mirroringState = "OFFLINE";
                    }

                    detailedStatus = string.Format("{0} ({1})", toReturn, mirroringState);
                    toReturn = mirroringState;
                }
            }
            catch (Exception ex)
            {
                // Nothing to set, already OFFLINE
                detailedStatus = "OFFLINE: " + ex.Message;
                toReturn = "OFFLINE";
            }
            finally
            {
                sqlConnection.Close();
                sqlConnection.Dispose();
            }

            return toReturn;
        }

    }
}
