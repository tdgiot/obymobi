﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Serialization;
using Obymobi.Attributes;

namespace Obymobi.Logic.Model
{
	/// <summary>
	/// Model class which represents a product
	/// </summary>
	[Serializable, XmlRootAttribute(ElementName = "Genericproduct"), IncludeInCodeGeneratorForFlex, IncludeInCodeGeneratorForAndroid, IncludeInCodeGeneratorForMobile, IncludeInCodeGeneratorForXamarin]
	public class ProductmenuItem : ModelBase
	{
		#region Fields

		private float ratingAverage = -1.0f;

		#endregion

		#region Constructors

		/// <summary>
		/// Constructs an instance of the Obymobi.Logic.Model.ProductmenuItem type
		/// </summary>
		public ProductmenuItem()
		{
			this.ParentProductmenuItemId = -1;
		}

        /// <summary>
        /// Constructs an instance of the Obymobi.Logic.Model.ProductmenuItem type
        /// </summary>
        public ProductmenuItem(int id, int parentProductmenuItemId)
        {
            this.Id = id;
            this.ParentProductmenuItemId = parentProductmenuItemId;
        }

		/// <summary>
		/// Constructs an instance of the Obymobi.Logic.Model.ProductmenuItem type using the specified parameters
		/// </summary>
		public ProductmenuItem(int Id, string Name, int ParentProductmenuItemId, int ItemType)
		{
			this.Id = Id;
			this.Name = Name;
			this.ParentProductmenuItemId = ParentProductmenuItemId;
			this.ItemType = ItemType;
		}

		#endregion

		#region Properties

		/// <summary>
		/// Gets or sets the id of the productmenu item
		/// </summary>
		[XmlElement, PrimaryKeyFieldOfModel]
		[IncludeInCodeGeneratorForFlex]
		[IncludeInCodeGeneratorForAndroid]
		[IncludeInCodeGeneratorForMobile]
        [IncludeInCodeGeneratorForXamarin]
		public int Id
		{ get; set; }

		/// <summary>
		/// Gets or sets the name of the productmenu item
		/// </summary>
		[XmlElement]
		[IncludeInCodeGeneratorForFlex]
		[IncludeInCodeGeneratorForAndroid]
		[IncludeInCodeGeneratorForMobile]
        [IncludeInCodeGeneratorForXamarin]
		public string Name
		{ get; set; }

		/// <summary>
		/// Gets or sets the price including vat in cents
		/// </summary>
		[XmlElement]
		[IncludeInCodeGeneratorForFlex]
		[IncludeInCodeGeneratorForAndroid]
		[IncludeInCodeGeneratorForMobile]
        [IncludeInCodeGeneratorForXamarin]
		public decimal PriceIn
		{ get; set; }

		/// <summary>
		/// Gets or sets the id of the parent productmenu item
		/// </summary>
		[XmlElement, ForeignKeyFieldOfModel]
		[IncludeInCodeGeneratorForFlex]
		[IncludeInCodeGeneratorForAndroid]
		[IncludeInCodeGeneratorForMobile]
        [IncludeInCodeGeneratorForXamarin]
		public int ParentProductmenuItemId
		{ get; set; }

		/// <summary>
		/// Gets or sets the item type of the productmenu item
		/// </summary>
		[XmlElement, ForeignKeyFieldOfModel]
		[IncludeInCodeGeneratorForFlex]
		[IncludeInCodeGeneratorForAndroid]
		[IncludeInCodeGeneratorForMobile]
        [IncludeInCodeGeneratorForXamarin]
		public int ItemType
		{ get; set; }

		/// <summary>
		/// Gets or sets the item's sortorder
		/// </summary>
		[XmlElement]
		[IncludeInCodeGeneratorForFlex]
		[IncludeInCodeGeneratorForAndroid]
		[IncludeInCodeGeneratorForMobile]
        [IncludeInCodeGeneratorForXamarin]
		public int SortOrder
		{ get; set; }

		/// <summary>
		/// Gets or sets the if the item is a favorite
		/// </summary>
		[XmlElement]
		[IncludeInCodeGeneratorForFlex]
		[IncludeInCodeGeneratorForMobile]
		public int IsFavorite
		{ get; set; }

		/// <summary>
		/// Extra field to be used on a terminal/client only
		/// </summary>
		[XmlElement]
		[IncludeInCodeGeneratorForFlex]
		[IncludeInCodeGeneratorForAndroid]
		[IncludeInCodeGeneratorForMobile]
		public int OrderedQuantity
		{ get; set; }

		/// <summary>
		/// Gets or sets the text color
		/// </summary>
		[XmlElement]
		[IncludeInCodeGeneratorForFlex]
		public string TextColor
		{ get; set; }

		/// <summary>
		/// Gets or sets the background color
		/// </summary>
		[XmlElement]
		[IncludeInCodeGeneratorForFlex]
		public string BackgroundColor
		{ get; set; }

		/// <summary>
		/// Gets or sets the description
		/// </summary>
		[XmlElement]
		[IncludeInCodeGeneratorForFlex]
		[IncludeInCodeGeneratorForAndroid]
		[IncludeInCodeGeneratorForMobile]
        [IncludeInCodeGeneratorForXamarin]
		public string Description
		{ get; set; }

		/// <summary>
		/// Gets or sets the flag which indicates whether the product should be displayed on the homepage
		/// </summary>
		[XmlElement]
		[IncludeInCodeGeneratorForFlex]
		[IncludeInCodeGeneratorForMobile]
		public bool DisplayOnHomepage
		{ get; set; }

		/// <summary>
		/// Gets or sets the flag which indicates whether the product is rateable
		/// </summary>
		[XmlElement]
		[IncludeInCodeGeneratorForMobile]
		public bool Rateable
		{ get; set; }

		/// <summary>
		/// Gets or sets the type of the category
		/// </summary>
		[XmlElement]
		[IncludeInCodeGeneratorForAndroid]
		[IncludeInCodeGeneratorForMobile]
        [IncludeInCodeGeneratorForXamarin]
		public int Type
		{ get; set; }

        /// <summary>
        /// Gets or sets the subtype of the category
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        [IncludeInCodeGeneratorForMobile]
        public int SubType
        { get; set; }

		/// <summary>
		/// Gets or sets the flag which indicates whether the SERVICE ITEM can have free text comments
		/// </summary>
		[XmlElement]
		[IncludeInCodeGeneratorForAndroid]
		[IncludeInCodeGeneratorForMobile]
        [IncludeInCodeGeneratorForXamarin]
		public bool AllowFreeText
		{ get; set; }

		/// <summary>
		/// Gets or sets the flag which indicates whether the category can be used as announcement action
		/// </summary>
		[XmlElement]
		[IncludeInCodeGeneratorForMobile]
		public bool AnnouncementAction
		{ get; set; }

		/// <summary>
		/// Gets or sets the hide prices flag
		/// </summary>
		[XmlElement]
		[IncludeInCodeGeneratorForAndroid]
        [IncludeInCodeGeneratorForXamarin]
		public bool HidePrices
		{ get; set; }

        /// <summary>
        /// Gets or sets the color
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int Color
        { get; set; }

        /// <summary>
        /// Gets or sets the custom button text
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string ButtonText
        { get; set; }

        /// <summary>
        /// Gets or sets the customize button text
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string CustomizeButtonText
        { get; set; }

        /// <summary>
        /// Gets or sets the text color
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        [IncludeInCodeGeneratorForMobile]
        public string WebTypeTabletUrl
        { get; set; }

        /// <summary>
        /// Gets or sets the text color
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForMobile]
        public string WebTypeSmartphoneUrl
        { get; set; }

        /// <summary>
        /// Gets or sets the schedule id
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int ScheduleId
        { get; set; }

        /// <summary>
        /// Gets or sets the view layout type
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int ViewLayoutType
        { get; set; }

        /// <summary>
        /// Gets or sets the delivery location type
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int DeliveryLocationType
        { get; set; }

        /// <summary>
        /// Gets or sets the visibility type
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int VisibilityType
        { get; set; }

        /// <summary>
        /// Gets or sets the instructions
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string Instructions
        { get; set; }

        /// <summary>
        /// Gets or sets the generic product id
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int GenericproductId
        { get; set; }

        /// <summary>
        /// Gets or sets the brand product id
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int BrandProductId
        { get; set; }

        /// <summary>
        /// Gets or sets the alterations of the product
        /// </summary>
        [XmlArray("Alterations")]
		[XmlArrayItem("Alteration")]
		[IncludeInCodeGeneratorForFlex]
		[IncludeInCodeGeneratorForAndroid]
		[IncludeInCodeGeneratorForMobile]
        [IncludeInCodeGeneratorForXamarin]
		public Alteration[] Alterations
		{ get; set; }

		/// <summary>
		/// Gets or sets the alterations of the product
		/// </summary>
		[XmlArray("Media")]
		[XmlArrayItem("Media")]
		[IncludeInCodeGeneratorForFlex]
		[IncludeInCodeGeneratorForAndroid]
		[IncludeInCodeGeneratorForMobile]
        [IncludeInCodeGeneratorForXamarin]
		public Media[] Media
		{ get; set; }

		/// <summary>
		/// Gets or sets the suggested products of the product
		/// </summary>
		[XmlArray("ProductSuggestions")]
		[XmlArrayItem("ProductSuggestion")]
		[IncludeInCodeGeneratorForFlex]
		[IncludeInCodeGeneratorForAndroid]
		[IncludeInCodeGeneratorForMobile]
        [IncludeInCodeGeneratorForXamarin]
		public ProductSuggestion[] ProductSuggestions
		{ get; set; }

		/// <summary>
		/// Gets or sets the tags of the advertisement
		/// </summary>
		[XmlArray("AdvertisementTags")]
		[XmlArrayItem("AdvertisementTag")]
		[IncludeInCodeGeneratorForFlex]
		[IncludeInCodeGeneratorForMobile]
		public AdvertisementTag[] AdvertisementTags
		{ get; set; }

		/// <summary>
		/// Gets or sets the ratings
		/// </summary>
		[XmlArray("Ratings")]
		[XmlArrayItem("Rating")]
		[IncludeInCodeGeneratorForMobile]
		public Rating[] Ratings
		{ get; set; }

        /// <summary>
        /// Gets or sets the ratings
        /// </summary>
        [XmlArray("Attachments")]
        [XmlArrayItem("Attachment")]
        [IncludeInCodeGeneratorForAndroid]
        [IncludeInCodeGeneratorForMobile]
        public Attachment[] Attachments
        { get; set; }

        /// <summary>
        /// Gets or sets the custom text collection
        /// </summary>
        [XmlArray("CustomTexts")]
        [XmlArrayItem("CustomText")]
        [IncludeInCodeGeneratorForAndroid]
        [IncludeInCodeGeneratorForMobile]
        [IncludeInCodeGeneratorForXamarin]
        public CustomText[] CustomTexts
        { get; set; }

		#endregion

		#region Custom Properties

		/// <summary>
		/// Gets total ratings
		/// </summary>
		[XmlIgnore]
		public int TotalRatings
		{
			get
			{
				if (Ratings == null)
					Ratings = new Rating[0];

				return Ratings.Length;
			}
		}

		/// <summary>
		/// Gets the average rating
		/// </summary>
		[XmlIgnore]
		public float RatingAverage
		{
			get
			{
				if (ratingAverage.Equals(-1.0f))
					CalculateRatingAverage();

				return ratingAverage;
			}
		}

		#endregion

		#region Custom Methods

		/// <summary>
		/// Add new rating to this product
		/// </summary>
		/// <param name="rating"></param>
		public void AddRating(Rating rating)
		{
			var ratingList = new List<Rating>();
			if (Ratings != null)
				ratingList.AddRange(this.Ratings);

			ratingList.Add(rating);
			this.Ratings = ratingList.ToArray();

			// Calculate average, if needed
			if (this.Rateable)
				CalculateRatingAverage();
		}

		/// <summary>
		/// Calculates the averate rating
		/// </summary>
		private void CalculateRatingAverage()
		{
			var total = this.Ratings.Aggregate<Rating, float>(0, (current, rating) => current + rating.Value);
			if (this.Ratings.Length > 0)
				this.ratingAverage = total / this.Ratings.Length;
		}

		#endregion

		#region Methods

		/// <summary>
		/// Clones this instance.
		/// </summary>
		/// <returns></returns>
		public ProductmenuItem Clone()
		{
			return this.Clone<ProductmenuItem>();
		}

		#endregion
	}
}
