﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Obymobi.Attributes;

namespace Obymobi.Logic.Model
{
    /// <summary>
    /// Model class which represents a survey language
    /// </summary>
    [Serializable, XmlRootAttribute(ElementName = "SurveyLanguage")]
    public class SurveyLanguage : ModelBase
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Obymobi.Logic.Model.SurveyAnswerLanguage type
        /// </summary>
        public SurveyLanguage()
        {
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the id of the surveyAnswer language
        /// </summary>
        [XmlElement, PrimaryKeyFieldOfModel]
        [IncludeInCodeGeneratorForAndroid]
        public int SurveyLanguageId
        { get; set; }

        /// <summary>
        /// Gets or sets the id of the surveyAnswer
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int SurveyId
        { get; set; }

        /// <summary>
        /// Gets or sets the language code
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string LanguageCode
        { get; set; }

        /// <summary>
        /// Gets or sets the saving title
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string SavingTitle
        { get; set; }

        /// <summary>
        /// Gets or sets the saving message
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string SavingMessage
        { get; set; }

        /// <summary>
        /// Gets or sets the processed title
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string ProcessedTitle
        { get; set; }

        /// <summary>
        /// Gets or sets the processed message
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string ProcessedMessage
        { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns></returns>
        public SurveyLanguage Clone()
        {
            return this.Clone<SurveyLanguage>();
        }

        #endregion
    }
}
