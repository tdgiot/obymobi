﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Obymobi.Attributes;

namespace Obymobi.Logic.Model
{
	/// <summary>
	/// Model class which represents a favorite company
	/// </summary>
	[Serializable, XmlRootAttribute(ElementName = "MobileClientStartupInfo"), IncludeInCodeGeneratorForMobile, IncludeInCodeGeneratorForFlex]
	public class MobileClientStartupInfo : ModelBase
	{
		#region Constructors

		/// <summary>
		/// Constructs an instance of the Obymobi.Logic.Model.MobileClientVersionInfo type
		/// </summary>
		public MobileClientStartupInfo()
		{
		}

		#endregion

		#region Properties

		/// <summary>
		/// Gets or sets the id of the favorite company
		/// </summary>
		[XmlElement, PrimaryKeyFieldOfModel]
        [IncludeInCodeGeneratorForFlex]
		public string RequiresUpdate
		{ get; set; }

		/// <summary>
		/// Gets or sets the id of the favorite company
		/// </summary>
		[XmlElement]
        [IncludeInCodeGeneratorForFlex]
		public string UpdateUrl
		{ get; set; }

		/// <summary>
		/// Gets or sets the id of the favorite company
		/// </summary>
		[XmlElement]
        [IncludeInCodeGeneratorForFlex]
		public string UpdateTitle
		{ get; set; }

		/// <summary>
		/// Gets or sets the id of the favorite company
		/// </summary>
		[XmlElement]
        [IncludeInCodeGeneratorForFlex]
		public string UpdateText
		{ get; set; }

		/// <summary>
		/// Gets or sets the id of the favorite company
		/// </summary>
		[XmlElement]
        [IncludeInCodeGeneratorForFlex]
		public string MotdTitle
		{ get; set; }

		/// <summary>
		/// Gets or sets the id of the favorite company
		/// </summary>
		[XmlElement]
        [IncludeInCodeGeneratorForFlex]
		public string MotdText
		{ get; set; }


		#endregion

        #region Methods

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns></returns>
        public MobileClientStartupInfo Clone()
        {
            return this.Clone<MobileClientStartupInfo>();
        }

        #endregion
	}
}