﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Obymobi.Attributes;

namespace Obymobi.Logic.Model
{
	/// <summary>
	/// Model class for the Terminal Status
	/// </summary>
	[Serializable, XmlRootAttribute(ElementName = "WebserviceStatus"), IncludeInCodeGeneratorForFlex]
    public class WebserviceStatus : ModelBase
	{
		/// <summary>
		/// Primary Key is required in a Model, but this one is not used.
		/// </summary>
		[XmlElement, PrimaryKeyFieldOfModel]
        [IncludeInCodeGeneratorForFlex]
		public int WebserviceStatusId
		{ get; set; }

		/// <summary>
		/// Indicates if the database is working
		/// </summary>
		[XmlElement]
        [IncludeInCodeGeneratorForFlex]
		public bool Database
		{ get; set; }

		/// <summary>
		/// Returns the mode in which a Otoucho Client should continue to operate.
		/// </summary>
		[XmlElement]
        [IncludeInCodeGeneratorForFlex]
		public int ClientOperationMode
		{ get; set; }

        #region Methods

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns></returns>
        public WebserviceStatus Clone()
        {
            return this.Clone<WebserviceStatus>();
        }

        #endregion
	}
}
