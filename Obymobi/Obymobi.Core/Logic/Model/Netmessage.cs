﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;
using Dionysos;
using Newtonsoft.Json;
using Obymobi.Attributes;
using Obymobi.Enums;

namespace Obymobi.Logic.Model
{
    /// <summary>
    /// Model class which represents an alteration
    /// </summary>
    [Serializable, XmlRoot(ElementName = "Netmessage"), IncludeInCodeGeneratorForAndroid, System.Runtime.InteropServices.GuidAttribute("9B16DC1B-C086-4BB5-B540-9611DC0C6F9D")]
    public class Netmessage : ModelBase
    {
        [XmlIgnore, JsonIgnore]
        public static readonly List<NetmessageType> IgnoredVerifyMessageTypes = new List<NetmessageType>
        {
            NetmessageType.Ping,
            NetmessageType.Pong,
            NetmessageType.Test,
        };

        public Netmessage()
        {
            this.SaveToDatabase = true;
            this.FromAwsIot = false;
        }

        public Netmessage(NetmessageType type)
            : this() => SetMessageType(type);

        public NetmessageType GetMessageType() => this.MessageTypeInt.ToEnum<NetmessageType>();

        public void SetMessageType(NetmessageType type) => this.MessageTypeInt = (int)type;

        /// <summary>
        /// Validate if all required fields have been filled with data. Default this method will do nothing.
        /// If you need to force people to fill in some fields, you can override this method and put your own logic in it. To make this method fail a ObymobiNetmessageException may be thrown.
        /// </summary>
        public virtual void Validate()
        {
        }

        public bool IsVerifyNeeded() => !IgnoredVerifyMessageTypes.Contains(GetMessageType());

        public T ConvertTo<T>(bool validate = true) where T : Netmessage, new()
        {
            T model = new T
            {
                NetmessageId = this.NetmessageId,
                Guid = this.Guid,
                MessageVersion = this.MessageVersion,
                MessageTypeInt = this.MessageTypeInt,
                SenderIdentifier = this.SenderIdentifier,
                ReceiverIdentifier = this.ReceiverIdentifier,
                ReceiverClientId = this.ReceiverClientId,
                ReceiverCompanyId = this.ReceiverCompanyId,
                ReceiverDeliverypointId = this.ReceiverDeliverypointId,
                ReceiverTerminalId = this.ReceiverTerminalId,
                SenderClientId = this.SenderClientId,
                SenderCompanyId = this.SenderCompanyId,
                SenderDeliverypointId = this.SenderDeliverypointId,
                SenderTerminalId = this.SenderTerminalId,
                FieldValue1 = this.FieldValue1,
                FieldValue2 = this.FieldValue2,
                FieldValue3 = this.FieldValue3,
                FieldValue4 = this.FieldValue4,
                FieldValue5 = this.FieldValue5,
                FieldValue6 = this.FieldValue6,
                FieldValue7 = this.FieldValue7,
                FieldValue8 = this.FieldValue8,
                FieldValue9 = this.FieldValue9,
                //model.FieldValue10 = this.FieldValue10;
                FieldValue11 = this.FieldValue11,
                FieldValue12 = this.FieldValue12,
                FieldValue13 = this.FieldValue13,
                FieldValue14 = this.FieldValue14,
                FieldValue15 = this.FieldValue15,
                FieldValue16 = this.FieldValue16,
                FieldValue17 = this.FieldValue17,
                FieldValue18 = this.FieldValue18,
                Status = this.Status,
                Submitted = this.Submitted
            };

            if (validate)
            {
                model.Validate();
            }

            return model;
        }

        /// <summary>
        /// This method is called to verify if the Netmessage found in the Netmessage table is a duplicate (i.e. 2 requests for restart) of this instance as is called upon saving the current instance of the Netmessage.
        /// With this method you can decide if the current message should be ignored since it's a duplicate and/or the existing message should be overwritten by this new message.        
        /// </summary>
        /// <param name="existingNetmessage">The message found in the Netmessage table</param>
        /// <param name="overwriteExistingNetmessageWithThisMessage">Decide wheter the existing message should be overwritten by the current instance.</param>
        /// <returns>False means it's not a duplicate and should be added to the Netmessage table, True means it's a duplicate and this instance should be disregarded, 
        /// but if overwriteExistingMessageWithThisMessage is true the existing message must be updated with the values of this instance and be saved.</returns>
        public virtual bool IsDuplicateMessage(Netmessage existingNetmessage, out bool overwriteExistingNetmessageWithThisMessage)
        {
            overwriteExistingNetmessageWithThisMessage = false;
            return false;
        }

        public new virtual string ToString()
        {
            return "FV1 '{0}', FV2 '{1}', FV3 '{2}', FV4 '{3}', FV5 '{4}', FV6 '{5}', FV7 '{6}', FV8 '{7}', FV9 '{8}', FV10 '{9}'".FormatSafe(
                                this.FieldValue1, this.FieldValue2, this.FieldValue3, this.FieldValue4, this.FieldValue5, this.FieldValue6,
                                this.FieldValue7, this.FieldValue8, this.FieldValue9, this.FieldValue10);
        }

        public new string ToJson()
        {
            JsonSerializerSettings settings = new JsonSerializerSettings
            {
                NullValueHandling = NullValueHandling.Ignore
            };
            return JsonConvert.SerializeObject(this, new Formatting(), settings);
        }

        public void AddTraceMessage(string message, params object[] args)
        {
            if (GetMessageType() != NetmessageType.Pong && (this.SaveToDatabase || IsVerifyNeeded()))
            {
                this.MessageLog += string.Format("[{0}] {1}{2}", DateTimeUtil.GetTimeString(), message.FormatSafe(args), Environment.NewLine);
            }
        }

        [XmlIgnore, JsonIgnore]
        public bool SaveToDatabase { get; set; }

        /// <summary>
        /// True or false whether the message has been received and processed by the receiver
        /// </summary>
        [XmlIgnore, JsonIgnore]
        public bool Submitted { get; set; }

        public bool FromAwsIot { get; set; }

        #region Properties

        /// <summary>
        /// Get or set Netmessage id
        /// </summary>
        [XmlElement, JsonProperty, PrimaryKeyFieldOfModel]
        [IncludeInCodeGeneratorForAndroid]
        public int NetmessageId { get; set; }

        /// <summary>
        /// GUID set by the client
        /// </summary>
        [XmlElement, JsonProperty, IncludeInCodeGeneratorForAndroid]
        public string Guid { get; set; }

        /// <summary>
        /// Separate version for each message used for backwards compatability
        /// </summary>
        [XmlElement, JsonProperty]
        public int MessageVersion { get; set; }

        /// <summary>
        /// Gets or sets the identifier of the receiver
        /// </summary>
        public string ReceiverIdentifier { get; set; }

        /// <summary>
        /// Gets or sets the identifier of the sender
        /// </summary>
        public string SenderIdentifier { get; set; }

        /// <summary>
        /// Gets or sets the Netmessage command type
        /// </summary>
        [XmlIgnore, JsonIgnore]
        public NetmessageType MessageType
        {
            get => this.MessageTypeInt.ToEnum<NetmessageType>();
            set => this.MessageTypeInt = (int)value;
        }

        /// <summary>
        /// Get/set property for serialization ONLY. Use GetMessageType and SetMessageType methods.
        /// </summary>
        [XmlElement]
        [JsonProperty("MessageType")]
        [IncludeInCodeGeneratorForAndroid]
        public int MessageTypeInt { get; set; }

        [XmlIgnore, JsonIgnore]
        public string Status { get; set; }

        /// <summary>
        /// Gets or sets the sender company id
        /// </summary>
        [XmlElement, JsonProperty]
        [IncludeInCodeGeneratorForAndroid]
        public int SenderCompanyId { get; set; }

        /// <summary>
        /// Gets or sets the receiver company id
        /// </summary>
        [XmlElement, JsonProperty]
        [IncludeInCodeGeneratorForAndroid]
        public int ReceiverCompanyId { get; set; }

        /// <summary>
        /// Gets or sets the sender client id
        /// </summary>
        [XmlElement, JsonProperty]
        [IncludeInCodeGeneratorForAndroid]
        public int SenderClientId { get; set; }

        /// <summary>
        /// Gets or sets the receiver client id
        /// </summary>
        [XmlElement, JsonProperty]
        [IncludeInCodeGeneratorForAndroid]
        public int ReceiverClientId { get; set; }

        /// <summary>
        /// Gets or sets the sender terminal id
        /// </summary>
        [XmlElement, JsonProperty]
        [IncludeInCodeGeneratorForAndroid]
        public int SenderTerminalId { get; set; }

        /// <summary>
        /// Gets or sets the receiver terminal id
        /// </summary>
        [XmlElement, JsonProperty]
        [IncludeInCodeGeneratorForAndroid]
        public int ReceiverTerminalId { get; set; }

        /// <summary>
        /// Gets or sets the sender deliverypoint id
        /// </summary>
        [XmlElement, JsonProperty]
        [IncludeInCodeGeneratorForAndroid]
        public int SenderDeliverypointId { get; set; }

        /// <summary>
        /// Gets or sets the receiver deliverypoint id
        /// </summary>
        [XmlElement, JsonProperty]
        [IncludeInCodeGeneratorForAndroid]
        public int ReceiverDeliverypointId { get; set; }

        /// <summary>
        /// Gets or sets field value 1
        /// </summary>
        [XmlElement, JsonProperty]
        [IncludeInCodeGeneratorForAndroid]
        public string FieldValue1 { get; set; }

        /// <summary>
        /// Gets or sets field value 2
        /// </summary>
        [XmlElement, JsonProperty]
        [IncludeInCodeGeneratorForAndroid]
        public string FieldValue2 { get; set; }

        /// <summary>
        /// Gets or sets field value 3
        /// </summary>
        [XmlElement, JsonProperty]
        [IncludeInCodeGeneratorForAndroid]
        public string FieldValue3 { get; set; }

        /// <summary>
        /// Gets or sets field value 4
        /// </summary>
        [XmlElement, JsonProperty]
        [IncludeInCodeGeneratorForAndroid]
        public string FieldValue4 { get; set; }

        /// <summary>
        /// Gets or sets field value 5
        /// </summary>
        [XmlElement, JsonProperty]
        [IncludeInCodeGeneratorForAndroid]
        public string FieldValue5 { get; set; }

        /// <summary>
        /// Gets or sets field value 6
        /// </summary>
        [XmlElement, JsonProperty]
        [IncludeInCodeGeneratorForAndroid]
        public string FieldValue6 { get; set; }

        /// <summary>
        /// Gets or sets field value 7
        /// </summary>
        [XmlElement, JsonProperty]
        [IncludeInCodeGeneratorForAndroid]
        public string FieldValue7 { get; set; }

        /// <summary>
        /// Gets or sets field value 8
        /// </summary>
        [XmlElement, JsonProperty]
        [IncludeInCodeGeneratorForAndroid]
        public string FieldValue8 { get; set; }

        /// <summary>
        /// Gets or sets field value 9
        /// </summary>
        [XmlElement, JsonProperty]
        [IncludeInCodeGeneratorForAndroid]
        public string FieldValue9 { get; set; }

        /// <summary>
        /// Gets or sets field value 10
        /// </summary>
        [XmlElement, JsonProperty]
        [IncludeInCodeGeneratorForAndroid]
        public string FieldValue10 { get; set; }

        [XmlIgnore, JsonIgnore]
        public DateTime Created { get; set; }

        [XmlIgnore, JsonIgnore]
        public string MessageLog { get; set; }

        /// <summary>
        /// Gets or sets field value 11. THIS FIELD IS NOT SAVED IN THE DATABASE!
        /// </summary>
        [XmlElement, JsonProperty]
        [IncludeInCodeGeneratorForAndroid]
        public string FieldValue11 { get; set; }

        /// <summary>
        /// Gets or sets field value 12. THIS FIELD IS NOT SAVED IN THE DATABASE!
        /// </summary>
        [XmlElement, JsonProperty]
        [IncludeInCodeGeneratorForAndroid]
        public string FieldValue12 { get; set; }

        /// <summary>
        /// Gets or sets field value 13. THIS FIELD IS NOT SAVED IN THE DATABASE!
        /// </summary>
        [XmlElement, JsonProperty]
        [IncludeInCodeGeneratorForAndroid]
        public string FieldValue13 { get; set; }

        /// <summary>
        /// Gets or sets field value 14. THIS FIELD IS NOT SAVED IN THE DATABASE!
        /// </summary>
        [XmlElement, JsonProperty]
        [IncludeInCodeGeneratorForAndroid]
        public string FieldValue14 { get; set; }

        /// <summary>
        /// Gets or sets field value 15. THIS FIELD IS NOT SAVED IN THE DATABASE!
        /// </summary>
        [XmlElement, JsonProperty]
        [IncludeInCodeGeneratorForAndroid]
        public string FieldValue15 { get; set; }

        /// <summary>
        /// Gets or sets field value 16. THIS FIELD IS NOT SAVED IN THE DATABASE!
        /// </summary>
        [XmlElement, JsonProperty]
        [IncludeInCodeGeneratorForAndroid]
        public string FieldValue16 { get; set; }

        /// <summary>
        /// Gets or sets field value 17. THIS FIELD IS NOT SAVED IN THE DATABASE!
        /// </summary>
        [XmlElement, JsonProperty]
        [IncludeInCodeGeneratorForAndroid]
        public string FieldValue17 { get; set; }

        /// <summary>
        /// Gets or sets field value 17. THIS FIELD IS NOT SAVED IN THE DATABASE!
        /// </summary>
        [XmlElement, JsonProperty]
        [IncludeInCodeGeneratorForAndroid]
        public string FieldValue18 { get; set; }

        #endregion
    }
}
