﻿using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Obymobi.Attributes;

namespace Obymobi.Logic.Model
{
    /// <summary>
    /// Model class which represents a point-of-sale category
    /// </summary>
    [Serializable, XmlRootAttribute(ElementName = "Poscategory")]
    [DataContract]
    public class Poscategory : ModelBase
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Obymobi.Logic.Model.Poscategory type
        /// </summary>
        public Poscategory()
        {
        }

        /// <summary>
        /// Constructs an instance of the Obymobi.Logic.Model.Poscategory type using the specified parameters
        /// </summary>
        public Poscategory(int PoscategoryId, int CompanyId, string ExternalId, string Name)
        {
            this.PoscategoryId = PoscategoryId;
            this.CompanyId = CompanyId;
            this.ExternalId = ExternalId;
            this.Name = Name;
        }

        public Poscategory(int PoscategoryId, int CompanyId, string ExternalId, string Name, string RevenueCenter)
        {
            this.PoscategoryId = PoscategoryId;
            this.CompanyId = CompanyId;
            this.ExternalId = ExternalId;
            this.Name = Name;
            this.RevenueCenter = RevenueCenter;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the id of the category
        /// </summary>
        [XmlElement, PrimaryKeyFieldOfModel]
        public int PoscategoryId
        { get; set; }

        /// <summary>
        /// Gets or sets the id of the company
        /// </summary>
        [XmlElement]
        public int CompanyId
        { get; set; }

        /// <summary>
        /// Gets or sets the external id of the pos category
        /// </summary>
        [XmlElement]
        [DataMember]
        public string ExternalId
        { get; set; }

        /// <summary>
        /// Gets or sets the name of the pos category
        /// </summary>
        [XmlElement]
        [DataMember]
        public string Name
        { get; set; }

        /// <summary>
        /// Gets or sets the posproduct external id.
        /// </summary>
        /// <value>
        /// The posproduct external id.
        /// </value>
        [XmlElement]
        public string PosproductExternalId
        { get; set; }

        /// <summary>
        /// Gets or sets an implementation specific custom-value
        /// </summary>
        [XmlElement]
        public string FieldValue1
        { get; set; }

        /// <summary>
        /// Gets or sets an implementation specific custom-value
        /// </summary>
        [XmlElement]
        public string FieldValue2
        { get; set; }

        /// <summary>
        /// Gets or sets an implementation specific custom-value
        /// </summary>
        [XmlElement]
        public string FieldValue3
        { get; set; }

        /// <summary>
        /// Gets or sets an implementation specific custom-value
        /// </summary>
        [XmlElement]
        public string FieldValue4
        { get; set; }

        /// <summary>
        /// Gets or sets an implementation specific custom-value
        /// </summary>
        [XmlElement]
        public string FieldValue5
        { get; set; }

        /// <summary>
        /// Gets or sets an implementation specific custom-value
        /// </summary>
        [XmlElement]
        public string FieldValue6
        { get; set; }

        /// <summary>
        /// Gets or sets an implementation specific custom-value
        /// </summary>
        [XmlElement]
        public string FieldValue7
        { get; set; }

        /// <summary>
        /// Gets or sets an implementation specific custom-value
        /// </summary>
        [XmlElement]
        public string FieldValue8
        { get; set; }

        /// <summary>
        /// Gets or sets an implementation specific custom-value
        /// </summary>
        [XmlElement]
        public string FieldValue9
        { get; set; }

        /// <summary>
        /// Gets or sets an implementation specific custom-value
        /// </summary>
        [XmlElement]
        public string FieldValue10
        { get; set; }

        [XmlElement]
        public string RevenueCenter
        { get; set; }



        #endregion

        #region Methods

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns></returns>
        public Poscategory Clone()
        {
            return this.Clone<Poscategory>();
        }

        #endregion
    }
}
