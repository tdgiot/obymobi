﻿using System;
using System.Xml.Serialization;
using Obymobi.Attributes;

namespace Obymobi.Logic.Model
{
    /// <summary>
    /// Model class which represents a rating
    /// </summary>
	[Serializable, XmlRootAttribute(ElementName = "Rating"), IncludeInCodeGeneratorForMobile]
    public class Rating : ModelBase
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Obymobi.Logic.Model.Rating type
        /// </summary>
        public Rating()
        {
        }

        #endregion

        #region Xml Properties

        /// <summary>
        /// Gets or sets the id of the rating
        /// </summary>
        [XmlElement, PrimaryKeyFieldOfModel]
        [IncludeInCodeGeneratorForAndroid]
        public int RatingId
        { get; set; }

        /// <summary>
        /// Gets or sets the id of the product of the rating
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int ProductId
        { get; set; }

        /// <summary>
        /// Gets or sets the value of the rating
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForJavascript]
        [IncludeInCodeGeneratorForAndroid]
        public int Value
        { get; set; }

        /// <summary>
        /// Gets or sets the flag which indicates whether the rating is visible
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public bool Visible
        { get; set; }

        /// <summary>
        /// Gets or sets the comments of the rating
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string Comments
        { get; set; }

        #endregion

        #region Custom properties

        /// <summary>
        /// Timestamp this rating was added
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string Created
        { get; set; }

        /// <summary>
        /// Gets or sets the created time.
        /// </summary>
        /// <value>
        /// The created time.
        /// </value>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string CreatedTime
        { get; set; }

        /// <summary>
        /// Table ID where the rating was done
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int DeliverypointId 
        { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns></returns>
        public Rating Clone()
        {
            return this.Clone<Rating>();
        }

        #endregion
    }
}
