﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Obymobi.Attributes;

namespace Obymobi.Logic.Model
{
    /// <summary>
    /// Announcement model
    /// </summary>
    [Serializable, XmlRootAttribute(ElementName = "Announcement"), IncludeInCodeGeneratorForAndroid]
    public class Announcement : ModelBase
    {
        /// <summary>
        /// Gets or sets the id of the announcement
        /// </summary>
        [XmlElement, PrimaryKeyFieldOfModel]
        [IncludeInCodeGeneratorForAndroid]
        public int AnnouncementId
        { get; set; }

        /// <summary>
        /// Gets or sets the title of the announcement
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string Title
        { get; set; }

        /// <summary>
        /// Gets or sets the subtitle of the announcement
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        [Obsolete("MB: Field does not exists anymore in database")]
        public string Subtitle
        { get; set; }

        /// <summary>
        /// Gets or sets the text of the announcement
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string Text
        { get; set; }

        /// <summary>
        /// Gets or sets the date to show the message, if not set it's everyday
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public DateTime DateToShow
        { get; set; }

        /// <summary>
        /// Gets or sets the time to show the message
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public DateTime TimeToShow
        { get; set; }

        /// <summary>
        /// Gets or sets the flag which indicates whether the announcement is recurring
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public bool Recurring
        { get; set; }

        /// <summary>
        /// Gets or sets the recurring period of the announcement
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int RecurringPeriod
        { get; set; }

        /// <summary>
        /// Gets or sets the begin of the recurring period of the announcement
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public DateTime RecurringBegin
        { get; set; }

        /// <summary>
        /// Gets or sets the end of the recurring period of the announcement
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public DateTime RecurringEnd
        { get; set; }

        /// <summary>
        /// Gets or sets the amount of the recurring period of the announcement
        /// </summary>
        [XmlElement]
        public int RecurringAmount
        { get; set; }

        /// <summary>
        /// Gets or sets the amount of the recurring minutes of the announcement
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int RecurringMinutes
        { get; set; }

        /// <summary>
        /// Gets or sets the type of dialog to display of the announcement
        /// </summary>
        [XmlElement]
        public int DialogType
        { get; set; }

        /// <summary>
        /// Gets or sets the onyes action of the announcement
        /// </summary>
        [XmlElement]
        public int OnYes
        { get; set; }

        /// <summary>
        /// Gets or sets the onno action of the announcement
        /// </summary>
        [XmlElement]
        public int OnNo
        { get; set; }

        /// <summary>
        /// Gets or sets the onyes category of the announcement
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int OnYesCategory
        { get; set; }

        /// <summary>
        /// Gets or sets the onno category of the announcement
        /// </summary>
        [XmlElement]
        public int OnNoCategory
        { get; set; }

        /// <summary>
        /// Gets or sets the onyes entertainment category of the announcement
        /// </summary>
        [XmlElement]
        public int OnYesEntertainmentCategory
        { get; set; }

        /// <summary>
        /// Gets or sets the onno entertainment category of the announcement
        /// </summary>
        [XmlElement]
        public int OnNoEntertainmentCategory
        { get; set; }

        /// <summary>
        /// Gets or sets the onyes entertainment of the announcement
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int OnYesEntertainment
        { get; set; }

        /// <summary>
        /// Gets or sets the onyes product of the announcement
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int OnYesProduct
        { get; set; }

        /// <summary>
        /// Gets or sets the image mediaId of the announcement
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int MediaId
        { get; set; }

        /// <summary>
        /// Gets or sets the onyes entertainment of the announcement
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int Duration
        { get; set; }

        /// <summary>
        /// Gets or sets the custom text collection
        /// </summary>
        [XmlArray("CustomTexts")]
        [XmlArrayItem("CustomText")]
        [IncludeInCodeGeneratorForAndroid]
        public CustomText[] CustomTexts
        { get; set; }

        #region Methods

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns></returns>
        public Announcement Clone()
        {
            return this.Clone<Announcement>();
        }

        #endregion

    }
}
