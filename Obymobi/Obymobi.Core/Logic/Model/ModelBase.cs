﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Runtime.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Obymobi.Logic.HelperClasses;
using System.Xml.Serialization;

namespace Obymobi.Logic.Model
{
    /// <summary>
    /// Base class for models
    /// </summary>
    [XmlTypeAttribute(TypeName = "ModelBase", Namespace = "urn:baz")]
    public class ModelBase
    {
        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        protected T Clone<T>()
        {
            return XmlHelper.Deserialize<T>(XmlHelper.Serialize(this));
        }

        /// <summary>
        /// Copies the fields to model.
        /// </summary>
        /// <param name="target">The target.</param>
        public void CopyFieldsToModel(object target)
        {
            var propertiesOfTarget = TypeDescriptor.GetProperties(target);
            var propertiesOfThisModel = TypeDescriptor.GetProperties(this);

            // List of existing Properties
            List<string> propertiesOnThisModelNames = new List<string>();


            foreach (PropertyDescriptor propertyOnTarget in propertiesOfTarget)
            {
                try
                {
                    // Get property in source
                    PropertyDescriptor propertyOnSource = null;
                    foreach (PropertyDescriptor prop in propertiesOfThisModel)
                    {
                        if (prop.Name == propertyOnTarget.Name)
                        {
                            propertyOnSource = prop;
                            break;
                        }
                    }

                    // Make sur the target property exists on this model before we transfer
                    if (propertyOnSource != null)
                    {
                        propertyOnTarget.SetValue(target, propertyOnSource.GetValue(this));
                    }
                }
                catch
                {
                    // For invalid types.
                }
            }
        }

        /// <summary>
        /// Serialize Model to XML
        /// </summary>
        /// <returns></returns>
        public string toXml()
        {
            return XmlHelper.Serialize(this);
        }

		public string ToJson()
		{
			var settings = new JsonSerializerSettings();
			settings.NullValueHandling = NullValueHandling.Ignore;
			settings.ContractResolver = new CamelCasePropertyNamesContractResolver();
			return JsonConvert.SerializeObject(this, new Formatting(), settings);
		}

        ///// <summary>
        ///// Contains the Xml from which the Model was deserialized. 
        ///// Is used by the Webservice when hashing parameters.
        ///// </summary>
        //[XmlIgnore]
        //public string DeserializedFromXml { get; set; }
    }
}
