﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.Logic.Model
{
    /// <summary>
    /// SocialmediaMessageAction model
    /// </summary>
    public class SocialmediaMessageAction : ModelBase
    {
        /// <summary>
        /// MessageAction enums
        /// </summary>
        public enum MessageAction : int
        {
            /// <summary>
            /// MessageAction is publish
            /// </summary>
            PUBLISH,
            /// <summary>
            /// MessageAction is remove
            /// </summary>
            REMOVE
        }

        /// <summary>
        /// Gets or sets the socialmedia message id.
        /// </summary>
        /// <value>
        /// The socialmedia message id.
        /// </value>
        public int SocialmediaMessageId { get; set; }

        /// <summary>
        /// Gets or sets the action.
        /// </summary>
        /// <value>
        /// The action.
        /// </value>
        public int Action { get; set; }

        #region Methods

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns></returns>
        public SocialmediaMessageAction Clone()
        {
            return this.Clone<SocialmediaMessageAction>();
        }

        #endregion
    }
}
