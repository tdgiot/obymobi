﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Obymobi.Attributes;

namespace Obymobi.Logic.Model
{
    /// <summary>
    /// Model class which represents a customer
    /// </summary>
    [Serializable, XmlRootAttribute(ElementName = "Customer"), IncludeInCodeGeneratorForFlex]
    public class Customer : ModelBase
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Obymobi.Logic.Model.Customer type 
        /// </summary>
        public Customer()
        {
        }

        /// <summary>
        /// Constructs an instance of the Obymobi.Logic.Model.Customer type using the specified parameters
        /// </summary>
        public Customer(int CustomerId, string Phonenumber, string Password, string Verified, string Firstname, string Lastname, string LastnamePrefix)
        {
            this.CustomerId = CustomerId;
            this.Phonenumber = Phonenumber;
            this.Password = Password;
            this.Verified = Verified;
            this.Firstname = Firstname;
            this.Lastname = Lastname;
            this.LastnamePrefix = LastnamePrefix;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the id of the customer
        /// </summary>
        [XmlElement, PrimaryKeyFieldOfModel]
        [IncludeInCodeGeneratorForFlex]
        public int CustomerId
        { get; set; }

        /// <summary>
        /// Gets or sets the phonenumber of the customer
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForFlex]
        public string Phonenumber
        { get; set; }

        /// <summary>
        /// Gets or sets the emailaddress of the customer
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForFlex]
        public string Email
        { get; set; }


        /// <summary>
        /// Gets or sets the password of the customer
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForFlex]
        public string Password
        { get; set; }

        /// <summary>
        /// Gets or sets the verified flag of the customer
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForFlex]
        public string Verified
        { get; set; }

        /// <summary>
        /// Gets or sets the firstname of the customer
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForFlex]
        public string Firstname
        { get; set; }

        /// <summary>
        /// Gets or sets the lastname of the customer
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForFlex]
        public string Lastname
        { get; set; }

        /// <summary>
        /// Gets or sets the lastname prefix of the customer
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForFlex]
        public string LastnamePrefix
        { get; set; }

        /// <summary>
        /// Gets or sets the birth date of the customer
        /// </summary>
        [XmlElement]
        public DateTime? Birthdate
        { get; set; }

        /// <summary>
        /// Gets or sets the Gender
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForFlex]
        public string GenderSingleLetter
        { get; set; }

        /// <summary>
        /// Gets or sets the addressline1
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForFlex]
        public string Addressline1
        { get; set; }

        /// <summary>
        /// Gets or sets the addressline2 
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForFlex]
        public string Addressline2
        { get; set; }

        /// <summary>
        /// Gets or sets the zipcode of the company
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForFlex]
        public string Zipcode
        { get; set; }

        /// <summary>
        /// Gets or sets the city of the company
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForFlex]
        public string City
        { get; set; }

        /// <summary>
        /// Gets or sets the Facebook ID
        /// </summary>
        [XmlElement]
        public string FacebookId
        { get; set; }

        /// <summary>
        /// Gets or sets the Facebook access token
        /// </summary>
        [XmlElement]
        public string FacebookToken
        { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns></returns>
        public Customer Clone()
        {
            return this.Clone<Customer>();
        }

        #endregion
    }
}
