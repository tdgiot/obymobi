﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Obymobi.Attributes;

namespace Obymobi.Logic.Model
{
    /// <summary>
    /// Model class which represents an alteration item
    /// </summary>
    [Serializable, XmlRootAttribute(ElementName = "Alterationitem"), IncludeInCodeGeneratorForFlex, IncludeInCodeGeneratorForAndroid, IncludeInCodeGeneratorForMobile, IncludeInCodeGeneratorForXamarin]
    public class Alterationitem : ModelBase
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Obymobi.Logic.Model.Alterationitem type
        /// </summary>
        public Alterationitem()
        {

        }

        #endregion

        #region Xml Properties

        /// <summary>
        /// Gets or sets the id of the alteration item
        /// </summary>
        [XmlElement, PrimaryKeyFieldOfModel]
        [IncludeInCodeGeneratorForFlex]
        [IncludeInCodeGeneratorForAndroid]
        [IncludeInCodeGeneratorForMobile]
        [IncludeInCodeGeneratorForXamarin]
        public int AlterationitemId
        { get; set; }

        /// <summary>
        /// Gets or sets the id of the alteration
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForFlex]
        [IncludeInCodeGeneratorForAndroid]
        [IncludeInCodeGeneratorForMobile]
        [IncludeInCodeGeneratorForXamarin]
        public int AlterationId
        { get; set; }

        /// <summary>
        /// Gets or sets a unique identifier
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForFlex]
        [IncludeInCodeGeneratorForAndroid]
        [IncludeInCodeGeneratorForMobile]
        [IncludeInCodeGeneratorForXamarin]
        public string Guid
        { get; set; }

        /// <summary>
        /// Gets or sets the id of the alteration
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForFlex]
        [IncludeInCodeGeneratorForAndroid]
        [IncludeInCodeGeneratorForMobile]
        [IncludeInCodeGeneratorForXamarin]
        public string AlterationName
        { get; set; }

        /// <summary>
        /// Gets or sets the alteration type
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForFlex]
        [IncludeInCodeGeneratorForAndroid]
        [IncludeInCodeGeneratorForMobile]
        [IncludeInCodeGeneratorForXamarin]
        public int AlterationType
        { get; set; }

        /// <summary>
        /// Gets or sets the order level enabled flag
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForFlex]
        [IncludeInCodeGeneratorForAndroid]
        [IncludeInCodeGeneratorForMobile]
        public bool AlterationOrderLevelEnabled
        { get; set; }

        /// <summary>
        /// Gets or sets the id of the alteration option
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForFlex]
        [IncludeInCodeGeneratorForAndroid]
        [IncludeInCodeGeneratorForMobile]
        [IncludeInCodeGeneratorForXamarin]
        public int AlterationoptionId
        { get; set; }

        /// <summary>
        /// Gets or sets the id of the alteration
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForFlex]
        [IncludeInCodeGeneratorForAndroid]
        [IncludeInCodeGeneratorForMobile]
        public string AlterationoptionName
        { get; set; }

        /// <summary>
        /// Gets or sets the sort order of the alteration option
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        [IncludeInCodeGeneratorForXamarin]
        public decimal AlterationoptionPriceIn
        { get; set; }

        /// <summary>
        /// Gets or sets the id of the alteration
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForFlex]
        [IncludeInCodeGeneratorForMobile]
        [IncludeInCodeGeneratorForXamarin]
        public bool SelectedOnDefault
        { get; set; }

        /// <summary>
        /// Gets or sets the sort order of the alteration option
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForFlex]
        [IncludeInCodeGeneratorForAndroid]
        [IncludeInCodeGeneratorForMobile]
        [IncludeInCodeGeneratorForXamarin]
        public int SortOrder
        { get; set; }

        /// <summary>
        /// Gets or sets selected time
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForFlex]
        [IncludeInCodeGeneratorForAndroid]
        [IncludeInCodeGeneratorForMobile]
        [IncludeInCodeGeneratorForXamarin]
        public string Time
        { get; set; }

        /// <summary>
        /// Gets or sets free text value
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForFlex]
        [IncludeInCodeGeneratorForAndroid]
        [IncludeInCodeGeneratorForMobile]
        [IncludeInCodeGeneratorForXamarin]
        public string Value
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int PriceLevelItemId { get; set; }

        /// <summary>
        /// Gets or sets the posalterationitem.
        /// </summary>
        /// <value>
        /// The posalterationitem.
        /// </value>
        [XmlElement]
        public Posalterationitem Posalterationitem
        { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns></returns>
        public Alterationitem Clone()
        {
            return this.Clone<Alterationitem>();
        }

        #endregion
    }
}
