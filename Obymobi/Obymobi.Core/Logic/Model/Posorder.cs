﻿using System;
using System.Text;
using System.Xml.Serialization;
using Dionysos;
using Obymobi.Attributes;

namespace Obymobi.Logic.Model
{
    /// <summary>
    /// Model class which represents a point-of-sale product
    /// </summary>
    [Serializable, XmlRootAttribute(ElementName = "Posorder")]
    public class Posorder : ModelBase
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Obymobi.Logic.Model.Posorder type
        /// </summary>
        public Posorder()
        {
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the id of the order
        /// </summary>
        [XmlElement, PrimaryKeyFieldOfModel]
        public int PosorderId
        { get; set; }

        /// <summary>
        /// Gets or sets the external id of the pos deliverypoint
        /// </summary>
        [XmlElement]
        public string PosdeliverypointExternalId
        { get; set; }

        /// <summary>
        /// Gets or sets the external id of the pos paymentmethod
        /// </summary>
        [XmlElement]
        public string PospaymentmethodExternalId
        { get; set; }

        /// <summary>
        /// Gets or sets the type of the order
        /// </summary>
        [XmlElement]
        public int Type
        { get; set; }

        /// <summary>
        /// Gets or sets the orderitems of the pos order
        /// </summary>
        [XmlArray]
        public Posorderitem[] Posorderitems
        { get; set; }

        /// <summary>
        /// Gets or sets an implementation specific custom-value
        /// </summary>
        [XmlElement]
        public string FieldValue1
        { get; set; }

        /// <summary>
        /// Gets or sets an implementation specific custom-value
        /// </summary>
        [XmlElement]
        public string FieldValue2
        { get; set; }

        /// <summary>
        /// Gets or sets an implementation specific custom-value
        /// </summary>
        [XmlElement]
        public string FieldValue3
        { get; set; }

        /// <summary>
        /// Gets or sets an implementation specific custom-value
        /// </summary>
        [XmlElement]
        public string FieldValue4
        { get; set; }

        /// <summary>
        /// Gets or sets an implementation specific custom-value
        /// </summary>
        [XmlElement]
        public string FieldValue5
        { get; set; }

        /// <summary>
        /// Gets or sets an implementation specific custom-value
        /// </summary>
        [XmlElement]
        public string FieldValue6
        { get; set; }

        /// <summary>
        /// Gets or sets an implementation specific custom-value
        /// </summary>
        [XmlElement]
        public string FieldValue7
        { get; set; }

        /// <summary>
        /// Gets or sets an implementation specific custom-value
        /// </summary>
        [XmlElement]
        public string FieldValue8
        { get; set; }

        /// <summary>
        /// Gets or sets an implementation specific custom-value
        /// </summary>
        [XmlElement]
        public string FieldValue9
        { get; set; }

        /// <summary>
        /// Gets or sets an implementation specific custom-value
        /// </summary>
        [XmlElement]
        public string FieldValue10
        { get; set; }

        [XmlElement]
        public int GuestCount
        { get; set; }

        [XmlElement]
        public string ErrorMessage { get; set; }

        [XmlElement]
        public string Notes
        { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns></returns>
        public Posorder Clone()
        {
            return this.Clone<Posorder>();
        }

        public new string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendFormatLine("PosorderId: {0}", PosorderId);
            sb.AppendFormatLine("Type: {0}", Type);
            sb.AppendFormatLine("Posorderitems Count: {0}", Posorderitems != null ? Posorderitems.Length : 0);
            if (Posorderitems != null)
            {
                foreach (Posorderitem posorderitem in Posorderitems)
                {
                    sb.AppendFormatLine(" - Posorderitem: {0} (Id: {1}, External: {2}, Quantity: {3}, Alterations: {4})",
                                        posorderitem.Description,
                                        posorderitem.PosorderitemId,
                                        posorderitem.PosproductExternalId, 
                                        posorderitem.Quantity,
                                        posorderitem.Posalterationitems != null ? posorderitem.Posalterationitems.Length : 0);
                    if (posorderitem.Posalterationitems != null)
                    {
                        foreach (Posalterationitem posalterationitem in posorderitem.Posalterationitems)
                        {
                            sb.AppendFormatLine("    * Posalterationitem: {0} (ExternalPosalterationId: {1}, ExternalPosalterationoptionId: {2})",
                                                posalterationitem.PosalterationitemId,
                                                posalterationitem.ExternalPosalterationId,
                                                posalterationitem.ExternalPosalterationoptionId);
                        }
                    }
                }
            }

            return sb.ToString();
        }

        #endregion
    }
}
