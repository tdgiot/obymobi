﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Obymobi.Attributes;

namespace Obymobi.Logic.Model
{
    /// <summary>
    /// Model class which represents an entertainmentcategory item
    /// </summary>
	[Serializable, XmlRootAttribute(ElementName = "Entertainmentcategory"), IncludeInCodeGeneratorForAndroid]
    public class Entertainmentcategory : ModelBase
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Obymobi.Logic.Model.Entertainmentcategory type
        /// </summary>
        public Entertainmentcategory()
        {
        }

        #endregion

        #region Xml Properties

        /// <summary>
        /// Gets or sets the id of the entertainment category
        /// </summary>
        [XmlElement, PrimaryKeyFieldOfModel]
        [IncludeInCodeGeneratorForAndroid]
        public int EntertainmentcategoryId
        { get; set; }

        /// <summary>
        /// Gets or sets the name of the category
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string Name
        { get; set; }        

        /// <summary>
        /// Gets or sets the custom texts
        /// </summary>
        [XmlArray("CustomTexts")]
        [XmlArrayItem("CustomText")]
        [IncludeInCodeGeneratorForAndroid]
        public CustomText[] CustomTexts
        { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns></returns>
        public Entertainmentcategory Clone()
        {
            return this.Clone<Entertainmentcategory>();
        }

        #endregion
    }
}
