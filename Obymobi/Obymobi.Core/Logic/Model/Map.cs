﻿using System;
using System.Xml.Serialization;
using Obymobi.Attributes;

namespace Obymobi.Logic.Model
{
    /// <summary>
    /// Model class which represents a map
    /// </summary>
    [Serializable, XmlRootAttribute(ElementName = "Map"), IncludeInCodeGeneratorForAndroid]
    public class Map : ModelBase
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Obymobi.Logic.Model.Map type
        /// </summary>
        public Map()
        {
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the map id
        /// </summary>
        [XmlElement, PrimaryKeyFieldOfModel]        
        [IncludeInCodeGeneratorForAndroid]
        public int MapId
        { get; set; }

        /// <summary>
        /// Gets or sets the name
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string Name
        { get; set; }

        /// <summary>
        /// Gets or sets the map type
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int MapType
        { get; set; }

        /// <summary>
        /// Gets or sets wether my location is enabled
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public bool MyLocationEnabled
        { get; set; }

        /// <summary>
        /// Gets or sets wether zoom controls are enabled
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public bool ZoomControlsEnabled
        { get; set; }

        /// <summary>
        /// Gets or sets wether indoor views are enabled
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public bool IndoorEnabled
        { get; set; }

        /// <summary>
        /// Gets or sets the zoom level
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int ZoomLevel
        { get; set; }

        /// <summary>
        /// Gets or sets the map provider
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int MapProvider
        { get; set; }

        /// <summary>
        /// Gets or sets the points of interest synopsis
        /// </summary>
        [XmlArray("PointOfInterestsSynopses")]
        [XmlArrayItem("PointOfInterestSynopsis")]
        [IncludeInCodeGeneratorForAndroid]
        public PointOfInterestSynopsis[] PointOfInterestSynopses
        { get; set; }        

        #endregion

        #region Methods

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns></returns>
        public Map Clone()
        {
            return this.Clone<Map>();
        }

        #endregion
    }
}
