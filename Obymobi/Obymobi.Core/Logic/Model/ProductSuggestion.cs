﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Obymobi.Attributes;

namespace Obymobi.Logic.Model
{
    /// <summary>
    /// Model class which represents an alteration item
    /// </summary>
	[Serializable, XmlRootAttribute(ElementName = "ProductSuggestion"), IncludeInCodeGeneratorForFlex, IncludeInCodeGeneratorForAndroid, IncludeInCodeGeneratorForMobile, IncludeInCodeGeneratorForXamarin]
    public class ProductSuggestion : ModelBase
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Obymobi.Logic.Model.ProductSuggestion type
        /// </summary>
        public ProductSuggestion()
        {
        }

        #endregion

        #region Xml Properties

        /// <summary>
        /// Gets or sets the id of the alteration item
        /// </summary>
        [XmlElement, PrimaryKeyFieldOfModel]
        [IncludeInCodeGeneratorForFlex]
        [IncludeInCodeGeneratorForAndroid]
        [IncludeInCodeGeneratorForMobile]
        [IncludeInCodeGeneratorForXamarin]
        public int ProductSuggestionId
        { get; set; }

        /// <summary>
        /// Gets or sets the id of the product
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForFlex]
        [IncludeInCodeGeneratorForAndroid]
        [IncludeInCodeGeneratorForMobile]
        [IncludeInCodeGeneratorForXamarin]
        public int ProductId
        { get; set; }

        /// <summary>
        /// Gets or sets the id of the suggested product
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForJavascript]
        [IncludeInCodeGeneratorForFlex]
        [IncludeInCodeGeneratorForAndroid]
        [IncludeInCodeGeneratorForMobile]
        [IncludeInCodeGeneratorForXamarin]
        public int SuggestedProductId
        { get; set; }

		/// <summary>
		/// Gets or sets Direct, en wat dat betekent zoek je GVD zelef maar uit...
		/// // GK Dit is gemaakt op 19-06-2009 0:41, eignelijk een donderdag
		/// </summary>
		[XmlElement]
        [IncludeInCodeGeneratorForFlex]
        [IncludeInCodeGeneratorForMobile]
		public bool Direct
		{ get; set; }

        /// <summary>
        /// Gets or sets if the suggestion should be shown during checkout
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForJavascript]
        [IncludeInCodeGeneratorForFlex]
        [IncludeInCodeGeneratorForAndroid]
        [IncludeInCodeGeneratorForMobile]
        [IncludeInCodeGeneratorForXamarin]
        public bool Checkout
        { get; set; }

        /// <summary>
        /// Gets or sets the categoryId which contains the suggested product
        /// </summary>
        [IncludeInCodeGeneratorForAndroid]
        public int CategoryId
        { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns></returns>
        public ProductSuggestion Clone()
        {
            return this.Clone<ProductSuggestion>();
        }

        #endregion
    }
}
