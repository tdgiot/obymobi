﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Obymobi.Attributes;

namespace Obymobi.Logic.Model
{
    /// <summary>
    /// Model class which represents a room control area
    /// </summary>
    [Serializable, XmlRootAttribute(ElementName = "RoomControlArea"), IncludeInCodeGeneratorForAndroid]
    public class RoomControlArea : ModelBase
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Obymobi.Logic.Model.RoomControlArea type
        /// </summary>
        public RoomControlArea()
        {
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the id of the room control area
        /// </summary>
        [XmlElement, PrimaryKeyFieldOfModel]
        [IncludeInCodeGeneratorForAndroid]
        public int RoomControlAreaId
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string Name
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string NameSystem
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int SortOrder
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public bool Visible
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int Type
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public bool IsNameSystemBaseId
        { get; set; }
        
        /// <summary>
        /// Gets or sets the items of the room control section
        /// </summary>
        [XmlArray("RoomControlSections")]
        [XmlArrayItem("RoomControlSection")]
        [IncludeInCodeGeneratorForAndroid]
        public RoomControlSection[] RoomControlSections
        { get; set; }

        /// <summary>
        /// Gets or sets the custom text collection
        /// </summary>
        [XmlArray("CustomTexts")]
        [XmlArrayItem("CustomText")]
        [IncludeInCodeGeneratorForAndroid]
        public CustomText[] CustomTexts
        { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns></returns>
        public RoomControlArea Clone()
        {
            return this.Clone<RoomControlArea>();
        }

        #endregion
    }
}
