﻿using System;
using System.Xml.Serialization;
using Newtonsoft.Json;

namespace Obymobi.Logic.Model
{
    /// <summary>
    /// LoginResult model
    /// </summary>
	[JsonObject(MemberSerialization.OptIn)]
    [Serializable]
	public class LoginResult
	{
        /// <summary>
        /// LoginStatus enums
        /// </summary>
		public enum LoginStatus : int
		{
            /// <summary>
            /// Logged in
            /// </summary>
            [XmlEnum("300")]
			LOGGED_IN = 300,
            /// <summary>
            /// Wrong account
            /// </summary>
            [XmlEnum("301")]
			WRONG_ACCOUNT = 301,
            /// <summary>
            /// Wrong version
            /// </summary>
            [XmlEnum("302")]
			WRONG_VERSION = 302,
            /// <summary>
            /// Client online
            /// </summary>
            [XmlEnum("303")]
            CLIENT_ONLINE = 303,
            /// <summary>
            /// No client
            /// </summary>
            [XmlEnum("304")]
            NO_CLIENT = 304,
            /// <summary>
            /// Queued
            /// </summary>
            [XmlEnum("305")]
            QUEUED = 305,
            /// <summary>
            /// Continue
            /// </summary>
            [XmlEnum("306")]
            CONTINUE = 306
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="LoginResult"/> class.
        /// </summary>
		public LoginResult()
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="LoginResult"/> class.
        /// </summary>
        /// <param name="status">The status.</param>
		public LoginResult(LoginStatus status)
		{
            this.Status = status;
            this.Code = (int)status;

			switch (status)
			{
				case LoginStatus.LOGGED_IN:
					Message = "OK";
					break;
				case LoginStatus.WRONG_ACCOUNT:
					Message = "Could not login with supplied credentials.";
					break;
				case LoginStatus.WRONG_VERSION:
					Message = "Client version out-dated.";
					break;
                case LoginStatus.CLIENT_ONLINE:
                    Message = "A client with this ID is already online.";
                    break;
                case LoginStatus.NO_CLIENT:
			        Message = "No client ID specified.";
			        break;
                case LoginStatus.QUEUED:
                    Message = "Client is queued.";
                    break;
                case LoginStatus.CONTINUE:
                    Message = "Client can continue login and start loading data.";
                    break;
			}
		}

		#region Properties

        /// <summary>
        /// Gets or sets the status.
        /// </summary>
        /// <value>
        /// The status.
        /// </value>
		[JsonProperty]
		public LoginStatus Status { get; set; }

        /// <summary>
        /// Gets or sets the code.
        /// </summary>
        /// <value>
        /// The code.
        /// </value>
		[JsonProperty]
		public int Code { get; set; }

        /// <summary>
        /// Gets or sets the message.
        /// </summary>
        /// <value>
        /// The message.
        /// </value>
		[JsonProperty]
		public string Message { get; set; }

        /// <summary>
        /// Gets or sets the latest version.
        /// </summary>
        /// <value>
        /// The latest version.
        /// </value>
        [JsonProperty]
        public int LatestVersion { get; set; }

        /// <summary>
        /// Gets or sets the queue position.
        /// </summary>
        /// <value>
        /// The queue position.
        /// </value>
        [JsonProperty]
        public int QueuePosition { get; set; }

        /// <summary>
        /// Gets or sets the length of the queue.
        /// </summary>
        /// <value>
        /// The length of the queue.
        /// </value>
        [JsonProperty]
        public int QueueLength { get; set; }

        /// <summary>
        /// Gets or sets the client GUID.
        /// </summary>
        /// <value>
        /// The client GUID.
        /// </value>
        [JsonProperty]
        public uint ClientGUID { get; set; }

		/// <summary>
		/// Gets sets the Deliverypointgroup
		/// </summary>
		[JsonProperty]
		public int DeliverypointgroupId { get; set; }

		#endregion
	}
}
