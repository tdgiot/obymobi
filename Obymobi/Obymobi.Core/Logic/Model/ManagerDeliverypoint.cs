﻿using Newtonsoft.Json;

namespace Obymobi.Logic.Model
{
    /// <summary>
    /// ManagerDeliverypoint model
    /// </summary>
    [JsonObject(MemberSerialization.OptIn)]
    public class ManagerDeliverypoint
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ManagerDeliverypoint"/> class.
        /// </summary>
        public ManagerDeliverypoint()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ManagerDeliverypoint"/> class.
        /// </summary>
        /// <param name="deliverypointId">The deliverypoint id.</param>
        /// <param name="deliverypointName">Name of the deliverypoint.</param>
        /// <param name="number">The number.</param>
        /// <param name="clients">The clients.</param>
        public ManagerDeliverypoint(int deliverypointId, string deliverypointName, int number, ManagerClient[] clients)
        {
            this.DeliverypointId = deliverypointId;
            this.DeliverypointName = deliverypointName;
            this.DeliverypointNumber = number;
            this.Clients = clients;
        }

        #region Properties

        /// <summary>
        /// Gets or sets the deliverypoint id.
        /// </summary>
        /// <value>
        /// The deliverypoint id.
        /// </value>
        [JsonProperty]
        public int DeliverypointId { get; set; }

        /// <summary>
        /// Gets or sets the name of the deliverypoint.
        /// </summary>
        /// <value>
        /// The name of the deliverypoint.
        /// </value>
        [JsonProperty]
        public string DeliverypointName { get; set; }

        /// <summary>
        /// Gets or sets the deliverypoint number.
        /// </summary>
        /// <value>
        /// The deliverypoint number.
        /// </value>
        [JsonProperty]
        public int DeliverypointNumber { get; set; }

        /// <summary>
        /// Gets or sets the clients.
        /// </summary>
        /// <value>
        /// The clients.
        /// </value>
        [JsonProperty]
        public ManagerClient[] Clients { get; set; }

        #endregion
    }
}
