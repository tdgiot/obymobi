﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Xml.Serialization;
using Obymobi.Attributes;

namespace Obymobi.Logic.Model
{
    /// <summary>
    /// Model class which represents a point-of-sale product
    /// </summary>
    [Serializable, XmlRootAttribute(ElementName = "Posproduct")]
    [DataContract]
    public class Posproduct : ModelBase
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Obymobi.Logic.Model.Posproduct type
        /// </summary>
        public Posproduct()
        {
        }

        /// <summary>
        /// Constructs an instance of the Obymobi.Logic.Model.Posproduct type using the specified parameters
        /// </summary>
        public Posproduct(int PosproductId, int CompanyId, string ExternalId, string ExternalPoscategoryId, string Name, decimal PriceIn, int VatTariff, int SortOrder, string Description)
        {
            this.PosproductId = PosproductId;
            this.CompanyId = CompanyId;
            this.ExternalId = ExternalId;
            this.ExternalPoscategoryId = ExternalPoscategoryId;
            this.Name = Name;
            this.PriceIn = PriceIn;
            this.VatTariff = VatTariff;
            this.SortOrder = SortOrder;
            this.Description = Description;
            this.Posalterations = new Posalteration[] { };
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the id of the product
        /// </summary>
        [XmlElement, PrimaryKeyFieldOfModel]
        public int PosproductId
        { get; set; }

        /// <summary>
        /// Gets or sets the id of the company
        /// </summary>
        [XmlElement]
        public int CompanyId
        { get; set; }

        /// <summary>
        /// Gets or sets the external id of the pos product
        /// </summary>
        [XmlElement]
        [DataMember]
        public string ExternalId
        { get; set; }

        /// <summary>
        /// Gets or sets the external id of the pos category
        /// </summary>
        [XmlElement]
        [DataMember]
        public string ExternalPoscategoryId
        { get; set; }

        /// <summary>
        /// Gets or sets the name of the pos product
        /// </summary>
        [XmlElement]
        [DataMember]
        public string Name
        { get; set; }

        /// <summary>
        /// Gets or sets the price including Vat of the pos product
        /// </summary>
        [XmlElement]
        [DataMember]
        public decimal PriceIn
        { get; set; }

        /// <summary>
        /// Gets or sets the vat tariff of the pos product
        /// </summary>
        [XmlElement]
        [DataMember]
        public int VatTariff
        { get; set; }

        /// <summary>
        /// Gets or sets the sort order of the pos product
        /// </summary>
        [XmlElement]
        public int SortOrder
        { get; set; }

        /// <summary>
        /// Gets or sets the description of the pos product
        /// </summary>
        [XmlElement]
        public string Description
        { get; set; }

        /// <summary>
        /// Gets or sets the posalterations.
        /// </summary>
        /// <value>
        /// The posalterations.
        /// </value>
        [XmlArray("Posalterations")]
        [XmlArrayItem("Posalteration")]
        [DataMember]
        public Posalteration[] Posalterations
        { get; set; }

        /// <summary>
        /// Gets or sets an implementation specific custom-value
        /// </summary>
        [XmlElement]
        public string FieldValue1
        { get; set; }

        /// <summary>
        /// Gets or sets an implementation specific custom-value
        /// </summary>
        [XmlElement]
        public string FieldValue2
        { get; set; }

        /// <summary>
        /// Gets or sets an implementation specific custom-value
        /// </summary>
        [XmlElement]
        public string FieldValue3
        { get; set; }

        /// <summary>
        /// Gets or sets an implementation specific custom-value
        /// </summary>
        [XmlElement]
        public string FieldValue4
        { get; set; }

        /// <summary>
        /// Gets or sets an implementation specific custom-value
        /// </summary>
        [XmlElement]
        public string FieldValue5
        { get; set; }

        /// <summary>
        /// Gets or sets an implementation specific custom-value
        /// </summary>
        [XmlElement]
        public string FieldValue6
        { get; set; }

        /// <summary>
        /// Gets or sets an implementation specific custom-value
        /// </summary>
        [XmlElement]
        public string FieldValue7
        { get; set; }

        /// <summary>
        /// Gets or sets an implementation specific custom-value
        /// </summary>
        [XmlElement]
        public string FieldValue8
        { get; set; }

        /// <summary>
        /// Gets or sets an implementation specific custom-value
        /// </summary>
        [XmlElement]
        public string FieldValue9
        { get; set; }

        /// <summary>
        /// Gets or sets an implementation specific custom-value
        /// </summary>
        [XmlElement]
        public string FieldValue10
        { get; set; }

        [XmlElement] 
        public string RevenueCenter { get; set; }


        #endregion

        #region Methods

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns></returns>
        public Posproduct Clone()
        {
            return this.Clone<Posproduct>();
        }

        #endregion
    }
}
