﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Obymobi.Attributes;

namespace Obymobi.Logic.Model
{
    /// <summary>
    /// IcrtouchprintermappingDeliverypoint class
    /// </summary>
    [Serializable, XmlRootAttribute(ElementName = "IcrtouchprintermappingDeliverypoint")]
    public class IcrtouchprintermappingDeliverypoint : ModelBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="IcrtouchprintermappingDeliverypoint"/> class.
        /// </summary>
        public IcrtouchprintermappingDeliverypoint()
        {
            this.Printer1 = 0;
            this.Printer2 = 0;
            this.Printer3 = 0;
            this.Printer4 = 0;
            this.Printer5 = 0;
            this.Printer6 = 0;
            this.Printer7 = 0;
            this.Printer8 = 0;
            this.Printer9 = 0;
            this.Printer10 = 0;
        }

        /// <summary>
        /// Gets or sets the icrtouchprintermapping deliverypoint id.
        /// </summary>
        /// <value>
        /// The icrtouchprintermapping deliverypoint id.
        /// </value>
        [XmlElement, PrimaryKeyFieldOfModel]
        public int IcrtouchprintermappingDeliverypointId
        { get; set; }

        /// <summary>
        /// Gets or sets the icrtouchprintermapping id.
        /// </summary>
        /// <value>
        /// The icrtouchprintermapping id.
        /// </value>
        [XmlElement, ForeignKeyFieldOfModel]
        public int IcrtouchprintermappingId
        { get; set; }

        /// <summary>
        /// Gets or sets the deliverypoint id.
        /// </summary>
        /// <value>
        /// The deliverypoint id.
        /// </value>
        [XmlElement, ForeignKeyFieldOfModel]
        public int DeliverypointId
        { get; set; }

        /// <summary>
        /// Define the target printer when printer 1 is targetted
        /// </summary>
        public int Printer1
        { get; set; }

        /// <summary>
        /// Define the target printer when printer 2 is targetted
        /// </summary>
        public int Printer2
        { get; set; }

        /// <summary>
        /// Define the target printer when printer is targetted
        /// </summary>
        public int Printer3
        { get; set; }

        /// <summary>
        /// Define the target printer when printer is targetted
        /// </summary>
        public int Printer4
        { get; set; }

        /// <summary>
        /// Define the target printer when printer is targetted
        /// </summary>
        public int Printer5
        { get; set; }

        /// <summary>
        /// Define the target printer when printer is targetted
        /// </summary>
        public int Printer6
        { get; set; }

        /// <summary>
        /// Define the target printer when printer is targetted
        /// </summary>
        public int Printer7
        { get; set; }

        /// <summary>
        /// Define the target printer when printer is targetted
        /// </summary>
        public int Printer8
        { get; set; }

        /// <summary>
        /// Define the target printer when printer is targetted
        /// </summary>
        public int Printer9
        { get; set; }

        /// <summary>
        /// Define the target printer when printer is targetted
        /// </summary>
        public int Printer10
        { get; set; }

        #region Methods

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns></returns>
        public IcrtouchprintermappingDeliverypoint Clone()
        {
            return this.Clone<IcrtouchprintermappingDeliverypoint>();
        }

        #endregion

    }
}
