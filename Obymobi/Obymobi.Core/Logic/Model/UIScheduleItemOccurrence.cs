﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Obymobi.Attributes;

namespace Obymobi.Logic.Model
{
    /// <summary>
    /// Model class which represents a scheduled item occurrence
    /// </summary>
	[Serializable, XmlRootAttribute(ElementName = "UIScheduleItemOccurrence"), IncludeInCodeGeneratorForFlex, IncludeInCodeGeneratorForAndroid, IncludeInCodeGeneratorForMobile]
    public class UIScheduleItemOccurrence : ModelBase
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Obymobi.Logic.Model.ItemScheduleitem type
        /// </summary>
        public UIScheduleItemOccurrence()
        {
        }

        #endregion

        #region Xml Properties

        /// <summary>
        /// Gets or sets the id of the itemschedule item
        /// </summary>
        [XmlElement, PrimaryKeyFieldOfModel]
        [IncludeInCodeGeneratorForAndroid]
        public int UIScheduleItemOccurrenceId
        { get; set; }

        /// <summary>
        /// Gets or sets the start time
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string StartTime
        { get; set; }

        /// <summary>
        /// Gets or sets the end time
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string EndTime
        { get; set; }

        /// <summary>
        /// Gets or sets whether or not the item is recurring
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public bool Recurring
        { get; set; }

        /// <summary>
        /// Gets or sets the recurrence type
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int RecurrenceType
        { get; set; }

        /// <summary>
        /// Gets or sets the recurrence range
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int RecurrenceRange
        { get; set; }

        /// <summary>
        /// Gets or sets the recurrence start time
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string RecurrenceStart
        { get; set; }

        /// <summary>
        /// Gets or sets the recurrence end time
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string RecurrenceEnd
        { get; set; }

        /// <summary>
        /// Gets or sets the recurrence occurence count
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int RecurrenceOccurenceCount
        { get; set; }

        /// <summary>
        /// Gets or sets the recurrence periodicity
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int RecurrencePeriodicity
        { get; set; }

        /// <summary>
        /// Gets or sets the recurrence day number
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int RecurrenceDayNumber
        { get; set; }

        /// <summary>
        /// Gets or sets the recurrence week days
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int RecurrenceWeekDays
        { get; set; }

        /// <summary>
        /// Gets or sets the recurrence week of month
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int RecurrenceWeekOfMonth
        { get; set; }

        /// <summary>
        /// Gets or sets the recurrence month
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int RecurrenceMonth
        { get; set; }

        /// <summary>
        /// Gets or sets the recurrence index
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int RecurrenceIndex
        { get; set; }

        /// <summary>
        /// Gets or sets the type 
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int Type
        { get; set; }

        /// <summary>
        /// Gets or sets parent ui schedule item occurrence id
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int ParentUIScheduleItemOccurrenceId
        { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns></returns>
        public UIScheduleItemOccurrence Clone()
        {
            return this.Clone<UIScheduleItemOccurrence>();
        }

        #endregion
    }
}
