﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Obymobi.Attributes;

namespace Obymobi.Logic.Model
{
    /// <summary>
    /// Model class which represents a media culture
    /// </summary>
    [Serializable, XmlRootAttribute(ElementName = "MediaCulture"), IncludeInCodeGeneratorForAndroid, IncludeInCodeGeneratorForMobile, IncludeInCodeGeneratorForXamarin]
    public class MediaCulture : ModelBase
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Obymobi.Logic.Model.MediaCulture type
        /// </summary>
        public MediaCulture()
        {
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the id of the media culture
        /// </summary>
        [XmlElement, PrimaryKeyFieldOfModel]
        [IncludeInCodeGeneratorForAndroid]
        [IncludeInCodeGeneratorForMobile]
        [IncludeInCodeGeneratorForXamarin]
        public int MediaCultureId
        { get; set; }

        /// <summary>
        /// Gets or sets the id of the media
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        [IncludeInCodeGeneratorForMobile]
        [IncludeInCodeGeneratorForXamarin]
        public int MediaId
        { get; set; }

        /// <summary>
        /// Parent of this media culture
        /// </summary>
        [XmlAttribute("Parent")]
        public string Parent
        { get; set; }

        /// <summary>
        /// Gets or sets the culture code
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        [IncludeInCodeGeneratorForMobile]
        [IncludeInCodeGeneratorForXamarin]
        public string CultureCode
        { get; set; }

        /// <summary>
        /// Gets or sets the name of the media culture
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        [IncludeInCodeGeneratorForMobile]
        [IncludeInCodeGeneratorForXamarin]
        public string Name
        { get; set; }

        /// <summary>
        /// Gets or sets the localized name of the media culture
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForMobile]
        [IncludeInCodeGeneratorForXamarin]
        public string LocalizedName
        { get; set; }        

        #endregion

        #region Methods

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns></returns>
        public MediaCulture Clone()
        {
            return this.Clone<MediaCulture>();
        }

        #endregion
    }
}
