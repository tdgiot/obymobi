﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Obymobi.Attributes;

namespace Obymobi.Logic.Model
{
    /// <summary>
    /// Model class which represents a social media item
    /// </summary>
    [Serializable, XmlRootAttribute(ElementName = "Socialmedia"), IncludeInCodeGeneratorForMobile]
    public class Socialmedia : ModelBase
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Obymobi.Logic.Model.SocialMedia type
        /// </summary>
        public Socialmedia()
        {
        }

        #endregion

        #region Xml Properties

        /// <summary>
        /// Gets or sets the id of the social media item
        /// </summary>
        [XmlElement, PrimaryKeyFieldOfModel]
        [IncludeInCodeGeneratorForMobile]
        public int SocialmediaId
        { get; set; }

        /// <summary>
        /// Gets or sets the id of the social media item
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForMobile]
        public int SocialmediaType
        { get; set; }

        /// <summary>
        /// Gets or sets the id of the company social media
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForMobile]
        public int CompanySocialmediaId
        { get; set; }

        /// <summary>
        /// Gets or sets the name of the social media item
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        [IncludeInCodeGeneratorForMobile]
        public string Name
        { get; set; }

        /// <summary>
        /// Gets or sets the title
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForMobile]
        public string Title
        { get; set; }

        /// <summary>
        /// Gets or sets the subtitle
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForMobile]
        public string Subtitle
        { get; set; }

        /// <summary>
        /// Gets or sets the field value
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForMobile]
        public string FieldValue1
        { get; set; }

        /// <summary>
        /// Gets or sets the field value
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForMobile]
        public string FieldValue2
        { get; set; }

        /// <summary>
        /// Gets or sets the field value
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForMobile]
        public string FieldValue3
        { get; set; }

        /// <summary>
        /// Gets or sets the field value
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForMobile]
        public string FieldValue4
        { get; set; }

        /// <summary>
        /// Gets or sets the field value
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForMobile]
        public string FieldValue5
        { get; set; }

        /// <summary>
        /// Gets or sets the field value
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForMobile]
        public string FieldValue6
        { get; set; }

        /// <summary>
        /// Gets or sets the field value
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForMobile]
        public string FieldValue7
        { get; set; }

        /// <summary>
        /// Gets or sets the field value
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForMobile]
        public string FieldValue8
        { get; set; }

        /// <summary>
        /// Gets or sets the field value
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForMobile]
        public string FieldValue9
        { get; set; }

        /// <summary>
        /// Gets or sets the field value
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForMobile]
        public string FieldValue10
        { get; set; }

        /// <summary>
        /// Gets or sets the livestream property
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForMobile]
        public bool Livestream
        { get; set; }

        /// <summary>
        /// Gets or sets the saving title
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForMobile]
        public string SavingTitle
        { get; set; }

        /// <summary>
        /// Gets or sets the saving message
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForMobile]
        public string SavingMessage
        { get; set; }

        /// <summary>
        /// Gets or sets the processed title
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForMobile]
        public string ProcessedTitle
        { get; set; }

        /// <summary>
        /// Gets or sets the processed message
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForMobile]
        public string ProcessedMessage
        { get; set; }

        /// <summary>
        /// Gets or sets the translations
        /// </summary>
        [IncludeInCodeGeneratorForMobile]
        [XmlArray("SocialmediaLanguages")]
        [XmlArrayItem("SocialmediaLanguage")]
        public SocialmediaLanguage[] SocialmediaLanguages
        { get; set; }

        /// <summary>
        /// Gets or sets the media of the social media
        /// </summary>
        [XmlArray("Media")]
        [XmlArrayItem("Media")]
        [IncludeInCodeGeneratorForMobile]
        public Media[] Media
        { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns></returns>
        public Socialmedia Clone()
        {
            return this.Clone<Socialmedia>();
        }

        #endregion
    }
}
