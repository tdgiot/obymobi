﻿using System;
using System.Xml.Serialization;
using Obymobi.Enums;

namespace Obymobi.Logic.Model.Weather
{
    [Serializable]
    public class WeatherItem
    {
        [XmlElement]
        public DateTime Date { get; set; }

        [XmlElement]
        public double Temperature { get; set; }
        
        [XmlElement]
        public double TemperatureNight { get; set; }
        
        [XmlElement]
        public double TemperatureMin { get; set; }
        
        [XmlElement]
        public double TemperatureMax { get; set; }

        [XmlElement]
        public WeatherType WeatherType { get; set; }
    }
}
