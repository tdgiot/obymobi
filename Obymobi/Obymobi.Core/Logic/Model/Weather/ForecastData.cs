﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace Obymobi.Logic.Model.Weather
{
    [Serializable]
    public class ForecastData
    {
        public ForecastData()
        {
            Forecast = new List<WeatherItem>();
        }

        [XmlArray("Forecast")]
        [XmlArrayItem("WeatherItem")]
        public List<WeatherItem> Forecast { get; set; }
    }
}
