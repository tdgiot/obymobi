﻿using System;
using System.Xml.Serialization;

namespace Obymobi.Logic.Model.Weather
{
    [Serializable]
    public class Weather
    {
        [XmlElement]
        public string City { get; set; }

        [XmlElement]
        public DateTime Date { get; set; }

        [XmlElement]
        public WeatherItem Current { get; set; }

        [XmlElement]
        public ForecastData Forecast { get; set; }
    }
}
