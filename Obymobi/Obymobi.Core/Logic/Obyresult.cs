﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Obymobi.Enums;

namespace Obymobi.Logic
{
    /// <summary>
    /// Obyresult class
    /// </summary>
    public class Obyresult
    {
        /// <summary>
        /// Obyresult code
        /// </summary>
        public int code = -1;
        /// <summary>
        /// Obyresult message
        /// </summary>
        public string message = string.Empty;
        /// <summary>
        /// Obyresult data
        /// </summary>
        public string data = string.Empty;

        /// <summary>
        /// Initializes a new instance of the <see cref="Obyresult"/> class.
        /// </summary>
        public Obyresult()
        {
        }

        /// <summary>
        /// Creates the obyresult.
        /// </summary>
        /// <returns></returns>
        public static Obyresult CreateObyresult()
        {
            Obyresult result = new Obyresult();
            result.code = (int)GenericWebserviceCallResult.Success;
			result.message = GenericWebserviceCallResult.Success.ToString();
            result.data = string.Empty;
            return result;
        }

        /// <summary>
        /// Creates the obyresult.
        /// </summary>
        /// <param name="code">The code.</param>
        /// <param name="message">The message.</param>
        /// <param name="data">The data.</param>
        /// <returns></returns>
        public static Obyresult CreateObyresult(int code, string message, string data)
        {
            Obyresult result = new Obyresult();
            result.code = code;
            result.message = message;
            result.data = data;
            return result;
        }
    }
}
