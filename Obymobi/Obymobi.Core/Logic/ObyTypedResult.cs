﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Obymobi.Logic.HelperClasses;
using Obymobi.Enums;
using System.Xml.Serialization;
using Dionysos.Reflection;

namespace Obymobi.Logic
{
    /// <summary>
    /// Obyresult class meant to transfer typed model(s).
    /// </summary>
    /// <typeparam name="T"></typeparam>
    [Serializable]
    public class ObyTypedResult<T> : object
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ObyTypedResult"/> class.
        /// </summary>
        public ObyTypedResult()
        {
            this.SetResult(GenericWebserviceCallResult.Success);
        }

        #region Properties
        private string resultType;
        private string resultMessage;
        private int resultCode;
    	private Type resultTypeObj;
        private List<T> modelCollection = new List<T>();

        /// <summary>
        /// Gets or sets the modelCollection
        /// </summary>
        public List<T> ModelCollection
        {
            get
            {
                return this.modelCollection;
            }
            set
            {
                this.modelCollection = value;
            }
        }

        /// <summary>
        /// Gets or sets the result state of the call
        /// </summary>			
        public void SetResult(Enum value)
        {
            this.resultMessage = value.ToString();
            this.resultType = value.GetType().ToString();
            this.resultCode = Convert.ToInt32(value);
        	this.resultTypeObj = value.GetType();
        }

        /// <summary>
        /// Sets the result state of a call from ObyombiException
        /// </summary>
        /// <param name="ex"></param>
        public void SetResult(ObymobiException ex)
        {
            this.resultMessage = ex.ErrorEnumValue + " - " + ex.BaseMessage;
            this.resultType = ex.ErrorEnumType;
            this.resultCode = ex.ErrorEnumValueInt;
            this.resultTypeObj = ex.ErrorEnumValue.GetType();
        }

        /// <summary>
        /// Set result from Enum and regular exception. Result message will be a combination of enum value and exception message.
        /// </summary>
        /// <param name="value"></param>
        /// <param name="ex"></param>
        public void SetResult(Enum value, Exception ex)
        {
            this.resultMessage = value + " - " + ex.Message;
            this.resultType = value.GetType().ToString();
            this.resultCode = Convert.ToInt32(value);
            this.resultTypeObj = value.GetType();
        }

        /// <summary>
        /// Set result values. (Used by WebserviceTester)
        /// </summary>
        /// <param name="resultType"></param>
        /// <param name="resultMessage"></param>
        /// <param name="resultCode"></param>
        public void SetResult(string resultMessage, string resultType, int resultCode)
        {
            this.resultMessage = resultMessage;
            this.resultType = resultType;
            this.resultCode = resultCode;
        }

        /// <summary>
        /// Gets the result.
        /// </summary>
        /// <returns></returns>
        public Enum GetResult()
        {
			return (Enum)Enum.Parse(this.resultTypeObj, this.resultMessage);
        }

        /// <summary>
        /// The type of the enum that reflects the Result (do not use setter!)
        /// <remarks>
        /// The SET part of this property is only made available for serialization purposes, it should not be used in code.
        /// </remarks>
        /// </summary>
        [XmlElement]
        public string ResultType
        {
            get
            {
                return this.resultType;
            }
            set // Dont remove this setter as the XML element will get lost
            {
                throw new Dionysos.TechnicalException("ObyTypedResult.ResultType cannot be set, use .SetResult()");
            }
        }

        /// <summary>
        /// The value of the enum that reflects the Result (do not use setter!)
        /// <remarks>
        /// The SET part of this property is only made available for serialization purposes, it should not be used in code.
        /// </remarks>
        /// </summary>
        [XmlElement]
        public string ResultMessage
        {
            get
            {
                return this.resultMessage;
            }
            set // Dont remove this setter as the XML element will get lost
            {
                throw new Dionysos.TechnicalException("ObyTypedResult.ResultType cannot be set, use .SetResult()");
            }
        }

        /// <summary>
        /// The value of the enum that reflects the Result (do not use setter!)
        /// <remarks>
        /// The SET part of this property is only made available for serialization purposes, it should not be used in code.
        /// </remarks>
        /// </summary>
        [XmlElement]
        public int ResultCode
        {
            get
            {
                return this.resultCode;
            }
            set // Dont remove this setter as the XML element will get lost
            {
                throw new Dionysos.TechnicalException("ObyTypedResult.ResultType cannot be set, use .SetResult()");
            }
        }

        /// <summary>
        /// Convert instance to a non-typed Obyresult
        /// </summary>
        /// <returns></returns>
        public Obyresult ToObyresult()
        {
            Obyresult result = new Obyresult();
            result.code = this.resultCode;
            result.message = this.resultMessage;
            result.data = XmlHelper.Serialize(this.ModelCollection.ToArray());
            return result;
        }

        public void CopyFromResult(object source)
        {
            if (Member.HasProperty(source, "ResultCode"))
                this.resultCode = (int)Member.InvokeProperty(source, "ResultCode");
            if (Member.HasProperty(source, "ResultMessage"))
                this.resultMessage = (string)Member.InvokeProperty(source, "ResultMessage");
            if (Member.HasProperty(source, "ResultType"))
                this.resultType = (string)Member.InvokeProperty(source, "ResultType");
        }

        #endregion
    }
}
