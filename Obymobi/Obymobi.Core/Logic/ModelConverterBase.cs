﻿using Dionysos;
using Obymobi.Enums;
using Obymobi.Logic.HelperClasses;
using Obymobi.Logic.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;
using Dionysos.Reflection;

namespace Obymobi.Logic
{
    public abstract class ModelConverterBase<LegacyModelType, CurrentModelType> where LegacyModelType : ModelBase where CurrentModelType : ModelBase
    {
        #region Fields

        private List<string> fieldsToExclude = new List<string>();
        private Dictionary<string, object> fieldValues = new Dictionary<string, object>();

        #endregion

        #region Constructors

        public ModelConverterBase()
        {
        }

        #endregion

        #region Methods

        #region Current to legacy

        public ObyTypedResult<LegacyModelType> ConvertResultToLegacyResult(ObyTypedResult<CurrentModelType> result)
        {
            ObyTypedResult<LegacyModelType> legacyResult = new ObyTypedResult<LegacyModelType>();

            if (result != null)
            {
                legacyResult.CopyFromResult(result);

                if (result.ModelCollection != null && result.ModelCollection.Count > 0)
                {
                    List<LegacyModelType> legacyModelCollection = new List<LegacyModelType>();

                    foreach (CurrentModelType model in result.ModelCollection)
                    {
                        if (model != null)
                        {
                            // Convert the model into a legacy model
                            LegacyModelType legacyModel = this.ConvertModelToLegacyModel(model);
                            if (legacyModel != null)
                                legacyModelCollection.Add(legacyModel);
                        }
                    }

                    legacyResult.ModelCollection = legacyModelCollection;
                }
            }

            return legacyResult;
        }

        public IList<LegacyModelType> ConvertArrayToLegacyArray(IList<CurrentModelType> currentModelList)
        {
            List<LegacyModelType> legacyModelList = new List<LegacyModelType>();

            if (currentModelList != null && currentModelList.Count > 0)
            {
                foreach (CurrentModelType model in currentModelList)
                {
                    if (model != null)
                    {
                        // Convert the model into a legacy model
                        LegacyModelType legacyModel = this.ConvertModelToLegacyModel(model);
                        if (legacyModel != null)
                            legacyModelList.Add(legacyModel);
                    }
                }
            }

            return legacyModelList.ToArray();
        }

        public abstract LegacyModelType ConvertModelToLegacyModel(CurrentModelType model);

        #endregion

        #region Legacy to current

        public ObyTypedResult<CurrentModelType> ConvertLegacyResultToResult(ObyTypedResult<LegacyModelType> result)
        {
            ObyTypedResult<CurrentModelType> currentResult = new ObyTypedResult<CurrentModelType>();

            if (result != null)
            {
                currentResult.CopyFromResult(result);

                if (result.ModelCollection != null && result.ModelCollection.Count > 0)
                {
                    List<CurrentModelType> currentModelCollection = new List<CurrentModelType>();

                    foreach (LegacyModelType model in result.ModelCollection)
                    {
                        if (model != null)
                        {
                            // Convert the legacy model into a current model
                            CurrentModelType currentModel = this.ConvertLegacyModelToModel(model);

                            if (currentModel != null)
                                currentModelCollection.Add(currentModel);
                        }
                    }

                    currentResult.ModelCollection = currentModelCollection;
                }
            }

            return currentResult;
        }

        public string ConvertLegacyXmlToModelXml(string xml)
        {
            LegacyModelType legacyModel = XmlHelper.Deserialize<LegacyModelType>(xml);
            CurrentModelType model = ConvertLegacyModelToModel(legacyModel);
            return XmlHelper.Serialize(model);
        }

        public CurrentModelType ConvertLegacyXmlToModel(string xml)
        {
            LegacyModelType legacyModel = XmlHelper.Deserialize<LegacyModelType>(xml);
            return ConvertLegacyModelToModel(legacyModel);
        }

        public CurrentModelType[] ConvertLegacyXmlArrayToModelArray(string xml)
        {
            LegacyModelType[] legacyArray = XmlHelper.Deserialize<LegacyModelType[]>(xml);
            return (CurrentModelType[])ConvertLegacyArrayToArray(legacyArray);
        }

        public IList<CurrentModelType> ConvertLegacyArrayToArray(IList<LegacyModelType> legacyModelList)
        {
            List<CurrentModelType> modelList = new List<CurrentModelType>();

            if (legacyModelList != null && legacyModelList.Count > 0)
            {
                foreach (LegacyModelType legacyModel in legacyModelList)
                {
                    // Convert the legacy model into a current model
                    CurrentModelType model = this.ConvertLegacyModelToModel(legacyModel);
                    modelList.Add(model);
                }
            }

            return modelList.ToArray();
        }

        public abstract CurrentModelType ConvertLegacyModelToModel(LegacyModelType legacyModel);

        public void CopyFieldsToModel(LegacyModelType source, CurrentModelType target)
        {
            var sourceProperties = TypeDescriptor.GetProperties(source);
            var targetProperties = TypeDescriptor.GetProperties(target);

            List<string> additionalPropertiesOnTarget = new List<string>();
            List<string> typeMismatchPropertiesOnTarget = new List<string>();
            List<string> typeMismatchFieldValues = new List<string>();

            foreach (PropertyDescriptor targetProperty in targetProperties)
            {
                // Check if the target property also exists on the source
                PropertyDescriptor sourceProperty = sourceProperties[targetProperty.Name];
                if (this.fieldsToExclude.Contains(targetProperty.Name))
                {
                    // Field is excluded
                }                
                else if (sourceProperty != null)
                {
                    // Target property exists on source
                    if (targetProperty.PropertyType.Equals(sourceProperty.PropertyType))                                    // Check if the types match
                    {
                        targetProperty.SetValue(target, sourceProperty.GetValue(source));
                    }
                    else if (ModelHelper.Models.Keys.Contains(targetProperty.PropertyType.Name.Replace("[]", string.Empty))) // Check if we have a complex type
                    {
                        string propertyType = targetProperty.PropertyType.Name;
                        string modelName = propertyType.Replace("[]", string.Empty);
                        string version = sourceProperty.PropertyType.FullName.Substring(sourceProperty.PropertyType.FullName.IndexOf(".v")+2);
                        version = version.Substring(0, version.IndexOf("."));

                        Assembly legacy = Global.AssemblyInfo["Legacy"].Assembly;

                        Type converterType = legacy.GetType(string.Format("Obymobi.Logic.Model.v{0}.Converters.{1}Converter", version, modelName));

                        object converter = InstanceFactory.CreateInstance(legacy, converterType);
                        if (converter != null)
                        {
                            object result = null;
                            if (propertyType.Contains("[]"))
                                result = Member.InvokeMethod(converter, "ConvertLegacyArrayToArray", new object[] {sourceProperty.GetValue(source)});
                            else
                                result = Member.InvokeMethod(converter, "ConvertLegacyModelToModel", new object[] { sourceProperty.GetValue(source) });

                            if (result != null)
                                targetProperty.SetValue(target, result);
                        }
                    }
                    else if (this.fieldValues.Keys.Contains(targetProperty.Name))                                           // Check if we have a custom field value for this field
                    {
                        if (targetProperty.PropertyType.Equals(this.fieldValues[targetProperty.Name].GetType()))
                            targetProperty.SetValue(target, this.fieldValues[targetProperty.Name]);
                        else
                            typeMismatchFieldValues.Add(targetProperty.Name);
                    }
                    else
                    {
                        typeMismatchPropertiesOnTarget.Add(targetProperty.Name);
                    }
                }                
                else if (this.fieldValues.Keys.Contains(targetProperty.Name))                                               // Check if we have a custom field value for this field
                {
                    if (targetProperty.PropertyType.Equals(this.fieldValues[targetProperty.Name].GetType()))
                        targetProperty.SetValue(target, this.fieldValues[targetProperty.Name]);
                    else
                        typeMismatchFieldValues.Add(targetProperty.Name);
                }
                else
                {
                    // Target property does not exist on source
                    additionalPropertiesOnTarget.Add(targetProperty.Name);
                }
            }

            if (additionalPropertiesOnTarget.Count > 0 || typeMismatchPropertiesOnTarget.Count > 0 || typeMismatchFieldValues.Count > 0)
            {
                string message = string.Format("The legacy model of type '{0}' could not be converted to model of type '{1}' due to the following conversion errors:", typeof(LegacyModelType).FullName, typeof(CurrentModelType).FullName);

                if (additionalPropertiesOnTarget.Count > 0)
                {
                    // GK Changed message, since I think it was unclear, unless I didn't understand te message (proving my point ;)).
                    //message += string.Format("\r\n\r\nThe following properties from type '{0}' could not be set on type '{1}' because they don't exist and those properties are not excluded using the 'FieldToExclude' property in the converter class:\r\n", typeof(LegacyModelType).FullName, typeof(CurrentModelType).FullName);
                    message += string.Format("\r\n\r\nThe following properties of the type '{1}' (current version) could not be read from '{0}' because they don't exist in the old model and those properties are not excluded using the 'FieldToExclude' property in the converter class:\r\n", typeof(LegacyModelType).FullName, typeof(CurrentModelType).FullName);
                    for (int i = 0; i < additionalPropertiesOnTarget.Count; i++)
                    {
                        message += additionalPropertiesOnTarget[i];

                        if (i != (additionalPropertiesOnTarget.Count - 1))
                            message += ", ";
                    }
                }

                if (typeMismatchPropertiesOnTarget.Count > 0)
                {
                    message += string.Format("\r\n\r\nThe following properties from type '{0}' could not be set on type '{1}' because they don't have the same type and those properties are not set using the 'FieldValues' property in the converter class:\r\n", typeof(LegacyModelType).FullName, typeof(CurrentModelType).FullName);
                    for (int i = 0; i < typeMismatchPropertiesOnTarget.Count; i++)
                    {
                        message += typeMismatchPropertiesOnTarget[i];

                        if (i != (typeMismatchPropertiesOnTarget.Count - 1))
                            message += ", ";
                    }
                }

                if (typeMismatchFieldValues.Count > 0)
                {
                    message += string.Format("\r\n\r\nThe following field values could not be set on type '{0}' because they don't have the same type specified using the 'FieldValues' property in the converter class:\r\n", typeof(CurrentModelType).FullName);
                    for (int i = 0; i < typeMismatchFieldValues.Count; i++)
                    {
                        message += typeMismatchFieldValues[i];

                        if (i != (typeMismatchFieldValues.Count - 1))
                            message += ", ";
                    }
                }

                throw new ObymobiException(GenericWebserviceCallResult.ModelConversionError, message);
            }
        }

        public abstract void CopyDefaultFields(List<string> fieldsToExclude, Dictionary<string, object> fieldValues);

        #endregion

        #endregion

        #region Properties

        public List<string> FieldsToExclude
        {
            get
            {
                return this.fieldsToExclude;
            }
            set
            {
                this.fieldsToExclude = value;
            }
        }

        public Dictionary<string, object> FieldValues
        {
            get
            {
                return this.fieldValues;
            }
            set
            {
                this.fieldValues = value;
            }
        }

        #endregion
    }
}
