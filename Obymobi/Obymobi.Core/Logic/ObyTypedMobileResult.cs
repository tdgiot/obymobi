﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Obymobi.Logic.HelperClasses;
using Obymobi.Enums;
using System.Xml.Serialization;
using Dionysos;
using Dionysos.Reflection;

namespace Obymobi.Logic
{
    /// <summary>
    /// Obyresult class meant to transfer typed model(s).
    /// </summary>
    /// <typeparam name="T"></typeparam>
    [Serializable]
    public class ObyTypedMobileResult<T> : object
    {
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="ObyTypedMobileResult"/> class.
        /// </summary>
        public ObyTypedMobileResult()
        {
            this.SetResult(GenericWebserviceCallResult.Success);
        }

        #endregion

        #region Fields

        private string resultType;
        private string resultMessage;
        private int resultCode;
        private int authenticationResultCode = (int)ValidateCredentialsResult.Unknown;
        private Type resultTypeObj;
        private string innerResultType;
        private string innerResultMessage;
        private int innerResultCode;
        private Type innerResultTypeObj;
        private List<T> modelCollection = new List<T>();
        private string additionalMessage = String.Empty;

        #endregion

        #region Methods

        /// <summary>
        /// Gets or sets the result state of the call
        /// </summary>			
        public void SetResult(Enum resultValue)
        {
            this.resultMessage = resultValue.ToString();
            this.resultType = resultValue.GetType().ToString();
            this.resultCode = Convert.ToInt32(resultValue);
            this.resultTypeObj = resultValue.GetType();
        }

        /// <summary>
        /// Gets or sets the result state of the call
        /// </summary>			
        public void SetResult(Enum resultValue, ValidateCredentialsResult authenticationResult)
        {
            this.resultMessage = resultValue.ToString();
            this.resultType = resultValue.GetType().ToString();
            this.resultCode = Convert.ToInt32(resultValue);
            this.resultTypeObj = resultValue.GetType();
            this.authenticationResultCode = (int)authenticationResult;
        }

        /// <summary>
        /// Gets or sets the result state of the call
        /// </summary>			
        public void SetResult(Enum value, string message)
        {
            this.resultMessage = value.ToString() + " - " + message;
            this.resultType = value.GetType().ToString();
            this.resultCode = Convert.ToInt32(value);
            this.resultTypeObj = value.GetType();
        }

        /// <summary>
        /// Sets the result state of a call from ObyombiException
        /// </summary>
        /// <param name="ex"></param>
        public void SetResult(ObymobiException ex)
        {
            // GK Little experiment, was ex.BaseMessage. To see if that gives a bit more detail (but not too much) 
            // couldn't test for all errors, so if you think something gives too much information discuss with GK/MB
            // how to move forward from there.
            // this.resultMessage = ex.ErrorEnumValue + " - " + ex.BaseMessage; 
            this.resultMessage = ex.ErrorEnumValue + " - " + ex.Message; 
            this.resultType = ex.ErrorEnumType;
            this.resultCode = ex.ErrorEnumValueInt;
            this.resultTypeObj = ex.ErrorEnumValue.GetType();
            this.additionalMessage = ex.AdditionalMessage;
        }

        /// <summary>
        /// Set result values. (Used by WebserviceTester)
        /// </summary>
        /// <param name="resultType"></param>
        /// <param name="resultMessage"></param>
        /// <param name="resultCode"></param>
        public void SetResult(string resultMessage, string resultType, int resultCode, string innerResultMessage, string innerResultType, int innerResultCode)
        {
            this.resultMessage = resultMessage;
            this.resultType = resultType;
            this.resultCode = resultCode;
            this.innerResultMessage = innerResultMessage;
            this.innerResultType = innerResultType;
            this.innerResultCode = innerResultCode;            
        }

        /// <summary>
        /// Gets the result.
        /// </summary>
        /// <returns></returns>
        public Enum GetResult()
        {
            return (Enum)Enum.Parse(this.resultTypeObj, this.resultMessage);
        }

        /// <summary>
        /// Gets or sets the result state of the call
        /// </summary>			
        public void SetInnerResult(Enum value)
        {
            this.innerResultMessage = value.ToString();
            this.innerResultType = value.GetType().ToString();
            this.innerResultCode = Convert.ToInt32(value);
            this.innerResultTypeObj = value.GetType();
        }

        /// <summary>
        /// Gets or sets the result state of the call
        /// </summary>			
        public void SetInnerResult(Enum value, string message)
        {
            this.innerResultMessage = value.ToString() + " - " + message;
            this.innerResultType = value.GetType().ToString();
            this.innerResultCode = Convert.ToInt32(value);
            this.innerResultTypeObj = value.GetType();
        }

        /// <summary>
        /// Sets the result state of a call from ObyombiException
        /// </summary>
        /// <param name="ex"></param>
        public void SetInnerResult(ObymobiException ex)
        {
            this.innerResultMessage = ex.ErrorEnumValue + " - " + ex.BaseMessage;
            this.innerResultType = ex.ErrorEnumType;
            this.innerResultCode = ex.ErrorEnumValueInt;
            this.innerResultTypeObj = ex.ErrorEnumValue.GetType();

            if(ex.ErrorEnumValue is ValidateCredentialsResult &&
                ex.ErrorEnumValueInt == (int)ValidateCredentialsResult.TimestampIsOutOfRange)
            {
                this.innerResultMessage += string.Format("[[TIME-{0}]]", DateTimeUtil.ToUnixTime(DateTime.UtcNow));
            }
        }

        /// <summary>
        /// Gets the result.
        /// </summary>
        /// <returns></returns>
        public Enum GetInnerResult()
        {
            return (Enum)Enum.Parse(this.innerResultTypeObj, this.innerResultMessage);
        }

        /// <summary>
        /// Convert instance to a non-typed Obyresult
        /// </summary>
        /// <returns></returns>
        public Obyresult ToObyresult()
        {
            Obyresult result = new Obyresult();
            result.code = this.resultCode;
            result.message = this.resultMessage;
            result.data = XmlHelper.Serialize(this.ModelCollection.ToArray());
            return result;
        }

        public void CopyFromResult(object source)
        {
            if (Member.HasProperty(source, "ResultCode"))
                this.resultCode = (int)Member.InvokeProperty(source, "ResultCode");
            if (Member.HasProperty(source, "ResultMessage"))
                this.resultMessage = (string)Member.InvokeProperty(source, "ResultMessage");
            if (Member.HasProperty(source, "ResultType"))
                this.resultType = (string)Member.InvokeProperty(source, "ResultType");
            if (Member.HasProperty(source, "InnerResultCode"))
                this.innerResultCode = (int)Member.InvokeProperty(source, "InnerResultCode");
            if (Member.HasProperty(source, "InnerResultMessage"))
                this.innerResultMessage = (string)Member.InvokeProperty(source, "InnerResultMessage");
            if (Member.HasProperty(source, "InnerResultType"))
                this.innerResultType = (string)Member.InvokeProperty(source, "InnerResultType");
            if (Member.HasProperty(source, "AdditionalMessage"))
                this.additionalMessage = (string)Member.InvokeProperty(source, "AdditionalMessage");
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the modelCollection
        /// </summary>
        public List<T> ModelCollection
        {
            get
            {
                return this.modelCollection;
            }
            set
            {
                this.modelCollection = value;
            }
        }

        /// <summary>
        /// The type of the enum that reflects the Result (do not use setter!)
        /// <remarks>
        /// The SET part of this property is only made available for serialization purposes, it should not be used in code.
        /// </remarks>
        /// </summary>
        [XmlElement]
        public string ResultType
        {
            get
            {
                return this.resultType;
            }
            set // Dont remove this setter as the XML element will get lost
            {
                throw new Dionysos.TechnicalException("ObyTypedResult.ResultType cannot be set, use .SetResult()");
            }
        }

        /// <summary>
        /// The value of the enum that reflects the Result (do not use setter!)
        /// <remarks>
        /// The SET part of this property is only made available for serialization purposes, it should not be used in code.
        /// </remarks>
        /// </summary>
        [XmlElement]
        public string ResultMessage
        {
            get
            {
                return this.resultMessage;
            }
            set // Dont remove this setter as the XML element will get lost
            {
                throw new Dionysos.TechnicalException("ObyTypedResult.ResultMessage cannot be set, use .SetResult()");
            }
        }

        /// <summary>
        /// The value of the enum that reflects the Result (do not use setter!)
        /// <remarks>
        /// The SET part of this property is only made available for serialization purposes, it should not be used in code.
        /// </remarks>
        /// </summary>
        [XmlElement]
        public int ResultCode
        {
            get
            {
                return this.resultCode;
            }
            set // Dont remove this setter as the XML element will get lost
            {
                throw new Dionysos.TechnicalException("ObyTypedResult.ResultCode cannot be set, use .SetResult()");
            }
        }

        /// <summary>
        /// The value of the enum that reflects the Result (do not use setter!)
        /// <remarks>
        /// The SET part of this property is only made available for serialization purposes, it should not be used in code.
        /// </remarks>
        /// </summary>
        [XmlElement]
        public int AuthenticationResultCode
        {
            get
            {
                return this.authenticationResultCode;
            }
            set // Dont remove this setter as the XML element will get lost
            {
                this.authenticationResultCode = value;
            }
        }

        /// <summary>
        /// The type of the enum that reflects the Result (do not use setter!)
        /// <remarks>
        /// The SET part of this property is only made available for serialization purposes, it should not be used in code.
        /// </remarks>
        /// </summary>
        [XmlElement]
        public string InnerResultType
        {
            get
            {
                return this.innerResultType;
            }
            set // Dont remove this setter as the XML element will get lost
            {
                throw new Dionysos.TechnicalException("ObyTypedResult.InnerResultType cannot be set, use .SetInnerResult()");
            }
        }

        /// <summary>
        /// The value of the enum that reflects the Result (do not use setter!)
        /// <remarks>
        /// The SET part of this property is only made available for serialization purposes, it should not be used in code.
        /// </remarks>
        /// </summary>
        [XmlElement]
        public string InnerResultMessage
        {
            get
            {
                return this.innerResultMessage;
            }
            set // Dont remove this setter as the XML element will get lost
            {
                throw new Dionysos.TechnicalException("ObyTypedResult.InnerResultMessage cannot be set, use .SetInnerResult()");
            }
        }

        /// <summary>
        /// The value of the enum that reflects the Result (do not use setter!)
        /// <remarks>
        /// The SET part of this property is only made available for serialization purposes, it should not be used in code.
        /// </remarks>
        /// </summary>
        [XmlElement]
        public int InnerResultCode
        {
            get
            {
                return this.innerResultCode;
            }
            set // Dont remove this setter as the XML element will get lost
            {
                throw new Dionysos.TechnicalException("ObyTypedResult.InnerResultCode cannot be set, use .SetInnerResult()");
            }
        }

        /// <summary>
        /// The value of the additional message that reflects the Result (do not use setter!)
        /// <remarks>
        /// The SET part of this property is only made available for serialization purposes, it should not be used in code.
        /// </remarks>
        /// </summary>
        [XmlElement]
        public string AdditionalMessage
        {
            get
            {
                return this.additionalMessage;
            }
            set // Dont remove this setter as the XML element will get lost
            {
                throw new Dionysos.TechnicalException("ObyTypedResult.AdditionalMessage cannot be set");
            }
        }

        #endregion
    }
}


