﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.Interfaces
{
    public interface IPmsModel
    {
        string DeliverypointNumber 
        { get; set; }

        string Name { get; }
        string ToJson();
    }
}
