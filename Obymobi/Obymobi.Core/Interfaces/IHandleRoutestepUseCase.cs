﻿using Obymobi.Logic.Routing.Requests;

namespace Obymobi.Interfaces
{
    public interface IHandleRoutestepUseCase
    {
        void HandleRoutestep(IHandleRoutestepRequest request);
    }
}
