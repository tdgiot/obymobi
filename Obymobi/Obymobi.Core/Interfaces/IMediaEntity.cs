﻿using Obymobi.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.Interfaces
{
    public interface IMediaEntity
    {
        Nullable<System.Int64> LastDistributedVersionTicks { get; set; }
        Nullable<System.Int64> LastDistributedVersionTicksAmazon { get; set; }
        bool IsGeneric { get; }

        string GetFileName(FileNameType type);
        int? GetRelatedBrandId();
        int? GetRelatedCompanyId();
    }
}
