﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.Interfaces
{
    public interface INocActivityEntity
    {
        DateTime? LastActivityUTC { get; }
    }
}
