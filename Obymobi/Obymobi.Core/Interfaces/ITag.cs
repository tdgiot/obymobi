﻿namespace Obymobi.Interfaces
{
    public interface ITag
    {
        int TagId { get; set; }

        string Name { get; set; }

        int? CompanyId { get; set; }
    }
}
