﻿using System;
using Dionysos;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Obymobi.Enums;
using System.Diagnostics;
using SD.LLBLGen.Pro.ORMSupportClasses;
//using Dionysos.Diagnostics;

namespace Obymobi
{
    [Serializable]
    public class ObymobiEntityException : ObymobiException
    {
        /// <summary>
        /// Constructs an instance of the Lynx_media.TechnicalException class
        /// </summary>		
        /// <param name="errorEnumValue">Enum value of the error</param>		
        public ObymobiEntityException(Enum errorEnumValue)
            : base(errorEnumValue)
        {
            this.errorEnumValue = errorEnumValue;
        }

        /// <summary>
        /// Constructs an instance of the Lynx_media.TechnicalException class
        /// </summary>		
        /// <param name="errorEnumValue">Enum value of the error</param>		
        /// <param name="ex">Exception that was thrown and is represented by this Obymobi exception</param>		
        public ObymobiEntityException(Enum errorEnumValue, Exception ex)
            : base(errorEnumValue)
        {
            this.errorEnumValue = errorEnumValue;
            this.additionalInformation += string.Format("InnerException: {0}\r\n\r\n Inner Stack Trace: {1}", ex.Message, ex.StackTrace);
        }        

        public ObymobiEntityException(Enum errorEnumValue, IEntity entity)
            : this(errorEnumValue, entity, string.Empty)
        {
        }

        /// <summary>
        /// Constructs an instance of the Lynx_media.TechnicalException class
        /// </summary>
        /// <param name="errorEnumValue">Enum value of the error</param>
        /// <param name="entity">The entity for which this Exception is thrown.</param>
        public ObymobiEntityException(Enum errorEnumValue, IEntity entity, string message, params object[] args)
            : base(errorEnumValue, message.FormatSafe(args) + "\r\n" + StringUtil.FormatSafe("{0}: {1}", entity.LLBLGenProEntityName, entity.PrimaryKeyFields[0].CurrentValue))
        {
            this.errorEnumValue = errorEnumValue;

            // Add the PK field info
            StringBuilder additionalInfo = new StringBuilder();
            additionalInfo.AppendFormatLine("Entity Type: {0}", entity.LLBLGenProEntityName);
            additionalInfo.AppendFormatLine("PK value: {0}", entity.PrimaryKeyFields[0].CurrentValue);

            // Add all other fields that end with Id
            foreach (var field in entity.Fields)
            {
                additionalInfo.AppendFormatLine("FK Field {0}: {1}", field.Name, field.CurrentValue);
            }

            this.additionalInformation = additionalInfo.ToString();
        }

        /// <summary>
        /// Constructs an instance of the Lynx_media.TechnicalException class
        /// </summary>
        /// <param name="errorEnumValue">Enum value of the error</param>		
        /// <param name="message">string.Format style message</param>		
        /// <param name="args">Arguments to be used in the message parameter</param>		
        public ObymobiEntityException(Enum errorEnumValue, string message, params object[] args)
            : base(errorEnumValue, message.FormatSafe(args))
        {
            this.errorEnumValue = errorEnumValue;
            this.additionalInformation = StringUtil.FormatSafe(message, args);
        }

        /// <summary>
        /// Constructs an instance of the Lynx_media.TechnicalException class
        /// </summary>
        /// <param name="errorEnumValue">Enum value of the error</param>		
        /// <param name="ex">Exception that was thrown and is represented by this Obymobi exception</param>		
        /// <param name="message">string.Format style message</param>			
        /// <param name="args">Arguments to be used in the message parameter</param>		
        public ObymobiEntityException(Enum errorEnumValue, Exception ex, string message, params object[] args)
            : base(errorEnumValue, message.FormatSafe(args))
        {
            this.errorEnumValue = errorEnumValue;
            this.additionalInformation = message.FormatSafe(args);

            if (ex != null)
                this.additionalInformation += StringUtil.FormatSafe("\r\n\r\nInnerException: {0}\r\n\r\n Inner Stack Trace: {1}", ex.Message, ex.StackTrace);
        }

        /// <summary>
        /// Constructs an instance of the Lynx_media.TechnicalException class
        /// </summary>
        /// <param name="errorEnumValue">Enum value of the error</param>		
        /// <param name="message">string.Format style message</param>		
        /// <param name="args">Arguments to be used in the message parameter</param>		
        public ObymobiEntityException(Enum errorEnumValue, string xml)
            : base(errorEnumValue)
        {
            this.errorEnumValue = errorEnumValue;
            this.additionalInformation = xml;
        }

    }
}
