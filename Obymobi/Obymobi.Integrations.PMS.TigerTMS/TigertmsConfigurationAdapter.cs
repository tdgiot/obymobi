﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Obymobi.Integrations.Interfaces;
using Obymobi.Logic.Model;
using Obymobi.Data.EntityClasses;
using Obymobi.Integrations.PMS.Configuration;

namespace Obymobi.Integrations.PMS.Tigertms
{
    public class TigertmsConfigurationAdapter : IConfigurationAdapter
    {
        #region Methods
        /// <summary>
        /// Read configuration from a Terminal Model
        /// </summary>
        /// <param name="terminal"></param>
        public void ReadFromTerminalModel(Terminal terminal)
        {
            this.WebserviceToCallToUrl = terminal.PosValue1;
            this.WebserviceUserkey = terminal.PosValue2;
            this.WebserviceToBeCalledOnUrl = terminal.PosValue3;
            this.testMode = terminal.PosValue4;

        }

        /// <summary>
        /// Read configuration from a Terminal Entity
        /// </summary>
        /// <param name="terminal"></param>
        public void ReadFromTerminalEntity(TerminalEntity terminal)
        {
            this.WebserviceToCallToUrl = terminal.PosValue1;
            this.WebserviceUserkey = terminal.PosValue2;
            this.WebserviceToBeCalledOnUrl = terminal.PosValue3;
            this.testMode = terminal.PosValue4;
        }

        /// <summary>
        /// Read configuration from Configuration Manager
        /// </summary>
        public void ReadFromConfigurationManager()
        {
            this.WebserviceToCallToUrl = Dionysos.ConfigurationManager.GetString(PmsConfigurationConstants.TigertmsWebserviceToCallToUrl);
            this.WebserviceUserkey = Dionysos.ConfigurationManager.GetString(PmsConfigurationConstants.TigertmsWebserviceUserkey);
            this.WebserviceToBeCalledOnUrl = Dionysos.ConfigurationManager.GetString(PmsConfigurationConstants.TigertmsWebserviceToBeCalledOnUrl);
            this.testMode = Dionysos.ConfigurationManager.GetString(PmsConfigurationConstants.TigertmsWebserviceTestMode);

        }

        /// <summary>
        /// Write configuration to Configuration Manager
        /// </summary>
        public void WriteToConfigurationManager()
        {
            Dionysos.ConfigurationManager.SetValue(PmsConfigurationConstants.TigertmsWebserviceToCallToUrl, this.WebserviceToCallToUrl);
            Dionysos.ConfigurationManager.SetValue(PmsConfigurationConstants.TigertmsWebserviceUserkey, this.WebserviceUserkey);
            Dionysos.ConfigurationManager.SetValue(PmsConfigurationConstants.TigertmsWebserviceToBeCalledOnUrl, this.WebserviceToBeCalledOnUrl);
            Dionysos.ConfigurationManager.SetValue(PmsConfigurationConstants.TigertmsWebserviceTestMode, this.testMode);
        }

        /// <summary>
        /// Write configuration to Terminal Entity
        /// </summary>
        /// <param name="terminal"></param>
        public void WriteToTerminalEntity(Data.EntityClasses.TerminalEntity terminal)
        {
            terminal.PosValue1 = this.WebserviceToCallToUrl;
            terminal.PosValue2 = this.WebserviceUserkey;
            terminal.PosValue3 = this.WebserviceToBeCalledOnUrl;
            terminal.PosValue4 = this.testMode;
        }

        /// <summary>
        /// Write configuration to Terminal Model
        /// </summary>
        /// <param name="terminal"></param>
        public void WriteToTerminalModel(Terminal terminal)
        {
            terminal.PosValue1 = this.WebserviceToCallToUrl;
            terminal.PosValue2 = this.WebserviceUserkey;
            terminal.PosValue3 = this.WebserviceToBeCalledOnUrl;
            terminal.PosValue4 = this.testMode;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the webservice to call to URL.
        /// </summary>
        /// <value>
        /// The webservice to call to URL.
        /// </value>
        public string WebserviceToCallToUrl { get; set; }

        /// <summary>
        /// Gets or sets the user key.
        /// </summary>
        /// <value>
        /// The user key.
        /// </value>
        public string WebserviceUserkey { get; set; }

        /// <summary>
        /// Gets or sets the webservice to be called on URL.
        /// </summary>
        /// <value>
        /// The webservice to be called on URL.
        /// </value>
        public string WebserviceToBeCalledOnUrl { get; set; }

        public bool ExternalNotUsed { get; set; }

        string testMode;
        public bool TestMode
        {
            get
            {
                bool testmodeBool = false;
                bool.TryParse(this.testMode, out testmodeBool);
                return testmodeBool;
            }
            set
            {
                this.testMode = value.ToString();
            }
        }

        #endregion
    }
}
