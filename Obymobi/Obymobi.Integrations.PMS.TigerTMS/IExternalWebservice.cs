﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace Obymobi.Integrations.PMS.Tigertms
{
    //[ServiceContract]
    [ServiceContract(Namespace = "http://tigergenericinterface.org/")]
    public interface IExternalWebservice
    {
        /// <summary>
        /// Receives a message from the TigerTMS system. 
        /// It's a strange name for here, but it's how it's required to be named.
        /// </summary>
        /// <param name="msg">The contents, an XML message.</param>
        /// <returns>SUCCESS or FAILED</returns>        
        [OperationContract(Action = "http://tigergenericinterface.org/SendMessageToExternalInterface")]
        string SendMessageToExternalInterface(string Msg);
    }
}
