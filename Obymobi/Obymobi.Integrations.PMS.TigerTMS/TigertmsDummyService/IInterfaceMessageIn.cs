﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;

namespace Obymobi.Integrations.PMS.Tigertms
{
    [ServiceContract(Namespace = "http://tigergenericinterface.org/")]
    public interface IInterfaceMessageIn
    {
        [OperationContract]
        string externalInterfaceMessageIn(string msg);
    }
}
