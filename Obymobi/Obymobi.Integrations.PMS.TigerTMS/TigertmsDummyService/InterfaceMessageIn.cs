﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using System.Threading;
using System.Threading.Tasks;
using Dionysos;
using Obymobi.Integrations.PMS.FlatWsdl;
using Obymobi.Integrations.PMS.Tigertms.Messages;

namespace Obymobi.Integrations.PMS.Tigertms
{
    [ServiceBehavior(Namespace = "http://tigergenericinterface.org/", InstanceContextMode = InstanceContextMode.Single)]
    [Soap11Conformant]
    public class InterfaceMessageIn : IInterfaceMessageIn
    {
        private ExternalWebservice webserviceToCallTo = null;
        private TigertmsConnector tigertmsConnector = null;

        private void Log(string methodName, string text)
        { 
            //DesktopLogger.Verbose(string.Format("InterfaceMessageIn.{0}", text));
        }

        public InterfaceMessageIn(ExternalWebservice webserviceToCallTo, TigertmsConnector tigertmsConnector)
        {
            this.webserviceToCallTo = webserviceToCallTo;
            this.tigertmsConnector = tigertmsConnector;
        }

        public string externalInterfaceMessageIn(string msg)
        {
            this.Log("externalInterfaceMessageIn", "A1");
            return this.externalInterfaceMessageIn(msg, 0);
        }

        public string externalInterfaceMessageIn(string msg, int delay)        
        {
            this.Log("externalInterfaceMessageIn", "B1");
            MessageBase message = MessageBase.GetMessageFromXml(msg);
            this.Log("externalInterfaceMessageIn", "B2");
            
            string toReturn = "SUCCESS";
            string responseXml = string.Empty;
            string responseXml2 = string.Empty;
            List<string> responseXmlModels = new List<string>();

            if (message.GetType() == typeof(Requestcheckout))
            {
                this.Log("externalInterfaceMessageIn", "Requestcheckout - 1");
                // Respond with a Checkoutresults message
                // Balance == 10 = OKAY CHECKOUT, all others not.
                Requestcheckout request = (Requestcheckout)message;
                this.Log("externalInterfaceMessageIn", "Requestcheckout - 2");
                Checkoutresults response = new Checkoutresults();
                this.Log("externalInterfaceMessageIn", "Requestcheckout - 3");
                response.ReservationNumber = message.ReservationNumber;
                this.Log("externalInterfaceMessageIn", "Requestcheckout - 4");
                response.Room = message.Room;
                this.Log("externalInterfaceMessageIn", "Requestcheckout - 5");
                response.Reply = request.Room == "1" ? "Complete" : "Balance Mismatch";
                this.Log("externalInterfaceMessageIn", "Requestcheckout - 6");

                this.tigertmsConnector.LogDebug("Received Express Checkout Request, respond with: '{0}'", response.Reply);

                responseXml = response.ToXml();
                this.Log("externalInterfaceMessageIn", "Requestcheckout - 7");

                Checkoutresults checkoutresults = new Checkoutresults();
                this.Log("externalInterfaceMessageIn", "Requestcheckout - 8");
                checkoutresults.Room = message.Room;
                this.Log("externalInterfaceMessageIn", "Requestcheckout - 9");
                responseXml2 = checkoutresults.ToXml();
                this.Log("externalInterfaceMessageIn", "Requestcheckout - 10");
            }
            else if (message.GetType() == typeof(Requestrefresh))
            {
                this.Log("externalInterfaceMessageIn", "Requestrefresh - 1");
                Requestrefresh request = (Requestrefresh)message;
                this.Log("externalInterfaceMessageIn", "Requestrefresh - 2");

                if (request.Room.Equals("ALL", StringComparison.InvariantCultureIgnoreCase))
                {
                    int till = RandomUtil.GetRandomNumber(20);
                    for (int i = 1; i < till; i++)
                    {
                        Refreshresults result = new Refreshresults();
                        result.Room = i.ToString();
                        result.Title = "Mr.";
                        result.First = RandomUtil.GetRandomLowerCaseString(4);
                        result.Last = RandomUtil.GetRandomLowerCaseString(7);
                        result.Language = "UK";
                        result.Group = "Hogwarts School of Witchcraft and Wizardry";
                        result.Viewbill = RandomUtil.NextBoolean() ? "True" : "False";
                        result.ExpressCheckout = RandomUtil.NextBoolean() ? "True" : "False";
                        result.MessageWaitingLamp = RandomUtil.NextBoolean() ? "Y" : "N";
                        result.Occupied = RandomUtil.NextBoolean() ? "Y" : "N";

                        responseXmlModels.Add(result.ToXml());
                    }
                }
                
                this.Log("externalInterfaceMessageIn", "Requestrefresh - 3");
            }
            else if (message.GetType() == typeof(Requestbill))
            {
                // Respond with a Checkoutresults message
                List<string> productNames = new List<string> { "Club Sandwich", "Cuba Libre Cocktail", "Champagne Afternoon Tea", "Continental Breakfast", "60 Minute Massage", "Skin Radiance Facials", "Chauffeur Hire" };
                this.Log("externalInterfaceMessageIn", "Requestbill - 1");
                Requestbill request = (Requestbill)message;
                this.Log("externalInterfaceMessageIn", "Requestbill - 2");
                Roombillresults response = new Roombillresults();
                this.Log("externalInterfaceMessageIn", "Requestbill - 3");
                response.ReservationNumber = message.ReservationNumber;
                this.Log("externalInterfaceMessageIn", "Requestbill - 4");
                response.Room = message.Room;
                this.Log("externalInterfaceMessageIn", "Requestbill - 5");

                this.Log("externalInterfaceMessageIn", "Requestbill - 6");
            	int numberOfProducts = RandomUtil.GetRandomNumber(10) + 2;
                this.Log("externalInterfaceMessageIn", "Requestbill - 7");
				response.Items = new List<BillItem>();
                this.Log("externalInterfaceMessageIn", "Requestbill - 8");
            	decimal balance = 0;
                this.Log("externalInterfaceMessageIn", "Requestbill - 9");
				for (int i = 1; i <= numberOfProducts; i++)
				{
                    this.Log("externalInterfaceMessageIn", "Requestbill - 10");
					decimal price = ((RandomUtil.GetRandomNumber(1000) + 100) / 100);
                    this.Log("externalInterfaceMessageIn", "Requestbill - 11");
					balance += price;
                    this.Log("externalInterfaceMessageIn", "Requestbill - 12");
					
					BillItem bi1 = new BillItem();
                    this.Log("externalInterfaceMessageIn", "Requestbill - 13");
					bi1.Charge = price.ToStringInvariantCulture("N2");
                    this.Log("externalInterfaceMessageIn", "Requestbill - 14");
					bi1.Code = "CODE" + i;
                    this.Log("externalInterfaceMessageIn", "Requestbill - 15");
					bi1.Description = string.Format("Charge - {0}", productNames.Pick());
                    this.Log("externalInterfaceMessageIn", "Requestbill - 16");
					bi1.DateTime = MessageBase.DateTimeToString(DateTime.Now.AddDays(0-RandomUtil.GetRandomNumber(8)).AddMinutes(RandomUtil.GetRandomNumber(1440)));
                    this.Log("externalInterfaceMessageIn", "Requestbill - 17");
					
					response.Items.Add(bi1);
                    this.Log("externalInterfaceMessageIn", "Requestbill - 18");
				}
                this.Log("externalInterfaceMessageIn", "Requestbill - 19");
				response.Balance = balance.ToStringInvariantCulture("N2");
                this.Log("externalInterfaceMessageIn", "Requestbill - 20");

                
                /*decimal price1 = ((RandomUtil.GetRandomNumber(1000) + 100) / 100);
                decimal price2 = ((RandomUtil.GetRandomNumber(1000) + 100) / 100);
                decimal price3 = ((RandomUtil.GetRandomNumber(1000) + 100) / 100);
                decimal price4 = ((RandomUtil.GetRandomNumber(1000) + 100) / 100);

                BillItem bi1 = new BillItem();
                bi1.Charge = price1.ToStringInvariantCulture("N2");
                bi1.Code = "CODE1";
                bi1.Description = string.Format("Charge 1 - {0} - Code 1 - {1}", price1, RandomUtil.GetRandomLowerCaseString(4));
                bi1.DateTime = MessageBase.DateTimeToString(DateTime.Now.AddDays(-4).AddMinutes(50));

                BillItem bi2 = new BillItem();
                bi2.Charge = price2.ToStringInvariantCulture("N2");
                bi2.Code = "CODE2";
                bi2.Description = string.Format("Charge 2 - {0} - Code 2 - {1}", price2, RandomUtil.GetRandomLowerCaseString(4));
                bi2.DateTime = MessageBase.DateTimeToString(DateTime.Now.AddDays(-3).AddHours(-2).AddMinutes(17));

                BillItem bi3 = new BillItem();
                bi3.Charge = price3.ToStringInvariantCulture("N2");
                bi3.Code = "CODE3";
                bi3.Description = string.Format("Charge 3 - {0} - Code 3 - {1}", price3, RandomUtil.GetRandomLowerCaseString(4));
                bi3.DateTime = MessageBase.DateTimeToString(DateTime.Now.AddDays(-7).AddHours(-8).AddMinutes(53));

                BillItem bi4 = new BillItem();
                bi4.Charge = price4.ToStringInvariantCulture("N2");
                bi4.Code = "CODE4";
                bi4.Description = string.Format("Charge 4 - {0} - Code 4 - {1}", price4, RandomUtil.GetRandomLowerCaseString(4));
                bi4.DateTime = MessageBase.DateTimeToString(DateTime.Now.AddDays(-7).AddHours(-8).AddMinutes(53));*/

                /*decimal balance = price1 + price2 + price3 + price4;
                response.Balance = NumericsUtil.ToStringInvariantCulture(balance, "N2");

                response.Items = new List<BillItem>();
                response.Items.Add(bi1);
                response.Items.Add(bi2);
                response.Items.Add(bi3);
                response.Items.Add(bi4);*/
                this.Log("externalInterfaceMessageIn", "Requestbill - 21");
                this.tigertmsConnector.LogDebug("Received Get Folio, respond with: '{0}' items, total value: '{1}'",
                    response.Items.Count, balance);
                this.Log("externalInterfaceMessageIn", "Requestbill - 22");
                responseXml = response.ToXml();
                this.Log("externalInterfaceMessageIn", "Requestbill - 23");
            }
            else
            {
                this.Log("externalInterfaceMessageIn", "C1");
                this.tigertmsConnector.LogDebug("Received call to InterfaceMessageIn (PMS Emulator) - Can't process");
                this.Log("externalInterfaceMessageIn", "C2");
                this.tigertmsConnector.LogDebug("Failed message: {0}", msg);
                this.Log("externalInterfaceMessageIn", "C3");
                toReturn = "FAILED";
            }

            this.Log("externalInterfaceMessageIn", "B3");
            this.tigertmsConnector.LogDebug("Response for call will be returned in {0}ms", delay);

            // Start handling on seperate thread to be able to delay without blocking the thread
            // Run on another thread to causes the oberersable collection event to come from a new thread that's not blocked.
            this.Log("externalInterfaceMessageIn", "B4");
            Task.Factory.StartNew(() =>
            {
                this.Log("externalInterfaceMessageIn", "B5");
                if (!responseXml.IsNullOrWhiteSpace())
                {
                    this.SendResponse(responseXml, delay);
                }
                else if (!responseXml2.IsNullOrWhiteSpace())
                {
                    this.SendResponse(responseXml2, delay);
                }                
                else if (responseXmlModels.Count > 0)
                {
                    foreach (string responseXmlModel in responseXmlModels)
                    {
                        this.SendResponse(responseXmlModel, delay);
                    }
                }
                this.Log("externalInterfaceMessageIn", "B6");
            });

            return toReturn;
        }

        private void SendResponse(string msg, int delay)
        {
            this.Log("SendResponse", "1");
            if (delay > 0)
            {
                this.Log("SendResponse", "2");
                Thread.Sleep(delay);
                this.Log("SendResponse", "3");
            }

            this.Log("SendResponse", "4");
            this.webserviceToCallTo.SendMessageToExternalInterface(msg);
            this.Log("SendResponse", "5");

            this.tigertmsConnector.LogDebug("Response is now sent for message: '{0}'", msg);
            this.Log("SendResponse", "6");
        }

    }
}
