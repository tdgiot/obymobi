﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Obymobi.Integrations.PMS.Tigertms.Messages
{
    [Serializable, XmlRootAttribute(ElementName = "item")]
    public class BillItem : MessageBase
    {
        [XmlElement("code")]
        public string Code
        { get; set; }

        [XmlElement("description")]
        public string Description
        { get; set; }

        [XmlElement("charge")]
        public string Charge
        { get; set; }

        [XmlElement("datetime")]
        public string DateTime
        { get; set; }

        public override string GetResponseMessageSignature()
        {
            throw new NotImplementedException("Message is a sub item of Bill.");
        }
    }
}
