﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.Xml;
using Dionysos;
using Obymobi.Enums;
using Obymobi.Logic.Model;

namespace Obymobi.Integrations.PMS.Tigertms.Messages
{
    [Serializable, XmlRootAttribute(ElementName = "addguestresults")]
    public class Addguestresults : MessageBase
    {
        [XmlArray("extensions")]
        [XmlArrayItem("extension")]
        public string[] Extensions
        { get; set; }

        [XmlElement("title")]
        public string Title
        { get; set; }

        [XmlElement("last")]
        public string Last
        { get; set; }

        [XmlElement("first")]
        public string First
        { get; set; }

        [XmlElement("lang")]
        public string Language
        { get; set; }

        [XmlElement("group")]
        public string Group
        { get; set; }

        [XmlElement("vip")]
        public string Vip
        { get; set; }

        [XmlElement("arrival")]
        public string Arrival
        { get; set; }

        [XmlElement("departure")]
        public string Departure
        { get; set; }

        [XmlElement("tv")]
        public string Tv
        { get; set; }

        [XmlElement("minibar")]
        public string Minibar
        { get; set; }

        [XmlElement("viewbill")]
        public string Viewbill
        { get; set; }

        [XmlElement("expressco")]
        public string ExpressCheckout
        { get; set; }

        public GuestInformation ToGuestInformationModel()
        {
            GuestInformation gi = new GuestInformation();
            gi.DeliverypointNumber = this.Room;            
            gi.Title = this.Title;
            gi.CustomerLastname = this.Last;
            gi.CustomerFirstname = this.First;
            gi.LanguageCode = this.Language;
            gi.GroupName = this.Group;
            gi.Vip = !this.Vip.IsNullOrWhiteSpace() && this.Vip.Equals("vip", StringComparison.InvariantCultureIgnoreCase);
            gi.Arrival = MessageBase.StringToDateTime(this.Arrival);
            gi.Departure = MessageBase.StringToDateTime(this.Departure);

            PMSTvSetting tvSetting;
            if (!PMSTvSetting.TryParse(this.Tv, true, out tvSetting))
                tvSetting = PMSTvSetting.Unknown;

            gi.TvSetting = (int)tvSetting;

            PMSMinibarSetting minibarSetting;
            if (!PMSMinibarSetting.TryParse(this.Minibar, true, out minibarSetting))
                minibarSetting = PMSMinibarSetting.Unknown;

            gi.MinibarSetting = (int)minibarSetting;

            gi.AllowViewFolio = Convert.ToBoolean(this.Viewbill);
            gi.AllowExpressCheckout = Convert.ToBoolean(this.ExpressCheckout);

            return gi;
        }

        public override string GetResponseMessageSignature()
        {
            throw new NotImplementedException("Message is a response itself");
        }
    }
}
