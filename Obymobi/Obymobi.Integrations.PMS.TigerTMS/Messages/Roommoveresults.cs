﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.Xml;
using Obymobi.Logic.Model;

namespace Obymobi.Integrations.PMS.Tigertms.Messages
{
    [Serializable, XmlRootAttribute(ElementName = "roommoveresults")]
    public class Roommoveresults : MessageBase
    {
        [XmlElement("roomold")]
        public string RoomOld
        { get; set; }

        public MoveRoom ToMoveRoomModel()
        {
            MoveRoom moveRoom = new MoveRoom();
            moveRoom.NewDeliverypointNumber = this.Room;
            moveRoom.OldDeliverypointNumber = this.RoomOld;

            return moveRoom;
        }

        public override string GetResponseMessageSignature()
        {
            throw new NotImplementedException("Message is a response itself");
        }
    }
}