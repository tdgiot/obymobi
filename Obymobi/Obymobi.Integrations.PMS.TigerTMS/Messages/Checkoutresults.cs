﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.Xml;
using Obymobi.Logic.Model;

namespace Obymobi.Integrations.PMS.Tigertms.Messages
{
    [Serializable, XmlRootAttribute(ElementName = "checkoutresults")]
    public class Checkoutresults : MessageBase
    {
        [XmlArray("extensions")]
        [XmlArrayItem("extension")]
        public string[] Extensions
        { get; set; }

        [XmlElement("title")]
        public string Title
        { get; set; }

        [XmlElement("last")]
        public string Last
        { get; set; }

        [XmlElement("first")]
        public string First
        { get; set; }

        /// <summary>
        /// Reply String  will be the following :  Complete, Balance Mismatch, Invalid Date, Request Denied, Feature Not Allowed, Invalid Account, Guest Not Found 
        /// </summary>
        [XmlElement("reply")]
        public string Reply
        { get; set; }

        public GuestInformation ToGuestInformationModel()
        {
            GuestInformation gi = new GuestInformation();
            gi.DeliverypointNumber = this.Room;
            gi.Title = this.Title;
            gi.CustomerLastname = this.Last;
            gi.CustomerFirstname = this.First;
            gi.Occupied = false;
            gi.AccountNumber = this.ReservationNumber;

            return gi;
        }

        public override string GetResponseMessageSignature()
        {
            throw new NotImplementedException("Message is a response itself");
        }
    }
}
