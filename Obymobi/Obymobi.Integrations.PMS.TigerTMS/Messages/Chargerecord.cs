﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.Xml;

namespace Obymobi.Integrations.PMS.Tigertms.Messages
{
    [Serializable, XmlRootAttribute(ElementName = "chargerecord")]
    public class Chargerecord : MessageBase
    {
        [XmlAttribute("type")]
        public string Type
        { get; set; }

        [XmlElement("room")]
        public new string Room
        { get; set; }

        [XmlElement("extension")]
        public string Extension
        { get; set; }

        [XmlElement("datetime")]
        public string DateTime
        { get; set; }

        [XmlElement("charge")]
        public string Charge
        { get; set; }

        [XmlElement("itemid")]
        public string ItemId
        { get; set; }

        [XmlElement("itemdescription")]
        public string ItemDescription
        { get; set; }

        [XmlElement("itemquantity")]
        public string ItemQuantity
        { get; set; }

        // Strange case, this message has no response...
        public override string GetResponseMessageSignature()
        {
            throw new NotImplementedException();
        }
    }
}
