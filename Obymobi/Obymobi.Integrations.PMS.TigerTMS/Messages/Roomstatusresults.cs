﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Obymobi.Logic.Model;

namespace Obymobi.Integrations.PMS.Tigertms.Messages
{
    [Serializable, XmlRootAttribute(ElementName = "roomstatusresults")]
    public class Roomstatusresults : MessageBase
    {
        [XmlElement("extension")]
        public string Extension
        { get; set; }
        /*
         * 1	– Dirty/Vacant
            2	– Dirty/Occupied
            3	– Clean/Vacant
            4	– Clean/Occupied
            5	– Inspected/Vacant
            6	– Inspected/Occupied
            */
        [XmlElement("status")]
        public int Status
        { get; set; }

        public RoomStatus ToGuestInformationModel()
        {
            RoomStatus roomStatus = new RoomStatus();
            roomStatus.DeliverypointNumber = this.Room;
            roomStatus.Extension = this.Extension;
            roomStatus.CleaningStatus = this.Status;

            return roomStatus;
        }

        public override string GetResponseMessageSignature()
        {
            throw new NotImplementedException("Message is a response itself");
        }
    }
}
