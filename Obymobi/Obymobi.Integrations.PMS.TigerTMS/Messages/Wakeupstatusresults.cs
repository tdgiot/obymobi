﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Obymobi.Logic.Model;

namespace Obymobi.Integrations.PMS.Tigertms.Messages
{
    [Serializable, XmlRootAttribute(ElementName = "wakeupstatusresults")]
    public class Wakeupstatusresults : MessageBase
    {
        [XmlElement("extension")]
        public string Extension
        { get; set; }

        /*  OK – Wakeup was answered.
            BY – Wakeup was Busy
            NR – Wakeup was unanswered (No Response)
        */
        [XmlElement("status")]
        public string Status
        { get; set; }

        [XmlElement("info")]
        public string Info
        { get; set; }        

        public override string GetResponseMessageSignature()
        {
            throw new NotImplementedException("Message is a response itself");
        }
    }
}
