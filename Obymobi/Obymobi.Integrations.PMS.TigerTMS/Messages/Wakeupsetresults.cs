﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.Xml;
using Obymobi.Logic.Model;

namespace Obymobi.Integrations.PMS.Tigertms.Messages
{
    [Serializable, XmlRootAttribute(ElementName = "wakeupsetresults")]
    public class Wakeupsetresults : MessageBase
    {
        [XmlArray("extensions")]
        [XmlArrayItem("extension")]
        public string[] Extensions
        { get; set; }

        [XmlElement("wakeupdate")]
        public string WakeUpDate
        { get; set; }

        [XmlElement("wakeuptime")]
        public string WakeUpTime
        { get; set; }

        public WakeUpStatus ToWakeUpStatusModel()
        {
            WakeUpStatus wakeUpStatus = new WakeUpStatus();
            wakeUpStatus.DeliverypointNumber = this.Room;
            wakeUpStatus.WakeUpDate = MessageBase.StringToDateTime(this.WakeUpDate, MessageBase.FirstDateStringFormat);
            wakeUpStatus.WakeUpTime = MessageBase.StringToDateTime(this.WakeUpTime, MessageBase.TimeStringFormat);

            return wakeUpStatus;
        }

        public override string GetResponseMessageSignature()
        {
            throw new NotImplementedException("Message is a response itself");
        }
    }
}
