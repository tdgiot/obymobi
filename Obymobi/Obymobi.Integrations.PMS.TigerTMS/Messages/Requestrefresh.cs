﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Obymobi.Integrations.PMS.Tigertms.Messages
{
    [Serializable, XmlRootAttribute(ElementName = "requestrefresh")]
    public class Requestrefresh : MessageBase
    {
        /// <summary>
        /// Number or ALL
        /// </summary>
        [XmlElement("extension")]
        public string Extension
        { get; set; }

        /// <summary>
        /// Gets the MessageSignature of the expected return message
        /// </summary>
        /// <returns></returns>
        public override string GetResponseMessageSignature()
        {
            return string.Format("refreshresults-{0}", this.Room);
        }        
    }
}
