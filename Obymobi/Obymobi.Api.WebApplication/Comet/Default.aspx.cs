﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using Dionysos;
using Obymobi.Enums;
using Obymobi.Logic.Comet;
using Obymobi.Logic.Comet.Netmessages;
using Obymobi.Security;

public partial class Comet_Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    #region Web methods

    [WebMethod(EnableSession = true)]
    public static object[] GetAuthentication(string cometIdentifier)
    {
        long timestamp = DateTime.UtcNow.ToUnixTime();
        string hash = Hasher.GetHashFromParameters(CometConstants.CometInternalSalt, timestamp, CometConstants.CometIdentifierNoc, cometIdentifier);

        return new object[] { timestamp, CometConstants.CometIdentifierNoc, hash };
    }

    [WebMethod(EnableSession = true)]
    public static string NetmessageSetClientType(int companyId)
    {
        var netmessage = new NetmessageSetClientType();
        netmessage.ClientType = NetmessageClientType.External;
        netmessage.CompanyId = companyId;

        return netmessage.ToJson();
    }

    [WebMethod(EnableSession = true)]
    public static string NetmessagePong()
    {
        var netmessage = new NetmessagePong();
        return netmessage.ToJson();
    }

    #endregion
}