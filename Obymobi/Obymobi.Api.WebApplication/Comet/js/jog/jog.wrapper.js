﻿/// <reference path="../../js/crave.utils.js" />
/// <reference path="../../js/jog/jog.js" />
/// <reference path="../../js/jquery.mobile-1.0.js" />
/// <reference path="../../js/jquery-1.6.4.vsdocs.js" />

(function (Jog, $, undefined) {

    var defaultSection = 'misc';

    Jog.log = function (intLevel, section, namespace, method, message, args) {
        try {
            $.jog(section).log(intLevel, namespace + "." + method + " - " + Utils.format(message, args));
        }
        catch (ex) {
            $.jog(section).log(intLevel, namespace + "." + method + " - " + message);
        }
    }

    Jog.fine = function (namespace, method, message, args) {
        /// <summary>Log your message at the 'Fine' level</summary>
        /// <param name="namespace">Namespace of where the log originates from</param>
        /// <param name="method">Metod of where the log originates from</param>
        /// <param name="message">Message to log</param>
        /// <param name="args">Arguments to be used for formatting of the message (optional)</param>
        /// <returns type="boolean">
        return Jog.log($.jog.levels.Fine, defaultSection, namespace, method, message, args);
    },

    Jog.fineSectioned = function (section, namespace, method, message, args) {
        /// <summary>Log your message at the 'Fine' level</summary>
        /// <param name="section">Section to log for</param>
        /// <param name="message">Namespace of where the log originates from</param>
        /// <param name="message">Metod of where the log originates from</param>
        /// <returns type="boolean">
        return Jog.log($.jog.levels.Fine, section, namespace, method, message, args);
    },

    Jog.debug = function (namespace, method, message, args) {
        /// <summary>Log your message at the 'Debug' level</summary>
        /// <param name="namespace">Namespace of where the log originates from</param>
        /// <param name="method">Metod of where the log originates from</param>
        /// <param name="message">Message to log</param>
        /// <param name="args">Arguments to be used for formatting of the message (optional)</param>
        /// <returns type="boolean">        
        return Jog.log($.jog.levels.Debug, defaultSection, namespace, method, message, args);
    },

    Jog.debugSectioned = function (section, namespace, method, message, args) {
        /// <summary>Log your message at the 'Debug' level</summary>
        /// <param name="section">Section to log for</param>
        /// <param name="message">Namespace of where the log originates from</param>
        /// <param name="message">Metod of where the log originates from</param>
        /// <returns type="boolean">
        return Jog.log($.jog.levels.Debug, section, namespace, method, message, args);
    },

    Jog.info = function (namespace, method, message, args) {
        /// <summary>Log your message at the 'Info' level</summary>
        /// <param name="namespace">Namespace of where the log originates from</param>
        /// <param name="method">Metod of where the log originates from</param>
        /// <param name="message">Message to log</param>
        /// <param name="args">Arguments to be used for formatting of the message (optional)</param>
        /// <returns type="boolean">
        return Jog.log($.jog.levels.Info, defaultSection, namespace, method, message, args);
    },

    Jog.infoSectioned = function (section, namespace, method, message, args) {
        /// <summary>Log your message at the 'Info' level</summary>
        /// <param name="section">Variable/object to check</param>
        /// <returns type="boolean">
        return Jog.log($.jog.levels.Info, section, namespace, method, message, args);
    },

    Jog.config = function (namespace, method, message, args) {
        /// <summary>Log your message at the 'Config' level</summary>
        /// <param name="namespace">Namespace of where the log originates from</param>
        /// <param name="method">Metod of where the log originates from</param>
        /// <param name="message">Message to log</param>
        /// <param name="args">Arguments to be used for formatting of the message (optional)</param>
        /// <returns type="boolean">
        return Jog.log($.jog.levels.Config, defaultSection, namespace, method, message, args);
    },

    Jog.configSectioned = function (section, namespace, method, message, args) {
        /// <summary>Log your message at the 'Config' level</summary>
        /// <param name="section">Variable/object to check</param>
        /// <returns type="boolean">
        return Jog.log($.jog.levels.Config, section, namespace, method, message, args);
    },

    Jog.warning = function (namespace, method, message, args) {
        /// <summary>Log your message at the 'Warning' level</summary>
        /// <param name="namespace">Namespace of where the log originates from</param>
        /// <param name="method">Metod of where the log originates from</param>
        /// <param name="message">Message to log</param>
        /// <param name="args">Arguments to be used for formatting of the message (optional)</param>
        /// <returns type="boolean">
        return Jog.log($.jog.levels.Warning, defaultSection, namespace, method, message, args);
    },

    Jog.warningSectioned = function (section, namespace, method, message, args) {
        /// <summary>Log your message at the 'Warning' level</summary>
        /// <param name="section">Variable/object to check</param>
        /// <returns type="boolean">
        return Jog.log($.jog.levels.Warning, section, namespace, method, message, args);
    },

    Jog.severe = function (namespace, method, message, args) {
        /// <summary>Log your message at the 'Severe' level</summary>
        /// <param name="namespace">Namespace of where the log originates from</param>
        /// <param name="method">Metod of where the log originates from</param>
        /// <param name="message">Message to log</param>
        /// <param name="args">Arguments to be used for formatting of the message (optional)</param>
        /// <returns type="boolean">
        return Jog.log($.jog.levels.Severe, defaultSection, namespace, method, message, args);
    },

    Jog.severeSectioned = function (section, namespace, method, message, args) {
        /// <summary>Log your message at the 'Severe' level</summary>
        /// <param name="section">Variable/object to check</param>
        /// <returns type="boolean">
        return Jog.log($.jog.levels.Severe, section, namespace, method, message, args);
    },

    Jog.alert = function (namespace, method, message, args) {
        /// <summary>Log your message at the 'Alert' level</summary>
        /// <param name="namespace">Namespace of where the log originates from</param>
        /// <param name="method">Metod of where the log originates from</param>
        /// <param name="message">Message to log</param>
        /// <param name="args">Arguments to be used for formatting of the message (optional)</param>
        /// <returns type="boolean">
        return Jog.log($.jog.levels.Alert, defaultSection, namespace, method, message, args);
    },

    Jog.alertSectioned = function (section, namespace, method, message, args) {
        /// <summary>Log your message at the 'Alert' level</summary>
        /// <param name="section">Variable/object to check</param>
        /// <returns type="boolean">
        return Jog.log($.jog.levels.Alert, section, namespace, method, message, args);
    }

} (window.Jog = window.Jog || {}, jQuery));
