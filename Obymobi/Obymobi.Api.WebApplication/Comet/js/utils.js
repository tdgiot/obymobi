﻿(function (Utils, $, undefined) {

	var namespace = "Utils";

	Utils.loadExternalJavascript = function (url, options) {
		/// <summary>Retrieve an external Javascript source (additional scripts, or maybe just data)</summary>
		/// <param name="url">Url where the javascript is avaible (doesn't work cross-domain!)</param>
		/// <param name="options">
		/// A set of key/value pairs that configure the call back methods. All options are optional.</param>
		/// success(data, textStatus, jqXHR): Callback on success
		/// error(jqXHR, textStatus, errorThrown): Callback on error
		/// complete(jqXHR, textStats): Callback on complete (is always executed, on success and failure)
		/// manualLoader: boolean indicating if the loader is handled manualy instead of this method.</param>
		/// <returns type="boolean">

		Jog.debug(namespace, "loadExternalJavascript", "Start - {0}", [url]);

		if (!Utils.isDefinedAndNotNull(options))
			options = {};

		ajaxCall(url, {}, "script", options);
	};

	Utils.jsonDateToDate = function (str) {
		/// <summary>Convert a json date string to a js date object</summary>
		var milli = str.replace(/\/Date\((-?\d+)([+-]\d{2})?(\d{2})?.*/g, '$1');
		var d = new Date(parseInt(milli));

		return d;
	};

	function ajaxCall(url, data, dataType, options) {
		if (!Utils.isDefinedAndNotNull(options.type))
			options.type = "GET";

		if (!Utils.isDefinedAndNotNull(options.type))
			options.cache = false;

		Jog.fine(namespace, 'ajaxCall', 'Url: {0}', [url]);

		$.ajax({
			url: url,
			dataType: dataType,
			global: false,
			data: data,
			cache: options.cache,
			type: options.type,
			contentType: "application/json; charset=utf-8",
			success: function (data, textStatus, jqXHR) {
				if (jqXHR.status != 200) {
					Jog.warning(namespace, 'ajaxCall', 'Success, but wrong answer (Status = {0}/{1}): {2}', [jqXHR.status, textStatus, url]);
				}
				else {
					Jog.fine(namespace, 'ajaxCall', 'Success: {0}', [url]);
					if (Utils.isDefinedAndNotNull(options.success)) {
						if (Utils.isDefinedAndNotNull(data) && Utils.isDefinedAndNotNull(data.d)) {
							var resultObject = eval(data.d);

							if (Utils.isDefinedAndNotNull(resultObject.ResultCode)) {
								if (resultObject.ResultCode != 100) {
									Jog.debug(namespace, "ajaxCall", "Non-Ok result for " + url + ": " + resultObject.ResultCode + " Type: " + resultObject.ResultCode + " Message: " + resultObject.ResultMessage);
								}
							}

							options.success(resultObject, textStatus, jqXHR);
						}
						else
							options.success(data, textStatus, jqXHR);
					}
				}
			},
			error: function (jqXHR, textStatus, errorThrown) {
				Jog.warning(namespace, 'ajaxCall', 'Error (Status = {0}/{1}): {2}', [jqXHR.status, textStatus, url]);
				if (Utils.isDefinedAndNotNull(options.error))
					options.error(jqXHR, textStatus, errorThrown);
			},
			complete: function (jqXHR, textStatus) {
				if (jqXHR.status == 200) {
					// Success
					Jog.fine(namespace, 'ajaxCall', 'Complete (Status = {0}/{1}): {2}', [jqXHR.status, textStatus, url]);
				}
				else
					Jog.debug(namespace, 'ajaxCall', 'Complete (Status = {0}/{1}): {2}', [jqXHR.status, textStatus, url]);

				if (Utils.isDefinedAndNotNull(options.complete))
					options.complete();
			}
		});
	};

	Utils.isDefinedAndNotNull = function (obj) {
		/// <summary>Checks if a variable/object is defined and not null</summary>
		/// <param name="obj">Variable/object to check</param>
		/// <returns type="boolean">
		try {
			// We have to do a typeof check here because
			// undefined can be set as a variable in our own script
			// Example: http://jsfiddle.net/Mr_Dark/3vRJw/
			if (typeof obj === "undefined") {
				return false;
			} else if (obj === null) {
				return false;
			}
			return true;
		}
		catch (err) {
			Jog.debug("Utils.isDefinedAndNotNull got an Error 'nondefined' is assumed. Add 'window.' in front of the variable to test.");
			return false;
		}
	};
} (window.Utils = window.Utils || {}, jQuery));