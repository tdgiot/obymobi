﻿

(function (PokeInScriptLoader, $, undefined) {

	var namespace = "PokeInScriptLoader";
	var failedCheckCount = 0;
	var pingCount = 1;

	PokeInScriptLoader.manualLoader = true;

	PokeInScriptLoader.load = function () {
		Utils.loadExternalJavascript(Global_PokeInURL, PokeInScriptLoader);
	}

	PokeInScriptLoader.startConnectionCheck = function () {
		clearInterval(PokeInScriptLoader.pollInterval);
		PokeInScriptLoader.pollInterval = setInterval(PokeInScriptLoader.connectionCheck, 10000);
	};

	PokeInScriptLoader.connectionCheck = function () {
		if (typeof PokeIn !== "undefined") {
			if (PokeIn.IsConnected == false || PokeIn.IsListenActive() == false) {
				if (failedCheckCount >= 2) {
					Jog.debug("PokeInScriptLoader", "connectionCheck  - Not connected reloading pokein script.");

					if (!$('#connectionInfo').is(':visible')) {
						$('#connectionInfo').text("Disconnected. Trying to reconnecting to server, please wait...");
						$('#connectionInfo').fadeIn(300);
					}
					
					PokeInScriptLoader.startConnectionCheck();
					PokeInScriptLoader.load();
				}
				else
					failedCheckCount++;
			} else {
				if (pingCount % 6 == 0) {
					PokeInHelper.sendPing(0);
					pingCount = 1;
				} else {
					pingCount++;
				}
				failedCheckCount = 0;
			}
		} else {
			if (failedCheckCount >= 2) {
				Jog.debug("PokeInScriptLoader", "connectionCheck  - PokeIn not Defined.");

				PokeInScriptLoader.startConnectionCheck();
				PokeInScriptLoader.load();
			}
			else
				failedCheckCount++;
		}
	};

	PokeInScriptLoader.error = function (jqXHR, textStatus, errorThrown) {

		Jog.warning('PokeIn', 'Error', textStatus);
		setTimeout("Utils.loadExternalJavascript(Global_PokeInURL, PokeInScriptLoader)", 10000);

	};

	PokeInScriptLoader.success = function () {

		document.OnPokeInReady = function () {
			PokeIn.Start(function (is_connected) {
				if (is_connected) {
					PokeInHelper.setExternalListener(Global_CompanyId);
					Jog.fine('PokeIn', 'Start', 'Connected -- ClientId : ' + PokeIn.GetClientId());

					if ($('#connectionInfo').is(':visible')) {
						$('#connectionInfo').text("Connected!");
						$('#connectionInfo').fadeOut(2000);
					}

					if (Global_CompanyId > 0 && Global_DemoMode) {
						clearInterval(PokeInHelper.fakeOrderInterval);
						PokeInHelper.fakeOrderInterval = setInterval(PokeInHelper.fakeOrderTimeout, 5000);
					}

				} else {
					Jog.warning('PokeIn', 'Start', 'Could not connect!');

					PokeInScriptLoader.startConnectionCheck();
					PokeInScriptLoader.load();
				}
			});

			PokeIn.OnClose = function () {
				Jog.warning('PokeIn', 'OnClose', 'Disconnected');
				clearInterval(PokeInHelper.fakeOrderInterval);

				$('#connectionInfo').text("Disconnected. Trying to reconnecting to server, please wait...");
				if (!$('#connectionInfo').is(':visible')) {
					$('#connectionInfo').fadeIn(1500);
				}

				// Make sure the connection checker initiates a reconnect
				if (Utils.isDefinedAndNotNull(PokeIn)) {
					PokeIn.IsConnected = false;
				}
			};

		};
	};

	PokeInScriptLoader.derp = function() {
		PokeInHelper.fakeOrderTimeout();
	};

} (window.PokeInScriptLoader = window.PokeInScriptLoader || {}, jQuery));

(function(PokeInReceiver, $, undefined) {

	var namespace = "PokeInReceiver";
	
	PokeInReceiver.pong = function () {
        // Do nothing
    };

	PokeInReceiver.NewEvent = function(eventType, data) {
		Jog.debug(namespace, "NewEvent", "Event: " + eventType);
		var dataStr = String(data);
		if (eventType == 1) { // order
			EventHandler.NewOrder(dataStr);
		}

		if (Global_CompanyId > 0 && Global_DemoMode) {
			clearInterval(PokeInHelper.fakeOrderInterval);
			PokeInHelper.fakeOrderInterval = setInterval(PokeInHelper.fakeOrderTimeout, 5000);
		}
	};
} (window.PokeInReceiver = window.PokeInReceiver || {}, jQuery));

(function(PokeInHelper, $, undefined) {
	var namespace = "PokeInHelper";
	var internalClass = "PokeInClient";

	PokeInHelper.isPokeInEnabled = true;

	PokeInHelper.isConnected = function() {
		if (PokeInHelper.isPokeInEnabled === false) {
			Jog.debug(namespace, "isConnected", "PokeIn not enabled!!");
			return false;
		}

		if (!Utils.isDefinedAndNotNull(window.pCall)) {
			Jog.debug(namespace, "isConnected", "pCall not defined");
			return false;
		}
		if (!Utils.isDefinedAndNotNull(window.PokeIn)) {
			Jog.debug(namespace, "isConnected", "PokeIn not defined");
			return false;
		}

		return PokeIn.IsConnected;
	};
	
	PokeInHelper.fakeOrderTimeout = function() {
		Jog.debug(namespace, "fakeOrderTimeout", "START");
		if (PokeInHelper.isConnected()) {
			Jog.debug(namespace, "fakeOrderTimeout", "Company: " + Global_CompanyId);
			window.pCall[internalClass].GenerateFakeOrder(Global_CompanyId, Global_DeliverypointsString);
        }
		Jog.debug(namespace, "fakeOrderTimeout", "START");
	};


	PokeInHelper.sendTestMethod = function() {
		if (PokeInHelper.isConnected()) {
			window.pCall[internalClass].TestMethod();
		}
	};

    PokeInHelper.sendPing = function (id) {
        if (PokeInHelper.isConnected()) {
            window.pCall[internalClass].Ping(id);
        }
    };

	PokeInHelper.setExternalListener = function(companyId) {
		if (PokeInHelper.isConnected()) {
			window.pCall[internalClass].SetExternalListener(companyId);
		}
	};
	
} (window.PokeInHelper = window.PokeInHelper || {}, jQuery));