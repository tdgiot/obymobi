﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="Comet_Default" Codebehind="Default.aspx.cs" %>
<%@ Import Namespace="Obymobi" %>
<%@ Import Namespace="Obymobi.Logic.Comet" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<title>Order Viewer</title>
<script src="js/jquery-2.1.0.min.js" type="text/javascript"></script>
<script src="js/jquery.signalR-2.1.1.js" type="text/javascript"></script>
<script src="<%= WebEnvironmentHelper.GetMessagingUrl() %>/signalr/js" type="text/javascript"></script>
<script src="js/utils.js" type="text/javascript"></script>
    	
<style type="text/css">
	body {
		font-family: Tahoma;
		font-size: 12px;
	}

	#connectionInfo {
		border: 1px solid #dbdbdb;
		display: block;
		padding: 10px 10px;
		margin-right: 10px;
		font-size: 18px;
		text-align: center;
		margin-bottom: 10px;
		background: #E75B43;
		-moz-border-radius: 3px;
		-webkit-border-radius: 3px;
		border-radius: 3px; /* future proofing */
		-khtml-border-radius: 3px; /* for old Konqueror browsers */
	}

    .greenBg {
        background: #008000 !important;
    }
	
	#maincontainer {
		width: auto;
	}
	
	#header {
		font: 24px Courier New;
		font-weight: bold;
		text-align: center;
		margin: 0 0 5px 0;
	}
	
	.ordercontainer {
		float: left;
		width: 246px;
	}

	.receipt 
	{
		font: 11px/16px Lucida Console, Courier New;
		text-align: center;
		white-space: nowrap;
		color: #666;
		background: #fff;
		overflow: hidden;
		width: 220px;
		padding: 8px 10px;
		margin: 0 13px 13px 0px;
		border: 1px solid #eaeaea;
	}
	
	.receipt .header {
		font: 18px Lucida Console, Courier New;
		font-weight: bold;
		margin: 5px 0;
		white-space: pre-wrap;      /* CSS3 */   
		white-space: -moz-pre-wrap; /* Firefox */    
		white-space: -pre-wrap;     /* Opera <7 */   
		white-space: -o-pre-wrap;   /* Opera 7 */    
		word-wrap: break-word;      /* IE */
	}

	.receipt .items {
		text-align: left;
		overflow: hidden;
	    text-overflow: ellipsis;
		width: 100%;
	}

	.receipt .footer {
		font-size: 10px;
		white-space: normal;
	}

    .display-inline-block {
        display: inline-block !important;
    }
	
	.clearfloats {
		clear: both;
	}

</style>
</head>
<body>
<form runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePageMethods="true"/>
    </form>
	<div id="container">
		<div id="connectionInfo">Connecting to server, please wait...</div>
		<div id="maincontainer"></div>
		<div id="ordercontainer"></div>
	</div>
</body>
<script language="javascript">

	function getQueryVariable(variable) {
		variable = variable.toLowerCase()
		var query = window.location.search.substring(1).toLowerCase();
		var vars = query.split('&');
		for (var i = 0; i < vars.length; i++) {
			var pair = vars[i].split('=');
			if (decodeURIComponent(pair[0]) == variable) {
				return decodeURIComponent(pair[1]);
			}
		}
		return null;
	}

	function is_int(input) {
		return typeof(input) == 'number' || parseInt(input) == input;
	}

	var Global_DemoMode = (getQueryVariable("demo") != null);
	var Global_CompanyId = getQueryVariable("companyId");
	var Global_DeliverypointsString = "";
	var Global_Deliverypoints = null;
	
	var deliverypoints = getQueryVariable("deliverypoints");
	if (deliverypoints != null && deliverypoints.length > 0) {
		Global_DeliverypointsString = deliverypoints;
		Global_Deliverypoints = deliverypoints.split(',');
	}

	if (Global_CompanyId == null || !is_int(Global_CompanyId) || Global_CompanyId < 0) {
		$('#connectionInfo').text("Unknown Company!");
	} else {
		if (Global_Deliverypoints != null) {
			for (var i = 0; i < Global_Deliverypoints.length; i++) {
				$("#maincontainer").append('<div id="ordercontainer-' + Global_Deliverypoints[i] + '" class="ordercontainer"><div id="header">Table: ' + Global_Deliverypoints[i] + '</div><div id="content"></div></div>');
			}
		}
	}

	(function(EventHandler, $, undefined) {

		var namespace = "EventHandler";

		EventHandler.NewOrder = function(jsonStr) {
			var obj = $.parseJSON(jsonStr);

			if (Global_Deliverypoints != null && $("#ordercontainer-" + obj.DeliverypointNumber + " #content").children().length == 0) {
				$("#ordercontainer-" + obj.DeliverypointNumber + " #header").text(obj.DeliverypointName + ': ' + obj.DeliverypointNumber);
			}
			
			if (Global_Deliverypoints == null || Global_Deliverypoints.indexOf(String(obj.DeliverypointNumber)) > -1) {
				var orderItems = "";
				for (var i in obj.Orderitems) {
					orderItems += "<b>" + obj.Orderitems[i].Quantity + " x " + obj.Orderitems[i].ProductName + "</b><br/>";

					if (Utils.isDefinedAndNotNull(obj.Orderitems[i].Alterationitems) && obj.Orderitems[i].Alterationitems.length > 0) {
						for (var a in obj.Orderitems[i].Alterationitems) {
							orderItems += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + obj.Orderitems[i].Alterationitems[a].AlterationoptionName + "<br/>";
						}
					}
				}

				var receiptCss = "receipt";
				if (Global_CompanyId <= 0) receiptCss += " display-inline-block";

				var orderHtml = "";
				orderHtml += '<div class="' + receiptCss + '">';
				orderHtml += '	<div class="header">' + obj.CompanyName + '</div>';
				orderHtml += '	-----------------------------<br />';
				orderHtml += '	' + obj.Created + '<br />';
				orderHtml += '	<b>' + obj.DeliverypointName + ':</b> ' + obj.DeliverypointNumber + '<br />';
				orderHtml += '	-----------------------------<br />';
				orderHtml += '	<div class="items">';
				orderHtml += '		' + orderItems;
				orderHtml += '	</div>';
				orderHtml += '	-----------------------------<br />';
				orderHtml += '	<div class="footer">Thank you for your order</div>';
				orderHtml += '	<img src="img/crave-logo.png" alt="Order processing by Crave Emenu" border="0" />';
				orderHtml += '</div>';

				if (Global_Deliverypoints == null) {
					$(orderHtml).hide().prependTo("#maincontainer").fadeIn(1500);
					
					if ($("#maincontainer").children().length > 10) {
						$("#maincontainer").children().last().fadeOut(1500, function () { $(this).remove(); });
					}
				} else {
					$(orderHtml).hide().prependTo("#ordercontainer-" + obj.DeliverypointNumber + " #content").fadeIn(1500);

					if ($("#ordercontainer-" + obj.DeliverypointNumber + " #content").children().length > 1) {
						$("#ordercontainer-" + obj.DeliverypointNumber + " #content").children().last().fadeOut(1500, function() { $(this).remove(); });
					}
				}
			} else {
				console.log("Not in array: " + obj.DeliverypointNumber);
			}
		};

	}(window.EventHandler = window.EventHandler || {}, jQuery));

	(function (SignalRConnector, $, undefined) {

	    SignalRConnector.Connect = function () {
	        $.signalR.hub.start()
                .done(function () {
                    console.log('Now connected, connection ID=' + $.connection.hub.id);
                    $('#connectionInfo').text("Authenticating");

                    PageMethods.GetAuthentication($.connection.hub.id, SignalRConnector.Authenticate);
                })
                .fail(function (err) {
                    console.log('Could not connect: ' + err);
                    $('#connectionInfo').text("Failed to connect: " + err);
                });
	    };

	    SignalRConnector.Authenticate = function (collection) {
	        $.signalR.signalRInterface.server.authenticate(collection[0], collection[1], collection[2]);
	    };

	    SignalRConnector.SendMessage = function (message) {
	        $.signalR.signalRInterface.server.receiveMessage(message);
	    };

	}(window.SignalRConnector = window.SignalRConnector || {}, $));

	$(function () {

	    $.signalR.signalRInterface.client.ReceiveMessage = function(message) {
	        //console.log("Message: " + message);
	        var jsonObject = jQuery.parseJSON(message);

	        if (jsonObject.messageType == 17) {
	            if (jsonObject.isAuthenticated) {
	                if (jsonObject.isAuthenticated) {
	                    console.log("Authenticated, sending ClientType");
	                    PageMethods.NetmessageSetClientType(Global_CompanyId, SignalRConnector.SendMessage);

	                    $('#connectionInfo').text("Connected").addClass("greenBg").fadeOut(2000);
	                } else {
	                    console.log("Failed to authenticate: " + jsonObject.authenticateErrorMessage);
	                    $('#connectionInfo').text("Failed to authenticate");
	                }
	            }
	        } else if (jsonObject.messageType == 24) {
	            var dataStr = String(jsonObject.fieldValue3);
	            if (jsonObject.fieldValue1 == "1") { // order
	                EventHandler.NewOrder(dataStr);
	            }
	        } else if (jsonObject.messageType == 9998) {
	            PageMethods.NetmessagePong(SignalRConnector.SendMessage);
	        } else if (jsonObject.messageType == 1000) {
	            console.log("Disconnected");
	            $.signalR.hub.reconnect();
	        } else {
	            console.log("Unknown message: " + jsonObject.messageType);
	        }
	    };
	    $.signalR.signalRInterface.client.MessageVerified = function (guid) { };

	    $.signalR.hub.reconnect = function () {
	        $('#connectionInfo').text("Reconnecting...").removeClass("greenBg").fadeIn(500);
	        $.signalR.hub.stop();
	        SignalRConnector.Connect();
	    };

	    // Configuration
	    $.signalR.hub.url = "<%= WebEnvironmentHelper.GetMessagingUrl() %>/signalr";
	    $.signalR.hub.qs = { "deviceIdentifier": "<%=CometConstants.CometIdentifierNoc%>" };
	    $.signalR.hub.error(function (error) {
	        console.log('SignalR error: ' + error);
	        $.signalR.hub.reconnect();
	    });

	    if (Global_CompanyId != null && is_int(Global_CompanyId)) {
	        // Connect
	        $('#connectionInfo').text("Connecting");
	        SignalRConnector.Connect();
	    }
	});
</script>
</html>