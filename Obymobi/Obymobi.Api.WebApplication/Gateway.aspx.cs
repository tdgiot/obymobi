﻿using System.Data;
using Dionysos;
using Dionysos.Data;
using Dionysos.Web;
using Obymobi;
using Obymobi.Data;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using Obymobi.Logic;
using Obymobi.Logic.Analytics;
using Obymobi.Logic.Cms;
using Obymobi.Logic.HelperClasses;
using Obymobi.Web.Google;
using SD.LLBLGen.Pro.ORMSupportClasses;
using SpreadsheetLight;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using Dionysos.Logging;

public partial class Gateway : System.Web.UI.Page
{
	protected void Page_Load(object sender, EventArgs e)
	{
        if (TestUtil.IsPcJosh)
        {
            this.RunJoshTests();
        }

        if (QueryStringHelper.HasValue("cleardatabase"))
        {
            if (!Dionysos.ConfigurationManager.GetBool(ObymobiDataConfigConstants.DatabaseClearedForNonLiveUsage))
            {
                DatabaseClearer clearer = new DatabaseClearer();
                clearer.ClearDatabase();
            }
        }

        if (TestUtil.IsPcGabriel && DateTime.Now.Date.Equals(new DateTime(2014,8,20).Date))
        {
            StringBuilderLogger logger = new StringBuilderLogger();
            CloudStorageCleanerMedia cleaner = new CloudStorageCleanerMedia(logger, true);
            cleaner.Clean();
            
            Response.Clear();
            Response.ClearHeaders();
            Response.AddHeader("Content-Type", "text/plain");
            Response.Write(logger.StringBuilder.ToString());
            Response.Flush();
            Response.End();            
        }   

        if (TestUtil.IsPcBattleStationDanny)
        {
            var api = new GoogleAPI();

            string profile = "UA-45902422-1";
            if (QueryStringHelper.HasValue("live"))
                profile = "UA-45902422-4";
            else if (QueryStringHelper.HasValue("test"))
                profile = "UA-45902422-2";

            if (!api.SetProfile(profile))
            {
                Response.Write("Failed to set profile!");
                Response.End();
                return;
            }

            var filter = new Filter(new DateTime(2014, 1, 1), DateTime.Now, 199);
            var stats = new UpsellingStatsTable(filter, api);
            var messaging = new MessagingStatsTable(filter, api);

            messaging.LoadData();
            stats.LoadData(messaging.Rows.Count);

            var sl = new SLDocument();
            stats.GenerateSummary(sl);
            stats.GenerateRevenuePerUpsellType(sl);
            stats.GenerateAdvertisementInfo(sl);
            messaging.GenerateAllData(sl);

            sl.SelectWorksheet("Summary");

            // Set all print sizes to A4
            sl.SetPageSizeOfWorksheets(SLPaperSizeValues.A4Paper);

            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.AddHeader("Content-Disposition", "attachment; filename=WebStreamDownload.xlsx");
            sl.SaveAs(Response.OutputStream);
            Response.End();
        }

        if ((TestUtil.IsPcGabriel && false) || QueryStringHelper.HasValue("ShowTranslationInconsitencies"))
        {
            //this.plhOutputPre.AddHtml(LanguageHelper.GetDefaultTranslationValuesInconsitencies(false));
        }

        if ((TestUtil.IsPcGabriel && false) || QueryStringHelper.HasValue("FixTranslationInconsitencies"))
        {
            //this.plhOutputPre.AddHtml(LanguageHelper.GetDefaultTranslationValuesInconsitencies(true));
        }

        if (TestUtil.IsPcBattleStationErik && QueryStringHelper.HasValue("YouDieToday"))
        { 
            Process.GetCurrentProcess().Kill();
        }
	}

    void RunJoshTests()
    {
        this.plhOutput.AddHtml("Hello Worldz!");

        // Get all companies

        this.plhOutput.AddHtml("<h3>Get all companies</h3>");
        
        CompanyCollection companies = new CompanyCollection();
        PredicateExpression filter = new PredicateExpression();
        filter.Add(CompanyFields.Name % "%e%");
        filter.Add(CountryFields.Name == "Nederland");
        SortExpression sort = new SortExpression();
        sort.Add(CompanyFields.Name | SortOperator.Descending);
        RelationCollection joins = new RelationCollection();
        joins.Add(CompanyEntity.Relations.CountryEntityUsingCountryId);

        companies.GetMulti(filter, 0, sort, joins);

        foreach (var company in companies)
        {
            this.plhOutput.AddHtml("<pre>    {0}<br /></pre>", company.Name);
        }

        /*var c = new CompanyEntity(199);
        this.plhOutput.AddHtml("Company - {0}", c.Name);

        this.plhOutput.AddHtml("Desc before - {0}", c.Description);
        c.Description += " Bkla";
        c.Save();

        c = new CompanyEntity(199);
        this.plhOutput.AddHtml("Desc after - {0} <br />", c.Description);
        */
        
        // Get all products for a company-----------------------------------------//        
        
        this.plhOutput.AddHtml("<h3>Get all products for a company</h3>");

        CompanyEntity chewton = new CompanyEntity(199);
        ProductCollection products = new ProductCollection();
        products = chewton.ProductCollection;
        SortExpression sortExp = new SortExpression();
        sortExp.Add(ProductFields.ProductId | SortOperator.Ascending);


        products.GetMulti(null, 50, sortExp, null);

        this.plhOutput.AddHtml("<b>{0} products:</b>", chewton.Name);

        foreach (var product in products) 
        {
            this.plhOutput.AddHtml("<pre>   {0}</pre>", product.Name);
        }
        this.plhOutput.AddHtml("<pre>etc...</pre>");

        // Get last 10 orders + their order tiems -------------------------------//

        
        OrderCollection orders = new OrderCollection();
        SortExpression sortExp2 = new SortExpression();
        sortExp2.Add(OrderFields.CreatedUTC | SortOperator.Descending);      

        orders.GetMulti(null, 10, sortExp2, null);

        this.plhOutput.AddHtml("<h3>Last 10 orders</h3>");
        foreach (var order in orders) 
        {
                this.plhOutput.AddHtml("<pre>   Id -> {0}, Created (UTC) -> {1}</pre>", order.OrderId, order.CreatedUTC);
        }
        this.plhOutput.AddHtml("<pre>etc...</pre>");
        // Get all Deliverypoints for a Company

        DeliverypointgroupCollection deliveryGrpColl = new DeliverypointgroupCollection();
        DeliverypointCollection deliveryPoints = new DeliverypointCollection();
        PredicateExpression predExp = new PredicateExpression();
        predExp.Add(DeliverypointFields.CompanyId == 199);
        RelationCollection RelCol = new RelationCollection();
        RelCol.Add(DeliverypointgroupEntity.Relations.DeliverypointEntityUsingDeliverypointgroupId);

        deliveryGrpColl.GetMulti(predExp, 20, null, RelCol);

        this.plhOutput.AddHtml("<h3>Get All Deliverypoints for a company (Chewton Glen)</h3>");

        foreach (var deliverypoint in deliveryGrpColl) 
        {
            this.plhOutput.AddHtml("<pre>{0}</pre>", deliverypoint.DeliverypointgroupId);
        }
        this.plhOutput.AddHtml("<pre>etc...</pre>");

        // Get all products that were never ordered for a Company        


        this.plhOutput.AddHtml("<h3>Products not ordered from chewton glen</h3>");
        ProductCollection products2 = new ProductCollection();
        products2 = chewton.ProductCollection;
        RelationCollection join = new RelationCollection();
        join.Add(ProductEntity.Relations.OrderitemEntityUsingProductId, JoinHint.Left);
        PredicateExpression predExpression = new PredicateExpression();
        predExpression.Add(OrderitemFields.OrderitemId == DBNull.Value);

        products2.GetMulti(predExpression, 20, null, join);

        foreach (var product in products2) 
        {
            this.plhOutput.AddHtml("<pre>{0}</pre>", product.Name);
        }
        this.plhOutput.AddHtml("<pre>etc...</pre>");
        
        
        // Print all product for a company by category for each menu 
        this.plhOutput.AddHtml("<h3>Print all products for a company by category for each menu</h3>");


        
        MenuCollection menus = chewton.MenuCollection;

        foreach (MenuEntity menu in menus) 
        {
            this.plhOutput.AddHtml("<h2>{0}</h2>", menu.Name);
            /*CategoryCollection cats = menu.CategoryCollection;
            foreach (CategoryEntity cat in cats) 
            {
                if(cat.Visible){
                this.plhOutput.AddHtml("<h4>{0}</h4>", cat.Name);
                }
                ProductCategoryCollection prodcats = cat.ProductCategoryCollection;
                foreach (ProductCategoryEntity prodcat in prodcats) 
                {
                    ProductEntity prod = prodcat.ProductEntity;
                    if (prod.Visible)
                    {
                        this.plhOutput.AddHtml("<pre>    {0}</pre>", prod.Name);
                    }
                }
            }*/
        }
            
        // Print a menu with all categories, products and their alterations
        this.plhOutput.AddHtml("<h3>Print menu, with all categories, products and alterations</h3><h2>{0}</h2>", chewton.MenuCollection[1].Name);

        CategoryCollection cats2 = chewton.MenuCollection[1].CategoryCollection;
        foreach (CategoryEntity cat2 in cats2) 
        {
            if (cat2.Visible)
            {
                this.plhOutput.AddHtml("<h4>{0}</h4>", cat2.Name);
            }
            ProductCategoryCollection prodcats2 = cat2.ProductCategoryCollection;
            foreach (ProductCategoryEntity prodcat2 in prodcats2)
            {
                ProductEntity prod2 = prodcat2.ProductEntity;
                if (prod2.Visible)
                {
                    this.plhOutput.AddHtml("<pre>    {0}</pre>", prod2.Name);
                    ProductAlterationCollection prodAlts2 = prod2.ProductAlterationCollection;

                    foreach (ProductAlterationEntity prodAlt2 in prodAlts2) 
                    {
                        AlterationEntity alt = prodAlt2.AlterationEntity;
                        this.plhOutput.AddHtml("<pre>         {0}</pre>", alt.Name);

                        AlterationitemCollection altItms = alt.AlterationitemCollection;
                        foreach (AlterationitemEntity altItm in altItms) 
                        {
                            AlterationoptionEntity altOpt = altItm.AlterationoptionEntity;
                            this.plhOutput.AddHtml("<pre>             {0}</pre>", altOpt.Name);
                        }
                    }
                    
                }
            }

        }
        // Do fun stuff.



        


    }

    void ShowMediaRatios()
    {
        StringBuilder sb = new StringBuilder();

        Dictionary<MediaTypeGroup, List<MediaRatioType>> groupedMediaTypes = new Dictionary<MediaTypeGroup, List<MediaRatioType>>();
        foreach (MediaType mediaType in Enum.GetValues(typeof(MediaType)))
        {
            MediaRatioType mrt = MediaRatioTypes.GetMediaRatioType(mediaType);
            if (mrt == null)
                continue;

            if (mrt.MediaTypeGroup != MediaTypeGroup.None)
            {
                if (!groupedMediaTypes.ContainsKey(mrt.MediaTypeGroup))
                    groupedMediaTypes.Add(mrt.MediaTypeGroup, new List<MediaRatioType>());

                groupedMediaTypes[mrt.MediaTypeGroup].Add(mrt);
            }
        }

        // Calculate the Ratio's
        foreach (KeyValuePair<MediaTypeGroup, List<MediaRatioType>> pair in groupedMediaTypes)
        {
            sb.AppendFormat("<h1>{0}</h1>", pair.Key);
            sb.AppendFormat("<table><tr><td>Name</td><td>Width</td><td>Height</td><td>Ratio</td></tr>", pair.Key);

            var values = pair.Value.OrderBy(x => x.Name).OrderBy(x => Decimal.Divide(x.Width, x.Height));

            foreach (var mrt in values)
            {
                sb.AppendFormat("<tr><td>{0}</td><td>{1}</td><td>{2}</td><td>{3}</td></tr>",
                    mrt.Name, mrt.Width, mrt.Height, Decimal.Divide(mrt.Width, mrt.Height).ToString("N2"));
            }
            sb.AppendFormat("</table>");
        }

        this.plhOutput.AddHtml(sb.ToString());
    }

    private System.Collections.Concurrent.BlockingCollection<string> blockingCollectionOne = new System.Collections.Concurrent.BlockingCollection<string>();
    private System.Collections.Concurrent.BlockingCollection<string> blockingCollectionTwo = new System.Collections.Concurrent.BlockingCollection<string>();

    private void RunTest()
    {
        // Initiate a Consumer
        System.Threading.Thread t = new System.Threading.Thread(new System.Threading.ThreadStart(this.ConsumeCollections));
        t.Start();

        // Start queueing tasks
        this.blockingCollectionOne.Add("Collection 1 - " + DateTime.Now.ToString());

        System.Threading.Thread.Sleep(750);

        this.blockingCollectionOne.Add("Collection 1 - " + DateTime.Now.ToString());

        System.Threading.Thread.Sleep(750);

        // This task will not be executed:
        this.blockingCollectionTwo.Add("Collection 2 - " + DateTime.Now.ToString());

        System.Threading.Thread.Sleep(3000);

        // This will execute again:
        this.blockingCollectionOne.Add("Collection 1 - " + DateTime.Now.ToString());

        System.Threading.Thread.Sleep(750);
    }

    public void ConsumeCollections()
    {
        while (true)
        {
            string text;
            System.Collections.Concurrent.BlockingCollection<string>.TakeFromAny(new[] {
					this.blockingCollectionOne,
					this.blockingCollectionTwo
				}, out text);

            System.Diagnostics.Debug.WriteLine(text);
        }
    }

    private void CombineOriginalAndCompressed(string originalPath, string compressedPath)
    {
        Image original = Image.FromFile(originalPath);
        Image compress = Image.FromFile(compressedPath);

        Bitmap output = new Bitmap(original.Width, original.Height);

        using (Graphics g = Graphics.FromImage(output))
        {
            g.DrawImage(original, new Rectangle(0, 0, 640, 400), new Rectangle(0, 0, 640, 400), GraphicsUnit.Pixel);
            g.DrawImage(original, new Rectangle(640, 400, 640, 400), new Rectangle(640, 400, 640, 400), GraphicsUnit.Pixel);
            g.DrawImage(compress, new Rectangle(640, 0, 640, 400), new Rectangle(640, 0, 640, 400), GraphicsUnit.Pixel);
            g.DrawImage(compress, new Rectangle(0, 400, 640, 400), new Rectangle(0, 400, 640, 400), GraphicsUnit.Pixel);
        }

        output.Save(compressedPath.Replace(".jpg", ".png"), ImageFormat.Png);
    }

    /// <summary>
    /// Retrieves the Encoder Information for a given MimeType
    /// </summary>
    /// <param name="mimeType">String: Mimetype</param>
    /// <returns>ImageCodecInfo: Mime info or null if not found</returns>
    private static ImageCodecInfo GetEncoderInfo(String mimeType)
    {
        var encoders = ImageCodecInfo.GetImageEncoders();
        return encoders.FirstOrDefault(t => t.MimeType == mimeType);
    }

    /// <summary>
    /// Save an Image as a JPeg with a given compression
    ///  Note: Filename suffix will not affect mime type which will be Jpeg.
    /// </summary>
    /// <param name="image">Image: Image to save</param>
    /// <param name="fileName">String: File name to save the image as. Note: suffix will not affect mime type which will be Jpeg.</param>
    /// <param name="compression">Long: Value between 0 and 100.</param>
    private static void SaveJpegWithCompressionSetting(Image image, string fileName, long compression)
    {
        var eps = new EncoderParameters(1);
        eps.Param[0] = new EncoderParameter(System.Drawing.Imaging.Encoder.Quality, compression);
        var ici = GetEncoderInfo("image/jpeg");
        image.Save(fileName, ici, eps);
    }

    /// <summary>
    /// W. Europe Standard Time
    /// (UTC+01:00) Amsterdam, Berlin, Bern, Rome, Stockholm, Vienna
    /// </summary>
    public static TimeZoneInfo GetAfghanistanStandardTime()
    {
        return TimeZoneInfo.FromSerializedString("");
    }

    private void TestDiskSpace()
    {
        foreach (DriveInfo drive in DriveInfo.GetDrives())
        {
            if (drive.IsReady && drive.Name.Equals("C:\\", StringComparison.CurrentCultureIgnoreCase))
            {
                Debug.WriteLine("Drive: {0} - Space: {1} - {2} - {3}", drive.Name, drive.VolumeLabel, drive.TotalFreeSpace / (1024 * 1024 * 1024), drive.AvailableFreeSpace / (1024 * 1024 * 1024));
           }
        }        
    }
}

