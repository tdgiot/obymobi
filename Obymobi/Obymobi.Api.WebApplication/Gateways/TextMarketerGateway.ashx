﻿<%@ WebHandler Language="C#" Class="TextMarketerGateway" %>

using System;
using System.Web;
using Dionysos;
using Dionysos.SMS;
using Obymobi;
using Obymobi.Enums;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Logging;
using SD.LLBLGen.Pro.ORMSupportClasses;

public class TextMarketerGateway : IHttpHandler {

    public enum TextMarketerGatewayResult
    { 
        SendingSmsFailed = 200,
        NumberParameterMissing = 201
    }
    
    public void ProcessRequest (HttpContext context) {
        bool success = false;
        try
        {                                    
            string number, network, message;
            if (Dionysos.Web.QueryStringHelper.TryGetValue("number", out number))
            {
                // Only respond three times in 48 hours
                IncomingSmsCollection smses = new IncomingSmsCollection();
                PredicateExpression filter = new PredicateExpression();
                filter.Add(IncomingSmsFields.Number == number);
                filter.Add(IncomingSmsFields.SmsType == SmsType.RequestForDownloadUrl);
                filter.Add(IncomingSmsFields.CreatedUTC > DateTime.UtcNow.AddHours(-48));

                int smsesRequested = smses.GetDbCount(filter);
                string responseText = string.Empty;
                
                IncomingSmsEntity sms = new IncomingSmsEntity();
                sms.SmsType = (int)SmsType.RequestForDownloadUrl;
                sms.Number = number;

                if (Dionysos.Web.QueryStringHelper.TryGetValue("network", out network))
                    sms.Operator = network;

                if (Dionysos.Web.QueryStringHelper.TryGetValue("message", out message))
                    sms.Message = message;

                sms.Save();
                
                string shortUrl = string.Empty;                                
                try
                {
                    Dionysos.Web.Google.UrlShortener shortner = new Dionysos.Web.Google.UrlShortener();          
                    // Include IncomingSMSId and last 2 numbers of phone number          
                    string urlAddition = string.Format("appinstall.aspx?n={0}&c={1}", sms.IncomingSmsId, number.Substring(number.Length - 2));
                    string longUrl = Obymobi.WebEnvironmentHelper.GetMobileUrl(urlAddition);
                    var shortenResult = shortner.Shorten(new Uri(longUrl));
                    shortUrl = shortenResult.Id;                    
                }
                catch {
                    shortUrl = string.Empty;            
                }

                //Dionysos.Web.CacheHelper.Clear();
                
                if(shortUrl.IsNullOrWhiteSpace())
                    shortUrl = context.Request.Url.ToString().Contains("dev") ? "http://goo.gl/PduYJ" : "http://goo.gl/TBzPg";
                                                                                                                                                                        
                System.Collections.Generic.List<string> excludedNumbers = new System.Collections.Generic.List<string> { "31629031729", "447763125564", "447785551196", "447584702465", "447584702395", "447831841841", "447584702467" };

                if (smsesRequested == 0 || excludedNumbers.Contains(number))
                {
                    // First text         
                    responseText = "To start using Crave Mobile you can download it from: " + shortUrl + " (We'll only send you this message once, future requests will give no response)";
                }
                else if (smsesRequested == 1)
                {
                    // Second text (we grace once)
                    responseText = "To start using Crave Mobile you can download it from: " + shortUrl + " (We'll only send you this message once, future requests will give no response)";
                }
                else
                {
                    // Just register, but no action from our side.
                }

                if (!responseText.IsNullOrWhiteSpace())
                {
                    MollieSMSRequest smsRequest = new MollieSMSRequest();
                    ///
                    //Gatewaynaam	Gateway ID
                    //Basic	2
                    //Business	4
                    //Business+	1
                    //Landline	8
                    smsRequest.Gateway = "4";
                    smsRequest.Originator = "Crave";
                    smsRequest.Recipients = number;
                    smsRequest.Message = responseText;
                    smsRequest.Send();

                    if (smsRequest.ResultCode != 10)
                        throw new ObymobiException(TextMarketerGatewayResult.SendingSmsFailed, "Sms result: {0}, Number: {1}", smsRequest.ResultMessage, number);                    
                }

                context.Response.StatusCode = (int)System.Net.HttpStatusCode.OK;
                context.Response.ContentType = "text/plain";
                context.Response.Write("OK");
                success = true;                
            }
            else
            {
                Requestlogger.UpdateRequestLog(TextMarketerGatewayResult.NumberParameterMissing, "Number missing", null);
                context.Response.StatusCode = (int)System.Net.HttpStatusCode.BadRequest;
                context.Response.ContentType = "text/plain";
                context.Response.Write("BadRequest: Number missing");                            
            }         
        }
        catch (Exception ex)
        {
            Requestlogger.UpdateRequestLog(Obymobi.Enums.GenericWebserviceCallResult.Failure, "Failure", ex);
            context.Response.StatusCode = (int)System.Net.HttpStatusCode.InternalServerError;
            context.Response.ContentType = "text/plain";
            context.Response.Write("InternalServerError");            
        }
        finally
        {
            if(success)
                Requestlogger.UpdateRequestLog(Obymobi.Enums.GenericWebserviceCallResult.Success, "Success", null);
            else
                Requestlogger.UpdateRequestLog(Obymobi.Enums.GenericWebserviceCallResult.Failure, "Failure", null);
        }
    }
 
    public bool IsReusable {
        get {
            return false;
        }
    }

}