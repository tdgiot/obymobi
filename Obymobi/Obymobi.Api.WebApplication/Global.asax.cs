using Dionysos;
using Dionysos.Configuration;
using Dionysos.Data;
using Dionysos.Data.LLBLGen;
using Dionysos.Logging;
using Dionysos.SMS;
using Dionysos.Verification;
using Dionysos.Web;
using Obymobi;
using Obymobi.Configuration;
using Obymobi.Enums;
using Obymobi.Integrations.POS;
using Obymobi.Integrations.Service.HotSOS.Routing;
using Obymobi.Integrations.Service.Hyatt.Routing;
using Obymobi.Integrations.Service.Quore.Routing;
using Obymobi.Logging;
using Obymobi.Logic.Analytics;
using Obymobi.Logic.Comet;
using Obymobi.Logic.HelperClasses;
using Obymobi.Logic.Routing;
using Obymobi.Logic.Verifiers;
using Obymobi.Web;
using Obymobi.Web.Azure.ServiceBus.Notifications;
using Obymobi.Web.Caching;
using Obymobi.Web.CloudStorage;
using Obymobi.Web.GoogleApis;
using Obymobi.Web.Slack;
using Obymobi.Web.Slack.Clients;
using SD.LLBLGen.Pro.DQE.SqlServer;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Web;
using System.Web.Hosting;
using CacheHelper = Dionysos.Web.CacheHelper;
using OperatingSystem = Obymobi.Logic.Analytics.OperatingSystem;

namespace ObymobiWebservice
{
    public class Global : HttpApplicationSystemBased
    {
        #region  Fields

        private static InfiniteLooper databasePollerThread;
        private static InfiniteLooper supportnotificationPollerThread;

        public static AnalyticsService AnalyticsService;

        #endregion

        public Global() : base("ObymobiWebservice")
        { }

        public override void DatabaseIndependentPreInitialization()
        {
            // Set the data providers
            DataFactory.EntityCollectionFactory = new LLBLGenEntityCollectionFactory();
            DataFactory.EntityFactory = new LLBLGenEntityFactory();
            DataFactory.EntityUtil = new LLBLGenEntityUtil();

            // Set the assembly information
            Dionysos.Global.AssemblyInfo.Add(Server.MapPath("~/Bin/Obymobi.Data.dll"), "Data");
            Dionysos.Global.AssemblyInfo.Add(Server.MapPath("~/Bin/Obymobi.Logic.Legacy.dll"), "Legacy");

            // Set the configuration provivder
            Dionysos.Global.ConfigurationProvider = new LLBLGenConfigurationProvider();

            // Set the application information
            Dionysos.Global.ApplicationInfo = new WebApplicationInfo();
            Dionysos.Global.ApplicationInfo.ApplicationName = "ObymobiWebservice";
            Dionysos.Global.ApplicationInfo.ApplicationVersion = "1.2019103101";
            Dionysos.Global.ApplicationInfo.BasePath = Server.MapPath("~");

            // Set the configuration definitions
            Dionysos.Global.ConfigurationInfo.Add(new DionysosConfigurationInfo());
            Dionysos.Global.ConfigurationInfo.Add(new DionysosWebConfigurationInfo());
            Dionysos.Global.ConfigurationInfo.Add(new DionysosSMSConfigurationInfo());
            Dionysos.Global.ConfigurationInfo.Add(new ObymobiConfigInfo());
            Dionysos.Global.ConfigurationInfo.Add(new ObymobiDataConfigInfo());
            Dionysos.Global.ConfigurationInfo.Add(new POSConfigurationInfo());
            Dionysos.Global.ConfigurationInfo.Add(new CraveCometConfigInfo());

            // Comet Helper (method none = redis)
            CometHelper.SetCometProvider(new CometDesktopProvider(ClientCommunicationMethod.SignalR, CometConstants.CometIdentifierWebservice, NetmessageClientType.Webservice));

            // Inject the routestep handler usecases
            RoutingHelper.RoutestepHandlerUseCases.Add(RoutestephandlerType.HotSOS, new HandleHotSOSServiceOrderRoutestepUseCase());
            RoutingHelper.RoutestepHandlerUseCases.Add(RoutestephandlerType.Quore, new HandleQuoreServiceOrderRoutestepUseCase());
            RoutingHelper.RoutestepHandlerUseCases.Add(RoutestephandlerType.Hyatt_HotSOS, new HandleHyattServiceOrderRoutestepUseCase());

            DatabaseIndependentConfigurationValidation();

            //RedisCaching();

            // Setup Azure Notification Provider
            new AzureNotificationProvider(new AsyncWebAppLoggingProvider(HostingEnvironment.MapPath("~/App_Data/Logs/"), "Webservice", 7));

            ConfigureAnalyticService();

            PreventAppDomainRecycleOnFileChange();
        }

        public override void DatabaseDependentInitialization()
        {
            // Set locking method
            try
            {
                DynamicQueryEngine.UseNoLockHintOnSelects = ConfigurationManager.GetBool(ObymobiDataConfigConstants.UseNoLockHintOnSelects);
            }
            catch
            {
                // Database might not be available at this moment... 
                DynamicQueryEngine.UseNoLockHintOnSelects = true;
            }

            // Update the version
            CloudApplicationVersionHelper.SetCloudApplicationVersion(CloudApplication.API, Dionysos.Global.ApplicationInfo.ApplicationVersion);

            // Verifiers (last, to be sure the other code is at least initialized)
            DatabaseDependentConfigurationValidation();
        }

        private void DatabaseIndependentConfigurationValidation()
        {
            VerifierCollection verifiers = new VerifierCollection();
            verifiers.Add(new DependencyInjectionVerifier());
            verifiers.Add(new EntityVerifier());

            verifiers.Add(new WritePermissionVerifier(HostingEnvironment.MapPath("~/Builds/")));
            verifiers.Add(new WritePermissionVerifier(HostingEnvironment.MapPath("~/Temp/")));
            verifiers.Add(new WritePermissionVerifier(HostingEnvironment.MapPath("~/Downloads/")));
            verifiers.Add(new WritePermissionVerifier(HostingEnvironment.MapPath("~/Downloads/Output")));
            verifiers.Add(new WritePermissionVerifier(HostingEnvironment.MapPath("~/Error/")));
            verifiers.Add(new WritePermissionVerifier(HostingEnvironment.MapPath("~/Files/")));
            verifiers.Add(new WritePermissionVerifier(HostingEnvironment.MapPath("~/App_Data/Logs/")));

            verifiers.Add(new AppSettingsVerifier(DionysosConfigurationConstants.BaseUrl));
            verifiers.Add(new AppSettingsVerifier(DionysosConfigurationConstants.CloudEnvironment));

            //verifiers.Add(new AmazonCloudStorageVerifier());
            //verifiers.Add(new EventHubVerifier());

            if (!TestUtil.IsPcDeveloper)
            {
                verifiers.Add(new GoogleApiKeyVerifier());
            }

            if (!verifiers.Verify())
            {
                throw new VerificationException(verifiers.ErrorMessage);
            }
        }

        private void DatabaseDependentConfigurationValidation()
        {
            VerifierCollection verifiers = new VerifierCollection();
            verifiers.Add(new LoggingVerifier());
            verifiers.Add(new MiniTixVerifier());
            verifiers.Add(new SMSVerifier());
            //verifiers.Add(new CloudEnvironmentConfigValidator());
            //verifiers.Add(new SmsGatewayValidator());

            if (!verifiers.Verify())
            {
                throw new VerificationException(verifiers.ErrorMessage);
            }

            if (WebEnvironmentHelper.ORMProfilerEnabled)
            {
                EnableDatabaseProfiler();
            }
        }

        private void ConfigureAnalyticService()
        {
            List<AnalyticsConnector> connectors = new List<AnalyticsConnector>();
            connectors.Add(new MixPanelConnector(WebEnvironmentHelper.CloudEnvironment));
            connectors.Add(new GoogleAnalyticsConnector(WebEnvironmentHelper.CloudEnvironment));
            Global.AnalyticsService = new AnalyticsService(connectors, ApplicationType.Api, Dionysos.Global.ApplicationInfo.ApplicationName, Dionysos.Global.ApplicationInfo.ApplicationVersion, OperatingSystem.NotSet, string.Empty, false, string.Empty, new Tuple<int, int>(0, 0), false);
        }

        private void PreventAppDomainRecycleOnFileChange()
        {
            // ES: The following piece of code stops the appdomain recycle (and therefore the sign-out) that happens when a folder gets removed (e.g. after generating configuration files)
            PropertyInfo p = typeof(System.Web.HttpRuntime).GetProperty("FileChangesMonitor", BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Static);
            object o = p.GetValue(null, null);
            FieldInfo f = o.GetType().GetField("_dirMonSubdirs", BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.IgnoreCase);
            object monitor = f.GetValue(o);
            MethodInfo m = monitor.GetType().GetMethod("StopMonitoring", BindingFlags.Instance | BindingFlags.NonPublic);
            m.Invoke(monitor, new object[] { });
        }

        private void RedisCaching()
        {
            if (RedisCacheHelper.Instance.RedisEnabled)
            {
                CacheHelper.SetCachingStore(RedisCacheHelper.Instance.CacheStore);
            }
        }

        protected void Session_Start(object sender, EventArgs e)
        {

        }

        protected override void Application_BeginRequest(object sender, EventArgs e)
        {
            base.Application_BeginRequest(sender, e);

            if (HttpApplicationSystemBased.ApplicationInitialized && (this.Request.RawUrl.Contains("CraveService", StringComparison.InvariantCultureIgnoreCase) || this.Request.RawUrl.Contains("MobileService", StringComparison.InvariantCultureIgnoreCase)))
            {
                Requestlogger.CreateRequestlog("Application_BeginRequest");
            }
        }

        protected void Application_EndRequest()
        {
            if (HttpApplicationSystemBased.ApplicationInitialized && (this.Request.RawUrl.Contains("CraveService", StringComparison.InvariantCultureIgnoreCase) || this.Request.RawUrl.Contains("MobileService", StringComparison.InvariantCultureIgnoreCase)))
            {
                Requestlogger.UpdateRequestLogOnEndRequest();
            }
        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {
        }

        protected override void Application_Error(Exception ex)
        {
            // Oops... Uncaught Error			
            Exceptionlogger.CreateExceptionlog(ex.GetBaseException(), "UNCAUGHT! (Caught in Application_Error)", string.Empty, string.Empty, string.Empty);

            try
            {
                Exception error = HttpContext.Current.Server.GetLastError().GetBaseException();
                HttpContext.Current.Application.Add("Exception", error);

                if (error is AuthorizationException)
                {
                    Redirect(WebShortcuts.ResolveUrl("~/401.aspx"), true);
                }
                else if (error is EntityNotFoundException || (error is HttpException && (error as HttpException).GetHttpCode() == 404))
                {
                    Redirect(WebShortcuts.ResolveUrl("~/404.aspx"), false);
                }
                else
                {
                    Redirect(WebShortcuts.ResolveUrl("~/500.aspx"), true);
                }
            }
            catch
            {
                // If the custom logging fails, use the framework logging
                base.Application_Error(ex);
            }
        }

        private void Redirect(string url, bool logError)
        {
            if (logError)
            {
                LogToSlack(HttpContext.Current);
            }

            Server.ClearError();
            Server.Transfer(url, true);
        }

        private static void LogToSlack(HttpContext context)
        {
            SlackMessage slackMessage = new SlackMessage(CreateMessageText(context), $"SOAP API ({context.Server.MachineName})", null, null);
            SlackClient slackClient = new SlackClient(SlackWebhookEndpoints.DevSupportNotifications);
            slackClient.SendMessage(slackMessage);
        }

        private static string CreateMessageText(HttpContext httpContext)
        {
            MessageFromExceptionFactory messageFactory = new MessageFromExceptionFactory();
            return messageFactory.Create(httpContext);
        }

        protected void Session_End(object sender, EventArgs e)
        {
        }

        protected void Application_End(object sender, EventArgs e)
        {
            if (Global.databasePollerThread != null)
            {
                Global.databasePollerThread.Stop();
                Global.databasePollerThread = null;
            }

            if (Global.supportnotificationPollerThread != null)
            {
                Global.supportnotificationPollerThread.Stop();
                Global.supportnotificationPollerThread = null;
            }
        }
    }
}
