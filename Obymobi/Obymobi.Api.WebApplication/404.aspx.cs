﻿using System;
using System.Web;
using System.Web.UI;

namespace Obymobi.Api.WebApplication
{
    public partial class _404 : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            lblException.Text = (HttpContext.Current.Application.Get("Exception") as Exception)?.Message;
        }
    }
}