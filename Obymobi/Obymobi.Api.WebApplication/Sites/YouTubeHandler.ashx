﻿<%@ WebHandler Language="C#" Class="YouTubeHandler" %>

using System;
using System.Web;
using Dionysos;
using Dionysos.Web;

public class YouTubeHandler : IHttpHandler {

    public const string outputHtml = "";
    
    public void ProcessRequest (HttpContext context) {
        context.Response.ContentType = "text/html";
        
        string autoHide = "1";
        string autoPlay = "1";
        string captions = "0";
        string loop = "0";
        string width;
        if (!QueryStringHelper.TryGetValue("width", out width)) width = "100%";
        string height;
        if (!QueryStringHelper.TryGetValue("height", out height)) height = "100%";
        
        bool dummyValue;
        if (QueryStringHelper.TryGetValue("autoHide", out dummyValue)) { autoHide = dummyValue ? "1" : "0"; }
        if (QueryStringHelper.TryGetValue("autoPlay", out dummyValue)) { autoPlay = dummyValue ? "1" : "0"; }
        if (QueryStringHelper.TryGetValue("captions", out dummyValue)) { captions = dummyValue ? "1" : "0"; }
        if (QueryStringHelper.TryGetValue("loop", out dummyValue)) { loop = dummyValue ? "1" : "0"; }
        
        string videoId;
        if (!QueryStringHelper.TryGetValue("videoId", out videoId) || videoId.IsNullOrWhiteSpace()) { videoId = "z5zI_6oT0Mc"; }

        string protocol = context.Request.IsSecureConnection ? "https" : "http";
        
        System.Text.StringBuilder sb = new System.Text.StringBuilder();
        sb.Append("<!DOCTYPE html><html lang=\"en\"><head><style>html, body, div#filmbox {width:100%;height:100%;margin:0;padding:0;font-size:0px;}</style><meta charset=\"utf-8\"><title>YouTubePlayer</title>");
        sb.Append("</head><body><div id=\"filmbox\">");
        sb.AppendFormatLine("<iframe title=\"Video\" ");
        sb.AppendFormatLine("src=\"{0}://www.youtube.com/embed/{1}?wmode=transparent&autohide={2}&autoplay={3}&cc_load_policy={4}&loop={5}&rel=0\"  ", protocol, videoId, autoHide, autoPlay, captions, loop);
        sb.AppendFormatLine("scrolling=\"no\" ");
        sb.AppendFormatLine("frameborder=\"0\"");
        sb.AppendFormatLine("width=\"{0}\" ", width);
        sb.AppendFormatLine("height=\"{0}\" ", height);
        sb.AppendFormatLine("style=\"border:none;\">");
        sb.AppendFormatLine("</iframe></div>");        
        sb.Append("</body></html>");
        context.Response.Write(sb.ToString());
        
    }
 
    public bool IsReusable {
        get {
            return false;
        }
    }

}