﻿<%@ WebHandler Language="C#" Class="MapsHandler" %>

using System;
using System.Web;
using System.Globalization;
using Dionysos;
using Dionysos.Web;

public class MapsHandler : IHttpHandler {
    
    public void ProcessRequest (HttpContext context) {
        context.Response.ContentType = "text/html";
        string width;
        if (!QueryStringHelper.TryGetValue("width", out width)) width = "100%";
        string height;
        if (!QueryStringHelper.TryGetValue("height", out height)) height = "100%";
        
        string markerPosition;
        if (!QueryStringHelper.TryGetValue("markerPosition", out markerPosition)) markerPosition = "52.038848,-0.760651"; // What's there? ;) Ask Mr Erik/Bill.
        int zoomLevel;
        if (!QueryStringHelper.TryGetValue("zoomLevel", out zoomLevel)) zoomLevel = 15;
        string markerLabel;
        if (!QueryStringHelper.TryGetValue("markerLabel", out markerLabel)) markerLabel = "Random Location";
        
        // Check for coordinates (should become regex!)
        string[] coordinates = markerPosition.Split(',', StringSplitOptions.RemoveEmptyEntries);
        double longitude = 0, latitude = 0;
        bool coordinatesValid = false;
        if (coordinates.Length == 2)
        {
            if (double.TryParse(coordinates[0], NumberStyles.Number, CultureInfo.InvariantCulture, out latitude) &&
                double.TryParse(coordinates[1], NumberStyles.Number, CultureInfo.InvariantCulture, out longitude))
            {
                coordinatesValid = true;
            }
        }

        string mapScript = "<script type=\"text/javascript\" src=\"https://maps.googleapis.com/maps/api/js?key={0}&sensor=false\"></script>".FormatSafe(Dionysos.Global.ConfigurationProvider.GetString(DionysosWebConfigurationConstants.GoogleMapsApiKey));
        System.Text.StringBuilder sb = new System.Text.StringBuilder();        
        sb.Append("<!DOCTYPE html><html lang=\"en\"><head><style>html, body {width:100%;height:100%;margin:0;padding:0;font-size:0px;}</style><meta charset=\"utf-8\"><title>Google Map</title>");
        sb.Append(mapScript);
        sb.AppendFormatLine("</head><body><div id=\"map-canvas\" style=\"width: {0}; height: {1}\"></div>", width, height);
        sb.AppendFormatLine("<script>");
        sb.AppendFormatLine("google.maps.event.addDomListener(window, 'load', initialize);");
        sb.AppendFormatLine("function initialize() {");
        if (coordinatesValid)
        {
            sb.AppendFormatLine("  var mapOptions = {");
            sb.AppendFormatLine("    zoom: {0},", zoomLevel);
            if (coordinatesValid)
                sb.AppendFormatLine("    center: new google.maps.LatLng({0}, {1})", latitude.ToString(CultureInfo.InvariantCulture), longitude.ToString(CultureInfo.InvariantCulture));
            sb.AppendFormatLine("  };");
            sb.AppendFormatLine("");
            sb.AppendFormatLine("  map = new google.maps.Map(document.getElementById('map-canvas'),");
            sb.AppendFormatLine("      mapOptions);");
            sb.AppendFormatLine("        drawMarker(new google.maps.LatLng({0}, {1}));", latitude.ToString(CultureInfo.InvariantCulture), longitude.ToString(CultureInfo.InvariantCulture));
        }
        else
        {
            sb.AppendFormatLine("geocoder = new google.maps.Geocoder();");
            sb.AppendFormatLine("geocoder.geocode({ 'address': '" + markerPosition + "' }, function (results, status) {");
            sb.AppendFormatLine("    if (status == google.maps.GeocoderStatus.OK) {");
            sb.AppendFormatLine("        ");
            sb.AppendFormatLine("");
            sb.AppendFormatLine("        var mapOptions = {");
            sb.AppendFormatLine("            zoom: {0},", zoomLevel);
            sb.AppendFormatLine("            center: results[0].geometry.location");
            sb.AppendFormatLine("        };");
            sb.AppendFormatLine("");
            sb.AppendFormatLine("        map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);");
            sb.AppendFormatLine("");
            sb.AppendFormatLine("        drawMarker(results[0].geometry.location);");
            sb.AppendFormatLine("    } else {");
            sb.AppendFormatLine("        alert(\"Geocode was not successful for the following reason: \" + status);");
            sb.AppendFormatLine("    }");
            sb.AppendFormatLine("});");
        }

        sb.AppendFormatLine("}");
        sb.AppendFormatLine("function drawMarker(positionParam)");
        sb.AppendFormatLine("{");
        sb.AppendFormatLine("    var marker = new google.maps.Marker({");
        sb.AppendFormatLine("        map: map,");
        sb.AppendFormatLine("        title: '{0}',", markerLabel);
        sb.AppendFormatLine("        position: positionParam");
        sb.AppendFormatLine("    });");
        sb.AppendFormatLine("}        ");
        sb.AppendFormatLine("");
        sb.AppendFormatLine("</script>");
        sb.Append("</body></html>");
        context.Response.Write(sb.ToString());        
    }
 
    public bool IsReusable {
        get {
            return false;
        }
    }

}