﻿using Dionysos;
using Obymobi.Enums;
using Obymobi.Logic.HelperClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class App_RequestPasswordReset : System.Web.UI.Page
{
    // Translatable strings (to be used via Resources, or better via the database later)
    string resource_EmailAddressUnknown = "There's no account linked to the supplied email address.";
    string resource_Blacklisted = "Your account is blocked, contact support for more information.";
    string resource_EmailAddressIncorrectFormat = "The email address is not in a recognized format.";
    string resource_YouHaveToEnterYouEmailAddress = "You have to enter your email address to reset your password.";    
    string resource_WeAreExperiencingIssues = "We're currently experiencing some issues. Please try again later.";

    protected void Page_Load(object sender, EventArgs e)
    {
        this.btResetPassword.Click += btResetPassword_Click;
    }

    void btResetPassword_Click(object sender, EventArgs e)
    {
        // Validate
        this.plhError.Visible = false;
        this.plhSuccess.Visible = false;
        this.plhTooManyRequests.Visible = false;
        if (this.Validate())
        {
            try
            {
                Obymobi.Web.HelperClasses.CustomerHelperWeb.ProcessResetPasswordRequest(this.tbEmail.Text);

                this.plhSuccess.Visible = true;
                this.btResetPassword.Visible = false;
                this.tbEmail.Visible = false;
                this.plhInstructions.Visible = false;
            }
            catch (Obymobi.ObymobiException oex)
            {
                if (oex.ErrorEnumValue is RequestPasswordResetResult)
                {
                    switch ((RequestPasswordResetResult)oex.ErrorEnumValue)
                    {
                        case RequestPasswordResetResult.Success:
                            // Can't happen, because it won't throw an exception.
                            break;
                        case RequestPasswordResetResult.EmailAddressUnknown:
                            this.lblError.Text = this.resource_EmailAddressUnknown;
                            break;
                        case RequestPasswordResetResult.Blacklisted:
                            this.lblError.Text = this.resource_Blacklisted;
                            break;
                        case RequestPasswordResetResult.EmailAddressIncorrectFormat:
                            this.lblError.Text = this.resource_EmailAddressIncorrectFormat;
                            break;
                        case RequestPasswordResetResult.TooManyPasswordResetRequests:
                            this.plhTooManyRequests.Visible = true;
                            break;
                        case RequestPasswordResetResult.NoEmailOrPhonenumberSet:
                        case RequestPasswordResetResult.NoPasswordResetForAnonymousAccounts:
                        case RequestPasswordResetResult.Failure:
                        case RequestPasswordResetResult.InvalidHash:
                        case RequestPasswordResetResult.Unknown:
                        default:
                            // All error that 'could not happen' or we can't give a good explanation for to
                            // the Customer
                            this.lblError.Text = this.resource_WeAreExperiencingIssues;
                            break;
                    }
                }
            }
            catch
            {
                this.lblError.Text = this.resource_WeAreExperiencingIssues;
            }

            this.plhError.Visible = !this.lblError.Text.IsNullOrWhiteSpace();
        }
    }

    private new bool Validate()
    {
        bool toReturn = false;

        if (this.tbEmail.Text.IsNullOrWhiteSpace())
        {
            this.lblError.Text = this.resource_YouHaveToEnterYouEmailAddress;
        }
        else if (!Dionysos.Text.RegularExpressions.RegEx.IsEmail(this.tbEmail.Text))
        {
            this.lblError.Text = this.resource_EmailAddressIncorrectFormat;
        }
        else
            this.lblError.Text = string.Empty;

        if (!this.lblError.Text.IsNullOrWhiteSpace())
            this.divEmail.Attributes.Add("class", "control-group error");
        else
        {
            this.divEmail.Attributes.Add("class", "control-group");
            toReturn = true;
        }

        this.plhError.Visible = !toReturn;

        return toReturn;
    }
}