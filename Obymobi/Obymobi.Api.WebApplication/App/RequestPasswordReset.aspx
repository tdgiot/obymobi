﻿<%@ Page Title="Crave - Reset Password" Language="C#" MasterPageFile="~/App/MasterPages/AppBase.master" AutoEventWireup="true" Inherits="App_RequestPasswordReset" Codebehind="RequestPasswordReset.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cplhBody" runat="Server">
    <div class="container">
        <form id="form1" runat="server" class="form-signin">            
            <D:HyperLink ID="hlLogo" runat="server" NavigateUrl="~/App/" ImageUrl="~/App/img/crave-logo.png" />
            <h2><D:LabelTextOnly runat="server" ID="lblTitle">Reset password</D:LabelTextOnly></h2>
            <D:PlaceHolder runat="server" ID="plhInstructions">
                <p><D:LabelTextOnly runat="server" ID="lblInstructions">Use the email address of your account to reset your password.</D:LabelTextOnly></p>
            </D:PlaceHolder>
            <D:PlaceHolder runat="server" ID="plhError" Visible="false">
                <div class="alert alert-error">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>                    
                    <D:LabelTextOnly runat="server" ID="lblError"></D:LabelTextOnly>
                </div>
            </D:PlaceHolder>
            <D:PlaceHolder runat="server" ID="plhTooManyRequests" Visible="false">
                <div class="alert alert-info">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>                    
                    <D:LabelTextOnly runat="server" ID="lblTooManyRequests">We've send you a password reset email less than 10 minutes ago, check your inbox and/or spam filter. You can request a new password reset email in 10 minutes.</D:LabelTextOnly> 
                </div>
            </D:PlaceHolder>
            <D:PlaceHolder runat="server" ID="plhSuccess" Visible="false">
                <div class="alert alert-success">                    
                    <D:LabelTextOnly runat="server" ID="lblPasswordIsResetMailSent"><strong>Success!</strong> An email has been sent to reset your password.</D:LabelTextOnly>
                </div>
            </D:PlaceHolder>
            <div runat="server" id="divEmail" class="control-group">
                <D:TextBoxString runat="server" ID="tbEmail" placeholder="email address" autocomplete="off" CssClass="input-block-level" />
            </div>
            <D:Button runat="server" ID="btResetPassword" Text="Reset Password" CssClass="btn btn-large btn-success btn-block" />
            <a class="btn btn-large btn-block" href="default.aspx" style="margin-top:20px">Back to Home</a>
        </form>
    </div>
</asp:Content>

