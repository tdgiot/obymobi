﻿<%@ Page Title="Crave - Reset Password" Language="C#" MasterPageFile="~/App/MasterPages/AppBase.master" AutoEventWireup="true" Inherits="App_ResetPassword" Codebehind="ResetPassword.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cplhBody" runat="Server">
    <div class="container">
        <form id="form1" runat="server" class="form-signin">            
            <D:HyperLink ID="hlLogo" runat="server" NavigateUrl="~/App/" ImageUrl="~/App/img/crave-logo.png" />
            <h2><D:LabelTextOnly runat="server" ID="lblTitle">Update password</D:LabelTextOnly></h2>
            <D:PlaceHolder runat="server" ID="plhInstructions">
                <p><D:LabelTextOnly runat="server" ID="lblInstructions">You can now change your password by entering your new password twice below.</D:LabelTextOnly></p>
            </D:PlaceHolder>
            <D:PlaceHolder runat="server" ID="plhError" Visible="false">
                <div class="alert alert-error">                    
                    <D:LabelTextOnly runat="server" ID="lblError"></D:LabelTextOnly>
                </div>
            </D:PlaceHolder>
            <D:PlaceHolder runat="server" ID="plhSuccess" Visible="false">
                <div class="alert alert-success">
                    <D:LabelTextOnly runat="server" ID="lblPasswordUpdated"><strong>Success!</strong> Your password has been updated.</D:LabelTextOnly>
                </div>
            </D:PlaceHolder>
            <D:PlaceHolder runat="server" ID="plhForm">
                <div runat="server" id="divPassword" class="control-group">
                    <D:TextBoxPassword runat="server" ID="tbPassword1" placeholder="password" autocomplete="off" CssClass="input-block-level" />
                    <D:TextBoxPassword runat="server" ID="tbPassword2" placeholder="confirm password" autocomplete="off" CssClass="input-block-level" />
                </div>
                <D:Button runat="server" ID="btUpdatePassword" Text="Update Password" CssClass="btn btn-large btn-success btn-block" />
            </D:PlaceHolder>                               
            <!--<a href="http://dev.crave-emenu.com/api/App/ConfirmEmail.aspx?a=8bee7610-d03c-4d40-92b4-e284db89b3ef" id="test" runat="server">Confirm Email</a><br />
            <a href="http://dev.crave-emenu.com/api/App/ResetPassword.aspx?a=3001&b=f71a4afda24341cbab679004a86b4b07" id="test2" runat="server">Reset Password</a>    -->                 
        </form>        
    </div>
</asp:Content>

