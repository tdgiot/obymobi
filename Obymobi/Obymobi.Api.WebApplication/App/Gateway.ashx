﻿<%@ WebHandler Language="C#" Class="Gateway" %>

using System;
using System.Web;
using System.Collections.Generic;
using Dionysos;
using Dionysos.Globalization;
using Dionysos.SMS;
using Dionysos.Web;
using Obymobi;
using Obymobi.Api.Logic.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Logging;
using Obymobi.Logic.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

public class Gateway : IHttpHandler 
{    
    public enum GatewayResult
    {
        SendingSmsFailed = 200,
    }
    
    public enum GatewayType
    {
        TextMarketer = 100,
    }    
    
    public void ProcessRequest (HttpContext context) 
    {
        context.Response.StatusCode = (int)System.Net.HttpStatusCode.OK;
        context.Response.ContentType = "text/plain";

        var isVerbose = QueryStringHelper.HasValue("verbose");
        var isNoSms = QueryStringHelper.HasValue("nosms");
        string gatewayTypeStr = QueryStringHelper.GetString("gateway", true);
        string gatewayKeyword = QueryStringHelper.GetString("keyword", true);
        string phoneNumber = string.Empty;
        string txtMessage = string.Empty;
        
        // Check if gateway is set
        int gatewayTypeInt;
        if (StringUtil.IsNullOrWhiteSpace(gatewayTypeStr))
        {
            this.Log("Gateway type not specified");
            context.Response.StatusCode = (int)System.Net.HttpStatusCode.BadRequest;
            context.Response.Write("Gateway type not specified");
            
            return;
        }
        else if (!(int.TryParse(gatewayTypeStr, out gatewayTypeInt) && Enum.IsDefined(typeof(GatewayType), gatewayTypeInt)))
        {
            this.Log("Invalid gatway type: " + gatewayTypeStr);
            context.Response.StatusCode = (int)System.Net.HttpStatusCode.BadRequest;
            context.Response.Write("Invalid gatway type: " + gatewayTypeStr);
            return;            
        }
        
        GatewayType gatewayType = gatewayTypeStr.ToEnum<GatewayType>();
        
        // Verify the Ip Address                
        string requesterIp;
        if (!this.ValidGatewayIp(gatewayType, out requesterIp))
        {            
            this.NotifyAboutInvalidIpAddress(gatewayType, requesterIp);            
            context.Response.StatusCode = (int)System.Net.HttpStatusCode.BadRequest;
            context.Response.Write("BadRequest: Ip Address '{0}' it not allowed.".FormatSafe(requesterIp));
            return;            
        }
        
        
        if (gatewayType == GatewayType.TextMarketer)
        {
            phoneNumber = QueryStringHelper.GetString("number", true);
            txtMessage = QueryStringHelper.GetString("message", true);
        }

        // Sanitized input 
        phoneNumber = phoneNumber.Trim();
        txtMessage = txtMessage.Trim().ToLower();        
        string cleanPhonenumber;
        
        if (StringUtil.IsNullOrWhiteSpace(phoneNumber) || StringUtil.IsNullOrWhiteSpace(txtMessage))
        {
            this.Log("BadRequest: Missing query parameters for number and/or message");
            context.Response.StatusCode = (int)System.Net.HttpStatusCode.BadRequest;
            context.Response.Write("BadRequest: Missing query parameters for number and/or message");
        }
        else if (!CountryInformationUtil.TryStandardizePhonenumber(phoneNumber, out cleanPhonenumber))
        {
            this.Log("BadRequest: Invalid phone number " + phoneNumber);
            context.Response.StatusCode = (int)System.Net.HttpStatusCode.BadRequest;
            context.Response.Write("BadRequest: Invalid phone number " + phoneNumber);
        }
        else
        {
            // Strip keywords from txt message
            txtMessage = StripKeywords(txtMessage, gatewayKeyword);

            // Create customer and generate URL
            var smsKeywordEntity = SmsInformationHelper.GetKeywordEntityForKeyword(txtMessage);
            var smsInformationEntity = smsKeywordEntity.SmsInformationEntity;

            var sourceName = GetSourceNameForKeywordEntity(smsKeywordEntity);
            var customerGUID = CreateCustomer(cleanPhonenumber);

            this.Log("Processed message: '{0}' from '{1}' ended as customer '{2}'", txtMessage, cleanPhonenumber, customerGUID);

            string smsUrl;
            bool hasShortUrl = CreateCraveUrl(sourceName, txtMessage, customerGUID, out smsUrl);

            try
            {
                if (!isNoSms)
                {
                    int messageLength = 2 + (smsInformationEntity.BodyText.Length + smsUrl.Length);
                    string responseText;
                    if (!hasShortUrl && messageLength > 160)
                        responseText = string.Format("{0}: {1}", "Get CraveWorld: ", smsUrl);
                    else
                        responseText = string.Format("{0}: {1}", smsInformationEntity.BodyText, smsUrl);
                        
                    // Send SMS back to customer
                    SendText(smsInformationEntity, cleanPhonenumber, responseText);
                }

                context.Response.Write("OK");
                if (TestUtil.IsPcDeveloper || isVerbose)
                {
                    context.Response.Write(string.Format(" (t={0}, m={1}, guid={2}, smsOriginator={3}, smsBody={4}, smsUrl={5})", cleanPhonenumber, txtMessage, customerGUID, smsInformationEntity.Originator, smsInformationEntity.BodyText, smsUrl));
                }
            }
            catch (Exception ex)
            {
                this.Log("InternalServerError: Failed to send SMS: " + ex.GetAllMessages(true));
                Requestlogger.UpdateRequestLog(Obymobi.Enums.GenericWebserviceCallResult.Failure, "SMS Failure", ex);
                context.Response.StatusCode = (int)System.Net.HttpStatusCode.InternalServerError;
                context.Response.Write("InternalServerError: Failed to send SMS");
            }
        }
    }

    private static DateTime lastInvalidIpAddressEmail = DateTime.MinValue;
    public void NotifyAboutInvalidIpAddress(GatewayType gateway, string ipAddress)
    {
        if(Gateway.lastInvalidIpAddressEmail < DateTime.Now.AddMinutes(-15))
        {
            SupportNotificationSendHelper sender = new SupportNotificationSendHelper("no-reply@crave-emenu.com", "{0} SMS Gateway".FormatSafe(WebEnvironmentHelper.CloudEnvironment), string.Empty);
            var smsRecievers = new System.Collections.Generic.List<string>(){};
            List<string> emailReceivers = SupportpoolHelper.GetSystemSupportpoolEmailaddresses();        
            string message = "Received a request for Gateway of type '{0}' from ipAddress '{1}', which is not allowed.".FormatSafe(gateway, ipAddress);
            this.Log(message);            
            
            string dummy;
            sender.SendSupportNotification(smsRecievers, emailReceivers, message, out dummy);
            Gateway.lastInvalidIpAddressEmail = DateTime.Now;
        }
    }    

    public bool ValidGatewayIp(GatewayType gateway, out string requesterIp)
    {
        requesterIp = this.GetRequesterIp();                
        
        // Always allow the Dutch office
        if (HttpContext.Current.Request.IsLocal && !TestUtil.IsPcGabriel) // Local == Always OK.
            return true;
        
        // Office IP's
        string officeIps = Dionysos.ConfigurationManager.GetString(ObymobiConfigConstants.OfficeIpAddresses);
        if (officeIps.Contains(requesterIp, StringComparison.InvariantCultureIgnoreCase))
            return true;       
        
        // Per gateway choose the allowed ips
        switch (gateway)
        {
            case GatewayType.TextMarketer:
                return "162.13.37.72, 162.13.37.73, 162.13.37.74, 162.13.37.127".Contains(requesterIp, StringComparison.InvariantCultureIgnoreCase);                
            default:
                throw new NotImplementedException("ValidGatewayIp not implemented for Gateway: " + gateway);                
        }                
    }

    public String GetRequesterIp()
    {
        HttpContext context = HttpContext.Current;
        string visitorIPAddress = context.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

        if (string.IsNullOrEmpty(visitorIPAddress))
            visitorIPAddress = context.Request.ServerVariables["REMOTE_ADDR"];

        if (string.IsNullOrEmpty(visitorIPAddress))
            visitorIPAddress = context.Request.UserHostAddress;

        if (visitorIPAddress.Contains(":"))
            visitorIPAddress = visitorIPAddress.Substring(0, visitorIPAddress.IndexOf(":"));

        return visitorIPAddress.Trim();
    }        

    public bool IsReusable 
    {
        get
        {
            return false;
        }
    }
    
    private string StripKeywords(string txtMessage, string gatewayKeyword)
    {
        if (gatewayKeyword.Length == 0)
        {
            if (txtMessage.StartsWith("crave world"))
                txtMessage = txtMessage.Replace("crave world", "");
            else if (txtMessage.StartsWith("crave"))
                txtMessage = txtMessage.Replace("crave", "");
        }
        else
        {
            txtMessage = txtMessage.Replace(gatewayKeyword, "");
        }
        
        txtMessage = txtMessage.Trim();
        if (txtMessage.Length == 0)
            txtMessage = "crave";

        return txtMessage;
    }

    private string CreateCustomer(string phonenumber)
    {
        // Check if there's already a customer with this phone number
        var customerEntity = CustomerHelper.GetCustomer(phonenumber);
        if (customerEntity == null || customerEntity.IsNew)
        {
            var emailAddress = string.Empty;
            var questStarted = DateTime.Now;
            var customers = new CustomerCollection();
            while ((DateTime.Now - questStarted).TotalSeconds < 5)
            {
                var filter = new PredicateExpression();
                filter.Add(CustomerFields.Email%emailAddress);
                if (customers.GetDbCount(filter) == 0)
                    break;

                emailAddress = RandomUtil.GetRandomEmail();
            }

            customerEntity = new CustomerEntity();
            customerEntity.Phonenumber = phonenumber;
            customerEntity.Email = emailAddress;
            customerEntity.AnonymousAccount = true;
            customerEntity.CanSingleSignOn = true;
            customerEntity.Save();
        }
        else if (customerEntity.AnonymousAccount)
        {
            customerEntity.CanSingleSignOn = true;
            customerEntity.Save();
        }
        
        // Return UUID of customer
        return customerEntity.GUID;
    }

    private bool CreateCraveUrl(string sourceName, string message, string customerGuid, out string shortUrl)
    {
        var urlAddition = string.Format("App/Download.aspx?n=Hotels&s={0}&m=sms&c={1}&u={2}", message, sourceName, customerGuid).Replace(" ", "%20");
        var fullUrl = WebEnvironmentHelper.GetApiUrl(urlAddition);

        try
        {
            var shortener = new Dionysos.Web.Google.UrlShortener().Shorten(new Uri(fullUrl));
            shortUrl = shortener.Id;
        }
        catch
        {
            shortUrl = string.Empty;
        }

        if (shortUrl.Length == 0)
            shortUrl = fullUrl;

        return (shortUrl != fullUrl);
    }

    private void SendText(SmsInformationEntity smsInformation, string number, string responseText)
    {
        var smsRequest = new MollieSMSRequest();
        
        //Gatewaynaam	Gateway ID
        //Basic	        2
        //Business	    4
        //Business+	    1
        //Landline	    8
        smsRequest.Gateway = "4";
        smsRequest.Originator = smsInformation.Originator;
        smsRequest.Recipients = number;
        smsRequest.Message = responseText;
        smsRequest.Send();

        if (smsRequest.ResultCode != 10)
            throw new ObymobiException(GatewayResult.SendingSmsFailed, "Sms result: {0}, Number: {1}", smsRequest.ResultMessage, number);    
    }
    
    private string GetSourceNameForKeywordEntity(SmsKeywordEntity entity)
    {
        if (entity.SmsInformationEntity.IsDefault)
            return "Crave";
        
        return entity.CompanyEntity.Name;
    }
    
    public static Dionysos.Logging.AsyncLoggingProvider loggingProvider = null; 
    
    public static Dionysos.Logging.AsyncLoggingProvider LoggingProvider
    {
        get
        {
            if (Gateway.loggingProvider == null)
            {
                Gateway.loggingProvider = new Dionysos.Logging.AsyncLoggingProvider(System.Web.Hosting.HostingEnvironment.MapPath("~/App_Data/Logs/"), "SmsGateway", 14, "SmsGateway");
            }

            return Gateway.loggingProvider;
        }            
    }    
    
    private void Log(string format, params object[] args)
    {
        Gateway.LoggingProvider.Log(LoggingLevel.Info, format.FormatSafe(args));    
        Gateway.LoggingProvider.Log(LoggingLevel.Info, "Url: " + HttpContext.Current.Request.RawUrl);    
    }    
}