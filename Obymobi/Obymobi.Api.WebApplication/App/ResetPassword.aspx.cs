﻿using Dionysos.Web;
using Dionysos;
using Obymobi.Data.EntityClasses;
using Obymobi.Logic.HelperClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Obymobi.Enums;

public partial class App_ResetPassword : System.Web.UI.Page
{
    // Translatable strings (to be used via Resources, or better via the database later)
    string resource_ResetLinkInvalid = "The link used to reset you password is invalid.<br />You can request a new link <a href=\"RequestPasswordReset.aspx\">here</a>.";
    string resource_YouMustEnterPasswordTwice = "You must enter your password twice.";
    string resource_PasswordsDontMatch = "The entered passwords do not match.";
    string resource_YouCannotUseSpaces = "You can not use spaces in your password.";
    string resource_YourPasswordMustBeAtLeastEightCharacters = "Your password must be at least 8 characters long.";
    string resource_PasswordResetLinkExpired = "The link used to reset your password has expired.<br />You can request a new link <a href=\"RequestPasswordReset.aspx\">here</a>.";
    string resource_TooManyFailedAttempts = "You've performed too many invalid attemps, you must request a new link <a href=\"RequestPasswordReset.aspx\">here</a>.";
    string resource_WeAreExperiencingIssues = "We're currently experiencing some issues. Please try again later.";


    private CustomerEntity customer;
    private string resetIdentifier;
    private int customerId;

    // http://app.crave-emenu.com/mobile/resetPassword.aspx??a=1699&b=c60361e6f4584c4db0799c1b25322821
    protected void Page_Load(object sender, EventArgs e)
    {
        this.ValidateRequest();
        this.btUpdatePassword.Click+=btUpdatePassword_Click;
    }

    void ValidateRequest()
    {
        int customerId;
        string resetIdentifier;
        this.divPassword.Attributes.Add("class", "control-group");                                            
        if (!QueryStringHelper.TryGetValue("a", out customerId) || !QueryStringHelper.TryGetValue("b", out resetIdentifier))
        {
            this.lblError.Text = this.resource_ResetLinkInvalid;            
        }
        else
        {
            this.customerId = customerId;
            this.resetIdentifier = resetIdentifier;
            // Get the Customer
            try
            {
                string passwordHash = string.Empty;
                this.lblError.Text = string.Empty;
                if (!this.IsPostBack)
                { 
                    // Initial request.
                }
                else if (this.tbPassword1.Text.IsNullOrWhiteSpace() || this.tbPassword2.Text.IsNullOrWhiteSpace())
                { 
                    this.lblError.Text = this.resource_YouMustEnterPasswordTwice;
                }
                else if (!this.tbPassword1.Text.Equals(this.tbPassword2.Text))
                {
                    this.lblError.Text = this.resource_PasswordsDontMatch;
                }
                else if(this.tbPassword1.Text.Contains(" "))
                {
                    this.lblError.Text = this.resource_YouCannotUseSpaces;
                }
                else if (this.tbPassword1.Text.Length < 8)
                {
                    this.lblError.Text = this.resource_YourPasswordMustBeAtLeastEightCharacters;
                }
                else
                {
                    passwordHash = Obymobi.Security.CustomerPasswordHelper.GetPasswordTransferHash(this.tbPassword1.Text);
                }

                if (!this.lblError.Text.IsNullOrWhiteSpace())
                {
                    this.divPassword.Attributes.Add("class", "control-group error");                                            
                    this.plhError.Visible = true;
                }

                this.customer = CustomerHelper.ResetPassword(customerId, passwordHash, resetIdentifier);

                if (!passwordHash.IsNullOrWhiteSpace())
                { 
                    // Password is now updated.
                    this.plhSuccess.Visible = true;
                    this.plhForm.Visible = false;
                    this.plhInstructions.Visible = false;
                }
            }
            catch (Obymobi.ObymobiException oex)
            {
                if (oex.ErrorEnumValue is ResetPasswordResult)
                {
                    switch ((ResetPasswordResult)oex.ErrorEnumValue)
                    {                                                    
                        case ResetPasswordResult.Success:
                            break;
                        case ResetPasswordResult.CustomerIdUnknown:
                        case ResetPasswordResult.ResetPasswordIdentifierInvalid:
                            this.lblError.Text = this.resource_ResetLinkInvalid;
                            break;                                                    
                        case ResetPasswordResult.ResetPasswordIdentifierExpired:
                            this.lblError.Text = this.resource_PasswordResetLinkExpired;
                            break;
                        case ResetPasswordResult.TooManyFailedAttempts:
                            this.lblError.Text = this.resource_TooManyFailedAttempts;
                            break;                                                
                        case ResetPasswordResult.Failure:
                        case ResetPasswordResult.Unspecified:
                        case ResetPasswordResult.InvalidHash:                            
                        default:
                            this.lblError.Text = this.resource_WeAreExperiencingIssues;
                            break;
                    }
                }
                else
                    this.lblError.Text = this.resource_WeAreExperiencingIssues;
            }
            catch
            {
                this.lblError.Text = this.resource_WeAreExperiencingIssues;
            }
        }

        if (!this.lblError.Text.IsNullOrWhiteSpace())
        {
            this.plhError.Visible = true;
            if (!this.IsPostBack)
            {
                this.plhForm.Visible = false;
                this.plhInstructions.Visible = false;
            }
        }
        else if(!this.IsPostBack)
        {
            this.plhInstructions.Visible = true;
            this.plhForm.Visible = true;
        }
    }

    void btUpdatePassword_Click(object sender, EventArgs e)
    {

    }
}