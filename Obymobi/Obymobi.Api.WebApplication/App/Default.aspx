﻿<%@ Page Title="Crave - Hotel Guide, In room Ordering and City Guide" Language="C#" MasterPageFile="~/App/MasterPages/AppBase.master" AutoEventWireup="true" Inherits="App_Default" Codebehind="Default.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cplhBody" runat="Server">
    <div class="container">
        <form id="form1" runat="server" class="form-signin">            
            <img src="img/crave-logo.png" />
            <a class="btn btn-large btn-block btn-success" href="Download.aspx">Download</a>                  
            <a class="btn btn-large btn-block" href="CreateAccount.aspx">Create an Account</a>                              
            <a class="btn btn-large btn-block" href="RequestPasswordReset.aspx">Reset Password</a>            
        </form>
    </div>
</asp:Content>

