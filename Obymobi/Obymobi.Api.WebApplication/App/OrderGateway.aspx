﻿<%@ Page Title="Crave - Update Order" Language="C#" MasterPageFile="~/App/MasterPages/AppBase.master" AutoEventWireup="true" Inherits="OrderGateway" Codebehind="OrderGateway.aspx.cs" %>
<asp:Content ID="Content2" ContentPlaceHolderID="cplhHeader" runat="Server">    
        <D:PlaceHolder runat="server" ID="plhNotificationSuccess" Visible="false">
            <div class="alert alert-success">
                <a class="close" data-dismiss="alert">×</a>
                <D:LabelTextOnly runat="server" ID="lblNotificationSuccess"></D:LabelTextOnly>
            </div>    
        </D:PlaceHolder>
        <D:PlaceHolder runat="server" ID="plhNotificationError" Visible="false">
            <div class="alert alert-error">
                <a class="close" data-dismiss="alert">×</a>
                <D:LabelTextOnly runat="server" ID="lblNotificationError"></D:LabelTextOnly>
            </div>
        </D:PlaceHolder>        
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="cplhBody" runat="Server">    
    <div class="container">        
        <form id="form1" runat="server" class="form-signin">            
            <D:HyperLink ID="hlLogo" runat="server" NavigateUrl="https://crave-emenu.com/" Target="_blank" ImageUrl="~/App/img/crave-logo.png" />
            <h2><D:LabelTextOnly runat="server" ID="lblTitle">Order status</D:LabelTextOnly></h2>
            <D:PlaceHolder runat="server" ID="plhInstructions" Visible="false">
                <p><D:LabelTextOnly runat="server" ID="lblInstructions">Use the buttons below to update the order status.</D:LabelTextOnly></p><br/>
            </D:PlaceHolder>            
            <D:PlaceHolder runat="server" ID="plhInformation" Visible="false">
                <div class="alert alert-info" style="padding: 8px 10px 8px 4px;">
                    <D:PlaceHolder runat="server" ID="plhOrder" ></D:PlaceHolder>
                </div>
            </D:PlaceHolder>       
            <D:PlaceHolder runat="server" ID="plhStatusButtons" Visible="true">
                <div>
                <D:Button runat="server" ID="btOnTheCase" Text="On the case" Enabled="false" CssClass="btn btn-large btn-block" /><br/>
                <D:Button runat="server" ID="btCompleted" Text="Completed" Enabled="false" CssClass="btn btn-large btn-success btn-block" />
                    </div>
            </D:PlaceHolder>                                    
        </form>
    </div>
</asp:Content>