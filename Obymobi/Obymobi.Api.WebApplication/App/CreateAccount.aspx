﻿<%@ Page Title="Crave - Create Account" Language="C#" MasterPageFile="~/App/MasterPages/AppBase.master" AutoEventWireup="true" Inherits="App_CreateAccount" Codebehind="CreateAccount.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cplhBody" Runat="Server">
        <div class="container">
        <form id="form1" runat="server" class="form-signin">
            <D:HyperLink ID="hlLogo" runat="server" NavigateUrl="~/App/" ImageUrl="~/App/img/crave-logo.png" />
            <h2><D:LabelTextOnly runat="server" ID="lblTitle">Create an Account</D:LabelTextOnly></h2>
            <D:PlaceHolder runat="server" ID="plhUseCraveWithoutAccount">
                <div class="alert alert-info" runat="server">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>                    
                    <D:LabelTextOnly runat="server" ID="lblYouCanUseCraveWithoutAnAccount">You can use Crave without an account, some functions do require an account.</D:LabelTextOnly>
                </div>                
            </D:PlaceHolder>
            <D:PlaceHolder runat="server" ID="plhError" Visible="false">
                <div class="alert alert-error">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>                    
                    <D:LabelTextOnly runat="server" ID="lblError"></D:LabelTextOnly>
                </div>
            </D:PlaceHolder>
            <D:PlaceHolder runat="server" ID="plhTooManyRequests" Visible="false">
                <div class="alert alert-info">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>                    
                    <D:LabelTextOnly runat="server" ID="lblTooManyRequests">We've send you a password reset email less than 10 minutes ago, check your inbox and/or spam filter. You can request a new password reset email in 10 minutes.</D:LabelTextOnly>
                </div>
            </D:PlaceHolder>
            <D:PlaceHolder runat="server" ID="plhSuccess" Visible="false">
                <div class="alert alert-success">                    
                    <D:LabelTextOnly runat="server" ID="lblAcountCreated"><strong>Account created!</strong> Your account is created and ready to use. Download our app now to start using Crave.</D:LabelTextOnly>
                </div>
            </D:PlaceHolder>
            <D:PlaceHolder runat="server" ID="plhForm">
                <div runat="server" id="divEmail" class="control-group" style="margin-bottom:0px;">
                    <D:TextBoxString runat="server" ID="tbEmail" placeholder="email address" autocomplete="off" CssClass="input-block-level" />
                </div>
                <div runat="server" id="divPassword" class="control-group" style="margin-bottom:0px;">
                    <D:TextBoxPassword runat="server" ID="tbPassword1" placeholder="password" autocomplete="off" CssClass="input-block-level" />
                    <D:TextBoxPassword runat="server" ID="tbPassword2" placeholder="confirm password" autocomplete="off" CssClass="input-block-level" />
                </div>
                <D:Button runat="server" ID="btResetPassword" Text="Create Account" CssClass="btn btn-large btn-success btn-block" />
            </D:PlaceHolder>
            <D:PlaceHolder runat="server" ID="plhDownloadButton" Visible="false">
                <a class="btn btn-large btn-block btn-success" href="Download.aspx">Download</a>
            </D:PlaceHolder>
            <a class="btn btn-large btn-block" href="default.aspx" style="margin-top:20px">Back to Home</a>                 
        </form>
    </div>
</asp:Content>

