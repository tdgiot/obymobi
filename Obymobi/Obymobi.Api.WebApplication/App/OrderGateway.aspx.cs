﻿using System;
using System.Text;
using Dionysos;
using Dionysos.Web;
using Obymobi.Api.Logic.HelperClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Enums;
using Obymobi.Logging;
using Obymobi.Logic.Model;
using Obymobi.Logic.Routing;

public partial class OrderGateway : System.Web.UI.Page
{
    private const string RESOURCE_ORDER_BEING_HANDLED = "The order is being handled.";
    private const string RESOURCE_ORDER_COMPLETED = "The order has been completed.";
    private const string RESOURCE_MISSING_PARAMETERS = "Bad request: Missing parameters.";
    private const string RESOURCE_UNKNOWN_ORDER = "Oops, we can't find the order you're looking for. It's already been completed, or it doesn't exist.";

    public enum OrderGatewayResult
    {
        ParameterMissing = 200,
        InvalidGuid = 201,
        InvalidStatus = 202,
        Failure = 999
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        this.HookUpEvents();

        string orderRoutestephandlerGuid;
        if (!QueryStringHelper.TryGetValue("Guid", out orderRoutestephandlerGuid))
        {
            Requestlogger.UpdateRequestLog(OrderGatewayResult.ParameterMissing, "Missing parameter(s)", null);

            this.lblNotificationError.Text = OrderGateway.RESOURCE_MISSING_PARAMETERS;
            this.plhNotificationError.Visible = true;            
        }
        else
        {
            OrderRoutestephandlerEntity orderRoutestephandler = OrderRoutestephandlerHelper.GetOrderRoutestephandlerByGuid(orderRoutestephandlerGuid);
            if (orderRoutestephandler == null)
            {
                Requestlogger.UpdateRequestLog(OrderGatewayResult.InvalidGuid, "Invalid Guid", null);

                this.lblNotificationError.Text = OrderGateway.RESOURCE_UNKNOWN_ORDER;
                this.plhNotificationError.Visible = true;            
            }
            else if (orderRoutestephandler.Status >= (int)OrderRoutestephandlerStatus.Completed)
            {
                this.lblNotificationError.Text = OrderGateway.RESOURCE_UNKNOWN_ORDER;
                this.plhNotificationError.Visible = true;   

                this.btOnTheCase.Enabled = false;
                this.btCompleted.Enabled = false;
            }
            else
            {
                this.btOnTheCase.Enabled = orderRoutestephandler.Status < (int)OrderRoutestephandlerStatus.BeingHandled;
                this.btCompleted.Enabled = orderRoutestephandler.Status < (int)OrderRoutestephandlerStatus.Completed;

                string orderHtml = this.GetOrderHtmlAsString(orderRoutestephandler.OrderEntity);

                this.plhInstructions.Visible = true;
                this.plhOrder.AddHtml(orderHtml);
                this.plhInformation.Visible = true;
                
                int status;
                if (QueryStringHelper.TryGetValue("Status", out status))
                {
                    // Request with guid and status
                    this.ProcessRequest(orderRoutestephandler, status);
                }                
            }
        }
    }

    private void HookUpEvents()
    {
        this.btOnTheCase.Click += btOnTheCase_Click;
        this.btCompleted.Click += btCompleted_Click;
    }

    private void ProcessRequest(string orderRoutestephandlerGuid, int status)
    {
        OrderRoutestephandlerEntity orderRoutestephandlerEntity = OrderRoutestephandlerHelper.GetOrderRoutestephandlerByGuid(orderRoutestephandlerGuid);
        if (orderRoutestephandlerEntity != null)
        {
            this.ProcessRequest(orderRoutestephandlerEntity, status);
        }
    }

    private void ProcessRequest(OrderRoutestephandlerEntity orderRoutestephandlerEntity, int status)
    {
        bool success = false;
        try
        {
            // Get order routestephandler
            if (status != (int)OrderRoutestephandlerStatus.BeingHandled && status != (int)OrderRoutestephandlerStatus.Completed)
            {
                // Wrong status
            }            
            else
            {
                // Update the order routestephandler status
                RoutingHelper.UpdateOrderroutestephandler(orderRoutestephandlerEntity.OrderRoutestephandlerId, (OrderRoutestephandlerStatus)status, OrderProcessingError.None);

                // Check for any expired orders
                RoutingHelper.FailExpiredRoutestephandlers(orderRoutestephandlerEntity.TerminalId, null);

                // Update buttons
                if (status == (int)OrderRoutestephandlerStatus.BeingHandled)
                {
                    this.btOnTheCase.Enabled = false;
                    this.lblNotificationSuccess.Text = OrderGateway.RESOURCE_ORDER_BEING_HANDLED;
                    this.plhNotificationSuccess.Visible = true;
                }                
                else if (status == (int)OrderRoutestephandlerStatus.Completed)
                {
                    this.btCompleted.Enabled = false;
                    this.lblNotificationSuccess.Text = OrderGateway.RESOURCE_ORDER_COMPLETED;
                    this.plhNotificationSuccess.Visible = true;
                }                

                // Success
                success = true;
            }
        }
        catch (Exception ex)
        {
            Requestlogger.UpdateRequestLog(Obymobi.Enums.GenericWebserviceCallResult.Failure, "Failure", ex);
            this.Response.StatusCode = (int)System.Net.HttpStatusCode.InternalServerError;
            this.Response.ContentType = "text/plain";
            this.Response.Write("InternalServerError");
        }
        finally
        {
            if (success)
                Requestlogger.UpdateRequestLog(Obymobi.Enums.GenericWebserviceCallResult.Success, "Success", null);
            else
                Requestlogger.UpdateRequestLog(Obymobi.Enums.GenericWebserviceCallResult.Failure, "Failure", null);
        }       
    }

    private void btOnTheCase_Click(object sender, EventArgs e)
    {
        string orderRoutestephandlerGuid;
        if (QueryStringHelper.TryGetValue("Guid", out orderRoutestephandlerGuid))
        {
            this.ProcessRequest(orderRoutestephandlerGuid, (int)OrderRoutestephandlerStatus.BeingHandled);
        }        
    }

    private void btCompleted_Click(object sender, EventArgs e)
    {
        string orderRoutestephandlerGuid;
        if (QueryStringHelper.TryGetValue("Guid", out orderRoutestephandlerGuid))
        {
            this.ProcessRequest(orderRoutestephandlerGuid, (int)OrderRoutestephandlerStatus.Completed);
        }
    }

    private string GetOrderHtmlAsString(OrderEntity orderEntity)
    {
        Order order = Obymobi.Logic.HelperClasses.OrderHelper.CreateOrderModelFromEntity(orderEntity, false, true);

        StringBuilder orderlistVar = new StringBuilder();
        orderlistVar.AppendLine("<table border='0'>");
        for (int i = 0; i < order.Orderitems.Length; i++)
        {
            Orderitem orderitem = order.Orderitems[i];

            if ((i == order.Orderitems.Length - 1) || (orderitem.Alterationitems != null && orderitem.Alterationitems.Length > 0))
                orderlistVar.Append("<tr><td width='30' style='font-size: 14px;font-weight: bold;text-align: center;'>");
            else 
                orderlistVar.Append("<tr style='border-bottom: 1px dotted gray;'><td width='30' style='font-size: 14px;font-weight: bold;text-align: center;'>");

            orderlistVar.Append(orderitem.Quantity + " ×");
            orderlistVar.Append("</td><td style='font-size: 14px;font-weight: bold;'>");
            orderlistVar.Append(orderitem.ProductName);
            orderlistVar.AppendLine("</td></tr>");

            if (orderitem.Alterationitems != null)
            {
                for (int j = 0; j < orderitem.Alterationitems.Length; j++)
                {
                    Alterationitem alterationitem = orderitem.Alterationitems[j];

                    if ((j == orderitem.Alterationitems.Length - 1) && i < order.Orderitems.Length - 1)
                        orderlistVar.Append("<tr style='border-bottom: 1px dotted gray;'><td width='30' style='font-size: 14px;font-weight: italic;'></td><td style='font-size: 14px;font-weight: italic;'>");
                    else
                        orderlistVar.Append("<tr><td width='30' style='font-size: 14px;font-weight: italic;'></td><td style='font-size: 14px;font-weight: italic;'>");
                    
                    orderlistVar.Append(alterationitem.AlterationName + ": ");

                    if (!alterationitem.AlterationoptionName.IsNullOrWhiteSpace())
                    {
                        orderlistVar.Append(alterationitem.AlterationoptionName);
                    }
                    else if (!alterationitem.Time.IsNullOrWhiteSpace())
                    {
                        orderlistVar.Append(alterationitem.Time);
                    }
                    else if (!alterationitem.Value.IsNullOrWhiteSpace())
                    {
                        orderlistVar.Append(alterationitem.Value);
                    }
                    else
                    {
                        orderlistVar.Append("????");
                    }

                    orderlistVar.AppendLine("</td></tr>");
                }
            }
        }
        orderlistVar.AppendLine("</table>");
        return orderlistVar.ToString();
    }
}