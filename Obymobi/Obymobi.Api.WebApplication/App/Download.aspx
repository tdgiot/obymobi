﻿<%@ Page Title="Crave - Download" Language="C#" MasterPageFile="~/App/MasterPages/AppBase.master" AutoEventWireup="true" Inherits="App_Download" Codebehind="Download.aspx.cs" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cplhBody" runat="Server">
<style>
    .form-signin {
        padding-bottom: 0;
    }
    .crave-logo {
        margin: 0 auto;
        margin-top: 12px;
        display: table;
    }
</style>
    <div class="container">
        <form id="form1" runat="server" class="form-signin">
            <D:HyperLink ID="hlLogo" runat="server" NavigateUrl="#" ImageUrl="~/App/img/crave-logo.png" />
            <D:PlaceHolder runat="server" ID="plhAndroid">
                <h2>Android</h2>
                <p>
                    <D:ImageButton runat="server" ImageUrl="img/google_play.png" ID="btnGooglePlay" Width="163" />&nbsp;&nbsp;&nbsp;<br />
					<D:LabelTextOnly runat="server" ID="lblPressToDownloadAndroid">Press to download.</D:LabelTextOnly>
                    <br /><br />
                    <D:LabelTextOnly runat="server" ID="lblAndroidCompatability">Compatible with most devices running Android 3.0 or higher.</D:LabelTextOnly>
                </p>
            </D:PlaceHolder>
            <D:PlaceHolder runat="server" ID="plhiPhone">
                <h2>iPhone & iPad</h2>
                <p>
                    <D:ImageButton runat="server" ImageUrl="img/app_store.png" ID="btnAppStore" Width="163" />&nbsp;&nbsp;&nbsp;<br />
					<D:LabelTextOnly runat="server" ID="lblPressToDownloadiOS">Press to download.</D:LabelTextOnly>
                    <br /><br />
                    <D:LabelTextOnly runat="server" ID="lblIosCompatability">Compatible with the iPhone 4 and higher, and iPad 2 and higher. Requires iOS 7 or later.</D:LabelTextOnly>
                </p>
            </D:PlaceHolder>  
            <D:PlaceHolder runat="server" ID="plhCraveLogo">
                <div class="crave-logo">
                    <a href="http://crave-hospitality.com/" target="_blank"><D:Image runat="server" ImageUrl="img/crave-logo-small.png" Width="100"/></a>
                </div>
            </D:PlaceHolder>                                              
        </form>
    </div>
    <script>
        function redirect(url) {
            if (url.length > 0)
                location.replace(url);
        }
    </script>
</asp:Content>

