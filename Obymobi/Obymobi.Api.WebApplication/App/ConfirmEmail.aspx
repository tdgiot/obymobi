﻿<%@ Page Title="Crave - Confirm Email" Language="C#" MasterPageFile="~/App/MasterPages/AppBase.master" AutoEventWireup="true" Inherits="App_ConfirmEmail" Codebehind="ConfirmEmail.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cplhBody" runat="Server">
    <div class="container">
        <form id="form1" runat="server" class="form-signin">            
            <D:HyperLink ID="hlLogo" runat="server" NavigateUrl="~/App/" ImageUrl="~/App/img/crave-logo.png" />
            <h2><D:LabelTextOnly runat="server" ID="lblTitle">Confirm email</D:LabelTextOnly></h2>
            <D:PlaceHolder runat="server" ID="plhError" Visible="false">
                <div class="alert alert-error">                    
                    <D:LabelTextOnly runat="server" ID="lblError"></D:LabelTextOnly>
                </div>
            </D:PlaceHolder>
            <D:PlaceHolder runat="server" ID="plhSuccess" Visible="false">
                <div class="alert alert-success">
                    <D:LabelTextOnly runat="server" ID="lblEmailConfirmed"><strong>Success!</strong> Your e-mail address has been succesfully confirmed. We hope you will enjoy our services which can be used from the Crave World App.</D:LabelTextOnly>
                </div>
            </D:PlaceHolder>            
        </form>
    </div>
</asp:Content>

