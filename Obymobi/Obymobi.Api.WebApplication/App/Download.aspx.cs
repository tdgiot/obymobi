﻿using System;
using System.Collections.Specialized;
using System.Text;
using System.Web;
using Dionysos;
using Dionysos.Diagnostics;
using Dionysos.Web;
using Obymobi;
using Obymobi.Api.Logic.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using Obymobi.Logic.Analytics;
using SD.LLBLGen.Pro.ORMSupportClasses;

public partial class App_Download : System.Web.UI.Page, System.Web.UI.ICallbackEventHandler
{
    SmsKeywordEntity smsSettings;
    int companyId;
    string companyName;
    Obymobi.Logic.Analytics.OperatingSystem operatingSystem;
    /**
     * URL Parameters
     * 
     * s = source (eg. Hotel name)
     * m = medium (eg. SMS/QR Code)
     * u = Global User ID (GUID)
     * c = Content (eg. folder model)
     * n = Campaign name (eg. In Hotels)
     */
    protected void Page_Load(object sender, EventArgs e)
    {
        var queryParameters = GetQueryParameters();
        this.smsSettings = SmsInformationHelper.GetKeywordEntityForKeyword(queryParameters["s"] ?? string.Empty);
        this.companyId = this.smsSettings.CompanyId ?? -1;
        this.companyName = (this.companyId > 0) ? this.smsSettings.CompanyEntity.Name : "Crave";
        this.operatingSystem = this.GetOS();

        if (this.companyId > 0)
        {
            PredicateExpression filter = new PredicateExpression();
            filter.Add(MediaRatioTypeMediaFields.MediaType == MediaType.DownloadPageBranding);
            filter.Add(MediaFields.CompanyId == this.companyId);

            RelationCollection relations = new RelationCollection();
            relations.Add(MediaRatioTypeMediaEntity.Relations.MediaEntityUsingMediaId);

            MediaRatioTypeMediaCollection mediaRatioTypeMediaCollection = new MediaRatioTypeMediaCollection();
            mediaRatioTypeMediaCollection.GetMulti(filter, relations);

            if (mediaRatioTypeMediaCollection.Count > 0)
            {
                string cdnPath = MediaHelper.GetMediaRatioTypeMediaPath(mediaRatioTypeMediaCollection[0]);
                string amazonUrl = WebEnvironmentHelper.GetCdnBaseUrlMedia(CloudStorageProviderIdentifier.Amazon, cdnPath, true);

                if (!amazonUrl.IsNullOrWhiteSpace())
                    this.hlLogo.ImageUrl = amazonUrl;
            }
        }
        else
            this.plhCraveLogo.Visible = false;

        Debug.WriteLine("Page Load: IP: {0} - Postback {1} - Callback {2} - Url: {3}".FormatSafe(
            this.GetRequesterIp(), this.IsPostBack, this.IsCallback, this.Request.RawUrl));

        if (!this.IsPostBack && !this.IsCallback && !CookieHelper.HasParameter("craveworld_uid") || queryParameters["u"] != CookieHelper.GetParameter("craveworld_uid") || QueryStringHelper.HasValue("test"))
        {
            Debug.WriteLine("Visited Mobile Landing Page: IP: {0} - Postback {1} - Callback {2} - Url: {3}".FormatSafe(
                this.GetRequesterIp(), this.IsPostBack, this.IsCallback, this.Request.RawUrl));
        }

        // Set Cookie (omnomnom)
        CookieHelper.AddCookie("craveworld_uid", queryParameters["u"], DateTime.MaxValue);

        SetGui();

        string redirectParameter = queryParameters["r"];
        if (redirectParameter == "android")
        {
            this.ForwardToStore(AppStore.GooglePlay);
        }
        else if (redirectParameter == "apple")
        {
            this.ForwardToStore(AppStore.AppleAppStore);
        }
        else
        {
            if (this.operatingSystem == Obymobi.Logic.Analytics.OperatingSystem.iOS ||
            this.operatingSystem == Obymobi.Logic.Analytics.OperatingSystem.Android ||
            TestUtil.IsPcGabriel)
            {
                this.AutoRedirectLogic();
            }
        }
    }

    public String GetRequesterIp()
    {
        HttpContext context = HttpContext.Current;
        string visitorIPAddress = context.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

        if (string.IsNullOrEmpty(visitorIPAddress))
            visitorIPAddress = context.Request.ServerVariables["REMOTE_ADDR"];

        if (string.IsNullOrEmpty(visitorIPAddress))
            visitorIPAddress = context.Request.UserHostAddress;

        return visitorIPAddress;
    }

    private NameValueCollection GetQueryParameters()
    {
        var queryParameters = new NameValueCollection(Request.QueryString);

        var sourceValue = queryParameters["s"];
        if (StringUtil.IsNullOrWhiteSpace(sourceValue))
        {
            queryParameters["s"] = "Direct";
        }

        var mediumValue = queryParameters["m"];
        if (StringUtil.IsNullOrWhiteSpace(mediumValue))
        {
            queryParameters["m"] = "Browser";
        }

        var userId = queryParameters["u"];
        if (StringUtil.IsNullOrWhiteSpace(userId))
        {
            userId = CookieHelper.GetParameter("craveworld_uid");
            if (userId.IsNullOrWhiteSpace())
                userId = Guid.NewGuid().ToString();

            queryParameters["u"] = userId;
        }

        var nameValue = queryParameters["n"];
        if (StringUtil.IsNullOrWhiteSpace(nameValue))
        {
            queryParameters["n"] = "None";
        }

        return queryParameters;
    }

    private Obymobi.Logic.Analytics.OperatingSystem GetOS()
    {
        var toReturn = Obymobi.Logic.Analytics.OperatingSystem.NotSet;
        var userAgent = Request.UserAgent;
        if (userAgent != null)
        {
            if (userAgent.Contains("Android", StringComparison.InvariantCultureIgnoreCase))
            {
                toReturn = Obymobi.Logic.Analytics.OperatingSystem.Android;
            }
            else if (userAgent.Contains("iPhone", StringComparison.InvariantCultureIgnoreCase) || userAgent.Contains("iPad", StringComparison.InvariantCultureIgnoreCase))
            {
                toReturn = Obymobi.Logic.Analytics.OperatingSystem.iOS;
            }
            else if (userAgent.Contains("Windows Phone", StringComparison.InvariantCultureIgnoreCase))
            {
                toReturn = Obymobi.Logic.Analytics.OperatingSystem.WindowsPhone;
            }
            else if (userAgent.Contains("Windows", StringComparison.InvariantCultureIgnoreCase))
            {
                toReturn = Obymobi.Logic.Analytics.OperatingSystem.WindowsDesktop;
            }
            else if (userAgent.Contains("Macintosh", StringComparison.InvariantCultureIgnoreCase))
            {
                toReturn = Obymobi.Logic.Analytics.OperatingSystem.MacOS;
            }
        }

        return toReturn;
    }

    private void SetGui()
    {
        bool autoRedirect = false; // Only on Phones/Tablets
        if (this.operatingSystem == Obymobi.Logic.Analytics.OperatingSystem.Android)
        {
            plhiPhone.Visible = false;
            autoRedirect = true;
        }
        else if (this.operatingSystem == Obymobi.Logic.Analytics.OperatingSystem.iOS)
        {
            plhAndroid.Visible = false;
            autoRedirect = true;
        }

        if (autoRedirect)
            this.AutoRedirectLogic();

        btnGooglePlay.Click += btnGooglePlay_Click;
        btnAppStore.Click += btnAppStore_Click;
    }

    void AutoRedirectLogic()
    {

        string callbackReference = Page.ClientScript.GetCallbackEventReference(this, "arg", "redirect", "context");
        string callbackFunction = "window.setTimeout(function getRedirectUrl(arg, context) {{ {0}; }}, 2500);".FormatSafe(callbackReference);
        Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "getRedirectUrl", callbackFunction, true);

    }

    string callbackRedirectResult = string.Empty;
    public string GetCallbackResult()
    {
        return this.callbackRedirectResult;
    }

    public void RaiseCallbackEvent(string eventArgument)
    {
        Obymobi.Logic.Analytics.OperatingSystem os = this.GetOS();

        if (os == Obymobi.Logic.Analytics.OperatingSystem.Android && !TestUtil.IsPcGabriel)
        {
            this.callbackRedirectResult = this.CreatePlayStoreLink(this.smsSettings.SmsInformationEntity, Request.QueryString);
            this.TrackWentToAppStore(AppStore.GooglePlay);
        }
        else if (os == Obymobi.Logic.Analytics.OperatingSystem.iOS)
        {
            this.callbackRedirectResult = this.CreateAppStoreLink(this.smsSettings.SmsInformationEntity);
            this.TrackWentToAppStore(AppStore.AppleAppStore);
        }
        else
            this.callbackRedirectResult = string.Empty;
    }

    void btnAppStore_Click(object sender, System.Web.UI.ImageClickEventArgs e)
    {
        this.ForwardToStore(AppStore.AppleAppStore);
    }

    void btnGooglePlay_Click(object sender, System.Web.UI.ImageClickEventArgs e)
    {
        this.ForwardToStore(AppStore.GooglePlay);
    }

    void ForwardToStore(AppStore store)
    {
        this.TrackWentToAppStore(store);

        if (store == AppStore.AppleAppStore)
            Response.Redirect(CreateAppStoreLink(smsSettings.SmsInformationEntity));
        else
            Response.Redirect(CreatePlayStoreLink(smsSettings.SmsInformationEntity, Request.QueryString));
    }

    private void TrackWentToAppStore(AppStore store)
    {
        var queryParameters = GetQueryParameters();

        var userId = queryParameters["u"];
        var sourceValue = queryParameters["s"];
        var mediumValue = queryParameters["m"];
        var nameValue = queryParameters["n"];
        var contentValue = queryParameters["c"];

        Debug.WriteLine("Track Went To App Store: IP: {0} - Postback {1} - Callback {2} - Url: {3}".FormatSafe(
            this.GetRequesterIp(), this.IsPostBack, this.IsCallback, this.Request.RawUrl));
    }

    private string CreatePlayStoreLink(SmsInformationEntity smsSettings, NameValueCollection queryParameters)
    {
        // Read url parameters
        var referrer = new StringBuilder();

        if (!string.IsNullOrWhiteSpace(queryParameters["s"]))
        {
            referrer.Append("utm_source=" + queryParameters["s"].Trim() + "&");
        }
        if (!string.IsNullOrWhiteSpace(queryParameters["m"]))
        {
            referrer.Append("utm_medium=" + queryParameters["m"].Trim() + "&");
        }
        if (!string.IsNullOrWhiteSpace(queryParameters["u"]))
        {
            referrer.Append("utm_term=" + queryParameters["u"].Trim() + "&");
        }
        if (!string.IsNullOrWhiteSpace(queryParameters["c"]))
        {
            referrer.Append("utm_content=" + queryParameters["c"].Trim() + "&");
        }
        if (!string.IsNullOrWhiteSpace(queryParameters["n"]))
        {
            referrer.Append("utm_campaign=" + queryParameters["n"].Trim() + "&");
        }

        var googlePlayUrl = string.Format("https://play.google.com/store/apps/details?id={0}", smsSettings.GooglePlayPackage);
        if (referrer.Length > 0)
        {
            var refParams = HttpUtility.UrlEncode(referrer.Remove(referrer.Length - 1, 1).ToString());
            googlePlayUrl += string.Format("&referrer={0}", refParams);
        }

        return googlePlayUrl;
    }

    private string CreateAppStoreLink(SmsInformationEntity smsSettings)
    {
        return string.Format("http://itunes.apple.com/app/{0}", smsSettings.AppStoreAppId);
    }

}