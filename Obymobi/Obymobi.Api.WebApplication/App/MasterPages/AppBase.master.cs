﻿using System;
using Obymobi;
using Obymobi.Enums;

public partial class App_MasterPages_AppBase : System.Web.UI.MasterPage
{
    private const string ANALYTICS_ID_DEMO = "UA-45902422-5";
    private const string ANALYTICS_ID_TEST = "UA-45902422-6";
    private const string ANALYTICS_ID_DEV = "UA-45902422-7";
    private const string ANALYTICS_ID_LIVE = "UA-45902422-8";

    protected void Page_Load(object sender, EventArgs e)
    {
        switch (WebEnvironmentHelper.CloudEnvironment)
        {
            case CloudEnvironment.ProductionPrimary:
            case CloudEnvironment.ProductionSecondary:
            case CloudEnvironment.ProductionOverwrite:
                GoogleAnalytics.TrackingId = ANALYTICS_ID_LIVE;
                break;
            case CloudEnvironment.Test:
                GoogleAnalytics.TrackingId = ANALYTICS_ID_TEST;
                break;
            case CloudEnvironment.Development:
                GoogleAnalytics.TrackingId = ANALYTICS_ID_DEV;
                break;
            case CloudEnvironment.Demo:
                GoogleAnalytics.TrackingId = ANALYTICS_ID_DEMO;
                break;
        }
    }
}
