﻿using Dionysos.Web;
using Dionysos;
using Obymobi.Data.EntityClasses;
using Obymobi.Logic.HelperClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Obymobi.Enums;

public partial class App_ConfirmEmail : System.Web.UI.Page
{
    // Translatable strings (to be used via Resources, or better via the database later)            
    string resource_EmailAlreadyVerified = "The e-mail address is already confirmed.";
    string resource_CustomerIdUnknown = "The customer couldn't be found in our system.";
    string resource_WeAreExperiencingIssues = "We're currently experiencing some issues. Please try again later.";
    string resource_NewEmailAddressAlreadyExists = "The new email that needs to be verified is already used for another registration.";    
    
    protected void Page_Load(object sender, EventArgs e)
    {
        this.ValidateRequest();        
    }

    void ValidateRequest()
    {
        string guid;
        if (QueryStringHelper.TryGetValue("a", out guid))
        {
            try
            {
                CustomerEntity customer;
                bool result = CustomerHelper.VerifyCustomer(guid, out customer);

                if (result)
                {
                    // Email confirmed.
                    this.plhSuccess.Visible = true;
                }
            }
            catch (Obymobi.ObymobiException oex)
            {
                if (oex.ErrorEnumValue is VerifyCustomerResult)
                {
                    switch ((VerifyCustomerResult)oex.ErrorEnumValue)
                    {
                        case VerifyCustomerResult.Success:
                            break;
                        case VerifyCustomerResult.CustomerIdUnknown:
                            this.lblError.Text = this.resource_CustomerIdUnknown;
                            break;
                        case VerifyCustomerResult.EmailAddressAlreadyVerified:
                            this.lblError.Text = this.resource_EmailAlreadyVerified;
                            break;
                        case VerifyCustomerResult.NewEmailAddressAlreadyExists:
                            this.lblError.Text = this.resource_NewEmailAddressAlreadyExists;
                            break;
                        case VerifyCustomerResult.Failure:
                        case VerifyCustomerResult.InvalidHash:
                        default:
                            this.lblError.Text = this.resource_WeAreExperiencingIssues;
                            break;
                    }
                }
                else
                    this.lblError.Text = this.resource_WeAreExperiencingIssues;
            }
            catch
            {
                this.lblError.Text = this.resource_WeAreExperiencingIssues;
            }
        }
        else
        {
            this.lblError.Text = this.resource_CustomerIdUnknown;
        }

        if (!this.lblError.Text.IsNullOrWhiteSpace())
        {
            this.plhError.Visible = true;
        }        
    }
}