﻿using Dionysos;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using Obymobi.Logic.HelperClasses;
using Obymobi.Security;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class App_CreateAccount : System.Web.UI.Page
{
    // Translatable strings (to be used via Resources, or better via the database later)
    string resource_AccountCreated = "Account Created";
    string resource_WeAreExperiencingIssues = "We're currently experiencing some issues. Please try again later.";
    string resource_EmailAlreadyUsed = "The e-mailadress you entered is already used for a Crave account. If you forgot your password, you can reset it <a href=\"RequestPasswordReset.aspx\">here</a>.";
    string resource_YouHaveToEnterAnEmailAddress = "You have to enter your email address.";
    string resource_EmailFormatIsIncorrect = "The email address is not in a recognized format.";
    string resource_YouMustEnterPasswordTwice = "You must enter your password twice.";
    string resource_PasswordsDoNotMatch = "The entered passwords do not match.";
    string resource_YouCantUseSpacesInPassword = "You can not use spaces in your password.";
    string resource_PasswordMustBeAtLeastSixCharacters = "Your password must be at least 6 characters long.";


    private const string captchaSessionKey = "captchaSessionKey";

    protected void Page_Load(object sender, EventArgs e)
    {
        if (this.IsPostBack) 
        {
            if (this.CustomValidation())
            {
                this.CreateAccount();
            }
        }

        if (this.IsPostBack)
        {
            this.plhUseCraveWithoutAccount.Visible = false;
        }
    }

    void CreateAccount()
    {
        string passwordTransferHash = CustomerPasswordHelper.GetPasswordTransferHash(this.tbPassword1.Text);

        try
        {
            CustomerHelper.CreateCustomer(new Obymobi.Data.EntityClasses.CustomerEntity(), this.tbEmail.Text, passwordTransferHash, false);
            this.plhError.Visible = false;
            this.plhSuccess.Visible = true;
            this.Session[captchaSessionKey] = false;
            this.plhForm.Visible = false;
            this.lblTitle.Text = this.resource_AccountCreated;
            this.plhDownloadButton.Visible = true;
        }
        catch (Obymobi.ObymobiException oex)
        {
            if (oex.ErrorEnumValue is Obymobi.Logic.HelperClasses.CustomerHelper.CustomerHelperResult)
            {
                this.lblError.Text = this.resource_WeAreExperiencingIssues;
            }
            else if (oex.ErrorEnumValue is CustomerSaveResult)
            {
                switch ((CustomerSaveResult)oex.ErrorEnumValue)
                {
                    case CustomerSaveResult.Success:
                        break;
                    case CustomerSaveResult.EmailAlreadyInUse:
                        this.lblError.Text = this.resource_EmailAlreadyUsed;
                        break;
                    case CustomerSaveResult.PhonenumberAlreadyInUse:                        
                    case CustomerSaveResult.PhonenumberEmpty:                        
                    case CustomerSaveResult.PasswordEmpty:                        
                    case CustomerSaveResult.SmsNotSent:                        
                    case CustomerSaveResult.PhonenumberIncorrectFormat:                                        
                    case CustomerSaveResult.EmailIncorrectFormat:
                    case CustomerSaveResult.EntitySaveError:
                    case CustomerSaveResult.EmailaddressMissing:
                    case CustomerSaveResult.InvalidCountryCode:                        
                    case CustomerSaveResult.Unknown:
                    case CustomerSaveResult.Failed:
                    default:
                        this.lblError.Text = this.resource_WeAreExperiencingIssues;
                        break;
                }
            }
            else
                this.lblError.Text = this.resource_WeAreExperiencingIssues;
        }
        catch
        {
            this.lblError.Text = this.resource_WeAreExperiencingIssues;
        }

        if (!this.lblError.Text.IsNullOrWhiteSpace())
        {
            this.lblError.Visible = true;
        }        
    }

    bool CustomValidation()
    {
        this.divPassword.Attributes.Add("class", "control-group");
        this.divEmail.Attributes.Add("class", "control-group");                                            

        // E-mail Address
        bool toReturn = false;
        if (this.tbEmail.Text.IsNullOrWhiteSpace())
        {
            this.lblError.Text = this.resource_YouHaveToEnterAnEmailAddress;
        }
        else if (!Dionysos.Text.RegularExpressions.RegEx.IsEmail(this.tbEmail.Text))
        {
            this.lblError.Text = this.resource_EmailFormatIsIncorrect;
        }
        else
        {
            PredicateExpression filter = new PredicateExpression();
            filter.Add(CustomerFields.Email % this.tbEmail.Text);
            CustomerCollection customers = new CustomerCollection();
            if (customers.GetDbCount(filter) > 0)
            {
                // Duplicate
                this.lblError.Text = this.resource_EmailAlreadyUsed;
            }
            else
            {
                toReturn = true;
                this.lblError.Text = string.Empty;
            }            
        }

        if (!toReturn)
        {
            this.plhError.Visible = true;
            this.divEmail.Attributes.Add("class", "control-group error");                                            
        }

        if(!toReturn)
            return toReturn;

        // Password
        toReturn = false;
        if (this.tbPassword1.Text.IsNullOrWhiteSpace() || this.tbPassword2.Text.IsNullOrWhiteSpace())
        { 
            this.lblError.Text = this.resource_YouMustEnterPasswordTwice;
        }
        else if (!this.tbPassword1.Text.Equals(this.tbPassword2.Text))
        {
            this.lblError.Text = this.resource_PasswordsDoNotMatch;
        }
        else if(this.tbPassword1.Text.Contains(" "))
        {
            this.lblError.Text = this.resource_YouCantUseSpacesInPassword;
        }
        else if (this.tbPassword1.Text.Length < 6)
        {
            this.lblError.Text = this.resource_PasswordMustBeAtLeastSixCharacters;
        }
        else
        {
            toReturn = true;
        }

        if(!toReturn)
        {
            this.tbPassword1.Text = string.Empty;
            this.tbPassword2.Text = string.Empty;
        }

        if (!toReturn)
        {
            this.divPassword.Attributes.Add("class", "control-group error");                                            
            this.plhError.Visible = true;
        }

        return toReturn;
    }
}