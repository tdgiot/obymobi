﻿<%@ Page Title="Crave - Support" Language="C#" MasterPageFile="~/App/MasterPages/AppBase.master" AutoEventWireup="true" Inherits="Support" Codebehind="Support.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cplhBody" runat="Server">
    <div class="container">
        <form id="form1" runat="server" class="form-signin">            
            <D:HyperLink ID="hlLogo" runat="server" NavigateUrl="~/App/" ImageUrl="~/App/img/crave-logo.png" />
            <h2><D:LabelTextOnly runat="server" ID="lblTitle">Support</D:LabelTextOnly></h2>
            <D:PlaceHolder runat="server" ID="plhInstructions">
                <p><D:LabelTextOnly runat="server" ID="lblInstructions">If you would like to ask a question or provide suggestions for future app enhancements, please contact the Crave support team via email to <a href="mailto:app@crave-emenu.com">app@crave-emenu.com</a>.</D:LabelTextOnly></p>
            </D:PlaceHolder>
        </form>
    </div>
</asp:Content>

