﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="404.aspx.cs" Inherits="Obymobi.Api.WebApplication._404" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            The item that you are looking for does not exist.
        </div>
        <div>
            <D:Label runat="server" ID="lblException"></D:Label>
        </div>
    </form>
</body>
</html>
