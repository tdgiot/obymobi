﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="401.aspx.cs" Inherits="Obymobi.Api.WebApplication._401" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            Oops, you don't have access to this piece of information
        </div>
        <div>
            <D:Label runat="server" ID="lblException"></D:Label>
        </div>
    </form>
</body>
</html>
