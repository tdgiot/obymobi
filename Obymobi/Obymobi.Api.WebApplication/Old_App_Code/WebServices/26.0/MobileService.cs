using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Web.Script.Services;
using Obymobi.Logic;
using Obymobi.Logic.Comet;
using ObymobiWebservice.Handlers;
using Obymobi.Logic.Model.Mobile.v26;
using Obymobi.Logic.Model.Mobile.v26.Converters;
using Obymobi.Enums;
using Obymobi;
using Dionysos;
using Obymobi.Logic.HelperClasses;
using Obymobi.Web.SoapExtensions;

namespace ObymobiWebservice.WebServices.v26
{
    /// <summary>
    /// Summary description for Service
    /// </summary>
    [WebService(Namespace = "http://crave-emenu.com/webservices/")]
    //[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    [SoapRpcService(RoutingStyle = SoapServiceRoutingStyle.RequestElement)]
    public class MobileService : System.Web.Services.WebService
    {
        #region Fields

        private MobileWebserviceHandler webserviceHandler = new MobileWebserviceHandler();

        #endregion

        #region GET methods

        // Generic
        [SoapRpcMethod(), WebMethod]
        [ScriptMethod(UseHttpGet = true)]
        public bool IsWebserviceAlive()
        {
            // __OBYMOBI_USER_CODE_REGION_START
            // __OBYMOBI_USER_CODE_REGION_END

            return this.webserviceHandler.IsWebserviceAlive();
        }

        // General
        [SoapRpcMethod(), WebMethod]
        [ScriptMethod(UseHttpGet = true)]
        public bool IsAlive()
        {
            // __OBYMOBI_USER_CODE_REGION_START
            // __OBYMOBI_USER_CODE_REGION_END

            return this.webserviceHandler.IsWebserviceAlive();
        }

        // Generic
        [SoapRpcMethod(), WebMethod]
        [ScriptMethod(UseHttpGet = true)]
        public bool IsDatabaseAlive()
        {
            // __OBYMOBI_USER_CODE_REGION_START
            // __OBYMOBI_USER_CODE_REGION_END

            return this.webserviceHandler.IsDatabaseAlive();
        }

        [SoapRpcMethod(), WebMethod]
        [ScriptMethod(UseHttpGet = true)]
        public ObyTypedMobileResult<long> GetCurrentTime()
        {
            // __OBYMOBI_USER_CODE_REGION_START
            // __OBYMOBI_USER_CODE_REGION_END                      
            return webserviceHandler.GetCurrentTime();
        }

        [SoapRpcMethod(), WebMethod]
        [ScriptMethod(UseHttpGet = true)]
        public ObyTypedMobileResult<long> GetMinimumRequiredVersion()
        {
            // __OBYMOBI_USER_CODE_REGION_START
            // __OBYMOBI_USER_CODE_REGION_END          
            return webserviceHandler.GetMinimumRequiredVersion();
        }

        [SoapRpcMethod(), WebMethod]
        [ScriptMethod(UseHttpGet = true)]
        public ObyTypedMobileResult<Customer> CreateCustomer(long timestamp, string email, string phonenumber, string passwordTransferHash, bool anonymousAccount, int deviceType, int devicePlatform, string deviceModel, string anonymousEmail, string anonymousPasswordTransferHash, string customerGuid, int socialmediaType, string externalId, string campaignName, string campaignSource, string campaignMedium, string campaignContent, string hash)
        {
            // __OBYMOBI_USER_CODE_REGION_START
            this.webserviceHandler.OriginalAdditionalParameters = new object[] { phonenumber, passwordTransferHash, anonymousAccount, deviceType, devicePlatform, deviceModel, anonymousEmail, anonymousPasswordTransferHash, customerGuid, socialmediaType, externalId, campaignName, campaignSource, campaignMedium, campaignContent };

            // __OBYMOBI_USER_CODE_REGION_END
            return new CustomerConverter().ConvertResultToLegacyResult(this.webserviceHandler.CreateCustomer(timestamp, email, phonenumber, passwordTransferHash, anonymousAccount, deviceType, devicePlatform, deviceModel, anonymousEmail, anonymousPasswordTransferHash, customerGuid, socialmediaType, externalId, campaignName, campaignSource, campaignMedium, campaignContent, hash));
        }  

        [SoapRpcMethod(), WebMethod]
        [ScriptMethod(UseHttpGet = true)]
        public ObyTypedMobileResult<bool> RequestPasswordReset(long timestamp, string emailAddress, string hash)
        {
            // __OBYMOBI_USER_CODE_REGION_START
            // __OBYMOBI_USER_CODE_REGION_END
            return webserviceHandler.RequestPasswordReset(timestamp, emailAddress, hash);
        }

        [SoapRpcMethod(), WebMethod]
        [ScriptMethod(UseHttpGet = true)]
        public ObyTypedMobileResult<Customer> ResetPassword(long timestamp, int customerId, string passwordResetIdentifier, string passwordTransferHash, string hash) 
        {
            // __OBYMOBI_USER_CODE_REGION_START
            // __OBYMOBI_USER_CODE_REGION_END
            return new CustomerConverter().ConvertResultToLegacyResult(this.webserviceHandler.ResetPassword(timestamp, customerId, passwordResetIdentifier, passwordTransferHash, hash));
        }

        [SoapRpcMethod(), WebMethod]
        [ScriptMethod(UseHttpGet = true)]
        public ObyTypedMobileResult<Customer> GetCustomer(long timestamp, string emailAddress, string passwordTransferHash, string anonymousEmail, string anonymousPasswordTransferHash, int socialmediaType, string externalId, string hash)
        {
            // __OBYMOBI_USER_CODE_REGION_START
            this.webserviceHandler.OriginalAdditionalParameters = new object[] { passwordTransferHash, anonymousEmail, anonymousPasswordTransferHash, socialmediaType, externalId };

            // __OBYMOBI_USER_CODE_REGION_END
            return new CustomerConverter().ConvertResultToLegacyResult(this.webserviceHandler.GetCustomer(timestamp, emailAddress, passwordTransferHash, anonymousEmail, anonymousPasswordTransferHash, socialmediaType, externalId, hash));
        }

        [SoapRpcMethod(), WebMethod]
        [ScriptMethod(UseHttpGet = true)]
        public ObyTypedMobileResult<Customer> GetCustomerWithSalt(long timestamp, string emailAddress, string hash)
        {
            // __OBYMOBI_USER_CODE_REGION_START
            // __OBYMOBI_USER_CODE_REGION_END
            return new CustomerConverter().ConvertResultToLegacyResult(this.webserviceHandler.GetCustomerWithSalt(timestamp, emailAddress, hash));
        }

        [SoapRpcMethod(), WebMethod]
        [ScriptMethod(UseHttpGet = true)]
        public ObyTypedMobileResult<Customer> UpdateCustomerDetails(long timestamp, string emailAddress, string newEmailAddress, string phonenumber, string firstname, string lastname, string lastnamePrefix, string birthdateDateStamp, int gender, string addressLine1, string addressLine2, string zipcode, string city, string countryCode, string passwordTransferHash, string newPasswordTransferHash, string hash)
        {
            // __OBYMOBI_USER_CODE_REGION_START
            // __OBYMOBI_USER_CODE_REGION_END
            return new CustomerConverter().ConvertResultToLegacyResult(this.webserviceHandler.UpdateCustomerDetails(timestamp, emailAddress, newEmailAddress, phonenumber, firstname, lastname, lastnamePrefix, birthdateDateStamp, gender, addressLine1, addressLine2, zipcode, city, countryCode, passwordTransferHash, newPasswordTransferHash, hash));
        }

        [SoapRpcMethod(), WebMethod]
        [ScriptMethod(UseHttpGet = true)]
        public ObyTypedMobileResult<long> GetCompanyListTicks(long timestamp, string emailAddress, string accessCodeIds, string hash)
        {
            // __OBYMOBI_USER_CODE_REGION_START
            // __OBYMOBI_USER_CODE_REGION_END          
            return webserviceHandler.GetCompanyListTicks(timestamp, emailAddress, accessCodeIds, hash);
        }

        [SoapRpcMethod(), WebMethod]
        [ScriptMethod(UseHttpGet = true)]
        [ASMXStripWhitespace]
        public ObyTypedMobileResult<CompanyList> GetCompanyList(long timestamp, string emailAddress, int deviceType, bool tablet, string accessCodes, string hash)
        {
            // __OBYMOBI_USER_CODE_REGION_START
            // __OBYMOBI_USER_CODE_REGION_END          
            return new CompanyListConverter().ConvertResultToLegacyResult(this.webserviceHandler.GetCompanyList(timestamp, emailAddress, deviceType, tablet, accessCodes, hash));
        }

        [SoapRpcMethod(), WebMethod]
        [ScriptMethod(UseHttpGet = true)]
        public ObyTypedMobileResult<long> GetCompanyTicks(long timestamp, string emailAddress, int companyId, string hash)
        {
            // __OBYMOBI_USER_CODE_REGION_START
            // __OBYMOBI_USER_CODE_REGION_END          
            return webserviceHandler.GetCompanyTicks(timestamp, emailAddress, companyId, hash);
        }

        [SoapRpcMethod(), WebMethod]
        [ScriptMethod(UseHttpGet = true)]
        public ObyTypedMobileResult<Company> GetCompany(long timestamp, string emailAddress, int companyId, int deviceType, bool tablet, string hash)
        {
            // __OBYMOBI_USER_CODE_REGION_START
            // __OBYMOBI_USER_CODE_REGION_END
            return new CompanyConverter().ConvertResultToLegacyResult(this.webserviceHandler.GetCompany(timestamp, emailAddress, companyId, deviceType, tablet, hash));
        }

        [SoapRpcMethod(), WebMethod]
        [ScriptMethod(UseHttpGet = true)]
        public ObyTypedMobileResult<long> GetMenuTicks(long timestamp, string emailAddress, int companyId, string hash)
        {
            // __OBYMOBI_USER_CODE_REGION_START
            // __OBYMOBI_USER_CODE_REGION_END          
            return webserviceHandler.GetMenuTicks(timestamp, emailAddress, companyId, hash);
        }

        [SoapRpcMethod(), WebMethod]
        [ScriptMethod(UseHttpGet = true)]
        public ObyTypedMobileResult<Menu> GetMenu(long timestamp, string emailAddress, int menuId, int deviceType, string hash)
        {
            // __OBYMOBI_USER_CODE_REGION_START
            // __OBYMOBI_USER_CODE_REGION_END
            return new MenuConverter().ConvertResultToLegacyResult(this.webserviceHandler.GetMenu(timestamp, emailAddress, menuId, deviceType, hash));
        }

        [SoapRpcMethod(), WebMethod]
        [ScriptMethod(UseHttpGet = true)]
        public ObyTypedMobileResult<Site> GetSite(long timestamp, string emailAddress, int siteId, string cultureCode, int deviceType, bool tablet, string hash)
        {
            // __OBYMOBI_USER_CODE_REGION_START
            // __OBYMOBI_USER_CODE_REGION_END
            return new SiteConverter().ConvertResultToLegacyResult(this.webserviceHandler.GetSite(timestamp, emailAddress, siteId, string.Empty, deviceType, tablet, cultureCode, hash));
        }

        [SoapRpcMethod(), WebMethod]
        [ScriptMethod(UseHttpGet = true)]
        public ObyTypedMobileResult<long> GetSiteTicks(long timestamp, string emailAddress, int siteId, string hash)
        {
            // __OBYMOBI_USER_CODE_REGION_START
            // __OBYMOBI_USER_CODE_REGION_END
            return webserviceHandler.GetSiteTicks(timestamp, emailAddress, siteId, hash);
        }

        [SoapRpcMethod(), WebMethod]
        [ScriptMethod(UseHttpGet = true)]
        public ObyTypedMobileResult<Company> GetPointOfInterest(long timestamp, string emailAddress, int pointOfInterestId, int deviceType, bool tablet, string hash)
        {
            // __OBYMOBI_USER_CODE_REGION_START
            // __OBYMOBI_USER_CODE_REGION_END
            return new CompanyConverter().ConvertResultToLegacyResult(this.webserviceHandler.GetPointOfInterest(timestamp, emailAddress, pointOfInterestId, deviceType, tablet, hash));
        }

        [SoapRpcMethod(), WebMethod]
        [ScriptMethod(UseHttpGet = true)]
        public ObyTypedMobileResult<long> GetPointOfInterestTicks(long timestamp, string emailAddress, int pointOfInterestId, string hash)
        {
            // __OBYMOBI_USER_CODE_REGION_START
            // __OBYMOBI_USER_CODE_REGION_END
            return webserviceHandler.GetPointOfInterestTicks(timestamp, emailAddress, pointOfInterestId, hash);
        }

        [SoapRpcMethod(), WebMethod]
        [ScriptMethod(UseHttpGet = true)]
        public ObyTypedMobileResult<CommonData> GetCommonData(long timestamp, string emailAddress, string hash)
        {
            // __OBYMOBI_USER_CODE_REGION_START
            // __OBYMOBI_USER_CODE_REGION_END
            return new CommonDataConverter().ConvertResultToLegacyResult(this.webserviceHandler.GetCommonData(timestamp, emailAddress, hash));
        }

        [SoapRpcMethod(), WebMethod]
        [ScriptMethod(UseHttpGet = true)]
        public ObyTypedMobileResult<long> GetCommonDataTicks(long timestamp, string emailAddress, string hash)
        {
            // __OBYMOBI_USER_CODE_REGION_START
            // __OBYMOBI_USER_CODE_REGION_END
            return webserviceHandler.GetCommonDataTicks(timestamp, emailAddress, hash);
        }

        [SoapRpcMethod(), WebMethod]
        [ScriptMethod(UseHttpGet = true)]
        public ObyTypedMobileResult<AccessCode> GetAccessCodes(long timestamp, string emailAddress, string accessCodeIds, string accessCode, string hash)
        {
            // __OBYMOBI_USER_CODE_REGION_START
            // __OBYMOBI_USER_CODE_REGION_END
            return new AccessCodeConverter().ConvertResultToLegacyResult(this.webserviceHandler.GetAccessCodes(timestamp, emailAddress, accessCodeIds, accessCode, hash));
        }

        [SoapRpcMethod(), WebMethod]
        [ScriptMethod(UseHttpGet = true)]
        public ObyTypedMobileResult<long> GetAccessCodesTicks(long timestamp, string emailAddress, string accessCodeIds, string hash)
        {
            // __OBYMOBI_USER_CODE_REGION_START
            // __OBYMOBI_USER_CODE_REGION_END
            return webserviceHandler.GetAccessCodesTicks(timestamp, emailAddress, accessCodeIds, hash);
        }

        #endregion

        #region PUT methods

        // Otoucho
        // Emenu - deprecated - use MAC address
        [SoapRpcMethod(), WebMethod]
        [ScriptMethod(UseHttpGet = true)]
        public ObyTypedMobileResult<Order> SaveOrder(long timestamp, string emailAddress, string xml, string hash)
        {
            // __OBYMOBI_USER_CODE_REGION_START

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { xml };

            // Create the order model using the specified xml
            Obymobi.Logic.Model.Mobile.Order order = new OrderConverter().ConvertLegacyXmlToModel(xml);

            // __OBYMOBI_USER_CODE_REGION_END

            return new OrderConverter().ConvertResultToLegacyResult(this.webserviceHandler.SaveOrder(timestamp, emailAddress, order, hash));
        }

     
        [SoapRpcMethod(), WebMethod]
        [ScriptMethod(UseHttpGet = true)]
        public ObyTypedMobileResult<Order> GetOrderHistory(long timestamp, string emailAddress, string hash)
        {
            // __OBYMOBI_USER_CODE_REGION_START
            // __OBYMOBI_USER_CODE_REGION_END

            return new OrderConverter().ConvertResultToLegacyResult(this.webserviceHandler.GetOrderHistory(timestamp, emailAddress, hash));
        }

        [SoapRpcMethod(), WebMethod]
        [ScriptMethod(UseHttpGet = true)]
        public ObyTypedMobileResult<Customer> VerifyCustomer(long timestamp, string initialLinkIdentifier, string hash)
        {
            // __OBYMOBI_USER_CODE_REGION_START
            this.webserviceHandler.OriginalAdditionalParameters = new object[] { initialLinkIdentifier };

            // __OBYMOBI_USER_CODE_REGION_END
            return new CustomerConverter().ConvertResultToLegacyResult(this.webserviceHandler.VerifyCustomer(timestamp, initialLinkIdentifier, hash));
        }

        [SoapRpcMethod(), WebMethod]
        [ScriptMethod(UseHttpGet = true)]
        public ObyTypedMobileResult<Customer> LinkSocialmedia(long timestamp, string identifier, int socialmediaType, string emailAddress, string passwordTransferHash, string externalId, string hash)
        {
            // __OBYMOBI_USER_CODE_REGION_START
            // __OBYMOBI_USER_CODE_REGION_END
            return new CustomerConverter().ConvertResultToLegacyResult(this.webserviceHandler.LinkSocialmedia(timestamp, identifier, socialmediaType, emailAddress, passwordTransferHash, externalId, hash));
        }

        [SoapRpcMethod(), WebMethod]
        [ScriptMethod(UseHttpGet = true)]
        public ObyTypedMobileResult<Customer> UnlinkSocialmedia(long timestamp, string emailAddress, string externalId, string hash)
        {
            // __OBYMOBI_USER_CODE_REGION_START
            // __OBYMOBI_USER_CODE_REGION_END
            return new CustomerConverter().ConvertResultToLegacyResult(this.webserviceHandler.UnlinkSocialmedia(timestamp, emailAddress, externalId, hash));
        }

        #endregion

        #region Helper methods

        private void SetNewRelicTransactionName(string methodName)
        {
        }

        #endregion
    }
}
