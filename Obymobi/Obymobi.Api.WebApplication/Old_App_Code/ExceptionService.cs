﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using Obymobi.Data;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Logging;
using System.Web.Services.Description;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace ObymobiWebservice
{
    /// <summary>
    /// Summary description for ExceptionService
    /// </summary>
    [WebService(Namespace = "http://obymobi.com/webservices/")]
    //[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    //[System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class ExceptionService : System.Web.Services.WebService
    {
        [SoapRpcMethod(), WebMethod]
		public bool CreateExceptionlog(string phonenumber, string message, string stacktrace, string deviceinfo, string log)
        {
            return Exceptionlogger.CreateExceptionlog(null, message, stacktrace, deviceinfo, log, phonenumber);
        }

        [SoapRpcMethod, WebMethod]
        public string DoDatabaseTest()
        {
            if (!TestUtil.IsPcDeveloper)
            {
                return "This test can only be ran on a development system";
            }

            var transaction = new Transaction(IsolationLevel.ReadCommitted, "TEST - GetMulti");
            try
            {
                var clientCollection = new ClientCollection();
                clientCollection.AddToTransaction(transaction);
                clientCollection.GetMulti(null);

                transaction.Commit();
            }
            catch (Exception ex)
            {
                transaction.Rollback();
                return string.Format("GetMulti - Exception: {0}", ex.Message);
            }
            finally
            {
                transaction.Dispose();
            }

            transaction = new Transaction(IsolationLevel.ReadCommitted, "TEST - GetDbCount");
            try
            {
                var clientCollection = new ClientCollection();
                clientCollection.AddToTransaction(transaction);
                clientCollection.GetDbCount();

                transaction.Commit();
            }
            catch (Exception ex)
            {
                transaction.Rollback();
                return string.Format("GetDbCount - Exception: {0}", ex.Message);
            }
            finally
            {
                transaction.Dispose();
            }

            transaction = new Transaction(IsolationLevel.ReadCommitted, "TEST - GetScalar");
            try
            {
                var clientCollection = new ClientCollection();
                clientCollection.AddToTransaction(transaction);
                clientCollection.GetScalar(ClientFieldIndex.ClientId, AggregateFunction.None);

                transaction.Commit();
            }
            catch (Exception ex)
            {
                transaction.Rollback();
                return string.Format("GetScalar - Exception: {0}", ex.Message);
            }
            finally
            {
                transaction.Dispose();
            }

            transaction = new Transaction(IsolationLevel.ReadCommitted, "TEST - SaveEntity");
            try
            {
                var client = new ClientEntity();
                client.AddToTransaction(transaction);
                client.FetchUsingPK(1);
                client.Notes = "TEST";
                client.Save();

                transaction.Commit();
            }
            catch (Exception ex)
            {
                transaction.Rollback();
                return string.Format("Save - Exception: {0}", ex.Message);
            }
            finally
            {
                transaction.Dispose();
            }

            return "PASS";
        }
    }
}

