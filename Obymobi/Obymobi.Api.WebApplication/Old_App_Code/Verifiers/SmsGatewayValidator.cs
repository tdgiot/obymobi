﻿using Dionysos.Interfaces;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for SmsGatewayValidator
/// </summary>
public class SmsGatewayValidator : IVerifier
{	
    private string errorMessage;
	private string stackTrace;	

	bool IVerifier.Verify()
	{
		bool toReturn = true;

		try
		{
            var filter = new PredicateExpression(SmsInformationFields.IsDefault == true);
            
            var smsInformationCollection = new SmsInformationCollection();
            smsInformationCollection.GetMulti(filter);

            if (smsInformationCollection.Count == 0)
            {
                this.errorMessage = "No default Sms Information setup for this environment. FIX IT!";                    
                toReturn = false;
            }                
		}
		catch (Exception ex) 
		{
			this.errorMessage = ex.Message;
			this.stackTrace = ex.StackTrace;
			toReturn = false;
		}

		return toReturn;
	}

	string IVerifier.ErrorMessage
	{
		get
		{
			return this.errorMessage;
		}
		set
		{
			throw new NotImplementedException();
		}
	}
}