﻿using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;

public class GetMapTimestampRequest : ITimestamped
{
    /// <summary>
    /// Gets or sets the id of the map to get the timestamp for.
    /// </summary>
    public int MapId { get; set; }

    public TimestampContainer TimestampContainer
    {
        get
        {
            return new TimestampContainer
            {
                EntityType = typeof(MapEntity),
                FieldValue = this.MapId
            };
        }
    }
}