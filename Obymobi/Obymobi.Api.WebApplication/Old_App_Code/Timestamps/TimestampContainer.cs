﻿using SD.LLBLGen.Pro.ORMSupportClasses;
using System;

/// <summary>
/// Class which serves as a container for the fields used to get a timestamp.
/// </summary>
public class TimestampContainer
{
    /// <summary>
    /// The type of the entity containing the timestamp.
    /// </summary>
    public Type EntityType { get; set; }

    /// <summary>
    /// Gets or sets the value of the field to use when geting the entity containing the timestamp.
    /// </summary>
    public object FieldValue { get; set; }    

    /// <summary>
    /// Creates a key combination of the EntityType + FieldValue.
    /// </summary>
    /// <returns>Key in format <EntityType>-<FieldValue>.</returns>
    public string GenerateKey()
    {
        return string.Format("{0}-{1}", this.EntityType.ToString(), this.FieldValue.ToString());
    }
}