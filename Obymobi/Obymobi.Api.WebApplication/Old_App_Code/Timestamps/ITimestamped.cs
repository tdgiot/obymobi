﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

public interface ITimestamped
{
    TimestampContainer TimestampContainer { get; }
}