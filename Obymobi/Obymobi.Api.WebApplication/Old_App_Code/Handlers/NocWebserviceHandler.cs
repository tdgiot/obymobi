﻿using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using Obymobi.Data;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using Obymobi.Logging;
using Obymobi.Logic.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

/// <summary>
/// Summary description for OnsiteServerWebserviceHandler
/// </summary>
public class NocWebserviceHandler
{
    internal static bool IsAlive()
    {
        // Below won't hit database because it's success, and therefore won't be saved.
        Requestlogger.CreateRequestlog(null, "IsWebserviceAlive");
        Requestlogger.UpdateRequestLog(GenericResult.Success, "Success", null);
        return true;
    }

    internal static bool IsDatabaseAlive()
    {
        return DatabaseHelper.IsDatabaseReady(Obymobi.Data.DaoClasses.CommonDaoBase.ActualConnectionString);
    }

    internal static List<CompanyInfo> GetCompaniesInfo()
    {
        PredicateExpression filter = new PredicateExpression();
        filter.Add(CompanyFields.SystemType == SystemType.Crave);

        PrefetchPath dpgPrefetch = new PrefetchPath(EntityType.CompanyEntity);
        dpgPrefetch.Add(CompanyEntityBase.PrefetchPathDeliverypointgroupCollection);

        IncludeFieldsList includeList = new IncludeFieldsList();
        includeList.Add(CompanyFields.CompanyId);
        includeList.Add(CompanyFields.CometHandlerType);
        includeList.Add(CompanyFields.ApiVersion);
        includeList.Add(CompanyFields.MessagingVersion);

        CompanyCollection collection = new CompanyCollection();
        collection.GetMulti(filter, includeList, dpgPrefetch);

        List<CompanyInfo> list = collection.Select(entity => new CompanyInfo
                                               {
                                                   CompanyId = entity.CompanyId,
                                                   CommunicationMethod = entity.CometHandlerType,
                                                   ApiVersion = entity.ApiVersion,
                                                   MessagingVersion = entity.MessagingVersion,
                                                   Deliverypointgroups = entity.DeliverypointgroupCollection.Select(dpgEntity => new DeliverypointgroupInfo
                                                                                                                                 {
                                                                                                                                     DeliverypointgroupId = dpgEntity.DeliverypointgroupId,
                                                                                                                                     ApiVersion = dpgEntity.ApiVersion
                                                                                                                                 }).ToList()
                                               }).ToList();
        return list;
    }

    [DataContract] 
    public class CompanyInfo
    {
        public CompanyInfo()
        {
            Deliverypointgroups = new List<DeliverypointgroupInfo>();
        }

        [DataMember]
        public int CompanyId { get; set; }
        [DataMember]
        public int CommunicationMethod { get; set; }
        [DataMember]
        public int ApiVersion { get; set; }
        [DataMember]
        public int MessagingVersion { get; set; }
        [DataMember]
        public List<DeliverypointgroupInfo> Deliverypointgroups { get; set; }
    }

    [DataContract]
    public class DeliverypointgroupInfo
    {
        [DataMember]
        public int DeliverypointgroupId { get; set; }
        [DataMember]
        public int ApiVersion { get; set; }
    }
}