﻿using System;
using System.Net;
using System.Web;
using Dionysos.Web;
using Obymobi.Data.EntityClasses;
using Obymobi.Enums;
using Obymobi.Logic.HelperClasses;
using Obymobi.Web.Google;
using Obymobi.Web.Google.Authorization;

public class ApiAuthenticationWebserviceHandler
{
    public static void SaveGoogleCode(int state, string error, string code)
    {
        string result = string.Empty;
        if (string.IsNullOrEmpty(error) && !string.IsNullOrWhiteSpace(code))
        {
            int companyId = state;

            string redirect = HttpContext.Current.Request.Url.Scheme + "://" + HttpContext.Current.Request.Url.Host;
            if (HttpContext.Current.Request.Url.Host == "localhost" || HttpContext.Current.Request.Url.Host == "127.0.0.1")
            {
                redirect += "/trunk";
            }
            redirect += "/api/ApiAuthenticationWebService.asmx/SaveGoogleCode";

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create("https://accounts.google.com/o/oauth2/token");
            request.Method = "POST";
            request.ContentType = "application/x-www-form-urlencoded";

            TokenRequest tokenRequest = GoogleAPI.RequestToken(redirect, code);
            if (tokenRequest != null)
            {
                ApiAuthenticationEntity googleApiAuthenticationEntity = ApiAuthenticationHelper.GetApiAuthenticationEntityByType(ApiAuthenticationType.Google, companyId);

                if (googleApiAuthenticationEntity == null)
                {
                    googleApiAuthenticationEntity = new ApiAuthenticationEntity();
                    googleApiAuthenticationEntity.CompanyId = companyId;
                    googleApiAuthenticationEntity.Type = ApiAuthenticationType.Google;
                    googleApiAuthenticationEntity.Name = "Crave Google Authentication";
                }

                if (!string.IsNullOrEmpty(tokenRequest.refresh_token))
                {
                    googleApiAuthenticationEntity.FieldValue1 = tokenRequest.refresh_token;
                }
                googleApiAuthenticationEntity.FieldValue2 = Dionysos.ConfigurationManager.GetString(DionysosWebConfigurationConstants.GoogleClientId); // Client Id
                googleApiAuthenticationEntity.FieldValue3 = Dionysos.ConfigurationManager.GetString(DionysosWebConfigurationConstants.GoogleClientSecret); // Client Secret
                googleApiAuthenticationEntity.ExpiryDateUTC = DateTime.UtcNow.AddSeconds(tokenRequest.expires_in);
                googleApiAuthenticationEntity.Save();

                result = "Google account linked successfully";
            }
            else
            {
                result = "Google account could not be linked";
            }
        }
        else
        {
            result = "An error occured while trying to link the Google account: " + error;
        }
        HttpContext.Current.Response.Write(result);
        HttpContext.Current.Response.Flush();
    }
}