﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Web;
using System.Web.Caching;
using System.Web.Hosting;
using Dionysos;
using Dionysos.Data.LLBLGen;
using Dionysos.IO;
using Dionysos.Net.Mail;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Obymobi;
using Obymobi.Api.Logic.EntityConverters;
using Obymobi.Api.Logic.HelperClasses;
using Obymobi.Constants;
using Obymobi.Data;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using Obymobi.Integrations.PMS;
using Obymobi.Integrations.POS;
using Obymobi.Logging;
using Obymobi.Logic;
using Obymobi.Logic.Comet;
using Obymobi.Logic.Comet.Netmessages;
using Obymobi.Logic.Globalization;
using Obymobi.Logic.Model;
using Obymobi.Logic.Routing;
using Obymobi.Security;
using Obymobi.Web.Azure.ServiceBus.Notifications;
using Obymobi.Web.Google;
using Obymobi.Web.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Attachment = System.Net.Mail.Attachment;
using SiteHelper = Obymobi.Cms.Logic.Sites.SiteHelper;

// ReSharper disable once CheckNamespace

namespace ObymobiWebservice.Handlers
{
    public class WebserviceHandler : GenericWebserviceHandler, IWebserviceHandler
    {
        private string applicationVersion = string.Empty;

        public enum WebserviceHandlerResult
        {
            OrderIdRequiredWithOrderRoutestephandlerSaveStatus = 200,
            NonExisitingOrderIdSuppliedWithOrderRoutestephandlerSaveStatus = 201,
            DeliverypointgroupIdIsNotSetForClient = 202,
            IncorrectOrMissingParameters = 203,
            FolioIsEmpty = 204,
            NoPrinterConfigured = 205,
            Base64StringInvalid = 206,
            UnzipFailed = 207,
            SuppliedDeviceTypeIsInvalid = 208,

            LegacyRequestFailedClientIdNonExistent = 906,
            LegacyRequestFailedTerminalIdNonExistent = 907,
            LegacyRequestFailedNoClientIdOrTerminalId = 908,
            LegacyRequestFailedSaltNotSetForCompany = 909,
            LegacyRequestFailedMacAddressEmptyOnDevice = 910
        }

        private CompanyEntity GetCachedCompanyTimestamps(int companyId)
        {
            CompanyEntity companyEntity = null;
            if (TestUtil.IsPcDeveloper && !TestUtil.IsPcBattleStationDanny)
            {
                companyEntity = new CompanyEntity(companyId);
            }
            else
            {
                if (Obymobi.Web.Caching.RedisCacheHelper.Instance.RedisEnabled)
                {
                    if (!Obymobi.Web.Caching.RedisCacheHelper.Instance.CacheStore.TryGetValue("CompanyCache-" + companyId, out companyEntity))
                    {
                        companyEntity = null;
                    }
                }
                else
                {
                    if (!Dionysos.Web.CacheHelper.TryGetValue("CompanyCache-" + companyId, false, out companyEntity))
                    {
                        companyEntity = null;
                    }
                }

                if (companyEntity == null)
                {
                    companyEntity = new CompanyEntity(companyId);
                    if (!companyEntity.IsNew)
                    {
                        Dionysos.Web.CacheHelper.AddAbsoluteExpire(false, "CompanyCache-" + companyId, companyEntity, 2);
                    }
                }
            }

            return companyEntity;
        }

        public string GetApplicationVersion()
        {
            if (this.applicationVersion.IsNullOrWhiteSpace())
            {
                string applicationVersion = Dionysos.Global.ApplicationInfo.ApplicationVersion; // Example: 1.2016041811
                if (!applicationVersion.IsNullOrWhiteSpace() && applicationVersion.Length > 2)
                {
                    // Remove the last two digits (revision number) from the version number
                    this.applicationVersion = applicationVersion.Substring(0, applicationVersion.Length - 2); // Example: 1.20160418
                }
            }

            return this.applicationVersion;
        }

        #region GET methods

        public ObyTypedResult<Client> GetClients(long timestamp, string macAddress, string hash)
        {
            // Create the return value
            ObyTypedResult<Client> result = new ObyTypedResult<Client>();

            // Create a requestlog
            Requestlogger.CreateRequestlog("WebserviceHandler.GetClients", timestamp, macAddress);

            try
            {
                // Validate the credentials
                TerminalEntity terminal = Obymobi.Logic.HelperClasses.WebserviceHelper.ValidateRequestRequireTerminal(timestamp, macAddress, hash);

                // Get the clients
                Client[] clients = ClientHelper.GetClientsForCompany(terminal.CompanyId, terminal.TerminalId);

                // Add the clients to the result
                result.ModelCollection.AddRange(clients);
            }
            catch (ObymobiException wex)
            {
                result.SetResult(wex);
                Exceptionlogger.CreateExceptionlog(wex);
            }
            catch (Exception ex)
            {
                result.SetResult(GenericWebserviceCallResult.Failure);
                Exceptionlogger.CreateExceptionlog(ex);
            }

            Requestlogger.UpdateRequestLog(result);

            return result;
        }

        public ObyTypedResult<Client> GetClient(long timestamp, string macAddress, int clientId, string clientmacAddress, int deliverypointgroupId, int deliverypointId, string applicationVersion, string osVersion, int deviceType, string hash)
        {
            return this.GetClient(timestamp, macAddress, clientId, clientmacAddress, deliverypointgroupId, deliverypointId, applicationVersion, osVersion, deviceType, null, hash);
        }

        public ObyTypedResult<Client> GetClient(long timestamp, string macAddress, int clientId, string clientmacAddress, int deliverypointgroupId, int deliverypointId, string applicationVersion, string osVersion, int deviceType, int? deviceModel, string hash)
        {
            // Create the return value
            ObyTypedResult<Client> result = new ObyTypedResult<Client>();

            // Create a request log
            Requestlogger.CreateRequestlog("WebserviceHandler.GetClient", timestamp, macAddress, clientId, hash);

            try
            {
                // In the case that a Client is doing this call for the first time (which means it doesn't have a Device yet) OR
                // when it just changed to be used for another company, the normal WebserviceHelper.ValidateRequest will fail, either becuase:
                // 1. There is no Device found
                // 2. The hash is invalid, since the device is still linked to a Client of the previous company and that Salt is used
                // therefore there is a failover.
                DeviceEntity device;
                int companyId = 0;
                try
                {
                    // Validate the credentials - Can fail if the Client is doing GetClient for the first time and the Device it's on
                    // is used for a different ClientId of another company before. In that case the ValidateRequest will use the Salt of
                    // the previous company.                    
                    device = Obymobi.Logic.HelperClasses.WebserviceHelper.ValidateRequest(timestamp, macAddress, hash, this.OriginalAdditionalParameters);
                    if (device != null)
                    {
                        // CompanyId
                        companyId = device.GetCompanyId().GetValueOrDefault(0);
                    }
                }
                catch (ObymobiException ex)
                {
                    bool reThrow;
                    if (ex.ErrorEnumValue.Equals(ValidateCredentialsResult.RequestHashIsInvalid) || 
                        ex.ErrorEnumValue.Equals(ValidateCredentialsResult.NoDeviceFoundForMacAddress) || 
                        ex.ErrorEnumValue.Equals(ValidateCredentialsResult.NoTerminalClientOrCustomerFoundForDevice))
                    {
                        // Check if it's valid if we use the Hash of the Company of this ClientId
                        DeliverypointgroupEntity dpg = new DeliverypointgroupEntity(deliverypointgroupId);
                        CompanyEntity company = this.GetCachedCompanyTimestamps(dpg.CompanyId);

                        companyId = company.CompanyId;

                        reThrow = !Hasher.IsHashValidForParameters(hash, company.Salt, timestamp, macAddress, this.OriginalAdditionalParameters);
                    }
                    else
                    {
                        reThrow = true;
                    }

                    if (reThrow)
                    {
                        throw;
                    }
                }

                // TEMP Get the deliverypoint group id                
                if (deliverypointgroupId == 0)
                {
                    PredicateExpression filter = new PredicateExpression();
                    filter.Add(DeliverypointgroupFields.CompanyId == companyId);

                    DeliverypointgroupCollection deliverypointgroupCollection = new DeliverypointgroupCollection();
                    deliverypointgroupCollection.GetMulti(filter, 1);

                    if (deliverypointgroupCollection.Count > 0)
                    {
                        deliverypointgroupId = deliverypointgroupCollection[0].DeliverypointgroupId;
                    }
                }

                Client clientModel = null;
                bool useClientCaching = true;

                if (useClientCaching && clientId > 0)
                {
                    string updatedTimestamp = "0";
                    ClientEntity clientEntity = new ClientEntity(clientId);
                    if (!clientEntity.IsNew && clientEntity.UpdatedUTC.HasValue)
                    {
                        updatedTimestamp = clientEntity.UpdatedUTC.Value.UtcToLocalTime().DateTimeToSimpleDateTimeStamp();

                        if (clientEntity.LastApiVersion != 1)
                        {
                            // Client switched from other API
                            ClientLogEntity log = new ClientLogEntity();
                            log.TypeEnum = ClientLogType.ServiceVersionSwitched;
                            log.ClientId = clientEntity.ClientId;
                            log.Message = string.Format("Switched to API version 1 (from {0})", clientEntity.LastApiVersion);
                            log.Save();

                            clientEntity.LastApiVersion = 1;
                            clientEntity.Save();
                        }
                    }

                    // Create the cache parameters
                    Obymobi.Logic.HelperClasses.CacheHelper.CacheParameters cacheParams = new Obymobi.Logic.HelperClasses.CacheHelper.CacheParameters("WebserviceHandler.GetClient", updatedTimestamp, GetApplicationVersion());
                    cacheParams.Add(clientId);
                    cacheParams.Add(deliverypointgroupId);
                    cacheParams.Add(deliverypointId);

                    if (!Obymobi.Logic.HelperClasses.CacheHelper.TryGetValue(cacheParams, out clientModel) || clientModel == null)
                    {
                        // Get the client
                        clientModel = ClientHelper.GetSetClientAndDevice(clientId, deviceType, deviceModel, companyId, deliverypointgroupId, deliverypointId, clientmacAddress, applicationVersion, osVersion);

                        // Add the client to the cache
                        Obymobi.Logic.HelperClasses.CacheHelper.CacheObject(cacheParams, clientModel);
                    }
                }
                else
                {
                    // Get the client
                    clientModel = ClientHelper.GetSetClientAndDevice(clientId, deviceType, deviceModel, companyId, deliverypointgroupId, deliverypointId, clientmacAddress, applicationVersion, osVersion);
                }

                // Refetch data in case anything changed
                string cacheKey = Obymobi.Logic.HelperClasses.WebserviceHelper.CACHE_KEY_PREFIX + macAddress;
                Dionysos.Web.CacheHelper.Remove(false, cacheKey);
                device = Obymobi.Logic.HelperClasses.DeviceHelper.GetDeviceEntityByIdentifier(macAddress, false);
                if (device != null)
                {
                    // Re-cache : Also see WebserviceHelper.ValidateRequest
                    Dionysos.Web.CacheHelper.AddAbsoluteExpire(false, cacheKey, device, 1);
                }

                try
                {
                    // Filter device specific media
                    DeviceType deviceTypeAsEnum;
                    if (device != null)
                    {
                        clientModel.Media = Obymobi.Logic.HelperClasses.MediaHelper.FilterMediaPerDevice(device.Type, clientModel.Media);
                    }
                    else if (EnumUtil.TryParse(deviceType, out deviceTypeAsEnum))
                    {
                        clientModel.Media = Obymobi.Logic.HelperClasses.MediaHelper.FilterMediaPerDevice(deviceTypeAsEnum, clientModel.Media);
                    }
                    else
                    {
                        throw new ObymobiException(WebserviceHandlerResult.SuppliedDeviceTypeIsInvalid);
                    }
                }
                catch (Exception)
                {
                    // ignored
                }
                
                // Add the client to the result
                result.ModelCollection.Add(clientModel);
            }
            catch (ObymobiException wex)
            {
                result.SetResult(wex);
                Exceptionlogger.CreateExceptionlog(wex);
            }
            catch (Exception ex)
            {
                result.SetResult(GenericWebserviceCallResult.Failure);
                Exceptionlogger.CreateExceptionlog(ex);
            }

            Requestlogger.UpdateRequestLog(result);

            return result;
        }

        public ObyTypedResult<Company> GetCompanies(long timestamp, string username, string password, string hash)
        {
            // Create the return value
            ObyTypedResult<Company> result = new ObyTypedResult<Company>();

            // Create a request log
            Requestlogger.CreateRequestlog("WebserviceHandler.GetCompanies", timestamp, username, password, hash);

            try
            {
                // Validate the credentials
                Obymobi.Logic.HelperClasses.WebserviceHelper.ValidateCredentialsWithHash(timestamp, username, password, hash);

                if (username.Equals("chewg", StringComparison.InvariantCultureIgnoreCase) && password.Equals("treehouse", StringComparison.InvariantCultureIgnoreCase))
                {
                    username = "Chewg";
                    password = "Treehouse";
                }
                else if (username.Equals("the", StringComparison.InvariantCultureIgnoreCase) && password.Equals("wellesley", StringComparison.InvariantCultureIgnoreCase))
                {
                    username = "the";
                    password = "wellesley";
                }

                // Get the companies
                Company[] companies = CompanyHelper.GetCompanyArrayByUsernameAndPassword(username, password);

                // Add the companies to the result
                result.ModelCollection.AddRange(companies);
            }
            catch (ObymobiException wex)
            {
                result.SetResult(wex);
                Exceptionlogger.CreateExceptionlog(wex);
            }
            catch (Exception ex)
            {
                result.SetResult(GenericWebserviceCallResult.Failure, ex);
                Exceptionlogger.CreateExceptionlog(ex);
            }

            Requestlogger.UpdateRequestLog(result);

            return result;
        }

        public ObyTypedResult<Company> GetCompany(long timestamp, string macAddress, int companyId, string hash)
        {
            // Create the return value
            ObyTypedResult<Company> result = new ObyTypedResult<Company>();

            // Create a request log
            Requestlogger.CreateRequestlog("WebserviceHandler.GetCompany", timestamp, macAddress, companyId, hash);

            try
            {
                // Validate the credentials
                DeviceEntity device = Obymobi.Logic.HelperClasses.WebserviceHelper.ValidateRequest(timestamp, macAddress, hash, this.OriginalAdditionalParameters);

                long companyDataLastModified = 0;
                long companyMediaLastModified = 0;
                CompanyEntity companyEntity = GetCachedCompanyTimestamps(companyId);
                if (!companyEntity.IsNew)
                {
                    companyDataLastModified = companyEntity.CompanyDataLastModifiedUTC.ToUnixTime();
                    companyMediaLastModified = companyEntity.CompanyMediaLastModifiedUTC.ToUnixTime();
                }
                string cacheTimestamp = string.Format("{0}-{1}", companyDataLastModified, companyMediaLastModified);

                // Create the cache parameters
                Obymobi.Logic.HelperClasses.CacheHelper.CacheParameters cacheParams = new Obymobi.Logic.HelperClasses.CacheHelper.CacheParameters("WebserviceHandler.GetCompany", cacheTimestamp, GetApplicationVersion());
                cacheParams.Add(companyId);

                Company company;
                if (!Obymobi.Logic.HelperClasses.CacheHelper.TryGetValue(cacheParams, out company) || company == null || TestUtil.IsPcBattleStationDanny || TestUtil.IsPcMathieu)
                {
                    // Get the company
                    companyEntity = Obymobi.Logic.HelperClasses.CompanyHelper.GetCompanyEntityByCompanyId(companyId);

                    // Create the company model
                    company = new CompanyEntityConverter().ConvertEntityToModel(companyEntity);

                    // Filter device specific media
                    company.Media = Obymobi.Logic.HelperClasses.MediaHelper.FilterMediaPerDevice(device.Type, company.Media);

                    // Add some info                
                    if (device.ClientCollection.Count == 1 && companyEntity.DeliverypointgroupCollection.Count > 0)
                    {
                        // GK For now we use the first, we don't know which DeliveyrpointNumber is assigned tot the tablet
                        company.UseAgeCheckInOtoucho = companyEntity.DeliverypointgroupCollection[0].UseAgeCheckInOtoucho;
                        company.UseManualDeliverypointsEntryInOtoucho = companyEntity.DeliverypointgroupCollection[0].UseManualDeliverypointsEntryInOtoucho;
                        company.UseStatelessInOtoucho = companyEntity.DeliverypointgroupCollection[0].UseStatelessInOtoucho;
                        company.SkipOrderOverviewAfterOrderSubmitInOtoucho = companyEntity.DeliverypointgroupCollection[0].SkipOrderOverviewAfterOrderSubmitInOtoucho;
                    }

                    Obymobi.Logic.HelperClasses.CacheHelper.CacheObject(cacheParams, company);
                }

                result.ModelCollection.Add(company);
            }
            catch (ObymobiException wex)
            {
                result.SetResult(wex);
                Exceptionlogger.CreateExceptionlog(wex);
            }
            catch (Exception ex)
            {
                result.SetResult(GenericWebserviceCallResult.Failure);
                Exceptionlogger.CreateExceptionlog(ex);
            }

            Requestlogger.UpdateRequestLog(result);

            return result;
        }

        /// <summary>
        ///     Gets the advertisements for the specified company
        /// </summary>
        public ObyTypedResult<Advertisement> GetAdvertisements(long timestamp, string macAddress, string hash)
        {
            // Create the return value
            ObyTypedResult<Advertisement> result = new ObyTypedResult<Advertisement>();

            // Create a request log
            Requestlogger.CreateRequestlog("WebserviceHandler.GetAdvertisements", timestamp, macAddress, hash);

            try
            {
                // Validate the credentials
                DeviceEntity device = Obymobi.Logic.HelperClasses.WebserviceHelper.ValidateRequest(timestamp, macAddress, hash, this.OriginalAdditionalParameters);

                // Update the last successful request
                //this.UpdateLastRequest(device);

                // Initialize an empty advertisement array
                Advertisement[] advertisements = null;

                // TEMP Get the deliverypoint group id            
                int deliverypointgroupId = 0;
                int companyId = 0;
                if (device.ClientEntitySingle != null)
                {
                    deliverypointgroupId = device.ClientEntitySingle.DeliverypointGroupId.GetValueOrDefault(0);
                    companyId = device.ClientEntitySingle.CompanyId;
                }

                if (deliverypointgroupId <= 0)
                {
                    PredicateExpression filter = new PredicateExpression();
                    filter.Add(DeliverypointgroupFields.CompanyId == companyId);

                    DeliverypointgroupCollection deliverypointgroupCollection = new DeliverypointgroupCollection();
                    deliverypointgroupCollection.GetMulti(filter, 1);

                    if (deliverypointgroupCollection.Count > 0)
                    {
                        deliverypointgroupId = deliverypointgroupCollection[0].DeliverypointgroupId;
                    }
                }

                if (deliverypointgroupId > 0)
                {
                    long advertisementDataLastModified = 0;
                    long advertisementMediaLastModified = 0;
                    CompanyEntity companyEntity = GetCachedCompanyTimestamps(companyId);
                    if (!companyEntity.IsNew)
                    {
                        advertisementDataLastModified = companyEntity.AdvertisementDataLastModifiedUTC.ToUnixTime();
                        advertisementMediaLastModified = companyEntity.AdvertisementMediaLastModifiedUTC.ToUnixTime();
                    }
                    string cacheTimestamp = string.Format("{0}-{1}", advertisementDataLastModified, advertisementMediaLastModified);

                    Obymobi.Logic.HelperClasses.CacheHelper.CacheParameters cacheParams = new Obymobi.Logic.HelperClasses.CacheHelper.CacheParameters("WebserviceHandler.GetAdvertisements", cacheTimestamp, GetApplicationVersion());
                    cacheParams.Add(companyId);
                    cacheParams.Add(deliverypointgroupId);

                    if (!Obymobi.Logic.HelperClasses.CacheHelper.TryGetValue(cacheParams, out advertisements))
                    {
                        // Get the advertisements by deliverypointgroupId
                        advertisements = AdvertisementHelper.GetAdvertisementsForDeliverypointgroup(deliverypointgroupId, companyId);

                        if (advertisements != null)
                        {
                            Obymobi.Logic.HelperClasses.CacheHelper.CacheObject(cacheParams, advertisements);
                        }
                    }
                }

                if (advertisements != null)
                {
                    foreach (Advertisement adv in advertisements)
                    {
                        // Filter device specific media
                        adv.Media = Obymobi.Logic.HelperClasses.MediaHelper.FilterMediaPerDevice(device.Type, adv.Media);
                    }

                    // Add the advertisements to the result
                    result.ModelCollection.AddRange(advertisements);
                }
            }
            catch (ObymobiException wex)
            {
                result.SetResult(wex);
                Exceptionlogger.CreateExceptionlog(wex);
            }
            catch (Exception ex)
            {
                result.SetResult(GenericWebserviceCallResult.Failure);
                Exceptionlogger.CreateExceptionlog(ex);
            }

            Requestlogger.UpdateRequestLog(result);

            return result;
        }

        public ObyTypedResult<Entertainment> GetEntertainment(long timestamp, string macAddress, string hash)
        {
            // Create the return value
            ObyTypedResult<Entertainment> result = new ObyTypedResult<Entertainment>();

            // Create a request log
            Requestlogger.CreateRequestlog("WebserviceHandler.GetEntertainment", timestamp, macAddress, hash);

            try
            {
                // Validate the request
                DeviceEntity device = Obymobi.Logic.HelperClasses.WebserviceHelper.ValidateRequest(timestamp, macAddress, hash, this.OriginalAdditionalParameters);

                // Update the last successful request
                //this.UpdateLastRequest(device);

                TerminalEntity terminal = null;
                int companyId = 0;
                int deliverypointgroupId = 0;
                if (device.ClientEntitySingle != null)
                {
                    deliverypointgroupId = device.ClientEntitySingle.DeliverypointGroupId.GetValueOrDefault(0);
                    companyId = device.ClientEntitySingle.CompanyId;
                }
                else if (device.TerminalEntitySingle != null)
                {
                    terminal = device.TerminalEntitySingle;
                    companyId = terminal.CompanyId;
                }
                
                Entertainment[] entertainment = new Entertainment[0];
                if (deliverypointgroupId > 0 && companyId > 0)
                {
                    long entertainmentDataLastModified = 0;
                    long entertainmentMediaLastModified = 0;
                    CompanyEntity companyEntity = GetCachedCompanyTimestamps(companyId);
                    if (!companyEntity.IsNew)
                    {
                        entertainmentDataLastModified = companyEntity.EntertainmentDataLastModifiedUTC.ToUnixTime();
                        entertainmentMediaLastModified = companyEntity.EntertainmentMediaLastModifiedUTC.ToUnixTime();
                    }

                    string cacheTimestamp = string.Format("{0}-{1}", entertainmentDataLastModified, entertainmentMediaLastModified);

                    // Create the cache parameters
                    Obymobi.Logic.HelperClasses.CacheHelper.CacheParameters cacheParams = new Obymobi.Logic.HelperClasses.CacheHelper.CacheParameters("WebserviceHandler.GetEntertainment", cacheTimestamp, GetApplicationVersion());
                    cacheParams.Add(companyId);
                    cacheParams.Add(deliverypointgroupId);

                    if (!Obymobi.Logic.HelperClasses.CacheHelper.TryGetValue(cacheParams, out entertainment) || entertainment == null)
                    {
                        // Get the advertisements for the company of the specified company id
                        entertainment = EntertainmentHelper.GetEntertainment(companyId, deliverypointgroupId, terminal, device.Type);

                        Obymobi.Logic.HelperClasses.CacheHelper.CacheObject(cacheParams, entertainment);
                    }
                }

                // Add the entertainment to the result
                result.ModelCollection.AddRange(entertainment);
            }
            catch (ObymobiException wex)
            {
                result.SetResult(wex);
                Exceptionlogger.CreateExceptionlog(wex);
            }
            catch (Exception ex)
            {
                result.SetResult(GenericWebserviceCallResult.Failure);
                Exceptionlogger.CreateExceptionlog(ex);
            }

            Requestlogger.UpdateRequestLog(result);

            return result;
        }

        public ObyTypedResult<Paymentmethod> GetPaymentmethods(long timestamp, string macAddress, string hash)
        {
            // Create the return value
            ObyTypedResult<Paymentmethod> result = new ObyTypedResult<Paymentmethod>();

            // Create a request log
            Requestlogger.CreateRequestlog("WebserviceHandler.GetPaymentmethods", timestamp, macAddress, hash);

            try
            {
                // Validate the request
                DeviceEntity device = Obymobi.Logic.HelperClasses.WebserviceHelper.ValidateRequest(timestamp, macAddress, hash, this.OriginalAdditionalParameters);

                //CompanyPaymentmethodEntity cash = new CompanyPaymentmethodEntity();
                //cash.PaymentmethodId = 1;
                //cash.CompanyEntity = company;
                //transactionManager.Add(cash);
                //cash.Save();

                //CompanyPaymentmethodEntity pinnen = new CompanyPaymentmethodEntity();
                //pinnen.PaymentmethodId = 5;
                //pinnen.CompanyEntity = company;
                //transactionManager.Add(pinnen);
                //pinnen.Save();

                //CompanyPaymentmethodEntity chipknip = new CompanyPaymentmethodEntity();
                //chipknip.PaymentmethodId = 6;
                //chipknip.CompanyEntity = company;
                //transactionManager.Add(chipknip);
                //chipknip.Save();

                //CompanyPaymentmethodEntity creditcard = new CompanyPaymentmethodEntity();
                //creditcard.PaymentmethodId = 7;
                //creditcard.CompanyEntity = company;
                //transactionManager.Add(creditcard);
                //creditcard.Save();

                //// Get an array containing paymentmethod instances using the specified company id
                //Paymentmethod[] paymentmethods = PaymentmethodHelper.GetPaymentmethods(device.GetCompanyId().Value, null);

                //// Add the payment methods to the result
                //result.ModelCollection.AddRange(paymentmethods);
            }
            catch (ObymobiException wex)
            {
                result.SetResult(wex);
                Exceptionlogger.CreateExceptionlog(wex);
            }
            catch (Exception ex)
            {
                result.SetResult(GenericWebserviceCallResult.Failure);
                Exceptionlogger.CreateExceptionlog(ex);
            }

            Requestlogger.UpdateRequestLog(result);

            return result;
        }

        public ObyTypedResult<ProductmenuItem> GetMenu(long timestamp, string macAddress, int deliverypointgroupId, bool applyMarkup, string hash)
        {
            // Create the return value
            ObyTypedResult<ProductmenuItem> result = new ObyTypedResult<ProductmenuItem>();

            // Create a request log
            Requestlogger.CreateRequestlog("WebserviceHandler.GetMenu", timestamp, macAddress, hash);

            try
            {
                // Validate the request
                DeviceEntity device = Obymobi.Logic.HelperClasses.WebserviceHelper.ValidateRequest(timestamp, macAddress, hash, this.OriginalAdditionalParameters);

                // TODO: Remove once all clients are updated to version 20150529+
                if (device.Type == DeviceType.Unknown)
                {
                    device.Type = DeviceType.SamsungP5110;
                }

                ProductmenuItem[] menu = null;
                bool returnEmptyMenuForAria = false;

                int companyId = device.GetCompanyId().GetValueOrDefault(0);
                if (companyId > 0)
                {
                    if (returnEmptyMenuForAria && device.GetCompanyId() == 343)
                    {
                        ProductmenuItem dummy = new ProductmenuItem();
                        dummy.Name = "Dummy";

                        menu = new ProductmenuItem[] { dummy };
                    }
                    else
                    {
                        // Get the company
                        var company = new CompanyEntity(companyId);
                        if (company.IsNew)
                        {
                            throw new ObymobiException(Obymobi.Logic.HelperClasses.MenuHelper.MenuHelperErrors.CompanyDoesNotExist, "Company with id '{0}' does not exist!", companyId);
                        }

                        // Create the cache parameters
                        Obymobi.Logic.HelperClasses.CacheHelper.CacheParameters cacheParams = new Obymobi.Logic.HelperClasses.CacheHelper.CacheParameters("MenuHelper.GetMenu", company.MenuDataLastModifiedAsTimeStamp, GetApplicationVersion());
                        cacheParams.Add(companyId);
                        cacheParams.Add(false);
                        cacheParams.Add(deliverypointgroupId);

                        // Get the menu for the company of the specified company id
                        menu = MenuHelper.GetMenu(companyId, deliverypointgroupId, true, device.Type, cacheParams, false);

                        foreach (ProductmenuItem item in menu)
                        {
                            // Filter device specific media
                            item.Media = Obymobi.Logic.HelperClasses.MediaHelper.FilterMediaPerDevice(device.Type, item.Media);
                        }
                    }

                    // Add the menu to the result
                    result.ModelCollection.AddRange(menu);
                }
            }
            catch (ObymobiException wex)
            {
                result.SetResult(wex);
                Exceptionlogger.CreateExceptionlog(wex);
            }
            catch (Exception ex)
            {
                result.SetResult(GenericWebserviceCallResult.Failure);
                Exceptionlogger.CreateExceptionlog(ex);

                Requestlogger.UpdateRequestLog(result);
            }

            Requestlogger.UpdateRequestLog(result);

            return result;
        }

        public ObyTypedResult<SimpleWebserviceResult> PublishMenu(long timestamp, string macAddress, int menuId, string hash)
        {
            // Create the return value
            ObyTypedResult<SimpleWebserviceResult> result = new ObyTypedResult<SimpleWebserviceResult>();

            // Create a request log
            Requestlogger.CreateRequestlog("WebserviceHandler.PublishMenu", timestamp, macAddress, hash);

            try
            {
                // Validate the request
                DeviceEntity device = Obymobi.Logic.HelperClasses.WebserviceHelper.ValidateRequest(timestamp, macAddress, hash, this.OriginalAdditionalParameters);

                // TODO: Remove once all clients are updated to version 20150529+
                if (device.Type == DeviceType.Unknown)
                {
                    device.Type = DeviceType.SamsungP5110;
                }

                int companyId = device.GetCompanyId().GetValueOrDefault(0);
                if (companyId > 0)
                {
                    MenuEntity menuEntity = new MenuEntity(menuId);
                    if (!menuEntity.IsNew)
                    {
                        string filePathToCopy = string.Empty;
                        ProductmenuItem[] menu = null;

                        DateTime utcNow = DateTime.UtcNow;
                        string lastModifiedTimestamp = DateTimeUtil.DateTimeToSimpleDateTimeStamp(utcNow);

                        foreach (DeliverypointgroupEntity deliverypointgroupEntity in menuEntity.DeliverypointgroupCollection)
                        {
                            if (deliverypointgroupEntity.Active)
                            {
                                if (menu == null)
                                {
                                    // Create the cache parameters
                                    Obymobi.Logic.HelperClasses.CacheHelper.CacheParameters cacheParams = new Obymobi.Logic.HelperClasses.CacheHelper.CacheParameters("MenuHelper.GetMenu", lastModifiedTimestamp, GetApplicationVersion());
                                    cacheParams.Add(companyId);
                                    cacheParams.Add(false);
                                    cacheParams.Add(deliverypointgroupEntity.DeliverypointgroupId);

                                    // Only get the menu once
                                    menu = MenuHelper.GetMenu(device.GetCompanyId().GetValueOrDefault(0), deliverypointgroupEntity.DeliverypointgroupId, true, device.Type, cacheParams, false);

                                    // Get the path of the cache file of the menu
                                    filePathToCopy = Path.Combine(Obymobi.Logic.HelperClasses.CacheHelper.TempFilePath, string.Format("{0}.cache", cacheParams.GetCacheKey()));
                                }
                                else if (!filePathToCopy.IsNullOrWhiteSpace() && File.Exists(filePathToCopy))
                                {
                                    Obymobi.Logic.HelperClasses.CacheHelper.CacheParameters cacheParams = new Obymobi.Logic.HelperClasses.CacheHelper.CacheParameters("MenuHelper.GetMenu", lastModifiedTimestamp, GetApplicationVersion());
                                    cacheParams.Add(companyId);
                                    cacheParams.Add(false);
                                    cacheParams.Add(deliverypointgroupEntity.DeliverypointgroupId);

                                    string filePath = Path.Combine(Obymobi.Logic.HelperClasses.CacheHelper.TempFilePath, string.Format("{0}.cache", cacheParams.GetCacheKey()));

                                    if (!File.Exists(filePath))
                                    {
                                        // Copy the cached menu file to a copy for the new delivery point group
                                        File.Copy(filePathToCopy, filePath);

                                        // Add the menu to the cache
                                        Dionysos.Web.CacheHelper.Add(false, cacheParams.GetCacheKey(), menu, new CacheDependency(filePath));
                                    }
                                }
                            }
                        }

                        if (menu != null)
                        {
                            CompanyEntity companyEntity = new CompanyEntity(companyId);
                            if (!companyEntity.IsNew)
                            {
                                companyEntity.MenuDataLastModifiedUTC = utcNow;
                                companyEntity.MenuMediaLastModifiedUTC = utcNow;
                                companyEntity.CompanyDataLastModifiedUTC = utcNow;
                                companyEntity.Save();
                            }

                            result.ModelCollection.Add(new SimpleWebserviceResult(true));
                        }
                    }
                }
            }
            catch (ObymobiException wex)
            {
                result.SetResult(wex);
                Exceptionlogger.CreateExceptionlog(wex);
            }
            catch (Exception ex)
            {
                result.SetResult(GenericWebserviceCallResult.Failure);
                Exceptionlogger.CreateExceptionlog(ex);

                Requestlogger.UpdateRequestLog(result);
            }

            Requestlogger.UpdateRequestLog(result);

            return result;
        }

        public ObyTypedResult<SimpleWebserviceResult> PublishCompany(long timestamp, string macAddress, int companyId, string hash)
        {
            // Create the return value
            ObyTypedResult<SimpleWebserviceResult> result = new ObyTypedResult<SimpleWebserviceResult>();

            // Create a request log
            Requestlogger.CreateRequestlog("WebserviceHandler.PublishCompany", timestamp, macAddress, hash);

            try
            {
                // Validate the request
                DeviceEntity device = Obymobi.Logic.HelperClasses.WebserviceHelper.ValidateRequest(timestamp, macAddress, hash, this.OriginalAdditionalParameters);

                if (companyId > 0)
                {
                    CompanyEntity companyEntity = new CompanyEntity(companyId);
                    if (!companyEntity.IsNew)
                    {
                        // Get the company
                        companyEntity = Obymobi.Logic.HelperClasses.CompanyHelper.GetCompanyEntityByCompanyId(companyId);

                        // Create the company model
                        Company company = new CompanyEntityConverter().ConvertEntityToModel(companyEntity);

                        // Filter device specific media
                        company.Media = Obymobi.Logic.HelperClasses.MediaHelper.FilterMediaPerDevice(device.Type, company.Media);

                        // Add some info                
                        if (device.ClientCollection.Count == 1 && companyEntity.DeliverypointgroupCollection.Count > 0)
                        {
                            // GK For now we use the first, we don't know which DeliveyrpointNumber is assigned tot the tablet
                            company.UseAgeCheckInOtoucho = companyEntity.DeliverypointgroupCollection[0].UseAgeCheckInOtoucho;
                            company.UseManualDeliverypointsEntryInOtoucho = companyEntity.DeliverypointgroupCollection[0].UseManualDeliverypointsEntryInOtoucho;
                            company.UseStatelessInOtoucho = companyEntity.DeliverypointgroupCollection[0].UseStatelessInOtoucho;
                            company.SkipOrderOverviewAfterOrderSubmitInOtoucho = companyEntity.DeliverypointgroupCollection[0].SkipOrderOverviewAfterOrderSubmitInOtoucho;
                        }

                        // Create the cache parameters
                        DateTime utcNow = DateTime.UtcNow;
                        string cacheTimestamp = string.Format("{0}-{1}", utcNow.ToUnixTime(), utcNow.ToUnixTime());

                        Obymobi.Logic.HelperClasses.CacheHelper.CacheParameters cacheParams = new Obymobi.Logic.HelperClasses.CacheHelper.CacheParameters("WebserviceHandler.GetCompany", cacheTimestamp, GetApplicationVersion());
                        cacheParams.Add(companyId);

                        // Save the company in cache
                        Obymobi.Logic.HelperClasses.CacheHelper.CacheObject(cacheParams, company);

                        // Update the company's timestamps
                        companyEntity.CompanyDataLastModifiedUTC = utcNow;
                        companyEntity.CompanyMediaLastModifiedUTC = utcNow;
                        companyEntity.Save();
                    }
                }
            }
            catch (ObymobiException wex)
            {
                result.SetResult(wex);
                Exceptionlogger.CreateExceptionlog(wex);
            }
            catch (Exception ex)
            {
                result.SetResult(GenericWebserviceCallResult.Failure);
                Exceptionlogger.CreateExceptionlog(ex);

                Requestlogger.UpdateRequestLog(result);
            }

            Requestlogger.UpdateRequestLog(result);

            return result;
        }

        public ObyTypedResult<Netmessage> GetNetmessages(long timestamp, string macAddress, int customerId, int clientId, int companyId, int deliverypointId, int terminalId, string hash)
        {
            // Create the return value
            ObyTypedResult<Netmessage> result = new ObyTypedResult<Netmessage>();

            // Create a request log
            Requestlogger.CreateRequestlog("WebserviceHandler.GetNetmessages", timestamp, macAddress, customerId, clientId, companyId, deliverypointId, terminalId, hash);

            try
            {
                DeviceEntity device = Obymobi.Logic.HelperClasses.WebserviceHelper.ValidateRequest(timestamp, macAddress, hash, this.OriginalAdditionalParameters);
                result.ModelCollection.AddRange(Obymobi.Logic.HelperClasses.NetmessageHelper.GetNetmessageForIdentifier(device.Identifier));

                /*if (clientId > 0)
                {
                    result.ModelCollection.AddRange(Obymobi.Logic.HelperClasses.NetmessageHelper.GetNetmessagesForClient(clientId));
                    if (this.IsEmenuRequest && device.ClientEntitySingle != null)
                    {
                        device.ClientEntitySingle.DeviceEntity.CommunicationMethod = (int)ClientCommunicationMethod.Webservice;
                    }
                }
                else if (terminalId > 0)
                {
                    result.ModelCollection.AddRange(Obymobi.Logic.HelperClasses.NetmessageHelper.GetNetmessagesForTerminal(terminalId));
                    if (this.IsEmenuRequest && device.TerminalEntitySingle != null)
                    {
                        device.CommunicationMethod = (int)ClientCommunicationMethod.Webservice;
                    }
                }*/
            }
            catch (ObymobiException wex)
            {
                result.SetResult(wex);
                //Exceptionlogger.CreateExceptionlog(wex);
            }
            catch (Exception)
            {
                result.SetResult(GenericWebserviceCallResult.Failure);
                //Exceptionlogger.CreateExceptionlog(ex);
            }

            //Requestlogger.UpdateRequestLog(result);

            return result;
        }

        public ObyTypedResult<Survey> GetSurveys(long timestamp, string macAddress, string hash)
        {
            // Create the return value
            ObyTypedResult<Survey> result = new ObyTypedResult<Survey>();

            // Create a request log
            Requestlogger.CreateRequestlog("WebserviceHandler.GetSurveys", timestamp, macAddress, hash);

            try
            {
                // Validate the request
                DeviceEntity device = Obymobi.Logic.HelperClasses.WebserviceHelper.ValidateRequest(timestamp, macAddress, hash, this.OriginalAdditionalParameters);

                // Deprecated
            }
            catch (ObymobiException wex)
            {
                result.SetResult(wex);
                Exceptionlogger.CreateExceptionlog(wex);
            }
            catch (Exception ex)
            {
                result.SetResult(GenericWebserviceCallResult.Failure);
                Exceptionlogger.CreateExceptionlog(ex);
            }

            Requestlogger.UpdateRequestLog(result);

            return result;
        }

        public ObyTypedResult<Announcement> GetAnnouncements(long timestamp, string macAddress, string hash)
        {
            // Create the return value
            ObyTypedResult<Announcement> result = new ObyTypedResult<Announcement>();

            // Create a request log
            Requestlogger.CreateRequestlog("WebserviceHandler.GetAnnouncements", timestamp, macAddress, hash);

            try
            {
                // Validate the request
                DeviceEntity device = Obymobi.Logic.HelperClasses.WebserviceHelper.ValidateRequest(timestamp, macAddress, hash, this.OriginalAdditionalParameters);

                int companyId = 0;
                int deliverypointgroupId = 0;
                if (device.ClientEntitySingle != null)
                {
                    deliverypointgroupId = device.ClientEntitySingle.DeliverypointGroupId.GetValueOrDefault(0);
                    companyId = device.ClientEntitySingle.CompanyId;
                }

                if (deliverypointgroupId > 0)
                {
                    long announcementDataLastModified = 0;
                    long announcementMediaLastModified = 0;
                    CompanyEntity companyEntity = GetCachedCompanyTimestamps(companyId);
                    if (!companyEntity.IsNew)
                    {
                        announcementDataLastModified = companyEntity.AnnouncementDataLastModifiedUTC.ToUnixTime();
                        announcementMediaLastModified = companyEntity.AnnouncementMediaLastModifiedUTC.ToUnixTime();
                    }
                    
                    Obymobi.Logic.HelperClasses.CacheHelper.CacheParameters cacheParams = new Obymobi.Logic.HelperClasses.CacheHelper.CacheParameters("WebserviceHandler.GetAnnouncements", string.Format("{0}-{1}", announcementDataLastModified, announcementMediaLastModified), GetApplicationVersion());
                    cacheParams.Add(companyId);
                    cacheParams.Add(deliverypointgroupId);

                    Announcement[] announcements;
                    if (!Obymobi.Logic.HelperClasses.CacheHelper.TryGetValue(cacheParams, out announcements) || announcements == null)
                    {
                        announcements = AnnouncementHelper.GetAnnouncementsForDeliverypointgroup(deliverypointgroupId);

                        Obymobi.Logic.HelperClasses.CacheHelper.CacheObject(cacheParams, announcements);
                    }

                    result.ModelCollection.AddRange(announcements);
                }
            }
            catch (ObymobiException wex)
            {
                result.SetResult(wex);
                Exceptionlogger.CreateExceptionlog(wex);
            }
            catch (Exception ex)
            {
                result.SetResult(GenericWebserviceCallResult.Failure);
                Exceptionlogger.CreateExceptionlog(ex);
            }

            Requestlogger.UpdateRequestLog(result);

            return result;
        }

        public ObyTypedResult<Deliverypoint> GetDeliverypoints(long timestamp, string macAddress, int deliverypointgroupId, string hash)
        {
            // Create the return value
            ObyTypedResult<Deliverypoint> result = new ObyTypedResult<Deliverypoint>();

            // Create a request log
            Requestlogger.CreateRequestlog("WebserviceHandler.GetDeliverypoints", timestamp, macAddress, hash, deliverypointgroupId);

            try
            {
                // Validate the request
                DeviceEntity device = Obymobi.Logic.HelperClasses.WebserviceHelper.ValidateRequest(timestamp, macAddress, hash, this.OriginalAdditionalParameters);

                int companyId = device.GetCompanyId().GetValueOrDefault();

                DateTime deliverypointDataLastModified = DateTime.MinValue;
                CompanyEntity companyEntity = GetCachedCompanyTimestamps(companyId);
                if (!companyEntity.IsNew)
                {
                    deliverypointDataLastModified = companyEntity.DeliverypointDataLastModifiedUTC;
                }

                if (deliverypointgroupId == 0 && device.ClientEntitySingle != null && device.ClientEntitySingle.DeliverypointGroupId.HasValue)
                {
                    deliverypointgroupId = device.ClientEntitySingle.DeliverypointGroupId.Value;
                }

                Obymobi.Logic.HelperClasses.CacheHelper.CacheParameters cacheParams = new Obymobi.Logic.HelperClasses.CacheHelper.CacheParameters("WebserviceHandler.GetDeliverypoints", deliverypointDataLastModified.ToUnixTime().ToString(), GetApplicationVersion());
                cacheParams.Add(companyId);
                cacheParams.Add(deliverypointgroupId);

                Deliverypoint[] deliverpoints;

                if (!Obymobi.Logic.HelperClasses.CacheHelper.TryGetValue(cacheParams, out deliverpoints))
                {
                    // Get an array of company instances using the specified query
                    deliverpoints = DeliverypointHelper.GetDeliverypoints(companyId, null, false, deliverypointgroupId);

                    if (deliverpoints.Length > 0)
                    {
                        Obymobi.Logic.HelperClasses.CacheHelper.CacheObject(cacheParams, deliverpoints);
                    }
                }

                // Add the deliverypoints to the result
                result.ModelCollection.AddRange(deliverpoints);
            }
            catch (ObymobiException wex)
            {
                result.SetResult(wex);
                Exceptionlogger.CreateExceptionlog(wex);
            }
            catch (Exception ex)
            {
                result.SetResult(GenericWebserviceCallResult.Failure);
                Exceptionlogger.CreateExceptionlog(ex);
            }

            Requestlogger.UpdateRequestLog(result);

            return result;
        }

        public ObyTypedResult<Terminal> GetTerminal(long timestamp, string macAddress, int terminalId, string applicationVersion, string osVersion, int deviceType, string hash)
        {
            // Create the return value
            ObyTypedResult<Terminal> result = new ObyTypedResult<Terminal>();

            // Create a request log
            Requestlogger.CreateRequestlog("WebserviceHandler.GetTerminal", timestamp, macAddress, hash);

            try
            {
                // In the case that a Terminal is doing this call for the first time (which means it doesn't have a Device yet) OR
                // when it just changed to be used for another company, the normal WebserviceHelper.ValidateRequest will fail, either becuase:
                // 1. There is no Device found
                // 2. The hash is invalid, since the device is still linked to a Terminal of the previous company and that Salt is used
                // therefore there is a failover.

                DeviceEntity device;
                TerminalEntity terminal;
                try
                {
                    // Validate the credentials - Can fail if the Client is doing GetClient for the first time and the Device it's on
                    // is used for a different ClientId of another company before. In that case the ValidateRequest will use the Salt of
                    // the previous company.                    
                    device = Obymobi.Logic.HelperClasses.WebserviceHelper.ValidateRequest(timestamp, macAddress, hash, this.OriginalAdditionalParameters);
                    terminal = device.TerminalEntitySingle;
                }
                catch (ObymobiException ex)
                {
                    if (ex.ErrorEnumValue.Equals(ValidateCredentialsResult.RequestHashIsInvalid) || ex.ErrorEnumValue.Equals(ValidateCredentialsResult.NoDeviceFoundForMacAddress) || ex.ErrorEnumValue.Equals(ValidateCredentialsResult.NoTerminalClientOrCustomerFoundForDevice))
                    {
                        // Check if it's valid if we use the Hash of the Company of this ClientId
                        terminal = new TerminalEntity(terminalId);

                        if (terminal.IsNew || terminal.CompanyEntity.IsNew)
                        {
                            throw new ObymobiException(WebserviceHandlerResult.IncorrectOrMissingParameters, "Could not find Company by TerminalId: '{0}'", terminalId);
                        }

                        CompanyEntity company = terminal.CompanyEntity;

                        if (company.Salt.IsNullOrWhiteSpace())
                        {
                            throw new ObymobiException(WebserviceHandlerResult.IncorrectOrMissingParameters, "Salt is missing for Company '{0}'", company.CompanyId);
                        }

                        if (!Hasher.IsHashValidForParameters(hash, company.Salt, timestamp, macAddress, this.OriginalAdditionalParameters))
                        {
                            throw new ObymobiException(ValidateCredentialsResult.RequestHashIsInvalid, "Tried hash for company: '{0}'", company.Name);
                        }
                    }
                    else
                    {
                        throw;
                    }
                }

                if (terminalId == 222) // Sprookjescamping
                {
                    deviceType = (int)DeviceType.Archos70;
                }

                // Get the terminal
                Terminal terminalModel = TerminalHelper.GetSetTerminalAndDevice(macAddress, terminalId, deviceType, applicationVersion, osVersion);
                TerminalHelper.FillTerminalAnnouncementActions(terminalModel);
                TerminalHelper.FillTerminalMessagegroups(terminalModel);                

                // Refetch data in case anything changed
                string cacheKey = Obymobi.Logic.HelperClasses.WebserviceHelper.CACHE_KEY_PREFIX + macAddress;
                Dionysos.Web.CacheHelper.Remove(false, cacheKey);
                device = Obymobi.Logic.HelperClasses.DeviceHelper.GetDeviceEntityByIdentifier(macAddress, false);
                if (device != null)
                {
                    // Re-cache : Also see WebserviceHelper.ValidateRequest
                    Dionysos.Web.CacheHelper.AddAbsoluteExpire(false, cacheKey, device, 1);
                }

                // Add the terminal to the result
                result.ModelCollection.Add(terminalModel);
            }
            catch (ObymobiException wex)
            {
                result.SetResult(wex);
                Exceptionlogger.CreateExceptionlog(wex);
            }
            catch (Exception ex)
            {
                result.SetResult(GenericWebserviceCallResult.Failure);
                Exceptionlogger.CreateExceptionlog(ex);
            }

            Requestlogger.UpdateRequestLog(result);

            return result;
        }

        public ObyTypedResult<PosIntegrationInformation> GetPosIntegrationInformation(long timestamp, string macAddress, string hash)
        {
            // Create the return value
            ObyTypedResult<PosIntegrationInformation> result = new ObyTypedResult<PosIntegrationInformation>();

            // Create a request log
            Requestlogger.CreateRequestlog("WebserviceHandler.GetPosIntegrationInformation", timestamp, macAddress, hash);

            try
            {
                // Validate the request
                TerminalEntity terminal = Obymobi.Logic.HelperClasses.WebserviceHelper.ValidateRequestRequireTerminal(timestamp, macAddress, hash, this.OriginalAdditionalParameters);

                PosIntegrationInformationHelper helper = new PosIntegrationInformationHelper(terminal.TerminalId, true);

                // Get the pos integration info
                PosIntegrationInformation posIntegrationInfo = helper.LoadData();

                // Add the info to the result
                result.ModelCollection.Add(posIntegrationInfo);
            }
            catch (ObymobiException wex)
            {
                result.SetResult(wex);
                Exceptionlogger.CreateExceptionlog(wex);
            }
            catch (Exception ex)
            {
                result.SetResult(GenericWebserviceCallResult.Failure);
                Exceptionlogger.CreateExceptionlog(ex);
            }

            Requestlogger.UpdateRequestLog(result);

            return result;
        }

        public ObyTypedResult<ClientStatus> GetClientStatuses(long timestamp, string macAddress, string hash)
        {
            // Create the return value
            ObyTypedResult<ClientStatus> result = new ObyTypedResult<ClientStatus>();

            // Create a request log
            Requestlogger.CreateRequestlog("WebserviceHandler.GetClientStatuses", timestamp, macAddress, hash);

            try
            {
                // Validate the request
                TerminalEntity terminal = Obymobi.Logic.HelperClasses.WebserviceHelper.ValidateRequestRequireTerminal(timestamp, macAddress, hash, this.OriginalAdditionalParameters);

                // TODO REWRITE / OPTIMIZE PERFORMANCE
                // Retrieve Clients for Terminal
                ClientCollection clientCollection = ClientHelper.GetClientEntitiesForCompany(terminal.CompanyId, terminal.TerminalId, true);

                foreach (ClientEntity clientEntity in clientCollection)
                {
                    ClientStatus status = new ClientStatus();
                    status.ClientStatusId = clientEntity.ClientId;

                    // Get Database State
                    status.Database = true; // We wouldn't be on this line if it weren't

                    // Update Operation Mode
                    if (clientEntity.ClientId > 0)
                    {
                        status.ClientOperationMode = (int)Obymobi.Logic.HelperClasses.ClientHelper.GetSetOperationMode(clientEntity, 0, clientEntity.ClientId, clientEntity.OperationModeEnum);
                    }
                    else
                    {
                        throw new ObymobiException(GenericWebserviceCallResult.Failure, "No client id or customer id");
                    }

                    // Check for processed order ids                  
                    status.ProcessedOrderIds = OrderHelper.GetLastProcessedOrderIds(0, null, 3);

                    // Add to the result
                    result.ModelCollection.Add(status);
                }
            }
            catch (ObymobiException wex)
            {
                result.SetResult(wex);
                Exceptionlogger.CreateExceptionlog(wex);
            }
            catch (Exception ex)
            {
                result.SetResult(GenericWebserviceCallResult.Failure);
                Exceptionlogger.CreateExceptionlog(ex);
            }

            Requestlogger.UpdateRequestLog(result);

            return result;
        }

        public ObyTypedResult<Product> GetProducts(long timestamp, string macAddress, string hash)
        {
            // Create the return value
            ObyTypedResult<Product> result = new ObyTypedResult<Product>();

            // Create a request log
            Requestlogger.CreateRequestlog("WebserviceHandler.GetProducts", timestamp, macAddress, hash);

            try
            {
                // Validate the request
                DeviceEntity device = Obymobi.Logic.HelperClasses.WebserviceHelper.ValidateRequest(timestamp, macAddress, hash, this.OriginalAdditionalParameters);

                // Get the products for the company
                Product[] products = ProductHelper.GetProducts(device.GetCompanyId().GetValueOrDefault(), null, true, false, device.Type);

                // Add the product to the result
                result.ModelCollection.AddRange(products);
            }
            catch (ObymobiException wex)
            {
                result.SetResult(wex);
                Exceptionlogger.CreateExceptionlog(wex);
            }
            catch (Exception ex)
            {
                result.SetResult(GenericWebserviceCallResult.Failure);
                Exceptionlogger.CreateExceptionlog(ex);
            }

            Requestlogger.UpdateRequestLog(result);

            return result;
        }

        public ObyTypedResult<Category> GetCategories(long timestamp, string macAddress, string hash)
        {
            // Create the return value
            ObyTypedResult<Category> result = new ObyTypedResult<Category>();

            // Create a request log
            Requestlogger.CreateRequestlog("WebserviceHandler.GetCategories", timestamp, macAddress, hash);

            try
            {
                // Validate the request
                DeviceEntity device = Obymobi.Logic.HelperClasses.WebserviceHelper.ValidateRequest(timestamp, macAddress, hash, this.OriginalAdditionalParameters);

                // Get the categories for the company
                Category[] categories = CategoryHelper.GetCategories(device.GetCompanyId().GetValueOrDefault(), null, false, false, null);

                // Add the categories to the result
                result.ModelCollection.AddRange(categories);
            }
            catch (ObymobiException wex)
            {
                result.SetResult(wex);
                Exceptionlogger.CreateExceptionlog(wex);
            }
            catch (Exception ex)
            {
                result.SetResult(GenericWebserviceCallResult.Failure);
                Exceptionlogger.CreateExceptionlog(ex);
            }

            Requestlogger.UpdateRequestLog(result);

            return result;
        }

        public ObyTypedResult<Terminal> GetTerminals(long timestamp, string macAddress, string hash)
        {
            // Create the return value
            ObyTypedResult<Terminal> result = new ObyTypedResult<Terminal>();

            // Create a request log
            Requestlogger.CreateRequestlog("WebserviceHandler.GetTerminals", timestamp, macAddress, hash);

            try
            {
                // Validate the request
                DeviceEntity device = Obymobi.Logic.HelperClasses.WebserviceHelper.ValidateRequest(timestamp, macAddress, hash, this.OriginalAdditionalParameters);

                // Get the terminals
                Terminal[] terminals = TerminalHelper.GetTerminalArrayByCompanyId(device.GetCompanyId().GetValueOrDefault());

                // Add the terminals to the result
                result.ModelCollection.AddRange(terminals);
            }
            catch (ObymobiException wex)
            {
                result.SetResult(wex);
                Exceptionlogger.CreateExceptionlog(wex);
            }
            catch (Exception ex)
            {
                result.SetResult(GenericWebserviceCallResult.Failure);
                Exceptionlogger.CreateExceptionlog(ex);
            }

            Requestlogger.UpdateRequestLog(result);

            return result;
        }

        public ObyTypedResult<Order> GetOrders(long timestamp, string macAddress, int statusCode, string statusMessage, string version, bool isConsole, bool isMasterConsole, string hash)
        {
            // Create the return value
            ObyTypedResult<Order> result = new ObyTypedResult<Order>();

            // Create a request log
            Requestlogger.CreateRequestlog("WebserviceHandler.GetOrders", timestamp, macAddress, statusCode, statusMessage, version, isConsole, isMasterConsole, hash);

            try
            {
                // Validate the request
                TerminalEntity terminal = Obymobi.Logic.HelperClasses.WebserviceHelper.ValidateRequestRequireTerminal(timestamp, macAddress, hash, this.OriginalAdditionalParameters);

                // Get an array of orders instances for the specified terminal                                    
                Order[] orders;
                if (isMasterConsole)
                {
                    List<Order> orderList = new List<Order>();

                    TerminalCollection terminals = new TerminalCollection();
                    terminals.GetMulti(new PredicateExpression(TerminalFields.CompanyId == terminal.CompanyId));

                    foreach (TerminalEntity terminalForMaster in terminals)
                    {
                        bool markAsRetrieved = (terminalForMaster.TerminalId == terminal.TerminalId);
                        orderList.AddRange(RoutingHelper.GetNonRetrievedOrdersForTerminal(terminalForMaster.TerminalId, isConsole, markAsRetrieved));
                        orderList.AddRange(RoutingHelper.GetUnprocessedOrdersForTerminal(terminalForMaster.TerminalId, markAsRetrieved));
                    }
                    orders = orderList.ToArray();
                }
                else
                {
                    orders = RoutingHelper.GetNonRetrievedOrdersForTerminal(terminal.TerminalId, isConsole, true);
                }

                Debug.WriteLine(StringUtil.FormatSafe("warning : Terminal {0} retrieved {1} Orders", terminal.Name, orders.Count()));

                // Add the orders to the result
                result.ModelCollection.AddRange(orders);
            }
            catch (ObymobiException wex)
            {
                result.SetResult(wex);
                Exceptionlogger.CreateExceptionlog(wex);
            }
            catch (Exception ex)
            {
                result.SetResult(GenericWebserviceCallResult.Failure);
                Exceptionlogger.CreateExceptionlog(ex);
            }

            Requestlogger.UpdateRequestLog(result);

            return result;
        }

        public ObyTypedResult<OrderRoutestephandlerSaveStatus> GetRoutestephandlerStatuses(long timestamp, string macAddress, string[] orderroutestephandlerGuids, string hash)
        {
            // Create the return value
            ObyTypedResult<OrderRoutestephandlerSaveStatus> result = new ObyTypedResult<OrderRoutestephandlerSaveStatus>();

            // Create a request log
            Requestlogger.CreateRequestlog("WebserviceHandler.GetRoutestephandlerStatuses", timestamp, macAddress, orderroutestephandlerGuids, hash);

            try
            {
                // Validate the request
                TerminalEntity terminal = Obymobi.Logic.HelperClasses.WebserviceHelper.ValidateRequestRequireTerminal(timestamp, macAddress, hash, this.OriginalAdditionalParameters);

                // Update to be expired RoutestephandlerStatuses
                RoutingHelper.FailExpiredRoutestephandlers(terminal.TerminalId, null);

                // Get the Order Statuses
                List<OrderRoutestephandlerSaveStatus> orderStatuses = RoutingHelper.GetRoutestephandlerStatuses(orderroutestephandlerGuids.ToList());

                // Add the statuses to the result
                result.ModelCollection.AddRange(orderStatuses);
            }
            catch (ObymobiException wex)
            {
                result.SetResult(wex);
                Exceptionlogger.CreateExceptionlog(wex);
            }
            catch (Exception ex)
            {
                result.SetResult(GenericWebserviceCallResult.Failure);
                Exceptionlogger.CreateExceptionlog(ex);
            }

            Requestlogger.UpdateRequestLog(result);

            return result;
        }

        public ObyTypedResult<TerminalOperationMode> GetTerminalOperationMode(long timestamp, string macAddress, string hash)
        {
            // Create the return value
            ObyTypedResult<TerminalOperationMode> result = new ObyTypedResult<TerminalOperationMode>();

            // Create a request log
            Requestlogger.CreateRequestlog("WebserviceHandler.GetTerminalOperationMode", timestamp, macAddress, hash);

            try
            {
                // Validate the credentials
                TerminalEntity terminal = Obymobi.Logic.HelperClasses.WebserviceHelper.ValidateRequestRequireTerminal(timestamp, macAddress, hash, this.OriginalAdditionalParameters);

                // Get the operation mode
                TerminalOperationMode mode = Obymobi.Logic.HelperClasses.TerminalHelper.GetTerminalOperationMode(terminal.TerminalId);

                // Add the operation mode to the result
                result.ModelCollection.Add(mode);
            }
            catch (ObymobiException wex)
            {
                result.SetResult(wex);
                Exceptionlogger.CreateExceptionlog(wex);
            }
            catch (Exception ex)
            {
                result.SetResult(GenericWebserviceCallResult.Failure);
                Exceptionlogger.CreateExceptionlog(ex);
            }

            Requestlogger.UpdateRequestLog(result);

            return result;
        }

        public ObyTypedResult<SimpleWebserviceResult> GetPmsFolio(long timestamp, string macAddress, string hash)
        {
            // Create the return value
            ObyTypedResult<SimpleWebserviceResult> result = new ObyTypedResult<SimpleWebserviceResult>();

            // Create a request log
            Requestlogger.CreateRequestlog("WebserviceHandler.GetPmsFolio", timestamp, macAddress);

            try
            {
                // Validate the request
                ClientEntity client = Obymobi.Logic.HelperClasses.WebserviceHelper.ValidateRequestRequireClient(timestamp, macAddress, hash, this.OriginalAdditionalParameters);

                // Send a Poke to  the Terminal to retrieve the PMS info.
                TerminalEntity terminal = PmsHelperWrapper.GetPmsConnectedTerminalByClientId(client.ClientId);
                DeliverypointEntity deliverypoint = client.DeliverypointId.HasValue ? new DeliverypointEntity(client.DeliverypointId.Value) : null;

                if (terminal == null)
                {
                    throw new ObymobiException(WebserviceHandlerResult.IncorrectOrMissingParameters, "TerminalId not supplied or non-existing");
                }
                if (deliverypoint == null)
                {
                    throw new ObymobiException(WebserviceHandlerResult.IncorrectOrMissingParameters, "DeliverypointId not set on Client");
                }
                // Forward message                    
                CometHelper.GetPmsFolio(terminal.TerminalId, deliverypoint.Number);

                // Set Results
                result.ModelCollection.Add(new SimpleWebserviceResult { Succes = true });
            }
            catch (ObymobiException wex)
            {
                result.SetResult(wex);
                Exceptionlogger.CreateExceptionlog(wex);
            }
            catch (Exception ex)
            {
                result.SetResult(GenericWebserviceCallResult.Failure, ex);
                Exceptionlogger.CreateExceptionlog(ex);
            }

            Requestlogger.UpdateRequestLog(result);

            return result;
        }

        public ObyTypedResult<SimpleWebserviceResult> GetPmsGuestInformation(long timestamp, string macAddress, string hash)
        {
            // Create the return value
            ObyTypedResult<SimpleWebserviceResult> result = new ObyTypedResult<SimpleWebserviceResult>();

            // Create a request log
            Requestlogger.CreateRequestlog("WebserviceHandler.GetPmsGuestInformation", timestamp, macAddress, hash);

            try
            {
                // Validate the request
                ClientEntity client = Obymobi.Logic.HelperClasses.WebserviceHelper.ValidateRequestRequireClient(timestamp, macAddress, hash, this.OriginalAdditionalParameters);

                // Send a Poke to  the Terminal to retrieve the PMS info.
                TerminalEntity terminal = PmsHelperWrapper.GetPmsConnectedTerminalByClientId(client.ClientId);
                DeliverypointEntity deliverypoint = client.DeliverypointId.HasValue ? new DeliverypointEntity(client.DeliverypointId.Value) : null;

                if (terminal == null)
                {
                    throw new ObymobiException(WebserviceHandlerResult.IncorrectOrMissingParameters, "TerminalId not supplied or non-existing");
                }
                if (deliverypoint == null)
                {
                    throw new ObymobiException(WebserviceHandlerResult.IncorrectOrMissingParameters, "DeliverypointId not supplied or non-existing");
                }
                // Forward message                    
                CometHelper.GetPmsGuestInformation(terminal.TerminalId, deliverypoint.Number);

                // Set Results
                SimpleWebserviceResult resultModel = new SimpleWebserviceResult();
                resultModel.Succes = true;
                result.ModelCollection.Add(resultModel);
            }
            catch (ObymobiException wex)
            {
                result.SetResult(wex);
                Exceptionlogger.CreateExceptionlog(wex);
            }
            catch (Exception ex)
            {
                result.SetResult(GenericWebserviceCallResult.Failure, ex);
                Exceptionlogger.CreateExceptionlog(ex);
            }

            Requestlogger.UpdateRequestLog(result);

            return result;
        }

        public ObyTypedResult<SimpleWebserviceResult> GetPmsExpressCheckout(long timestamp, string macAddress, decimal balance, string hash)
        {
            // Create the return value
            ObyTypedResult<SimpleWebserviceResult> result = new ObyTypedResult<SimpleWebserviceResult>();

            // Create a request log
            Requestlogger.CreateRequestlog("WebserviceHandler.GetPmsExpressCheckout", timestamp, macAddress, balance, hash);

            try
            {
                // Validate the request
                ClientEntity client = Obymobi.Logic.HelperClasses.WebserviceHelper.ValidateRequestRequireClient(timestamp, macAddress, hash, this.OriginalAdditionalParameters);

                // Send a Poke to the Terminal to do the checkout
                TerminalEntity terminal = PmsHelperWrapper.GetPmsConnectedTerminalByClientId(client.ClientId);
                DeliverypointEntity deliverypoint = client.DeliverypointId.HasValue ? new DeliverypointEntity(client.DeliverypointId.Value) : null;

                if (terminal == null)
                {
                    throw new ObymobiException(WebserviceHandlerResult.IncorrectOrMissingParameters, "TerminalId not supplied or non-existing");
                }
                if (deliverypoint == null)
                {
                    throw new ObymobiException(WebserviceHandlerResult.IncorrectOrMissingParameters, "DeliverypointId not supplied or non-existing");
                }
                // Forward message     
                // GK TODO
                CometHelper.GetPmsExpressCheckout(terminal.TerminalId, deliverypoint.Number, balance);

                // Set Results
                SimpleWebserviceResult resultModel = new SimpleWebserviceResult();
                resultModel.Succes = true;
                result.ModelCollection.Add(resultModel);
            }
            catch (ObymobiException wex)
            {
                result.SetResult(wex);
                Exceptionlogger.CreateExceptionlog(wex);
            }
            catch (Exception ex)
            {
                result.SetResult(GenericWebserviceCallResult.Failure, ex);
                Exceptionlogger.CreateExceptionlog(ex);
            }

            Requestlogger.UpdateRequestLog(result);

            return result;
        }

        public ObyTypedResult<UIScheduleItem> GetUISchedule(long timestamp, string macAddress, int deliverypointgroupId, string hash)
        {
            // Create the return value
            ObyTypedResult<UIScheduleItem> result = new ObyTypedResult<UIScheduleItem>();

            // Create a request log
            Requestlogger.CreateRequestlog("WebserviceHandler.GetUISchedule", timestamp, macAddress, hash, deliverypointgroupId);

            try
            {
                // Validate the request
                DeviceEntity device = Obymobi.Logic.HelperClasses.WebserviceHelper.ValidateRequest(timestamp, macAddress, hash, this.OriginalAdditionalParameters);

                int companyId = device.GetCompanyId().GetValueOrDefault();

                DateTime companyDataLastModified = DateTime.MinValue;
                CompanyEntity companyEntity = GetCachedCompanyTimestamps(companyId);
                if (!companyEntity.IsNew)
                {
                    companyDataLastModified = companyEntity.CompanyDataLastModifiedUTC;
                }

                if (deliverypointgroupId == 0 && device.ClientEntitySingle != null)
                {
                    deliverypointgroupId = device.ClientEntitySingle.DeliverypointGroupId.GetValueOrDefault(0);
                }

                Obymobi.Logic.HelperClasses.CacheHelper.CacheParameters cacheParams = new Obymobi.Logic.HelperClasses.CacheHelper.CacheParameters("WebserviceHandler.GetUISchedule", companyDataLastModified.ToUnixTime().ToString(), GetApplicationVersion());
                cacheParams.Add(companyId);
                cacheParams.Add(deliverypointgroupId);

                UIScheduleItem[] uiScheduleItems;
                if (!Obymobi.Logic.HelperClasses.CacheHelper.TryGetValue(cacheParams, out uiScheduleItems))
                {
                    uiScheduleItems = UIScheduleHelper.GetUIScheduleDeliverypointgroup(deliverypointgroupId);

                    if (uiScheduleItems.Length > 0)
                    {
                        Obymobi.Logic.HelperClasses.CacheHelper.CacheObject(cacheParams, uiScheduleItems);
                    }
                }

                // Add the deliverypoints to the result
                result.ModelCollection.AddRange(uiScheduleItems);
            }
            catch (ObymobiException wex)
            {
                result.SetResult(wex);
                Exceptionlogger.CreateExceptionlog(wex);
            }
            catch (Exception ex)
            {
                result.SetResult(GenericWebserviceCallResult.Failure);
                Exceptionlogger.CreateExceptionlog(ex);
            }

            Requestlogger.UpdateRequestLog(result);

            return result;
        }

        public ObyTypedResult<PriceSchedule> GetPriceSchedule(long timestamp, string macAddress, int priceScheduleId, string hash)
        {
            // Create the return value
            ObyTypedResult<PriceSchedule> result = new ObyTypedResult<PriceSchedule>();

            // Create a request log
            Requestlogger.CreateRequestlog("WebserviceHandler.GetPriceSchedule", timestamp, macAddress, hash, priceScheduleId);

            try
            {
                // Validate the request
                DeviceEntity device = Obymobi.Logic.HelperClasses.WebserviceHelper.ValidateRequest(timestamp, macAddress, hash, this.OriginalAdditionalParameters);

                int companyId = device.GetCompanyId().GetValueOrDefault();

                DateTime companyDataLastModified = DateTime.MinValue;
                CompanyEntity companyEntity = GetCachedCompanyTimestamps(companyId);
                if (!companyEntity.IsNew)
                {
                    companyDataLastModified = companyEntity.CompanyDataLastModifiedUTC;
                }

                Obymobi.Logic.HelperClasses.CacheHelper.CacheParameters cacheParams = new Obymobi.Logic.HelperClasses.CacheHelper.CacheParameters("WebserviceHandler.GetPriceSchedule", companyDataLastModified.ToUnixTime().ToString(), GetApplicationVersion());
                cacheParams.Add(companyId);
                cacheParams.Add(priceScheduleId);

                PriceSchedule priceSchedule;
                if (!Obymobi.Logic.HelperClasses.CacheHelper.TryGetValue(cacheParams, out priceSchedule))
                {
                    // Get data
                    PriceScheduleEntity entity = PriceScheduleHelper.GetPriceScheduleEntity(priceScheduleId);

                    // Process data
                    priceSchedule = new PriceScheduleEntityConverter().ConvertEntityToModel(entity);

                    // Store data
                    if (priceSchedule != null)
                    {
                        Obymobi.Logic.HelperClasses.CacheHelper.CacheObject(cacheParams, priceSchedule);
                    }
                }

                result.ModelCollection.Add(priceSchedule);
            }
            catch (ObymobiException wex)
            {
                result.SetResult(wex);
                Exceptionlogger.CreateExceptionlog(wex);
            }
            catch (Exception ex)
            {
                result.SetResult(GenericWebserviceCallResult.Failure);
                Exceptionlogger.CreateExceptionlog(ex);
            }

            Requestlogger.UpdateRequestLog(result);

            return result;
        }

        public ObyTypedResult<CustomText> GetTranslations(long timestamp, string macAddress, string cultureCode, string hash)
        {
            // Create the return value
            ObyTypedResult<CustomText> result = new ObyTypedResult<CustomText>();

            // Create a request log
            Requestlogger.CreateRequestlog("WebserviceHandler.GetTranslations", timestamp, macAddress, cultureCode, hash);

            try
            {
                // Validate the request
                DeviceEntity device = Obymobi.Logic.HelperClasses.WebserviceHelper.ValidateRequest(timestamp, macAddress, hash, this.OriginalAdditionalParameters);

                int companyId = device.GetCompanyId().GetValueOrDefault();

                DateTime companyDataLastModified = DateTime.MinValue;
                CompanyEntity companyEntity = GetCachedCompanyTimestamps(companyId);
                if (!companyEntity.IsNew)
                {
                    companyDataLastModified = companyEntity.CompanyDataLastModifiedUTC;
                }

                Obymobi.Logic.HelperClasses.CacheHelper.CacheParameters cacheParams = new Obymobi.Logic.HelperClasses.CacheHelper.CacheParameters("WebserviceHandler.GetTranslations", companyDataLastModified.ToUnixTime().ToString(), GetApplicationVersion());
                cacheParams.Add(companyId);
                cacheParams.Add(cultureCode);

                CustomText[] customTexts;
                if (!Obymobi.Logic.HelperClasses.CacheHelper.TryGetValue(cacheParams, out customTexts))
                {
                    // Get data
                    CompanyTranslationManager translationManager = new CompanyTranslationManager(companyId, cultureCode);
                    translationManager.FetchData();                    

                    // Process data
                    customTexts = Obymobi.Logic.HelperClasses.CustomTextHelper.ConvertCustomTextCollectionToArray(translationManager.GetCustomTexts());

                    // Store data
                    if (customTexts != null)
                    {
                        Obymobi.Logic.HelperClasses.CacheHelper.CacheObject(cacheParams, customTexts);
                    }
                }

                result.ModelCollection.AddRange(customTexts);
            }
            catch (ObymobiException wex)
            {
                result.SetResult(wex);
                Exceptionlogger.CreateExceptionlog(wex);
            }
            catch (Exception ex)
            {
                result.SetResult(GenericWebserviceCallResult.Failure);
                Exceptionlogger.CreateExceptionlog(ex);
            }

            Requestlogger.UpdateRequestLog(result);

            return result;
        }

        public ObyTypedResult<SimpleWebserviceResult> SetPmsWakeUp(long timestamp, string macAddress, WakeUpStatus wakeUpStatus, string hash)
        {
            // Create the return value
            ObyTypedResult<SimpleWebserviceResult> result = new ObyTypedResult<SimpleWebserviceResult>();

            // Create a request log
            Requestlogger.CreateRequestlog("WebserviceHandler.SetPmsWakeUp", timestamp, macAddress, wakeUpStatus, hash);

            try
            {
                // Validate the company credentials
                TerminalEntity terminal = Obymobi.Logic.HelperClasses.WebserviceHelper.ValidateRequestRequireTerminal(timestamp, macAddress, hash, this.OriginalAdditionalParameters);
                if (StringUtil.IsNullOrWhiteSpace(wakeUpStatus.DeliverypointNumber))
                {
                    result.SetResult(SetPmsCheckedInResult.DeliverypointNumberMissing);
                }
                else
                {
                    DeliverypointEntity deliverypoint = Obymobi.Logic.HelperClasses.DeliverypointHelper.GetDeliverypoint(terminal.CompanyId, wakeUpStatus.DeliverypointNumber, false, true, terminal.DeliverypointgroupId);
                    if (deliverypoint == null || deliverypoint.IsNew)
                    {
                        result.SetResult(SetPmsCheckedInResult.DeliverypointNumberUnknown);
                    }
                    else
                    {
                        // Forward wakeUpStatus to PMS 
                        JsonSerializerSettings settings = new JsonSerializerSettings();
                        settings.NullValueHandling = NullValueHandling.Ignore;
                        settings.ContractResolver = new CamelCasePropertyNamesContractResolver();
                        string json = JsonConvert.SerializeObject(wakeUpStatus, new Formatting(), settings);

                        CometHelper.SetPmsWakeUp(deliverypoint.DeliverypointId, json);

                        // Add the result
                        result.ModelCollection.Add(new SimpleWebserviceResult(true));
                    }
                }
            }
            catch (ObymobiException wex)
            {
                result.SetResult(wex);
                Exceptionlogger.CreateExceptionlog(wex);
            }
            catch (Exception ex)
            {
                result.SetResult(GenericWebserviceCallResult.Failure, ex);
                Exceptionlogger.CreateExceptionlog(ex);
            }

            Requestlogger.UpdateRequestLog(result);

            return result;
        }

        public ObyTypedResult<SimpleWebserviceResult> ClearPmsWakeUp(long timestamp, string macAddress, WakeUpStatus wakeUpStatus, string hash)
        {
            // Create the return value
            ObyTypedResult<SimpleWebserviceResult> result = new ObyTypedResult<SimpleWebserviceResult>();

            // Create a request log
            Requestlogger.CreateRequestlog("WebserviceHandler.ClearPmsWakeUpStatus", timestamp, macAddress, wakeUpStatus, hash);

            try
            {
                // Validate the company credentials
                TerminalEntity terminal = Obymobi.Logic.HelperClasses.WebserviceHelper.ValidateRequestRequireTerminal(timestamp, macAddress, hash, this.OriginalAdditionalParameters);
                if (StringUtil.IsNullOrWhiteSpace(wakeUpStatus.DeliverypointNumber))
                {
                    result.SetResult(SetPmsCheckedInResult.DeliverypointNumberMissing);
                }
                else
                {
                    DeliverypointEntity deliverypoint = Obymobi.Logic.HelperClasses.DeliverypointHelper.GetDeliverypoint(terminal.CompanyId, wakeUpStatus.DeliverypointNumber, false, true, terminal.DeliverypointgroupId);
                    if (deliverypoint == null || deliverypoint.IsNew)
                    {
                        result.SetResult(SetPmsCheckedInResult.DeliverypointNumberUnknown);
                    }
                    else
                    {
                        // Forward wakeUpStatus to PMS 
                        JsonSerializerSettings settings = new JsonSerializerSettings();
                        settings.NullValueHandling = NullValueHandling.Ignore;
                        settings.ContractResolver = new CamelCasePropertyNamesContractResolver();
                        string json = JsonConvert.SerializeObject(wakeUpStatus, new Formatting(), settings);

                        CometHelper.ClearPmsWakeUp(deliverypoint.DeliverypointId, json);

                        // Add the result
                        result.ModelCollection.Add(new SimpleWebserviceResult(true));
                    }
                }
            }
            catch (ObymobiException wex)
            {
                result.SetResult(wex);
                Exceptionlogger.CreateExceptionlog(wex);
            }
            catch (Exception ex)
            {
                result.SetResult(GenericWebserviceCallResult.Failure, ex);
                Exceptionlogger.CreateExceptionlog(ex);
            }

            Requestlogger.UpdateRequestLog(result);

            return result;
        }

        public ObyTypedResult<SimpleWebserviceResult> SetClientWakeUpTime(long timestamp, string macAddress, string wakeUpTime, string hash)
        {
            // Create the return value
            ObyTypedResult<SimpleWebserviceResult> result = new ObyTypedResult<SimpleWebserviceResult>();

            // Create a request log
            Requestlogger.CreateRequestlog("WebserviceHandler.SetClientWakeUpTimer", timestamp, macAddress, wakeUpTime, hash);

            try
            {
                // Validate the request
                DeviceEntity device = Obymobi.Logic.HelperClasses.WebserviceHelper.ValidateRequest(timestamp, macAddress, hash, this.OriginalAdditionalParameters);

                if (device.ClientEntitySingle != null)
                {
                    if (string.IsNullOrWhiteSpace(wakeUpTime))
                    {
                        device.ClientEntitySingle.WakeUpTimeUtc = null;

                        Obymobi.Logic.HelperClasses.ClientLogHelper.CreateClientLogEntity(device.ClientEntitySingle.ClientId, ClientLogType.ClientWakeUpSet, "Wake-up has been cleared", 0).Save();
                    }
                    else
                    {
                        DateTime time = DateTime.ParseExact(wakeUpTime, "dd-MM-yyyy HH:mm:ss", CultureInfo.InvariantCulture);
                        DateTime timeUtc = TimeZoneInfo.ConvertTimeToUtc(time, device.ClientEntitySingle.CompanyEntity.TimeZoneInfo);
                        device.ClientEntitySingle.WakeUpTimeUtc = timeUtc;

                        string message = string.Format("Wake-up set to {0:g} UTC (Raw: {1}, Parsed: {2:g})", timeUtc, wakeUpTime, time);

                        Obymobi.Logic.HelperClasses.ClientLogHelper.CreateClientLogEntity(device.ClientEntitySingle.ClientId, ClientLogType.ClientWakeUpSet, message, 0).Save();
                    }
                    device.ClientEntitySingle.Save();
                    result.SetResult(GenericWebserviceCallResult.Success);
                }
            }
            catch (ObymobiException wex)
            {
                result.SetResult(wex);
                Exceptionlogger.CreateExceptionlog(wex);
            }
            catch (Exception ex)
            {
                result.SetResult(GenericWebserviceCallResult.Failure);
                Exceptionlogger.CreateExceptionlog(ex);
            }

            Requestlogger.UpdateRequestLog(result);

            return result;
        }

        public ObyTypedResult<Site> GetSite(long timestamp, string macAddress, int siteId, int deviceTypeInt, string languageCode, string cultureCode, string hash)
        {
            ObyTypedResult<Site> result = new ObyTypedResult<Site>();

            try
            {
                // Validate the request
                ClientEntity clientEntity = Obymobi.Logic.HelperClasses.WebserviceHelper.ValidateRequestRequireClient(timestamp, macAddress, hash, this.OriginalAdditionalParameters);

                // Parse DeviceType
                DeviceType deviceType;
                if (!EnumUtil.TryParse(deviceTypeInt, out deviceType))
                {
                    throw new ObymobiException(GetCompanyResult.InvalidDeviceType, deviceTypeInt.ToString());
                }

                Site site;
                const bool tablet = true;
                int companyId = 0;

                long ticks = SiteHelper.GetSiteTicks(siteId);

                bool isGenericSite = SiteHelper.IsGenericSite(siteId);
                if (!isGenericSite)
                {
                    companyId = clientEntity.CompanyId;
                }

                // Create the cache parameters
                Obymobi.Logic.HelperClasses.CacheHelper.CacheParameters cacheParams = new Obymobi.Logic.HelperClasses.CacheHelper.CacheParameters("WebserviceHandler.GetSite", ticks.ToString(), GetApplicationVersion());
                cacheParams.Add(companyId);
                cacheParams.Add(siteId);
                cacheParams.Add(languageCode);
                cacheParams.Add(deviceType);
                cacheParams.Add(tablet);
                cacheParams.Add(cultureCode);

                if (!Obymobi.Logic.HelperClasses.CacheHelper.TryGetValue(cacheParams, out site))
                {
                    if (cultureCode.IsNullOrWhiteSpace())
                    {
                        cultureCode = Obymobi.Logic.HelperClasses.CompanyCultureHelper.GetCultureByCompanyLanguageCode(companyId, languageCode).Code;
                    }

                    SiteEntity siteEntity = SiteHelper.GetTranslatedSite(siteId, cultureCode, deviceType, tablet);
                    if (siteEntity != null)
                    {
                        site = new Obymobi.Api.Logic.EntityConverters.Mobile.SiteEntityConverter(deviceType).ConvertEntityToModel(siteEntity);
                        Obymobi.Logic.HelperClasses.CacheHelper.CacheObject(cacheParams, site);
                    }
                }

                result.ModelCollection.Add(site);
                result.SetResult(GenericWebserviceCallResult.Success);
            }
            catch (ObymobiException wex)
            {
                if (wex.ErrorEnumValue is SiteHelper.SiteHelperResult)
                {
                    switch ((SiteHelper.SiteHelperResult)wex.ErrorEnumValue)
                    {
                        case SiteHelper.SiteHelperResult.NoSiteIsExistingForSiteId:
                            result.SetResult(GetSiteResult.InvalidSiteId);
                            break;
                        case SiteHelper.SiteHelperResult.LanguageCodeInvalid:
                            result.SetResult(GetSiteResult.InvalidLanguageCode);
                            break;
                        default:
                            result.SetResult(GetSiteResult.Failure);
                            break;
                    }
                }
                else if (wex.ErrorEnumValue is ValidateCredentialsResult)
                {
                    result.SetResult(GetSiteResult.AuthenticationError, wex);
                }
                else
                {
                    result.SetResult(GetSiteResult.Failure, wex);
                }
            }
            catch (Exception ex)
            {
                result.SetResult(GenericWebserviceCallResult.Failure, ex);
            }

            Requestlogger.UpdateRequestLog(result);

            return result;
        }

        public ObyTypedResult<SimpleWebserviceResult> GetSiteTicks(long timestamp, string macAddress, int siteId, string hash)
        {
            // Create the return value
            ObyTypedResult<SimpleWebserviceResult> result = new ObyTypedResult<SimpleWebserviceResult>();

            // Create a request log
            Requestlogger.CreateRequestlog("WebserviceHandler.GetSiteTicks", timestamp, macAddress, siteId, hash);

            try
            {
                // Validate the request
                Obymobi.Logic.HelperClasses.WebserviceHelper.ValidateRequestRequireClient(timestamp, macAddress, hash, this.OriginalAdditionalParameters);

                // Get the Point of Interest              
                long ticks = SiteHelper.GetSiteTicks(siteId);

                SimpleWebserviceResult resultModel = new SimpleWebserviceResult();
                resultModel.Succes = true;
                resultModel.Ticks = ticks;

                result.ModelCollection.Add(resultModel);
                result.SetResult(GetSiteTicksResult.Success);
            }
            catch (ObymobiException wex)
            {
                if (wex.ErrorEnumValue is GetSiteTicksResult)
                {
                    result.SetResult(wex);
                }
                else if (wex.ErrorEnumValue is ValidateCredentialsResult value)
                {
                    if (value == ValidateCredentialsResult.TimestampIsOutOfRange)
                    {
                        result.SetResult(ValidateCredentialsResult.TimestampIsOutOfRange);
                    }
                    else
                    {
                        result.SetResult(GetSiteTicksResult.AuthenticationError);
                    }
                }
                else
                {
                    result.SetResult(GetSiteTicksResult.Failure);
                }

                Exceptionlogger.CreateExceptionlog(wex);
            }
            catch (Exception ex)
            {
                result.SetResult(GetSiteTicksResult.Failure);
                Exceptionlogger.CreateExceptionlog(ex);
            }

            Requestlogger.UpdateRequestLog(result);

            return result;
        }

        public ObyTypedResult<Company> GetPointOfInterest(long timestamp, string macAddress, int pointOfInterestId, int deviceTypeInt, string hash)
        {
            // Create the return value
            ObyTypedResult<Company> result = new ObyTypedResult<Company>();

            // Create a request log
            Requestlogger.CreateRequestlog("WebserviceHandler.GetPointOfInterest", timestamp, macAddress, pointOfInterestId, hash);

            try
            {
                // Validate the credentials
                Obymobi.Logic.HelperClasses.WebserviceHelper.ValidateRequest(timestamp, macAddress, hash, pointOfInterestId, deviceTypeInt);

                // Parse DeviceType
                DeviceType deviceType;
                if (!EnumUtil.TryParse(deviceTypeInt, out deviceType))
                {
                    throw new ObymobiException(GetPointOfInterestResult.InvalidDeviceType, deviceTypeInt.ToString());
                }

                PointOfInterestEntity poiEntity = new PointOfInterestEntity(pointOfInterestId, PointOfInterestHelper.GetPrefetchPathForMobileWebservice());
                if (poiEntity.IsNew)
                {
                    throw new ObymobiException(GetPointOfInterestResult.PointOfInterestIdUnknown, "Id: {0}", poiEntity.PointOfInterestId);
                }

                // Create the Company model
                Company company = new Obymobi.Api.Logic.EntityConverters.Mobile.PointOfInterestEntityConverter(false, deviceType, true).ConvertEntityToModel(poiEntity);

                result.ModelCollection.Add(company);
                result.SetResult(GetPointOfInterestResult.Success);
            }
            catch (ObymobiException wex)
            {
                if (wex.ErrorEnumValue is GetPointOfInterestResult)
                {
                    result.SetResult(wex);
                }
                else if (wex.ErrorEnumValue is ValidateCredentialsResult)
                {
                    result.SetResult(GetPointOfInterestResult.AuthenticationError);
                }
                else
                {
                    result.SetResult(GetPointOfInterestResult.Failure);
                }

                Exceptionlogger.CreateExceptionlog(wex);
            }
            catch (Exception ex)
            {
                result.SetResult(GetPointOfInterestResult.Failure);
                Exceptionlogger.CreateExceptionlog(ex);
            }

            Requestlogger.UpdateRequestLog(result);

            return result;
        }

        public ObyTypedResult<SimpleWebserviceResult> GetPointOfInterestTicks(long timestamp, string macAddress, int pointOfInterestId, string hash)
        {
            // Create the return value
            ObyTypedResult<SimpleWebserviceResult> result = new ObyTypedResult<SimpleWebserviceResult>();

            // Create a request log
            Requestlogger.CreateRequestlog("MobileWebserviceHandler.GetPointOfInterestTicks", timestamp, macAddress, pointOfInterestId, hash);

            try
            {
                // Validate the credentials
                Obymobi.Logic.HelperClasses.WebserviceHelper.ValidateRequestRequireClient(timestamp, macAddress, hash, this.OriginalAdditionalParameters);

                // Get the Point of Interest              
                PointOfInterestEntity poiEntity = new PointOfInterestEntity(pointOfInterestId);
                if (poiEntity.IsNew)
                {
                    throw new ObymobiException(GetPointOfInterestTicksResult.UnknownPointOfInterestId, "Id: {0}", pointOfInterestId);
                }

                SimpleWebserviceResult resultModel = new SimpleWebserviceResult();
                resultModel.Succes = true;
                resultModel.Ticks = poiEntity.LastModifiedUTC.Ticks;

                result.ModelCollection.Add(resultModel);
                result.SetResult(GetCompanyTicksResult.Success);
            }
            catch (ObymobiException wex)
            {
                if (wex.ErrorEnumValue is GetPointOfInterestTicksResult)
                {
                    result.SetResult(wex);
                }
                else if (wex.ErrorEnumValue is ValidateCredentialsResult)
                {
                    result.SetResult(GetPointOfInterestTicksResult.AuthenticationError);
                }
                else
                {
                    result.SetResult(GetPointOfInterestTicksResult.Failure);
                }

                Exceptionlogger.CreateExceptionlog(wex);
            }
            catch (Exception ex)
            {
                result.SetResult(GetPointOfInterestTicksResult.Failure);
                Exceptionlogger.CreateExceptionlog(ex);
            }

            Requestlogger.UpdateRequestLog(result);

            return result;
        }

        public ObyTypedResult<RoomControlConfiguration> GetRoomControlConfiguration(long timestamp, string macAddress, string hash)
        {
            // Create the return value
            ObyTypedResult<RoomControlConfiguration> result = new ObyTypedResult<RoomControlConfiguration>();

            // Create a request log
            Requestlogger.CreateRequestlog("WebserviceHandler.GetRoomControlConfigurations", timestamp, macAddress, hash);

            try
            {
                // Validate the request
                DeviceEntity device = Obymobi.Logic.HelperClasses.WebserviceHelper.ValidateRequest(timestamp, macAddress, hash, this.OriginalAdditionalParameters);
                if (device.ClientEntitySingle != null)
                {
                    RoomControlConfiguration[] configurations = null;
                    Obymobi.Logic.HelperClasses.CacheHelper.CacheParameters cacheParams = null;

                    ClientEntity clientEntity = device.ClientEntitySingle;
                    if (clientEntity.DeliverypointId.HasValue && clientEntity.DeliverypointEntity.RoomControlConfigurationId.HasValue)
                    {
                        cacheParams = new Obymobi.Logic.HelperClasses.CacheHelper.CacheParameters("WebserviceHandler.GetRoomControlConfigurations", "0", GetApplicationVersion());
                        cacheParams.Add(clientEntity.DeliverypointEntity.RoomControlConfigurationId.Value);

                        if (!Obymobi.Logic.HelperClasses.CacheHelper.TryGetValue(cacheParams, out configurations))
                        {
                            configurations = null;
                        }
                    }
                    
                    if (configurations == null)
                    {
                        // Get the room control configurations
                        if (clientEntity.DeliverypointId.HasValue && clientEntity.DeliverypointEntity.RoomControlConfigurationId.HasValue)
                        {
                            configurations = RoomControlConfigurationHelper.GetRoomControlConfiguration(clientEntity.DeliverypointEntity.RoomControlConfigurationId.Value);
                        }
                        else
                        {
                            configurations = new RoomControlConfiguration[0];
                        }
                        
                        if (configurations.Length > 0 && cacheParams != null)
                        {
                            Obymobi.Logic.HelperClasses.CacheHelper.CacheObject(cacheParams, configurations);
                        }
                    }

                    // Add the configuration to the result
                    result.ModelCollection.AddRange(configurations);
                }
            }
            catch (ObymobiException wex)
            {
                result.SetResult(wex);
                Exceptionlogger.CreateExceptionlog(wex);
            }
            catch (Exception ex)
            {
                result.SetResult(GenericWebserviceCallResult.Failure);
                Exceptionlogger.CreateExceptionlog(ex);
            }

            Requestlogger.UpdateRequestLog(result);

            return result;
        }

        public ObyTypedResult<SimpleWebserviceResult> PublishRoomControlConfiguration(long timestamp, string macAddress, int roomControlConfigurationId, string hash)
        {
            // Create the return value
            ObyTypedResult<SimpleWebserviceResult> result = new ObyTypedResult<SimpleWebserviceResult>();

            // Create a request log
            Requestlogger.CreateRequestlog("WebserviceHandler.PublishRoomControlConfiguration", timestamp, macAddress, hash);

            try
            {
                // Validate the request
                Obymobi.Logic.HelperClasses.WebserviceHelper.ValidateRequest(timestamp, macAddress, hash, this.OriginalAdditionalParameters);

                Obymobi.Logic.HelperClasses.CacheHelper.CacheParameters cacheParams = new Obymobi.Logic.HelperClasses.CacheHelper.CacheParameters("WebserviceHandler.GetRoomControlConfigurations", "0", GetApplicationVersion());
                cacheParams.Add(roomControlConfigurationId);

                RoomControlConfiguration[] configurations = RoomControlConfigurationHelper.GetRoomControlConfiguration(roomControlConfigurationId);
                Obymobi.Logic.HelperClasses.CacheHelper.CacheObject(cacheParams, configurations);

                result.ModelCollection.Add(new SimpleWebserviceResult(true));
            }
            catch (ObymobiException wex)
            {
                result.SetResult(wex);
                Exceptionlogger.CreateExceptionlog(wex);
            }
            catch (Exception ex)
            {
                result.SetResult(GenericWebserviceCallResult.Failure);
                Exceptionlogger.CreateExceptionlog(ex);

                Requestlogger.UpdateRequestLog(result);
            }

            Requestlogger.UpdateRequestLog(result);

            return result;
        }

        public ObyTypedResult<SimpleWebserviceResult> GetEstimatedDeliveryTime(long timestamp, string macAddress, int deliverypointgroupId, string hash)
        {
            ObyTypedResult<SimpleWebserviceResult> result = new ObyTypedResult<SimpleWebserviceResult>();

            // Create a request log
            Requestlogger.CreateRequestlog("WebserviceHandler.GetEstimatedDeliveryTime", timestamp, macAddress, hash);

            try
            {
                // Validate the request
                Obymobi.Logic.HelperClasses.WebserviceHelper.ValidateRequest(timestamp, macAddress, hash, this.OriginalAdditionalParameters);

                string cacheKey = "GetEstimatedDeliveryTime-" + deliverypointgroupId;

                SimpleWebserviceResult deliveryTimeResult;
                if (Obymobi.Web.Caching.RedisCacheHelper.Instance.RedisEnabled)
                {
                    if (!Obymobi.Web.Caching.RedisCacheHelper.Instance.CacheStore.TryGetValue(cacheKey, out deliveryTimeResult))
                    {
                        deliveryTimeResult = null;
                    }
                }
                else if (!Dionysos.Web.CacheHelper.TryGetValue(cacheKey, false, out deliveryTimeResult))
                {
                    deliveryTimeResult = null;
                }

                if (deliveryTimeResult == null)
                {
                    DeliveryTime deliveryTime = DeliveryTimeHelper.GetDeliveryTimeForDeliverypointgroup(deliverypointgroupId);
                    if (deliveryTime != null)
                    {
                        deliveryTimeResult = new SimpleWebserviceResult();
                        deliveryTimeResult.Succes = true;
                        deliveryTimeResult.Ticks = deliveryTime.Time;

                        // Cache it
                        Dionysos.Web.CacheHelper.Add(false, cacheKey, deliveryTimeResult);
                    }
                    else
                    {
                        result.SetResult(GenericWebserviceCallResult.EntityNotFoundByPrimaryKey);
                    }
                }

                result.ModelCollection.Add(deliveryTimeResult);
            }
            catch (ObymobiException wex)
            {
                result.SetResult(wex);
                Exceptionlogger.CreateExceptionlog(wex);
            }
            catch (Exception ex)
            {
                result.SetResult(GenericWebserviceCallResult.Failure);
                Exceptionlogger.CreateExceptionlog(ex);
            }

            Requestlogger.UpdateRequestLog(result);

            return result;
        }

        public ObyTypedResult<DeliveryTime> GetEstimatedDeliveryTimes(long timestamp, string macAddress, string hash)
        {
            ObyTypedResult<DeliveryTime> result = new ObyTypedResult<DeliveryTime>();

            // Create a request log
            Requestlogger.CreateRequestlog("WebserviceHandler.GetEstimatedDeliveryTimes", timestamp, macAddress, hash);

            try
            {
                // Validate the request
                TerminalEntity terminal = Obymobi.Logic.HelperClasses.WebserviceHelper.ValidateRequestRequireTerminal(timestamp, macAddress, hash, this.OriginalAdditionalParameters);

                string cacheKey = "GetEstimatedDeliveryTimes-" + terminal.CompanyId;

                DeliveryTime[] deliveryTimes;
                if (Obymobi.Web.Caching.RedisCacheHelper.Instance.RedisEnabled)
                {
                    if (!Obymobi.Web.Caching.RedisCacheHelper.Instance.CacheStore.TryGetValue(cacheKey, out deliveryTimes))
                    {
                        deliveryTimes = null;
                    }
                }
                else if (!Dionysos.Web.CacheHelper.TryGetValue(cacheKey, false, out deliveryTimes))
                {
                    deliveryTimes = null;
                }

                if (deliveryTimes == null)
                {
                    // Get the delivery times by companyId
                    deliveryTimes = Obymobi.Logic.HelperClasses.DeliveryTimeHelper.GetDeliveryTimesForCompany(terminal.CompanyId).ToArray();

                    // Cache
                    Dionysos.Web.CacheHelper.Add(false, cacheKey, deliveryTimes);
                }

                result.ModelCollection.AddRange(deliveryTimes);
            }
            catch (ObymobiException wex)
            {
                result.SetResult(wex);
                Exceptionlogger.CreateExceptionlog(wex);
            }
            catch (Exception ex)
            {
                result.SetResult(GenericWebserviceCallResult.Failure);
                Exceptionlogger.CreateExceptionlog(ex);
            }

            Requestlogger.UpdateRequestLog(result);

            return result;
        }

        public ObyTypedResult<Map> GetMap(long timestamp, string macAddress, int mapId, string hash)
        {
            ObyTypedResult<Map> result = new ObyTypedResult<Map>();

            Requestlogger.CreateRequestlog("WebserviceHandler.GetMap", timestamp, macAddress, mapId, hash);

            try
            {
                Obymobi.Logic.HelperClasses.WebserviceHelper.ValidateRequest(timestamp, macAddress, hash, this.OriginalAdditionalParameters);

                Obymobi.Logic.HelperClasses.CacheHelper.CacheParameters cacheParams = new Obymobi.Logic.HelperClasses.CacheHelper.CacheParameters("WebserviceHandler.Map", "0", "");
                cacheParams.Add(mapId);

                Map map;
                if (!Obymobi.Logic.HelperClasses.CacheHelper.TryGetValue(cacheParams, out map))
                {
                    map = null;
                }

                result.ModelCollection.Add(map);
            }
            catch (ObymobiException wex)
            {
                result.SetResult(wex);
                Exceptionlogger.CreateExceptionlog(wex);
            }
            catch (Exception ex)
            {
                result.SetResult(GenericWebserviceCallResult.Failure);
                Exceptionlogger.CreateExceptionlog(ex);
            }

            Requestlogger.UpdateRequestLog(result);

            return result;
        }

        public ObyTypedResult<SimpleWebserviceResult> GetMapTimestamp(long timestamp, string macAddress, int mapId, string hash)
        {
            GetMapTimestampRequest request = new GetMapTimestampRequest
            {
                MapId = mapId
            };
            return this.GetTimestamp(timestamp, macAddress, request, hash);
        }

        private ObyTypedResult<SimpleWebserviceResult> GetTimestamp(long timestamp, string macAddress, ITimestamped timestamped, string hash)
        {
            // Create the return value
            ObyTypedResult<SimpleWebserviceResult> result = new ObyTypedResult<SimpleWebserviceResult>();

            try
            {
                TimestampContainer container = timestamped.TimestampContainer;
                string key = timestamped.TimestampContainer.GenerateKey();

                Obymobi.Logic.HelperClasses.CacheHelper.CacheParameters cacheParams = new Obymobi.Logic.HelperClasses.CacheHelper.CacheParameters(key, "0", "");

                long ticks;
                if (Obymobi.Logic.HelperClasses.CacheHelper.TryGetValue(cacheParams, out ticks))
                {
                    SimpleWebserviceResult cachedTicks = new SimpleWebserviceResult(true);
                    cachedTicks.Ticks = ticks;
                    result.ModelCollection.Add(cachedTicks);
                }
            }
            catch (ObymobiException wex)
            {
                result.SetResult(wex);
                Exceptionlogger.CreateExceptionlog(wex);
            }
            catch (Exception ex)
            {
                result.SetResult(GenericWebserviceCallResult.Failure);
                Exceptionlogger.CreateExceptionlog(ex);                
            }            

            return result;
        }

        #endregion

        #region PUT methods        

        public ObyTypedResult<SimpleWebserviceResult> PublishMapTimestamp(long timestamp, string macAddress, int mapId, string hash)
        {
            GetMapTimestampRequest request = new GetMapTimestampRequest
            {
                MapId = mapId
            };
            return this.PublishTimestamp(timestamp, macAddress, request, hash);
        }

        private ObyTypedResult<SimpleWebserviceResult> PublishTimestamp(long timestamp, string macAddress, ITimestamped timestamped, string hash)
        {
            // Create the return value
            ObyTypedResult<SimpleWebserviceResult> result = new ObyTypedResult<SimpleWebserviceResult>();

            // Create a request log
            Requestlogger.CreateRequestlog("WebserviceHandler.PublishTimestamp", timestamp, macAddress, hash);

            try
            {
                // Validate the request
                Obymobi.Logic.HelperClasses.WebserviceHelper.ValidateRequest(timestamp, macAddress, hash, this.OriginalAdditionalParameters);

                TimestampContainer container = timestamped.TimestampContainer;
                string key = timestamped.TimestampContainer.GenerateKey();

                Obymobi.Logic.HelperClasses.CacheHelper.CacheParameters cacheParams = new Obymobi.Logic.HelperClasses.CacheHelper.CacheParameters(key, "0", "");
                Obymobi.Logic.HelperClasses.CacheHelper.CacheObject(cacheParams, DateTime.UtcNow.Ticks);

                result.ModelCollection.Add(new SimpleWebserviceResult(true));
            }
            catch (ObymobiException wex)
            {
                result.SetResult(wex);
                Exceptionlogger.CreateExceptionlog(wex);
            }
            catch (Exception ex)
            {
                result.SetResult(GenericWebserviceCallResult.Failure);
                Exceptionlogger.CreateExceptionlog(ex);

                Requestlogger.UpdateRequestLog(result);
            }

            Requestlogger.UpdateRequestLog(result);

            return result;
        }

        public ObyTypedResult<SimpleWebserviceResult> PublishMap(long timestamp, string macAddress, int mapId, string hash)
        {
            // Create the return value
            ObyTypedResult<SimpleWebserviceResult> result = new ObyTypedResult<SimpleWebserviceResult>();

            // Create a request log
            Requestlogger.CreateRequestlog("WebserviceHandler.PublishMap", timestamp, macAddress, mapId, hash);

            try
            {
                // Validate the request
                Obymobi.Logic.HelperClasses.WebserviceHelper.ValidateRequest(timestamp, macAddress, hash, this.OriginalAdditionalParameters);

                MapEntity entity = MapHelper.GetPrefetchedMapEntity(mapId);
                Map model = new MapEntityConverter().ConvertEntityToModel(entity);

                Obymobi.Logic.HelperClasses.CacheHelper.CacheParameters cacheParams = new Obymobi.Logic.HelperClasses.CacheHelper.CacheParameters("WebserviceHandler.Map", "0", "");
                cacheParams.Add(mapId);

                Obymobi.Logic.HelperClasses.CacheHelper.CacheObject(cacheParams, model);

                result.ModelCollection.Add(new SimpleWebserviceResult(true));
            }
            catch (ObymobiException wex)
            {
                result.SetResult(wex);
                Exceptionlogger.CreateExceptionlog(wex);
            }
            catch (Exception ex)
            {
                result.SetResult(GenericWebserviceCallResult.Failure);
                Exceptionlogger.CreateExceptionlog(ex);

                Requestlogger.UpdateRequestLog(result);
            }

            Requestlogger.UpdateRequestLog(result);

            return result;
        }

        public ObyTypedResult<ClientStatus> GetSetClientStatus(long timestamp, string macAddress, ClientStatus clientStatus, string hash)
        {
            ObyTypedResult<ClientStatus> result = new ObyTypedResult<ClientStatus>();

            // Create a request log
            Requestlogger.CreateRequestlog("WebserviceHandler.GetSetClientStatus", timestamp, macAddress, hash);

            try
            {
                // Validate the request
                DeviceEntity deviceEntity = Obymobi.Logic.HelperClasses.WebserviceHelper.ValidateRequest(timestamp, macAddress, hash, this.OriginalAdditionalParameters);
                ClientEntity clientEntity = deviceEntity.ClientEntitySingle;
                
                if (clientEntity != null)
                {
                    // Make result
                    ClientStatus status = new ClientStatus();
                    status.Database = true; // Of course the database is alive, otherwise the ValidateRequest above would have failed already
                    status.ShowBatteryLowDialog = false;
                    status.ClientOperationMode = clientEntity.OperationMode;

                    // Retrieve order statuses if it's requested for.
                    if (clientStatus.PendingOrderIdsOnEmenu != null && clientStatus.PendingOrderIdsOnEmenu.Length > 0)
                    {
                        PredicateExpression filterPendingOrdersForEmenu = new PredicateExpression();
                        filterPendingOrdersForEmenu.Add(OrderFields.OrderId == clientStatus.PendingOrderIdsOnEmenu);

                        IncludeFieldsList includeOrderFields = new IncludeFieldsList();
                        includeOrderFields.Add(OrderFields.Status);
                        includeOrderFields.Add(OrderFields.ErrorCode);

                        IncludeFieldsList includeOrderitemFields = new IncludeFieldsList();
                        includeOrderitemFields.Add(OrderitemFields.OrderitemId);

                        PrefetchPath path = new PrefetchPath(EntityType.OrderEntity);
                        IPrefetchPathElement orderitemPath = OrderEntityBase.PrefetchPathOrderitemCollection;
                        orderitemPath.ExcludedIncludedFields = includeOrderitemFields;
                        path.Add(orderitemPath);

                        IPrefetchPathElement childOrderPath = OrderEntityBase.PrefetchPathOrderCollection;
                        childOrderPath.ExcludedIncludedFields = includeOrderFields;

                        IPrefetchPathElement childOrderitemPath = OrderEntityBase.PrefetchPathOrderitemCollection;
                        childOrderitemPath.ExcludedIncludedFields = includeOrderitemFields;
                        childOrderPath.SubPath.Add(childOrderitemPath);

                        path.Add(childOrderPath);

                        OrderCollection orders = new OrderCollection();
                        orders.GetMulti(filterPendingOrdersForEmenu, 0, null, null, path, includeOrderFields, 0, 0);

                        List<int> processedOrderIds = new List<int>();
                        List<int> processedOrderitemIds = new List<int>();
                        List<int> failedOrderIds = new List<int>();
                        List<int> failedOrderitemIds = new List<int>();
                        // Loop through the master orders
                        foreach (OrderEntity order in orders)
                        {
                            if (order.OrderCollection.Count == 0) // No child orders
                            {
                                if (order.StatusAsEnum == OrderStatus.Processed && order.ErrorCodeAsEnum == OrderProcessingError.None)
                                {
                                    processedOrderIds.Add(order.OrderId);
                                }
                                else if (order.StatusAsEnum == OrderStatus.Unprocessable || (order.StatusAsEnum == OrderStatus.Processed && order.ErrorCodeAsEnum != OrderProcessingError.None))
                                {
                                    failedOrderIds.Add(order.OrderId);
                                }
                            }
                            else
                            {
                                // Loop through sub orders
                                foreach (OrderEntity subOrder in order.OrderCollection)
                                {
                                    if (subOrder.StatusAsEnum == OrderStatus.Processed && subOrder.ErrorCodeAsEnum == OrderProcessingError.None)
                                    {
                                        foreach (OrderitemEntity orderitem in subOrder.OrderitemCollection)
                                        {
                                            processedOrderitemIds.Add(orderitem.OrderitemId);
                                        }
                                    }
                                    else if (subOrder.StatusAsEnum == OrderStatus.Unprocessable || (subOrder.StatusAsEnum == OrderStatus.Processed && subOrder.ErrorCodeAsEnum != OrderProcessingError.None))
                                    {
                                        foreach (OrderitemEntity orderitem in subOrder.OrderitemCollection)
                                        {
                                            failedOrderitemIds.Add(orderitem.OrderitemId);
                                        }
                                    }
                                }

                                // Walk through the order items of the master order
                                foreach (OrderitemEntity orderitem in order.OrderitemCollection)
                                {
                                    if (order.StatusAsEnum == OrderStatus.Processed && order.ErrorCodeAsEnum == OrderProcessingError.None)
                                    {
                                        processedOrderitemIds.Add(orderitem.OrderitemId);
                                    }
                                    else if (order.StatusAsEnum == OrderStatus.Unprocessable || (order.StatusAsEnum == OrderStatus.Processed && order.ErrorCodeAsEnum != OrderProcessingError.None))
                                    {
                                        failedOrderitemIds.Add(orderitem.OrderitemId);
                                    }
                                }
                            }
                        }

                        status.ProcessedOrderIds = processedOrderIds.ToArray();
                        status.ProcessedOrderitemIds = processedOrderitemIds.ToArray();
                        status.FailedOrderIds = failedOrderIds.ToArray();
                        status.FailedOrderitemIds = failedOrderitemIds.ToArray();
                    }

                    // --------------------------------------------------
                    // Update client data
                    clientEntity.LastOperationMode = clientStatus.ClientOperationMode;
                    if (clientStatus.DeliverypointId > 0)
                    {
                        clientEntity.DeliverypointId = clientStatus.DeliverypointId;
                    }

                    // Handle a supplied log
                    if (!clientStatus.Log.IsNullOrWhiteSpace())
                    {
                        ClientLogEntity logEntity = new ClientLogEntity();
                        logEntity.ClientId = clientEntity.ClientId;
                        logEntity.TypeEnum = ClientLogType.ReceivedLog;
                        logEntity.Message = clientStatus.Log;
                        logEntity.Save();
                    }

                    DateTime utcNow = DateTime.UtcNow;

                    clientEntity.LastWifiSsid = clientStatus.WifiSsid;
                    
                    if (clientEntity.RoomControlConnected != clientStatus.RoomControlConnected)
                    {
                        ClientLogEntity logEntity = new ClientLogEntity();
                        logEntity.ClientId = clientEntity.ClientId;
                        logEntity.TypeEnum = clientStatus.RoomControlConnected ? ClientLogType.RoomControlConnected : ClientLogType.RoomControlDisconnected;
                        logEntity.Save();
                    }
                    clientEntity.RoomControlConnected = clientStatus.RoomControlConnected;
                    clientEntity.DoNotDisturbActive = clientStatus.DoNotDisturbActive;
                    clientEntity.ServiceRoomActive = clientStatus.ServiceRoomActive;
                    clientEntity.Save();

                    deviceEntity.LastRequestUTC = utcNow;
                    deviceEntity.ApplicationVersion = clientStatus.ApplicationVersion;
                    deviceEntity.OsVersion = clientStatus.OsVersion;
                    deviceEntity.PrivateIpAddresses = clientStatus.IpAddress;
                    deviceEntity.AgentRunning = clientStatus.AgentIsRunning;
                    deviceEntity.SupportToolsRunning = clientStatus.SupportToolsIsRunning;
                    deviceEntity.BatteryLevel = clientStatus.BatteryLevel;
                    deviceEntity.WifiStrength = clientStatus.WifiStrength;
                    deviceEntity.IsDevelopment = clientStatus.IsDevelopmentDevice;
                    deviceEntity.CloudEnvironment = string.Format("{0} ({1})", WebEnvironmentHelper.CloudEnvironment, TestUtil.MachineName);
                    deviceEntity.Save();

                    // --------------------------------------------------

                    // Add the client status to the result
                    result.ModelCollection.Add(status);
                }
            }
            catch (ObymobiException wex)
            {
                result.SetResult(wex);
                Exceptionlogger.CreateExceptionlog(wex);
            }
            catch (Exception ex)
            {
                result.SetResult(GenericWebserviceCallResult.Failure);
                Exceptionlogger.CreateExceptionlog(ex);
            }

            Requestlogger.UpdateRequestLog(result);

            return result;
        }

        public ObyTypedResult<TerminalStatus> GetSetTerminalStatus(long timestamp, string macAddress, TerminalStatus terminalStatus, string hash)
        {
            ObyTypedResult<TerminalStatus> result = new ObyTypedResult<TerminalStatus>();

            // Create a request log
            Requestlogger.CreateRequestlog("WebserviceHandler.GetSetTerminalStatus", timestamp, macAddress, hash);

            try
            {
                // Validate the request
                DeviceEntity device = Obymobi.Logic.HelperClasses.WebserviceHelper.ValidateRequest(timestamp, macAddress, hash, this.OriginalAdditionalParameters);
                TerminalEntity terminal = device.TerminalEntitySingle;

                if (terminal != null)
                {
                    // Update TerminalStatus
                    terminal.DeviceEntity.BatteryLevel = terminalStatus.BatteryLevel;
                    terminal.DeviceEntity.IsCharging = terminalStatus.IsCharging;
                    terminal.DeviceEntity.PrivateIpAddresses = terminalStatus.PrivateIpAddresses;
                    terminal.DeviceEntity.PublicIpAddress = terminalStatus.PublicIpAddress;
                    terminal.DeviceEntity.CloudEnvironment = string.Format("{0} ({1})", WebEnvironmentHelper.CloudEnvironment, TestUtil.MachineName);
                    terminal.LastStatus = (int)TerminalStatusCode.OK;
                    //terminal.LastStatusText = terminal.LastStatus
                    terminal.DeviceEntity.LastRequestUTC = DateTime.UtcNow;
                    terminal.DeviceEntity.WifiStrength = terminalStatus.WifiStrength;
                    terminal.DeviceEntity.AgentRunning = terminalStatus.AgentIsRunning;
                    terminal.DeviceEntity.SupportToolsRunning = terminalStatus.SupportToolsIsRunning;
                    terminal.PrinterConnected = terminalStatus.PrinterConnected;
                    terminal.Save();
                    terminal.DeviceEntity.Save();

                    // GK For now we don't really communicate stuff back, but might need in the future, therefore it's returned as it came now.
                    if (terminal.PmsTerminalOfflineNotificationEnabled)
                    {
                        TerminalEntity pmsTerminal = TerminalHelper.GetPmsTerminal(terminal.CompanyId);
                        if (pmsTerminal != null)
                        {
                            terminalStatus.PmsActive = true;
                            terminalStatus.LastPmsSync = pmsTerminal.LastSyncUTC.HasValue ? Obymobi.Logic.HelperClasses.TerminalHelper.GetLastSyncFromTerminal(pmsTerminal) : string.Empty;
                            terminalStatus.PmsTerminalOnline = pmsTerminal.DeviceEntity.LastRequestUTC.HasValue && pmsTerminal.DeviceEntity.LastRequestUTC.Value > DateTime.UtcNow.AddMilliseconds(ObymobiIntervals.DEVICE_OFFLINE_THRESHOLD * -1);
                        }
                    }

                    // Add the client status to the result
                    result.ModelCollection.Add(terminalStatus);
                }
            }
            catch (ObymobiException wex)
            {
                result.SetResult(wex);
                Exceptionlogger.CreateExceptionlog(wex);
            }
            catch (Exception ex)
            {
                result.SetResult(GenericWebserviceCallResult.Failure);
                Exceptionlogger.CreateExceptionlog(ex);
            }

            Requestlogger.UpdateRequestLog(result);

            return result;
        }

        public ObyTypedResult<SimpleWebserviceResult> SetClientStatuses(long timestamp, string macAddress, ClientStatus[] clientStatuses, string hash)
        {
            // Create the return value
            ObyTypedResult<SimpleWebserviceResult> result = new ObyTypedResult<SimpleWebserviceResult>();

            // Create a request log
            Requestlogger.CreateRequestlog("WebserviceHandler.SetClientStatuses", timestamp, macAddress, clientStatuses, hash);

            try
            {
                // Validate the request
                Obymobi.Logic.HelperClasses.WebserviceHelper.ValidateRequest(timestamp, macAddress, hash, this.OriginalAdditionalParameters);

                foreach (ClientStatus clientStatus in clientStatuses)
                {
                    // Get the client entity
                    ClientEntity clientEntity = new ClientEntity(clientStatus.ClientStatusId); // TODO ClientEntity initialize with ClientStatusId!?!?

                    // Update the last request of the client
                    if (!clientEntity.IsNew)
                    {
                        clientEntity.OperationMode = clientStatus.ClientOperationMode;

                        if (!clientEntity.DeliverypointId.HasValue || clientEntity.DeliverypointEntity.Number != clientStatus.DeliverypointNumber)
                        {
                            PredicateExpression deliverypointFilter = new PredicateExpression();
                            deliverypointFilter.Add(DeliverypointFields.CompanyId == clientEntity.CompanyId);
                            deliverypointFilter.Add(DeliverypointFields.Number == clientStatus.DeliverypointNumber);
                            DeliverypointCollection deliverypoints = new DeliverypointCollection();
                            deliverypoints.GetMulti(deliverypointFilter);

                            if (deliverypoints.Count == 1)
                            {
                                clientEntity.DeliverypointId = deliverypoints[0].DeliverypointId;
                            }
                        }

                        // Handle a supplied log
                        if (!clientStatus.Log.IsNullOrWhiteSpace())
                        {
                            ClientLogEntity logEntity = new ClientLogEntity();
                            logEntity.ClientId = clientEntity.ClientId;
                            logEntity.TypeEnum = ClientLogType.ReceivedLog;
                            logEntity.Message = clientStatus.Log;
                            logEntity.Save();
                        }

                        clientEntity.Save();

                        clientEntity.DeviceEntity.BatteryLevel = clientStatus.BatteryLevel;
                        clientEntity.DeviceEntity.ApplicationVersion = clientStatus.ApplicationVersion;
                        clientEntity.DeviceEntity.PrivateIpAddresses = clientStatus.IpAddress;
                        clientEntity.DeviceEntity.Save();
                    }
                }

                // Add the success flag to the result
                result.ModelCollection.Add(new SimpleWebserviceResult(true));
            }
            catch (ObymobiException wex)
            {
                result.SetResult(wex);
                Exceptionlogger.CreateExceptionlog(wex);
            }
            catch (Exception ex)
            {
                result.SetResult(GenericWebserviceCallResult.Failure);
                Exceptionlogger.CreateExceptionlog(ex);
            }

            Requestlogger.UpdateRequestLog(result);

            return result;
        }

        public ObyTypedResult<Order> SaveOrder(long timestamp, string macAddress, Order order, string hash)
        {
            // Create the return value
            ObyTypedResult<Order> result = new ObyTypedResult<Order>();

            // Create a request log
            Requestlogger.CreateRequestlog("WebserviceHandler.SaveOrder", timestamp, macAddress, order, hash);

            try
            {
                // Validate the credentials
                DeviceEntity device = Obymobi.Logic.HelperClasses.WebserviceHelper.ValidateRequest(timestamp, macAddress, hash, this.OriginalAdditionalParameters);

                // Old logic was - question is, what to 
                // ClientEntity clientEntity = ClientHelper.GetClientEntityByClientId(clientId);
                // Get the client
                ClientEntity clientEntity;
                if (device.ClientEntitySingle != null)
                {
                    clientEntity = device.ClientEntitySingle;
                }
                else
                {
                    clientEntity = null;
                }

                for (int i = 0; i < order.Orderitems.Length; i++)
                {
                    Orderitem orderitem = order.Orderitems[i];
                    for (int j = 0; j < orderitem.Alterationitems.Length; j++)
                    {
                        if (orderitem.Alterationitems[j].AlterationType == 0)
                        {
                            orderitem.Alterationitems[j].AlterationType = 1;
                        }
                    }
                }

                // Because of Special Character Stripping we must be Base64 the AnalyticsPayload - Decode here as this seems to be the earliest moment
                try
                {
                    // Padding Base64: http://stackoverflow.com/a/25754723
                    if (!order.AnalyticsPayLoad.IsNullOrWhiteSpace())
                    {
                        order.AnalyticsPayLoad = order.AnalyticsPayLoad.Replace("\n", string.Empty);
                        if (order.AnalyticsPayLoad.Length % 4 != 0)
                            order.AnalyticsPayLoad += new string('=', 4 - order.AnalyticsPayLoad.Length % 4);
                        order.AnalyticsPayLoad = Encoding.UTF8.GetString(Convert.FromBase64String(order.AnalyticsPayLoad));
                    }

                    if (!order.AnalyticsTrackingIds.IsNullOrWhiteSpace())
                    {
                        order.AnalyticsTrackingIds = order.AnalyticsTrackingIds.Replace("\n", string.Empty);
                        if (order.AnalyticsTrackingIds.Length % 4 != 0)
                            order.AnalyticsTrackingIds += new string('=', 4 - order.AnalyticsTrackingIds.Length % 4);
                        order.AnalyticsTrackingIds = Encoding.UTF8.GetString(Convert.FromBase64String(order.AnalyticsTrackingIds));
                    }
                    else
                    {
                        // Find Id's server side.
                        List<string> googlePropertyIds = new List<string>();
                        googlePropertyIds.Add(Obymobi.Analytics.Google.AccountConfiguration.GetByCloudEnvironment(WebEnvironmentHelper.CloudEnvironment).PropertyId);

                        string companyAnalyticsId = CompanyHelper.GetGoogleAnalyticsPropertyId(order.CompanyId);
                        if (!companyAnalyticsId.IsNullOrWhiteSpace())
                            googlePropertyIds.Add(companyAnalyticsId);
                        order.AnalyticsTrackingIds = String.Join(";", googlePropertyIds.ToArray());
                    }
                }
                catch
                {
                    order.AnalyticsPayLoad = string.Empty;
                    order.AnalyticsTrackingIds = string.Empty;
                    // No need to crash on live
                    if (TestUtil.IsPcDeveloper)
                        throw;
                }                                

                bool isOnSiteServer = (order.AutoId > 0);

                Obymobi.Logic.HelperClasses.OrderProcessingHelper.CreateNewOrder(null, clientEntity, order, ref result, isOnSiteServer);
            }
            catch (ObymobiException wex)
            {
                result.SetResult(wex);
                Exceptionlogger.CreateExceptionlog(wex);
            }
            catch (Exception ex)
            {
                result.SetResult(GenericWebserviceCallResult.Failure);
                Exceptionlogger.CreateExceptionlog(ex);
            }
            
            try
            {
                CometHelper.PushExternalOrderPlacedEvent(order.CompanyId, order);
            }
            catch (Exception)
            {
                // ignored
            }

            Requestlogger.UpdateRequestLog(result);

            return result;
        }

        public ObyTypedResult<Posorder> CreatePosorder(long timestamp, string macAddress, Order order, string hash)
        {
            // Create the return value
            ObyTypedResult<Posorder> result = new ObyTypedResult<Posorder>();

            // Create a request log
            Requestlogger.CreateRequestlog("WebserviceHandler.CreatePosorder", timestamp, macAddress, order, hash);

            try
            {
                // Validate the credentials
                Obymobi.Logic.HelperClasses.WebserviceHelper.ValidateRequestRequireTerminal(timestamp, macAddress, hash, this.OriginalAdditionalParameters);

                // and create a pos order
                Posorder posorder = Obymobi.Logic.HelperClasses.PosorderHelper.CreatePosorderFromOrder(order);

                // Add the pos order to the result
                result.ModelCollection.Add(posorder);
            }
            catch (ObymobiException wex)
            {
                result.SetResult(wex);
                Exceptionlogger.CreateExceptionlog(wex);
            }
            catch (Exception ex)
            {
                result.SetResult(GenericWebserviceCallResult.Failure);
                Exceptionlogger.CreateExceptionlog(ex);
            }

            Requestlogger.UpdateRequestLog(result);

            return result;
        }

        public ObyTypedResult<SurveyResult> SaveSurveyResults(long timestamp, string macAddress, SurveyResult results, string hash)
        {
            // Create the return value
            ObyTypedResult<SurveyResult> result = new ObyTypedResult<SurveyResult>();

            // Create a request log
            Requestlogger.CreateRequestlog("WebserviceHandler.SaveSurveyResults", timestamp, macAddress, results, hash);

            try
            {
                // Validate the credentials
                DeviceEntity device = Obymobi.Logic.HelperClasses.WebserviceHelper.ValidateRequest(timestamp, macAddress, hash, this.OriginalAdditionalParameters);

                // Deprecated
            }
            catch (ObymobiException wex)
            {
                result.SetResult(wex);
                Exceptionlogger.CreateExceptionlog(wex);
            }
            catch (Exception ex)
            {
                result.SetResult(GenericWebserviceCallResult.Failure);
                Exceptionlogger.CreateExceptionlog(ex);
            }

            Requestlogger.UpdateRequestLog(result);

            return result;
        }

        public ObyTypedResult<Rating> SaveRatings(long timestamp, string macAddress, Rating[] ratings, string hash)
        {
            // Create the return value
            ObyTypedResult<Rating> result = new ObyTypedResult<Rating>();

            // Create a request log
            Requestlogger.CreateRequestlog("WebserviceHandler.SaveRatings", timestamp, macAddress, ratings, hash);

            try
            {
                // Validate the credentials
                Obymobi.Logic.HelperClasses.WebserviceHelper.ValidateRequest(timestamp, macAddress, hash, this.OriginalAdditionalParameters);

                // No
            }
            catch (ObymobiException wex)
            {
                result.SetResult(wex);
                Exceptionlogger.CreateExceptionlog(wex);
            }
            catch (Exception ex)
            {
                result.SetResult(GenericWebserviceCallResult.Failure);
                Exceptionlogger.CreateExceptionlog(ex);
            }

            Requestlogger.UpdateRequestLog(result);

            return result;
        }

        public ObyTypedResult<ClientEntertainment> SaveClientEntertainment(long timestamp, string macAddress, ClientEntertainment[] clientEntertainments, string hash)
        {
            // Create the return value
            ObyTypedResult<ClientEntertainment> result = new ObyTypedResult<ClientEntertainment>();

            // Create a request log
            Requestlogger.CreateRequestlog("WebserviceHandler.SaveClientEntertainment", timestamp, macAddress, clientEntertainments, hash);

            try
            {
                // Validate the credentials
                Obymobi.Logic.HelperClasses.WebserviceHelper.ValidateRequest(timestamp, macAddress, hash, this.OriginalAdditionalParameters);

                // Create the ratings using the specified xml
                ClientEntertainmentHelper.SaveClientEntertainment(clientEntertainments, ref result);
            }
            catch (ObymobiException wex)
            {
                result.SetResult(wex);
                Exceptionlogger.CreateExceptionlog(wex);
            }
            catch (Exception ex)
            {
                result.SetResult(GenericWebserviceCallResult.Failure);
                Exceptionlogger.CreateExceptionlog(ex);
            }

            Requestlogger.UpdateRequestLog(result);

            return result;
        }

        public ObyTypedResult<SimpleWebserviceResult> SaveClientLogZipped(long timestamp, string macAddress, int logType, string base64Zip, long fileDate, string hash)
        {
            // Create the return value
            ObyTypedResult<SimpleWebserviceResult> result = new ObyTypedResult<SimpleWebserviceResult>();

            // Create a request log (on purpose, base64Zip is not included, could be big!)
            Requestlogger.CreateRequestlog("WebserviceHandler.SaveClientLogZipped", timestamp, macAddress, logType, hash);

            try
            {
                // Validate the credentials                                
                ClientEntity client = Obymobi.Logic.HelperClasses.WebserviceHelper.ValidateRequestRequireClient(timestamp, macAddress, hash, this.OriginalAdditionalParameters);

                // Get log from base 64 zip
                string filename;
                string log = WebserviceHandler.base64ZipToString(base64Zip, out filename);

                // Create the client log
                ClientLogType logTypeEnum;
                if (!EnumUtil.TryParse(logType, out logTypeEnum))
                {
                    logTypeEnum = ClientLogType.Unknown;
                }

                ClientLogEntity clientLog = Obymobi.Logic.HelperClasses.ClientLogHelper.CreateClientLogEntity(client.ClientId, logTypeEnum, log, fileDate, filename);
                clientLog.Save();
            }
            catch (ObymobiException wex)
            {
                result.SetResult(wex);
                Exceptionlogger.CreateExceptionlog(wex);
            }
            catch (Exception ex)
            {
                result.SetResult(GenericWebserviceCallResult.Failure);
                Exceptionlogger.CreateExceptionlog(ex);
            }

            return result;
        }

        public ObyTypedResult<SimpleWebserviceResult> SaveClientLog(long timestamp, string macAddress, int logType, string log, long fileDate, string hash)
        {
            // Create the return value
            ObyTypedResult<SimpleWebserviceResult> result = new ObyTypedResult<SimpleWebserviceResult>();

            // Create a request log
            Requestlogger.CreateRequestlog("WebserviceHandler.SaveClientLog", timestamp, macAddress, logType, log, hash);

            try
            {
                // Validate the credentials
                ClientEntity client = Obymobi.Logic.HelperClasses.WebserviceHelper.ValidateRequestRequireClient(timestamp, macAddress, hash, this.OriginalAdditionalParameters);

                // Create the client log
                ClientLogType logTypeEnum;
                if (!EnumUtil.TryParse(logType, out logTypeEnum))
                {
                    logTypeEnum = ClientLogType.Unknown;
                }

                if (logTypeEnum == ClientLogType.ApplicationStarted)
                {
                    client.LoadedSuccessfully = true;
                    client.Save();
                }
                else if (logTypeEnum == ClientLogType.ApplicationStopped)
                {
                    client.LoadedSuccessfully = false;
                    client.Save();
                }

                ClientLogEntity clientLog = Obymobi.Logic.HelperClasses.ClientLogHelper.CreateClientLogEntity(client.ClientId, logTypeEnum, log, fileDate);
                clientLog.Save();
            }
            catch (ObymobiException wex)
            {
                result.SetResult(wex);
                Exceptionlogger.CreateExceptionlog(wex);
            }
            catch (Exception ex)
            {
                result.SetResult(GenericWebserviceCallResult.Failure);
                Exceptionlogger.CreateExceptionlog(ex);
            }

            return result;
        }

        public ObyTypedResult<Posproduct> SetPosProducts(long timestamp, string macAddress, long batchId, Posproduct[] posproducts, string hash)
        {
            // Create the return value
            ObyTypedResult<Posproduct> result = new ObyTypedResult<Posproduct>();

            // Create a request log
            Requestlogger.CreateRequestlog("WebserviceHandler.SetPosProducts", timestamp, macAddress, posproducts, hash);

            try
            {
                // Validate the credentials
                TerminalEntity terminal = Obymobi.Logic.HelperClasses.WebserviceHelper.ValidateRequestRequireTerminal(timestamp, macAddress, hash, this.OriginalAdditionalParameters);

                // Synchronize the POS products
                Obymobi.Logic.HelperClasses.PosproductHelper.SynchronizePosproducts(posproducts, terminal.CompanyId, batchId);
            }
            catch (ObymobiException wex)
            {
                result.SetResult(wex);
                Exceptionlogger.CreateExceptionlog(wex);
            }
            catch (Exception ex)
            {
                result.SetResult(GenericWebserviceCallResult.Failure);
                Exceptionlogger.CreateExceptionlog(ex);
            }

            Requestlogger.UpdateRequestLog(result);

            return result;
        }

        public ObyTypedResult<Poscategory> SetPosCategories(long timestamp, string macAddress, long batchId, Poscategory[] poscategories, string hash)
        {
            // Create the return value
            ObyTypedResult<Poscategory> result = new ObyTypedResult<Poscategory>();

            // Create a request log
            Requestlogger.CreateRequestlog("WebserviceHandler.SetPosCategories", timestamp, macAddress, poscategories, hash);

            try
            {
                // Validate the credentials
                TerminalEntity terminal = Obymobi.Logic.HelperClasses.WebserviceHelper.ValidateRequestRequireTerminal(timestamp, macAddress, hash, this.OriginalAdditionalParameters);

                // Synchronize the POS categories
                Obymobi.Logic.HelperClasses.PoscategoryHelper.SynchronizePoscategories(poscategories, terminal.CompanyId, batchId);
            }
            catch (ObymobiException wex)
            {
                result.SetResult(wex);
                Exceptionlogger.CreateExceptionlog(wex);
            }
            catch (Exception ex)
            {
                result.SetResult(GenericWebserviceCallResult.Failure);
                Exceptionlogger.CreateExceptionlog(ex);
            }

            Requestlogger.UpdateRequestLog(result);

            return result;
        }

        public ObyTypedResult<Posdeliverypoint> SetPosDeliverypoints(long timestamp, string macAddress, long batchId, Posdeliverypoint[] posdeliverypoints, string hash)
        {
            // Create the return value
            ObyTypedResult<Posdeliverypoint> result = new ObyTypedResult<Posdeliverypoint>();

            // Create a request log
            Requestlogger.CreateRequestlog("WebserviceHandler.SetPosDeliverypoints", timestamp, macAddress, batchId, posdeliverypoints, hash);

            try
            {
                // Validate the credentials
                TerminalEntity terminal = Obymobi.Logic.HelperClasses.WebserviceHelper.ValidateRequestRequireTerminal(timestamp, macAddress, hash, this.OriginalAdditionalParameters);

                // Synchronize the POS deliverypoints with the POS
                Obymobi.Logic.HelperClasses.PosdeliverypointHelper.ImportPosdeliverypoints(posdeliverypoints, terminal.CompanyId, batchId);
            }
            catch (ObymobiException wex)
            {
                result.SetResult(wex);
                Exceptionlogger.CreateExceptionlog(wex);
            }
            catch (Exception ex)
            {
                result.SetResult(GenericWebserviceCallResult.Failure);
                Exceptionlogger.CreateExceptionlog(ex);
            }

            Requestlogger.UpdateRequestLog(result);

            return result;
        }

        public ObyTypedResult<Posdeliverypointgroup> SetPosDeliverypointgroups(long timestamp, string macAddress, long batchId, Posdeliverypointgroup[] posdeliverypointgroups, string hash)
        {
            // Create the return value
            ObyTypedResult<Posdeliverypointgroup> result = new ObyTypedResult<Posdeliverypointgroup>();

            // Create a request log
            Requestlogger.CreateRequestlog("WebserviceHandler.SetPosDeliverypointgroups", timestamp, macAddress, batchId, posdeliverypointgroups, hash);

            try
            {
                // Validate the credentials
                TerminalEntity terminal = Obymobi.Logic.HelperClasses.WebserviceHelper.ValidateRequestRequireTerminal(timestamp, macAddress, hash, this.OriginalAdditionalParameters);

                // Synchronze the POS deliverypoint groups with the POS
                Obymobi.Logic.HelperClasses.PosdeliverypointgroupHelper.ImportPosdeliverypointgroups(posdeliverypointgroups, terminal.CompanyId, batchId);
            }
            catch (ObymobiException wex)
            {
                result.SetResult(wex);
                Exceptionlogger.CreateExceptionlog(wex);
            }
            catch (Exception ex)
            {
                result.SetResult(GenericWebserviceCallResult.Failure);
                Exceptionlogger.CreateExceptionlog(ex);
            }

            Requestlogger.UpdateRequestLog(result);

            return result;
        }

        public ObyTypedResult<OrderRoutestephandlerSaveStatus> SetRoutestephandlerSaveStatuses(long timestamp, string macAddress, OrderRoutestephandlerSaveStatus[] orderSaveStatuses, string hash)
        {
            // Create the return value
            ObyTypedResult<OrderRoutestephandlerSaveStatus> result = new ObyTypedResult<OrderRoutestephandlerSaveStatus>();

            // Create a request log
            Requestlogger.CreateRequestlog("WebserviceHandler.SetRoutestephandlerSaveStatuses", timestamp, macAddress, orderSaveStatuses, hash);

            try
            {
                // Validate the credentials
                TerminalEntity terminal = Obymobi.Logic.HelperClasses.WebserviceHelper.ValidateRequestRequireTerminal(timestamp, macAddress, hash, this.OriginalAdditionalParameters);

                // Save the Order Statuses
                RoutingHelper.UpdateOrderroutestephandlers(orderSaveStatuses.ToList(), terminal);

                // Check for any expired orders
                RoutingHelper.FailExpiredRoutestephandlers(terminal.TerminalId, null);

                // First fetch the orders of which we want to sent the order status via PokeIn
                PredicateExpression filterOrders = new PredicateExpression();
                foreach (OrderRoutestephandlerSaveStatus orderSaveStatus in orderSaveStatuses)
                {
                    if (orderSaveStatus.OrderId <= 0 && orderSaveStatus.OrderGuid == null)
                    {
                        throw new ObymobiException(WebserviceHandlerResult.OrderIdRequiredWithOrderRoutestephandlerSaveStatus, "It's required to have a OrderId or Guid to update OrderSaveStatuses");
                    }

                    if (orderSaveStatus.OrderId > 0)
                    {
                        filterOrders.AddWithOr(OrderFields.OrderId == orderSaveStatus.OrderId);
                    }
                    else
                    {
                        filterOrders.AddWithOr(OrderFields.Guid == orderSaveStatus.OrderGuid);
                    }
                }
            }
            catch (ObymobiException wex)
            {
                result.SetResult(wex);
                Exceptionlogger.CreateExceptionlog(wex);
            }
            catch (Exception ex)
            {
                result.SetResult(GenericWebserviceCallResult.Failure);
                Exceptionlogger.CreateExceptionlog(ex);
            }

            Requestlogger.UpdateRequestLog(result);

            return result;
        }

        public ObyTypedResult<TerminalOperationMode> SetTerminalOperationMode(long timestamp, string macAddress, int terminalOperationMode, string hash)
        {
            // Create the return value
            ObyTypedResult<TerminalOperationMode> result = new ObyTypedResult<TerminalOperationMode>();

            // Create a request log
            Requestlogger.CreateRequestlog("WebserviceHandler.SetTerminalOperationMode", timestamp, macAddress, terminalOperationMode, hash);

            try
            {
                // Validate the credentials
                TerminalEntity terminal = Obymobi.Logic.HelperClasses.WebserviceHelper.ValidateRequestRequireTerminal(timestamp, macAddress, hash, this.OriginalAdditionalParameters);

                // Update the operation mode
                TerminalHelper.UpdateTerminalOperationMode(terminal.TerminalId, terminalOperationMode.ToEnum<TerminalOperationMode>());
            }
            catch (ObymobiException wex)
            {
                result.SetResult(wex);
                Exceptionlogger.CreateExceptionlog(wex);
            }
            catch (Exception ex)
            {
                result.SetResult(GenericWebserviceCallResult.Failure);
                Exceptionlogger.CreateExceptionlog(ex);
            }

            Requestlogger.UpdateRequestLog(result);

            return result;
        }

        public ObyTypedResult<TerminalOperationState> SetTerminalOperationState(long timestamp, string macAddress, int terminalOperationState, string hash)
        {
            // Create the return value
            ObyTypedResult<TerminalOperationState> result = new ObyTypedResult<TerminalOperationState>();

            // Create a request log
            Requestlogger.CreateRequestlog("WebserviceHandler.SetTerminalOperationState", timestamp, macAddress, terminalOperationState, hash);

            try
            {
                // Validate the credentials
                TerminalEntity terminal = Obymobi.Logic.HelperClasses.WebserviceHelper.ValidateRequestRequireTerminal(timestamp, macAddress, hash, this.OriginalAdditionalParameters);

                // Update the operation mode
                Obymobi.Logic.HelperClasses.TerminalHelper.UpdateTerminalOperationState(terminal.TerminalId, terminalOperationState.ToEnum<TerminalOperationState>());
            }
            catch (ObymobiException wex)
            {
                result.SetResult(wex);
                Exceptionlogger.CreateExceptionlog(wex);
            }
            catch (Exception ex)
            {
                result.SetResult(GenericWebserviceCallResult.Failure);
                Exceptionlogger.CreateExceptionlog(ex);
            }

            Requestlogger.UpdateRequestLog(result);

            return result;
        }

        public ObyTypedResult<SimpleWebserviceResult> SetClientDeliverypoint(long timestamp, string macAddress, int deliverypointId, int deliverypointgroupId, string hash)
        {
            // Create the return value
            ObyTypedResult<SimpleWebserviceResult> result = new ObyTypedResult<SimpleWebserviceResult>();

            // Create a request log
            Requestlogger.CreateRequestlog("WebserviceHandler.SetClientDeliverypoint", timestamp, macAddress, deliverypointId, hash);

            try
            {
                // Validate the company credentials
                ClientEntity client = Obymobi.Logic.HelperClasses.WebserviceHelper.ValidateRequestRequireClient(timestamp, macAddress, hash, this.OriginalAdditionalParameters);

                // Get the client                
                if (client != null)
                {
                    // Set the deliverypoint ID
                    if (deliverypointId <= 0)
                    {
                        client.DeliverypointId = null;

                        if (deliverypointgroupId > 0)
                        {
                            client.DeliverypointGroupId = deliverypointgroupId;
                        }
                    }
                    else
                    {
                        // Always check if it's also the correct DeliverypointGroup, if not, update the DeliveyrpointGroup
                        DeliverypointEntity dp = new DeliverypointEntity(deliverypointId);
                        if (dp.IsNew)
                        {
                            throw new ObymobiException(GenericWebserviceCallResult.EntityNotFoundByPrimaryKey, "DeliverypointId: {0}", deliverypointId);
                        }
                        
                        if (client.DeliverypointGroupId != dp.DeliverypointgroupId)
                        {
                            client.DeliverypointGroupId = dp.DeliverypointgroupId;
                        }

                        client.DeliverypointId = deliverypointId;
                    }

                    client.Save();
                }

                // Add the result
                result.ModelCollection.Add(new SimpleWebserviceResult(true));
            }
            catch (ObymobiException wex)
            {
                result.SetResult(wex);
                Exceptionlogger.CreateExceptionlog(wex);
            }
            catch (Exception ex)
            {
                result.SetResult(GenericWebserviceCallResult.Failure);
                Exceptionlogger.CreateExceptionlog(ex);
            }

            Requestlogger.UpdateRequestLog(result);

            return result;
        }

        public ObyTypedResult<SimpleWebserviceResult> SendMessage(long timestamp, string macAddress, string deliverypointNumbers, string deliverypointgroupIds, int clientId, int orderId, string title, string message, int duration, int mediaId, int categoryId, int entertainmentId, int productId, bool urgent, bool saveMessage, bool mobileClients, string messagegroupIds, int messageLayoutType, Obymobi.Logic.Model.Action[] actions, string hash)
        {
            // Create the return value
            ObyTypedResult<SimpleWebserviceResult> result = new ObyTypedResult<SimpleWebserviceResult>();

            // Create a request log
            Requestlogger.CreateRequestlog("WebserviceHandler.SendMessage", timestamp, macAddress, deliverypointNumbers, deliverypointgroupIds,
                clientId, orderId, title, message, duration, mediaId, categoryId, entertainmentId, productId, urgent, saveMessage, messagegroupIds, messageLayoutType, actions, hash);

            try
            {
                TerminalEntity terminal = Obymobi.Logic.HelperClasses.WebserviceHelper.ValidateRequestRequireTerminal(timestamp, macAddress, hash, this.OriginalAdditionalParameters);

                if (saveMessage)
                {
                    if (mobileClients)
                    {                        
                        string mediaCdnFileName = Obymobi.Logic.HelperClasses.MediaHelper.GetMediaCdnFileName(mediaId);
                        string accessCodes = MessageHelper.GetAccessCodesAsStringForMobileClients(terminal.CompanyId);
                        if (!accessCodes.IsNullOrWhiteSpace())
                        {
                            AzureNotificationProvider.AzureNotificationContainer container = new AzureNotificationProvider.AzureNotificationContainer(accessCodes, title, message)
                                    {
                                        {"mediaOnCdn", mediaCdnFileName},
                                        {"duration", duration},
                                        {"urgent", urgent},
                                        {"companyId", terminal.CompanyId },
                                        {"categoryId", categoryId},
                                        {"productId", productId},
                                    };
                            AzureNotificationProvider.SendNotification(container);
                        }
                    }
                    else
                    {
                        MessageEntity messageEntity = MessageHelper.CreateMessageEntity(orderId, terminal.CompanyId, title, message, duration, mediaId, categoryId, entertainmentId, productId, urgent, messageLayoutType);

                        string recipientsAsString = string.Empty;
                        if (clientId > 0 && deliverypointNumbers.Length == 0)
                        {
                            messageEntity.RecipientType = RecipientType.Client;
                            messageEntity.MessageRecipientCollection.Add(Obymobi.Logic.HelperClasses.MessageHelper.CreateMessageRecipientEntity(0, clientId, 0, null));
                            recipientsAsString = string.Join(",", messageEntity.MessageRecipientCollection.Select(x => x.ClientId.ToString()));
                        }
                        else if (messagegroupIds.Length > 0)
                        {
                            messageEntity.RecipientType = RecipientType.Deliverypoint;
                            messageEntity.MessageRecipientCollection.AddRange(MessageHelper.CreateMessageRecipientsForMessagegroups(messagegroupIds, clientId, terminal.CompanyId, actions.ToList()));
                            recipientsAsString = string.Join(",", messageEntity.MessageRecipientCollection.Select(x => x.DeliverypointId.ToString()));
                        }
                        else
                        {
                            messageEntity.RecipientType = RecipientType.Deliverypoint;
                            messageEntity.MessageRecipientCollection.AddRange(MessageHelper.CreateMessageRecipientsForDeliverypoints(deliverypointNumbers, deliverypointgroupIds, clientId, terminal.CompanyId, actions.ToList()));
                            recipientsAsString = string.Join(",", messageEntity.MessageRecipientCollection.Select(x => x.DeliverypointId.ToString()));
                        }

                        if (messageEntity.MessageRecipientCollection.Count > 0)
                        {
                            messageEntity.RecipientsCount = messageEntity.MessageRecipientCollection.Count;
                            messageEntity.RecipientsAsString = recipientsAsString;
                            messageEntity.Save(true);
                        }
                    }
                }                
                
                result.ModelCollection.Add(new SimpleWebserviceResult(true));
            }
            catch (Obymobi.ObymobiException wex)
            {
                result.SetResult(wex);
                Exceptionlogger.CreateExceptionlog(wex);
            }
            catch (Exception ex)
            {
                result.SetResult(GenericWebserviceCallResult.Failure);
                Exceptionlogger.CreateExceptionlog(ex);
            }

            Requestlogger.UpdateRequestLog(result);

            return result;
        }

        private MessageEntity GetMessageEntity(int customerId, int clientId, int deliverypointId, int orderId, int companyId, string title, string message, int duration, int mediaId, int categoryId, int entertainmentId, int productId, bool urgent, int messageLayoutType)
        {
            MessageEntity entity = new MessageEntity();
            entity.CompanyId = companyId;
            
            
            
            return entity;
        }


        public ObyTypedResult<SimpleWebserviceResult> SetPmsFolio(long timestamp, string macAddress, Folio folio, string hash)
        {
            // Create the return value
            ObyTypedResult<SimpleWebserviceResult> result = new ObyTypedResult<SimpleWebserviceResult>();

            // Create a request log
            Requestlogger.CreateRequestlog("WebserviceHandler.SetPmsFolio", timestamp, macAddress, folio, hash);

            try
            {
                // Validate the company credentials
                TerminalEntity terminal = Obymobi.Logic.HelperClasses.WebserviceHelper.ValidateRequestRequireTerminal(timestamp, macAddress, hash, this.OriginalAdditionalParameters);
                if (StringUtil.IsNullOrWhiteSpace(folio.DeliverypointNumber))
                {
                    result.SetResult(SetPmsCheckedInResult.DeliverypointNumberMissing);
                }
                else
                {
                    DeliverypointEntity deliverypoint = Obymobi.Logic.HelperClasses.DeliverypointHelper.GetDeliverypoint(terminal.CompanyId, folio.DeliverypointNumber, false, true, terminal.DeliverypointgroupId);
                    if (deliverypoint == null || deliverypoint.IsNew)
                    {
                        result.SetResult(SetPmsCheckedInResult.DeliverypointNumberUnknown);
                    }
                    else
                    {
                        // Forward folio to Deliverypoint (means to Clients subscribed to this Deliverypoint)   
                        JsonSerializerSettings settings = new JsonSerializerSettings();
                        settings.NullValueHandling = NullValueHandling.Ignore;
                        settings.ContractResolver = new CamelCasePropertyNamesContractResolver();
                        string json = JsonConvert.SerializeObject(folio, new Formatting(), settings);

                        CometHelper.SetPmsFolio(deliverypoint.DeliverypointId, json);

                        // Add the result
                        result.ModelCollection.Add(new SimpleWebserviceResult(true));
                    }
                }
            }
            catch (ObymobiException wex)
            {
                result.SetResult(wex);
                Exceptionlogger.CreateExceptionlog(wex);
            }
            catch (Exception ex)
            {
                result.SetResult(GenericWebserviceCallResult.Failure, ex);
                Exceptionlogger.CreateExceptionlog(ex);
            }

            Requestlogger.UpdateRequestLog(result);

            return result;
        }

        public ObyTypedResult<SimpleWebserviceResult> SetPmsGuestInformation(long timestamp, string macAddress, GuestInformation guestInformation, string hash)
        {
            // Create the return value
            ObyTypedResult<SimpleWebserviceResult> result = new ObyTypedResult<SimpleWebserviceResult>();

            // Create a request log
            Requestlogger.CreateRequestlog("WebserviceHandler.SetPmsGuestInformation", timestamp, macAddress, guestInformation, hash);

            try
            {
                // Validate the company credentials
                TerminalEntity terminal = Obymobi.Logic.HelperClasses.WebserviceHelper.ValidateRequestRequireTerminal(timestamp, macAddress, hash, this.OriginalAdditionalParameters);
                if (StringUtil.IsNullOrWhiteSpace(guestInformation.DeliverypointNumber))
                {
                    result.SetResult(SetPmsCheckedInResult.DeliverypointNumberMissing);
                }
                else
                {
                    DeliverypointEntity deliverypoint = Obymobi.Logic.HelperClasses.DeliverypointHelper.GetDeliverypoint(terminal.CompanyId, guestInformation.DeliverypointNumber, false, true, terminal.DeliverypointgroupId);
                    if (deliverypoint == null || deliverypoint.IsNew)
                    {
                        result.SetResult(SetPmsCheckedInResult.DeliverypointNumberUnknown);
                    }
                    else
                    {
                        // Forward guestinformation to Deliverypoint (means to Clients subscribed to this Deliverypoint)    
                        JsonSerializerSettings settings = new JsonSerializerSettings();
                        settings.NullValueHandling = NullValueHandling.Ignore;
                        settings.ContractResolver = new CamelCasePropertyNamesContractResolver();
                        string json = JsonConvert.SerializeObject(guestInformation, new Formatting(), settings);

                        CometHelper.SetPmsGuestInformation(deliverypoint.DeliverypointId, json);

                        // Add the result
                        result.ModelCollection.Add(new SimpleWebserviceResult(true));
                    }
                }
            }
            catch (ObymobiException wex)
            {
                result.SetResult(wex);
                Exceptionlogger.CreateExceptionlog(wex);
            }
            catch (Exception ex)
            {
                result.SetResult(GenericWebserviceCallResult.Failure, ex);
                Exceptionlogger.CreateExceptionlog(ex);
            }

            Requestlogger.UpdateRequestLog(result);

            return result;
        }

        public ObyTypedResult<SimpleWebserviceResult> SetPmsCheckedOut(long timestamp, string macAddress, int deliverypointId, string deliverypointNumber, string hash)
        {
            // Create the return value
            ObyTypedResult<SimpleWebserviceResult> result = new ObyTypedResult<SimpleWebserviceResult>();

            // Create a request log
            Requestlogger.CreateRequestlog("WebserviceHandler.SetPmsCheckedOut", timestamp, macAddress, deliverypointId, deliverypointNumber, hash);

            try
            {
                // Validate the company credentials
                TerminalEntity terminal = Obymobi.Logic.HelperClasses.WebserviceHelper.ValidateRequestRequireTerminal(timestamp, macAddress, hash, this.OriginalAdditionalParameters);
                DeliverypointEntity deliverypoint = null;
                if (deliverypointId > 0)
                {
                    deliverypoint = new DeliverypointEntity(deliverypointId);
                }
                else if (!deliverypointNumber.IsNullOrWhiteSpace())
                {
                    deliverypoint = Obymobi.Logic.HelperClasses.DeliverypointHelper.GetDeliverypoint(terminal.CompanyId, deliverypointNumber, false, true, terminal.DeliverypointgroupId);
                }

                if (deliverypoint == null || deliverypoint.IsNew)
                {
                    result.SetResult(SetPmsCheckedInResult.DeliverypointNumberUnknown);
                }
                else
                {
                    // Forward folio to Deliverypoint (means to Clients subscribed to this Deliverypoint)                
                    CometHelper.SetPmsCheckedOut(deliverypoint.DeliverypointId);

                    // Add the result
                    result.ModelCollection.Add(new SimpleWebserviceResult(true));
                }
            }
            catch (ObymobiException wex)
            {
                result.SetResult(wex);
                //Exceptionlogger.CreateExceptionlog(wex);
            }
            catch (Exception ex)
            {
                result.SetResult(GenericWebserviceCallResult.Failure, ex);
                Exceptionlogger.CreateExceptionlog(ex);
            }

            Requestlogger.UpdateRequestLog(result);

            return result;
        }

        public enum SetPmsCheckedInResult
        {
            DeliverypointNumberMissing = 201,
            DeliverypointNumberUnknown = 202
        }

        public ObyTypedResult<SimpleWebserviceResult> SetPmsCheckedIn(long timestamp, string macAddress, GuestInformation guestInformation, string hash)
        {
            // Create the return value
            ObyTypedResult<SimpleWebserviceResult> result = new ObyTypedResult<SimpleWebserviceResult>();

            // Create a request log
            Requestlogger.CreateRequestlog("WebserviceHandler.SetPmsCheckedIn", timestamp, macAddress, guestInformation, hash);

            try
            {
                // Validate the company credentials
                TerminalEntity terminal = Obymobi.Logic.HelperClasses.WebserviceHelper.ValidateRequestRequireTerminal(timestamp, macAddress, hash, this.OriginalAdditionalParameters);
                if (StringUtil.IsNullOrWhiteSpace(guestInformation.DeliverypointNumber))
                {
                    result.SetResult(SetPmsCheckedInResult.DeliverypointNumberMissing);
                }
                else
                {
                    DeliverypointEntity deliverypoint = Obymobi.Logic.HelperClasses.DeliverypointHelper.GetDeliverypoint(terminal.CompanyId, guestInformation.DeliverypointNumber, false, true, terminal.DeliverypointgroupId);
                    if (deliverypoint == null || deliverypoint.IsNew)
                    {
                        result.SetResult(SetPmsCheckedInResult.DeliverypointNumberUnknown);
                    }
                    else
                    {
                        // Forward guestinformation to Deliverypoint (means to Clients subscribed to this Deliverypoint)    
                        JsonSerializerSettings settings = new JsonSerializerSettings();
                        settings.NullValueHandling = NullValueHandling.Ignore;
                        settings.ContractResolver = new CamelCasePropertyNamesContractResolver();
                        string json = JsonConvert.SerializeObject(guestInformation, new Formatting(), settings);

                        CometHelper.SetPmsCheckedIn(deliverypoint.DeliverypointId, json);

                        // Add the result
                        result.ModelCollection.Add(new SimpleWebserviceResult(true));
                    }
                }
            }
            catch (ObymobiException wex)
            {
                result.SetResult(wex);
                //Exceptionlogger.CreateExceptionlog(wex);
            }
            catch (Exception ex)
            {
                result.SetResult(GenericWebserviceCallResult.Failure, ex);
                Exceptionlogger.CreateExceptionlog(ex);
            }

            Requestlogger.UpdateRequestLog(result);

            return result;
        }

        public ObyTypedResult<SimpleWebserviceResult> SetPmsExpressCheckedOut(long timestamp, string macAddress, Checkout checkoutInfo, string hash)
        {
            // Create the return value
            ObyTypedResult<SimpleWebserviceResult> result = new ObyTypedResult<SimpleWebserviceResult>();

            // Create a request log
            Requestlogger.CreateRequestlog("WebserviceHandler.SetPmsExpressCheckedOut", timestamp, macAddress, checkoutInfo, hash);

            try
            {
                // Validate the company credentials
                TerminalEntity terminal = Obymobi.Logic.HelperClasses.WebserviceHelper.ValidateRequestRequireTerminal(timestamp, macAddress, hash, this.OriginalAdditionalParameters);

                if (StringUtil.IsNullOrWhiteSpace(checkoutInfo.DeliverypointNumber))
                {
                    result.SetResult(SetPmsCheckedInResult.DeliverypointNumberMissing);
                }
                else
                {
                    DeliverypointEntity deliverypoint = Obymobi.Logic.HelperClasses.DeliverypointHelper.GetDeliverypoint(terminal.CompanyId, checkoutInfo.DeliverypointNumber, false, true, terminal.DeliverypointgroupId);
                    if (deliverypoint == null || deliverypoint.IsNew)
                    {
                        result.SetResult(SetPmsCheckedInResult.DeliverypointNumberUnknown);
                    }
                    else
                    {
                        // Forward guestinformation to Deliverypoint (means to Clients subscribed to this Deliverypoint)    
                        JsonSerializerSettings settings = new JsonSerializerSettings();
                        settings.NullValueHandling = NullValueHandling.Ignore;
                        settings.ContractResolver = new CamelCasePropertyNamesContractResolver();
                        string json = JsonConvert.SerializeObject(checkoutInfo, new Formatting(), settings);

                        CometHelper.SetPmsExpressCheckedOut(deliverypoint.DeliverypointId, json);

                        // Add the result
                        result.ModelCollection.Add(new SimpleWebserviceResult(true));
                    }
                }
            }
            catch (ObymobiException wex)
            {
                result.SetResult(wex);
                Exceptionlogger.CreateExceptionlog(wex);
            }
            catch (Exception ex)
            {
                result.SetResult(GenericWebserviceCallResult.Failure, ex);
                Exceptionlogger.CreateExceptionlog(ex);
            }

            Requestlogger.UpdateRequestLog(result);

            return result;
        }

        public ObyTypedResult<SimpleWebserviceResult> SetForwardTerminal(long timestamp, string macAddress, int forwardToTerminalId, string hash)
        {
            // Create the return value
            ObyTypedResult<SimpleWebserviceResult> result = new ObyTypedResult<SimpleWebserviceResult>();

            // Create a request log
            Requestlogger.CreateRequestlog("WebserviceHandler.SetForwardTerminal", timestamp, macAddress, forwardToTerminalId, hash);

            try
            {
                // Validate the company credentials
                TerminalEntity terminal = Obymobi.Logic.HelperClasses.WebserviceHelper.ValidateRequestRequireTerminal(timestamp, macAddress, hash, this.OriginalAdditionalParameters);

                if (!terminal.IsNew)
                {
                    if (forwardToTerminalId > 0)
                    {
                        terminal.ForwardToTerminalId = forwardToTerminalId;
                    }
                    else
                    {
                        terminal.ForwardToTerminalId = null;
                    }
                    terminal.Save();
                }

                // Add the result
                result.ModelCollection.Add(new SimpleWebserviceResult(true));
            }
            catch (ObymobiException wex)
            {
                result.SetResult(wex);
                Exceptionlogger.CreateExceptionlog(wex);
            }
            catch (Exception ex)
            {
                result.SetResult(GenericWebserviceCallResult.Failure);
                Exceptionlogger.CreateExceptionlog(ex);
            }

            Requestlogger.UpdateRequestLog(result);

            return result;
        }

        public ObyTypedResult<Order> GetOrderHistory(long timestamp, string macAddress, string hash)
        {
            // Create the return value
            ObyTypedResult<Order> result = new ObyTypedResult<Order>();

            // Create a request log
            Requestlogger.CreateRequestlog("WebserviceHandler.GetOrderHistory", timestamp, macAddress, hash);

            try
            {
                // Validate the company credentials
                TerminalEntity terminal = Obymobi.Logic.HelperClasses.WebserviceHelper.ValidateRequestRequireTerminal(timestamp, macAddress, hash, this.OriginalAdditionalParameters);

                // Get the order history for the specified terminal 
                Order[] orders = OrderHelper.GetOrderHistory(terminal.TerminalId);

                // Add the result
                result.ModelCollection.AddRange(orders);
            }
            catch (ObymobiException wex)
            {
                result.SetResult(wex);
                Exceptionlogger.CreateExceptionlog(wex);
            }
            catch (Exception ex)
            {
                result.SetResult(GenericWebserviceCallResult.Failure);
                Exceptionlogger.CreateExceptionlog(ex);
            }

            Requestlogger.UpdateRequestLog(result);

            return result;
        }

        public ObyTypedResult<SimpleWebserviceResult> SetEstimatedDeliveryTime(long timestamp, string macAddress, int deliverypointgroupId, int estimatedDeliveryTime, string hash)
        {
            // Create the return value
            ObyTypedResult<SimpleWebserviceResult> result = new ObyTypedResult<SimpleWebserviceResult>();

            // Create a request log
            Requestlogger.CreateRequestlog("WebserviceHandler.SetEstimatedDeliveryTime", timestamp, macAddress, deliverypointgroupId, estimatedDeliveryTime, hash);

            try
            {
                // Validate the credentials
                Obymobi.Logic.HelperClasses.WebserviceHelper.ValidateRequestRequireTerminal(timestamp, macAddress, hash, this.OriginalAdditionalParameters);

                DeliverypointgroupEntity dpgEntity = new DeliverypointgroupEntity(deliverypointgroupId);
                if (dpgEntity.IsNew)
                {
                    result.SetResult(GenericWebserviceCallResult.EntityNotFoundByPrimaryKey);
                }
                else
                {
                    dpgEntity.EstimatedDeliveryTime = estimatedDeliveryTime;
                    if (dpgEntity.Save())
                    {
                        // Cache on api
                        this.CacheDeliveryTimeOnApi(dpgEntity.CompanyId, dpgEntity.DeliverypointgroupId, estimatedDeliveryTime);

                        // Cache on cloud
                        this.CacheDeliveryTimeOnCloud(dpgEntity.DeliverypointgroupId, estimatedDeliveryTime);

                        result.SetResult(GenericWebserviceCallResult.Success);
                    }                    
                }
            }
            catch (ObymobiException wex)
            {
                result.SetResult(wex);
                Exceptionlogger.CreateExceptionlog(wex);
            }
            catch (Exception ex)
            {
                result.SetResult(GenericWebserviceCallResult.Failure);
                Exceptionlogger.CreateExceptionlog(ex);
            }

            Requestlogger.UpdateRequestLog(result);

            return result;
        }

        private void CacheDeliveryTimeOnApi(int companyId, int deliverypointgroupId, int deliveryTime)
        {
            string cacheKey = "GetEstimatedDeliveryTime-" + deliverypointgroupId;
            Dionysos.Web.CacheHelper.Add(false, cacheKey, deliveryTime);

            cacheKey = "GetEstimatedDeliveryTimes-" + companyId;
            DeliveryTime[] deliveryTimes = Obymobi.Logic.HelperClasses.DeliveryTimeHelper.GetDeliveryTimesForCompany(companyId).ToArray();
            Dionysos.Web.CacheHelper.Add(false, cacheKey, deliveryTimes);
        }

        private void CacheDeliveryTimeOnCloud(int deliverypointgroupId, int deliveryTime)
        {
            Obymobi.Logic.HelperClasses.CloudTaskHelper.UploadDeliveryTime(deliverypointgroupId, deliveryTime);
        }

        public ObyTypedResult<SimpleWebserviceResult> UpdateUIWidget(long timestamp, string macAddress, UIWidget uiWidget, string hash)
        {
            // Create the return value
            ObyTypedResult<SimpleWebserviceResult> result = new ObyTypedResult<SimpleWebserviceResult>();

            // Create a request log
            Requestlogger.CreateRequestlog("WebserviceHandler.UpdateUIWidget", timestamp, macAddress, uiWidget, hash);

            try
            {
                // Validate the credentials
                Obymobi.Logic.HelperClasses.WebserviceHelper.ValidateRequestRequireTerminal(timestamp, macAddress, hash, this.OriginalAdditionalParameters);

                UIWidgetEntity entity = UIWidgetHelper.UpdateUIWidget(uiWidget);
                if (entity != null)
                {
                    UIWidgetHelper.PushUIWidgetEntityToCompany(entity);

                    if (entity.ParentCompanyId.HasValue)
                    {
                        PublishHelper.Publish(entity.ParentCompanyId.Value, (service, publishTimestamp, publishMac, publishHash) =>
                        {
                            service.PublishCompanyAsync(publishTimestamp, publishMac, entity.ParentCompanyId.Value, publishHash);
                        });
                    }

                    result.SetResult(GenericWebserviceCallResult.Success);
                }                
            }
            catch (ObymobiException wex)
            {
                result.SetResult(wex);
                Exceptionlogger.CreateExceptionlog(wex);
            }
            catch (Exception ex)
            {
                result.SetResult(GenericWebserviceCallResult.Failure);
                Exceptionlogger.CreateExceptionlog(ex);
            }

            Requestlogger.UpdateRequestLog(result);

            return result;
        }

        public ObyTypedResult<SimpleWebserviceResult> UpdateAvailability(long timestamp, string macAddress, Availability availability, string hash)
        {
            // Create the return value
            ObyTypedResult<SimpleWebserviceResult> result = new ObyTypedResult<SimpleWebserviceResult>();

            // Create a request log
            Requestlogger.CreateRequestlog("WebserviceHandler.UpdateAvailabilities", timestamp, macAddress, availability, hash);

            try
            {
                // Validate the credentials
                Obymobi.Logic.HelperClasses.WebserviceHelper.ValidateRequestRequireTerminal(timestamp, macAddress, hash, this.OriginalAdditionalParameters);

                AvailabilityEntity updatedAvailabilityEntity = AvailabilityHelper.UpdateAvailability(availability);
                if (updatedAvailabilityEntity != null)
                {
                    AvailabilityHelper.PushAvailabilityToCompany(updatedAvailabilityEntity);
                    result.SetResult(GenericWebserviceCallResult.Success);
                }                
            }
            catch (ObymobiException wex)
            {
                result.SetResult(wex);
                Exceptionlogger.CreateExceptionlog(wex);
            }
            catch (Exception ex)
            {
                result.SetResult(GenericWebserviceCallResult.Failure);
                Exceptionlogger.CreateExceptionlog(ex);
            }

            Requestlogger.UpdateRequestLog(result);

            return result;
        }

        #endregion

        #region MISC methods

        public ObyTypedResult<SimpleWebserviceResult> VerifyTerminal(long timestamp, string macAddress, int terminalId, string hash)
        {
            // Create the return value
            ObyTypedResult<SimpleWebserviceResult> result = new ObyTypedResult<SimpleWebserviceResult>();

            // Create a request log
            Requestlogger.CreateRequestlog("WebserviceHandler.VerifyTerminal", timestamp, macAddress, terminalId, hash);

            try
            {
                // Validate the credentials
                TerminalEntity terminal = Obymobi.Logic.HelperClasses.WebserviceHelper.ValidateRequestRequireTerminal(timestamp, macAddress, hash, this.OriginalAdditionalParameters);
            }
            catch (ObymobiException wex)
            {
                result.SetResult(wex);
                Exceptionlogger.CreateExceptionlog(wex);
            }
            catch (Exception ex)
            {
                result.SetResult(GenericWebserviceCallResult.Failure);
                Exceptionlogger.CreateExceptionlog(ex);
            }

            Requestlogger.UpdateRequestLog(result);

            return result;
        }

        public ObyTypedResult<SimpleWebserviceResult> CleanUpPosData(long timestamp, string macAddress, string hash)
        {
            // Create the return value
            ObyTypedResult<SimpleWebserviceResult> result = new ObyTypedResult<SimpleWebserviceResult>();

            // Create a request log
            Requestlogger.CreateRequestlog("WebserviceHandler.CleanUpPosData", timestamp, macAddress, hash);

            try
            {
                // Get the company for the terminal
                StringBuilder synchronisationReport = new StringBuilder();

                // Validate the credentials
                TerminalEntity terminal = Obymobi.Logic.HelperClasses.WebserviceHelper.ValidateRequestRequireTerminal(timestamp, macAddress, hash);
                int companyId = terminal.CompanyId;

                //synchronisationReport.
                long batchId;

                // Clean up PosproductPosalteration
                Obymobi.Logic.HelperClasses.PosproductPosalterationHelper.CleanUp(companyId, ref synchronisationReport, out batchId);
                if (batchId > 0)
                {
                    WebserviceHandler.ReportCreatesAndUpdates(EntityType.PosproductPosalterationEntity, companyId, batchId, ref synchronisationReport);
                }

                // Clean up Posalterationitems
                Obymobi.Logic.HelperClasses.PosalterationitemHelper.CleanUp(companyId, ref synchronisationReport, out batchId);
                if (batchId > 0)
                {
                    WebserviceHandler.ReportCreatesAndUpdates(EntityType.PosalterationitemEntity, companyId, batchId, ref synchronisationReport);
                }

                // Clean up Posalterationoptions
                Obymobi.Logic.HelperClasses.PosalterationoptionHelper.CleanUp(companyId, ref synchronisationReport, out batchId);
                if (batchId > 0)
                {
                    WebserviceHandler.ReportCreatesAndUpdates(EntityType.PosalterationoptionEntity, companyId, batchId, ref synchronisationReport);
                }

                // Clean up Posalterations
                PosalterationHelper.CleanUp(companyId, ref synchronisationReport, out batchId);
                if (batchId > 0)
                {
                    WebserviceHandler.ReportCreatesAndUpdates(EntityType.PosalterationEntity, companyId, batchId, ref synchronisationReport);
                }

                // Clean up Products
                Obymobi.Logic.HelperClasses.PosproductHelper.CleanUp(companyId, ref synchronisationReport, out batchId);
                if (batchId > 0)
                {
                    WebserviceHandler.ReportCreatesAndUpdates(EntityType.PosproductEntity, companyId, batchId, ref synchronisationReport);
                }

                // Clean up Poscategories
                Obymobi.Logic.HelperClasses.PoscategoryHelper.CleanUp(companyId, ref synchronisationReport, out batchId);
                if (batchId > 0)
                {
                    WebserviceHandler.ReportCreatesAndUpdates(EntityType.PoscategoryEntity, companyId, batchId, ref synchronisationReport);
                }

                // Clean up Posdeliverypoints
                Obymobi.Logic.HelperClasses.PosdeliverypointHelper.CleanUp(companyId, ref synchronisationReport, out batchId);
                if (batchId > 0)
                {
                    WebserviceHandler.ReportCreatesAndUpdates(EntityType.PosdeliverypointEntity, companyId, batchId, ref synchronisationReport);
                }

                // Clean up Posdeliverypointgroups
                Obymobi.Logic.HelperClasses.PosdeliverypointgroupHelper.CleanUp(companyId, ref synchronisationReport, out batchId);
                if (batchId > 0)
                {
                    WebserviceHandler.ReportCreatesAndUpdates(EntityType.PosdeliverypointgroupEntity, companyId, batchId, ref synchronisationReport);
                }

                // Clean up Pospaymentmethods
                Obymobi.Logic.HelperClasses.PospaymentmethodHelper.CleanUp(companyId, ref synchronisationReport, out batchId);
                if (batchId > 0)
                {
                    WebserviceHandler.ReportCreatesAndUpdates(EntityType.PospaymentmethodEntity, companyId, batchId, ref synchronisationReport);
                }

                // Write the Report to the TerminalLog of the first found terminal.
                TerminalLogEntity log = new TerminalLogEntity();
                log.TerminalId = terminal.TerminalId;
                log.TypeAsEnum = TerminalLogType.PosSynchronisationReport;
                log.Message = "POS Synchronised";
                log.Log = synchronisationReport.ToString();
                log.Save();

                // GK, No idea where the code below is for.
                // Clean up ProductAlterations
                // Prepate filter for company
                PredicateExpression filter = new PredicateExpression();
                filter.Add(AlterationFields.CompanyId == companyId);

                RelationCollection relations = new RelationCollection();
                relations.Add(ProductAlterationEntityBase.Relations.AlterationEntityUsingAlterationId);

                ProductAlterationCollection productalterations = new ProductAlterationCollection();
                productalterations.GetMulti(filter, relations);

                foreach (ProductAlterationEntity productalteration in productalterations)
                {
                    string posproductExternalId1 = productalteration.ProductEntity.PosproductEntity.ExternalId;
                    string posproductExternalId2 = productalteration.PosproductPosalterationEntity.PosproductExternalId;

                    if (!posproductExternalId1.Equals(posproductExternalId2, StringComparison.InvariantCultureIgnoreCase))
                    {
                        productalteration.Delete();
                    }
                }

                // Synchronize all existing imported products
                PosDataSynchronizer syncer = new PosDataSynchronizer(companyId, CmsSessionHelper.AlterationDialogMode);
                syncer.SynchronizeExistingProducts(CmsSessionHelper.AlterationDialogMode);
            }
            catch (ObymobiException wex)
            {
                result.SetResult(wex);
                Exceptionlogger.CreateExceptionlog(wex);
            }
            catch (Exception ex)
            {
                result.SetResult(GenericWebserviceCallResult.Failure);
                Exceptionlogger.CreateExceptionlog(ex);
            }

            Requestlogger.UpdateRequestLog(result);

            return result;
        }

        public ObyTypedResult<SimpleWebserviceResult> WriteToLogZipped(long timestamp, string macAddress, int terminalId, int type, string message, string base64Zip, long fileDate, string hash)
        {
            // Create the return value
            ObyTypedResult<SimpleWebserviceResult> result = new ObyTypedResult<SimpleWebserviceResult>();

            try
            {
                // Validate the company owner, company, terminal combination
                Obymobi.Logic.HelperClasses.WebserviceHelper.ValidateRequestRequireTerminal(timestamp, macAddress, hash, this.OriginalAdditionalParameters);

                string filename;
                string log = WebserviceHandler.base64ZipToString(base64Zip, out filename);

                TerminalLogHelper.WriteToTerminalLog(terminalId, null, string.Empty, type, message, string.Empty, log, fileDate, filename);

                // Add the result
                result.ModelCollection.Add(new SimpleWebserviceResult(true));
            }
            catch (ObymobiException wex)
            {
                result.SetResult(wex);
                Exceptionlogger.CreateExceptionlog(wex);
            }
            catch (Exception ex)
            {
                result.SetResult(GenericWebserviceCallResult.Failure);
                Exceptionlogger.CreateExceptionlog(ex);
            }

            Requestlogger.UpdateRequestLog(result);

            return result;
        }

        public ObyTypedResult<SimpleWebserviceResult> WriteToLog(long timestamp, string macAddress, int terminalId, int orderId, string orderGuid, int type, string message, string status, string log, string hash)
        {
            // Create the return value
            ObyTypedResult<SimpleWebserviceResult> result = new ObyTypedResult<SimpleWebserviceResult>();

            try
            {
                // Validate the company owner, company, terminal combination
                TerminalEntity terminal = Obymobi.Logic.HelperClasses.WebserviceHelper.ValidateRequestRequireTerminal(timestamp, macAddress, hash, this.OriginalAdditionalParameters);

                // Write to the log
                if (orderId > 0)
                {
                    TerminalLogHelper.WriteToTerminalLog(terminalId, orderId, orderGuid, type, message, status, log);
                }
                else
                {
                    TerminalLogHelper.WriteToTerminalLog(terminalId, null, orderGuid, type, message, status, log);
                }

                if (type == (int)TerminalLogType.Start)
                {
                    terminal.LoadedSuccessfully = true;                    
                    terminal.Save();
                }
                else if (type == (int)TerminalLogType.Stop)
                {
                    terminal.LoadedSuccessfully = false;
                    terminal.Save();
                }

                // Add the result
                result.ModelCollection.Add(new SimpleWebserviceResult(true));
            }
            catch (ObymobiException wex)
            {
                result.SetResult(wex);
                Exceptionlogger.CreateExceptionlog(wex);
            }
            catch (Exception ex)
            {
                result.SetResult(GenericWebserviceCallResult.Failure);
                Exceptionlogger.CreateExceptionlog(ex);
            }

            Requestlogger.UpdateRequestLog(result);

            return result;
        }

        public ObyTypedResult<Message> GetMessages(long timestamp, string macAddress, int deliverypointId, string hash)
        {
            ObyTypedResult<Message> result = new ObyTypedResult<Message>();

            // Create a request log
            Requestlogger.CreateRequestlog("WebserviceHandler.GetMessages", timestamp, macAddress, deliverypointId, hash);

            try
            {
                // Validate the credentials
                ClientEntity client = Obymobi.Logic.HelperClasses.WebserviceHelper.ValidateRequestRequireClient(timestamp, macAddress, hash, this.OriginalAdditionalParameters);
                result.ModelCollection = MessageHelper.GetMessages(0, client.ClientId, deliverypointId).ToList();
                result.SetResult(GenericWebserviceCallResult.Success);
            }
            catch (ObymobiException wex)
            {
                result.SetResult(wex);
                Exceptionlogger.CreateExceptionlog(wex);
            }
            catch (Exception ex)
            {
                result.SetResult(GenericWebserviceCallResult.Failure);
                Exceptionlogger.CreateExceptionlog(ex);
            }

            Requestlogger.UpdateRequestLog(result);

            return result;
        }

        public ObyTypedResult<SimpleWebserviceResult> RemoveMessages(long timestamp, string macAddress, string messageIds, int dialogCloseResult, string hash)
        {
            // Create the return value
            ObyTypedResult<SimpleWebserviceResult> result = new ObyTypedResult<SimpleWebserviceResult>();

            // Create a request log
            Requestlogger.CreateRequestlog("WebserviceHandler.RemoveMessages", timestamp, macAddress, messageIds, dialogCloseResult, hash);

            try
            {
                // GK TODO Phase out - Reverse compatability
                if (timestamp == 0 && macAddress == "reversecompatability")
                {
                    // Don't validate request
                }
                else
                {
                    // Validate the company owner, company, terminal combination
                    Obymobi.Logic.HelperClasses.WebserviceHelper.ValidateRequest(timestamp, macAddress, hash, this.OriginalAdditionalParameters);
                }

                // Process the messages
                string[] ids = messageIds.Split(',');
                foreach (string messageRecipientId in ids)
                {
                    int messageRecipientIdInt;
                    if (int.TryParse(messageRecipientId, out messageRecipientIdInt) && messageRecipientIdInt > 0)
                    {
                        MessageHelper.RemoveMessageRecipientById(messageRecipientIdInt, dialogCloseResult.ToEnum<DialogCloseResult>());
                    }
                }

                // Add the result
                result.ModelCollection.Add(new SimpleWebserviceResult(true));
            }
            catch (ObymobiException wex)
            {
                result.SetResult(wex);
                Exceptionlogger.CreateExceptionlog(wex);
            }
            catch (Exception ex)
            {
                result.SetResult(GenericWebserviceCallResult.Failure);
                Exceptionlogger.CreateExceptionlog(ex);
            }

            Requestlogger.UpdateRequestLog(result);

            return result;
        }

        [Obsolete("Not used anymore in V9.0+")]
        public ObyTypedResult<SimpleWebserviceResult> ProcessNetmessages(long timestamp, string macAddress, int customerId, int clientId, int companyId, int deliverypointId, int terminalId, string hash)
        {
            // Create the return value
            ObyTypedResult<SimpleWebserviceResult> result = new ObyTypedResult<SimpleWebserviceResult>();

            // Create a request log
            Requestlogger.CreateRequestlog("WebserviceHandler.ProcessNetmessages", timestamp, macAddress, customerId, companyId, deliverypointId, terminalId, hash);

            try
            {
                Obymobi.Logic.HelperClasses.WebserviceHelper.ValidateRequest(timestamp, macAddress, hash, this.OriginalAdditionalParameters);

                CometHelper.ProcessNetmessagesForCustomer(customerId, clientId, companyId);
                if (deliverypointId > 0)
                {
                    CometHelper.ProcessNetmessagesForDeliverypoint(deliverypointId);
                }
                if (terminalId > 0)
                {
                    CometHelper.ProcessNetmessagesForTerminal(terminalId);
                }
            }
            catch (ObymobiException wex)
            {
                result.SetResult(wex);
                Exceptionlogger.CreateExceptionlog(wex);
            }
            catch (Exception ex)
            {
                result.SetResult(GenericWebserviceCallResult.Failure);
                Exceptionlogger.CreateExceptionlog(ex);
            }

            Requestlogger.UpdateRequestLog(result);

            return result;
        }

        public ObyTypedResult<SimpleWebserviceResult> VerifyNetmessage(long timestamp, string macAddress, int messageId, string hash)
        {
            // Create the return value
            ObyTypedResult<SimpleWebserviceResult> result = new ObyTypedResult<SimpleWebserviceResult>();

            // Create a request log
            Requestlogger.CreateRequestlog("WebserviceHandler.VerifyNetmessage", timestamp, macAddress, messageId, hash);

            try
            {
                // GK TODO - Remove when phased out!
                // GK Reverse compatability... :(
                if (timestamp == 0 && macAddress == "reversecompatability")
                {
                    CometHelper.VerifyNetmessage(messageId);
                }
                else
                {
                    Obymobi.Logic.HelperClasses.WebserviceHelper.ValidateRequest(timestamp, macAddress, hash, this.OriginalAdditionalParameters);

                    CometHelper.VerifyNetmessage(messageId);
                }
                // Add the result
                result.ModelCollection.Add(new SimpleWebserviceResult(true));
            }
            catch (ObymobiException wex)
            {
                result.SetResult(wex);
                Exceptionlogger.CreateExceptionlog(wex);
            }
            catch (Exception ex)
            {
                result.SetResult(GenericWebserviceCallResult.Failure);
                Exceptionlogger.CreateExceptionlog(ex);
            }

            Requestlogger.UpdateRequestLog(result);

            return result;
        }

        public ObyTypedResult<SimpleWebserviceResult> VerifyNetmessageByGuid(long timestamp, string macAddress, string messageGuid, bool isCompleted, string errorMessage, string trace, string hash)
        {
            // Create the return value
            ObyTypedResult<SimpleWebserviceResult> result = new ObyTypedResult<SimpleWebserviceResult>();

            // Create a request log
            Requestlogger.CreateRequestlog("WebserviceHandler.VerifyNetmessageByGuid", timestamp, macAddress, messageGuid, hash);

            try
            {
                Obymobi.Logic.HelperClasses.WebserviceHelper.ValidateRequest(timestamp, macAddress, hash, this.OriginalAdditionalParameters);
                
                CometHelper.VerifyNetmessage(null, messageGuid, isCompleted, errorMessage, trace);

                // Add the result
                result.ModelCollection.Add(new SimpleWebserviceResult(true));
            }
            catch (ObymobiException wex)
            {
                result.SetResult(wex);
                Exceptionlogger.CreateExceptionlog(wex);
            }
            catch (Exception ex)
            {
                result.SetResult(GenericWebserviceCallResult.Failure);
                Exceptionlogger.CreateExceptionlog(ex);
            }

            Requestlogger.UpdateRequestLog(result);

            return result;
        }

        public ObyTypedResult<SimpleWebserviceResult> ProcessOrders(long timestamp, string macAddress, int clientId, string hash)
        {
            // Create the return value
            ObyTypedResult<SimpleWebserviceResult> result = new ObyTypedResult<SimpleWebserviceResult>();

            // Create a request log
            Requestlogger.CreateRequestlog("WebserviceHandler.ProcessOrders", timestamp, macAddress, clientId, hash);

            try
            {
                // Validate the company owner, company, terminal combination
                DeviceEntity device = Obymobi.Logic.HelperClasses.WebserviceHelper.ValidateRequest(timestamp, macAddress, hash, this.OriginalAdditionalParameters);

                // Process the orders
                RoutingHelper.CancelRoutes(0, clientId, device.GetCompanyId().Value, string.Empty, null);

                // Add the result
                result.ModelCollection.Add(new SimpleWebserviceResult(true));
            }
            catch (ObymobiException wex)
            {
                result.SetResult(wex);
                Exceptionlogger.CreateExceptionlog(wex);
            }
            catch (Exception ex)
            {
                result.SetResult(GenericWebserviceCallResult.Failure);
                Exceptionlogger.CreateExceptionlog(ex);
            }

            Requestlogger.UpdateRequestLog(result);

            return result;
        }

        public ObyTypedResult<SimpleWebserviceResult> CancelOrder(long timestamp, string macAddress, int clientId, int customerId, string orderGuid, string hash)
        {
            // Create the return value
            ObyTypedResult<SimpleWebserviceResult> result = new ObyTypedResult<SimpleWebserviceResult>();

            // Create a request log
            Requestlogger.CreateRequestlog("WebserviceHandler.CancelOrder", timestamp, macAddress, clientId, customerId, orderGuid, hash);

            try
            {
                // Validate the company credentials
                Obymobi.Logic.HelperClasses.WebserviceHelper.ValidateRequest(timestamp, macAddress, hash, this.OriginalAdditionalParameters);

                // Process the orders
                RoutingHelper.CancelRoutes(customerId, clientId, 0, orderGuid, OrderProcessingError.CancelledByCustomer);

                // Add the result
                result.ModelCollection.Add(new SimpleWebserviceResult(true));
            }
            catch (ObymobiException wex)
            {
                result.SetResult(wex);
                Exceptionlogger.CreateExceptionlog(wex);
            }
            catch (Exception ex)
            {
                result.SetResult(GenericWebserviceCallResult.Failure);
                Exceptionlogger.CreateExceptionlog(ex);
            }

            Requestlogger.UpdateRequestLog(result);

            return result;
        }

        public ObyTypedResult<SimpleWebserviceResult> ManuallyProcessOrder(long timestamp, string macAddress, string orderGuid, string hash)
        {
            // Return object
            ObyTypedResult<SimpleWebserviceResult> result = new ObyTypedResult<SimpleWebserviceResult>();

            // Create a request log
            Requestlogger.CreateRequestlog("WebserviceHandler.ManuallyProcessOrder", timestamp, macAddress, orderGuid, hash);

            try
            {
                // Validate credentials
                TerminalEntity terminal = Obymobi.Logic.HelperClasses.WebserviceHelper.ValidateRequestRequireTerminal(timestamp, macAddress, hash, this.OriginalAdditionalParameters);

                OrderEntity orderEntity = OrderHelper.GetOrderByGuid(orderGuid, false);
                if (orderEntity == null)
                {
                    throw new ObymobiException(GenericWebserviceCallResult.Failure, "No order found with supplied GUID.");
                }

                // Process the order
                RoutingHelper.UpdateOrderStatus(orderEntity, OrderStatus.Processed, false, terminal);

                result.ModelCollection.Add(new SimpleWebserviceResult(true));
            }
            catch (ObymobiException wex)
            {
                result.SetResult(wex);
                Exceptionlogger.CreateExceptionlog(wex);
            }
            catch (Exception ex)
            {
                result.SetResult(GenericWebserviceCallResult.Failure);
                Exceptionlogger.CreateExceptionlog(ex);
            }

            Requestlogger.UpdateRequestLog(result);

            return result;
        }

        public ObyTypedResult<SimpleWebserviceResult> UploadFile(long timestamp, string macAddress, string f, string fileName, string hash)
        {
            // Create the return value
            ObyTypedResult<SimpleWebserviceResult> result = new ObyTypedResult<SimpleWebserviceResult>();

            // Create a request log
            Requestlogger.CreateRequestlog("WebserviceHandler.UploadFile", timestamp, macAddress, f, fileName, hash);

            try
            {
                // Validate request 
                DeviceEntity device = Obymobi.Logic.HelperClasses.WebserviceHelper.ValidateRequest(timestamp, macAddress, hash, this.OriginalAdditionalParameters);

                // the byte array argument contains the content of the file
                // the string argument contains the name and extension
                // of the file passed in the byte array

                // Check if Devicemedia folder exists
                string devicemediaDir = HostingEnvironment.MapPath("~/Downloads/Devicemedia/");
                if (!Directory.Exists(devicemediaDir))
                {
                    Directory.CreateDirectory(devicemediaDir);
                }

                // instance a memory stream and pass the
                // byte array to its constructor
                using (MemoryStream ms = new MemoryStream(Convert.FromBase64String(f)))
                {
                    // instance a filestream pointing to the
                    // storage folder, use the original file name
                    // to name the resulting file
                    using (FileStream fs = new FileStream(HostingEnvironment.MapPath("~/Downloads/Devicemedia/") + fileName, FileMode.CreateNew))
                    {
                        // write the memory stream containing the original
                        // file as a byte array to the filestream
                        ms.WriteTo(fs);
                    }
                }

                // Dump in database			
                DevicemediaEntity devicemediaEntity = new DevicemediaEntity();
                devicemediaEntity.DeviceId = device.DeviceId;
                devicemediaEntity.Filename = fileName;
                devicemediaEntity.WebserviceUrl = HttpContext.Current.Request.Url.Scheme + "://" + HttpContext.Current.Request.Url.Host + HttpContext.Current.Request.ApplicationPath;
                devicemediaEntity.Save();

                // return OK if we made it this far
                result.SetResult(GenericWebserviceCallResult.Success);
            }
            catch (ObymobiException wex)
            {
                result.SetResult(wex);
                Exceptionlogger.CreateExceptionlog(wex);
            }
            catch (Exception ex)
            {
                result.SetResult(GenericWebserviceCallResult.Failure);
                Exceptionlogger.CreateExceptionlog(ex);
            }

            Requestlogger.UpdateRequestLog(result);

            return result;
        }

        public ObyTypedResult<SimpleWebserviceResult> UploadFileInPost()
        {
            // Create the return value
            ObyTypedResult<SimpleWebserviceResult> result = new ObyTypedResult<SimpleWebserviceResult>();

            // Create a request log
            Requestlogger.CreateRequestlog("WebserviceHandler.UploadFileInPost");

            try
            {
                long timestamp = Convert.ToInt64(HttpContext.Current.Request.Params["timestamp"]);
                string macAddress = HttpContext.Current.Request.Params["macAddress"];
                string fileName = HttpContext.Current.Request.Params["filename"];
                string hash = HttpContext.Current.Request.Params["hash"];

                DeviceEntity device = Obymobi.Logic.HelperClasses.WebserviceHelper.ValidateRequest(timestamp, macAddress, hash, fileName);

                // Check if a file was included in the request
                if (HttpContext.Current.Request.Files.Count > 0)
                {
                    byte[] fileData = new byte[HttpContext.Current.Request.Files[0].InputStream.Length];
                    bool hasFile = fileData.Length > 0;

                    //Read the file from the InputStream
                    HttpContext.Current.Request.Files[0].InputStream.Read(fileData, 0, fileData.Length);

                    if (!Directory.Exists(HostingEnvironment.MapPath("~/Downloads/Devicemedia/")))
                    {
                        Directory.CreateDirectory(HostingEnvironment.MapPath("~/Downloads/Devicemedia/"));
                    }

                    if (hasFile)
                    {
                        // instance a filestream pointing to the storage folder, use the original file name to name the resulting file
                        using (FileStream fs = new FileStream(HostingEnvironment.MapPath("~/Downloads/Devicemedia/") + fileName, FileMode.Create))
                        {
                            fs.Write(fileData, 0, fileData.Length);
                            fs.Close();
                        }
                    }

                    string apiUrl = HttpContext.Current.Request.Url.Scheme + "://" + HttpContext.Current.Request.Url.Host + HttpContext.Current.Request.ApplicationPath;

                    // Dump in database				
                    DevicemediaEntity devicemediaEntity = new DevicemediaEntity();
                    devicemediaEntity.DeviceId = device.DeviceId;
                    devicemediaEntity.Filename = fileName;
                    devicemediaEntity.WebserviceUrl = hasFile ? apiUrl : string.Empty;
                    devicemediaEntity.Save();

                    // return OK if we made it this far
                    result.SetResult(GenericWebserviceCallResult.Success);
                }
                else
                {
                    throw new ObymobiException(GenericWebserviceCallResult.Failure, "No file could be retrieved");
                }
            }
            catch (ObymobiException wex)
            {
                result.SetResult(wex);
                Exceptionlogger.CreateExceptionlog(wex);
            }
            catch (Exception ex)
            {
                result.SetResult(GenericWebserviceCallResult.Failure);
                Exceptionlogger.CreateExceptionlog(ex);
            }

            Requestlogger.UpdateRequestLog(result);

            return result;
        }

        public ObyTypedResult<SimpleWebserviceResult> PrintFile()
        {
            // Create the return value
            ObyTypedResult<SimpleWebserviceResult> result = new ObyTypedResult<SimpleWebserviceResult>();

            // Create a request log
            Requestlogger.CreateRequestlog("WebserviceHandler.PrintFile");

            try
            {
                // Get the parameters
                long timestamp = Convert.ToInt64(HttpContext.Current.Request.Params["timestamp"]);
                string macAddress = HttpContext.Current.Request.Params["macAddress"];
                string hash = HttpContext.Current.Request.Params["hash"];

                // Validate the company credentials
                ClientEntity client = Obymobi.Logic.HelperClasses.WebserviceHelper.ValidateRequestRequireClient(timestamp, macAddress, hash);

                // Check if this client exists            
                if (!client.IsNew)
                {
                    // Check if this client has a valid deliverypointgroup
                    DeliverypointgroupEntity deliverypointgroup = client.DeliverypointgroupEntity;
                    if (!deliverypointgroup.IsNew)
                    {
                        // Check if this deliverypointgroup has a printer id selected
                        if (deliverypointgroup.GooglePrinterId.Length > 0)
                        {
                            // Create a suitable filename
                            string filename = string.Format("printerjob-{0}-{1}.png", TimeStamp.CreateTimeStamp(), client.ClientId);

                            // Check if a file was included in the request
                            if (HttpContext.Current.Request.Files.Count > 0)
                            {
                                HttpPostedFile file = HttpContext.Current.Request.Files[0];

                                byte[] fileData = new byte[HttpContext.Current.Request.Files[0].InputStream.Length];

                                //Read the file from the InputStream
                                HttpContext.Current.Request.Files[0].InputStream.Read(fileData, 0, (int)HttpContext.Current.Request.Files[0].InputStream.Length);

                                // Send the file data to Google Cloud Print
                                GoogleAPI google = new GoogleAPI();
                                if (!google.Print(deliverypointgroup.GooglePrinterId, filename, "data:image/png;base64," + Convert.ToBase64String(fileData), "dataUrl"))
                                {
                                    throw new ObymobiException(GenericWebserviceCallResult.Failure, "File could not be printed");
                                }
                            }
                            else
                            {
                                throw new ObymobiException(GenericWebserviceCallResult.Failure, "No file could be retrieved");
                            }
                        }
                        else
                        {
                            throw new ObymobiException(WebserviceHandlerResult.NoPrinterConfigured, "No printer is configured for this deliverypointgroup");
                        }
                    }
                    else
                    {
                        throw new ObymobiException(GenericWebserviceCallResult.Failure, "This client is not linked to a deliverypointgroup");
                    }
                }
                else
                {
                    throw new ObymobiException(GenericWebserviceCallResult.Failure, "Unknown or invalid client ID");
                }

                // return OK if we made it this far
                result.SetResult(GenericWebserviceCallResult.Success);
            }
            catch (ObymobiException wex)
            {
                result.SetResult(wex);
                Exceptionlogger.CreateExceptionlog(wex);
            }
            catch (Exception ex)
            {
                result.SetResult(GenericWebserviceCallResult.Failure);
                Exceptionlogger.CreateExceptionlog(ex);
            }

            Requestlogger.UpdateRequestLog(result);

            return result;
        }

        public ObyTypedResult<SimpleWebserviceResult> PrintPdf()
        {
            // Create the return value
            ObyTypedResult<SimpleWebserviceResult> result = new ObyTypedResult<SimpleWebserviceResult>();

            // Create a request log
            Requestlogger.CreateRequestlog("WebserviceHandler.PrintPdf");

            try
            {
                // Get the parameters
                long timestamp = Convert.ToInt64(HttpContext.Current.Request.Params["timestamp"]);
                string macAddress = HttpContext.Current.Request.Params["macAddress"];
                string hash = HttpContext.Current.Request.Params["hash"];
                string filename = HttpContext.Current.Request.Params["filename"];
                string title = HttpContext.Current.Request.Params["title"];

                // Validate the company credentials
                ClientEntity client = Obymobi.Logic.HelperClasses.WebserviceHelper.ValidateRequestRequireClient(timestamp, macAddress, hash, filename, title);
                if (client.DeliverypointEntity != null)
                {
                    byte[] mergedFile = PdfHelper.MergeWithCoverPage(HttpContext.Current.Request.Files[0], client);

                    if (client.DeliverypointgroupEntity.EmailDocumentRouteId.HasValue)
                    {
                        // Email document and notify on console

                        if (mergedFile.Length >= 15360000) // Max size: 20480000 * 0.75m (for BASE64 encoding)
                        {
                            // File size is too large, max of 20 mb can be attached to an email
                            result.SetResult(PrintPdfResult.FileSizeTooLarge);
                        }
                        else
                        {
                            Order order = OrderProcessingHelper.CreateEmailDocumentOrder(client, title, mergedFile);

                            ObyTypedResult<Order> orderResult = new ObyTypedResult<Order>();
                            Obymobi.Logic.HelperClasses.OrderProcessingHelper.CreateNewOrder(null, client, order, ref orderResult, false);

                            result.SetResult((GenericWebserviceCallResult)orderResult.ResultCode);
                        }
                    }
                    else
                    {
                        // Print the document directly through cloud printing

                        if (GoogleHelper.PrintFile(client, title, mergedFile))
                        {
                            result.SetResult(GenericWebserviceCallResult.Success);
                        }
                    }
                }
            }
            catch (ObymobiException wex)
            {
                result.SetResult(wex);
                Exceptionlogger.CreateExceptionlog(wex);
            }
            catch (Exception ex)
            {
                result.SetResult(GenericWebserviceCallResult.Failure);
                Exceptionlogger.CreateExceptionlog(ex);
            }

            Requestlogger.UpdateRequestLog(result);

            return result;
        }

        public ObyTypedResult<SimpleWebserviceResult> SetOrderPrinted(long timestamp, string macAddress, int orderId, string hash)
        {
            // Create the return value
            ObyTypedResult<SimpleWebserviceResult> result = new ObyTypedResult<SimpleWebserviceResult>();

            // Create a request log
            Requestlogger.CreateRequestlog("WebserviceHandler.SetOrderPrinted", timestamp, macAddress, orderId, hash);

            try
            {
                // Validate the company credentials
                Obymobi.Logic.HelperClasses.WebserviceHelper.ValidateRequestRequireTerminal(timestamp, macAddress, hash, this.OriginalAdditionalParameters);

                // Get the order and set the Printed flag to true
                OrderEntity orderEntity = new OrderEntity(orderId);
                if (!orderEntity.IsNew)
                {
                    orderEntity.Printed = true;
                    orderEntity.Save();
                }

                // Add the result
                result.ModelCollection.Add(new SimpleWebserviceResult(true));
            }
            catch (ObymobiException wex)
            {
                result.SetResult(wex);
                Exceptionlogger.CreateExceptionlog(wex);
            }
            catch (Exception ex)
            {
                result.SetResult(GenericWebserviceCallResult.Failure);
                Exceptionlogger.CreateExceptionlog(ex);
            }

            Requestlogger.UpdateRequestLog(result);

            return result;
        }

        public ObyTypedResult<Analytics> GetAnalytics(long timestamp, string macAddress, string hash)
        {
            // Create the return value
            ObyTypedResult<Analytics> result = new ObyTypedResult<Analytics>();

            // Create a request log
            Requestlogger.CreateRequestlog("WebserviceHandler.GetAnalytics", timestamp, macAddress, hash);

            try
            {
                // Validate the company credentials
                TerminalEntity terminal = Obymobi.Logic.HelperClasses.WebserviceHelper.ValidateRequestRequireTerminal(timestamp, macAddress, hash);

                // Get the analytics for the specified company
                Analytics analytics = AnalyticsHelper.CreateAnalyticsForCompany(terminal.CompanyEntity);

                // Add the result
                result.ModelCollection = new[] {analytics}.ToList();

                // Set the success flag
                result.SetResult(GenericWebserviceCallResult.Success);
            }
            catch (ObymobiException wex)
            {
                result.SetResult(wex);
                Exceptionlogger.CreateExceptionlog(wex);
            }
            catch (Exception ex)
            {
                result.SetResult(GenericWebserviceCallResult.Failure);
                Exceptionlogger.CreateExceptionlog(ex);
            }

            Requestlogger.UpdateRequestLog(result);

            return result;
        }

        public ObyTypedResult<SimpleWebserviceResult> SetMobileOrdering(long timestamp, string macAddress, bool enabled, string hash)
        {
            // Create the return value
            ObyTypedResult<SimpleWebserviceResult> result = new ObyTypedResult<SimpleWebserviceResult>();

            // Create a request log
            Requestlogger.CreateRequestlog("WebserviceHandler.SetMobileOrdering", timestamp, macAddress, enabled, hash);

            try
            {
                // Validate the company credentials
                Obymobi.Logic.HelperClasses.WebserviceHelper.ValidateRequestRequireTerminal(timestamp, macAddress, hash, this.OriginalAdditionalParameters);

                /*foreach (DeliverypointgroupEntity deliverypoint in terminal.DeliverypointgroupCollection)
	            {
		            // Send the mobile ordering update to PokeIn
		            CometHelper.SetMobileOrdering(terminal.CompanyId, deliverypoint.DeliverypointgroupId, enabled);
	            }*/

                result.SetResult(GenericWebserviceCallResult.Success);

                // Add the result
                result.ModelCollection.Add(new SimpleWebserviceResult(enabled));
            }
            catch (ObymobiException wex)
            {
                result.SetResult(wex);
                Exceptionlogger.CreateExceptionlog(wex);
            }
            catch (Exception ex)
            {
                result.SetResult(GenericWebserviceCallResult.Failure);
                Exceptionlogger.CreateExceptionlog(ex);
            }

            Requestlogger.UpdateRequestLog(result);

            return result;
        }

        [Obsolete]
        public ObyTypedResult<SimpleWebserviceResult> UpdateLastRequest(long timestamp, string macAddress, int clientId, int terminalId, string hash)
        {
            // This method has been marked as obsolete
            throw new NotImplementedException();
        }

        public ObyTypedResult<string> BroadcastDeliverypointMessage(long timestamp, string macAddress, int companyId, string message, int ttl, string deliverypoints, string hash)
        {
            ObyTypedResult<string> result = new ObyTypedResult<string>();

            // Create a request log
            Requestlogger.CreateRequestlog("WebserviceHandler.BroadcastDeliverypointMessage", timestamp, macAddress, companyId, message, ttl, deliverypoints, hash);

            try
            {
                Obymobi.Logic.HelperClasses.WebserviceHelper.ValidateRequest(timestamp, macAddress, hash, this.OriginalAdditionalParameters);

                if (deliverypoints.Length > 0)
                {
                    DeliverypointCollection deliverypointCollection = new DeliverypointCollection();

                    PredicateExpression filter = new PredicateExpression();
                    filter.Add(DeliverypointFields.CompanyId == companyId);

                    deliverypointCollection.GetMulti(filter);
                    EntityView<DeliverypointEntity> deliverypointView = deliverypointCollection.DefaultView;

                    string[] deliverypointsStr = deliverypoints.Split(',');
                    foreach (string deliverypointNumber in deliverypointsStr)
                    {
                        int numberInt;
                        if (int.TryParse(deliverypointNumber, out numberInt) && numberInt > 0)
                        {
                            deliverypointView.Filter = new PredicateExpression(DeliverypointFields.Number == deliverypointNumber);
                            if (deliverypointView.Count > 0)
                            {
                                CometHelper.BroadcastDeliverypointMessage(ttl, message, deliverypointView[0].DeliverypointId, companyId);
                            }
                        }
                    }
                }
                else
                {
                    // Send message to all deliverypointgroups aka. whole company
                    CometHelper.BroadcastDeliverypointMessage(ttl, message, 0, companyId);
                }

                result.SetResult(GenericWebserviceCallResult.Success);
            }
            catch (ObymobiException wex)
            {
                result.SetResult(wex);
                Exceptionlogger.CreateExceptionlog(wex);
            }
            catch (Exception ex)
            {
                result.SetResult(GenericWebserviceCallResult.Failure);
                Exceptionlogger.CreateExceptionlog(ex);
            }

            Requestlogger.UpdateRequestLog(result);

            return result;
        }

        public ObyTypedResult<Release> GetRelease(long timestamp, string macAddress, string applicationCode, string hash)
        {
            // Create the return value
            ObyTypedResult<Release> result = new ObyTypedResult<Release>();

            // Create a request log
            Requestlogger.CreateRequestlog("WebserviceHandler.GetRelease", timestamp, macAddress, hash);

            try
            {
                // Validate the request
                DeviceEntity device = Obymobi.Logic.HelperClasses.WebserviceHelper.ValidateRequest(timestamp, macAddress, hash, this.OriginalAdditionalParameters);

                try
                {
                    int companyId = (device.GetCompanyId() ?? 0);

                    // Get the release for the company of the specified company id
                    Release release = null;

                    if (applicationCode.StartsWith("CraveOS", StringComparison.InvariantCultureIgnoreCase))
                    {
                        string lastOSVersion = device.OsVersion;
                        ReleaseCollection releases = ReleaseHelper.GetCraveOSReleasesForDevice(device.DeviceModel.GetValueOrDefault(DeviceModel.Unknown), lastOSVersion, companyId);

                        if (releases.Count == 1 && releases[0].IntermediateReleaseId.HasValue)
                        {
                            release = ReleaseHelper.GetRelease(releases[0].IntermediateReleaseId.Value, companyId);
                        }
                        else if (releases.Count >= 1)
                        {
                            release = ReleaseHelper.GetRelease(releases[0].ReleaseId, companyId);
                        }
                        else if (releases.Count == 0)
                        {
                            throw new ObymobiException(Obymobi.Logic.HelperClasses.ReleaseHelper.ReleaseHelperErrors.NoStableReleaseSetForApplication, "AppliationCode: {0}", applicationCode);
                        }
                    }
                    else
                    {
                        release = ReleaseHelper.GetRelease(applicationCode, (device.GetCompanyId() ?? 0), false);
                    }

                    if (release != null)
                    {
                        result.ModelCollection.Add(release);
                    }
                }
                catch (ObymobiException oex)
                {
                    if (!oex.ErrorEnumValue.Equals(Obymobi.Logic.HelperClasses.ReleaseHelper.ReleaseHelperErrors.NoStableReleaseSetForApplication) && !oex.ErrorEnumValue.Equals(Obymobi.Logic.HelperClasses.ReleaseHelper.ReleaseHelperErrors.ReleaseDoesNotExistsOnDisk))
                    {
                        throw;
                    }
                }
            }
            catch (ObymobiException wex)
            {
                result.SetResult(wex);
            }
            catch (Exception ex)
            {
                result.SetResult(GenericWebserviceCallResult.Failure);
                Exceptionlogger.CreateExceptionlog(ex);
            }

            Requestlogger.UpdateRequestLog(result);

            return result;
        }

        public ObyTypedResult<SimpleWebserviceResult> SetUpdateStatus(long timestamp, string macAddress, string applicationCode, int updateStatus, string customMessage, int releaseId, string hash)
        {
            // Create the return value
            ObyTypedResult<SimpleWebserviceResult> result = new ObyTypedResult<SimpleWebserviceResult>();

            // Create a request log
            Requestlogger.CreateRequestlog("WebserviceHandler.SetDownloadStatus", timestamp, macAddress, hash);

            try
            {
                // Validate the request
                DeviceEntity device = Obymobi.Logic.HelperClasses.WebserviceHelper.ValidateRequest(timestamp, macAddress, hash, this.OriginalAdditionalParameters);

                // Get the release for the company of the specified company id
                ReleaseHelper.SetUpdateStatus((device.GetCompanyId() ?? 0), updateStatus, customMessage, device, releaseId);

                result.ModelCollection.Add(new SimpleWebserviceResult(true));
            }
            catch (ObymobiException wex)
            {
                result.SetResult(wex);
                Exceptionlogger.CreateExceptionlog(wex);
            }
            catch (Exception ex)
            {
                result.SetResult(GenericWebserviceCallResult.Failure);
                Exceptionlogger.CreateExceptionlog(ex);
            }

            Requestlogger.UpdateRequestLog(result);

            return result;
        }

        public ObyTypedResult<SimpleWebserviceResult> EmailFile()
        {
            // Create the return value
            ObyTypedResult<SimpleWebserviceResult> result = new ObyTypedResult<SimpleWebserviceResult>();

            // Create a request log
            Requestlogger.CreateRequestlog("WebserviceHandler.EmailFile");

            try
            {
                long timestamp = Convert.ToInt64(HttpContext.Current.Request.Params["timestamp"]);
                string macAddress = HttpContext.Current.Request.Params["macAddress"];
                string fileName = HttpContext.Current.Request.Params["filename"];
                string hash = HttpContext.Current.Request.Params["hash"];

                DeviceEntity device = Obymobi.Logic.HelperClasses.WebserviceHelper.ValidateRequest(timestamp, macAddress, hash, fileName);

                // Check if a file was included in the request
                if (HttpContext.Current.Request.Files.Count > 0)
                {
                    // Send the file to the company email
                    string subject = "File received";
                    string body = "Received on " + DateTimeUtil.GetDateTimeString();
                    string deliverypointCaption = "room";

                    CompanyEntity company = null;
                    ClientEntity client = device.ClientEntitySingle;
                    if (client != null)
                    {
                        if (client.DeliverypointgroupEntity.DeliverypointCaption.Length > 0)
                        {
                            deliverypointCaption = client.DeliverypointgroupEntity.DeliverypointCaption;
                        }
                        subject = "File received from " + deliverypointCaption + " " + client.DeliverypointEntity.Number;

                        company = client.CompanyEntity;
                    }
                    else
                    {
                        TerminalEntity terminal = device.TerminalEntitySingle;
                        if (terminal != null)
                        {
                            company = terminal.CompanyEntity;
                        }
                    }

                    if (company != null && company.Email.Length > 0)
                    {
                        bool canSendEmail = true;
                        if (client != null)
                        {
                            canSendEmail = Obymobi.Logic.HelperClasses.ClientHelper.WritePrintNotification(client, fileName);
                        }

                        if (canSendEmail)
                        {
                            const int timeout = 30000;

                            string[] emails = company.Email.SplitLines();
                            foreach (string email in emails)
                            {
                                MailUtilV2 mailUtil = new MailUtilV2("no-reply@crave-emenu.com", email, subject);
                                mailUtil.SmtpClient.Timeout = timeout;
                                mailUtil.BodyHtml = body;
                                mailUtil.Attachments.Add(new Attachment(HttpContext.Current.Request.Files[0].InputStream, fileName));

                                try
                                {
                                    mailUtil.SendMail().Wait(timeout);
                                }
                                catch (SmtpException)
                                {
                                    mailUtil.SendMail().Wait(timeout);
                                }
                            }
                        }
                    }
                    else
                    {
                        throw new ObymobiException(GenericWebserviceCallResult.Failure, "No company email found");
                    }

                    // return OK if we made it this far
                    result.SetResult(GenericWebserviceCallResult.Success);
                }
                else
                {
                    throw new ObymobiException(GenericWebserviceCallResult.Failure, "No file could be retrieved");
                }
            }
            catch (ObymobiException wex)
            {
                result.SetResult(wex);
                Exceptionlogger.CreateExceptionlog(wex);
            }
            catch (Exception ex)
            {
                result.SetResult(GenericWebserviceCallResult.Failure);
                Exceptionlogger.CreateExceptionlog(ex);
            }

            Requestlogger.UpdateRequestLog(result);

            return result;
        }

        public ObyTypedResult<SimpleWebserviceResult> SetApplicationVersion(long timestamp, string macAddress, string applicationCode, string versionNumber, string hash)
        {
            // Create the return value
            ObyTypedResult<SimpleWebserviceResult> result = new ObyTypedResult<SimpleWebserviceResult>();

            // Create a request log
            Requestlogger.CreateRequestlog("WebserviceHandler.SetApplicationVersion", timestamp, macAddress, hash);

            try
            {
                // Validate the request
                DeviceEntity deviceEntity = Obymobi.Logic.HelperClasses.WebserviceHelper.ValidateRequest(timestamp, macAddress, hash, this.OriginalAdditionalParameters);

                // Remove the leading 1. from the version number 
                if (versionNumber.StartsWith("1.") && versionNumber.Length > 2)
                {
                    versionNumber = versionNumber.Substring(2);
                }

                if (deviceEntity.ClientEntitySingle != null)
                {
                    Obymobi.Api.Logic.HelperClasses.DeviceHelper.SetApplicationVersion(deviceEntity, applicationCode, versionNumber);
                }
                else if (deviceEntity.TerminalEntitySingle != null)
                {
                    Obymobi.Api.Logic.HelperClasses.DeviceHelper.SetApplicationVersion(deviceEntity, applicationCode, versionNumber);
                }
                else
                {
                    throw new ObymobiException(GenericWebserviceCallResult.Failure, "Device is not linked to a client or terminal");
                }

                result.ModelCollection.Add(new SimpleWebserviceResult(true));
            }
            catch (ObymobiException wex)
            {
                result.SetResult(wex);
                Exceptionlogger.CreateExceptionlog(wex);
            }
            catch (Exception ex)
            {
                result.SetResult(GenericWebserviceCallResult.Failure);
                Exceptionlogger.CreateExceptionlog(ex);
            }

            Requestlogger.UpdateRequestLog(result);

            return result;
        }

        public ObyTypedResult<SimpleWebserviceResult> SetAgentCommandReponse(long timestamp, string macAddress, string resultMessage, string hash)
        {
            // Create the return value
            ObyTypedResult<SimpleWebserviceResult> result = new ObyTypedResult<SimpleWebserviceResult>();

            // Create a request log
            Requestlogger.CreateRequestlog("WebserviceHandler.SetAgentCommandReponse", timestamp, macAddress, hash);

            Transaction transaction = null;

            try
            {
                // Validate the request
                Obymobi.Logic.HelperClasses.WebserviceHelper.ValidateRequest(timestamp, macAddress, hash, this.OriginalAdditionalParameters);

                // This method only gets called when SupportTools is not connected to the CometServer, so save the message
                NetmessageAgentCommandResponse message = new NetmessageAgentCommandResponse();
                message.SenderMacAddress = macAddress;
                message.ReponseMessage = resultMessage;

                // Try to send result using the Comet connection of the webservice
                CometHelper.SendAgentCommandResponse(macAddress, resultMessage);

                transaction = new Transaction(IsolationLevel.ReadUncommitted, "Webservice-SetAgentCommandResponse-" + message.MessageType);

                NetmessageEntity messageEntity = Obymobi.Logic.HelperClasses.NetmessageHelper.CreateNetmessageEntityFromModel(message);
                messageEntity.AddToTransaction(transaction);
                messageEntity.StatusAsEnum = NetmessageStatus.Completed;
                messageEntity.Save();

                transaction.Commit();

                result.ModelCollection.Add(new SimpleWebserviceResult(true));
            }
            catch (ObymobiException wex)
            {
                if (transaction != null)
                {
                    transaction.Rollback();
                }

                result.SetResult(wex);
                Exceptionlogger.CreateExceptionlog(wex);
            }
            catch (Exception ex)
            {
                if (transaction != null)
                {
                    transaction.Rollback();
                }

                result.SetResult(GenericWebserviceCallResult.Failure);
                Exceptionlogger.CreateExceptionlog(ex);
            }
            finally
            {
                if (transaction != null)
                {
                    transaction.Dispose();
                }
            }

            Requestlogger.UpdateRequestLog(result);

            return result;
        }

        public ObyTypedResult<SimpleWebserviceResult> DailyOrderReset(long timestamp, string macAddress, string deliverypointNumbers, string deliverypointgroupIds, string hash)
        {
            // Create the return value
            ObyTypedResult<SimpleWebserviceResult> result = new ObyTypedResult<SimpleWebserviceResult>();

            // Create a request log
            Requestlogger.CreateRequestlog("WebserviceHandler.DailyOrderReset", timestamp, macAddress, deliverypointNumbers, deliverypointgroupIds, hash);

            try
            {
                // Validate the request
                TerminalEntity terminal = Obymobi.Logic.HelperClasses.WebserviceHelper.ValidateRequestRequireTerminal(timestamp, macAddress, hash, this.OriginalAdditionalParameters);

                PrefetchPath clientPrefetch = new PrefetchPath(EntityType.DeliverypointEntity);
                clientPrefetch.Add(DeliverypointEntityBase.PrefetchPathClientCollection);

                DeliverypointCollection deliverypointCollection = new DeliverypointCollection();
                deliverypointCollection.GetMulti(new PredicateExpression(DeliverypointFields.CompanyId == terminal.CompanyId), clientPrefetch);
                EntityView<DeliverypointEntity> deliverypointView = deliverypointCollection.DefaultView;

                if (deliverypointNumbers.Length > 0)
                {
                    PredicateExpression deliverypointFilter = new PredicateExpression();
                    string[] deliverypointsStr = deliverypointNumbers.Split(',');
                    for (int i = 0; i < deliverypointsStr.Length; i++)
                    {
                        int numberInt;
                        if (int.TryParse(deliverypointsStr[i], out numberInt) && numberInt > 0)
                        {
                            if (i == 0)
                            {
                                deliverypointFilter.Add(DeliverypointFields.Number == numberInt);
                            }
                            else
                            {
                                deliverypointFilter.AddWithOr(DeliverypointFields.Number == numberInt);
                            }
                        }
                    }
                    deliverypointView.Filter = deliverypointFilter;
                }
                else if (deliverypointgroupIds.Length > 0)
                {
                    PredicateExpression deliverypointgroupFilter = new PredicateExpression();
                    string[] deliverypointgroupsStr = deliverypointgroupIds.Split(',');
                    for (int i = 0; i < deliverypointgroupsStr.Length; i++)
                    {
                        int numberInt;
                        if (int.TryParse(deliverypointgroupsStr[i], out numberInt) && numberInt > 0)
                        {
                            if (i == 0)
                            {
                                deliverypointgroupFilter.Add(DeliverypointFields.DeliverypointgroupId == numberInt);
                            }
                            else
                            {
                                deliverypointgroupFilter.AddWithOr(DeliverypointFields.DeliverypointgroupId == numberInt);
                            }
                        }
                    }
                    deliverypointView.Filter = deliverypointgroupFilter;
                }

                // Send daily order reset to all clients
                foreach (DeliverypointEntity deliverypointEntity in deliverypointView)
                {
                    foreach (ClientEntity client in deliverypointEntity.ClientCollection)
                    {
                        CometHelper.SendDailyOrderReset(client.ClientId);
                    }
                }

                result.ModelCollection.Add(new SimpleWebserviceResult(true));
            }
            catch (ObymobiException wex)
            {
                result.SetResult(wex);
                Exceptionlogger.CreateExceptionlog(wex);
            }
            catch (Exception ex)
            {
                result.SetResult(GenericWebserviceCallResult.Failure);
                Exceptionlogger.CreateExceptionlog(ex);
            }

            Requestlogger.UpdateRequestLog(result);

            return result;
        }

        public ObyTypedResult<SimpleWebserviceResult> SendClearMessages(long timestamp, string macAddress, string deliverypointNumbers, string deliverypointgroupIds, string messageGroupIds, string hash)
        {
            // Create the return value
            ObyTypedResult<SimpleWebserviceResult> result = new ObyTypedResult<SimpleWebserviceResult>();

            // Create a request log
            Requestlogger.CreateRequestlog("WebserviceHandler.SendClearMessages", timestamp, macAddress, deliverypointNumbers, deliverypointgroupIds, hash);

            try
            {
                // Validate the request
                TerminalEntity terminal = Obymobi.Logic.HelperClasses.WebserviceHelper.ValidateRequestRequireTerminal(timestamp, macAddress, hash, this.OriginalAdditionalParameters);

                DeliverypointCollection deliverypoints = Obymobi.Logic.HelperClasses.DeliverypointHelper.GetDeliverypointsLinkedToOnlineClientsForCompany(terminal.CompanyId);
                EntityView<DeliverypointEntity> deliverypointView = deliverypoints.DefaultView;

                PredicateExpression deliverypointFilter = new PredicateExpression();
                if (messageGroupIds.Length > 0)
                {
                    List<int> messagegroupIds = new List<int>();

                    string[] messagegroupsStr = messageGroupIds.Split(',');
                    for (int i = 0; i < messagegroupsStr.Length; i++)
                    {
                        int messagegroupId;
                        if (int.TryParse(messagegroupsStr[i], out messagegroupId) && messagegroupId > 0)
                        {
                            messagegroupIds.Add(messagegroupId);
                        }
                    }

                    if (messagegroupIds.Count > 0)
                    {
                        DateTime localDateTime = DateTime.UtcNow.UtcToLocalTime(Obymobi.Logic.HelperClasses.CompanyHelper.GetCompanyTimeZone(terminal.CompanyId));

                        PredicateExpression filter = new PredicateExpression();
                        filter.Add(MessagegroupFields.MessagegroupId == messagegroupIds);

                        IncludeFieldsList deliverypointFields = new IncludeFieldsList();
                        deliverypointFields.Add(DeliverypointFields.DeliverypointId);
                        deliverypointFields.Add(DeliverypointFields.DeliverypointgroupId);

                        PrefetchPath prefetch = new PrefetchPath(EntityType.MessagegroupEntity);
                        prefetch.Add(MessagegroupEntityBase.PrefetchPathDeliverypointCollectionViaMessagegroupDeliverypoint, deliverypointFields);

                        MessagegroupCollection messagegroupEntities = new MessagegroupCollection();
                        messagegroupEntities.GetMulti(filter, prefetch);

                        List<int> deliverypointIdsForFilter = new List<int>();
                        List<string> deliverypointNumbersForFilter = new List<string>();

                        foreach (MessagegroupEntity messagegroup in messagegroupEntities)
                        {
                            if (messagegroup.Type == MessagegroupType.Custom)
                            {
                                deliverypointIdsForFilter.AddRange(messagegroup.DeliverypointCollectionViaMessagegroupDeliverypoint.Select(x => x.DeliverypointId));
                            }
                            else
                            {
                                Messagegroup[] models = PmsMessagegroupHelper.GetPmsMessagegroups(terminal.CompanyId, messagegroup.Type, localDateTime, messagegroup.GroupName);
                                if (models.Length > 0)
                                {
                                    deliverypointNumbersForFilter.AddRange(models[0].DeliverypointNumbers.Select(x => x.ToString()).ToList());
                                }
                            }
                        }

                        deliverypointFilter.Add(DeliverypointFields.DeliverypointId == deliverypointIdsForFilter);
                        deliverypointFilter.AddWithOr(DeliverypointFields.Number == deliverypointNumbersForFilter);
                    }
                }
                else if (deliverypointNumbers.Length > 0)
                {
                    List<string> deliverypointNumbersForFilter = deliverypointNumbers.Split(',').ToList();
                    deliverypointFilter.Add(DeliverypointFields.Number == deliverypointNumbersForFilter);
                }
                else if (deliverypointgroupIds.Length > 0)
                {
                    List<int> deliverypointgroupIdsForFilter = new List<int>();
                    foreach (string deliverypointgroupIdStr in deliverypointgroupIds.Split(','))
                    {
                        int numberInt;
                        if (int.TryParse(deliverypointgroupIdStr, out numberInt) && numberInt > 0)
                        {
                            deliverypointgroupIdsForFilter.Add(numberInt);
                        }
                    }
                    deliverypointFilter.Add(DeliverypointFields.DeliverypointgroupId == deliverypointgroupIdsForFilter);
                }

                deliverypointView.Filter = deliverypointFilter;
                
                foreach (DeliverypointEntity deliverypointEntity in deliverypointView)
                {
                    foreach (ClientEntity client in deliverypointEntity.ClientCollection)
                    {
                        CometHelper.SendClearMessages(client.ClientId);
                    }
                }

                result.ModelCollection.Add(new SimpleWebserviceResult(true));
            }
            catch (ObymobiException wex)
            {
                result.SetResult(wex);
                Exceptionlogger.CreateExceptionlog(wex);
            }
            catch (Exception ex)
            {
                result.SetResult(GenericWebserviceCallResult.Failure);
                Exceptionlogger.CreateExceptionlog(ex);
            }

            Requestlogger.UpdateRequestLog(result);

            return result;
        }

        public ObyTypedResult<SimpleWebserviceResult> SaveGuestInformations(long timestamp, string macAddress, GuestInformation[] guestInformations, string hash)
        {
            // Create the return value
            ObyTypedResult<SimpleWebserviceResult> result = new ObyTypedResult<SimpleWebserviceResult>();

            // Create a request log
            Requestlogger.CreateRequestlog("WebserviceHandler.SaveGuestInformations", timestamp, macAddress, guestInformations, hash);

            try
            {
                // Validate the company credentials
                TerminalEntity terminal = Obymobi.Logic.HelperClasses.WebserviceHelper.ValidateRequestRequireTerminal(timestamp, macAddress, hash, this.OriginalAdditionalParameters);

                // Save guest informations to db
                GuestInformationHelper.SaveGuestInformations(terminal.CompanyId, guestInformations);

                // Update the last synced datetime on the terminal
                terminal.LastSyncUTC = DateTime.UtcNow;
                terminal.Save();
            }
            catch (ObymobiException wex)
            {
                result.SetResult(wex);
                Exceptionlogger.CreateExceptionlog(wex);
            }
            catch (Exception ex)
            {
                result.SetResult(GenericWebserviceCallResult.Failure, ex);
                Exceptionlogger.CreateExceptionlog(ex);
            }

            Requestlogger.UpdateRequestLog(result);

            return result;
        }

        public ObyTypedResult<SimpleWebserviceResult> SaveFolios(long timestamp, string macAddress, Folio[] folios, string hash)
        {
            // Create the return value
            ObyTypedResult<SimpleWebserviceResult> result = new ObyTypedResult<SimpleWebserviceResult>();

            // Create a request log
            Requestlogger.CreateRequestlog("WebserviceHandler.SaveFolios", timestamp, macAddress, folios, hash);

            try
            {
                // Validate the company credentials
                TerminalEntity terminal = Obymobi.Logic.HelperClasses.WebserviceHelper.ValidateRequestRequireTerminal(timestamp, macAddress, hash, this.OriginalAdditionalParameters);

                // Save folios to db
                FolioHelper.SaveFolios(terminal.CompanyId, folios);                
            }
            catch (ObymobiException wex)
            {
                result.SetResult(wex);
                Exceptionlogger.CreateExceptionlog(wex);
            }
            catch (Exception ex)
            {
                result.SetResult(GenericWebserviceCallResult.Failure, ex);
                Exceptionlogger.CreateExceptionlog(ex);
            }

            Requestlogger.UpdateRequestLog(result);

            return result;
        }

        public ObyTypedResult<Messagegroup> GetPmsMessagegroups(long timestamp, string macAddress, int messagegroupType, string date, string groupName, string hash)
        {
            ObyTypedResult<Messagegroup> result = new ObyTypedResult<Messagegroup>();
            
            Requestlogger.CreateRequestlog("WebserviceHandler.GetPmsMessagegroups", timestamp, macAddress, hash);

            try
            {             
                TerminalEntity terminal = Obymobi.Logic.HelperClasses.WebserviceHelper.ValidateRequestRequireTerminal(timestamp, macAddress, hash, this.OriginalAdditionalParameters);

                MessagegroupType type = messagegroupType.ToEnum<MessagegroupType>();
                DateTime dateAsDateTime = !date.IsNullOrWhiteSpace() ? DateTime.ParseExact(date, "dd-MM-yyyy", CultureInfo.InvariantCulture) : new DateTime();

                Messagegroup[] messagegroups = PmsMessagegroupHelper.GetPmsMessagegroups(terminal.CompanyId, type, dateAsDateTime, groupName);
                result.ModelCollection.AddRange(messagegroups);                
            }
            catch (ObymobiException wex)
            {
                result.SetResult(wex);
                Exceptionlogger.CreateExceptionlog(wex);
            }
            catch (Exception ex)
            {
                result.SetResult(GenericWebserviceCallResult.Failure, ex);
                Exceptionlogger.CreateExceptionlog(ex);
            }

            Requestlogger.UpdateRequestLog(result);

            return result;
        }

        public ObyTypedResult<GuestGroup> GetGuestGroups(long timestamp, string macAddress, string hash)
        {
            ObyTypedResult<GuestGroup> result = new ObyTypedResult<GuestGroup>();

            Requestlogger.CreateRequestlog("WebserviceHandler.GetGuestGroups", timestamp, macAddress, hash);

            try
            {
                TerminalEntity terminal = Obymobi.Logic.HelperClasses.WebserviceHelper.ValidateRequestRequireTerminal(timestamp, macAddress, hash, this.OriginalAdditionalParameters);

                List<GuestGroup> guestGroups = new List<GuestGroup>();
                foreach (string groupName in Obymobi.Logic.HelperClasses.GuestInformationHelper.GetGroupNamesForCompany(terminal.CompanyId))
                {
                    guestGroups.Add(new GuestGroup
                                    {
                                        Name = groupName
                                    });
                }
                result.ModelCollection.AddRange(guestGroups);
            }
            catch (ObymobiException wex)
            {
                result.SetResult(wex);
                Exceptionlogger.CreateExceptionlog(wex);
            }
            catch (Exception ex)
            {
                result.SetResult(GenericWebserviceCallResult.Failure, ex);
                Exceptionlogger.CreateExceptionlog(ex);
            }

            Requestlogger.UpdateRequestLog(result);

            return result;
        }

        public ObyTypedResult<SimpleWebserviceResult> RegisterGameSession(long timestamp, string macAddress, string url, string hash)
        {
            // Create the return value
            ObyTypedResult<SimpleWebserviceResult> result = new ObyTypedResult<SimpleWebserviceResult>();

            // Create a request log
            Requestlogger.CreateRequestlog("WebserviceHandler.RegisterGameSession", timestamp, macAddress, hash);

            try
            {
                ClientEntity clientEntity = Obymobi.Logic.HelperClasses.WebserviceHelper.ValidateRequestRequireClient(timestamp, macAddress, hash, this.OriginalAdditionalParameters);
                if (clientEntity == null)
                    throw new ObymobiException(GenericWebserviceCallResult.Failure, "Device is not linked to a client");

                Obymobi.Logic.HelperClasses.GameSessionHelper.RegisterGameSession(clientEntity, url);

                result.ModelCollection.Add(new SimpleWebserviceResult(true));
            }
            catch (ObymobiException wex)
            {
                result.SetResult(wex);
                Exceptionlogger.CreateExceptionlog(wex);
            }
            catch (Exception ex)
            {
                result.SetResult(GenericWebserviceCallResult.Failure);
                Exceptionlogger.CreateExceptionlog(ex);
            }

            Requestlogger.UpdateRequestLog(result);

            return result;
        }

        #endregion

        #region Mobile methods

        public ObyTypedResult<CompanyDirectoryEntry> GetCompanyDirectory(long timestamp, string identifier, int customerId, string hash)
        {
            // Create the return value
            ObyTypedResult<CompanyDirectoryEntry> result = new ObyTypedResult<CompanyDirectoryEntry>();

            // Create a request log
            Requestlogger.CreateRequestlog("WebserviceHandler.GetCompanyDirectory", timestamp, identifier, customerId, hash);

            try
            {
                // Validate the request
                Obymobi.Logic.HelperClasses.WebserviceHelper.ValidateRequest(timestamp, identifier, hash, this.OriginalAdditionalParameters);

                // Get the company directory
                result.ModelCollection.AddRange(CompanyHelper.GetCompanyDirectoryEntryArray(customerId));
            }
            catch (ObymobiException wex)
            {
                result.SetResult(wex);
                Exceptionlogger.CreateExceptionlog(wex);
            }
            catch (Exception ex)
            {
                result.SetResult(GenericWebserviceCallResult.Failure);
                Exceptionlogger.CreateExceptionlog(ex);
            }

            Requestlogger.UpdateRequestLog(result);

            return result;
        }

        #endregion

        #region Helper methods

        private static void ReportCreatesAndUpdates(EntityType entityType, int companyId, long batch, ref StringBuilder report)
        {
            // Retrieve created & updated entities of this batch
            /*string entityName = entityType.ToString();
            IEntityCollection collection = Dionysos.Data.LLBLGen.LLBLGenEntityCollectionUtil.GetEntityCollection(entityName);

            // Max BatchId            

            // Start with the CREATED entities 
            report.AppendFormatLine("*** START: Created {0} entities", entityName);

            // Retrieve the entities
            PredicateExpression createdFilter = new PredicateExpression();
            Dionysos.Data.LLBLGen.LLBLGenFilterUtil.GetFieldCompareValueEqualPredicate(entityName, "CompanyId", companyId);
            Dionysos.Data.LLBLGen.LLBLGenFilterUtil.GetFieldCompareValueEqualPredicate(entityName, "CreatedInBatch", batch);
            collection.GetMulti(createdFilter);

            renderEntitiesToReport(report, collection);

            report.AppendFormatLine("*** FINISHED: Created {0} entities", entityName);

            // Now do the UPDATED entities
            report.AppendFormatLine("*** START: Created {0} entities", entityName);

            // Retrieve the entities
            PredicateExpression updatedFilter = new PredicateExpression();
            Dionysos.Data.LLBLGen.LLBLGenFilterUtil.GetFieldCompareValueEqualPredicate(entityName, "CompanyId", companyId);
            Dionysos.Data.LLBLGen.LLBLGenFilterUtil.GetFieldCompareValueEqualPredicate(entityName, "UpdatedInBatch", batch);
            collection.GetMulti(createdFilter);

            renderEntitiesToReport(report, collection);

            report.AppendFormatLine("*** FINISHED: Created {0} entities", entityName);*/
        }

        private static string base64ZipToString(string base64Zip, out string filename)
        {
            filename = "";
            string toReturn;
            byte[] zipBytes;
            try
            {
                zipBytes = Convert.FromBase64String(base64Zip);
            }
            catch
            {
                throw new ObymobiException(WebserviceHandlerResult.Base64StringInvalid);
            }

            // Unzip the log
            try
            {
                using (Stream str = new MemoryStream(zipBytes))
                {
                    toReturn = ZipUtil.UnzipText(str, out filename);
                }
            }
            catch
            {
                throw new ObymobiException(WebserviceHandlerResult.UnzipFailed);
            }

            return toReturn;
        }

        /// <summary>
        ///     All parameters not being timestamp, macAddress and hash, in the order they were supliied
        ///     this is to get a correct hash of complex objects that's given in XML.
        /// </summary>
        public object[] OriginalAdditionalParameters
        {
            get { return HttpContext.Current.Items["WebserviceHandler.OriginalRequestParameters"] as object[]; }
            set { HttpContext.Current.Items["WebserviceHandler.OriginalRequestParameters"] = value; }
        }

        /// <summary>
        ///     Implemented to be used by the LegacyRequestConvertor
        /// </summary>
        public static object[] OriginalAdditionalParametersStatic
        {
            get { return HttpContext.Current.Items["WebserviceHandler.OriginalRequestParameters"] as object[]; }
            set { HttpContext.Current.Items["WebserviceHandler.OriginalRequestParameters"] = value; }
        }

        public static void LegacyRequestConvertor(string username, string password, int? terminalId, int? clientId, out long timestamp, out string macAddress, out string hash, out DeviceEntity device, params object[] additionalParameters)
        {
            int deliverypointGroupId;
            WebserviceHandler.LegacyRequestConvertor(username, password, terminalId, clientId, out timestamp, out macAddress, out hash, out device, out deliverypointGroupId, false, additionalParameters);
        }

        public static void LegacyRequestConvertor(string username, string password, int? terminalId, int? clientId, out long timestamp, out string macAddress, out string hash, out DeviceEntity device, out int deliverypointGroupId, params object[] additionalParameters)
        {
            WebserviceHandler.LegacyRequestConvertor(username, password, terminalId, clientId, out timestamp, out macAddress, out hash, out device, out deliverypointGroupId, true, additionalParameters);
        }

        public const string EMPTY_USERNAME_AND_PASSWORD_VALUE = "JepIkBenLeeg";

        private static void LegacyRequestConvertor(string username, string password, int? terminalId, int? clientId, out long timestamp, out string macAddress, out string hash, out DeviceEntity device, out int deliverypointGroupId, bool getDeliverypointgroupId, params object[] additionalParameters)
        {
            if (username.Equals("chewg", StringComparison.InvariantCultureIgnoreCase) && password.Equals("treehouse", StringComparison.InvariantCultureIgnoreCase))
            {
                username = "Chewg";
                password = "Treehouse";
            }
            else if (username.Equals("the", StringComparison.InvariantCultureIgnoreCase) && password.Equals("wellesley", StringComparison.InvariantCultureIgnoreCase))
            {
                username = "the";
                password = "wellesley";
            }

            // Validate username/password combination
            if (username.Equals(WebserviceHandler.EMPTY_USERNAME_AND_PASSWORD_VALUE) && password.Equals(WebserviceHandler.EMPTY_USERNAME_AND_PASSWORD_VALUE))
            {
                // skip auth.
            }
            else
            {
                Obymobi.Logic.HelperClasses.WebserviceHelper.ValidateCredentials(username, password, null, false);
            }

            bool deviceRefetchRequired = false;
            deliverypointGroupId = 0;
            string salt;

            // Get a timestamp
            timestamp = DateTime.UtcNow.ToUnixTime();
            macAddress = string.Empty;

            // Get a Mac
            CompanyEntity company;
            device = null;
            if (clientId.HasValue && clientId > 0)
            {
                // Get by Client
                ClientEntity client = new ClientEntity(clientId.Value);

                if (client.IsNew)
                {
                    throw new ObymobiException(WebserviceHandlerResult.LegacyRequestFailedClientIdNonExistent, "ClientId: '{0}'", clientId);
                }

                company = client.CompanyEntity;
                salt = client.CompanyEntity.Salt;
                if (client.DeviceEntity.IsNew)
                {
                    // No Device Yet, FAKE one.

                    // Check if we had a fake one already
                    macAddress = "FAKE-Company-{0}-Client-{1}".FormatSafe(client.CompanyId, clientId.Value);
                    PredicateExpression filterFakeMac = new PredicateExpression(DeviceFields.Identifier == macAddress);
                    device = LLBLGenEntityUtil.GetSingleEntityUsingPredicateExpression<DeviceEntity>(filterFakeMac);

                    if (device == null)
                    {
                        device = new DeviceEntity();
                        device.Identifier = macAddress;
                        device.Save();
                    }

                    client.DeviceId = device.DeviceId;
                    client.Save();

                    deviceRefetchRequired = true;
                }
                else
                {
                    device = client.DeviceEntity;
                    macAddress = client.DeviceEntity.Identifier;
                }

                if (client.DeliverypointGroupId.HasValue)
                {
                    deliverypointGroupId = client.DeliverypointGroupId.Value;
                }
            }
            else if (terminalId.HasValue && terminalId > 0)
            {
                TerminalEntity terminal = new TerminalEntity(terminalId.Value);

                if (terminal.IsNew)
                {
                    throw new ObymobiException(WebserviceHandlerResult.LegacyRequestFailedTerminalIdNonExistent, "TerminalId: '{0}'", terminalId);
                }

                company = terminal.CompanyEntity;
                salt = terminal.CompanyEntity.Salt;

                if (terminal.DeviceEntity.IsNew)
                {
                    // No Device Yet, FAKE one.

                    // Check if we had a fake one already
                    macAddress = "FAKE-Company-{0}-Terminal-{1}".FormatSafe(terminal.CompanyId, terminalId.Value);
                    PredicateExpression filterFakeMac = new PredicateExpression(DeviceFields.Identifier == macAddress);
                    device = LLBLGenEntityUtil.GetSingleEntityUsingPredicateExpression<DeviceEntity>(filterFakeMac);

                    if (device == null)
                    {
                        device = new DeviceEntity();
                        device.Identifier = macAddress;
                        device.Save();
                    }
                    terminal.DeviceId = device.DeviceId;
                    terminal.Save();

                    deviceRefetchRequired = true;
                }
                else
                {
                    device = terminal.DeviceEntity;
                    macAddress = terminal.DeviceEntity.Identifier;
                }
            }
            else
            {
                throw new ObymobiException(WebserviceHandlerResult.LegacyRequestFailedNoClientIdOrTerminalId);
            }

            // Refetch device
            if (deviceRefetchRequired)
            {
                device = new DeviceEntity(device.DeviceId);
            }

            if (getDeliverypointgroupId && deliverypointGroupId == 0)
            {
                if (device.ClientEntitySingle != null)
                {
                    deliverypointGroupId = device.ClientEntitySingle.DeliverypointGroupId ?? 0;
                }

                if (deliverypointGroupId == 0)
                {
                    deliverypointGroupId = new CompanyEntity(device.GetCompanyId().GetValueOrDefault()).DeliverypointgroupCollection[0].DeliverypointgroupId;
                }
            }

            if (salt.IsNullOrWhiteSpace())
            {
                if (company != null)
                {
                    throw new ObymobiException(WebserviceHandlerResult.LegacyRequestFailedSaltNotSetForCompany, "CompanyId: '{0}'", company.CompanyId);
                }

                throw new ObymobiException(WebserviceHandlerResult.LegacyRequestFailedSaltNotSetForCompany);
            }

            if (macAddress.IsNullOrWhiteSpace())
            {
                if (device != null)
                {
                    throw new ObymobiException(WebserviceHandlerResult.LegacyRequestFailedMacAddressEmptyOnDevice, "DeviceId: '{0}'", device.DeviceId);
                }

                throw new ObymobiException(WebserviceHandlerResult.LegacyRequestFailedMacAddressEmptyOnDevice);
            }

            // Create Hash
            if (getDeliverypointgroupId)
            {
                hash = Hasher.GetHashFromParameters(salt, timestamp, macAddress, deliverypointGroupId, additionalParameters);
            }
            else
            {
                hash = Hasher.GetHashFromParameters(salt, timestamp, macAddress, additionalParameters);
            }

            // Forward additional parameters to Webservice handler via a bit of a dirty way.
            WebserviceHandler.OriginalAdditionalParametersStatic = additionalParameters;
        }

        /// <summary>
        ///     Returns true or false depending on whether 'applicationType' header is set with 'SupportTools' value.
        /// </summary>
        private bool IsSupportToolsRequest
        {
            get
            {
                string applicationType = HttpContext.Current.Request.Headers.Get("applicationType");
                return (applicationType != null && applicationType.Equals("SupportTools", StringComparison.InvariantCultureIgnoreCase));
            }
        }

        /// <summary>
        ///     Returns true or false depending on whether 'applicationType' header is set with 'Emenu' value.
        /// </summary>
        private bool IsEmenuRequest
        {
            get
            {
                string applicationType = HttpContext.Current.Request.Headers.Get("applicationType");
                return (applicationType != null && applicationType.Equals("Emenu", StringComparison.InvariantCultureIgnoreCase));
            }
        }

        #endregion
    }
}