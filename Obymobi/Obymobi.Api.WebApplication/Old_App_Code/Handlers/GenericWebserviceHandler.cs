﻿using System;
using Dionysos;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using Obymobi.Logging;
using Obymobi.Logic;
using Obymobi.Logic.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace ObymobiWebservice.Handlers
{
    public class GenericWebserviceHandler
    {
        /// <summary>
        /// Validate if the Webservice is alive (online)
        /// </summary>
        /// <returns>A boolean indicating if the webservice is alive.</returns>
        public bool IsWebserviceAlive()
        {
            Requestlogger.CreateRequestlog((int?)null, "GenericWebserviceHandler.IsWebserviceAlive");
            Requestlogger.UpdateRequestLog(GenericWebserviceCallResult.Success, "Success", null);
            return true;
        }

        public ObyTypedResult<long> GetCurrentTime()
        {
            var toReturn = new ObyTypedResult<long>();
            toReturn.ModelCollection.Add(DateTime.UtcNow.ToUnixTime());
            toReturn.SetResult(GenericResult.Success);

            return toReturn;
        }

        /// <summary>
        /// Validate if the database used by the webservice is alive (online)
        /// </summary>
        /// <returns>A boolean indicating if the the database is alive.</returns>
        public bool IsDatabaseAlive()
        {
            bool success = false;

            try
            {
                var vattariffCollection = new VattariffCollection();
                if (vattariffCollection.GetDbCount() > 0)
                    success = true;
                else
                    Requestlogger.UpdateRequestLog(GenericWebserviceCallResult.Failure, "Failure", null);
            }
            catch (Exception ex)
            {
                Requestlogger.UpdateRequestLog(GenericWebserviceCallResult.Failure, "Failure", ex);
            }

            return success;
        }

        public string GetReleaseAndDownloadLocation(string username, string password, int something, string somethine2)
        {
            // GK TODO > Added this so it would ocmpile after check in.
            //throw new NotImplementedException();
	        return "";
        }

        /// <summary>
        /// Gets the release version and download link for the requested application and company
        /// </summary>
        /// <param name="applicationCode">Code of the application.</param>
        /// <returns>A <see cref="String"/> instance containing the latest release and the download location</returns>
        public string GetReleaseAndDownloadLocation(long sequence, string macAddress, string applicationCode, string hash)
        {
            Requestlogger.CreateRequestlog((int?)null, "GenericWebserviceHandler.GetReleaseAndDownloadLocation");
            string toReturn = "";

            try
            {
                // Validate the credentials
                DeviceEntity device = WebserviceHelper.ValidateRequest(sequence, macAddress, hash, applicationCode);

                // Get the company releases
                CompanyReleaseCollection companyReleases = new CompanyReleaseCollection();

                RelationCollection companyReleaseRelations = new RelationCollection();
                companyReleaseRelations.Add(CompanyReleaseEntityBase.Relations.ReleaseEntityUsingReleaseId);
                companyReleaseRelations.Add(ReleaseEntityBase.Relations.ApplicationEntityUsingApplicationId);
                companyReleaseRelations.Add(CompanyReleaseEntityBase.Relations.CompanyEntityUsingCompanyId);

                PredicateExpression companyReleasefilter = new PredicateExpression();
                companyReleasefilter.Add(ApplicationFields.Code == applicationCode);

                if (device.GetCompanyId().Value > 0)
                    companyReleasefilter.Add(CompanyFields.CompanyId == device.GetCompanyId().Value);

                companyReleases.GetMulti(companyReleasefilter, companyReleaseRelations);

                CompanyReleaseEntity companyRelease = null;

                if (companyReleases.Count > 0)
                    companyRelease = companyReleases[0];

                // Get the stable release
                PredicateExpression releaseFilter = new PredicateExpression();
                releaseFilter.Add(ApplicationFields.Code == applicationCode);
                releaseFilter.Add(ApplicationFields.ReleaseId != DBNull.Value);

                RelationCollection releaseRelations = new RelationCollection();
                releaseRelations.Add(ApplicationEntityBase.Relations.ReleaseEntityUsingReleaseId);

                ReleaseCollection releases = new ReleaseCollection();
                releases.GetMulti(releaseFilter, releaseRelations);

                ReleaseEntity stableRelease = null;

                if (releases.Count > 0)
                {
                    // Get the latest stable release
                    stableRelease = releases[0];

                    // Create the object for the release to download
                    ReleaseEntity releaseToDownload = null;

                    if (companyRelease != null)
                    {
                        int companyVersion = -1;
                        int stableVersion = -1;

                        if (int.TryParse(companyRelease.ReleaseEntity.Version, out companyVersion) && int.TryParse(stableRelease.Version, out stableVersion))
                        {
                            releaseToDownload = companyRelease.ReleaseEntity;
                        }
                        else
                            releaseToDownload = stableRelease;
                    }
                    else
                        releaseToDownload = stableRelease;

                    toReturn = string.Format("[{0}|{1}]", releaseToDownload.Version, releaseToDownload.Filename);
                }
            }
            catch (Exception ex)
            {
                Requestlogger.UpdateRequestLog(GenericWebserviceCallResult.Failure, "Failure", ex);
            }

            return toReturn;
        }
    }
}