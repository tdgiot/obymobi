﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using Dionysos;
using Dionysos.Data.LLBLGen;
using Obymobi;
using Obymobi.Api.Logic.EntityConverters.Mobile;
using Obymobi.Api.Logic.HelperClasses;
using Obymobi.Cms.Logic.Sites;
using Obymobi.Constants;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using Obymobi.Logging;
using Obymobi.Logic;
using Obymobi.Logic.Model.Mobile;
using Obymobi.Security;
using Obymobi.Web.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace ObymobiWebservice.Handlers
{
    public class MobileWebserviceHandler : GenericWebserviceHandler
    {
        #region GET methods

        public new ObyTypedMobileResult<long> GetCurrentTime()
        {
            // Create a request log
            Requestlogger.CreateRequestlog("MobileWebserviceHandler.GetCurrentTime");            

            // Create the return value
            var result = new ObyTypedMobileResult<long>();
            result.ModelCollection.Add(DateTime.UtcNow.ToUnixTime());

            Requestlogger.UpdateRequestLog(result);

            return result;
        }

        public ObyTypedMobileResult<long> GetMinimumRequiredVersion()
        {
            // Create the return value
            ObyTypedMobileResult<long> result = new ObyTypedMobileResult<long>();

            // Create a request log
            Requestlogger.CreateRequestlog("MobileWebserviceHandler.GetMinimumRequiredVersion");

            try
            {
                // Get the company
                string minVersionText = ConfigurationManager.GetString(ObymobiConfigConstants.MinimumVersionMobileApp);
                minVersionText = StringUtil.GetAllAfterFirstOccurenceOf(minVersionText, '.', true);
                long version = Convert.ToInt32(minVersionText);

                result.ModelCollection.Add(version);
            }
            catch (Obymobi.ObymobiException wex)
            {
                result.SetResult(GenericWebserviceCallResult.Failure);
                result.SetInnerResult(wex);
                Exceptionlogger.CreateExceptionlog(wex);
            }
            catch (Exception ex)
            {
                result.SetResult(GenericWebserviceCallResult.Failure);
                result.SetInnerResult(GenericWebserviceCallResult.Failure, ex.Message);
                Exceptionlogger.CreateExceptionlog(ex);
            }

            Requestlogger.UpdateRequestLog(result);

            return result;
        }

        #region 'Non-secure' methods (methods which work with the Mobile Generic Salt

        public ObyTypedMobileResult<Customer> CreateCustomer(long timestamp, string emailAddress, string phonenumber, string passwordTransferHash, bool anonymousAccount, int deviceType, int devicePlatform, string deviceModel, string anonymousEmail, string anonymousPasswordTransferHash, string customerGuid, int socialMediaType, string externalId, string campaignName, string campaignSource, string campaignMedium, string campaignContent, string hash)
        {
            // Create the return value
            ObyTypedMobileResult<Customer> result = new ObyTypedMobileResult<Customer>();

            // Create a request log
            Requestlogger.CreateRequestlog("MobileWebserviceHandler.CreateCustomer", timestamp, phonenumber, emailAddress, hash);

            try
            {
                // Validate the request
                Obymobi.Logic.HelperClasses.WebserviceHelper.ValidateTimestamp(timestamp);
                string locallyCalculatedHash = Obymobi.Security.Hasher.GetHashFromParameters(passwordTransferHash + ObymobiConstants.MobileSalt, timestamp, emailAddress, this.OriginalAdditionalParameters);
                
                // Validate DevicePlatform
                DevicePlatform devicePlatformAsEnum;
                if (!EnumUtil.TryParse(devicePlatform, out devicePlatformAsEnum))
                    throw new ObymobiException(CreateCustomerResult.InvalidDevicePlatform, "Invalid Platform: {0}", devicePlatform);

                // Validate DeviceType
                DeviceType deviceTypeAsEnum;
                if (!EnumUtil.TryParse(deviceType, out deviceTypeAsEnum))
                    throw new ObymobiException(CreateCustomerResult.InvalidDeviceType, "Invalid Type: {0}", deviceType);

                // Validate SocialMediaType
                SocialmediaType socialmediaTypeAsEnum;
                if (!EnumUtil.TryParse(socialMediaType, out socialmediaTypeAsEnum))
                    throw new ObymobiException(CreateCustomerResult.InvalidSocialMediaType, "Invalid SocialMediaType: {0}", socialMediaType);

                if (!locallyCalculatedHash.Equals(hash))
                { 
                    // Corrupt call.
                    throw new ObymobiException(CreateCustomerResult.InvalidHash);
                }

                // Empty customer entity
                CustomerEntity customerEntity = null;

                // Check if we create a spankin' new account, or create it based on an Anonymous account
                if (!anonymousEmail.IsNullOrWhiteSpace() && !anonymousPasswordTransferHash.IsNullOrWhiteSpace() && !anonymousAccount)
                {
                    // No customer with this email, convert the anonymous customer to a real customer
                    var anonCustomer = Obymobi.Logic.HelperClasses.CustomerHelper.GetAnonymousCustomerByEmail(anonymousEmail, anonymousPasswordTransferHash);

                    // Try to get the customer matching the desired email
                    var existingCustomer = Obymobi.Logic.HelperClasses.CustomerHelper.GetCustomerByEmail(emailAddress);
                    if (existingCustomer == null)
                    {
                        if (socialmediaTypeAsEnum == SocialmediaType.Unknown)
                        {
                            customerEntity = Obymobi.Logic.HelperClasses.CustomerHelper.CreateCustomerFromAnonymous(anonCustomer, emailAddress, phonenumber, passwordTransferHash);
                        }
                        else
                        {
                            throw new ObymobiException(CreateCustomerResult.InvalidSocialMediaType, "Invalid SocialMediaType: {0}", socialMediaType);

                            // Refactored out: Obsolete
                            // customerEntity = CustomerHelperWeb.LinkSocialmediaToCustomer(anonCustomer, socialmediaTypeAsEnum, emailAddress, passwordTransferHash, externalId);
                        }

                        if (customerEntity.IsNew)
                        {
                            // Set the device for the new customer
                            Obymobi.Logic.HelperClasses.CustomerHelper.SetDevices(customerEntity, deviceTypeAsEnum, devicePlatformAsEnum, deviceModel);
                        } 
                    }
                    else if (socialmediaTypeAsEnum == SocialmediaType.Unknown)
                    {
                        // Throw exception of there's already a customer with the same email. (not for socialmedia, another account can be linked, see below...)
                        throw new ObymobiException(CustomerSaveResult.EmailAlreadyInUse);
                    }
                    else
                    {
                        throw new ObymobiException(CreateCustomerResult.InvalidSocialMediaType, "Invalid SocialMediaType: {0}", socialMediaType);

                        //// Link a socialmedia entity to the customer
                        //customerEntity = CustomerHelperWeb.LinkSocialmediaToCustomer(existingCustomer, socialmediaTypeAsEnum, emailAddress, passwordTransferHash, externalId);

                        //// Delete the anonymous customer
                        //if (anonCustomer != null) anonCustomer.Delete();
                    }
                }
                else
                {
                    if (customerGuid.Length > 0)
                    {
                        var filter = new PredicateExpression(CustomerFields.GUID == customerGuid);
                        customerEntity = LLBLGenEntityUtil.GetSingleEntityUsingPredicateExpression<CustomerEntity>(filter);
                    }

                    if (customerEntity == null || !customerEntity.CanSingleSignOn)
                    {
                        // If it's a Anonymous account, first check if the username is unique, if not, generate a new one - Take a maximum of 5 seconds to fine one.
                        if (anonymousAccount)
                        {
                            var customers = new CustomerCollection();
                            var questStarted = DateTime.Now;
                            while ((DateTime.Now - questStarted).TotalSeconds < 5)
                            {
                                var filter = new PredicateExpression();
                                filter.Add(CustomerFields.Email % emailAddress);
                                if (customers.GetDbCount(filter) == 0)
                                    break;

                                emailAddress = RandomUtil.GetRandomLowerCaseString(32);
                            }
                        }

                        customerEntity = Obymobi.Logic.HelperClasses.CustomerHelper.GetCustomerByEmail(emailAddress);
                        if (customerEntity == null)
                        {
                            customerEntity = new CustomerEntity();
                            customerEntity.Phonenumber = phonenumber;
                            customerEntity.Email = emailAddress;
                            customerEntity.AnonymousAccount = anonymousAccount;
                            customerEntity.GUID = customerGuid;
                            customerEntity.CampaignName = campaignName;
                            customerEntity.CampaignSource = campaignSource;
                            customerEntity.CampaignMedium = campaignMedium;
                            customerEntity.CampaignContent = campaignContent;
                        }
                        else if (socialmediaTypeAsEnum == SocialmediaType.Unknown)
                        {
                            // Throw exception of there's already a customer with the same email. (not for socialmedia, another account can be linked, see below...)
                            throw new ObymobiException(CustomerSaveResult.EmailAlreadyInUse);
                        }
                    }

                    if (customerEntity.IsNew || customerEntity.CanSingleSignOn)
                    {
                        // Create the customer
                        customerEntity = Obymobi.Logic.HelperClasses.CustomerHelper.CreateCustomer(customerEntity, emailAddress, passwordTransferHash, anonymousAccount, deviceTypeAsEnum, devicePlatformAsEnum, deviceModel);
                    }
                        
                    if (socialmediaTypeAsEnum != SocialmediaType.Unknown)
                    {
                        throw new ObymobiException(CreateCustomerResult.InvalidSocialMediaType, "Invalid SocialMediaType: {0}", socialMediaType);

                        //// Link a socialmedia entity to the customer
                        //customerEntity = CustomerHelperWeb.LinkSocialmediaToCustomer(customerEntity, socialmediaTypeAsEnum, emailAddress, passwordTransferHash, externalId);
                    }
                }

                //if (!customerEntity.AnonymousAccount && !customerEntity.Verified && socialMediaType == (int)SocialmediaType.Unknown)
                //{
                //    // Send the confirmation email for the customer to confirm their email
                //    CustomerHelperWeb.SendConfirmationEmail(customerEntity, customerEntity.Email);
                //}

                Customer customer = new CustomerEntityConverter().ConvertEntityToMobileModel(customerEntity);
                customer.SocialmediaType = socialMediaType;
                customer.ExternalId = externalId;

                result.ModelCollection.Add(customer);
                result.SetResult(CreateCustomerResult.Success);
            }
            catch (Obymobi.ObymobiException wex)
            {
                ConvertToCreateCustomerResult(result, wex);

                Exceptionlogger.CreateExceptionlog(wex);
            }
            catch (Exception ex)
            {
                result.SetResult(GetCustomerResult.Failure);
                result.SetInnerResult(GenericWebserviceCallResult.Failure, ex.Message);
                Exceptionlogger.CreateExceptionlog(ex);
            }

            Requestlogger.UpdateRequestLog(result);

            return result;
        }

        public ObyTypedMobileResult<Customer> VerifyCustomer(long timestamp, string initialLinkIdentifier, string hash)
        {
            // Create the return value
            ObyTypedMobileResult<Customer> result = new ObyTypedMobileResult<Customer>();

            // Create a request log
            Requestlogger.CreateRequestlog("MobileWebserviceHandler.VerifyCustomer", timestamp, initialLinkIdentifier, hash);

            try
            {
                // Validate the request
                Obymobi.Logic.HelperClasses.WebserviceHelper.ValidateTimestamp(timestamp);
                string locallyCalculatedHash = Obymobi.Security.Hasher.GetHashFromParameters(ObymobiConstants.MobileSalt, timestamp, this.OriginalAdditionalParameters);

                if (!locallyCalculatedHash.Equals(hash))
                {
                    // Corrupt call.
                    throw new ObymobiException(VerifyCustomerResult.InvalidHash);
                }

                // Empty customer entity
                CustomerEntity customerEntity;

                // Verify the customer based on the InitialLinkIdentifier
                Obymobi.Logic.HelperClasses.CustomerHelper.VerifyCustomer(initialLinkIdentifier, out customerEntity);

                // Add the result
                Customer customer = new CustomerEntityConverter().ConvertEntityToMobileModel(customerEntity);
                
                result.ModelCollection.Add(customer);
                result.SetResult(VerifyCustomerResult.Success);
            }
            catch (Obymobi.ObymobiException wex)
            {
                if (wex.ErrorEnumValue is VerifyCustomerResult)
                {
                    result.SetResult(wex);
                }
                else if (wex.ErrorEnumValue is ValidateCredentialsResult)
                {
                    result.SetResult(GetCustomerResult.AuthenticationError, (ValidateCredentialsResult)wex.ErrorEnumValue);
                    result.SetInnerResult(wex);
                }
                else
                {
                    result.SetResult(VerifyCustomerResult.Failure);
                    result.SetInnerResult(wex);
                }

                Exceptionlogger.CreateExceptionlog(wex);
            }
            catch (Exception ex)
            {
                result.SetResult(VerifyCustomerResult.Failure);
                result.SetInnerResult(GenericWebserviceCallResult.Failure, ex.Message);
                Exceptionlogger.CreateExceptionlog(ex);
            }

            Requestlogger.UpdateRequestLog(result);

            return result;
        }

        public ObyTypedMobileResult<bool> RequestPasswordReset(long timestamp, string emailAddress, string hash)
        {
            // Create the return value
            ObyTypedMobileResult<bool> result = new ObyTypedMobileResult<bool>();

            // Create a request log
            Requestlogger.CreateRequestlog("MobileWebserviceHandler.RequestPasswordReset", timestamp, emailAddress, hash);

            try
            {
                // Validate the request
                Obymobi.Logic.HelperClasses.WebserviceHelper.ValidateTimestamp(timestamp);
                string valueToHash = timestamp + emailAddress;
                string locallyCalculatedHash = Obymobi.Security.Hasher.GetHash(ObymobiConstants.MobileSalt, valueToHash);

                if (!locallyCalculatedHash.Equals(hash))
                {
                    // Corrupt call.
                    throw new ObymobiException(RequestPasswordResetResult.InvalidHash);
                }

                CustomerHelperWeb.ProcessResetPasswordRequest(emailAddress);
                result.ModelCollection.Add(true);
            }
            catch (Obymobi.ObymobiException wex)
            {
                if (wex.ErrorEnumValue is RequestPasswordResetResult)
                {
                    result.SetResult(wex);
                }
                else
                {
                    result.SetResult(GetCustomerResult.Failure);
                    result.SetInnerResult(wex);
                }

                Exceptionlogger.CreateExceptionlog(wex);
            }
            catch (Exception ex)
            {
                result.SetResult(RequestPasswordResetResult.Failure);
                result.SetInnerResult(GenericWebserviceCallResult.Failure, ex.Message);
                Exceptionlogger.CreateExceptionlog(ex);
            }

            Requestlogger.UpdateRequestLog(result);

            return result;
        }

        public ObyTypedMobileResult<Customer> ResetPassword(long timestamp, int customerId, string passwordResetIdentifier, string passwordTransferHash, string hash)
        {
            // Create the return value
            ObyTypedMobileResult<Customer> result = new ObyTypedMobileResult<Customer>();

            // Create a request log
            Requestlogger.CreateRequestlog("MobileWebserviceHandler.ResetPassword", timestamp, customerId, passwordResetIdentifier, hash);

            try
            {
                // Validate the request
                Obymobi.Logic.HelperClasses.WebserviceHelper.ValidateTimestamp(timestamp);
                string valueToHash = timestamp + customerId.ToString() + passwordResetIdentifier + passwordTransferHash;
                string locallyCalculatedHash = Obymobi.Security.Hasher.GetHash(passwordTransferHash + ObymobiConstants.MobileSalt, valueToHash);

                if (!locallyCalculatedHash.Equals(hash))
                {
                    // Corrupt call.
                    throw new ObymobiException(ResetPasswordResult.InvalidHash);
                }

                var customerEntity = Obymobi.Logic.HelperClasses.CustomerHelper.ResetPassword(customerId, passwordTransferHash, passwordResetIdentifier);

                Customer customer = new CustomerEntityConverter().ConvertEntityToMobileModel(customerEntity);
                result.ModelCollection.Add(customer);
            }
            catch (Obymobi.ObymobiException wex)
            {
                if (wex.ErrorEnumValue is ResetPasswordResult)
                {
                    result.SetResult(wex);
                }
                else
                {
                    result.SetResult(GetCustomerResult.Failure);
                    result.SetInnerResult(wex);
                }

                Exceptionlogger.CreateExceptionlog(wex);
            }
            catch (Exception ex)
            {
                result.SetResult(ResetPasswordResult.Failure);
                result.SetInnerResult(GenericWebserviceCallResult.Failure, ex.Message);
                Exceptionlogger.CreateExceptionlog(ex);
            }

            Requestlogger.UpdateRequestLog(result);

            return result;
        }

        public ObyTypedMobileResult<Customer> GetCustomer(long timestamp, string emailAddress, string passwordTransferHash, string anonymousEmail, string anonymousPasswordTransferHash, int socialmediaType, string externalId, string hash)
        {
            // Create the return value
            ObyTypedMobileResult<Customer> result = new ObyTypedMobileResult<Customer>();

            // Create a request log
            Requestlogger.CreateRequestlog("MobileWebserviceHandler.GetCustomer", timestamp, emailAddress, passwordTransferHash, anonymousEmail, anonymousPasswordTransferHash, socialmediaType, hash);

            try
            {
                // Validate the request
                Obymobi.Logic.HelperClasses.WebserviceHelper.ValidateTimestamp(timestamp);                
                string locallyCalculatedHash = Obymobi.Security.Hasher.GetHashFromParameters(passwordTransferHash + ObymobiConstants.MobileSalt, timestamp, emailAddress, this.OriginalAdditionalParameters);

                // Validate SocialMediaType
                SocialmediaType socialmediaTypeAsEnum;
                if (!EnumUtil.TryParse(socialmediaType, out socialmediaTypeAsEnum))
                    throw new ObymobiException(GetCustomerResult.InvalidSocialMediaType, "Invalid SocialMediaType: {0}", socialmediaType);

                if (!locallyCalculatedHash.Equals(hash))
                {
                    // Corrupt call.
                    throw new ObymobiException(GetCustomerResult.InvalidHash);
                }

                CustomerEntity customerEntity = null;
                if (socialmediaTypeAsEnum == SocialmediaType.Unknown)
                {
                    CustomerHelperWeb.AuthenticateCustomer(emailAddress, passwordTransferHash, out customerEntity);
                }
                else
                {
                    throw new ObymobiException(GetCustomerResult.InvalidSocialMediaType, "Invalid SocialMediaType: {0}. Refactored out: Obsolete.", socialmediaType);

                    // Refactored out: obsolete
                    //CustomerHelperWeb.AuthenticateCustomerBySocialmedia(emailAddress, passwordTransferHash, socialmediaTypeAsEnum, externalId, out customerEntity);
                }                    

                // Validate customer settings
                MobileWebserviceHandler.GetCustomerValidation(customerEntity);

                if (customerEntity != null &&
                    !anonymousEmail.IsNullOrWhiteSpace() && !anonymousPasswordTransferHash.IsNullOrWhiteSpace())
                {
                    // We have found an existing customer, so we can delete the anonymous customer
                    var anonCustomer = Obymobi.Logic.HelperClasses.CustomerHelper.GetAnonymousCustomerByEmail(anonymousEmail, anonymousPasswordTransferHash);
                    if (anonCustomer != null) 
                        anonCustomer.Delete();
                }

                // Create the customer model
                Customer customer = new CustomerEntityConverter().ConvertEntityToMobileModel(customerEntity);
                customer.SocialmediaType = socialmediaType;
                customer.ExternalId = externalId;

                result.ModelCollection.Add(customer);
                result.SetResult(GetCustomerResult.Success);
            }
            catch (Obymobi.ObymobiException wex)
            {
                if (wex.ErrorEnumValue is GetCustomerResult)
                {
                    result.SetResult(wex);
                }
                else if (wex.ErrorEnumValue is ValidateCredentialsResult)
                {
                    result.SetResult(GetCustomerResult.AuthenticationError, (ValidateCredentialsResult)wex.ErrorEnumValue);
                    result.SetInnerResult(wex);
                }
                else
                {
                    result.SetResult(GetCustomerResult.Failure);
                    result.SetInnerResult(wex);
                }

                Exceptionlogger.CreateExceptionlog(wex);
            }
            catch (Exception ex)
            {
                result.SetResult(GetCustomerResult.Failure);
                result.SetInnerResult(GenericWebserviceCallResult.Failure, ex.Message);
                Exceptionlogger.CreateExceptionlog(ex);
            }

            Requestlogger.UpdateRequestLog(result);

            return result;
        }

        #endregion        

        public ObyTypedMobileResult<Customer> GetCustomerWithSalt(long timestamp, string emailAddress, string hash)
        {
            // Create the return value
            ObyTypedMobileResult<Customer> result = new ObyTypedMobileResult<Customer>();

            // Create a request log
            Requestlogger.CreateRequestlog("MobileWebserviceHandler.GetCustomerWithSalt", timestamp, emailAddress, hash);

            try
            {
                // Validate the request
                CustomerEntity customerEntity = Obymobi.Logic.HelperClasses.WebserviceHelper.ValidateRequestRequireCustomer(timestamp, emailAddress, hash);

                // Validate Customer
                MobileWebserviceHandler.GetCustomerValidation(customerEntity);

                // Create the customer model
                Customer customer = new CustomerEntityConverter().ConvertEntityToMobileModel(customerEntity);
                result.ModelCollection.Add(customer);
                result.SetResult(GetCustomerResult.Success);
            }
            catch (Obymobi.ObymobiException wex)
            {
                if (wex.ErrorEnumValue is GetCustomerResult)
                {
                    result.SetResult(wex);
                }
                else if (wex.ErrorEnumValue is ValidateCredentialsResult)
                {
                    result.SetResult(GetCustomerResult.AuthenticationError, (ValidateCredentialsResult)wex.ErrorEnumValue);
                    result.SetInnerResult(wex);
                }
                else
                {
                    result.SetResult(GetCustomerResult.Failure);
                    result.SetInnerResult(wex);
                }

                Exceptionlogger.CreateExceptionlog(wex);
            }
            catch (Exception ex)
            {
                result.SetResult(GetCustomerResult.Failure);
                result.SetInnerResult(GenericWebserviceCallResult.Failure, ex.Message);
                Exceptionlogger.CreateExceptionlog(ex);
            }

            Requestlogger.UpdateRequestLog(result);

            return result;
        }

        public ObyTypedMobileResult<Customer> UpdateCustomerDetails(long timestamp, string emailAddress, string newEmailAddress, string phonenumber, string firstname, string lastname, string lastnamePrefix, string birthdateDateStamp, 
            int gender, string addressLine1, string addressLine2, string zipcode, string city, string countryCode, string passwordTransferHash, string newPasswordTransferHash, string hash)
        {
            // Create the return value
            ObyTypedMobileResult<Customer> result = new ObyTypedMobileResult<Customer>();

            // Create a request log
            Requestlogger.CreateRequestlog("MobileWebserviceHandler.UpdateCustomerDetails", timestamp, emailAddress, newEmailAddress, phonenumber, firstname, lastname, lastnamePrefix, birthdateDateStamp, gender, addressLine1, addressLine2, zipcode, city, countryCode, passwordTransferHash, newPasswordTransferHash);

            try
            {
                // Validate the request
                CustomerEntity customer = Obymobi.Logic.HelperClasses.WebserviceHelper.ValidateRequestRequireCustomer(timestamp, emailAddress, hash, newEmailAddress, phonenumber, firstname, lastname, lastnamePrefix, birthdateDateStamp, gender, addressLine1, addressLine2, zipcode, city, countryCode, passwordTransferHash, newPasswordTransferHash);

                if (!passwordTransferHash.Equals("[null]") && !newPasswordTransferHash.Equals("[null]"))
                {
                    // Authenticate the customer with the current password before changing its password
                    CustomerHelperWeb.AuthenticateCustomer(customer.Email, passwordTransferHash, out customer);

                    // Change the password
                    customer.Password = CustomerPasswordHelper.GetPasswordStorageHash(newPasswordTransferHash, customer.PasswordSalt);
                }                    
                
                if (!newEmailAddress.Equals("[null]"))
                    CustomerHelperWeb.ChangeEmail(customer, newEmailAddress);

                if (!phonenumber.Equals("[null]"))
                    customer.Phonenumber = phonenumber;

                if (!firstname.Equals("[null]"))
                    customer.Firstname= firstname;

                if (!lastname.Equals("[null]"))
                    customer.Lastname= lastname;

                if (!lastnamePrefix.Equals("[null]"))
                    customer.LastnamePrefix = lastnamePrefix;

                if (gender == -1)
                    customer.Gender = null;
                else 
                    customer.Gender = Convert.ToBoolean(gender);

                if (!addressLine1.Equals("[null]"))
                    customer.Addressline1 = addressLine1;

                if (!addressLine2.Equals("[null]"))
                    customer.Addressline2 = addressLine2;

                if (!zipcode.Equals("[null]"))
                    customer.Zipcode = zipcode;

                if (!city.Equals("[null]"))
                    customer.City = city;

                if (countryCode.Equals("[null]"))
                    customer.CountryCode = null;
                else
                    customer.CountryCode = countryCode;

                if (birthdateDateStamp.Equals("[null]"))
                {
                    customer.Birthdate = null;   
                }
                else
                {
                    DateTime birthday;
                    if (DateTime.TryParseExact(birthdateDateStamp, "yyyyMMdd", CultureInfo.InvariantCulture, DateTimeStyles.None, out birthday))
                    {
                        customer.Birthdate = birthday;
                    }
                }
 
                customer.Save();

                Customer customerModel = new CustomerEntityConverter().ConvertEntityToMobileModel(customer);
                result.ModelCollection.Add(customerModel);
                result.SetResult(GetCustomerResult.Success);
            }
            catch (Obymobi.ObymobiException wex)
            {
                ConvertToCreateCustomerResult(result, wex);

                Exceptionlogger.CreateExceptionlog(wex);
            }
            catch (Exception ex)
            {
                result.SetResult(CreateCustomerResult.Failure);
                result.SetInnerResult(GenericWebserviceCallResult.Failure, ex.Message);
                Exceptionlogger.CreateExceptionlog(ex);
            }

            Requestlogger.UpdateRequestLog(result);

            return result;
        }

        public ObyTypedMobileResult<Customer> LinkSocialmedia(long timestamp, string identifier, int socialmediaType, string emailAddress, string passwordTransferHash, string externalId, string hash)
        {
            // Create the return value
            ObyTypedMobileResult<Customer> result = new ObyTypedMobileResult<Customer>();

            // Create a request log
            Requestlogger.CreateRequestlog("MobileWebserviceHandler.LinkSocialmedia", timestamp, identifier, socialmediaType, emailAddress, externalId);

            try
            {
                SocialmediaType socialmediaTypeAsEnum;
                if (!EnumUtil.TryParse(socialmediaType, out socialmediaTypeAsEnum))
                    throw new ObymobiException(CreateCustomerResult.InvalidSocialMediaType, "Invalid SocialMediaType: {0}", socialmediaType);

                // Validate the request
                CustomerEntity customerEntity = Obymobi.Logic.HelperClasses.WebserviceHelper.ValidateRequestRequireCustomer(timestamp, identifier, hash, socialmediaType.ToString(), emailAddress, passwordTransferHash, externalId);

                //// Check if theres already a customerSocialmedia with the externalId
                //var customerSocialmedia = CustomerHelper.GetCustomerSocialmediaByExternalId(externalId);
                //if (customerSocialmedia != null)
                //{
                //    // Socialmedia account already used
                //    throw new ObymobiException(LinkSocialmediaResult.SocialmediaAccountAlreadyLinked, "{0}", customerSocialmedia.CustomerEntity.Email);
                //}
                //else
                //{
                //    // Check if another customer exists with the same email address
                //    //var filter = new PredicateExpression();
                //    //filter.Add(CustomerFields.Email % emailAddress);
                //    //filter.AddWithAnd(CustomerFields.CustomerId != customerEntity.CustomerId);
                //    //var customers = new CustomerCollection();
                //    //if (customers.GetDbCount(filter) > 0)
                //    //    throw new ObymobiException(LinkSocialmediaResult.EmailAlreadyUsedByCustomer, "{0}", emailAddress);

                //    // Link the socialmedia to customer
                //    customerEntity = CustomerHelperWeb.LinkSocialmediaToCustomer(customerEntity, socialmediaTypeAsEnum, emailAddress, passwordTransferHash, externalId);
                //}

                //// Add the result
                //Customer customer = new CustomerEntityConverter().ConvertEntityToMobileModel(customerEntity);

                //result.ModelCollection.Add(customer);
                //result.SetResult(LinkSocialmediaResult.Success);
            }
            catch (Obymobi.ObymobiException wex)
            {
                if (wex.ErrorEnumValue is LinkSocialmediaResult)
                {
                    result.SetResult(wex);
                }
                else if (wex.ErrorEnumValue is ValidateCredentialsResult)
                {
                    result.SetResult(GetCustomerResult.AuthenticationError, (ValidateCredentialsResult)wex.ErrorEnumValue);
                    result.SetInnerResult(wex);
                }
                else
                {
                    result.SetResult(LinkSocialmediaResult.Failure);
                    result.SetInnerResult(wex);
                }

                Exceptionlogger.CreateExceptionlog(wex);
            }
            catch (Exception ex)
            {
                result.SetResult(LinkSocialmediaResult.Failure);
                result.SetInnerResult(GenericWebserviceCallResult.Failure, ex.Message);
                Exceptionlogger.CreateExceptionlog(ex);
            }

            Requestlogger.UpdateRequestLog(result);

            return result;
        }

        public ObyTypedMobileResult<Customer> UnlinkSocialmedia(long timestamp, string emailAddress, string externalId, string hash)
        {
            // Create the return value
            ObyTypedMobileResult<Customer> result = new ObyTypedMobileResult<Customer>();

            // Create a request log
            Requestlogger.CreateRequestlog("MobileWebserviceHandler.UnlinkSocialmedia", timestamp, emailAddress, externalId);

            try
            {
                // Validate the request
                CustomerEntity customerEntity = Obymobi.Logic.HelperClasses.WebserviceHelper.ValidateRequestRequireCustomer(timestamp, emailAddress, hash, externalId);

                //// Check for a CustomerSocialmedia entity matching the externalId
                //var customerSocialmedia = customerEntity.CustomerSocialmediaCollection.SingleOrDefault(s => s.FieldValue1 == externalId);
                //if (customerSocialmedia == null)
                //{
                //    // No entity found
                //    throw new ObymobiException(UnlinkSocialmediaResult.CustomerSocialmediaNotFound, "ExternalId: {0}", externalId);
                //}
                //else
                //{
                //    // Remove the CustomerSocialmedia entity
                //    customerEntity.CustomerSocialmediaCollection.Remove(customerSocialmedia);
                //    customerSocialmedia.Delete();
                //}

                // Add the result
                Customer customer = new CustomerEntityConverter().ConvertEntityToMobileModel(customerEntity);

                result.ModelCollection.Add(customer);
                result.SetResult(UnlinkSocialmediaResult.Success);
            }
            catch (Obymobi.ObymobiException wex)
            {
                if (wex.ErrorEnumValue is UnlinkSocialmediaResult)
                {
                    result.SetResult(wex);
                }
                else if (wex.ErrorEnumValue is ValidateCredentialsResult)
                {
                    result.SetResult(GetCustomerResult.AuthenticationError, (ValidateCredentialsResult)wex.ErrorEnumValue);
                    result.SetInnerResult(wex);
                }
                else
                {
                    result.SetResult(UnlinkSocialmediaResult.Failure);
                    result.SetInnerResult(wex);
                }

                Exceptionlogger.CreateExceptionlog(wex);
            }
            catch (Exception ex)
            {
                result.SetResult(UnlinkSocialmediaResult.Failure);
                result.SetInnerResult(GenericWebserviceCallResult.Failure, ex.Message);
                Exceptionlogger.CreateExceptionlog(ex);
            }

            Requestlogger.UpdateRequestLog(result);

            return result;
        }

        private long RetrieveCompanyListTicks(List<int> companyIds, List<int> poiIds)
        {
            // Get the most recent Ticks for a Company
            long companiesTicks = Obymobi.Logic.HelperClasses.CompanyHelper.GetCompanyTicksForObymobi(companyIds, true, true, true);

            // Get the most recent Ticks for a Point of Interest
            long pointOfInterestTicks = PointOfInterestHelper.GetLastModifiedTicks(poiIds);

            // Get the ticks from the Configuration for changed items
            long configurationTicks = Dionysos.ConfigurationManager.GetDateTimeRefreshed(ObymobiDataConfigConstants.LastItemRemovedFromMobileCompanyList).Ticks;

            // Get Max
            long maxTicks = new[] { companiesTicks, pointOfInterestTicks, configurationTicks }.Max();

            return maxTicks;
        }

        public ObyTypedMobileResult<long> GetCompanyListTicks(long timestamp, string emailAddress, string accessCodeIds, string hash)
        {
            // Create the return value
            ObyTypedMobileResult<long> result = new ObyTypedMobileResult<long>();

            // Create a request log
            Requestlogger.CreateRequestlog("MobileWebserviceHandler.GetCompaniesTicks", timestamp, emailAddress, accessCodeIds, hash);

            try
            {
                // Validate the credentials
                CustomerEntity customerEntity = Obymobi.Logic.HelperClasses.WebserviceHelper.ValidateRequestRequireCustomer(timestamp, emailAddress, hash, accessCodeIds);

                //// Update the last successful request
                //this.UpdateLastRequest(terminalId, clientId);

                List<int> accessCodeIdList = new List<int>();
                AccessCodeType? accessCodeType = null;
                if (!accessCodeIds.IsNullOrWhiteSpace())
                    AccessCodeHelper.GetAccessCodeIdsAndTypeFromCommaSeparatedString(accessCodeIds, out accessCodeIdList, out accessCodeType);

                List<int> companyIds = Obymobi.Logic.HelperClasses.CompanyHelper.GetCompanyIdList(accessCodeIdList, accessCodeType);
                List<int> poiIds = PointOfInterestHelper.GetPointOfInterestIdList(accessCodeIdList, accessCodeType);

                long ticks = this.RetrieveCompanyListTicks(companyIds, poiIds);

                // Get the ticks from the access codes
                List<DateTime> lastModifiedList = new List<DateTime>();

                DateTime? lastModifiedFromAccessCodes = AccessCodeHelper.GetMaxLastModifiedFromCommaSeparatedString(accessCodeIds);
                if (lastModifiedFromAccessCodes.HasValue)
                    lastModifiedList.Add(lastModifiedFromAccessCodes.Value);

                DateTime lastModifiedFromConfig = ConfigurationManager.GetDateTimeRefreshed(ObymobiDataConfigConstants.LastItemRemovedFromAccessCodes);
                lastModifiedList.Add(lastModifiedFromConfig);

                if (lastModifiedList.Max().Ticks > ticks)
                    ticks = lastModifiedList.Max().Ticks;
                
                // Add to result
                result.ModelCollection.Add(ticks);
                result.SetResult(GetCompanyListTicksResult.Success);
            }
            catch (Obymobi.ObymobiException wex)
            {
                if (wex.ErrorEnumValue is GetCompanyListTicksResult)
                {
                    result.SetResult(wex);
                }
                else if (wex.ErrorEnumValue is ValidateCredentialsResult)
                {
                    result.SetResult(GetCompanyListTicksResult.AuthenticationError, (ValidateCredentialsResult)wex.ErrorEnumValue);
                    result.SetInnerResult(wex);
                }
                else
                {
                    result.SetResult(GetCompanyListTicksResult.Failure);
                    result.SetInnerResult(wex);
                }

                Exceptionlogger.CreateExceptionlog(wex);
            }
            catch (Exception ex)
            {
                result.SetResult(GetCompanyListTicksResult.Failure);
                result.SetInnerResult(GenericWebserviceCallResult.Failure, ex.Message);
                Exceptionlogger.CreateExceptionlog(ex);
            }

            Requestlogger.UpdateRequestLog(result);

            return result;
        }

        public ObyTypedMobileResult<CompanyList> GetCompanyList(long timestamp, string emailAddress, int deviceTypeInt, bool tablet, string accessCodeIds, string hash)
        {
            // Create the return value
            ObyTypedMobileResult<CompanyList> result = new ObyTypedMobileResult<CompanyList>();

            // Create a request log
            Requestlogger.CreateRequestlog("MobileWebserviceHandler.GetCompanies", timestamp, emailAddress, accessCodeIds, hash);

            try
            {
                // Validate the credentials
                DeviceEntity deviceEntity = Obymobi.Logic.HelperClasses.WebserviceHelper.ValidateRequest(timestamp, emailAddress, hash, deviceTypeInt, tablet, accessCodeIds);

                // Parse DeviceType
                DeviceType deviceType;
                if (!EnumUtil.TryParse(deviceTypeInt, out deviceType))
                    throw new ObymobiException(GetCompanyListResult.InvalidDeviceType, deviceTypeInt.ToString());

                List<int> accessCodeIdList = new List<int>();
                AccessCodeType? accessCodeType = null;
                if (!accessCodeIds.IsNullOrWhiteSpace())
                    AccessCodeHelper.GetAccessCodeIdsAndTypeFromCommaSeparatedString(accessCodeIds, out accessCodeIdList, out accessCodeType);

                // Get the ids of the companies and the POIs
                List<int> companyIds = Obymobi.Logic.HelperClasses.CompanyHelper.GetCompanyIdList(accessCodeIdList, accessCodeType);
                List<int> poiIds = PointOfInterestHelper.GetPointOfInterestIdList(accessCodeIdList, accessCodeType);

                // Get the ticks
                long ticks = this.RetrieveCompanyListTicks(companyIds, poiIds);

                // Get the ticks from the access codes
                List<DateTime> lastModifiedList = new List<DateTime>();

                DateTime? lastModifiedFromAccessCodes = AccessCodeHelper.GetMaxLastModifiedFromCommaSeparatedString(accessCodeIds);
                if (lastModifiedFromAccessCodes.HasValue)
                    lastModifiedList.Add(lastModifiedFromAccessCodes.Value);

                DateTime lastModifiedFromConfig = ConfigurationManager.GetDateTimeRefreshed(ObymobiDataConfigConstants.LastItemRemovedFromAccessCodes);
                lastModifiedList.Add(lastModifiedFromConfig);

                if (lastModifiedList.Max().Ticks > ticks)
                    ticks = lastModifiedList.Max().Ticks;

                // Create the cache params
                Obymobi.Logic.HelperClasses.CacheHelper.CacheParameters cacheParams = new Obymobi.Logic.HelperClasses.CacheHelper.CacheParameters("MobileService.GetCompanyList", ticks.ToString(), Dionysos.Global.ApplicationInfo.ApplicationVersion);
                cacheParams.Add(deviceType.ToString());
                cacheParams.Add(tablet);
                if (!accessCodeIds.IsNullOrWhiteSpace()) cacheParams.Add(accessCodeIds);

                CompanyList companyList = null;

                // Try to get the companies from the cache
                if (!Obymobi.Logic.HelperClasses.CacheHelper.TryGetValue(cacheParams, out companyList))
                {
                    // Get the companies
                    CompanyCollection companyCollection = CompanyHelper.GetCompanyCollectionForObymobi(companyIds);
                    Company[] companiesFromCompanies = new CompanyEntityConverter(true, deviceType, tablet).ConvertEntityCollectionToMobileModelArray(companyCollection);

                    // Get the points of interest
                    PointOfInterestCollection pointOfInterestCollection = PointOfInterestHelper.GetPointOfInterestCollection(poiIds);
                    Company[] companiesFromPointsOfInterests = new PointOfInterestEntityConverter(true, deviceType, tablet).ConvertEntityCollectionToMobileModelArray(pointOfInterestCollection);

                    // Combine the result of the companies and points of interest
                    Company[] companies = companiesFromCompanies.Concat(companiesFromPointsOfInterests).ToArray();
                    
                    Dictionary<int, bool> heroFlags = AccessCodeHelper.GetHeroFlags(accessCodeIdList);
                    foreach (Company company in companies)
                    {
                        company.IsHero = true;
                        if (heroFlags.ContainsKey(company.CompanyId))
                            company.IsHero = heroFlags[company.CompanyId];
                    }

                    // Set the combined Result
                    companyList = new CompanyList();
                    companyList.Companies = companies;
                    companyList.Ticks = ticks;

                    // Add the company list to the cache
                    Obymobi.Logic.HelperClasses.CacheHelper.CacheObject(cacheParams, companyList);
                }

                #region External content phase 2, automatically include items from feed in company list

                ////
                //// This is phase 2, for now we only import external items
                ////
                //if (ExternalContentHelper.IsExternalDatabaseAvailable())
                //{
                //    CacheHelper.CacheParameters cacheParams2 = new CacheHelper.CacheParameters("MobileService.GetCompanyList-ExternalRestaurants", ticks.ToString(), Dionysos.Global.ApplicationInfo.ApplicationVersion);

                //    Company[] companiesFromRestaurants = new Company[] {};

                //    // Try to get the external restaurants from the cache
                //    if (!CacheHelper.TryGetValue(cacheParams2, out companiesFromRestaurants))
                //    {
                //        // Get the restaurants from the external content
                //        RestaurantCollection restaurantCollection = ExternalContentHelper.GetRestaurantCollectionFromExternalContent(1000);
                //        if (restaurantCollection.Count > 0)
                //        {
                //            companiesFromRestaurants = new RestaurantEntityConverter(true, deviceType, tablet).ConvertEntityCollectionToMobileModelArray(restaurantCollection);
                //            //Company[] companiesFromRestaurants = new Company[] {new RestaurantEntityConverter(false, deviceType, tablet).ConvertEntityToMobileModel(restaurantCollection[0])};

                //            // Add the company list to the cache
                //            CacheHelper.CacheObject(cacheParams2, companiesFromRestaurants);
                //        }
                //    }

                //    if (companiesFromRestaurants.Length > 0)
                //        companyList.Companies = companyList.Companies.Concat(companiesFromRestaurants).ToArray();

                //    ///// Get the hotels from the external content
                //    //HotelCollection hotelCollection = ExternalContentHelper.GetHotelCollectionFromExternalContent();
                //    //if (hotelCollection.Count > 0)
                //    //{
                //    //    //Company[] companiesFromHotels = new HotelEntityConverter(false, deviceType, tablet).ConvertEntityCollectionToMobileModelArray(hotelCollection);
                //    //    Company[] companiesFromHotels = new Company[] { new HotelEntityConverter(false, deviceType, tablet).ConvertEntityToMobileModel(hotelCollection[0]) };
                //    //    companyList.Companies = companyList.Companies.Concat(companiesFromHotels).ToArray();
                //    //}
                //}

                #endregion

                result.ModelCollection.Add(companyList);
                result.SetResult(GetCompanyListResult.Success);

                // For testing purposes
                //if (true)
                //{
                //    // JSON
                //    using (StreamWriter streamWriter = new StreamWriter(HttpContext.Current.Server.MapPath("~/App_Data/json.json")))
                //    {
                //        Newtonsoft.Json.JsonSerializer serializer = new Newtonsoft.Json.JsonSerializer();
                //        serializer.Formatting = Newtonsoft.Json.Formatting.Indented;
                //        serializer.NullValueHandling = Newtonsoft.Json.NullValueHandling.Ignore;
                //        serializer.Serialize(streamWriter, companyList);
                //    }

                //    // XML
                //    File.WriteAllText(HttpContext.Current.Server.MapPath("~/App_Data/xml.xml"), Obymobi.Logic.HelperClasses.XmlHelper.Serialize(companyList, false));
                //}
                
            }
            catch (Obymobi.ObymobiException wex)
            {
                if (wex.ErrorEnumValue is GetCompanyListResult)
                {
                    result.SetResult(wex);
                }
                else if (wex.ErrorEnumValue is ValidateCredentialsResult)
                {
                    result.SetResult(GetCompanyListResult.AuthenticationError, (ValidateCredentialsResult)wex.ErrorEnumValue);
                    result.SetInnerResult(wex);
                }
                else
                {
                    result.SetResult(GetCompanyListResult.Failure);
                    result.SetInnerResult(wex);
                }

                Exceptionlogger.CreateExceptionlog(wex);
            }
            catch (Exception ex)
            {
                result.SetResult(GetCompanyListResult.Failure);
                result.SetInnerResult(GenericWebserviceCallResult.Failure, ex.Message);
                Exceptionlogger.CreateExceptionlog(ex);
            }

            Requestlogger.UpdateRequestLog(result);

            return result;
        }

        public ObyTypedMobileResult<long> GetCompanyTicks(long timestamp, string emailAddress, int companyId, string hash)
        {
            // Create the return value
            ObyTypedMobileResult<long> result = new ObyTypedMobileResult<long>();

            // Create a request log
            Requestlogger.CreateRequestlog("MobileWebserviceHandler.GetCompanyTicks", timestamp, emailAddress, companyId, hash);

            try
            {
                // Validate the credentials
                CustomerEntity customerEntity = Obymobi.Logic.HelperClasses.WebserviceHelper.ValidateRequestRequireCustomer(timestamp, emailAddress, hash, companyId);

                // Get the company
                long companiesTicks = Obymobi.Logic.HelperClasses.CompanyHelper.GetCompanyTicksForObymobi(companyId, true, false, false);
                result.ModelCollection.Add(companiesTicks);
                result.SetResult(GetCompanyTicksResult.Success);
            }
            catch (Obymobi.ObymobiException wex)
            {
                if (wex.ErrorEnumValue is GetCompanyTicksResult)
                {
                    result.SetResult(wex);
                }
                else if (wex.ErrorEnumValue is ValidateCredentialsResult)
                {
                    result.SetResult(GetCompanyTicksResult.AuthenticationError, (ValidateCredentialsResult)wex.ErrorEnumValue);
                    result.SetInnerResult(wex);
                }
                else
                {
                    result.SetResult(GetCompanyTicksResult.Failure);
                    result.SetInnerResult(wex);
                }

                Exceptionlogger.CreateExceptionlog(wex);
            }
            catch (Exception ex)
            {
                result.SetResult(GetCompanyTicksResult.Failure);
                result.SetInnerResult(GenericWebserviceCallResult.Failure, ex.Message);
                Exceptionlogger.CreateExceptionlog(ex);
            }

            Requestlogger.UpdateRequestLog(result);

            return result;
        }

        public ObyTypedMobileResult<Company> GetCompany(long timestamp, string emailAddress, int companyId, int deviceTypeInt, bool tablet, string hash)
        {
            // Create the return value
            ObyTypedMobileResult<Company> result = new ObyTypedMobileResult<Company>();

            // Create a request log
            Requestlogger.CreateRequestlog("MobileWebserviceHandler.GetCompany", timestamp, emailAddress, companyId, hash);

            try
            {
                // Validate the credentials
                DeviceEntity device = Obymobi.Logic.HelperClasses.WebserviceHelper.ValidateRequest(timestamp, emailAddress, hash, companyId, deviceTypeInt, tablet);

                // Parse DeviceType
                DeviceType deviceType;
                if (!EnumUtil.TryParse(deviceTypeInt, out deviceType))
                {
                    throw new ObymobiException(GetCompanyResult.InvalidDeviceType, deviceTypeInt.ToString());
                }

                CompanyEntity companyEntity = CompanyHelper.GetCompanyEntityByCompanyIdForMobileWebservice(companyId);

                // Create the company model
                Company company = new CompanyEntityConverter(false, deviceType, tablet).ConvertEntityToMobileModel(companyEntity);

                result.ModelCollection.Add(company);
                result.SetResult(GetCompanyResult.Success);
            }
            catch (Obymobi.ObymobiException wex)
            {
                if (wex.ErrorEnumValue is GetCompanyResult)
                {
                    result.SetResult(wex);
                }
                else if (wex.ErrorEnumValue is ValidateCredentialsResult)
                {
                    result.SetResult(GetCompanyResult.AuthenticationError, (ValidateCredentialsResult)wex.ErrorEnumValue);
                    result.SetInnerResult(wex);
                }
                else
                {
                    result.SetResult(GetCompanyResult.Failure);
                    result.SetInnerResult(wex);
                }

                Exceptionlogger.CreateExceptionlog(wex);
            }
            catch (Exception ex)
            {
                result.SetResult(GetCompanyResult.Failure);
                result.SetInnerResult(GenericWebserviceCallResult.Failure, ex.Message);
                Exceptionlogger.CreateExceptionlog(ex);
            }

            Requestlogger.UpdateRequestLog(result);

            return result;
        }

        public ObyTypedMobileResult<long> GetPointOfInterestTicks(long timestamp, string emailAddress, int pointOfInterestId, string hash)
        {
            // Create the return value
            ObyTypedMobileResult<long> result = new ObyTypedMobileResult<long>();

            // Create a request log
            Requestlogger.CreateRequestlog("MobileWebserviceHandler.GetPointOfInterestTicks", timestamp, emailAddress, pointOfInterestId, hash);

            try
            {
                // Validate the credentials
                CustomerEntity customerEntity = Obymobi.Logic.HelperClasses.WebserviceHelper.ValidateRequestRequireCustomer(timestamp, emailAddress, hash, pointOfInterestId);

                // Get the Point of Interest              
                IncludeFieldsList fields = new IncludeFieldsList();
                fields.Add(PointOfInterestFields.LastModifiedUTC);
                var poiEntity = new PointOfInterestEntity(pointOfInterestId);
                if (poiEntity.IsNew)
                    throw new ObymobiException(GetPointOfInterestTicksResult.UnknownPointOfInterestId, "Id: {0}", pointOfInterestId);
                
                result.ModelCollection.Add(poiEntity.LastModifiedUTC.Ticks);
                result.SetResult(GetCompanyTicksResult.Success);
            }
            catch (Obymobi.ObymobiException wex)
            {
                if (wex.ErrorEnumValue is GetPointOfInterestTicksResult)
                {
                    result.SetResult(wex);
                }
                else if (wex.ErrorEnumValue is ValidateCredentialsResult)
                {
                    result.SetResult(GetPointOfInterestTicksResult.AuthenticationError, (ValidateCredentialsResult)wex.ErrorEnumValue);
                    result.SetInnerResult(wex);
                }
                else
                {
                    result.SetResult(GetPointOfInterestTicksResult.Failure);
                    result.SetInnerResult(wex);
                }

                Exceptionlogger.CreateExceptionlog(wex);
            }
            catch (Exception ex)
            {
                result.SetResult(GetPointOfInterestTicksResult.Failure);
                result.SetInnerResult(GenericWebserviceCallResult.Failure, ex.Message);
                Exceptionlogger.CreateExceptionlog(ex);
            }

            Requestlogger.UpdateRequestLog(result);

            return result;
        }

        public ObyTypedMobileResult<Company> GetPointOfInterest(long timestamp, string emailAddress, int pointOfInterestId, int deviceTypeInt, bool tablet, string hash)
        {
            // Create the return value
            ObyTypedMobileResult<Company> result = new ObyTypedMobileResult<Company>();

            // Create a request log
            Requestlogger.CreateRequestlog("MobileWebserviceHandler.GetPointOfInterest", timestamp, emailAddress, pointOfInterestId, hash);

            try
            {
                // Validate the credentials
                DeviceEntity device = Obymobi.Logic.HelperClasses.WebserviceHelper.ValidateRequest(timestamp, emailAddress, hash, pointOfInterestId, deviceTypeInt, tablet);

                // Parse DeviceType
                DeviceType deviceType;
                if (!EnumUtil.TryParse(deviceTypeInt, out deviceType))
                {
                    throw new ObymobiException(GetPointOfInterestResult.InvalidDeviceType, deviceTypeInt.ToString());
                }

                var poiEntity = new PointOfInterestEntity(pointOfInterestId, PointOfInterestHelper.GetPrefetchPathForMobileWebservice());
                if (poiEntity.IsNew)
                {
                    throw new ObymobiException(GetPointOfInterestResult.PointOfInterestIdUnknown, "Id: {0}", poiEntity.PointOfInterestId);
                }

                // Create the Company model
                Company company = new PointOfInterestEntityConverter(false, deviceType, tablet).ConvertEntityToMobileModel(poiEntity);

                result.ModelCollection.Add(company);
                result.SetResult(GetPointOfInterestResult.Success);
            }
            catch (Obymobi.ObymobiException wex)
            {
                if (wex.ErrorEnumValue is GetPointOfInterestResult)
                {
                    result.SetResult(wex);
                }
                else if (wex.ErrorEnumValue is ValidateCredentialsResult)
                {
                    result.SetResult(GetPointOfInterestResult.AuthenticationError, (ValidateCredentialsResult)wex.ErrorEnumValue);
                    result.SetInnerResult(wex);
                }
                else
                {
                    result.SetResult(GetPointOfInterestResult.Failure);
                    result.SetInnerResult(wex);
                }

                Exceptionlogger.CreateExceptionlog(wex);
            }
            catch (Exception ex)
            {
                result.SetResult(GetPointOfInterestResult.Failure);
                result.SetInnerResult(GenericWebserviceCallResult.Failure, ex.Message);
                Exceptionlogger.CreateExceptionlog(ex);
            }

            Requestlogger.UpdateRequestLog(result);

            return result;
        }

        public ObyTypedMobileResult<long> GetMenuTicks(long timestamp, string emailAddress, int companyId, string hash)
        {
            // Create the return value
            ObyTypedMobileResult<long> result = new ObyTypedMobileResult<long>();

            // Create a request log
            Requestlogger.CreateRequestlog("MobileWebserviceHandler.GetMenuTicks", timestamp, emailAddress, companyId, hash);

            try
            {
                // Validate the credentials
                DeviceEntity deviceEntity = Obymobi.Logic.HelperClasses.WebserviceHelper.ValidateRequest(timestamp, emailAddress, hash, companyId);                                

                // Get the company
                long companiesTicks = Obymobi.Logic.HelperClasses.CompanyHelper.GetCompanyTicksForObymobi(companyId, false, true, false);
                result.ModelCollection.Add(companiesTicks);
                result.SetResult(GetMenuTicksResult.Success);
            }
            catch (Obymobi.ObymobiException wex)
            {
                if (wex.ErrorEnumValue is GetMenuTicksResult)
                {
                    result.SetResult(wex);
                }
                else if (wex.ErrorEnumValue is ValidateCredentialsResult)
                {
                    result.SetResult(GetMenuTicksResult.AuthenticationError, (ValidateCredentialsResult)wex.ErrorEnumValue);
                    result.SetInnerResult(wex);
                }
                else
                {
                    result.SetResult(GetMenuTicksResult.Failure);
                    result.SetInnerResult(wex);
                }

                Exceptionlogger.CreateExceptionlog(wex);
            }
            catch (Exception ex)
            {
                result.SetResult(GetMenuTicksResult.Failure);
                result.SetInnerResult(GenericWebserviceCallResult.Failure, ex.Message);
                Exceptionlogger.CreateExceptionlog(ex);
            }

            Requestlogger.UpdateRequestLog(result);

            return result;
        }

        public ObyTypedMobileResult<Menu> GetMenu(long timestamp, string emailAddress, int menuId, int deviceTypeInt, string hash)
        {
            // Create the return value
            ObyTypedMobileResult<Menu> result = new ObyTypedMobileResult<Menu>();

            // Create a request log
            Requestlogger.CreateRequestlog("MobileWebserviceHandler.GetMenu", timestamp, emailAddress, menuId, hash);

            try
            {
                // Validate the request
                //DeviceEntity deviceEntity = WebserviceHelper.ValidateRequest(timestamp, emailAddress, hash, menuId, deviceTypeInt);

                // Parse DeviceType
                DeviceType deviceType;
                if (!EnumUtil.TryParse(deviceTypeInt, out deviceType))
                    throw new ObymobiException(GetMenuResult.InvalidDeviceType, deviceTypeInt.ToString());

                // Get the company
                CompanyEntity companyEntity = EntityCollection.GetEntity<CompanyCollection, CompanyEntity>(MenuFields.MenuId == menuId, CompanyEntityBase.Relations.MenuEntityUsingCompanyId, CompanyFields.CompanyId);

                // Get the ticks
                long ticks = Obymobi.Logic.HelperClasses.CompanyHelper.GetCompanyTicksForObymobi(companyEntity.CompanyId, false, true, false);

                // Create the cache params
                Obymobi.Logic.HelperClasses.CacheHelper.CacheParameters cacheParams = new Obymobi.Logic.HelperClasses.CacheHelper.CacheParameters("MobileService.GetMenu", ticks.ToString(), Dionysos.Global.ApplicationInfo.ApplicationVersion);
                cacheParams.Add(deviceType.ToString());
                cacheParams.Add(menuId);

                Menu menu = null;

                // Try to get the menu from the cache
                if (!Obymobi.Logic.HelperClasses.CacheHelper.TryGetValue(cacheParams, out menu))
                {
                    // Get the menu for the company of the specified company id
                    // PILS Optimize for speed!! Prefetch and that kind of shit
                    MenuEntity menuEntity = MenuHelper.GetMenu(menuId);

                    // Create the menu model
                    menu = new MenuEntityConverter(deviceType).ConvertEntityToMobileModel(menuEntity);
                    menu.Ticks = ticks;

                    // Add the menu to the cache
                    Obymobi.Logic.HelperClasses.CacheHelper.CacheObject(cacheParams, menu);
                }

                result.ModelCollection.Add(menu);
                result.SetResult(GetMenuResult.Success);
            }
            catch (Obymobi.ObymobiException wex)
            {
                if (wex.ErrorEnumValue is GetMenuResult)
                {
                    result.SetResult(wex);
                }
                else if (wex.ErrorEnumValue is ValidateCredentialsResult)
                {
                    result.SetResult(GetMenuResult.AuthenticationError, (ValidateCredentialsResult)wex.ErrorEnumValue);
                    result.SetInnerResult(wex);
                }
                else
                {
                    result.SetResult(GetMenuResult.Failure);
                    result.SetInnerResult(wex);
                }

                Exceptionlogger.CreateExceptionlog(wex);
            }
            catch (Exception ex)
            {
                result.SetResult(GetMenuResult.Failure);
                result.SetInnerResult(GenericWebserviceCallResult.Failure, ex.Message);
                Exceptionlogger.CreateExceptionlog(ex);
            }

            Requestlogger.UpdateRequestLog(result);

            return result;
        }

        public ObyTypedMobileResult<long> GetSiteTicks(long timestamp, string emailAddress, int siteId, string hash)
        {
            // Create the return value
            ObyTypedMobileResult<long> result = new ObyTypedMobileResult<long>();

            // Create a request log
            Requestlogger.CreateRequestlog("MobileWebserviceHandler.GetSiteTicks", timestamp, emailAddress, siteId, hash);

            try
            {
                // Validate the credentials
                CustomerEntity customerEntity = Obymobi.Logic.HelperClasses.WebserviceHelper.ValidateRequestRequireCustomer(timestamp, emailAddress, hash, siteId);

                // Get the Point of Interest              
                IncludeFieldsList fields = new IncludeFieldsList();
                fields.Add(SiteFields.LastModifiedUTC);
                var siteEntity = new SiteEntity(siteId);
                if (siteEntity.IsNew)
                    throw new ObymobiException(GetSiteTicksResult.UnknownSiteId, "Id: {0}", siteId);

                result.ModelCollection.Add(siteEntity.LastModifiedUTC.Ticks);
                result.SetResult(GetSiteTicksResult.Success);
            }
            catch (Obymobi.ObymobiException wex)
            {
                if (wex.ErrorEnumValue is GetSiteTicksResult)
                {
                    result.SetResult(wex);
                }
                else if (wex.ErrorEnumValue is ValidateCredentialsResult)
                {
                    result.SetResult(GetSiteTicksResult.AuthenticationError, (ValidateCredentialsResult)wex.ErrorEnumValue);
                    result.SetInnerResult(wex);
                }
                else
                {
                    result.SetResult(GetSiteTicksResult.Failure);
                    result.SetInnerResult(wex);
                }

                Exceptionlogger.CreateExceptionlog(wex);
            }
            catch (Exception ex)
            {
                result.SetResult(GetSiteTicksResult.Failure);
                result.SetInnerResult(GenericWebserviceCallResult.Failure, ex.Message);
                Exceptionlogger.CreateExceptionlog(ex);
            }

            Requestlogger.UpdateRequestLog(result);

            return result;
        }        

        public ObyTypedMobileResult<Site> GetSite(long timestamp, string emailAddress, int siteId, string languageCode, int deviceTypeInt, bool tablet, string cultureCode, string hash)
        {
            // Create the return value
            ObyTypedMobileResult<Site> result = new ObyTypedMobileResult<Site>();

            // Create a request log
            Requestlogger.CreateRequestlog("MobileWebserviceHandler.GetSite", timestamp, emailAddress, siteId, languageCode, hash);

            try
            {
                // Validate the credentials
                DeviceEntity device = Obymobi.Logic.HelperClasses.WebserviceHelper.ValidateRequest(timestamp, emailAddress, hash, siteId, languageCode, deviceTypeInt, tablet, cultureCode);

                // Parse DeviceType
                DeviceType deviceType;
                if (!EnumUtil.TryParse(deviceTypeInt, out deviceType))
                {
                    throw new ObymobiException(GetCompanyResult.InvalidDeviceType, deviceTypeInt.ToString());
                }

                Site siteModel = null;
                int companyId = 0;

                long ticks = SiteHelper.GetSiteTicks(siteId);

                var siteEntity = new SiteEntity(siteId);
                if (siteEntity.IsNew)
                    throw new ObymobiException(GetSiteResult.InvalidSiteId, "Id: {0}", siteId);

                if (siteEntity.CompanyId.HasValue)
                    companyId = siteEntity.CompanyId.Value;

                // Create the cache parameters
                Obymobi.Logic.HelperClasses.CacheHelper.CacheParameters cacheParams = new Obymobi.Logic.HelperClasses.CacheHelper.CacheParameters("MobileWebserviceHandler.GetSite", ticks.ToString(), Dionysos.Global.ApplicationInfo.ApplicationVersion);
                cacheParams.Add(companyId);
                cacheParams.Add(siteId);
                cacheParams.Add(languageCode);
                cacheParams.Add(deviceType);
                cacheParams.Add(tablet);
                cacheParams.Add(cultureCode);

                if (!Obymobi.Logic.HelperClasses.CacheHelper.TryGetValue(cacheParams, out siteModel))
                {
                    if (cultureCode.IsNullOrWhiteSpace())
                    {
                        cultureCode = Obymobi.Logic.HelperClasses.CompanyCultureHelper.GetCultureByCompanyLanguageCode(companyId, languageCode).Code;
                    }

                    // Get the Site
                    siteEntity = SiteHelper.GetTranslatedSite(siteId, cultureCode, deviceType, tablet);

                    // Create the company model
                    siteModel = new SiteEntityConverter(deviceType).ConvertEntityToMobileModel(siteEntity);

                    Obymobi.Logic.HelperClasses.CacheHelper.CacheObject(cacheParams, siteModel);
                }

                result.ModelCollection.Add(siteModel);
                result.SetResult(GetCompanyResult.Success);
            }
            catch (Obymobi.ObymobiException wex)
            {
                if (wex.ErrorEnumValue is SiteHelper.SiteHelperResult)
                {
                    switch ((SiteHelper.SiteHelperResult)wex.ErrorEnumValue)
                    {
                        case SiteHelper.SiteHelperResult.NoSiteIsExistingForSiteId:
                            result.SetResult(GetSiteResult.InvalidSiteId);
                            break;
                        case SiteHelper.SiteHelperResult.LanguageCodeInvalid:
                            result.SetResult(GetSiteResult.InvalidLanguageCode);
                            break;
                        default:
                            result.SetResult(GetSiteResult.Failure);
                            break;
                    }

                }
                else if (wex.ErrorEnumValue is ValidateCredentialsResult)
                {
                    result.SetResult(GetSiteResult.AuthenticationError, (ValidateCredentialsResult)wex.ErrorEnumValue);
                    result.SetInnerResult(wex);
                }
                else
                {
                    result.SetResult(GetSiteResult.Failure);
                    result.SetInnerResult(wex);
                }

                Exceptionlogger.CreateExceptionlog(wex);
            }
            catch (Exception ex)
            {
                result.SetResult(GetSiteResult.Failure);
                result.SetInnerResult(GenericWebserviceCallResult.Failure, ex.Message);
                Exceptionlogger.CreateExceptionlog(ex);
            }

            Requestlogger.UpdateRequestLog(result);

            return result;
        }

        public ObyTypedMobileResult<Order> GetOrderHistory(long timestamp, string emailAddress, string hash)
        {
            // Create the return value
            ObyTypedMobileResult<Order> result = new ObyTypedMobileResult<Order>();

            // Create a request log
            Requestlogger.CreateRequestlog("MobileWebserviceHandler.GetOrderHistory", timestamp, emailAddress, hash);

            try
            {
                // Validate the company credentials
                CustomerEntity customer = Obymobi.Logic.HelperClasses.WebserviceHelper.ValidateRequestRequireCustomer(timestamp, emailAddress, hash);

                //// Initialize an empty order array
                //Order[] orders = null;

                //// Get the order history for the specified terminal 
                //orders = OrderHelper.GetOrderHistory(terminal.TerminalId);

                //// Add the result
                //result.ModelCollection.AddRange(orders);
                result.SetResult(GetOrderHistoryResult.Success);
            }
            catch (Obymobi.ObymobiException wex)
            {
                if (wex.ErrorEnumValue is GetOrderHistoryResult)
                {
                    result.SetResult(wex);
                }
                else if (wex.ErrorEnumValue is ValidateCredentialsResult)
                {
                    result.SetResult(GetOrderHistoryResult.AuthenticationError, (ValidateCredentialsResult)wex.ErrorEnumValue);
                    result.SetInnerResult(wex);
                }
                else
                {
                    result.SetResult(GetOrderHistoryResult.Failure);
                    result.SetInnerResult(wex);
                }

                Exceptionlogger.CreateExceptionlog(wex);
            }
            catch (Exception ex)
            {
                result.SetResult(GetOrderHistoryResult.Failure);
                result.SetInnerResult(GenericWebserviceCallResult.Failure, ex.Message);
                Exceptionlogger.CreateExceptionlog(ex);
            }

            Requestlogger.UpdateRequestLog(result);

            return result;
        }

        public ObyTypedMobileResult<long> GetCommonDataTicks(long timestamp, string emailAddress, string hash)
        {
            // Create the return value
            ObyTypedMobileResult<long> result = new ObyTypedMobileResult<long>();

            // Create a request log
            Requestlogger.CreateRequestlog("MobileWebserviceHandler.GetCommonDataTicks", timestamp, emailAddress, hash);

            try
            {
                // Validate the credentials
                CustomerEntity customerEntity = Obymobi.Logic.HelperClasses.WebserviceHelper.ValidateRequestRequireCustomer(timestamp, emailAddress, hash);

                // Determine the ticks
                result.ModelCollection.Add(CommonDataHelper.GetLastModifiedTicks());
                result.SetResult(GetCommonDataTicksResult.Success);
            }
            catch (Obymobi.ObymobiException wex)
            {
                if (wex.ErrorEnumValue is GetCommonDataTicksResult)
                {
                    result.SetResult(wex);
                }
                else if (wex.ErrorEnumValue is ValidateCredentialsResult)
                {
                    result.SetResult(GetCommonDataTicksResult.AuthenticationError, (ValidateCredentialsResult)wex.ErrorEnumValue);
                    result.SetInnerResult(wex);
                }
                else
                {
                    result.SetResult(GetCommonDataTicksResult.Failure);
                    result.SetInnerResult(wex);
                }

                Exceptionlogger.CreateExceptionlog(wex);
            }
            catch (Exception ex)
            {
                result.SetResult(GetCommonDataTicksResult.Failure);
                result.SetInnerResult(GenericWebserviceCallResult.Failure, ex.Message);
                Exceptionlogger.CreateExceptionlog(ex);
            }

            Requestlogger.UpdateRequestLog(result);

            return result;
        }

        public ObyTypedMobileResult<CommonData> GetCommonData(long timestamp, string emailAddress, string hash)
        {
            // Create the return value
            ObyTypedMobileResult<CommonData> result = new ObyTypedMobileResult<CommonData>();

            // Create a request log
            Requestlogger.CreateRequestlog("MobileWebserviceHandler.GetCommonData", timestamp, emailAddress, hash);

            try
            {
                // Validate the company credentials
                CustomerEntity customer = Obymobi.Logic.HelperClasses.WebserviceHelper.ValidateRequestRequireCustomer(timestamp, emailAddress, hash);

                // Get the ticks
                long ticks = CommonDataHelper.GetLastModifiedTicks();

                // Create the cache params
                Obymobi.Logic.HelperClasses.CacheHelper.CacheParameters cacheParams = new Obymobi.Logic.HelperClasses.CacheHelper.CacheParameters("MobileService.GetCommonData", ticks.ToString(), Dionysos.Global.ApplicationInfo.ApplicationVersion);

                // Create the model
                CommonData commonData = null;

                if (!Obymobi.Logic.HelperClasses.CacheHelper.TryGetValue(cacheParams, out commonData))
                {
                    commonData = new CommonData();
                    commonData.LastModifiedTicks = ticks;
                    commonData.VenueCategories = new VenueCategoryEntityConverter().ConvertEntityCollectionToMobileModelArray(CommonDataHelper.GetVenueCategories());
                    commonData.Amenities = new AmenityEntityConverter().ConvertEntityCollectionToMobileModelArray(CommonDataHelper.GetAmenities());
                    commonData.ActionButtons = new ActionButtonEntityConverter().ConvertEntityCollectionToMobileModelArray(CommonDataHelper.GetActionButtons());

                    // Add the common data to the cache
                    Obymobi.Logic.HelperClasses.CacheHelper.CacheObject(cacheParams, commonData);
                }

                // Add the result
                result.ModelCollection.Add(commonData);
                result.SetResult(GetCommonDataResult.Success);
            }
            catch (Obymobi.ObymobiException wex)
            {
                if (wex.ErrorEnumValue is GetCommonDataResult)
                {
                    result.SetResult(wex);
                }
                else if (wex.ErrorEnumValue is ValidateCredentialsResult)
                {
                    result.SetResult(GetCommonDataResult.AuthenticationError, (ValidateCredentialsResult)wex.ErrorEnumValue);
                    result.SetInnerResult(wex);
                }
                else
                {
                    result.SetResult(GetCommonDataResult.Failure);
                    result.SetInnerResult(wex);
                }

                Exceptionlogger.CreateExceptionlog(wex);
            }
            catch (Exception ex)
            {
                result.SetResult(GetCommonDataResult.Failure);
                result.SetInnerResult(GenericWebserviceCallResult.Failure, ex.Message);
                Exceptionlogger.CreateExceptionlog(ex);
            }

            Requestlogger.UpdateRequestLog(result);

            return result;
        }

        public ObyTypedMobileResult<long> GetAccessCodesTicks(long timestamp, string emailAddress, string accessCodeIds, string hash)
        {
            // Create the return value
            ObyTypedMobileResult<long> result = new ObyTypedMobileResult<long>();

            // Create a request log
            Requestlogger.CreateRequestlog("MobileWebserviceHandler.GetAccessCodesTicks", timestamp, emailAddress, accessCodeIds, hash);

            try
            {
                // Validate the credentials
                CustomerEntity customerEntity = Obymobi.Logic.HelperClasses.WebserviceHelper.ValidateRequestRequireCustomer(timestamp, emailAddress, hash, accessCodeIds);

                // Determine the ticks
                List<DateTime> lastModifiedList = new List<DateTime>();

                DateTime? lastModifiedFromAccessCodes = AccessCodeHelper.GetMaxLastModifiedFromCommaSeparatedString(accessCodeIds);
                if (lastModifiedFromAccessCodes.HasValue)
                    lastModifiedList.Add(lastModifiedFromAccessCodes.Value);

                DateTime lastModifiedFromConfig = ConfigurationManager.GetDateTimeRefreshed(ObymobiDataConfigConstants.LastItemRemovedFromAccessCodes);
                lastModifiedList.Add(lastModifiedFromConfig);

                result.ModelCollection.Add(lastModifiedList.Max().Ticks);
                result.SetResult(GetAccessCodesTicksResult.Success);
            }
            catch (Obymobi.ObymobiException wex)
            {
                if (wex.ErrorEnumValue is GetAccessCodesTicksResult)
                {
                    result.SetResult(wex);
                }
                else if (wex.ErrorEnumValue is ValidateCredentialsResult)
                {
                    result.SetResult(GetAccessCodesTicksResult.AuthenticationError, (ValidateCredentialsResult)wex.ErrorEnumValue);
                    result.SetInnerResult(wex);
                }
                else
                {
                    result.SetResult(GetAccessCodesTicksResult.Failure);
                    result.SetInnerResult(wex);
                }

                Exceptionlogger.CreateExceptionlog(wex);
            }
            catch (Exception ex)
            {
                result.SetResult(GetAccessCodesTicksResult.Failure);
                result.SetInnerResult(GenericWebserviceCallResult.Failure, ex.Message);
                Exceptionlogger.CreateExceptionlog(ex);
            }

            Requestlogger.UpdateRequestLog(result);

            return result;
        }

        public ObyTypedMobileResult<AccessCode> GetAccessCodes(long timestamp, string emailAddress, string accessCodeIds, string accessCode, string hash)
        {
            // Create the return value
            ObyTypedMobileResult<AccessCode> result = new ObyTypedMobileResult<AccessCode>();

            // Create a request log
            Requestlogger.CreateRequestlog("MobileWebserviceHandler.GetAccessCodes", timestamp, emailAddress, accessCodeIds, accessCode, hash);

            try
            {
                // Validate the company credentials
                CustomerEntity customer = Obymobi.Logic.HelperClasses.WebserviceHelper.ValidateRequestRequireCustomer(timestamp, emailAddress, hash, accessCodeIds, accessCode);

                AccessCodeCollection existingAccessCodeCollection = AccessCodeHelper.GetAccessCodeCollectionFromCommaSeparatedString(accessCodeIds);
                result.ModelCollection.AddRange(new AccessCodeEntityConverter().ConvertEntityCollectionToMobileModelArray(existingAccessCodeCollection).ToList());

                // Get the access code using the specified access code
                if (!accessCode.IsNullOrWhiteSpace())
                {
                    AccessCodeCollection accessCodeCollection = AccessCodeHelper.GetAccessCodeCollectionUsingCode(accessCode);

                    if (accessCodeCollection.Count == 0)
                        throw new ObymobiException(GetAccessCodesResult.NoAccessCodeFound);
                    else if (accessCodeCollection.Count > 1)
                        throw new ObymobiException(GetAccessCodesResult.MultipleAccessCodesFound);
                    else
                    {
                        // We found the access code
                        AccessCodeEntity accessCodeEntity = accessCodeCollection[0];

                        if (existingAccessCodeCollection.Count > 0 && existingAccessCodeCollection.Any(x => x.Code.Equals(accessCodeEntity.Code, StringComparison.InvariantCultureIgnoreCase)))
                            throw new ObymobiException(GetAccessCodesResult.AccessCodeAlreadyAdded); // Access code was already added

                        #region Access code type validation (deprecated)



                        /*
                        // Verify that the access codes are compatible
                        if (existingAccessCodeCollection.Count == 0)
                        {
                            // Type of access code doesnt matter, access code can be added anyway
                        }
                        else if (existingAccessCodeCollection.Count == 1)
                        {
                            if (existingAccessCodeCollection[0].TypeAsEnum == AccessCodeType.ShowOnly)
                                throw new ObymobiException(GetAccessCodesResult.AccessCodesIncompatible); // Cannot add another access code if there is already a ShowOnly present
                            else if (existingAccessCodeCollection[0].TypeAsEnum == AccessCodeType.Show && accessCodeEntity.TypeAsEnum == AccessCodeType.ShowOnly) 
                                throw new ObymobiException(GetAccessCodesResult.AccessCodesIncompatible); // Cannot add another ShowOnly access code if there is already a Show present
                            else if (existingAccessCodeCollection[0].Code.Equals(accessCodeEntity.Code, StringComparison.InvariantCultureIgnoreCase))
                                throw new ObymobiException(GetAccessCodesResult.AccessCodeAlreadyAdded); // Access code was already added
                        }
                        else // Multiple existing access codes (All have to be of type Show)
                        {
                            if (accessCodeEntity.TypeAsEnum == AccessCodeType.ShowOnly) // Cannot add ShowOnly
                                throw new ObymobiException(GetAccessCodesResult.AccessCodesIncompatible);
                            else if (existingAccessCodeCollection.Any(x => x.Code.Equals(accessCodeEntity.Code, StringComparison.InvariantCultureIgnoreCase)))
                                throw new ObymobiException(GetAccessCodesResult.AccessCodeAlreadyAdded); // Access code was already added
                        }
                         */
                        #endregion

                        result.ModelCollection.Add(new AccessCodeEntityConverter().ConvertEntityToMobileModel(accessCodeEntity));
                    }
                }

                // Sort the access codes
                if (result.ModelCollection != null && result.ModelCollection.Count > 0)
                    result.ModelCollection = result.ModelCollection.OrderBy(x => x.Code).ToList();

                // Set the result
                result.SetResult(GetAccessCodesResult.Success);
            }
            catch (Obymobi.ObymobiException wex)
            {
                if (wex.ErrorEnumValue is GetAccessCodesResult)
                {
                    result.SetResult(wex);
                }
                else if (wex.ErrorEnumValue is ValidateCredentialsResult)
                {
                    result.SetResult(GetAccessCodesResult.AuthenticationError, (ValidateCredentialsResult)wex.ErrorEnumValue);
                    result.SetInnerResult(wex);
                }
                else
                {
                    result.SetResult(GetAccessCodesResult.Failure);
                    result.SetInnerResult(wex);
                }

                Exceptionlogger.CreateExceptionlog(wex);
            }
            catch (Exception ex)
            {
                result.SetResult(GetAccessCodesResult.Failure);
                result.SetInnerResult(GenericWebserviceCallResult.Failure, ex.Message);
                Exceptionlogger.CreateExceptionlog(ex);
            }

            Requestlogger.UpdateRequestLog(result);

            return result;
        }

        #endregion

        #region PUT methods

        public ObyTypedMobileResult<Order> SaveOrder(long timestamp, string emailAddress, Order order, string hash)
        {
            // Create the return value
            ObyTypedMobileResult<Order> result = new ObyTypedMobileResult<Order>();

            // Create a request log
            Requestlogger.CreateRequestlog("MobileWebserviceHandler.SaveOrder", timestamp, emailAddress, hash, this.OriginalAdditionalParameters);

            try
            {
                // Validate the customer
                CustomerEntity customer = Obymobi.Logic.HelperClasses.WebserviceHelper.ValidateRequestRequireCustomer(timestamp, emailAddress, hash, this.OriginalAdditionalParameters);

                // Set the customer id
                order.CustomerId = customer.CustomerId;

                // Convert the mobile model to a normal model
                Obymobi.Logic.Model.Order orderModel = new OrderEntityConverter().ConvertMobileModelToModel(order);

                // Create the order entity
                ObyTypedResult<Obymobi.Logic.Model.Order> result2 = new ObyTypedResult<Obymobi.Logic.Model.Order>();
                OrderEntity orderEntity = Obymobi.Logic.HelperClasses.OrderProcessingHelper.CreateNewOrder(customer, null, orderModel, ref result2);

                // Convert the entity to a mobile model
                Obymobi.Logic.Model.Mobile.Order mobileOrderModel = new OrderEntityConverter().ConvertEntityToMobileModel(orderEntity);
                
                result.ModelCollection.Add(mobileOrderModel);
                result.SetResult(OrderSaveResult.Success);

                //CometHelper.PushExternalOrderPlacedEvent(order.CompanyId, order);
            }
            catch (Obymobi.ObymobiException wex)
            {
                if (wex.ErrorEnumValue is OrderSaveResult)
                {
                    result.SetResult(wex);
                }
                else if (wex.ErrorEnumValue is BusinesshourCheckResult) // Convert the BusinesshourCheckResult exception to OrderSaveResult
                {
                    switch ((BusinesshourCheckResult)wex.ErrorEnumValue)
                    {
                        case BusinesshourCheckResult.OverlappingBusinesshourexceptions:
                            result.SetResult(OrderSaveResult.OverlappingBusinesshourexceptions);
                            break;
                        case BusinesshourCheckResult.NoBusinesshoursAvailable:
                            result.SetResult(OrderSaveResult.NoBusinesshoursAvailable);
                            break;
                    }
                }
                else if (wex.ErrorEnumValue is ValidateCredentialsResult)
                {
                    result.SetResult(OrderSaveResult.AuthenticationError, (ValidateCredentialsResult)wex.ErrorEnumValue);
                    result.SetInnerResult(wex);
                }
                else
                {
                    result.SetResult(OrderSaveResult.Failure);
                    result.SetInnerResult(wex);
                }

                Exceptionlogger.CreateExceptionlog(wex);
            }
            catch (Exception ex)
            {
                result.SetResult(OrderSaveResult.Failure);
                result.SetInnerResult(GenericWebserviceCallResult.Failure, ex.Message);
                Exceptionlogger.CreateExceptionlog(ex);
            }

            Requestlogger.UpdateRequestLog(result);

            return result;
        }
        
        #endregion

        #region Helper Methods

        private static void ConvertToCreateCustomerResult<T>(ObyTypedMobileResult<T> result, ObymobiException wex)
        {
            if (wex.ErrorEnumValue is CreateCustomerResult)
            {
                result.SetResult(wex);
            }
            else if (wex.ErrorEnumValue is Obymobi.Logic.HelperClasses.CustomerHelper.CustomerHelperResult)
            {
                result.SetInnerResult(wex);
                switch ((Obymobi.Logic.HelperClasses.CustomerHelper.CustomerHelperResult)wex.ErrorEnumValue)
                {
                    case Obymobi.Logic.HelperClasses.CustomerHelper.CustomerHelperResult.EmailAddressIsRequired:
                        result.SetResult(CreateCustomerResult.EmailAddressIsRequired);
                        break;
                    case Obymobi.Logic.HelperClasses.CustomerHelper.CustomerHelperResult.NoPasswordTransferHashWasSupplied:
                        result.SetResult(CreateCustomerResult.PasswordMissing);
                        break;
                    case Obymobi.Logic.HelperClasses.CustomerHelper.CustomerHelperResult.UsernameIsRequired:
                    case Obymobi.Logic.HelperClasses.CustomerHelper.CustomerHelperResult.CustomerIsNotNew:
                    default:
                        result.SetResult(CreateCustomerResult.Failure);
                        break;
                }
            }
            else if (wex.ErrorEnumValue is CustomerSaveResult)
            {
                result.SetInnerResult(wex);
                switch ((CustomerSaveResult)wex.ErrorEnumValue)
                {
                    case CustomerSaveResult.PhonenumberAlreadyInUse:
                        result.SetResult(CreateCustomerResult.PhonenumberAlreadyInUse);
                        break;
                    case CustomerSaveResult.PhonenumberIncorrectFormat:
                        result.SetResult(CreateCustomerResult.PhonenumberIncorrectFormat);
                        break;
                    case CustomerSaveResult.EmailAlreadyInUse:
                        result.SetResult(CreateCustomerResult.EmailAlreadyInUse);
                        break;
                    case CustomerSaveResult.EmailIncorrectFormat:
                        result.SetResult(CreateCustomerResult.EmailIncorrectFormat);
                        break;
                    case CustomerSaveResult.EmailaddressMissing:
                        result.SetResult(CreateCustomerResult.EmailAddressIsRequired);
                        break;
                    case CustomerSaveResult.InvalidCountryCode:
                        result.SetResult(CreateCustomerResult.InvalidCountryCode);
                        break;
                    case CustomerSaveResult.PhonenumberEmpty:
                    case CustomerSaveResult.PasswordEmpty:
                    case CustomerSaveResult.SmsNotSent:
                    case CustomerSaveResult.EntitySaveError:
                    case CustomerSaveResult.Unknown:
                    case CustomerSaveResult.Failed:
                    default:
                        result.SetResult(CreateCustomerResult.Failure);
                        break;
                }
            }
            else if (wex.ErrorEnumValue is ValidateCredentialsResult)
            {
                result.SetResult(CreateCustomerResult.AuthenticationError, (ValidateCredentialsResult)wex.ErrorEnumValue);
                result.SetInnerResult(wex);
            }
            else if (wex.ErrorEnumValue is DeviceSaveResult)
            {
                result.SetInnerResult(wex);
                switch ((DeviceSaveResult)wex.ErrorEnumValue)
                { 
                    case DeviceSaveResult.IdentifierAlreadyUsedForDevice:
                        result.SetResult(CreateCustomerResult.IdentifierAlreadyUsedForDevice);
                        break;
                    default:
                        result.SetResult(CreateCustomerResult.Failure);
                        break;
                }
            }
            else
            {
                result.SetResult(GetCustomerResult.Failure);
                result.SetInnerResult(wex);
            }
        }

        private static void GetCustomerValidation(CustomerEntity customerEntity)
        {
            // Ensure Salt
            if (customerEntity.CommunicationSalt.IsNullOrWhiteSpace())
            {
                customerEntity.CommunicationSalt = Dionysos.RandomUtil.GetRandomLowerCaseString(256);
            }

            // Ensure Device
            if (customerEntity.DeviceCollection.Count == 0)
            {
                DeviceEntity device = new DeviceEntity();
                device.Identifier = customerEntity.Email;
                device.CustomerId = customerEntity.CustomerId;
                device.Save();
            }
        }

        #endregion

        #region Properties

        public object[] OriginalAdditionalParameters
        {
            get
            {
                return HttpContext.Current.Items["WebserviceHandler.OriginalRequestParameters"] as object[];
            }
            set
            {
                HttpContext.Current.Items["WebserviceHandler.OriginalRequestParameters"] = value;
            }
        }

        #endregion
    }
}