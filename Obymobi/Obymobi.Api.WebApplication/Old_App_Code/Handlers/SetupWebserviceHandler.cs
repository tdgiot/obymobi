﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using Dionysos;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using Obymobi.Logic.HelperClasses;
using Obymobi.Logic.Model;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Handlers
{
    /// <summary>
    ///     Summary description for SetupWebserviceHandler
    /// </summary>
    public class SetupWebserviceHandler
    {
        public bool ValidateMacAddress(string macAddress)
        {
            if (!macAddress.Contains(":"))
            {
                for (int i = 2; i < macAddress.Length; i += 3)
                {
                    macAddress = macAddress.Insert(i, ":");
                }
            }

            PredicateExpression filter = new PredicateExpression();
            filter.Add(DeviceFields.Identifier == macAddress);

            return (new DeviceCollection().GetDbCount(filter) > 0);
        }

        public List<AppVersion> GetAppVersions(string appCode)
        {
            List<AppVersion> list = new List<AppVersion>();

            try
            {
                int appId = ReleaseHelper.GetApplicationIdForApplicationCode(appCode);

                PredicateExpression filter = new PredicateExpression();
                filter.Add(ReleaseFields.ApplicationId == appId);
                filter.Add(ReleaseFields.Filename != DBNull.Value);
                filter.Add(ReleaseFields.ReleaseGroup == ReleaseGroup.All);

                IncludeFieldsList includes = new IncludeFieldsList();
                includes.Add(ReleaseFields.Filename);
                includes.Add(ReleaseFields.Version);

                SortExpression sort = new SortExpression(ReleaseFields.Version | SortOperator.Descending);

                ReleaseCollection releases = new ReleaseCollection();
                releases.GetMulti(filter, 0, sort, null, null, includes, 0, 0);

                foreach (ReleaseEntity releaseEntity in releases)
                {
                    if (releaseEntity.Filename.Length > 0)
                    {
                        list.Add(new AppVersion(releaseEntity.Version, releaseEntity.Filename));
                    }
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.ProcessStackTrace(true));
            }

            return list;
        }

        public EmenuConfiguration GetConfiguration(string macAddress, string setupCode)
        {
            EmenuConfiguration config = new EmenuConfiguration();

            if (!macAddress.Equals("12:34:56:78:90:AB") && !this.ValidateMacAddress(macAddress))
            {
                // Not auto-setup mac or actual valid device    
                config.ErrorCode = 200;
                config.Error = "No device found for MAC address: " + macAddress;
                return config;
            }

            config = SetupCodeHelper.GetConfiguration(setupCode);


            return config;
        }

        #region Container Classes

        public class AppVersion
        {
            #region  Fields

            public string Filename;
            public string Version;

            #endregion

            #region Constructors

            public AppVersion(string version, string filename)
            {
                this.Version = version;
                this.Filename = filename;
            }

            #endregion
        }

        #endregion
    }
}