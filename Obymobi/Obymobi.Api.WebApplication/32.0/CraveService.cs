using System.Web.Services;
using System.Web.Services.Protocols;
using System.Web.Script.Services;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Obymobi.Logic;
using ObymobiWebservice.Handlers;
using Obymobi.Logic.Model;
using Obymobi.Enums;
using Obymobi.Logic.HelperClasses;
using Obymobi;
using Action = Obymobi.Logic.Model.Action;

namespace ObymobiWebservice.WebServices.v32
{
    /// <summary>
    /// Summary description for Service
    /// </summary>
    [WebService(Namespace = "http://crave-emenu.com/webservices/")]
    //[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    [SoapRpcService(RoutingStyle = SoapServiceRoutingStyle.RequestElement)]
    public class CraveService : System.Web.Services.WebService
    {
        #region Fields

        private WebserviceHandler webserviceHandler = new WebserviceHandler();

        #endregion

        [SoapRpcMethod(), WebMethod(EnableSession=false)]
        [ScriptMethod(UseHttpGet = true)]
        public void TestMethod()
        {
            //CometHelper.AnnounceNewMessage(220, 2025, 15194);
        }

        #region GET methods

        // Generic
        [SoapRpcMethod(), WebMethod(EnableSession=false)]
        [ScriptMethod(UseHttpGet = true)]
        public bool IsWebserviceAlive()
        {
            this.SetNewRelicTransactionName("IsWebserviceAlive");

            // __OBYMOBI_USER_CODE_REGION_START
            // __OBYMOBI_USER_CODE_REGION_END
            
            return this.webserviceHandler.IsWebserviceAlive();
        }

        // General
        [SoapRpcMethod(), WebMethod(EnableSession=false)]
        [ScriptMethod(UseHttpGet = true)]
        public bool IsAlive()
        {
            this.SetNewRelicTransactionName("IsAlive");

            // __OBYMOBI_USER_CODE_REGION_START
            // __OBYMOBI_USER_CODE_REGION_END

            return this.webserviceHandler.IsWebserviceAlive();
        }

        // Generic
        [SoapRpcMethod(), WebMethod(EnableSession=false)]
        [ScriptMethod(UseHttpGet = true)]
        public bool IsDatabaseAlive()
        {
            this.SetNewRelicTransactionName("IsDatabaseAlive");

            // __OBYMOBI_USER_CODE_REGION_START
            // __OBYMOBI_USER_CODE_REGION_END

            return this.webserviceHandler.IsDatabaseAlive();
        }

        // Generic
        [SoapRpcMethod(), WebMethod(EnableSession=false)]
        [ScriptMethod(UseHttpGet = true)]
        public string GetReleaseAndDownloadLocation(long timestamp, string macAddress, string applicationCode, string hash)
        {
            this.SetNewRelicTransactionName("GetReleaseAndDownloadLocation");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { applicationCode };

            // __OBYMOBI_USER_CODE_REGION_START
            // __OBYMOBI_USER_CODE_REGION_END

            return this.webserviceHandler.GetReleaseAndDownloadLocation(timestamp, macAddress, applicationCode, hash);
        }

        // Terminal
        [SoapRpcMethod(), WebMethod(EnableSession=false)]
        [ScriptMethod(UseHttpGet = true)]
        public ObyTypedResult<Client> GetClients(long timestamp, string macAddress, string hash)
        {
            this.SetNewRelicTransactionName("GetClients");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { };

            // __OBYMOBI_USER_CODE_REGION_START
            // __OBYMOBI_USER_CODE_REGION_END

            return webserviceHandler.GetClients(timestamp, macAddress, hash);          
        }

        // Otoucho 
        // Emenu - deprecated - use MAC address
        [SoapRpcMethod(), WebMethod(EnableSession=false)]
        [ScriptMethod(UseHttpGet = true)]
        public ObyTypedResult<Client> GetClient(long timestamp, string macAddress, int clientId, string clientmacAddress, int deliverypointgroupId, int deliverypointId, string applicationVersion, string osVersion, int deviceType, int deviceModel, string hash)
        {
            this.SetNewRelicTransactionName("GetClient");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { clientId, clientmacAddress, deliverypointgroupId, deliverypointId, applicationVersion, osVersion, deviceType, deviceModel };

            // __OBYMOBI_USER_CODE_REGION_START            
            // __OBYMOBI_USER_CODE_REGION_END

            return webserviceHandler.GetClient(timestamp, macAddress, clientId, clientmacAddress, deliverypointgroupId, deliverypointId, applicationVersion, osVersion, deviceType, deviceModel, hash);

        }

        // Emenu
        [SoapRpcMethod(), WebMethod(EnableSession=false)]
        [ScriptMethod(UseHttpGet = true)]
        public ObyTypedResult<Company> GetCompanies(long timestamp, string username, string password, string hash)
        {
            this.SetNewRelicTransactionName("GetCompanies");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { };
            // __OBYMOBI_USER_CODE_REGION_START
            // __OBYMOBI_USER_CODE_REGION_END          
            return webserviceHandler.GetCompanies(timestamp, username, password, hash);

        }

        // Otoucho
        // Emenu - deprecated - use MAC address
        [SoapRpcMethod(), WebMethod(EnableSession=false)]
        [ScriptMethod(UseHttpGet = true)]
        public ObyTypedResult<Company> GetCompany(long timestamp, string macAddress, int companyId, string hash)
        {
            this.SetNewRelicTransactionName("GetCompany");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { companyId };
            // __OBYMOBI_USER_CODE_REGION_START
            // __OBYMOBI_USER_CODE_REGION_END
            return webserviceHandler.GetCompany(timestamp, macAddress, companyId, hash);

        }

        // Otoucho
        // Emenu - deprecated - use MAC address
        [SoapRpcMethod(), WebMethod(EnableSession=false)]
        [ScriptMethod(UseHttpGet = true)]
        public ObyTypedResult<Advertisement> GetAdvertisements(long timestamp, string macAddress, string hash)
        {
            this.SetNewRelicTransactionName("GetAdvertisements");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { };
            // __OBYMOBI_USER_CODE_REGION_START
            // __OBYMOBI_USER_CODE_REGION_END

            return webserviceHandler.GetAdvertisements(timestamp, macAddress, hash);

        }

        // Otoucho
        // Emenu - deprecated - use MAC address
        [SoapRpcMethod(), WebMethod(EnableSession=false)]
        [ScriptMethod(UseHttpGet = true)]
        public ObyTypedResult<Entertainment> GetEntertainment(long timestamp, string macAddress, string hash)
        {
            this.SetNewRelicTransactionName("GetEntertainment");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { };
            // __OBYMOBI_USER_CODE_REGION_START
            // __OBYMOBI_USER_CODE_REGION_END

            return webserviceHandler.GetEntertainment(timestamp, macAddress, hash);

        }

        // Otoucho
        // Emenu - deprecated - use MAC address
        [SoapRpcMethod(), WebMethod(EnableSession=false)]
        [ScriptMethod(UseHttpGet = true)]
        public ObyTypedResult<Paymentmethod> GetPaymentmethods(long timestamp, string macAddress, string hash)
        {
            this.SetNewRelicTransactionName("GetPaymentmethods");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { };
            // __OBYMOBI_USER_CODE_REGION_START
            // __OBYMOBI_USER_CODE_REGION_END

            return webserviceHandler.GetPaymentmethods(timestamp, macAddress, hash);

        }

        // Otoucho
        // Emenu - deprecated - use MAC address
        [SoapRpcMethod(), WebMethod(EnableSession=false)]
        [ScriptMethod(UseHttpGet = true)]
        public ObyTypedResult<ProductmenuItem> GetMenu(long timestamp, string macAddress, int deliverypointgroupId, string hash)
        {
            this.SetNewRelicTransactionName("GetMenu");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { deliverypointgroupId };
            // __OBYMOBI_USER_CODE_REGION_START
            // __OBYMOBI_USER_CODE_REGION_END

            return webserviceHandler.GetMenu(timestamp, macAddress, deliverypointgroupId, true, hash);
        }

        // Otoucho
        // Emenu - deprecated - use MAC address
        [SoapRpcMethod(), WebMethod(EnableSession = false)]
        [ScriptMethod(UseHttpGet = true)]
        public ObyTypedResult<SimpleWebserviceResult> PublishMenu(long timestamp, string macAddress, int menuId, string hash)
        {
            this.SetNewRelicTransactionName("PublishMenu");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { menuId };
            // __OBYMOBI_USER_CODE_REGION_START
            // __OBYMOBI_USER_CODE_REGION_END

            return webserviceHandler.PublishMenu(timestamp, macAddress, menuId, hash);
        }

        [SoapRpcMethod(), WebMethod(EnableSession = false)]
        [ScriptMethod(UseHttpGet = true)]
        public ObyTypedResult<SimpleWebserviceResult> PublishCompany(long timestamp, string macAddress, int companyId, string hash)
        {
            this.SetNewRelicTransactionName("PublishCompany");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { companyId };

            // __OBYMOBI_USER_CODE_REGION_START
            // __OBYMOBI_USER_CODE_REGION_END

            return webserviceHandler.PublishCompany(timestamp, macAddress, companyId, hash);
        }

        [SoapRpcMethod(), WebMethod(EnableSession = false)]
        [ScriptMethod(UseHttpGet = true)]
        public ObyTypedResult<SimpleWebserviceResult> PublishRoomControlConfiguration(long timestamp, string macAddress, int roomControlConfigurationId, string hash)
        {
            this.SetNewRelicTransactionName("PublishRoomControlConfiguration");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { roomControlConfigurationId };
            // __OBYMOBI_USER_CODE_REGION_START
            // __OBYMOBI_USER_CODE_REGION_END

            return webserviceHandler.PublishRoomControlConfiguration(timestamp, macAddress, roomControlConfigurationId, hash);
        }

        [SoapRpcMethod(), WebMethod(EnableSession=false)]
		[ScriptMethod(UseHttpGet = true)]
		public ObyTypedResult<Netmessage> GetNetmessages(long timestamp, string macAddress, int customerId, int clientId, int companyId, int deliverypointId, int terminalId, string hash)
		{
            this.SetNewRelicTransactionName("GetNetmessages");

			this.webserviceHandler.OriginalAdditionalParameters = new object[] { customerId, clientId, companyId, deliverypointId, terminalId };

			// __OBYMOBI_USER_CODE_REGION_START
			// __OBYMOBI_USER_CODE_REGION_END

			return webserviceHandler.GetNetmessages(timestamp, macAddress, customerId, clientId, companyId, deliverypointId, terminalId, hash);
		}

			// Otoucho
        // Emenu - deprecated - use MAC address
        [SoapRpcMethod(), WebMethod(EnableSession=false)]
        [ScriptMethod(UseHttpGet = true)]
        public ObyTypedResult<Survey> GetSurveys(long timestamp, string macAddress, string hash)
        {
            this.SetNewRelicTransactionName("GetSurveys");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { };
            // __OBYMOBI_USER_CODE_REGION_START
            // __OBYMOBI_USER_CODE_REGION_END

            return webserviceHandler.GetSurveys(timestamp, macAddress, hash);

        }

        // Emenu
        [SoapRpcMethod(), WebMethod(EnableSession=false)]
        [ScriptMethod(UseHttpGet = true)]
        public ObyTypedResult<Announcement> GetAnnouncements(long timestamp, string macAddress, string hash)
        {
            this.SetNewRelicTransactionName("GetAnnouncements");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { };
            // __OBYMOBI_USER_CODE_REGION_START
            // __OBYMOBI_USER_CODE_REGION_END

            return webserviceHandler.GetAnnouncements(timestamp, macAddress, hash);

        }

        // Emenu
        [SoapRpcMethod(), WebMethod(EnableSession=false)]
        [ScriptMethod(UseHttpGet = true)]
        public ObyTypedResult<Deliverypoint> GetDeliverypoints(long timestamp, string macAddress, int deliverypointgroupId, string hash)
        {
            this.SetNewRelicTransactionName("GetDeliverypoints");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { deliverypointgroupId };
            // __OBYMOBI_USER_CODE_REGION_START
            // __OBYMOBI_USER_CODE_REGION_END

            return webserviceHandler.GetDeliverypoints(timestamp, macAddress, deliverypointgroupId, hash);

        }

        // Terminal
        [SoapRpcMethod(), WebMethod(EnableSession=false)]
        [ScriptMethod(UseHttpGet = true)]
        public ObyTypedResult<Terminal> GetTerminal(long timestamp, string macAddress, int terminalId, string applicationVersion, string osVersion, int deviceType, string hash)
        {
            this.SetNewRelicTransactionName("GetTerminal");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { terminalId, applicationVersion, osVersion, deviceType };
            // __OBYMOBI_USER_CODE_REGION_START
            // __OBYMOBI_USER_CODE_REGION_END

            return webserviceHandler.GetTerminal(timestamp, macAddress, terminalId, applicationVersion, osVersion, deviceType, hash);
        }

        // Terminal - OSS
        [SoapRpcMethod(), WebMethod(EnableSession=false)]
        [ScriptMethod(UseHttpGet = true)]
        public ObyTypedResult<PosIntegrationInformation> GetPosIntegrationInformation(long timestamp, string macAddress, string hash)
        {
            this.SetNewRelicTransactionName("GetPosIntegrationInformation");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { };
            // __OBYMOBI_USER_CODE_REGION_START
            // __OBYMOBI_USER_CODE_REGION_END

            return webserviceHandler.GetPosIntegrationInformation(timestamp, macAddress, hash);

        }

        // Terminal - OSS
        [SoapRpcMethod(), WebMethod(EnableSession=false)]
        [ScriptMethod(UseHttpGet = true)]
        public ObyTypedResult<ClientStatus> GetClientStatuses(long timestamp, string macAddress, string hash)
        {
            this.SetNewRelicTransactionName("GetClientStatuses");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { };
            // __OBYMOBI_USER_CODE_REGION_START
            // __OBYMOBI_USER_CODE_REGION_END

            return webserviceHandler.GetClientStatuses(timestamp, macAddress, hash);

        }

        // Terminal - OSS / REQ
        [SoapRpcMethod(), WebMethod(EnableSession=false)]
        public ObyTypedResult<Product> GetProducts(long timestamp, string macAddress, string hash)
        {
            this.SetNewRelicTransactionName("GetProducts");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { };
            // __OBYMOBI_USER_CODE_REGION_START
            // __OBYMOBI_USER_CODE_REGION_END

            return webserviceHandler.GetProducts(timestamp, macAddress, hash);

        }

        // Terminal - OSS / REQ
        [SoapRpcMethod(), WebMethod(EnableSession=false)]
        public ObyTypedResult<Category> GetCategories(long timestamp, string macAddress, string hash)
        {
            this.SetNewRelicTransactionName("GetCategories");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { };
            // __OBYMOBI_USER_CODE_REGION_START
            // __OBYMOBI_USER_CODE_REGION_END

            return webserviceHandler.GetCategories(timestamp, macAddress, hash);

        }

        // NOC
        [SoapRpcMethod(), WebMethod(EnableSession=false)]
        public ObyTypedResult<Terminal> GetTerminals(long timestamp, string macAddress, string hash)
        {
            this.SetNewRelicTransactionName("GetTerminals");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { };
            // __OBYMOBI_USER_CODE_REGION_START
            // __OBYMOBI_USER_CODE_REGION_END

            return webserviceHandler.GetTerminals(timestamp, macAddress, hash);

        }

        // Terminal - REQ & OSS
        [SoapRpcMethod(), WebMethod(EnableSession=false)]
        public ObyTypedResult<Order> GetOrders(long timestamp, string macAddress, int statusCode, string statusMessage, string version, bool isConsole, bool isMasterConsole, string hash)
        {
            this.SetNewRelicTransactionName("GetOrders");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { statusCode, statusMessage, version, isConsole, isMasterConsole };
            // __OBYMOBI_USER_CODE_REGION_START
            // __OBYMOBI_USER_CODE_REGION_END

            return webserviceHandler.GetOrders(timestamp, macAddress, statusCode, statusMessage, version, isConsole, isMasterConsole, hash);

        }

        // Terminal - REQ
        [SoapRpcMethod(), WebMethod(EnableSession=false)]
        public ObyTypedResult<TerminalOperationMode> GetTerminalOperationMode(long timestamp, string macAddress, string hash)
        {
            this.SetNewRelicTransactionName("GetTerminalOperationMode");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { };
            // __OBYMOBI_USER_CODE_REGION_START
            // __OBYMOBI_USER_CODE_REGION_END

            return webserviceHandler.GetTerminalOperationMode(timestamp, macAddress, hash);

        }

        // Terminal - OSS
        [SoapRpcMethod(), WebMethod(EnableSession=false)]
        public ObyTypedResult<OrderRoutestephandlerSaveStatus> GetRoutestephandlerStatuses(long timestamp, string macAddress, string[] orderroutestephandlerGuids, string hash)
        {
            this.SetNewRelicTransactionName("GetRoutestephandlerStatuses");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { orderroutestephandlerGuids };
            // __OBYMOBI_USER_CODE_REGION_START
            // __OBYMOBI_USER_CODE_REGION_END

            return webserviceHandler.GetRoutestephandlerStatuses(timestamp, macAddress, orderroutestephandlerGuids, hash);

        }

        // Emenu
        [SoapRpcMethod(), WebMethod(EnableSession=false)]
        public ObyTypedResult<SocialmediaMessage> GetSocialmediaMessages(long timestamp, string macAddress, string hash)
        {
            this.SetNewRelicTransactionName("GetSocialmediaMessages");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { };
            // __OBYMOBI_USER_CODE_REGION_START
            // __OBYMOBI_USER_CODE_REGION_END

            return new ObyTypedResult<SocialmediaMessage>();

        }

        // Emenu
        [SoapRpcMethod(), WebMethod(EnableSession=false)]
        [ScriptMethod(UseHttpGet = true)]
        public ObyTypedResult<SimpleWebserviceResult> GetPmsFolio(long timestamp, string macAddress, string hash)
        {
            this.SetNewRelicTransactionName("GetPmsFolio");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { };
            // __OBYMOBI_USER_CODE_REGION_START
            // __OBYMOBI_USER_CODE_REGION_END

            return webserviceHandler.GetPmsFolio(timestamp, macAddress, hash);

        }

        // Emenu
        [SoapRpcMethod(), WebMethod(EnableSession=false)]
        [ScriptMethod(UseHttpGet = true)]
        public ObyTypedResult<SimpleWebserviceResult> GetPmsGuestInformation(long timestamp, string macAddress, string hash)
        {
            this.SetNewRelicTransactionName("GetPmsGuestInformation");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { };
            // __OBYMOBI_USER_CODE_REGION_START
            // __OBYMOBI_USER_CODE_REGION_END

            return webserviceHandler.GetPmsGuestInformation(timestamp, macAddress, hash);

        }

        // Emenu
        [SoapRpcMethod(), WebMethod(EnableSession=false)]
        [ScriptMethod(UseHttpGet = true)]
        public ObyTypedResult<SimpleWebserviceResult> GetPmsExpressCheckout(long timestamp, string macAddress, decimal balance, string hash)
        {
            this.SetNewRelicTransactionName("GetPmsExpressCheckout");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { balance };
            // __OBYMOBI_USER_CODE_REGION_START
            // __OBYMOBI_USER_CODE_REGION_END

            return webserviceHandler.GetPmsExpressCheckout(timestamp, macAddress, balance, hash);

        }

        [SoapRpcMethod(), WebMethod(EnableSession = false)]
        [ScriptMethod(UseHttpGet = true)]
        public ObyTypedResult<Site> GetSite(long timestamp, string macAddress, int siteId, int deviceType, string cultureCode, string hash)
        {
            this.SetNewRelicTransactionName("GetSite");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { siteId, deviceType, cultureCode };
            // __OBYMOBI_USER_CODE_REGION_START
            // __OBYMOBI_USER_CODE_REGION_END

            return webserviceHandler.GetSite(timestamp, macAddress, siteId, deviceType, string.Empty, cultureCode, hash);
        }

        [SoapRpcMethod(), WebMethod]
        [ScriptMethod(UseHttpGet = true)]
        public ObyTypedResult<SimpleWebserviceResult> GetSiteTicks(long timestamp, string macAddress, int siteId, string hash)
        {
            this.SetNewRelicTransactionName("GetSiteTicks");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { siteId };

            // __OBYMOBI_USER_CODE_REGION_START
            // __OBYMOBI_USER_CODE_REGION_END
            return webserviceHandler.GetSiteTicks(timestamp, macAddress, siteId, hash);
        }

        [SoapRpcMethod(), WebMethod(EnableSession = false)]
        [ScriptMethod(UseHttpGet = true)]
        public ObyTypedResult<Company> GetPointOfInterest(long timestamp, string macAddress, int pointOfInterestId, int deviceTypeInt, string hash)
        {
            this.SetNewRelicTransactionName("GetPointOfInterest");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { pointOfInterestId, deviceTypeInt };
            // __OBYMOBI_USER_CODE_REGION_START
            // __OBYMOBI_USER_CODE_REGION_END

            return webserviceHandler.GetPointOfInterest(timestamp, macAddress, pointOfInterestId, deviceTypeInt, hash);
        }

        [SoapRpcMethod(), WebMethod]
        [ScriptMethod(UseHttpGet = true)]
        public ObyTypedResult<SimpleWebserviceResult> GetPointOfInterestTicks(long timestamp, string macAddress, int pointOfInterestId, string hash)
        {
            this.SetNewRelicTransactionName("GetPointOfInterestTicks");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { pointOfInterestId };

            // __OBYMOBI_USER_CODE_REGION_START
            // __OBYMOBI_USER_CODE_REGION_END
            return webserviceHandler.GetPointOfInterestTicks(timestamp, macAddress, pointOfInterestId, hash);
        }

        [SoapRpcMethod(), WebMethod]
        [ScriptMethod(UseHttpGet = true)]
        public ObyTypedResult<RoomControlConfiguration> GetRoomControlConfiguration(long timestamp, string macAddress, string hash)
        {
            this.SetNewRelicTransactionName("GetRoomControlConfigurations");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { };

            // __OBYMOBI_USER_CODE_REGION_START
            // __OBYMOBI_USER_CODE_REGION_END
            return webserviceHandler.GetRoomControlConfiguration(timestamp, macAddress, hash);
        }

        [SoapRpcMethod(), WebMethod]
        [ScriptMethod(UseHttpGet = true)]
        public ObyTypedResult<SimpleWebserviceResult> GetEstimatedDeliveryTime(long timestamp, string macAddress, int deliverypointgroupId, string hash)
        {
            this.SetNewRelicTransactionName("GetEstimatedDeliveryTime");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { deliverypointgroupId };

            // __OBYMOBI_USER_CODE_REGION_START
            // __OBYMOBI_USER_CODE_REGION_END

            return this.webserviceHandler.GetEstimatedDeliveryTime(timestamp, macAddress, deliverypointgroupId, hash);
        }

        [SoapRpcMethod(), WebMethod(EnableSession = false)]
        [ScriptMethod(UseHttpGet = true)]
        public ObyTypedResult<UIScheduleItem> GetUISchedule(long timestamp, string macAddress, int deliverypointgroupId, string hash)
        {
            this.SetNewRelicTransactionName("GetUISchedule");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { deliverypointgroupId };
            // __OBYMOBI_USER_CODE_REGION_START
            // __OBYMOBI_USER_CODE_REGION_END

            return webserviceHandler.GetUISchedule(timestamp, macAddress, deliverypointgroupId, hash);
        }

        [SoapRpcMethod(), WebMethod]
        [ScriptMethod(UseHttpGet = true)]
        public ObyTypedResult<DeliveryTime> GetEstimatedDeliveryTimes(long timestamp, string macAddress, string hash)
        {
            this.SetNewRelicTransactionName("GetEstimatedDeliveryTimes");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { };

            // __OBYMOBI_USER_CODE_REGION_START
            // __OBYMOBI_USER_CODE_REGION_END

            return this.webserviceHandler.GetEstimatedDeliveryTimes(timestamp, macAddress, hash);
        }

        [SoapRpcMethod(), WebMethod(EnableSession = false)]
        [ScriptMethod(UseHttpGet = true)]
        public ObyTypedResult<PriceSchedule> GetPriceSchedule(long timestamp, string macAddress, int priceScheduleId, string hash)
        {
            this.SetNewRelicTransactionName("GetPriceSchedule");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { priceScheduleId };
            // __OBYMOBI_USER_CODE_REGION_START
            // __OBYMOBI_USER_CODE_REGION_END

            return webserviceHandler.GetPriceSchedule(timestamp, macAddress, priceScheduleId, hash);
        }

        [SoapRpcMethod(), WebMethod(EnableSession = false)]
        [ScriptMethod(UseHttpGet = true)]
        public ObyTypedResult<CustomText> GetTranslations(long timestamp, string macAddress, string cultureCode, string hash)
        {
            this.SetNewRelicTransactionName("GetTranslations");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { cultureCode };
            // __OBYMOBI_USER_CODE_REGION_START
            // __OBYMOBI_USER_CODE_REGION_END
             
            return webserviceHandler.GetTranslations(timestamp, macAddress, cultureCode, hash);
        }

        [SoapRpcMethod(), WebMethod(EnableSession = false)]
        [ScriptMethod(UseHttpGet = true)]
        public ObyTypedResult<Messagegroup> GetPmsMessagegroups(long timestamp, string macAddress, int messagegroupType, string date, string groupName, string hash)
        {
            this.SetNewRelicTransactionName("GetPmsMessagegroups");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { messagegroupType, date, groupName };
            // __OBYMOBI_USER_CODE_REGION_START
            // __OBYMOBI_USER_CODE_REGION_END

            return webserviceHandler.GetPmsMessagegroups(timestamp, macAddress, messagegroupType, date, groupName, hash);
        }

        [SoapRpcMethod(), WebMethod(EnableSession = false)]
        [ScriptMethod(UseHttpGet = true)]
        public ObyTypedResult<GuestGroup> GetGuestGroups(long timestamp, string macAddress, string hash)
        {
            this.SetNewRelicTransactionName("GetGuestGroups");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { };
            // __OBYMOBI_USER_CODE_REGION_START
            // __OBYMOBI_USER_CODE_REGION_END

            return webserviceHandler.GetGuestGroups(timestamp, macAddress, hash);
        }

        [SoapRpcMethod(), WebMethod(EnableSession = false)]
        [ScriptMethod(UseHttpGet = true)]
        public ObyTypedResult<SimpleWebserviceResult> RegisterGameSession(long timestamp, string macAddress, string url, string hash)
        {
            this.SetNewRelicTransactionName("RegisterGameSession");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { url };
            // __OBYMOBI_USER_CODE_REGION_START
            // __OBYMOBI_USER_CODE_REGION_END

            return webserviceHandler.RegisterGameSession(timestamp, macAddress, url, hash);
        }

        [SoapRpcMethod(), WebMethod(EnableSession = false)]
        [ScriptMethod(UseHttpGet = true)]
        public ObyTypedResult<Map> GetMap(long timestamp, string macAddress, int mapId, string hash)
        {
            this.SetNewRelicTransactionName("GetMap");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { mapId };

            // __OBYMOBI_USER_CODE_REGION_START
            // __OBYMOBI_USER_CODE_REGION_END

            return webserviceHandler.GetMap(timestamp, macAddress, mapId, hash);
        }

        [SoapRpcMethod(), WebMethod(EnableSession = false)]
        [ScriptMethod(UseHttpGet = true)]
        public ObyTypedResult<SimpleWebserviceResult> GetMapTimestamp(long timestamp, string macAddress, int mapId, string hash)
        {
            this.SetNewRelicTransactionName("GetMapTimestamp");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { mapId };

            // __OBYMOBI_USER_CODE_REGION_START            
            // __OBYMOBI_USER_CODE_REGION_END

            return webserviceHandler.GetMapTimestamp(timestamp, macAddress, mapId, hash);
        }

        #endregion

        #region PUT methods

        [SoapRpcMethod(), WebMethod(EnableSession = false)]
        [ScriptMethod(UseHttpGet = true)]
        public ObyTypedResult<SimpleWebserviceResult> PublishMapTimestamp(long timestamp, string macAddress, int mapId, string hash)
        {
            this.SetNewRelicTransactionName("PublishMapTimestamp");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { mapId };
            // __OBYMOBI_USER_CODE_REGION_START            
            // __OBYMOBI_USER_CODE_REGION_END

            return webserviceHandler.PublishMapTimestamp(timestamp, macAddress, mapId, hash);
        }

        // Otoucho
        // Emenu - deprecated - use MAC address
        [SoapRpcMethod(), WebMethod(EnableSession=false)]
        [ScriptMethod(UseHttpGet = true)]
        public ObyTypedResult<ClientStatus> GetSetClientStatus(long timestamp, string macAddress, string xml, string hash)
        {
            this.SetNewRelicTransactionName("GetSetClientStatus");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { xml };
            // __OBYMOBI_USER_CODE_REGION_START

            ClientStatus clientStatus = XmlHelper.Deserialize<ClientStatus>(xml);

            // __OBYMOBI_USER_CODE_REGION_END

            return webserviceHandler.GetSetClientStatus(timestamp, macAddress, clientStatus, hash);
        }

        // Otoucho
        // Emenu - deprecated - use MAC address
        [SoapRpcMethod(), WebMethod(EnableSession=false)]
        [ScriptMethod(UseHttpGet = true)]
        public ObyTypedResult<TerminalStatus> GetSetTerminalStatus(long timestamp, string macAddress, string xml, string hash)
        {
            this.SetNewRelicTransactionName("GetSetTerminalStatus");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { xml };
            // __OBYMOBI_USER_CODE_REGION_START

            TerminalStatus terminalStatus = XmlHelper.Deserialize<TerminalStatus>(xml);

            // __OBYMOBI_USER_CODE_REGION_END

            return webserviceHandler.GetSetTerminalStatus(timestamp, macAddress, terminalStatus, hash);

        }

        // Terminal - OSS
        [SoapRpcMethod(), WebMethod(EnableSession=false)]
        [ScriptMethod(UseHttpGet = true)]
        public ObyTypedResult<SimpleWebserviceResult> SetClientStatuses(long timestamp, string macAddress, string xml, string hash)
        {
            this.SetNewRelicTransactionName("SetClientStatuses");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { xml };
            // __OBYMOBI_USER_CODE_REGION_START

            ClientStatus[] clientStatuses = XmlHelper.Deserialize<ClientStatus[]>(xml);

            // __OBYMOBI_USER_CODE_REGION_END

            return webserviceHandler.SetClientStatuses(timestamp, macAddress, clientStatuses, hash);

        }

        // Otoucho
        // Emenu - deprecated - use MAC address
        [SoapRpcMethod(), WebMethod(EnableSession=false)]
        [ScriptMethod(UseHttpGet = true)]
        public ObyTypedResult<Order> SaveOrder(long timestamp, string macAddress, string xml, string hash)
        {
            this.SetNewRelicTransactionName("SaveOrder");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { xml };
            // __OBYMOBI_USER_CODE_REGION_START

            // Crave Mobile hackz0r
            if (xml.StartsWith("\"") && xml.Length > 1)
                xml = xml.Substring(1);
            if (xml.EndsWith("\""))
                xml = xml.Substring(0, xml.Length - 1);

            // Create the order using the specified xml
            Order order = XmlHelper.Deserialize<Order>(xml);

            // __OBYMOBI_USER_CODE_REGION_END

            return webserviceHandler.SaveOrder(timestamp, macAddress, order, hash);

        }

        // Terminal
        [SoapRpcMethod(), WebMethod(EnableSession=false)]
        [ScriptMethod(UseHttpGet = true)]
        public ObyTypedResult<Posorder> CreatePosorder(long timestamp, string macAddress, string xml, string hash)
        {
            this.SetNewRelicTransactionName("CreatePosorder");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { xml };
            // __OBYMOBI_USER_CODE_REGION_START

            Order order = XmlHelper.Deserialize<Order>(xml);

            // __OBYMOBI_USER_CODE_REGION_END

            return webserviceHandler.CreatePosorder(timestamp, macAddress, order, hash);

        }

        // Otoucho
        // Emenu - deprecated - use MAC address
        [SoapRpcMethod(), WebMethod(EnableSession=false)]
        [ScriptMethod(UseHttpGet = true)]
        public ObyTypedResult<SurveyResult> SaveSurveyResults(long timestamp, string macAddress, string xml, string hash)
        {
            this.SetNewRelicTransactionName("SaveSurveyResults");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { xml };
            // __OBYMOBI_USER_CODE_REGION_START

            SurveyResult results = XmlHelper.Deserialize<SurveyResult>(xml);

            // __OBYMOBI_USER_CODE_REGION_END

            return webserviceHandler.SaveSurveyResults(timestamp, macAddress, results, hash);

        }

        // Emenu - deprecated - use MAC address
        [SoapRpcMethod(), WebMethod(EnableSession=false)]
        [ScriptMethod(UseHttpGet = true)]
        public ObyTypedResult<Rating> SaveRatings(long timestamp, string macAddress, string xml, string hash)
        {
            this.SetNewRelicTransactionName("SaveRatings");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { xml };
            // __OBYMOBI_USER_CODE_REGION_START

            Rating[] ratings = XmlHelper.Deserialize<Rating[]>(xml);

            // __OBYMOBI_USER_CODE_REGION_END

            return webserviceHandler.SaveRatings(timestamp, macAddress, ratings, hash);

        }

        // Emenu - deprecated - use MAC address
        [SoapRpcMethod(), WebMethod(EnableSession=false)]
        [ScriptMethod(UseHttpGet = true)]
        public ObyTypedResult<SocialmediaMessage> SaveSocialmediaMessage(long timestamp, string macAddress, string xml, string hash)
        {
            this.SetNewRelicTransactionName("SaveSocialmediaMessage");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { xml };
            // __OBYMOBI_USER_CODE_REGION_START

            SocialmediaMessage message = XmlHelper.Deserialize<SocialmediaMessage>(xml);

            // __OBYMOBI_USER_CODE_REGION_END

            return new ObyTypedResult<SocialmediaMessage>();

        }

        // Terminal - OSS
        [SoapRpcMethod(), WebMethod(EnableSession=false)]
        [ScriptMethod(UseHttpGet = true)]
        public ObyTypedResult<ClientEntertainment> SaveClientEntertainment(long timestamp, string macAddress, string xml, string hash)
        {
            this.SetNewRelicTransactionName("SaveClientEntertainment");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { xml };
            // __OBYMOBI_USER_CODE_REGION_START

            ClientEntertainment[] clientEntertainments = XmlHelper.Deserialize<ClientEntertainment[]>(xml);

            // __OBYMOBI_USER_CODE_REGION_END

            return webserviceHandler.SaveClientEntertainment(timestamp, macAddress, clientEntertainments, hash);

        }

        // Emenu
        [SoapRpcMethod(), WebMethod(EnableSession=false)]
        [ScriptMethod(UseHttpGet = true)]
        public ObyTypedResult<SimpleWebserviceResult> SaveClientLog(long timestamp, string macAddress, int logType, string log, long fileDate, string hash)
        {
            this.SetNewRelicTransactionName("SaveClientLog");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { logType, log, fileDate };
            // __OBYMOBI_USER_CODE_REGION_START
            // __OBYMOBI_USER_CODE_REGION_END

            return webserviceHandler.SaveClientLog(timestamp, macAddress, logType, log, fileDate, hash);

        }

        [SoapRpcMethod(), WebMethod(EnableSession=false)]
        [ScriptMethod(UseHttpGet = true)]
        public ObyTypedResult<SimpleWebserviceResult> SaveClientLogZipped(long timestamp, string macAddress, int logType, string base64Zip, long fileDate, string hash)
        {
            this.SetNewRelicTransactionName("SaveClientLogZipped");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { logType, fileDate }; // base64Zip is ginored on purpose, not part of the hash!
            // __OBYMOBI_USER_CODE_REGION_START
            // __OBYMOBI_USER_CODE_REGION_END

            return this.webserviceHandler.SaveClientLogZipped(timestamp, macAddress, logType, base64Zip, fileDate, hash);
        }

        // Terminal - OSS /. REQ
        [SoapRpcMethod(), WebMethod(EnableSession=false)]
        public ObyTypedResult<Posproduct> SetPosProducts(long timestamp, string macAddress, long batchId, string xml, string hash)
        {
            this.SetNewRelicTransactionName("SetPosProducts");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { batchId, xml };
            // __OBYMOBI_USER_CODE_REGION_START

            if (xml.Trim().Length == 0)
                throw new ObymobiException(ImportPosproductsResult.XmlIsEmpty, "Xml string containing serialized Posproduct array is empty.");

            Posproduct[] posproducts = XmlHelper.Deserialize<Posproduct[]>(xml);

            // __OBYMOBI_USER_CODE_REGION_END

            return webserviceHandler.SetPosProducts(timestamp, macAddress, batchId, posproducts, hash);

        }

        // Terminal - OSS / REQ
        [SoapRpcMethod(), WebMethod(EnableSession=false)]
        public ObyTypedResult<Poscategory> SetPosCategories(long timestamp, string macAddress, long batchId, string xml, string hash)
        {
            this.SetNewRelicTransactionName("SetPosCategories");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { batchId, xml };
            // __OBYMOBI_USER_CODE_REGION_START

            if (xml.Trim().Length == 0)
                throw new ObymobiException(ImportPoscategoriesResult.XmlIsEmpty, "Xml string containing serialized Poscategory array is empty.");

            Poscategory[] poscategories = XmlHelper.Deserialize<Poscategory[]>(xml);

            // __OBYMOBI_USER_CODE_REGION_END

            return webserviceHandler.SetPosCategories(timestamp, macAddress, batchId, poscategories, hash);

        }

        // Terminal - OSS / REQ
        [SoapRpcMethod(), WebMethod(EnableSession=false)]
        public ObyTypedResult<Posdeliverypoint> SetPosDeliverypoints(long timestamp, string macAddress, long batchId, string xml, string hash)
        {
            this.SetNewRelicTransactionName("SetPosDeliverypoints");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { batchId, xml };
            // __OBYMOBI_USER_CODE_REGION_START

            if (xml.Trim().Length == 0)
                throw new ObymobiException(ImportPosdeliverypointsResult.XmlIsEmpty, "Xml string containing serialized Posdeliverypoint array is empty.");

            Posdeliverypoint[] posdeliverypoints = XmlHelper.Deserialize<Posdeliverypoint[]>(xml);

            // __OBYMOBI_USER_CODE_REGION_END

            return webserviceHandler.SetPosDeliverypoints(timestamp, macAddress, batchId, posdeliverypoints, hash);

        }

        // Terminal - OSS / REQ
        [SoapRpcMethod(), WebMethod(EnableSession=false)]
        public ObyTypedResult<Posdeliverypointgroup> SetPosDeliverypointgroups(long timestamp, string macAddress, long batchId, string xml, string hash)
        {
            this.SetNewRelicTransactionName("SetPosDeliverypointgroups");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { batchId, xml };
            // __OBYMOBI_USER_CODE_REGION_START

            if (xml.Trim().Length == 0)
                throw new ObymobiException(ImportPosdeliverypointgroupsResult.XmlIsEmpty, "Xml string containing serialized Posdeliverypointgroup array is empty.");

            Posdeliverypointgroup[] posdeliverypointgroups = XmlHelper.Deserialize<Posdeliverypointgroup[]>(xml);

            // __OBYMOBI_USER_CODE_REGION_END

            return webserviceHandler.SetPosDeliverypointgroups(timestamp, macAddress, batchId, posdeliverypointgroups, hash);

        }

        // Terminal - REQ
        [SoapRpcMethod(), WebMethod(EnableSession=false)]
        public ObyTypedResult<TerminalOperationMode> SetTerminalOperationMode(long timestamp, string macAddress, int terminalOperationMode, string hash)
        {
            this.SetNewRelicTransactionName("SetTerminalOperationMode");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { terminalOperationMode };
            // __OBYMOBI_USER_CODE_REGION_START
            // __OBYMOBI_USER_CODE_REGION_END

            return webserviceHandler.SetTerminalOperationMode(timestamp, macAddress, terminalOperationMode, hash);

        }

        // Terminal - REQ
        [SoapRpcMethod(), WebMethod(EnableSession=false)]
        public ObyTypedResult<TerminalOperationState> SetTerminalOperationState(long timestamp, string macAddress, int terminalOperationState, string hash)
        {
            this.SetNewRelicTransactionName("SetTerminalOperationState");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { terminalOperationState };
            // __OBYMOBI_USER_CODE_REGION_START
            // __OBYMOBI_USER_CODE_REGION_END

            return webserviceHandler.SetTerminalOperationState(timestamp, macAddress, terminalOperationState, hash);

        }

        [SoapRpcMethod(), WebMethod(EnableSession=false)]
        public ObyTypedResult<OrderRoutestephandlerSaveStatus> SetRoutestephandlerSaveStatuses(long timestamp, string macAddress, string xml, string hash)
        {
            this.SetNewRelicTransactionName("SetRoutestephandlerSaveStatuses");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { xml };
            // __OBYMOBI_USER_CODE_REGION_START

            // Deserialize the order save statuses
            OrderRoutestephandlerSaveStatus[] orderSaveStatuses = XmlHelper.Deserialize<OrderRoutestephandlerSaveStatus[]>(xml);

            // __OBYMOBI_USER_CODE_REGION_END

            return webserviceHandler.SetRoutestephandlerSaveStatuses(timestamp, macAddress, orderSaveStatuses, hash);

        }

        // Emenu
        [SoapRpcMethod(), WebMethod(EnableSession=false)]
        [ScriptMethod(UseHttpGet = true)]
        public ObyTypedResult<SimpleWebserviceResult> SetClientDeliverypoint(long timestamp, string macAddress, int deliverypointId, int deliverypointgroupId, string hash)
        {
            this.SetNewRelicTransactionName("SetClientDeliverypoint");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { deliverypointId, deliverypointgroupId };
            // __OBYMOBI_USER_CODE_REGION_START
            // __OBYMOBI_USER_CODE_REGION_END

            return webserviceHandler.SetClientDeliverypoint(timestamp, macAddress, deliverypointId, deliverypointgroupId, hash);

        }

        [SoapRpcMethod(), WebMethod(EnableSession=false)]
        [ScriptMethod(UseHttpGet = true)]
        public ObyTypedResult<SimpleWebserviceResult> SendMessage(long timestamp, string macAddress, string deliverypointNumbers, string deliverypointgroupIds,
            int clientId, int orderId, string title, string message, int duration, int mediaId, int categoryId, int entertainmentId, int productId, bool urgent, 
            bool saveMessage, bool mobileClients, string messagegroupIds, int messageLayoutType, string actions, string hash)
        {
            this.SetNewRelicTransactionName("SendMessage");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] {
                deliverypointNumbers, deliverypointgroupIds, clientId, orderId, title, message, duration, mediaId, categoryId, 
                entertainmentId, productId, urgent, saveMessage, mobileClients, messagegroupIds, messageLayoutType, actions };

            // __OBYMOBI_USER_CODE_REGION_START
            Action[] actionModels = XmlHelper.Deserialize<Action[]>(actions);
            // __OBYMOBI_USER_CODE_REGION_END

            return webserviceHandler.SendMessage(timestamp, macAddress, deliverypointNumbers, deliverypointgroupIds, clientId,
                orderId, title, message, duration, mediaId, categoryId, entertainmentId, productId, urgent, saveMessage, mobileClients, messagegroupIds, messageLayoutType, actionModels, hash);
        }

        [SoapRpcMethod(), WebMethod(EnableSession=false)]
        [ScriptMethod(UseHttpGet = true)]
        public ObyTypedResult<SimpleWebserviceResult> SetPmsFolio(long timestamp, string macAddress, string xml, string hash)
        {
            this.SetNewRelicTransactionName("SetPmsFolio");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { xml };
            // __OBYMOBI_USER_CODE_REGION_START

            Folio folio = XmlHelper.Deserialize<Folio>(xml);

            // __OBYMOBI_USER_CODE_REGION_END

            return webserviceHandler.SetPmsFolio(timestamp, macAddress, folio, hash);

        }

        [SoapRpcMethod(), WebMethod(EnableSession=false)]
        [ScriptMethod(UseHttpGet = true)]
        public ObyTypedResult<SimpleWebserviceResult> SetPmsGuestInformation(long timestamp, string macAddress, string xml, string hash)
        {
            this.SetNewRelicTransactionName("SetPmsGuestInformation");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { xml };
            // __OBYMOBI_USER_CODE_REGION_START

            GuestInformation guestInformation = XmlHelper.Deserialize<GuestInformation>(xml, false);

            // __OBYMOBI_USER_CODE_REGION_END

            return webserviceHandler.SetPmsGuestInformation(timestamp, macAddress, guestInformation, hash);

        }

        [SoapRpcMethod(), WebMethod(EnableSession=false)]
        [ScriptMethod(UseHttpGet = true)]
        public ObyTypedResult<SimpleWebserviceResult> SetPmsCheckedIn(long timestamp, string macAddress, string xml, string hash)
        {
            this.SetNewRelicTransactionName("SetPmsCheckedIn");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { xml };
            // __OBYMOBI_USER_CODE_REGION_START

            GuestInformation guestInformation = XmlHelper.Deserialize<GuestInformation>(xml);

            // __OBYMOBI_USER_CODE_REGION_END

            return webserviceHandler.SetPmsCheckedIn(timestamp, macAddress, guestInformation, hash);

        }

        [SoapRpcMethod(), WebMethod(EnableSession=false)]
        [ScriptMethod(UseHttpGet = true)]
        public ObyTypedResult<SimpleWebserviceResult> SetPmsCheckedOut(long timestamp, string macAddress, int deliverypointId, string deliverypointNumber, string hash)
        {
            this.SetNewRelicTransactionName("SetPmsCheckedOut");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { deliverypointId, deliverypointNumber };
            // __OBYMOBI_USER_CODE_REGION_START
            // __OBYMOBI_USER_CODE_REGION_END

            return webserviceHandler.SetPmsCheckedOut(timestamp, macAddress, deliverypointId, deliverypointNumber, hash);

        }

        [SoapRpcMethod(), WebMethod(EnableSession=false)]
        [ScriptMethod(UseHttpGet = true)]
        public ObyTypedResult<SimpleWebserviceResult> SetPmsExpressCheckedOut(long timestamp, string macAddress, string xml, string hash)
        {
            this.SetNewRelicTransactionName("SetPmsExpressCheckedOut");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { xml };
            // __OBYMOBI_USER_CODE_REGION_START

            Checkout checkoutInfo = XmlHelper.Deserialize<Checkout>(xml);

            // __OBYMOBI_USER_CODE_REGION_END

            return webserviceHandler.SetPmsExpressCheckedOut(timestamp, macAddress, checkoutInfo, hash);

        }

        [SoapRpcMethod(), WebMethod(EnableSession = false)]
        [ScriptMethod(UseHttpGet = true)]
        public ObyTypedResult<SimpleWebserviceResult> SetPmsWakeUp(long timestamp, string macAddress, string xml, string hash)
        {
            this.SetNewRelicTransactionName("SetPmsWakeUp");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { xml };
            // __OBYMOBI_USER_CODE_REGION_START

            WakeUpStatus wakeUpStatus = XmlHelper.Deserialize<WakeUpStatus>(xml);

            // __OBYMOBI_USER_CODE_REGION_END

            return webserviceHandler.SetPmsWakeUp(timestamp, macAddress, wakeUpStatus, hash);

        }

        [SoapRpcMethod(), WebMethod(EnableSession = false)]
        [ScriptMethod(UseHttpGet = true)]
        public ObyTypedResult<SimpleWebserviceResult> ClearPmsWakeUp(long timestamp, string macAddress, string xml, string hash)
        {
            this.SetNewRelicTransactionName("ClearPmsWakeUp");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { xml };
            // __OBYMOBI_USER_CODE_REGION_START

            WakeUpStatus wakeUpStatus = XmlHelper.Deserialize<WakeUpStatus>(xml);

            // __OBYMOBI_USER_CODE_REGION_END

            return webserviceHandler.ClearPmsWakeUp(timestamp, macAddress, wakeUpStatus, hash);

        }

        [SoapRpcMethod(), WebMethod(EnableSession = false)]
        [ScriptMethod(UseHttpGet = true)]
        public ObyTypedResult<SimpleWebserviceResult> SetClientWakeUpTime(long timestamp, string macAddress, string wakeUpTime, string hash)
        {
            this.SetNewRelicTransactionName("SetClientWakeUpTimer");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { wakeUpTime };
            // __OBYMOBI_USER_CODE_REGION_START
            // __OBYMOBI_USER_CODE_REGION_END

            return webserviceHandler.SetClientWakeUpTime(timestamp, macAddress, wakeUpTime, hash);

        }

        [SoapRpcMethod(), WebMethod(EnableSession=false)]
        [ScriptMethod(UseHttpGet = true)]
        public ObyTypedResult<SimpleWebserviceResult> SetForwardTerminal(long timestamp, string macAddress, int forwardToTerminalId, string hash)
        {
            this.SetNewRelicTransactionName("SetForwardTerminal");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { forwardToTerminalId };
            // __OBYMOBI_USER_CODE_REGION_START
            // __OBYMOBI_USER_CODE_REGION_END

            return webserviceHandler.SetForwardTerminal(timestamp, macAddress, forwardToTerminalId, hash);

        }

        [SoapRpcMethod(), WebMethod(EnableSession=false)]
        [ScriptMethod(UseHttpGet = true)]
        public ObyTypedResult<Order> GetOrderHistory(long timestamp, string macAddress, string hash)
        {
            this.SetNewRelicTransactionName("GetOrderHistory");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { };
            // __OBYMOBI_USER_CODE_REGION_START
            // __OBYMOBI_USER_CODE_REGION_END

            return webserviceHandler.GetOrderHistory(timestamp, macAddress, hash);

        }

        [SoapRpcMethod(), WebMethod(EnableSession = false)]
        [ScriptMethod(UseHttpGet = true)]
        public ObyTypedResult<SimpleWebserviceResult> SetEstimatedDeliveryTime(long timestamp, string macAddress, int deliverypointgroupId, int estimatedDeliveryTime, string hash)
        {
            this.SetNewRelicTransactionName("SetEstimatedDeliveryTime");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { deliverypointgroupId, estimatedDeliveryTime };
            // __OBYMOBI_USER_CODE_REGION_START
            // __OBYMOBI_USER_CODE_REGION_END

            return webserviceHandler.SetEstimatedDeliveryTime(timestamp, macAddress, deliverypointgroupId, estimatedDeliveryTime, hash);
        }

        [SoapRpcMethod(), WebMethod(EnableSession = false)]
        [ScriptMethod(UseHttpGet = true)]
        public ObyTypedResult<SimpleWebserviceResult> UpdateUIWidget(long timestamp, string macAddress, string json, string hash)
        {
            this.SetNewRelicTransactionName("UpdateUIWidget");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { json };
            // __OBYMOBI_USER_CODE_REGION_START

            JsonSerializerSettings settings = new JsonSerializerSettings();
            settings.NullValueHandling = NullValueHandling.Ignore;
            settings.ContractResolver = new CamelCasePropertyNamesContractResolver();
            UIWidget widget = JsonConvert.DeserializeObject<UIWidget>(json, settings);

            // __OBYMOBI_USER_CODE_REGION_END

            return webserviceHandler.UpdateUIWidget(timestamp, macAddress, widget, hash);
        }

        [SoapRpcMethod(), WebMethod(EnableSession = false)]
        [ScriptMethod(UseHttpGet = true)]
        public ObyTypedResult<SimpleWebserviceResult> UpdateAvailability(long timestamp, string macAddress, string json, string hash)
        {
            this.SetNewRelicTransactionName("UpdateAvailabilities");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { json };
            // __OBYMOBI_USER_CODE_REGION_START

            JsonSerializerSettings settings = new JsonSerializerSettings();
            settings.NullValueHandling = NullValueHandling.Ignore;
            settings.ContractResolver = new CamelCasePropertyNamesContractResolver();
            Availability availability = JsonConvert.DeserializeObject<Availability>(json, settings);

            // __OBYMOBI_USER_CODE_REGION_END

            return webserviceHandler.UpdateAvailability(timestamp, macAddress, availability, hash);
        }

        [SoapRpcMethod(), WebMethod(EnableSession = false)]
        [ScriptMethod(UseHttpGet = true)]
        public ObyTypedResult<SimpleWebserviceResult> SaveGuestInformations(long timestamp, string macAddress, string xml, string hash)
        {
            this.SetNewRelicTransactionName("SaveGuestInformations");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { xml };

            // __OBYMOBI_USER_CODE_REGION_START

            GuestInformation[] guestInformations = XmlHelper.Deserialize<GuestInformation[]>(xml);

            // __OBYMOBI_USER_CODE_REGION_END

            return webserviceHandler.SaveGuestInformations(timestamp, macAddress, guestInformations, hash);
        }

        [SoapRpcMethod(), WebMethod(EnableSession = false)]
        [ScriptMethod(UseHttpGet = true)]
        public ObyTypedResult<SimpleWebserviceResult> SaveFolios(long timestamp, string macAddress, string xml, string hash)
        {
            this.SetNewRelicTransactionName("SaveFolios");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { xml };

            // __OBYMOBI_USER_CODE_REGION_START

            Folio[] folios = XmlHelper.Deserialize<Folio[]>(xml);

            // __OBYMOBI_USER_CODE_REGION_END

            return webserviceHandler.SaveFolios(timestamp, macAddress, folios, hash);
        }

        [SoapRpcMethod(), WebMethod(EnableSession = false)]
        [ScriptMethod(UseHttpGet = true)]
        public ObyTypedResult<SimpleWebserviceResult> PublishMap(long timestamp, string macAddress, int mapId, string hash)
        {
            this.SetNewRelicTransactionName("PublishMap");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { mapId };

            // __OBYMOBI_USER_CODE_REGION_START            
            // __OBYMOBI_USER_CODE_REGION_END

            return webserviceHandler.PublishMap(timestamp, macAddress, mapId, hash);
        }

        #endregion

        #region MISC methods

        // Mobile
        // Emenu - deprecated - use MAC address
        [SoapRpcMethod(), WebMethod(EnableSession=false)]
        [ScriptMethod(UseHttpGet = true)]
        public ObyTypedResult<SimpleWebserviceResult> ProcessNetmessages(long timestamp, string macAddress, int customerId, int clientId, int companyId, int deliverypointId, int terminalId, string hash)
        {
            this.SetNewRelicTransactionName("ProcessNetmessages");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { customerId, clientId, companyId, deliverypointId, terminalId };
            // __OBYMOBI_USER_CODE_REGION_START
            // __OBYMOBI_USER_CODE_REGION_END

            return webserviceHandler.ProcessNetmessages(timestamp, macAddress, customerId, clientId, companyId, deliverypointId, terminalId, hash);

        }

        // Mobile
        // Emenu
        [SoapRpcMethod(), WebMethod(EnableSession=false)]
        [ScriptMethod(UseHttpGet = true)]
        public ObyTypedResult<SimpleWebserviceResult> VerifyNetmessage(long timestamp, string macAddress, int messageId, string hash)
        {
            this.SetNewRelicTransactionName("VerifyNetmessage");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { messageId };
            // __OBYMOBI_USER_CODE_REGION_START
            // __OBYMOBI_USER_CODE_REGION_END

            return webserviceHandler.VerifyNetmessage(timestamp, macAddress, messageId, hash);
        }

		// Emenu
		// OSS
		[SoapRpcMethod(), WebMethod(EnableSession=false)]
		[ScriptMethod(UseHttpGet = true)]
        public ObyTypedResult<SimpleWebserviceResult> VerifyNetmessageByGuid(long timestamp, string macAddress, string messageGuid, string trace, string hash)
		{
            this.SetNewRelicTransactionName("VerifyNetmessageByGuid");

			this.webserviceHandler.OriginalAdditionalParameters = new object[] { messageGuid, trace };
			// __OBYMOBI_USER_CODE_REGION_START
			// __OBYMOBI_USER_CODE_REGION_END

			return webserviceHandler.VerifyNetmessageByGuid(timestamp, macAddress, messageGuid, false, "", trace, hash);
		}

        // Emenu
        // OSS
        [SoapRpcMethod(), WebMethod(EnableSession=false)]
        [ScriptMethod(UseHttpGet = true)]
        public ObyTypedResult<SimpleWebserviceResult> CompleteNetmessageByGuid(long timestamp, string macAddress, string messageGuid, string errorMessage, string trace, string hash)
        {
            this.SetNewRelicTransactionName("CompleteNetmessageByGuid");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { messageGuid, errorMessage, trace };
            // __OBYMOBI_USER_CODE_REGION_START
            // __OBYMOBI_USER_CODE_REGION_END

            return webserviceHandler.VerifyNetmessageByGuid(timestamp, macAddress, messageGuid, true, errorMessage, trace, hash);
        }

            // Emenu
        [SoapRpcMethod(), WebMethod(EnableSession=false)]
        [ScriptMethod(UseHttpGet = true)]
        public ObyTypedResult<Message> GetMessages(long timestamp, string macAddress, int deliverypointId, string hash)
        {
            this.SetNewRelicTransactionName("GetMessages");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { deliverypointId };
            // __OBYMOBI_USER_CODE_REGION_START
            // __OBYMOBI_USER_CODE_REGION_END

            return webserviceHandler.GetMessages(timestamp, macAddress, deliverypointId, hash);
        }

        // Emenu
        [SoapRpcMethod, WebMethod(EnableSession=false)]
        [ScriptMethod(UseHttpGet = true)]
        public ObyTypedResult<SimpleWebserviceResult> RemoveMessages(long timestamp, string macAddress, string messageIds, int dialogCloseResult, string hash)
        {
            this.SetNewRelicTransactionName("RemoveMessages");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { messageIds, dialogCloseResult };
            // __OBYMOBI_USER_CODE_REGION_START
            // __OBYMOBI_USER_CODE_REGION_END

            return this.webserviceHandler.RemoveMessages(timestamp, macAddress, messageIds, dialogCloseResult, hash);
        }

        // Emenu
        [SoapRpcMethod(), WebMethod(EnableSession=false)]
        [ScriptMethod(UseHttpGet = true)]
        public ObyTypedResult<long> GetCurrentTime()
        {
            this.SetNewRelicTransactionName("GetCurrentTime");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { };
            // __OBYMOBI_USER_CODE_REGION_START
            // __OBYMOBI_USER_CODE_REGION_END

            return this.webserviceHandler.GetCurrentTime();
        }

        // Terminal - OSS
        [SoapRpcMethod(), WebMethod(EnableSession=false)]
        [ScriptMethod(UseHttpGet = true)]
        public ObyTypedResult<SimpleWebserviceResult> PublishSocialmediaMessage(long timestamp, string macAddress, int socialmediaMessageId, string hash)
        {
            this.SetNewRelicTransactionName("PublishSocialmediaMessage");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { socialmediaMessageId };
            // __OBYMOBI_USER_CODE_REGION_START
            // __OBYMOBI_USER_CODE_REGION_END

            return new ObyTypedResult<SimpleWebserviceResult>();

        }

        // Terminal - OSS
        [SoapRpcMethod(), WebMethod(EnableSession=false)]
        [ScriptMethod(UseHttpGet = true)]
        public ObyTypedResult<SimpleWebserviceResult> RemoveSocialmediaMessage(long timestamp, string macAddress, int socialmediaMessageId, string hash)
        {
            this.SetNewRelicTransactionName("RemoveSocialmediaMessage");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { socialmediaMessageId };
            // __OBYMOBI_USER_CODE_REGION_START
            // __OBYMOBI_USER_CODE_REGION_END

            return new ObyTypedResult<SimpleWebserviceResult>();

        }

        // Terminal - OSS / REQ
        [SoapRpcMethod(), WebMethod(EnableSession=false)]
        public ObyTypedResult<SimpleWebserviceResult> VerifyTerminal(long timestamp, string macAddress, int terminalId, string hash)
        {
            this.SetNewRelicTransactionName("VerifyTerminal");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { terminalId };
            // __OBYMOBI_USER_CODE_REGION_START
            // __OBYMOBI_USER_CODE_REGION_END

            return webserviceHandler.VerifyTerminal(timestamp, macAddress, terminalId, hash);
        }

        // Terminal - OSS / REQ
        [SoapRpcMethod(), WebMethod(EnableSession=false)]
        public ObyTypedResult<SimpleWebserviceResult> CleanUpPosData(long timestamp, string macAddress, string hash)
        {
            this.SetNewRelicTransactionName("CleanUpPosData");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { };
            // __OBYMOBI_USER_CODE_REGION_START
            // __OBYMOBI_USER_CODE_REGION_END

            return webserviceHandler.CleanUpPosData(timestamp, macAddress, hash);
        }

        // Terminal - REQ
        [SoapRpcMethod(), WebMethod(EnableSession=false)]
        public ObyTypedResult<SimpleWebserviceResult> WriteToLog(long timestamp, string macAddress, int companyId, int terminalId, int orderId, string orderGuid,
            int type, string message, string status, string log, string hash)
        {
            this.SetNewRelicTransactionName("WriteToLog");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { 
                companyId, terminalId, orderId, orderGuid, type, message, status, log };
            // __OBYMOBI_USER_CODE_REGION_START
            // __OBYMOBI_USER_CODE_REGION_END

            return webserviceHandler.WriteToLog(timestamp, macAddress, terminalId, orderId, orderGuid, type, message, status, log, hash);

        }

        [SoapRpcMethod(), WebMethod(EnableSession=false)]
        public ObyTypedResult<SimpleWebserviceResult> WriteToLogZipped(long timestamp, string macAddress, int terminalId, int type, string message, string base64Zip, long fileDate, string hash)
        {
            this.SetNewRelicTransactionName("WriteToLogZipped");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { terminalId, type, message, fileDate };
            // __OBYMOBI_USER_CODE_REGION_START
            // __OBYMOBI_USER_CODE_REGION_END

            return webserviceHandler.WriteToLogZipped(timestamp, macAddress, terminalId, type, message, base64Zip, fileDate, hash);
        }

        // Emenu
        // Mobile?
        [SoapRpcMethod(), WebMethod(EnableSession=false)]
        public ObyTypedResult<SimpleWebserviceResult> SaveFBCode(string credentials, string code)
        {
            this.SetNewRelicTransactionName("SaveFBCode");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { };
            // __OBYMOBI_USER_CODE_REGION_START
            // __OBYMOBI_USER_CODE_REGION_END

            return new ObyTypedResult<SimpleWebserviceResult>();

        }

        [SoapRpcMethod(), WebMethod(EnableSession=false)]
        public ObyTypedResult<SimpleWebserviceResult> ProcessOrders(long timestamp, string macAddress, int companyId, int clientId, string hash)
        {
            this.SetNewRelicTransactionName("ProcessOrders");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { companyId, clientId };
            // __OBYMOBI_USER_CODE_REGION_START
            // __OBYMOBI_USER_CODE_REGION_END

            return webserviceHandler.ProcessOrders(timestamp, macAddress, clientId, hash);

        }

        [SoapRpcMethod(), WebMethod(EnableSession=false)]
        public ObyTypedResult<SimpleWebserviceResult> CancelOrder(long timestamp, string macAddress, int clientId, int customerId, string orderGuid, string hash)
        {
            this.SetNewRelicTransactionName("CancelOrder");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { clientId, customerId, orderGuid };
            // __OBYMOBI_USER_CODE_REGION_START
            // __OBYMOBI_USER_CODE_REGION_END

            return webserviceHandler.CancelOrder(timestamp, macAddress, clientId, customerId, orderGuid, hash);

        }

        [SoapRpcMethod(), WebMethod(EnableSession=false)]
        public ObyTypedResult<SimpleWebserviceResult> ManuallyProcessOrder(long timestamp, string macAddress, int companyId, int terminalId, string orderGuid, string hash)
        {
            this.SetNewRelicTransactionName("ManuallyProcessOrder");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { companyId, terminalId, orderGuid };
            // __OBYMOBI_USER_CODE_REGION_START
            // __OBYMOBI_USER_CODE_REGION_END

            return webserviceHandler.ManuallyProcessOrder(timestamp, macAddress, orderGuid, hash);


        }

        [SoapRpcMethod(), WebMethod(EnableSession=false)]
        public ObyTypedResult<SimpleWebserviceResult> UploadFile(long timestamp, string macAddress, string file, string filename, string hash)
        {
            this.SetNewRelicTransactionName("UploadFile");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { file, filename, };
            // __OBYMOBI_USER_CODE_REGION_START
            // __OBYMOBI_USER_CODE_REGION_END

            return webserviceHandler.UploadFile(timestamp, macAddress, file, filename, hash);

        }

        [SoapRpcMethod(), WebMethod(EnableSession=false)]
        public ObyTypedResult<SimpleWebserviceResult> UploadFileInPost()
        {
            this.SetNewRelicTransactionName("UploadFileInPost");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { };
            // __OBYMOBI_USER_CODE_REGION_START
            // __OBYMOBI_USER_CODE_REGION_END

            return webserviceHandler.UploadFileInPost();

        }

        [SoapRpcMethod(), WebMethod(EnableSession=false)]
        public ObyTypedResult<SimpleWebserviceResult> PrintFile()
        {
            this.SetNewRelicTransactionName("PrintFile");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { };
            // __OBYMOBI_USER_CODE_REGION_START
            // __OBYMOBI_USER_CODE_REGION_END

            return webserviceHandler.PrintFile();

        }

        [SoapRpcMethod(), WebMethod(EnableSession = false)]
        public ObyTypedResult<SimpleWebserviceResult> PrintPdf()
        {
            this.webserviceHandler.OriginalAdditionalParameters = new object[] { };
            // __OBYMOBI_USER_CODE_REGION_START
            // __OBYMOBI_USER_CODE_REGION_END

            return webserviceHandler.PrintPdf();

        }

        [SoapRpcMethod(), WebMethod(EnableSession=false)]
        public ObyTypedResult<SimpleWebserviceResult> EmailFile()
        {
            this.SetNewRelicTransactionName("EmailFile");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { };
            // __OBYMOBI_USER_CODE_REGION_START
            // __OBYMOBI_USER_CODE_REGION_END

            return webserviceHandler.EmailFile();

        }

        [SoapRpcMethod(), WebMethod(EnableSession=false)]
        public ObyTypedResult<SimpleWebserviceResult> SetOrderPrinted(long timestamp, string macAddress, int orderId, string hash)
        {
            this.SetNewRelicTransactionName("SetOrderPrinted");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { orderId };
            // __OBYMOBI_USER_CODE_REGION_START
            // __OBYMOBI_USER_CODE_REGION_END

            return webserviceHandler.SetOrderPrinted(timestamp, macAddress, orderId, hash);

        }

        [SoapRpcMethod(), WebMethod(EnableSession=false)]
        public ObyTypedResult<Analytics> GetAnalytics(long timestamp, string macAddress, string hash)
        {
            this.SetNewRelicTransactionName("GetAnalytics");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { };
            // __OBYMOBI_USER_CODE_REGION_START
            // __OBYMOBI_USER_CODE_REGION_END

            return webserviceHandler.GetAnalytics(timestamp, macAddress, hash);

        }

        [SoapRpcMethod(), WebMethod(EnableSession=false)]
        public ObyTypedResult<SimpleWebserviceResult> SetMobileOrdering(long timestamp, string macAddress, bool enabled, string hash)
        {
            this.SetNewRelicTransactionName("SetMobileOrdering");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { enabled };
            // __OBYMOBI_USER_CODE_REGION_START
            // __OBYMOBI_USER_CODE_REGION_END

            return webserviceHandler.SetMobileOrdering(timestamp, macAddress, enabled, hash);

        }

        [SoapRpcMethod(), WebMethod(EnableSession=false)]
        public ObyTypedResult<SimpleWebserviceResult> UpdateLastRequest(long timestamp, string macAddress, int clientId, int terminalId, string hash)
        {
            this.SetNewRelicTransactionName("UpdateLastRequest");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { clientId, terminalId };

            // __OBYMOBI_USER_CODE_REGION_START
            // __OBYMOBI_USER_CODE_REGION_END

            return webserviceHandler.UpdateLastRequest(timestamp, macAddress, clientId, terminalId, hash);
        }

        [SoapRpcMethod(), WebMethod(EnableSession=false)]
        public ObyTypedResult<Release> GetRelease(long timestamp, string macAddress, string applicationCode, string hash)
        {
            this.SetNewRelicTransactionName("GetRelease");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { applicationCode };

            // __OBYMOBI_USER_CODE_REGION_START
            // __OBYMOBI_USER_CODE_REGION_END

            return webserviceHandler.GetRelease(timestamp, macAddress, applicationCode, hash);
        }

        [SoapRpcMethod(), WebMethod(EnableSession=false)]
        public ObyTypedResult<SimpleWebserviceResult> SetUpdateStatus(long timestamp, string macAddress, string applicationCode, int updateStatus, string customMessage, int releaseId, string hash)
        {
            this.SetNewRelicTransactionName("SetUpdateStatus");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { applicationCode, updateStatus, customMessage, releaseId };

            // __OBYMOBI_USER_CODE_REGION_START
            // __OBYMOBI_USER_CODE_REGION_END

            return webserviceHandler.SetUpdateStatus(timestamp, macAddress, applicationCode, updateStatus, customMessage, releaseId, hash);
        }

        [SoapRpcMethod, WebMethod(EnableSession=false)]
        public ObyTypedResult<SimpleWebserviceResult> SetApplicationVersion(long timestamp, string macAddress, string applicationCode, string versionNumber, string hash)
        {
            this.SetNewRelicTransactionName("SetApplicationVersion");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { applicationCode, versionNumber };

            // __OBYMOBI_USER_CODE_REGION_START
            // __OBYMOBI_USER_CODE_REGION_END

            return webserviceHandler.SetApplicationVersion(timestamp, macAddress, applicationCode, versionNumber, hash);
        }

        [SoapRpcMethod, WebMethod(EnableSession=false)]
        public ObyTypedResult<SimpleWebserviceResult> SetAgentCommandReponse(long timestamp, string macAddress, string resultMessage, string hash)
        {
            this.SetNewRelicTransactionName("SetAgentCommandResponse");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { resultMessage };

            // __OBYMOBI_USER_CODE_REGION_START
            // __OBYMOBI_USER_CODE_REGION_END

            return webserviceHandler.SetAgentCommandReponse(timestamp, macAddress, resultMessage, hash);
        }

        [SoapRpcMethod(), WebMethod(EnableSession=false)]
        [ScriptMethod(UseHttpGet = true)]
        public ObyTypedResult<SimpleWebserviceResult> DailyOrderReset(long timestamp, string macAddress, string deliverypointNumbers, string deliverypointgroupIds, string hash)
        {
            this.SetNewRelicTransactionName("DailyOrderReset");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { deliverypointNumbers, deliverypointgroupIds };
            // __OBYMOBI_USER_CODE_REGION_START
            // __OBYMOBI_USER_CODE_REGION_END

            return webserviceHandler.DailyOrderReset(timestamp, macAddress, deliverypointNumbers, deliverypointgroupIds, hash);
        }

        [SoapRpcMethod(), WebMethod(EnableSession = false)]
        [ScriptMethod(UseHttpGet = true)]
        public ObyTypedResult<SimpleWebserviceResult> ClearSendMessages(long timestamp, string macAddress, string deliverypointNumbers, string deliverypointgroupIds, string messageGroupIds, string hash)
        {
            this.SetNewRelicTransactionName("ClearSendMessages");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { deliverypointNumbers, deliverypointgroupIds, messageGroupIds };
            // __OBYMOBI_USER_CODE_REGION_START
            // __OBYMOBI_USER_CODE_REGION_END

            return webserviceHandler.SendClearMessages(timestamp, macAddress, deliverypointNumbers, deliverypointgroupIds, messageGroupIds, hash);
        }

        #endregion

        #region Helper methods

        private void SetNewRelicTransactionName(string methodName)
        {
            //NewRelic.Api.Agent.NewRelic.SetTransactionName("Other", string.Format("/api/30.0/CraveService/{0}", methodName));
        }

        #endregion
    }
}
