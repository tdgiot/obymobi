﻿using System;
using System.Collections.Generic;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.Services.Protocols;
using Dionysos;
using Handlers;
using Obymobi.Logic.HelperClasses;
using Obymobi.Logic.Model;

/// <summary>
///     Summary description for SetupService
/// </summary>
[WebService(Namespace = "http://crave-emenu.com/webservices/")]
[ScriptService]
[SoapRpcService(RoutingStyle = SoapServiceRoutingStyle.RequestElement)]
public class SetupService : WebService
{
    #region  Fields

    private static readonly JavaScriptSerializer scriptSerializer = new JavaScriptSerializer();
    private static readonly SetupWebserviceHandler setupHandler = new SetupWebserviceHandler();

    #endregion

    [SoapRpcMethod, WebMethod(EnableSession = false)]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public void GetTimeUtc(long timestamp, string hash)
    {
        DateContainer container = new DateContainer(DateTime.UtcNow);
        WriteJson(container);
    }

    [SoapRpcMethod, WebMethod(EnableSession = false)]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public void GetAppVersionsJson(long timestamp, string appCode, string hash)
    {
        WebserviceHelper.ValidateRequestSetupService(timestamp, hash, appCode);

        List<SetupWebserviceHandler.AppVersion> appList = SetupService.setupHandler.GetAppVersions(appCode);
        WriteJson(appList);
    }

    [SoapRpcMethod, WebMethod(EnableSession = false)]
    [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public void GetConfiguration(long timestamp, string macAddr, string setupCode, string hash)
    {
        WebserviceHelper.ValidateRequestSetupService(timestamp, hash, macAddr, setupCode);

        EmenuConfiguration emenuConfig = SetupService.setupHandler.GetConfiguration(macAddr, setupCode);
        WriteJson(emenuConfig);
    }

    #region Private Helpers

    private void WriteJson(object data)
    {
        Context.Response.Clear();
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(SetupService.scriptSerializer.Serialize(data));
    }

    #endregion
}

public class DateContainer
{
    #region  Fields

    public long Unix;

    #endregion

    #region Constructors

    public DateContainer(DateTime dt)
    {
        Unix = dt.ToUnixTime();
    }

    #endregion
}