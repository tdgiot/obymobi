﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Threading;
using System.Web;
using Dionysos;
using Dionysos.Web;
using Obymobi;
using Obymobi.Constants;
using Obymobi.Logging;
using Obymobi.Security;

public partial class ClearCache : System.Web.UI.Page
{
    private const bool DEBUG_MODE = false;

    public enum ClearCacheResult
    {
        MissingParameters = 200,
        RequestHashIsInvalid = 300,
        TimestampIsOutOfRange = 400
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        Response.Clear();
        Response.ContentType = "text/plain";

        if (QueryStringHelper.HasValue("lastcacheclear"))
        {
            Response.Write(CacheHelper.GetLastCacheCleared());
            Response.StatusCode = 200;
        }
        else if (QueryStringHelper.HasValue("appstarted"))
        {
            string appStarted;
            if (HttpContext.Current.Application["APPLICATION_STARTED"] == null)
                appStarted = "Never";
            else
                appStarted = (string)HttpContext.Current.Application["APPLICATION_STARTED"];

            Response.Write(appStarted);
            Response.StatusCode = 200;
        }
        else if (QueryStringHelper.HasValue("redis"))
        {
            if (Obymobi.Web.Caching.RedisCacheHelper.Instance.RedisEnabled)
            {
                Obymobi.Web.Caching.RedisCacheHelper.Instance.Add("TestKey-API", DateTime.Now.ToString("yy-MM-dd HH:mm:ss"));
                Response.Write("OK");
            }
            else
            {
                Response.Write("Redis not enabled");
            }
            Response.StatusCode = 200;
        }
        else if (QueryStringHelper.HasValue("clearmemory"))
        {
            List<string> keyList = new List<string>();
            IDictionaryEnumerator cacheEnum = HttpContext.Current.Cache.GetEnumerator();
            while (cacheEnum.MoveNext())
            {
                keyList.Add(cacheEnum.Key.ToString());
            }

            foreach (string key in keyList)
            {
                HttpContext.Current.Cache.Remove(key);
            }
        }
        else if (ProcessRequest())
        {
            Response.Write("OK");
            Response.StatusCode = 200;
        }
        else
        {
            Response.Write("FAILED");
            Response.StatusCode = 500;
        }

        Response.End();
    }

    private bool ProcessRequest()
    {
        int timestamp = 0;
        string request = "";
        string hash = string.Empty;        

        if (!QueryStringHelper.TryGetValue("timestamp", out timestamp))
        {
            if (DEBUG_MODE)
                throw new ObymobiException(ClearCacheResult.MissingParameters, "timestamp");

            return false;
        }                

        if (!QueryStringHelper.TryGetValue("hash", out hash))
        {
            if (DEBUG_MODE)
                throw new ObymobiException(ClearCacheResult.MissingParameters, "hash");
            
            return false;
        }

        QueryStringHelper.TryGetValue("request", out request);
        if (request == null)
            request = "";        
        
        if (Hasher.IsHashValidForParameters(hash, ObymobiConstants.GenericSalt, timestamp, request))
        {
            DateTime requestDateTime = DateTimeUtil.FromUnixTime(timestamp);
            TimeSpan requestAge = (DateTime.UtcNow - requestDateTime);
            if (requestAge.TotalMinutes > 5 || requestAge.TotalMinutes < -5)
            {
                if (DEBUG_MODE)
                    throw new ObymobiException(ClearCacheResult.TimestampIsOutOfRange, "Timestamp is: '{0}', Age: '{1}m' - Server time: '{2}'", requestDateTime, requestAge.ToString(), DateTime.UtcNow);

					return false;
            }

            int companyId;
            if (QueryStringHelper.TryGetValue("companyId", out companyId))
            {
                Requestlogger.CreateRequestlog("ClearCache for company: " + companyId, timestamp, hash);

                Obymobi.Logic.HelperClasses.CacheHelper.RemoveCacheObjectsForCompany(companyId);
            }
			else if (request.IsNullOrWhiteSpace())
            {
                Requestlogger.CreateRequestlog("ClearCache", timestamp, hash);
                CacheHelper.Clear();    
            }
            else if (request.Equals("recycle"))
            {
                Requestlogger.CreateRequestlog("RecycleApplication", timestamp, request, hash);

                System.Threading.Tasks.Task.Factory.StartNew(() =>
                                                             {
                                                                 Thread.Sleep(500);
                                                                 this.KillProcesses();
                                                             });

                return true;                
            }
        }
        else
        {
            if (DEBUG_MODE)
                throw new ObymobiException(ClearCacheResult.RequestHashIsInvalid, "The supplied hash is invalid: '{0}' for '{1}'.", hash, timestamp);

				return false;
        }

        return true;
    }

    private void KillProcesses()
    {
        Process[] processes = Process.GetProcessesByName("w3wp");
        foreach (Process process in processes)
        {
            try
            {
                process.Kill();
            }
            catch
            {

            }
        }
    }

    private bool RecycleApplication()
    {
        bool error = false;
        try
        {
            this.KillProcesses();
        }
        catch
        {
            error = true;
        }

        if (error)
        {
            // Couldn't unload with Runtime - let's try modifying web.config
            string configPath = HttpContext.Current.Request.PhysicalApplicationPath + "\\web.config";
            try
            {
                File.SetLastWriteTimeUtc(configPath, DateTime.UtcNow);
            }
            catch
            {
                error = true;
            }
        }

        return !error;
    }
}
