﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.IO;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.HelperClasses;
using Dionysos.Web;

[WebService(Namespace = "http://obymobi.com/webservices/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
public class ApiAuthenticationWebService : System.Web.Services.WebService
{
    [WebMethod]
    public void SaveGoogleCode(int state)
    {
        ApiAuthenticationWebserviceHandler.SaveGoogleCode(state, this.Context.Request["error"], this.Context.Request["code"]);
    }
}
