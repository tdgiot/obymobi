﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

// NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "NocWebservice" in code, svc and config file together.
public class NocWebservice : INocWebservice
{
	public bool IsAlive()
	{
        return NocWebserviceHandler.IsAlive();
	}

	public bool IsDatabaseAlive()
	{
        return NocWebserviceHandler.IsDatabaseAlive();
	}

	public List<NocWebserviceHandler.CompanyInfo> GetCompaniesInfo()
    {
        return NocWebserviceHandler.GetCompaniesInfo();
    }
}
