﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="500.aspx.cs" Inherits="Obymobi.Api.WebApplication._500" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            Oops, something went wrong. It shouldn't, but it just did. A notification has been sent to the development team.
        </div>
        <div>
            <D:Label runat="server" ID="lblException"></D:Label>
        </div>
    </form>
</body>
</html>
