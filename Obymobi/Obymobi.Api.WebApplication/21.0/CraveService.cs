using System;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.Services.Protocols;
using Obymobi;
using Obymobi.Enums;
using Obymobi.Logic;
using Obymobi.Logic.Model.v21;
using Obymobi.Logic.Model.v21.Converters;
using ObymobiWebservice.Handlers;

namespace ObymobiWebservice.WebServices.v21
{
    /// <summary>
    /// Summary description for Service
    /// </summary>
    [WebService(Namespace = "http://crave-emenu.com/webservices/")]
    //[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    [SoapRpcService(RoutingStyle = SoapServiceRoutingStyle.RequestElement)]
    public class CraveService : System.Web.Services.WebService
    {
        #region Fields

        private WebserviceHandler webserviceHandler = new WebserviceHandler();

        #endregion

        [SoapRpcMethod(), WebMethod(EnableSession=false)]
        [ScriptMethod(UseHttpGet = true)]
        public DateTime TestMethod()
        {
            //CometHelper.AnnounceNewMessage(220, 2025, 15194);
            return DateTime.Now;
        }

        #region GET methods

        // Generic
        [SoapRpcMethod(), WebMethod(EnableSession=false)]
        [ScriptMethod(UseHttpGet = true)]
        public bool IsWebserviceAlive()
        {
            this.SetNewRelicTransactionName("IsWebserviceAlive");

            // __OBYMOBI_USER_CODE_REGION_START
            // __OBYMOBI_USER_CODE_REGION_END
            
            return this.webserviceHandler.IsWebserviceAlive();
        }

        // General
        [SoapRpcMethod(), WebMethod(EnableSession=false)]
        [ScriptMethod(UseHttpGet = true)]
        public bool IsAlive()
        {
            this.SetNewRelicTransactionName("IsAlive");

            // __OBYMOBI_USER_CODE_REGION_START
            // __OBYMOBI_USER_CODE_REGION_END

            return this.webserviceHandler.IsWebserviceAlive();
        }

        // Generic
        [SoapRpcMethod(), WebMethod(EnableSession=false)]
        [ScriptMethod(UseHttpGet = true)]
        public bool IsDatabaseAlive()
        {
            this.SetNewRelicTransactionName("IsDatabaseAlive");

            // __OBYMOBI_USER_CODE_REGION_START
            // __OBYMOBI_USER_CODE_REGION_END

            return this.webserviceHandler.IsDatabaseAlive();
        }

        // Generic
        [SoapRpcMethod(), WebMethod(EnableSession=false)]
        [ScriptMethod(UseHttpGet = true)]
        public string GetReleaseAndDownloadLocation(long timestamp, string macAddress, string applicationCode, string hash)
        {
            this.SetNewRelicTransactionName("GetReleaseAndDownloadLocation");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { applicationCode };

            // __OBYMOBI_USER_CODE_REGION_START
            // __OBYMOBI_USER_CODE_REGION_END

            return this.webserviceHandler.GetReleaseAndDownloadLocation(timestamp, macAddress, applicationCode, hash);
        }

        // Terminal
        [SoapRpcMethod(), WebMethod(EnableSession=false)]
        [ScriptMethod(UseHttpGet = true)]
        public ObyTypedResult<Client> GetClients(long timestamp, string macAddress, string hash)
        {
            this.SetNewRelicTransactionName("GetClients");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { };

            // __OBYMOBI_USER_CODE_REGION_START
            // __OBYMOBI_USER_CODE_REGION_END

            return new ClientConverter().ConvertResultToLegacyResult(this.webserviceHandler.GetClients(timestamp, macAddress, hash));          
        }

        // Otoucho 
        // Emenu - deprecated - use MAC address
        [SoapRpcMethod(), WebMethod(EnableSession=false)]
        [ScriptMethod(UseHttpGet = true)]
        public ObyTypedResult<Client> GetClient(long timestamp, string macAddress, int clientId, string clientmacAddress, int deliverypointgroupId, int deliverypointId, string applicationVersion, string osVersion, int deviceType, int deviceModel, string hash)
        {
            this.SetNewRelicTransactionName("GetClient");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { clientId, clientmacAddress, deliverypointgroupId, deliverypointId, applicationVersion, osVersion, deviceType, deviceModel };

            // __OBYMOBI_USER_CODE_REGION_START            
            // __OBYMOBI_USER_CODE_REGION_END

            return new ClientConverter().ConvertResultToLegacyResult(this.webserviceHandler.GetClient(timestamp, macAddress, clientId, clientmacAddress, deliverypointgroupId, deliverypointId, applicationVersion, osVersion, deviceType, deviceModel, hash));

        }

        // Emenu
        [SoapRpcMethod(), WebMethod(EnableSession=false)]
        [ScriptMethod(UseHttpGet = true)]
        public ObyTypedResult<Company> GetCompanies(long timestamp, string username, string password, string hash)
        {
            this.SetNewRelicTransactionName("GetCompanies");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { };
            // __OBYMOBI_USER_CODE_REGION_START
            // __OBYMOBI_USER_CODE_REGION_END          
            return new CompanyConverter().ConvertResultToLegacyResult(this.webserviceHandler.GetCompanies(timestamp, username, password, hash));

        }

        // Otoucho
        // Emenu - deprecated - use MAC address
        [SoapRpcMethod(), WebMethod(EnableSession=false)]
        [ScriptMethod(UseHttpGet = true)]
        public ObyTypedResult<Company> GetCompany(long timestamp, string macAddress, int companyId, string hash)
        {
            this.SetNewRelicTransactionName("GetCompany");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { companyId };
            // __OBYMOBI_USER_CODE_REGION_START
            // __OBYMOBI_USER_CODE_REGION_END
            return new CompanyConverter().ConvertResultToLegacyResult(this.webserviceHandler.GetCompany(timestamp, macAddress, companyId, hash));

        }

        // Otoucho
        // Emenu - deprecated - use MAC address
        [SoapRpcMethod(), WebMethod(EnableSession=false)]
        [ScriptMethod(UseHttpGet = true)]
        public ObyTypedResult<Advertisement> GetAdvertisements(long timestamp, string macAddress, string hash)
        {
            this.SetNewRelicTransactionName("GetAdvertisements");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { };
            // __OBYMOBI_USER_CODE_REGION_START
            // __OBYMOBI_USER_CODE_REGION_END

            return new AdvertisementConverter().ConvertResultToLegacyResult(this.webserviceHandler.GetAdvertisements(timestamp, macAddress, hash));

        }

        // Otoucho
        // Emenu - deprecated - use MAC address
        [SoapRpcMethod(), WebMethod(EnableSession=false)]
        [ScriptMethod(UseHttpGet = true)]
        public ObyTypedResult<Entertainment> GetEntertainment(long timestamp, string macAddress, string hash)
        {
            this.SetNewRelicTransactionName("GetEntertainment");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { };
            // __OBYMOBI_USER_CODE_REGION_START
            // __OBYMOBI_USER_CODE_REGION_END

            return new EntertainmentConverter().ConvertResultToLegacyResult(this.webserviceHandler.GetEntertainment(timestamp, macAddress, hash));

        }

        // Otoucho
        // Emenu - deprecated - use MAC address
        [SoapRpcMethod(), WebMethod(EnableSession=false)]
        [ScriptMethod(UseHttpGet = true)]
        public ObyTypedResult<Paymentmethod> GetPaymentmethods(long timestamp, string macAddress, string hash)
        {
            this.SetNewRelicTransactionName("GetPaymentmethods");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { };
            // __OBYMOBI_USER_CODE_REGION_START
            // __OBYMOBI_USER_CODE_REGION_END

            return new PaymentmethodConverter().ConvertResultToLegacyResult(this.webserviceHandler.GetPaymentmethods(timestamp, macAddress, hash));

        }

        // Otoucho
        // Emenu - deprecated - use MAC address
        [SoapRpcMethod(), WebMethod(EnableSession=false)]
        [ScriptMethod(UseHttpGet = true)]
        public ObyTypedResult<ProductmenuItem> GetMenu(long timestamp, string macAddress, int deliverypointgroupId, string hash)
        {
            this.SetNewRelicTransactionName("GetMenu");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { deliverypointgroupId };
            // __OBYMOBI_USER_CODE_REGION_START
            // __OBYMOBI_USER_CODE_REGION_END

            return new ProductmenuItemConverter().ConvertResultToLegacyResult(this.webserviceHandler.GetMenu(timestamp, macAddress, deliverypointgroupId, false, hash));

        }

		[SoapRpcMethod(), WebMethod(EnableSession=false)]
		[ScriptMethod(UseHttpGet = true)]
		public ObyTypedResult<Netmessage> GetNetmessages(long timestamp, string macAddress, int customerId, int clientId, int companyId, int deliverypointId, int terminalId, string hash)
		{
            this.SetNewRelicTransactionName("GetNetmessages");

			this.webserviceHandler.OriginalAdditionalParameters = new object[] { customerId, clientId, companyId, deliverypointId, terminalId };

			// __OBYMOBI_USER_CODE_REGION_START
			// __OBYMOBI_USER_CODE_REGION_END

			return new NetmessageConverter().ConvertResultToLegacyResult(this.webserviceHandler.GetNetmessages(timestamp, macAddress, customerId, clientId, companyId, deliverypointId, terminalId, hash));
		}

			// Otoucho
        // Emenu - deprecated - use MAC address
        [SoapRpcMethod(), WebMethod(EnableSession=false)]
        [ScriptMethod(UseHttpGet = true)]
        public ObyTypedResult<Survey> GetSurveys(long timestamp, string macAddress, string hash)
        {
            this.SetNewRelicTransactionName("GetSurveys");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { };
            // __OBYMOBI_USER_CODE_REGION_START
            // __OBYMOBI_USER_CODE_REGION_END

            return new SurveyConverter().ConvertResultToLegacyResult(this.webserviceHandler.GetSurveys(timestamp, macAddress, hash));

        }

        // Emenu
        [SoapRpcMethod(), WebMethod(EnableSession=false)]
        [ScriptMethod(UseHttpGet = true)]
        public ObyTypedResult<Announcement> GetAnnouncements(long timestamp, string macAddress, string hash)
        {
            this.SetNewRelicTransactionName("GetAnnouncements");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { };
            // __OBYMOBI_USER_CODE_REGION_START
            // __OBYMOBI_USER_CODE_REGION_END

            return new AnnouncementConverter().ConvertResultToLegacyResult(this.webserviceHandler.GetAnnouncements(timestamp, macAddress, hash));

        }

        // Emenu
        [SoapRpcMethod(), WebMethod(EnableSession=false)]
        [ScriptMethod(UseHttpGet = true)]
        public ObyTypedResult<Deliverypoint> GetDeliverypoints(long timestamp, string macAddress, int deliverypointgroupId, string hash)
        {
            this.SetNewRelicTransactionName("GetDeliverypoints");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { deliverypointgroupId };
            // __OBYMOBI_USER_CODE_REGION_START
            // __OBYMOBI_USER_CODE_REGION_END

            return new DeliverypointConverter().ConvertResultToLegacyResult(this.webserviceHandler.GetDeliverypoints(timestamp, macAddress, deliverypointgroupId, hash));

        }

        // Terminal
        [SoapRpcMethod(), WebMethod(EnableSession=false)]
        [ScriptMethod(UseHttpGet = true)]
        public ObyTypedResult<Terminal> GetTerminal(long timestamp, string macAddress, int terminalId, string applicationVersion, string osVersion, int deviceType, string hash)
        {
            this.SetNewRelicTransactionName("GetTerminal");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { terminalId, applicationVersion, osVersion, deviceType };
            // __OBYMOBI_USER_CODE_REGION_START
            // __OBYMOBI_USER_CODE_REGION_END

            return new TerminalConverter().ConvertResultToLegacyResult(this.webserviceHandler.GetTerminal(timestamp, macAddress, terminalId, applicationVersion, osVersion, deviceType, hash));
        }

        // Terminal - OSS
        [SoapRpcMethod(), WebMethod(EnableSession=false)]
        [ScriptMethod(UseHttpGet = true)]
        public ObyTypedResult<PosIntegrationInformation> GetPosIntegrationInformation(long timestamp, string macAddress, string hash)
        {
            this.SetNewRelicTransactionName("GetPosIntegrationInformation");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { };
            // __OBYMOBI_USER_CODE_REGION_START
            // __OBYMOBI_USER_CODE_REGION_END

            return new PosIntegrationInformationConverter().ConvertResultToLegacyResult(this.webserviceHandler.GetPosIntegrationInformation(timestamp, macAddress, hash));

        }

        // Terminal - OSS
        [SoapRpcMethod(), WebMethod(EnableSession=false)]
        [ScriptMethod(UseHttpGet = true)]
        public ObyTypedResult<ClientStatus> GetClientStatuses(long timestamp, string macAddress, string hash)
        {
            this.SetNewRelicTransactionName("GetClientStatuses");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { };
            // __OBYMOBI_USER_CODE_REGION_START
            // __OBYMOBI_USER_CODE_REGION_END

            return new ClientStatusConverter().ConvertResultToLegacyResult(this.webserviceHandler.GetClientStatuses(timestamp, macAddress, hash));

        }

        // Terminal - OSS / REQ
        [SoapRpcMethod(), WebMethod(EnableSession=false)]
        public ObyTypedResult<Product> GetProducts(long timestamp, string macAddress, string hash)
        {
            this.SetNewRelicTransactionName("GetProducts");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { };
            // __OBYMOBI_USER_CODE_REGION_START
            // __OBYMOBI_USER_CODE_REGION_END

            return new ProductConverter().ConvertResultToLegacyResult(this.webserviceHandler.GetProducts(timestamp, macAddress, hash));

        }

        // Terminal - OSS / REQ
        [SoapRpcMethod(), WebMethod(EnableSession=false)]
        public ObyTypedResult<Category> GetCategories(long timestamp, string macAddress, string hash)
        {
            this.SetNewRelicTransactionName("GetCategories");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { };
            // __OBYMOBI_USER_CODE_REGION_START
            // __OBYMOBI_USER_CODE_REGION_END

            return new CategoryConverter().ConvertResultToLegacyResult(this.webserviceHandler.GetCategories(timestamp, macAddress, hash));

        }

        // NOC
        [SoapRpcMethod(), WebMethod(EnableSession=false)]
        public ObyTypedResult<Terminal> GetTerminals(long timestamp, string macAddress, string hash)
        {
            this.SetNewRelicTransactionName("GetTerminals");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { };
            // __OBYMOBI_USER_CODE_REGION_START
            // __OBYMOBI_USER_CODE_REGION_END

            return new TerminalConverter().ConvertResultToLegacyResult(this.webserviceHandler.GetTerminals(timestamp, macAddress, hash));

        }

        // Terminal - REQ & OSS
        [SoapRpcMethod(), WebMethod(EnableSession=false)]
        public ObyTypedResult<Order> GetOrders(long timestamp, string macAddress, int statusCode, string statusMessage, string version, bool isConsole, bool isMasterConsole, string hash)
        {
            this.SetNewRelicTransactionName("GetOrders");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { statusCode, statusMessage, version, isConsole, isMasterConsole };
            // __OBYMOBI_USER_CODE_REGION_START
            // __OBYMOBI_USER_CODE_REGION_END

            return new OrderConverter().ConvertResultToLegacyResult(this.webserviceHandler.GetOrders(timestamp, macAddress, statusCode, statusMessage, version, isConsole, isMasterConsole, hash));

        }

        // Terminal - REQ
        [SoapRpcMethod(), WebMethod(EnableSession=false)]
        public ObyTypedResult<TerminalOperationMode> GetTerminalOperationMode(long timestamp, string macAddress, string hash)
        {
            this.SetNewRelicTransactionName("GetTerminalOperationMode");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { };
            // __OBYMOBI_USER_CODE_REGION_START
            // __OBYMOBI_USER_CODE_REGION_END

            return webserviceHandler.GetTerminalOperationMode(timestamp, macAddress, hash);

        }

        // Terminal - OSS
        [SoapRpcMethod(), WebMethod(EnableSession=false)]
        public ObyTypedResult<OrderRoutestephandlerSaveStatus> GetRoutestephandlerStatuses(long timestamp, string macAddress, string[] orderroutestephandlerGuids, string hash)
        {
            this.SetNewRelicTransactionName("GetRoutestephandlerStatuses");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { orderroutestephandlerGuids };
            // __OBYMOBI_USER_CODE_REGION_START
            // __OBYMOBI_USER_CODE_REGION_END

            return new OrderRoutestephandlerSaveStatusConverter().ConvertResultToLegacyResult(this.webserviceHandler.GetRoutestephandlerStatuses(timestamp, macAddress, orderroutestephandlerGuids, hash));

        }

        // Emenu
        [SoapRpcMethod(), WebMethod(EnableSession=false)]
        public ObyTypedResult<SocialmediaMessage> GetSocialmediaMessages(long timestamp, string macAddress, string hash)
        {
            this.SetNewRelicTransactionName("GetSocialmediaMessages");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { };
            // __OBYMOBI_USER_CODE_REGION_START
            // __OBYMOBI_USER_CODE_REGION_END

            return new ObyTypedResult<SocialmediaMessage>();

        }

        // Emenu
        [SoapRpcMethod(), WebMethod(EnableSession=false)]
        [ScriptMethod(UseHttpGet = true)]
        public ObyTypedResult<SimpleWebserviceResult> GetPmsFolio(long timestamp, string macAddress, string hash)
        {
            this.SetNewRelicTransactionName("GetPmsFolio");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { };
            // __OBYMOBI_USER_CODE_REGION_START
            // __OBYMOBI_USER_CODE_REGION_END

            return new SimpleWebserviceResultConverter().ConvertResultToLegacyResult(this.webserviceHandler.GetPmsFolio(timestamp, macAddress, hash));

        }

        // Emenu
        [SoapRpcMethod(), WebMethod(EnableSession=false)]
        [ScriptMethod(UseHttpGet = true)]
        public ObyTypedResult<SimpleWebserviceResult> GetPmsGuestInformation(long timestamp, string macAddress, string hash)
        {
            this.SetNewRelicTransactionName("GetPmsGuestInformation");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { };
            // __OBYMOBI_USER_CODE_REGION_START
            // __OBYMOBI_USER_CODE_REGION_END

            return new SimpleWebserviceResultConverter().ConvertResultToLegacyResult(this.webserviceHandler.GetPmsGuestInformation(timestamp, macAddress, hash));

        }

        // Emenu
        [SoapRpcMethod(), WebMethod(EnableSession=false)]
        [ScriptMethod(UseHttpGet = true)]
        public ObyTypedResult<SimpleWebserviceResult> GetPmsExpressCheckout(long timestamp, string macAddress, decimal balance, string hash)
        {
            this.SetNewRelicTransactionName("GetPmsExpressCheckout");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { balance };
            // __OBYMOBI_USER_CODE_REGION_START
            // __OBYMOBI_USER_CODE_REGION_END

            return new SimpleWebserviceResultConverter().ConvertResultToLegacyResult(this.webserviceHandler.GetPmsExpressCheckout(timestamp, macAddress, balance, hash));

        }

        [SoapRpcMethod(), WebMethod(EnableSession = false)]
        [ScriptMethod(UseHttpGet = true)]
        public ObyTypedResult<Site> GetSite(long timestamp, string macAddress, int siteId, int deviceType, string languageCode, string hash)
        {
            this.SetNewRelicTransactionName("GetSite");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { siteId, deviceType, languageCode };
            // __OBYMOBI_USER_CODE_REGION_START
            // __OBYMOBI_USER_CODE_REGION_END

            return new SiteConverter().ConvertResultToLegacyResult(this.webserviceHandler.GetSite(timestamp, macAddress, siteId, deviceType, languageCode, string.Empty, hash));
        }

        [SoapRpcMethod(), WebMethod]
        [ScriptMethod(UseHttpGet = true)]
        public ObyTypedResult<SimpleWebserviceResult> GetSiteTicks(long timestamp, string macAddress, int siteId, string hash)
        {
            this.SetNewRelicTransactionName("GetSiteTicks");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { siteId };

            // __OBYMOBI_USER_CODE_REGION_START
            // __OBYMOBI_USER_CODE_REGION_END
            return new SimpleWebserviceResultConverter().ConvertResultToLegacyResult(this.webserviceHandler.GetSiteTicks(timestamp, macAddress, siteId, hash));
        }

        [SoapRpcMethod(), WebMethod(EnableSession = false)]
        [ScriptMethod(UseHttpGet = true)]
        public ObyTypedResult<Company> GetPointOfInterest(long timestamp, string macAddress, int pointOfInterestId, int deviceTypeInt, string hash)
        {
            this.SetNewRelicTransactionName("GetPointOfInterest");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { pointOfInterestId, deviceTypeInt };
            // __OBYMOBI_USER_CODE_REGION_START
            // __OBYMOBI_USER_CODE_REGION_END

            return new CompanyConverter().ConvertResultToLegacyResult(this.webserviceHandler.GetPointOfInterest(timestamp, macAddress, pointOfInterestId, deviceTypeInt, hash));
        }

        [SoapRpcMethod(), WebMethod]
        [ScriptMethod(UseHttpGet = true)]
        public ObyTypedResult<SimpleWebserviceResult> GetPointOfInterestTicks(long timestamp, string macAddress, int pointOfInterestId, string hash)
        {
            this.SetNewRelicTransactionName("GetPointOfInterestTicks");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { pointOfInterestId };

            // __OBYMOBI_USER_CODE_REGION_START
            // __OBYMOBI_USER_CODE_REGION_END
            return new SimpleWebserviceResultConverter().ConvertResultToLegacyResult(this.webserviceHandler.GetPointOfInterestTicks(timestamp, macAddress, pointOfInterestId, hash));
        }

        [SoapRpcMethod(), WebMethod]
        [ScriptMethod(UseHttpGet = true)]
        public ObyTypedResult<RoomControlConfiguration> GetRoomControlConfiguration(long timestamp, string macAddress, string hash)
        {
            this.SetNewRelicTransactionName("GetRoomControlConfiguration");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { };

            // __OBYMOBI_USER_CODE_REGION_START
            // __OBYMOBI_USER_CODE_REGION_END
            return new RoomControlConfigurationConverter().ConvertResultToLegacyResult(this.webserviceHandler.GetRoomControlConfiguration(timestamp, macAddress, hash));
        }

        [SoapRpcMethod(), WebMethod]
        [ScriptMethod(UseHttpGet = true)]
        public ObyTypedResult<SimpleWebserviceResult> GetEstimatedDeliveryTime(long timestamp, string macAddress, int deliverypointgroupId, string hash)
        {
            this.SetNewRelicTransactionName("GetEstimatedDeliveryTime");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { deliverypointgroupId };

            // __OBYMOBI_USER_CODE_REGION_START
            // __OBYMOBI_USER_CODE_REGION_END

            return new SimpleWebserviceResultConverter().ConvertResultToLegacyResult(this.webserviceHandler.GetEstimatedDeliveryTime(timestamp, macAddress, deliverypointgroupId, hash));
        }

        [SoapRpcMethod(), WebMethod(EnableSession = false)]
        [ScriptMethod(UseHttpGet = true)]
        public ObyTypedResult<UIScheduleItem> GetUISchedule(long timestamp, string macAddress, int deliverypointgroupId, string hash)
        {
            this.SetNewRelicTransactionName("GetUISchedule");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { deliverypointgroupId };
            // __OBYMOBI_USER_CODE_REGION_START
            // __OBYMOBI_USER_CODE_REGION_END

            return new UIScheduleItemConverter().ConvertResultToLegacyResult(this.webserviceHandler.GetUISchedule(timestamp, macAddress, deliverypointgroupId, hash));
        }

        #endregion

        #region PUT methods

        // Otoucho
        // Emenu - deprecated - use MAC address
        [SoapRpcMethod(), WebMethod(EnableSession=false)]
        [ScriptMethod(UseHttpGet = true)]
        public ObyTypedResult<ClientStatus> GetSetClientStatus(long timestamp, string macAddress, string xml, string hash)
        {
            this.SetNewRelicTransactionName("GetSetClientStatus");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { xml };
            // __OBYMOBI_USER_CODE_REGION_START

            Obymobi.Logic.Model.ClientStatus clientStatus = new ClientStatusConverter().ConvertLegacyXmlToModel(xml);

            // __OBYMOBI_USER_CODE_REGION_END

            return new ClientStatusConverter().ConvertResultToLegacyResult(this.webserviceHandler.GetSetClientStatus(timestamp, macAddress, clientStatus, hash));

        }

        // Otoucho
        // Emenu - deprecated - use MAC address
        [SoapRpcMethod(), WebMethod(EnableSession=false)]
        [ScriptMethod(UseHttpGet = true)]
        public ObyTypedResult<TerminalStatus> GetSetTerminalStatus(long timestamp, string macAddress, string xml, string hash)
        {
            this.SetNewRelicTransactionName("GetSetTerminalStatus");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { xml };
            // __OBYMOBI_USER_CODE_REGION_START

            Obymobi.Logic.Model.TerminalStatus terminalStatus = new TerminalStatusConverter().ConvertLegacyXmlToModel(xml);

            // __OBYMOBI_USER_CODE_REGION_END

            return new TerminalStatusConverter().ConvertResultToLegacyResult(this.webserviceHandler.GetSetTerminalStatus(timestamp, macAddress, terminalStatus, hash));

        }

        // Terminal - OSS
        [SoapRpcMethod(), WebMethod(EnableSession=false)]
        [ScriptMethod(UseHttpGet = true)]
        public ObyTypedResult<SimpleWebserviceResult> SetClientStatuses(long timestamp, string macAddress, string xml, string hash)
        {
            this.SetNewRelicTransactionName("SetClientStatuses");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { xml };
            // __OBYMOBI_USER_CODE_REGION_START

            Obymobi.Logic.Model.ClientStatus[] clientStatuses = new ClientStatusConverter().ConvertLegacyXmlArrayToModelArray(xml);

            // __OBYMOBI_USER_CODE_REGION_END

            return new SimpleWebserviceResultConverter().ConvertResultToLegacyResult(this.webserviceHandler.SetClientStatuses(timestamp, macAddress, clientStatuses, hash));

        }

        // Otoucho
        // Emenu - deprecated - use MAC address
        [SoapRpcMethod(), WebMethod(EnableSession=false)]
        [ScriptMethod(UseHttpGet = true)]
        public ObyTypedResult<Order> SaveOrder(long timestamp, string macAddress, string xml, string hash)
        {
            this.SetNewRelicTransactionName("SaveOrder");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { xml };
            // __OBYMOBI_USER_CODE_REGION_START

            // Crave Mobile hackz0r
            if (xml.StartsWith("\"") && xml.Length > 1)
                xml = xml.Substring(1);
            if (xml.EndsWith("\""))
                xml = xml.Substring(0, xml.Length - 1);

            // Create the order using the specified xml
            Obymobi.Logic.Model.Order order = new OrderConverter().ConvertLegacyXmlToModel(xml);

            // __OBYMOBI_USER_CODE_REGION_END

            return new OrderConverter().ConvertResultToLegacyResult(this.webserviceHandler.SaveOrder(timestamp, macAddress, order, hash));

        }

        // Terminal
        [SoapRpcMethod(), WebMethod(EnableSession=false)]
        [ScriptMethod(UseHttpGet = true)]
        public ObyTypedResult<Posorder> CreatePosorder(long timestamp, string macAddress, string xml, string hash)
        {
            this.SetNewRelicTransactionName("CreatePosorder");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { xml };
            // __OBYMOBI_USER_CODE_REGION_START

            Obymobi.Logic.Model.Order order = new OrderConverter().ConvertLegacyXmlToModel(xml);

            // __OBYMOBI_USER_CODE_REGION_END

            return new PosorderConverter().ConvertResultToLegacyResult(this.webserviceHandler.CreatePosorder(timestamp, macAddress, order, hash));

        }

        // Otoucho
        // Emenu - deprecated - use MAC address
        [SoapRpcMethod(), WebMethod(EnableSession=false)]
        [ScriptMethod(UseHttpGet = true)]
        public ObyTypedResult<SurveyResult> SaveSurveyResults(long timestamp, string macAddress, string xml, string hash)
        {
            this.SetNewRelicTransactionName("SaveSurveyResults");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { xml };
            // __OBYMOBI_USER_CODE_REGION_START

            Obymobi.Logic.Model.SurveyResult results = new SurveyResultConverter().ConvertLegacyXmlToModel(xml);

            // __OBYMOBI_USER_CODE_REGION_END

            return new SurveyResultConverter().ConvertResultToLegacyResult(this.webserviceHandler.SaveSurveyResults(timestamp, macAddress, results, hash));

        }

        // Emenu - deprecated - use MAC address
        [SoapRpcMethod(), WebMethod(EnableSession=false)]
        [ScriptMethod(UseHttpGet = true)]
        public ObyTypedResult<Rating> SaveRatings(long timestamp, string macAddress, string xml, string hash)
        {
            this.SetNewRelicTransactionName("SaveRatings");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { xml };
            // __OBYMOBI_USER_CODE_REGION_START

            Obymobi.Logic.Model.Rating[] ratings = new RatingConverter().ConvertLegacyXmlArrayToModelArray(xml);

            // __OBYMOBI_USER_CODE_REGION_END

            return new RatingConverter().ConvertResultToLegacyResult(this.webserviceHandler.SaveRatings(timestamp, macAddress, ratings, hash));

        }

        // Emenu - deprecated - use MAC address
        [SoapRpcMethod(), WebMethod(EnableSession=false)]
        [ScriptMethod(UseHttpGet = true)]
        public ObyTypedResult<SocialmediaMessage> SaveSocialmediaMessage(long timestamp, string macAddress, string xml, string hash)
        {
            this.SetNewRelicTransactionName("SaveSocialmediaMessage");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { xml };
            // __OBYMOBI_USER_CODE_REGION_START

            Obymobi.Logic.Model.SocialmediaMessage message = new SocialmediaMessageConverter().ConvertLegacyXmlToModel(xml);

            // __OBYMOBI_USER_CODE_REGION_END

            return new ObyTypedResult<SocialmediaMessage>();

        }

        // Terminal - OSS
        [SoapRpcMethod(), WebMethod(EnableSession=false)]
        [ScriptMethod(UseHttpGet = true)]
        public ObyTypedResult<ClientEntertainment> SaveClientEntertainment(long timestamp, string macAddress, string xml, string hash)
        {
            this.SetNewRelicTransactionName("SaveClientEntertainment");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { xml };
            // __OBYMOBI_USER_CODE_REGION_START

            Obymobi.Logic.Model.ClientEntertainment[] clientEntertainments = new ClientEntertainmentConverter().ConvertLegacyXmlArrayToModelArray(xml);

            // __OBYMOBI_USER_CODE_REGION_END

            return new ClientEntertainmentConverter().ConvertResultToLegacyResult(this.webserviceHandler.SaveClientEntertainment(timestamp, macAddress, clientEntertainments, hash));

        }

        // Emenu
        [SoapRpcMethod(), WebMethod(EnableSession=false)]
        [ScriptMethod(UseHttpGet = true)]
        public ObyTypedResult<SimpleWebserviceResult> SaveClientLog(long timestamp, string macAddress, int logType, string log, long fileDate, string hash)
        {
            this.SetNewRelicTransactionName("SaveClientLog");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { logType, log, fileDate };
            // __OBYMOBI_USER_CODE_REGION_START
            // __OBYMOBI_USER_CODE_REGION_END

            return new SimpleWebserviceResultConverter().ConvertResultToLegacyResult(this.webserviceHandler.SaveClientLog(timestamp, macAddress, logType, log, fileDate, hash));

        }

        [SoapRpcMethod(), WebMethod(EnableSession=false)]
        [ScriptMethod(UseHttpGet = true)]
        public ObyTypedResult<SimpleWebserviceResult> SaveClientLogZipped(long timestamp, string macAddress, int logType, string base64Zip, long fileDate, string hash)
        {
            this.SetNewRelicTransactionName("SaveClientLogZipped");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { logType, fileDate }; // base64Zip is ginored on purpose, not part of the hash!
            // __OBYMOBI_USER_CODE_REGION_START
            // __OBYMOBI_USER_CODE_REGION_END

            return new SimpleWebserviceResultConverter().ConvertResultToLegacyResult(this.webserviceHandler.SaveClientLogZipped(timestamp, macAddress, logType, base64Zip, fileDate, hash));
        }

        // Terminal - OSS /. REQ
        [SoapRpcMethod(), WebMethod(EnableSession=false)]
        public ObyTypedResult<Posproduct> SetPosProducts(long timestamp, string macAddress, long batchId, string xml, string hash)
        {
            this.SetNewRelicTransactionName("SetPosProducts");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { batchId, xml };
            // __OBYMOBI_USER_CODE_REGION_START

            if (xml.Trim().Length == 0)
                throw new ObymobiException(ImportPosproductsResult.XmlIsEmpty, "Xml string containing serialized Posproduct array is empty.");

            Obymobi.Logic.Model.Posproduct[] posproducts = new PosproductConverter().ConvertLegacyXmlArrayToModelArray(xml);

            // __OBYMOBI_USER_CODE_REGION_END

            return new PosproductConverter().ConvertResultToLegacyResult(this.webserviceHandler.SetPosProducts(timestamp, macAddress, batchId, posproducts, hash));

        }

        // Terminal - OSS / REQ
        [SoapRpcMethod(), WebMethod(EnableSession=false)]
        public ObyTypedResult<Poscategory> SetPosCategories(long timestamp, string macAddress, long batchId, string xml, string hash)
        {
            this.SetNewRelicTransactionName("SetPosCategories");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { batchId, xml };
            // __OBYMOBI_USER_CODE_REGION_START

            if (xml.Trim().Length == 0)
                throw new ObymobiException(ImportPoscategoriesResult.XmlIsEmpty, "Xml string containing serialized Poscategory array is empty.");

            Obymobi.Logic.Model.Poscategory[] poscategories = new PoscategoryConverter().ConvertLegacyXmlArrayToModelArray(xml);

            // __OBYMOBI_USER_CODE_REGION_END

            return new PoscategoryConverter().ConvertResultToLegacyResult(this.webserviceHandler.SetPosCategories(timestamp, macAddress, batchId, poscategories, hash));

        }

        // Terminal - OSS / REQ
        [SoapRpcMethod(), WebMethod(EnableSession=false)]
        public ObyTypedResult<Posdeliverypoint> SetPosDeliverypoints(long timestamp, string macAddress, long batchId, string xml, string hash)
        {
            this.SetNewRelicTransactionName("SetPosDeliverypoints");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { batchId, xml };
            // __OBYMOBI_USER_CODE_REGION_START

            if (xml.Trim().Length == 0)
                throw new ObymobiException(ImportPosdeliverypointsResult.XmlIsEmpty, "Xml string containing serialized Posdeliverypoint array is empty.");

            Obymobi.Logic.Model.Posdeliverypoint[] posdeliverypoints = new PosdeliverypointConverter().ConvertLegacyXmlArrayToModelArray(xml);

            // __OBYMOBI_USER_CODE_REGION_END

            return new PosdeliverypointConverter().ConvertResultToLegacyResult(this.webserviceHandler.SetPosDeliverypoints(timestamp, macAddress, batchId, posdeliverypoints, hash));

        }

        // Terminal - OSS / REQ
        [SoapRpcMethod(), WebMethod(EnableSession=false)]
        public ObyTypedResult<Posdeliverypointgroup> SetPosDeliverypointgroups(long timestamp, string macAddress, long batchId, string xml, string hash)
        {
            this.SetNewRelicTransactionName("SetPosDeliverypointgroups");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { batchId, xml };
            // __OBYMOBI_USER_CODE_REGION_START

            if (xml.Trim().Length == 0)
                throw new ObymobiException(ImportPosdeliverypointgroupsResult.XmlIsEmpty, "Xml string containing serialized Posdeliverypointgroup array is empty.");

            Obymobi.Logic.Model.Posdeliverypointgroup[] posdeliverypointgroups = new PosdeliverypointgroupConverter().ConvertLegacyXmlArrayToModelArray(xml);

            // __OBYMOBI_USER_CODE_REGION_END

            return new PosdeliverypointgroupConverter().ConvertResultToLegacyResult(this.webserviceHandler.SetPosDeliverypointgroups(timestamp, macAddress, batchId, posdeliverypointgroups, hash));

        }

        // Terminal - REQ
        [SoapRpcMethod(), WebMethod(EnableSession=false)]
        public ObyTypedResult<TerminalOperationMode> SetTerminalOperationMode(long timestamp, string macAddress, int terminalOperationMode, string hash)
        {
            this.SetNewRelicTransactionName("SetTerminalOperationMode");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { terminalOperationMode };
            // __OBYMOBI_USER_CODE_REGION_START
            // __OBYMOBI_USER_CODE_REGION_END

            return webserviceHandler.SetTerminalOperationMode(timestamp, macAddress, terminalOperationMode, hash);

        }

        // Terminal - REQ
        [SoapRpcMethod(), WebMethod(EnableSession=false)]
        public ObyTypedResult<TerminalOperationState> SetTerminalOperationState(long timestamp, string macAddress, int terminalOperationState, string hash)
        {
            this.SetNewRelicTransactionName("SetTerminalOperationState");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { terminalOperationState };
            // __OBYMOBI_USER_CODE_REGION_START
            // __OBYMOBI_USER_CODE_REGION_END

            return webserviceHandler.SetTerminalOperationState(timestamp, macAddress, terminalOperationState, hash);

        }

        [SoapRpcMethod(), WebMethod(EnableSession=false)]
        public ObyTypedResult<OrderRoutestephandlerSaveStatus> SetRoutestephandlerSaveStatuses(long timestamp, string macAddress, string xml, string hash)
        {
            this.SetNewRelicTransactionName("SetRoutestephandlerSaveStatuses");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { xml };
            // __OBYMOBI_USER_CODE_REGION_START

            // Deserialize the order save statuses
            Obymobi.Logic.Model.OrderRoutestephandlerSaveStatus[] orderSaveStatuses = new OrderRoutestephandlerSaveStatusConverter().ConvertLegacyXmlArrayToModelArray(xml);

            // __OBYMOBI_USER_CODE_REGION_END

            return new OrderRoutestephandlerSaveStatusConverter().ConvertResultToLegacyResult(this.webserviceHandler.SetRoutestephandlerSaveStatuses(timestamp, macAddress, orderSaveStatuses, hash));

        }

        // Emenu
        [SoapRpcMethod(), WebMethod(EnableSession=false)]
        [ScriptMethod(UseHttpGet = true)]
        public ObyTypedResult<SimpleWebserviceResult> SetClientDeliverypoint(long timestamp, string macAddress, int deliverypointId, int deliverypointgroupId, string hash)
        {
            this.SetNewRelicTransactionName("SetClientDeliverypoint");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { deliverypointId, deliverypointgroupId };
            // __OBYMOBI_USER_CODE_REGION_START
            // __OBYMOBI_USER_CODE_REGION_END

            return new SimpleWebserviceResultConverter().ConvertResultToLegacyResult(this.webserviceHandler.SetClientDeliverypoint(timestamp, macAddress, deliverypointId, deliverypointgroupId, hash));

        }

        [SoapRpcMethod(), WebMethod(EnableSession=false)]
        [ScriptMethod(UseHttpGet = true)]
        public ObyTypedResult<SimpleWebserviceResult> SendMessage(long timestamp, string macAddress, string deliverypointNumbers, string deliverypointgroupIds,
            int clientId, int orderId, string title, string message, int duration, int mediaId, int categoryId, int entertainmentId, int productId, bool urgent, bool saveMessage, bool mobileClients, string messagegroupIds, string hash)
        {
            this.SetNewRelicTransactionName("SendMessage");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { deliverypointNumbers, deliverypointgroupIds, clientId, orderId, title, message, duration, mediaId, categoryId, 
                entertainmentId, productId, urgent, saveMessage, mobileClients, messagegroupIds };
            // __OBYMOBI_USER_CODE_REGION_START
            // __OBYMOBI_USER_CODE_REGION_END

            return new SimpleWebserviceResultConverter().ConvertResultToLegacyResult(this.webserviceHandler.SendMessage(timestamp, macAddress, deliverypointNumbers, deliverypointgroupIds, clientId,
                orderId, title, message, duration, mediaId, categoryId, entertainmentId, productId, urgent, saveMessage, mobileClients, messagegroupIds, 0, new Obymobi.Logic.Model.Action[0], hash));

        }

        [SoapRpcMethod(), WebMethod(EnableSession=false)]
        [ScriptMethod(UseHttpGet = true)]
        public ObyTypedResult<SimpleWebserviceResult> SetPmsFolio(long timestamp, string macAddress, string xml, string hash)
        {
            this.SetNewRelicTransactionName("SetPmsFolio");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { xml };
            // __OBYMOBI_USER_CODE_REGION_START

            Obymobi.Logic.Model.Folio folio = new FolioConverter().ConvertLegacyXmlToModel(xml);

            // __OBYMOBI_USER_CODE_REGION_END

            return new SimpleWebserviceResultConverter().ConvertResultToLegacyResult(this.webserviceHandler.SetPmsFolio(timestamp, macAddress, folio, hash));

        }

        [SoapRpcMethod(), WebMethod(EnableSession=false)]
        [ScriptMethod(UseHttpGet = true)]
        public ObyTypedResult<SimpleWebserviceResult> SetPmsGuestInformation(long timestamp, string macAddress, string xml, string hash)
        {
            this.SetNewRelicTransactionName("SetPmsGuestInformation");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { xml };
            // __OBYMOBI_USER_CODE_REGION_START

            Obymobi.Logic.Model.GuestInformation guestInformation = new GuestInformationConverter().ConvertLegacyXmlToModel(xml);

            // __OBYMOBI_USER_CODE_REGION_END

            return new SimpleWebserviceResultConverter().ConvertResultToLegacyResult(this.webserviceHandler.SetPmsGuestInformation(timestamp, macAddress, guestInformation, hash));

        }

        [SoapRpcMethod(), WebMethod(EnableSession=false)]
        [ScriptMethod(UseHttpGet = true)]
        public ObyTypedResult<SimpleWebserviceResult> SetPmsCheckedIn(long timestamp, string macAddress, string xml, string hash)
        {
            this.SetNewRelicTransactionName("SetPmsCheckedIn");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { xml };
            // __OBYMOBI_USER_CODE_REGION_START

            Obymobi.Logic.Model.GuestInformation guestInformation = new GuestInformationConverter().ConvertLegacyXmlToModel(xml);

            // __OBYMOBI_USER_CODE_REGION_END

            return new SimpleWebserviceResultConverter().ConvertResultToLegacyResult(this.webserviceHandler.SetPmsCheckedIn(timestamp, macAddress, guestInformation, hash));

        }

        [SoapRpcMethod(), WebMethod(EnableSession=false)]
        [ScriptMethod(UseHttpGet = true)]
        public ObyTypedResult<SimpleWebserviceResult> SetPmsCheckedOut(long timestamp, string macAddress, int deliverypointId, string deliverypointNumber, string hash)
        {
            this.SetNewRelicTransactionName("SetPmsCheckedOut");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { deliverypointId, deliverypointNumber };
            // __OBYMOBI_USER_CODE_REGION_START
            // __OBYMOBI_USER_CODE_REGION_END

            return new SimpleWebserviceResultConverter().ConvertResultToLegacyResult(this.webserviceHandler.SetPmsCheckedOut(timestamp, macAddress, deliverypointId, deliverypointNumber, hash));

        }

        [SoapRpcMethod(), WebMethod(EnableSession=false)]
        [ScriptMethod(UseHttpGet = true)]
        public ObyTypedResult<SimpleWebserviceResult> SetPmsExpressCheckedOut(long timestamp, string macAddress, string xml, string hash)
        {
            this.SetNewRelicTransactionName("SetPmsExpressCheckedOut");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { xml };
            // __OBYMOBI_USER_CODE_REGION_START

            Obymobi.Logic.Model.Checkout checkoutInfo = new CheckoutConverter().ConvertLegacyXmlToModel(xml);

            // __OBYMOBI_USER_CODE_REGION_END

            return new SimpleWebserviceResultConverter().ConvertResultToLegacyResult(this.webserviceHandler.SetPmsExpressCheckedOut(timestamp, macAddress, checkoutInfo, hash));

        }

        [SoapRpcMethod(), WebMethod(EnableSession = false)]
        [ScriptMethod(UseHttpGet = true)]
        public ObyTypedResult<SimpleWebserviceResult> SetPmsWakeUp(long timestamp, string macAddress, string xml, string hash)
        {
            this.SetNewRelicTransactionName("SetPmsWakeUp");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { xml };
            // __OBYMOBI_USER_CODE_REGION_START

            Obymobi.Logic.Model.WakeUpStatus wakeUpStatus = new WakeUpStatusConverter().ConvertLegacyXmlToModel(xml);

            // __OBYMOBI_USER_CODE_REGION_END

            return new SimpleWebserviceResultConverter().ConvertResultToLegacyResult(this.webserviceHandler.SetPmsWakeUp(timestamp, macAddress, wakeUpStatus, hash));

        }

        [SoapRpcMethod(), WebMethod(EnableSession = false)]
        [ScriptMethod(UseHttpGet = true)]
        public ObyTypedResult<SimpleWebserviceResult> ClearPmsWakeUp(long timestamp, string macAddress, string xml, string hash)
        {
            this.SetNewRelicTransactionName("ClearPmsWakeUp");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { xml };
            // __OBYMOBI_USER_CODE_REGION_START

            Obymobi.Logic.Model.WakeUpStatus wakeUpStatus = new WakeUpStatusConverter().ConvertLegacyXmlToModel(xml);

            // __OBYMOBI_USER_CODE_REGION_END

            return new SimpleWebserviceResultConverter().ConvertResultToLegacyResult(this.webserviceHandler.ClearPmsWakeUp(timestamp, macAddress, wakeUpStatus, hash));

        }

        [SoapRpcMethod(), WebMethod(EnableSession = false)]
        [ScriptMethod(UseHttpGet = true)]
        public ObyTypedResult<SimpleWebserviceResult> SetClientWakeUpTime(long timestamp, string macAddress, string wakeUpTime, string hash)
        {
            this.SetNewRelicTransactionName("SetClientWakeUpTimer");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { wakeUpTime };
            // __OBYMOBI_USER_CODE_REGION_START
            // __OBYMOBI_USER_CODE_REGION_END

            return new SimpleWebserviceResultConverter().ConvertResultToLegacyResult(this.webserviceHandler.SetClientWakeUpTime(timestamp, macAddress, wakeUpTime, hash));

        }

        [SoapRpcMethod(), WebMethod(EnableSession=false)]
        [ScriptMethod(UseHttpGet = true)]
        public ObyTypedResult<SimpleWebserviceResult> SetForwardTerminal(long timestamp, string macAddress, int forwardToTerminalId, string hash)
        {
            this.SetNewRelicTransactionName("SetForwardTerminal");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { forwardToTerminalId };
            // __OBYMOBI_USER_CODE_REGION_START
            // __OBYMOBI_USER_CODE_REGION_END

            return new SimpleWebserviceResultConverter().ConvertResultToLegacyResult(this.webserviceHandler.SetForwardTerminal(timestamp, macAddress, forwardToTerminalId, hash));

        }

        [SoapRpcMethod(), WebMethod(EnableSession=false)]
        [ScriptMethod(UseHttpGet = true)]
        public ObyTypedResult<Order> GetOrderHistory(long timestamp, string macAddress, string hash)
        {
            this.SetNewRelicTransactionName("GetOrderHistory");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { };
            // __OBYMOBI_USER_CODE_REGION_START
            // __OBYMOBI_USER_CODE_REGION_END

            return new OrderConverter().ConvertResultToLegacyResult(this.webserviceHandler.GetOrderHistory(timestamp, macAddress, hash));

        }

        [SoapRpcMethod(), WebMethod(EnableSession = false)]
        [ScriptMethod(UseHttpGet = true)]
        public ObyTypedResult<SimpleWebserviceResult> SetEstimatedDeliveryTime(long timestamp, string macAddress, int deliverypointgroupId, int estimatedDeliveryTime, string hash)
        {
            this.SetNewRelicTransactionName("SetEstimatedDeliveryTime");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { deliverypointgroupId, estimatedDeliveryTime };
            // __OBYMOBI_USER_CODE_REGION_START
            // __OBYMOBI_USER_CODE_REGION_END

            return new SimpleWebserviceResultConverter().ConvertResultToLegacyResult(this.webserviceHandler.SetEstimatedDeliveryTime(timestamp, macAddress, deliverypointgroupId, estimatedDeliveryTime, hash));
        }

        #endregion

        #region MISC methods

        // Mobile
        // Emenu - deprecated - use MAC address
        [SoapRpcMethod(), WebMethod(EnableSession=false)]
        [ScriptMethod(UseHttpGet = true)]
        public ObyTypedResult<SimpleWebserviceResult> ProcessNetmessages(long timestamp, string macAddress, int customerId, int clientId, int companyId, int deliverypointId, int terminalId, string hash)
        {
            this.SetNewRelicTransactionName("ProcessNetmessages");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { customerId, clientId, companyId, deliverypointId, terminalId };
            // __OBYMOBI_USER_CODE_REGION_START
            // __OBYMOBI_USER_CODE_REGION_END

            return new SimpleWebserviceResultConverter().ConvertResultToLegacyResult(this.webserviceHandler.ProcessNetmessages(timestamp, macAddress, customerId, clientId, companyId, deliverypointId, terminalId, hash));

        }

        // Mobile
        // Emenu
        [SoapRpcMethod(), WebMethod(EnableSession=false)]
        [ScriptMethod(UseHttpGet = true)]
        public ObyTypedResult<SimpleWebserviceResult> VerifyNetmessage(long timestamp, string macAddress, int messageId, string hash)
        {
            this.SetNewRelicTransactionName("VerifyNetmessage");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { messageId };
            // __OBYMOBI_USER_CODE_REGION_START
            // __OBYMOBI_USER_CODE_REGION_END

            return new SimpleWebserviceResultConverter().ConvertResultToLegacyResult(this.webserviceHandler.VerifyNetmessage(timestamp, macAddress, messageId, hash));
        }

		// Emenu
		// OSS
		[SoapRpcMethod(), WebMethod(EnableSession=false)]
		[ScriptMethod(UseHttpGet = true)]
        public ObyTypedResult<SimpleWebserviceResult> VerifyNetmessageByGuid(long timestamp, string macAddress, string messageGuid, string trace, string hash)
		{
            this.SetNewRelicTransactionName("VerifyNetmessageByGuid");

			this.webserviceHandler.OriginalAdditionalParameters = new object[] { messageGuid, trace };
			// __OBYMOBI_USER_CODE_REGION_START
			// __OBYMOBI_USER_CODE_REGION_END

			return new SimpleWebserviceResultConverter().ConvertResultToLegacyResult(this.webserviceHandler.VerifyNetmessageByGuid(timestamp, macAddress, messageGuid, false, "", trace, hash));
		}

        // Emenu
        // OSS
        [SoapRpcMethod(), WebMethod(EnableSession=false)]
        [ScriptMethod(UseHttpGet = true)]
        public ObyTypedResult<SimpleWebserviceResult> CompleteNetmessageByGuid(long timestamp, string macAddress, string messageGuid, string errorMessage, string trace, string hash)
        {
            this.SetNewRelicTransactionName("CompleteNetmessageByGuid");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { messageGuid, errorMessage, trace };
            // __OBYMOBI_USER_CODE_REGION_START
            // __OBYMOBI_USER_CODE_REGION_END

            return new SimpleWebserviceResultConverter().ConvertResultToLegacyResult(this.webserviceHandler.VerifyNetmessageByGuid(timestamp, macAddress, messageGuid, true, errorMessage, trace, hash));
        }

            // Emenu
        [SoapRpcMethod(), WebMethod(EnableSession=false)]
        [ScriptMethod(UseHttpGet = true)]
        public ObyTypedResult<Message> GetMessages(long timestamp, string macAddress, int deliverypointId, string hash)
        {
            this.SetNewRelicTransactionName("GetMessages");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { deliverypointId };
            // __OBYMOBI_USER_CODE_REGION_START
            // __OBYMOBI_USER_CODE_REGION_END

            return new MessageConverter().ConvertResultToLegacyResult(this.webserviceHandler.GetMessages(timestamp, macAddress, deliverypointId, hash));
        }

        // Emenu
        [SoapRpcMethod, WebMethod(EnableSession=false)]
        [ScriptMethod(UseHttpGet = true)]
        public ObyTypedResult<SimpleWebserviceResult> RemoveMessages(long timestamp, string macAddress, string messageIds, string hash)
        {
            this.SetNewRelicTransactionName("RemoveMessages");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { messageIds };
            // __OBYMOBI_USER_CODE_REGION_START
            // __OBYMOBI_USER_CODE_REGION_END

            return new SimpleWebserviceResultConverter().ConvertResultToLegacyResult(this.webserviceHandler.RemoveMessages(timestamp, macAddress, messageIds, 0, hash));
        }

        // Emenu
        [SoapRpcMethod(), WebMethod(EnableSession=false)]
        [ScriptMethod(UseHttpGet = true)]
        public ObyTypedResult<long> GetCurrentTime()
        {
            this.SetNewRelicTransactionName("GetCurrentTime");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { };
            // __OBYMOBI_USER_CODE_REGION_START
            // __OBYMOBI_USER_CODE_REGION_END

            return this.webserviceHandler.GetCurrentTime();
        }

        // Terminal - OSS
        [SoapRpcMethod(), WebMethod(EnableSession=false)]
        [ScriptMethod(UseHttpGet = true)]
        public ObyTypedResult<SimpleWebserviceResult> PublishSocialmediaMessage(long timestamp, string macAddress, int socialmediaMessageId, string hash)
        {
            this.SetNewRelicTransactionName("PublishSocialmediaMessage");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { socialmediaMessageId };
            // __OBYMOBI_USER_CODE_REGION_START
            // __OBYMOBI_USER_CODE_REGION_END

            return new ObyTypedResult<SimpleWebserviceResult>();

        }

        // Terminal - OSS
        [SoapRpcMethod(), WebMethod(EnableSession=false)]
        [ScriptMethod(UseHttpGet = true)]
        public ObyTypedResult<SimpleWebserviceResult> RemoveSocialmediaMessage(long timestamp, string macAddress, int socialmediaMessageId, string hash)
        {
            this.SetNewRelicTransactionName("RemoveSocialmediaMessage");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { socialmediaMessageId };
            // __OBYMOBI_USER_CODE_REGION_START
            // __OBYMOBI_USER_CODE_REGION_END

            return new ObyTypedResult<SimpleWebserviceResult>();

        }

        // Terminal - OSS / REQ
        [SoapRpcMethod(), WebMethod(EnableSession=false)]
        public ObyTypedResult<SimpleWebserviceResult> VerifyTerminal(long timestamp, string macAddress, int terminalId, string hash)
        {
            this.SetNewRelicTransactionName("VerifyTerminal");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { terminalId };
            // __OBYMOBI_USER_CODE_REGION_START
            // __OBYMOBI_USER_CODE_REGION_END

            return new SimpleWebserviceResultConverter().ConvertResultToLegacyResult(this.webserviceHandler.VerifyTerminal(timestamp, macAddress, terminalId, hash));
        }

        // Terminal - OSS / REQ
        [SoapRpcMethod(), WebMethod(EnableSession=false)]
        public ObyTypedResult<SimpleWebserviceResult> CleanUpPosData(long timestamp, string macAddress, string hash)
        {
            this.SetNewRelicTransactionName("CleanUpPosData");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { };
            // __OBYMOBI_USER_CODE_REGION_START
            // __OBYMOBI_USER_CODE_REGION_END

            return new SimpleWebserviceResultConverter().ConvertResultToLegacyResult(this.webserviceHandler.CleanUpPosData(timestamp, macAddress, hash));
        }

        // Terminal - REQ
        [SoapRpcMethod(), WebMethod(EnableSession=false)]
        public ObyTypedResult<SimpleWebserviceResult> WriteToLog(long timestamp, string macAddress, int companyId, int terminalId, int orderId, string orderGuid,
            int type, string message, string status, string log, string hash)
        {
            this.SetNewRelicTransactionName("WriteToLog");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { 
                companyId, terminalId, orderId, orderGuid, type, message, status, log };
            // __OBYMOBI_USER_CODE_REGION_START
            // __OBYMOBI_USER_CODE_REGION_END

            return new SimpleWebserviceResultConverter().ConvertResultToLegacyResult(this.webserviceHandler.WriteToLog(timestamp, macAddress, terminalId, orderId, orderGuid, type, message, status, log, hash));

        }

        [SoapRpcMethod(), WebMethod(EnableSession=false)]
        public ObyTypedResult<SimpleWebserviceResult> WriteToLogZipped(long timestamp, string macAddress, int terminalId, int type, string message, string base64Zip, long fileDate, string hash)
        {
            this.SetNewRelicTransactionName("WriteToLogZipped");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { terminalId, type, message, fileDate };
            // __OBYMOBI_USER_CODE_REGION_START
            // __OBYMOBI_USER_CODE_REGION_END

            return new SimpleWebserviceResultConverter().ConvertResultToLegacyResult(this.webserviceHandler.WriteToLogZipped(timestamp, macAddress, terminalId, type, message, base64Zip, fileDate, hash));
        }

        // Emenu
        // Mobile?
        [SoapRpcMethod(), WebMethod(EnableSession=false)]
        public ObyTypedResult<SimpleWebserviceResult> SaveFBCode(string credentials, string code)
        {
            this.SetNewRelicTransactionName("SaveFBCode");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { };
            // __OBYMOBI_USER_CODE_REGION_START
            // __OBYMOBI_USER_CODE_REGION_END

            return new ObyTypedResult<SimpleWebserviceResult>();

        }

        [SoapRpcMethod(), WebMethod(EnableSession=false)]
        public ObyTypedResult<SimpleWebserviceResult> ProcessOrders(long timestamp, string macAddress, int companyId, int clientId, string hash)
        {
            this.SetNewRelicTransactionName("ProcessOrders");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { companyId, clientId };
            // __OBYMOBI_USER_CODE_REGION_START
            // __OBYMOBI_USER_CODE_REGION_END

            return new SimpleWebserviceResultConverter().ConvertResultToLegacyResult(this.webserviceHandler.ProcessOrders(timestamp, macAddress, clientId, hash));

        }

        [SoapRpcMethod(), WebMethod(EnableSession=false)]
        public ObyTypedResult<SimpleWebserviceResult> CancelOrder(long timestamp, string macAddress, int clientId, int customerId, string orderGuid, string hash)
        {
            this.SetNewRelicTransactionName("CancelOrder");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { clientId, customerId, orderGuid };
            // __OBYMOBI_USER_CODE_REGION_START
            // __OBYMOBI_USER_CODE_REGION_END

            return new SimpleWebserviceResultConverter().ConvertResultToLegacyResult(this.webserviceHandler.CancelOrder(timestamp, macAddress, clientId, customerId, orderGuid, hash));

        }

        [SoapRpcMethod(), WebMethod(EnableSession=false)]
        public ObyTypedResult<SimpleWebserviceResult> ManuallyProcessOrder(long timestamp, string macAddress, int companyId, int terminalId, string orderGuid, string hash)
        {
            this.SetNewRelicTransactionName("ManuallyProcessOrder");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { companyId, terminalId, orderGuid };
            // __OBYMOBI_USER_CODE_REGION_START
            // __OBYMOBI_USER_CODE_REGION_END

            return new SimpleWebserviceResultConverter().ConvertResultToLegacyResult(this.webserviceHandler.ManuallyProcessOrder(timestamp, macAddress, orderGuid, hash));


        }

        [SoapRpcMethod(), WebMethod(EnableSession=false)]
        public ObyTypedResult<SimpleWebserviceResult> UploadFile(long timestamp, string macAddress, string identifier, string f, string filename)
        {
            this.SetNewRelicTransactionName("UploadFile");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { identifier, f, };
            // __OBYMOBI_USER_CODE_REGION_START
            // __OBYMOBI_USER_CODE_REGION_END

            return new SimpleWebserviceResultConverter().ConvertResultToLegacyResult(this.webserviceHandler.UploadFile(timestamp, macAddress, identifier, f, filename));

        }

        [SoapRpcMethod(), WebMethod(EnableSession=false)]
        public ObyTypedResult<SimpleWebserviceResult> UploadFileInPost()
        {
            this.SetNewRelicTransactionName("UploadFileInPost");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { };
            // __OBYMOBI_USER_CODE_REGION_START
            // __OBYMOBI_USER_CODE_REGION_END

            return new SimpleWebserviceResultConverter().ConvertResultToLegacyResult(this.webserviceHandler.UploadFileInPost());

        }

        [SoapRpcMethod(), WebMethod(EnableSession=false)]
        public ObyTypedResult<SimpleWebserviceResult> PrintFile()
        {
            this.SetNewRelicTransactionName("PrintFile");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { };
            // __OBYMOBI_USER_CODE_REGION_START
            // __OBYMOBI_USER_CODE_REGION_END

            return new SimpleWebserviceResultConverter().ConvertResultToLegacyResult(this.webserviceHandler.PrintFile());

        }

        [SoapRpcMethod(), WebMethod(EnableSession = false)]
        public ObyTypedResult<SimpleWebserviceResult> PrintPdf()
        {
            this.webserviceHandler.OriginalAdditionalParameters = new object[] { };
            // __OBYMOBI_USER_CODE_REGION_START
            // __OBYMOBI_USER_CODE_REGION_END

            return new SimpleWebserviceResultConverter().ConvertResultToLegacyResult(this.webserviceHandler.PrintPdf());

        }

        [SoapRpcMethod(), WebMethod(EnableSession=false)]
        public ObyTypedResult<SimpleWebserviceResult> EmailFile()
        {
            this.SetNewRelicTransactionName("EmailFile");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { };
            // __OBYMOBI_USER_CODE_REGION_START
            // __OBYMOBI_USER_CODE_REGION_END

            return new SimpleWebserviceResultConverter().ConvertResultToLegacyResult(this.webserviceHandler.EmailFile());

        }

        [SoapRpcMethod(), WebMethod(EnableSession=false)]
        public ObyTypedResult<SimpleWebserviceResult> SetOrderPrinted(long timestamp, string macAddress, int orderId, string hash)
        {
            this.SetNewRelicTransactionName("SetOrderPrinted");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { orderId };
            // __OBYMOBI_USER_CODE_REGION_START
            // __OBYMOBI_USER_CODE_REGION_END

            return new SimpleWebserviceResultConverter().ConvertResultToLegacyResult(this.webserviceHandler.SetOrderPrinted(timestamp, macAddress, orderId, hash));

        }

        [SoapRpcMethod(), WebMethod(EnableSession=false)]
        public ObyTypedResult<Analytics> GetAnalytics(long timestamp, string macAddress, string hash)
        {
            this.SetNewRelicTransactionName("GetAnalytics");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { };
            // __OBYMOBI_USER_CODE_REGION_START
            // __OBYMOBI_USER_CODE_REGION_END

            return new AnalyticsConverter().ConvertResultToLegacyResult(this.webserviceHandler.GetAnalytics(timestamp, macAddress, hash));

        }

        [SoapRpcMethod(), WebMethod(EnableSession=false)]
        public ObyTypedResult<SimpleWebserviceResult> SetMobileOrdering(long timestamp, string macAddress, bool enabled, string hash)
        {
            this.SetNewRelicTransactionName("SetMobileOrdering");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { enabled };
            // __OBYMOBI_USER_CODE_REGION_START
            // __OBYMOBI_USER_CODE_REGION_END

            return new SimpleWebserviceResultConverter().ConvertResultToLegacyResult(this.webserviceHandler.SetMobileOrdering(timestamp, macAddress, enabled, hash));

        }

        [SoapRpcMethod(), WebMethod(EnableSession=false)]
        public ObyTypedResult<SimpleWebserviceResult> UpdateLastRequest(long timestamp, string macAddress, int terminalId, string hash)
        {
            this.SetNewRelicTransactionName("UpdateLastRequest");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { terminalId };

            // __OBYMOBI_USER_CODE_REGION_START
            // __OBYMOBI_USER_CODE_REGION_END

            return new SimpleWebserviceResultConverter().ConvertResultToLegacyResult(this.webserviceHandler.UpdateLastRequest(timestamp, macAddress, 0, terminalId, hash));
        }

        [SoapRpcMethod(), WebMethod(EnableSession=false)]
        public ObyTypedResult<Release> GetRelease(long timestamp, string macAddress, string applicationCode, string hash)
        {
            this.SetNewRelicTransactionName("GetRelease");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { applicationCode };

            // __OBYMOBI_USER_CODE_REGION_START
            // __OBYMOBI_USER_CODE_REGION_END

            return new ReleaseConverter().ConvertResultToLegacyResult(this.webserviceHandler.GetRelease(timestamp, macAddress, applicationCode, hash));
        }

        [SoapRpcMethod(), WebMethod(EnableSession=false)]
        public ObyTypedResult<SimpleWebserviceResult> SetUpdateStatus(long timestamp, string macAddress, string applicationCode, int updateStatus, string customMessage, int releaseId, string hash)
        {
            this.SetNewRelicTransactionName("SetUpdateStatus");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { applicationCode, updateStatus, customMessage, releaseId };

            // __OBYMOBI_USER_CODE_REGION_START
            // __OBYMOBI_USER_CODE_REGION_END

            return new SimpleWebserviceResultConverter().ConvertResultToLegacyResult(this.webserviceHandler.SetUpdateStatus(timestamp, macAddress, applicationCode, updateStatus, customMessage, releaseId, hash));
        }

        [SoapRpcMethod, WebMethod(EnableSession=false)]
        public ObyTypedResult<SimpleWebserviceResult> SetApplicationVersion(long timestamp, string macAddress, string applicationCode, string versionNumber, string hash)
        {
            this.SetNewRelicTransactionName("SetApplicationVersion");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { applicationCode, versionNumber };

            // __OBYMOBI_USER_CODE_REGION_START
            // __OBYMOBI_USER_CODE_REGION_END

            return new SimpleWebserviceResultConverter().ConvertResultToLegacyResult(this.webserviceHandler.SetApplicationVersion(timestamp, macAddress, applicationCode, versionNumber, hash));
        }

        [SoapRpcMethod, WebMethod(EnableSession=false)]
        public ObyTypedResult<SimpleWebserviceResult> SetAgentCommandReponse(long timestamp, string macAddress, string resultMessage, string hash)
        {
            this.SetNewRelicTransactionName("SetAgentCommandResponse");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { resultMessage };

            // __OBYMOBI_USER_CODE_REGION_START
            // __OBYMOBI_USER_CODE_REGION_END

            return new SimpleWebserviceResultConverter().ConvertResultToLegacyResult(this.webserviceHandler.SetAgentCommandReponse(timestamp, macAddress, resultMessage, hash));
        }

        [SoapRpcMethod(), WebMethod(EnableSession=false)]
        [ScriptMethod(UseHttpGet = true)]
        public ObyTypedResult<SimpleWebserviceResult> DailyOrderReset(long timestamp, string macAddress, string deliverypointNumbers, string deliverypointgroupIds, string hash)
        {
            this.SetNewRelicTransactionName("DailyOrderReset");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { deliverypointNumbers, deliverypointgroupIds };
            // __OBYMOBI_USER_CODE_REGION_START
            // __OBYMOBI_USER_CODE_REGION_END

            return new SimpleWebserviceResultConverter().ConvertResultToLegacyResult(this.webserviceHandler.DailyOrderReset(timestamp, macAddress, deliverypointNumbers, deliverypointgroupIds, hash));
        }

        [SoapRpcMethod(), WebMethod(EnableSession = false)]
        [ScriptMethod(UseHttpGet = true)]
        public ObyTypedResult<SimpleWebserviceResult> ClearSendMessages(long timestamp, string macAddress, string deliverypointNumbers, string deliverypointgroupIds, string messageGroupIds, string hash)
        {
            this.SetNewRelicTransactionName("ClearSendMessages");

            this.webserviceHandler.OriginalAdditionalParameters = new object[] { deliverypointNumbers, deliverypointgroupIds, messageGroupIds };
            // __OBYMOBI_USER_CODE_REGION_START
            // __OBYMOBI_USER_CODE_REGION_END

            return new SimpleWebserviceResultConverter().ConvertResultToLegacyResult(this.webserviceHandler.SendClearMessages(timestamp, macAddress, deliverypointNumbers, deliverypointgroupIds, messageGroupIds, hash));
        }

        #endregion

        #region Helper methods

        private void SetNewRelicTransactionName(string methodName)
        {
        }

        #endregion
    }
}
