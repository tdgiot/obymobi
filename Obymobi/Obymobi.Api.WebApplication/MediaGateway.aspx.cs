﻿using Dionysos;
using Dionysos.IO;
using Dionysos.Security.Cryptography;
using Dionysos.Web;
using Obymobi;
using Obymobi.Constants;
using Obymobi.Enums;
using Obymobi.Api.Logic.EntityConverters.Mobile;
using Obymobi.Logic.HelperClasses;
using Obymobi.Security;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class MediaGateway : System.Web.UI.Page
{
    public enum MediaGatewayResult
    { 
        Unknown = 0,
        Success = 1,
        InvalidHash = 2,
        MissingParameters = 3
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        this.ProcessRequest();
    }

    private string hash = string.Empty;
    private string action = string.Empty;
    private int timestamp = 0;
    private string suppliedPath = string.Empty;
    private bool ProcessRequest()
    {
        if (!QueryStringHelper.TryGetValue("timestamp", out this.timestamp))
        {
            this.SetResponse(500, "Timestamp missing");
            return false;
        }        

        if (!QueryStringHelper.TryGetValue("hash", out this.hash))
        {
            this.SetResponse(500, "Hash missing");
            return false;
        }

        if (!QueryStringHelper.TryGetValue("action", out this.action))
        {
            this.SetResponse(500, "Action missing");
            return false;
        }

        try
        {
            MediaGatewayAction gatewayAction;
            if (EnumUtil.TryParse(action, out gatewayAction))
            {
                switch (gatewayAction)
                {
                    case MediaGatewayAction.Ping:
                        this.Ping();
                        break;
                    case MediaGatewayAction.FileExists:
                        this.FileExists();
                        break;
                    case MediaGatewayAction.FileRequiresUpload:
                        this.FileRequiresUpload();
                        break;
                    case MediaGatewayAction.ReadFile:
                        this.ReadFile();
                        break;
                    case MediaGatewayAction.WriteFile:
                        this.WriteFile();
                        break;
                    case MediaGatewayAction.DeleteFile:
                        this.DeleteFile();
                        break;
                    default:
                        this.SetResponse(500, "Action not supported: " + action);
                        break;
                }
            }
            else
                this.SetResponse(500, "Action not supported: " + action);
        }
        catch (ThreadAbortException)
        { 
            // No problem.
        }
        catch (Exception ex)
        {
            if (TestUtil.IsPcDeveloper)
            {
                throw;
            }
            else
            {
                this.SetResponse(500, "Error: " + ex.Message);
            }
        }                     

        return true;
    }

    private void Ping()
    {
        if (this.ValidateRequest())
        {
            this.SetResponse(200, "Pong");
        }
        else
        {
            this.SetResponse(500, "Invalid Request: Reason unknown");
        }
    }

    private void FileExists()
    {
        string path;
        if (this.GetLocalFullPath(out path))
        {
            if (this.ValidateRequest(this.suppliedPath))
            {             
                if (File.Exists(path))
                    this.SetResponse(200, "1");
                else
                    this.SetResponse(200, "0");
            }
            else
            {
                this.SetResponse(500, "Invalid Request: Reason unknown");
            }
        }
    }

    private void FileRequiresUpload()
    {
        string path;
        if (this.GetLocalFullPath(out path))
        {
            string md5;
            if(!QueryStringHelper.TryGetValue("md5", out md5))
            {
                this.SetResponse(500, "Md5 missing");
            }

            if (this.ValidateRequest(this.suppliedPath, md5))
            {
                if (File.Exists(path))
                {
                    string savedFileHash = Dionysos.Security.Cryptography.Hasher.GetHashForFile(path, HashType.MD5);
                    if(savedFileHash.Equals(md5, StringComparison.InvariantCultureIgnoreCase))
                        this.SetResponse(200, "0");
                    else
                        this.SetResponse(200, "1");
                }
                else
                    this.SetResponse(200, "1");
            }
            else
            {
                this.SetResponse(500, "Invalid Request: Reason unknown");
            }
        }
    }

    private void ReadFile()
    { 
        string path;
        if (this.GetLocalFullPath(out path))
        {
            if (this.ValidateRequest(this.suppliedPath))
            {
                if (File.Exists(path))
                {
                    // for now jpg only
                    this.Response.Clear();
                    this.Response.ContentType = "image/jpeg";
                    this.Response.Buffer = true;
                    this.Response.WriteFile(path);
                    this.Response.End();
                }
                else
                {
                    this.SetResponse(500, "File not found.");
                }
            }
        }
    }

    private void WriteFile()
    {
        string path;
        if (this.GetLocalFullPath(out path))
        {
            string md5;
            if(!QueryStringHelper.TryGetValue("md5", out md5))
            {
                this.SetResponse(500, "Md5 missing");
            }

            if (this.ValidateRequest(this.suppliedPath, md5))
            {
                if (this.Request.Files.Count == 1)
                { 
                    // Delete existing file
                    if(File.Exists(path))
                        File.Delete(path);

                    // Save file           
                    string directory = Path.GetDirectoryName(path);
                    Directory.CreateDirectory(directory);
                    this.Request.Files[0].SaveAs(path);

                    if (!FileUtil.GetMD5Hash(path).Equals(md5, StringComparison.InvariantCultureIgnoreCase))
                    {
                        this.SetResponse(500, "Md5 incorrect");
                    }
                    else
                    {
                        this.SetResponse(200, "1");
                    }                    
                }
                else
                {
                    this.SetResponse(500, "Files Count != 1, it's " + this.Request.Files.Count);
                }
            }
        }
    }

    private void DeleteFile()
    {
        string path;

        string prefix;        
        if (!QueryStringHelper.TryGetValue("prefix", out prefix))
        {
            this.SetResponse(500, "Prefix missing");
        }
        bool isPrefix = prefix.Equals("1");

        if (this.GetLocalFullPath(out path))
        {
            if (this.ValidateRequest(this.suppliedPath, prefix))
            {
                if (isPrefix)
                {
                    string directory = Path.GetDirectoryName(path);
                    string searchPattern = Path.GetFileName(path) + "*";
                    if (Directory.Exists(directory))
                    {
                        var filesToDelete = Directory.GetFiles(directory, searchPattern);

                        foreach (var file in filesToDelete)
                        {
                            File.Delete(file);
                        }
                    }
                }
                else
                {
                    if (File.Exists(path))
                        File.Delete(path);
                }

                this.SetResponse(200, "1");
            }
        }
    }

    private string GetPathFromQueryString()
    {
        string path;
        if (!QueryStringHelper.TryGetValue("path", out path))        
            return string.Empty;
        this.suppliedPath = HttpUtility.UrlEncode(path);
        return HttpUtility.UrlDecode(path);
    }

    private bool GetLocalFullPath(out string path)
    {        
        path = this.GetPathFromQueryString();        
        if (!path.IsNullOrWhiteSpace())
        {            
            string filesPath = this.MapPath(string.Format("~/{0}/", ObymobiConstants.CloudContainerMediaFiles));            
            path = Path.Combine(filesPath, path);
            return true;
        }
        else
        {
            this.SetResponse(500, "Path missing");
            return false;
        }
    }

    private bool ValidateRequest(params string[] parameters)
    {        
        if (Obymobi.Security.Hasher.IsHashValidForParameters(hash, ObymobiConstants.MediaGatewaySalt, timestamp, this.action.ToString(), parameters))
        {
            DateTime requestDateTime = DateTimeUtil.FromUnixTime(timestamp);
            TimeSpan requestAge = (DateTime.UtcNow - requestDateTime);
            if (requestAge.TotalMinutes > 5 || requestAge.TotalMinutes < -5)
            {
                this.SetResponse(500, string.Format("Timestamp out of range: '{0}', Age: '{1}m' - Server time: '{2}'", requestDateTime, requestAge.ToString(), DateTime.UtcNow));
                return false;
            }         
        }
        else
        {
            //string toBeHashed = timestamp + string.Join(string.Empty, parameters);
            this.SetResponse(500, string.Format("The supplied hash is invalid: '{0}'.", hash));
            return false;
        }

        return true;
    }

    private void SetResponse(int code, string text)
    {
        this.Response.Clear();
        this.Response.ContentType = "text/plain";
        this.Response.StatusCode = code;
        this.Response.Write(text);
        this.Response.End();
    }
}