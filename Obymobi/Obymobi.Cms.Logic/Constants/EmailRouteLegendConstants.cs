﻿using System.ComponentModel;
using Obymobi.Cms.Logic.Attributes;
using Obymobi.Cms.Logic.Resources;

namespace Obymobi.Cms.Logic.Constants
{
    public class EmailRouteLegendConstants
    {
        [Description("The choosen checkout method for the order.")]
        public const string CHECKOUT_METHOD = "CHECKOUTMETHOD";

        [Description("Displays the name of the company.")]
        public const string COMPANYNAME = "COMPANYNAME";

        [Description("Displays the email address of the customer.")]
        public const string CUSTOMER_EMAIL = "CUSTOMEREMAIL";

        [Description("Displays the last name of the customer.")]
        public const string CUSTOMER_LASTNAME = "CUSTOMERLASTNAME";

        [Description("Displays the phonenumber of the customer.")]
        public const string CUSTOMER_PHONE = "CUSTOMERPHONE";

        [Description("The delivery instructions submitted by the customer.")]
        public const string DELIVERY_INFORMATION = "DELIVERYINFORMATION";

        [Description("Displays the caption of the deliverypoint.")]
        public const string DELIVERYPOINTCAPTION = "DELIVERYPOINTCAPTION";

        [Description("Displays the deliverypoint name of which the order was sent.")]
        public const string DELIVERYPOINTNAME = "DELIVERYPOINTNAME";

        [Description("Displays the deliverypoint number of which the order was sent.")]
        public const string DELIVERYPOINTNUMBER = "DELIVERYPOINTNUMBER";

        [Description("Displays the orderitems.")]
        public const string ORDER = "ORDER";

        [Description("Displays the date and time of order creation.")]
        public const string ORDER_CREATED = "CREATED";

        [Description("Displays the notes passed when saving the order.")]
        public const string ORDER_NOTES = "NOTES";

        [Description("Displays the orderId.")]
        public const string ORDER_NUMBER = "ORDERNUMBER";

        [Description("Displays the total price.")]
        public const string ORDER_PRICE = "PRICE";

        [Description("Displays the status in text.")]
        public const string ORDER_STATUSTEXT = "STATUSTEXT";

        [Description("Payment information of the transaction.")]
        public const string PAYMENT_DETAILS = "PAYMENTDETAILS";

        [Description("The current status of the payment.")]
        public const string PAYMENT_STATUS = "PAYMENTSTATUS";

        [Description("The seller address information based on the outlet.")]
        public const string SELLER_ADDRESS = "SELLERADDRESS";

        [Description("The seller contact phone number based on the outlet.")]
        public const string SELLER_CONTACT = "SELLERCONTACT";

        [Description("The seller email based on the outlet.")]
        public const string SELLER_CONTACT_EMAIL = "SELLERCONTACTEMAIL";

        [Description("The seller name based on the outlet.")]
        public const string SELLER_NAME = "SELLERNAME";

        [Description("The customers choosen service method.")]
        public const string SERVICE_METHOD = "SERVICEMETHOD";

        [Description("Shows the order total.")]
        [Tooltip(nameof(TooltipExplanations.ORDERTOTAL))]
        public const string ORDERTOTAL = "ORDERTOTAL";


        [Description("The selected number of covers.")]
        public const string NUMBEROFCOVERS = "NUMBEROFCOVERS";

        [Description("Shows tax breakdown.")]
        [Tooltip(nameof(TooltipExplanations.TAXBREAKDOWN))]
        public const string TAXBREAKDOWN = "TAXBREAKDOWN";
    }
}
