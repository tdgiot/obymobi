﻿namespace Obymobi.Cms.Logic.ViewModels
{
    public class TemplateVariable
    {
        public TemplateVariable(string name, string description, string tooltip)
        {
            if (!string.IsNullOrWhiteSpace(name))
                this.Name = $"[[{name}]]";

            this.Description = description;
            this.Tooltip = tooltip;
        }

        public string Name { get; }

        public string Description { get; }

        public string Tooltip { get; }
    }
}
