﻿using Obymobi.Data.EntityClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Obymobi.Cms.Logic.Requests
{
    public class CopyDeliverypointgroupRequest
    {
        public int DeliverypointgroupIdToCopy { get; set; }

        public string NewDeliverypointgroupName { get; set; }
        
        public Func<MediaEntity, MediaEntity, bool> CopyMediaOnCdn { get;set; }

        public DeliverypointgroupEntity NewDeliverypointgroupEntity { get; set; }
    }
}
