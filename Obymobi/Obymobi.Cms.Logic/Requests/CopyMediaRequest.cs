﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Cms.Logic.Requests
{
    public class CopyMediaRequest
    {
        public MediaEntity MediaToCopy { get; set; }
        public EntityField ReferenceField { get; set; }
        public object ReferenceValue { get; set; }
        public Transaction Transaction { get; set; }
        public bool ToGenericEntity { get; set; }
        public bool SkipSave { get; set; }
        public Func<MediaEntity, MediaEntity, bool> CopyMediaOnCdn { get; set; }
    }
}
