﻿using System;
using Dionysos;

namespace Obymobi.Cms.Logic.Attributes
{
    [AttributeUsage(AttributeTargets.Field, AllowMultiple = false, Inherited = false)]
    public class TooltipAttribute : Attribute
    {
        public TooltipAttribute(string key)
        {
            this.Key = key;

            if (this.Key.IsNullOrWhiteSpace())
            {
                throw new ArgumentNullException(nameof(Key));
            }

            this.Description = Resources.TooltipExplanations.ResourceManager.GetString(this.Key);
        }

        public string Key { get; }

        public string Description { get; }
    }
}
