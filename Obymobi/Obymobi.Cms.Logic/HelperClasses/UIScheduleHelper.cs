﻿using System;
using System.Collections;
using Dionysos.Data.LLBLGen;
using Obymobi.Data;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System.Collections.Generic;
using Obymobi.Cms.Logic.Factories;
using Obymobi.Enums;

namespace Obymobi.Cms.Logic.HelperClasses
{
    public class UIScheduleHelper
    {
        public static UIScheduleCollection GetUISchedules(int companyId)
        {
            PredicateExpression filter = PredicateFactory.CreateUIScheduleFilter(companyId, UIScheduleType.Legacy);
            
            UIScheduleCollection uiSchedules = new UIScheduleCollection();
            uiSchedules.GetMulti(filter);

            return uiSchedules;
        }

        public static UIScheduleEntity GetPrefetchedUISchedule(int uiScheduleId)
        {
            PredicateExpression filter = new PredicateExpression(UIScheduleFields.UIScheduleId == uiScheduleId);

            PrefetchPath prefetch = new PrefetchPath(EntityType.UIScheduleEntity);
            IPrefetchPathElement prefetchScheduleItems = prefetch.Add(UIScheduleEntityBase.PrefetchPathUIScheduleItemCollection);
            IPrefetchPathElement prefetchScheduleItemsOccurences = prefetchScheduleItems.SubPath.Add(UIScheduleItemEntityBase.PrefetchPathUIScheduleItemOccurrenceCollection);

            UIScheduleCollection uiSchedules = new UIScheduleCollection();
            uiSchedules.GetMulti(filter, prefetch);

            if (uiSchedules.Count > 0)
            {
                return uiSchedules[0];
            }
            else
            {
                return null;
            }
        }

        public static UIScheduleEntity CopyUISchedule(int oldUIScheduleId, string newUIScheduleName, int newDeliverypointgroupId)
        {
            UIScheduleEntity newUISchedule = null;

            UIScheduleEntity oldUISchedule = UIScheduleHelper.GetPrefetchedUISchedule(oldUIScheduleId);
            if (oldUISchedule != null)
            {
                Transaction transaction = new Transaction(System.Data.IsolationLevel.ReadUncommitted, string.Format("CopyUISchedule-UIScheduleId{0}-to-{1}", oldUIScheduleId, newUIScheduleName));

                try
                {
                    ArrayList uiScheduleFieldsNotToBeCopied = new ArrayList { UIScheduleFields.UIScheduleId, UIScheduleFields.Name, UIScheduleFields.DeliverypointgroupId };
                    ArrayList uiScheduleItemFieldsNotToBeCopied = new ArrayList { UIScheduleItemFields.UIScheduleId };
                    ArrayList uiScheduleItemOccurrenceFieldsNotToBeCopied = new ArrayList { UIScheduleItemOccurrenceFields.UIScheduleItemId };

                    Dictionary<int, UIScheduleItemOccurrenceEntity> scheduleItemOccurenceCopyMapping = new Dictionary<int, UIScheduleItemOccurrenceEntity>();

                    // UI Schedule
                    newUISchedule = new UIScheduleEntity();
                    newUISchedule.AddToTransaction(transaction);
                    LLBLGenEntityUtil.CopyFields(oldUISchedule, newUISchedule, uiScheduleFieldsNotToBeCopied);
                    newUISchedule.Name = newUIScheduleName;
                    newUISchedule.DeliverypointgroupId = newDeliverypointgroupId;

                    // UI Schedule Item
                    foreach (UIScheduleItemEntity oldUIScheduleItem in oldUISchedule.UIScheduleItemCollection)
                    {
                        UIScheduleItemEntity newUIScheduleItem = new UIScheduleItemEntity();
                        newUIScheduleItem.AddToTransaction(transaction);
                        LLBLGenEntityUtil.CopyFields(oldUIScheduleItem, newUIScheduleItem, uiScheduleItemFieldsNotToBeCopied);

                        // UI Schedule Item Occurrence
                        foreach (UIScheduleItemOccurrenceEntity oldUIScheduleItemOccurrence in oldUIScheduleItem.UIScheduleItemOccurrenceCollection)
                        {
                            UIScheduleItemOccurrenceEntity newUIScheduleItemOccurence = new UIScheduleItemOccurrenceEntity();
                            newUIScheduleItemOccurence.AddToTransaction(transaction);
                            LLBLGenEntityUtil.CopyFields(oldUIScheduleItemOccurrence, newUIScheduleItemOccurence, uiScheduleItemOccurrenceFieldsNotToBeCopied);

                            newUIScheduleItem.UIScheduleItemOccurrenceCollection.Add(newUIScheduleItemOccurence);

                            scheduleItemOccurenceCopyMapping.Add(oldUIScheduleItemOccurrence.UIScheduleItemOccurrenceId, newUIScheduleItemOccurence);
                        }
                        newUISchedule.UIScheduleItemCollection.Add(newUIScheduleItem);
                    }

                    foreach (UIScheduleItemEntity newUIScheduleItem in newUISchedule.UIScheduleItemCollection)
                    {
                        foreach (UIScheduleItemOccurrenceEntity newUIScheduleItemOccurrence in newUIScheduleItem.UIScheduleItemOccurrenceCollection)
                        {
                            if (newUIScheduleItemOccurrence.ParentUIScheduleItemOccurrenceId.HasValue)
                            {
                                UIScheduleItemOccurrenceEntity newUIScheduleItemOccurence;
                                if (scheduleItemOccurenceCopyMapping.TryGetValue(newUIScheduleItemOccurrence.ParentUIScheduleItemOccurrenceId.Value, out newUIScheduleItemOccurence))
                                {
                                    newUIScheduleItemOccurrence.ParentUIScheduleItemOccurrenceId = null;
                                    newUIScheduleItemOccurrence.ParentUIScheduleItemOccurrenceEntity = newUIScheduleItemOccurence;
                                }
                            }
                        }
                    }

                    newUISchedule.Save(true);
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw (ex);
                }
                finally
                {
                    transaction.Dispose();
                }
            }

            return newUISchedule;
        }
    }
}
