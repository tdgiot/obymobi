﻿using Obymobi.Data;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Cms.Logic.HelperClasses
{
    public class DeliverypointHelper
    {
        public static DeliverypointCollection GetDeliverypoints(int companyId, int? terminalId, bool addPosData, int deliverypointgroupId)
        {
            // First, create and initialize a filter
            PredicateExpression filter = new PredicateExpression();
            filter.Add(DeliverypointFields.CompanyId == companyId);

            SortExpression sort = new SortExpression();
            sort.Add(new SortClause(DeliverypointFields.Number, SortOperator.Ascending));

            PrefetchPath prefetch = new PrefetchPath(EntityType.DeliverypointEntity);
            prefetch.Add(DeliverypointEntityBase.PrefetchPathDeviceEntity);

            if (addPosData)
                prefetch.Add(DeliverypointEntityBase.PrefetchPathPosdeliverypointEntity);

            RelationCollection relations = new RelationCollection();

            if (deliverypointgroupId > 0)
            {
                // Add DPG relation for filter
                relations.Add(DeliverypointgroupEntityBase.Relations.DeliverypointEntityUsingDeliverypointgroupId);

                filter.Add(DeliverypointgroupFields.DeliverypointgroupId == deliverypointgroupId);
            }
            else
            {
                relations.Add(DeliverypointEntityBase.Relations.DeliverypointgroupEntityUsingDeliverypointgroupId);
                filter.Add(DeliverypointgroupFields.Active == true);
            }

            // Create and initialize a DeliverypointCollection instance
            // and retrieve the items using the filter
            DeliverypointCollection deliverypointCollection = new DeliverypointCollection();
            deliverypointCollection.GetMulti(filter, 0, sort, relations, prefetch);

            return deliverypointCollection;
        }
    }
}
