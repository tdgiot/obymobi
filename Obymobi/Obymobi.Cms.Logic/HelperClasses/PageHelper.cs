﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Cms.Logic.HelperClasses
{
    public class PageHelper
    {
        public static PageCollection GetPages(int siteId, bool onlyVisible)
        {
            if (siteId > 0)
            {
                PredicateExpression filter = new PredicateExpression();
                filter.Add(PageFields.SiteId == siteId);

                SortExpression sort = new SortExpression(new SortClause(PageFields.Name, SortOperator.Ascending));

                if (onlyVisible)
                {
                    filter.Add(PageFields.Visible == true);
                }

                return PageHelper.GetPages(filter, sort);
            }
            else
            {
                return new PageCollection();
            }
        }

        public static PageCollection GetPages(PredicateExpression filter, SortExpression sort)
        {
            PageCollection pages = new PageCollection();
            pages.GetMulti(filter, 0, sort, null, null);

            return pages;
        }
    }
}
