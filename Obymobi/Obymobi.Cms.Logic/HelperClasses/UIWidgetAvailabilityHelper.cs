﻿using Dionysos.Data.LLBLGen;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Cms.Logic.HelperClasses
{
    public class UIWidgetAvailabilityHelper
    {
        public static AvailabilityCollection GetAvailabilitiesByCompanyId(int companyId)
        {
            return EntityCollection.GetMulti<AvailabilityCollection>(new PredicateExpression(AvailabilityFields.CompanyId == companyId));
        }
    }
}