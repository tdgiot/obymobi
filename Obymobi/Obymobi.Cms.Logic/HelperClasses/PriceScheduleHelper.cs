﻿using Obymobi.Data.CollectionClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Obymobi.Cms.Logic.HelperClasses
{
    public class PriceScheduleHelper
    {
        public static PriceScheduleCollection GetPriceSchedules(int companyId)
        {
            PriceScheduleCollection priceSchedules = new PriceScheduleCollection();
            priceSchedules.GetMulti(new PredicateExpression(PriceScheduleFields.CompanyId == companyId));

            return priceSchedules;
        }
    }
}
