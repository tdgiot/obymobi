﻿using Obymobi.Data.CollectionClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Obymobi.Cms.Logic.HelperClasses
{
    public class PointOfInterestHelper
    {
        public static PointOfInterestCollection GetPointOfInterests()
        {
            PredicateExpression filter = new PredicateExpression();

            IncludeFieldsList fields = new IncludeFieldsList(PointOfInterestFields.PointOfInterestId);
            fields.Add(PointOfInterestFields.Name);

            SortExpression sort = new SortExpression(new SortClause(PointOfInterestFields.Name, SortOperator.Ascending));

            PointOfInterestCollection pois = new PointOfInterestCollection();
            pois.GetMulti(filter, 0, sort, null, null, fields, 0, 0);

            return pois;
        }
    }
}
