﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Logic.Model;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data;
using Obymobi.Enums;

namespace Obymobi.Cms.Logic.HelperClasses
{
    public static class UITabHelper
    {
        public static UITabCollection GetUiTabCollectionByClientId(int clientId)
        {
            PredicateExpression filter = new PredicateExpression(ClientFields.ClientId == clientId);

            RelationCollection relations = new RelationCollection();
            relations.Add(UITabEntityBase.Relations.UIModeEntityUsingUIModeId);
            relations.Add(UIModeEntityBase.Relations.DeliverypointgroupEntityUsingUIModeId);
            relations.Add(DeliverypointgroupEntityBase.Relations.ClientEntityUsingDeliverypointGroupId);

            UITabCollection uiTabCollection = new UITabCollection();
            uiTabCollection.GetMulti(filter, relations);

            return uiTabCollection;
        }

        public static UITabCollection GetUiTabCollectionByTerminalId(int terminalId)
        {
            PredicateExpression filter = new PredicateExpression(TerminalFields.TerminalId == terminalId);

            RelationCollection relations = new RelationCollection();
            relations.Add(UITabEntityBase.Relations.UIModeEntityUsingUIModeId);
            relations.Add(UIModeEntityBase.Relations.TerminalEntityUsingUIModeId);

            UITabCollection uiTabCollection = new UITabCollection();
            uiTabCollection.GetMulti(filter, relations);

            return uiTabCollection;
        }
        
        public static List<UITabType> GetSupportedUITabTypes(UIModeType uiModeType)
        {
            List<UITabType> uiTabs = new List<UITabType>();

            switch (uiModeType)
            {
                case UIModeType.VenueOwnedUserDevices:
                    uiTabs.Add(UITabType.Home);
                    uiTabs.Add(UITabType.Basket);
                    uiTabs.Add(UITabType.More);
                    uiTabs.Add(UITabType.Category);
                    uiTabs.Add(UITabType.Entertainment);
                    uiTabs.Add(UITabType.Web);
                    uiTabs.Add(UITabType.Site);
                    uiTabs.Add(UITabType.Placeholder);
                    uiTabs.Add(UITabType.RoomControl);
                    uiTabs.Add(UITabType.ExploreMap);
                    break;
                case UIModeType.VenueOwnedStaffDevices:
                    uiTabs.Add(UITabType.Orders);
                    uiTabs.Add(UITabType.Message);
                    uiTabs.Add(UITabType.Management);
                    uiTabs.Add(UITabType.Socialmedia);
                    uiTabs.Add(UITabType.TerminalStatus);
                    uiTabs.Add(UITabType.OrderCount);
                    uiTabs.Add(UITabType.Cms);
                    uiTabs.Add(UITabType.Entertainment);
                    uiTabs.Add(UITabType.Web);
                    uiTabs.Add(UITabType.DeliveryTimes);
                    uiTabs.Add(UITabType.UIModes);
                    break;
                case UIModeType.GuestOwnedMobileDevices:
                    uiTabs.Add(UITabType.Category);
                    uiTabs.Add(UITabType.Web);
                    uiTabs.Add(UITabType.Site);
                    uiTabs.Add(UITabType.ExploreMap);
                    break;
                case UIModeType.GuestOwnedTabletDevices:
                    uiTabs.Add(UITabType.Home);
                    uiTabs.Add(UITabType.Category);
                    uiTabs.Add(UITabType.Web);
                    uiTabs.Add(UITabType.Site);
                    uiTabs.Add(UITabType.ExploreMap);
                    break;
                default:
                    throw new NotImplementedException("UITabHelper.GetSupportedUITabTypes not implemented for UImodeType: " + uiModeType);                    
            }

            return uiTabs;
        }
    }
}