﻿using Crave.Adyen.Api.Client;
using System.Net.Http;
using Obymobi.Data.EntityClasses;

namespace Obymobi.Cms.Logic.HelperClasses
{
    public static class AdyenClientFactory
    {
        private static readonly string BaseUrl;

        static AdyenClientFactory()
        {
            BaseUrl = System.Configuration.ConfigurationManager.AppSettings["CraveGatewayPublicUrl"] + "/adyen";
        }

        public static AccountHoldersClient CreateAccountHoldersClient(PaymentProviderEntity paymentProvider)
        {
            HttpClient httpClient = new HttpClient();
            httpClient.DefaultRequestHeaders.Add("Authorization", CognitoHelper.IdToken);
            httpClient.DefaultRequestHeaders.Add("adyen-environment", paymentProvider.Environment.ToString().ToLowerInvariant());

            return new AccountHoldersClient(httpClient)
            {
                BaseUrl = BaseUrl
            };
        }
    }
}
