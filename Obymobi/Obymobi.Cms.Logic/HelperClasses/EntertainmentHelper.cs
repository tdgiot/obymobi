﻿using Obymobi.Data.CollectionClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Obymobi.Enums;

namespace Obymobi.Cms.Logic.HelperClasses
{
    public class EntertainmentHelper
    {
        public static EntertainmentCollection GetEntertainment(int companyId, EntertainmentType type)
        {
            PredicateExpression filter = new PredicateExpression();
            filter.Add(EntertainmentFields.EntertainmentType == type);

            PredicateExpression subFilter = new PredicateExpression();
            subFilter.Add(EntertainmentFields.CompanyId == companyId);
            subFilter.AddWithOr(EntertainmentFields.CompanyId == DBNull.Value);

            filter.Add(subFilter);

            SortExpression sort = new SortExpression();
            sort.Add(new SortClause(EntertainmentFields.Name, SortOperator.Ascending));

            EntertainmentCollection entertainment = new EntertainmentCollection();
            entertainment.GetMulti(filter, 0, sort);

            return entertainment;
        }

        public static EntertainmentCollection GetEntertainments(int companyId, bool includeGenericEntertertainment, bool onlyVisible)
        {
            PredicateExpression companyFilter = new PredicateExpression();
            companyFilter.Add(EntertainmentFields.CompanyId == companyId);

            SortExpression sort = new SortExpression(new SortClause(EntertainmentFields.Name, SortOperator.Ascending));

            if (includeGenericEntertertainment)
            {
                companyFilter.AddWithOr(EntertainmentFields.CompanyId == DBNull.Value);
            }

            PredicateExpression filter = new PredicateExpression();
            filter.Add(companyFilter);

            if (onlyVisible)
            {
                filter.Add(EntertainmentFields.Visible == true);
            }

            return EntertainmentHelper.GetEntertainments(filter, sort);
        }

        public static EntertainmentCollection GetEntertainments(PredicateExpression filter, SortExpression sort)
        {
            EntertainmentCollection entertainments = new EntertainmentCollection();
            entertainments.GetMulti(filter, 0, sort, null, null);

            return entertainments;
        }
    }
}
