﻿using System.Collections.Generic;
using System.Linq;
using Dionysos.Data.LLBLGen;
using Obymobi.Data;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Cms.Logic.HelperClasses
{
    public class CategoryHelper
    {
        public static CategoryCollection GetCategoriesWithPrefetchedParents(List<int> categoryIds)
        {
            var filter = new PredicateExpression();
            filter.Add(CategoryFields.CategoryId == categoryIds);

            var prefetch = new PrefetchPath(EntityType.CategoryEntity);
            prefetch.Add(CategoryEntity.PrefetchPathCategorySuggestionCollection);
            prefetch.Add(CategoryEntity.PrefetchPathCategoryAlterationCollection);

            var sortCategorySuggestions = new SortExpression(CategorySuggestionFields.SortOrder | SortOperator.Ascending);
            var sortCategoryAlterations = new SortExpression(CategoryAlterationFields.SortOrder | SortOperator.Ascending);

            List<IPrefetchPathElement> categoryPaths = new List<IPrefetchPathElement>();
            for (int i = 0; i < 5; i++)
            {
                IPrefetchPathElement prefetchParentCategory = CategoryEntity.PrefetchPathParentCategoryEntity;

                // Category > CategorySuggestion 
                prefetchParentCategory.SubPath.Add(CategoryEntity.PrefetchPathCategorySuggestionCollection, 0, null, null, sortCategorySuggestions);

                // Category > CategoryAlteration
                prefetchParentCategory.SubPath.Add(CategoryEntity.PrefetchPathCategoryAlterationCollection, 0, null, null, sortCategoryAlterations);

                categoryPaths.Add(prefetchParentCategory);
            }
            prefetch.Add(categoryPaths[0]).SubPath.Add(categoryPaths[1]).SubPath.Add(categoryPaths[2]).SubPath.Add(categoryPaths[3]).SubPath.Add(categoryPaths[4]);

            var categoryCollection = new CategoryCollection();
            categoryCollection.GetMulti(filter, 0, null, null, prefetch);

            return categoryCollection;
        }

        public static void AddAlterationsFromCategories(CategoryAlterationCollection categoryAlterations, CategoryEntity category, int version)
        {
            if (category.CategoryAlterationCollection.Count > 0)
            {
                categoryAlterations.AddRange(category.CategoryAlterationCollection.Where(x => x.Version == version));
            }
            else if (category.ParentCategoryId.HasValue)
            {
                CategoryHelper.AddAlterationsFromCategories(categoryAlterations, category.ParentCategoryEntity, version);
            }
        }

        public static void AddSuggestionsFromCategories(CategorySuggestionCollection categorySuggestions, CategoryEntity category)
        {
            if (category.CategorySuggestionCollection.Count > 0)
            {
                categorySuggestions.AddRange(category.CategorySuggestionCollection);
            }
            else if (category.ParentCategoryId.HasValue)
            {
                CategoryHelper.AddSuggestionsFromCategories(categorySuggestions, category.ParentCategoryEntity);
            }
        }

        public static CategoryCollection GetCategories(int companyId, bool onlyVisible)
        {
            PredicateExpression filter = new PredicateExpression(CategoryFields.CompanyId == companyId);
            if (onlyVisible)
            {
                filter.Add(CategoryFields.Visible == true);
            }

            SortExpression sort = new SortExpression();
            sort.Add(CategoryFields.MenuId | SortOperator.Ascending);
            sort.Add(CategoryFields.Name | SortOperator.Ascending);

            return CategoryHelper.GetCategories(filter, sort);
        }

        public static CategoryCollection GetCategories(PredicateExpression filter, SortExpression sort)
        {
            CategoryCollection categories = new CategoryCollection();
            categories.GetMulti(filter, 0, sort, null, null);

            return categories;
        }

        public static CategoryCollection GetOrderedCategories(int menuId)
        {
            PredicateExpression filter = new PredicateExpression();
            filter.Add(CategoryFields.MenuId == menuId);

            CategoryCollection allCategories = new CategoryCollection();
            allCategories.GetMulti(filter);

            CategoryCollection orderedCategories = new CategoryCollection();
            foreach (CategoryEntity rootCategory in allCategories.Where(x => !x.ParentCategoryId.HasValue).OrderBy(x => x.SortOrder))
            {
                orderedCategories.Add(rootCategory);
                orderedCategories.AddRange(CategoryHelper.GetOrderedChildCategories(rootCategory.ChildCategoryCollection));
            }

            return orderedCategories;
        }

        private static CategoryCollection GetOrderedChildCategories(CategoryCollection children)
        {
            CategoryCollection toReturn = new CategoryCollection();

            CategoryCollection orderedChildren = children.OrderBy(x => x.SortOrder).ToEntityCollection<CategoryCollection>();
            foreach (CategoryEntity orderedChild in orderedChildren)
            {
                toReturn.Add(orderedChild);

                if (orderedChild.ChildCategoryCollection.Count > 0)
                {
                    toReturn.AddRange(CategoryHelper.GetOrderedChildCategories(orderedChild.ChildCategoryCollection));
                }
            }

            return toReturn;
        }
    }
}
