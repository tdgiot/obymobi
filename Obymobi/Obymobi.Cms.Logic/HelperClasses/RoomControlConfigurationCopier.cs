﻿using System;
using System.Collections;
using System.Collections.Generic;
using Dionysos;
using Dionysos.Data.LLBLGen;
using Obymobi.Data;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Logic.HelperClasses;
using Obymobi.Web.CloudStorage;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Cms.Logic.HelperClasses
{
    public class RoomControlConfigurationCopier
    {
        private readonly int sourceRoomControlConfigurationId;
        private readonly string destinationRoomControlConfigurationName;
        private readonly int? destinationCompanyId;

        private readonly IDictionary<int, RoomControlComponentEntity> roomControlComponents;
        private readonly IDictionary<int, RoomControlWidgetEntity> roomControlWidgets;
        private readonly IDictionary<MediaEntity, MediaEntity> medias;

        private static readonly ArrayList RoomControlAreaFieldsNotToCopy = new ArrayList
        {
            RoomControlAreaFields.RoomControlConfigurationId
        };

        private static readonly ArrayList RoomControlComponentFieldsNotToCopy = new ArrayList
        {
            RoomControlComponentFields.RoomControlSectionId
        };        

        private static readonly ArrayList RoomControlSectionFieldsNotToCopy = new ArrayList
        {
            RoomControlSectionFields.RoomControlAreaId
        };

        private static readonly ArrayList RoomControlSectionItemFieldsNotToCopy = new ArrayList
        {
            RoomControlSectionItemFields.RoomControlSectionId
        };

        private static readonly ArrayList RoomControlWidgetFieldsNotToCopy = new ArrayList
        {
            RoomControlWidgetFields.RoomControlSectionItemId
        };        

        private static readonly ArrayList MediaFieldsNotToCopy = new ArrayList
        {
            MediaFields.RoomControlSectionId,
            MediaFields.RoomControlSectionItemId,
            MediaFields.LastDistributedVersionTicksAmazon
        };

        private static readonly ArrayList MediaRatioTypeMediaFieldsNotToCopy = new ArrayList
        {
            MediaRatioTypeMediaFields.MediaId
        };        

        private static readonly ArrayList CustomTextFieldsNotToCopy = new ArrayList
        {
            CustomTextFields.RoomControlAreaId,
            CustomTextFields.RoomControlSectionId,
            CustomTextFields.RoomControlComponentId,
            CustomTextFields.RoomControlSectionItemId,
            CustomTextFields.RoomControlWidgetId
        };        

        public RoomControlConfigurationCopier(
            int sourceRoomControlConfigurationId,
            string destinationRoomControlConfigurationName,
            int? destinationCompanyId)
        {
            this.sourceRoomControlConfigurationId = sourceRoomControlConfigurationId;
            this.destinationRoomControlConfigurationName = destinationRoomControlConfigurationName;
            this.destinationCompanyId = destinationCompanyId;

            roomControlComponents = new Dictionary<int, RoomControlComponentEntity>();
            roomControlWidgets = new Dictionary<int, RoomControlWidgetEntity>();
            medias = new Dictionary<MediaEntity, MediaEntity>();
        }

        public RoomControlConfigurationEntity Copy()
        {
            roomControlComponents.Clear();
            roomControlWidgets.Clear();
            medias.Clear();

            PrefetchPath path = new PrefetchPath(EntityType.RoomControlConfigurationEntity);
            
            IPrefetchPathElement roomAreasPath = path.Add(RoomControlConfigurationEntityBase.PrefetchPathRoomControlAreaCollection);
            roomAreasPath.SubPath.Add(RoomControlAreaEntityBase.PrefetchPathCustomTextCollection);
            
            IPrefetchPathElement roomSectionsPath = roomAreasPath.SubPath.Add(RoomControlAreaEntityBase.PrefetchPathRoomControlSectionCollection);
            roomSectionsPath.SubPath.Add(RoomControlSectionEntityBase.PrefetchPathCustomTextCollection);
            IPrefetchPathElement roomSectionsMediaPath = roomSectionsPath.SubPath.Add(RoomControlSectionEntityBase.PrefetchPathMediaCollection);
            roomSectionsMediaPath.SubPath.Add(MediaEntityBase.PrefetchPathMediaRatioTypeMediaCollection);
            
            IPrefetchPathElement roomComponentsPath = roomSectionsPath.SubPath.Add(RoomControlSectionEntityBase.PrefetchPathRoomControlComponentCollection);
            roomComponentsPath.SubPath.Add(RoomControlComponentEntityBase.PrefetchPathCustomTextCollection);
            
            IPrefetchPathElement roomSectionItemsPath = roomSectionsPath.SubPath.Add(RoomControlSectionEntityBase.PrefetchPathRoomControlSectionItemCollection);
            IPrefetchPathElement roomSectionItemsMediaPath = roomSectionItemsPath.SubPath.Add(RoomControlSectionItemEntityBase.PrefetchPathMediaCollection);
            roomSectionItemsMediaPath.SubPath.Add(MediaEntityBase.PrefetchPathMediaRatioTypeMediaCollection);
            roomSectionItemsPath.SubPath.Add(RoomControlSectionItemEntityBase.PrefetchPathCustomTextCollection);
            
            IPrefetchPathElement sectionWidgetsPath = roomSectionsPath.SubPath.Add(RoomControlSectionEntityBase.PrefetchPathRoomControlWidgetCollection);
            sectionWidgetsPath.SubPath.Add(RoomControlWidgetEntityBase.PrefetchPathCustomTextCollection);
            IPrefetchPathElement sectionItemWidgetsPath = roomSectionItemsPath.SubPath.Add(RoomControlSectionItemEntityBase.PrefetchPathRoomControlWidgetCollection);
            sectionItemWidgetsPath.SubPath.Add(RoomControlWidgetEntityBase.PrefetchPathCustomTextCollection);

            RoomControlConfigurationEntity source = new RoomControlConfigurationEntity(sourceRoomControlConfigurationId, path);

            Transaction transaction = new Transaction(System.Data.IsolationLevel.ReadUncommitted, string.Format("RoomControlConfigurationHelper.CopyConfiguration-RoomControlConfigurationId{0}-to-{1}", sourceRoomControlConfigurationId, destinationRoomControlConfigurationName));
            RoomControlConfigurationEntity target;
            try
            {
                target = new RoomControlConfigurationEntity();
                target.AddToTransaction(transaction);
                target.CompanyId = destinationCompanyId ?? source.CompanyId;
                target.Name = destinationRoomControlConfigurationName;
                target.Save();

                target.RoomControlAreaCollection.AddRange(CopyAreas(source.RoomControlAreaCollection));
                target.Save(true);

                UpdateWidgetComponentIds(transaction);
                
                transaction.Commit();

                UploadMedia();
            }
            catch (Exception ex)
            {
                transaction.Rollback();
                throw(ex);
            }
            finally
            {
                transaction.Dispose();
            }

            return target;
        }

        private void UploadMedia()
        {
            foreach (KeyValuePair<MediaEntity, MediaEntity> pair in medias)
            {
                MediaEntity source = pair.Key;
                MediaEntity target = pair.Value;

                target.FileBytes = source.Download();
                target.Upload(target.FileBytes);

                foreach (MediaRatioTypeMediaEntity mediaRatioTypeMedia in target.MediaRatioTypeMediaCollection)
                {
                    MediaHelper.UploadToCdn(mediaRatioTypeMedia, target);
                }
            }
        }

        private RoomControlAreaCollection CopyAreas(RoomControlAreaCollection sources)
        {
            RoomControlAreaCollection targets = new RoomControlAreaCollection();

            foreach (RoomControlAreaEntity source in sources)
            {
                RoomControlAreaEntity target = new RoomControlAreaEntity();
                LLBLGenEntityUtil.CopyFields(source, target, RoomControlAreaFieldsNotToCopy);
                targets.Add(target);

                foreach (CustomTextEntity customTextSource in source.CustomTextCollection)
                {
                    CustomTextEntity customTextTarget = new CustomTextEntity();
                    LLBLGenEntityUtil.CopyFields(customTextSource, customTextTarget, CustomTextFieldsNotToCopy);
                    target.CustomTextCollection.Add(customTextTarget);

                    if (customTextTarget.Text.IsNullOrWhiteSpace())
                    {
                        throw new Exception("Empty Text - Area - " + source.NameSystem);
                    }
                }

                target.RoomControlSectionCollection.AddRange(CopySections(source.RoomControlSectionCollection));
            }

            return targets;
        }

        private RoomControlSectionCollection CopySections(RoomControlSectionCollection sources)
        {
            RoomControlSectionCollection targets = new RoomControlSectionCollection();                                              

            foreach (RoomControlSectionEntity source in sources)
            {
                RoomControlSectionEntity target = new RoomControlSectionEntity();
                LLBLGenEntityUtil.CopyFields(source, target, RoomControlSectionFieldsNotToCopy);
                targets.Add(target);

                foreach (CustomTextEntity sectionCustomTextSource in source.CustomTextCollection)
                {
                    CustomTextEntity sectionCustomTextTarget = new CustomTextEntity();
                    LLBLGenEntityUtil.CopyFields(sectionCustomTextSource, sectionCustomTextTarget, CustomTextFieldsNotToCopy);
                    target.CustomTextCollection.Add(sectionCustomTextTarget);

                    if (sectionCustomTextTarget.Text.IsNullOrWhiteSpace())
                    {
                        throw new Exception("Empty Text - Section - " + source.NameSystem);
                    }
                }                

                foreach (MediaEntity mediaSource in source.MediaCollection)
                {
                    MediaEntity mediaTarget = new MediaEntity();
                    LLBLGenEntityUtil.CopyFields(mediaSource, mediaTarget, MediaFieldsNotToCopy);
                    target.MediaCollection.Add(mediaTarget);
                    medias.Add(mediaSource, mediaTarget);

                    foreach (MediaRatioTypeMediaEntity mrtmSource in mediaSource.MediaRatioTypeMediaCollection)
                    {
                        MediaRatioTypeMediaEntity mrtmTarget = new MediaRatioTypeMediaEntity();
                        LLBLGenEntityUtil.CopyFields(mrtmSource, mrtmTarget, MediaRatioTypeMediaFieldsNotToCopy);                        

                        mediaTarget.MediaRatioTypeMediaCollection.Add(mrtmTarget);
                    }
                }

                target.RoomControlComponentCollection.AddRange(CopyComponents(source.RoomControlComponentCollection));
                target.RoomControlSectionItemCollection.AddRange(CopySectionItems(source.RoomControlSectionItemCollection));
                target.RoomControlWidgetCollection.AddRange(CopyWidgets(source.RoomControlWidgetCollection));
            }

            return targets;
        }

        private RoomControlComponentCollection CopyComponents(RoomControlComponentCollection sources)
        {
            RoomControlComponentCollection targets = new RoomControlComponentCollection();            

            foreach (RoomControlComponentEntity source in sources)
            {
                RoomControlComponentEntity target = new RoomControlComponentEntity();
                LLBLGenEntityUtil.CopyFields(source, target, RoomControlComponentFieldsNotToCopy);
                targets.Add(target);

                foreach (CustomTextEntity componentCustomTextSource in source.CustomTextCollection)
                {
                    CustomTextEntity componentCustomTextTarget = new CustomTextEntity();
                    LLBLGenEntityUtil.CopyFields(componentCustomTextSource, componentCustomTextTarget, CustomTextFieldsNotToCopy);
                    target.CustomTextCollection.Add(componentCustomTextTarget);

                    if (componentCustomTextTarget.Text.IsNullOrWhiteSpace())
                    {
                        throw new Exception("Empty Text - Component - " + source.NameSystem);
                    }
                }     
           
                roomControlComponents.Add(source.RoomControlComponentId, target);
            }

            return targets;
        }

        private RoomControlSectionItemCollection CopySectionItems(RoomControlSectionItemCollection sources)
        {
            RoomControlSectionItemCollection targets = new RoomControlSectionItemCollection();
            
            foreach (RoomControlSectionItemEntity source in sources)
            {
                RoomControlSectionItemEntity target = new RoomControlSectionItemEntity();
                LLBLGenEntityUtil.CopyFields(source, target, RoomControlSectionItemFieldsNotToCopy);
                target.StationListId = IsCrossCompanyCopy ? null : target.StationListId;
                targets.Add(target);

                foreach (CustomTextEntity sectionItemCustomTextSource in source.CustomTextCollection)
                {
                    CustomTextEntity sectionItemCustomTextTarget = new CustomTextEntity();
                    LLBLGenEntityUtil.CopyFields(sectionItemCustomTextSource, sectionItemCustomTextTarget, CustomTextFieldsNotToCopy);
                    target.CustomTextCollection.Add(sectionItemCustomTextTarget);

                    if (sectionItemCustomTextTarget.Text.IsNullOrWhiteSpace())
                    {
                        throw new Exception("Empty Text - SectionItem - " + source.NameSystem);
                    }
                }

                foreach (MediaEntity mediaSource in source.MediaCollection)
                {
                    MediaEntity mediaTarget = new MediaEntity();
                    LLBLGenEntityUtil.CopyFields(mediaSource, mediaTarget, MediaFieldsNotToCopy);
                    target.MediaCollection.Add(mediaTarget);
                    medias.Add(mediaSource, mediaTarget);

                    foreach (MediaRatioTypeMediaEntity mrtmSource in mediaSource.MediaRatioTypeMediaCollection)
                    {
                        MediaRatioTypeMediaEntity mrtmTarget = new MediaRatioTypeMediaEntity();
                        LLBLGenEntityUtil.CopyFields(mrtmSource, mrtmTarget, MediaRatioTypeMediaFieldsNotToCopy);                      

                        mediaTarget.MediaRatioTypeMediaCollection.Add(mrtmTarget);
                    }
                }

                target.RoomControlWidgetCollection.AddRange(CopyWidgets(source.RoomControlWidgetCollection));
            }

            return targets;
        }

        private RoomControlWidgetCollection CopyWidgets(RoomControlWidgetCollection sources)
        {
            RoomControlWidgetCollection targets = new RoomControlWidgetCollection();            

            foreach (RoomControlWidgetEntity source in sources)
            {
                RoomControlWidgetEntity target = new RoomControlWidgetEntity();
                LLBLGenEntityUtil.CopyFields(source, target, RoomControlWidgetFieldsNotToCopy);
                targets.Add(target);

                foreach (CustomTextEntity widgetCustomTextSource in source.CustomTextCollection)
                {
                    if (!widgetCustomTextSource.Text.IsNullOrWhiteSpace())
                    {
                        CustomTextEntity widgetCustomTextTarget = new CustomTextEntity();
                        LLBLGenEntityUtil.CopyFields(widgetCustomTextSource, widgetCustomTextTarget, CustomTextFieldsNotToCopy);
                        target.CustomTextCollection.Add(widgetCustomTextTarget);
                    }
                }      
          
                roomControlWidgets.Add(source.RoomControlWidgetId, target);
            }

            return targets;
        }

        private void UpdateWidgetComponentIds(ITransaction transaction)
        {
            foreach (KeyValuePair<int, RoomControlWidgetEntity> valuePair in roomControlWidgets)
            {
                RoomControlWidgetEntity widget = valuePair.Value;

                widget.RoomControlComponentId1 = ConvertRoomControlComponent(widget.RoomControlComponentId1);
                widget.RoomControlComponentId2 = ConvertRoomControlComponent(widget.RoomControlComponentId2);
                widget.RoomControlComponentId3 = ConvertRoomControlComponent(widget.RoomControlComponentId3);
                widget.RoomControlComponentId4 = ConvertRoomControlComponent(widget.RoomControlComponentId4);
                widget.RoomControlComponentId5 = ConvertRoomControlComponent(widget.RoomControlComponentId5);
                widget.RoomControlComponentId6 = ConvertRoomControlComponent(widget.RoomControlComponentId6);
                widget.RoomControlComponentId7 = ConvertRoomControlComponent(widget.RoomControlComponentId7);
                widget.RoomControlComponentId8 = ConvertRoomControlComponent(widget.RoomControlComponentId8);
                widget.RoomControlComponentId9 = ConvertRoomControlComponent(widget.RoomControlComponentId9);
                widget.RoomControlComponentId10 = ConvertRoomControlComponent(widget.RoomControlComponentId10);

                widget.AddToTransaction(transaction);
                widget.Save();
            }
        }

        private int? ConvertRoomControlComponent(int? value)
        {
            if (value.IsNullOrZero())
            {
                return value;
            }

            int componentId = value.GetValueOrDefault();

            if (!roomControlComponents.TryGetValue(componentId, out RoomControlComponentEntity componentEntity))
            {
                return value;
            }

            return componentEntity.RoomControlComponentId;
        }

        private bool IsCrossCompanyCopy => destinationCompanyId.GetValueOrDefault(0) > 0;
    }
}