﻿using Obymobi.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Obymobi.Cms.Logic.HelperClasses
{
    public class AlterationoptionHelper
    {
        public static List<AlterationType> GetSupportedAlterationoptionTypes(AlterationDialogMode mode)
        {
            if (mode == AlterationDialogMode.v3)
            {
                return AlterationoptionHelper.AlterationoptionTypesForV3;
            }
            else
            {
                return AlterationoptionHelper.AlterationoptionTypesForV1And2;
            }
        }

        public static List<AlterationType> GetSupportedAlterationoptionTypesForAlteration(AlterationType alterationType)
        {
            if (alterationType == AlterationType.Options)
            {
                return AlterationoptionHelper.AlterationoptionTypesForOptionsAlteration;
            }
            else if (alterationType == AlterationType.Form)
            {
                return AlterationoptionHelper.AlterationoptionTypesForFormAlteration;
            }
            else if (alterationType == AlterationType.Package)
            {
                return AlterationoptionHelper.AlterationoptionTypesForPackageAlteration;
            }
            else if (alterationType == AlterationType.Page)
            {
                return AlterationoptionHelper.AlterationoptionTypesForPageAlteration;
            }
            else if (alterationType == AlterationType.Category)
            {
                return AlterationoptionHelper.AlterationoptionTypesForCategoryAlteration;
            }
            return new List<AlterationType>();
        }

        private static readonly List<AlterationType> AlterationoptionTypesForV1And2 = new List<AlterationType>
                                                                           {
                                            AlterationType.Options
                                                                           };

        private static readonly List<AlterationType> AlterationoptionTypesForV3 = new List<AlterationType>
                                                                           {
                                            AlterationType.Options,
                                            AlterationType.Text,
                                            AlterationType.Numeric,
                                            AlterationType.Email
                                                                           };

        private static readonly List<AlterationType> AlterationoptionTypesForOptionsAlteration = new List<AlterationType>
                                                                            {
                                            AlterationType.Options
                                                                           };

        private static readonly List<AlterationType> AlterationoptionTypesForFormAlteration = new List<AlterationType>
                                                                            {
                                            AlterationType.Text,
                                            AlterationType.Numeric,
                                            AlterationType.Email
                                                                           };

        private static readonly List<AlterationType> AlterationoptionTypesForPackageAlteration = new List<AlterationType>
                                                                            {
                                            AlterationType.Page,
                                            AlterationType.Summary
                                                                           };

        private static readonly List<AlterationType> AlterationoptionTypesForPageAlteration = new List<AlterationType>
                                                                            {
                                            AlterationType.Options,
                                            AlterationType.DateTime,
                                            AlterationType.Date,
                                            AlterationType.Email,
                                            AlterationType.Text,
                                            AlterationType.Numeric,
                                            AlterationType.Category,
                                            AlterationType.Notes,
                                            AlterationType.Image,
                                            AlterationType.Instruction
                                                                           };

        private static readonly List<AlterationType> AlterationoptionTypesForCategoryAlteration = new List<AlterationType>
                                                                            {
                                            AlterationType.Category
                                                                            };
    }
}
