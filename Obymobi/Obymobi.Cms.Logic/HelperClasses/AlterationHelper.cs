﻿using System;
using System.Collections;
using System.Collections.Generic;
using Dionysos.Data.LLBLGen;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Cms.Logic.HelperClasses
{
    public class AlterationHelper
    {
        /// <summary>
        /// Removes the alteration from all products.
        /// </summary>
        /// <param name="alterationId">The alteration id.</param>
        /// <param name="removedFromXProducts">The amount of products from which the alteration was removed.</param>
        public static void RemoveAlterationFromProducts(int alterationId, out int removedFromXProducts)
        {
            ProductAlterationCollection productalterations = new ProductAlterationCollection();
            PredicateExpression filterProductAlterations = new PredicateExpression();
            filterProductAlterations.Add(ProductAlterationFields.AlterationId == alterationId);
            productalterations.GetMulti(filterProductAlterations);

            Transaction transaction = new Transaction(System.Data.IsolationLevel.ReadUncommitted, "AlterationHelper.RemoveAlterationFromProducts." + alterationId);
            try
            {
                removedFromXProducts = productalterations.Count;
                foreach (var pa in productalterations)
                {
                    transaction.Add(pa);
                    pa.Delete();
                }
                transaction.Commit();
            }
            catch
            {
                transaction.Rollback();
                throw;
            }
            finally
            {
                transaction.Dispose();
            }
        }

        /// <summary>
        /// Creates and returns a copy of the old alteration etity.
        /// </summary>
        /// <param name="oldAlteration"></param>
        /// <param name="transaction"></param>
        /// <returns></returns>
        public static AlterationEntity CopyAlteration(AlterationEntity oldAlteration, int companyId, Transaction transaction = null)
        {
            AlterationEntity newAlteration = null;
            if (oldAlteration != null)
            {
                // FO-CMS: Copy the fields?
                // Copy alteration entity
                ArrayList alterationFieldsNotToBeCopied = new ArrayList
                {
                    AlterationFields.CompanyId,
                    AlterationFields.PosalterationId
                };

                newAlteration = new AlterationEntity();
                if (transaction != null)
                    newAlteration.AddToTransaction(transaction);

                LLBLGenEntityUtil.CopyFields(oldAlteration, newAlteration, alterationFieldsNotToBeCopied);
                newAlteration.CompanyId = companyId;

                foreach (AlterationitemEntity oldAlterationitemEntity in oldAlteration.AlterationitemCollection)
                {
                    // Copy alteration item
                    ArrayList alterationitemFieldsNotToBeCopied = new ArrayList
                    {
                        AlterationitemFields.AlterationId,
                        AlterationitemFields.AlterationoptionId,
                        AlterationitemFields.ParentCompanyId
                    };

                    AlterationitemEntity newAlterationitemEntity = newAlteration.AlterationitemCollection.AddNew();
                    if (transaction != null)
                        newAlterationitemEntity.AddToTransaction(transaction);

                    LLBLGenEntityUtil.CopyFields(oldAlterationitemEntity, newAlterationitemEntity, alterationitemFieldsNotToBeCopied);
                    newAlterationitemEntity.ParentCompanyId = companyId;

                    // Copy alteration option
                    ArrayList alterationoptionFieldsNotToBeCopied = new ArrayList
                    {
                        AlterationoptionFields.CompanyId,
                        AlterationoptionFields.PosalterationoptionId,
                        AlterationoptionFields.PosproductId
                    };

                    AlterationoptionEntity newAlterationoptionEntity = new AlterationoptionEntity();
                    if (transaction != null)
                        newAlterationoptionEntity.AddToTransaction(transaction);

                    LLBLGenEntityUtil.CopyFields(oldAlterationitemEntity.AlterationoptionEntity, newAlterationoptionEntity, alterationoptionFieldsNotToBeCopied);
                    newAlterationoptionEntity.CompanyId = companyId;
                    newAlterationitemEntity.AlterationoptionEntity = newAlterationoptionEntity;
                }
            }
            return newAlteration;
        }

        public static List<AlterationType> GetSupportedAlterationTypes(AlterationDialogMode mode)
        {
            if (mode == AlterationDialogMode.v3)
            {
                return AlterationHelper.AlterationTypesForV3;
            }
            else
            {
                return AlterationHelper.AlterationTypesForV1And2;
            }
        }

        private static readonly List<AlterationType> AlterationTypesForV1And2 = new List<AlterationType>
                                                                           {
                                            AlterationType.Options,
                                            AlterationType.DateTime,
                                            AlterationType.Date,
                                            AlterationType.Email,
                                            AlterationType.Text,
                                            AlterationType.Numeric,
                                            AlterationType.Package
                                                                           };

        private static readonly List<AlterationType> AlterationTypesForV3 = new List<AlterationType>
                                                                           {
                                            AlterationType.Options,
                                            AlterationType.DateTime,
                                            AlterationType.Date,
                                            AlterationType.Form
                                                                           };
    }
}
