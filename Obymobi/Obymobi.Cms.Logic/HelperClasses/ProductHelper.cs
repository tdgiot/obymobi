﻿using Obymobi.Data;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Obymobi.Cms.Logic.HelperClasses
{
    public class ProductHelper
    {
        public static ProductCategoryCollection GetProductCategoriesAvailableAsSuggestionForCompany(int companyId, bool NameFieldOnly, int? menuId)
        {
            PredicateExpression filterProductCategories = new PredicateExpression();
            filterProductCategories.Add(ProductFields.CompanyId == companyId);
            filterProductCategories.Add(ProductFields.VisibilityType != VisibilityType.Never);
            filterProductCategories.Add(ProductFields.Type == (int)ProductType.Product);

            if (menuId.HasValue)
                filterProductCategories.Add(CategoryFields.MenuId == menuId.Value);

            PredicateExpression excludeServiceRequestsFilter = new PredicateExpression();
            excludeServiceRequestsFilter.Add(CategoryFields.Type != CategoryType.ServiceItems);
            excludeServiceRequestsFilter.Add(ProductFields.SubType == ProductSubType.Inherit);

            PredicateExpression subFilter = new PredicateExpression();
            subFilter.Add(ProductFields.SubType == (int)ProductSubType.Normal);
            subFilter.AddWithOr(excludeServiceRequestsFilter);

            filterProductCategories.Add(subFilter);

            ExcludeIncludeFieldsList fieldsList;
            if (NameFieldOnly)
            {
                fieldsList = new IncludeFieldsList();
                fieldsList.Add(ProductFields.Name);
                fieldsList.Add(CategoryFields.Name);
                fieldsList.Add(MenuFields.Name);
                fieldsList.Add(ProductCategoryFields.ParentCompanyId);
            }
            else
                fieldsList = new ExcludeIncludeFieldsList();

            PrefetchPath prefetch = new PrefetchPath(EntityType.ProductCategoryEntity);
            prefetch.Add(ProductCategoryEntityBase.PrefetchPathProductEntity);
            prefetch.Add(ProductCategoryEntityBase.PrefetchPathCategoryEntity).SubPath.Add(CategoryEntityBase.PrefetchPathMenuEntity);

            RelationCollection relations = new RelationCollection();
            relations.Add(ProductCategoryEntityBase.Relations.ProductEntityUsingProductId);
            relations.Add(ProductCategoryEntityBase.Relations.CategoryEntityUsingCategoryId);

            SortExpression sortProductCategories = new SortExpression();
            sortProductCategories.Add(ProductFields.Name | SortOperator.Ascending);
            sortProductCategories.Add(CategoryFields.Name | SortOperator.Ascending);

            ProductCategoryCollection productCategories = new ProductCategoryCollection();
            productCategories.GetMulti(filterProductCategories, 0, sortProductCategories, relations, prefetch, fieldsList, 0, 0);

            return productCategories;
        }

        public static CategoryCollection GetCategoriesWithProducts(int companyId)
        {
            PredicateExpression filter = new PredicateExpression();
            filter.Add(CategoryFields.CompanyId == companyId);
            filter.Add(CategoryFields.VisibilityType != VisibilityType.Never);

            PredicateExpression productFilter = new PredicateExpression();
            productFilter.Add(ProductFields.VisibilityType != VisibilityType.Never);

            RelationCollection relations = new RelationCollection();
            relations.Add(CategoryEntityBase.Relations.ProductCategoryEntityUsingCategoryId);
            relations.Add(CategoryEntityBase.Relations.MenuEntityUsingMenuId);

            IncludeFieldsList menuIncludes = new IncludeFieldsList(MenuFields.Name);
            IncludeFieldsList categoryIncludes = new IncludeFieldsList(CategoryFields.Name);
            IncludeFieldsList productIncludes = new IncludeFieldsList(ProductFields.Name);

            PrefetchPath prefetch = new PrefetchPath(EntityType.CategoryEntity);
            IPrefetchPathElement prefetchProductCategory = prefetch.Add(CategoryEntityBase.PrefetchPathProductCategoryCollection);
            IPrefetchPathElement prefetchProductCategoryProduct = prefetchProductCategory.SubPath.Add(ProductCategoryEntityBase.PrefetchPathProductEntity);
            prefetchProductCategoryProduct.ExcludedIncludedFields = productIncludes;
            prefetchProductCategoryProduct.Filter = productFilter;
            IPrefetchPathElement prefetchMenu = prefetch.Add(CategoryEntityBase.PrefetchPathMenuEntity);
            prefetchMenu.ExcludedIncludedFields = menuIncludes;

            SortExpression sort = new SortExpression(new SortClause(MenuFields.Name, SortOperator.Ascending));
            sort.Add(new SortClause(CategoryFields.Name, SortOperator.Ascending));

            CategoryCollection categories = new CategoryCollection();
            categories.GetMulti(filter, 0, sort, relations, prefetch, categoryIncludes, 0, 0);

            return categories;
        }
    }
}
