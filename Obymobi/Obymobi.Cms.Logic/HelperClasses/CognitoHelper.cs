﻿using Dionysos.Web;
using Dionysos.Web.Security;
using Newtonsoft.Json.Linq;
using Obymobi.Data.EntityClasses;
using Obymobi.Enums;
using System;
using System.Text;

namespace Obymobi.Cms.Logic.HelperClasses
{
    public static class CognitoHelper
    {
        public static string IdToken
        {
            get
            {
                SessionHelper.TryGetValue("IdToken", out string idToken);
                return idToken;
            }
            set
            {
                SessionHelper.SetValue("IdToken", value);
            }
        }

        public static string JwtPayLoad => IdToken == null ? null : Encoding.UTF8.GetString(FromBase64Url(IdToken.Split('.')[1]));

        public static DateTime Unix() => UnixTimeStampToDateTime(Convert.ToDouble(JObject.Parse(JwtPayLoad)["exp"]));

        public static bool IsLoggedIn => IdToken != null && Unix() > DateTime.Now;

        public static bool AllowedByCmsToLogin => (UserManager.CurrentUser as UserEntity)?.HasCboAccount ?? false;

        public static bool ShowLoginButton(Role role) => AllowedByCmsToLogin && !IsLoggedIn && role >= Role.Crave;

        public static DateTime UnixTimeStampToDateTime(double unixTimeStamp)
        {
            System.DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
            dtDateTime = dtDateTime.AddSeconds(unixTimeStamp).ToLocalTime();
            return dtDateTime;
        }

        static byte[] FromBase64Url(string base64Url)
        {
            string padded = base64Url.Length % 4 == 0
                ? base64Url : base64Url + "====".Substring(base64Url.Length % 4);
            string base64 = padded.Replace("_", "/")
                                  .Replace("-", "+");
            return Convert.FromBase64String(base64);
        }
    }
}
