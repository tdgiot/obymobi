﻿using System;
using System.Collections;
using Dionysos.Data.LLBLGen;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;

namespace Obymobi.Cms.Logic.HelperClasses
{
    public class ProductAlterationHelper
    {
        public static ProductAlterationEntity CopyProductAlteration(ProductAlterationEntity oldProductAlterationEntity, Transaction transaction = null)
        {
            ProductAlterationEntity newProductAlterationEntity = null;
            if (oldProductAlterationEntity != null)
            {
                ArrayList productAlterationFieldsNotToBeCopied = new ArrayList();
                productAlterationFieldsNotToBeCopied.Add(ProductAlterationFields.AlterationId);
                productAlterationFieldsNotToBeCopied.Add(ProductAlterationFields.ProductId);
                productAlterationFieldsNotToBeCopied.Add(ProductAlterationFields.GenericproductGenericalterationId);

                newProductAlterationEntity = new ProductAlterationEntity();
                if (transaction != null)
                    newProductAlterationEntity.AddToTransaction(transaction);
                LLBLGenEntityUtil.CopyFields(oldProductAlterationEntity, newProductAlterationEntity, productAlterationFieldsNotToBeCopied);
            }
            return newProductAlterationEntity;
        }
    }
}