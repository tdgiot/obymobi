﻿using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Cms.Logic.HelperClasses
{
    public static class RouteHelper
    {
        public static RouteCollection GetRouteCollection(int companyId, bool documentRoutesOnly = false)
        {
            RouteCollection routes = new RouteCollection();
            PredicateExpression routeFilter = new PredicateExpression(RouteFields.CompanyId == companyId);
            SortExpression routeSort = new SortExpression(RouteFields.Name | SortOperator.Ascending);

            if (documentRoutesOnly)
            {
                routeFilter.Add(RoutestephandlerFields.HandlerType == RoutestephandlerType.EmailDocument);
            }
            else
            {
                routeFilter.Add(RoutestephandlerFields.HandlerType != RoutestephandlerType.EmailDocument);
            }

            RelationCollection relations = new RelationCollection();
            relations.Add(RouteEntityBase.Relations.RoutestepEntityUsingRouteId);
            relations.Add(RoutestepEntityBase.Relations.RoutestephandlerEntityUsingRoutestepId);

            routes.GetMulti(routeFilter, 0, routeSort, relations);

            return routes;
        }
    }
}
