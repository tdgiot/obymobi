﻿using Dionysos.Data.LLBLGen;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Obymobi.Cms.Logic.HelperClasses
{
    public class MapHelper
    {
        public static MapCollection GetMaps(int companyId)
        {
            PredicateExpression filter = new PredicateExpression();
            filter.Add(MapFields.CompanyId == DBNull.Value);
            filter.AddWithOr(MapFields.CompanyId == companyId);

            SortExpression sort = new SortExpression(new SortClause(MapFields.Name, SortOperator.Ascending));

            MapCollection maps = new MapCollection();
            maps.GetMulti(filter, 0, sort);

            return maps;
        }

        public static MapEntity Copy(MapEntity from, string newMapName)
        {
            MapEntity to = new MapEntity();
            LLBLGenEntityUtil.CopyFields(from, to, null);
            to.Name = newMapName;

            foreach (MapPointOfInterestEntity oldMapPointOfInterestEntity in from.MapPointOfInterestCollection)
            {
                MapPointOfInterestEntity newMapPointOfInterestEntity = new MapPointOfInterestEntity();
                LLBLGenEntityUtil.CopyFields(oldMapPointOfInterestEntity, newMapPointOfInterestEntity, null);

                to.MapPointOfInterestCollection.Add(newMapPointOfInterestEntity);
            }

            return to;
        }
    }
}
