﻿using Dionysos.Data.LLBLGen;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Cms.Logic.HelperClasses
{
    public static class ProductgroupHelper
    {
        public static ProductgroupCollection GetProductgroups(int companyId)
        {
            PredicateExpression filter = new PredicateExpression(ProductgroupFields.CompanyId == companyId);            
            SortExpression sort = new SortExpression(new SortClause(ProductgroupFields.Name, SortOperator.Ascending));
            return EntityCollection.GetMulti<ProductgroupCollection>(filter, sort, null, null, new IncludeFieldsList(ProductgroupFields.Name));
        }
    }
}
