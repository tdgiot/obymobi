﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dionysos.Data.LLBLGen;
using Obymobi.Data;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Cms.Logic.HelperClasses
{
    public class PriceLevelHelper
    {
        public static bool TryAndCopy(int priceLevelId, string newName, out PriceLevelEntity newPriceLevel)
        {
            bool success;
            
            try
            {
                PriceLevelEntity priceLevel = PriceLevelHelper.GetPriceLevel(priceLevelId);

                newPriceLevel = new PriceLevelEntity();
                LLBLGenEntityUtil.CopyFields(priceLevel, newPriceLevel, null);
                newPriceLevel.Name = newName;

                ArrayList priceLevelItemFieldsToExclude = new ArrayList() { PriceLevelItemFields.PriceLevelId };
                foreach (PriceLevelItemEntity priceLevelItem in priceLevel.PriceLevelItemCollection)
                {
                    PriceLevelItemEntity newPriceLevelItem = new PriceLevelItemEntity();
                    LLBLGenEntityUtil.CopyFields(priceLevelItem, newPriceLevelItem, priceLevelItemFieldsToExclude);
                    newPriceLevel.PriceLevelItemCollection.Add(newPriceLevelItem);
                }

                success = newPriceLevel.Save(true);
            }
            catch (Exception ex)
            {
                success = false;
                newPriceLevel = null;
            }
            return success;
        }

        public static PriceLevelEntity GetPriceLevel(int priceLevelId)
        {
            PredicateExpression filter = new PredicateExpression(PriceLevelFields.PriceLevelId == priceLevelId);

            PrefetchPath prefetch = new PrefetchPath(EntityType.PriceLevelEntity);
            prefetch.Add(PriceLevelEntityBase.PrefetchPathPriceLevelItemCollection);

            PriceLevelCollection priceLevels = new PriceLevelCollection();
            priceLevels.GetMulti(filter, prefetch);

            if (priceLevels.Count > 0)
            {
                return priceLevels[0];
            }
            else
            {
                return null;
            }
        }
    }
}
