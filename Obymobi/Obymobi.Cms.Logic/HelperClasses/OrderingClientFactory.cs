﻿using System;
using Crave.Adyen.Api.Client;
using System.Net.Http;
using Crave.Ordering.Api.Client;

namespace Obymobi.Cms.Logic.HelperClasses
{
    public static class OrderingClientFactory
    {
        private static readonly HttpClient httpClient = new HttpClient();

        static OrderingClientFactory()
        {
            httpClient.BaseAddress = BaseUrl;
        }
        
        public static PaymentStatusClient GetPaymentStatusClient()
        {
            return new PaymentStatusClient(httpClient);
        }

        public static OrdersClient GetOrderClient()
        {
            return new OrdersClient(httpClient);
        }

        private static Uri BaseUrl => new Uri(WebEnvironmentHelper.GetOrderingApiBaseUrl());
    }
}
