﻿using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Resources;
using Dionysos;
using Obymobi.Cms.Logic.Models;
using Obymobi.Data;
using Obymobi.Data.EntityClasses;
using Obymobi.Enums;
using Obymobi.Logic.HelperClasses;

namespace Obymobi.Cms.Logic.HelperClasses
{
    public static class TranslationHelper
    {
        private static readonly CultureInfo FallbackCulture = new CultureInfo("en");
        private static readonly CultureInfo[] AvailableDefaultTranslationCultures = new[] { new CultureInfo("nl"), new CultureInfo("en") };

        public static void SetDefaultTranslations(ResourceManager resourceManager, ICustomTextContainingEntity entity, CultureInfo defaultCompanyCulture, IEnumerable<CultureInfo> companyTranslationCultures)
        {
            List<CultureInfo> allCulturesToEvaluate = new List<CultureInfo>(companyTranslationCultures.Distinct());
            if (!allCulturesToEvaluate.Contains(defaultCompanyCulture))
            {
                allCulturesToEvaluate.Add(defaultCompanyCulture);
            }

            foreach (var culture in allCulturesToEvaluate)
            {
                AddDefaultTranslationsToEntity(resourceManager, entity, culture, culture == defaultCompanyCulture);
            }

            entity.CustomTextCollection.SaveMulti();
        }

        internal static void AddDefaultTranslationsToEntity(ResourceManager resourceManager, ICustomTextContainingEntity entity, CultureInfo culture, bool useFallbackWhenMissingDefaults = false)
        {
            IEnumerable<CustomTextType> missingTranslationTypes = GetMissingTranslationTypes(entity, culture);
            if (!missingTranslationTypes.Any())
            {
                return;
            }

            TranslationDefaults translationDefaults = GetTranslationDefaults(resourceManager, culture, useFallbackWhenMissingDefaults);
            if (translationDefaults == null)
            {
                return;
            }

            foreach (CustomTextType missingTranslationType in missingTranslationTypes)
            {
                string translation = translationDefaults.ResourceSet.GetString(missingTranslationType.ToString());
                if (string.IsNullOrWhiteSpace(translation))
                {
                    continue;
                }

                entity.CustomTextCollection.Add(new CustomTextEntity()
                {
                    Type = missingTranslationType,
                    CultureCode = culture.ToString(),
                    Text = translation
                });
            }
        }

        internal static TranslationDefaults GetTranslationDefaults(ResourceManager resourceManager, CultureInfo culture, bool useFallbackTranslations)
        {
            ResourceSet cultureSpecificTranslations = null;

            if (HasMatchingCultureDefaultTranslations(culture))
            {
                cultureSpecificTranslations = resourceManager.GetResourceSet(culture, true, true);
            }
            else
            {
                if (useFallbackTranslations)
                {
                    cultureSpecificTranslations = resourceManager.GetResourceSet(FallbackCulture, true, true);
                }
            }

            return cultureSpecificTranslations != null ? new TranslationDefaults(culture, cultureSpecificTranslations) : null;
        }

        internal static IEnumerable<CustomTextType> GetMissingTranslationTypes(ICustomTextContainingEntity entity, CultureInfo cultureInfo)
        {
            List<CustomTextType> customTextTypesForEntity = CustomTextHelper.GetCustomTextTypesForEntity(entity);
            IEnumerable<CustomTextType> existingCustomTextTypes = entity.CustomTextCollection.Where(c => c.CultureCode == cultureInfo.ToString()).Select(c => c.Type);

            return customTextTypesForEntity.Except(existingCustomTextTypes);
        }

        private static bool HasMatchingCultureDefaultTranslations(CultureInfo culture)
        {
            return AvailableDefaultTranslationCultures.Contains(culture) || AvailableDefaultTranslationCultures.Any(c => c.TwoLetterISOLanguageName == culture.Parent.TwoLetterISOLanguageName);
        }
    }
}
