﻿using System.Collections.Generic;
using Newtonsoft.Json;
using Obymobi.Interfaces;

namespace Obymobi.Cms.Logic.HelperClasses
{
    public class PlaceholderHelpers
    {
        public static string ReplaceVariable(string src, string key, IEnumerable<ITag> tagCollection)
        {
            List<object> convertedCollection = new List<object>();
            foreach (ITag tag in tagCollection)
            {
                convertedCollection.Add(new { Id = tag.TagId, Name = tag.Name, CompanyId = tag.CompanyId.GetValueOrDefault(0) });
            }

            src = src.Replace($"'[[{key}]]'", JsonConvert.SerializeObject(convertedCollection));

            return src;
        }
    }
}
