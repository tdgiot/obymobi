﻿using System;
using System.Collections.Generic;
using System.Linq;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Cms.Logic.Factories
{
    public class PredicateFactory
    {
        public static PredicateExpression CreatePaymentProviderCompanyIdFilter(IEnumerable<int> companyIds, Role role)
        {
            PredicateExpression filter = new PredicateExpression();

            if (role == Role.GodMode)
            {
                return filter;
            }

            filter.Add(PaymentProviderFields.CompanyId == DBNull.Value);
            filter.AddWithOr(PaymentProviderFields.CompanyId == companyIds.ToList());

            return filter;
        }

        public static PredicateExpression CreateCompanyIdFilter(IEnumerable<int> companyIds, Role role)
        {
            PredicateExpression filter = new PredicateExpression();

            if (role == Role.GodMode)
            {
                return filter;
            }

            filter.Add(CompanyFields.CompanyId == companyIds.ToList());

            return filter;
        }

        public static PredicateExpression CreateUIScheduleFilter(int companyId, UIScheduleType uiScheduleType)
        {
            PredicateExpression filter = new PredicateExpression();
            filter.Add(UIScheduleFields.CompanyId == companyId);
            filter.Add(UIScheduleFields.Type == uiScheduleType);

            return filter;
        }
    }
}
