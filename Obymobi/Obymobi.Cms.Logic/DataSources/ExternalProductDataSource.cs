﻿using Obymobi.Data;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Cms.Logic.DataSources
{
    public class ExternalProductDataSource
    {
        public ExternalProductCollection GetDropdownDataSource(int companyId, ExternalProductType externalProductType)
        {
            RelationCollection relations = new RelationCollection();
            relations.Add(ExternalProductEntity.Relations.ExternalSystemEntityUsingExternalSystemId);
            relations.Add(ExternalProductEntity.Relations.ExternalMenuEntityUsingExternalMenuId, JoinHint.Left);

            PredicateExpression filter = new PredicateExpression();
            filter.Add(ExternalProductFields.ParentCompanyId == companyId);
            filter.Add(ExternalProductFields.Type == externalProductType);

            SortExpression sort = new SortExpression(new SortClause(ExternalProductFields.ExternalProductId, SortOperator.Ascending));
            IncludeFieldsList includes = new IncludeFieldsList(ExternalProductFields.ExternalProductId, ExternalProductFields.Name, ExternalProductFields.Id, ExternalProductFields.ParentCompanyId);

            PrefetchPath prefetch = new PrefetchPath(EntityType.ExternalProductEntity);
            prefetch.Add(ExternalProductEntity.PrefetchPathExternalSystemEntity, new IncludeFieldsList { ExternalSystemFields.Name });
            prefetch.Add(ExternalProductEntity.PrefetchPathExternalMenuEntity, new IncludeFieldsList { ExternalMenuFields.Name });

            ExternalProductCollection entityCollection = new ExternalProductCollection();
            entityCollection.GetMulti(filter, 0, sort, relations, prefetch, includes, 0, 0);

            return entityCollection;
        }
    }
}
