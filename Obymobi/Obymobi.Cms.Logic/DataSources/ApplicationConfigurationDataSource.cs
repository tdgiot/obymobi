﻿using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System.Linq;
using Dionysos;

namespace Obymobi.Cms.Logic.DataSources
{
    public class ApplicationConfigurationDataSource
    {
        public ApplicationConfigurationEntity GetFirstApplicationConfigurationEntity(int companyId)
        {
            PredicateExpression filter = new PredicateExpression(ApplicationConfigurationFields.CompanyId == companyId);

            ApplicationConfigurationCollection applicationConfigurationCollection = new ApplicationConfigurationCollection();
            applicationConfigurationCollection.GetMulti(filter);

            return applicationConfigurationCollection.FirstOrDefault();
        }

        public ApplicationConfigurationEntity GetApplicationConfigurationEntity(int applicationConfigurationId)
        {
            PredicateExpression filter = new PredicateExpression(ApplicationConfigurationFields.ApplicationConfigurationId == applicationConfigurationId);

            ApplicationConfigurationCollection applicationConfigurationCollection = new ApplicationConfigurationCollection();
            applicationConfigurationCollection.GetMulti(filter);

            return applicationConfigurationCollection.FirstOrDefault();
        }

        public void SetDefaultValuesOnApplicationConfiguration(ApplicationConfigurationEntity applicationConfigurationEntity, int companyId)
        {
            applicationConfigurationEntity.CompanyId = companyId;
            applicationConfigurationEntity.ExternalId = RandomUtil.GetRandomString(6).ToUpperInvariant();
            applicationConfigurationEntity.AnalyticsEnvironment = Enums.AnalyticsEnvironment.NonProduction;
        }
    }
}
