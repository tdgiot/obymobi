﻿using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System.Linq;

namespace Obymobi.Cms.Logic.DataSources
{
    public class WidgetDataSource
    {
        #region Fields

        private ApplicationConfigurationDataSource applicationConfigurationDataSource = new ApplicationConfigurationDataSource();

        #endregion

        #region Methods        

        public void SetDefaultValuesOnWidget(WidgetEntity widgetEntity, int applicationConfigurationId)
        {
            ApplicationConfigurationEntity applicationConfigurationEntity = this.applicationConfigurationDataSource.GetApplicationConfigurationEntity(applicationConfigurationId);
            if (applicationConfigurationEntity != null)
            {
                widgetEntity.ApplicationConfigurationId = applicationConfigurationEntity.ApplicationConfigurationId;
                widgetEntity.ParentCompanyId = applicationConfigurationEntity.CompanyId;
            }
        }

        public WidgetEntity GetWidget(int widgetId)
        {
            WidgetCollection widgetCollection = new WidgetCollection();
            widgetCollection.GetMulti(new PredicateExpression(WidgetFields.WidgetId == widgetId));

            return widgetCollection.FirstOrDefault();
        }

        public WidgetGroupEntity GetWidgetGroup(int widgetId)
        {
            return new WidgetGroupEntity(widgetId);
        }

        public WidgetEntity CreateWidget(ApplessWidgetType type, string name, int companyId, int applicationConfigurationId)
        {
            WidgetEntity entity;

            switch (type)
            {
                case ApplessWidgetType.PageTitle:
                    entity = this.CreatePageTitleWidgetEntity(name);
                    break;
                case ApplessWidgetType.Carousel:
                    entity = this.CreateCarouselWidgetEntity();
                    break;
                case ApplessWidgetType.ActionButtons:
                    entity = this.CreateGroupWidgetEntity(name);
                    break;
                case ApplessWidgetType.ActionButton:
                    entity = this.CreateActionButtonWidgetEntity(name, companyId, applicationConfigurationId);
                    break;
                case ApplessWidgetType.ActionBanner:
                    entity = this.CreateActionBannerWidgetEntity(name);
                    break;
                case ApplessWidgetType.LanguageSwitcher:
                    entity = this.CreateLanguageSwitcherWidgetEntity(name);
                    break;
                case ApplessWidgetType.Hero:
                    entity = this.CreateHeroWidgetEntity();
                    break;
                case ApplessWidgetType.WaitTime:
                    entity = this.CreateWaitTimeWidgetEntity(name);
                    break;
                case ApplessWidgetType.OpeningTime:
                    entity = this.CreateOpeningTimeWidgetEntity(name);
                    break;
                case ApplessWidgetType.Markdown:
                    entity = this.CreateMarkdownWidgetEntity(name);
                    break;
                default:
                    throw new System.Exception($"Unknown widget type: {type}");
            }

            entity.ParentCompanyId = companyId;
            entity.ApplicationConfigurationId = applicationConfigurationId;

            return entity;
        }

        private WidgetPageTitleEntity CreatePageTitleWidgetEntity(string name)
        {
            return new WidgetPageTitleEntity
            {
                Title = name
            };
        }

        private WidgetCarouselEntity CreateCarouselWidgetEntity()
        {
            return new WidgetCarouselEntity();
        }

        private WidgetGroupEntity CreateGroupWidgetEntity(string name)
        {
            return new WidgetGroupEntity
            {
                Name = name
            };
        }

        private WidgetActionButtonEntity CreateActionButtonWidgetEntity(string name, int companyId, int applicationConfigurationId)
        {
            return new WidgetActionButtonEntity
            {
                ParentCompanyId = companyId,
                ApplicationConfigurationId = applicationConfigurationId,
                IconType = ActionButtonIconType.None,
                Name = name,
                Text = name
            };
        }

        private WidgetActionBannerEntity CreateActionBannerWidgetEntity(string name)
        {
            return new WidgetActionBannerEntity
            {
                Text = name
            };
        }

        private WidgetLanguageSwitcherEntity CreateLanguageSwitcherWidgetEntity(string name)
        {
            return new WidgetLanguageSwitcherEntity
            {
                Text = name
            };
        }

        private WidgetHeroEntity CreateHeroWidgetEntity()
        {
            return new WidgetHeroEntity();
        }

        private WidgetWaitTimeEntity CreateWaitTimeWidgetEntity(string name)
        {
            return new WidgetWaitTimeEntity
            {
                Name = name
            };
        }

        private WidgetOpeningTimeEntity CreateOpeningTimeWidgetEntity(string name)
        {
            return new WidgetOpeningTimeEntity
            {
                Name = name
            };
        }

        private WidgetMarkdownEntity CreateMarkdownWidgetEntity(string name)
        {
            return new WidgetMarkdownEntity
            {
                Title = name,
                Text = name
            };
        }

        #endregion
    }
}
