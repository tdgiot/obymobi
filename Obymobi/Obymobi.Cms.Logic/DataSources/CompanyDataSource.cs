﻿using Obymobi.Data.CollectionClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Cms.Logic.DataSources
{
    public class CompanyDataSource
    {
        public CompanyCollection FetchAll(IncludeFieldsList fields = null)
        {
            CompanyCollection companyCollection = new CompanyCollection();
            companyCollection.GetMulti(null, fields, 0);

            return companyCollection;
        }

        public int Save(CompanyCollection companyCollection)
        {
            return companyCollection.SaveMulti();
        }
    }
}
