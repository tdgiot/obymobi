﻿using Dionysos;
using Obymobi.Data.EntityClasses;

namespace Obymobi.Cms.Logic.DataSources
{
    public class LandingPageDataSource
    {
        #region Fields

        private ApplicationConfigurationDataSource applicationConfigurationDataSource = new ApplicationConfigurationDataSource();
        private NavigationMenuDataSource navigationMenuDataSource = new NavigationMenuDataSource();
        private ThemeDataSource themeDataSource = new ThemeDataSource();

        #endregion

        #region Methods

        public LandingPageEntity GetLandingPage(int landingPageId)
        {
            return new LandingPageEntity(landingPageId);
        }

        public void SetDefaultValuesOnPage(LandingPageEntity landingPageEntity, int applicationConfigurationId)
        {
            landingPageEntity.ExternalId = RandomUtil.GetRandomString(6).ToUpperInvariant();

            ApplicationConfigurationEntity applicationConfigurationEntity = this.applicationConfigurationDataSource.GetApplicationConfigurationEntity(applicationConfigurationId);
            if (applicationConfigurationEntity != null)
            {
                landingPageEntity.ApplicationConfigurationId = applicationConfigurationEntity.ApplicationConfigurationId;
                landingPageEntity.ParentCompanyId = applicationConfigurationEntity.CompanyId;
            }            

            NavigationMenuEntity navigationMenuEntity = this.navigationMenuDataSource.GetFirstNavigationMenuEntity(applicationConfigurationId);
            if (navigationMenuEntity != null)
            {
                landingPageEntity.NavigationMenuId = navigationMenuEntity.NavigationMenuId;
            }            

            ThemeEntity themeEntity = this.themeDataSource.GetFirstThemeEntity(applicationConfigurationId);
            if (themeEntity != null)
            {
                landingPageEntity.ThemeId = themeEntity.ThemeId;
            }            
        }

        #endregion
    }
}
