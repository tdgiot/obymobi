﻿using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System.Linq;

namespace Obymobi.Cms.Logic.DataSources
{
    public class NavigationMenuDataSource
    {
        #region Fields

        private ApplicationConfigurationDataSource applicationConfigurationDataSource = new ApplicationConfigurationDataSource();

        #endregion

        #region Methods

        public NavigationMenuEntity GetFirstNavigationMenuEntity(int applicationConfigurationId)
        {
            PredicateExpression filter = new PredicateExpression(NavigationMenuFields.ApplicationConfigurationId == applicationConfigurationId);

            NavigationMenuCollection navigationMenuCollection = new NavigationMenuCollection();
            navigationMenuCollection.GetMulti(filter);

            return navigationMenuCollection.FirstOrDefault();
        }

        public void SetDefaultValuesOnNavigationMenu(NavigationMenuEntity navigationMenuEntity, int applicationConfigurationId)
        {
            ApplicationConfigurationEntity applicationConfigurationEntity = this.applicationConfigurationDataSource.GetApplicationConfigurationEntity(applicationConfigurationId);
            if (applicationConfigurationEntity != null)
            {
                navigationMenuEntity.ApplicationConfigurationId = applicationConfigurationEntity.ApplicationConfigurationId;
                navigationMenuEntity.CompanyId = applicationConfigurationEntity.CompanyId;
            }
        }

        #endregion
    }
}
