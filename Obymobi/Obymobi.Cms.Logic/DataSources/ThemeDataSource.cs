﻿using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System.Linq;

namespace Obymobi.Cms.Logic.DataSources
{
    public class ThemeDataSource
    {
        #region Fields

        private ApplicationConfigurationDataSource applicationConfigurationDataSource = new ApplicationConfigurationDataSource();

        #endregion

        #region Methods

        public ThemeEntity GetFirstThemeEntity(int applicationConfigurationId)
        {
            PredicateExpression filter = new PredicateExpression(ThemeFields.ApplicationConfigurationId == applicationConfigurationId);

            ThemeCollection themeCollection = new ThemeCollection();
            themeCollection.GetMulti(filter);

            return themeCollection.FirstOrDefault();
        }

        public void SetDefaultValuesOnTheme(ThemeEntity themeEntity, int applicationConfigurationId)
        {
            ApplicationConfigurationEntity applicationConfigurationEntity = this.applicationConfigurationDataSource.GetApplicationConfigurationEntity(applicationConfigurationId);
            if (applicationConfigurationEntity != null)
            {
                themeEntity.ApplicationConfigurationId = applicationConfigurationEntity.ApplicationConfigurationId;
                themeEntity.ParentCompanyId = applicationConfigurationEntity.CompanyId;
            }
        }

        #endregion
    }
}
