﻿using System.Linq;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Cms.Logic.DataSources
{
    public class FeatureToggleAvailabilityDataSource
    {
        public FeatureToggleAvailabilityEntity Get(FeatureToggle featureToggle)
        {
            IPredicate predicateExpression = new PredicateExpression {FeatureToggleAvailabilityFields.FeatureToggle == featureToggle};

            FeatureToggleAvailabilityCollection featureToggleAvailabilityCollection = new FeatureToggleAvailabilityCollection();
            featureToggleAvailabilityCollection.GetMulti(predicateExpression, null, 1);

            return featureToggleAvailabilityCollection.FirstOrDefault();
        }

        public FeatureToggleAvailabilityEntity GetOrCreateDefault(FeatureToggle featureToggle)
        {
            FeatureToggleAvailabilityEntity featureToggleAvailability = Get(featureToggle);
            return featureToggleAvailability ?? CreateDefaultFeatureToggleAvailability(featureToggle);
        }

        public bool Save(FeatureToggleAvailabilityEntity featureToggleAvailability)
        {
            return featureToggleAvailability.Save();
        }

        private static FeatureToggleAvailabilityEntity CreateDefaultFeatureToggleAvailability(FeatureToggle featureToggle) => new FeatureToggleAvailabilityEntity
        {
            FeatureToggle = featureToggle,
            ReleaseGroup = ReleaseGroup.Dev,
            Role = Role.GodMode
        };
    }
}
