﻿using Dionysos;
using Obymobi.Data;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Logic.Cms;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace Obymobi.Cms.Logic.Sites
{
    /// <summary>
    /// Summary description for PageManager
    /// </summary>
    public class PageManager
    {
        private readonly EntityType entityType;
        private readonly int siteId;

        public PageManager(EntityType entityType, int siteId, List<IPage> datasource)
        {
            this.entityType = entityType;
            this.siteId = siteId;
            this.dataSource = datasource;
        }

        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public IEnumerable GetPages()
        {
            return this.DataSource.OrderBy(x => x.SortOrder);
        }

        [DataObjectMethod(DataObjectMethodType.Insert, true)]
        public void InsertPage(IPage page)
        {
            IEnumerable<IPage> existingChilderen = this.DataSource.Where(x => x.ParentPageId == page.ParentPageId);
            if (!existingChilderen.Any())
                page.SortOrder = 0;
            else
                page.SortOrder = existingChilderen.Max(x => x.SortOrder) + 1;
            
            page.Save();

            this.DataSource.Add(page);
        }

        [DataObjectMethod(DataObjectMethodType.Update, true)]
        public void UpdatePage(IPage page)
        {
            IPage pageInDataSource = this.DataSource.FirstOrDefault(x => x.PrimaryId == page.PrimaryId);
            if (pageInDataSource != null)
            {
                if (!string.IsNullOrEmpty(page.Name))
                {
                    pageInDataSource.Name = page.Name;
                }

                if (page.ParentPageId == page.PrimaryId ||
                    pageInDataSource.ParentPageId == pageInDataSource.PrimaryId)
                {
                    // DK (2019-06-15) - Apparently this can happen
                    page.ParentPageId = null;
                    pageInDataSource.ParentPageId = null;
                }

                if (page.PageType > 0)
                {
                    pageInDataSource.PageType = page.PageType;
                }
                if (page.ParentPageId > 0)
                {
                    if (page.ParentPageId != pageInDataSource.ParentPageId)
                    {
                        // Changed parent, make is last child of this parent
                        IEnumerable<IPage> existingChilderen = this.DataSource.Where(x => x.ParentPageId == page.ParentPageId);
                        if (existingChilderen.Any())
                        {
                            pageInDataSource.SortOrder = existingChilderen.Max(x => x.SortOrder) + 1;
                        }
                        else
                        {
                            pageInDataSource.SortOrder = 1;
                        }                        
                    }

                    pageInDataSource.ParentPageId = page.ParentPageId;
                }
                if (page.SortOrder > 0)
                {
                    // If it's dragged up on another one, ensure sort orders are not double
                    IEnumerable<IPage> existingChilderen = this.DataSource.Where(x => x.ParentPageId == page.ParentPageId);
                    if (existingChilderen.Any(x => x.SortOrder == page.SortOrder))
                    {
                        IEnumerable<IPage> childerenToUpdate = existingChilderen.Where(x => x.SortOrder >= page.SortOrder && x.PrimaryId != page.PrimaryId);
                        foreach (IPage childToUpdate in childerenToUpdate)
                        {
                            childToUpdate.SortOrder++;
                        }                        
                    }
                    pageInDataSource.SortOrder = page.SortOrder;
                }

                if (pageInDataSource is PageEntity)
                {
                    ((PageEntity)pageInDataSource).ValidatorCreateOrUpdateDefaultPageLanguage = true;
                }
                else if (pageInDataSource is PageTemplateEntity)
                {
                    ((PageTemplateEntity)pageInDataSource).ValidatorCreateOrUpdatePageTemplateLanguage = true;
                }

                pageInDataSource.Save();
            }
            else
                throw new KeyNotFoundException("Page isn't available with id: " + page.PrimaryId);
        }

        [DataObjectMethod(DataObjectMethodType.Delete, true)]
        public void DeletePage(IPage page)
        {
            IPage pageInDataSource = this.DataSource.FirstOrDefault(x => x.PrimaryId == page.PrimaryId);

            if (pageInDataSource != null)
            {
                this.DataSource.Remove(pageInDataSource);
                pageInDataSource.Delete();

                if (pageInDataSource is PageEntity)
                {
                    if (this.siteId > 0)
                    {
                        // Reload the cache with the new site
                        SiteHelper.GetSite(this.siteId, true);
                    }
                }
            }
            else
                throw new KeyNotFoundException("Page isn't available with id: " + page.PrimaryId);
        }

        #region Type Specific Overloads

        // Not proud of this, but in a next iteration might prevent this with a custom implementation based on DataSourceControl
        // http://msdn.microsoft.com/en-us/library/system.web.ui.datasourcecontrol%28v=vs.110%29.aspx

        public void InsertPage(PageEntity page)
        {
            page.SiteId = this.siteId;
            page.ValidatorCreateDefaultLanguageAgnosticElements = true;
            page.ValidatorCreateOrUpdateDefaultPageLanguage = true;

            this.InsertPage((IPage)page);
        }

        public void InsertPage(PageTemplateEntity page)
        {
            page.SiteTemplateId = this.siteId;
            page.ValidatorCreateOrUpdatePageTemplateLanguage = true;

            this.InsertPage((IPage)page);
        }

        public void UpdatePage(PageEntity page)
        {
            this.UpdatePage((IPage)page);
        }

        public void UpdatePage(PageTemplateEntity page)
        {
            this.UpdatePage((IPage)page);
        }

        public void DeletePage(PageEntity page)
        {
            this.DeletePage((IPage)page);
        }

        public void DeletePage(PageTemplateEntity page)
        {
            this.DeletePage((IPage)page);
        }

        #endregion

        private List<IPage> dataSource = null;
        private List<IPage> DataSource
        {
            get
            {
                if (this.dataSource == null)
                {
                    PredicateExpression filter = new PredicateExpression();
                    this.dataSource = new List<IPage>();

                    if (this.entityType == EntityType.PageEntity)
                    {
                        PageCollection pages = new PageCollection();
                        filter.Add(PageFields.SiteId == this.siteId);

                        pages.GetMulti(filter);

                        this.dataSource.AddRange(pages);
                    }
                    else if (this.entityType == EntityType.PageTemplateEntity)
                    {
                        PageTemplateCollection pages = new PageTemplateCollection();
                        filter.Add(PageTemplateFields.SiteTemplateId == this.siteId);

                        pages.GetMulti(filter);

                        this.dataSource.AddRange(pages);
                    }
                    else
                    {
                        throw new NotImplementedException("PageManager is not implemented for EntityType: {0}".FormatSafe(this.entityType));
                    }
                }

                return this.dataSource;
            }
        }

        public void DraggedToSort(int draggedPageId, int draggedUponPageId)
        {
            // Find both pages
            IPage pageDragged = this.DataSource.FirstOrDefault(x => x.PrimaryId == draggedPageId);
            IPage pageDraggedUpon = this.DataSource.FirstOrDefault(x => x.PrimaryId == draggedUponPageId);

            bool draggedDown = false;
            // Sorting within a group of childeren is a bit more advanced, so 
            // that both up and down dragging can be used. 
            // (Instead of always putting the dragged item above the item it was dragged upon)
            if (pageDragged.ParentPageId == pageDraggedUpon.ParentPageId)
            {
                if (pageDragged.SortOrder < pageDraggedUpon.SortOrder)
                {
                    // It was above it, so it's dragged down
                    draggedDown = true;
                }

                // Put the items in the correct order - then renumber the whole lot
                // Get all pages for this parent page
                List<IPage> pages = this.DataSource.Where(x => x.ParentPageId == pageDragged.ParentPageId).OrderBy(x => x.SortOrder).ToList();

                // Remove the dragged page from the list (we're going to insert that again on it's 
                // correct new place, and then re-number all sort orders.
                pages.Remove(pageDragged);

                for (int i = 0; i < pages.Count; i++)
                {
                    if (pages[i] == pageDraggedUpon)
                    {
                        if (draggedDown)
                        {
                            // Place it below this item
                            pages.Insert(i + 1, pageDragged);
                        }
                        else
                        {
                            // Place it in front of this item.
                            pages.Insert(i, pageDragged);
                        }
                        break;
                    }
                }

                int orderNo = 1;
                foreach (IPage page in pages)
                {
                    page.SortOrder = orderNo;
                    page.Save();
                    orderNo++;
                }
            }
            else
            {
                pageDragged.ParentPageId = pageDraggedUpon.ParentPageId;
                if (pageDragged.ParentPageId == pageDragged.PrimaryId)
                {
                    // DK (2019-06-15) - Apparently this can happen
                    pageDragged.ParentPageId = null;
                }

                pageDragged.SortOrder = pageDraggedUpon.SortOrder;
                pageDragged.Save();

                // Get other childeren to sort
                List<IPage> childerenToSort = this.DataSource.Where(x => x.ParentPageId == pageDraggedUpon.ParentPageId && x.SortOrder >= pageDragged.SortOrder && x.PrimaryId != draggedPageId).OrderBy(x => x.SortOrder).ToList();
                int currentSortOrder = pageDragged.SortOrder + 1;
                for (int i = 0; i < childerenToSort.Count; i++)
                {
                    if (i == 0 && draggedDown)
                        childerenToSort[i].SortOrder = currentSortOrder - 2; // Dragged down with same parent, needs to be placed above it.
                    else
                        childerenToSort[i].SortOrder = currentSortOrder;
                    childerenToSort[i].Save();
                    currentSortOrder++;
                }
            }
        }
    }
}