﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Obymobi.Data.EntityClasses;

namespace Obymobi.Cms.Logic.Sites
{
    public class CopySiteRequest
    {
        public int SiteId;
        public string SiteName;        
        public Func<MediaEntity, MediaEntity, bool> CopyMediaOnCdn;
    }
}
