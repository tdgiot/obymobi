﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Obymobi.Data.EntityClasses;

namespace Obymobi.Cms.Logic.Sites
{
    public class CopySiteTemplateRequest
    {
        public SiteTemplateEntity SiteTemplate;
        public SiteEntity Site;
        public string SiteName;
        public bool CopyAttachments;
        public bool CopyMedia;
        public Func<MediaEntity, MediaEntity, bool> CopyMediaOnCdn;
    }
}
