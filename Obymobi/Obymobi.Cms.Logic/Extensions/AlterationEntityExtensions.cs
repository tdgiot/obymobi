﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Obymobi.Data.EntityClasses;
using Obymobi.Enums;

namespace Obymobi.Cms.Logic.Extensions
{
    public static class AlterationEntityExtensions
    {
        public static string GetCmsPage(this AlterationEntity alterationEntity)
        {
            AlterationVersion version = (AlterationVersion)alterationEntity.Version;

            switch (version)
            {
                case AlterationVersion.OptionsWithinOptions:
                    return $"AlterationV3.aspx?id={alterationEntity.AlterationId}";
                default:
                    return $"Alteration.aspx?id={alterationEntity.AlterationId}";
            }
        }
    }
}
