﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Reflection;
using Obymobi.Cms.Logic.Attributes;

namespace Obymobi.Cms.Logic.Extensions
{
    public static class FieldInfoExtensions
    {
        public static IEnumerable<FieldInfo> GetConstantsFromType(this Type type)
        {
            List<FieldInfo> constants = new List<FieldInfo>();

            foreach (FieldInfo fieldInfo in type.GetFields(BindingFlags.Public | BindingFlags.Static | BindingFlags.FlattenHierarchy))
            {
                if (fieldInfo.IsLiteral && !fieldInfo.IsInitOnly)
                {
                    constants.Add(fieldInfo);
                }
            }

            return constants;
        }

        public static string GetDescription(this FieldInfo fieldInfo)
        {
            if (!(fieldInfo.GetCustomAttribute(typeof(DescriptionAttribute)) is DescriptionAttribute descriptionAttribute))
                return string.Empty;

            return descriptionAttribute.Description;
        }

        public static string GetTooltipDescription(this FieldInfo fieldInfo)
        {
            if (!(fieldInfo.GetCustomAttribute(typeof(TooltipAttribute)) is TooltipAttribute tooltipAttribute))
                return string.Empty;

            return tooltipAttribute.Description;
        }
    }
}
