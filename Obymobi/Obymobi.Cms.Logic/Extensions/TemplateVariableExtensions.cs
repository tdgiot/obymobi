﻿using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Obymobi.Cms.Logic.Attributes;
using Obymobi.Cms.Logic.ViewModels;

namespace Obymobi.Cms.Logic.Extensions
{
    public static class TemplateVariableExtensions
    {
        public static IEnumerable<TemplateVariable> ConvertToTemplateVaribales(this IEnumerable<FieldInfo> constants)
        {
            foreach (FieldInfo constant in constants.OrderBy(fi => fi.Name))
            {
                string name = constant.GetValue(null).ToString();
                string description = constant.GetDescription();
                string tooltip = constant.GetTooltipDescription();

                yield return new TemplateVariable(name, description, tooltip);
            }
        }
    }
}
