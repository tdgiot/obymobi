﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Obymobi.Data.EntityClasses;

namespace Obymobi.Cms.Logic.Extensions
{
    public static class OrderEntityExtensions
    {
        public static bool GetDisplayPriceIncludingTaxes(this OrderEntity order)
        {
            return order.CompanyEntity.PricesIncludeTaxes;
        }
    }
}
