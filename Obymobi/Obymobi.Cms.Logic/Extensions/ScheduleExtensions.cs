﻿using Obymobi.Data.EntityClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Obymobi.Cms.Logic
{
    public static class ScheduleExtensions
    {
        public static string TimeStartFromDateTime(this ScheduleitemEntity scheduleitem, DateTime time)
        {
            scheduleitem.TimeStart = time.ToString("HHmm");
            return scheduleitem.TimeStart;
        }

        public static string TimeEndFromDateTime(this ScheduleitemEntity scheduleitem, DateTime time)
        {
            scheduleitem.TimeEnd = time.ToString("HHmm");
            return scheduleitem.TimeEnd;
        }
    }
}
