﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Obymobi.Data.EntityClasses;
using Obymobi.Extensions;

namespace Obymobi.Cms.Logic.Extensions
{
    public static class OrderitemEntityExtensions
    {
        /// <summary>
        /// Gets the sub total price (excluding tax).
        /// </summary>
        public static decimal GetSubTotal(this OrderitemEntity orderitem)
        {
            return orderitem.PriceTotalExTax + orderitem.OrderitemAlterationitemCollection.Sum(o => o.PriceTotalExTax);
        }

        /// <summary>
        /// Gets the total price (including tax)
        /// </summary>
        public static decimal GetTotal(this OrderitemEntity orderitem)
        {
            return orderitem.PriceTotalInTax + orderitem.OrderitemAlterationitemCollection.Sum(o => o.PriceTotalInTax);
        }

        /// <summary>
        /// Gets the price as shown to the customer (with or without tax).
        /// </summary>
        public static decimal GetDisplayPrice(this OrderitemEntity orderitem)
        {
            return orderitem.OrderEntity.GetDisplayPriceIncludingTaxes() ? orderitem.PriceInTax : orderitem.PriceExTax;
        }

        /// <summary>
        /// Gets the total price as shown to the customer (with or without tax).
        /// </summary>
        public static decimal GetTotalDisplayPrice(this OrderitemEntity orderitem)
        {
            return orderitem.OrderEntity.GetDisplayPriceIncludingTaxes() ? orderitem.PriceTotalInTax : orderitem.PriceTotalExTax;
        }

        /// <summary>
        /// Gets the price as shown to the customer (with or without tax) formatted based on company culture.
        /// </summary>
        public static string GetDisplayPriceFormatted(this OrderitemEntity orderitem)
        {
            return orderitem.GetDisplayPrice().Format(orderitem.OrderEntity.CultureCode);
        } 

        /// <summary>
        /// Gets the total price as shown to the customer (with or without tax) formatted based on company culture.
        /// </summary>
        public static string GetTotalDisplayPriceFormatted(this OrderitemEntity orderitem)
        {
            return orderitem.GetTotalDisplayPrice().Format(orderitem.OrderEntity.CultureCode);
        }

        /// <summary>
        /// Gets the tax total (including alteration items).
        /// </summary>
        public static decimal GetTaxTotalIncludingAlterationitems(this OrderitemEntity orderitem)
        {
            return orderitem.TaxTotal + orderitem.OrderitemAlterationitemCollection.Sum(o => o.TaxTotal);
        }
    }
}
