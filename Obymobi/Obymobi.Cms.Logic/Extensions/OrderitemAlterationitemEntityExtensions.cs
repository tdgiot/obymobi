﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Obymobi.Data.EntityClasses;
using Obymobi.Extensions;

namespace Obymobi.Cms.Logic.Extensions
{
    public static class OrderitemAlterationitemEntityExtensions
    {
        /// <summary>
        /// Gets the quantity of the orderitem, returns 1 if the order level enabled property is set to True.
        /// </summary>
        public static int GetQuantity(this OrderitemAlterationitemEntity orderitemAlterationitem)
        {
            return orderitemAlterationitem.OrderLevelEnabled ? 1 : orderitemAlterationitem.OrderitemEntity.Quantity;
        }

        /// <summary>
        /// Gets the price as shown to the customer (with or without tax).
        /// </summary>
        public static decimal GetDisplayPrice(this OrderitemAlterationitemEntity orderitemAlterationitem)
        {
            return orderitemAlterationitem.OrderitemEntity.OrderEntity.GetDisplayPriceIncludingTaxes() ? orderitemAlterationitem.PriceInTax : orderitemAlterationitem.PriceExTax;
        }

        /// <summary>
        /// Gets the total price as shown to the customer (with or without tax).
        /// </summary>
        public static decimal GetTotalDisplayPrice(this OrderitemAlterationitemEntity orderitemAlterationitem)
        {
            return orderitemAlterationitem.OrderitemEntity.OrderEntity.GetDisplayPriceIncludingTaxes() ? orderitemAlterationitem.PriceTotalInTax : orderitemAlterationitem.PriceTotalExTax;
        }

        /// <summary>
        /// Gets the price as shown to the customer (with or without tax) formatted based on company culture.
        /// </summary>
        public static string GetDisplayPriceFormatted(this OrderitemAlterationitemEntity orderitemAlterationitem)
        {
            return orderitemAlterationitem.GetDisplayPrice().Format(orderitemAlterationitem.OrderitemEntity.OrderEntity.CultureCode);
        } 

        /// <summary>
        /// Gets the total price as shown to the customer (with or without tax) formatted based on company culture.
        /// </summary>
        public static string GetTotalDisplayPriceFormatted(this OrderitemAlterationitemEntity orderitemAlterationitem)
        {
            return orderitemAlterationitem.GetTotalDisplayPrice().Format(orderitemAlterationitem.OrderitemEntity.OrderEntity.CultureCode);
        }
    }
}
