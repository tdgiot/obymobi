﻿using System.Collections.Generic;
using Dionysos.Web.UI;
using ServiceStack.Text;

namespace Obymobi.Cms.Logic.Extensions
{
    public static class PageEntityExtensions
    {
        public static void ExportToCsv<T>(this PageEntity page, string filename, IEnumerable<T> collection)
        {
            if (!filename.EndsWith(".csv"))
            {
                filename += ".csv";
            }

            page.Response.Clear();
            page.Response.ContentType = "text/csv";
            page.Response.AddHeader("Content-Disposition", $"attachment; filename={filename}");
            page.Response.OutputStream.WriteCsv(collection);
            page.Response.Flush();
            page.Response.Close();
            page.Response.End();
        }
    }
}
