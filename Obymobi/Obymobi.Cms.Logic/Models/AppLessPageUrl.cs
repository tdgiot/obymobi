﻿using System;
using Obymobi.Enums;

namespace Obymobi.Cms.Logic.Models
{
    public class AppLessPageUrl
    {
        private const string SCHEME = "https";

        private const string PREFIX = "appless.";
        private const string HOST = "trueguest.tech";
        private const string PROD_HOST = PREFIX + "prod." + HOST;
        private const string STAGING_HOST = PREFIX + "stag." + HOST;
        private const string TEST_HOST = PREFIX + "test." + HOST;
        private const string DEV_HOST = PREFIX + "dev." + HOST;
        private const string LOCAL_HOST = "localhost";
        private const int LOCAL_PORT = 4200;

        private CloudEnvironment CloudEnvironment { get; }
        private string ExternalApplicationId { get; }
        private string ExternalPageId { get; }

        public AppLessPageUrl(CloudEnvironment cloudEnvironment, string externalApplicationId, string externalPageId)
        {
            CloudEnvironment = cloudEnvironment;
            ExternalApplicationId = externalApplicationId;
            ExternalPageId = externalPageId;
        }

        public static implicit operator string(AppLessPageUrl appLessUrl) => FormatUrl(appLessUrl);

        private static string FormatUrl(AppLessPageUrl appLessUrl)
        {
            UriBuilder uriBuilder = new UriBuilder();
            uriBuilder.Scheme = SCHEME;
            uriBuilder.Path = $"{appLessUrl.ExternalApplicationId}/{appLessUrl.ExternalPageId}";

            switch (appLessUrl.CloudEnvironment)
            {
                case CloudEnvironment.ProductionPrimary:
                case CloudEnvironment.ProductionSecondary:
                case CloudEnvironment.ProductionOverwrite:
                    uriBuilder.Host = PROD_HOST;
                    break;
                case CloudEnvironment.Staging:
                    uriBuilder.Host = STAGING_HOST;
                    break;
                case CloudEnvironment.Test:
                    uriBuilder.Host = TEST_HOST;
                    break;
                case CloudEnvironment.Development:
                    uriBuilder.Host = DEV_HOST;
                    break;
                case CloudEnvironment.Manual:
                    uriBuilder.Host = LOCAL_HOST;
                    uriBuilder.Port = LOCAL_PORT;
                    break;
            }

            return uriBuilder.ToString();
        }
    }
}
