﻿using System.Diagnostics;

namespace Obymobi.Cms.Logic.Models
{
    [DebuggerDisplay("CustomerName={CustomerName}, EmailAddress={EmailAddress}, PhoneNumber={PhoneNumber}")]
    public class OptInRecord
    {
        public string Date { get; set; }

        public string Time { get; set; }

        public string EmailAddress { get; set; }

        public string PhoneNumber { get; set; }

        public string CustomerName { get; set; }
    }
}
