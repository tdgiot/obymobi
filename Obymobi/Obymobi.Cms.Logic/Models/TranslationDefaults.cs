﻿using System.Globalization;
using System.Resources;

namespace Obymobi.Cms.Logic.Models
{
    public class TranslationDefaults
    {
        public TranslationDefaults(CultureInfo cultureInfo, ResourceSet resourceSet)
        {
            CultureInfo = cultureInfo;
            ResourceSet = resourceSet;
        }

        public CultureInfo CultureInfo { get; }
        public ResourceSet ResourceSet { get; }
    }
}
