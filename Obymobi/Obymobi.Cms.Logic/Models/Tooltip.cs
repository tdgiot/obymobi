﻿using System;

namespace Obymobi.Cms.Logic.Models
{
    public class Tooltip
    {
        public Tooltip(string name, string description)
        {
            this.Identifier = Guid.NewGuid();

            this.Name = name;
            this.Description = description;
        }

        public Guid Identifier { get; }

        public string Name { get; }

        public string Description { get; }

        public string InstanceName => $"{Identifier}-{Name}-{nameof(Tooltip)}";
    }
}
