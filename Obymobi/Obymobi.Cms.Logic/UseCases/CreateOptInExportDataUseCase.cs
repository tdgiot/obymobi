﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Dionysos;
using Obymobi.Cms.Logic.Models;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;

namespace Obymobi.Cms.Logic.UseCases
{
    public class CreateOptInExportDataUseCase
    {
        public ICollection<OptInRecord> Execute(OptInCollection optIns, DateTimeFormatInfo dateTimeFormatInfo)
        {
            if (!optIns.Any())
            {
                return new List<OptInRecord>(0);
            }
            
            ICollection<OptInRecord> optInRecords = ConvertOptInEntitiesToOptInRecords(optIns, dateTimeFormatInfo);
            
            if (HasMergeableOptInRecords(optInRecords))
            {
                optInRecords = MergeOptInRecords(optInRecords);
            }

            return optInRecords;
        }

        private bool HasMergeableOptInRecords(ICollection<OptInRecord> optInRecords) => optInRecords.Any(x => !x.CustomerName.IsNullOrWhiteSpace());

        private ICollection<OptInRecord> MergeOptInRecords(ICollection<OptInRecord> optInRecords)
        {
            List<OptInRecord> resultCollection = new List<OptInRecord>(optInRecords.Where(x => x.CustomerName.IsNullOrWhiteSpace()));
            
            List<OptInRecord> optInsWithCustomerName = optInRecords.Where(x => !x.CustomerName.IsNullOrWhiteSpace()).ToList();
            foreach (OptInRecord optInRecord in optInsWithCustomerName)
            {
                List<OptInRecord> recordsWithSameCustomerName = optInsWithCustomerName.Where(x => x.CustomerName == optInRecord.CustomerName).ToList();
                if (recordsWithSameCustomerName.Count == 2)
                {
                    if (resultCollection.Any(x => x.CustomerName.Equals(optInRecord.CustomerName, StringComparison.CurrentCultureIgnoreCase)))
                    {
                        continue;
                    }

                    resultCollection.Add(new OptInRecord
                    {
                        Date = optInRecord.Date,
                        Time = optInRecord.Time,
                        CustomerName = optInRecord.CustomerName,
                        EmailAddress = recordsWithSameCustomerName.FirstOrDefault(x => !x.EmailAddress.IsNullOrWhiteSpace())?.EmailAddress,
                        PhoneNumber = recordsWithSameCustomerName.FirstOrDefault(x => !x.PhoneNumber.IsNullOrWhiteSpace())?.PhoneNumber
                    });
                }
                else
                {
                    resultCollection.Add(optInRecord);
                }
            }

            return resultCollection;
        }

        private ICollection<OptInRecord> ConvertOptInEntitiesToOptInRecords(OptInCollection optIns, DateTimeFormatInfo dateTimeFormatInfo)
        {
            List<OptInRecord> optInRecords = new List<OptInRecord>(optIns.Count);

            foreach (OptInEntity optIn in optIns)
            {
                string date = optIn.CreatedUTC.ToString(dateTimeFormatInfo.ShortDatePattern);
                string time = optIn.CreatedUTC.ToString(dateTimeFormatInfo.LongTimePattern);

                optInRecords.Add(new OptInRecord
                {
                    Date = date,
                    Time = time,
                    EmailAddress = optIn.Email,
                    PhoneNumber = optIn.PhoneNumber,
                    CustomerName = optIn.CustomerName?.ToLowerInvariant()
                });
            }

            return optInRecords;
        }
    }
}
