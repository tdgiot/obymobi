﻿using System;
using System.Collections.Generic;
using System.Linq;
using Dionysos;
using Obymobi.Data;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Cms.Logic.UseCases
{
    public class UpdateOrderitemCategoryPathUseCase
    {
        public int Execute(bool testRun = false)
        {
            int numberOfOrderitemsUpdated = 0;

            Dictionary<int, string> menuNameLookup = GetMenuNameLookup();
            OrderitemCollection appLessOrderitems = GetAppLessOrderitems();

            foreach (OrderitemEntity orderitemEntity in appLessOrderitems)
            {
                if (!orderitemEntity.CategoryPath.Contains("Crave.Ordering.Data", StringComparison.InvariantCultureIgnoreCase))
                {
                    continue;
                }
                
                string categoryPath = orderitemEntity.CategoryEntity.FullCategoryName;
                if (menuNameLookup.TryGetValue(orderitemEntity.CategoryEntity.MenuId.GetValueOrDefault(0), out string menuName))
                {
                    categoryPath = $"{menuName}\\{categoryPath}";
                }

                if (!categoryPath.IsNullOrWhiteSpace())
                {
                    orderitemEntity.CategoryPath = categoryPath;
                    numberOfOrderitemsUpdated++;
                }
            }

            if (!testRun)
            {
                appLessOrderitems.SaveMulti();
            }

            return numberOfOrderitemsUpdated;
        }

        private OrderitemCollection GetAppLessOrderitems()
        {
            RelationCollection relations = new RelationCollection();
            relations.Add(OrderitemEntity.Relations.OrderEntityUsingOrderId);

            PredicateExpression filter = new PredicateExpression();
            filter.Add(OrderitemFields.CategoryId != DBNull.Value);
            filter.Add(OrderFields.OutletId > 0);
            
            IncludeFieldsList categoryIncludedFields = new IncludeFieldsList(CategoryFields.Name);

            PrefetchPath prefetch = new PrefetchPath(EntityType.OrderitemEntity);
            var categoryPrefetch = prefetch.Add(OrderitemEntity.PrefetchPathCategoryEntity, categoryIncludedFields);
            categoryPrefetch.SubPath.Add(CategoryEntity.PrefetchPathParentCategoryEntity, categoryIncludedFields)
                    .SubPath.Add(CategoryEntity.PrefetchPathParentCategoryEntity, categoryIncludedFields)
                    .SubPath.Add(CategoryEntity.PrefetchPathParentCategoryEntity, categoryIncludedFields)
                    .SubPath.Add(CategoryEntity.PrefetchPathParentCategoryEntity, categoryIncludedFields);

            OrderitemCollection orderitemCollection = new OrderitemCollection();
            orderitemCollection.GetMulti(filter, 0, null, relations, prefetch, null, 0, 0);

            return orderitemCollection;
        }

        private Dictionary<int, string> GetMenuNameLookup()
        {
            IncludeFieldsList menuIncludedFields = new IncludeFieldsList(MenuFields.Name);

            MenuCollection menuCollection = new MenuCollection();
            menuCollection.GetMulti(null, menuIncludedFields, null);

            return menuCollection.ToDictionary(m => m.MenuId, m => m.Name);
        }
    }
}
