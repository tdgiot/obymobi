﻿using System;
using System.Collections.Generic;
using Dionysos.Data.LLBLGen;
using Obymobi.Cms.Logic.Requests;
using Obymobi.Data;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Logic.HelperClasses;
using Obymobi.Web.CloudStorage;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Cms.Logic.UseCases
{
    public class CopyDeliverypointgroupUseCase
    {
        private readonly CopyMediaUseCase copyMediaUseCase = new CopyMediaUseCase(); // TODO Use dependency injection

        public DeliverypointgroupEntity Execute(CopyDeliverypointgroupRequest request)
        {
            int deliverypointgroupIdToCopy = request.DeliverypointgroupIdToCopy;
            string newDeliverypointgroupName = request.NewDeliverypointgroupName;
            Func<MediaEntity, MediaEntity, bool> copyMediaOnCdn = request.CopyMediaOnCdn;
            DeliverypointgroupEntity newDeliverypointgroupEntity = request.NewDeliverypointgroupEntity;

            Transaction transaction = new Transaction(System.Data.IsolationLevel.ReadUncommitted, "DeliverypointgroupCopier");

            try
            {
                // Get the prefetched deliverypointgroup
                DeliverypointgroupEntity deliverypointgroupFrom = this.PrefetchDeliverypointgroup(deliverypointgroupIdToCopy, true);

                // Import the deliverypointgroup
                newDeliverypointgroupEntity = this.ImportDeliverypointgroup(transaction, deliverypointgroupFrom, newDeliverypointgroupName, copyMediaOnCdn); 

                // Commit the transaction
                transaction.Commit();
            }
            catch (Exception ex)
            {
                newDeliverypointgroupEntity = null;
                transaction.Rollback();
                throw ex;
            }
            finally
            {                
                transaction.Dispose();
            }
           
            return newDeliverypointgroupEntity;
        }

        private DeliverypointgroupEntity PrefetchDeliverypointgroup(int deliverypointgroupId, bool includeMedia)
        {
            // Deliverypointgroup
            PrefetchPath prefetch = new PrefetchPath(EntityType.DeliverypointgroupEntity);
            // Deliverypoints
            prefetch.Add(DeliverypointgroupEntityBase.PrefetchPathDeliverypointCollection);
            // DeliverypointgroupAdvertisement
            prefetch.Add(DeliverypointgroupEntityBase.PrefetchPathDeliverypointgroupAdvertisementCollection);
            // DeliverypointgroupAnnouncement
            prefetch.Add(DeliverypointgroupEntityBase.PrefetchPathDeliverypointgroupAnnouncementCollection);
            // DeliverypointgroupEntertainment
            prefetch.Add(DeliverypointgroupEntityBase.PrefetchPathDeliverypointgroupEntertainmentCollection);
            // CustomText
            prefetch.Add(DeliverypointgroupEntityBase.PrefetchPathCustomTextCollection);
            // DeliverypointgroupOccupancy
            prefetch.Add(DeliverypointgroupEntityBase.PrefetchPathDeliverypointgroupOccupancyCollection);
            // DeliverypointgroupProduct
            prefetch.Add(DeliverypointgroupEntityBase.PrefetchPathDeliverypointgroupProductCollection);
            // ClientConfiguration
            prefetch.Add(DeliverypointgroupEntityBase.PrefetchPathClientConfigurationEntity);

            if (includeMedia)
            {
                IPrefetchPathElement prefetchMediaDeliverypointgroup = prefetch.Add(DeliverypointgroupEntityBase.PrefetchPathMediaCollection);
                IPrefetchPathElement prefetchMediaRatioTypeMediaDeliverypointgroup = prefetchMediaDeliverypointgroup.SubPath.Add(MediaEntityBase.PrefetchPathMediaRatioTypeMediaCollection);
                IPrefetchPathElement prefetchMediaRatioTypemediaFileDeliverypointgroup = prefetchMediaRatioTypeMediaDeliverypointgroup.SubPath.Add(MediaRatioTypeMediaEntityBase.PrefetchPathMediaRatioTypeMediaFileEntity);
            }

            DeliverypointgroupEntity deliverypointgroup = new DeliverypointgroupEntity(deliverypointgroupId, prefetch);
            return deliverypointgroup;
        }

        private DeliverypointgroupEntity ImportDeliverypointgroup(Transaction transaction, DeliverypointgroupEntity copyFrom, string newDeliverypointgroupName, Func<MediaEntity, MediaEntity, bool> copyMediaOnCdn)
        {
            // Create new client config for new DPG
            ClientConfigurationEntity newClientConfigurationEntity = CreateClientConfigurationForDeliverypointgroup(copyFrom, newDeliverypointgroupName, transaction);
            
            DeliverypointgroupEntity newDeliverypointgroup = new DeliverypointgroupEntity();
            LLBLGenEntityUtil.CopyFields(copyFrom, newDeliverypointgroup, null);
            newDeliverypointgroup.Name = newDeliverypointgroupName;
            newDeliverypointgroup.BeingCopied = true;
            newDeliverypointgroup.ClientConfigurationId = newClientConfigurationEntity.ClientConfigurationId; // Set new client configuration id on DPG
            newDeliverypointgroup.AddToTransaction(transaction);
            newDeliverypointgroup.Save();

            // Update DPG id on new client configuration
            newClientConfigurationEntity.DeliverypointgroupId = newDeliverypointgroup.DeliverypointgroupId;
            newClientConfigurationEntity.Save();

            // Deliverypoints
            this.ImportDeliverypoints(copyFrom.DeliverypointCollection, newDeliverypointgroup.DeliverypointgroupId, transaction);

            // DeliverypointgroupAdvertisement
            this.ImportDeliverypointgroupAdvertisement(copyFrom.DeliverypointgroupAdvertisementCollection, newDeliverypointgroup.DeliverypointgroupId, transaction);

            // DeliverypointgroupAnnouncement
            this.ImportDeliverypointgroupAnnouncement(copyFrom.DeliverypointgroupAnnouncementCollection, newDeliverypointgroup.DeliverypointgroupId, transaction);

            // DeliverypointgroupEntertainment
            this.ImportDeliverypointgroupEntertainment(copyFrom.DeliverypointgroupEntertainmentCollection, newDeliverypointgroup.DeliverypointgroupId, transaction);

            // CustomText
            this.ImportCustomText(copyFrom.CustomTextCollection, newDeliverypointgroup.DeliverypointgroupId, transaction);

            // DeliverypointgroupOccupancy
            this.ImportDeliverypointgroupOccupancy(copyFrom.DeliverypointgroupOccupancyCollection, newDeliverypointgroup.DeliverypointgroupId, transaction);

            // DeliverypointgroupProduct
            this.ImportDeliverypointgroupProduct(copyFrom.DeliverypointgroupProductCollection, newDeliverypointgroup.DeliverypointgroupId, transaction);

            // Media
            this.ImportMedia(copyFrom.MediaCollection, newDeliverypointgroup.DeliverypointgroupId, copyMediaOnCdn, transaction);

            return newDeliverypointgroup;
        }

        private ClientConfigurationEntity CreateClientConfigurationForDeliverypointgroup(DeliverypointgroupEntity copyFromDeliverypointgroup, string newDeliverypointgroupName, ITransaction transaction)
        {
            ClientConfigurationEntity newClientConfig = new ClientConfigurationEntity();
            LLBLGenEntityUtil.CopyFields(copyFromDeliverypointgroup.ClientConfigurationEntity, newClientConfig, null);
            newClientConfig.Name = newDeliverypointgroupName;
            newClientConfig.AddToTransaction(transaction);
            newClientConfig.Save();

            return newClientConfig;
        }

        private void ImportDeliverypoints(DeliverypointCollection deliverypoints, int deliverypointgroupId, Transaction transaction)
        {
            foreach (DeliverypointEntity deliverypoint in deliverypoints)
            {
                DeliverypointEntity newDeliverypoint = new DeliverypointEntity();
                LLBLGenEntityUtil.CopyFields(deliverypoint, newDeliverypoint, null);
                newDeliverypoint.DeliverypointgroupId = deliverypointgroupId;
                newDeliverypoint.PosdeliverypointId = null;

                newDeliverypoint.AddToTransaction(transaction);
                newDeliverypoint.Save();
            }
        }

        private void ImportDeliverypointgroupAdvertisement(DeliverypointgroupAdvertisementCollection deliverypointgroupAdvertisements, int deliverypointgroupId, Transaction transaction)
        {
            foreach (DeliverypointgroupAdvertisementEntity deliverypointgroupAdvertisement in deliverypointgroupAdvertisements)
            {
                DeliverypointgroupAdvertisementEntity newDeliverypointgroupAdvertisement = new DeliverypointgroupAdvertisementEntity();
                newDeliverypointgroupAdvertisement.ParentCompanyId = deliverypointgroupAdvertisement.ParentCompanyId;
                newDeliverypointgroupAdvertisement.AdvertisementId = deliverypointgroupAdvertisement.AdvertisementId;
                newDeliverypointgroupAdvertisement.DeliverypointgroupId = deliverypointgroupId;

                newDeliverypointgroupAdvertisement.AddToTransaction(transaction);
                newDeliverypointgroupAdvertisement.Save();
            }
        }

        private void ImportDeliverypointgroupAnnouncement(DeliverypointgroupAnnouncementCollection deliverypointgroupAnnouncements, int deliverypointgroupId, Transaction transaction)
        {
            foreach (DeliverypointgroupAnnouncementEntity deliverypointgroupAnnouncement in deliverypointgroupAnnouncements)
            {
                DeliverypointgroupAnnouncementEntity newDeliverypointgroupAnnouncement = new DeliverypointgroupAnnouncementEntity();
                newDeliverypointgroupAnnouncement.ParentCompanyId = deliverypointgroupAnnouncement.ParentCompanyId;
                newDeliverypointgroupAnnouncement.AnnouncementId = deliverypointgroupAnnouncement.AnnouncementId;
                newDeliverypointgroupAnnouncement.DeliverypointgroupId = deliverypointgroupId;

                newDeliverypointgroupAnnouncement.AddToTransaction(transaction);
                newDeliverypointgroupAnnouncement.Save();
            }
        }

        private void ImportDeliverypointgroupEntertainment(DeliverypointgroupEntertainmentCollection deliverypointgroupEntertainments, int deliverypointgroupId, Transaction transaction)
        {
            foreach (DeliverypointgroupEntertainmentEntity deliverypointgroupEntertainment in deliverypointgroupEntertainments)
            {
                DeliverypointgroupEntertainmentEntity newDeliverypointgroupEntertainment = new DeliverypointgroupEntertainmentEntity();
                newDeliverypointgroupEntertainment.ParentCompanyId = deliverypointgroupEntertainment.ParentCompanyId;
                newDeliverypointgroupEntertainment.EntertainmentId = deliverypointgroupEntertainment.EntertainmentId;
                newDeliverypointgroupEntertainment.DeliverypointgroupId = deliverypointgroupId;

                newDeliverypointgroupEntertainment.AddToTransaction(transaction);
                newDeliverypointgroupEntertainment.Save();
            }
        }

        private void ImportCustomText(CustomTextCollection customTexts, int deliverypointgroupId, Transaction transaction)
        {
            foreach (CustomTextEntity customText in customTexts)
            {
                CustomTextEntity newCustomText = new CustomTextEntity();
                LLBLGenEntityUtil.CopyFields(customText, newCustomText, null);
                newCustomText.DeliverypointgroupId = deliverypointgroupId;

                newCustomText.AddToTransaction(transaction);
                newCustomText.Save();
            }
        }

        private void ImportDeliverypointgroupOccupancy(DeliverypointgroupOccupancyCollection deliverypointgroupOccupancies, int deliverypointgroupId, Transaction transaction)
        {
            foreach (DeliverypointgroupOccupancyEntity deliverypointgroupOccupancy in deliverypointgroupOccupancies)
            {
                DeliverypointgroupOccupancyEntity newDeliverypointgroupOccupancy = new DeliverypointgroupOccupancyEntity();
                LLBLGenEntityUtil.CopyFields(deliverypointgroupOccupancy, newDeliverypointgroupOccupancy, null);
                newDeliverypointgroupOccupancy.DeliverypointgroupId = deliverypointgroupId;

                newDeliverypointgroupOccupancy.AddToTransaction(transaction);
                newDeliverypointgroupOccupancy.Save();
            }
        }

        private void ImportDeliverypointgroupProduct(DeliverypointgroupProductCollection deliverypointgroupProducts, int deliverypointgroupId, Transaction transaction)
        {
            foreach (DeliverypointgroupProductEntity deliverypointgroupProduct in deliverypointgroupProducts)
            {
                DeliverypointgroupProductEntity newDeliverypointgroupProduct = new DeliverypointgroupProductEntity();
                LLBLGenEntityUtil.CopyFields(deliverypointgroupProduct, newDeliverypointgroupProduct, null);
                newDeliverypointgroupProduct.DeliverypointgroupId = deliverypointgroupId;

                newDeliverypointgroupProduct.AddToTransaction(transaction);
                newDeliverypointgroupProduct.Save();
            }
        }

        private void ImportMedia(MediaCollection medias, int deliverypointgroupId, Func<MediaEntity, MediaEntity, bool> copyMediaOnCdn, Transaction transaction)
        {
            // Copy agnostic media first, old mediaId, new mediaId 
            Dictionary<int, int> newMedias = new Dictionary<int, int>();

            foreach (MediaEntity media in medias)
            {
                if (media.AgnosticMediaId.HasValue && !newMedias.ContainsKey(media.AgnosticMediaId.Value))
                {
                    CopyMediaRequest request = new CopyMediaRequest
                    {
                        MediaToCopy = media,
                        ReferenceField = MediaFields.DeliverypointgroupId,
                        ReferenceValue = deliverypointgroupId,
                        CopyMediaOnCdn = copyMediaOnCdn,
                        Transaction = transaction
                    };
                    MediaEntity newAgnosticMedia = this.copyMediaUseCase.Execute(request);
                    newMedias.Add(media.AgnosticMediaId.Value, newAgnosticMedia.MediaId);
                }
            }

            // Set the agnostic media ids for the newly created media
            foreach (MediaEntity media in medias)
            {
                if (!newMedias.ContainsKey(media.MediaId))
                {
                    CopyMediaRequest request = new CopyMediaRequest
                    {
                        MediaToCopy = media,
                        ReferenceField = MediaFields.DeliverypointgroupId,
                        ReferenceValue = deliverypointgroupId,
                        CopyMediaOnCdn = copyMediaOnCdn,
                        Transaction = transaction
                    };

                    MediaEntity newMedia = this.copyMediaUseCase.Execute(request);
                    if (media.AgnosticMediaId.HasValue && newMedias.ContainsKey(media.AgnosticMediaId.Value))
                    {
                        newMedia.AgnosticMediaId = newMedias[media.AgnosticMediaId.Value];
                    }

                    newMedia.Save(true);
                }
            }
        }
    }
}
