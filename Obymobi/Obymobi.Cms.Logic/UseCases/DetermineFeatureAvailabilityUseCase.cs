﻿using Obymobi.Cms.Logic.DataSources;
using Obymobi.Data.EntityClasses;
using Obymobi.Enums;

namespace Obymobi.Cms.Logic.UseCases
{
    public class DetermineFeatureAvailabilityUseCase
    {
        private readonly FeatureToggleAvailabilityDataSource featureToggleAvailabilityDataSource = new FeatureToggleAvailabilityDataSource();

        public bool Execute(Role role, ReleaseGroup releaseGroup, FeatureToggle toggleType)
        {
            FeatureToggleAvailabilityEntity featureToggleAvailability = featureToggleAvailabilityDataSource.GetOrCreateDefault(toggleType);

            return DetermineVisibilityForUserRole(featureToggleAvailability, role) &&
                   DetermineVisibilityForReleaseGroup(featureToggleAvailability, releaseGroup);
        }

        private static bool DetermineVisibilityForUserRole(FeatureToggleAvailabilityEntityBase featureToggleAvailability, Role userRole) => featureToggleAvailability.Role <= userRole;
        private static bool DetermineVisibilityForReleaseGroup(FeatureToggleAvailabilityEntityBase featureToggleAvailability, ReleaseGroup releaseGroup) => featureToggleAvailability.ReleaseGroup >= releaseGroup;
    }
}
