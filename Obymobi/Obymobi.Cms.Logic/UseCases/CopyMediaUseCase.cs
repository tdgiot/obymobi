﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dionysos;
using Dionysos.Data.LLBLGen;
using Obymobi.Cms.Logic.Requests;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;

namespace Obymobi.Cms.Logic.UseCases
{
    public class CopyMediaUseCase
    {
        private readonly ArrayList mediaFieldsNotToBeCopied = new ArrayList
        {
            MediaFields.ProductId,
            MediaFields.CategoryId,
            MediaFields.AdvertisementId,
            MediaFields.DeliverypointgroupId,
            MediaFields.SurveyId,
            MediaFields.SurveyPageId,
            MediaFields.EntertainmentId,
            MediaFields.PageId,
            MediaFields.PageElementId,
            MediaFields.PageTemplateId,
            MediaFields.PageTemplateElementId,
            MediaFields.UIWidgetId,
            MediaFields.UIThemeId,
            MediaFields.LastDistributedVersionTicks,
            MediaFields.LastDistributedVersionTicksAmazon
        };

        public MediaEntity Execute(CopyMediaRequest request)
        {
            MediaEntity newMedia = new MediaEntity();

            if (request.Transaction != null) 
                newMedia.AddToTransaction(request.Transaction);

            LLBLGenEntityUtil.CopyFields(request.MediaToCopy, newMedia, this.mediaFieldsNotToBeCopied);

            if (request.ReferenceValue != null)
            {
                newMedia.SetNewFieldValue(request.ReferenceField.Name, request.ReferenceValue);
            }

            if (newMedia.Extension.IsNullOrWhiteSpace() || newMedia.MimeType.IsNullOrWhiteSpace())
            {
                newMedia.SetMimeTypeAndExtension(newMedia.FilePathRelativeToMediaPath);
            }

            // Copy media ratio type medias
            foreach (MediaRatioTypeMediaEntity oldMediaRatioTypeMedia in request.MediaToCopy.MediaRatioTypeMediaCollection)
            {
                ArrayList mediaRatioTypeMediaFieldsNotToBeCopied = new ArrayList();
                if (request.ToGenericEntity) mediaRatioTypeMediaFieldsNotToBeCopied.Add(MediaRatioTypeMediaFields.MediaType);

                MediaRatioTypeMediaEntity newMediaRatioTypeMedia = new MediaRatioTypeMediaEntity();
                if (request.Transaction != null) newMediaRatioTypeMedia.AddToTransaction(request.Transaction);
                LLBLGenEntityUtil.CopyFields(oldMediaRatioTypeMedia, newMediaRatioTypeMedia, mediaRatioTypeMediaFieldsNotToBeCopied);
                newMediaRatioTypeMedia.LastDistributedVersionTicks = null;
                newMediaRatioTypeMedia.LastDistributedVersionTicksAmazon = null;

                if (request.ToGenericEntity)
                {
                    // Set the right media ratio type if the media entity is being copied to a generic entity
                    newMediaRatioTypeMedia.MediaType = (int)MediaRatioTypes.GetGenericMediaTypeByNormalMediaType(oldMediaRatioTypeMedia.MediaTypeAsEnum);
                }

                if (newMediaRatioTypeMedia.MediaTypeAsEnum != MediaType.NoMediaTypeSpecified && oldMediaRatioTypeMedia.MediaRatioTypeMediaFileEntity != null)
                {
                    // Copy media ratio type media file                    
                    MediaRatioTypeMediaFileEntity newMediaRatioTypeMediaFile = new MediaRatioTypeMediaFileEntity();
                    if (request.Transaction != null) newMediaRatioTypeMediaFile.AddToTransaction(request.Transaction);
                    LLBLGenEntityUtil.CopyFields(oldMediaRatioTypeMedia.MediaRatioTypeMediaFileEntity, newMediaRatioTypeMediaFile, null);
                    newMediaRatioTypeMedia.MediaRatioTypeMediaFileEntity = newMediaRatioTypeMediaFile;

                    newMedia.MediaRatioTypeMediaCollection.Add(newMediaRatioTypeMedia);
                }
            }

            // Copy media cultures
            foreach (MediaCultureEntity oldMediaCulture in request.MediaToCopy.MediaCultureCollection)
            {
                MediaCultureEntity newMediaCulture = new MediaCultureEntity();
                if (request.Transaction != null) newMediaCulture.AddToTransaction(request.Transaction);
                LLBLGenEntityUtil.CopyFields(oldMediaCulture, newMediaCulture, null);
                newMedia.MediaCultureCollection.Add(newMediaCulture);
            }

            if (!request.SkipSave)
            {
                if (newMedia.Save(true))
                {
                    // Download the media file from old media and save it for new media
                    request.CopyMediaOnCdn(request.MediaToCopy, newMedia);
                }
            }

            return newMedia;
        }
    }
}
