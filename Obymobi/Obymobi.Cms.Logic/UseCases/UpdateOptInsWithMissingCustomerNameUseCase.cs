﻿using System;
using System.Linq;
using Dionysos;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Cms.Logic.UseCases
{
    public class UpdateOptInsWithMissingCustomerNameUseCase
    {
        public int Execute(bool testRun = false)
        {
            var outletsWithOptIns = this.GetOutletsWithOptIns();

            int numberOfOptInsUpdated = 0;
            foreach (OutletEntity outlet in outletsWithOptIns)
            {
                numberOfOptInsUpdated += CopyCustomerNameFromOrderToOptInForOutlet(outlet.OutletId, testRun);
            }

            return numberOfOptInsUpdated;
        }

        public int CopyCustomerNameFromOrderToOptInForOutlet(int outletId, bool testRun)
        {
            int numberOfOptInsToUpdate = 0;

            OptInCollection optIns = GetOptIns(outletId);
            if (!optIns.Any())
            {
                return 0;
            }

            OrderCollection orders = GetOptInOrders(outletId);
            foreach (OptInEntity optIn in optIns)
            {
                OrderEntity order = null;
                
                if (!optIn.Email.IsNullOrWhiteSpace())
                {
                    order = orders.FirstOrDefault(x => x.Email == optIn.Email);
                }
                else if (!optIn.PhoneNumber.IsNullOrWhiteSpace())
                {
                    order = orders.FirstOrDefault(x => x.CustomerPhonenumber == optIn.PhoneNumber);
                }

                if (order == null)
                {
                    continue;
                }

                numberOfOptInsToUpdate++;
                optIn.CustomerName = order.CustomerLastname;
                optIn.UpdatedUTC = DateTime.UtcNow;
            }

            if (numberOfOptInsToUpdate == 0)
            {
                return 0;
            }

            if (testRun)
            {
                return numberOfOptInsToUpdate;
            }

            return optIns.SaveMulti();
        }

        private OutletCollection GetOutletsWithOptIns()
        {
            RelationCollection relations = new RelationCollection();
            relations.Add(OutletEntity.Relations.OptInEntityUsingOutletId);

            OutletCollection outletCollection = new OutletCollection();
            outletCollection.GetMulti(null, relations);

            return outletCollection;
        }

        private OptInCollection GetOptIns(int outletId)
        {
            PredicateExpression filter = new PredicateExpression();
            filter.Add(OptInFields.OutletId == outletId);
            filter.Add(OptInFields.CustomerName == DBNull.Value);

            OptInCollection optInCollection = new OptInCollection();
            optInCollection.GetMulti(filter);

            return optInCollection;
        }

        private OrderCollection GetOptInOrders(int outletId)
        {
            PredicateExpression filter = new PredicateExpression();
            filter.Add(OrderFields.OutletId == outletId);
            filter.Add(OrderFields.OptInStatus == OptInStatus.HardOptIn);
            filter.Add(OrderFields.CustomerLastname != DBNull.Value);
            filter.Add(OrderFields.CustomerLastname != string.Empty);

            IncludeFieldsList includeFields = new IncludeFieldsList(
                OrderFields.CustomerLastname, 
                OrderFields.Email,
                OrderFields.CustomerPhonenumber,
                OrderFields.OptInStatus,
                OrderFields.CreatedUTC
            );

            OrderCollection orderCollection = new OrderCollection();
            orderCollection.GetMulti(filter, includeFields, null);

            return orderCollection;
        }
    }
}
