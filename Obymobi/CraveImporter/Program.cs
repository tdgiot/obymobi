﻿using System;
using System.Diagnostics;
using System.IO;
using System.Text;
using Dionysos;
using Dionysos.Configuration;
using Dionysos.Data.LLBLGen;
using Dionysos.Web;
using Obymobi;

namespace CraveImporter
{
    class Program
    {
        static void Main(string[] args)
        {
            // Set the application information
            Global.ApplicationInfo = new WebApplicationInfo();
            Global.ApplicationInfo.ApplicationName = "Crave Importer";
            Global.ApplicationInfo.BasePath = Environment.CurrentDirectory;
            Global.ApplicationInfo.ApplicationVersion = "1.0.0";

            // Set the configuration provivder
            Global.ConfigurationProvider = new LLBLGenConfigurationProvider();

            // Set the data providers
            DataFactory.EntityCollectionFactory = new LLBLGenEntityCollectionFactory();
            DataFactory.EntityFactory = new LLBLGenEntityFactory();
            DataFactory.EntityUtil = new LLBLGenEntityUtil();

            // Set the configuration definitions
            Global.ConfigurationInfo.Add(new DionysosConfigurationInfo());
            Global.ConfigurationInfo.Add(new DionysosWebConfigurationInfo());
            Global.ConfigurationInfo.Add(new ObymobiConfigInfo());
            Global.ConfigurationInfo.Add(new ObymobiDataConfigInfo());

            // Initialize the assembly information
            Global.AssemblyInfo.Add(AppDomain.CurrentDomain.BaseDirectory + "Obymobi.Data.dll", "Data");

            FileLogging();

            ShowMainMenu();
        }

        private static void ShowMainMenu()
        {
            Console.Clear();

            StringBuilder sb = new StringBuilder();
            sb.AppendLine("----------------------------------");
            sb.AppendLine("-- Crave Importer");
            sb.AppendLine("----------------------------------");
            sb.AppendLine("[1] Room Control Settings");
            sb.AppendLine();
            sb.AppendLine("[0] Exit");
            sb.AppendLine();

            Console.Write(sb);

            Console.Write("Action: ");
            string input = Console.ReadLine();

            switch (input)
            {
                case "E":
                case "e":
                    ChangeEnvironment();
                    break;
                case "1":
                    RoomControlSettings();
                    break;
                case "0":
                    ExitApp();
                    break;
                default:
                    ShowMainMenu();
                    break;
            }
        }

        static void ChangeEnvironment()
        {
            
        }

        static void RoomControlSettings()
        {
            RoomControlImporter importer = new RoomControlImporter();
            importer.ShowMenu();
            
            ShowMainMenu();
        }

        static void ExitApp()
        {
            Environment.Exit(1);
        }

        public static void FileLogging()
        {
            Trace.Listeners.Clear();

            TextWriterTraceListener twtl = new TextWriterTraceListener(Path.Combine(Environment.CurrentDirectory, "output.txt"));
            twtl.Name = "TextLogger";
            twtl.TraceOutputOptions = TraceOptions.Timestamp;

            Trace.Listeners.Add(twtl);

            ConsoleTraceListener ctl = new ConsoleTraceListener(false);
            ctl.TraceOutputOptions = TraceOptions.DateTime;
            
            Trace.Listeners.Add(ctl);
            Trace.AutoFlush = true;
        }
    }
}
