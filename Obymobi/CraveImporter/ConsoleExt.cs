﻿using System;

namespace CraveImporter
{
    public static class ConsoleExt
    {
        public static void Write(string message, params object[] args)
        {
            Console.Write(string.Format(message, args));
        }
    }
}