﻿using System.Text;

namespace CraveImporter
{
    public static class StringBuilderExt
    {
        public static StringBuilder AppendLine(this StringBuilder sb, string line, params object[] args)
        {
            return sb.AppendLine(string.Format(line, args));
        }
    }
}