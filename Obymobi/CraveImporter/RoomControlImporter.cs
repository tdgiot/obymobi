﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Text;
using Google.Apis.Util;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace CraveImporter
{
    public class RoomControlImporter
    {
        private int CompanyId { get; set; }
        private string ImportFile { get; set; }
        private int DeliverypointNumberColumn { get; set; }
        private int RoomConfigColumn { get; set; }
        private int RoomControllerIpColumn { get; set; }
        private RoomControlType RoomControlType { get; set; }

        public RoomControlImporter()
        {
            this.CompanyId = 0;
            this.ImportFile = string.Empty;
            this.DeliverypointNumberColumn = 0;
            this.RoomConfigColumn = 1;
            this.RoomControllerIpColumn = 2;
            this.RoomControlType = RoomControlType.None;
        }

        public void ShowMenu()
        {
            Console.Clear();

            StringBuilder sb = new StringBuilder();
            sb.AppendLine("----------------------------------");
            sb.AppendLine("-- Import Room Control Settings");
            sb.AppendLine("----------------------------------");
            sb.AppendLine("[1] Company Id: {0}", this.CompanyId);
            sb.AppendLine("[2] Import File: {0}", this.ImportFile);
            sb.AppendLine("[3] DeliverypointNumber Column: {0}", this.DeliverypointNumberColumn);
            sb.AppendLine("[4] Room Control Config Column: {0}", this.RoomConfigColumn);
            sb.AppendLine("[5] Room Controller IP Column: {0}", this.RoomControllerIpColumn);
            sb.AppendLine("[6] Room Control Type: {0}", this.RoomControlType);
            sb.AppendLine();
            sb.AppendLine("[9] Start Import");
            sb.AppendLine("[0] Return");
            sb.AppendLine();

            Console.Write(sb);

            Console.Write("Action: ");
            string input = Console.ReadLine();

            switch (input)
            {
                case "1":
                    SetCompanyId();
                    break;
                case "2":
                    SetImportFile();
                    break;
                case "3":
                    SetDeliverypointNumberColumn();
                    break;
                case "4":
                    SetRoomControlConfigColumn();
                    break;
                case "5":
                    SetRoomControllerIpColumn();
                    break;
                case "9":
                    StartImport();
                    break;
                case "0":
                    return;
            }

            ShowMenu();
        }

        void SetCompanyId()
        {
            Console.WriteLine();
            Console.Write("Company Id ({0}): ", this.CompanyId);
            
            string input = Console.ReadLine();
            if (input.IsNotNullOrEmpty())
            {
                int inputInt;
                if (!int.TryParse(input, out inputInt))
                {
                    SetCompanyId();
                    return;
                }

                if (inputInt > 0)
                {
                    this.CompanyId = inputInt;
                }
            }
        }

        void SetImportFile()
        {
            Console.WriteLine();
            Console.WriteLine("Import File ({0}):", this.ImportFile);
            
            string input = Console.ReadLine();
            if (!string.IsNullOrEmpty(input))
            {
                input = input.Replace("\"", "");
                if (!input.EndsWith(".csv") || !File.Exists(input))
                {
                    Console.WriteLine("Error: File does not exists or not CSV");
                    SetImportFile();
                    return;
                }

                this.ImportFile = input;
            }
        }

        void SetDeliverypointNumberColumn()
        {
            Console.WriteLine();
            Console.Write("DeliverypointNumber Column ({0}): ", this.DeliverypointNumberColumn);

            string input = Console.ReadLine();
            if (input.IsNotNullOrEmpty())
            {
                int inputInt;
                if (!int.TryParse(input, out inputInt))
                {
                    SetDeliverypointNumberColumn();
                    return;
                }

                this.DeliverypointNumberColumn = inputInt;    
            }
        }

        void SetRoomControlConfigColumn()
        {
            Console.WriteLine();
            Console.Write("Room Control Config Column ({0}): ", this.RoomConfigColumn);

            string input = Console.ReadLine();
            if (input.IsNotNullOrEmpty())
            {
                int inputInt;
                if (!int.TryParse(input, out inputInt))
                {
                    SetRoomControlConfigColumn();
                    return;
                }

                this.RoomConfigColumn = inputInt;
            }
        }

        void SetRoomControllerIpColumn()
        {
            Console.WriteLine();
            Console.Write("Room Controller Ip Column ({0}): ", this.RoomControllerIpColumn);

            string input = Console.ReadLine();
            if (input.IsNotNullOrEmpty())
            {
                int inputInt;
                if (!int.TryParse(input, out inputInt))
                {
                    SetRoomControllerIpColumn();
                    return;
                }

                this.RoomControllerIpColumn = inputInt;
            }
        }

        void StartImport()
        {
            if (!Validate())
            {
                Console.ReadLine();
                return;
            }

            IEnumerable<string> fileLines;
            try
            {
                fileLines = File.ReadLines(this.ImportFile);
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex.Message);
                Console.ReadLine();
                return;
            }

            // Load all deliverypoints
            PredicateExpression filter = new PredicateExpression();
            filter.Add(DeliverypointFields.CompanyId == this.CompanyId);

            DeliverypointCollection deliverypointCollection = new DeliverypointCollection();
            deliverypointCollection.GetMulti(filter);

            // Load all room control configuration
            PredicateExpression roomControlConfigFilter = new PredicateExpression();
            roomControlConfigFilter.Add(RoomControlConfigurationFields.CompanyId == this.CompanyId);

            IncludeFieldsList roomControlConfigFieldsList = new IncludeFieldsList();
            roomControlConfigFieldsList.Add(RoomControlConfigurationFields.RoomControlConfigurationId);
            roomControlConfigFieldsList.Add(RoomControlConfigurationFields.CompanyId);
            roomControlConfigFieldsList.Add(RoomControlConfigurationFields.Name);

            RoomControlConfigurationCollection configCollection = new RoomControlConfigurationCollection();
            configCollection.GetMulti(roomControlConfigFilter, roomControlConfigFieldsList, null);

            // Process file
            int okCount = 0;
            int errorCount = 0;

            foreach (string line in fileLines)
            {
                string[] split = line.Split(',');

                string deliverypointNumber = split[this.DeliverypointNumberColumn].Trim();
                
                string roomConfig = null;
                if (this.RoomConfigColumn >= 0)
                    roomConfig = split[this.RoomConfigColumn].Trim();

                string roomControllerIp = null;
                if (this.RoomControllerIpColumn >= 0)
                    roomControllerIp = split[this.RoomControllerIpColumn].Trim();

                string baseMessage = string.Format("Number: {0} | Config: {1} | ", deliverypointNumber, roomConfig);

                // Check if DP exists
                PredicateExpression numberFilter = new PredicateExpression();
                numberFilter.Add(DeliverypointFields.Number == deliverypointNumber);

                EntityView<DeliverypointEntity> deliverypointView = deliverypointCollection.DefaultView;
                deliverypointView.Filter = numberFilter;

                if (deliverypointView.Count == 0)
                {
                    Trace.WriteLine(string.Format("{0}DP with number {1} does not exists.", baseMessage, deliverypointNumber));
                    errorCount++;

                    continue;
                }

                List<int> duplicateDpgList = new List<int>();
                List<int> deliverypointgroupList = new List<int>();
                foreach (DeliverypointEntity entity in deliverypointView)
                {
                    if (deliverypointgroupList.Contains(entity.DeliverypointgroupId))
                    {                        
                        Trace.WriteLine(string.Format("{0}Multiple DP exist with number {1} for DPG {2}. Skipping.", baseMessage, deliverypointNumber, entity.DeliverypointgroupId));

                        duplicateDpgList.Add(entity.DeliverypointgroupId);
                        errorCount++;

                        continue;
                    }

                    deliverypointgroupList.Add(entity.DeliverypointgroupId);
                }

                foreach (DeliverypointEntity entity in deliverypointView)
                {
                    if (duplicateDpgList.Contains(entity.DeliverypointgroupId))
                    {
                        continue;
                    }

                    if (roomConfig != null)
                    {
                        // Check if room control config exists
                        PredicateExpression roomConfigFilter = new PredicateExpression();
                        roomConfigFilter.Add(RoomControlConfigurationFields.Name == roomConfig);

                        EntityView<RoomControlConfigurationEntity> roomControlConfigView = configCollection.DefaultView;
                        roomControlConfigView.Filter = roomConfigFilter;

                        if (roomControlConfigView.Count != 1)
                        {
                            Trace.WriteLine(string.Format("{0}Room Control Config with name {1} does not exists.", baseMessage, roomConfig));
                            errorCount++;

                            continue;
                        }

                        entity.RoomControlConfigurationId = roomControlConfigView[0].RoomControlConfigurationId;
                    }


                    if (this.RoomControlType != RoomControlType.None)
                        entity.RoomControllerType = this.RoomControlType;

                    if (roomControllerIp != null)
                        entity.RoomControllerIp = roomControllerIp;

                    if (!entity.Save())
                    {
                        Trace.WriteLine("{0}Failed to save entity!", baseMessage);
                        errorCount++;

                        continue;
                    }

                    okCount++;
                }
            }

            Trace.WriteLine("");
            Trace.WriteLine(string.Format("Finished importing - OK: {0}, Error: {1} - Press enter to continue", okCount, errorCount));

            Console.ReadLine();
        }

        bool Validate()
        {
            Trace.WriteLine("--------------------------------------------------");
            Trace.WriteLine(DateTime.Now.ToString(CultureInfo.InvariantCulture));

            if (this.CompanyId <= 0)
            {
                Trace.WriteLine("Company ID has to be bigger than 0");
                return false;
            }


            if (this.DeliverypointNumberColumn < 0)
            {
                Trace.WriteLine("Deliverypoint column has to be set");
                return false;
            }
            
            IncludeFieldsList includeFields = new IncludeFieldsList(CompanyFields.Name);
            PredicateExpression companyFilter = new PredicateExpression(CompanyFields.CompanyId == this.CompanyId);

            CompanyCollection companyCollection = new CompanyCollection();
            companyCollection.GetMulti(companyFilter, includeFields, null);

            if (companyCollection.Count == 0)
            {
                Trace.WriteLine(string.Format("Company with ID {0} does not exists.", this.CompanyId));
                return false;
            }    
            Trace.WriteLine(string.Format("Company: {0}", companyCollection[0].Name));
            
            if (!File.Exists(this.ImportFile))
            {
                Trace.WriteLine(string.Format("Import file ({0}) does not exists", this.ImportFile));
                return false;
            }
            Trace.WriteLine(string.Format("Importing from: {0}", this.ImportFile));

            return true;
        }
    }
}