﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.Security.Cryptography;
using System.IO.Compression;
using System.IO;
using Dionysos;
using Dionysos.Web;
using System.Reflection;
using Obymobi.Attributes;
using System.Globalization;
using util;

namespace Crave.Web.Mobile
{
    /// <summary>
    /// Summary description for DynamicDataHandler
    /// </summary>
    public abstract class JavascriptDataHandler : IHttpHandler
    {

        #region Fields

        private StringBuilder javascript = new StringBuilder();
        private Dictionary<string, PropertyInfo> primaryKeysPerModel = new Dictionary<string, PropertyInfo>();
        private Dictionary<string, List<PropertyInfo>> propertiesToExportPerModel = new Dictionary<string, List<PropertyInfo>>();
        private string encoding, hash;
        private HttpContext context;
        private List<string> fileNames = new List<string>();

        #endregion

        #region Properties

        /// <summary>
        /// Gets the cache duration.
        /// </summary>
        public TimeSpan CacheDuration
        {
            get
            {
                return TimeSpan.FromMinutes(5); // 5 minutes
            }
        }

        /// <summary>
        /// Gets the expiry duration.
        /// </summary>
        public TimeSpan ExpiryDuration
        {
            get
            {
                return TimeSpan.FromDays(365.25); // 1 year
            }
        }

        #endregion

        #region IHttpHandler Members

        public bool IsReusable
        {
            get
            {
                return true;
            }
        }

        /// <summary>
        /// Render the request javascript to the this.javascript StringBuilder
        /// </summary>
        /// <returns></returns>
        public abstract bool RenderJavascript();

        public void ProcessRequest(HttpContext httpContext)
        {
            context = httpContext;

            // Create a string for the cache
            string cacheKey = context.Request.Url.AbsoluteUri;

            StringBuilder sb = new StringBuilder();

            // Get the accepted encoding (compression)
            this.encoding = "none";
            this.SetEncoding();

            // Set content type        
            string contentType = "application/javascript";
            context.Response.ContentType = contentType;

            // create the hash used for the etag
            hash = this.GetMd5Sum(cacheKey);
            bool usesVersioning = !String.IsNullOrEmpty(context.Request.QueryString["version"]);

            // Only add caching if version parameter is added
            if (usesVersioning)
            {
                // Set etag
                context.Response.Cache.SetETag(hash);

                // Set max age
                TimeSpan expiryDuration = this.ExpiryDuration;
                context.Response.Cache.SetMaxAge(expiryDuration);

                // Set expiration
                DateTime expirationDate = DateTime.Now.Add(expiryDuration);
                context.Response.Cache.SetExpires(expirationDate);

                // Set revalidation
                context.Response.Cache.SetRevalidation(HttpCacheRevalidation.AllCaches);

                // Set cache control
                context.Response.Cache.SetCacheability(HttpCacheability.Public);
            }

            Uri baseUri = new Uri(context.Request.Url.AbsoluteUri);

            // If cached on the browser, nothing more to do.
            if (IsCachedOnBrowser())
                return;

            // Here we read the data from the file cache, or retrieve it.
            string cachedFileName = HttpContext.Current.Server.MapPath(string.Format("~/Temp/{0}.cache", hash));
            string data = string.Empty;

            // TODO: PROPER CACHING TO FILE!
            // TODO: PROPER CACHING TO FILE!
            // TODO: PROPER CACHING TO FILE!
            // TODO: PROPER CACHING TO FILE!
            if (usesVersioning && CacheHelper.TryGetValue(hash, false, out data))
            {
                // From memory cache
                bool bullshit = TestUtil.IsPcGabriel;
            }
            else if (usesVersioning && File.Exists(cachedFileName))
            {
                data = File.ReadAllText(cachedFileName);
                CacheHelper.AddSlidingExpire(false, hash, data, 120);
            }
            else
            {
                try
                {
                    if (this.RenderJavascript())
                    {
                        data = this.javascript.ToString();
                        data = MyMin.parse(data);
                        CacheHelper.AddSlidingExpire(false, hash, data, 120);
                        File.WriteAllText(cachedFileName, data);
                    }
                    else
                    {
                        // Cancel cache of error response
                        context.Response.Cache.SetExpires(DateTime.Now.AddYears(-1));
                        context.Response.Cache.SetCacheability(HttpCacheability.NoCache);
                        context.Response.StatusCode = 400;
                    }
                }
                catch (Exception ex)
                {
                    // Cancel cache of error response
                    context.Response.Cache.SetExpires(DateTime.Now.AddYears(-1));
                    context.Response.Cache.SetCacheability(HttpCacheability.NoCache);

                    ErrorLoggerWeb.LogError(ex, ex.Message);
                    context.Response.Write("Hmmm, something went wrong. Of course that should never happen, but it just did. More details can be found in the error log.");

                    if (TestUtil.IsPcDeveloper)
                        context.Response.Write(ex.ToString());

                    context.Response.StatusCode = 500;
                }
            }

            // Write contents        
            context.Response.Write(data);

            // If compression is accepted compress the output
            if (encoding != "none")
            {
                if (encoding == "gzip")
                    context.Response.Filter = new GZipStream(context.Response.Filter, CompressionMode.Compress);
                else
                    context.Response.Filter = new DeflateStream(context.Response.Filter, CompressionMode.Compress);

                context.Response.AppendHeader("Content-Encoding", encoding);
                context.Response.AppendHeader("Vary", "Accept-Encoding");
            }
        }

        #endregion

        #region Methods

        private void SetEncoding()
        {
            bool gzip, deflate;

            // get the type of compression to use
            if (!string.IsNullOrEmpty(context.Request.ServerVariables["HTTP_ACCEPT_ENCODING"]))
            {
                // get supported compression methods
                string acceptedTypes = context.Request.ServerVariables["HTTP_ACCEPT_ENCODING"].ToLower();
                gzip = acceptedTypes.Contains("gzip") || acceptedTypes.Contains("x-gzip") || acceptedTypes.Contains("*");
                deflate = acceptedTypes.Contains("deflate");
            }
            else
                gzip = deflate = false;

            //determin which to use
            encoding = gzip ? "gzip" : (deflate ? "deflate" : "none");

            // check for buggy versions of Internet Explorer
            if (context.Request.Browser.Browser == "IE")
            {
                if (context.Request.Browser.MajorVersion < 6)
                    encoding = "none";
                else if (context.Request.Browser.MajorVersion == 6 &&
                    !string.IsNullOrEmpty(context.Request.ServerVariables["HTTP_USER_AGENT"]) &&
                    context.Request.ServerVariables["HTTP_USER_AGENT"].Contains("EV1"))
                    encoding = "none";
            }
        }

        private bool IsCachedOnBrowser()
        {
            // check if the requesting browser sent an etag
            if (!string.IsNullOrEmpty(context.Request.ServerVariables["HTTP_IF_NONE_MATCH"]) &&
                context.Request.ServerVariables["HTTP_IF_NONE_MATCH"].Equals(hash))
            {
                context.Response.ClearHeaders();
                context.Response.AppendHeader("Etag", hash);
                context.Response.Status = "304 Not Modified";
                context.Response.AppendHeader("Content-Length", "0");
                return true;
            }
            return false;
        }

        private string GetMd5Sum(string str)
        {
            // First we need to convert the string into bytes, which
            // means using a text encoder.
            Encoder enc = System.Text.Encoding.Unicode.GetEncoder();

            // Create a buffer large enough to hold the string
            byte[] unicodeText = new byte[str.Length * 2];
            enc.GetBytes(str.ToCharArray(), 0, str.Length, unicodeText, 0, true);

            // Now that we have a byte array we can ask the CSP to hash it
            MD5 md5 = new MD5CryptoServiceProvider();
            byte[] result = md5.ComputeHash(unicodeText);

            // Build the final string by converting each byte
            // into hex and appending it to a StringBuilder
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < result.Length; i++)
            {
                sb.Append(result[i].ToString("X2"));
            }

            // And return it
            return sb.ToString();
        }

        #endregion

        #region Helper Methods

        protected void AppendJavascript(string format, params object[] args)
        {
            this.javascript.AppendFormatLine(format, args);
        }

        protected void WriteModelToJavascript(string variablePrefix, Object model)
        {
            string modelName = model.GetType().Name;

            if (!this.primaryKeysPerModel.ContainsKey(modelName))
                this.GatherModelMetaData(model);

            // Retrieve th pk field data
            var pkFieldProperty = this.primaryKeysPerModel[modelName];
            var pkFieldValue = pkFieldProperty.GetValue(model, null).ToString();
            variablePrefix += pkFieldValue;

            // Write the variable declaration
            this.AppendJavascript("{0}= {{}};", variablePrefix);
            this.AppendJavascript("{0}.{1} = {2};", variablePrefix, pkFieldProperty.Name, pkFieldValue);

            foreach (var property in this.propertiesToExportPerModel[modelName])
            {
                var propertyType = property.PropertyType;
                if (property.GetValue(model, null) == null || property.GetValue(model, null).ToString().IsNullOrWhiteSpace())
                {
                    // Don't export empty properties
                }
                else if (property.PropertyType == typeof(string))
                {
                    this.AppendJavascript("{0}.{1}={2};", variablePrefix, property.Name, this.EncodeJsString((string)property.GetValue(model, null)));
                }
                else if (property.PropertyType == typeof(decimal))
                {
                    this.AppendJavascript("{0}.{1}={2};", variablePrefix, property.Name, ((decimal)property.GetValue(model, null)).ToString(CultureInfo.InvariantCulture));
                }
                else if (property.PropertyType == typeof(DateTime))
                {
                    this.AppendJavascript("{0}.{1}={2};", variablePrefix, property.Name, this.EncodeJsString(((DateTime)property.GetValue(model, null)).ToShortDateString()));
                }
                else if (property.PropertyType == typeof(DateTime?))
                {
                    this.AppendJavascript("{0}.{1}={2};", variablePrefix, property.Name, this.EncodeJsString(((DateTime)property.GetValue(model, null)).ToShortDateString()));
                }
                else if (propertyType.IsGenericType && propertyType.GetGenericTypeDefinition() == typeof(Nullable<>))
                {
                    string value = property.GetValue(model, null).ToString();
                    this.AppendJavascript("{0}.{1}='{2}';", variablePrefix, property.Name, value.Replace(',', '.'));
                }
                else if (property.PropertyType == typeof(Boolean))
                {
                    this.AppendJavascript("{0}.{1}={2};", variablePrefix, property.Name, property.GetValue(model, null).ToString().ToLower());
                }
                else if (property.PropertyType == typeof(int[]))
                {
                    this.AppendJavascript("{0}.{1}={2};", variablePrefix, property.Name, IntArrayString((int[])property.GetValue(model, null)));
                }
                else
                {
                    this.AppendJavascript("{0}.{1}={2};", variablePrefix, property.Name, property.GetValue(model, null));
                }
            }
        }

        protected void GatherModelMetaData(Object model)
        {
            string modelName = model.GetType().Name;

            // Check if we haven't retrieved the meta data already
            if (this.primaryKeysPerModel.ContainsKey(modelName))
                return;

            PropertyInfo[] properties = Dionysos.Reflection.Member.GetPropertyInfo(model);
            for (int j = 0; j < properties.Length; j++)
            {
                PropertyInfo property = properties[j];

                // Check whether the property has a ExcludeFromCodeGeneratorToMobile
                if (property.GetCustomAttributes(typeof(PrimaryKeyFieldOfModel), false).Length > 0)
                {
                    this.primaryKeysPerModel.Add(modelName, property);
                }
                else if (property.GetCustomAttributes(typeof(IncludeInCodeGeneratorForJavascript), false).Length > 0)
                {
                    if (!this.propertiesToExportPerModel.ContainsKey(modelName))
                    {
                        this.propertiesToExportPerModel.Add(modelName, new List<PropertyInfo>());
                    }

                    this.propertiesToExportPerModel[modelName].Add(property);
                }
            }

            if (!this.primaryKeysPerModel.ContainsKey(modelName))
                throw new Dionysos.TechnicalException("Model: {0} is missing a Property that's marked as Primary Key", modelName);
        }

        /// <summary>
        /// Encodes a string to be represented as a string literal. The format
        /// is essentially a JSON string.
        /// 
        /// The string returned includes outer quotes 
        /// Example Output: "Hello \"Rick\"!\r\nRock on"
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public string EncodeJsString(string s)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("\"");
            foreach (char c in s)
            {
                switch (c)
                {
                    case '\"':
                        sb.Append("\\\"");
                        break;
                    case '\\':
                        sb.Append("\\\\");
                        break;
                    case '\b':
                        sb.Append("\\b");
                        break;
                    case '\f':
                        sb.Append("\\f");
                        break;
                    case '\n':
                        sb.Append("\\n");
                        break;
                    case '\r':
                        sb.Append("\\r");
                        break;
                    case '\t':
                        sb.Append("\\t");
                        break;
                    default:
                        int i = (int)c;
                        if (i < 32 || i > 127)
                        {
                            sb.AppendFormat("\\u{0:X04}", i);
                        }
                        else
                        {
                            sb.Append(c);
                        }
                        break;
                }
            }
            sb.Append("\"");

            return sb.ToString();
        }


        private static string Clean(string str)
        {
            str = str.HtmlEncode();
            str = str.Replace("'", "");
            str = str.Replace("\r\n", "");

            return str;
        }

        private static string IntArrayString(int[] arr)
        {
            String str = "[";
            foreach (int item in arr)
            {
                if (!str.Equals("["))
                    str += ", ";

                str += item;
            }
            str += "]";
            return str;
        }

        #endregion

    }
}