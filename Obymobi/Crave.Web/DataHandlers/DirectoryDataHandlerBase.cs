﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Crave.Web.Mobile;
using Obymobi.Logic.Model;
using Obymobi.Logic.HelperClasses;

namespace Crave.Web.DataHandlers
{
    public class DirectoryDataHandlerBase : JavascriptDataHandler
    {
        public override bool RenderJavascript()
        {
            bool toReturn = false;

            // Retrieve all companies
            var companies = CompanyHelper.GetCompanyDirectoryEntryArray(-1);

            // Open the namespace
            this.AppendJavascript("(function (DirectoryData, $, undefined) {{");
            this.AppendJavascript("Jog.debug(\"DirectoryData\", \"class\", \"Init start \")");

            this.AppendJavascript("DirectoryData.companies = [];");

            this.WriteCompanies(companies);

            // Close the namespace
            this.AppendJavascript("}} ( window.DirectoryData = window.DirectoryData || {{}}, jQuery))");
           
            toReturn = true;

            return toReturn;
        }

        public void WriteCompanies(CompanyDirectoryEntry[] companies)
        {
            foreach (var company in companies)
            {
                this.WriteModelToJavascript("DirectoryData.company", company);

                this.AppendJavascript("DirectoryData.company{0}.deliverypointgroups=[];", company.CompanyId);
                foreach (var deliverypointgroup in company.Deliverypointgroups)
                {
                    this.AppendJavascript("DirectoryData.deliverypointgroup{0}={{}};", deliverypointgroup.DeliverypointgroupId);
                    this.AppendJavascript("DirectoryData.deliverypointgroup{0}.DeliverypointgroupId={1};", deliverypointgroup.DeliverypointgroupId, deliverypointgroup.DeliverypointgroupId);
                    this.AppendJavascript("DirectoryData.deliverypointgroup{0}.Name={1};", deliverypointgroup.DeliverypointgroupId, this.EncodeJsString(deliverypointgroup.Name));
                    this.AppendJavascript("DirectoryData.company{0}.deliverypointgroups.push(DirectoryData.deliverypointgroup{1});", company.CompanyId, deliverypointgroup.DeliverypointgroupId);
                }

                // Append to a array so we can iterate
                this.AppendJavascript("DirectoryData.companies.push(DirectoryData.company{0});", company.CompanyId);
            }


        }

    }
}
