﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using Crave.Web.Mobile;
using Dionysos;
using Obymobi.Logic.Model;
using System.Web;
using Obymobi.Data.EntityClasses;
using Obymobi.Logic.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data;
using Obymobi.Enums;
using Dionysos.Web;
using System.IO;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.HelperClasses;

namespace Crave.Web.DataHandlers
{
    public class CompanyDataHandlerBase : JavascriptDataHandler
    {
        public enum CompanyDataHandlerBaseResult : int
        {
            MenuIsEmpty = 200,
        }

        private int companyId = -1;
        private int terminalId = -1;
        private int deliverypointgroupId = -1;
        private bool isConsoleRequest = false;

        private List<Category> menu;
        private List<int> writtenCategoryIds = new List<int>();
        private List<int> writtenProductIds = new List<int>();
        private List<int> writtenAlterationIds = new List<int>();
        private List<int> writtenAlterationOptionIds = new List<int>();
        private List<int> writtenSurveyIds = new List<int>();
        private List<int> writtenSurveyQuestionIds = new List<int>();
        private List<int> writtenSurveyAnwserIds = new List<int>();
        private List<int> writtenDeliverypointIds = new List<int>();
        private List<int> writtenSuggestionIds = new List<int>();
        private List<int> writtenClientIds = new List<int>();

        public override bool RenderJavascript()
        {
            bool toReturn = false;

            // Check if a CompanyId was supplied        
            if (!Dionysos.Web.QueryStringHelper.TryGetValue("c", out this.companyId))
            {
                // Missing CompanyId
                return false;
            }

            // Check if a TerminalId was supplied or deliverypointgroupId
            if (!Dionysos.Web.QueryStringHelper.TryGetValue("t", out this.terminalId) && !Dionysos.Web.QueryStringHelper.TryGetValue("deliverypointgroup", out this.deliverypointgroupId))
            {
                // Missing TerminalId and deliverypointgroupId
                return false;
            }

            // Check if it's a console request
            Dionysos.Web.QueryStringHelper.TryGetValue("console", out this.isConsoleRequest);

            // Check if company exists            
            PrefetchPath companyPath = new PrefetchPath(EntityType.CompanyEntity);
            companyPath.Add(CompanyEntity.PrefetchPathMediaCollection);
            companyPath.Add(CompanyEntity.PrefetchPathSurveyCollection);
            companyPath.Add(CompanyEntity.PrefetchPathDeliverypointCollection);
            CompanyEntity c = new CompanyEntity(this.companyId, companyPath);
            if (c.IsNew)
            {
                // Company is non existent
                return false;
            }

            // Open the namespace
            this.AppendJavascript("(function (CompanyData, $, undefined) {{");
            this.AppendJavascript("Jog.debug(\"CompanyData\", \"class\", \"Init start \");");

            // Handle Company Details
            var company = CompanyHelper.GetCompanyByCompanyId(this.companyId);
            this.WriteCompany(company);

            // Handle Company Features
            if (c.CompanyFeatureCollection.Count > 0)
            {
                this.AppendJavascript("CompanyData.features = [];");

                var features = FeatureHelper.GetFeatures(companyId);

                foreach (var feature in features)
                    this.WriteFeature(feature);
            }

            // Handle terminal details
            this.AppendJavascript("CompanyData.terminal = {{}};");

            // ES: temporary (?) fix to make mobile work
            if (this.terminalId <= 0)
            {
                var terminals = TerminalHelper.GetTerminalArrayByCompanyId(companyId);
                if (terminals.Length > 0)
                {
                    this.terminalId = terminals[0].TerminalId;
                }
            }

            if (this.terminalId > 0)
            {
                var terminals = TerminalHelper.GetTerminalArrayByTerminalId(this.terminalId);
                if (terminals.Length > 0)
                {
                    Terminal terminal = terminals[0];

                    // DANGER! THESE HAVE TO START WITH CAPS!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!                    
                    this.AppendJavascript("CompanyData.terminal.TerminalId = {0};", terminal.TerminalId);
                    this.AppendJavascript("CompanyData.terminal.TerminalName = {0};", this.EncodeJsString(terminal.Name));
                    this.AppendJavascript("CompanyData.terminal.HandlingMethod = {0};", terminal.HandlingMethod);
                    this.AppendJavascript("CompanyData.terminal.ServiceSuspended = {0};", (terminal.SuspendService) ? "true" : "false");
                    this.AppendJavascript("CompanyData.terminal.NotificationForNewOrder = {0};", terminal.NotificationForNewOrder);
                    this.AppendJavascript("CompanyData.terminal.NotificationForOverdueOrder = {0};", terminal.NotificationForOverdueOrder);
                    this.AppendJavascript("CompanyData.terminal.MaxProcessTime = {0};", terminal.MaxProcessTime);

                    // GK Fall back to a default, but should be removed.
                    this.AppendJavascript("CompanyData.terminal.DeliverypointCaption = \"Deliverypoint\";");//, this.EncodeJsString(terminal.DeliverypointCaption));
                }
            }

            // Handle Discount
            if (c.DiscountCollection.Count > 0)
            {
                this.AppendJavascript("CompanyData.discounts = [];");

                var discounts = DiscountHelper.GetDiscounts(companyId);

                foreach (var discount in discounts)
                    this.WriteDiscount(discount);
            }

            // Handle Surveys
            if (c.SurveyCollection.Count > 0)
            {
                this.AppendJavascript("CompanyData.surveys = [];");

                var surveys = SurveyHelper.GetSurveys(companyId, null);

                foreach (var survey in surveys)
                    this.WriteSurvey(survey);
            }

            // Handle Deliverypoints
            if (c.DeliverypointCollection.Count > 0)
            {
                this.AppendJavascript("CompanyData.deliverypoints = [];");

                var deliverypoints = DeliverypointHelper.GetDeliverypoints(companyId, this.terminalId, false, 0);

                foreach (var deliverypoint in deliverypoints)
                    this.WriteDeliverypoint(deliverypoint);
            }

            // Handle Payment Methods
            {
                this.AppendJavascript("CompanyData.paymentmethods = [];");

                var paymentMethods = PaymentmethodHelper.GetPaymentmethods(companyId, null);

                foreach (var paymentMethod in paymentMethods)
                    this.WritePaymentmethod(paymentMethod);
            }

            // Handle service requests 
            this.AppendJavascript("CompanyData.servicerequests = [];");

            Product[] serviceRequests = ProductHelper.GetServiceProducts(companyId, null, false, null);

            foreach (var serviceRequest in serviceRequests)
            {
                this.WriteServiceRequest(serviceRequest);
            }

            // Handle business hours
            if (c.BusinesshoursCollection.Count > 0)
            {
                this.AppendJavascript("CompanyData.businesshours = [];");

                var businesshours = BusinesshoursHelper.GetBusinesshours(companyId, null);

                foreach (var hours in businesshours)
                    this.WriteBusinesshours(hours);
            }

            // Handle the Menu
            if (TestUtil.IsPcGabriel && DateTime.Now.Date <= new DateTime(2012, 01, 05))
            {
                if (HttpContext.Current.Cache["menuVoorGabriel" + this.companyId] == null)
                {
                    HttpContext.Current.Cache["menuVoorGabriel" + this.companyId] = MenuHelper.GetNestedMenu(this.companyId, null, false, null, true);
                }

                this.menu = (List<Category>)HttpContext.Current.Cache["menuVoorGabriel" + this.companyId];
            }
            else
            {
                int? deliverypointgroup = null;
                if (this.deliverypointgroupId > 0)
                    deliverypointgroup = this.deliverypointgroupId;

                this.menu = MenuHelper.GetNestedMenu(this.companyId, deliverypointgroup, false, null, true);
            }

            System.Text.StringBuilder sb = new System.Text.StringBuilder();

            if (this.menu != null && menu.Count > 0)
            {
                // Render the arrays
                this.AppendJavascript("CompanyData.categories = [];");
                this.AppendJavascript("CompanyData.products = [];");
                this.AppendJavascript("CompanyData.alterations = [];");
                this.AppendJavascript("CompanyData.media = [];");
                this.AppendJavascript("CompanyData.suggestions = [];");

                // Write the categories
                this.WriteCategories();

                this.AppendJavascript("Jog.debug(\"CompanyData\", \"class\", \"Init finish \")");

                // Original:
                // this.AppendJavascript("}( window.CompanyData = window.CompanyData || {}, jQuery));
                // GK Approach, ALWAYS reset the full object when reloaded

                toReturn = true;
            }
            else
            {
                throw new Obymobi.ObymobiException(CompanyDataHandlerBaseResult.MenuIsEmpty);
            }

            // Get message templates
            var messageTemplates = MessageHelper.GetMessageTemplatesForCompany(this.companyId);
            this.AppendJavascript("CompanyData.messageTemplates = new Array({0});", messageTemplates.Length);
            int arrayIndex = 0;
            foreach (MessageTemplate messageTemplate in messageTemplates)
            {
                this.AppendJavascript("CompanyData.messageTemplates[{0}] = {{}};", arrayIndex);
                this.AppendJavascript("CompanyData.messageTemplates[{0}].id = {1};", arrayIndex, messageTemplate.MessageTemplateId);
                this.AppendJavascript("CompanyData.messageTemplates[{0}].title = {1};", arrayIndex, this.EncodeJsString(messageTemplate.Title));
                this.AppendJavascript("CompanyData.messageTemplates[{0}].message = {1};", arrayIndex, this.EncodeJsString(messageTemplate.Message));
                arrayIndex++;
            }

            // Write deliverypointgroup settings
            this.AppendJavascript("CompanyData.settings = {{}};");
            DeliverypointgroupCollection deliverypointGroups = new DeliverypointgroupCollection();
            deliverypointGroups.GetMulti(new PredicateExpression(DeliverypointgroupFields.CompanyId == this.companyId));
            if (deliverypointGroups.Count > 0)
            {
                // Use the first
                DeliverypointgroupEntity deliverypointGroup = deliverypointGroups[0];

                this.AppendJavascript("CompanyData.settings.OrderProcessingNotificationEnabled = {0};", deliverypointGroup.OrderProcessingNotificationEnabled ? "true" : "false");
                if (deliverypointGroup.OrderProcessingNotificationEnabled)
                {
                    this.AppendJavascript("CompanyData.settings.OrderProcessingNotification = {0};", this.EncodeJsString(deliverypointGroup.OrderProcessingNotification));
                }

                this.AppendJavascript("CompanyData.settings.OrderProcessedNotificationEnabled = {0};", deliverypointGroup.OrderProcessedNotificationEnabled ? "true" : "false");
                if (deliverypointGroup.OrderProcessedNotificationEnabled)
                {
                    this.AppendJavascript("CompanyData.settings.OrderProcessedNotification = {0};", this.EncodeJsString(deliverypointGroup.OrderProcessedNotification));
                }

                if (deliverypointGroup.Pincode != 0)
                {
                    string pincode = PasswordHashUtil.GetPasswordHash(deliverypointGroup.Pincode + "-crave-sauce");
                    this.AppendJavascript("CompanyData.settings.Pincode = '{0}';", pincode);
                }

                if (deliverypointGroup.PincodeGM != 0)
                {
                    string pincode = PasswordHashUtil.GetPasswordHash(deliverypointGroup.PincodeGM + "-crave-sauce");
                    this.AppendJavascript("CompanyData.settings.PincodeGM = '{0}';", pincode);
                }

                if (deliverypointGroup.PincodeSU != 0)
                {
                    string pincode = PasswordHashUtil.GetPasswordHash(deliverypointGroup.PincodeSU + "-crave-sauce");
                    this.AppendJavascript("CompanyData.settings.PincodeSU = '{0}';", pincode);
                }

                this.AppendJavascript("CompanyData.settings.PreorderingEnabled = {0};", deliverypointGroup.PreorderingEnabled ? "true" : "false");
                this.AppendJavascript("CompanyData.settings.PreorderingActiveMinutes = {0};", deliverypointGroup.PreorderingActiveMinutes);

                this.AppendJavascript("CompanyData.settings.AnalyticsOrderingVisible = {0};", deliverypointGroup.AnalyticsOrderingVisible ? "true" : "false");
                this.AppendJavascript("CompanyData.settings.AnalyticsBestsellersVisible = {0};", deliverypointGroup.AnalyticsBestsellersVisible ? "true" : "false");

                if (deliverypointGroup.MediaCollection.Count > 0)
                {
                    string relativeFilePath = string.Empty;
                    foreach (var mediaItem in deliverypointGroup.MediaCollection)
                    {
                        foreach (var mediaRatio in mediaItem.MediaRatioTypeMediaCollection)
                        {
                            if (mediaRatio.MediaTypeAsEnum == MediaType.CraveConsoleLogo)
                            {
                                relativeFilePath = string.Format("{0}/Deliverypointgroups/{1}", this.companyId, mediaRatio.FileName);
                                this.AppendJavascript("CompanyData.settings.ConsoleLogo=\"{0}\";", relativeFilePath);
                            }
                        }
                    }
                }
            }

            // Write company clients
            if (this.isConsoleRequest)
            {
                WriteClients();
            }

            // Close the namespace
            this.AppendJavascript("}} ( window.CompanyData = window.CompanyData || {{}}, jQuery))");

            return toReturn;
        }

        private void WriteClients()
        {
            this.AppendJavascript("CompanyData.clients = [];");
            Client[] clients = ClientHelper.GetClientsForCompany(this.companyId, this.terminalId);
            foreach (Client client in clients)
            {
                this.WriteModelToJavascript("CompanyData.client", client);

                if (!this.writtenClientIds.Contains(client.ClientId))
                {
                    this.AppendJavascript("CompanyData.clients.push(CompanyData.client{0});", client.ClientId);
                    this.writtenClientIds.Add(client.ClientId);
                }
            }
        }

        private void WriteCategories()
        {
            foreach (var category in this.menu)
            {
                if (!this.writtenCategoryIds.Contains(category.CategoryId))
                {
                    this.WriteCategory(category);
                    this.writtenCategoryIds.Add(category.CategoryId);
                }

            }
        }

        private void WriteCategory(Category category)
        {
            this.WriteModelToJavascript("CompanyData.category", category);

            if (category.ParentCategoryId == -1)
            {
                if (category.Media != null && category.Media.Length > 0)
                {
                    string absoluteCompanyPath = Path.Combine(Dionysos.Global.ApplicationInfo.BasePath, string.Format("Media\\{0}", this.companyId));
                    string relativeFilePath = string.Empty;
                    string absoluteFilePath = string.Empty;

                    foreach (var mediaItem in category.Media)
                    {
                        if (mediaItem.MediaType == MediaType.CategoryBranding800x480.ToInt())
                        {
                            // Branding image
                            absoluteFilePath = Path.Combine(absoluteCompanyPath, mediaItem.FilePathRelativeToMediaPath);
                            if (File.Exists(absoluteFilePath))
                            {
                                relativeFilePath = string.Format("{0}/{1}", this.companyId, mediaItem.FilePathRelativeToMediaPath.Replace("\\", "/"));
                                this.AppendJavascript("CompanyData.category{0}.BrandingImage=\"{1}\" ", category.CategoryId, relativeFilePath);
                            }
                        }
                        else if (mediaItem.MediaType == MediaType.CategoryPhoto800x480.ToInt())
                        {
                            // Photo image
                            absoluteFilePath = Path.Combine(absoluteCompanyPath, mediaItem.FilePathRelativeToMediaPath);
                            if (File.Exists(absoluteFilePath))
                            {
                                relativeFilePath = string.Format("{0}/{1}", this.companyId, mediaItem.FilePathRelativeToMediaPath.Replace("\\", "/"));
                                this.AppendJavascript("CompanyData.category{0}.PhotoImage=\"{1}\" ", category.CategoryId, relativeFilePath);
                            }
                        }
                    }
                }
            }

            // Determine what to render: Subcategories or Products
            if (category.Categories != null && category.Categories.Length > 0)
            {
                // Subcategories
                int subCategoryNumber = 0;
                this.AppendJavascript("CompanyData.category{0}.Categories=[{1}];", category.CategoryId, category.Categories.Length);

                foreach (var subCategory in category.Categories)
                {
                    // Recursively write this category
                    this.WriteCategory(subCategory);

                    // Add the category as subcategory
                    this.AppendJavascript("CompanyData.category{0}.Categories[{1}]=CompanyData.category{2};", category.CategoryId, subCategoryNumber, subCategory.CategoryId);
                    subCategoryNumber++;

                    if (!this.writtenCategoryIds.Contains(category.CategoryId))
                    {
                        this.AppendJavascript("CompanyData.categories.push(CompanyData.category{0});", category.CategoryId);
                        this.writtenCategoryIds.Add(category.CategoryId);
                    }
                }
            }
            else if (category.Products != null && category.Products.Length > 0)
            {
                // Products
                int productNumber = 0;
                this.AppendJavascript("CompanyData.category{0}.Products=new Array({1});", category.CategoryId, category.Products.Length);

                foreach (var product in category.Products)
                {
                    // Prevent writing a product twice (could happen when it appears in multiple categories
                    if (!this.writtenProductIds.Contains(product.ProductId))
                    {
                        this.WriteProduct(product);
                        this.writtenProductIds.Add(product.ProductId);
                    }

                    // Add the Product to the Category
                    this.AppendJavascript("CompanyData.category{0}.Products[{1}]=CompanyData.product{2};", category.CategoryId, productNumber, product.ProductId);
                    productNumber++;
                }
            }

            if (!this.writtenCategoryIds.Contains(category.CategoryId))
                this.AppendJavascript("CompanyData.categories.push(CompanyData.category{0});", category.CategoryId);
        }

        private void WriteProduct(Product product)
        {
            // Write the basic product model
            this.WriteModelToJavascript("CompanyData.product", product);

            // Ratings
            if (product.Ratings != null && product.Ratings.Length > 0)
            {
                int totalRating = 0;
                foreach (var rating in product.Ratings)
                {
                    totalRating += rating.Value;
                }
                double avgRating = totalRating / product.Ratings.Length;

                this.AppendJavascript("CompanyData.product{0}.Rating={1};", product.ProductId, avgRating);
            }

            // Alterations
            if (product.Alterations != null && product.Alterations.Length > 0)
            {
                this.AppendJavascript("CompanyData.product{0}.Alterations=[{1}];", product.ProductId, product.Alterations.Length);

                int alterationNumber = 0;
                foreach (var alteration in product.Alterations)
                {
                    // Prevent writing an Alteration twice
                    if (!this.writtenAlterationIds.Contains(alteration.AlterationId))
                    {
                        WriteAlteration(alteration);
                        this.writtenAlterationIds.Add(alteration.AlterationId);
                    }

                    // Add the Alteration to the Product
                    this.AppendJavascript("CompanyData.product{0}.Alterations[{1}]=CompanyData.alteration{2};", product.ProductId, alterationNumber, alteration.AlterationId);
                    alterationNumber++;
                }
            }

            // Suggestions
            if (product.ProductSuggestions != null && product.ProductSuggestions.Length > 0)
            {
                this.AppendJavascript("CompanyData.product{0}.suggestions=[{1}];", product.ProductId, product.ProductSuggestions.Length);

                int suggestionNumber = 0;
                foreach (var productSuggestion in product.ProductSuggestions)
                {
                    // Prevent writing a suggestion twice
                    if (!this.writtenSuggestionIds.Contains(productSuggestion.ProductSuggestionId))
                    {
                        WriteSuggestion(productSuggestion);
                        this.writtenSuggestionIds.Add(productSuggestion.ProductSuggestionId);
                    }

                    // Add the suggestion to the product
                    this.AppendJavascript("CompanyData.product{0}.suggestions[{1}]=CompanyData.suggestion{2};", product.ProductId, suggestionNumber, productSuggestion.ProductSuggestionId);
                    suggestionNumber++;
                }
            }

            // Media
            if (product.Media != null && product.Media.Length > 0)
            {
                string absoluteCompanyPath = Path.Combine(Dionysos.Global.ApplicationInfo.BasePath, string.Format("Media\\{0}", this.companyId));
                string relativeFilePath = string.Empty;
                string absoluteFilePath = string.Empty;

                foreach (var mediaItem in product.Media)
                {
                    if (mediaItem.MediaType == MediaType.ProductButton800x480.ToInt() || mediaItem.MediaType == MediaType.ProductButton.ToInt() || mediaItem.MediaType == MediaType.GenericproductButton.ToInt())
                    {
                        // Button image
                        absoluteFilePath = Path.Combine(absoluteCompanyPath, mediaItem.FilePathRelativeToMediaPath);
                        if (File.Exists(absoluteFilePath))
                        {
                            relativeFilePath = string.Format("{0}/{1}", this.companyId, mediaItem.FilePathRelativeToMediaPath.Replace("\\", "/"));
                            this.AppendJavascript("CompanyData.product{0}.ThumbnailImage=\"{1}\";", product.ProductId, relativeFilePath);
                        }
                    }
                    else if (mediaItem.MediaType == MediaType.CraveProductBrandingMobile.ToInt())
                    {
                        // Branding image
                        absoluteFilePath = Path.Combine(absoluteCompanyPath, mediaItem.FilePathRelativeToMediaPath);
                        if (File.Exists(absoluteFilePath))
                        {
                            relativeFilePath = string.Format("{0}/{1}", this.companyId, mediaItem.FilePathRelativeToMediaPath.Replace("\\", "/"));
                            this.AppendJavascript("CompanyData.product{0}.BrandingImage=\"{1}\";", product.ProductId, relativeFilePath);
                        }
                    }
                }
            }

            // Order hours
            if (product.OrderHours != null && product.OrderHours.Length > 0)
            {
                this.AppendJavascript("CompanyData.product{0}.orderhours= [];", product.ProductId);
                for (int day = 0; day < 7; day++)
                {
                    this.AppendJavascript("CompanyData.product{0}.orderhours[{1}] = [];", product.ProductId, day);
                    foreach (var orderHour in product.OrderHours)
                    {
                        if (orderHour.DayOfWeek == day)
                        {
                            this.AppendJavascript("CompanyData.product{0}.orderhours[{1}].push({{ TimeStart: {2}, TimeEnd: {3} }});", product.ProductId, day, int.Parse(orderHour.TimeStart), int.Parse(orderHour.TimeEnd));
                        }
                    }
                }
            }

            this.AppendJavascript("CompanyData.products.push(CompanyData.product{0});", product.ProductId);
        }

        private void WriteSuggestion(ProductSuggestion productSuggestion)
        {
            this.WriteModelToJavascript("CompanyData.suggestion", productSuggestion);
            this.AppendJavascript("CompanyData.suggestions.push(CompanyData.suggestion{0});", productSuggestion.ProductSuggestionId);
        }

        private void WriteAlteration(Alteration alteration)
        {
            this.WriteModelToJavascript("CompanyData.alteration", alteration);

            // Alteration Options
            if (alteration.Alterationoptions != null)
            {
                this.AppendJavascript("CompanyData.alteration{0}.alterationOptions=[{1}];", alteration.AlterationId, alteration.Alterationoptions.Length);

                int alterationOptionNumber = 0;
                foreach (var alterationOption in alteration.Alterationoptions)
                {
                    // Prevent writing an Alteration Option Twice
                    if (!this.writtenAlterationOptionIds.Contains(alterationOption.AlterationoptionId))
                    {
                        this.WriteModelToJavascript("CompanyData.alterationOption", alterationOption);
                        this.writtenAlterationOptionIds.Add(alterationOption.AlterationoptionId);
                    }

                    this.AppendJavascript("CompanyData.alteration{0}.alterationOptions[{1}]=CompanyData.alterationOption{2};", alteration.AlterationId, alterationOptionNumber, alterationOption.AlterationoptionId);

                    alterationOptionNumber++;
                }
            }

            this.AppendJavascript("CompanyData.alterations.push(CompanyData.alteration{0});", alteration.AlterationId);
        }

        private void WriteMedia(Media mediaItem)
        {
            this.WriteModelToJavascript("CompanyData.media", mediaItem);
            this.AppendJavascript("CompanyData.media.push(CompanyData.media{0});", mediaItem.MediaId);
        }

        private void WriteCompany(Company company)
        {
            this.WriteModelToJavascript("CompanyData.company", company);

            // Media
            if (company.Media != null && company.Media.Length > 0)
            {
                string absoluteCompanyPath = Path.Combine(Dionysos.Global.ApplicationInfo.BasePath, string.Format("Media\\{0}", this.companyId));
                string relativeFilePath = string.Empty;
                string absoluteFilePath = string.Empty;

                foreach (var mediaItem in company.Media)
                {
                    if (mediaItem.MediaType == MediaType.Homepage800x480.ToInt())
                    {
                        // Button image
                        absoluteFilePath = Path.Combine(absoluteCompanyPath, mediaItem.FilePathRelativeToMediaPath);
                        if (File.Exists(absoluteFilePath))
                        {
                            relativeFilePath = string.Format("{0}/{1}", this.companyId, mediaItem.FilePathRelativeToMediaPath.Replace("\\", "/"));
                            this.AppendJavascript("CompanyData.company{0}.EyecatcherImage=\"{1}\";", company.CompanyId, relativeFilePath);
                        }
                    }
                }
            }

            this.AppendJavascript("CompanyData.company = CompanyData.company{0}", company.CompanyId);
        }

        private void WriteFeature(Feature feature)
        {
            this.WriteModelToJavascript("CompanyData.feature", feature);
            this.AppendJavascript("CompanyData.features.push(CompanyData.feature{0});", feature.FeatureId);
        }

        private void WriteDiscount(Discount discount)
        {
            this.WriteModelToJavascript("CompanyData.discount", discount);

            // Schedule
            if (discount.Schedule != null && discount.Schedule.Length > 0)
            {
                this.AppendJavascript("CompanyData.discount{0}.schedule = [];", discount.DiscountId);
                for (int day = 0; day < 7; day++)
                {
                    this.AppendJavascript("CompanyData.discount{0}.schedule[{1}] = [];", discount.DiscountId, day);
                    foreach (var schedule in discount.Schedule)
                    {
                        if (schedule.DayOfWeek == day)
                        {
                            this.AppendJavascript("CompanyData.discount{0}.schedule[{1}].push({{ TimeStart: {2}, TimeEnd: {3} }});", discount.DiscountId, day, int.Parse(schedule.TimeStart), int.Parse(schedule.TimeEnd));
                        }
                    }
                }
            }

            this.AppendJavascript("CompanyData.discounts.push(CompanyData.discount{0});", discount.DiscountId);
        }

        private void WriteSurvey(Survey survey)
        {
            //if (survey.SurveyQuestions != null && survey.SurveyQuestions.Length > 0)
            //{
            //    // Prevent writing a product twice (could happen when it appears in multiple categories
            //    if (!this.writtenSurveyIds.Contains(survey.SurveyId))
            //    {
            //        this.WriteModelToJavascript("CompanyData.survey", survey);
            //        this.writtenSurveyIds.Add(survey.SurveyId);
            //    }
            //    else
            //        return;

            //    // Survey Questions
            //    this.AppendJavascript("CompanyData.survey{0}.surveyQuestions=[{1}];", survey.SurveyId, survey.SurveyQuestions.Length);

            //    int surveyQuestionNumber = 0;
            //    foreach (var question in survey.SurveyQuestions)
            //    {
            //        // Prevent writing an Question twice
            //        if (!this.writtenSurveyQuestionIds.Contains(question.SurveyQuestionId))
            //        {
            //            this.WriteModelToJavascript("CompanyData.surveyQuestion", question);
            //            this.writtenSurveyQuestionIds.Add(question.SurveyQuestionId);
            //        }

            //        this.AppendJavascript("CompanyData.survey{0}.surveyQuestions[{1}] = CompanyData.surveyQuestion{2};", survey.SurveyId, surveyQuestionNumber, question.SurveyQuestionId);

            //        // Survey Answers
            //        this.AppendJavascript("CompanyData.surveyQuestion{0}.surveyAnswers=[{1}];", question.SurveyQuestionId, question.SurveyAnswers.Length);

            //        // Write the answers to the questions
            //        this.AppendJavascript("CompanyData.surveyQuestion{0}.surveyAnswers=[{1}];", question.SurveyQuestionId, question.SurveyAnswers.Length);

            //        int surveyAnswerNumber = 0;
            //        foreach (var answer in question.SurveyAnswers)
            //        {
            //            // Prevent writing an Answer twice
            //            if (!this.writtenSurveyAnwserIds.Contains(answer.SurveyAnswerId))
            //            {
            //                this.WriteModelToJavascript("CompanyData.surveyAnswer", answer);
            //                this.writtenSurveyAnwserIds.Add(answer.SurveyAnswerId);
            //            }

            //            this.AppendJavascript("CompanyData.surveyQuestion{0}.surveyAnswers[{1}] = CompanyData.surveyAnswer{2};",
            //                question.SurveyQuestionId, surveyAnswerNumber, answer.SurveyAnswerId);

            //            surveyAnswerNumber++;
            //        }

            //        surveyQuestionNumber++;
            //    }
            //}

            //this.AppendJavascript("CompanyData.surveys.push(CompanyData.survey{0});", survey.SurveyId);
        }

        private void WriteSurveyQuestion(SurveyQuestion question)
        {

        }

        private void WriteDeliverypoint(Deliverypoint deliverypoint)
        {
            this.WriteModelToJavascript("CompanyData.deliverypoint", deliverypoint);
            this.AppendJavascript("CompanyData.deliverypoints.push(CompanyData.deliverypoint{0});", deliverypoint.DeliverypointId);
        }

        private void WritePaymentmethod(Paymentmethod paymentmethod)
        {
            this.WriteModelToJavascript("CompanyData.paymentmethod", paymentmethod);
            this.AppendJavascript("CompanyData.paymentmethods.push(CompanyData.paymentmethod{0});", paymentmethod.PaymentmethodId);
        }

        private void WriteServiceRequest(Product servicerequest)
        {
            this.WriteModelToJavascript("CompanyData.servicerequest", servicerequest);
            this.AppendJavascript("CompanyData.servicerequests.push(CompanyData.servicerequest{0});", servicerequest.ProductId);
        }

        private void WriteBusinesshours(Businesshours businesshours)
        {
            this.WriteModelToJavascript("CompanyData.businesshours", businesshours);
            this.AppendJavascript("CompanyData.businesshours.push(CompanyData.businesshours{0});", businesshours.BusinesshoursId);
        }

        public new bool IsReusable
        {
            get
            {
                return false;
            }
        }

    }
}
