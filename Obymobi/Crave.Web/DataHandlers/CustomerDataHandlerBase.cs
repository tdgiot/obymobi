﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Crave.Web.Mobile;
using Obymobi.Logic.Model;
using System.Web;
using Obymobi.Data.EntityClasses;
using Obymobi.Logic.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data;
using System.Collections;
using Obymobi.Enums;

namespace Crave.Web.DataHandlers
{
    public class CustomerDataHandlerBase : JavascriptDataHandler
    {

        private int customerId = -1;

        public override bool RenderJavascript()
        {
            bool toReturn = false;

            // Check if a CustomerId was supplied        
            if (HttpContext.Current != null &&
                HttpContext.Current.User != null &&
                HttpContext.Current.User.Identity != null &&
                int.TryParse(HttpContext.Current.User.Identity.Name, out this.customerId))
            {
                // Customer Id from current signed in user
            }
            else if (Dionysos.Web.QueryStringHelper.TryGetValue("c", out this.customerId))
            {
                // Customer ID is retrieved from the QueryStringHelper
            }
            else
            {
                // Missing CustomerId
                return false;
            }

            // Check if Customer exists            
            PrefetchPath customerPath = new PrefetchPath(EntityType.CustomerEntity);
            customerPath.Add(CustomerEntity.PrefetchPathFavoritecompanyCollection);
            CustomerEntity c = new CustomerEntity(this.customerId, customerPath);
            if (c.IsNew)
            {
                // Customer is non existent
                return false;
            }
            else if (DateTime.Now.Date == new DateTime(2011, 12, 6) && TestUtil.IsPcDeveloper)
            {
                // DEVELOPER OVERRIDE
            }
            else if (c.CustomerId.ToString() != HttpContext.Current.User.Identity.Name && !TestUtil.IsPcGabriel)
            {
                // Customer is trying to access another user's details                
                return false;
            }

            // Retrieve
            var customer = CustomerHelper.CreateCustomerModelFromEntity(c);

            // Open the namespace
            this.AppendJavascript("(function (CustomerData, $, undefined) {{");
            this.AppendJavascript("Jog.debug(\"CustomerData\", \"class\", \"Init start \")");

            this.WriteCustomer(customer);

            // Handle favorite companies
            this.AppendJavascript("CustomerData.favoritecompanies = [];");
            if (c.FavoritecompanyCollection.Count > 0)
            {
                foreach (FavoritecompanyEntity favoritecompanyEntity in c.FavoritecompanyCollection)
                {
                    if (favoritecompanyEntity.CompanyEntity.AvailableOnObymobi)
                    {
                        var favoriteCompany = FavoritecompanyHelper.ConvertEntityToModel(favoritecompanyEntity);
                        this.WriteFavoriteCompany(favoriteCompany);
                    }
                }
            }

            if (c.CustomerPaymentCollection.Count > 0)
            {
                this.AppendJavascript("CustomerData.customerpaymentmethods = [];");

                var customerPayments = PaymentHelper.ConvertEntityCollectionToModelCollection(c.CustomerPaymentCollection);

                for (int i = 0; i < customerPayments.Length; i++)
                {
                    this.WriteCustomerPayment(customerPayments[i], c.CustomerPaymentCollection[i].PaymentmethodEntity.Name, c.CustomerPaymentCollection[i].PaymentmethodEntity.PaymentmethodType);
                }
            }

            WriteCustomerSocialmedia(c);

            // Close the namespace
            this.AppendJavascript("}} ( window.CustomerData = window.CustomerData || {{}}, jQuery))");

            toReturn = true;

            return toReturn;
        }

        public void WriteCustomer(Customer customer)
        {
            this.WriteModelToJavascript("CustomerData.customer", customer);
            this.AppendJavascript("CustomerData.customer = CustomerData.customer{0};", customer.CustomerId);

            var companies = CustomerHelper.GetRecentlyVisitedCompanies(customer.CustomerId);
            if (companies.Count > 0)
            {
                // Write Recently Visited Companies
                this.AppendJavascript("CustomerData.recentlyVisited = [{0}];", companies.Count);

                int companyNumber = 0;
                foreach (var company in companies)
                {
                    this.WriteModelToJavascript("CustomerData.company", CompanyHelper.CreateCompanyModelFromEntity(company, false));

                    // Append to a array so we can iterate
                    this.AppendJavascript("CustomerData.recentlyVisited[{0}] = CustomerData.company{1};", companyNumber, company.CompanyId);
                    companyNumber++;
                }
            }
        }

        private void WriteFavoriteCompany(Favoritecompany favoriteCompany)
        {
            this.WriteModelToJavascript("CustomerData.favoritecompany", favoriteCompany);
            this.AppendJavascript("CustomerData.favoritecompanies.push(CustomerData.favoritecompany{0});", favoriteCompany.FavoritecompanyId);
        }

        private void WriteCustomerPayment(CustomerPaymentmethod customerPaymentmethod, string paymentmethodName, int paymentmethodType)
        {
            this.WriteModelToJavascript("CustomerData.customerpaymentmethod", customerPaymentmethod);

            this.AppendJavascript("CustomerData.customerpaymentmethod{0}.PaymentmethodName={1};", customerPaymentmethod.CustomerPaymentmethodId, this.EncodeJsString(paymentmethodName));
            this.AppendJavascript("CustomerData.customerpaymentmethod{0}.PaymentmethodType=\"{1}\";", customerPaymentmethod.CustomerPaymentmethodId, paymentmethodType);

            this.AppendJavascript("CustomerData.customerpaymentmethods.push(CustomerData.customerpaymentmethod{0});", customerPaymentmethod.CustomerPaymentmethodId);
        }

        private void WriteCustomerSocialmedia(CustomerEntity customer)
        {
            this.AppendJavascript("CustomerData.socialmedia = {{}};");
            if (customer.CustomerSocialmediaCollection != null)
            {
                foreach (CustomerSocialmediaEntity socialmedia in customer.CustomerSocialmediaCollection)
                {
                    if (socialmedia.SocialmediaType == SocialmediaType.Facebook.ToInt())
                    {
                        this.AppendJavascript("CustomerData.socialmedia.Facebook = {{}};");
                        this.AppendJavascript("CustomerData.socialmedia.Facebook.UserId = {0};", this.EncodeJsString(socialmedia.FieldValue1));
                        this.AppendJavascript("CustomerData.socialmedia.Facebook.Name = {0};", this.EncodeJsString(socialmedia.FieldValue3));
                        this.AppendJavascript("CustomerData.socialmedia.Facebook.Email = {0};", this.EncodeJsString(socialmedia.FieldValue4));

                        // If we don't have a phonenumber yet, we need the app to ask for it
                        if (customer.Phonenumber == null || customer.Phonenumber.Length <= 0)
                        {
                            this.AppendJavascript("CustomerData.socialmedia.Facebook.RequestPhonenumber = true;");
                        }
                    }
                    else if (socialmedia.SocialmediaType == SocialmediaType.Twitter.ToInt())
                    {
                        this.AppendJavascript("CustomerData.socialmedia.Twitter = {{}};");
                        this.AppendJavascript("CustomerData.socialmedia.Twitter.UserId = {0};", this.EncodeJsString(socialmedia.FieldValue3));
                        this.AppendJavascript("CustomerData.socialmedia.Twitter.ScreenName = {0};", this.EncodeJsString(socialmedia.FieldValue4));
                    }
                    else if (socialmedia.SocialmediaType == SocialmediaType.Foursquare.ToInt())
                    {
                        this.AppendJavascript("CustomerData.socialmedia.Foursquare = {{}};");
                        this.AppendJavascript("CustomerData.socialmedia.Foursquare.UserId = {0};", this.EncodeJsString(socialmedia.FieldValue1));
                        this.AppendJavascript("CustomerData.socialmedia.Foursquare.Email = {0};", this.EncodeJsString(socialmedia.FieldValue3));
                    }
                }
            }

        }

        public new bool IsReusable
        {
            get
            {
                return false;
            }
        }

    }
}
