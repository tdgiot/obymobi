﻿using System;
using System.Collections.Generic;
using PokeIn;
using PokeIn.Comet;

namespace Crave.Web
{
    [Serializable]
    public class SignInResult
    {
        public int ResultCode;
        public string Message;

        //You should define an Empty Consturctor to allow PokeIn to de-serialize
        public SignInResult()
        {
            ResultCode = 0;
            Message = "";
        }

        public SignInResult(int resultCode, string message)
        {
            ResultCode = resultCode;
            Message = message;
        }
    }

    /// <summary>
    /// Summary description for ConsoleClient
    /// </summary>
    public class ConsoleClient : IDisposable
    {
        public static Dictionary<string, string> connectedClients = new Dictionary<string, string>();

        private readonly string clientId;
        private string userName;

        public ConsoleClient(string clientId)
        {
            this.clientId = clientId;
        }

        public void SignIn(string username)
        {
            SignInResult message;

            if (this.userName == "")
            {
                this.userName = username;
                lock(connectedClients)
                {
                    connectedClients.Add(this.clientId, this.userName);
                }

                message = new SignInResult(100, "OK!");
            }
            else
            {
                // report back to the page that we already have an username assigned
                message = new SignInResult(200, "You are already logged in.");
            }

            string json = JSON.Method("OrderWindow.signInCallback", message);
            CometWorker.SendToClient(this.clientId, json);
        }

        public void TestMethod()
        {
            string[] clients = CometWorker.GetClientIds();
            CometWorker.SendToAll("alert('Alert invoked from other application! [" + clients.Length + "]');");
        }

        #region Implementation of IDisposable

        public void Dispose()
        {
            lock(connectedClients)
            {
                connectedClients.Remove(this.clientId);
            }
        }

        #endregion
    }
}