﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="Obymobi.Noc.WebApplication._401" Codebehind="401.aspx.cs" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server" id="HtmlHeader">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Crave Content Management System</title>
    <meta http-equiv="imagetoolbar" content="no" />
    <link href="~/favicon.ico" type="image/x-icon" link rel="shortcut icon" />
    
    <link href="~/css/default.css" type="text/css" link rel="Stylesheet" />
    <link href="~/css/default-hide-left-column.css" type="text/css" link rel="Stylesheet" />
    <link href="~/css/base.css" type="text/css" link rel="Stylesheet" />
    <link href="~/css/eWorldUiControls.css" type="text/css" link rel="Stylesheet" />
    <link href="~/css/mediacollection.css" type="text/css" link rel="Stylesheet" />
    <link href="~/css/mediabox.css" type="text/css" link rel="Stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
    <div id="head">
        <div class="mainmenu">
            <ul class="nav">
            </ul>
        </div>
                   
        <div class="titles">
            <div class="module">

            </div>
            <div class="seperator">&nbsp;</div>
            <div class="pagetitle">
                <div style="padding-left: 6px;">
                    Oops, you don't have access to this piece of information
                </div>
            </div>
            <div class="company">
            </div>                        
            <div class="user">
                <strong>
                    
                </strong>
            </div>
        </div>
    </div>
    <div id="navigation">
        <ul>
        </ul>
        <div class="history">
            <span></span>
            <ul>
            </ul>
        </div>
    </div>
    <div id="toolbar">
        <div class="buttons">
            <span class="toolbar">
            </span>
        </div>
    </div>
    <div id="poweredby">
    </div>
    <div id="content">
        <div class="padTop" id="contentTopSpacer"></div>
        <script language="javascript">
            function toggle() {
                var ele = document.getElementById("toggleDetails");
                var link = document.getElementById("displayText");
                if (ele.style.display == "block") {
                    ele.style.display = "none";
                    link.innerHTML = "Show details";
                }
                else {
                    ele.style.display = "block";
                    link.innerHTML = "Hide details";
                }
            } 
        </script>
        <D:PlaceHolder ID="plhEmailSent" runat="server" Visible="false"><strong>Your e-mail has been sent to Crave.</strong></D:PlaceHolder><br /><br /> 
        <D:PlaceHolder ID="plhMessage" runat="server"></D:PlaceHolder> 
        <a id="displayText" href="javascript:toggle();">Show details</a><br /><br />
        <div id="toggleDetails" style="display: none">
            <D:PlaceHolder ID="plhDetails" runat="server"></D:PlaceHolder> 
        </div> 
        <D:Button runat="server" ID="btnBack" Text="Go back" />
        <D:Button runat="server" ID="btnEmail" Text="E-mail to Crave" />		
    </div>
    </form>
</body>
</html>
