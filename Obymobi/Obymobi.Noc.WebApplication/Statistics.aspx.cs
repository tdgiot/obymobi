﻿using System;
using System.Collections.Generic;
using Dionysos.Web.UI;

public partial class Statistics : PageDefault
{
    protected void Page_Load(object sender, EventArgs e)
    {
        this.Title = string.Format("{0} - {1}", Resources.strings.Statistics, Resources.strings.Crave_NOC);
        this.plhStatisticsPopupBtn.AddHtml("<div class=\"button\" onclick=\"return textMessage(event, 'The following statistics show the number of processed orders for your venue over a defined period. The percentages in brackets indicate if there has been an increase or decrease in the number of orders over the previous corresponding period. ')\" ><img class='infoButton' src='Images/Icons/info.png'></div>");


        List<int> companyIds = CraveMobileNocHelper.AvailableCompanyIds;
        if (companyIds.Count == 0)
        {
            UserControls_Listitems_Statistic statisticControl = this.LoadControl<UserControls_Listitems_Statistic>("~/UserControls/Listitems/Statistic.ascx");
            statisticControl.CompanyId = null;
            this.plhList.Controls.Add(statisticControl);
        }
        else
        {
            foreach (var companyId in companyIds)
            {
                UserControls_Listitems_Statistic statisticControl = this.LoadControl<UserControls_Listitems_Statistic>("~/UserControls/Listitems/Statistic.ascx");
                statisticControl.CompanyId = companyId;
                this.plhList.Controls.Add(statisticControl);
            }
        }
    }

    
}