﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Dionysos.Web;
using Obymobi.Enums;

public partial class LogSortingPaging : System.Web.UI.UserControl
{
    #region Fields

    /// <summary>
    /// The navigate URL.
    /// </summary>
    private string navigateUrl;

    #endregion

    #region Properties

    /// <summary>
    /// Gets or sets the CSS class.
    /// </summary>
    /// <value>The CSS class.</value>
    public string CssClass { get; set; }

    /// <summary>
    /// Gets or sets a value indicating whether to render the sort controls.
    /// </summary>
    /// <value>
    ///   <c>true</c> if the sort controls are rendered; otherwise, <c>false</c>.
    /// </value>
    public bool RenderSort { get; set; }

    /// <summary>
    /// Gets or sets the log sort order.
    /// </summary>
    /// <value>
    /// The log sort order.
    /// </value>
    public LogSortOrder SortOrder { get; set; }

    /// <summary>
    /// Gets a value indicating whether to render the paging controls.
    /// </summary>
    /// <value>
    ///   <c>true</c> if paging controls are rendered; otherwise, <c>false</c>.
    /// </value>
    public bool RenderPaging
    {
        get
        {
            return ((this.RenderSort && this.PageNumber >= 0) || this.PageNumber > 0) && this.PageSize > 0 && this.Count > 0 && this.PageCount > 1;
        }
    }

    /// <summary>
    /// Gets or sets the page number.
    /// </summary>
    /// <value>
    /// The page number.
    /// </value>
    public int PageNumber { get; set; }

    /// <summary>
    /// Gets or sets the size of the page.
    /// </summary>
    /// <value>
    /// The size of the page.
    /// </value>
    public int PageSize { get; set; }

    /// <summary>
    /// Gets or sets the count.
    /// </summary>
    /// <value>
    /// The count.
    /// </value>
    public int Count { get; set; }

    /// <summary>
    /// Gets the page count.
    /// </summary>
    public int PageCount
    {
        get
        {
            return (int)System.Math.Ceiling(this.Count / (decimal)this.PageSize);
        }
    }

    /// <summary>
    /// Gets or sets the navigate URL.
    /// </summary>
    /// <value>
    /// The navigate URL.
    /// </value>
    public string NavigateUrl
    {
        get
        {
            return this.navigateUrl ?? (this.navigateUrl = new Uri(this.Request.Url, this.Request.RawUrl).AbsolutePath);
        }
        set
        {
            this.navigateUrl = value;
        }
    }

    #endregion

    #region Methods

    /// <summary>
    /// Sends server control content to a provided <see cref="T:System.Web.UI.HtmlTextWriter"/> object, which writes the content to be rendered on the client.
    /// </summary>
    /// <param name="writer">The <see cref="T:System.Web.UI.HtmlTextWriter"/> object that receives the server control content.</param>
    protected override void Render(HtmlTextWriter writer)
    {
        if (this.RenderSort ||
            this.RenderPaging)
        {
            // Add class
            if (!String.IsNullOrEmpty(this.CssClass))
            {
                writer.AddAttribute(HtmlTextWriterAttribute.Class, this.CssClass);
            }
            writer.RenderBeginTag(HtmlTextWriterTag.Div);

            if (this.RenderSort)
            {
                writer.AddAttribute(HtmlTextWriterAttribute.Class, "sorting");
                writer.RenderBeginTag(HtmlTextWriterTag.Dl);

                writer.RenderBeginTag(HtmlTextWriterTag.Dt);
                writer.WriteEncodedText(Resources.strings.PagingSortOn);
                writer.RenderEndTag(); // Dt

                writer.RenderBeginTag(HtmlTextWriterTag.Dd);
                writer.RenderBeginTag(HtmlTextWriterTag.Ul);

                foreach (LogSortOrder sortOrder in EnumUtil.ToArray<LogSortOrder>())
                {
                    bool isActive = this.SortOrder == sortOrder;
                    string name = Resources.strings.ResourceManager.GetString("PagingSortOn" + sortOrder.ToString());

                    if (isActive) writer.AddAttribute(HtmlTextWriterAttribute.Class, "active");
                    writer.RenderBeginTag(HtmlTextWriterTag.Li);

                    if (!isActive)
                    {
                        QueryStringHelper qsh = new QueryStringHelper().AddRange(this.Request.QueryString, Resources.strings.QueryStringPage);
                        if (sortOrder != default(LogSortOrder))
                        {
                            qsh.AddItem(Resources.strings.QueryStringSort, name.ToLowerInvariant());
                        }
                        else
                        {
                            qsh.RemoveItem(Resources.strings.QueryStringSort);
                        }
                        string navigateUrl = qsh.MergeQuerystringWithUrl(this.NavigateUrl, false);

                        writer.AddAttribute(HtmlTextWriterAttribute.Href, navigateUrl, true);
                        writer.AddAttribute(HtmlTextWriterAttribute.Rel, "nofollow");
                        writer.RenderBeginTag(HtmlTextWriterTag.A);
                    }

                    writer.WriteEncodedText(name);

                    if (!isActive)
                    {
                        writer.RenderEndTag(); // A
                    }

                    writer.RenderEndTag(); // Li
                }

                writer.RenderEndTag(); // Ul
                writer.RenderEndTag(); // Dd

                writer.RenderEndTag(); // Dl
            }

            if (this.RenderPaging)
            {
                writer.AddAttribute(HtmlTextWriterAttribute.Class, "paging");
                writer.RenderBeginTag(HtmlTextWriterTag.Dl);

                writer.RenderBeginTag(HtmlTextWriterTag.Dt);
                if (this.PageNumber == 0)
                {
                    // Show paged link
                    this.RenderPagingLink(writer, 1, String.Format(Resources.strings.PagingShowPaged, this.PageSize));
                }
                else
                {
                    // Show all link
                    this.RenderPagingLink(writer, 0, String.Format(Resources.strings.PagingShowAll, this.Count));
                }
                writer.RenderEndTag(); // Dt

                if (this.PageNumber > 0)
                {
                    writer.RenderBeginTag(HtmlTextWriterTag.Dd);
                    writer.RenderBeginTag(HtmlTextWriterTag.Ul);

                    // Previous link
                    if (this.PageNumber > 1)
                    {
                        writer.AddAttribute(HtmlTextWriterAttribute.Class, "previous");
                        writer.RenderBeginTag(HtmlTextWriterTag.Li);
                        this.RenderPagingLink(writer, this.PageNumber - 1, Resources.strings.Previous);
                        writer.RenderEndTag(); // Li
                    }

                    // Page number links
                    for (int i = 1, pageCount = this.PageCount; i <= pageCount; i++)
                    {
                        bool isActive = i == this.PageNumber;

                        if (isActive) writer.AddAttribute(HtmlTextWriterAttribute.Class, "active");
                        writer.RenderBeginTag(HtmlTextWriterTag.Li);

                        if (isActive)
                        {
                            writer.WriteEncodedText(i.ToString());
                        }
                        else
                        {
                            this.RenderPagingLink(writer, i, null);
                        }

                        writer.RenderEndTag(); // Li
                    }

                    // Next link
                    if (this.PageNumber < this.PageCount)
                    {
                        writer.AddAttribute(HtmlTextWriterAttribute.Class, "next");
                        writer.RenderBeginTag(HtmlTextWriterTag.Li);
                        this.RenderPagingLink(writer, this.PageNumber + 1, Resources.strings.Next);
                        writer.RenderEndTag(); // Li
                    }

                    writer.RenderEndTag(); // Ul
                    writer.RenderEndTag(); // Dd
                }

                writer.RenderEndTag(); // Dl
            }

            writer.RenderEndTag(); // Div
        }

        base.Render(writer);
    }

    /// <summary>
    /// Renders the paging link.
    /// </summary>
    /// <param name="writer">The writer.</param>
    /// <param name="pageNumber">The page number.</param>
    /// <param name="text">The text.</param>
    protected void RenderPagingLink(HtmlTextWriter writer, int pageNumber, string text)
    {
        QueryStringHelper qsh = new QueryStringHelper().AddRange(this.Request.QueryString, Resources.strings.QueryStringSort);
        if (pageNumber != 1)
        {
            qsh.AddItem(Resources.strings.QueryStringPage, pageNumber);
        }
        else
        {
            qsh.RemoveItem(Resources.strings.QueryStringPage);
        }
        string navigateUrl = qsh.MergeQuerystringWithUrl(this.NavigateUrl, false);

        writer.AddAttribute(HtmlTextWriterAttribute.Href, navigateUrl, true);
        writer.AddAttribute(HtmlTextWriterAttribute.Rel, "nofollow");
        writer.RenderBeginTag(HtmlTextWriterTag.A);
        writer.WriteEncodedText(text ?? pageNumber.ToString());
        writer.RenderEndTag(); // A
    }

    #endregion

    #region Event Handlers

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    #endregion
}