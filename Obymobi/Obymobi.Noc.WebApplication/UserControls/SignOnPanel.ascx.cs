﻿using System;
using System.Linq;
using System.Web;
using System.Web.Security;
using Dionysos.Web.Security;
using Dionysos.Web.UI.WebControls;
using Obymobi.Data.EntityClasses;
using Obymobi.Enums;

public partial class SignOnPanel : SignOnControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (IsAuthenticated)
        {
            if (this.lblUsername.Text.Length == 0)
            {
                this.lblUsername.Text = "Administrator";
            }
        }
    }

    public override void SignOn(string username, string password)
    {
        CraveMobileNocHelper.ClearSession();
        base.SignOn(username, password);
    }

    public override void AfterSignOn()
    {
        // Reset the password format to hashed (We need a clear passwordFormat for automatic login)
        UserManager.Instance.PasswordFormat = PasswordFormat.Hashed;

        // Only allow employees or non related users
        UserEntity user = this.User as UserEntity;

        if (this.Result == AuthenticateResult.Succes && user != null)
        {
            if (user.Roles.Length == 0)
            {
                this.Result = AuthenticateResult.NoRoleAssignedToUser;
            }
            else
            {
                if (user.AccountId.HasValue)
                {
                    if (!user.IsAdministrator && user.AccountEntity.AccountCompanyCollection.Count == 0)
                    {
                        this.Result = AuthenticateResult.NoCompanyAssignedToRelatedAccount;
                    }
                    else if (user.AccountEntity.AccountCompanyCollection.Count >= 1)
                    {
                        CraveMobileNocHelper.AvailableCompanyIds = user.AccountEntity.AccountCompanyCollection.Select(c => c.CompanyId).ToList();

                        if (user.AccountEntity.AccountCompanyCollection.Count == 1)
                        {
                            // Only forward Dutch, Otoucho customers with POS to the CustomerStatus page.
                            CompanyEntity company = user.AccountEntity.AccountCompanyCollection[0].CompanyEntity;
                            if (company.AvailableOnOtoucho && company.CultureCode.Equals("nl-NL", StringComparison.OrdinalIgnoreCase) && company.POSConnectorType != POSConnectorType.Unknown)
                            {
                                this.Response.Redirect("~/CustomerStatus.aspx", true);
                                return;
                            }                            
                        }
                    }
                }
            }
        }

        if (this.Result != AuthenticateResult.Succes)
        {
            string message;
            switch (this.Result)
            {
                case AuthenticateResult.UsernameEmpty:
                case AuthenticateResult.PasswordEmpty:
                    message = Resources.strings.SignOn_specify_a_username_and_password;
                    break;
                case AuthenticateResult.LoginUnkown:
                case AuthenticateResult.PasswordIncorrect:
                    message = Resources.strings.SignOn_login_is_incorrect;
                    break;
                case AuthenticateResult.UserIsLockedOut:
                    message = Resources.strings.SignOn_temporarily_locked_out;
                    break;
                case AuthenticateResult.UserIsNotActivated:
                    message = Resources.strings.SignOn_account_not_acitvated;
                    break;
                case AuthenticateResult.AwaitingAttempts:
                    message = Resources.strings.SignOn_awaiting_attempts;
                    break;
                case AuthenticateResult.NoRoleAssignedToUser:
                    message = Resources.strings.SignOn_no_role_assigned_to_user;
                    break;
                case AuthenticateResult.NoCompanyAssignedToUser:
                    message = Resources.strings.SignOn_no_account_related_to_user;
                    break;
                case AuthenticateResult.NoCompanyAssignedToRelatedAccount:
                    message = Resources.strings.SignOn_no_company_related_to_account;
                    break;
                default:
                    message = Resources.strings.SignOn_technical_failure;
                    break;
            }

            FormsAuthentication.SignOut();
            HttpContext.Current.Response.Redirect(ResolveUrl(this.SignOnPageRelativeUrl + "?msg=" + message), true);
        }
        else
        {
            base.AfterSignOn();
        }
    }
}