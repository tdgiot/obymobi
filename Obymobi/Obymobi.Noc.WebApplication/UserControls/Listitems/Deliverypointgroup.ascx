﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="UserControls_Listitems_Deliverypointgroup" Codebehind="Deliverypointgroup.ascx.cs" %>
<li>
    <D:HyperLink ID="hlDeliverypointgroup" runat="server">
        <div class="title"><D:Label ID="lblTitle" runat="server"></D:Label></div>
        <div class="label"><D:LabelTextOnly ID="lblDeliverypointCountLabel" runat="server" Text="<%$ Resources:strings, Deliverypointgroup_deliverypoint_count %>"></D:LabelTextOnly>:</div>
        <div class="value"><D:Label ID="lblDeliverypointCount" runat="server"></D:Label></div>
    </D:HyperLink>
</li>