﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using Obymobi.Data.EntityClasses;
using Obymobi.Enums;

public partial class UserControls_Listitems_Product : UserControl
{
    #region Constructors

    public UserControls_Listitems_Product()
    { }

    #endregion

    #region Properties

    public ProductEntity ProductEntity { get; set; }

    private IEnumerable<string> FullCategoryNames => this.ProductEntity.ProductCategoryCollection.Select(category => category.CategoryEntity.FullCategoryName);

    private bool IsVisible => this.ProductEntity.VisibilityType != VisibilityType.Never;
    
    #endregion

    #region Event handlers

    protected void Page_Load(object sender, EventArgs e)
    {
        if (this.ProductEntity == null)
        {
            return;
        }
        
        this.lblNameValue.Text = this.ProductEntity.Name;
        
        if (FullCategoryNames != null)
        {
            this.lblCategories.Text = string.Join(Environment.NewLine, FullCategoryNames);
        }

        this.btnVisibility.Text = IsVisible ? "HIDE" : "UNHIDE";
        this.btnVisibility.CssClass = IsVisible ? string.Empty : "hidden";
    }

    #endregion
}
