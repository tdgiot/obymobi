﻿using System;
using Dionysos;
using Obymobi.Data.EntityClasses;
using Obymobi.Enums;
using Obymobi.Extensions;

public partial class UserControls_Listitems_Orderitem : System.Web.UI.UserControl
{
    #region Properties

    public OrderitemEntity Orderitem { get; set; }

    #endregion

    #region Event handlers

    protected void Page_Load(object sender, EventArgs e)
    {
        if (this.Orderitem != null)
        {
            this.hlOrderitem.NavigateUrl = ResolveUrl(string.Format("~/Orderitem.aspx?id={0}", this.Orderitem.OrderitemId));

            switch (this.Orderitem.OrderEntity.StatusAsEnum)
            {
                case OrderStatus.NotRouted:
                case OrderStatus.Routed:
                    this.imgOrderitem.ImageUrl = ResolveUrl("~/Images/Icons/exclamation.png");
                    break;
                case OrderStatus.InRoute:
                case OrderStatus.WaitingForPayment:
                case OrderStatus.WaitingForPaymentCompleted:
                    this.imgOrderitem.ImageUrl = ResolveUrl("~/Images/Icons/hourglass.png");
                    break;
                case OrderStatus.Processed:
                    this.imgOrderitem.ImageUrl = ResolveUrl("~/Images/Icons/checkmark.png");
                    break;
                case OrderStatus.Unprocessable:
                    this.imgOrderitem.ImageUrl = ResolveUrl("~/Images/Icons/error.png");
                    break;
            }

            this.lblTitle.Text = string.Format("{0} × {1}", this.Orderitem.Quantity, this.Orderitem.ProductName);

            // Alterations
            string alterations = string.Empty;
            foreach (OrderitemAlterationitemEntity orderitemAlterationitemEntity in this.Orderitem.OrderitemAlterationitemCollection)
            {
                string alterationName;
                string alterationOptionName = string.Empty;

                if (!orderitemAlterationitemEntity.AlterationName.IsNullOrWhiteSpace())
                {
                    alterationName = orderitemAlterationitemEntity.AlterationName;
                }
                else if (orderitemAlterationitemEntity.AlterationitemId.HasValue)
                {
                    if (!orderitemAlterationitemEntity.AlterationitemEntity.AlterationName.IsNullOrWhiteSpace())
                    {
                        alterationName = orderitemAlterationitemEntity.AlterationitemEntity.AlterationName;
                    }
                    else
                    {
                        alterationName = orderitemAlterationitemEntity.AlterationitemEntity.AlterationEntity.Name;
                    }
                }
                else
                {
                    alterationName = "????";
                }

                if (orderitemAlterationitemEntity.AlterationType == (int)AlterationType.Form)
                {
                    if (!orderitemAlterationitemEntity.AlterationoptionName.IsNullOrWhiteSpace())
                    {
                        alterationOptionName = orderitemAlterationitemEntity.AlterationoptionName + ": ";
                    }

                    if (!orderitemAlterationitemEntity.Value.IsNullOrWhiteSpace())
                    {
                        alterationOptionName += orderitemAlterationitemEntity.Value;
                    }
                }
                else if (!orderitemAlterationitemEntity.AlterationoptionName.IsNullOrWhiteSpace())
                {
                    alterationOptionName = orderitemAlterationitemEntity.AlterationoptionName;
                }
                else if (orderitemAlterationitemEntity.AlterationitemId.HasValue && orderitemAlterationitemEntity.AlterationitemId.Value > 0)
                {
                    if (!orderitemAlterationitemEntity.AlterationitemEntity.AlterationoptionName.IsNullOrWhiteSpace())
                    {
                        alterationOptionName = orderitemAlterationitemEntity.AlterationitemEntity.AlterationoptionName;
                    }
                    else
                    {
                        alterationOptionName = orderitemAlterationitemEntity.AlterationitemEntity.AlterationoptionEntity.Name;
                    }
                }
                else if (((AlterationType)orderitemAlterationitemEntity.AlterationType).IsIn(AlterationType.Date, AlterationType.DateTime)
                    && orderitemAlterationitemEntity.TimeUTC.HasValue)
                {
                    CompanyEntity companyEntity = new CompanyEntity(orderitemAlterationitemEntity.ParentCompanyId);
                    DateTime localDateTime = orderitemAlterationitemEntity.TimeUTC.Value.UtcToLocalTime(companyEntity.TimeZoneInfo);

                    if (orderitemAlterationitemEntity.AlterationType == (int)AlterationType.DateTime)
                    {
                        alterationName = localDateTime.ToString("dd/MM/yyyy HH:mm");
                    }
                    else
                    {
                        alterationName = localDateTime.ToString("dd/MM/yyyy");
                    }
                }
                else if (!orderitemAlterationitemEntity.Value.IsNullOrWhiteSpace())
                {
                    alterationOptionName = orderitemAlterationitemEntity.Value;
                }
                else
                {
                    alterationOptionName = "????";
                }

                string nameAndOption = string.Format("{0}: <em>{1}</em>", alterationName, alterationOptionName);

                alterations = StringUtil.CombineWithSeperator("<br/>", alterations, nameAndOption);
            }

            if (!this.Orderitem.Notes.IsNullOrWhiteSpace())
            {
                alterations = string.Format("<span>Notes: <em>{0}</em><br/>{1}</span>", this.Orderitem.Notes, alterations);
            }

            this.lblAlterations.Text = alterations;
        }
    }

    #endregion
}
