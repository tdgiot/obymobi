﻿using System;
using System.Web;
using Obymobi.Data.EntityClasses;
using Obymobi.Enums;

public partial class UserControls_Listitems_OrderRoutestephandler : System.Web.UI.UserControl
{
    #region Properties

    public OrderRoutestephandlerEntity OrderRoutestephandler { get; set; }

    #endregion

    #region Event handlers

    protected void Page_Load(object sender, EventArgs e)
    {
        if (this.OrderRoutestephandler != null)
        {
            this.hlOrderRoutestephandler.NavigateUrl = ResolveUrl(string.Format("~/OrderRoutestephandler.aspx?id={0}", this.OrderRoutestephandler.OrderRoutestephandlerId));

            switch (this.OrderRoutestephandler.StatusAsEnum)
            {
                case OrderRoutestephandlerStatus.WaitingForPreviousStep:
                    this.imgOrderRoutestephandler.ImageUrl = ResolveUrl("~/Images/Icons/hourglass.png");
                    this.lblTitle.Text = string.Format(Resources.strings.OrderRoutestephandler_waiting_for_previous_step, this.OrderRoutestephandler.Number);
                    break;
                case OrderRoutestephandlerStatus.WaitingToBeRetrieved:
                    this.imgOrderRoutestephandler.ImageUrl = ResolveUrl("~/Images/Icons/hourglass.png");
                    this.lblTitle.Text = string.Format(Resources.strings.OrderRoutestephandler_waiting_to_be_retrieved, this.OrderRoutestephandler.Number);
                    break;
                case OrderRoutestephandlerStatus.RetrievedByHandler:
                    this.imgOrderRoutestephandler.ImageUrl = ResolveUrl("~/Images/Icons/hourglass.png");
                    this.lblTitle.Text = string.Format(Resources.strings.OrderRoutestephandler_retrieved_by_handler, this.OrderRoutestephandler.Number);
                    break;
                case OrderRoutestephandlerStatus.BeingHandled:
                    this.imgOrderRoutestephandler.ImageUrl = ResolveUrl("~/Images/Icons/hourglass.png");
                    this.lblTitle.Text = string.Format(Resources.strings.OrderRoutestephandler_being_handled, this.OrderRoutestephandler.Number);
                    break;
                case OrderRoutestephandlerStatus.Completed:
                    this.imgOrderRoutestephandler.ImageUrl = ResolveUrl("~/Images/Icons/checkmark.png");
                    this.lblTitle.Text = string.Format(Resources.strings.OrderRoutestephandler_completed, this.OrderRoutestephandler.Number);
                    break;
                case OrderRoutestephandlerStatus.CompletedByOtherStep:
                    this.imgOrderRoutestephandler.ImageUrl = ResolveUrl("~/Images/Icons/checkmark.png");
                    this.lblTitle.Text = string.Format(Resources.strings.OrderRoutestephandler_completed_by_other_step, this.OrderRoutestephandler.Number);
                    break;
                case OrderRoutestephandlerStatus.CancelledDueToOtherStepFailure:
                    this.imgOrderRoutestephandler.ImageUrl = ResolveUrl("~/Images/Icons/error.png");
                    this.lblTitle.Text = string.Format(Resources.strings.OrderRoutestephandler_cancelled_due_to_other_step_failure, this.OrderRoutestephandler.Number);
                    this.lblStatus.CssClass = "error";
                    break;
                case OrderRoutestephandlerStatus.Failed:
                    this.imgOrderRoutestephandler.ImageUrl = ResolveUrl("~/Images/Icons/error.png");
                    this.lblTitle.Text = string.Format(Resources.strings.OrderRoutestephandler_failed, this.OrderRoutestephandler.Number);
                    this.lblStatus.CssClass = "error";
                    break;
            }

            if (this.OrderRoutestephandler.ErrorCodeAsEnum != OrderProcessingError.None)
                this.lblErrorCode.CssClass = "error";

            SetHandlerTypeLabel();

            DateTime? statusDateTime = null;
            switch (this.OrderRoutestephandler.StatusAsEnum)
            {
                case OrderRoutestephandlerStatus.WaitingForPreviousStep:
                    statusDateTime = this.OrderRoutestephandler.CreatedUTC;
                    break;
                case OrderRoutestephandlerStatus.WaitingToBeRetrieved:
                    statusDateTime = this.OrderRoutestephandler.WaitingToBeRetrievedUTC;                    
                    break;
                case OrderRoutestephandlerStatus.RetrievedByHandler:
                    statusDateTime = this.OrderRoutestephandler.RetrievedByHandlerUTC;
                    break;
                case OrderRoutestephandlerStatus.BeingHandled:                    
                case OrderRoutestephandlerStatus.BeingHandledByOtherStep:
                    statusDateTime = this.OrderRoutestephandler.BeingHandledUTC;
                    break;
                case OrderRoutestephandlerStatus.Completed:                    
                case OrderRoutestephandlerStatus.CompletedByOtherStep:
                case OrderRoutestephandlerStatus.CancelledDueToOtherStepFailure:
                case OrderRoutestephandlerStatus.Failed:
                    statusDateTime = this.OrderRoutestephandler.CompletedUTC;
                    break;
            }

            if(statusDateTime.HasValue)
                this.lblStatus.Text = string.Format("{0} ({1}) @ {2} ({3})", 
                    this.OrderRoutestephandler.StatusAsEnum, 
                    this.OrderRoutestephandler.Status, 
                    CraveMobileNocHelper.ConvertUtcDateTimeToUserTimeZone(statusDateTime.Value), 
                    CraveMobileNocHelper.GetSpanTextBetweenDateTimeAndUtcNow(statusDateTime.Value));
            else
                this.lblStatus.Text = string.Format("{0} ({1})", this.OrderRoutestephandler.StatusAsEnum, this.OrderRoutestephandler.Status);

            if (this.OrderRoutestephandler.ErrorCodeAsEnum == OrderProcessingError.None && this.OrderRoutestephandler.ErrorText.Equals(OrderProcessingError.None.ToString(), StringComparison.InvariantCultureIgnoreCase))
            {
                // No error
                this.plhError.Visible = false;
            }
            else
            {
                this.lblErrorCode.Text = string.Format("{0} ({1})", this.OrderRoutestephandler.ErrorCodeAsEnum, this.OrderRoutestephandler.ErrorCode);
                this.lblErrorCode.CssClass = "error";

                this.lblErrorText.Text = HttpUtility.HtmlEncode( this.OrderRoutestephandler.ErrorText);
                this.lblErrorText.CssClass = "error";
            }
        }
    }

    private void SetHandlerTypeLabel()
    {
        string handlerType = OrderRoutestephandler.HandlerTypeAsEnum.ToString();
        string handlerName = null;

        switch (OrderRoutestephandler.HandlerTypeAsEnum)
        {
            case RoutestephandlerType.Printer:
            case RoutestephandlerType.Console:
                handlerName = OrderRoutestephandler.TerminalEntity.Name;
                break;
            case RoutestephandlerType.ExternalSystem:
                handlerName = OrderRoutestephandler.ExternalSystemEntity.TypeText;
                break;
        }

        lblHandlerType.Text = handlerName == null 
            ? handlerType 
            : $"{handlerType} ({handlerName})";
    }

    #endregion
}