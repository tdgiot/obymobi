﻿using System;
using Obymobi.Data.EntityClasses;
using Obymobi.Enums;
using Dionysos;
using Dionysos.Web;
using Dionysos.Web.DevExpress;

public partial class UserControls_Listitems_Order : System.Web.UI.UserControl
{
    #region Properties

    public OrderEntity Order { get; set; }

    public bool ShowSubOrderWarning { get; set; }

    public bool IsWaitingForPayment { get; set; } = false;

    #endregion

    public UserControls_Listitems_Order()
    {
        ShowSubOrderWarning = true;
    }

    #region Event handlers

    protected void Page_Load(object sender, EventArgs e)
    {
        if (this.Order != null)
        {
            this.hlOrder.NavigateUrl = ResolveUrl(string.Format("~/Order.aspx?id={0}", this.Order.OrderId));

            QueryStringHelper qs = new QueryStringHelper(string.Format("?ProcessOrderId={0}", this.Order.OrderId));
            this.plhProcessOrderButton.AddHtml("<div class=\"action\" onclick=\"return confirmLinkClick(event, 'Are you sure you want to manually process the order?', '{0}')\" >{1}</div>", ResolveUrl(qs.MergeQuerystringWithRawUrl()), Resources.strings.Order_process_order_manually);
            this.plhWaitingForPaymentBtn.AddHtml("<div class=\"button\" onclick=\"return textMessage(event, 'The customer has started the checkout, but has not completed the payment.')\" ><img src='Images/Icons/info.png'></div>");
            switch (this.Order.StatusAsEnum)
            {
                case OrderStatus.NotRouted:
                    this.imgOrder.ImageUrl = ResolveUrl("~/Images/Icons/exclamation.png");
                    this.lblTitle.Text = string.Format(Resources.strings.Order_could_not_be_routed, this.Order.CompanyName, this.Order.DeliverypointNumber);
                    this.lblStatus.CssClass = "error";
                    this.plhProcessOrder.Visible = true;
                    break;
                case OrderStatus.WaitingForPayment:
                    this.IsWaitingForPayment = true;
                    this.imgOrder.ImageUrl = ResolveUrl("~/Images/Icons/hourglass.png");
                    this.lblTitle.Text = string.Format(Resources.strings.Order_is_waiting_for_payment, this.Order.CompanyName, this.Order.DeliverypointNumber);
                    this.lblStatus.CssClass = "error";
                    this.plhProcessOrder.Visible = true;
                    break;
                case OrderStatus.Routed:
                    this.imgOrder.ImageUrl = ResolveUrl("~/Images/Icons/error.png");
                    this.lblTitle.Text = string.Format(Resources.strings.Order_is_routed_but_not_started, this.Order.CompanyName, this.Order.DeliverypointNumber);
                    this.lblStatus.CssClass = "error";
                    this.plhProcessOrder.Visible = true;
                    break;
                case OrderStatus.InRoute:
                    this.imgOrder.ImageUrl = ResolveUrl("~/Images/Icons/hourglass.png");
                    this.lblTitle.Text = string.Format(Resources.strings.Order_is_currently_being_processed, this.Order.CompanyName, this.Order.DeliverypointNumber);
                    this.lblStatus.CssClass = "error";
                    this.plhProcessOrder.Visible = true;
                    break;
                case OrderStatus.WaitingForPaymentCompleted:
                    this.imgOrder.ImageUrl = ResolveUrl("~/Images/Icons/hourglass.png");
                    this.lblTitle.Text = string.Format(Resources.strings.Order_is_waiting_for_payment_complete, this.Order.CompanyName, this.Order.DeliverypointNumber);
                    this.lblStatus.CssClass = "error";
                    this.plhProcessOrder.Visible = true;
                    break;
                case OrderStatus.Processed:
                    this.imgOrder.ImageUrl = ResolveUrl("~/Images/Icons/checkmark.png");
                    if (this.Order.ErrorCode == 0)
                    {
                        this.lblTitle.Text = string.Format(Resources.strings.Order_is_successfully_processed, this.Order.CompanyName, this.Order.DeliverypointNumber);
                    }
                    else
                    {
                        this.lblTitle.Text = string.Format(Resources.strings.Order_is_manually_processed, this.Order.CompanyName, this.Order.DeliverypointNumber);
                        this.lblErrorCode.CssClass = "error";
                    }
                    break;
                case OrderStatus.Unprocessable:
                    this.imgOrder.ImageUrl = ResolveUrl("~/Images/Icons/error.png");
                    this.lblTitle.Text = string.Format(Resources.strings.Order_could_not_be_processed, this.Order.CompanyName, this.Order.DeliverypointNumber);
                    this.lblStatus.CssClass = "error";
                    this.lblErrorCode.CssClass = "error";
                    this.plhProcessOrder.Visible = true;
                    break;
            }

            DateTime createdLocalTime = CraveMobileNocHelper.ConvertUtcDateTimeToUserTimeZone(this.Order.CreatedUTC.GetValueOrDefault());
            string timeSinceCreated = CraveMobileNocHelper.GetSpanTextBetweenDateTimeAndUtcNow(this.Order.CreatedUTC.GetValueOrDefault());

            this.lblTitle.Text += string.Format(" (ID: {0})", this.Order.OrderId);
            this.lblCreated.Text = string.Format("{0} ({1})", createdLocalTime, timeSinceCreated);
            this.lblStatus.Text = string.Format("{0} ({1})", this.Order.StatusText, this.Order.Status);
            this.lblErrorCode.Text = string.Format("{0} ({1})", this.Order.ErrorCodeAsEnum, this.Order.ErrorCode);

            if (!this.Order.MasterOrderId.IsNullOrZero() && ShowSubOrderWarning)
            {
                this.plhIsSubOrder.Visible = true;
            }
        }
    }
    #endregion
}
