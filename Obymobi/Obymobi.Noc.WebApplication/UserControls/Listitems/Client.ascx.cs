﻿using System;
using System.Globalization;
using Obymobi.Data.EntityClasses;
using Dionysos;
using Obymobi.Enums;
using Obymobi;
using Obymobi.Logic.HelperClasses;
using Obymobi.Constants;

public partial class UserControls_Listitems_Client : System.Web.UI.UserControl
{
    private bool renderLinkable = true;

    #region Properties

    public ClientEntity Client { get; set; }

    /// <summary>
    /// Gets or sets the renderLinkable
    /// </summary>
    public bool RenderLinkable
    {
        get
        {
            return this.renderLinkable;
        }
        set
        {
            this.renderLinkable = value;
        }
    }

    public string EmenuReleaseVersion
    { get; set; }

    #endregion

    #region Event handlers

    protected void Page_Load(object sender, EventArgs e)
    {
        if (this.Client != null && this.Client.DeviceId.HasValue)
        {
            if (this.renderLinkable)
            {
                this.plhContainerOpenTags.AddHtml("<li><a class=\"order\" href=\"{0}\">", ResolveUrl(string.Format("~/Client.aspx?id={0}", this.Client.ClientId)));
                this.plhContainerClosingTags.AddHtml("</a></li>");
            }
            else
            {
                this.plhContainerOpenTags.AddHtml("<li class=\"nonlink\"><div class=\"order\">");
                this.plhContainerClosingTags.AddHtml("</div></li>");
            }

            string lastRequestValue = Resources.strings.Unknown;
            string timeSinceLastRequest = "0";
            if (this.Client.DeviceEntity.LastRequestUTC.HasValue)
            {
                lastRequestValue = CraveMobileNocHelper.ConvertUtcDateTimeToUserTimeZone(this.Client.DeviceEntity.LastRequestUTC.Value).ToString(CultureInfo.InvariantCulture);
                timeSinceLastRequest = CraveMobileNocHelper.GetSpanTextBetweenDateTimeAndUtcNow(this.Client.DeviceEntity.LastRequestUTC.Value);
            }

            if (this.Client.IsOnline) // Client is online
            {
                switch (this.Client.OperationModeEnum)
                {
                    case ClientOperationMode.Unknown:
                        this.lblTitle.Text = string.Format(Resources.strings.Client_has_an_unknown_operation_mode, this.Client.DeviceEntity.Identifier);
                        this.imgClient.ImageUrl = ResolveUrl("~/Images/Icons/error.png");
                        break;
                    case ClientOperationMode.Ordering:
                        this.lblTitle.Text = string.Format(Resources.strings.Client_is_online, this.Client.DeviceEntity.Identifier);
                        this.imgClient.ImageUrl = ResolveUrl("~/Images/Icons/checkmark.png");
                        break;
                    case ClientOperationMode.KioskManual:
                        this.lblTitle.Text = string.Format(Resources.strings.Client_is_manually_set_to_kiosk_mode, this.Client.DeviceEntity.Identifier);
                        this.imgClient.ImageUrl = ResolveUrl("~/Images/Icons/exclamation.png");
                        break;
                    case ClientOperationMode.KioskAutomaticUnprocessedOrders:
                        this.lblTitle.Text = string.Format(Resources.strings.Client_has_unprocessed_orders, this.Client.DeviceEntity.Identifier);
                        this.imgClient.ImageUrl = ResolveUrl("~/Images/Icons/error.png");
                        break;
                    case ClientOperationMode.KioskAutomaticUnprocessableOrders:
                        this.lblTitle.Text = string.Format(Resources.strings.Client_has_unprocessable_orders, this.Client.DeviceEntity.Identifier);
                        this.imgClient.ImageUrl = ResolveUrl("~/Images/Icons/error.png");
                        break;
                    case ClientOperationMode.KioskAutomaticNoTerminalOnline:
                        this.lblTitle.Text = string.Format(Resources.strings.Client_has_an_offline_terminal, this.Client.DeviceEntity.Identifier);
                        this.imgClient.ImageUrl = ResolveUrl("~/Images/Icons/error.png");
                        break;
                    case ClientOperationMode.KioskNoDeliverypointConfigured:
                        this.lblTitle.Text = string.Format(Resources.strings.Client_has_no_deliverypoint_configured, this.Client.DeviceEntity.Identifier);
                        this.imgClient.ImageUrl = ResolveUrl("~/Images/Icons/error.png");
                        break;
                    case ClientOperationMode.KioskDisconnectedFromWifi:
                        this.lblTitle.Text = string.Format(Resources.strings.Client_is_disconnected_from_wifi, this.Client.DeviceEntity.Identifier);
                        this.imgClient.ImageUrl = ResolveUrl("~/Images/Icons/error.png");
                        break;
                    case ClientOperationMode.KioskDisconnectedFromWebservice:
                        this.lblTitle.Text = string.Format(Resources.strings.Client_is_disconnected_from_webservice, this.Client.DeviceEntity.Identifier);
                        this.imgClient.ImageUrl = ResolveUrl("~/Images/Icons/error.png");
                        break;
                    case ClientOperationMode.KioskDisconnectedFromPokeIn:
                        this.lblTitle.Text = string.Format(Resources.strings.Client_is_disconnected_from_PokeIn, this.Client.DeviceEntity.Identifier);
                        this.imgClient.ImageUrl = ResolveUrl("~/Images/Icons/error.png");
                        break;
                    case ClientOperationMode.Disabled:
                        this.lblTitle.Text = string.Format(Resources.strings.Client_is_disabled, this.Client.DeviceEntity.Identifier);
                        this.imgClient.ImageUrl = ResolveUrl("~/Images/Icons/exclamation.png");
                        break;
                }

                this.lblLastRequest.Text = string.Format("{0} ({1})", lastRequestValue, timeSinceLastRequest);
                this.lblLastRequest.CssClass = "";
            }
            else if (this.Client.DeviceEntity.LastRequestUTC.HasValue) // Client has last request value but not online
            {
                this.imgClient.ImageUrl = ResolveUrl("~/Images/Icons/error.png");
                this.lblTitle.Text = string.Format(Resources.strings.Client_is_offline_for_x, this.Client.DeviceEntity.Identifier, CraveMobileNocHelper.GetSpanTextBetweenDateTimeAndUtcNow(this.Client.DeviceEntity.LastRequestUTC.Value));

                this.lblLastRequest.Text = string.Format("{0} ({1})", lastRequestValue, timeSinceLastRequest);
                this.lblLastRequest.CssClass = "error";
            }
            else // Client has no last request value
            {
                this.imgClient.ImageUrl = ResolveUrl("~/Images/Icons/exclamation.png");
                this.lblTitle.Text = string.Format(Resources.strings.Client_has_not_been_online_yet, this.Client.DeviceEntity.Identifier);

                this.lblLastRequest.Text = Resources.strings.Unknown;
                this.lblLastRequest.CssClass = "";
            }

            #region LastApplicationVersion

            this.imgLastApplicationVersion.Visible = false;
            
            if (this.Client.DeviceEntity.ApplicationVersion.IsNullOrWhiteSpace()) // Client has no last version
            {
                this.lblLastApplicationVersion.Text = Resources.strings.Unknown;
            }
            else
            {
                this.lblLastApplicationVersion.Text = new VersionNumber(this.Client.DeviceEntity.ApplicationVersion).NumberAsString;
                this.lblLastApplicationVersion.CssClass = "";
                
                VersionNumber.VersionState versionState = VersionNumber.CompareVersions(this.Client.DeviceEntity.ApplicationVersion, this.EmenuReleaseVersion);
                switch (versionState)
                {
                    case VersionNumber.VersionState.Unknown:
                        // Unknown?
                        break;
                    case VersionNumber.VersionState.Newer:
                        this.lblLastApplicationVersion.CssClass = "info";
                        break;
                    case VersionNumber.VersionState.Older:
                        this.imgLastApplicationVersion.ImageUrl = ResolveUrl("~/Images/Icons/exclamation.png");
                        this.imgLastApplicationVersion.Visible = true;
                        break;
                }
            }

            #endregion

            #region LastOsVersion

            this.imgLastOsVersion.Visible = false;

            if (this.Client.DeviceEntity.OsVersion.IsNullOrWhiteSpace())
			{
				this.lblLastOsVersion.Text = Resources.strings.Unknown;
			}
			else
			{
                this.lblLastOsVersion.Text = new VersionNumber(this.Client.DeviceEntity.OsVersion).NumberAsString;
                this.lblLastOsVersion.CssClass = "";

                var companyRelease = ReleaseHelper.GetVersionForCompany(ApplicationCode.CraveOSSamsungP5110, this.Client.CompanyId);
                VersionNumber.VersionState versionState = VersionNumber.CompareVersions(this.Client.DeviceEntity.OsVersion, companyRelease);
				switch (versionState)
				{
					case VersionNumber.VersionState.Unknown:
						this.lblLastOsVersion.Text = Resources.strings.Unknown;
						break;
                    case VersionNumber.VersionState.Newer:
                        this.lblLastOsVersion.CssClass = "info";
                        break;
                    case VersionNumber.VersionState.Older:
						this.imgLastOsVersion.ImageUrl = ResolveUrl("~/Images/Icons/exclamation.png");
						this.imgLastOsVersion.Visible = true;
						break;
				}
            }

            #endregion

            #region LastSupportToolsVersion

            this.imgLastSupportToolsVersion.Visible = false;

            string lastSupportToolsRequest = Resources.strings.Unknown;
            if (this.Client.DeviceEntity.LastSupportToolsRequestUTC.HasValue)
                lastSupportToolsRequest = CraveMobileNocHelper.ConvertUtcDateTimeToUserTimeZone(this.Client.DeviceEntity.LastSupportToolsRequestUTC.Value).ToString(CultureInfo.InvariantCulture);

            string lastSupportToolsVersion = (this.Client.DeviceEntity.SupportToolsVersion.IsNullOrWhiteSpace() ? Resources.strings.Unknown : new VersionNumber(this.Client.DeviceEntity.SupportToolsVersion).NumberAsString);
            string lastSupportToolsIsRunning = (this.Client.DeviceEntity.SupportToolsRunning ? Resources.strings.running : Resources.strings.not_running);
            this.lblLastSupportToolsVersion.Text = string.Format("{0} ({1}) ({2})", lastSupportToolsVersion, lastSupportToolsIsRunning, lastSupportToolsRequest);
            this.lblLastSupportToolsVersion.CssClass = "";

            if (!this.Client.DeviceEntity.SupportToolsVersion.IsNullOrWhiteSpace())
            {
                var companyRelease = ReleaseHelper.GetVersionForCompany(ApplicationCode.SupportTools, this.Client.CompanyId);
                VersionNumber.VersionState versionState = VersionNumber.CompareVersions(this.Client.DeviceEntity.SupportToolsVersion, companyRelease);
                switch (versionState)
                {
                    case VersionNumber.VersionState.Unknown:
                        // Unknown?
                        break;
                    case VersionNumber.VersionState.Newer:
                        this.lblLastSupportToolsVersion.CssClass = "info";
                        break;
                    case VersionNumber.VersionState.Older:
                        this.imgLastSupportToolsVersion.ImageUrl = ResolveUrl("~/Images/Icons/exclamation.png");
                        this.imgLastSupportToolsVersion.Visible = true;
                        break;
                }
            }

            #endregion

            #region LastAgentVersion

            this.imgLastAgentVersion.Visible = false;

            string lastAgentVersion = (this.Client.DeviceEntity.AgentVersion.IsNullOrWhiteSpace() ? Resources.strings.Unknown : new VersionNumber(this.Client.DeviceEntity.AgentVersion).NumberAsString);
            string lastAgentIsRunning = (this.Client.DeviceEntity.AgentRunning ? Resources.strings.running : Resources.strings.not_running);

            this.lblLastAgentVersion.Text = string.Format("{0} ({1})", lastAgentVersion, lastAgentIsRunning);
            this.lblLastAgentVersion.CssClass = "";

            if (!this.Client.DeviceEntity.AgentVersion.IsNullOrWhiteSpace())
            {
                var companyRelease = ReleaseHelper.GetVersionForCompany(ApplicationCode.Agent, this.Client.CompanyId);
                VersionNumber.VersionState versionState = VersionNumber.CompareVersions(this.Client.DeviceEntity.AgentVersion, companyRelease);
                switch (versionState)
                {
                    case VersionNumber.VersionState.Unknown:
                        // Unknown?
                        break;
                    case VersionNumber.VersionState.Newer:
                        this.lblLastAgentVersion.CssClass = "info";
                        break;
                    case VersionNumber.VersionState.Older:
                        this.imgLastAgentVersion.ImageUrl = ResolveUrl("~/Images/Icons/exclamation.png");
                        this.imgLastAgentVersion.Visible = true;
                        break;
                }
            }

            #endregion

            #region LastMessagingServiceVersion

            this.imgLastMessagingServiceVersion.Visible = false;

            string lastMessagingServiceVersion = (this.Client.DeviceEntity.MessagingServiceVersion.IsNullOrWhiteSpace() ? Resources.strings.Unknown : new VersionNumber(this.Client.DeviceEntity.MessagingServiceVersion).NumberAsString);
            //string lastMessagingServiceIsRunning = (this.Client.DeviceEntity.AgentRunning ? Resources.strings.running : Resources.strings.not_running);

            this.lblLastMessagingServiceVersion.Text = string.Format("{0}", lastMessagingServiceVersion);
            this.lblLastMessagingServiceVersion.CssClass = "";

            if (!this.Client.DeviceEntity.MessagingServiceVersion.IsNullOrWhiteSpace())
            {
                var companyRelease = ReleaseHelper.GetVersionForCompany(ApplicationCode.MessagingService, this.Client.CompanyId);
                VersionNumber.VersionState versionState = VersionNumber.CompareVersions(this.Client.DeviceEntity.MessagingServiceVersion, companyRelease);
                switch (versionState)
                {
                    case VersionNumber.VersionState.Unknown:
                        // Unknown?
                        break;
                    case VersionNumber.VersionState.Newer:
                        this.lblLastMessagingServiceVersion.CssClass = "info";
                        break;
                    case VersionNumber.VersionState.Older:
                        this.imgLastMessagingServiceVersion.ImageUrl = ResolveUrl("~/Images/Icons/exclamation.png");
                        this.imgLastMessagingServiceVersion.Visible = true;
                        break;
                }
            }

            #endregion

            // Operation mode
            this.lblOperationMode.Text = string.Format("{0} ({1})", this.Client.OperationModeEnum, this.Client.OperationMode);

            switch (this.Client.OperationModeEnum)
            {
                case ClientOperationMode.Ordering:
                    //this.imgOperationMode.ImageUrl = ResolveUrl("~/Images/Icons/checkmark.png");
                    break;
                case ClientOperationMode.KioskManual:
                case ClientOperationMode.Disabled:
                case ClientOperationMode.Unknown:
                case ClientOperationMode.KioskAutomaticUnprocessedOrders:
                case ClientOperationMode.KioskAutomaticUnprocessableOrders:
                case ClientOperationMode.KioskAutomaticNoTerminalOnline:
                case ClientOperationMode.KioskNoDeliverypointConfigured:
                case ClientOperationMode.KioskDisconnectedFromWifi:
                case ClientOperationMode.KioskDisconnectedFromWebservice:
                case ClientOperationMode.KioskDisconnectedFromPokeIn:
                    this.lblOperationMode.CssClass = "error";
                    break;
            }

            // The client doesn't have the operation mode it should have
            if (this.Client.LastOperationMode.HasValue &&
                this.Client.LastOperationMode != this.Client.OperationMode)
            {
                this.lblOperationMode.Font.Bold = true;
                this.lblOperationMode.CssClass = "orange";

                ClientOperationMode lastOperationMode;
                if (EnumUtil.TryParse(this.Client.LastOperationMode.Value, out lastOperationMode))
                    this.lblOperationMode.Text += string.Format(" / Actual: {0} ({1})", lastOperationMode, (int)lastOperationMode);
                else
                {
                    this.lblOperationMode.CssClass = "error";
                    this.lblOperationMode.Text += " / Actual: UNKNOWN";
                }
            }

            

            // Company (if on generic page)
            if (!this.Request.Url.ToString().Contains("Company.aspx", StringComparison.OrdinalIgnoreCase))
                this.lblCompany.Text = this.Client.CompanyEntity.Name;
            else
                this.plhCompany.Visible = false;

            // IP-/MAC-address
            string ipAddress = (!this.Client.DeviceEntity.PrivateIpAddresses.IsNullOrWhiteSpace() ? this.Client.DeviceEntity.PrivateIpAddresses : Resources.strings.Unknown);
            string macAddress = (this.Client.DeviceId.HasValue && !this.Client.DeviceEntity.Identifier.IsNullOrWhiteSpace() ? this.Client.DeviceEntity.Identifier : Resources.strings.Unknown);
            this.lblIpMacAddress.Text = string.Format("{0} ({1})", ipAddress, macAddress);

            // Battery level
            this.lblBatteryLevel.Text = (!this.Client.DeviceEntity.BatteryLevel.HasValue ? this.Client.DeviceEntity.BatteryLevel + "%" : Resources.strings.Unknown);
            if (this.Client.DeviceEntity.BatteryLevel.HasValue)
            {
                this.lblBatteryLevel.Text = this.Client.DeviceEntity.BatteryLevel.Value + "%";
                this.lblBatteryLevel.CssClass = (this.Client.DeviceEntity.BatteryLevel.Value < 20) ? "error" : "";
            }
            else
            {
                this.lblBatteryLevel.Text = Resources.strings.Unknown;
            }

            if (this.Client.DeviceEntity.IsCharging)
            {
                this.lblBatteryLevel.Text += " (charging)";
            }
            else
            {
                this.lblBatteryLevel.Text += " (de-charging)";
            }

            // Last communication method
            this.lblLastCommunicationMethod.Text = string.Format("{0} ({1})", this.Client.DeviceEntity.CommunicationMethod.ToEnum<ClientCommunicationMethod>(), this.Client.DeviceEntity.CommunicationMethod);

            // Last cloud environment
            this.lblLastCloudEnvironment.Text = this.Client.DeviceEntity.CloudEnvironment;

            // Wifi strength
            if (!this.Client.DeviceEntity.WifiStrength.HasValue)
                this.lblWifiStrength.Text = Resources.strings.Unknown;
            else
            {
                int strength = this.Client.DeviceEntity.WifiStrength.Value;
                if (strength > -55)
                    this.lblWifiStrength.Text = Resources.strings.Wifi_strength_excellent;
                else if (strength > -66)
                    this.lblWifiStrength.Text = Resources.strings.Wifi_strength_good;
                else if (strength > -77)
                    this.lblWifiStrength.Text = Resources.strings.Wifi_strength_average;
                else if (strength > -88)
                    this.lblWifiStrength.Text = Resources.strings.Wifi_strength_poor;
                else
                    this.lblWifiStrength.Text = Resources.strings.Wifi_strength_bad;

                this.lblWifiStrength.Text += string.Format(" ({0})", this.Client.DeviceEntity.WifiStrength.Value);
            }
            
            // Deliverypoint number & Group
            SetDeliverypointNumberAndGroup();
        }
    }

    private void SetDeliverypointNumberAndGroup()
    {
        string dpgName = string.Empty;
        string dpName = string.Empty;
        bool isWarning = false;
        bool isError = false;

        if (!this.Client.LastDeliverypointId.HasValue && !this.Client.DeliverypointId.HasValue)
        {
            isError = true;
        }
        else if (this.Client.DeliverypointId.HasValue)
        {
            // Show DP and DPG
            dpName = this.Client.DeliverypointEntity.Number;
            if (!this.Client.DeliverypointEntity.Name.Equals(this.Client.DeliverypointEntity.Number))
            {
                dpName = string.Format("{0} - {1}", this.Client.DeliverypointEntity.Name, this.Client.DeliverypointEntity.Number);
            }
            dpgName = this.Client.DeliverypointEntity.DeliverypointgroupEntity.Name;

            // Check if DP belongs to DPG (which it should)
            if (this.Client.DeliverypointGroupId.HasValue &&
                this.Client.DeliverypointEntity.DeliverypointgroupId != this.Client.DeliverypointGroupId.Value)
            {
                // Hmmmm mismatch!
                dpgName += " & " + this.Client.DeliverypointgroupEntity.Name;
                isError = true;
            }
        }
        else if (this.Client.DeliverypointGroupId.HasValue)
        {
            dpgName = this.Client.DeliverypointgroupEntity.Name;
            isWarning = true;
        }
        else if (this.Client.LastDeliverypointId != this.Client.DeliverypointId)
        {
            // Actual DeliverypointId is different from Last DeliverypointId (could happen if it's changed in CMS and the Tablet was configured for another DPG)
            dpName = "Actual: {0} ({1}) <br />Cms: {2} ({3})".FormatSafe(
                   this.Client.LastDeliverypointEntity.Name, this.Client.LastDeliverypointEntity.DeliverypointgroupEntity.Name,
                   this.Client.DeliverypointEntity.Name, this.Client.DeliverypointEntity.DeliverypointgroupEntity.Name);

            if (this.Client.LastDeliverypointEntity.DeliverypointgroupId != this.Client.DeliverypointEntity.DeliverypointgroupId)
                isError = true;
            else
                isWarning = true;

            // Fill, so it doesn't get filles automatically
            dpgName = "&nbsp;";
        }

        if (dpName.IsNullOrWhiteSpace() && !this.Client.DeliverypointId.HasValue)
        {
            // Show dp number
            dpName = this.Client.DeliverypointEntity.Number;
            isWarning = true;
        }

        if (!dpName.IsNullOrWhiteSpace() && !dpgName.IsNullOrWhiteSpace())
        {
            // DP and DPG
            this.lblLastDeliverypointNumber.Text = !dpgName.Equals("&nbsp;") ? string.Format("{0} ({1})", dpName, dpgName) : dpName;
        }
        else if (dpName.IsNullOrWhiteSpace() && !dpgName.IsNullOrWhiteSpace())
        {
            // DPG, no DP
            this.lblLastDeliverypointNumber.Text = string.Format("{0} ({1})", Resources.strings.Unknown, dpgName);
        }
        else if (!dpName.IsNullOrWhiteSpace())
        {
            // DP, no DPG
            this.lblLastDeliverypointNumber.Text = string.Format("{0} ({1})", dpName, Resources.strings.Unknown);
        }
        else
            this.lblLastDeliverypointNumber.Text = Resources.strings.Unknown;

        if (isError)
            this.lblLastDeliverypointNumber.CssClass = "error";
        else if (isWarning)
            this.lblLastDeliverypointNumber.CssClass = "warning";
    }

    #endregion
}