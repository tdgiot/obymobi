﻿using System;
using Obymobi.Data.EntityClasses;
using Dionysos;
using Obymobi.Enums;
using Obymobi.Logic.HelperClasses;
using Obymobi.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

public partial class UserControls_Listitems_DeviceActivityLog : System.Web.UI.UserControl
{
    #region Fields

    private bool renderLinkable = true;

    #endregion

    #region Properties

    public Obymobi.Logic.HelperClasses.DeviceActivityLog Activity { get; set; }

    /// <summary>
    /// Gets or sets the renderLinkable
    /// </summary>
    public bool RenderLinkable
    {
        get
        {
            return this.renderLinkable;
        }
        set
        {
            this.renderLinkable = value;
        }
    }

    public bool FullHeight { get; set; }

    #endregion

    #region Methods

    private void RenderNetmessage(NetmessageEntity message)
    {
        if (this.renderLinkable)
        {
            this.plhContainerOpenTags.AddHtml("<li><a class=\"order\" href=\"{0}\">", ResolveUrl(string.Format("~/DeviceActivityLog.aspx?id={0}&entity={1}", message.NetmessageId, ((IEntity)message).LLBLGenProEntityName)));
            this.plhContainerClosingTags.AddHtml("</a></li>");
        }
        else
        {
            this.plhContainerOpenTags.AddHtml("<li class=\"nonlink\"><div class=\"order\">");
            this.plhContainerClosingTags.AddHtml("</div></li>");
        }

        this.plhType.Visible = false;
        this.plhSubmitted.Visible = true;

        this.lblTitle.Text = "{0} ({1})".FormatSafe(message.MessageTypeAsEnum, message.MessageType);
        this.imgLog.ImageUrl = this.ResolveUrl("~/Images/Icons/connect.png");

        this.lblSubmitted.Text = "{0} ({1})".FormatSafe(message.StatusAsEnum.ToString(), message.Status);
        this.lblSubmitted.CssClass = message.StatusAsEnum == NetmessageStatus.Completed ? "" : "orange";
        this.lblSubmitted.Font.Bold = (message.StatusAsEnum != NetmessageStatus.Completed);

        if(message.CreatedUTC.HasValue)
            this.lblCreated.Text = string.Format("{0} ({1})", CraveMobileNocHelper.ConvertUtcDateTimeToUserTimeZone(message.CreatedUTC.Value), CraveMobileNocHelper.GetSpanTextBetweenDateTimeAndUtcNow(message.CreatedUTC.Value));
        this.lblSummary.Text = NetmessageHelper.CreateTypedNetmessageModelFromEntity(message).ToString();
    }

    private void RenderClientLog(ClientLogEntity log)
    {
        if (this.renderLinkable)
        {
            this.plhContainerOpenTags.AddHtml("<li><a class=\"order\" href=\"{0}\">", ResolveUrl(string.Format("~/DeviceActivityLog.aspx?id={0}&entity={1}", log.ClientLogId, ((IEntity)log).LLBLGenProEntityName)));
            this.plhContainerClosingTags.AddHtml("</a></li>");
        }
        else
        {
            this.plhContainerOpenTags.AddHtml("<li class=\"nonlink\"><div class=\"order\">");
            this.plhContainerClosingTags.AddHtml("</div></li>");
        }

        switch (log.TypeEnum)
        {            
            case ClientLogType.FailToKioskForUnprocessedOrders:
                this.imgLog.ImageUrl = ResolveUrl("~/Images/Icons/error.png");
                this.lblTitle.Text = Resources.strings.ClientLog_FailToKioskForUnprocessedOrders;
                break;
            case ClientLogType.FailToKioskNoTerminalOnline:
                this.imgLog.ImageUrl = ResolveUrl("~/Images/Icons/error.png");
                this.lblTitle.Text = Resources.strings.ClientLog_FailToKioskNoTerminalOnline;
                break;
            case ClientLogType.FailToKioskForUnprocessableOrders:
                this.imgLog.ImageUrl = ResolveUrl("~/Images/Icons/error.png");
                this.lblTitle.Text = Resources.strings.ClientLog_FailToKioskForUnprocessableOrders;
                break;
            case ClientLogType.AutomaticReturnToOrdering:
                this.imgLog.ImageUrl = ResolveUrl("~/Images/Icons/start.png");
                this.lblTitle.Text = Resources.strings.ClientLog_AutomaticReturnToOrdering;
                break;
            case ClientLogType.DownloadStarted:
                this.imgLog.ImageUrl = ResolveUrl("~/Images/Icons/application_put.png");
                this.lblTitle.Text = Resources.strings.UpdateStatus_DownloadStarted;
                break;
            case ClientLogType.DownloadCompleted:
                this.imgLog.ImageUrl = ResolveUrl("~/Images/Icons/application_put.png");
                this.lblTitle.Text = Resources.strings.UpdateStatus_DownloadCompleted;
                break;
            case ClientLogType.DownloadFailed:
                this.imgLog.ImageUrl = ResolveUrl("~/Images/Icons/error.png");
                this.lblTitle.Text = Resources.strings.UpdateStatus_DownloadFailed;
                break;
            case ClientLogType.InstallationStarted:
                this.imgLog.ImageUrl = ResolveUrl("~/Images/Icons/application_put.png");
                this.lblTitle.Text = Resources.strings.UpdateStatus_InstallationStarted;
                break;
            case ClientLogType.InstallationCompleted:
                this.imgLog.ImageUrl = ResolveUrl("~/Images/Icons/application_put.png");
                this.lblTitle.Text = Resources.strings.UpdateStatus_InstallationCompleted;
                break;
            case ClientLogType.InstallationFailed:
                this.imgLog.ImageUrl = ResolveUrl("~/Images/Icons/error.png");
                this.lblTitle.Text = Resources.strings.UpdateStatus_InstallationFailed;
                break;
            case ClientLogType.ApplicationStarted:
                this.imgLog.ImageUrl = ResolveUrl("~/Images/Icons/start.png");
                this.lblTitle.Text = Resources.strings.ClientLog_ApplicationStarted;
                break;
            case ClientLogType.ApplicationStopped:
                this.imgLog.ImageUrl = ResolveUrl("~/Images/Icons/stop.png");
                this.lblTitle.Text = Resources.strings.ClientLog_ApplicationStopped;
                break;
            case ClientLogType.ConfigurationBackupRestored:
                this.imgLog.ImageUrl = ResolveUrl("~/Images/Icons/cog_error.png");
                this.lblTitle.Text = Resources.strings.ConfigurationRestored;
                break;
            case ClientLogType.DeliverypointChanged:
                this.imgLog.ImageUrl = ResolveUrl("~/Images/Icons/cog_edit.png");
                this.lblTitle.Text = Resources.strings.ClientLog_DeliverypointChanged;
                break;
            case ClientLogType.DeviceChanged:
                this.imgLog.ImageUrl = ResolveUrl("~/Images/Icons/cog_edit.png");
                this.lblTitle.Text = Resources.strings.ClientLog_DeviceChanged;
                break;
            case ClientLogType.NonQualifiedException:
                this.imgLog.ImageUrl = ResolveUrl("~/Images/Icons/error.png");
                this.lblTitle.Text = Resources.strings.ClientLog_NonQualifiedException;
                break;
            case ClientLogType.OsUpdated:
                this.imgLog.ImageUrl = ResolveUrl("~/Images/Icons/application_put.png");
                this.lblTitle.Text = Resources.strings.ClientLog_OsUpdated;
                break;
            case ClientLogType.ReceivedLog:
            case ClientLogType.ShippedLog:
                this.imgLog.ImageUrl = ResolveUrl("~/Images/Icons/report.png");
                this.lblTitle.Text = Resources.strings.ClientLog_ShippedLog;
                break;
            case ClientLogType.DeviceReboot:
                this.imgLog.ImageUrl = ResolveUrl("~/Images/Icons/stop.png");
                this.lblTitle.Text = Resources.strings.ClientLog_DeviceReboot;
                break;
            case ClientLogType.DeviceShutdown:
                this.imgLog.ImageUrl = ResolveUrl("~/Images/Icons/stop.png");
                this.lblTitle.Text = Resources.strings.ClientLog_DeviceShutdown;
                break;
            default:
                this.imgLog.ImageUrl = ResolveUrl("~/Images/Icons/message.png");
                this.lblTitle.Text = Resources.strings.ClientLog_Message;
                break;
        }

        if (log.FromOperationMode != log.ToOperationMode && log.FromOperationModeEnum != ClientOperationMode.Unknown)
        {
            this.lblSummary.Text = "Operation Mode: {0} ({1}) &gt; {1} ({2})<br />".FormatSafe(
                log.FromOperationModeName,
                log.FromOperationMode,
                log.ToOperationModeName,
                log.ToOperationMode);
        }
        
        this.lblSummary.Text += log.Message.Truncate(255);

        this.lblType.Text = string.Format("{0} ({1})", log.TypeName, log.Type);
        if(log.CreatedUTC.HasValue)
            this.lblCreated.Text = string.Format("{0} ({1})", CraveMobileNocHelper.ConvertUtcDateTimeToUserTimeZone(log.CreatedUTC.Value), CraveMobileNocHelper.GetSpanTextBetweenDateTimeAndUtcNow(log.CreatedUTC.Value));
    }

    private void RenderClientState(ClientStateEntity clientState)
    {
        if (this.renderLinkable)
        {
            this.plhContainerOpenTags.AddHtml("<li><a class=\"order\" href=\"{0}\">", ResolveUrl(string.Format("~/DeviceActivityLog.aspx?id={0}&entity={1}", clientState.ClientStateId, ((IEntity)clientState).LLBLGenProEntityName)));
            this.plhContainerClosingTags.AddHtml("</a></li>");
        }
        else
        {
            this.plhContainerOpenTags.AddHtml("<li class=\"nonlink\"><div class=\"order\">");
            this.plhContainerClosingTags.AddHtml("</div></li>");
        }

        this.plhType.Visible = false;
        this.plhSubmitted.Visible = false;

        this.lblTitle.Text = clientState.Message;

        this.imgLog.ImageUrl = this.ResolveUrl(clientState.Online ? "~/Images/Icons/checkmark.png" : "~/Images/Icons/error.png");

        if (clientState.CreatedUTC.HasValue)
            this.lblCreated.Text = string.Format("{0} ({1})", CraveMobileNocHelper.ConvertUtcDateTimeToUserTimeZone(clientState.CreatedUTC.Value), CraveMobileNocHelper.GetSpanTextBetweenDateTimeAndUtcNow(clientState.CreatedUTC.Value));

        this.lblSummary.Text = clientState.ToString(); // Overridden
    }

    private void RenderTerminalLog(TerminalLogEntity log)
    {
        if (this.renderLinkable)
        {
            this.plhContainerOpenTags.AddHtml("<li><a class=\"order\" href=\"{0}\">", ResolveUrl(string.Format("~/DeviceActivityLog.aspx?id={0}&entity={1}", log.TerminalLogId, ((IEntity)log).LLBLGenProEntityName)));
            this.plhContainerClosingTags.AddHtml("</a></li>");
        }
        else
        {
            this.plhContainerOpenTags.AddHtml("<li class=\"nonlink\"><div class=\"order\">");
            this.plhContainerClosingTags.AddHtml("</div></li>");
        }

        string logMessage = this.FullHeight ? log.Message : log.Message.Truncate(75);
        string logLog = this.FullHeight ? log.Log : log.Log.Truncate(256);
        logLog = logLog.HtmlEncode().ReplaceLineBreakWithBR();

        switch (log.TypeAsEnum)
        {
            case TerminalLogType.Start:
                this.imgLog.ImageUrl = ResolveUrl("~/Images/Icons/start.png");
                this.lblTitle.Text = logMessage;
                break;
            case TerminalLogType.Stop:
                this.imgLog.ImageUrl = ResolveUrl("~/Images/Icons/stop.png");
                this.lblTitle.Text = logMessage;
                break;
            case TerminalLogType.ShippedLog:
                this.imgLog.ImageUrl = ResolveUrl("~/Images/Icons/report.png");
                this.lblTitle.Text = logMessage;
                break;
            case TerminalLogType.Message:
                this.imgLog.ImageUrl = ResolveUrl("~/Images/Icons/message.png");
                this.lblTitle.Text = logMessage;
                break;
            case TerminalLogType.PosSynchronisationReport:
                this.imgLog.ImageUrl = ResolveUrl("~/Images/Icons/report.png");
                this.lblTitle.Text = logMessage;
                break;
            case TerminalLogType.DownloadStarted:
                this.imgLog.ImageUrl = ResolveUrl("~/Images/Icons/application_put.png");
                this.lblTitle.Text = Resources.strings.UpdateStatus_DownloadStarted;
                break;
            case TerminalLogType.ConfigurationBackupRestored:
                this.imgLog.ImageUrl = ResolveUrl("~/Images/Icons/cog_error.png");
                this.lblTitle.Text = Resources.strings.ConfigurationRestored;
                break;
            case TerminalLogType.DownloadCompleted:
                this.imgLog.ImageUrl = ResolveUrl("~/Images/Icons/application_put.png");
                this.lblTitle.Text = Resources.strings.UpdateStatus_DownloadCompleted;
                break;
            case TerminalLogType.DownloadFailed:
                this.imgLog.ImageUrl = ResolveUrl("~/Images/Icons/error.png");
                this.lblTitle.Text = Resources.strings.UpdateStatus_DownloadFailed;
                break;
            case TerminalLogType.InstallationStarted:
                this.imgLog.ImageUrl = ResolveUrl("~/Images/Icons/application_put.png");
                this.lblTitle.Text = Resources.strings.UpdateStatus_InstallationStarted;
                break;
            case TerminalLogType.InstallationCompleted:
                this.imgLog.ImageUrl = ResolveUrl("~/Images/Icons/application_put.png");
                this.lblTitle.Text = Resources.strings.UpdateStatus_InstallationCompleted;
                break;
            case TerminalLogType.InstallationFailed:
                this.imgLog.ImageUrl = ResolveUrl("~/Images/Icons/error.png");
                this.lblTitle.Text = Resources.strings.UpdateStatus_InstallationFailed;
                break;
            case TerminalLogType.DeviceChanged:
                this.imgLog.ImageUrl = ResolveUrl("~/Images/Icons/cog_edit.png");
                this.lblTitle.Text = Resources.strings.TerminalLog_DeviceChanged;
                break;
            case TerminalLogType.DeviceReboot:
                this.imgLog.ImageUrl = ResolveUrl("~/Images/Icons/stop.png");
                this.lblTitle.Text = Resources.strings.TerminalLog_DeviceReboot;
                break;
            case TerminalLogType.DeviceShutdown:
                this.imgLog.ImageUrl = ResolveUrl("~/Images/Icons/stop.png");
                this.lblTitle.Text = Resources.strings.TerminalLog_DeviceShutdown;
                break;
            case TerminalLogType.NonQualifiedException:
            case TerminalLogType.OrderTypeNotSupported:
            case TerminalLogType.NonQualifiedPosException:
            case TerminalLogType.DeliverypointLockedPosException:
            case TerminalLogType.InvalidProductsOrAlterationsPosException:
            case TerminalLogType.UnlockTablesRequestFailedPosException:
            case TerminalLogType.ConnectivityException:
            case TerminalLogType.UnspecifiedNonFatalPosProblem:
                this.imgLog.ImageUrl = ResolveUrl("~/Images/Icons/error.png");
                this.lblTitle.Text = logMessage;
                break;
        }

        this.lblSummary.Text = logLog;
        this.lblType.Text = string.Format("{0} ({1})", log.TypeName, log.Type);

        if(log.CreatedUTC.HasValue)
            this.lblCreated.Text = string.Format("{0} ({1})", CraveMobileNocHelper.ConvertUtcDateTimeToUserTimeZone(log.CreatedUTC.Value), CraveMobileNocHelper.GetSpanTextBetweenDateTimeAndUtcNow(log.CreatedUTC.Value));       
    }

    private void RenderTerminalState(TerminalStateEntity terminalState)
    {
        if (this.renderLinkable)
        {
            this.plhContainerOpenTags.AddHtml("<li><a class=\"order\" href=\"{0}\">", ResolveUrl(string.Format("~/DeviceActivityLog.aspx?id={0}&entity={1}", terminalState.TerminalStateId, ((IEntity)terminalState).LLBLGenProEntityName)));
            this.plhContainerClosingTags.AddHtml("</a></li>");
        }
        else
        {
            this.plhContainerOpenTags.AddHtml("<li class=\"nonlink\"><div class=\"order\">");
            this.plhContainerClosingTags.AddHtml("</div></li>");
        }

        this.plhType.Visible = false;
        this.plhSubmitted.Visible = false;

        this.lblTitle.Text = terminalState.Message;

        this.imgLog.ImageUrl = this.ResolveUrl(terminalState.Online ? "~/Images/Icons/checkmark.png" : "~/Images/Icons/error.png");

        if (terminalState.CreatedUTC.HasValue)
            this.lblCreated.Text = string.Format("{0} ({1})", CraveMobileNocHelper.ConvertUtcDateTimeToUserTimeZone(terminalState.CreatedUTC.Value), CraveMobileNocHelper.GetSpanTextBetweenDateTimeAndUtcNow(terminalState.CreatedUTC.Value));

        this.lblSummary.Text = terminalState.ToString(); // Overridden
    }

    #endregion

    #region Event handlers

    protected void Page_Load(object sender, EventArgs e)
    {
        if (this.Activity == null)
            return;

        if (this.Activity.Entity.LLBLGenProEntityTypeValue == (int)EntityType.NetmessageEntity)
        { 
            // Netmessage
            this.RenderNetmessage((NetmessageEntity)this.Activity.Entity);
        }
        else if (this.Activity.Entity.LLBLGenProEntityTypeValue == (int)EntityType.ClientLogEntity)
        { 
            // ClientLog
            this.RenderClientLog((ClientLogEntity)this.Activity.Entity);
        }
        else if (this.Activity.Entity.LLBLGenProEntityTypeValue == (int)EntityType.ClientStateEntity)
        {
            // ClientState
            this.RenderClientState((ClientStateEntity)this.Activity.Entity);
        }
        else if (this.Activity.Entity.LLBLGenProEntityTypeValue == (int)EntityType.TerminalLogEntity)
        {
            // TerminalLog
            this.RenderTerminalLog((TerminalLogEntity)this.Activity.Entity);
        }
        else if (this.Activity.Entity.LLBLGenProEntityTypeValue == (int)EntityType.TerminalStateEntity)
        {
            // TerminalState
            this.RenderTerminalState((TerminalStateEntity)this.Activity.Entity);
        }

        if (this.FullHeight)
            this.divSummary.Attributes.Add("style", "max-height:none");
    }

    #endregion
}