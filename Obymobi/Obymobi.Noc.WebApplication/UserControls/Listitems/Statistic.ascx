﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="UserControls_Listitems_Statistic" Codebehind="Statistic.ascx.cs" %>
<ul class="details statistics">
    <li>
        <div class="label"><D:LabelTextOnly ID="lblOrdersLastHourLabel" runat="server" Text="<%$ Resources:strings, Statistics_orders_last_hour %>"></D:LabelTextOnly>:</div>
        <div class="value"><strong><D:LabelTextOnly ID="lblOrdersLastHour" runat="server"></D:LabelTextOnly></strong></div>
    </li>
    <li>
        <div class="label"><D:LabelTextOnly ID="lblOrdersTodayLabel" runat="server" Text="<%$ Resources:strings, Statistics_orders_today %>"></D:LabelTextOnly>:</div>
        <div class="value"><strong><D:LabelTextOnly ID="lblOrdersToday" runat="server"></D:LabelTextOnly></strong></div>
    </li>
    <li>
        <div class="label"><D:LabelTextOnly ID="lblOrdersWeekLabel" runat="server" Text="<%$ Resources:strings, Statistics_orders_week %>"></D:LabelTextOnly>:</div>
        <div class="value"><strong><D:LabelTextOnly ID="lblOrdersWeek" runat="server"></D:LabelTextOnly></strong> (<D:Label ID="lblWeekBalanceThisWeek" runat="server"></D:Label>)</div>
    </li>
    <li>
        <div class="label"><D:LabelTextOnly ID="lblOrdersTwoWeeksAgoLabel" runat="server" Text="<%$ Resources:strings, Statistics_orders_two_weeks_ago %>"></D:LabelTextOnly>:</div>
        <div class="value"><strong><D:LabelTextOnly ID="lblOrdersTwoWeeksAgo" runat="server"></D:LabelTextOnly></strong> (<D:Label ID="lblWeekBalanceTwoWeeksAgo" runat="server"></D:Label>)</div>
    </li>
    <li>
        <div class="label"><D:LabelTextOnly ID="lblOrdersThreeWeeksAgoLabel" runat="server" Text="<%$ Resources:strings, Statistics_orders_three_weeks_ago %>"></D:LabelTextOnly>:</div>
        <div class="value"><strong><D:LabelTextOnly ID="lblOrdersThreeWeeksAgo" runat="server"></D:LabelTextOnly></strong> (<D:Label ID="lblWeekBalanceThreeWeeksAgo" runat="server"></D:Label>)</div>
    </li>
    <li>
        <div class="label"><D:LabelTextOnly ID="lblOrdersFourWeeksAgoLabel" runat="server" Text="<%$ Resources:strings, Statistics_orders_four_weeks_ago %>"></D:LabelTextOnly>:</div>
        <div class="value"><strong><D:LabelTextOnly ID="lblOrdersFourWeeksAgo" runat="server"></D:LabelTextOnly></strong> (<D:Label ID="lblWeekBalanceFourWeeksAgo" runat="server"></D:Label>)</div>
    </li>
    <li>
        <div class="label"><D:LabelTextOnly ID="lblOrdersFiveWeeksAgoLabel" runat="server" Text="<%$ Resources:strings, Statistics_orders_five_weeks_ago %>"></D:LabelTextOnly>:</div>
        <div class="value"><strong><D:LabelTextOnly ID="lnlOrdersFiveWeeksAgo" runat="server"></D:LabelTextOnly></strong></div>
    </li>
</ul>