﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="UserControls_Listitems_Client" Codebehind="Client.ascx.cs" %>
<D:PlaceHolder runat="server" ID="plhContainerOpenTags"></D:PlaceHolder>    
        <D:Image ID="imgClient" runat="server" />
        <div class="title"><D:Label ID="lblTitle" runat="server"></D:Label></div>
        <D:PlaceHolder runat="server" ID="plhCompany">
        <div class="label"><D:LabelTextOnly ID="lblCompanyLabel" runat="server" Text="<%$ Resources:strings, Company %>"></D:LabelTextOnly>:</div>
        <div class="value"><D:Label ID="lblCompany" runat="server"></D:Label></div>
        </D:PlaceHolder>
        <div class="label"><D:LabelTextOnly ID="lblLastDeliverypointNumberLabel" runat="server" Text="<%$ Resources:strings, Deliverypoint %>"></D:LabelTextOnly>:</div>
        <div class="value"><D:Label ID="lblLastDeliverypointNumber" runat="server"></D:Label></div>
        <div class="label"><D:LabelTextOnly ID="lblOperationModeLabel" runat="server" Text="<%$ Resources:strings, Client_operation_mode %>"></D:LabelTextOnly>:</div>
        <div class="value"><D:Label ID="lblOperationMode" runat="server"></D:Label></div>
        <div class="label"><D:LabelTextOnly ID="lblLastCommunicationMethodLabel" runat="server" Text="<%$ Resources:strings, Client_last_communication_method %>"></D:LabelTextOnly>:</div>
        <div class="value"><D:Label ID="lblLastCommunicationMethod" runat="server"></D:Label></div>
        <div class="label"><D:LabelTextOnly ID="lblLastCloudEnvironmentLabel" runat="server" Text="<%$ Resources:strings, Cloud_Environment %>"></D:LabelTextOnly>:</div>
        <div class="value"><D:Label ID="lblLastCloudEnvironment" runat="server"></D:Label></div>
        <div class="label"><D:LabelTextOnly ID="lblLastRequestLabel" runat="server" Text="<%$ Resources:strings, Client_last_request %>"></D:LabelTextOnly>:</div>
        <div class="value"><D:Label ID="lblLastRequest" runat="server"></D:Label></div>
        <div class="label"><D:LabelTextOnly ID="lblBatteryLevelLabel" runat="server" Text="<%$ Resources:strings, Client_battery_level %>"></D:LabelTextOnly>:</div>
        <div class="value"><D:Label ID="lblBatteryLevel" runat="server"></D:Label></div>
        <div class="label"><D:LabelTextOnly ID="lblWifiStrengthLabel" runat="server" Text="<%$ Resources:strings, Client_wifi_strength %>"></D:LabelTextOnly>:</div>
        <div class="value"><D:Label ID="lblWifiStrength" runat="server"></D:Label></div>
        <div class="label"><D:LabelTextOnly ID="lblLastOsVersionLabel" runat="server" Text="<%$ Resources:strings, OS %>"></D:LabelTextOnly>:</div>
        <div class="value"><D:Label ID="lblLastOsVersion" runat="server"></D:Label></div>
        <D:Image ID="imgLastOsVersion" runat="server" />
        <div class="label"><D:LabelTextOnly ID="lblLastApplicationVersionLabel" runat="server" Text="<%$ Resources:strings, Application %>"></D:LabelTextOnly>:</div>
        <div class="value"><D:Label ID="lblLastApplicationVersion" runat="server"></D:Label></div>
        <D:Image ID="imgLastApplicationVersion" runat="server" />        
        <div class="label"><D:LabelTextOnly ID="lblLastAgentVersionLabel" runat="server" Text="<%$ Resources:strings, Agent %>"></D:LabelTextOnly>:</div>
        <div class="value"><D:Label ID="lblLastAgentVersion" runat="server"></D:Label></div>
        <D:Image ID="imgLastAgentVersion" runat="server" />
        <div class="label"><D:LabelTextOnly ID="lblLastSupportToolsVersionLabel" runat="server" Text="<%$ Resources:strings, SupportTools %>"></D:LabelTextOnly>:</div>
        <div class="value"><D:Label ID="lblLastSupportToolsVersion" runat="server"></D:Label></div>
        <D:Image ID="imgLastSupportToolsVersion" runat="server" />
        <div class="label"><D:LabelTextOnly ID="lblLastMessagingServiceVersionLabel" runat="server" Text="<%$ Resources:strings, MessagingService %>"></D:LabelTextOnly>:</div>
        <div class="value"><D:Label ID="lblLastMessagingServiceVersion" runat="server"></D:Label></div>
        <D:Image ID="imgLastMessagingServiceVersion" runat="server" />
        <div class="label"><D:LabelTextOnly ID="lblIpMacAddressLabel" runat="server" Text="<%$ Resources:strings, Client_IP_MAC_address %>"></D:LabelTextOnly>:</div>
        <div class="value"><D:Label ID="lblIpMacAddress" runat="server"></D:Label></div>
<D:PlaceHolder runat="server" ID="plhContainerClosingTags"></D:PlaceHolder>