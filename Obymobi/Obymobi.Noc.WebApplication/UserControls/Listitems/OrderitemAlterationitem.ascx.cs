﻿using System;
using Dionysos;
using Obymobi.Data.EntityClasses;
using Obymobi.Enums;
using Obymobi.Extensions;

public partial class UserControls_Listitems_OrderitemAlterationitem : System.Web.UI.UserControl
{
    #region Properties

    public OrderitemAlterationitemEntity OrderitemAlterationitem { get; set; }

    #endregion

    #region Event handlers

    protected void Page_Load(object sender, EventArgs e)
    {
        // Note [GK]: If you change something here, you need to do that in Orderitem.ascx.cs as well!
        if (this.OrderitemAlterationitem != null)
        {
            OrderitemAlterationitemEntity orderitemAlterationitemEntity = this.OrderitemAlterationitem;

            string alterationName;
            string alterationOptionName = string.Empty;

            if (!orderitemAlterationitemEntity.AlterationName.IsNullOrWhiteSpace())
            {
                alterationName = orderitemAlterationitemEntity.AlterationName;
            }
            else if (orderitemAlterationitemEntity.AlterationitemId.HasValue)
            {
                if (!orderitemAlterationitemEntity.AlterationitemEntity.AlterationName.IsNullOrWhiteSpace())
                {
                    alterationName = orderitemAlterationitemEntity.AlterationitemEntity.AlterationName;
                }
                else
                {
                    alterationName = orderitemAlterationitemEntity.AlterationitemEntity.AlterationEntity.Name;
                }
            }
            else
            {
                alterationName = "????";
            }

            if (orderitemAlterationitemEntity.AlterationType == (int)AlterationType.Form)
            {
                if (!orderitemAlterationitemEntity.AlterationoptionName.IsNullOrWhiteSpace())
                {
                    alterationOptionName = orderitemAlterationitemEntity.AlterationoptionName + ": ";
                }

                if (!orderitemAlterationitemEntity.Value.IsNullOrWhiteSpace())
                {
                    alterationOptionName += orderitemAlterationitemEntity.Value;
                }
            }
            else if (!orderitemAlterationitemEntity.AlterationoptionName.IsNullOrWhiteSpace())
            {
                alterationOptionName = orderitemAlterationitemEntity.AlterationoptionName;
            }
            else if (orderitemAlterationitemEntity.AlterationitemId.HasValue && orderitemAlterationitemEntity.AlterationitemId.Value > 0)
            {
                if (!orderitemAlterationitemEntity.AlterationitemEntity.AlterationoptionName.IsNullOrWhiteSpace())
                {
                    alterationOptionName = orderitemAlterationitemEntity.AlterationitemEntity.AlterationoptionName;
                }
                else
                {
                    alterationOptionName = orderitemAlterationitemEntity.AlterationitemEntity.AlterationoptionEntity.Name;
                }
            }
            else if (((AlterationType)orderitemAlterationitemEntity.AlterationType).IsIn(AlterationType.Date, AlterationType.DateTime)
                && orderitemAlterationitemEntity.TimeUTC.HasValue)
            {
                CompanyEntity companyEntity = new CompanyEntity(orderitemAlterationitemEntity.ParentCompanyId);
                DateTime localDateTime = orderitemAlterationitemEntity.TimeUTC.Value.UtcToLocalTime(companyEntity.TimeZoneInfo);

                if (OrderitemAlterationitem.AlterationType == (int)AlterationType.DateTime)
                {
                    alterationName = localDateTime.ToString("dd/MM/yyyy HH:mm");
                }
                else
                {
                    alterationName = localDateTime.ToString("dd/MM/yyyy");
                }
            }
            else if (!orderitemAlterationitemEntity.Value.IsNullOrWhiteSpace())
            {
                alterationOptionName = orderitemAlterationitemEntity.Value;
            }
            else
            {
                alterationOptionName = "????";
            }

            string nameAndOption = string.Format("{0}: <em>{1}</em>", alterationName, alterationOptionName);
            this.lblTitle.Text = nameAndOption;
        }
    }

    #endregion
}
