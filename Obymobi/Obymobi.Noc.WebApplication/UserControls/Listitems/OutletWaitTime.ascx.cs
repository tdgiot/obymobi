﻿using System;
using Obymobi.Data.EntityClasses;

public partial class UserControls_Listitems_OutletWaitTime : System.Web.UI.UserControl
{
	public OutletEntity Outlet { get; set; }

	protected void Page_Load(object sender, EventArgs e)
	{
		if (Outlet == null)
		{
			return;
		}

		lblNameValue.Text = this.Outlet.Name;

        if (Outlet.OutletOperationalStateEntity?.WaitTime.HasValue ?? false)
		{
			WaitTime.Value = Outlet.OutletOperationalStateEntity.WaitTime.Value.TotalMinutes.ToString();
		}
	}

    internal void Save()
    {		
		int waitTime;

		if(Int32.TryParse(this.WaitTime.Value, out waitTime))
        {
			Outlet.OutletOperationalStateEntity.WaitTime = TimeSpan.FromMinutes(waitTime);
			Outlet.OutletOperationalStateEntity.Save();
		}
	}
}
