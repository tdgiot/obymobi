﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="UserControls_Listitems_Orderitem" Codebehind="Orderitem.ascx.cs" %>
<li>
    <D:HyperLink ID="hlOrderitem" runat="server">
        <D:Image ID="imgOrderitem" runat="server" />
        <div class="subtitle"><D:Label ID="lblTitle" runat="server"></D:Label></div>
        <div class="value"><D:LabelTextOnly ID="lblAlterations" runat="server"></D:LabelTextOnly></div>
    </D:HyperLink>
</li>