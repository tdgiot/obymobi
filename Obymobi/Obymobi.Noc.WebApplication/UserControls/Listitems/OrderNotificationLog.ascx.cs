﻿using System;
using Obymobi.Data.EntityClasses;
using Obymobi.Enums;
using Dionysos;
using System.Web;

public partial class UserControls_Listitems_OrderNotificationLog : System.Web.UI.UserControl
{
    #region Properties

    public OrderNotificationLogEntity OrderNotificationLog { get; set; }
    
    #endregion

    public UserControls_Listitems_OrderNotificationLog()
    {
    }

    #region Event handlers

    protected void Page_Load(object sender, EventArgs e)
    {
        if (this.OrderNotificationLog == null)
        {
            return;
        }

        DateTime createdLocalTime = CraveMobileNocHelper.ConvertUtcDateTimeToUserTimeZone(this.OrderNotificationLog.CreatedUTC);
        
        this.lblTitle.Text = $@"{this.OrderNotificationLog.Type.ToString()} - {this.OrderNotificationLog.Status.ToString()}";
        this.lblType.Text = this.OrderNotificationLog.Type.ToString();
        this.lblMethod.Text = this.OrderNotificationLog.Method.ToString();
        this.lblStatus.Text = this.OrderNotificationLog.Status.ToString();
        this.lblReceiver.Text = this.OrderNotificationLog.Receiver;
            this.lblSubject.Text = this.OrderNotificationLog.Method == OrderNotificationMethod.Email
            ? this.OrderNotificationLog.Subject
            : HttpUtility.HtmlEncode(this.OrderNotificationLog.Message);
        this.lblLog.Text = HttpUtility.HtmlEncode(this.OrderNotificationLog.Log).ReplaceLineBreakWithBR();
        this.lblCreated.Text = createdLocalTime.ToString("F");

        switch (this.OrderNotificationLog.Status)
        {
            case OrderNotificationLogStatus.Pending:
                this.imgStatus.ImageUrl = ResolveUrl("~/Images/Icons/hourglass.png");
                break;
            case OrderNotificationLogStatus.Success:
                this.imgStatus.ImageUrl = ResolveUrl("~/Images/Icons/checkmark.png");
                break;
            default:
                this.imgStatus.ImageUrl = ResolveUrl("~/Images/Icons/error.png");
                break;
        }
    }
    #endregion
}
