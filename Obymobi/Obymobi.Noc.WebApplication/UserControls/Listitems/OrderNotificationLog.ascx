﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="UserControls_Listitems_OrderNotificationLog" Codebehind="OrderNotificationLog.ascx.cs" %>
<li>
    <D:HyperLink ID="hlPaymentTransaction" runat="server">
        <D:Image ID="imgStatus" runat="server" />
        <div class="title"><D:Label ID="lblTitle" runat="server"></D:Label></div>
        <div class="label"><D:LabelTextOnly ID="lblTypeLabel" runat="server" Text="<%$ Resources:strings, OrderNotification_Type %>"></D:LabelTextOnly>:</div>
        <div class="value"><D:Label ID="lblType" runat="server"></D:Label></div>
        <div class="label"><D:LabelTextOnly ID="lblMethodLabel" runat="server" Text="<%$ Resources:strings, OrderNotification_Method %>"></D:LabelTextOnly>:</div>
        <div class="value"><D:Label ID="lblMethod" runat="server"></D:Label></div>
        <div class="label"><D:LabelTextOnly ID="lblStatusLabel" runat="server" Text="<%$ Resources:strings, Status %>"></D:LabelTextOnly>:</div>
        <div class="value"><D:Label ID="lblStatus" runat="server"></D:Label></div>
        <div class="label"><D:LabelTextOnly ID="lblReceiverLabel" runat="server" Text="<%$ Resources:strings, OrderNotification_Receiver %>"></D:LabelTextOnly>:</div>
        <div class="value"><D:Label ID="lblReceiver" runat="server" RenderAsLinkIfLinkable="False"></D:Label></div>
        <div class="label"><D:LabelTextOnly ID="lblSubjectLabel" runat="server" Text="<%$ Resources:strings, OrderNotification_Subject %>"></D:LabelTextOnly>:</div>
        <div class="value"><D:Label ID="lblSubject" runat="server"></D:Label></div>
        <div class="label"><D:LabelTextOnly ID="lblLogLabel" runat="server" Text="<%$ Resources:strings, OrderNotification_Log %>"></D:LabelTextOnly>:</div>
        <div class="value"><D:Label ID="lblLog" runat="server"></D:Label></div>
        <div class="label"><D:LabelTextOnly ID="lblCreatedLabel" runat="server" Text="<%$ Resources:strings, Order_created %>"></D:LabelTextOnly>:</div>
        <div class="value"><D:Label ID="lblCreated" runat="server"></D:Label></div>
    </D:HyperLink>
</li>
