﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="UserControls_Listitems_PaymentTransaction" Codebehind="PaymentTransaction.ascx.cs" %>
<li>
    <D:HyperLink ID="hlPaymentTransaction" runat="server">
        <D:Image ID="imgStatus" runat="server" />
        <div class="title"><D:Label ID="lblTitle" runat="server"></D:Label></div>
        <div class="label"><D:LabelTextOnly ID="lblStatusLabel" runat="server" Text="<%$ Resources:strings, Status %>"></D:LabelTextOnly>:</div>
        <div class="value"><D:Label ID="lblStatus" runat="server"></D:Label></div>
        <div class="label"><D:LabelTextOnly ID="lblPaymentProviderLabel" runat="server" Text="<%$ Resources:strings, PaymentProvider %>"></D:LabelTextOnly>:</div>
        <div class="value"><D:Label ID="lblPaymentProvider" runat="server"></D:Label></div>
        <div class="label"><D:LabelTextOnly ID="lblEnvironmentLabel" runat="server" Text="<%$ Resources:strings, Environment %>"></D:LabelTextOnly>:</div>
        <div class="value"><D:Label ID="lblEnvironment" runat="server"></D:Label></div>
        <div class="label"><D:LabelTextOnly ID="lblPaymentMethodLabel" runat="server" Text="<%$ Resources:strings, Payment_Method %>"></D:LabelTextOnly>:</div>
        <div class="value"><D:Label ID="lblPaymentMethod" runat="server"></D:Label></div>
        <div class="label"><D:LabelTextOnly ID="lblReferenceLabel" runat="server" Text="<%$ Resources:strings, Payment_ReferenceId %>"></D:LabelTextOnly>:</div>
        <div class="value"><D:Label ID="lblReferenceId" runat="server"></D:Label></div>
        <div class="label"><D:LabelTextOnly ID="lblMerchantRefLabel" runat="server" Text="<%$ Resources:strings, Payment_MerchantReference %>"></D:LabelTextOnly>:</div>
        <div class="value"><D:Label ID="lblMerchantRef" runat="server"></D:Label></div>
        <div class="label"><D:LabelTextOnly ID="lblMerchantAccountLabel" runat="server" Text="<%$ Resources:strings, Payment_MerchantAccount %>"></D:LabelTextOnly>:</div>
        <div class="value"><D:Label ID="lblMerchantAccount" runat="server"></D:Label></div>
        <div class="label"><D:LabelTextOnly ID="lblCreatedLabel" runat="server" Text="<%$ Resources:strings, Order_created %>"></D:LabelTextOnly>:</div>
        <div class="value"><D:Label ID="lblCreated" runat="server"></D:Label></div>
    </D:HyperLink>
</li>
