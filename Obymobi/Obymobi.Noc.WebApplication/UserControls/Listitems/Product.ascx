﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="UserControls_Listitems_Product" Codebehind="Product.ascx.cs" %>

<li data-id="<%= this.ProductEntity.ProductId %>" class="post">
    <div class="container">
        <div class="box-button"><D:Button ID="btnVisibility" UseSubmitBehavior="false" runat="server" /></div>
        <div class="box-title"><D:Label ID="lblNameValue" CssClass="title" runat="server" /></div>
    </div>
    <div class="box-categories"><D:Label ID="lblCategories" CssClass="category pre" runat="server" /></div>
</li>
