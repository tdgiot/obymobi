﻿using System;
using Obymobi.Data.EntityClasses;

public partial class UserControls_Listitems_Deliverypointgroup : System.Web.UI.UserControl
{
    #region Properties

    public DeliverypointgroupEntity Deliverypointgroup { get; set; }

    #endregion

    #region Event handlers

    protected void Page_Load(object sender, EventArgs e)
    {
        if (this.Deliverypointgroup != null)
        {
            this.hlDeliverypointgroup.NavigateUrl = ResolveUrl(string.Format("~/Deliverypointgroup.aspx?id={0}", this.Deliverypointgroup.DeliverypointgroupId));

            this.lblTitle.Text = this.Deliverypointgroup.Name;
            this.lblDeliverypointCount.Text = this.Deliverypointgroup.DeliverypointCollection.Count.ToString();
        }
    }

    #endregion
}