﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="UserControls_Listitems_Order" Codebehind="Order.ascx.cs" %>
<%@ Register TagPrefix="dx" Namespace="DevExpress.Web" Assembly="DevExpress.Web.v20.1, Version=20.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" %>
<li>
    <D:HyperLink ID="hlOrder" runat="server">
        <D:Image ID="imgOrder" runat="server" />
        <div class="title"><D:Label ID="lblTitle" runat="server"></D:Label></div>
        <div class="label"><D:LabelTextOnly ID="lblCreatedLabel" runat="server" Text="<%$ Resources:strings, Order_created %>"></D:LabelTextOnly>:</div>
        <div class="value"><D:Label ID="lblCreated" runat="server"></D:Label></div>
        <div class="label"><D:LabelTextOnly ID="lblStatusLabel" runat="server" Text="<%$ Resources:strings, Status %>"></D:LabelTextOnly>:</div>
        <div class="value"><D:Label ID="lblStatus" runat="server"></D:Label>
            <% if (IsWaitingForPayment) { %>
                <D:PlaceHolder ID="plhWaitingForPaymentPh" runat="server" Visible="true">
                    <div class="value"><D:PlaceHolder ID="plhWaitingForPaymentBtn" runat="server"></D:PlaceHolder></div>
                </D:PlaceHolder>
            <% } %>
        </div>
        <div class="label"><D:LabelTextOnly ID="lblErrorCodeLabel" runat="server" Text="<%$ Resources:strings, Order_errorcode %>"></D:LabelTextOnly>:</div>
        <div class="value"><D:Label ID="lblErrorCode" runat="server"></D:Label></div>
        <D:PlaceHolder runat="server" ID="plhIsSubOrder" Visible="False">
            <div class="label">&nbsp;</div>
            <div class="value"><strong><D:LabelTextOnly ID="lblIsSubOrder" runat="server" Text="<%$ Resources:strings, Order_is_sub_order %>"></D:LabelTextOnly></strong></div>
        </D:PlaceHolder>
        <D:PlaceHolder ID="plhProcessOrder" runat="server" Visible="false">
            <div class="label"><D:LabelTextOnly ID="lblAction" runat="server" Text="<%$ Resources:strings, Order_action %>"></D:LabelTextOnly>:</div>
            <div class="value"><D:PlaceHolder ID="plhProcessOrderButton" runat="server"></D:PlaceHolder></div>
        </D:PlaceHolder>
    </D:HyperLink>
</li>