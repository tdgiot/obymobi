﻿using System;
using Obymobi.Data.EntityClasses;
using Obymobi.Enums;
using Dionysos;
using Dionysos.Web;

public partial class UserControls_Listitems_PaymentTransaction : System.Web.UI.UserControl
{
    #region Properties

    public PaymentTransactionEntity PaymentTransaction { get; set; }
    
    #endregion

    public UserControls_Listitems_PaymentTransaction()
    {
    }

    #region Event handlers

    protected void Page_Load(object sender, EventArgs e)
    {
        if (this.PaymentTransaction == null)
        {
            return;
        }

        DateTime createdLocalTime = CraveMobileNocHelper.ConvertUtcDateTimeToUserTimeZone(this.PaymentTransaction.CreatedUTC);

        this.hlPaymentTransaction.NavigateUrl = ResolveUrl($"~/PaymentTransaction.aspx?id={this.PaymentTransaction.PaymentTransactionId}");

        this.lblTitle.Text = $@"{this.PaymentTransaction.ReferenceId} - {this.PaymentTransaction.Status}";
        this.lblReferenceId.Text = this.PaymentTransaction.ReferenceId;
        this.lblMerchantRef.Text = this.PaymentTransaction.MerchantReference;
        this.lblMerchantAccount.Text = this.PaymentTransaction.MerchantAccount;
        this.lblStatus.Text = this.PaymentTransaction.Status.ToString();
        this.lblPaymentProvider.Text = this.PaymentTransaction.PaymentProviderType.ToString();
        this.lblEnvironment.Text = this.PaymentTransaction.PaymentEnvironment.ToString();
        this.lblPaymentMethod.Text = this.PaymentTransaction.PaymentMethod;
        this.lblCreated.Text = createdLocalTime.ToString("F");

        switch (this.PaymentTransaction.Status)
        {
            case PaymentTransactionStatus.Pending:
            case PaymentTransactionStatus.Initiated:
                this.imgStatus.ImageUrl = ResolveUrl("~/Images/Icons/hourglass.png");
                break;
            case PaymentTransactionStatus.Paid:
                this.imgStatus.ImageUrl = ResolveUrl("~/Images/Icons/checkmark.png");
                break;
            default:
                this.imgStatus.ImageUrl = ResolveUrl("~/Images/Icons/error.png");
                break;
        }
    }
    #endregion
}
