﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="UserControls_Listitems_PaymentTransactionLog" Codebehind="PaymentTransactionLog.ascx.cs" %>
<li class="nonlink">
    <div class="order">
        <D:Image ID="imgStatus" runat="server" />
        <div class="title"><D:Label ID="lblTitle" runat="server"></D:Label></div>
        <div class="label"><D:LabelTextOnly ID="lblLogTypeLabel" runat="server" Text="<%$ Resources:strings, PaymentLog_LogType %>"></D:LabelTextOnly>:</div>
        <div class="value"><D:Label ID="lblLogType" runat="server"></D:Label></div>
        <div class="label"><D:LabelTextOnly ID="lblAmountLabel" runat="server" Text="<%$ Resources:strings, PaymentLog_Amount %>"></D:LabelTextOnly>:</div>
        <div class="value"><D:Label ID="lblAmount" runat="server"></D:Label></div>
        <div class="label"><D:LabelTextOnly ID="lblEventCodeLabel" runat="server" Text="<%$ Resources:strings, PaymentLog_EventCode %>"></D:LabelTextOnly>:</div>
        <div class="value"><D:Label ID="lblEventCode" runat="server"></D:Label></div>
        <div class="label"><D:LabelTextOnly ID="lblEventTextLabel" runat="server" Text="<%$ Resources:strings, PaymentLog_EventText %>"></D:LabelTextOnly>:</div>
        <div class="value"><D:Label ID="lblEventText" runat="server"></D:Label></div>
        <div class="label"><D:LabelTextOnly ID="lblEventDateLabel" runat="server" Text="<%$ Resources:strings, PaymentLog_EventDate %>"></D:LabelTextOnly>:</div>
        <div class="value"><D:Label ID="lblEventDate" runat="server"></D:Label></div>
        <div class="label"><D:LabelTextOnly ID="lblCreatedLabel" runat="server" Text="<%$ Resources:strings, Order_created %>"></D:LabelTextOnly>:</div>
        <div class="value"><D:Label ID="lblCreated" runat="server"></D:Label></div>
        
        <D:PlaceHolder runat="server" ID="plhRawResponse" Visible="False">
            <div class="label"><D:LabelTextOnly ID="lblRawLabel" runat="server" Text="<%$ Resources:strings, PaymentLog_RawResponse %>"></D:LabelTextOnly>:</div>
            <div class="value_nolimit"><D:Label ID="lblRaw" runat="server"></D:Label></div>
        </D:PlaceHolder>
    </div>
</li>
