﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="UserControls_Listitems_PaymentTransactionSplit" Codebehind="PaymentTransactionSplit.ascx.cs" %>
<li>
    <D:HyperLink ID="hlPaymentTransactionSplit" runat="server">
        <D:Image ID="imgStatus" runat="server" />
        <div class="title"><D:Label ID="lblTitle" runat="server"></D:Label></div>
        <div class="label"><D:LabelTextOnly ID="lblReferenceLabel" runat="server" Text="<%$ Resources:strings, PaymentSplit_Reference %>"></D:LabelTextOnly>:</div>
        <div class="value"><D:Label ID="lblReference" runat="server"></D:Label></div>
        <div class="label"><D:LabelTextOnly ID="lblTypeLabel" runat="server" Text="<%$ Resources:strings, PaymentSplit_Type %>"></D:LabelTextOnly>:</div>
        <div class="value"><D:Label ID="lblType" runat="server"></D:Label></div>
        <D:PlaceHolder ID="plhAccountCode" runat="server">
	        <div class="label"><D:LabelTextOnly ID="lblAccountCodeLabel" runat="server" Text="<%$ Resources:strings, PaymentSplit_AccountCode %>"></D:LabelTextOnly>:</div>
	        <div class="value"><D:Label ID="lblAccountCode" runat="server"></D:Label></div>
        </D:PlaceHolder>
        <div class="label"><D:LabelTextOnly ID="lblAmountLabel" runat="server" Text="<%$ Resources:strings, PaymentSplit_Amount %>"></D:LabelTextOnly>:</div>
        <div class="value"><D:Label ID="lblAmount" runat="server"></D:Label></div>
        <div class="label"><D:LabelTextOnly ID="lblCreatedLabel" runat="server" Text="<%$ Resources:strings, Created %>"></D:LabelTextOnly>:</div>
        <div class="value"><D:Label ID="lblCreated" runat="server"></D:Label></div>
    </D:HyperLink>
</li>
