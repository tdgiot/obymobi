﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="UserControls_Listitems_DeviceActivityLog" Codebehind="DeviceActivityLog.ascx.cs" %>
<D:PlaceHolder runat="server" ID="plhContainerOpenTags"></D:PlaceHolder>
        <D:Image ID="imgLog" runat="server" />
        <div class="subtitle"><D:Label ID="lblTitle" runat="server"></D:Label></div>
        <D:PlaceHolder runat="server" ID="plhType" Visible="true">
        <div class="label"><D:LabelTextOnly ID="lblTypeLabel" runat="server" Text="<%$ Resources:strings, DeviceActivityLog_Type %>"></D:LabelTextOnly>:</div>
        <div class="value"><D:Label ID="lblType" runat="server"></D:Label></div>
        </D:PlaceHolder>
        <D:PlaceHolder runat="server" ID="plhSubmitted" Visible="false">
        <div class="label"><D:LabelTextOnly ID="lblSubmittedLabel" runat="server" Text="<%$ Resources:strings, DeviceActivityLog_Submitted %>"></D:LabelTextOnly>:</div>
        <div class="value"><D:Label ID="lblSubmitted" runat="server"></D:Label></div>
        </D:PlaceHolder>
        <div class="label"><D:LabelTextOnly ID="lblCreatedLabel" runat="server" Text="<%$ Resources:strings, DeviceActivityLog_Created %>"></D:LabelTextOnly>:</div>
        <div class="value"><D:Label ID="lblCreated" runat="server"></D:Label></div>        
        <div class="value fullwidth" runat="server" ID="divSummary">&nbsp;<br /><D:LabelTextOnly runat="server" ID="lblSummary"></D:LabelTextOnly></div>        
<D:PlaceHolder runat="server" ID="plhContainerClosingTags"></D:PlaceHolder>