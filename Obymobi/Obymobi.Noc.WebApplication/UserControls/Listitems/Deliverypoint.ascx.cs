﻿using System;
using Obymobi.Data.EntityClasses;

public partial class UserControls_Listitems_Deliverypoint : System.Web.UI.UserControl
{
    #region Properties

    public DeliverypointEntity Deliverypoint { get; set; }

    #endregion

    #region Event handlers

    protected void Page_Load(object sender, EventArgs e)
    {
        if (this.Deliverypoint != null)
        {
            this.hlDeliverypoint.NavigateUrl = ResolveUrl(string.Format("~/Deliverypoint.aspx?id={0}", this.Deliverypoint.DeliverypointId));

            if (this.Deliverypoint.ClientCollection.Count > 0)
            {
                if (this.Deliverypoint.HasOnlineClient)
                {
                    this.imgDeliverypoint.ImageUrl = ResolveUrl("~/Images/Icons/checkmark.png");
                    this.lblTitle.Text = string.Format(Resources.strings.Deliverypoint_is_online, this.Deliverypoint.Name);
                }
                else
                {
                    this.imgDeliverypoint.ImageUrl = ResolveUrl("~/Images/Icons/error.png");
                    this.lblTitle.Text = string.Format(Resources.strings.Deliverypoint_has_offline_clients, this.Deliverypoint.Name);
                    this.lblLastRequest.CssClass = "error";
                }

                DateTime? lastRequest = this.Deliverypoint.LastRequest;
                if (lastRequest.HasValue)
                    this.lblLastRequest.Text = string.Format("{0} ({1})", CraveMobileNocHelper.ConvertUtcDateTimeToUserTimeZone(lastRequest.Value), CraveMobileNocHelper.GetSpanTextBetweenDateTimeAndUtcNow(lastRequest.Value));
                else
                    this.lblLastRequest.Text = Resources.strings.Unknown;
            }
            else
            {
                this.imgDeliverypoint.ImageUrl = ResolveUrl("~/Images/Icons/exclamation.png");
                this.lblTitle.Text = string.Format(Resources.strings.Deliverypoint_has_no_clients, this.Deliverypoint.Name);

                this.lblLastRequest.Text = Resources.strings.Unknown;
            }

            // Deliverypoint group
            this.lblDeliverypointgroup.Text = this.Deliverypoint.DeliverypointgroupEntity.Name;
        }
    }

    #endregion
}