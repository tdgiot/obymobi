﻿using System;
using System.Collections.Specialized;
using System.Web;
using Obymobi.Data.EntityClasses;
using Dionysos.Web;

public partial class UserControls_Listitems_CompanySimplified : System.Web.UI.UserControl
{
    private CompanyEntity company;

    public CompanyEntity Company
    {
        get
        {
            return this.company;
        }
        set
        {
            this.company = value;
            this.SetGui();
        }
    }

    public void SetGui()
    {
        if (this.Company != null)
        {
            this.hlCompany_s.NavigateUrl = this.CreateCompanySpecificReturnUrl(this.Company.CompanyId);
            this.lblTitle_s.Text = string.Format(this.Company.Name);
        }
    }

    protected string CreateCompanySpecificReturnUrl(int companyid)
    {
        NameValueCollection urlQuery = HttpUtility.ParseQueryString(HttpContext.Current.Request.Url.Query);
        string target = urlQuery.Get("target");

        if (!string.IsNullOrWhiteSpace(target))
        {
            var targetPageUrl = new Uri(target);
            targetPageUrl = targetPageUrl.AddQueryString("cid", companyid);
            return targetPageUrl.ToString();
        }

        return string.Empty;
    }
}
