﻿using System;
using System.Web;
using Dionysos;
using Obymobi.Data.EntityClasses;
using Obymobi.Enums;

public partial class UserControls_Listitems_OrderRoutestephandlerHistory : System.Web.UI.UserControl
{
    #region Properties

    public OrderRoutestephandlerHistoryEntity OrderRoutestephandlerHistory { get; set; }

    #endregion

    #region Event handlers

    protected void Page_Load(object sender, EventArgs e)
    {
        if (this.OrderRoutestephandlerHistory != null)
        {
            this.hlOrderRoutestephandlerHistory.NavigateUrl = ResolveUrl(string.Format("~/OrderRoutestephandlerHistory.aspx?id={0}", this.OrderRoutestephandlerHistory.OrderRoutestephandlerHistoryId));

            switch (this.OrderRoutestephandlerHistory.StatusAsEnum)
            {
                case OrderRoutestephandlerStatus.WaitingForPreviousStep:
                    this.imgOrderRoutestephandlerHistory.ImageUrl = ResolveUrl("~/Images/Icons/hourglass.png");
                    this.lblTitle.Text = string.Format(Resources.strings.OrderRoutestephandler_waiting_for_previous_step, this.OrderRoutestephandlerHistory.Number);
                    break;
                case OrderRoutestephandlerStatus.WaitingToBeRetrieved:
                    this.imgOrderRoutestephandlerHistory.ImageUrl = ResolveUrl("~/Images/Icons/hourglass.png");
                    this.lblTitle.Text = string.Format(Resources.strings.OrderRoutestephandler_waiting_to_be_retrieved, this.OrderRoutestephandlerHistory.Number);
                    break;
                case OrderRoutestephandlerStatus.RetrievedByHandler:
                    this.imgOrderRoutestephandlerHistory.ImageUrl = ResolveUrl("~/Images/Icons/hourglass.png");
                    this.lblTitle.Text = string.Format(Resources.strings.OrderRoutestephandler_retrieved_by_handler, this.OrderRoutestephandlerHistory.Number);
                    break;
                case OrderRoutestephandlerStatus.BeingHandled:
                    this.imgOrderRoutestephandlerHistory.ImageUrl = ResolveUrl("~/Images/Icons/hourglass.png");
                    this.lblTitle.Text = string.Format(Resources.strings.OrderRoutestephandler_being_handled, this.OrderRoutestephandlerHistory.Number);
                    break;
                case OrderRoutestephandlerStatus.Completed:
                    this.imgOrderRoutestephandlerHistory.ImageUrl = ResolveUrl("~/Images/Icons/checkmark.png");
                    this.lblTitle.Text = string.Format(Resources.strings.OrderRoutestephandler_completed, this.OrderRoutestephandlerHistory.Number);
                    break;
                case OrderRoutestephandlerStatus.CompletedByOtherStep:
                    this.imgOrderRoutestephandlerHistory.ImageUrl = ResolveUrl("~/Images/Icons/checkmark.png");
                    this.lblTitle.Text = string.Format(Resources.strings.OrderRoutestephandler_completed_by_other_step, this.OrderRoutestephandlerHistory.Number);
                    break;
                case OrderRoutestephandlerStatus.CancelledDueToOtherStepFailure:
                    this.imgOrderRoutestephandlerHistory.ImageUrl = ResolveUrl("~/Images/Icons/error.png");
                    this.lblTitle.Text = string.Format(Resources.strings.OrderRoutestephandler_cancelled_due_to_other_step_failure, this.OrderRoutestephandlerHistory.Number);
                    this.lblStatus.CssClass = "error";
                    break;
                case OrderRoutestephandlerStatus.Failed:
                    this.imgOrderRoutestephandlerHistory.ImageUrl = ResolveUrl("~/Images/Icons/error.png");
                    this.lblTitle.Text = string.Format(Resources.strings.OrderRoutestephandler_failed, this.OrderRoutestephandlerHistory.Number);
                    this.lblStatus.CssClass = "error";
                    break;
            }

            if (this.OrderRoutestephandlerHistory.ErrorCodeAsEnum != OrderProcessingError.None)
                this.lblErrorCode.CssClass = "error";

            SetHandlerTypeLabel();

            DateTime? statusDateTime = null;
            switch (this.OrderRoutestephandlerHistory.StatusAsEnum)
            {
                case OrderRoutestephandlerStatus.WaitingForPreviousStep:
                    statusDateTime = this.OrderRoutestephandlerHistory.CreatedUTC;
                    break;
                case OrderRoutestephandlerStatus.WaitingToBeRetrieved:
                    statusDateTime = this.OrderRoutestephandlerHistory.WaitingToBeRetrievedUTC;
                    break;
                case OrderRoutestephandlerStatus.RetrievedByHandler:
                    statusDateTime = this.OrderRoutestephandlerHistory.RetrievedByHandlerUTC;
                    break;
                case OrderRoutestephandlerStatus.BeingHandled:
                case OrderRoutestephandlerStatus.BeingHandledByOtherStep:
                    statusDateTime = this.OrderRoutestephandlerHistory.BeingHandledUTC;
                    break;
                case OrderRoutestephandlerStatus.Completed:
                case OrderRoutestephandlerStatus.CompletedByOtherStep:
                case OrderRoutestephandlerStatus.CancelledDueToOtherStepFailure:
                case OrderRoutestephandlerStatus.Failed:
                    statusDateTime = this.OrderRoutestephandlerHistory.CompletedUTC;
                    break;
            }

            if (statusDateTime.HasValue)
                this.lblStatus.Text = string.Format("{0} ({1}) @ {2:HH:mm:ss} ({3})", 
                    this.OrderRoutestephandlerHistory.StatusAsEnum, 
                    this.OrderRoutestephandlerHistory.Status, 
                    CraveMobileNocHelper.ConvertUtcDateTimeToUserTimeZone(statusDateTime.Value), 
                    CraveMobileNocHelper.GetSpanTextBetweenDateTimeAndUtcNow(statusDateTime.Value));
            else
                this.lblStatus.Text = string.Format("{0} ({1})", this.OrderRoutestephandlerHistory.StatusAsEnum, this.OrderRoutestephandlerHistory.Status);            

            if (this.OrderRoutestephandlerHistory.ErrorCodeAsEnum == OrderProcessingError.None && this.OrderRoutestephandlerHistory.ErrorText.Equals(OrderProcessingError.None.ToString(), StringComparison.InvariantCultureIgnoreCase))
            {
                // No error
                this.plhError.Visible = false;
            }
            else
            {
                this.lblErrorCode.Text = string.Format("{0} ({1})", this.OrderRoutestephandlerHistory.ErrorCodeAsEnum, this.OrderRoutestephandlerHistory.ErrorCode);
                this.lblErrorCode.CssClass = "error";

                this.lblErrorText.Text = HttpUtility.HtmlEncode(this.OrderRoutestephandlerHistory.ErrorText);
                this.lblErrorText.CssClass = "error";
            }            
        }
    }

    private void SetHandlerTypeLabel()
    {
        string handlerType = OrderRoutestephandlerHistory.HandlerTypeAsEnum.ToString();
        string handlerName = null;

        switch (OrderRoutestephandlerHistory.HandlerTypeAsEnum)
        {
            case RoutestephandlerType.Printer:
            case RoutestephandlerType.Console:
                handlerName = OrderRoutestephandlerHistory.TerminalEntity.Name;
                break;
            case RoutestephandlerType.ExternalSystem:
                handlerName = OrderRoutestephandlerHistory.ExternalSystemEntity.TypeText;
                break;
        }

        lblHandlerType.Text = handlerName == null
            ? handlerType
            : $"{handlerType} ({handlerName})";
    }

    #endregion
}