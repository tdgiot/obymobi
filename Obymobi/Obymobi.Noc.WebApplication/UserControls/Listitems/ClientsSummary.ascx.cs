﻿using System;
using Dionysos;
using Obymobi.Enums;
using System.Data;
using System.Text;

public partial class UserControls_Listitems_ClientsSummary : System.Web.UI.UserControl
{
    private bool renderLinkable = true;

    #region Properties

    public ClientsSummaryBase Summary { get; set; }

    /// <summary>
    /// Gets or sets the renderLinkable
    /// </summary>
    public bool RenderLinkable
    {
        get
        {
            return this.renderLinkable;
        }
        set
        {
            this.renderLinkable = value;
        }
    }

    #endregion

    #region Event handlers

    protected void Page_Load(object sender, EventArgs e)
    {
        if (this.Summary != null)
        {
            if (this.renderLinkable)
            {
                this.plhContainerOpenTags.AddHtml("<li><a class=\"order\" href=\"{0}\">", ResolveUrl(string.Format("~/Clients.aspx?cid={0}", this.Summary.CompanyId)));
                this.plhContainerClosingTags.AddHtml("</a></li>");
            }
            else
            {
                this.plhContainerOpenTags.AddHtml("<li class=\"nonlink\"><div class=\"order\">");
                this.plhContainerClosingTags.AddHtml("</div></li>");
            }

            this.lblTitle.Text = this.Summary.CompanyName;
            
            StringBuilder builder = new StringBuilder();

            var clientSummaryOperationModes = this.Summary as ClientsSummaryOperationModes;
            if (clientSummaryOperationModes != null)
            {
                foreach (DataRow row in clientSummaryOperationModes.OperationModes.Rows)
                {
                    builder.AppendFormatLine("<div class='label clientssummary'>{0}:</div>", row[2].ToString().ToEnum<ClientOperationMode>().ToString());
                    builder.AppendFormatLine("<div class='value'>{0}</div>", row[3]);
                }
            }
            else
            {
                ClientsSummaryPercentage clientSummaryPercentage = this.Summary as ClientsSummaryPercentage;
                if (clientSummaryPercentage != null)
                {
                    builder.AppendFormatLine("<div class='label clientssummary'>{0}:</div>", Resources.strings.Client_percentage_offline);
                    builder.AppendFormatLine("<div class='value'>{0}%</div>", clientSummaryPercentage.Percentage);
                }
            }

            this.plhOperationModes.AddHtml(builder.ToString());
        }
    }

    #endregion
}