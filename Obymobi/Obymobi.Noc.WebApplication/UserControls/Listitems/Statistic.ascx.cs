﻿using System;
using Dionysos;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Enums;
using Math = System.Math;

public partial class UserControls_Listitems_Statistic : System.Web.UI.UserControl
{
    public int? CompanyId { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        RenderStatisticsForCompany(CompanyId);
    }

    private void RenderStatisticsForCompany(int? companyId)
    {
        // Orders last 60 mins
        int ordersLastHour = GetOrderCount(companyId, DateTime.UtcNow.AddHours(-1), DateTime.UtcNow);

        // Orders today
        int ordersToday = GetOrderCount(companyId, DateTime.UtcNow.MakeBeginOfDay(), DateTime.UtcNow);
        
        // Orders last 7 days (this week)
        int ordersThisWeek = GetOrderCount(companyId, DateTime.UtcNow.AddDays(-7), DateTime.UtcNow);

        // Orders 2 weeks ago (last week)
        int ordersLastWeek = GetOrderCount(companyId, DateTime.UtcNow.AddDays(-14), DateTime.UtcNow.AddDays(-7));

        // Orders 3 weeks ago
        int ordersThreeWeeksAgo = GetOrderCount(companyId, DateTime.UtcNow.AddDays(-21), DateTime.UtcNow.AddDays(-14));

        // Orders 4 weeks ago
        int ordersFourWeeksAgo = GetOrderCount(companyId, DateTime.UtcNow.AddDays(-28), DateTime.UtcNow.AddDays(-21));

        // Orders 5 weeks ago
        int ordersFiveWeeksAgo = GetOrderCount(companyId, DateTime.UtcNow.AddDays(-35), DateTime.UtcNow.AddDays(-28));

        this.lblOrdersLastHour.Text = ordersLastHour.ToString();
        this.lblOrdersToday.Text = ordersToday.ToString("N0");
        this.lblOrdersWeek.Text = ordersThisWeek.ToString("N0");
        this.lblOrdersTwoWeeksAgo.Text = ordersLastWeek.ToString("N0");
        this.lblOrdersThreeWeeksAgo.Text = ordersThreeWeeksAgo.ToString("N0");
        this.lblOrdersFourWeeksAgo.Text = ordersFourWeeksAgo.ToString("N0");
        this.lnlOrdersFiveWeeksAgo.Text = ordersFiveWeeksAgo.ToString("N0");

        // Balances
        double balanceThisWeek = ((double)(ordersThisWeek - ordersLastWeek) / ordersLastWeek);
        this.lblWeekBalanceThisWeek.Text = balanceThisWeek.ToString("P1");
        this.lblWeekBalanceThisWeek.CssClass = balanceThisWeek > 0 ? "green" : "red";

        double balanceTwoWeeksAgo = ((double)(ordersLastWeek - ordersThreeWeeksAgo) / ordersThreeWeeksAgo);
        this.lblWeekBalanceTwoWeeksAgo.Text = balanceTwoWeeksAgo.ToString("P1");
        this.lblWeekBalanceTwoWeeksAgo.CssClass = balanceTwoWeeksAgo > 0 ? "green" : "red";

        double balanceThreeWeeksAgo = ((double)(ordersThreeWeeksAgo - ordersFourWeeksAgo) / ordersFourWeeksAgo);
        this.lblWeekBalanceThreeWeeksAgo.Text = balanceThreeWeeksAgo.ToString("P1");
        this.lblWeekBalanceThreeWeeksAgo.CssClass = balanceThreeWeeksAgo > 0 ? "green" : "red";

        double balanceFourWeeksAgo = ((double)(ordersFourWeeksAgo - ordersFiveWeeksAgo) / ordersFiveWeeksAgo);
        this.lblWeekBalanceFourWeeksAgo.Text = balanceFourWeeksAgo.ToString("P1");
        this.lblWeekBalanceFourWeeksAgo.CssClass = balanceFourWeeksAgo > 0 ? "green" : "red";
    }

    private static int GetOrderCount(int? companyId, DateTime startTime, DateTime endTime)
    {
        PredicateExpression filter = new PredicateExpression();
        filter.Add(OrderFields.Status == OrderStatus.Processed);
        filter.Add(OrderFields.ErrorCode == OrderProcessingError.None);

        filter.Add(OrderFields.CreatedUTC >= startTime);
        filter.Add(OrderFields.CreatedUTC <= endTime);

        if (companyId.HasValue)
        {
            filter.Add(OrderFields.CompanyId == companyId);
        }

        return new OrderCollection().GetDbCount(filter);
    }
}