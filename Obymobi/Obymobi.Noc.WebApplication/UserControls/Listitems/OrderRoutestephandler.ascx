﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="UserControls_Listitems_OrderRoutestephandler" Codebehind="OrderRoutestephandler.ascx.cs" %>
<li>
    <D:HyperLink ID="hlOrderRoutestephandler" runat="server">
        <D:Image ID="imgOrderRoutestephandler" runat="server" />
        <div class="title"><D:Label ID="lblTitle" runat="server"></D:Label></div>
        <div class="label"><D:LabelTextOnly ID="lblHandlerTypeLabel" runat="server" Text="<%$ Resources:strings, OrderRoutestephandler_HandlerType %>"></D:LabelTextOnly>:</div>
        <div class="value"><D:Label ID="lblHandlerType" runat="server"></D:Label></div>
        <div class="label"><D:LabelTextOnly ID="lblStatusLabel" runat="server" Text="<%$ Resources:strings, Status %>"></D:LabelTextOnly>:</div>
        <div class="value"><D:Label ID="lblStatus" runat="server"></D:Label></div>
        <D:PlaceHolder runat="server" ID="plhError">
            <div class="label"><D:LabelTextOnly ID="lblErrorCodeLabel" runat="server" Text="Error code"></D:LabelTextOnly>:</div>
            <div class="value"><D:Label ID="lblErrorCode" runat="server"></D:Label></div>
            <div class="label"><D:LabelTextOnly ID="lblErrorTextLabel" runat="server" Text="Error text"></D:LabelTextOnly>:</div>
            <div class="value"><D:Label ID="lblErrorText" runat="server"></D:Label></div>
        </D:PlaceHolder>
    </D:HyperLink>
</li>