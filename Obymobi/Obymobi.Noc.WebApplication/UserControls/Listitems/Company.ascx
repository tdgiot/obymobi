﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="UserControls_Listitems_Company" Codebehind="Company.ascx.cs" %>
<li>
    <D:HyperLink ID="hlCompany" runat="server">
        <D:Image ID="imgCompany" runat="server" />
        <div class="title"><D:LabelTextOnly ID="lblTitle" runat="server"></D:LabelTextOnly></div>
        <div class="label"><D:LabelTextOnly ID="lblStatusLabel" runat="server" Text="<%$ Resources:strings, Company_status %>"></D:LabelTextOnly>:</div>
        <div class="value"><D:Label ID="lblLastStatus" runat="server"></D:Label></div>
    </D:HyperLink>
</li>