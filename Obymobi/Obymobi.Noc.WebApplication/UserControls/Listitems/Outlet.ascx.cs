﻿using System;
using Obymobi.Data.EntityClasses;

public partial class UserControls_Listitems_Outlet : System.Web.UI.UserControl
{
	public OutletEntity Outlet { get; set; }

	public string StatusClass => this.Outlet.OutletOperationalStateEntity.OrderIntakeDisabled ? "Hidden" : string.Empty;


	protected void Page_Load(object sender, EventArgs e)
	{
		if (Outlet == null)
		{
			return;
		}

		lblNameValue.Text = this.Outlet.Name;

		if (Outlet.OutletOperationalStateEntity.OrderIntakeDisabled)
		{
			btnVisibility.Text = "Switch on";
			btnVisibility.CssClass = "hidden";
		}
		else
		{
			btnVisibility.Text = "Switch off";
		}

	}
}
