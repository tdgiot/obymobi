﻿using System;
using System.Globalization;
using Obymobi.Data.EntityClasses;
using Dionysos;
using Obymobi;
using Obymobi.Enums;
using Obymobi.Constants;
using Obymobi.Logic.HelperClasses;

public partial class UserControls_Listitems_Terminal : System.Web.UI.UserControl
{
    private bool renderLinkable = true;

    #region Properties

    public TerminalEntity Terminal { get; set; }

    /// <summary>
    /// Gets or sets the renderLinkable
    /// </summary>
    public bool RenderLinkable
    {
        get
        {
            return this.renderLinkable;
        }
        set
        {
            this.renderLinkable = value;
        }
    }

    #endregion

    #region Event handlers

    protected void Page_Load(object sender, EventArgs e)
    {
        if (this.Terminal != null && this.Terminal.DeviceId.HasValue)
        {
            if (this.renderLinkable)
            {
                this.plhContainerOpenTags.AddHtml("<li><a class=\"order\" href=\"{0}\">", ResolveUrl(string.Format("~/Terminal.aspx?id={0}", this.Terminal.TerminalId)));
                this.plhContainerClosingTags.AddHtml("</a></li>");
            }
            else
            {
                this.plhContainerOpenTags.AddHtml("<li class=\"nonlink\"><div class=\"order\">");
                this.plhContainerClosingTags.AddHtml("</div></li>");
            }

            string lastRequestValue = Resources.strings.Unknown;
            string timeSinceLastRequest = "0";
            if (this.Terminal.DeviceEntity.LastRequestUTC.HasValue)
            {
                lastRequestValue = CraveMobileNocHelper.ConvertUtcDateTimeToUserTimeZone(this.Terminal.DeviceEntity.LastRequestUTC.Value).ToString(CultureInfo.InvariantCulture);
                timeSinceLastRequest = CraveMobileNocHelper.GetSpanTextBetweenDateTimeAndUtcNow(this.Terminal.DeviceEntity.LastRequestUTC.Value);
            }

            if (this.Terminal.IsOnline) // Terminal is online
            {
                this.imgTerminal.ImageUrl = ResolveUrl("~/Images/Icons/checkmark.png");
                this.lblTitle.Text = string.Format(Resources.strings.Terminal_is_online, this.Terminal.CompanyEntity.Name, this.Terminal.Name);

                this.lblLastRequest.Text = string.Format("{0} ({1})", lastRequestValue, timeSinceLastRequest);
            }
            else if (this.Terminal.DeviceEntity.LastRequestUTC.HasValue) // Terminal has last request value but not online
            {
                this.imgTerminal.ImageUrl = ResolveUrl("~/Images/Icons/error.png");
                this.lblTitle.Text = string.Format(Resources.strings.Terminal_is_offline, this.Terminal.CompanyEntity.Name, this.Terminal.Name, CraveMobileNocHelper.GetSpanTextBetweenDateTimeAndUtcNow(this.Terminal.DeviceEntity.LastRequestUTC.Value));

                this.lblLastRequest.Text = string.Format("{0} ({1})", lastRequestValue, timeSinceLastRequest);
                this.lblLastRequest.CssClass = "error";
            }
            else // Terminal has no last request value
            {
                this.imgTerminal.ImageUrl = ResolveUrl("~/Images/Icons/exclamation.png");
                this.lblTitle.Text = string.Format(Resources.strings.Terminal_has_not_been_online_yet, this.Terminal.CompanyEntity.Name, this.Terminal.Name);

                this.lblLastRequest.Text = Resources.strings.Unknown;
            }

            #region LastApplicationVersion

            this.imgLastApplicationVersion.Visible = false;

            if (this.Terminal.DeviceEntity.ApplicationVersion.IsNullOrWhiteSpace()) // Terminal has no last version
            {
                this.lblLastApplicationVersion.Text = Resources.strings.Unknown;
            }
            else
            {
                this.lblLastApplicationVersion.Text = new VersionNumber(this.Terminal.DeviceEntity.ApplicationVersion).NumberAsString;
                this.lblLastApplicationVersion.CssClass = "";
                
                var companyRelease = ReleaseHelper.GetVersionForCompany(ApplicationCode.Console, this.Terminal.CompanyId);
                VersionNumber.VersionState versionState = VersionNumber.CompareVersions(this.Terminal.DeviceEntity.ApplicationVersion, companyRelease);
                switch (versionState)
                {
                    case VersionNumber.VersionState.Unknown:
                        // Unknown?
                        break;
                    case VersionNumber.VersionState.Newer:
                        this.lblLastApplicationVersion.CssClass = "info";
                        break;
                    case VersionNumber.VersionState.Older:
                        this.imgLastApplicationVersion.ImageUrl = ResolveUrl("~/Images/Icons/exclamation.png");
                        this.imgLastApplicationVersion.Visible = true;
                        break;
                }
            }

            #endregion

            #region LastOsVersion

            this.imgLastOsVersion.Visible = false;

            if (this.Terminal.DeviceEntity.OsVersion.IsNullOrWhiteSpace())
            {
                this.lblLastOsVersion.Text = Resources.strings.Unknown;
            }
            else
            {
                this.lblLastOsVersion.Text = new VersionNumber(this.Terminal.DeviceEntity.OsVersion).NumberAsString;
                this.lblLastOsVersion.CssClass = "";

                var companyRelease = ReleaseHelper.GetVersionForCompany(ApplicationCode.CraveOSSamsungP5110, this.Terminal.CompanyId);
                VersionNumber.VersionState versionState = VersionNumber.CompareVersions(this.Terminal.DeviceEntity.OsVersion, companyRelease);
                switch (versionState)
                {
                    case VersionNumber.VersionState.Unknown:
                        this.lblLastOsVersion.Text = Resources.strings.Unknown;
                        break;
                    case VersionNumber.VersionState.Newer:
                        this.lblLastOsVersion.CssClass = "info";
                        break;
                    case VersionNumber.VersionState.Older:
                        this.imgLastOsVersion.ImageUrl = ResolveUrl("~/Images/Icons/exclamation.png");
                        this.imgLastOsVersion.Visible = true;
                        break;
                }
            }

            #endregion

            #region LastSupportToolsVersion

            this.imgLastSupportToolsVersion.Visible = false;

            if (this.Terminal.DeviceEntity.SupportToolsVersion.IsNullOrWhiteSpace()) // Terminal has no last support tools version
            {
                this.lblLastSupportToolsVersion.Text = Resources.strings.Unknown;
            }
            else
            {
                string lastSupportToolsRequest = Resources.strings.Unknown;
                if (this.Terminal.DeviceEntity.LastSupportToolsRequestUTC.HasValue)
                    lastSupportToolsRequest = CraveMobileNocHelper.ConvertUtcDateTimeToUserTimeZone(this.Terminal.DeviceEntity.LastSupportToolsRequestUTC.Value).ToString(CultureInfo.InvariantCulture);

                string lastSupportToolsVersion = (this.Terminal.DeviceEntity.SupportToolsVersion.IsNullOrWhiteSpace() ? Resources.strings.Unknown : new VersionNumber(this.Terminal.DeviceEntity.SupportToolsVersion).NumberAsString);
                this.lblLastSupportToolsVersion.Text = string.Format("{0} ({1})", lastSupportToolsVersion, lastSupportToolsRequest);
                this.lblLastSupportToolsVersion.CssClass = "";
                
                var companyRelease = ReleaseHelper.GetVersionForCompany(ApplicationCode.SupportTools, this.Terminal.CompanyId);
                VersionNumber.VersionState versionState = VersionNumber.CompareVersions(this.Terminal.DeviceEntity.SupportToolsVersion, companyRelease);
                switch (versionState)
                {
                    case VersionNumber.VersionState.Unknown:
                        // Unknown?
                        break;
                    case VersionNumber.VersionState.Newer:
                        this.lblLastSupportToolsVersion.CssClass = "info";
                        break;
                    case VersionNumber.VersionState.Older:
                        this.imgLastSupportToolsVersion.ImageUrl = ResolveUrl("~/Images/Icons/exclamation.png");
                        this.imgLastSupportToolsVersion.Visible = true;
                        break;
                }
            }

            #endregion

            #region LastAgentVersion

            this.imgLastAgentVersion.Visible = false;

            if (this.Terminal.DeviceEntity.AgentVersion.IsNullOrWhiteSpace()) // Client has no last agent version
            {
                this.lblLastAgentVersion.Text = Resources.strings.Unknown;
            }
            else
            {
                this.lblLastAgentVersion.Text = new VersionNumber(this.Terminal.DeviceEntity.AgentVersion).NumberAsString;
                this.lblLastAgentVersion.CssClass = "";
                
                var companyRelease = ReleaseHelper.GetVersionForCompany(ApplicationCode.Agent, this.Terminal.CompanyId);
                VersionNumber.VersionState versionState = VersionNumber.CompareVersions(this.Terminal.DeviceEntity.AgentVersion, companyRelease);
                switch (versionState)
                {
                    case VersionNumber.VersionState.Unknown:
                        // Unknown?
                        break;
                    case VersionNumber.VersionState.Newer:
                        this.lblLastAgentVersion.CssClass = "info";
                        break;
                    case VersionNumber.VersionState.Older:
                        this.imgLastAgentVersion.ImageUrl = ResolveUrl("~/Images/Icons/exclamation.png");
                        this.imgLastAgentVersion.Visible = true;
                        break;
                }
            }

            #endregion

            #region LastMessagingServiceVersion

            this.imgLastMessagingServiceVersion.Visible = false;

            string lastMessagingServiceVersion = (this.Terminal.DeviceEntity.MessagingServiceVersion.IsNullOrWhiteSpace() ? Resources.strings.Unknown : new VersionNumber(this.Terminal.DeviceEntity.MessagingServiceVersion).NumberAsString);
            //string lastMessagingServiceIsRunning = (this.Client.DeviceEntity.AgentRunning ? Resources.strings.running : Resources.strings.not_running);

            this.lblLastMessagingServiceVersion.Text = string.Format("{0}", lastMessagingServiceVersion);
            this.lblLastMessagingServiceVersion.CssClass = "";

            if (!this.Terminal.DeviceEntity.MessagingServiceVersion.IsNullOrWhiteSpace())
            {
                var companyRelease = ReleaseHelper.GetVersionForCompany(ApplicationCode.MessagingService, this.Terminal.CompanyId);
                VersionNumber.VersionState versionState = VersionNumber.CompareVersions(this.Terminal.DeviceEntity.MessagingServiceVersion, companyRelease);
                switch (versionState)
                {
                    case VersionNumber.VersionState.Unknown:
                        // Unknown?
                        break;
                    case VersionNumber.VersionState.Newer:
                        this.lblLastMessagingServiceVersion.CssClass = "info";
                        break;
                    case VersionNumber.VersionState.Older:
                        this.imgLastMessagingServiceVersion.ImageUrl = ResolveUrl("~/Images/Icons/exclamation.png");
                        this.imgLastMessagingServiceVersion.Visible = true;
                        break;
                }
            }

            #endregion

            // Last status
            if (this.Terminal.LastStatus.HasValue && this.Terminal.LastStatus.Value == (int)TerminalState.Online) // Terminal state is OK
            {
                this.lblLastStatus.Text = this.Terminal.LastStatusText;
            }
            else if (Terminal.LastStatus.HasValue)
            {
                this.lblLastStatus.Text = this.Terminal.LastStatusText;
                this.lblLastStatus.CssClass = "error";
            }
            else
            {
                this.lblLastStatus.Text = Resources.strings.Unknown;
            }

            // Last communication method
            this.lblLastCommunicationMethod.Text = string.Format("{0} ({1})", this.Terminal.DeviceEntity.CommunicationMethod.ToEnum<ClientCommunicationMethod>(), this.Terminal.DeviceEntity.CommunicationMethod);

            // Last cloud environment
            this.lblLastCloudEnvironment.Text = this.Terminal.DeviceEntity.CloudEnvironment;

            // Last wifi strength
            if (!this.Terminal.DeviceEntity.WifiStrength.HasValue)
                this.lblWifiStrength.Text = Resources.strings.Unknown;
            else
            {
                int strength = this.Terminal.DeviceEntity.WifiStrength.Value;
                if (strength > -55)
                    this.lblWifiStrength.Text = Resources.strings.Wifi_strength_excellent;
                else if (strength > -66)
                    this.lblWifiStrength.Text = Resources.strings.Wifi_strength_good;
                else if (strength > -77)
                    this.lblWifiStrength.Text = Resources.strings.Wifi_strength_average;
                else if (strength > -88)
                    this.lblWifiStrength.Text = Resources.strings.Wifi_strength_poor;
                else
                    this.lblWifiStrength.Text = Resources.strings.Wifi_strength_bad;

                this.lblWifiStrength.Text += string.Format(" ({0})", this.Terminal.DeviceEntity.WifiStrength.Value);
            }

            // Battery level
            this.lblBatteryLevel.Text = (!this.Terminal.DeviceEntity.BatteryLevel.HasValue ? this.Terminal.DeviceEntity.BatteryLevel + "%" : Resources.strings.Unknown);
            if (this.Terminal.DeviceEntity.BatteryLevel.HasValue)
            {
                this.lblBatteryLevel.Text = this.Terminal.DeviceEntity.BatteryLevel.Value + "%";
                if (this.Terminal.DeviceEntity.BatteryLevel.Value < 20)
                    this.lblBatteryLevel.CssClass = "error";
            }
            else
                this.lblBatteryLevel.Text = Resources.strings.Unknown;

        }
    }

    #endregion
}