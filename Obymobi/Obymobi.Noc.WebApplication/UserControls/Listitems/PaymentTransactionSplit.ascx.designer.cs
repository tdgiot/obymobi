﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated. 
// </auto-generated>
//------------------------------------------------------------------------------



public partial class UserControls_Listitems_PaymentTransactionSplit
{

    /// <summary>
    /// hlPaymentTransactionSplit control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::Dionysos.Web.UI.WebControls.HyperLink hlPaymentTransactionSplit;

    /// <summary>
    /// imgStatus control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::Dionysos.Web.UI.WebControls.Image imgStatus;

    /// <summary>
    /// lblTitle control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::Dionysos.Web.UI.WebControls.Label lblTitle;

    /// <summary>
    /// lblReferenceLabel control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::Dionysos.Web.UI.WebControls.LabelTextOnly lblReferenceLabel;

    /// <summary>
    /// lblReference control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::Dionysos.Web.UI.WebControls.Label lblReference;

    /// <summary>
    /// lblTypeLabel control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::Dionysos.Web.UI.WebControls.LabelTextOnly lblTypeLabel;

    /// <summary>
    /// lblType control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::Dionysos.Web.UI.WebControls.Label lblType;

    /// <summary>
    /// plhAccountCode control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::Dionysos.Web.UI.WebControls.PlaceHolder plhAccountCode;

    /// <summary>
    /// lblAccountCodeLabel control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::Dionysos.Web.UI.WebControls.LabelTextOnly lblAccountCodeLabel;

    /// <summary>
    /// lblAccountCode control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::Dionysos.Web.UI.WebControls.Label lblAccountCode;

    /// <summary>
    /// lblAmountLabel control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::Dionysos.Web.UI.WebControls.LabelTextOnly lblAmountLabel;

    /// <summary>
    /// lblAmount control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::Dionysos.Web.UI.WebControls.Label lblAmount;

    /// <summary>
    /// lblCreatedLabel control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::Dionysos.Web.UI.WebControls.LabelTextOnly lblCreatedLabel;

    /// <summary>
    /// lblCreated control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::Dionysos.Web.UI.WebControls.Label lblCreated;
}
