﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="UserControls_Listitems_Deliverypoint" Codebehind="Deliverypoint.ascx.cs" %>
<li>
    <D:HyperLink ID="hlDeliverypoint" runat="server">
        <D:Image ID="imgDeliverypoint" runat="server" />
        <div class="title"><D:Label ID="lblTitle" runat="server"></D:Label></div>
        <div class="label"><D:LabelTextOnly ID="lblLastRequestLabel" runat="server" Text="<%$ Resources:strings, Deliverypoint_last_request %>"></D:LabelTextOnly>:</div>
        <div class="value"><D:Label ID="lblLastRequest" runat="server"></D:Label></div>
        <div class="label"><D:LabelTextOnly ID="lblDeliverypointgroupLabel" runat="server" Text="<%$ Resources:strings, Deliverypointgroup %>"></D:LabelTextOnly>:</div>
        <div class="value"><D:Label ID="lblDeliverypointgroup" runat="server"></D:Label></div>
    </D:HyperLink>
</li>