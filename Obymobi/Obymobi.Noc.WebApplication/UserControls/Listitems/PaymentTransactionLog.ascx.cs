﻿using System;
using System.Globalization;
using Dionysos;
using Obymobi.Data.EntityClasses;
using Obymobi.Enums;

public partial class UserControls_Listitems_PaymentTransactionLog : System.Web.UI.UserControl
{
    public PaymentTransactionLogEntity PaymentTransactionLog { get; set; }

    
    protected void Page_Load(object sender, EventArgs e)
    {
        if (this.PaymentTransactionLog == null)
        {
            return;
        }

        DateTime createdLocalTime = CraveMobileNocHelper.ConvertUtcDateTimeToUserTimeZone(this.PaymentTransactionLog.CreatedUTC);
        
        this.lblTitle.Text = $@"{this.PaymentTransactionLog.LogType.ToString()} - {this.PaymentTransactionLog.EventCode}";
        this.lblLogType.Text = this.PaymentTransactionLog.LogType.ToString();
        this.lblAmount.Text = this.PaymentTransactionLog.Amount.HasValue ? this.PaymentTransactionLog.Amount.ToString() : "";
        this.lblEventCode.Text = this.PaymentTransactionLog.EventCode;
        this.lblEventText.Text = this.PaymentTransactionLog.EventText;
        this.lblEventDate.Text = this.PaymentTransactionLog.EventDate.ToString();
        this.lblCreated.Text = createdLocalTime.ToString("F");

        this.imgStatus.ImageUrl = this.PaymentTransactionLog.Success ? ResolveUrl("~/Images/Icons/checkmark.png") : ResolveUrl("~/Images/Icons/warning.png");

        if (CraveMobileNocHelper.CurrentUser.Role > Role.Administrator)
        {
            this.plhRawResponse.Visible = true;
            this.lblRaw.Text = this.PaymentTransactionLog.RawResponse;
        }
    }
}
