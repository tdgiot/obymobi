﻿using System;
using System.Collections.Generic;
using Obymobi.Data.EntityClasses;
using Dionysos;
using Obymobi.Noc.Logic.HelperClasses;

public partial class UserControls_Listitems_Company : System.Web.UI.UserControl
{
    #region Fields
    private CompanyEntity company;
    #endregion

    #region Properties

    public CompanyEntity Company
    {
        get
        {
            return this.company;
        }
        set
        {
            this.company = value;
            this.SetGui();
        }
    }

    public bool HasFaultyOrders
    { get; set; }

    public bool HasNonOrderingClients
    { get; set; }

    public bool HasOfflineTerminals
    { get; set; }

    public void SetGui()
    {
        if (this.Company != null)
        {
            this.hlCompany.NavigateUrl = ResolveUrl(string.Format("~/Company.aspx?id={0}", this.Company.CompanyId));

            string title = string.Empty;

            // Faulty orders
            int orderTotal = NocOrderHelper.GetFaultyOrderCountPastDay(new List<int> {this.Company.CompanyId});
            if (orderTotal > 0)
            {
                if (orderTotal == 1)
                    title += Resources.strings.One_unprocessed_order;
                else
                    title += string.Format(Resources.strings.X_unprocessed_orders, orderTotal);

                this.HasFaultyOrders = true;
            }

            // Clients in non ordering mode
            // DK (24-05-17): Temp ;) disabled becaues it caused a deadlock
            int nonOrderingClientCount = 0; //NocCompanyHelper.GetNonOrderingClientsGroupedByOperationModeCount(new List<int> { this.Company.CompanyId });
            if (nonOrderingClientCount > 0)
            {
                if (title.Length > 0)
                    title += ", ";

                if (nonOrderingClientCount == 1)
                    title += Resources.strings.One_non_ordering_client;
                else
                    title += string.Format(Resources.strings.X_non_ordering_clients, nonOrderingClientCount);

                this.HasNonOrderingClients = true;
            }

            // Offline terminals
            int offlineTerminalCount = NocCompanyHelper.GetOfflineTerminalsCount(new List<int> { this.Company.CompanyId });
            if (offlineTerminalCount > 0)
            {
                if (title.Length > 0)
                    title += ", ";

                if (offlineTerminalCount == 1)
                    title += Resources.strings.One_offline_terminal;
                else
                    title += string.Format(Resources.strings.X_offline_terminals, offlineTerminalCount);

                this.HasOfflineTerminals = true;
            }

            if (title.IsNullOrWhiteSpace())
            {
                this.imgCompany.ImageUrl = ResolveUrl("~/Images/Icons/checkmark.png");
                this.lblTitle.Text = string.Format(Resources.strings.Company_has_no_issues, this.Company.Name);
                this.lblLastStatus.Text = Resources.strings.No_issues;
            }
            else
            {
                this.imgCompany.ImageUrl = ResolveUrl("~/Images/Icons/error.png");
                this.lblTitle.Text = string.Format(Resources.strings.Company_has_issues, this.Company.Name, title);
                this.lblLastStatus.Text = title.TurnFirstToUpper();
                this.lblLastStatus.CssClass = "error";
            }
        }
    }

    #endregion

    #region Event handlers

    protected void Page_Load(object sender, EventArgs e)
    {        
    }

    #endregion
}