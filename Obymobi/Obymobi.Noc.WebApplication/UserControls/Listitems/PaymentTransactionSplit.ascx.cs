﻿using System;
using Dionysos;
using Obymobi.Data.EntityClasses;
using Obymobi.Extensions;

public partial class UserControls_Listitems_PaymentTransactionSplit : System.Web.UI.UserControl
{
    #region Properties

    public PaymentTransactionSplitEntity PaymentTransactionSplit { get; set; }
    
    #endregion

    public UserControls_Listitems_PaymentTransactionSplit()
    {
    }

    #region Event handlers

    protected void Page_Load(object sender, EventArgs e)
    {
        if (this.PaymentTransactionSplit == null)
        {
            return;
        }

        DateTime createdLocalTime = CraveMobileNocHelper.ConvertUtcDateTimeToUserTimeZone(this.PaymentTransactionSplit.CreatedUTC);

        lblTitle.Text = PaymentTransactionSplit.SplitTypeText;
        lblReference.Text = PaymentTransactionSplit.ReferenceId;
        lblType.Text = PaymentTransactionSplit.SplitTypeText;
        
        SetAccountCode(PaymentTransactionSplit.AccountCode);

        lblAmount.Text = PaymentTransactionSplit.Amount.Format(PaymentTransactionSplit.PaymentTransactionEntity.OrderEntity.CultureCode);
        lblCreated.Text = createdLocalTime.ToString("F");
    }

    private void SetAccountCode(string accountCode)
    {
        plhAccountCode.Visible = !accountCode.IsNullOrWhiteSpace();
        lblAccountCode.Text = accountCode;
    }

    #endregion
}
