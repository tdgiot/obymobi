﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated. 
// </auto-generated>
//------------------------------------------------------------------------------



public partial class SignOnPanel
{

    /// <summary>
    /// panelLogin control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::System.Web.UI.WebControls.Panel panelLogin;

    /// <summary>
    /// tbLogin control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::Dionysos.Web.UI.WebControls.TextBoxString tbLogin;

    /// <summary>
    /// tbPassword control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::Dionysos.Web.UI.WebControls.TextBoxPassword tbPassword;

    /// <summary>
    /// btLogin control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::Dionysos.Web.UI.WebControls.Button btLogin;

    /// <summary>
    /// panelLoggedIn control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::System.Web.UI.WebControls.Panel panelLoggedIn;

    /// <summary>
    /// lblUsernameLabel control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::Dionysos.Web.UI.WebControls.LabelTextOnly lblUsernameLabel;

    /// <summary>
    /// lblUsername control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::Dionysos.Web.UI.WebControls.Label lblUsername;

    /// <summary>
    /// btSignOut control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::Dionysos.Web.UI.WebControls.Button btSignOut;
}
