﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="SignOnPanel" Codebehind="SignOnPanel.ascx.cs" %>
<asp:Panel runat="server" ID="panelLogin">
<ul>
    <li>
        <D:TextBoxString runat="server" ID="tbLogin" autocomplete="username" UseDataBinding="false" RenderLabel="false" UseValidation="false" Placeholder="<%$ Resources:strings, SignOn_username %>"/>
    </li>
    <li>
        <D:TextBoxPassword runat="server" ID="tbPassword" autocomplete="current-password" UseDataBinding="false" RenderLabel="false" UseValidation="false" Placeholder="<%$ Resources:strings, SignOn_password %>" TextMode="Password"/>                
    </li>
    <li>
        <D:Button runat="server" ID="btLogin" CssClass="button" Text="<%$ Resources:strings, SignOn_login %>" />
    </li>
</ul>
</asp:Panel>
<asp:Panel runat="server" ID="panelLoggedIn">
    <ul>
        <li>
            <div class="label"><D:LabelTextOnly ID="lblUsernameLabel" runat="server" Text="<%$ Resources:strings, SignOn_username %>"/>: <D:Label ID="lblUsername" runat="server"></D:Label></div>
        </li>
        <li>
            <D:Button runat="server" ID="btSignOut" CssClass="button" Text="<%$ Resources:strings, SignOn_logout %>" />
        </li>
    </ul>
</asp:Panel>