﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/CraveMobileNocBase.master" AutoEventWireup="true" Inherits="Deliverypoints" Codebehind="Deliverypoints.aspx.cs" %>
<%@ Reference VirtualPath="~/UserControls/Listitems/Deliverypoint.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<div id="settings">
    <div id="filter">
        <D:DropDownList ID="ddlFilter" runat="server" AutoPostBack="true">
            <asp:ListItem Text="<%$ Resources:strings, Deliverypoint_all_deliverypoints %>"></asp:ListItem>
            <asp:ListItem Text="<%$ Resources:strings, Deliverypoint_with_online_clients %>"></asp:ListItem>
            <asp:ListItem Text="<%$ Resources:strings, Deliverypoint_with_offline_clients %>"></asp:ListItem>
            <asp:ListItem Text="<%$ Resources:strings, Deliverypoint_without_clients %>"></asp:ListItem>
        </D:DropDownList>
    </div>
    <div id="sort">
        <D:DropDownList ID="ddlSort" runat="server" AutoPostBack="true">
            <asp:ListItem Text="<%$ Resources:strings, Deliverypoint_number_asc %>"></asp:ListItem>
            <asp:ListItem Text="<%$ Resources:strings, Deliverypoint_last_request_desc %>"></asp:ListItem>
        </D:DropDownList>
    </div>
    <div id="title"><D:LabelTextOnly ID="lblTitle" runat="server"></D:LabelTextOnly></div>

</div>
<ul class="list">
    <D:PlaceHolder ID="plhList" runat="server"></D:PlaceHolder>
</ul>    
</asp:Content>

