﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/CraveMobileNocBase.master" AutoEventWireup="true" Inherits="Products" Codebehind="Products.aspx.cs" %>
<%@ Reference VirtualPath="~/UserControls/Listitems/Product.ascx" %>

<asp:Content ID="Content2" ContentPlaceHolderID="plhStylesheets" runat="server">
    <link rel="stylesheet" type="text/css" href="Css/Products/Products.css" />
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div id="titleHeader">
        <h1>Menu Items</h1><br>
        <h2>Hide or Unhide menu items</h2>
    </div>
    <div id="settings">
        <div class="category-group">
             <D:DropDownList ID="ddlFilter" runat="server" AutoPostBack="false">
                 <asp:ListItem Value="0" Text="<%$ Resources:strings, Products_all %>"></asp:ListItem>
                 <asp:ListItem Value="1" Text="<%$ Resources:strings, Products_visible_always %>"></asp:ListItem>
                 <asp:ListItem Value="2" Text="<%$ Resources:strings, Products_visible_never %>"></asp:ListItem>
             </D:DropDownList>
            <D:Button ID="btnApplyFilter" Text="Apply" runat="server" />
        </div>
         <div class="search-group">
             <D:TextBox ID="tbSearch" AutoPostBack="false" Placeholder="Search menu items..." runat="server" />
             <D:Button ID="btnSearch" Text="Search" runat="server" /> 
         </div>
    </div>
    <% if (ProductCount == 0){ %>
        <h1 class="no-search-results-found-text">No search results found</h1>
        <% }%>
       <ul id="product-list" class="list">
        <D:PlaceHolder ID="plhList" runat="server"></D:PlaceHolder>
    </ul>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="plhScripts" runat="server">
    <script type="text/javascript" src="Js/Products/Products.js"></script>
</asp:Content>
