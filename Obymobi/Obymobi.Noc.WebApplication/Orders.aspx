﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/CraveMobileNocBase.master" AutoEventWireup="true" Inherits="Orders" Codebehind="Orders.aspx.cs" %>
<%@ Reference VirtualPath="~/UserControls/Listitems/Order.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<div id="settings">
    <div class="order-selection">
        <D:DropDownList ID="ddlFilter" runat="server" AutoPostBack="true">
            <asp:ListItem Text="<%$ Resources:strings, Order_all_orders %>"></asp:ListItem>
            <asp:ListItem Text="<%$ Resources:strings, Order_processed_orders %>"></asp:ListItem>
            <asp:ListItem Text="<%$ Resources:strings, Order_unprocessed_orders %>"></asp:ListItem>
            <asp:ListItem Text="<%$ Resources:strings, Order_waiting_for_payment %>"></asp:ListItem>
        </D:DropDownList>
        <D:DropDownList ID="ddlOrderCount" runat="server" AutoPostBack="true">
            <asp:ListItem Text="50"/>
            <asp:ListItem Text="100"/>
            <asp:ListItem Text="250"/>
            <asp:ListItem Text="500"/>
        </D:DropDownList>
    </div>
    <div class="order-search">
            <D:TextBoxMobileNumeric ID="tbSearch" AutoPostBack="false" Placeholder="Enter order ID number..." runat="server" />
            <D:Button ID="btnSearch" Text="Search" runat="server" />
        </div>
</div>

<ul class="list">
    <D:PlaceHolder ID="plhList" runat="server"></D:PlaceHolder>
</ul>
    <% if (Count == 0) { %>
        <h1 class="no-search-results-found-text"><%= EmptyResultText %></h1>
        <% } %>
</asp:Content>

