﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/CraveMobileNocBase.master" AutoEventWireup="true" Inherits="Statistics" Codebehind="Statistics.aspx.cs" %>
<%@ Reference VirtualPath="~/UserControls/Listitems/Statistic.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div id="titleHeader">
        <h1>Statistics</h1>                
        <D:PlaceHolder ID="plhStatisticsPopupPh" runat="server" Visible="true">
            <div class="value"><D:PlaceHolder ID="plhStatisticsPopupBtn" runat="server"></D:PlaceHolder></div>
        </D:PlaceHolder>
        <h2 class="StatisticsH2">View the number of orders for your venue</h2>
    </div>
<D:PlaceHolder ID="plhList" runat="server"></D:PlaceHolder>
</asp:Content>

