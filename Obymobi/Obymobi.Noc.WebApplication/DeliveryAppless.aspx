﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/CraveMobileNocBase.master" AutoEventWireup="true" Inherits="DeliveryAppless" CodeBehind="DeliveryAppless.aspx.cs" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
	<link href="Css/Delivery.css" rel="stylesheet" />
	
	<div class="outletContainer">

	<h3>Mobile Ordering Wait Time</h3>

	<div id="outletApplessGrid" runat="server" class="outletApplessGrid">

		<b>Outlet</b>
		   <b>Wait Time (min)</b>
		<D:PlaceHolder ID="plhList" runat="server"></D:PlaceHolder>
	</div>
	
	<div class="buttonContainer">
			<asp:Button Text="Update Wait Times" ID="AppLessSaveButton" runat="server" CssClass="listButton" />
		</div></div>
	<script src="Js/Outlets/DeliveryOutlets.js"></script>

</asp:Content>
