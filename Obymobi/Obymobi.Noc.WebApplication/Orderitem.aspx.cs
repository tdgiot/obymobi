﻿using System;
using Obymobi.Data.EntityClasses;
using Obymobi.Enums;

public partial class Orderitem : PageEntity<OrderitemEntity>
{
    #region Event handlers

    protected void Page_Load(object sender, EventArgs e)
    {
        this.InitializeEntity();

        if (this.Entity != null)
        {
            this.Title = string.Format("{0} '{1} × {2}' - {3}", Resources.strings.Order, this.Entity.Quantity, this.Entity.ProductName, Resources.strings.Crave_NOC);

            switch (this.Entity.OrderEntity.StatusAsEnum)
            {
                case OrderStatus.NotRouted:
                    this.imgOrderitem.ImageUrl = ResolveUrl("~/Images/Icons/exclamation.png");
                    this.lblTitle.Text = string.Format(Resources.strings.Orderitem_could_not_be_routed, this.Entity.Quantity, this.Entity.ProductName);
                    this.lblOrderStatus.CssClass = "error";
                    break;
                case OrderStatus.WaitingForPayment:
                    this.imgOrderitem.ImageUrl = ResolveUrl("~/Images/Icons/hourglass.png");
                    this.lblTitle.Text = string.Format(Resources.strings.Orderitem_is_waiting_for_payment, this.Entity.Quantity, this.Entity.ProductName);
                    break;
                case OrderStatus.Routed:
                    this.imgOrderitem.ImageUrl = ResolveUrl("~/Images/Icons/exclamation.png");
                    this.lblTitle.Text = string.Format(Resources.strings.Orderitem_is_routed_but_not_started, this.Entity.Quantity, this.Entity.ProductName);
                    this.lblOrderStatus.CssClass = "error";
                    break;
                case OrderStatus.InRoute:
                    this.imgOrderitem.ImageUrl = ResolveUrl("~/Images/Icons/hourglass.png");
                    this.lblTitle.Text = string.Format(Resources.strings.Orderitem_is_currently_being_processed, this.Entity.Quantity, this.Entity.ProductName);
                    break;
                case OrderStatus.WaitingForPaymentCompleted:
                    this.imgOrderitem.ImageUrl = ResolveUrl("~/Images/Icons/hourglass.png");
                    this.lblTitle.Text = string.Format(Resources.strings.Orderitem_is_waiting_for_payment_complete, this.Entity.Quantity, this.Entity.ProductName);
                    break;
                case OrderStatus.Processed:
                    this.imgOrderitem.ImageUrl = ResolveUrl("~/Images/Icons/checkmark.png");
                    if (this.Entity.OrderEntity.ErrorCode == 0)
                    {
                        this.lblTitle.Text = string.Format(Resources.strings.Orderitem_is_successfully_processed, this.Entity.Quantity, this.Entity.ProductName);
                    }
                    else
                    {
                        this.lblTitle.Text = string.Format(Resources.strings.Orderitem_is_manually_processed, this.Entity.Quantity, this.Entity.ProductName);
                        this.lblOrderErrorCode.CssClass = "error";
                    }
                    break;
                case OrderStatus.Unprocessable:
                    this.imgOrderitem.ImageUrl = ResolveUrl("~/Images/Icons/error.png");
                    this.lblTitle.Text = string.Format(Resources.strings.Orderitem_could_not_be_processed, this.Entity.Quantity, this.Entity.ProductName);
                    this.lblOrderStatus.CssClass = "error";
                    this.lblOrderErrorCode.CssClass = "error";
                    break;
            }

            if (this.Entity.OrderEntity.MasterOrderId.HasValue)
            {
                this.hlMasterOrder.Text = this.Entity.OrderEntity.MasterOrderId.ToString();
                this.hlMasterOrder.NavigateUrl = ResolveUrl(string.Format("~/Order.aspx?id={0}", this.Entity.OrderEntity.MasterOrderId.Value));
            }
            this.hlOrderId.Text = this.Entity.OrderId.ToString();
            this.hlOrderId.NavigateUrl = ResolveUrl(string.Format("~/Order.aspx?id={0}", this.Entity.OrderId));
            this.lblOrderStatus.Text = string.Format("{0} ({1})", this.Entity.OrderEntity.StatusText, this.Entity.OrderEntity.Status);
            this.lblOrderErrorCode.Text = string.Format("{0} ({1})", this.Entity.OrderEntity.ErrorCodeAsEnum, this.Entity.OrderEntity.ErrorCode);
            this.lblOrderitemNotes.Text = this.Entity.Notes;

            // Alterations
            if (this.Entity.OrderitemAlterationitemCollection != null && this.Entity.OrderitemAlterationitemCollection.Count > 0)
            {
                foreach (var orderitemAlterationitem in this.Entity.OrderitemAlterationitemCollection)
                {
                    UserControls_Listitems_OrderitemAlterationitem orderitemAlterationitemControl = this.LoadControl<UserControls_Listitems_OrderitemAlterationitem>("~/UserControls/Listitems/OrderitemAlterationitem.ascx");
                    orderitemAlterationitemControl.OrderitemAlterationitem = orderitemAlterationitem;
                    this.plhAlterationsList.Controls.Add(orderitemAlterationitemControl);
                }
            }
            else
                this.plhAlterations.Visible = false;
        }
    }

    #endregion
}