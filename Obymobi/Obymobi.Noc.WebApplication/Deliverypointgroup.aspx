﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/CraveMobileNocBase.master" AutoEventWireup="true" Inherits="Deliverypointgroup" Codebehind="Deliverypointgroup.aspx.cs" %>
<%@ Reference VirtualPath="~/UserControls/Listitems/Deliverypoint.ascx" %>
<%@ Reference VirtualPath="~/UserControls/Listitems/Order.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<ul class="details">
    <li>
        <div class="title"><D:Label ID="lblTitle" runat="server"></D:Label></div>
    </li>
    <li>
        <div class="label"><D:LabelTextOnly ID="lblNameLabel" runat="server" Text="<%$ Resources:strings, Deliverypointgroup_name %>"></D:LabelTextOnly>:</div>
        <div class="value"><D:Label ID="lblName" runat="server"></D:Label></div>
    </li>
</ul>
<!-- Actions -->
<h1><D:LabelTextOnly ID="lblFunctions" runat="server" Text="<%$ Resources:strings, Functions %>"></D:LabelTextOnly></h1>
<ul class="buttonlist">
    <li><D:HyperLink ID="hlOrderTime" runat="server" Text="<%$ Resources:strings, Delivery_Order_Time %>"></D:HyperLink></li>
    <li><D:HyperLink ID="hlOpenInCms" runat="server" Target="_blank" Text="<%$ Resources:strings, Generic_open_in_cms %>"></D:HyperLink></li>
</ul>
<!-- Deliverypoints -->
<D:PlaceHolder ID="plhDeliverypoints" runat="server">
<h1><D:LabelTextOnly ID="lblDeliverypoints" runat="server" Text="<%$ Resources:strings, Deliverypoints %>"></D:LabelTextOnly></h1>
<ul class="list sub">
    <D:PlaceHolder ID="plhDeliverypointsList" runat="server"></D:PlaceHolder>
</ul>    
</D:PlaceHolder>
<!-- Recent Orders -->
<D:PlaceHolder ID="plhOrders" runat="server">
    <h1><D:LabelTextOnly ID="lblOrders" runat="server" Text="<%$ Resources:strings, Orders %>"></D:LabelTextOnly></h1>
    <ul class="list sub">
        <D:PlaceHolder ID="plhOrdersList" runat="server"></D:PlaceHolder>
    </ul>  
</D:PlaceHolder>
</asp:Content>

