﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/CraveMobileNocBase.master" AutoEventWireup="true" Inherits="CustomerStatus" Codebehind="CustomerStatus.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <D:PlaceHolder runat="server" ID="plhWarningPanel" Visible="false">
        <div id="customerstatus">
            <h1 class="red"><D:Label runat="server" ID="lblTitle">Onverwerkte orders</D:Label></h1>
            <D:PlaceHolder runat="server" ID="plhUnknownProblem" Visible="false">
                <p>Op dit moment kunnen Otoucho orders niet worden verwerkt. U kunt uw systeem weer vrijgeven door hieronder te klikken op 'Systeem Vrijgeven'.<br />
                <br /> 
                Wanneer dit voor een tweede keer gebeurt in korte tijd dan moet u de kassa opnieuw opstarten.<br />
                <br />
                Als dit probleem zich voor blijft doen kunt u contact opnemen met onze storingsdienst via 0174 614014.<br />
                <br />
                De volgende tafel(s) hebben geprobeerd te bestellen: <D:LabelTextOnly runat="server" ID="lblUnknownProblemTableNumber"></D:LabelTextOnly></p>
            </D:PlaceHolder>
            <D:PlaceHolder runat="server" ID="plhClearHoldBuffers" Visible="false">
                <p>De kassaintegratie werkt niet meer naar behoren. U kunt de functie 'Clear Hold Buffers' uit te voeren, of op andere wijze de kassa herstarten en kunt daarna op de knop 'Systeem Vrijgeven' hieronder drukken om het systeem weer vrij te geven. Als het herstarten niet lukt kunt u de eerste keer ook proberen het systeem direct vrij te geven.<br />
                <br />
                De volgende tafel(s) hebben geprobeerd te bestellen: <D:LabelTextOnly runat="server" ID="lblClearHoldBuffersTableNumber"></D:LabelTextOnly></p>
            </D:PlaceHolder>
            <D:PlaceHolder runat="server" ID="plhTableWasLocked" Visible="false">
                <p>Er is een tafel open op uw kassa en/of handheld. Er is meerdere malen geprobeerd een bestelling te doen voor tafel(s) <D:LabelTextOnly runat="server" ID="lblTableNumberLocked"></D:LabelTextOnly> maar deze was op uw kassa ingebruik door u of uw medewerkers. <br />
                <br />
                Als deze tafel weer vrij is op uw kassa kunt u onderaan klikken op 'Systeem Vrijgeven'</p>
            </D:PlaceHolder>
            <D:PlaceHolder runat="server" ID="plhRestartPos" Visible="false">
                <p>De kassaintegratie werkt niet meer naar behoren. De kassa dient herstart te worden, indien u dit niet zelf kunt dient u contact op te nemen met de kassaleverancier.<br />
                <br />
                 Dit was voor tafel(s) <D:LabelTextOnly runat="server" ID="lblRestartPosTableNumber"></D:LabelTextOnly> </p>
            </D:PlaceHolder>
            <D:PlaceHolder runat="server" ID="plhUnOrderableProduct" Visible="false">
                <p>Er is geprobeerd een product (en/of optie) te bestellen die de kassa niet kan verwerken.<br />
                <br />
                 Dit was voor tafel(s) <D:LabelTextOnly runat="server" ID="lblTableNumberUnOrderableProduct"></D:LabelTextOnly> </p>
            </D:PlaceHolder>            
            <p><D:LinkButton runat="server" ID="btProcessOrdersManually" CssClass="button">Systeem Vrijgeven</D:LinkButton></p>
            <p><small>Na het vrijgeven moet Otoucho na ca. 2 - 5 minuten weer naar behoren functioneren. Mocht u toch nog problemen hebben neem dan contact op met onze storingsdienst via 0174 614014</small></p>
        </div>
    </D:PlaceHolder>
    <D:PlaceHolder runat="server" ID="plhNoWarnings" Visible="false">
        <div class="nowarnings">
            <h1 class="green">Otoucho orderverwerking: OK</h1>
            <p>Op dit moment worden alle orders verwerkt en werkt Otoucho naar behoren.<br /><br />Ondervindt u toch problemen neem dan contact op met onze storingsdienst via 0174 614014.</p>
        </div>
    </D:PlaceHolder>
</asp:Content>

