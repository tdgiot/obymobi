﻿using System;
using Dionysos.Web;

public partial class Error : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        this.Title = string.Format("Error - {0}", Resources.strings.Crave_NOC);

        string message = "Sorry, something went wrong while opening the requested page :(";

        if (QueryStringHelper.HasValue("code"))
        {
            ErrorCodes errorCode;
            if (Enum.TryParse(QueryStringHelper.GetString("code"), out errorCode))
            {
                switch (errorCode)
                {
                    case ErrorCodes.NoContent:
                        message = Resources.strings.Error_204;
                        break;
                    case ErrorCodes.Unauthorized:
                        message = Resources.strings.Error_401;
                        break;
                    case ErrorCodes.NotFound:
                        message = Resources.strings.Error_404;
                        break;
                }
            }
        }

        this.lblMessage.Text = message;
    }
}