﻿using System;
using System.Collections.Generic;
using System.Linq;
using Obymobi.Data.EntityClasses;
using Dionysos;
using Obymobi.Logic.HelperClasses;
using Obymobi.Noc.Logic.HelperClasses;
using Dionysos.Data.LLBLGen;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Constants;
using Obymobi.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;
using SpreadsheetLight;
using Obymobi.Enums;

public partial class Company : PageEntity<CompanyEntity>
{
    private void HookUpEvents()
    {
        this.btOfflineClientsReport.Click += btOfflineClientsReport_Click;
    }

    #region Event handlers

    void btOfflineClientsReport_Click(object sender, EventArgs e)
    {
        SLDocument report = ClientHelper.GetOfflineClientsReport(5, this.Entity.CompanyId, null);
        Response.Clear();
        Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
        Response.AddHeader("Content-Disposition", "attachment; filename=OfflineClients-{0}-{1}.xlsx".FormatSafe(this.Entity.CompanyId, DateTime.Now.DateTimeToSimpleDateTimeStamp()));
        report.SaveAs(Response.OutputStream);
        Response.End();
    }

    private void ShowHideApplessLinks()
    {
        ApplicationConfigurationCollection applicationConfigurationCollection = new ApplicationConfigurationCollection();
        int count = applicationConfigurationCollection.GetDbCount(new PredicateExpression(ApplicationConfigurationFields.CompanyId == this.Entity.CompanyId));

        bool hasAppless = count > 0;

        this.hlProducts.Visible = hasAppless;
        this.hlOutlets.Visible = hasAppless;
        this.hlOrderTimeAppless.Visible = hasAppless;
    }

    private void ShowHideCraveStaffActions()
    {
        bool isCrave = CraveMobileNocHelper.CurrentUser.Role >= Role.Crave;
        this.hlDisconnectedFromComet.Visible = isCrave;
        this.hlVersions.Visible = isCrave;
        this.hlOpenInCms.Visible = isCrave;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        this.InitializeEntity();

        this.HookUpEvents();
        if (this.Entity != null)
        {
            this.ShowHideApplessLinks();
            this.ShowHideCraveStaffActions();

            this.Title = string.Format("{0} '{1}' - {2}", Resources.strings.Company, this.Entity.Name, Resources.strings.Crave_NOC);

            this.imgCompany.ImageUrl = ResolveUrl("~/Images/Icons/checkmark.png");
            this.lblTitle.Text = string.Format(Resources.strings.Company_has_no_issues, this.Entity.Name);

            DateTime? lastTerminalActivity = NocHelper.GetLastTerminalActivity(this.Entity.CompanyId);
            DateTime? lastClientActivity = NocHelper.GetLastClientActivity(this.Entity.CompanyId);
            DateTime? lastOrderPlaced = NocHelper.GetLastOrderPlaced(this.Entity.CompanyId);

            if (lastTerminalActivity.HasValue)
                this.lblLastTerminalActivity.Text = string.Format("{0} ({1})", CraveMobileNocHelper.ConvertUtcDateTimeToUserTimeZone(lastTerminalActivity.Value), CraveMobileNocHelper.GetSpanTextBetweenDateTimeAndUtcNow(lastTerminalActivity.Value));
            else
                this.lblLastTerminalActivity.Text = Resources.strings.Unknown;

            if (lastClientActivity.HasValue)
                this.lblLastClientActivity.Text = string.Format("{0} ({1})", CraveMobileNocHelper.ConvertUtcDateTimeToUserTimeZone(lastClientActivity.Value), CraveMobileNocHelper.GetSpanTextBetweenDateTimeAndUtcNow(lastClientActivity.Value));
            else
                this.lblLastClientActivity.Text = Resources.strings.Unknown;

            if (lastOrderPlaced.HasValue)
                this.lblLastOrderPlaced.Text = string.Format("{0} ({1})", CraveMobileNocHelper.ConvertUtcDateTimeToUserTimeZone(lastOrderPlaced.Value), CraveMobileNocHelper.GetSpanTextBetweenDateTimeAndUtcNow(lastOrderPlaced.Value));
            else
                this.lblLastOrderPlaced.Text = Resources.strings.Unknown;

            string title = string.Empty;
            string status = string.Empty;
            
            // Faulty orders
            int orderTotal = NocOrderHelper.GetFaultyOrderCountPastDay(new List<int> { this.Entity.CompanyId } );
            if (orderTotal > 0)
            {
                if (orderTotal == 1)
                {
                    title += Resources.strings.One_unprocessed_order;
                    status += string.Format("<li><a href=\"{0}\">{1}</a></li>", ResolveUrl(string.Format("~/Orders.aspx?cid={0}", this.Entity.CompanyId)), Resources.strings.One_unprocessed_order);
                }
                else
                {
                    title += string.Format(Resources.strings.X_unprocessed_orders, orderTotal);
                    status += string.Format("<li><a href=\"{0}\">{1}</a></li>", ResolveUrl(string.Format("~/Orders.aspx?cid={0}", this.Entity.CompanyId)), string.Format(Resources.strings.X_unprocessed_orders, orderTotal));
                }
            }

            // Clients in non ordering mode
            int nonOrderingClientCount = NocCompanyHelper.GetNonOrderingClientsGroupedByOperationModeCount(new List<int> { this.Entity.CompanyId } );
            if (nonOrderingClientCount > 0)
            {
                if (title.Length > 0)
                    title += ", ";

                if (nonOrderingClientCount == 1)
                {
                    title += Resources.strings.One_non_ordering_client;
                    status += string.Format("<li><a href=\"{0}\">{1}</a></li>", ResolveUrl(string.Format("~/Clients.aspx?cid={0}", this.Entity.CompanyId)), Resources.strings.One_non_ordering_client);
                }
                else
                {
                    title += string.Format(Resources.strings.X_non_ordering_clients, nonOrderingClientCount);
                    status += string.Format("<li><a href=\"{0}\">{1}</a></li>", ResolveUrl(string.Format("~/Clients.aspx?cid={0}", this.Entity.CompanyId)), string.Format(Resources.strings.X_non_ordering_clients, nonOrderingClientCount));
                }
            }

            // Get all of the terminals with online clients
            TerminalCollection terminals = NocCompanyHelper.GetTerminals(this.Entity.CompanyId);

            // Terminal count
            int terminalCount = terminals.Count;
            this.lblTerminalsCount.Text = terminalCount.ToString();

            // Online
            int terminalOnlineCount = terminals.Count(x => x.DeviceId.HasValue && x.DeviceEntity.LastRequestUTC.HasValue && x.DeviceEntity.LastRequestUTC.Value > DateTime.UtcNow.AddMilliseconds(ObymobiIntervals.DEVICE_OFFLINE_THRESHOLD * -1));
            this.lblTerminalsOnlineCount.Text = string.Format("{0} ({1}%)", terminalOnlineCount, Dionysos.Math.Percentage(terminalOnlineCount, terminalCount, true));

            // Offline (battery > 6)
            int terminalOfflineCount = terminals.Count(x => x.DeviceId.HasValue && (!x.DeviceEntity.LastRequestUTC.HasValue || x.DeviceEntity.LastRequestUTC.Value <= DateTime.UtcNow.AddMilliseconds(ObymobiIntervals.DEVICE_OFFLINE_THRESHOLD * -1)) && (!x.DeviceEntity.BatteryLevel.HasValue || x.DeviceEntity.BatteryLevel > 6));
            this.lblTerminalsOfflineCount.Text = string.Format("{0} ({1}%)", terminalOfflineCount, Dionysos.Math.Percentage(terminalOfflineCount, terminalCount, true));

            // Offline (battery <= 6)
            int terminalOfflineBatteryCount = terminals.Count(x => x.DeviceId.HasValue && x.DeviceEntity.LastRequestUTC.HasValue && x.DeviceEntity.LastRequestUTC.Value <= DateTime.UtcNow.AddMilliseconds(ObymobiIntervals.DEVICE_OFFLINE_THRESHOLD * -1) && (!x.DeviceEntity.BatteryLevel.HasValue || x.DeviceEntity.BatteryLevel <= 6));
            this.lblTerminalsOfflineBatteryCount.Text = string.Format("{0} ({1}%)", terminalOfflineBatteryCount, Dionysos.Math.Percentage(terminalOfflineBatteryCount, terminalCount, true));

            // Offline terminals
            int offlineTerminalCount = terminalOfflineCount + terminalOfflineBatteryCount;
            if (offlineTerminalCount > 0)
            {
                if (title.Length > 0)
                    title += ", ";

                if (offlineTerminalCount == 1)
                {
                    title += Resources.strings.One_offline_terminal;
                    status += string.Format("<li><a href=\"{0}\">{1}</a></li>", ResolveUrl(string.Format("~/Terminals.aspx?cid={0}", this.Entity.CompanyId)), Resources.strings.One_offline_terminal);
                }
                else
                {
                    title += string.Format(Resources.strings.X_offline_terminals, offlineTerminalCount);
                    status += string.Format("<li><a href=\"{0}\">{1}</a></li>", ResolveUrl(string.Format("~/Terminals.aspx?cid={0}", this.Entity.CompanyId)), string.Format(Resources.strings.X_offline_terminals, offlineTerminalCount));
                }
            }

            // Get all of the clients
            PrefetchPath clientPrefetchPath = new PrefetchPath(EntityType.ClientEntity);
            clientPrefetchPath.Add(ClientEntityBase.PrefetchPathDeviceEntity);

            IPredicateExpression filter = new PredicateExpression();
            filter.Add(ClientFields.CompanyId == this.Entity.CompanyId);

            ClientCollection clients = EntityCollection.GetMulti<ClientCollection>(filter, null, ClientEntityBase.Relations.DeviceEntityUsingDeviceId, clientPrefetchPath);

            // Client count
            int clientCount = clients.Count;
            this.lblClientsCount.Text = clientCount.ToString();

            // Online
            int clientOnlineCount = clients.Count(x => x.DeviceId.HasValue && x.DeviceEntity.LastRequestUTC.HasValue && x.DeviceEntity.LastRequestUTC.Value > DateTime.UtcNow.AddMilliseconds(ObymobiIntervals.DEVICE_OFFLINE_THRESHOLD * -1));
            this.lblClientsOnlineCount.Text = string.Format("{0} ({1}%)", clientOnlineCount, Dionysos.Math.Percentage(clientOnlineCount, clientCount, true));

            // Offline (battery > 6)
            int clientOfflineCount = clients.Count(x => x.DeviceId.HasValue && (!x.DeviceEntity.LastRequestUTC.HasValue || x.DeviceEntity.LastRequestUTC.Value <= DateTime.UtcNow.AddMilliseconds(ObymobiIntervals.DEVICE_OFFLINE_THRESHOLD * -1)) && (!x.DeviceEntity.BatteryLevel.HasValue || x.DeviceEntity.BatteryLevel > 6));
            this.lblClientsOfflineCount.Text = string.Format("{0} ({1}%)", clientOfflineCount, Dionysos.Math.Percentage(clientOfflineCount, clientCount, true));

            // Offline (battery <= 6)
            int clientOfflineBatteryCount = clients.Count(x => x.DeviceId.HasValue && x.DeviceEntity.LastRequestUTC.HasValue && x.DeviceEntity.LastRequestUTC.Value <= DateTime.UtcNow.AddMilliseconds(ObymobiIntervals.DEVICE_OFFLINE_THRESHOLD * -1) && (!x.DeviceEntity.BatteryLevel.HasValue || x.DeviceEntity.BatteryLevel <= 6));
            this.lblClientsOfflineBatteryCount.Text = string.Format("{0} ({1}%)", clientOfflineBatteryCount, Dionysos.Math.Percentage(clientOfflineBatteryCount, clientCount, true));

            int clientsApiV1 = clients.Count(x => x.LastApiVersion == 1 && x.DeviceId.HasValue && x.DeviceEntity.LastRequestUTC.HasValue && x.DeviceEntity.LastRequestUTC.Value > DateTime.UtcNow.AddMilliseconds(ObymobiIntervals.DEVICE_OFFLINE_THRESHOLD * -1));
            int clientsApiV1Percentage = clientOnlineCount > 0 ? (int)(((double)clientsApiV1 / clientOnlineCount) * 100.0) : 0;
            int clientsApiV2 = clients.Count(x => x.LastApiVersion == 2 && x.DeviceId.HasValue && x.DeviceEntity.LastRequestUTC.HasValue && x.DeviceEntity.LastRequestUTC.Value > DateTime.UtcNow.AddMilliseconds(ObymobiIntervals.DEVICE_OFFLINE_THRESHOLD * -1));
            int clientsApiV2Percentage = clientOnlineCount > 0 ? (int)(((double)clientsApiV2 / clientOnlineCount) * 100.0) : 0;

            this.lblApiV1Count.Text = string.Format("{0}/{1} ({2}%)", clientsApiV1, clientOnlineCount, clientsApiV1Percentage);
            this.lblApiV2Count.Text = string.Format("{0}/{1} ({2}%)", clientsApiV2, clientOnlineCount, clientsApiV2Percentage);

            if (!this.Entity.Telephone.IsNullOrWhiteSpace())
            {
                this.hlTelephone.Text = this.Entity.Telephone.RemoveAllNonDigitCharacters();
                this.hlTelephone.NavigateUrl = "tel:" + this.Entity.Telephone.RemoveAllNonDigitCharacters();
            }

            if (title.IsNullOrWhiteSpace())
            {
                this.imgCompany.ImageUrl = ResolveUrl("~/Images/Icons/checkmark.png");
                this.lblTitle.Text = string.Format(Resources.strings.Company_has_no_issues, this.Entity.Name);
            }
            else
            {
                this.imgCompany.ImageUrl = ResolveUrl("~/Images/Icons/error.png");
                this.lblTitle.Text = string.Format(Resources.strings.Company_has_issues, this.Entity.Name, title);
            }

            if (status.IsNullOrWhiteSpace())
                this.plhStatus.AddHtml(Resources.strings.No_issues);
            else
                this.plhStatus.AddHtml("<ul>{0}</ul>", status);

            // Set the button links
            if (!this.Entity.Telephone.IsNullOrWhiteSpace())
                this.hlCallCustomer.NavigateUrl = "tel:" + this.Entity.Telephone.RemoveAllNonDigitCharacters();
            else
                this.hlCallCustomer.Visible = false;
                        
            this.hlTerminals.NavigateUrl = ResolveUrl(string.Format("~/Terminals.aspx?cid={0}", this.Entity.CompanyId));
            this.hlClients.NavigateUrl = ResolveUrl(string.Format("~/Clients.aspx?cid={0}", this.Entity.CompanyId));
            this.hlOrders.NavigateUrl = ResolveUrl(string.Format("~/Orders.aspx?cid={0}", this.Entity.CompanyId));
            this.hlOutlets.NavigateUrl = ResolveUrl(string.Format("~/Outlets.aspx?cid={0}", this.Entity.CompanyId));
            this.hlProducts.NavigateUrl = ResolveUrl(string.Format("~/Products.aspx?cid={0}", this.Entity.CompanyId));
            this.hlDeliverypointgroups.NavigateUrl = ResolveUrl(string.Format("~/Deliverypointgroups.aspx?cid={0}", this.Entity.CompanyId));
            this.hlDeliverypoints.NavigateUrl = ResolveUrl(string.Format("~/Deliverypoints.aspx?cid={0}", this.Entity.CompanyId));
            this.hlDeliverypointgroups.NavigateUrl = ResolveUrl(string.Format("~/Deliverypointgroups.aspx?cid={0}", this.Entity.CompanyId));
            this.hlLatestActivity.NavigateUrl = ResolveUrl(string.Format("~/LatestActivity.aspx?cid={0}", this.Entity.CompanyId));
            this.hlVersions.NavigateUrl = ResolveUrl(string.Format("~/Versions.aspx?cid={0}", this.Entity.CompanyId));
            this.hlDisconnectedFromComet.NavigateUrl = ResolveUrl(string.Format("~/DisconnectedFromComet.aspx?cid={0}", this.Entity.CompanyId));
            this.hlOpenInCms.NavigateUrl = this.GetCmsUrl(this.Entity.CompanyId, "Company/Company.aspx?id={0}", this.Entity.CompanyId);
            this.hlOrderTime.NavigateUrl = ResolveUrl(string.Format("~/Delivery.aspx?cid={0}", this.Entity.CompanyId));
            this.hlOrderTimeAppless.NavigateUrl = ResolveUrl(string.Format("~/DeliveryAppless.aspx?cid={0}", this.Entity.CompanyId));

        }
    }

    #endregion
}
