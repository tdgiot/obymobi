﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/CraveMobileNocBase.master" AutoEventWireup="true" Inherits="Versions" Codebehind="Versions.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <ul class="details">
        <li>            
            <div class="title"><D:LabelTextOnly runat="server" Text="<%$ Resources:strings, Versions_clients %>"></D:LabelTextOnly></div>
        </li>
        <li class="spacer"></li>
        <li>            
            <div class="subtitle"><D:LabelTextOnly runat="server" Text="<%$ Resources:strings, Versions_clients_application %>"></D:LabelTextOnly></div>
        </li>
        <D:PlaceHolder ID="plhClientApplicationVersions" runat="server"></D:PlaceHolder>
        <li class="spacer"></li>
        <li>            
            <div class="subtitle"><D:LabelTextOnly runat="server" Text="<%$ Resources:strings, Versions_clients_agent %>"></D:LabelTextOnly></div>
        </li>
        <D:PlaceHolder ID="plhClientAgentVersions" runat="server"></D:PlaceHolder>
        <li class="spacer"></li>
        <li>            
            <div class="subtitle"><D:LabelTextOnly runat="server" Text="<%$ Resources:strings, Versions_clients_supporttools %>"></D:LabelTextOnly></div>
        </li>
        <D:PlaceHolder ID="plhClientSupportToolsVersions" runat="server"></D:PlaceHolder>
        <li class="spacer"></li>
    </ul>
    <ul class="details">
        <li>            
            <div class="title"><D:LabelTextOnly runat="server" Text="<%$ Resources:strings, Versions_terminals %>"></D:LabelTextOnly></div>
        </li>
        <li class="spacer"></li>
        <li>            
            <div class="subtitle"><D:LabelTextOnly runat="server" Text="<%$ Resources:strings, Versions_terminals_application %>"></D:LabelTextOnly></div>
        </li>
        <D:PlaceHolder ID="plhTerminalApplicationVersions" runat="server"></D:PlaceHolder>
        <li class="spacer"></li>
        <li>            
            <div class="subtitle"><D:LabelTextOnly runat="server" Text="<%$ Resources:strings, Versions_terminals_agent %>"></D:LabelTextOnly></div>
        </li>
        <D:PlaceHolder ID="plhTerminalAgentVersions" runat="server"></D:PlaceHolder>
        <li class="spacer"></li>
        <li>            
            <div class="subtitle"><D:LabelTextOnly runat="server" Text="<%$ Resources:strings, Versions_terminals_supporttools %>"></D:LabelTextOnly></div>
        </li>
        <D:PlaceHolder ID="plhTerminalSupportToolsVersions" runat="server"></D:PlaceHolder>
        <li class="spacer"></li>
    </ul>
</asp:Content>

