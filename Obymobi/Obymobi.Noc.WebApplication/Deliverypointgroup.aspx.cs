﻿using System;
using System.Linq;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Noc.Logic.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

public partial class Deliverypointgroup : PageEntity<DeliverypointgroupEntity>
{
    protected void Page_Load(object sender, EventArgs e)
    {
        this.InitializeEntity();

        if (this.Entity != null)
        {
            this.Title = string.Format("{0} '{1}' - {2}", Resources.strings.Deliverypointgroup, this.Entity.Name, Resources.strings.Crave_NOC);

            // Title
            this.lblTitle.Text = string.Format(Resources.strings.Deliverypointgroup_has_x_rooms, this.Entity.Name, this.Entity.DeliverypointCollection.Count);

            // Name
            this.lblName.Text = this.Entity.Name;

            // Deliverypoints
            this.Entity.DeliverypointCollection.Sort("NumberAsInteger", System.ComponentModel.ListSortDirection.Ascending, null);
            if (this.Entity.DeliverypointCollection.Count > 0)
            {
                foreach (DeliverypointEntity deliverypoint in this.Entity.DeliverypointCollection.Take(10))
                {
                    UserControls_Listitems_Deliverypoint deliverypointControl = this.LoadControl<UserControls_Listitems_Deliverypoint>("~/UserControls/Listitems/Deliverypoint.ascx");
                    deliverypointControl.Deliverypoint = deliverypoint;
                    this.plhDeliverypointsList.Controls.Add(deliverypointControl);
                }
            }

            // Orders                
            OrderCollection orderCollection = null;

            var filter = new PredicateExpression(DeliverypointFields.DeliverypointgroupId == this.Entity.DeliverypointgroupId);
            var relations = new RelationCollection(OrderEntityBase.Relations.DeliverypointEntityUsingDeliverypointId);
            orderCollection = NocHelper.GetOrders(filter, relations, false, 10);

            foreach (var order in orderCollection)
            {
                UserControls_Listitems_Order orderControl = this.LoadControl<UserControls_Listitems_Order>("~/UserControls/Listitems/Order.ascx");
                orderControl.Order = order;
                this.plhOrdersList.Controls.Add(orderControl);
            }

            this.plhOrders.Visible = (orderCollection.Count > 0);

            this.hlOpenInCms.NavigateUrl = this.GetCmsUrl(this.Entity.CompanyId, "Company/Deliverypointgroup.aspx?id={0}", this.Entity.DeliverypointgroupId);
            this.hlOrderTime.NavigateUrl = ResolveUrl(string.Format("~/Delivery.aspx?dpgid={0}", this.Entity.DeliverypointgroupId));
        }
    }
}