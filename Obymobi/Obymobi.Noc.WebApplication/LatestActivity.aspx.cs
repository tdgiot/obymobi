﻿using System;
using System.Collections.Generic;
using Dionysos.Web;
using Obymobi.Constants;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Interfaces;
using Obymobi.Logic.HelperClasses;
using Obymobi.Noc.Logic.HelperClasses;

public partial class LatestActivity : System.Web.UI.Page
{
    private const int MINUTES = 30;

    private List<int> GetCurrentCompanyId()
    {
        if (QueryStringHelper.HasValue("cid"))
            return new List<int> { QueryStringHelper.GetInt("cid") };

        if (CraveMobileNocHelper.AvailableCompanyIds.Count > 0)
            return CraveMobileNocHelper.AvailableCompanyIds;

        return CraveMobileNocHelper.EmptyIntList;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        this.Title = string.Format("{0} - {1}", Resources.strings.Latest_activity, Resources.strings.Crave_NOC);

        List<INocActivityEntity> activityList = this.GetActivityList();
        this.RenderActivityList(activityList);
    }

    private List<INocActivityEntity> GetActivityList()
    {
        List<INocActivityEntity> activityList = new List<INocActivityEntity>();

        List<int> companyIds = GetCurrentCompanyId();

        // Get the latest terminals
        TerminalCollection terminals = NocHelper.GetTerminals(companyIds, false, MINUTES);
        activityList.AddRange(terminals);

        // Get the latest clients
        ClientCollection clients = NocHelper.GetClients(companyIds, MINUTES, false);
        activityList.AddRange(clients);

        // Get the latest orders
        OrderCollection orders = NocHelper.GetOrders(companyIds, null, MINUTES, null);
        activityList.AddRange(orders);

        if (activityList.Count == 0)
        {
            this.plhNoActivity.Visible = true;
            this.lblMinutes.Text = string.Format(Resources.strings.LatestActivity_no_activity, MINUTES);
        }
        else
        {
            // Sort the list based on the latest
            activityList.Sort(new NocActivityComparer());
        }

        return activityList;
    }

    private void RenderActivityList(List<INocActivityEntity> activityList)
    {
        Dictionary<int, string> companyVersions = new Dictionary<int, string>();

        foreach (INocActivityEntity activityEntity in activityList)
        {
            if (activityEntity is TerminalEntity)
            {
                TerminalEntity terminal = activityEntity as TerminalEntity;
                
                UserControls_Listitems_Terminal terminalControl = this.LoadControl<UserControls_Listitems_Terminal>("~/UserControls/Listitems/Terminal.ascx");
                terminalControl.Terminal = terminal;
                this.plhList.Controls.Add(terminalControl);
            }
            else if (activityEntity is ClientEntity)
            {
                ClientEntity client = activityEntity as ClientEntity;

                string emenuReleaseVersion;
                if (!companyVersions.TryGetValue(client.CompanyId, out emenuReleaseVersion))
                {
                    emenuReleaseVersion = ReleaseHelper.GetVersionForCompany(ApplicationCode.Emenu, client.CompanyId);
                    companyVersions.Add(client.CompanyId, emenuReleaseVersion);
                }

                UserControls_Listitems_Client clientControl = this.LoadControl<UserControls_Listitems_Client>("~/UserControls/Listitems/Client.ascx");
                clientControl.Client = client;
                clientControl.EmenuReleaseVersion = emenuReleaseVersion;
                this.plhList.Controls.Add(clientControl);
            }
            else if (activityEntity is OrderEntity)
            {
                OrderEntity order = activityEntity as OrderEntity;

                UserControls_Listitems_Order orderControl = this.LoadControl<UserControls_Listitems_Order>("~/UserControls/Listitems/Order.ascx");
                orderControl.Order = order;
                this.plhList.Controls.Add(orderControl);
            }
        }
    }

    class NocActivityComparer : IComparer<INocActivityEntity>
    {
        public int Compare(INocActivityEntity x, INocActivityEntity y)
        {
            if (x.LastActivityUTC.HasValue && y.LastActivityUTC.HasValue)
                return x.LastActivityUTC.Value.CompareTo(y.LastActivityUTC.Value) * -1;
            
            if (x.LastActivityUTC.HasValue)
                return 1;
            
            return -1;
        }
    }
}