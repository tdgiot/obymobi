﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated. 
// </auto-generated>
//------------------------------------------------------------------------------



public partial class Deliverypointgroup
{

    /// <summary>
    /// lblTitle control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::Dionysos.Web.UI.WebControls.Label lblTitle;

    /// <summary>
    /// lblNameLabel control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::Dionysos.Web.UI.WebControls.LabelTextOnly lblNameLabel;

    /// <summary>
    /// lblName control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::Dionysos.Web.UI.WebControls.Label lblName;

    /// <summary>
    /// lblFunctions control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::Dionysos.Web.UI.WebControls.LabelTextOnly lblFunctions;

    /// <summary>
    /// hlOrderTime control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::Dionysos.Web.UI.WebControls.HyperLink hlOrderTime;

    /// <summary>
    /// hlOpenInCms control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::Dionysos.Web.UI.WebControls.HyperLink hlOpenInCms;

    /// <summary>
    /// plhDeliverypoints control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::Dionysos.Web.UI.WebControls.PlaceHolder plhDeliverypoints;

    /// <summary>
    /// lblDeliverypoints control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::Dionysos.Web.UI.WebControls.LabelTextOnly lblDeliverypoints;

    /// <summary>
    /// plhDeliverypointsList control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::Dionysos.Web.UI.WebControls.PlaceHolder plhDeliverypointsList;

    /// <summary>
    /// plhOrders control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::Dionysos.Web.UI.WebControls.PlaceHolder plhOrders;

    /// <summary>
    /// lblOrders control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::Dionysos.Web.UI.WebControls.LabelTextOnly lblOrders;

    /// <summary>
    /// plhOrdersList control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::Dionysos.Web.UI.WebControls.PlaceHolder plhOrdersList;
}
