﻿using System;
using System.Collections.Generic;
using Dionysos.Web.UI;
using Obymobi.Noc.Logic.HelperClasses;
using Obymobi.Data.EntityClasses;
using System.Data;
using System.Linq;
using Obymobi.Data.CollectionClasses;

public partial class Default : PageDefault
{
    private CompanyCollection Companies { get; set; }

    private protected void Page_Load(object sender, EventArgs e)
    {
        Companies = NocHelper.GetCompanies(CraveMobileNocHelper.AvailableCompanyIds);
        // If a single company is present, redirect to /Company?id=123, if multiple are present, redirect to /Companies.
        if (Companies.Count == 1)
            Response.Redirect("~/Company.aspx?id=" + Companies.First().CompanyId.ToString());
        else
            Response.Redirect("~/Companies.aspx");
    }

}
