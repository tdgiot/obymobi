﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="404.aspx.cs" Inherits="Obymobi.Noc.WebApplication._404" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server" id="HtmlHeader">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Crave Content Management System</title>
    <meta http-equiv="imagetoolbar" content="no" />
    <link href="~/favicon.ico" type="image/x-icon" link rel="shortcut icon" />
    <link href="~/css/base.css" type="text/css" link rel="Stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
        <div id="header">   
            <D:PlaceHolder ID="plhLogo" runat="server"></D:PlaceHolder>
        </div>
        <div id="content">
            <div style="padding: 30px;">
                The item that you are looking for does not exist.
            </div>
            <D:PlaceHolder ID="plhMessage" runat="server"></D:PlaceHolder> 
            <D:Button runat="server" ID="btnBack" Text="Go back" style="cursor: pointer;"/>	
        </div>
    </form>
</body>
</html>

