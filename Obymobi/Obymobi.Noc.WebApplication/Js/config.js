$(document).ready(function () {

});

function confirmLinkClick(event, confirmationText, url)
{
    if (confirm(confirmationText))
    {  
        window.location.href = url;
        return false;
    } 
    else
    { 
        if (event.stopPropagation) 
        {
            //event.cancelBubble = true;
            event.stopPropagation(); 
        } 
        else if (window.event) 
        { 
            window.event.cancelBubble = true; 
        } 
        return false;
    }
}

function textMessage(event, confirmationText) {
    alert(confirmationText);
    if (event.stopPropagation) {
        event.stopPropagation();
    }
    else if (window.event) {
        window.event.cancelBubble = true;
    }
    return false;
}