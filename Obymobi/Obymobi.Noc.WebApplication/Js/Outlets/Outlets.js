﻿function getTracker() {
    if ("ga" in window) {
        tracker = ga.getAll()[0];

        return tracker;
    }

    return undefined;
}

$(function () {
    // Clear DevExpress onclick behavior.
    $("#outlet-list").find("input[type='button']").each(function (index, element) {
        $(element).removeAttr('onclick');
    });

    $("#outlet-list").on("click", "input[type='button']", function () {
        var confirmed;
        if ($(this)[0].defaultValue === 'Switch on') {
            confirmed = confirm("Are you sure you want to switch on ordering for this outlet?");
        } else {
            confirmed = confirm("Are you sure you want to switch off ordering for this outlet?");
        }
        if (!confirmed) {
            return;
        }
  

        var button = $(this);
        var id = $(this).closest("li").data("id");
        var outletName = $(this).closest("li").first("li").text().trim();

        if (id === undefined || id <= 0) {
            console.error("Failed to fetch the outlet identity.");
        }

        $.ajax({
            url: 'Handlers/PatchOutletState.ashx',
            method: "PATCH",
            data: {
                id: id
            },
            success: function (newState) {
                var tracker = getTracker();

                if (newState == 'Switch off') {
                    if (tracker) {
                        tracker.send('event', 'Kill Switches', 'Ordering Enabled', outletName);
                    }

                    $(button).attr('value', 'Switch off');
                    $(button).removeClass('hidden');
                } else {
                    if (tracker) {
                        tracker.send('event', 'Kill Switches', 'Ordering Disabled', outletName);
                    }

                    $(button).attr('value', 'Switch on');
                    $(button).addClass('hidden');
                }
            },
            error: function (xhr, status, response) {
                console.error(status, response);
            }
        });
    });
});
