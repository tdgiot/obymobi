﻿function showPopup(id)
{
    var div = document.getElementById(id);

    div.style.display = "block";
    div.style.visibility = "visible";
}

function hidePopup(id)
{
    var div = document.getElementById(id);

    div.style.display = "none";
    div.style.visibility = "hidden";
}