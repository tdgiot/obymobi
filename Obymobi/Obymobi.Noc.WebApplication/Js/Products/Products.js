﻿function getTracker() {
    if ("ga" in window) {
        tracker = ga.getAll()[0];

        return tracker;
    }

    return undefined;
}

$(function () {
    // Clear DevExpress onclick behavior.
    $("#product-list").find("input[type='button']").each(function (index, element) {
        $(element).removeAttr('onclick');
    });

    $("#product-list").on("click", "input[type='button']", function () {
        var confirmed
        if ($(this)[0].defaultValue === 'UNHIDE') {
            confirmed = confirm("Are you sure you want to unhide this menu item?");
        } else {
            confirmed = confirm("Are you sure you want to hide this menu item?");
        }
        if (!confirmed) {
            return;
        }

        var button = $(this);
        var id = $(this).closest("li").data("id");
        var productName = $(this).closest("li").first("li").text().trim();

        if (id === undefined || id <= 0) {
            console.error("Failed to fetch the product identity.");
        }

        $.ajax({
            url: 'Handlers/PatchProductVisibility.ashx',
            method: "PATCH",
            data: {
                id: id
            },
            success: function (response) {
                var tracker = getTracker();

                $(button).attr("value", response);
                console.log(response);
                if (response == "UNHIDE") {
                    if (tracker) {
                        tracker.send('event', 'Kill Switches', 'Product Hidden', productName);
                    }

                    $(button).addClass("hidden");
                } else {
                    if (tracker) {
                        tracker.send('event', 'Kill Switches', 'Product Shown', productName);
                    }

                    $(button).removeClass("hidden");
                }
            },
            error: function (xhr, status, response) {
                console.error(status, response);
            }
        });
    });
});
