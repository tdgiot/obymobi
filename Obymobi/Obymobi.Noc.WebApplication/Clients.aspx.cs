﻿using System;
using System.Collections.Generic;
using Obymobi.Constants;
using Obymobi.Logic.HelperClasses;
using Obymobi.Data.CollectionClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Dionysos.Web;
using Obymobi.Data;
using Obymobi.Enums;

public partial class Clients : PageListview<ClientEntity>
{
    #region Methods

    private List<int> GetCurrentCompanyId()
    {
        if (QueryStringHelper.HasValue("cid"))
            return new List<int> {QueryStringHelper.GetInt("cid")};

        if (CraveMobileNocHelper.AvailableCompanyIds.Count > 0)
            return CraveMobileNocHelper.AvailableCompanyIds;        

        return null;
    }

    #region Base class methods

    protected override EntityView<ClientEntity> GetEntityViewFromSession()
    {
        return null; //Session[this.SessionKeyEntityView] as EntityView<ClientEntity>;
    }

    protected override void GetFilterAndSortFromSession()
    {
        object filter = Session[this.SessionKeyFilter];
        if (filter != null)
            this.ddlFilter.SelectedIndex = (int)filter;

        object sort = Session[this.SessionKeySort];
        if (sort != null)
            this.ddlSort.SelectedIndex = (int)sort;
    }

    protected override EntityView<ClientEntity> InitializeEntityView()
    {
        if (!IsPostBack)
            this.GetFilterAndSortFromSession();

        // Create the filter
        // Only get the clients where monitoring is enabled
        PredicateExpression filter = new PredicateExpression();
        filter.Add(CompanyFields.UseMonitoring == true);

        // Only show clients that are linked to a device OR belong to an Otouch Company           
        filter.Add(ClientFields.DeviceId != DBNull.Value | CompanyFields.SystemType == (int)SystemType.Otoucho);
        filter.Add(ClientFields.CompanyId == GetCurrentCompanyId());

        PredicateExpression extraFilter = CreateFilter();
        if (extraFilter != null)
        {
            filter.Add(extraFilter);
        }

        // Create the relations
        RelationCollection relations = new RelationCollection();
        relations.Add(ClientEntityBase.Relations.CompanyEntityUsingCompanyId);
        relations.Add(ClientEntityBase.Relations.DeviceEntityUsingDeviceId);
        relations.Add(ClientEntityBase.Relations.DeliverypointEntityUsingDeliverypointId);

        // Create the sort
        SortExpression sort = new SortExpression();
        sort.Add(new SortClause(DeviceFields.LastRequestUTC, SortOperator.Descending));

        // TODO, only certain fields? > GK: First take the bigger performance gain - Prefetch!
        PrefetchPath path = new PrefetchPath(EntityType.ClientEntity);
        path.Add(ClientEntityBase.PrefetchPathDeliverypointEntity).SubPath.Add(DeliverypointEntityBase.PrefetchPathDeliverypointgroupEntity);
        path.Add(ClientEntityBase.PrefetchPathCompanyEntity);
        path.Add(ClientEntityBase.PrefetchPathDeviceEntity);

        // Get the clients
        ClientCollection clients = new ClientCollection();
        clients.GetMulti(filter, 0, sort, relations, path);

        // Return the view
        return clients.DefaultView;
    }

    private PredicateExpression CreateFilter()
    {
        // Filter
        switch (this.ddlFilter.SelectedIndex)
        {
            case 0:
                // All clients
                return null;
            case 1:
                // Online clients
                return new PredicateExpression(DeviceFields.LastRequestUTC > DateTime.UtcNow.AddMinutes(-3));
                //return new PredicateExpression(new EntityProperty("LastActivityUTC") > DateTime.UtcNow.AddMinutes(-2));
            case 2:
                // Offline clients
                PredicateExpression offlineFilter = new PredicateExpression();
                offlineFilter.Add(DeviceFields.LastRequestUTC <= DateTime.UtcNow.AddMinutes(-3));
                offlineFilter.AddWithOr(DeviceFields.LastRequestUTC == DBNull.Value);
                return offlineFilter;
            case 3:
                // Low battery clients
                PredicateExpression lowBatteryFilter = new PredicateExpression();
                lowBatteryFilter.Add(DeviceFields.BatteryLevel != DBNull.Value);
                lowBatteryFilter.Add(DeviceFields.BatteryLevel < 20);
                return lowBatteryFilter;
            case 4:
                // Outdated clients
                return CreateOutdatePredicateExpression();
        }

        return null;
    }

    protected override void SetFilter()
    {
        // Filter
        /*switch (this.ddlFilter.SelectedIndex)
        {
            case 0:
                // All clients
                this.EntityView.Filter = null;
                break;
            case 1:
                // Online clients
                //this.EntityView.Filter = new PredicateExpression(DeviceFields.LastRequestUTC > DateTime.UtcNow.AddMinutes(-2));
                this.EntityView.Filter = new PredicateExpression(new EntityProperty("LastActivityUTC") > DateTime.UtcNow.AddMinutes(-2));
                break;
            case 2:
                // Offline clients
                PredicateExpression offlineFilter = new PredicateExpression();
                offlineFilter.Add(new EntityProperty("LastActivityUTC") <= DateTime.UtcNow.AddMinutes(-2));
                offlineFilter.AddWithOr(DeviceFields.LastRequestUTC == DBNull.Value);
                this.EntityView.Filter = offlineFilter;
                break;
            case 3:
                // Low battery clients
                PredicateExpression lowBatteryFilter = new PredicateExpression();
                lowBatteryFilter.Add(ClientFields.LastBatteryLevel != DBNull.Value);
                lowBatteryFilter.Add(ClientFields.LastBatteryLevel < 20);
                this.EntityView.Filter = lowBatteryFilter;
                break;
            case 4:
                // Outdated clients
                var outdatedFilter = CreateOutdatePredicateExpression();
                this.EntityView.Filter = outdatedFilter;
                break;
        }*/

        // Store the current filter in the session
        Session[this.SessionKeyFilter] = this.ddlFilter.SelectedIndex;
    }

    private PredicateExpression CreateOutdatePredicateExpression()
    {
        var companyIds = GetCurrentCompanyId();
        if (companyIds == null)
            return null;

        var outdatedFilter = new PredicateExpression();
        foreach (var companyId in companyIds)
        {
            var companyFilter = new PredicateExpression();
            companyFilter.Add(ClientFields.CompanyId == companyId);

            var versionFilter = new PredicateExpression();
            versionFilter.Add(DeviceFields.ApplicationVersion != ReleaseHelper.GetVersionForCompany(ApplicationCode.Emenu, companyId));
            versionFilter.AddWithOr(DeviceFields.AgentVersion != ReleaseHelper.GetVersionForCompany(ApplicationCode.Agent, companyId));
            versionFilter.AddWithOr(DeviceFields.SupportToolsVersion != ReleaseHelper.GetVersionForCompany(ApplicationCode.SupportTools, companyId));

            companyFilter.Add(versionFilter);
            outdatedFilter.AddWithOr(companyFilter);
        }

        return outdatedFilter;
    }

    protected override void SetSort()
    {
        // Sort
        switch (this.ddlSort.SelectedIndex)
        {
            case 0:
                // Last request, descending
                this.EntityView.Sorter = new SortExpression(new SortClause(DeviceFields.LastRequestUTC, SortOperator.Descending));
                break;
            case 1:
                // Status, ascending
                this.EntityView.Sorter = new SortExpression(new SortClause(ClientFields.LastStatus, SortOperator.Ascending));
                break;
            case 2:
                // Version, descending
                this.EntityView.Sorter = new SortExpression(new SortClause(DeviceFields.ApplicationVersion, SortOperator.Descending));
                break;
            case 3:
                // IP-address, ascending
                this.EntityView.Sorter = new SortExpression(new SortClause(DeviceFields.PrivateIpAddresses, SortOperator.Ascending));
                break;
            case 4:
                // Battery level, ascending
                this.EntityView.Sorter = new SortExpression(new SortClause(DeviceFields.BatteryLevel, SortOperator.Ascending));
                break;
        }

        // Store the current sort in the session
        Session[this.SessionKeySort] = this.ddlSort.SelectedIndex;
    }

    protected override void RenderList()
    {
        this.lblTitle.Text = this.Count == 1 ? Resources.strings.One_item : string.Format(Resources.strings.X_items, this.Count);

        Dictionary<int, string> companyVersions = new Dictionary<int, string>();
        foreach (var client in this.EntityView)
        {
            string emenuReleaseVersion;
            if (!companyVersions.TryGetValue(client.CompanyId, out emenuReleaseVersion))
            {
                emenuReleaseVersion = ReleaseHelper.GetVersionForCompany(ApplicationCode.Emenu, client.CompanyId);
                companyVersions.Add(client.CompanyId, emenuReleaseVersion);
            }

            UserControls_Listitems_Client clientControl = this.LoadControl<UserControls_Listitems_Client>("~/UserControls/Listitems/Client.ascx");
            clientControl.Client = client;
            clientControl.EmenuReleaseVersion = emenuReleaseVersion;
            this.plhList.Controls.Add(clientControl);
        }
    }

    #endregion

    #endregion

    #region Properties

    public override string SessionKey
    {
        get { return "Clients"; }
    }

    #endregion

    #region Event handlers

    protected void Page_Load(object sender, EventArgs e)
    {
        this.Title = string.Format("{0} - {1}", Resources.strings.Clients, Resources.strings.Crave_NOC);
        this.Initialize();
    }

    #endregion
}