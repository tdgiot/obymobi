﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/CraveMobileNocBase.master" AutoEventWireup="true" Inherits="Company" Codebehind="Company.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<ul class="details">
	<li>
		<D:Image ID="imgCompany" runat="server" />
		<div class="title"><D:LabelTextOnly ID="lblTitle" runat="server"></D:LabelTextOnly></div>
	</li>
	<li>
		<div class="label"><D:LabelTextOnly ID="lblLastTerminalActivityLabel" runat="server" Text="<%$ Resources:strings, Company_last_terminal_activity %>"></D:LabelTextOnly>:</div>
		<div class="value"><D:LabelTextOnly ID="lblLastTerminalActivity" runat="server"></D:LabelTextOnly></div>
	</li>
	<li>
		<div class="label"><D:LabelTextOnly ID="lblLastClientActivityLabel" runat="server" Text="<%$ Resources:strings, Company_last_client_activity %>"></D:LabelTextOnly>:</div>
		<div class="value"><D:LabelTextOnly ID="lblLastClientActivity" runat="server"></D:LabelTextOnly></div>
	</li>
	<li>
		<div class="label"><D:LabelTextOnly ID="lblLastOrderPlacedLabel" runat="server" Text="<%$ Resources:strings, Company_last_order_placed %>"></D:LabelTextOnly>:</div>
		<div class="value"><D:LabelTextOnly ID="lblLastOrderPlaced" runat="server"></D:LabelTextOnly></div>
	</li>
	<li>
		<div class="label"><D:LabelTextOnly ID="lblTelephoneLabel" runat="server" Text="<%$ Resources:strings, Company_telephone %>"></D:LabelTextOnly>:</div>
		<div class="value"><D:HyperLink ID="hlTelephone" runat="server"></D:HyperLink></div>
	</li>
	<li>
		<div class="label"><D:LabelTextOnly ID="lblStatusLabel" runat="server" Text="<%$ Resources:strings, Company_status %>"></D:LabelTextOnly>:</div>
		<div class="value"><D:PlaceHolder ID="plhStatus" runat="server"></D:PlaceHolder></div>
	</li>
	<li class="spacer"></li>
	<li>
		<div class="subtitle"><D:LabelTextOnly runat="server" Text="<%$ Resources:strings, Terminals %>"></D:LabelTextOnly>:</div>
	</li>
	<li>
		<div class="label"><D:LabelTextOnly ID="lblTerminalsCountLabel" runat="server" Text="<%$ Resources:strings, Company_terminals_count %>"></D:LabelTextOnly>:</div>
		<div class="value"><D:LabelTextOnly ID="lblTerminalsCount" runat="server"></D:LabelTextOnly></div>
	</li>
	<li>
		<div class="label"><D:LabelTextOnly ID="lblTerminalsOnlineCountLabel" runat="server" Text="<%$ Resources:strings, Company_terminals_online_count %>"></D:LabelTextOnly>:</div>
		<div class="value"><D:LabelTextOnly ID="lblTerminalsOnlineCount" runat="server"></D:LabelTextOnly></div>
	</li>
	<li>
		<div class="label"><D:LabelTextOnly ID="lblTerminalsOfflineCountLabel" runat="server" Text="<%$ Resources:strings, Company_terminals_offline_count %>"></D:LabelTextOnly>:</div>
		<div class="value"><D:LabelTextOnly ID="lblTerminalsOfflineCount" runat="server"></D:LabelTextOnly></div>
	</li>
	<li>
		<div class="label"><D:LabelTextOnly ID="lblTerminalsOfflineBatteryCountLabel" runat="server" Text="<%$ Resources:strings, Company_terminals_offline_battery_count %>"></D:LabelTextOnly>:</div>
		<div class="value"><D:LabelTextOnly ID="lblTerminalsOfflineBatteryCount" runat="server"></D:LabelTextOnly></div>
	</li>
	<li class="spacer"></li>
	<li>
		<div class="subtitle"><D:LabelTextOnly runat="server" Text="<%$ Resources:strings, Clients %>"></D:LabelTextOnly>:</div>
	</li>
	<li>
		<div class="label"><D:LabelTextOnly ID="lblClientsCountLabel" runat="server" Text="<%$ Resources:strings, Company_clients_count %>"></D:LabelTextOnly>:</div>
		<div class="value"><D:LabelTextOnly ID="lblClientsCount" runat="server"></D:LabelTextOnly></div>
	</li>
	<li>
		<div class="label"><D:LabelTextOnly ID="lblClientsOnlineCountLabel" runat="server" Text="<%$ Resources:strings, Company_clients_online_count %>"></D:LabelTextOnly>:</div>
		<div class="value"><D:LabelTextOnly ID="lblClientsOnlineCount" runat="server"></D:LabelTextOnly></div>
	</li>
	<li>
		<div class="label"><D:LabelTextOnly ID="lblClientsOfflineCountLabel" runat="server" Text="<%$ Resources:strings, Company_clients_offline_count %>"></D:LabelTextOnly>:</div>
		<div class="value"><D:LabelTextOnly ID="lblClientsOfflineCount" runat="server"></D:LabelTextOnly></div>
	</li>
	<li>
		<div class="label"><D:LabelTextOnly ID="lblClientsOfflineBatteryCountLabel" runat="server" Text="<%$ Resources:strings, Company_clients_offline_battery_count %>"></D:LabelTextOnly>:</div>
		<div class="value"><D:LabelTextOnly ID="lblClientsOfflineBatteryCount" runat="server"></D:LabelTextOnly></div>
	</li>
	<li>
		<div class="label"><D:LabelTextOnly ID="lblApiV1CountLabel" runat="server" Text="<%$ Resources:strings, Company_Api_v1_count %>"></D:LabelTextOnly>:</div>
		<div class="value"><D:LabelTextOnly ID="lblApiV1Count" runat="server"></D:LabelTextOnly></div>
	</li>
	<li>
		<div class="label"><D:LabelTextOnly ID="lblApiV2CountLabel" runat="server" Text="<%$ Resources:strings, Company_Api_v2_count %>"></D:LabelTextOnly>:</div>
		<div class="value"><D:LabelTextOnly ID="lblApiV2Count" runat="server"></D:LabelTextOnly></div>
	</li>
	<li class="spacer"></li>
</ul>

<ul class="buttonlist">
	<li><D:HyperLink ID="hlProducts" runat="server" Text="<%$ Resources:strings, Company_view_products %>"></D:HyperLink></li>
	<li><D:HyperLink ID="hlOutlets" runat="server" Text="<%$ Resources:strings, Company_view_outlets %>"></D:HyperLink></li>
    <li><D:HyperLink ID="hlOrders" runat="server" Text="<%$ Resources:strings, Company_view_orders %>"></D:HyperLink></li>
	<li><D:HyperLink ID="hlOrderTimeAppless" runat="server" Text="<%$ Resources:strings, Appless_Delivery_Order_Time %>"></D:HyperLink></li>
    <li><D:HyperLink ID="hlOrderTime" runat="server" Text="<%$ Resources:strings, Delivery_Order_Time %>"></D:HyperLink></li>
	<li><D:HyperLink ID="hlCallCustomer" runat="server" Text="<%$ Resources:strings, Company_call_customer %>"></D:HyperLink></li>
	<li><D:HyperLink ID="hlTerminals" runat="server" Text="<%$ Resources:strings, Company_view_terminals %>"></D:HyperLink></li>
	<li><D:HyperLink ID="hlClients" runat="server" Text="<%$ Resources:strings, Company_view_clients %>"></D:HyperLink></li>
	<li><D:LinkButton ID="btOfflineClientsReport" runat="server" Text="<%$ Resources:strings, Company_offline_client_report %>"></D:LinkButton></li>    
	<li><D:HyperLink ID="hlDeliverypoints" runat="server" Text="<%$ Resources:strings, Company_view_deliverypoints %>"></D:HyperLink></li>
	<li><D:HyperLink ID="hlDeliverypointgroups" runat="server" Text="<%$ Resources:strings, Company_view_deliverypointgroups %>"></D:HyperLink></li>
	<li><D:HyperLink ID="hlLatestActivity" runat="server" Text="<%$ Resources:strings, Company_view_latest_activity %>"></D:HyperLink></li>
	<li><D:HyperLink ID="hlVersions" runat="server" Text="<%$ Resources:strings, Company_view_versions %>"></D:HyperLink></li>
	<li><D:HyperLink ID="hlDisconnectedFromComet" runat="server" Text="<%$ Resources:strings, Company_view_units_disconnected_from_comet %>"></D:HyperLink></li>
	<li><D:HyperLink ID="hlOpenInCms" runat="server" Target="_blank" Text="<%$ Resources:strings, Generic_open_in_cms %>"></D:HyperLink></li>
</ul>

</asp:Content>

