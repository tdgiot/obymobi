﻿using System;
using System.Collections.Generic;
using System.Linq;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Noc.Logic.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

public partial class CompanySelection : PageListview<CompanyEntity>
{
    #region Methods

    #region Base class methods

    protected override EntityView<CompanyEntity> GetEntityViewFromSession()
    {
        return Session[this.SessionKeyEntityView] as EntityView<CompanyEntity>;
    }

    protected override void GetFilterAndSortFromSession()
    {
    }

    protected override EntityView<CompanyEntity> InitializeEntityView()
    {
        // Get the companies
        CompanyCollection companyCollection = NocHelper.GetCompanies(CraveMobileNocHelper.AvailableCompanyIds);

        // Return the view
        return companyCollection.DefaultView;
    }

    protected override void SetFilter()
    {
    }

    protected override void SetSort()
    {
        this.EntityView.Sorter = new SortExpression(new SortClause(CompanyFields.Name, SortOperator.Ascending));
    }

    protected override void RenderList()
    {
        foreach (var company in this.EntityView) 
        {
           UserControls_Listitems_CompanySimplified companyControl = this.LoadControl<UserControls_Listitems_CompanySimplified>("~/UserControls/Listitems/CompanySimplified.ascx");
           companyControl.Company = company;
           plhListCompanies.Controls.Add(companyControl);
        }
    }

    #endregion

    #endregion

    #region Properties

    public override string SessionKey
    {
        get { return "Companies"; }
    }

    #endregion

    #region Event handlers

    protected void Page_Load(object sender, EventArgs e)
    {
        this.Title = string.Format("{0} - {1}", Resources.strings.Companies, Resources.strings.Crave_NOC);
        this.Initialize();
    }

    #endregion
}