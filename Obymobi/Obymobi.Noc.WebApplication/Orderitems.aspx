﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/CraveMobileNocBase.master" AutoEventWireup="true" Inherits="Orderitems" Codebehind="Orderitems.aspx.cs" %>
<%@ Reference VirtualPath="~/UserControls/Listitems/Orderitem.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<div id="settings">
    <div id="filter">
        <D:DropDownList ID="ddlFilter" runat="server" AutoPostBack="true">
            <asp:ListItem Text="<%$ Resources:strings, Orderitem_all_items %>"></asp:ListItem>
            <asp:ListItem Text="<%$ Resources:strings, Orderitem_processed_items %>"></asp:ListItem>
            <asp:ListItem Text="<%$ Resources:strings, Orderitem_unprocessed_items %>"></asp:ListItem>
        </D:DropDownList>
    </div>
    <div id="title"><D:LabelTextOnly ID="lblTitle" runat="server"></D:LabelTextOnly></div>
    <div id="sort">
        <D:DropDownList ID="ddlSort" runat="server" AutoPostBack="true">
            <asp:ListItem Text="<%$ Resources:strings, Orderitem_number_asc %>"></asp:ListItem>
            <asp:ListItem Text="<%$ Resources:strings, Orderitem_product_name_asc %>"></asp:ListItem>
        </D:DropDownList>
    </div>
</div>
<ul class="list">
    <D:PlaceHolder ID="plhList" runat="server"></D:PlaceHolder>
</ul>    
</asp:Content>

