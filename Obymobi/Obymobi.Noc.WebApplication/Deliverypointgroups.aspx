﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/CraveMobileNocBase.master" AutoEventWireup="true" Inherits="Deliverypointgroups" Codebehind="Deliverypointgroups.aspx.cs" %>
<%@ Reference VirtualPath="~/UserControls/Listitems/Deliverypointgroup.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<div id="settings" class="category-group">
    <div id="filter">
        <D:DropDownList ID="ddlFilter" runat="server" AutoPostBack="true">
            <asp:ListItem Text="<%$ Resources:strings, Deliverypointgroup_all_deliverypointgroups %>"></asp:ListItem>
        </D:DropDownList>
    </div>
    <div id="sort">
        <D:DropDownList ID="ddlSort" runat="server" AutoPostBack="true">
            <asp:ListItem Text="<%$ Resources:strings, Deliverypoint_name_asc %>"></asp:ListItem>
        </D:DropDownList>
    </div>
    <div id="title"><D:LabelTextOnly ID="lblTitle" runat="server"></D:LabelTextOnly></div>

</div>
<ul class="list">
    <D:PlaceHolder ID="plhList" runat="server"></D:PlaceHolder>
</ul>    
</asp:Content>

