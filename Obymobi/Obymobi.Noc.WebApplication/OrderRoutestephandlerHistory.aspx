﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/CraveMobileNocBase.master" AutoEventWireup="true" Inherits="OrderRoutestephandlerHistory" Codebehind="OrderRoutestephandlerHistory.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<ul class="details">
    <li>
        <D:Image ID="imgOrderRoutestephandlerHistory" runat="server" />
        <div class="title"><D:Label ID="lblTitle" runat="server"></D:Label></div>
    </li>
    <li>
        <div class="label"><D:LabelTextOnly ID="lblNumberLabel" runat="server" Text="<%$ Resources:strings, OrderRoutestephandler_Number %>"></D:LabelTextOnly>:</div>
        <div class="value"><D:Label ID="lblNumber" runat="server"></D:Label></div>
    </li>
    <li>
        <div class="label"><D:LabelTextOnly ID="lblGuidLabel" runat="server" Text="<%$ Resources:strings, OrderRoutestephandler_Guid %>"></D:LabelTextOnly>:</div>
        <div class="value"><D:Label ID="lblGuid" runat="server"></D:Label></div>
    </li>
    <li>
        <div class="label"><D:LabelTextOnly ID="lblOrder" runat="server" Text="<%$ Resources:strings, Order %>"></D:LabelTextOnly>:</div>
        <div class="value"><D:HyperLink ID="hlOrderId" runat="server"></D:HyperLink></div>
    </li>
    <li>
        <div class="label"><D:LabelTextOnly ID="lblTerminal" runat="server" Text="<%$ Resources:strings, Terminal %>"></D:LabelTextOnly>:</div>
        <div class="value"><D:HyperLink ID="hlTerminalId" runat="server"></D:HyperLink></div>
    </li>
    <li>
        <div class="label"><D:LabelTextOnly ID="lblHandlerTypeLabel" runat="server" Text="<%$ Resources:strings, OrderRoutestephandler_HandlerType %>"></D:LabelTextOnly>:</div>
        <div class="value"><D:Label ID="lblHandlerType" runat="server"></D:Label></div>
    </li>
    <li>
        <div class="label"><D:LabelTextOnly ID="lblStatusLabel" runat="server" Text="<%$ Resources:strings, Status %>"></D:LabelTextOnly>:</div>
        <div class="value"><D:Label ID="lblStatus" runat="server"></D:Label></div>
    </li>
    <li>
        <div class="label"><D:LabelTextOnly ID="lblErrorCodeLabel" runat="server" Text="Error code"></D:LabelTextOnly>:</div>
        <div class="value"><D:Label ID="lblErrorCode" runat="server"></D:Label></div>
    </li>
    <li>
        <div class="label"><D:LabelTextOnly ID="lblErrorTextLabel" runat="server" Text="Error text"></D:LabelTextOnly>:</div>
        <div class="value"><D:Label ID="lblErrorText" runat="server"></D:Label></div>
    </li>
    <li>
        <div class="label"><D:LabelTextOnly ID="LabelTextOnly1" runat="server" Text="<%$ Resources:strings, Order_RoutingLog %>"></D:LabelTextOnly>:</div>
        <div class="value"><D:Label ID="lblRoutingLog" runat="server"></D:Label></div>
    </li>
</ul>
</asp:Content>

