﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/CraveMobileNocBase.master" AutoEventWireup="true" Inherits="CompanySelection" Codebehind="CompanySelection.aspx.cs" %>
<%@ Reference VirtualPath="~/UserControls/Listitems/CompanySimplified.ascx" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div id="titleHeader">
        <h1>Select your venue</h1><br>
        <h2>Choose the venue you wish to manage</h2>
    </div>
    <ul class="list">
        <D:PlaceHolder ID="plhListCompanies" runat="server"></D:PlaceHolder>
    </ul>    
</asp:Content>

