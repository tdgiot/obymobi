﻿using System;
using System.Web;

namespace Obymobi.Noc.WebApplication
{
    public partial class _500 : Dionysos.Web.UI.PageDefault
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            btnBack.Attributes.Add("onClick", "javascript:history.back(); return false;");
            plhLogo.AddHtml("<a id=\"logo\" class=\"logo\" href=\"{0}\"></a>", ResolveUrl("~"));
            lblException.Text = (HttpContext.Current.Application.Get("Exception") as Exception)?.Message;
        }
    }
}