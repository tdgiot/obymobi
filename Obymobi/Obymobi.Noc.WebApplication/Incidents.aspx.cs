﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Dionysos.Web;
using Dionysos.Web.UI;
using Obymobi.Data.EntityClasses;
using Obymobi.Noc.Logic.HelperClasses;

public partial class Incidents : PageDefault
{
    private bool noResults = true;
    #region Event handlers

    protected void Page_Load(object sender, EventArgs e)
    {
        this.Title = string.Format("{0} - {1}", Resources.strings.Overview, Resources.strings.Crave_NOC);
        try
        {
            // Orders
            this.RenderNonRoutedOrders();
            this.RenderNonStartedRoutesOrders();
            // this.RenderExpiredOrders();
            this.RenderExpiredRetrievedOrders();
            this.RenderUnprocessableOrders();

            // Client
            // this.RenderNonOrderingClients();
            this.RenderCompaniesWithTooMuchOfflineClients();

            // Terminals
            // this.RenderOfflineTerminals();

            // Outdated terminals/clients
            // this.RenderOutdatedTerminals();

            this.plhNoResults.Visible = noResults;
        }
        catch
        {
            if (CraveMobileNocHelper.IsAdministrator)
            {
                this.Response.Redirect("Navigation.aspx?databaseError=true");
            }
        }
    }

    #endregion

    #region Methods

    private void RenderNonRoutedOrders()
    {
        var nonRoutedOrders = NocOrderHelper.GetNonRoutedOrders(CraveMobileNocHelper.AvailableCompanyIds);
        if (nonRoutedOrders.Count > 0)
        {
            this.noResults = false;
            this.plhNonRoutedOrders.Visible = true;
            this.lblNonRoutedOrderCount.Text = nonRoutedOrders.Count.ToString();

            foreach (OrderEntity order in nonRoutedOrders)
            {
                UserControls_Listitems_Order orderControl = this.LoadControl<UserControls_Listitems_Order>("~/UserControls/Listitems/Order.ascx");
                orderControl.Order = order;
                this.plhNonRoutedOrdersList.Controls.Add(orderControl);
            }
        }
    }

    private void RenderNonStartedRoutesOrders()
    {
        var nonStartedRoutesOrders = NocOrderHelper.GetNotStartedRoutedOrders(CraveMobileNocHelper.AvailableCompanyIds);
        if (nonStartedRoutesOrders.Count > 0)
        {
            this.noResults = false;

            this.plhNonStartedRoutesOrders.Visible = true;
            this.lblNonStartedRoutesOrderCount.Text = nonStartedRoutesOrders.Count.ToString();

            foreach (OrderEntity order in nonStartedRoutesOrders)
            {
                UserControls_Listitems_Order orderControl = this.LoadControl<UserControls_Listitems_Order>("~/UserControls/Listitems/Order.ascx");
                orderControl.Order = order;
                this.plhNonStartedRoutesOrdersList.Controls.Add(orderControl);
            }
        }
    }

    private void RenderExpiredRetrievedOrders()
    {
        var expiredOrders = NocOrderHelper.GetRetrievedOrders(CraveMobileNocHelper.AvailableCompanyIds);
        if (expiredOrders.Count > 0)
        {
            this.noResults = false;

            this.plhExpiredRetrievedOrders.Visible = true;
            this.lblExpiredRetrievedOrdersCount.Text = expiredOrders.Count.ToString();

            foreach (OrderEntity order in expiredOrders)
            {
                UserControls_Listitems_Order orderControl = this.LoadControl<UserControls_Listitems_Order>("~/UserControls/Listitems/Order.ascx");
                orderControl.Order = order;
                this.plhExpiredRetrievedOrdersList.Controls.Add(orderControl);
            }
        }
    }

    private void RenderExpiredOrders()
    {
        var expiredOrders = NocOrderHelper.GetExpiredOrders(CraveMobileNocHelper.AvailableCompanyIds);
        if (expiredOrders.Count > 0)
        {
            this.noResults = false;
            this.plhExpiredOrders.Visible = true;
            this.lblExpiredOrderCount.Text = expiredOrders.Count.ToString();

            foreach (OrderEntity order in expiredOrders)
            {
                UserControls_Listitems_Order orderControl = this.LoadControl<UserControls_Listitems_Order>("~/UserControls/Listitems/Order.ascx");
                orderControl.Order = order;
                this.plhExpiredOrdersList.Controls.Add(orderControl);
            }
        }
    }

    private void RenderUnprocessableOrders()
    {
        const int numberOfItemsToDisplay = 25;
        var unprocessableOrders = NocOrderHelper.GetUnprocessableOrders(CraveMobileNocHelper.AvailableCompanyIds, numberOfItemsToDisplay);

        if (unprocessableOrders.Count > 0)
        {
            this.noResults = false;
            this.plhUnprocessableOrders.Visible = true;
            if (unprocessableOrders.Count <= numberOfItemsToDisplay)
            {
                this.lblUnprocessableOrderCount.Text = unprocessableOrders.Count.ToString();
            }
            else
            {
                this.lblUnprocessableOrderCount.Text = $"${numberOfItemsToDisplay}+";
            }


            foreach (OrderEntity order in unprocessableOrders)
            {
                UserControls_Listitems_Order orderControl = this.LoadControl<UserControls_Listitems_Order>("~/UserControls/Listitems/Order.ascx");
                orderControl.Order = order;
                this.plhUnprocessableOrdersList.Controls.Add(orderControl);
            }
        }
    }

    private void RenderNonOrderingClients()
    {
        var nonOrderingClientsSummaries = NocCompanyHelper.GetNonOrderingClientsGroupedByOperationMode(CraveMobileNocHelper.AvailableCompanyIds);
        if (nonOrderingClientsSummaries.Rows.Count > 0)
        {
            this.noResults = false;
            this.plhNonOrderingClientsSummaries.Visible = true;

            DataView dv = nonOrderingClientsSummaries.DefaultView;

            // Get the distinct companyIds
            DataTable companyIds = dv.ToTable(true, "CompanyId");

            this.lblNonOrderingClientsSummaryCount.Text = companyIds.Rows.Count.ToString();

            foreach (DataRow row in companyIds.Rows)
            {
                int companyId = Int32.Parse(row[0].ToString());

                // Filter on companyId
                dv.RowFilter = string.Format("CompanyId={0}", companyId);

                DataTable dt = dv.ToTable();

                if (dt.Rows.Count > 0)
                {
                    string companyName = dt.Rows[0][1].ToString();

                    // Create summary
                    ClientsSummaryOperationModes summary = new ClientsSummaryOperationModes(companyId, companyName, dt);

                    UserControls_Listitems_ClientsSummary clientsSummaryControl = this.LoadControl<UserControls_Listitems_ClientsSummary>("~/UserControls/Listitems/ClientsSummary.ascx");
                    clientsSummaryControl.Summary = summary;
                    this.plhNonOrderingClientsSummaryList.Controls.Add(clientsSummaryControl);
                }
            }
        }
    }

    private void RenderCompaniesWithTooMuchOfflineClients()
    {
        List<Tuple<int, string, int>> companiesWithTooMuchOfflineClients = NocCompanyHelper.GetCompaniesWithTooMuchOfflineClients(CraveMobileNocHelper.AvailableCompanyIds);
        if (companiesWithTooMuchOfflineClients.Count > 0)
        {
            this.noResults = false;
            this.plhCompaniesWithTooMuchOfflineClients.Visible = true;
            this.lblCompaniesWithTooMuchOfflineClientsCount.Text = companiesWithTooMuchOfflineClients.Count.ToString();

            foreach (Tuple<int, string, int> item in companiesWithTooMuchOfflineClients)
            {
                int companyId = item.Item1;
                string companyName = item.Item2;
                int percentage = item.Item3;

                // Create summary
                ClientsSummaryPercentage summary = new ClientsSummaryPercentage(companyId, companyName, percentage);

                UserControls_Listitems_ClientsSummary clientsSummaryControl = this.LoadControl<UserControls_Listitems_ClientsSummary>("~/UserControls/Listitems/ClientsSummary.ascx");
                clientsSummaryControl.Summary = summary;
                this.plhCompaniesWithTooMuchOfflineClientsList.Controls.Add(clientsSummaryControl);
            }
        }
    }

    private void RenderOfflineTerminals()
    {
        var offlineTerminalsWithOnlineClients = NocCompanyHelper.GetOfflineTerminals2(CraveMobileNocHelper.AvailableCompanyIds);
        if (offlineTerminalsWithOnlineClients.Count > 0)
        {
            this.noResults = false;
            this.plhOfflineTerminals.Visible = true;
            this.lblOfflineTerminalCount.Text = offlineTerminalsWithOnlineClients.Count.ToString();

            foreach (TerminalEntity terminal in offlineTerminalsWithOnlineClients)
            {
                UserControls_Listitems_Terminal terminalControl = this.LoadControl<UserControls_Listitems_Terminal>("~/UserControls/Listitems/Terminal.ascx");
                terminalControl.Terminal = terminal;
                this.plhOfflineTerminalsList.Controls.Add(terminalControl);
            }
        }
    }

    private void RenderOutdatedTerminals()
    {
        var outdatedTerminals = NocReleaseHelper.GetOutdatedTerminals(CraveMobileNocHelper.AvailableCompanyIds);
        if (outdatedTerminals.Count > 0)
        {
            this.noResults = false;
            this.plhOutdatedTerminals.Visible = true;
            this.lblOutdatedTerminalCount.Text = outdatedTerminals.Count.ToString();

            foreach (TerminalEntity terminal in outdatedTerminals)
            {
                UserControls_Listitems_Terminal terminalControl = this.LoadControl<UserControls_Listitems_Terminal>("~/UserControls/Listitems/Terminal.ascx");
                terminalControl.Terminal = terminal;
                this.plhOutdatedTerminalsList.Controls.Add(terminalControl);
            }
        }
    }

    #endregion
}