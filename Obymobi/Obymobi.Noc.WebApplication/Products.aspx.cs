﻿using System;
using System.Collections.Generic;
using Amazon.SimpleWorkflow.Model;
using Dionysos.Web;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using Obymobi.Noc.Logic.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

public partial class Products : PageListview<ProductEntity>
{
    #region Fields

    private const int DEFAULT_PAGE_SIZE = 250;
    private const string COMPANY_IDENTITIES_QUERY_STRING = "cid";
    #endregion

    #region Properties

    public override string SessionKey => nameof(Products);

    private string SessionKeyProductCount => $"{this.SessionKey}-ProductCount";

    public int ProductCount;

    #endregion

    #region Event handlers

    protected void Page_Load(object sender, EventArgs e)
    {
        this.Title = $"{Resources.strings.Products} - {Resources.strings.Crave_NOC}";

        this.btnSearch.Click += BtnSearch_Click;

        this.Initialize();
    }

    private void BtnSearch_Click(object sender, EventArgs e)
    {
        string searchRequest = this.tbSearch.Text;
        if (string.IsNullOrWhiteSpace(searchRequest))
        {
            return;
        }

        int.TryParse(ddlFilter.Value.ToString(), out int visibilityTypeIdentifier);
        VisibilityType? visibilityType = GetVisibilityTypeByIdentifier(visibilityTypeIdentifier);
        IReadOnlyCollection<int> companyIdentities = this.GetCurrentCompanyIdentities();

        ProductCollection productCollection = NocHelper.GetLinkedProducts(companyIdentities, DEFAULT_PAGE_SIZE, searchRequest, visibilityType);

        this.plhList.Controls.Clear();

        this.EntityView = productCollection.CreateView() as EntityView<ProductEntity>;
        this.RenderList();
    }

    #endregion

    #region Methods

    protected override EntityView<ProductEntity> GetEntityViewFromSession()
    {
        return this.Session[this.SessionKeyEntityView] as EntityView<ProductEntity>;
    }

    protected override void GetFilterAndSortFromSession()
    {
        object filter = this.Session[this.SessionKeyFilter];
        if (filter != null)
        {
            this.ddlFilter.SelectedIndex = (int)filter;
        }
    }

    protected override EntityView<ProductEntity> InitializeEntityView()
    {
        IReadOnlyCollection<int> companyIdentities = this.GetCurrentCompanyIdentities();

        ProductCollection productCollection = NocHelper.GetLinkedProducts(companyIdentities, DEFAULT_PAGE_SIZE);

        return productCollection.DefaultView;
    }

    protected override void RenderList()
    {
        foreach (ProductEntity productEntity in this.EntityView)
        {
            if (!IsOrderableProduct(productEntity)) continue;

            UserControls_Listitems_Product productControl = this.LoadControl<UserControls_Listitems_Product>("~/UserControls/ListItems/Product.ascx");
            productControl.ProductEntity = productEntity;
            this.plhList.Controls.Add(productControl);
        }
        this.ProductCount = this.plhList.Controls.Count;
    }

    private bool IsOrderableProduct(ProductEntity productEntity)
    {
        if (productEntity.SubTypeAsEnum == ProductSubType.Normal ||
            productEntity.SubTypeAsEnum == ProductSubType.ServiceItems)
        {
            return true;
        }

        if (productEntity.SubTypeAsEnum == ProductSubType.Inherit)
        {
            foreach (ProductCategoryEntity productCategoryEntity in productEntity.ProductCategoryCollection)
            {
                CategoryType categoryType = GetInheritedCategoryType(productCategoryEntity.CategoryEntity);
                if (categoryType == CategoryType.Directory)
                {
                    return false;
                }
            }

            return true;
        }

        return false;
    }

    private CategoryType GetInheritedCategoryType(CategoryEntity categoryEntity)
    {
        if (categoryEntity.TypeAsEnum == CategoryType.Inherit)
        {
            if (categoryEntity.ParentCategoryId.HasValue)
            {
                return GetInheritedCategoryType(categoryEntity.ParentCategoryEntity);
            }
        }

        return categoryEntity.TypeAsEnum;
    }

    protected override void SetFilter()
    {
        switch (this.ddlFilter.SelectedIndex)
        {
            case 1:
                this.EntityView.Filter = new PredicateExpression(ProductFields.VisibilityType == VisibilityType.Always);
                break;
            case 2:
                this.EntityView.Filter = new PredicateExpression(ProductFields.VisibilityType == VisibilityType.Never);
                break;
            case 0:
            default:
                this.EntityView.Filter = null;
                break;
        }

        this.Session[this.SessionKeyFilter] = this.ddlFilter.SelectedIndex;
    }

    protected override void SetSort()
    {
        this.EntityView.Sorter = new SortExpression(new SortClause(ProductFields.Name, SortOperator.Ascending));
    }

    private IReadOnlyCollection<int> GetCurrentCompanyIdentities()
    {
        if (QueryStringHelper.HasValue(COMPANY_IDENTITIES_QUERY_STRING))
            return new List<int> { QueryStringHelper.GetInt(COMPANY_IDENTITIES_QUERY_STRING) };

        if (CraveMobileNocHelper.AvailableCompanyIds.Count > 0)
            return CraveMobileNocHelper.AvailableCompanyIds;

        return CraveMobileNocHelper.EmptyIntList;
    }

    private VisibilityType? GetVisibilityTypeByIdentifier(int identifier)
    {
        switch (identifier)
        {
            case 1:
                return VisibilityType.Always;
            case 2:
                return VisibilityType.Never;
            default:
                return null;
        }
    }

    #endregion
}
