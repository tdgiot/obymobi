﻿using System;
using Dionysos.Web;

public partial class Navigation : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        this.Title = string.Format("{0} - {1}", Resources.strings.Navigation, Resources.strings.Crave_NOC);

        if (CraveMobileNocHelper.AvailableCompanyIds.Count == 1)
            this.AddLink(string.Format("Company.aspx?id={0}", CraveMobileNocHelper.AvailableCompanyIds[0]), Resources.strings.Company_details);
        else
            this.AddLink("Companies.aspx", Resources.strings.Companies); 

        // Requested for change by mail, agreed by HR and CB 20130625
        // this.AddLink("Terminals.aspx", Resources.strings.Terminals);
        // this.AddLink("Clients.aspx", Resources.strings.Clients);
        // this.AddLink("LatestActivity.aspx", Resources.strings.Latest_activity);

        // Requested to be added (again) - Pivotal ID: 87003652 (20150127)
        this.AddLink("Orders.aspx", Resources.strings.Orders);
        
        this.AddLink("Statistics.aspx", Resources.strings.Statistics);

        if (CraveMobileNocHelper.IsAdministrator)
        {
            this.AddLink("SystemState.aspx", Resources.strings.System_state);
            this.AddLink("Tools.aspx", "Tools");
        }
        
        this.AddLink("SignOn.aspx?SignOut=True", Resources.strings.SignOut);
        
        this.plhDatabaseIssues.Visible = QueryStringHelper.HasValue("databaseError");
    }

    private void AddLink(string url, string name)
    {
        this.plhList.AddHtml("<li><a href=\"{0}\"><div class=\"navigation\">{1}</div></a></li>", ResolveUrl(string.Format("~/{0}", url)), name);
    }
}