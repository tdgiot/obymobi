﻿using System;
using System.Collections.Generic;
using Dionysos.Web;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Noc.Logic.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

public partial class Deliverypointgroups : PageListview<DeliverypointgroupEntity>
{
    #region Methods

    private List<int> GetCurrentCompanyId()
    {
        if (QueryStringHelper.HasValue("cid"))
            return new List<int> { QueryStringHelper.GetInt("cid") };

        if (CraveMobileNocHelper.AvailableCompanyIds.Count > 0)
            return CraveMobileNocHelper.AvailableCompanyIds;

        return CraveMobileNocHelper.EmptyIntList;
    }

    #region Base class methods

    protected override EntityView<DeliverypointgroupEntity> GetEntityViewFromSession()
    {
        return Session[this.SessionKeyEntityView] as EntityView<DeliverypointgroupEntity>;
    }

    protected override void GetFilterAndSortFromSession()
    {
        object filter = Session[this.SessionKeyFilter];
        if (filter != null)
            this.ddlFilter.SelectedIndex = (int)filter;

        object sort = Session[this.SessionKeySort];
        if (sort != null)
            this.ddlSort.SelectedIndex = (int)sort;
    }

    protected override EntityView<DeliverypointgroupEntity> InitializeEntityView()
    {
        // Get the deliverypoint groups
        DeliverypointgroupCollection deliverypointgroupCollection = NocHelper.GetDeliverypointgroups(GetCurrentCompanyId());

        // Return the view
        return deliverypointgroupCollection.DefaultView;
    }

    protected override void SetFilter()
    {
        // Filter
        switch (this.ddlFilter.SelectedIndex)
        {
            case 0:
                // All deliverypointgroups
                this.EntityView.Filter = null;
                break;
        }

        // Store the current filter in the session
        Session[this.SessionKeyFilter] = this.ddlFilter.SelectedIndex;
    }

    protected override void SetSort()
    {
        // Sort
        switch (this.ddlSort.SelectedIndex)
        {
            case 0:
                // Name, Ascending
                this.EntityView.Sorter = new SortExpression(new SortClause(DeliverypointgroupFields.Name, SortOperator.Ascending));
                break;
        }

        // Store the current sort in the session
        Session[this.SessionKeySort] = this.ddlSort.SelectedIndex;
    }

    protected override void RenderList()
    {
        if (this.Count == 1)
            this.lblTitle.Text = Resources.strings.One_item;
        else
            this.lblTitle.Text = string.Format(Resources.strings.X_items, this.Count);

        foreach (var deliverypointgroup in this.EntityView)
        {
            UserControls_Listitems_Deliverypointgroup deliverypointgroupControl = this.LoadControl<UserControls_Listitems_Deliverypointgroup>("~/UserControls/Listitems/Deliverypointgroup.ascx");
            deliverypointgroupControl.Deliverypointgroup = deliverypointgroup;
            this.plhList.Controls.Add(deliverypointgroupControl);
        }
    }

    #endregion

    #endregion

    #region Properties

    public override string SessionKey
    {
        get { return "Deliverypointgroups"; }
    }

    #endregion

    #region Event handlers

    protected void Page_Load(object sender, EventArgs e)
    {
        this.Title = string.Format("{0} - {1}", Resources.strings.Deliverypointgroups, Resources.strings.Crave_NOC);
        this.Initialize();
    }

    #endregion
}