﻿using System;
using Obymobi.Constants;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Logic.HelperClasses;
using Obymobi.Noc.Logic.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

public partial class Deliverypoint : PageEntity<DeliverypointEntity>
{
    protected void Page_Load(object sender, EventArgs e)
    {
        this.InitializeEntity();

        if (this.Entity != null)
        {
            this.Title = string.Format("{0} '{1}' - {2}", Resources.strings.Deliverypoint, this.Entity.Name, Resources.strings.Crave_NOC);

            if (this.Entity.ClientCollection.Count > 0)
            {
                if (this.Entity.HasOnlineClient)
                {
                    this.imgDeliverypoint.ImageUrl = ResolveUrl("~/Images/Icons/checkmark.png");
                    this.lblTitle.Text = string.Format(Resources.strings.Deliverypoint_is_online, this.Entity.Name);
                }
                else
                {
                    this.imgDeliverypoint.ImageUrl = ResolveUrl("~/Images/Icons/error.png");
                    this.lblTitle.Text = string.Format(Resources.strings.Deliverypoint_has_offline_clients, this.Entity.Name);
                    this.lblLastRequest.CssClass = "error";
                }

                // Last request
                DateTime? lastRequest = this.Entity.LastRequest;
                if (lastRequest.HasValue)
                    this.lblLastRequest.Text = string.Format("{0} ({1})", CraveMobileNocHelper.ConvertUtcDateTimeToUserTimeZone(lastRequest.Value), CraveMobileNocHelper.GetSpanTextBetweenDateTimeAndUtcNow(lastRequest.Value));
                else
                    this.lblLastRequest.Text = Resources.strings.Unknown;

                // Clients
                // REFACTOR this.Entity.ClientCollection.Sort(ClientFields.LastRequestUTC.FieldIndex, System.ComponentModel.ListSortDirection.Descending);
                string emenuReleaseVersion = ReleaseHelper.GetVersionForCompany(ApplicationCode.Emenu, this.Entity.CompanyId);
                foreach (ClientEntity client in this.Entity.ClientCollection)
                {
                    UserControls_Listitems_Client clientControl = this.LoadControl<UserControls_Listitems_Client>("~/UserControls/Listitems/Client.ascx");
                    clientControl.Client = client;
                    clientControl.EmenuReleaseVersion = emenuReleaseVersion;
                    this.plhClientsList.Controls.Add(clientControl);
                }
            }
            else
            {
                this.imgDeliverypoint.ImageUrl = ResolveUrl("~/Images/Icons/exclamation.png");
                this.lblTitle.Text = string.Format(Resources.strings.Deliverypoint_has_no_clients, this.Entity.Name);
                this.plhClientsList.AddHtml("<li>" + string.Format(Resources.strings.Deliverypoint_has_no_clients, this.Entity.Name) + "</li>");
                this.lblLastRequest.Text = Resources.strings.Unknown;

            }

            // Orders                
            var filter = new PredicateExpression(OrderFields.DeliverypointId == this.Entity.DeliverypointId);
            var relations = new RelationCollection(OrderEntityBase.Relations.DeliverypointEntityUsingDeliverypointId);
            var orderCollection = NocHelper.GetOrders(filter, relations, false, 10);
            foreach (var order in orderCollection)
            {
                UserControls_Listitems_Order orderControl = this.LoadControl<UserControls_Listitems_Order>("~/UserControls/Listitems/Order.ascx");
                orderControl.Order = order;
                this.plhOrdersList.Controls.Add(orderControl);
            }

            this.plhOrders.Visible = (orderCollection.Count > 0);

            // Deliverypoint group
            this.hlDeliverypointgroup.Text = this.Entity.DeliverypointgroupEntity.Name;
            this.hlDeliverypointgroup.NavigateUrl = ResolveUrl(string.Format("~/Deliverypointgroup.aspx?id={0}", this.Entity.DeliverypointgroupId));
            
            this.hlOpenInCms.NavigateUrl = this.GetCmsUrl(this.Entity.CompanyId, "Company/Deliverypoint.aspx?id={0}", this.Entity.DeliverypointId);
        }
    }
}