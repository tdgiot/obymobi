﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/CraveMobileNocBase.master" AutoEventWireup="true" Inherits="DisconnectedFromComet" Codebehind="DisconnectedFromComet.aspx.cs" %>
<%@ Reference VirtualPath="~/UserControls/Listitems/Client.ascx" %>
<%@ Reference VirtualPath="~/UserControls/Listitems/Terminal.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <ul class="details">
        <li>            
            <div class="title"><D:LabelTextOnly runat="server" Text="<%$ Resources:strings, Clients %>"></D:LabelTextOnly></div>
        </li>
    </ul>
    <ul class="list">
        <D:PlaceHolder ID="plhClients" runat="server"></D:PlaceHolder>
    </ul>
    <ul class="details">
        <li class="spacer"></li>
        <li>            
            <div class="title"><D:LabelTextOnly runat="server" Text="<%$ Resources:strings, Terminals %>"></D:LabelTextOnly></div>
        </li>
    </ul>
    <ul class="list">
        <D:PlaceHolder ID="plhTerminals" runat="server"></D:PlaceHolder>
    </ul>
</asp:Content>

