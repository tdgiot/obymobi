﻿using Dionysos.Web;
using Obymobi.Data.EntityClasses;
using Obymobi.Enums;
using Obymobi.Logic.HelperClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Gateway : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        long timestamp;
        string macAddress;
        int cloudApplication;
        string hash;

        if (!QueryStringHelper.TryGetValue("ts", out timestamp))
        {
            // No timestamp supplied
        }
        else if (!QueryStringHelper.TryGetValue("id", out macAddress))
        {
            // No MAC address supplied
        }
        else if (!QueryStringHelper.TryGetValue("app", out cloudApplication))
        {
            // No cloud application supplied
        }
        else if (!QueryStringHelper.TryGetValue("hash", out hash))
        {
            // No hash supplied
        }
        else
        {
            // Validate the parameters
            DeviceEntity deviceEntity = WebserviceHelper.ValidateRequest(timestamp, macAddress, hash, cloudApplication);

            CloudApplication cloudApplicationEnum = (CloudApplication)cloudApplication;
            switch (cloudApplicationEnum)
            {
                case CloudApplication.API:
                    break;
                case CloudApplication.Management:
                    break;
                case CloudApplication.Messaging:
                    break;
                case CloudApplication.NOC:
                    break;
                case CloudApplication.Services:
                    break;
                case CloudApplication.NOCService:
                    break;
                default:
                    break;
            }
        }
    }
}