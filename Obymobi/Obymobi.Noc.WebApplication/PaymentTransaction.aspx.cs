﻿using System;
using System.Linq;
using Obymobi.Data.EntityClasses;
using Dionysos;
using Obymobi.Data.CollectionClasses;
using Obymobi.Enums;

public partial class PaymentTransaction : PageEntity<PaymentTransactionEntity>
{
    protected void Page_Load(object sender, EventArgs e)
    {
        this.InitializeEntity();

        if (this.Entity == null)
        {
            return;
        }

        DateTime createdLocalTime = CraveMobileNocHelper.ConvertUtcDateTimeToUserTimeZone(this.Entity.CreatedUTC);
        
        this.lblTitle.Text = $@"{this.Entity.ReferenceId} - {this.Entity.Status}";
        this.lblReferenceId.Text = this.Entity.ReferenceId;
        this.lblMerchantRef.Text = this.Entity.MerchantReference;
        this.lblMerchantAccount.Text = this.Entity.MerchantAccount;
        this.lblStatus.Text = this.Entity.Status.ToString();
        this.hlOrderId.NavigateUrl = ResolveUrl($"~/Order.aspx?id={this.Entity.OrderId}");
        this.hlOrderId.Text = this.Entity.OrderId.ToString();
        this.lblPaymentMethod.Text = this.Entity.PaymentMethod;
        this.lblEnvironment.Text = this.Entity.PaymentEnvironment.ToString();
        this.lblPaymentProvider.Text = this.Entity.PaymentProviderType.ToString();
        this.lblCreated.Text = createdLocalTime.ToString("F");

        switch (this.Entity.Status)
        {
            case PaymentTransactionStatus.Pending:
            case PaymentTransactionStatus.Initiated:
                this.imgStatus.ImageUrl = ResolveUrl("~/Images/Icons/hourglass.png");
                break;
            case PaymentTransactionStatus.Paid:
                this.imgStatus.ImageUrl = ResolveUrl("~/Images/Icons/checkmark.png");
                break;
            default:
                this.imgStatus.ImageUrl = ResolveUrl("~/Images/Icons/error.png");
                break;
        }

        RenderPaymentTransactions(this.Entity);
    }

    private void RenderPaymentTransactions(PaymentTransactionEntity entity)
    {
        PaymentTransactionLogCollection logCollection = entity.PaymentTransactionLogCollection;
        if (logCollection.Count == 0) return;

        this.plhPaymentTransactionLogs.Visible = true;

        foreach (PaymentTransactionLogEntity logEntity in logCollection.OrderByDescending(logEntity => logEntity.EventDate))
        {
            var control = this.LoadControl<UserControls_Listitems_PaymentTransactionLog>("~/UserControls/Listitems/PaymentTransactionLog.ascx");
            control.PaymentTransactionLog = logEntity;
            this.plhPaymentTransactionLogList.Controls.Add(control);
        }

    }
}