﻿using System;
using System.Collections.Generic;
using Dionysos;
using Dionysos.Web;
using Obymobi;
using Obymobi.Constants;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Logic.HelperClasses;
using Obymobi.Noc.Logic.HelperClasses;

public partial class Versions : System.Web.UI.Page
{
    private readonly SortedDictionary<string, int> terminalApplicationVersions = new SortedDictionary<string, int>();
    private readonly SortedDictionary<string, int> terminalAgentVersions = new SortedDictionary<string, int>();
    private readonly SortedDictionary<string, int> terminalSupportToolsVersions = new SortedDictionary<string, int>();

    private int totalTerminalApplicationVersions;
    private int totalTerminalAgentVersions;
    private int totalTerminalSupportToolsVersions;

    private readonly SortedDictionary<string, int> clientApplicationVersions = new SortedDictionary<string, int>();
    private readonly SortedDictionary<string, int> clientAgentVersions = new SortedDictionary<string, int>();
    private readonly SortedDictionary<string, int> clientSupportToolsVersions = new SortedDictionary<string, int>();

    private int totalClientApplicationVersions;
    private int totalClientAgentVersions;
    private int totalClientSupportToolsVersions;

    private string emenuVersion = string.Empty;
    private string agentVersion = string.Empty;
    private string supportToolsVersion = string.Empty;
    private string consoleVersion = string.Empty;

    protected void Page_Load(object sender, EventArgs e)
    {
        this.Title = string.Format("{0} - {1}", Resources.strings.Versions, Resources.strings.Crave_NOC);
        this.Initialize();
    }

    private void Initialize()
    {
        if (QueryStringHelper.HasValue("cid"))
        { 
            int companyId = QueryStringHelper.GetInt("cid");
            if (CraveMobileNocHelper.CheckAuthorization(companyId))
            {
                List<int> companyIds = new List<int>() {companyId};

                ClientCollection clientCollection = NocHelper.GetClients(companyIds, null, false);
                TerminalCollection terminalCollection = NocHelper.GetTerminals(companyIds, false, null);

                this.emenuVersion = ReleaseHelper.GetVersionForCompany(ApplicationCode.Emenu, companyId);
                this.agentVersion = ReleaseHelper.GetVersionForCompany(ApplicationCode.Agent, companyId);
                this.supportToolsVersion = ReleaseHelper.GetVersionForCompany(ApplicationCode.SupportTools, companyId);
                this.consoleVersion = ReleaseHelper.GetVersionForCompany(ApplicationCode.Console, companyId);

                this.GetClientVersions(clientCollection);
                this.GetTerminalVersions(terminalCollection);

                this.RenderClientVersions();
                this.RenderTerminalVersions();
            }
        }
    }

    private void GetClientVersions(ClientCollection clientCollection)
    {
        foreach (ClientEntity clientEntity in clientCollection)
        {
            if (!clientEntity.DeviceEntity.ApplicationVersion.IsNullOrWhiteSpace())
            {
                string applicationVersion = clientEntity.DeviceEntity.ApplicationVersion;

                if (!this.clientApplicationVersions.ContainsKey(applicationVersion))
                    this.clientApplicationVersions.Add(applicationVersion, 1);
                else
                    this.clientApplicationVersions[applicationVersion]++;

                this.totalClientApplicationVersions++;
            }

            if (!clientEntity.DeviceEntity.AgentVersion.IsNullOrWhiteSpace())
            {
                string agentVersion = clientEntity.DeviceEntity.AgentVersion;

                if (!this.clientAgentVersions.ContainsKey(agentVersion))
                    this.clientAgentVersions.Add(agentVersion, 1);
                else
                    this.clientAgentVersions[agentVersion]++;

                this.totalClientAgentVersions++;
            }

            if (!clientEntity.DeviceEntity.SupportToolsVersion.IsNullOrWhiteSpace())
            {
                string supportToolsVersion = clientEntity.DeviceEntity.SupportToolsVersion;

                if (!this.clientSupportToolsVersions.ContainsKey(supportToolsVersion))
                    this.clientSupportToolsVersions.Add(supportToolsVersion, 1);
                else
                    this.clientSupportToolsVersions[supportToolsVersion]++;

                this.totalClientSupportToolsVersions++;
            }
        }
    }

    private void GetTerminalVersions(TerminalCollection terminalCollection)
    {
        foreach (TerminalEntity terminalEntity in terminalCollection)
        {
            if (!terminalEntity.DeviceEntity.ApplicationVersion.IsNullOrWhiteSpace())
            {
                string applicationVersion = terminalEntity.DeviceEntity.ApplicationVersion;

                if (!this.terminalApplicationVersions.ContainsKey(applicationVersion))
                    this.terminalApplicationVersions.Add(applicationVersion, 1);
                else
                    this.terminalApplicationVersions[applicationVersion]++;

                this.totalTerminalApplicationVersions++;
            }

            if (!terminalEntity.DeviceEntity.AgentVersion.IsNullOrWhiteSpace())
            {
                string agentVersion = terminalEntity.DeviceEntity.AgentVersion;

                if (!this.terminalAgentVersions.ContainsKey(agentVersion))
                    this.terminalAgentVersions.Add(agentVersion, 1);
                else
                    this.terminalAgentVersions[agentVersion]++;

                this.totalTerminalAgentVersions++;
            }

            if (!terminalEntity.DeviceEntity.SupportToolsVersion.IsNullOrWhiteSpace())
            {
                string supportToolsVersion = terminalEntity.DeviceEntity.SupportToolsVersion;

                if (!this.terminalSupportToolsVersions.ContainsKey(supportToolsVersion))
                    this.terminalSupportToolsVersions.Add(supportToolsVersion, 1);
                else
                    this.terminalSupportToolsVersions[supportToolsVersion]++;

                this.totalTerminalSupportToolsVersions++;
            }
        }
    }

    private void RenderClientVersions()
    {
        foreach (string clientApplicationVersion in this.clientApplicationVersions.Keys)
        {
            this.plhClientApplicationVersions.AddHtml("<li>");

            if (!this.emenuVersion.IsNullOrWhiteSpace() && VersionNumber.CompareVersions(this.emenuVersion, clientApplicationVersion) == VersionNumber.VersionState.Equal)
            {
                this.plhClientApplicationVersions.AddHtml("<div class=\"label\"><span class=\"info\">{0}</span></div>", clientApplicationVersion);
                this.plhClientApplicationVersions.AddHtml("<div class=\"value\"><span class=\"info\">{0} ({1}%)</span></div>", this.clientApplicationVersions[clientApplicationVersion], Dionysos.Math.Percentage(this.clientApplicationVersions[clientApplicationVersion], this.totalClientApplicationVersions, true));
            }
            else
            {
                this.plhClientApplicationVersions.AddHtml("<div class=\"label\">{0}</div>", clientApplicationVersion);
                this.plhClientApplicationVersions.AddHtml("<div class=\"value\">{0} ({1}%)</div>", this.clientApplicationVersions[clientApplicationVersion], Dionysos.Math.Percentage(this.clientApplicationVersions[clientApplicationVersion], this.totalClientApplicationVersions, true));
            }

            this.plhClientApplicationVersions.AddHtml("</li>");
        }

        foreach (string clientAgentVersion in this.clientAgentVersions.Keys)
        {
            this.plhClientAgentVersions.AddHtml("<li>");

            if (!this.agentVersion.IsNullOrWhiteSpace() && VersionNumber.CompareVersions(this.agentVersion, clientAgentVersion) == VersionNumber.VersionState.Equal)
            {
                this.plhClientAgentVersions.AddHtml("<div class=\"label\"><span class=\"info\">{0}</span></div>", clientAgentVersion);
                this.plhClientAgentVersions.AddHtml("<div class=\"value\"><span class=\"info\">{0} ({1}%)</span></div>", this.clientAgentVersions[clientAgentVersion], Dionysos.Math.Percentage(this.clientAgentVersions[clientAgentVersion], this.totalClientAgentVersions, true));
            }
            else
            {
                this.plhClientAgentVersions.AddHtml("<div class=\"label\">{0}</div>", clientAgentVersion);
                this.plhClientAgentVersions.AddHtml("<div class=\"value\">{0} ({1}%)</div>", this.clientAgentVersions[clientAgentVersion], Dionysos.Math.Percentage(this.clientAgentVersions[clientAgentVersion], this.totalClientAgentVersions, true));
            }

            this.plhClientAgentVersions.AddHtml("</li>");
        }

        foreach (string clientSupportToolsVersion in this.clientSupportToolsVersions.Keys)
        {
            this.plhClientSupportToolsVersions.AddHtml("<li>");

            if (!this.supportToolsVersion.IsNullOrWhiteSpace() && VersionNumber.CompareVersions(this.supportToolsVersion, clientSupportToolsVersion) == VersionNumber.VersionState.Equal)
            {
                this.plhClientSupportToolsVersions.AddHtml("<div class=\"label\"><span class=\"info\">{0}</span></div>", clientSupportToolsVersion);
                this.plhClientSupportToolsVersions.AddHtml("<div class=\"value\"><span class=\"info\">{0} ({1}%)</span></div>", this.clientSupportToolsVersions[clientSupportToolsVersion], Dionysos.Math.Percentage(this.clientSupportToolsVersions[clientSupportToolsVersion], this.totalClientSupportToolsVersions, true));
            }
            else
            {
                this.plhClientSupportToolsVersions.AddHtml("<div class=\"label\">{0}</div>", clientSupportToolsVersion);
                this.plhClientSupportToolsVersions.AddHtml("<div class=\"value\">{0} ({1}%)</div>", this.clientSupportToolsVersions[clientSupportToolsVersion], Dionysos.Math.Percentage(this.clientSupportToolsVersions[clientSupportToolsVersion], this.totalClientSupportToolsVersions, true));
            }

            this.plhClientSupportToolsVersions.AddHtml("</li>");
        }
    }

    private void RenderTerminalVersions()
    {
        foreach (string terminalApplicationVersion in this.terminalApplicationVersions.Keys)
        {
            this.plhTerminalApplicationVersions.AddHtml("<li>");

            if (!this.consoleVersion.IsNullOrWhiteSpace() && VersionNumber.CompareVersions(this.consoleVersion, terminalApplicationVersion) == VersionNumber.VersionState.Equal)
            {
                this.plhTerminalApplicationVersions.AddHtml("<div class=\"label\"><span class=\"info\">{0}</span></div>", terminalApplicationVersion);
                this.plhTerminalApplicationVersions.AddHtml("<div class=\"value\"><span class=\"info\">{0} ({1}%)</span></div>", this.terminalApplicationVersions[terminalApplicationVersion], Dionysos.Math.Percentage(this.terminalApplicationVersions[terminalApplicationVersion], this.totalTerminalApplicationVersions, true));
            }
            else
            {
                this.plhTerminalApplicationVersions.AddHtml("<div class=\"label\">{0}</div>", terminalApplicationVersion);
                this.plhTerminalApplicationVersions.AddHtml("<div class=\"value\">{0} ({1}%)</div>", this.terminalApplicationVersions[terminalApplicationVersion], Dionysos.Math.Percentage(this.terminalApplicationVersions[terminalApplicationVersion], this.totalTerminalApplicationVersions, true));
            }

            this.plhTerminalApplicationVersions.AddHtml("</li>");
        }

        foreach (string terminalAgentVersion in this.terminalAgentVersions.Keys)
        {
            this.plhTerminalAgentVersions.AddHtml("<li>");

            if (!this.agentVersion.IsNullOrWhiteSpace() && VersionNumber.CompareVersions(this.agentVersion, terminalAgentVersion) == VersionNumber.VersionState.Equal)
            {
                this.plhTerminalAgentVersions.AddHtml("<div class=\"label\"><span class=\"info\">{0}</span></div>", terminalAgentVersion);
                this.plhTerminalAgentVersions.AddHtml("<div class=\"value\"><span class=\"info\">{0} ({1}%)</span></div>", this.terminalAgentVersions[terminalAgentVersion], Dionysos.Math.Percentage(this.terminalAgentVersions[terminalAgentVersion], this.totalTerminalAgentVersions, true));
            }
            else
            {
                this.plhTerminalAgentVersions.AddHtml("<div class=\"label\">{0}</div>", terminalAgentVersion);
                this.plhTerminalAgentVersions.AddHtml("<div class=\"value\">{0} ({1}%)</div>", this.terminalAgentVersions[terminalAgentVersion], Dionysos.Math.Percentage(this.terminalAgentVersions[terminalAgentVersion], this.totalTerminalAgentVersions, true));
            }

            this.plhTerminalAgentVersions.AddHtml("</li>");
        }

        foreach (string terminalSupportToolsVersion in this.terminalSupportToolsVersions.Keys)
        {
            this.plhTerminalSupportToolsVersions.AddHtml("<li>");

            if (!this.supportToolsVersion.IsNullOrWhiteSpace() && VersionNumber.CompareVersions(this.supportToolsVersion, terminalSupportToolsVersion) == VersionNumber.VersionState.Equal)
            {
                this.plhTerminalSupportToolsVersions.AddHtml("<div class=\"label\"><span class=\"info\">{0}</span></div>", terminalSupportToolsVersion);
                this.plhTerminalSupportToolsVersions.AddHtml("<div class=\"value\"><span class=\"info\">{0} ({1}%)</span></div>", this.terminalSupportToolsVersions[terminalSupportToolsVersion], Dionysos.Math.Percentage(this.terminalSupportToolsVersions[terminalSupportToolsVersion], this.totalTerminalSupportToolsVersions, true));
            }
            else
            {
                this.plhTerminalSupportToolsVersions.AddHtml("<div class=\"label\">{0}</div>", terminalSupportToolsVersion);
                this.plhTerminalSupportToolsVersions.AddHtml("<div class=\"value\">{0} ({1}%)</div>", this.terminalSupportToolsVersions[terminalSupportToolsVersion], Dionysos.Math.Percentage(this.terminalSupportToolsVersions[terminalSupportToolsVersion], this.totalTerminalSupportToolsVersions, true));
            }

            this.plhTerminalSupportToolsVersions.AddHtml("</li>");
        }
    }
}