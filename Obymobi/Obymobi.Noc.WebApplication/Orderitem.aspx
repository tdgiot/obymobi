﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/CraveMobileNocBase.master" AutoEventWireup="true" Inherits="Orderitem" Codebehind="Orderitem.aspx.cs" %>
<%@ Reference VirtualPath="~/UserControls/Listitems/OrderitemAlterationitem.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<ul class="details">
    <li>
        <D:Image ID="imgOrderitem" runat="server" />
        <div class="title"><D:LabelTextOnly ID="lblTitle" runat="server"></D:LabelTextOnly></div>
    </li>
    <li>
        <div class="label"><D:LabelTextOnly ID="lblMasterOrder" runat="server" Text="<%$ Resources:strings, Orderitem_master_order %>"></D:LabelTextOnly>:</div>
        <div class="value"><D:HyperLink ID="hlMasterOrder" runat="server"></D:HyperLink></div>
    </li>
    <li>
        <div class="label"><D:LabelTextOnly ID="lblOrder" runat="server" Text="<%$ Resources:strings, Orderitem_order %>"></D:LabelTextOnly>:</div>
        <div class="value"><D:HyperLink ID="hlOrderId" runat="server"></D:HyperLink></div>
    </li>
    <li>
        <div class="label"><D:LabelTextOnly ID="lblOrderStatusLabel" runat="server" Text="<%$ Resources:strings, Status %>"></D:LabelTextOnly>:</div>
        <div class="value"><D:Label ID="lblOrderStatus" runat="server"></D:Label></div>
    </li>
    <li>
        <div class="label"><D:LabelTextOnly ID="lblOrderErrorCodeLabel" runat="server" Text="<%$ Resources:strings, Order_errorcode %>"></D:LabelTextOnly>:</div>
        <div class="value"><D:Label ID="lblOrderErrorCode" runat="server"></D:Label></div>
    </li>
    <li>
        <div class="label"><D:LabelTextOnly ID="lblOrderitemNotesLabel" runat="server" Text="<%$ Resources:strings, Order_notes %>"></D:LabelTextOnly>:</div>
        <div class="value"><D:Label ID="lblOrderitemNotes" runat="server"></D:Label></div>
    </li>
</ul>
<D:PlaceHolder ID="plhAlterations" runat="server">
<h1><D:LabelTextOnly ID="lblAlterations" runat="server" Text="<%$ Resources:strings, Orderitem_alterations %>"></D:LabelTextOnly></h1>
<ul class="list">
    <D:PlaceHolder ID="plhAlterationsList" runat="server"></D:PlaceHolder>
</ul>    
</D:PlaceHolder>
</asp:Content>


