﻿using System;
using Dionysos;
using Obymobi.Logic.HelperClasses;
using Obymobi.Data.CollectionClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Dionysos.Web;
using Obymobi.Logic.Routing;
using Obymobi.Noc.Logic.HelperClasses;
using Obymobi.Enums;

public partial class OrderRoutestephandlers : PageListview<OrderRoutestephandlerEntity>
{
    #region Methods

    #region Base class methods

    protected override EntityView<OrderRoutestephandlerEntity> GetEntityViewFromSession()
    {
        return Session[this.SessionKeyEntityView] as EntityView<OrderRoutestephandlerEntity>;
    }

    protected override void GetFilterAndSortFromSession()
    {
        object filter = Session[this.SessionKeyFilter];
        if (filter != null)
            this.ddlFilter.SelectedIndex = (int)filter;

        object sort = Session[this.SessionKeySort];
        if (sort != null)
            this.ddlSort.SelectedIndex = (int)sort;
    }

    protected override EntityView<OrderRoutestephandlerEntity> InitializeEntityView()
    {
        // Get the terminals
        OrderRoutestephandlerCollection orderRoutestephandlerCollection;

        if (QueryStringHelper.HasValue("oid"))
            orderRoutestephandlerCollection = NocHelper.GetOrderRoutestephandlers(QueryStringHelper.GetInt("oid"));
        else
            throw new FunctionalException("'OrderId' parameter is missing, could not retrieve OrderRoutestephandlers.");

        // Return the view
        return orderRoutestephandlerCollection.DefaultView;
    }

    protected override void SetFilter()
    {
        // Filter
        switch (this.ddlFilter.SelectedIndex)
        {
            case 0:
                // All order routestephandlers
                this.EntityView.Filter = null;
                break;
            case 1:
                // Processed order routestephandlers
                PredicateExpression processedFilter = new PredicateExpression();
                processedFilter.Add(OrderRoutestephandlerFields.Status == OrderRoutestephandlerStatus.Completed);
                processedFilter.AddWithOr(OrderRoutestephandlerFields.Status == OrderRoutestephandlerStatus.CompletedByOtherStep);
                this.EntityView.Filter = processedFilter;

                break;
            case 2:
                // Unprocessed order routestephandlers
                PredicateExpression unprocessedFilter = new PredicateExpression();
                unprocessedFilter.Add(OrderRoutestephandlerFields.Status != OrderRoutestephandlerStatus.Completed);
                unprocessedFilter.Add(OrderRoutestephandlerFields.Status != OrderRoutestephandlerStatus.CompletedByOtherStep);
                this.EntityView.Filter = unprocessedFilter;
                break;
        }

        // Store the current filter in the session
        Session[this.SessionKeyFilter] = this.ddlFilter.SelectedIndex;
    }

    protected override void SetSort()
    {
        // Sort
        switch (this.ddlSort.SelectedIndex)
        {
            case 0:
                // Number, ascending
                this.EntityView.Sorter = new SortExpression(new SortClause(OrderRoutestephandlerFields.Number, SortOperator.Ascending));
                break;
            case 1:
                // Status, ascending
                this.EntityView.Sorter = new SortExpression(new SortClause(OrderRoutestephandlerFields.Status, SortOperator.Ascending));
                break;
       }

        // Store the current sort in the session
        Session[this.SessionKeySort] = this.ddlSort.SelectedIndex;
    }

    protected override void RenderList()
    {
        if (this.Count == 1)
            this.lblTitle.Text = Resources.strings.One_item;
        else
            this.lblTitle.Text = string.Format(Resources.strings.X_items, this.Count);

        foreach (var orderRoutestephandler in this.EntityView)
        {
            UserControls_Listitems_OrderRoutestephandler orderRoutestephandlerControl = this.LoadControl<UserControls_Listitems_OrderRoutestephandler>("~/UserControls/Listitems/OrderRoutestephandler.ascx");
            orderRoutestephandlerControl.OrderRoutestephandler = orderRoutestephandler;
            this.plhList.Controls.Add(orderRoutestephandlerControl);
        }
    }

    #endregion

    #endregion

    #region Properties

    public override string SessionKey
    {
        get { return "OrderRoutestephandlers"; }
    }

    #endregion

    #region Event handlers

    protected void Page_Load(object sender, EventArgs e)
    {
        this.Title = string.Format("{0} - {1}", Resources.strings.Order_steps, Resources.strings.Crave_NOC);
        this.Initialize();
    }

    #endregion
}