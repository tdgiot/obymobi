﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;
using System.Web;
using System.Threading;
using Dionysos;
using Obymobi.Constants;
using Dionysos.Web;
using Obymobi.Enums;
using System.ServiceModel;
using Obymobi.Security;
using System.Net;
using System.IO;
using Obymobi;
using Obymobi.Logic.Model;
using Obymobi.Logic.HelperClasses;
using System.Text;
using System.Data.SqlClient;
using Obymobi.Data.EntityClasses;
using Dionysos.Net;
using Obymobi.Data.CollectionClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Logging;
using Org.BouncyCastle.Pkcs;

public partial class Tools : System.Web.UI.Page
{
    readonly string applicationKeyWebserviceStateRefreshing = "Tools.webserviceStateRefreshing";
    readonly string applicationKeyPrimaryWebserviceOnline = "Tools.PrimaryWebserviceOnline";
    readonly string applicationKeySecondaryWebserviceOnline = "Tools.SecondaryWebserviceOnline";
    readonly string applicationKeyNocStatusReport = "Tools.NocStatusReport";
    readonly string applicationKeyDatabaseStateRefreshing = "Tools.databaseStateRefreshing";
    readonly string applicationKeyPrimaryDatabaseOnline = "Tools.PrimaryDatabaseOnline";
    readonly string applicationKeySecondaryDatabaseOnline = "Tools.SecondaryDatabaseOnline";
    readonly string applicationKeyDatabaseConnectionInformation = "Tools.DatabaseConnectionInformation";
    readonly string applicationKeyLastDbRefresh = "Tools.LastDbRefresh";
    readonly string applicationKeyLastCloudRefresh = "Tools.LastCloudRefresh";
    readonly string applicationKeyCacheStateRefreshing = "Tools.LastCacheStateRefreshing";
    readonly string applicationKeyLastCacheClearCMS = "Tools.LastCacheClearCMS";
    readonly string applicationKeyLastCacheClearWebservice = "Tools.LastCacheClearWebservice";
    readonly string applicationKeyLastCacheClearCometServer = "Tools.LastCacheClearCometServer";
    readonly string applicationKeyLastCacheClearNOC = "Tools.LastCacheClearNOC";
    readonly string applicationKeyApplicationPoolStateRefreshing = "Tools.ApplicationPoolStateRefreshing";
    readonly string applicationKeyApplicationPoolStateCMS = "Tools.ApplicationPoolStateCMS";
    readonly string applicationKeyApplicationPoolStateWebservice = "Tools.ApplicationPoolStateWebservice";
    readonly string applicationKeyApplicationPoolStateCometServer = "Tools.ApplicationPoolStateCometServer";
    readonly string applicationKeyApplicationPoolStateNOC = "Tools.ApplicationPoolStateNOC";


    // The ConnectionStrings are set here, instead of in the Obymobi constants, because then they don't get installed with the OSS code.
    // ALSO this is copied in the NocService in Constansts, so if you update these, also update those!    
    readonly string productionPrimaryConnectionString = GetProductionPrimaryConnectionString();
    readonly string productionSecondaryConnectionString = GetProductionSecondaryConnectionString();

    private static string GetProductionPrimaryConnectionString()
    { 
        if(TestUtil.IsPcGabriel)
            return "Server=192.168.198.129,1433;Initial Catalog=msdb;User Id=sa;Password=1Administrator2x@home;";
        
        return "Server=appdb.crave-emenu.com,6736;Initial Catalog=msdb;User Id=cravedb;Password=pSEm8tRV3UCdHnefLJa9;";
    }

    private static string GetProductionSecondaryConnectionString()
    {
        if (TestUtil.IsPcGabriel)
            return "Server=192.168.198.130,1433;Initial Catalog=msdb;User Id=sa;Password=1Administrator2x@home;";
        
        return "Server=appdb.craveinteractive.net,6736;Initial Catalog=msdb;User Id=cravedb;Password=pSEm8tRV3UCdHnefLJa9;";
    }

    protected override void OnInit(EventArgs e)
    {        
        base.OnInit(e);

        if (!CraveMobileNocHelper.IsAdministrator)
            throw new AuthorizationException("Not Allowed!");

    }

    protected void Page_Load(object sender, EventArgs e)
    {
        this.Title = string.Format("{0} - {1}", Resources.strings.Tools, Resources.strings.Crave_NOC);

		this.DoUpdaters();

        this.plhCacheManagement.Visible = false;
        this.plhCloudManagement.Visible = false;
        this.plhDatabaseManagement.Visible = false;
        this.plhApplicationPoolManagement.Visible = false;
        this.phlSetCometHandlers.Visible = false;
        this.plhConfirmCloudEnvironmentSwitch.Visible = false;
        this.plOrderManagement.Visible = false;
        this.plOrderManagementOptions.Visible = false;

        int newCloudEnvironment;
        int breakMirror = 0;
        int clearCache = 0;
        int recyleApp = 0;
        CloudEnvironment environment;
        if (QueryStringHelper.TryGetValue("switchToCloudEnvironment", out newCloudEnvironment) && EnumUtil.TryParse(newCloudEnvironment, out environment))
        {            
            this.plhConfirmCloudEnvironmentSwitch.Visible = true;            
        }       
        else if(QueryStringHelper.HasValue("switchToDb") && QueryStringHelper.TryGetValue("breakMirror", out breakMirror))
        {
            this.plhConfirmCloudEnvironmentSwitch.Visible = true;

            if (breakMirror == 0)
                this.lblConfirmEnvironmentSwitchTitle.Text = Resources.strings.Tools_Failover_Database_Confirmation;
            else
                this.lblConfirmEnvironmentSwitchTitle.Text = Resources.strings.Tools_BreakMirror_Database_Confirmation;       
        }
        else if (QueryStringHelper.HasValue("updateConnectionString"))
        {
            this.UpdateConnectionStringManually();
        }
        else if (QueryStringHelper.TryGetValue("clearCache", out clearCache))
        {
            this.plhClearCacheResult.Visible = true;

            if (this.ClearCache(clearCache))
            {
                this.plhClearCacheResultReport.AddHtml("<strong>Cache has been cleared successfully.</strong>");
            }
            else
            {
                this.plhClearCacheResultReport.AddHtml("<strong>Something went wrong while trying to clear cache.</strong>");
            }
        }
        else if (QueryStringHelper.TryGetValue("recycleApp", out recyleApp))
        {
            this.plhClearCacheResult.Visible = true;

            if (this.RecycleApplication(recyleApp))
            {
                this.plhClearCacheResultReport.AddHtml("<strong>Application has been recycled successfully.</strong>");                
            }
            else
            {
                this.plhClearCacheResultReport.AddHtml("<strong>Something went wrong while trying to recycle the application.</strong>");
            }
        }
        else if (QueryStringHelper.HasValue("setCometHandlers"))
        {
            this.phlSetCometHandlers.Visible = true;
            this.btnSwitchCometHandlers.Click += btnSwitchCometHandlers_Click;

            if (!this.IsPostBack)
                RenderSetCometHandlers();
        }
        else if (QueryStringHelper.HasValue("handleOrders"))
        {
            this.plOrderManagement.Visible = true;
            this.hlHandleOrders.Visible = false;
            this.hlProcessOrderConfirmationYes.Click += this.SetUnprocessedOrdersToProcessed;
        }
        else
        {
            this.plhCacheManagement.Visible = true;
            this.plhCloudManagement.Visible = true;
            this.plhDatabaseManagement.Visible = true;
            this.plhApplicationPoolManagement.Visible = true;
            this.plOrderManagementOptions.Visible = true;
            this.hlHandleOrders.Visible = true;
        }
        
        if (!this.IsPostBack)
            this.RenderContent();

        // Only for Administrators
        if (!CraveMobileNocHelper.IsAdministrator)
        {
            this.plhDatabaseManagementSwitching.Visible = false;
            this.plhCloudManagementSwitching.Visible = false;

            // But even then we don't want playing around with the CloudStatus and ConnectionString anymore
            // the functionality isn't tested good enough and probably not reliable. If you need to do those
            // actions, for now you would need access to a PC.
            this.hlSwitchToPrimaryCloudEnvironment.Visible = false;
            this.hlSwitchToSecondaryCloudEnvironment.Visible = false;
            this.hlDatabaseSwitchToPrimary.Visible = false;
            this.hlDatabaseSwitchToSecondary.Visible = false;
            this.hlDatabaseBreakToPrimary.Visible = false;
            this.hlDatabaseBreakToSecondary.Visible = false;
        }

        if (CraveMobileNocHelper.CurrentUser.Role < Role.Crave)
        {
            this.plOrderManagementOptions.Visible = false;
        }
    }

    private void SetUnprocessedOrdersToProcessed(object sender, EventArgs e)
    {
        if (CraveMobileNocHelper.CurrentUser.Role < Role.Crave)
        {
            throw new Exception("You need crave access rights to change unprocessed orders to processed orders");
        }

        try
        {
            OrderCollection orders = new OrderCollection();
            PredicateExpression filter = new PredicateExpression();
            filter.Add(OrderFields.Status == OrderStatus.Unprocessable);
            filter.Add(OrderFields.CreatedUTC < DateTime.UtcNow.AddDays(-3));

            OrderEntity orderEntity = new OrderEntity();
            orderEntity.Status = 1000;
            orderEntity.UpdatedUTC = DateTime.UtcNow;
            int count = orders.UpdateMulti(orderEntity, filter);
            this.lblProcessOrderResult.AddHtml("{0} orders updated", count);

            string logMessage = $"{count} orders updated to from OrdersStatus {OrderStatus.Unprocessable} to {OrderStatus.Processed} with UpdatedUtc {DateTime.UtcNow} by {CraveMobileNocHelper.CurrentUser.Fullname}";
            File.AppendAllLines(HttpContext.Current.Server.MapPath($"~/App_Data/Audit/order_unprocessed_to_processed_{DateTime.UtcNow:yyyyMMdd}.txt"), new[] { logMessage });
            ConsoleLogger.WriteToLog(logMessage);
        }
        catch (Exception ex)
        {
            throw new Exception("Something went wrong while processing the orders. {0}", ex);
        }
    }

    private bool ClearCache(int cache)
    {
        try
        {
            long timestamp = DateTimeUtil.ToUnixTime(DateTime.UtcNow);
            string hash = Hasher.GetHashFromParameters(ObymobiConstants.GenericSalt, timestamp);

            string fileToCall = string.Format("ClearCache.aspx?timestamp={0}&hash={1}", timestamp, hash);
            string url;
            if (cache == 1)
                url = WebEnvironmentHelper.GetManagementUrl(fileToCall);
            else if (cache == 2)
                url = WebEnvironmentHelper.GetApiUrl(fileToCall);
            else if (cache == 3)                
                url = WebEnvironmentHelper.GetMessagingUrl(fileToCall);
            else if (cache == 4)
                url = WebEnvironmentHelper.GetNocUrl(fileToCall);                
            else
                return false;                        

            string response = HttpRequestHelper.GetResponseAsString(url);
            return response.Equals("OK");
        }
        catch
        {
            if(TestUtil.IsPcDeveloper)
                throw;

            return false;
        }        
    }

    private bool RecycleApplication(int app)
    {
        try
        {
            long timestamp = DateTimeUtil.ToUnixTime(DateTime.UtcNow);
            string hash = Hasher.GetHashFromParameters(ObymobiConstants.GenericSalt, timestamp, "recycle");

            string fileToCall = string.Format("ClearCache.aspx?timestamp={0}&request=recycle&hash={1}", timestamp, hash);
            string url;
            if (app == 1)
                url = WebEnvironmentHelper.GetManagementUrl(fileToCall);
            else if (app == 2)
                url = WebEnvironmentHelper.GetApiUrl(fileToCall);
            else if (app == 3)
                url = WebEnvironmentHelper.GetMessagingUrl(fileToCall);
            else if (app == 4)
                url = WebEnvironmentHelper.GetNocUrl(fileToCall);
            else
                return false;

            string response = HttpRequestHelper.GetResponseAsString(url);
            return response.Equals("OK");
        }
        catch
        {
            if (TestUtil.IsPcDeveloper)
                throw;

            return false;
        }         
    }

    #region Rendering content

    public void RenderContent()
    {
        // Cloud Management
        this.RenderCloudManagement();

        // Database Management
        this.RenderDatabaseManagement();

        // Cache Management
        this.RenderCacheManagement();

        // AppPool Management
        this.RenderApplicationPoolManagement();
    }

    private void RenderCloudManagement()
    {   
        this.lblLastCloudRefresh.Text = "{0} ({1})".FormatSafe(this.LastCloudRefresh, CraveMobileNocHelper.GetSpanTextBetweenDateTimeAndUtcNow(this.LastCloudRefresh));

        if(WebEnvironmentHelper.CloudEnvironment == CloudEnvironment.ProductionPrimary ||
            WebEnvironmentHelper.CloudEnvironment == CloudEnvironment.ProductionSecondary)
        {
            // Get online status of Primary Webservice
            bool? ws1Online = this.PrimaryWebserviceOnline;
            if (ws1Online.HasValue)
                this.lblPrimaryCloudServerStatus.Text = (ws1Online.Value ? "Online" : "Offline");
            else
            {
                this.lblPrimaryCloudServerStatus.Text = "Refreshing";
                ws1Online = false;
            }

            // Get online status of Secondary Webservice
            bool? ws2Online = this.SecondaryWebserviceOnline;
            if (ws2Online.HasValue)
                this.lblSecondaryCloudServerStatus.Text = (ws2Online.Value ? "Online" : "Offline");
            else
            {
                this.lblSecondaryCloudServerStatus.Text = "Refreshing";
                ws2Online = false;
            }

            if (this.NocStatusReport != null)
            {
                if (this.NocStatusReport.Environment == (int)CloudEnvironment.ProductionPrimary)
                {
                    this.lblCloudCurrentEnvironment.Text = CloudEnvironment.ProductionPrimary.ToString();
                    this.hlSwitchToPrimaryCloudEnvironment.Visible = false;
                    this.hlSwitchToSecondaryCloudEnvironment.Visible = ws2Online.Value;                    
                }
                else if (this.NocStatusReport.Environment == (int)CloudEnvironment.ProductionSecondary)
                {
                    this.lblCloudCurrentEnvironment.Text = CloudEnvironment.ProductionSecondary.ToString();
                    this.hlSwitchToPrimaryCloudEnvironment.Visible = ws1Online.Value;
                    this.hlSwitchToSecondaryCloudEnvironment.Visible = false;
                }

                if (!this.hlSwitchToPrimaryCloudEnvironment.Visible && !this.hlSwitchToSecondaryCloudEnvironment.Visible)
                {
                    this.plhCloudManagementSwitching.Visible = false;
                    this.plhCloudManagementUnavailable.Visible = true;
                }
                else
                {
                    this.plhCloudManagementSwitching.Visible = true;
                    this.plhCloudManagementUnavailable.Visible = false;
                }

                this.lblCloudCometHandlers.Text = this.NocStatusReport.ClientCommunication.ToEnum<ClientCommunicationMethod>().ToString();
            }
            else
            {
                // No switchting if we can't get the report.
                this.plhCloudManagementSwitching.Visible = false;
                this.plhCloudManagementUnavailable.Visible = true;
                this.lblCloudCurrentEnvironment.Text = Resources.strings.Refreshing;
                this.lblCloudCometHandlers.Text = Resources.strings.Refreshing;
            }

            this.hlResetCloudEnvironment.Visible = false;
        }
        else
        {
            if (this.NocStatusReport == null)
            {
                this.lblCloudCurrentEnvironment.Text = Resources.strings.Refreshing;
                this.lblCloudCometHandlers.Text = Resources.strings.Refreshing;
            }
            else
            {
                CloudEnvironment c;
                if (EnumUtil.TryParse(NocStatusReport.Environment, out c))
                    this.lblCloudCurrentEnvironment.Text = c.ToString();
                else
                    this.lblCloudCurrentEnvironment.Text = "Unknown: " + NocStatusReport.Environment;

                this.lblCloudCometHandlers.Text = this.NocStatusReport.ClientCommunication.ToEnum<ClientCommunicationMethod>().ToString();
            }

            // Get status of Primary Server
            if (this.PrimaryWebserviceOnline.HasValue)
                this.lblPrimaryCloudServerStatus.Text = (this.PrimaryWebserviceOnline.Value ? "Online" : "Offline");
            else
                this.lblPrimaryCloudServerStatus.Text = Resources.strings.Refreshing;

            this.plhSecondaryCloudServerStatus.Visible = false;
            this.plhCloudManagementSwitching.Visible = true;
            this.hlSwitchToPrimaryCloudEnvironment.Visible = false;
            this.hlSwitchToSecondaryCloudEnvironment.Visible = false;
            this.hlResetCloudEnvironment.Visible = true;
            this.hlResetCloudEnvironment.NavigateUrl = "~/Tools.aspx?switchToCloudEnvironment={0}".FormatSafe(WebEnvironmentHelper.CloudEnvironment.ToIntString());
            this.plhCloudManagementUnavailable.Visible = false;
        }
    }

    private void GetLastCacheClearStatus()
    {
        if (this.CacheStateRefreshing)
            return;

        this.CacheStateRefreshing = true;

        try
        {
            for (int i = 0; i < 4; i++)
            {
                string url;
                if (i == 0)
                    url = WebEnvironmentHelper.GetManagementUrl("ClearCache.aspx?lastcacheclear=");
                else if (i == 1)
                    url = WebEnvironmentHelper.GetApiUrl("ClearCache.aspx?lastcacheclear=");
                else if (i == 2)
                    url = WebEnvironmentHelper.GetMessagingUrl("ClearCache.aspx?lastcacheclear=");
                else if (i == 3)
                    url = WebEnvironmentHelper.GetNocUrl("ClearCache.aspx?lastcacheclear=");
                else
                    continue;

                var myHttpWebRequest = (HttpWebRequest) WebRequest.Create(new Uri(url));
                myHttpWebRequest.Timeout = 5000;
                string cacheResult = "Could not be determined";
                try
                {
                    var result = myHttpWebRequest.GetResponse().GetResponseStream();
                    if (result != null)
                    {
                        var stream = new StreamReader(result);
                        var buffer = new char[myHttpWebRequest.GetResponse().ContentLength];
                        stream.Read(buffer, 0, buffer.Length);

                        cacheResult = new string(buffer);
                    }
                }
                catch (Exception ex)
                {
                    if(TestUtil.IsPcDeveloper)
                        cacheResult = ex.Message;
                }

                if (i == 0)
                    LastCacheClearCMS = cacheResult;
                else if (i == 1)
                    LastCacheClearWebservice = cacheResult;
                else if (i == 2)
                    LastCacheClearCometServer = cacheResult;
                else if (i == 3)
                    LastCacheClearMobileNOC = cacheResult;
            }
        }
        finally
        {
            this.CacheStateRefreshing = false;
        }
    }

    private void RenderCacheManagement()
    {
        if (!this.CacheStateRefreshing)
        {
            Task.Factory.StartNew(GetLastCacheClearStatus);
        }

        this.lblCacheLastClearCMS.Text = LastCacheClearCMS;
        this.lblCacheLastClearWebservice.Text = LastCacheClearWebservice;
        this.lblCacheLastClearCometServer.Text = LastCacheClearCometServer;
        this.lblCacheLastClearNOC.Text = LastCacheClearMobileNOC;
    }

    private void GetApplicationPoolStatus()
    {
        if (this.ApplicationPoolStateRefreshing)
            return;

        this.ApplicationPoolStateRefreshing = true;

        try
        {
            for (int i = 0; i < 4; i++)
            {
                string url;
                if (i == 0)
                    url = WebEnvironmentHelper.GetManagementUrl("ClearCache.aspx?appstarted=");
                else if (i == 1)
                    url = WebEnvironmentHelper.GetApiUrl("ClearCache.aspx?appstarted=");
                else if (i == 2)
                    url = WebEnvironmentHelper.GetMessagingUrl("ClearCache.aspx?appstarted=");
                else if (i == 3)
                    url = WebEnvironmentHelper.GetNocUrl("ClearCache.aspx?appstarted=");
                else
                    continue;

                var myHttpWebRequest = (HttpWebRequest)WebRequest.Create(new Uri(url));
                myHttpWebRequest.Timeout = 5000;
                string cacheResult = "Could not be determined";
                try
                {
                    var result = myHttpWebRequest.GetResponse().GetResponseStream();
                    if (result != null)
                    {
                        var stream = new StreamReader(result);
                        var buffer = new char[myHttpWebRequest.GetResponse().ContentLength];
                        stream.Read(buffer, 0, buffer.Length);

                        cacheResult = new string(buffer);
                    }
                }
                catch (Exception ex)
                {
                    if (TestUtil.IsPcDeveloper)
                        cacheResult = ex.Message;
                }

                if (i == 0)
                    ApplicationPoolStateCMS = cacheResult;
                else if (i == 1)
                    ApplicationPoolStateWebservice = cacheResult;
                else if (i == 2)
                    ApplicationPoolStateCometServer = cacheResult;
                else if (i == 3)
                    ApplicationPoolStateMobileNOC = cacheResult;
            }
        }
        finally
        {
            this.ApplicationPoolStateRefreshing = false;
        }
    }

    private void RenderApplicationPoolManagement()
    {
        if (!this.ApplicationPoolStateRefreshing)
        {
            Task.Factory.StartNew(GetApplicationPoolStatus);
        }

        this.lblApplicationPoolStateCMS.Text = ApplicationPoolStateCMS;
        this.lblApplicationPoolStateWebservice.Text = ApplicationPoolStateWebservice;
        this.lblApplicationPoolStateCometServer.Text = ApplicationPoolStateCometServer;
        this.lblApplicationPoolStateMobileNOC.Text = ApplicationPoolStateMobileNOC;
    }

    private void GetDatabaseStatus()
    {
        if(this.DatbaseStateRefreshing)
            return;

        this.DatbaseStateRefreshing = true;

        try
        {
            string databasename = WebEnvironmentHelper.GetDatabaseName();
            if (WebEnvironmentHelper.CloudEnvironment == CloudEnvironment.ProductionPrimary ||
                WebEnvironmentHelper.CloudEnvironment == CloudEnvironment.ProductionSecondary)
            {
                this.PrimaryDatabaseOnline = DatabaseHelper.GetDatabaseStatus(this.productionPrimaryConnectionString, databasename, true);
                this.SecondaryDatabaseOnline = DatabaseHelper.GetDatabaseStatus(this.productionSecondaryConnectionString, databasename, true);
            }
            else
            {
                this.PrimaryDatabaseOnline = DatabaseHelper.GetDatabaseStatus(Obymobi.Data.DaoClasses.CommonDaoBase.ActualConnectionString, true);
            }

            DatabaseConnectionInformation dci;
            List<string> urls = WebEnvironmentHelper.GetNocServiceConnectionStringTxtUrls();
            if (!DatabaseConnectionInformation.TryRetrieveFromNocService(urls, out dci))
                this.DatabaseConnectionInformation = null;
            else
                this.DatabaseConnectionInformation = dci;

            this.LastDbRefresh = DateTime.UtcNow;
        }
        finally
        {
            this.DatbaseStateRefreshing = false;
        }
    }

    private void RenderDatabaseManagement()
    {        
        if (!this.DatbaseStateRefreshing)
        {
            Thread t = new Thread(this.GetDatabaseStatus);
            t.Start();
        }

        this.lblLastDatabaseRefresh.Text = "{0} ({1})".FormatSafe(this.LastDbRefresh, CraveMobileNocHelper.GetSpanTextBetweenDateTimeAndUtcNow(this.LastDbRefresh));

        if (WebEnvironmentHelper.CloudEnvironment == CloudEnvironment.ProductionPrimary ||
            WebEnvironmentHelper.CloudEnvironment == CloudEnvironment.ProductionSecondary)
        {            
            // Check Database Statuses            
            string primaryDatabaseStatus = !this.PrimaryDatabaseOnline.IsNullOrWhiteSpace() ? this.PrimaryDatabaseOnline : Resources.strings.Refreshing;
            string secondaryDatabaseStatus = !this.SecondaryDatabaseOnline.IsNullOrWhiteSpace() ? this.SecondaryDatabaseOnline : Resources.strings.Refreshing;

            this.lblPrimaryDatabaseStatus.Text = primaryDatabaseStatus;
            this.lblSecondaryDatabaseStatus.Text = secondaryDatabaseStatus;

            SqlConnectionStringBuilder sb = new SqlConnectionStringBuilder(Obymobi.Data.DaoClasses.CommonDaoBase.ActualConnectionString);
            this.lblActiveDatabaseServer.Text = sb.DataSource;

            if (this.DatabaseConnectionInformation == null)
            {
                this.lblBroadcastedServer.Text = "COULD NOT BE DETERMINED";
            }
            else
            {
                sb = new SqlConnectionStringBuilder(this.DatabaseConnectionInformation.ConnectionString);
                this.lblBroadcastedServer.Text = sb.DataSource;
            }

            // Determine which Switch links we can show
            // Switch to Secondary (Primary is ONLINE, Secondary is Mirroring)
            this.hlDatabaseSwitchToSecondary.Visible = primaryDatabaseStatus.Equals("ONLINE") && secondaryDatabaseStatus.Equals("RESTORING (MIRRORING)");
            this.hlDatabaseSwitchToPrimary.Visible = primaryDatabaseStatus.Equals("RESTORING (MIRRORING)") && secondaryDatabaseStatus.Equals("ONLINE");
            this.hlDatabaseBreakToPrimary.Visible = primaryDatabaseStatus.Equals("ONLINE") || primaryDatabaseStatus.Equals("RESTORING (MIRRORING)") || primaryDatabaseStatus.Equals("RECOVERING");
            this.hlDatabaseBreakToSecondary.Visible = secondaryDatabaseStatus.Equals("ONLINE") || secondaryDatabaseStatus.Equals("RESTORING (MIRRORING)") || secondaryDatabaseStatus.Equals("RECOVERING");
            
            this.plhDatabaseManagementSwitching.Visible = this.hlDatabaseSwitchToSecondary.Visible | this.hlDatabaseSwitchToPrimary.Visible | this.hlDatabaseBreakToPrimary.Visible | this.hlDatabaseBreakToSecondary.Visible;

            this.hlDatabaseResetConnectionString.Visible = false;
        }
        else
        {

            SqlConnectionStringBuilder sb = new SqlConnectionStringBuilder(Obymobi.Data.DaoClasses.CommonDaoBase.ActualConnectionString);
            this.lblActiveDatabaseServer.Text = sb.DataSource;
            this.lblPrimaryDatabaseStatus.Text = !this.PrimaryDatabaseOnline.IsNullOrWhiteSpace() ? this.PrimaryDatabaseOnline : Resources.strings.Refreshing;

            if (this.DatabaseConnectionInformation == null)
            {
                this.lblBroadcastedServer.Text = "COULD NOT BE DETERMINED";
            }
            else
            {
                sb = new SqlConnectionStringBuilder(this.DatabaseConnectionInformation.ConnectionString);
                this.lblBroadcastedServer.Text = sb.DataSource;
            }          

            // Get Database Status
            this.plhSecondaryDatabaseStatus.Visible = false;
            this.plhDatabaseManagementSwitching.Visible = true;
            this.hlDatabaseBreakToPrimary.Visible = false;
            this.hlDatabaseBreakToSecondary.Visible = false;
            this.hlDatabaseSwitchToPrimary.Visible = false;
            this.hlDatabaseSwitchToSecondary.Visible = false;
            this.hlDatabaseResetConnectionString.Visible = true;
        }

        // Check the Database Statuses
        

        //if (dci != null)
        //{
        //    if (report.Environment == (int)CloudEnvironment.ProductionPrimary)
        //    {
        //        this.lblCloudCurrentServer.Text = "Primary";
        //        this.hlSwitchToPrimaryCloudEnvironment.Visible = false;
        //    }
        //    else if (report.Environment == (int)CloudEnvironment.ProductionSecondary)
        //    {
        //        this.lblCloudCurrentServer.Text = "Secondary";
        //        this.hlSwitchToSecondaryCloudEnvironment.Visible = false;
        //    }
        //    else if (report.Environment != (int)CloudEnvironment.ProductionOverwrite)
        //    {
        //        // No switching when not in Production
        //        this.hlSwitchToPrimaryCloudEnvironment.Visible = false;
        //        this.hlSwitchToSecondaryCloudEnvironment.Visible = false;
        //        this.lblCloudCurrentServer.Text = WebEnvironmentHelper.CloudEnvironment.ToString();
        //        this.lblCloudSwitchingOnlyOnProduction.Visible = true;
        //    }
        //    this.plhCloudManagementUnavailable.Visible = false;
        //}
        //else
        //{
        //    this.plhCloudManagementSwitching.Visible = false;
        //    this.plhCloudManagementUnavailable.Visible = true;
        //}
    }

    private void RenderSetCometHandlers()
    {
        this.cblCometHandlers.DataSource = Enum.GetNames(typeof (ClientCommunicationMethod))
                                               .Where(p => p != ClientCommunicationMethod.All.ToString() && p != ClientCommunicationMethod.None.ToString() && p != ClientCommunicationMethod.Webservice.ToString())
                                               .ToArray();        
        this.cblCometHandlers.DataBind();
        if (this.NocStatusReport != null)
        {
            if (this.NocStatusReport.ClientCommunication == (int)ClientCommunicationMethod.All)
                this.cblCometHandlers.SelectAll();
            else
            {
                var cometHandlers = this.NocStatusReport.ClientCommunication.ToEnum<ClientCommunicationMethod>().ToString().Replace(", ", ",");
                this.cblCometHandlers.SelectMultipleItems(cometHandlers, ',');
            }
        }
        else
            this.cblCometHandlers.SelectAll();
    }

    #endregion

    #region Switching logic 

    private void UpdateConnectionStringManually()
    {
        string connectionstring;
        StringBuilder switchReport = new StringBuilder();
        if (QueryStringHelper.TryGetValue("updateConnectionString", out connectionstring))
        {
            string databaseStatus = DatabaseHelper.GetDatabaseStatus(connectionstring, false);            

            if (databaseStatus == "ONLINE")
            {
                switchReport.AppendFormatLine("Update to: {0}", connectionstring);
                var dciAfter = new DatabaseConnectionInformation();
                dciAfter.ConnectionString = connectionstring;
                dciAfter.Age = TimeSpan.FromSeconds(1);
                dciAfter.Database = 1;
            }
            else
            {
                switchReport.AppendFormatLine("Not switched, database not online: {0}", databaseStatus);
            }
        }

        this.plhCloudManagement.Visible = false;
        this.plhDatabaseManagement.Visible = false;
        this.plhDatabaseSwitchResultReport.AddHtml("<h1>Manual ConnectionString Update</h1>");
        this.plhDatabaseSwitchResultReport.AddHtml("<p>" + switchReport.ToString().ReplaceLineBreakWithBR() + "</p>");
        this.plhConfirmCloudEnvironmentSwitch.Visible = false;
        this.plhDatabaseSwitchResult.Visible = true;         
    }

    private string GetUserIp()    
    {
        if (HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"].IsNullOrWhiteSpace())
            return HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
        else if (HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"].IsNullOrWhiteSpace())
            return HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
        else if (HttpContext.Current.Request.UserHostAddress.IsNullOrWhiteSpace())
            return HttpContext.Current.Request.UserHostAddress;
        else
            return "UNKNOWN-IP";
    }

    #endregion

    #region Event Handlers

    void btnSwitchCometHandlers_Click(object sender, EventArgs e)
    {
        var newCommunicationMethods = ClientCommunicationMethod.Webservice;
        var selectedValues = this.cblCometHandlers.SelectedValues(",");
        if (selectedValues != string.Empty)
        {
            foreach (string value in selectedValues.Split(','))
            {
                newCommunicationMethods |= value.ToEnum<ClientCommunicationMethod>();
            }
        }
    }

    #endregion

    #region Properties

    private NocStatusReport NocStatusReport
    {
        get
        {
            return this.Application[applicationKeyNocStatusReport] as NocStatusReport;
        }
        set
        {
            this.Application[applicationKeyNocStatusReport] = value;
        }
    }

    private DatabaseConnectionInformation DatabaseConnectionInformation
    {
        get
        {
            return this.Application[applicationKeyDatabaseConnectionInformation] as DatabaseConnectionInformation;
        }
        set
        {
            this.Application[applicationKeyDatabaseConnectionInformation] = value;
        }
    }

    private bool DatbaseStateRefreshing
    {
        get
        {
            if (this.Application[applicationKeyDatabaseStateRefreshing] == null)
                this.Application[applicationKeyDatabaseStateRefreshing] = false;

            return (bool)this.Application[applicationKeyDatabaseStateRefreshing];
        }
        set
        {
            this.Application[applicationKeyDatabaseStateRefreshing] = value;
        }
    }

    private bool CacheStateRefreshing
    {
        get
        {
            if (this.Application[applicationKeyCacheStateRefreshing] == null)
                this.Application[applicationKeyCacheStateRefreshing] = false;

            return (bool) this.Application[applicationKeyCacheStateRefreshing];
        }
        set { this.Application[applicationKeyCacheStateRefreshing] = value; }
    }

    private bool ApplicationPoolStateRefreshing
    {
        get
        {
            if (this.Application[applicationKeyApplicationPoolStateRefreshing] == null)
                this.Application[applicationKeyApplicationPoolStateRefreshing] = false;

            return (bool)this.Application[applicationKeyApplicationPoolStateRefreshing];
        }
        set { this.Application[applicationKeyApplicationPoolStateRefreshing] = value; }
    }

    private string PrimaryDatabaseOnline
    {
        get
        {
            if (this.Application[applicationKeyPrimaryDatabaseOnline] == null)
                return null;
            else
                return (string)this.Application[applicationKeyPrimaryDatabaseOnline];
        }
        set
        {
            this.Application[applicationKeyPrimaryDatabaseOnline] = value;
        }
    }

    private string SecondaryDatabaseOnline
    {
        get
        {
            if (this.Application[applicationKeySecondaryDatabaseOnline] == null)
                return null;
            else
                return (string)this.Application[applicationKeySecondaryDatabaseOnline];
        }
        set
        {
            this.Application[applicationKeySecondaryDatabaseOnline] = value;
        }
    }

    private bool WebserviceStateRefreshing
    {
        get
        {
            if (this.Application[applicationKeyWebserviceStateRefreshing] == null)
                this.Application[applicationKeyWebserviceStateRefreshing] = false;

            return (bool)this.Application[applicationKeyWebserviceStateRefreshing];
        }
        set
        {
            this.Application[applicationKeyWebserviceStateRefreshing] = value;
        }
    }

    private bool? PrimaryWebserviceOnline
    {
        get
        {
            if (this.Application[applicationKeyPrimaryWebserviceOnline] == null)
                return null;
            else
                return (bool)this.Application[applicationKeyPrimaryWebserviceOnline];
        }
        set
        {
            this.Application[applicationKeyPrimaryWebserviceOnline] = value;
        }
    }

    private bool? SecondaryWebserviceOnline
    {
        get
        {
            if (this.Application[applicationKeySecondaryWebserviceOnline] == null)
                return null;
            else
                return (bool)this.Application[applicationKeySecondaryWebserviceOnline];
        }
        set
        {
            this.Application[applicationKeySecondaryWebserviceOnline] = value;
        }
    }

    private DateTime LastCloudRefresh
    {
        get
        {
            if (this.Application[applicationKeyLastCloudRefresh] == null)
                this.Application[applicationKeyLastCloudRefresh] = DateTime.MinValue;

            return (DateTime)this.Application[applicationKeyLastCloudRefresh];
        }
        set
        {
            this.Application[applicationKeyLastCloudRefresh] = value;
        }
    }

    private DateTime LastDbRefresh
    {
        get
        {
            if (this.Application[applicationKeyLastDbRefresh] == null)
                this.Application[applicationKeyLastDbRefresh] = DateTime.MinValue;

            return (DateTime)this.Application[applicationKeyLastDbRefresh];
        }
        set
        {
            this.Application[applicationKeyLastDbRefresh] = value;
        }
    }

    private string LastCacheClearCMS
    {
        get
        {
            if (this.Application[applicationKeyLastCacheClearCMS] == null)
                return "Refreshing";
            
            return (string)this.Application[applicationKeyLastCacheClearCMS];
        }
        set
        {
            this.Application[applicationKeyLastCacheClearCMS] = value;
        }
    }

    private string LastCacheClearWebservice
    {
        get
        {
            if (this.Application[applicationKeyLastCacheClearWebservice] == null)
                return "Refreshing";
            
            return (string)this.Application[applicationKeyLastCacheClearWebservice];
        }
        set
        {
            this.Application[applicationKeyLastCacheClearWebservice] = value;
        }
    }

    private string LastCacheClearCometServer
    {
        get
        {
            if (this.Application[applicationKeyLastCacheClearCometServer] == null)
                return "Refreshing";

            return (string)this.Application[applicationKeyLastCacheClearCometServer];
        }
        set
        {
            this.Application[applicationKeyLastCacheClearCometServer] = value;
        }
    }

    private string LastCacheClearMobileNOC
    {
        get
        {
            if (this.Application[applicationKeyLastCacheClearNOC] == null)
                return "Refreshing";

            return (string)this.Application[applicationKeyLastCacheClearNOC];
        }
        set
        {
            this.Application[applicationKeyLastCacheClearNOC] = value;
        }
    }

    private string ApplicationPoolStateCMS
    {
        get
        {
            if (this.Application[applicationKeyApplicationPoolStateCMS] == null)
                return "Refreshing";

            return (string)this.Application[applicationKeyApplicationPoolStateCMS];
        }
        set
        {
            this.Application[applicationKeyApplicationPoolStateCMS] = value;
        }
    }

    private string ApplicationPoolStateWebservice
    {
        get
        {
            if (this.Application[applicationKeyApplicationPoolStateWebservice] == null)
                return "Refreshing";

            return (string)this.Application[applicationKeyApplicationPoolStateWebservice];
        }
        set
        {
            this.Application[applicationKeyApplicationPoolStateWebservice] = value;
        }
    }

    private string ApplicationPoolStateCometServer
    {
        get
        {
            if (this.Application[applicationKeyApplicationPoolStateCometServer] == null)
                return "Refreshing";

            return (string)this.Application[applicationKeyApplicationPoolStateCometServer];
        }
        set
        {
            this.Application[applicationKeyApplicationPoolStateCometServer] = value;
        }
    }

    private string ApplicationPoolStateMobileNOC
    {
        get
        {
            if (this.Application[applicationKeyApplicationPoolStateNOC] == null)
                return "Refreshing";

            return (string)this.Application[applicationKeyApplicationPoolStateNOC];
        }
        set
        {
            this.Application[applicationKeyApplicationPoolStateNOC] = value;
        }
    }

    #endregion

    #region Old Temp Stuff

    // GK TEMP FUNCTION
    public class Updater
    {
        public DateTime runningSince = DateTime.Now;
        public int companyId;
        public Dictionary<int, DateTime> restartTimes = new Dictionary<int, DateTime>();
        public List<int> restartedClients = new List<int>();
        public bool keepLooping = true;
        public StringBuilder loopLog = new StringBuilder();

        public void Start(int companyId)
        {
            this.companyId = companyId;

            while (true)
            {
                this.loopLog = new StringBuilder();

                RelationCollection relation = new RelationCollection();
                relation.Add(ClientEntityBase.Relations.DeviceEntityUsingDeviceId);

                // Get all Clients
                ClientCollection clients = new ClientCollection();
                ClientLogCollection clientLogs = new ClientLogCollection();
                PredicateExpression filter = new PredicateExpression();
                filter.Add(ClientFields.CompanyId == companyId);
                SortExpression sort = new SortExpression(DeviceFields.LastRequestUTC | SortOperator.Descending);
                clients.GetMulti(filter, 0, sort, relation);

                // Check how many are 'possbily' restarting
                int restarting = clients.Count(x => x.OperationMode == 400);

                this.loopLog.AppendFormat("Started: {0}<br />", DateTime.Now);
                this.loopLog.AppendFormat("Restarting at the moment: {0}<br />", restarting);

                if (restarting < 5)
                {
                    // Pick new Clients to restart
                    int toRestart = 5 - restarting;
                    for (int i = 0; i < clients.Count; i++)
                    {
                        var client = clients[i];
                        if (client.OperationMode != 400 && !this.restartTimes.ContainsKey(client.ClientId))
                        {
                            filter = new PredicateExpression();
                            filter.Add(ClientLogFields.ClientId == client.ClientId);
                            filter.Add(ClientLogFields.CreatedUTC > DateTime.UtcNow.AddMinutes(-30));
                            filter.Add(ClientLogFields.Message % "%Client started%");

                            if (clientLogs.GetDbCount(filter) <= 0)
                            {
                                this.loopLog.AppendFormat("Set Client to Restart: {0}<br />", client.ClientId);
                                client.OperationMode = 400;
                                client.Save();
                                this.restartTimes.Add(client.ClientId, DateTime.Now);
                                toRestart--;
                            }
                            else
                                this.loopLog.AppendFormat("Client not restarted because restarted less than 5 minnutes ago: {0}<br />", client.ClientId);
                        }

                        if (toRestart <= 0)
                            break;
                    }
                }

                // Check for the ones that have restart if they have restarted
                foreach (KeyValuePair<int, DateTime> restarteDevice in this.restartTimes)
                {
                    filter = new PredicateExpression();
                    filter.Add(ClientLogFields.ClientId == restarteDevice.Key);
                    filter.Add(ClientLogFields.CreatedUTC > restarteDevice.Value);
                    filter.Add(ClientLogFields.Message % "%Client started%");

                    if (clientLogs.GetDbCount(filter) > 0)
                    {
                        var client = clients.FirstOrDefault(x => x.ClientId == restarteDevice.Key);
                        if (client != null)
                        {
                            if (!this.restartedClients.Contains(client.ClientId))
                            {
                                this.loopLog.AppendFormat("Restart Cliented Done: {0}<br />", client.ClientId);
                                this.restartedClients.Add(client.ClientId);
                            }

                            if (client.OperationMode != 1)
                            {
                                client.OperationMode = 1;
                                client.Save();
                            }
                        }
                    }
                }

                string requestedToRestart = "";
                foreach (KeyValuePair<int, DateTime> restarteDevice in this.restartTimes.OrderBy(x => x.Key))
                {
                    requestedToRestart += restarteDevice.Key + ", ";
                }

                string restartCompleted = "";
                this.restartedClients.Sort();
                foreach (int restartedClientId in this.restartedClients)
                {
                    restartCompleted += restartedClientId + ", ";
                }

                this.loopLog.AppendFormat("Requested to restart: {0}<br />", requestedToRestart);
                this.loopLog.AppendFormat("Restart completed: {0}<br />", restartCompleted);

                this.loopLog.AppendFormat("Finished iteration: {0}<br />", DateTime.Now);
                this.loopLog.AppendFormat("Keep looping: {0} <br />", this.keepLooping);

                // Wait 5 seconds for next iteration
                Thread.Sleep(5000);

                if (!this.keepLooping)
                    break;

                if ((DateTime.Now - this.runningSince).TotalMinutes > 60)
                {
                    this.keepLooping = false;

                    // Update all Clients to Operation Mode 1
                    clients = new ClientCollection();
                    clients.GetMulti(new PredicateExpression(ClientFields.CompanyId == this.companyId));
                    foreach (var client in clients)
                    {
                        client.OperationMode = 1;
                        client.Save();
                    }

                    this.loopLog.AppendFormat("Stopped running after 1 hour");
                }
            }
        }
    }

    void DoUpdaters()
    {
        int autoUpdateCompany;

        try
        {
            if (this.Application["Updater"] != null)
            {
                ((Updater)this.Application["Updater"]).keepLooping = ((Updater)this.Application["Updater"]).keepLooping;
            }
        }
        catch
        {
            this.Application["Updater"] = null;
        }

        if (QueryStringHelper.TryGetValue("autoupdate", out autoUpdateCompany))
        {
            if (autoUpdateCompany == 1000)
            {
                if (this.Application["Updater"] != null)
                {
                    // Update all Clients to Operation Mode 1
                    ClientCollection clients = new ClientCollection();
                    clients.GetMulti(new PredicateExpression(ClientFields.CompanyId == ((Updater)this.Application["Updater"]).companyId));
                    foreach (var client in clients)
                    {
                        client.OperationMode = 1;
                        client.Save();
                    }

                    ((Updater)this.Application["Updater"]).keepLooping = false;
                    ((Updater)this.Application["Updater"]).loopLog.AppendFormat("Stopped by User");
                }
            }
            else if (this.Application["Updater"] == null || !((Updater)this.Application["Updater"]).keepLooping)
            {
                var updater = new Updater();
                this.Application["Updater"] = updater;
                Task.Factory.StartNew(() => updater.Start(autoUpdateCompany));
            }
            else
                throw new Exception("Updater already running, to stop do autoupdate = 1000");
        }

        if (this.Application["Updater"] != null)
        {
            this.plhStaggeredOutput.AddHtml(((Updater)this.Application["Updater"]).loopLog.ToString());
        }
    }

    #endregion

}