﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using Dionysos.Web;
using Dionysos.Web.UI;
using Google.Apis.Manual.Util;
using Obymobi.Data.EntityClasses;
using Obymobi.Logic.Routing;
using Obymobi.Logic.Status;
using Obymobi.Noc.Logic.HelperClasses;

public partial class MasterPages_CraveMobileNocBase : MasterPage
{
    #region Constructors

    public MasterPages_CraveMobileNocBase()
    {
        this.GoogleAnalyticsToken = WebConfigurationManager.AppSettings[nameof(GoogleAnalyticsToken)];
    }

    #endregion

    #region Properties

    public int UserId = NocHelper.CurrentUser.UserId;

    public string GoogleAnalyticsToken { get; }

    protected bool EmbeddedMode { get; private set; } = false;

    #endregion

    #region Event handlers

    protected void Page_Load(object sender, EventArgs e)
    {
        // Hookup the events
        this.lbSignOut.Click += (lbSignOut_Click);

        // Add the CSS and JS
        LinkedCssFiles.Add("~/Css/base.css", 0);
        LinkedJsFiles.Add("~/js/jquery-1.8.3.min.js", 0);
        LinkedJsFiles.Add("~/js/config.js", 0);
        LinkedJsFiles.Add("~/js/popup.js", 1);

        // Render the logo
        this.plhLogo.AddHtml("<a id=\"logo\" class=\"logo\" href=\"{0}\"></a>", ResolveUrl("~"));  

        // Set the issue count
        int issueCount = RefreshIssueCount(CraveMobileNocHelper.AvailableCompanyIds);

        this.hlIssues.CssClass = (issueCount == 0 ? "no-issues" : "issues");
        if (issueCount == -1)
            this.lblIssueCount.Text = "RFRSH";
        else if (issueCount == -2)
            this.lblIssueCount.Text = "NO DB";
        else
            this.lblIssueCount.Text = issueCount.ToString();        

        // Set the link of the refresh button
        this.hlRefresh.NavigateUrl = this.Request.RawUrl;

        // Check for manually processed orders
        CheckForManuallyProcessedOrders();
        CheckCompanyIdPermission();

        // Check if session or query parameter for Embedded Mode is present.
        CheckEmbeddedMode();

    }

    private int RefreshIssueCount(List<int> companyIds)
    {
        if (ConnectionStringPoller.Instance.IsDatabaseAvailable)
        {
            try
            {
                return NocHelper.GetIssueCount(companyIds);
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
            }
        }

        return -2;
    }


    private void lbSignOut_Click(object sender, EventArgs e)
    {
        Response.Redirect(ResolveUrl("~/SignOn.aspx?SignOut=True"));
    }

    private void CheckForManuallyProcessedOrders()
    {
        if (QueryStringHelper.HasValue("ProcessOrderId"))
        {
            int orderId = QueryStringHelper.GetInt("ProcessOrderId");
            if (orderId > 0)
            {
                OrderEntity order = new OrderEntity(orderId);
                if (!order.IsNew)
                {
                    // Check if order belongs to company we can view.
                    if (CraveMobileNocHelper.CheckAuthorization(order.CompanyId))
                    {
                        // Process the order
                        RoutingHelper.UpdateOrderStatus(order, Obymobi.Enums.OrderStatus.Processed, false);

                        // Refresh page, so the change is reflected directly                        
                        QueryStringHelper qs = new QueryStringHelper(this.Request.QueryString);
                        qs.RemoveItem("ProcessOrderId");
                        Response.Redirect(this.Request.Url.AbsolutePath + qs.Value);
                    }
                }
            }
        }
    }

    private void CheckCompanyIdPermission()
    {
        if (QueryStringHelper.HasValue("cid"))
        {
            int companyId = QueryStringHelper.GetInt("cid");
            CraveMobileNocHelper.CheckAuthorization(companyId);
        }
    }

    private void CheckEmbeddedMode()
    {
        if (QueryStringHelper.HasValue("embeddedMode"))
        {
            bool embeddedMode = QueryStringHelper.GetBool("embeddedMode");
            Session["embeddedMode"] = embeddedMode.ToString().ToLower();
            this.EmbeddedMode = embeddedMode;
        }
        else
        {
            var sessionKey = Session["embeddedMode"];
            if (sessionKey != null)
            {
                this.EmbeddedMode = (string)sessionKey == "true";
            }
            else
            {
                Session["embeddedMode"] = "false";
            }
        }

    }
    
    protected override bool CssExplicitlyMediaScreenOnly
    {
        get
        {
            if (ConnectionStringPoller.Instance.IsDatabaseAvailable)
                return base.CssExplicitlyMediaScreenOnly;
            
            return false;
        }
    }

    protected override bool CssJsCombining
    {
        get
        {
            if (ConnectionStringPoller.Instance.IsDatabaseAvailable)
                return base.CssJsCombining;
            
            return false;
        }
    }

    protected override string GetGoogleAnalyticsTrackerId()
    {
        if (ConnectionStringPoller.Instance.IsDatabaseAvailable)
            return base.GetGoogleAnalyticsTrackerId();
        
        return string.Empty;
    }

    #endregion

    #region Methods


    #endregion
}
