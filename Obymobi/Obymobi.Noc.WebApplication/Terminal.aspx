﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/CraveMobileNocBase.master" AutoEventWireup="true" Inherits="Terminal" Codebehind="Terminal.aspx.cs" %>
<%@ Reference VirtualPath="~/UserControls/Listitems/Terminal.ascx" %>
<%@ Reference VirtualPath="~/UserControls/Listitems/DeviceActivityLog.ascx" %>
<%@ Register Src="~/UserControls/Paging/LogSortingPaging.ascx" TagPrefix="uc" TagName="LogSortingPaging" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<div class="paging">
    <ul class="list">
        <D:PlaceHolder runat="server" ID="plhDetails"></D:PlaceHolder>
    </ul>

    <h1><D:LabelTextOnly ID="lblFunctions" runat="server" Text="<%$ Resources:strings, Functions %>"></D:LabelTextOnly></h1>
    <ul class="buttonlist">    
        <li><D:HyperLink ID="hlOpenInCms" runat="server" Target="_blank" Text="<%$ Resources:strings, Generic_open_in_cms %>"></D:HyperLink></li>
    </ul>
    
    <D:PlaceHolder ID="plhTerminalOrders" runat="server" Visible="false">
        <h1><D:LabelTextOnly ID="lblTerminalOrders" runat="server" Text="<%$ Resources:strings, Terminal_orders %>"></D:LabelTextOnly></h1>
        <ul class="list">
            <D:PlaceHolder ID="plhTerminalOrdersList" runat="server"></D:PlaceHolder>
        </ul> 
    </D:PlaceHolder>

    <D:PlaceHolder ID="plhTerminalLogs" runat="server">
        <h1><D:LabelTextOnly ID="lblTerminalsLogs" runat="server" Text="<%$ Resources:strings, Terminal_logs %>"></D:LabelTextOnly></h1>
        <h1 class="filter-text"><D:LinkButton runat="server" Text="<%$ Resources:strings, Apply_filter %>" OnClientClick="showPopup('pnlLogFilter'); return false;"></D:LinkButton></h1>
        
        <uc:LogSortingPaging runat="server" id="ucLogSortingPagingHeader" CssClass="sorting-paging" />
        <ul class="list">
            <D:PlaceHolder ID="plhTerminalLogsList" runat="server"></D:PlaceHolder>
        </ul> 
        <uc:LogSortingPaging runat="server" id="ucLogSortingPagingFooter" CssClass="sorting-paging" />
    </D:PlaceHolder>

    <div id="pnlLogFilter" class="log-filter">
        <h1><D:LabelTextOnly runat="server" Text="Filter logs"></D:LabelTextOnly></h1>

        <D:DropDownList ID="ddlTimespan" runat="server" CssClass="log-timespan">
            <asp:ListItem Text="<%$ Resources:strings, Last_hour %>" Value="1"></asp:ListItem>
            <asp:ListItem Text="<%$ Resources:strings, Last_6_hours %>" Value="6"></asp:ListItem>
            <asp:ListItem Text="<%$ Resources:strings, Last_24_hours %>" Value="24"></asp:ListItem>
            <asp:ListItem Text="<%$ Resources:strings, Last_week %>" Value="168" Selected="True"></asp:ListItem>
            <asp:ListItem Text="<%$ Resources:strings, Last_month %>" Value="672"></asp:ListItem>
            <asp:ListItem Text="<%$ Resources:strings, Last_3_months %>" Value="2016"></asp:ListItem>
            <asp:ListItem Text="<%$ Resources:strings, All_results %>" Value="99999"></asp:ListItem>
        </D:DropDownList>

        <div class="log-type">
            <D:CheckBox ID="cbTerminalLogs" Checked="true" CssClass="checkbox" runat="server" Text="Terminal logs" />
            <D:CheckBox ID="cbNetmessages" CssClass="checkbox" runat="server" Text="Netmessages" />
            <D:CheckBox ID="cbTerminalStates" Checked="true" CssClass="checkbox" runat="server" Text="Terminal states" />
        </div>

        <ul class="buttonlist">
            <li><D:LinkButton ID="btnFilter" runat="server" Text="<%$ Resources:strings, Apply_filter %>" /></li>
            <li><D:LinkButton ID="btnClose" runat="server" Text="<%$ Resources:strings, Close %>" OnClientClick="hidePopup('pnlLogFilter'); return false;" /></li>
        </ul>
    </div>
</div>
</asp:Content>

