﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/CraveMobileNocBase.master" AutoEventWireup="true" Inherits="OrderRoutestephandlers" Codebehind="OrderRoutestephandlers.aspx.cs" %>
<%@ Reference VirtualPath="~/UserControls/Listitems/OrderRoutestephandler.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<div id="settings">
    <div id="filter">
        <D:DropDownList ID="ddlFilter" runat="server" AutoPostBack="true">
            <asp:ListItem Text="<%$ Resources:strings, OrderRoutestephandler_all_route_steps %>"></asp:ListItem>
            <asp:ListItem Text="<%$ Resources:strings, OrderRoutestephandler_processed_route_steps %>"></asp:ListItem>
            <asp:ListItem Text="<%$ Resources:strings, OrderRoutestephandler_unprocessed_route_steps %>"></asp:ListItem>
        </D:DropDownList>
    </div>
    <div id="title"><D:LabelTextOnly ID="lblTitle" runat="server"></D:LabelTextOnly></div>
    <div id="sort">
        <D:DropDownList ID="ddlSort" runat="server" AutoPostBack="true">
            <asp:ListItem Text="<%$ Resources:strings, OrderRoutestephandler_step_number_asc %>"></asp:ListItem>
            <asp:ListItem Text="<%$ Resources:strings, OrderRoutestephandler_status_asc %>"></asp:ListItem>
        </D:DropDownList>
    </div>
</div>
<ul class="list">
    <D:PlaceHolder ID="plhList" runat="server"></D:PlaceHolder>
</ul>    
</asp:Content>

