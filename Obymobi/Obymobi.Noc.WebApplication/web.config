﻿<?xml version="1.0" encoding="utf-8"?>
<configuration>
	<!-- DECLARATION OF CUSTOM CONFIGURATION  SECTIONS -->
	<configSections>
		<section name="sqlServerCatalogNameOverwrites" type="System.Configuration.NameValueFileSectionHandler, System, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" />
		<section name="dependencyInjectionInformation" type="SD.LLBLGen.Pro.ORMSupportClasses.DependencyInjectionSectionHandler, SD.LLBLGen.Pro.ORMSupportClasses, Version=4.2.0.0, Culture=neutral, PublicKeyToken=ca73b74ba4e3ff27" />
		<sectionGroup name="devExpress">
			<section name="compression" type="DevExpress.Web.CompressionConfigurationSection, DevExpress.Web.v20.1, Version=20.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" requirePermission="false" />
			<section name="themes" type="DevExpress.Web.ThemesConfigurationSection, DevExpress.Web.v20.1, Version=20.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" requirePermission="false" />
			<section name="errors" type="DevExpress.Web.ErrorsConfigurationSection, DevExpress.Web.v20.1, Version=20.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" requirePermission="false" />
			<section name="settings" type="DevExpress.Web.SettingsConfigurationSection, DevExpress.Web.v20.1, Version=20.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" requirePermission="false" />
		</sectionGroup>
	</configSections>
	<!-- DEPENDENCY INJECTION -->
	<dependencyInjectionInformation>
		<additionalAssemblies>
			<assembly fullName="Obymobi.Data.Injectables, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null" />
		</additionalAssemblies>
		<instanceTypeFilters>
			<!-- DIT IS  NIET DE CONNECTIONSTRING.CATALOG OVERWRITE!  -->
			<instanceTypeFilter namespace="Obymobi" />
			<!-- DIT IS NIET DE CONNECTIONSTRING.CATALOG OVERWRITE!  -->
		</instanceTypeFilters>
	</dependencyInjectionInformation>
	<!-- SQL SERVER DATABASE NAME OVERWRITE -->
	<sqlServerCatalogNameOverwrites file="Web.SqlServerCatalogNameOverwrites.config">
		<add key="Obymobi" value="ObymobiTest" />
	</sqlServerCatalogNameOverwrites>
	<!-- APPLICATION/DATABASE SETTINGS -->
	<appSettings file="Web.AppSettings.config">
		<!-- DATABASE -->
		<add key="ObymobiData.ConnectionString" value="data source=localhost;initial catalog=ObymobiDevelopment;integrated security=SSPI;persist security info=False;packet size=4096" />
		<add key="SqlServerDQECompatibilityLevel" value="2" />
		<add key="ORMProfilerEnabled" value="false" />
		<!-- ENVIRONMENT -->
		<add key="BaseUrl" value="http://192.168.58.48/" />
		<add key="CloudEnvironment" value="__CLOUDENVIRONMENT__" />
		<!-- SERVICES -->
		<add key="Crave.Ordering.ApiUrl" value="https://ordering-api.dev.trueguest.tech/" />
		<!-- AWS:CLOUD STORAGE -->
		<add key="AWSAccessKey" value="__AMAZON_S3_ACCESSKEY__" />
		<add key="AWSSecretKey" value="__Amazon.S3.SecretKey__" />
		<!-- KEYS / LICENSES -->
		<add key="SendGridApiKey" value="" />
		<add key="GoogleAnalyticsToken" value="" />
		<!-- Done as Base64 as I needed a Json token and Json doesn't really work within Xml: Quick encoding: https://www.base64encode.org/ -->
		<add key="servicestack:license" value="" />

        <add key="Crave.Obymobi.Cms.Url" value="" />
        <add key="Crave.Obymobi.Noc.Url" value="" />
        <add key="Crave.Obymobi.Messaging.Url" value="" />
        <add key="Crave.Obymobi.Api.Url" value="" />
        <add key="Crave.Obymobi.Services.Url" value="" />

        <!-- NO LONGER USED? -->
		<add key="GoogleApisJsonWebTokenBase64" value="" />
		<add key="CraveService.CraveService" value="http://localhost/2019022500/api/21.0/CraveService.asmx" />
		<add key="MobileService.mobileservice" value="http://localhost/2019022500/api/21.0/mobileservice.asmx" />		
	</appSettings>
	<!-- WEB APPLICATION SETTINGS -->
	<!--
    For a description of web.config changes see http://go.microsoft.com/fwlink/?LinkId=235367.

    The following attributes can be set on the <httpRuntime> tag.
      <system.Web>
        <httpRuntime targetFramework="4.5.2" />
      </system.Web>
  -->
	<system.web>
		<!-- TRUST LEVEL -->
		<!--<trust level="Full" />-->
		<!-- INCLUDE Dionysos.Web.UI.Controls as <D:[ControlName] /> -->
		<pages theme="Glass" validateRequest="true" enableEventValidation="false" viewStateEncryptionMode="Never" enableViewStateMac="false" controlRenderingCompatibilityVersion="3.5" clientIDMode="AutoID" enableSessionState="ReadOnly">
			<controls>
				<add tagPrefix="artem" namespace="Artem.Web.UI.Controls" assembly="Artem.GoogleMap" />
				<add assembly="DevExpress.Web.v20.1" namespace="DevExpress.Web" tagPrefix="DxTab" />
				<add assembly="DevExpress.Web.v20.1" namespace="DevExpress.Web" tagPrefix="dxe" />
				<add assembly="DevExpress.Web.v20.1" namespace="DevExpress.Web" tagPrefix="dxwgv" />
				<add assembly="Dionysos.Web" namespace="Dionysos.Web.UI.WebControls" tagPrefix="D" />
				<add assembly="Dionysos.Web.DevExpress" namespace="Dionysos.Web.UI.DevExControls" tagPrefix="X" />
				<add assembly="Dionysos.Web.DevExpress" namespace="Dionysos.Web.UI.UltraControls" tagPrefix="U" />
				<add src="~/SubPanels/EntityCollectionFilteringPanel.ascx" tagName="EntityCollectionFilteringPanel" tagPrefix="uc" />
				<add src="~/Generic/UserControls/CreatedUpdatedPanel.ascx" tagName="CreatedUpdatedPanel" tagPrefix="uc" />
			</controls>
		</pages>
		<!-- GLOBALIZATION (ENCODING / CULTURE) -->
		<globalization fileEncoding="utf-8" requestEncoding="utf-8" responseEncoding="utf-8" culture="en-GB" uiCulture="en-GB" />
		<!-- COMPILATION (DEBUG/RELEASE) -->
		<compilation debug="true" targetFramework="4.5.2">
			<assemblies>
				<add assembly="DevExpress.Charts.v20.1.Core, Version=20.1.7.0, Culture=neutral, PublicKeyToken=B88D1754D700E49A" />
				<add assembly="DevExpress.Data.v20.1, Version=20.1.7.0, Culture=neutral, PublicKeyToken=B88D1754D700E49A" />
				<add assembly="DevExpress.PivotGrid.v20.1.Core, Version=20.1.7.0, Culture=neutral, PublicKeyToken=B88D1754D700E49A" />
				<add assembly="DevExpress.Printing.v20.1.Core, Version=20.1.7.0, Culture=neutral, PublicKeyToken=B88D1754D700E49A" />
				<add assembly="DevExpress.RichEdit.v20.1.Core, Version=20.1.7.0, Culture=neutral, PublicKeyToken=B88D1754D700E49A" />
				<add assembly="DevExpress.Web.ASPxHtmlEditor.v20.1, Version=20.1.7.0, Culture=neutral, PublicKeyToken=B88D1754D700E49A" />
				<add assembly="DevExpress.Web.ASPxPivotGrid.v20.1, Version=20.1.7.0, Culture=neutral, PublicKeyToken=B88D1754D700E49A" />
				<add assembly="DevExpress.Web.ASPxScheduler.v20.1, Version=20.1.7.0, Culture=neutral, PublicKeyToken=B88D1754D700E49A" />
				<add assembly="DevExpress.Web.ASPxSpellChecker.v20.1, Version=20.1.7.0, Culture=neutral, PublicKeyToken=B88D1754D700E49A" />
				<add assembly="DevExpress.Web.ASPxThemes.v20.1, Version=20.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" />
				<add assembly="DevExpress.Web.ASPxTreeList.v20.1, Version=20.1.7.0, Culture=neutral, PublicKeyToken=B88D1754D700E49A" />
				<add assembly="DevExpress.Web.v20.1, Version=20.1.7.0, Culture=neutral, PublicKeyToken=B88D1754D700E49A" />
				<add assembly="DevExpress.XtraCharts.v20.1, Version=20.1.7.0, Culture=neutral, PublicKeyToken=B88D1754D700E49A" />
				<add assembly="DevExpress.XtraCharts.v20.1.Web, Version=20.1.7.0, Culture=neutral, PublicKeyToken=B88D1754D700E49A" />
				<add assembly="DevExpress.XtraPrinting.v20.1, Version=20.1.7.0, Culture=neutral, PublicKeyToken=B88D1754D700E49A" />
				<add assembly="DevExpress.XtraReports.v20.1, Version=20.1.7.0, Culture=neutral, PublicKeyToken=B88D1754D700E49A" />
				<add assembly="DevExpress.XtraReports.v20.1.Web, Version=20.1.7.0, Culture=neutral, PublicKeyToken=B88D1754D700E49A" />
				<add assembly="DevExpress.XtraReports.v20.1.Web, Version=20.1.7.0, Culture=neutral, PublicKeyToken=B88D1754D700E49A" />
				<add assembly="DevExpress.XtraScheduler.v20.1, Version=20.1.7.0, Culture=neutral, PublicKeyToken=B88D1754D700E49A" />
				<add assembly="DevExpress.XtraScheduler.v20.1.Core, Version=20.1.7.0, Culture=neutral, PublicKeyToken=B88D1754D700E49A" />
				<add assembly="System.Data.Linq, Version=4.0.0.0, Culture=neutral, PublicKeyToken=B77A5C561934E089" />
				<add assembly="System.Net, Version=4.0.0.0, Culture=neutral, PublicKeyToken=B03F5F7F11D50A3A" />
				<add assembly="System.Runtime, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" />
			</assemblies>
			<!-- GK Unnessecary? (Btw, you're not allowed to supply *.Design.dll libraries.
        <add assembly="DevExpress.XtraCharts.v20.1.Design, Version=13.2.5.0, Culture=neutral, PublicKeyToken=B88D1754D700E49A" />
        <add assembly="DevExpress.XtraCharts.v20.1.Web.Design, Version=13.2.5.0, Culture=neutral, PublicKeyToken=B88D1754D700E49A" />
        <add assembly="DevExpress.XtraCharts.v20.1.UI, Version=13.2.5.0, Culture=neutral, PublicKeyToken=B88D1754D700E49A" />
        <add assembly="DevExpress.XtraCharts.v20.1.UI, Version=13.2.5.0, Culture=neutral, PublicKeyToken=B88D1754D700E49A" />
        -->
		</compilation>
		<!-- AUTHORIZATION / AUTHENTICATION -->
		<authorization>
			<!-- DENY ALL PAGES FOR UNAUTHENTICATED USERS -->
			<deny users="?" />
		</authorization>
		<sessionState configSource="Web.SessionState.Config" />
		<authentication configSource="Web.Authentication.Config" />
		<!-- CUSTOM ERRORS -->
		<customErrors mode="RemoteOnly" defaultRedirect="~/500.aspx" redirectMode="ResponseRewrite">
			<error statusCode="401" redirect="~/401.aspx" />
		</customErrors>
		<!-- PARTLY FIX FOR XSS ATTACKS -->
		<httpCookies httpOnlyCookies="true" requireSSL="false" />
		<!-- EXTRA PARAMETERS FOR MAC FAILED PROBLEM  -->
		<httpRuntime targetFramework="4.5" executionTimeout="3600" maxRequestLength="1049000000" requestValidationMode="2.0" />
		<httpHandlers>
			<add verb="*" path="css.axd" validate="false" type="Dionysos.Web.HttpHandlers.CombineCompressHandler, Dionysos.Web" />
			<add verb="*" path="js.axd" validate="false" type="Dionysos.Web.HttpHandlers.CombineCompressHandler, Dionysos.Web" />
			<add type="DevExpress.Web.ASPxHttpHandlerModule, DevExpress.Web.v20.1, Version=20.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" verb="GET,POST" path="DX.ashx" validate="false" />
		</httpHandlers>
		<httpModules>
			<add type="DevExpress.Web.ASPxHttpHandlerModule, DevExpress.Web.v20.1, Version=20.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" name="ASPxHttpHandlerModule" />
		</httpModules>
	</system.web>
	<!--  AUTHORIZATION PER PAGE
          This section sets the authorization policies of the application. You can allow or deny access
          to application resources by user or role. "*" mean everyone (anonymous & authenticated), "?" means anonymous only
          (unauthenticated) users.
    -->
	<location path="500.aspx">
		<system.web>
			<authorization>
				<allow users="?" />
			</authorization>
		</system.web>
	</location>
	<location path="SignOn.aspx">
		<system.web>
			<authorization>
				<allow users="?" />
			</authorization>
		</system.web>
	</location>
	<location path="ClearCache.aspx">
		<system.web>
			<authorization>
				<allow users="?" />
			</authorization>
		</system.web>
	</location>
	<location path="css">
		<system.web>
			<authorization>
				<allow users="?" />
			</authorization>
		</system.web>
	</location>
	<location path="js">
		<system.web>
			<authorization>
				<allow users="?" />
			</authorization>
		</system.web>
	</location>
	<location path="favicon.ico">
		<system.web>
			<authorization>
				<allow users="?" />
			</authorization>
		</system.web>
	</location>
	<system.webServer>
		<security>
			<requestFiltering>
				<requestLimits maxAllowedContentLength="1049000000" />
			</requestFiltering>
		</security>
		<validation validateIntegratedModeConfiguration="false" />
		<modules runAllManagedModulesForAllRequests="false">
			<!-- UNCOMMENT FOR COMPRESSION <add name="HttpCompressModule" type="DC.Web.HttpCompress.HttpModule,Dionysos.Web" />-->
			<add name="ASPxHttpHandlerModule" type="DevExpress.Web.ASPxHttpHandlerModule, DevExpress.Web.v20.1, Version=20.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" />
			<add name="HttpResponseHeaderRemovalModule" type="Obymobi.Noc.WebApplication.HttpResponseHeaderRemovalModule" />
		</modules>
		<httpProtocol>
			<customHeaders>
				<add name="X-XSS-Protection" value="1"/>
				<remove name="X-Powered-By"/>
			</customHeaders>
		</httpProtocol>
		<handlers>
			<!-- UNCOMMENT FOR COMPRESSION <add name="CompressionHandler" preCondition="integratedMode" verb="*" path="js.axd,css.axd"  type="DC.Web.HttpCompress.CompressionHandler,Dionysos.Web" />-->
			<remove name="CombineCompressCss" />
			<add verb="*" path="css.axd" name="CombineCompressCss" type="Dionysos.Web.HttpHandlers.CombineCompressHandler, Dionysos.Web" />
			<remove name="CombineCompressJs" />
			<add verb="*" path="js.axd" name="CombineCompressJs" type="Dionysos.Web.HttpHandlers.CombineCompressHandler, Dionysos.Web" />
			<add type="DevExpress.Web.ASPxUploadProgressHttpHandler, DevExpress.Web.v20.1, Version=20.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" verb="GET,POST" path="ASPxUploadProgressHandlerPage.ashx" name="ASPxUploadProgressHandler" preCondition="integratedMode" />
			<add type="DevExpress.Web.ASPxHttpHandlerModule, DevExpress.Web.v20.1, Version=20.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" verb="GET,POST" path="DX.ashx" name="ASPxHttpHandlerModule" preCondition="integratedMode" />
		</handlers>
        <rewrite>
            <outboundRules>
                <clear/>
                <rule name="Add SameSite" preCondition="No SameSite">
                    <match serverVariable="RESPONSE_Set_Cookie" pattern=".*" negate="false"/>
                    <action type="Rewrite" value="{R:0}; SameSite=lax"/>
                </rule>
                <preConditions>
                    <preCondition name="No SameSite">
                        <add input="{RESPONSE_Set_Cookie}" pattern="."/>
                        <add input="{RESPONSE_Set_Cookie}" pattern="; SameSite=lax" negate="true"/>
                    </preCondition>
                </preConditions>
            </outboundRules>
        </rewrite>
	</system.webServer>
	<system.web.extensions>
		<scripting>
			<webServices>
				<jsonSerialization maxJsonLength="2147483647" />
			</webServices>
		</scripting>
	</system.web.extensions>
	<devExpress>
		<compression enableHtmlCompression="true" enableCallbackCompression="true" enableResourceCompression="true" enableResourceMerging="true" />
		<themes enableThemesAssembly="True" styleSheetTheme="" theme="Glass" customThemeAssemblies="" />
		<errors callbackErrorRedirectUrl="" />
		<settings rightToLeft="false" doctypeMode="Xhtml" embedRequiredClientLibraries="false" />
	</devExpress>
	<system.serviceModel>
		<bindings>
			<basicHttpBinding>
				<binding name="BasicHttpBinding_MTechAPI">
					<security mode="TransportWithMessageCredential" />
				</binding>
			</basicHttpBinding>
		</bindings>
		<client>
			<endpoint address="https://ifc.int.hot-sos.net/API/Service.svc/soap" binding="basicHttpBinding" bindingConfiguration="BasicHttpBinding_MTechAPI" contract="MTechAPI.MTechAPI" name="MTechAPI" />
		</client>
	</system.serviceModel>
	<runtime>
		<assemblyBinding xmlns="urn:schemas-microsoft-com:asm.v1">
			<dependentAssembly>
				<assemblyIdentity name="AWSSDK" publicKeyToken="9f476d3089b52be3" culture="neutral" />
				<bindingRedirect oldVersion="0.0.0.0-2.1.7.0" newVersion="2.1.7.0" />
			</dependentAssembly>
			<dependentAssembly>
				<assemblyIdentity name="Microsoft.Data.Edm" publicKeyToken="31bf3856ad364e35" culture="neutral" />
				<bindingRedirect oldVersion="0.0.0.0-5.2.0.0" newVersion="5.2.0.0" />
			</dependentAssembly>
			<dependentAssembly>
				<assemblyIdentity name="Microsoft.Data.OData" publicKeyToken="31bf3856ad364e35" culture="neutral" />
				<bindingRedirect oldVersion="0.0.0.0-5.2.0.0" newVersion="5.2.0.0" />
			</dependentAssembly>
			<dependentAssembly>
				<assemblyIdentity name="System.Spatial" publicKeyToken="31bf3856ad364e35" culture="neutral" />
				<bindingRedirect oldVersion="0.0.0.0-5.6.0.0" newVersion="5.6.0.0" />
			</dependentAssembly>
			<dependentAssembly>
				<assemblyIdentity name="System.Net.Http" publicKeyToken="b03f5f7f11d50a3a" culture="neutral" />
				<bindingRedirect oldVersion="0.0.0.0-4.0.0.0" newVersion="4.0.0.0" />
			</dependentAssembly>
			<dependentAssembly>
				<assemblyIdentity name="System.Net.Http.WebRequest" publicKeyToken="b03f5f7f11d50a3a" culture="neutral" />
				<bindingRedirect oldVersion="0.0.0.0-4.0.0.0" newVersion="4.0.0.0" />/&gt;
			</dependentAssembly>
			<dependentAssembly>
				<assemblyIdentity name="System.Threading.Tasks" publicKeyToken="b03f5f7f11d50a3a" culture="neutral" />
				<bindingRedirect oldVersion="0.0.0.0-4.0.0.0" newVersion="4.0.0.0" />
			</dependentAssembly>
			<dependentAssembly>
				<assemblyIdentity name="System.Net.Http.Primitives" publicKeyToken="b03f5f7f11d50a3a" culture="neutral" />
				<bindingRedirect oldVersion="0.0.0.0-4.0.0.0" newVersion="4.0.0.0" />
			</dependentAssembly>
			<dependentAssembly>
				<assemblyIdentity name="System.IO" publicKeyToken="b03f5f7f11d50a3a" culture="neutral" />
				<bindingRedirect oldVersion="0.0.0.0-2.6.6.0" newVersion="2.6.6.0" />
			</dependentAssembly>
			<dependentAssembly>
				<assemblyIdentity name="Google.Apis" publicKeyToken="4b01fa6e34db77ab" culture="neutral" />
				<bindingRedirect oldVersion="0.0.0.0-1.13.1.0" newVersion="1.13.1.0" />
			</dependentAssembly>
			<dependentAssembly>
				<assemblyIdentity name="Google.Apis.Core" publicKeyToken="4b01fa6e34db77ab" culture="neutral" />
				<bindingRedirect oldVersion="0.0.0.0-1.17.0.0" newVersion="1.17.0.0" />
			</dependentAssembly>
			<dependentAssembly>
				<assemblyIdentity name="SD.LLBLGen.Pro.ORMSupportClasses" publicKeyToken="ca73b74ba4e3ff27" culture="neutral" />
				<bindingRedirect oldVersion="0.0.0.0-4.2.0.0" newVersion="4.2.0.0" />
			</dependentAssembly>
			<dependentAssembly>
				<assemblyIdentity name="SD.LLBLGen.Pro.DQE.SqlServer" publicKeyToken="ca73b74ba4e3ff27" culture="neutral" />
				<bindingRedirect oldVersion="0.0.0.0-4.2.0.0" newVersion="4.2.0.0" />
			</dependentAssembly>
			<dependentAssembly>
				<assemblyIdentity name="Newtonsoft.Json" publicKeyToken="30ad4fe6b2a6aeed" culture="neutral" />
				<bindingRedirect oldVersion="0.0.0.0-12.0.0.0" newVersion="12.0.0.0" />
			</dependentAssembly>
			<dependentAssembly>
				<assemblyIdentity name="log4net" publicKeyToken="669e0ddf0bb1aa2a" culture="neutral" />
				<bindingRedirect oldVersion="0.0.0.0-1.2.13.0" newVersion="1.2.13.0" />
			</dependentAssembly>
			<dependentAssembly>
				<assemblyIdentity name="DocumentFormat.OpenXml" publicKeyToken="31bf3856ad364e35" culture="neutral" />
				<bindingRedirect oldVersion="0.0.0.0-2.5.5631.0" newVersion="2.5.5631.0" />
			</dependentAssembly>
			<dependentAssembly>
				<assemblyIdentity name="System.ComponentModel.Annotations" publicKeyToken="b03f5f7f11d50a3a" culture="neutral" />
				<bindingRedirect oldVersion="0.0.0.0-4.2.1.0" newVersion="4.2.1.0" />
			</dependentAssembly>
			<dependentAssembly>
				<assemblyIdentity name="System.Buffers" publicKeyToken="cc7b13ffcd2ddd51" culture="neutral" />
				<bindingRedirect oldVersion="0.0.0.0-4.0.3.0" newVersion="4.0.3.0" />
			</dependentAssembly>
			<dependentAssembly>
				<assemblyIdentity name="System.Memory" publicKeyToken="cc7b13ffcd2ddd51" culture="neutral" />
				<bindingRedirect oldVersion="0.0.0.0-4.0.1.1" newVersion="4.0.1.1" />
			</dependentAssembly>
		</assemblyBinding>
	</runtime>
</configuration>
