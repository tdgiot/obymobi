﻿using Dionysos.Web;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Logic.HelperClasses;
using System;
using System.Collections.Generic;
using Obymobi.Constants;
using Obymobi.Noc.Logic.HelperClasses;

public partial class DisconnectedFromComet : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        this.Title = string.Format("{0} - {1}", Resources.strings.Disconnected_from_comet, Resources.strings.Crave_NOC);
        this.Initialize();
    }

    private void Initialize()
    {
        if (QueryStringHelper.HasValue("cid"))
        { 
            int companyId = QueryStringHelper.GetInt("cid");

            ClientCollection clientCollection = NocHelper.GetClients(new List<int>(companyId), 3, false, true);
            TerminalCollection terminalCollection = NocHelper.GetTerminals(new List<int>(companyId), false, 3, true);

            this.RenderClients(clientCollection, companyId);
            this.RenderTerminals(terminalCollection);
        }
    }

    private void RenderClients(ClientCollection clients, int companyId)
    {
        string emenuReleaseVersion = ReleaseHelper.GetVersionForCompany(ApplicationCode.Emenu, companyId);

        foreach (ClientEntity client in clients)
        {
            UserControls_Listitems_Client clientControl = this.LoadControl<UserControls_Listitems_Client>("~/UserControls/Listitems/Client.ascx");
            clientControl.Client = client;
            clientControl.EmenuReleaseVersion = emenuReleaseVersion;
            this.plhClients.Controls.Add(clientControl);
        }
    }

    private void RenderTerminals(TerminalCollection terminals)
    {
        foreach (TerminalEntity terminal in terminals)
        {
            UserControls_Listitems_Terminal terminalControl = this.LoadControl<UserControls_Listitems_Terminal>("~/UserControls/Listitems/Terminal.ascx");
            terminalControl.Terminal = terminal;
            this.plhTerminals.Controls.Add(terminalControl);
        }
    }
}