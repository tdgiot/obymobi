﻿<%@ Page Async="true" Title="" Language="C#" MasterPageFile="~/MasterPages/CraveMobileNocBase.master" AutoEventWireup="true" Inherits="Order" Codebehind="Order.aspx.cs" %>
<%@ Reference VirtualPath="~/UserControls/Listitems/OrderRoutestephandler.ascx" %>
<%@ Reference VirtualPath="~/UserControls/Listitems/OrderRoutestephandlerHistory.ascx" %>
<%@ Reference VirtualPath="~/UserControls/Listitems/Orderitem.ascx" %>
<%@ Reference VirtualPath="~/UserControls/Listitems/DeviceActivityLog.ascx" %>
<%@ Reference VirtualPath="~/UserControls/Listitems/Order.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <ul class="details">
    <li>
        <D:Image ID="imgOrder" runat="server" />
        <div class="title"><D:Label ID="lblTitle" runat="server"></D:Label></div>
    </li>
    <li>
        <div class="label"><D:LabelTextOnly ID="lblCompany" runat="server" Text="<%$ Resources:strings, Company %>"></D:LabelTextOnly>:</div>
        <div class="value"><D:HyperLink ID="hlCompany" runat="server"></D:HyperLink></div>
    </li>
    <li>
        <div class="label"><D:LabelTextOnly ID="lblOrderIdLabel" runat="server" Text="<%$ Resources:strings, OrderId %>"></D:LabelTextOnly>:</div>
        <div class="value"><D:Label ID="lblOrderId" runat="server"></D:Label></div>
    </li>
    <D:PlaceHolder runat="server" ID="plhMasterOrder" Visible="False">
        <li>
            <div class="label error-text"><D:LabelTextOnly ID="lblMasterOrderIdLabel" runat="server" Text="<%$ Resources:strings, MasterOrderId %>"/>:</div>
            <div class="value error-text">PART OF MASTER ORDER: <D:HyperLink ID="hlMasterOrderId" runat="server" /></div>
        </li>
    </D:PlaceHolder>
    <li>
        <div class="label"><D:LabelTextOnly ID="lblStatusLabel" runat="server" Text="<%$ Resources:strings, Status %>"></D:LabelTextOnly>:</div>
        <div class="value"><D:Label ID="lblStatus" runat="server"></D:Label></div>
    </li>
    <li>
        <div class="label"><D:LabelTextOnly ID="lblErrorCodeLabel" runat="server" Text="<%$ Resources:strings, Order_errorcode %>"></D:LabelTextOnly>:</div>
        <div class="value"><D:Label ID="lblErrorCode" runat="server"></D:Label></div>
    </li>
    <li>
        <div class="label"><D:LabelTextOnly ID="lblExpiredLabel" runat="server" Text="<%$ Resources:strings, Order_expired %>"></D:LabelTextOnly>:</div>
        <div class="value"><D:Label ID="lblExpired" runat="server"></D:Label></div>
    </li>
    <li>
        <div class="label"><D:LabelTextOnly ID="lblCreatedLabel" runat="server" Text="<%$ Resources:strings, Order_created %>"></D:LabelTextOnly>:</div>
        <div class="value"><D:Label ID="lblCreated" runat="server"></D:Label></div>
    </li>
    <li>
        <div class="label"><D:LabelTextOnly ID="lblUpdatedLabel" runat="server" Text="<%$ Resources:strings, Order_updated %>"></D:LabelTextOnly>:</div>
        <div class="value"><D:Label ID="lblUpdated" runat="server"></D:Label></div>
    </li>
    <li>
        <div class="label"><D:Label ID="lblProcessingTimeLabel" runat="server"></D:Label></div>
        <div class="value"><D:Label ID="lblProcessingTime" runat="server"></D:Label></div>
    </li>
    <li>
        <div class="label"><D:LabelTextOnly ID="LabelTextOnly2" runat="server" Text="<%$ Resources:strings, Order_notes %>"></D:LabelTextOnly>:</div>
        <div class="value"><D:Label ID="lblNotes" runat="server" RenderAsLinkIfLinkable="false"></D:Label></div>
    </li>
    <li>&nbsp;</li>
    <li>
        <div class="label"><D:LabelTextOnly ID="lblOrderServiceMethodTypeLabel" runat="server" Text="<%$ Resources:strings, Order_service_method_type %>"></D:LabelTextOnly>:</div>
        <div class="value"><D:Label ID="lblOrderServiceMethodType" runat="server" RenderAsLinkIfLinkable="false"></D:Label></div>
    </li>
    <li>
        <div class="label"><D:LabelTextOnly ID="lblOrderCheckoutMethodTypeLabel" runat="server" Text="<%$ Resources:strings, Order_checkout_method_type %>"></D:LabelTextOnly>:</div>
        <div class="value"><D:Label ID="lblOrderCheckoutMethodType" runat="server" RenderAsLinkIfLinkable="false"></D:Label></div>
    </li>
        <li>
            <div class="label"><D:LabelTextOnly ID="lblNumberOfCoversLabel" runat="server" Text="<%$ Resources:strings, Order_number_of_covers %>"></D:LabelTextOnly>:</div>
            <div class="value"><D:Label ID="lblNumberOfCovers" runat="server" RenderAsLinkIfLinkable="false"></D:Label></div>
        </li>
    <D:PlaceHolder runat="server" ID="plhCustomer" Visible="False">
        <li>&nbsp;</li>
        <li>
            <div class="label" style="font-weight: bold"><D:LabelTextOnly ID="lblCustomerInformation" runat="server" Text="<%$ Resources:strings, Customer_Information %>"></D:LabelTextOnly></div>
        </li>
        <li>
            <div class="label"><D:LabelTextOnly ID="lblCustomerNameLabel" runat="server" Text="<%$ Resources:strings, Customer_Name %>"></D:LabelTextOnly>:</div>
            <div class="value"><D:Label ID="lblCustomerName" runat="server"></D:Label></div>
        </li>
        <li>
            <div class="label"><D:LabelTextOnly ID="lblCustomerPhoneLable" runat="server" Text="<%$ Resources:strings, Customer_Phone %>"></D:LabelTextOnly>:</div>
            <div class="value"><D:Label ID="lblCustomerPhone" runat="server"></D:Label></div>
        </li>
        <li>
            <div class="label"><D:LabelTextOnly ID="lblCustomerEmailLabel" runat="server" Text="<%$ Resources:strings, Customer_Email %>"></D:LabelTextOnly>:</div>
            <div class="value"><D:Label ID="lblCustomerEmail" runat="server"></D:Label></div>
        </li>
    </D:PlaceHolder>
    <D:PlaceHolder runat="server" ID="plhDeliverypoint" Visible="False">
        <li>&nbsp;</li>
	    <li>
		    <div class="label" style="font-weight: bold"><D:LabelTextOnly ID="lblServicePointInformation" runat="server" Text="<%$ Resources:strings, Deliverypoint_title %>"></D:LabelTextOnly></div>
	    </li>
        <li>
            <div class="label"><D:LabelTextOnly ID="lblDeliverypointNumberLabel" runat="server" Text="<%$ Resources:strings, Deliverypoint_room %>"></D:LabelTextOnly>:</div>
            <div class="value"><D:Label ID="lblDeliverypointNumber" runat="server"></D:Label></div>
        </li>
	    <li>
		    <div class="label"><D:LabelTextOnly ID="lblClientIdLabel" runat="server" Text="Client ID"></D:LabelTextOnly>:</div>
		    <div class="value"><D:Label ID="lblClientId" runat="server"></D:Label></div>
	    </li>
    </D:PlaceHolder>
	<D:PlaceHolder runat="server" ID="plhChargeToRoom" Visible="False">
		<li>&nbsp;</li>
		<li>
			<div class="label" style="font-weight: bold"><D:LabelTextOnly ID="lblChargeToRoomInformation" runat="server" Text="<%$ Resources:strings, ChargeToRoom %>"></D:LabelTextOnly></div>
		</li>
		<li>
			<div class="label"><D:LabelTextOnly ID="lblChargeToRoomNumberLabel" runat="server" Text="<%$ Resources:strings, ChargeToRoom_RoomNumber %>"></D:LabelTextOnly>:</div>
			<div class="value"><D:Label ID="lblChargeToRoomNumber" runat="server" RenderAsLinkIfLinkable="false"></D:Label></div>
		</li>
		<li>
			<div class="label"><D:LabelTextOnly ID="lblChargeToRoomDeliverypointgroupLabel" runat="server" Text="<%$ Resources:strings, ChargeToRoom_Deliverypointgroup %>"></D:LabelTextOnly>:</div>
			<div class="value"><D:Label ID="lblChargeToRoomDeliverypointgroup" runat="server" RenderAsLinkIfLinkable="false"></D:Label></div>
		</li>
	</D:PlaceHolder>
    <D:PlaceHolder runat="server" ID="plhDeliveryInformation" Visible="False">
        <li>&nbsp;</li>
        <li>
            <div class="label" style="font-weight: bold"><D:LabelTextOnly ID="lblDeliveryInformation" runat="server" Text="<%$ Resources:strings, Delivery_Information %>"></D:LabelTextOnly></div>
        </li>
        <li>
            <div class="label"><D:LabelTextOnly ID="lblBuildingNameLabel" runat="server" Text="<%$ Resources:strings, Building_Name %>"></D:LabelTextOnly>:</div>
            <div class="value"><D:Label ID="lblBuildingName" runat="server"></D:Label></div>
        </li>
        <li>
            <div class="label"><D:LabelTextOnly ID="lblAddressLabel" runat="server" Text="<%$ Resources:strings, Address %>"></D:LabelTextOnly>:</div>
            <div class="value"><D:Label ID="lblAddress" runat="server"></D:Label></div>
        </li>
        <li>
            <div class="label"><D:LabelTextOnly ID="lblZipcodeLabel" runat="server" Text="<%$ Resources:strings, Zipcode %>"></D:LabelTextOnly>:</div>
            <div class="value"><D:Label ID="lblZipcode" runat="server"></D:Label></div>
        </li>
        <li>
            <div class="label"><D:LabelTextOnly ID="lblCityLabel" runat="server" Text="<%$ Resources:strings, City %>"></D:LabelTextOnly>:</div>
            <div class="value"><D:Label ID="lblCity" runat="server"></D:Label></div>
        </li>
        <li>
            <div class="label"><D:LabelTextOnly ID="lblInstructionsLabel" runat="server" Text="<%$ Resources:strings, Instructions %>"></D:LabelTextOnly>:</div>
            <div class="value"><D:Label ID="lblInstructions" runat="server"></D:Label></div>
        </li>
    </D:PlaceHolder>
    <D:PlaceHolder runat="server" ID="plhRoutingLog">
	    <li>&nbsp;</li>
	    <li>
	        <div class="label"><D:LabelTextOnly ID="LabelTextOnly1" runat="server" Text="<%$ Resources:strings, Order_RoutingLog %>"></D:LabelTextOnly>:</div>
	        <div class="value"><D:Label ID="lblRoutingLog" runat="server"></D:Label></div>
	    </li>
    </D:PlaceHolder>
</ul>
<!-- Functions -->
<h1>Functions</h1>
<ul class="buttonlist">
    <li><D:HyperLink ID="hlOpenMainorder" runat="server" Visible="False" Text="<%$ Resources:strings, Order_open_master_order %>"></D:HyperLink></li>
    <li><D:HyperLink ID="hlProcessOrder" runat="server" Visible="false" Text="<%$ Resources:strings, Order_process_order_manually %>"></D:HyperLink></li>
    <li><D:HyperLink ID="hlViewOrderitems" runat="server" Visible="false" Text="<%$ Resources:strings, Order_view_orderitems %>"></D:HyperLink></li>
    <li><D:HyperLink ID="hlOpenInCms" runat="server" Target="_blank" Text="<%$ Resources:strings, Generic_open_in_cms %>"></D:HyperLink></li>
    <li><asp:LinkButton Visible="False" ID="lbForcePayment" Text="<%$ Resources:strings, Order_force_payment %>" runat="server" OnCommand="MarkOrderAsPaid"></asp:LinkButton></li>
</ul>
<!-- Order Items -->
<D:PlaceHolder ID="plhOrderitems" runat="server">
<h1><D:LabelTextOnly ID="lblOrderitems" runat="server" Text="<%$ Resources:strings, Order_orderitems %>"></D:LabelTextOnly></h1>
<ul class="list sub compact">
    <D:PlaceHolder ID="plhOrderitemsList" runat="server"></D:PlaceHolder>
</ul>    
</D:PlaceHolder>
<!-- Sub Orders -->
<D:PlaceHolder runat="server" ID="plhSubOrders" Visible="False">
<h1><D:LabelTextOnly runat="server" Text="<%$ Resources:strings, Order_SubOrders %>"></D:LabelTextOnly></h1>
<ul class="list sub compact">
    <D:PlaceHolder ID="phlSubOrderList" runat="server"></D:PlaceHolder>
</ul>    
</D:PlaceHolder>
<!-- Notifications -->
<D:PlaceHolder ID="plhOrderNotifications" runat="server" Visible="False">
    <h1><D:LabelTextOnly ID="lblOrderNotifications" runat="server" Text="<%$ Resources:strings, Order_Notifications %>"></D:LabelTextOnly></h1>
    <ul class="list">
        <D:PlaceHolder ID="plhOrderNotificationList" runat="server"></D:PlaceHolder>
    </ul>    
</D:PlaceHolder>
<!-- PaymentTransactions -->
<D:PlaceHolder ID="plhPaymentTransactions" runat="server" Visible="False">
    <h1><D:LabelTextOnly ID="lblPaymentTransactions" runat="server" Text="<%$ Resources:strings, Order_PaymentTransactions %>"></D:LabelTextOnly></h1>
    <ul class="list">
        <D:PlaceHolder ID="plhPaymentTransactionList" runat="server"></D:PlaceHolder>
    </ul>    
</D:PlaceHolder>
<!-- Payment Transaction Splits -->
<D:Placeholder ID="plhPaymentTransactionSplits" runat="server" Visible="False">
    <h1><D:LabelTextOnly ID="lblPaymentTransactionSplits" runat="server" Text="<%$ Resources:strings, Order_PaymentTransactionSplits %>"></D:LabelTextOnly></h1>
    <ul class="list">
        <D:PlaceHolder ID="plhPaymentTransactionSplitList" runat="server"></D:PlaceHolder>
    </ul>
</D:Placeholder>    
<!-- OrderRoutestephandlers -->
<D:PlaceHolder ID="plhOrderRoutestephandlers" runat="server">
<h1><D:LabelTextOnly ID="lblSteps" runat="server" Text="<%$ Resources:strings, Order_steps %>"></D:LabelTextOnly></h1>
<ul class="list">
    <D:PlaceHolder ID="plhOrderRoutestephandlersList" runat="server"></D:PlaceHolder>
</ul>    
</D:PlaceHolder>
<!-- Terminal Logs -->
<D:PlaceHolder ID="plhTerminalLogs" runat="server">
    <h1><D:LabelTextOnly ID="lblTerminalsLogs" runat="server" Text="<%$ Resources:strings, Terminal_logs %>"></D:LabelTextOnly></h1>
    <ul class="list">
        <D:PlaceHolder ID="plhTerminalLogsList" runat="server"></D:PlaceHolder>
    </ul> 
</D:PlaceHolder>
</asp:Content>

