﻿using System;
using System.Collections.Generic;
using Dionysos.Web;
using Obymobi.Data.EntityClasses;
using Obymobi.Noc.Logic.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

public partial class Outlets : PageListview<OutletEntity>
{
	protected override EntityView<OutletEntity> GetEntityViewFromSession() => Session[SessionKeyEntityView] as EntityView<OutletEntity>;
    protected string EmptyResultText = Resources.strings.No_Outlet_Found;

    protected override void GetFilterAndSortFromSession()
	{
	}

	protected override EntityView<OutletEntity> InitializeEntityView()
	{
		List<int> companyIds = GetCurrentCompanyId();
		var outletCollection = NocHelper.GetOutlets(companyIds);
		return outletCollection.DefaultView;
	}

	protected override void SetFilter()
	{
	}

	protected override void SetSort()
	{
	}

	protected override void RenderList()
	{
		foreach (OutletEntity outlet in EntityView)
		{
			UserControls_Listitems_Outlet outletControl = LoadControl<UserControls_Listitems_Outlet>("~/UserControls/Listitems/Outlet.ascx");
			outletControl.Outlet = outlet;
			
            plhList.Controls.Add(outletControl);
		}
	}

	public override string SessionKey => "Outlets";

	private static List<int> GetCurrentCompanyId()
	{
		if (QueryStringHelper.HasValue("cid"))
		{
			return new List<int> {QueryStringHelper.GetInt("cid")};
		}

		return CraveMobileNocHelper.AvailableCompanyIds.Count > 0 ? CraveMobileNocHelper.AvailableCompanyIds : CraveMobileNocHelper.EmptyIntList;
	}

	protected void Page_Load(object sender, EventArgs e)
	{
		this.Title = $@"{Resources.strings.Outlets} - {Resources.strings.Crave_NOC}";
		this.Initialize();
	}
}
