﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/CraveMobileNocBase.master" AutoEventWireup="true" Inherits="Deliverypoint" Codebehind="Deliverypoint.aspx.cs" %>
<%@ Reference VirtualPath="~/UserControls/Listitems/Order.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<ul class="details">
    <li>
        <D:Image ID="imgDeliverypoint" runat="server" />
        <div class="title"><D:Label ID="lblTitle" runat="server"></D:Label></div>
    </li>
    <li>
        <div class="label"><D:LabelTextOnly ID="lblLastRequestLabel" runat="server" Text="<%$ Resources:strings, Deliverypoint_last_request %>"></D:LabelTextOnly>:</div>
        <div class="value"><D:Label ID="lblLastRequest" runat="server"></D:Label></div>
    </li>
    <li>
        <div class="label"><D:LabelTextOnly ID="lblDeliverypointgroupLabel" runat="server" Text="<%$ Resources:strings, Deliverypointgroup %>"></D:LabelTextOnly>:</div>
        <div class="value"><D:HyperLink ID="hlDeliverypointgroup" runat="server"></D:HyperLink></div>
    </li>
</ul>
<!-- Actions -->
<h1><D:LabelTextOnly ID="lblFunctions" runat="server" Text="<%$ Resources:strings, Functions %>"></D:LabelTextOnly></h1>
<ul class="buttonlist">
    <li><D:HyperLink ID="hlOpenInCms" runat="server" Target="_blank" Text="<%$ Resources:strings, Generic_open_in_cms %>"></D:HyperLink></li>
</ul>
<!-- Clients -->
<D:PlaceHolder ID="plhClients" runat="server">
    <h1><D:LabelTextOnly ID="lblClients" runat="server" Text="<%$ Resources:strings, Clients %>"></D:LabelTextOnly></h1>
    <ul class="list sub">
        <D:PlaceHolder ID="plhClientsList" runat="server"></D:PlaceHolder>
    </ul> 
</D:PlaceHolder>   
<!-- Recent Orders -->
<D:PlaceHolder ID="plhOrders" runat="server">
    <h1><D:LabelTextOnly ID="lblOrders" runat="server" Text="<%$ Resources:strings, Orders %>"></D:LabelTextOnly></h1>
    <ul class="list sub">
        <D:PlaceHolder ID="plhOrdersList" runat="server"></D:PlaceHolder>
    </ul>  
</D:PlaceHolder>
</asp:Content>

