﻿using System;
using System.Linq;
using Dionysos;
using Dionysos.Web.UI;
using Obymobi.Data;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Integrations.POS;
using Obymobi.Enums;
using Obymobi.Logic.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

public partial class CustomerStatus : PageDefault
{
    protected void Page_Load(object sender, EventArgs e)
    {
        this.Title = Resources.strings.Crave_NOC;

        if (CraveMobileNocHelper.AvailableCompanyIds.Count == 0)
            this.Response.Redirect("~", true);

        OrderCollection orders = this.GetErrorAndExpiredOrders();
        if (orders.Count > 0)
        {
            this.plhNoWarnings.Visible = false;
            this.plhWarningPanel.Visible = true;

            CompanyEntity company = new CompanyEntity(CraveMobileNocHelper.AvailableCompanyIds[0]);
            var postype = company.GetRelatedPosConnectorType();

            // Get last TerminalLog            
            PredicateExpression filterTerminalLog = new PredicateExpression();
            var orderIds = from o in orders select o.OrderId;
            var orderGuids = from o in orders where !o.Guid.IsNullOrWhiteSpace() select o.Guid;

            // GK Try by OrderId / Guid, since there's no direct link with Terminal any more (sorry, not tested... !!)
            PredicateExpression orderFilter = new PredicateExpression();            
            orderFilter.AddWithOr(TerminalLogFields.OrderId == orderIds.ToList());
            orderFilter.AddWithOr(TerminalLogFields.OrderGuid == orderGuids.ToList());

            filterTerminalLog.Add(orderFilter);
            filterTerminalLog.Add(TerminalLogFields.Type >= (int)TerminalLogType.Message);

            SortExpression sortTerminalLog = new SortExpression();
            sortTerminalLog.Add(TerminalLogFields.TerminalLogId | SortOperator.Descending);            

            TerminalLogCollection logs = new TerminalLogCollection();
            logs.GetMulti(filterTerminalLog, 1, sortTerminalLog);

            var involvedTableNumbers = (from o in orders select o.DeliverypointEntity.Number).Distinct().ToList();
            
            string tableNumbers = involvedTableNumbers.Aggregate("", (current, tableNumber) => StringUtil.CombineWithComma(current, tableNumber));

            string errorMessage = string.Empty;
            if (logs.Count > 0)
            {
                errorMessage = logs[0].Message;
            }

            bool errorSpecified = false;
            if (orders.Any(o => o.ErrorCodeAsEnum == OrderProcessingError.PosErrorDeliverypointLocked))
            {
                this.lblTitle.Text = "Onverwerkte orders: Tafel op kassa is/was open.";
                this.plhTableWasLocked.Visible = true;
                this.lblTableNumberLocked.Text = tableNumbers;
                errorSpecified = true;
            }
            else if (errorMessage.Length > 0)
            {
                switch (postype)
                {
                    case POSConnectorType.Unknown:
                        break;
                }
            }

            if (!errorSpecified)
            {
                this.lblUnknownProblemTableNumber.Text = tableNumbers;
                this.plhUnknownProblem.Visible = true;
            }

            this.btProcessOrdersManually.Click += (btProcessOrdersManually_Click);
        }
        else
        {
            this.plhNoWarnings.Visible = true;
            this.plhWarningPanel.Visible = false;
        }
    }

    void btProcessOrdersManually_Click(object sender, EventArgs e)
    {
        var orders = this.GetErrorAndExpiredOrders();
     
        foreach(var order in orders)
        {
            OrderHelper.UpdateOrderStatus(order, OrderStatus.Processed);
        }

        this.Response.Redirect("~/CustomerStatus.aspx");
    }

    private OrderCollection GetErrorAndExpiredOrders()
    {
        OrderCollection orders = new OrderCollection();

        PrefetchPath pathOrders = new PrefetchPath(EntityType.OrderEntity);
        pathOrders.Add(OrderEntityBase.PrefetchPathDeliverypointEntity).SubPath.Add(DeliverypointEntityBase.PrefetchPathDeliverypointgroupEntity);

        RelationCollection joins = new RelationCollection();
        joins.Add(OrderEntityBase.Relations.CompanyEntityUsingCompanyId);

        SortExpression sort = new SortExpression();
        sort.Add(OrderFields.CreatedUTC | SortOperator.Ascending);

        // Unprocessable orders: Red
        var filter = new PredicateExpression();
        int companyId = -1;
        if (CraveMobileNocHelper.AvailableCompanyIds.Count > 0)
        {
            companyId = CraveMobileNocHelper.AvailableCompanyIds[0];
            filter.Add(CompanyFields.CompanyId == companyId);
        }
        else
            this.Response.Redirect("~", true);

        // Either they are Unproccesable, or haven't been completed in 5 minutes
        filter.Add(OrderFields.Status == (int)OrderStatus.Unprocessable);

        PredicateExpression expiredOrders = new PredicateExpression();
        expiredOrders.Add(OrderFields.Status != (int)OrderStatus.Processed);
        expiredOrders.Add(OrderFields.CreatedUTC < DateTime.UtcNow.AddMinutes(-5));
        expiredOrders.Add(OrderFields.CompanyId < companyId);
        filter.AddWithOr(expiredOrders);

        orders.GetMulti(filter, 0, null, joins, pathOrders);

        return orders;
    }
}