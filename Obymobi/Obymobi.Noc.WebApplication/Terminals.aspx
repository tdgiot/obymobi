﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/CraveMobileNocBase.master" AutoEventWireup="true" Inherits="Terminals" Codebehind="Terminals.aspx.cs" %>
<%@ Reference VirtualPath="~/UserControls/Listitems/Terminal.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<div id="settings">
    <div id="filter">
        <D:DropDownList ID="ddlFilter" runat="server" AutoPostBack="true">
            <asp:ListItem Text="<%$ Resources:strings, Terminal_all_terminals %>"></asp:ListItem>
            <asp:ListItem Text="<%$ Resources:strings, Terminal_online_terminals %>"></asp:ListItem>
            <asp:ListItem Text="<%$ Resources:strings, Terminal_offline_terminals %>"></asp:ListItem>
        </D:DropDownList>
    </div>
    <div id="sort">
        <D:DropDownList ID="ddlSort" runat="server" AutoPostBack="true">
            <asp:ListItem Text="<%$ Resources:strings, Terminal_last_request_desc %>"></asp:ListItem>
            <asp:ListItem Text="<%$ Resources:strings, Terminal_status_asc %>"></asp:ListItem>
            <asp:ListItem Text="<%$ Resources:strings, Terminal_version_desc %>"></asp:ListItem>
        </D:DropDownList>
    </div>
    <div id="title"><D:LabelTextOnly ID="lblTitle" runat="server"></D:LabelTextOnly></div>
</div>
<ul class="list">
    <D:PlaceHolder ID="plhList" runat="server"></D:PlaceHolder>
</ul>    
</asp:Content>

