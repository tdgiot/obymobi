﻿using System;
using Dionysos;
using Obymobi.Logic.HelperClasses;
using Obymobi.Data.CollectionClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Dionysos.Web;
using Obymobi.Enums;
using Obymobi.Noc.Logic.HelperClasses;

public partial class Orderitems : PageListview<OrderitemEntity>
{
    #region Methods

    #region Base class methods

    protected override EntityView<OrderitemEntity> GetEntityViewFromSession()
    {
        return Session[this.SessionKeyEntityView] as EntityView<OrderitemEntity>;
    }

    protected override void GetFilterAndSortFromSession()
    {
        object filter = Session[this.SessionKeyFilter];
        if (filter != null)
            this.ddlFilter.SelectedIndex = (int)filter;

        object sort = Session[this.SessionKeySort];
        if (sort != null)
            this.ddlSort.SelectedIndex = (int)sort;
    }

    protected override EntityView<OrderitemEntity> InitializeEntityView()
    {
        // Get the orderitems
        OrderitemCollection orderitemCollection = null;

        if (QueryStringHelper.HasValue("oid"))
            orderitemCollection = NocHelper.GetOrderitems(QueryStringHelper.GetInt("oid"));
        else
            throw new FunctionalException("'OrderId' parameter is missing, could not retrieve Orderitems.");

        // Return the view
        return orderitemCollection.DefaultView;
    }

    protected override void SetFilter()
    {
        // Filter
        switch (this.ddlFilter.SelectedIndex)
        {
            case 0:
                // All order items
                this.EntityView.Filter = null;
                break;
            case 1:
                // Processed order items
                this.EntityView.Filter = new PredicateExpression(new EntityProperty("OrderStatus") == OrderStatus.Processed);
                break;                     
            case 2:
                // Unprocessed order items
                this.EntityView.Filter = new PredicateExpression(new EntityProperty("OrderStatus") != OrderStatus.Processed);
                break;
        }

        // Store the current filter in the session
        Session[this.SessionKeyFilter] = this.ddlFilter.SelectedIndex;
    }

    protected override void SetSort()
    {
        // Sort
        switch (this.ddlSort.SelectedIndex)
        {
            case 0:
                // Number, ascending
                this.EntityView.Sorter = new SortExpression(new SortClause(OrderitemFields.OrderitemId, SortOperator.Ascending));
                break;
            case 1:
                // Productname, ascending
                this.EntityView.Sorter = new SortExpression(new SortClause(OrderitemFields.ProductName, SortOperator.Ascending));
                break;
       }

        // Store the current sort in the session
        Session[this.SessionKeySort] = this.ddlSort.SelectedIndex;
    }

    protected override void RenderList()
    {
        if (this.Count == 1)
            this.lblTitle.Text = Resources.strings.One_item;
        else
            this.lblTitle.Text = string.Format(Resources.strings.X_items, this.Count);

        foreach (var orderitem in this.EntityView)
        {
            UserControls_Listitems_Orderitem orderitemControl = this.LoadControl<UserControls_Listitems_Orderitem>("~/UserControls/Listitems/Orderitem.ascx");
            orderitemControl.Orderitem = orderitem;
            this.plhList.Controls.Add(orderitemControl);
        }
    }

    #endregion

    #endregion

    #region Properties

    public override string SessionKey
    {
        get { return "Orderitems"; }
    }

    #endregion

    #region Event handlers

    protected void Page_Load(object sender, EventArgs e)
    {
        this.Title = string.Format("{0} - {1}", Resources.strings.Order_orderitems, Resources.strings.Crave_NOC);
        this.Initialize();
    }

    #endregion
}