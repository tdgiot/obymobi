﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/CraveMobileNocBase.master" AutoEventWireup="true" Inherits="Delivery" Codebehind="Delivery.aspx.cs" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">        
    <D:PlaceHolder runat="server" ID="plhDeliveryTime">
        <ul class="details">
            <li>            
                <div class="title"><D:LabelTextOnly runat="server" Text="<%$ Resources:strings, Delivery_Order_Time %>"></D:LabelTextOnly></div>
            </li>                          
            <li>
                <div class="label"><D:LabelTextOnly runat="server" Text="<%$ Resources:strings, Delivery_Current_Time %>"></D:LabelTextOnly></div>
                <div class="value"><D:Label ID="lblDeliveryCurrentTime" runat="server"></D:Label></div>
            </li>                        
        </ul>           
        <ul class="buttonlist">
            <li>
                <D:TextBoxInt ID="tbDeliveryTime" runat="server"></D:TextBoxInt>
            </li>
            <li>
                <D:LinkButton ID="hlSetOrderTime" NavigateUrl="~/Delivery.aspx?setDeliveryTime=0" runat="server" PreSubmitWarning="<%$ Resources:strings, Delivery_Time_Change_Confirmation %>" Text="<%$ Resources:strings, Delivery_Change_Time %>" />
            </li>
        </ul>
    </D:PlaceHolder>     

</asp:Content>