﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/CraveMobileNocBase.master" AutoEventWireup="true" Inherits="SystemState" Codebehind="SystemState.aspx.cs" %>
<%@ Import Namespace="Obymobi" %>
<%@ Import Namespace="Obymobi.Logic.Comet" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <style type="text/css">
  .divInfo { display: inline; }
</style>
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePageMethods="true"/>
    <D:PlaceHolder runat="server" ID="plhEnvironmentWarning"></D:PlaceHolder>
    <h1><D:Image ID="imgNocServiceStatus" runat="server" />Noc Service Status</h1>    
    <p id="phlNocStatus">
        <D:PlaceHolder runat="server" ID="plhNocServiceStatus"></D:PlaceHolder>

    </p>        
    <D:PlaceHolder ID="plhPollers" runat="server" Visible="False">
    <ul class="details">
        <li>
            <D:Image ID="imgPollers" runat="server" />
            <div class="title"><D:LabelTextOnly runat="server" Text="<%$ Resources:strings, System_state_PollersTitle %>"></D:LabelTextOnly></div>
        </li>
        <li>
            <div class="label"><strong><D:LabelTextOnly runat="server" Text="<%$ Resources:strings, System_state_PrimaryServer %>"></D:LabelTextOnly></strong></div>
        </li>
        <li>
            <div class="label"><D:LabelTextOnly runat="server" Text="<%$ Resources:strings, System_state_NocPoller %>"></D:LabelTextOnly></div>
            <div class="value"><D:Label ID="lblNocPoller1" runat="server"></D:Label></div>
        </li>
        <D:PlaceHolder runat="server" ID="plhGenericPollersPrimary"></D:PlaceHolder>
        <li>
            <div class="label"><D:LabelTextOnly ID="LabelTextOnly7" runat="server" Text="<%$ Resources:strings, System_state_MediaTaskQueue %>"></D:LabelTextOnly></div>
            <div class="value"><D:Label ID="lblMediaTaskQueueLength" runat="server"></D:Label></div>
        </li>                                           
        <D:PlaceHolder runat="server" ID="plhSecondaryServer">
        <li>
            <div class="label"><strong><D:LabelTextOnly ID="LabelTextOnly2" runat="server" Text="<%$ Resources:strings, System_state_SecondaryServer %>"></D:LabelTextOnly></strong></div>
        </li>
        <li>
            <div class="label"><D:LabelTextOnly ID="LabelTextOnly3" runat="server" Text="<%$ Resources:strings, System_state_NocPoller %>"></D:LabelTextOnly></div>
            <div class="value"><D:Label ID="lblNocPoller2" runat="server"></D:Label></div>
        </li>          
        <D:PlaceHolder runat="server" ID="plhGenericPollersSecondary"></D:PlaceHolder>               
        </D:PlaceHolder>
    </ul>
    </D:PlaceHolder>
    <p>&nbsp;</p>
    <D:PlaceHolder ID="plhCometServer" runat="server">
    
    <ul class="details">
        <li>
            <img ID="imgCometState" runat="server" alt="" src="~/Images/Icons/error.png"></img>
            <div class="title"><D:LabelTextOnly ID="lblCometState" runat="server" Text="<%$ Resources:strings, Comet_state %>"></D:LabelTextOnly> - <D:Label ID="lblCometLastRefresh" runat="server" Text=""></D:Label></div>
        </li>
        <li>
            <div class="label"><D:LabelTextOnly ID="lblCometConnectedLabel" runat="server" Text="<%$ Resources:strings, Comet_state_Connected %>"></D:LabelTextOnly></div>
            <div class="value"><D:Label ID="lblCometConnected" runat="server"></D:Label></div>
        </li>
        <li>
            <div class="label"><D:LabelTextOnly ID="lblActiveRequestsLabel" runat="server" Text="Active requests"></D:LabelTextOnly></div>
            <div class="value"><D:Label ID="lblActiveRequests" runat="server" LocalizeText="false"></D:Label></div>
        </li>
        <li>
            <div class="label"><D:LabelTextOnly ID="lblClientsTotalLabel" runat="server" Text="Clients online total"></D:LabelTextOnly></div>
            <div class="value"><D:Label ID="lblClientsTotal" runat="server" LocalizeText="false"></D:Label></div>
        </li>
        <li>
            <div class="label"><D:LabelTextOnly ID="lblEmenusConnectedToSignalRLabel" runat="server" Text="Emenu's connected to SignalR"></D:LabelTextOnly></div>
            <div class="value"><D:Label ID="lblEmenusConnectedToSignalR" runat="server" LocalizeText="false"></D:Label></div>
        </li>
        <li>
            <div class="label"><D:LabelTextOnly ID="lblSupportToolsConnectedToSignalRLabel" runat="server" Text="SupportTools connected to SignalR"></D:LabelTextOnly></div>
            <div class="value"><D:Label ID="lblSupportToolsConnectedToSignalR" runat="server" LocalizeText="false"></D:Label></div>
        </li>
        <li>
        <table class="label">
            <tr>
                <td>&nbsp;</td>
                <td><strong><D:LabelTextOnly ID="lblTitleTotal" runat="server" Text="<%$ Resources:strings, Comet_state_TotalConnections %>"></D:LabelTextOnly></strong></td>
                <td><strong>SignalR</strong></td>
            </tr>
            <tr>
                <td><D:LabelTextOnly ID="lblCometConnectedTerminalsLabel" runat="server" Text="<%$ Resources:strings, Comet_state_TerminalConnections %>"></D:LabelTextOnly></td>
                <td><D:Label ID="lblCometConnectedTerminals" runat="server"></D:Label></td>
                <td><D:Label ID="lblCometConnectedTerminalsSignalR" runat="server"></D:Label></td>
            </tr>
            <tr>
                <td><D:LabelTextOnly ID="lblCometConnectedOssLabel" runat="server" Text="<%$ Resources:strings, Comet_state_OSSConnections %>"></D:LabelTextOnly></td>
                <td><D:Label ID="lblCometConnectedOss" runat="server"></D:Label></td>
                <td><D:Label ID="lblCometConnectedOssSignalR" runat="server"></D:Label></td>
            </tr>
            <tr>
                <td><D:LabelTextOnly ID="lblCometConnectedClientsLabel" runat="server" Text="<%$ Resources:strings, Comet_state_ClientConnections %>"></D:LabelTextOnly></td>
                <td><D:Label ID="lblCometConnectedClients" runat="server"></D:Label></td>
                <td><D:Label ID="lblCometConnectedClientsSignalR" runat="server"></D:Label></td>
            </tr>
            <tr>
                <td><D:LabelTextOnly ID="lblCometConnectedSupportToolsLabel" runat="server" Text="<%$ Resources:strings, Comet_state_SupportToolsConnections %>"></D:LabelTextOnly></td>
                <td><D:Label ID="lblCometConnectedSupportTools" runat="server"></D:Label></td>
                <td><D:Label ID="lblCometConnectedSupportToolsSignalR" runat="server"></D:Label></td>
            </tr>
            <tr>
                <td><D:LabelTextOnly ID="lblCometConnectedSandboxLabel" runat="server" Text="<%$ Resources:strings, Comet_state_SandboxConnections %>"></D:LabelTextOnly></td>
                <td><D:Label ID="lblCometConnectedSandbox" runat="server"></D:Label></td>
                <td colspan="2"></td>
            </tr>
            <tr>
                <td><D:LabelTextOnly ID="lblCometConnectedWebAppsLabel" runat="server" Text="<%$ Resources:strings, Comet_state_WebAppConnections %>"></D:LabelTextOnly></td>
                <td><D:Label ID="lblCometConnectedWebApps" runat="server"></D:Label></td>
                <td colspan="2"></td>
            </tr>
            <tr class="totals">
                <td><D:LabelTextOnly ID="lblCometTotalConnectionsLabel" runat="server" Text="<%$ Resources:strings, Comet_state_TotalConnections %>"></D:LabelTextOnly></td>
                <td><D:Label ID="lblCometTotalConnections" runat="server"></D:Label></td>
                <td><D:Label ID="lblCometTotalConnectionsSignalR" runat="server"></D:Label></td>
            </tr>
            <li>
                <div class="label"><D:LabelTextOnly ID="lblApiV1CountLabel" runat="server" Text="<%$ Resources:strings, Company_Api_v1_count %>"></D:LabelTextOnly>:</div>
                <div class="value"><D:LabelTextOnly ID="lblApiV1Count" runat="server"></D:LabelTextOnly></div>
            </li>
            <li>
                <div class="label"><D:LabelTextOnly ID="lblApiV2CountLabel" runat="server" Text="<%$ Resources:strings, Company_Api_v2_count %>"></D:LabelTextOnly>:</div>
                <div class="value"><D:LabelTextOnly ID="lblApiV2Count" runat="server"></D:LabelTextOnly></div>
            </li>
            <tr>
                <td colspan="4" />
            </tr>
            <tr>
                <td><D:LabelTextOnly ID="lblCometWebserviceLabel" runat="server" Text="<%$ Resources:strings, Comet_state_Webservice %>"></D:LabelTextOnly></td>
                <td colspan="3"><D:Label ID="lblCometWebservice" runat="server"></D:Label></td>
            </tr>
            <tr>
                <td><D:LabelTextOnly ID="lblCometCmsLabel" runat="server" Text="<%$ Resources:strings, Comet_state_Cms %>"></D:LabelTextOnly></td>
                <td colspan="3"><D:Label ID="lblCometCms" runat="server"></D:Label></td>
            </tr>
            <tr>
                <td><D:LabelTextOnly ID="lblCometNocLabel" runat="server" Text="<%$ Resources:strings, Comet_state_MobileNoc %>"></D:LabelTextOnly></td>
                <td colspan="3"><D:Label ID="lblCometNoc" runat="server"></D:Label></td>
            </tr>
        </table>
        </li>
    </ul>
    </D:PlaceHolder>
    <p>&nbsp;</p>   
    <D:PlaceHolder runat="server" ID="plhRefreshing" Visible="false">
        <h1><D:LabelTextOnly ID="lblRefreshing" runat="server" Text="<%$ Resources:strings, System_state_refreshing %>"></D:LabelTextOnly></h1>
    </D:PlaceHolder>
    <D:PlaceHolder ID="plhStatus" runat="server" Visible="false">
        <ul class="details">
            <li>
                <D:Image ID="imgSystemState" runat="server" />
                <div class="title"><D:LabelTextOnly ID="lblSystemState" runat="server" Text="<%$ Resources:strings, System_state %>"></D:LabelTextOnly> - <D:LabelTextOnly ID="lblLastRefresh" runat="server"></D:LabelTextOnly></div>
            </li>
            <li>
                <div class="label"><D:LabelTextOnly ID="lblOKLabel" runat="server" Text="<%$ Resources:strings, System_state_OK %>"></D:LabelTextOnly></div>
                <div class="value"><D:Label ID="lblOK" runat="server"></D:Label></div>
            </li>
            <li>
                <div class="label"><D:LabelTextOnly ID="lblCloudEnvironmentLabel" runat="server" Text="<%$ Resources:strings, System_state_cloudstatus_cloudenvironment %>"></D:LabelTextOnly></div>
                <div class="value"><D:Label ID="lblCloudEnvironment" runat="server"></D:Label></div>
            </li>
            <li>
                <div class="label"><D:LabelTextOnly ID="lblDatabaseLabel" runat="server" Text="<%$ Resources:strings, System_state_database %>"></D:LabelTextOnly></div>                
                <div class="value"><D:Label ID="lblDatabase" runat="server"></D:Label></div>
            </li>
            <li>
                <div class="label"><D:LabelTextOnly ID="lblWebserviceLabel" runat="server" Text="<%$ Resources:strings, System_state_webservice %>"></D:LabelTextOnly></div>
                <div class="value"><D:Label ID="lblWebservice" runat="server"></D:Label></div>
            </li>
            <li>
                <div class="label"><D:LabelTextOnly ID="lblDatabase1Label" runat="server" Text="<%$ Resources:strings, System_state_database_1 %>"></D:LabelTextOnly></div>
                <div class="value"><D:Label ID="lblDatabase1" runat="server"></D:Label></div>
            </li>
            <li>
                <div class="label"><D:LabelTextOnly ID="lblDatabase2Label" runat="server" Text="<%$ Resources:strings, System_state_database_2 %>"></D:LabelTextOnly></div>
                <div class="value"><D:Label ID="lblDatabase2" runat="server"></D:Label></div>
            </li>
            <li>
                <div class="label"><D:LabelTextOnly ID="lblLogLabel" runat="server" Text="<%$ Resources:strings, System_state_log %>"></D:LabelTextOnly></div>
                <div class="value"><D:Label ID="lblLog" runat="server"></D:Label></div>
            </li>
            <li>
                <div class="label"><D:LabelTextOnly ID="lblWebservice1Label" runat="server" Text="<%$ Resources:strings, System_state_webservice_1 %>"></D:LabelTextOnly></div>
                <div class="value"><D:Label ID="lblWebservice1" runat="server"></D:Label></div>
            </li>
            <li>
                <div class="label"><D:LabelTextOnly ID="lblWebservice2Label" runat="server" Text="<%$ Resources:strings, System_state_webservice_2 %>"></D:LabelTextOnly></div>
                <div class="value"><D:Label ID="lblWebservice2" runat="server"></D:Label></div>
            </li>
            <li>
                <div class="label"><D:LabelTextOnly ID="lblNocStatusServiceLabel" runat="server" Text="<%$ Resources:strings, System_state_nocstatusservice %>"></D:LabelTextOnly></div>
                <div class="value"><D:Label ID="lblNocStatusService" runat="server"></D:Label></div>
            </li>
            <li>
                <div class="label"><D:LabelTextOnly ID="lblNocStatusService1Label" runat="server" Text="<%$ Resources:strings, System_state_nocstatusservice_1 %>"></D:LabelTextOnly></div>
                <div class="value"><D:Label ID="lblNocStatusService1" runat="server"></D:Label></div>
            </li>
            <li>
                <div class="label"><D:LabelTextOnly ID="lblNocStatusService2Label" runat="server" Text="<%$ Resources:strings, System_state_nocstatusservice_2 %>"></D:LabelTextOnly></div>
                <div class="value"><D:Label ID="lblNocStatusService2" runat="server"></D:Label></div>
            </li>
        </ul>
        <p>&nbsp;</p>
    </D:PlaceHolder>   

    <ul class="details">
        <li>
            <D:Image ID="imgCloudApplicationVersions" runat="server" />
            <div class="title"><D:LabelTextOnly runat="server" Text="<%$ Resources:strings, System_state_cloud_application_versions %>"></D:LabelTextOnly></div>
        </li>
        <li class="spacer"></li>
        <D:PlaceHolder ID="plhCloudApplicationVersions" runat="server"></D:PlaceHolder>
        <p>&nbsp;</p>
    </ul>

<script src="Scripts/jquery-1.6.4.min.js" type="text/javascript"></script>
    <script src="Scripts/jquery.signalR-2.1.1.min.js" type="text/javascript"></script>
    <script src="<%= WebEnvironmentHelper.GetMessagingUrl() %>/signalr/js" type="text/javascript"></script>
    
    <script type="text/javascript">
        var plhNocServiceStatus = document.getElementById('phlNocStatus');
        var imgCometState = document.getElementById('<%=imgCometState.ClientID %>');
        var lblCometConnected = document.getElementById('<%=lblCometConnected.ClientID %>');
        var lblCometWebservice = document.getElementById('<%=lblCometWebservice.ClientID %>');
        var lblCometCms = document.getElementById('<%=lblCometCms.ClientID %>');
        var lblCometNoc = document.getElementById('<%=lblCometNoc.ClientID %>');
        var lblTotalWebApps = document.getElementById('<%=lblCometConnectedWebApps.ClientID %>');
        var lblLastRefresh = document.getElementById('<%=lblCometLastRefresh.ClientID %>');
        var lblTotalSandbox = document.getElementById('<%=lblCometConnectedSandbox.ClientID %>');

        var lblTotalConnections = document.getElementById('<%=lblCometTotalConnections.ClientID %>');
        var lblTotalConnectionsSignalR = document.getElementById('<%=lblCometTotalConnectionsSignalR.ClientID %>');
        
        var lblTotalTerminals = document.getElementById('<%=lblCometConnectedTerminals.ClientID %>');
        var lblTotalTerminalsSignalR = document.getElementById('<%=lblCometConnectedTerminalsSignalR.ClientID %>');

        var lblTotalClients = document.getElementById('<%=lblCometConnectedClients.ClientID %>');
        var lblTotalClientsSignalR = document.getElementById('<%=lblCometConnectedClientsSignalR.ClientID %>');
        
        var lblTotalSupportTools = document.getElementById('<%=lblCometConnectedSupportTools.ClientID %>');
        var lblTotalSupportToolsSignalR = document.getElementById('<%=lblCometConnectedSupportToolsSignalR.ClientID %>');
        
        var lblTotalConnectedOss = document.getElementById('<%=lblCometConnectedOss.ClientID %>');
        var lblTotalConnectedOssSignalR = document.getElementById('<%=lblCometConnectedOssSignalR.ClientID %>');
        
        var intervalListener = null;

        String.prototype.capitalize = function() {
            return this.charAt(0).toUpperCase() + this.slice(1);
        };

        (function (SignalRConnector, $, undefined) {

            SignalRConnector.Connect = function () {
                $.signalR.hub.start()
                    .done(function () {
                        console.log('Now connected, connection ID=' + $.connection.hub.id);
                        lblCometConnected.innerHTML = "Authenticating";

                        PageMethods.GetAuthentication($.connection.hub.id, SignalRConnector.Authenticate);
                    })
                    .fail(function (err) {
                        console.log('Could not connect: ' + err);
                        lblCometConnected.innerHTML = "Failed to connect: " + err;
                    });
            };

            SignalRConnector.Authenticate = function (collection) {
                $.signalR.signalRInterface.server.authenticate(collection[0], collection[1], collection[2]);
            };

            SignalRConnector.SendMessage = function (message) {
                $.signalR.signalRInterface.server.receiveMessage(message);
            };

        }(window.SignalRConnector = window.SignalRConnector || {}, $));

        $(function () {
            var updateNocServiceStatus = function (status) {
                plhNocServiceStatus.innerHTML = status;
            };

            var getSystemState = function () {
                PageMethods.NetmessageSystemState(SignalRConnector.SendMessage);

                PageMethods.GetNocServiceStatus(updateNocServiceStatus);
            };

            $.signalR.signalRInterface.client.ReceiveMessage = function (message) {
                console.log("Message: " + message);
                var jsonObject = jQuery.parseJSON(message);

                if (jsonObject.messageType == 17) {
                    if (jsonObject.isAuthenticated) {
                        if (jsonObject.isAuthenticated) {
                            console.log("Authenticated, sending ClientType");
                            PageMethods.NetmessageSetClientType(SignalRConnector.SendMessage);

                            lblCometConnected.innerHTML = "Connected";
                            console.log("Requesting first system state");
                            PageMethods.NetmessageSystemState(SignalRConnector.SendMessage);

                            if (intervalListener == null) {
                                console.log("Set refresh interval");
                                intervalListener = window.setInterval(getSystemState, 10000);
                            }

                        } else {
                            console.log("Failed to authenticate: " + jsonObject.authenticateErrorMessage);
                        }
                    }
                } else if (jsonObject.messageType == 9998) {
                    PageMethods.NetmessagePong(SignalRConnector.SendMessage);
                } else if (jsonObject.messageType == 5000) {
                    updateSystemState(jsonObject);
                } else if (jsonObject.messageType == 1000) {
                    console.log("Disconnected");
                    $.signalR.hub.reconnect();
                } else {
                    console.log("Unknown message: " + jsonObject.messageType);
                }
            };
            $.signalR.signalRInterface.client.MessageVerified = function (guid) { };

            $.signalR.hub.reconnect = function () {
                window.clearInterval(intervalListener);

                lblCometConnected.innerHTML = "Reconnecting";
                $.signalR.hub.stop();
                SignalRConnector.Connect();
            };

            // Configuration
            $.signalR.hub.url = "<%= WebEnvironmentHelper.GetMessagingUrl() %>/signalr";
            $.signalR.hub.qs = { "deviceIdentifier": "<%=CometConstants.CometIdentifierNoc%>" };
            $.signalR.hub.error(function(error) {
                console.log('SignalR error: ' + error);
                $.signalR.hub.reconnect();
            });
            // Connect
            SignalRConnector.Connect();

            PageMethods.GetNocServiceStatus(updateNocServiceStatus);
        });

        var updateSystemState = function (netmessage) {
            var d = new Date();
            lblLastRefresh.innerHTML = d.toLocaleDateString() + " " + d.toLocaleTimeString();

            lblTotalConnections.innerHTML = netmessage.totalConnectedClients[0];
            lblTotalConnectionsSignalR.innerHTML = netmessage.totalConnectedClients[2];

            lblTotalTerminals.innerHTML = netmessage.connectedTerminals[0];
            lblTotalTerminalsSignalR.innerHTML = netmessage.connectedTerminals[2];
            
            lblTotalClients.innerHTML = netmessage.connectedEmenu[0];
            lblTotalClientsSignalR.innerHTML = netmessage.connectedEmenu[2];
            
            lblTotalSupportTools.innerHTML = netmessage.connectedSupportTools[0];
            lblTotalSupportToolsSignalR.innerHTML = netmessage.connectedSupportTools[2];
            
            lblTotalConnectedOss.innerHTML = netmessage.connectedOnSiteServers[0];
            lblTotalConnectedOssSignalR.innerHTML = netmessage.connectedOnSiteServers[2];
            
            lblTotalSandbox.innerHTML = netmessage.connectedSandboxClients;
            
            lblCometWebservice.innerHTML = netmessage.isWebserviceConnected.toString().capitalize();
            lblCometWebservice.className = (netmessage.isWebserviceConnected ? 'green' : 'red');
            
            lblCometCms.innerHTML = netmessage.isCmsConnected.toString().capitalize();
            lblCometCms.className = (netmessage.isCmsConnected ? 'green' : 'red');
            
            lblCometNoc.innerHTML = netmessage.isNocConnected.toString().capitalize();
            lblCometNoc.className = (netmessage.isNocConnected ? 'green' : 'red');
            
            var totalWebAppsConnected = (netmessage.isWebserviceConnected ? 1 : 0);
            totalWebAppsConnected += (netmessage.isCmsConnected ? 1 : 0);
            totalWebAppsConnected += (netmessage.isNocConnected ? 1 : 0);
            lblTotalWebApps.innerHTML = totalWebAppsConnected;

            var allOk = (netmessage.isWebserviceConnected && netmessage.isCmsConnected && netmessage.isNocConnected);
            imgCometState.src = allOk ? "Images/Icons/checkmark.png" : "Images/Icons/error.png";
        };

        function ToggleList(ids) {
            var CState = document.getElementById(ids);
            if (CState.style.display != "block") { CState.style.display = "block"; }
            else { CState.style.display = "none"; }
        }
    </script>
</asp:Content>