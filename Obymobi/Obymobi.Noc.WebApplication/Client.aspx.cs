﻿using System;
using Obymobi.Data.EntityClasses;
using Obymobi.Logic.HelperClasses;
using Obymobi.Constants;

public partial class Client : PageEntity<ClientEntity>
{
    #region Event handlers

    protected void Page_Load(object sender, EventArgs e)
    {
        this.InitializeEntity();

        this.hlSetStatus1.Click += hlSetStatus1_Click;
        this.hlSetStatus400.Click += hlSetStatus400_Click;

        var client = this.Entity;

        if (this.Entity != null)
        {
            this.Title = string.Format("{0} {1} - {2}", Resources.strings.Client, this.Entity.ClientId, Resources.strings.Crave_NOC);

            UserControls_Listitems_Client clientControl = this.LoadControl<UserControls_Listitems_Client>("~/UserControls/Listitems/Client.ascx");
            clientControl.Client = client;
            clientControl.EmenuReleaseVersion = ReleaseHelper.GetVersionForCompany(ApplicationCode.Emenu, client.CompanyId);
            clientControl.RenderLinkable = false;
            this.plhDetails.Controls.Add(clientControl);

            // View latest orders link
            this.hlViewOrders.NavigateUrl = ResolveUrl(string.Format("~/Orders.aspx?clid={0}", this.Entity.ClientId));
            this.hlOpenInCms.NavigateUrl = this.GetCmsUrl(this.Entity.CompanyId, "Company/Client.aspx?id={0}", this.Entity.ClientId);
        }

        // Get the right minutes from the ddl
        int? hours = null;
        if (!this.ddlTimespan.SelectedValue.Equals("99999"))
            hours = Int32.Parse(this.ddlTimespan.SelectedValue);

        // Get client logs
        var activities = this.Entity.GetClientActivityLogs(this.cbNetmessages.Checked, this.cbClientLogs.Checked, this.cbClientStates.Checked, hours);

        this.lblClientsLogs.Text = String.Format(Resources.strings.Client_logs, activities.Count);

        if (activities.Count > 0)
        {
            foreach (var activity in activities)
            {
                UserControls_Listitems_DeviceActivityLog deviceActivityLogControl = this.LoadControl<UserControls_Listitems_DeviceActivityLog>("~/UserControls/Listitems/DeviceActivityLog.ascx");
                deviceActivityLogControl.Activity = activity;
                this.plhClientLogsList.Controls.Add(deviceActivityLogControl);
            }
        }

        this.plhClientLogsList.Visible = (activities.Count != 0);
    }

    void hlSetStatus400_Click(object sender, EventArgs e)
    {
        this.Entity.OperationMode = 400;
        this.Entity.Save();
        this.RedirectUsingRawUrl();
    }

    void hlSetStatus1_Click(object sender, EventArgs e)
    {
        this.Entity.OperationMode = 1;        
        this.Entity.Save();
        this.RedirectUsingRawUrl();
    }

    #endregion
}