﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/CraveMobileNocBase.master" AutoEventWireup="true" Inherits="LatestActivity" Codebehind="LatestActivity.aspx.cs" %>
<%@ Reference VirtualPath="~/UserControls/Listitems/Terminal.ascx" %>
<%@ Reference VirtualPath="~/UserControls/Listitems/Client.ascx" %>
<%@ Reference VirtualPath="~/UserControls/Listitems/Order.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <D:PlaceHolder ID="plhNoActivity" runat="server" Visible="false">
        <h1><D:LabelTextOnly ID="lblMinutes" runat="server"></D:LabelTextOnly></h1>
        <br /><br />
    </D:PlaceHolder>
    <D:PlaceHolder ID="plhActivity" runat="server">
        <ul class="list">
            <D:PlaceHolder ID="plhList" runat="server"></D:PlaceHolder>
        </ul>
    </D:PlaceHolder>
</asp:Content>

