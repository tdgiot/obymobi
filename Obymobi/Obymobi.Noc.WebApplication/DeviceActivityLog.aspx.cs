﻿using System;
using Obymobi.Data.EntityClasses;

public partial class DeviceActivityLog : PageEntity<CommonEntityBase>
{
    #region Event handlers

    protected void Page_Load(object sender, EventArgs e)
    {
        this.InitializeEntity();

        if (this.Entity != null)
        {
            this.Title = string.Format("{0} - {1}", Resources.strings.DeviceActivityLog, Resources.strings.Crave_NOC);

            Obymobi.Logic.HelperClasses.DeviceActivityLog deviceActivityLog = null;

            if (this.Entity is NetmessageEntity)
                deviceActivityLog = new Obymobi.Logic.HelperClasses.DeviceActivityLog(this.Entity as NetmessageEntity);
            else if (this.Entity is ClientLogEntity)
                deviceActivityLog = new Obymobi.Logic.HelperClasses.DeviceActivityLog(this.Entity as ClientLogEntity);
            else if (this.Entity is ClientStateEntity)
                deviceActivityLog = new Obymobi.Logic.HelperClasses.DeviceActivityLog(this.Entity as ClientStateEntity);
            else if (this.Entity is TerminalLogEntity)
                deviceActivityLog = new Obymobi.Logic.HelperClasses.DeviceActivityLog(this.Entity as TerminalLogEntity);
            else if (this.Entity is TerminalStateEntity)
                deviceActivityLog = new Obymobi.Logic.HelperClasses.DeviceActivityLog(this.Entity as TerminalStateEntity);

            UserControls_Listitems_DeviceActivityLog deviceActivityLogControl = this.LoadControl<UserControls_Listitems_DeviceActivityLog>("~/UserControls/Listitems/DeviceActivityLog.ascx");
            deviceActivityLogControl.Activity = deviceActivityLog;
            deviceActivityLogControl.RenderLinkable = false;
            deviceActivityLogControl.FullHeight = true;
            this.plhDetails.Controls.Add(deviceActivityLogControl);
        }
    }

    #endregion
}