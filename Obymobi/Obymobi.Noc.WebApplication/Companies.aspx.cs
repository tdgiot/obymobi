﻿using System;
using System.Collections.Generic;
using System.Linq;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Noc.Logic.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

public partial class Companies : PageListview<CompanyEntity>
{
    #region Methods

    #region Base class methods

    protected override EntityView<CompanyEntity> GetEntityViewFromSession()
    {
        return Session[this.SessionKeyEntityView] as EntityView<CompanyEntity>;
    }

    protected override void GetFilterAndSortFromSession()
    {
        object filter = Session[this.SessionKeyFilter];
        if (filter != null)
            this.ddlFilter.SelectedIndex = (int)filter;
    }

    protected override EntityView<CompanyEntity> InitializeEntityView()
    {
        // Get the companies
        CompanyCollection companyCollection = NocHelper.GetCompanies(CraveMobileNocHelper.AvailableCompanyIds);

        // Return the view
        return companyCollection.DefaultView;
    }

    protected override void SetFilter()
    {
        // Filter
        switch (this.ddlFilter.SelectedIndex)
        {
            case 0:
                // All companies
                this.EntityView.Filter = null;
                break;
            case 1:
                // Companies with issues
                // Done in RenderList with Linq.
                break;
            case 2:
                // Companies without issues
                // Done in RenderList with Linq.
                break;
        }

        // Store the current filter in the session
        Session[this.SessionKeyFilter] = this.ddlFilter.SelectedIndex;
    }

    protected override void SetSort()
    {
        this.EntityView.Sorter = new SortExpression(new SortClause(CompanyFields.Name, SortOperator.Ascending));
    }

    protected override void RenderList()
    {
        if (this.Count == 1)
            this.lblTitle.Text = Resources.strings.One_item;
        else
            this.lblTitle.Text = string.Format(Resources.strings.X_items, this.Count);

        List<UserControls_Listitems_Company> companyUserControls = new List<UserControls_Listitems_Company>();

        foreach (var company in this.EntityView)
        {
            UserControls_Listitems_Company companyControl = this.LoadControl<UserControls_Listitems_Company>("~/UserControls/Listitems/Company.ascx");
            companyControl.Company = company;
            companyUserControls.Add(companyControl);
        }

        if (this.ddlFilter.SelectedIndex == 1)
        {
            // Companies with errors
            companyUserControls = companyUserControls.Where(c => c.HasFaultyOrders || c.HasNonOrderingClients || c.HasOfflineTerminals).ToList();
        }
        else if (this.ddlFilter.SelectedIndex == 2)
        {
            // Companies without errors
            companyUserControls = companyUserControls.Where(c => !c.HasFaultyOrders && !c.HasNonOrderingClients && !c.HasOfflineTerminals).ToList();
        }

        foreach (var control in companyUserControls)
            this.plhList.Controls.Add(control);
    }

    #endregion

    #endregion

    #region Properties

    public override string SessionKey
    {
        get { return "Companies"; }
    }

    #endregion

    #region Event handlers

    protected void Page_Load(object sender, EventArgs e)
    {
        this.Title = string.Format("{0} - {1}", Resources.strings.Companies, Resources.strings.Crave_NOC);
        this.Initialize();
    }

    #endregion
}