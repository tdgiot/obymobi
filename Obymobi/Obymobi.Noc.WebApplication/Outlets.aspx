﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/CraveMobileNocBase.master" AutoEventWireup="true" CodeBehind="Outlets.aspx.cs" Inherits="Outlets" %>
<%@ Reference VirtualPath="~/UserControls/Listitems/Outlet.ascx" %>

<asp:Content ID="Content2" ContentPlaceHolderID="plhStylesheets" runat="server">
	<link rel="stylesheet" type="text/css" href="Css/Outlets/Outlets.css" />
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div id="titleHeader">
        <h1>Ordering</h1><br>
        <h2>Switch OFF/ON ordering</h2>
    </div>
    <% if (Count > 0) { %>
    <ul id="outlet-list" class="list">
		<D:PlaceHolder ID="plhList" runat="server"></D:PlaceHolder>
	</ul>
        <% }
       else
       { %>
        <h1 class="no-search-results-found-text"><%= EmptyResultText %></h1>
        <% } %>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="plhScripts" runat="server">
	<script type="text/javascript" src="Js/Outlets/Outlets.js"></script>
</asp:Content>
