using Dionysos.Web;
using Dionysos.Web.Security;
using Obymobi;
using Obymobi.Enums;
using Obymobi.Logic.Comet;
using Obymobi.Web;
using Obymobi.Web.CloudStorage;
using System;
using System.Collections.Specialized;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using Dionysos;
using Dionysos.Verification;
using Obymobi.Data.EntityClasses;
using Obymobi.Web.Slack;
using Obymobi.Web.Slack.Clients;

namespace CraveMobileNocV2
{
    public class Global : HttpApplicationSystemBased
    {
        private readonly string[] companySpecificPages = { "products", "outlets", "deliveryappless" };

        public Global() : base("CraveMobileNoc")
        { }

        public override void DatabaseIndependentPreInitialization()
        {
            // Set the data providers
            Dionysos.DataFactory.EntityCollectionFactory = new Dionysos.Data.LLBLGen.LLBLGenEntityCollectionFactory();
            Dionysos.DataFactory.EntityFactory = new Dionysos.Data.LLBLGen.LLBLGenEntityFactory();
            Dionysos.DataFactory.EntityUtil = new Dionysos.Data.LLBLGen.LLBLGenEntityUtil();

            // Set the assembly information
            Dionysos.Global.AssemblyInfo.Add(Server.MapPath("~/Bin/Obymobi.Data.dll"), "Data");

            // Set the configuration provivder
            Dionysos.Global.ConfigurationProvider = new Dionysos.Configuration.LLBLGenConfigurationProvider();

            // Set the application information
            Dionysos.Global.ApplicationInfo = new Dionysos.Web.WebApplicationInfo();
            Dionysos.Global.ApplicationInfo.ApplicationName = "CraveMobileNoc";
            Dionysos.Global.ApplicationInfo.ApplicationVersion = "1.2019103101";

            //// Set the configuration definitions            
            Dionysos.Global.ConfigurationInfo.Add(new Dionysos.DionysosConfigurationInfo());
            Dionysos.Global.ConfigurationInfo.Add(new Dionysos.Web.DionysosWebConfigurationInfo());
            Dionysos.Global.ConfigurationInfo.Add(new Obymobi.ObymobiConfigInfo());
            Dionysos.Global.ConfigurationInfo.Add(new Obymobi.Configuration.CraveCloudConfigInfo());

            var verifiers = new Dionysos.VerifierCollection();
            verifiers.Add(new AppSettingsVerifier(DionysosConfigurationConstants.BaseUrl));
            verifiers.Add(new AppSettingsVerifier(DionysosConfigurationConstants.CloudEnvironment));
            verifiers.Add(new WritePermissionVerifier(System.Web.Hosting.HostingEnvironment.MapPath("~/App_Data/Logs/")));
            verifiers.Add(new WritePermissionVerifier(System.Web.Hosting.HostingEnvironment.MapPath("~/App_Data/Audit/")));
            verifiers.Add(new AmazonCloudStorageVerifier());

            if (!verifiers.Verify())
            {
                throw new VerificationException(verifiers.ErrorMessage);
            }

            // Comet Helper
            CometHelper.SetCometProvider(new CometDesktopProvider(ClientCommunicationMethod.SignalR, CometConstants.CometIdentifierNoc, NetmessageClientType.MobileNoc));
        }

        public override void DatabaseDependentInitialization()
        {
            // Initialize the user manager            
            UserManager.Instance.FieldMappings.Username = Obymobi.Data.HelperClasses.UserFields.Username.Name;
            UserManager.Instance.UserEntityName = "UserEntity";

            // Nothing
            var verifiers = new Dionysos.VerifierCollection();
            verifiers.Add(new Obymobi.Logic.Verifiers.CloudEnvironmentConfigValidator());
            if (!verifiers.Verify())
            {
                throw new Dionysos.VerificationException(verifiers.ErrorMessage);
            }

            if (WebEnvironmentHelper.ORMProfilerEnabled)
            {
                EnableDatabaseProfiler();
            }
        }

        private void CheckForCompanySelection()
        {
            string pageName = System.IO.Path.GetFileNameWithoutExtension(Request.Url.AbsolutePath);
            if (!IsCompanySpecificPageCheck(pageName))
            {
                return;
            }

            NameValueCollection queryString = HttpUtility.ParseQueryString(HttpContext.Current.Request.Url.Query);
            if (queryString["cid"] != null)
            {
                return;
            }

            if (CraveMobileNocHelper.AvailableCompanyIds.Count == 1)
            {
                int companyId = CraveMobileNocHelper.AvailableCompanyIds.First();
                Uri uri = new Uri(Request.Url.AbsoluteUri).AddQueryString("cid", companyId);

                Response.Redirect(uri.AbsoluteUri);
            }

            Response.Redirect("~/CompanySelection.aspx?target=" + HttpUtility.UrlEncode(Request.Url.AbsoluteUri));
        }

        private bool IsCompanySpecificPageCheck(string pageName)
        {
            return this.companySpecificPages.Contains(pageName.ToLowerInvariant());
        }

        protected void Session_Start(object sender, EventArgs e)
        {
        }

        protected void Application_AcquireRequestState(object sender, EventArgs e)
        {
            CheckForCompanySelection();
        }

        protected override void Application_BeginRequest(object sender, EventArgs e)
        {
            if (TestUtil.IsPcBattleStationDanny)
            {
                Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture("en-us");
                Thread.CurrentThread.CurrentUICulture = CultureInfo.CreateSpecificCulture("en-us");
            }

            base.Application_BeginRequest(sender, e);
        }

        protected void Application_EndRequest()
        {
        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {

        }

        protected override void Application_Error(Exception ex)
        {
            try
            {
                Exception error = HttpContext.Current.Server.GetLastError().GetBaseException();
                HttpContext.Current.Application.Add("Exception", error);

                if (error is AuthorizationException)
                {
                    Redirect(WebShortcuts.ResolveUrl("~/401.aspx"), true);
                }
                else if (error is EntityNotFoundException || (error is HttpException && (error as HttpException).GetHttpCode() == 404))
                {
                    Redirect(WebShortcuts.ResolveUrl("~/404.aspx"), false);
                }
                else
                {
                    Redirect(WebShortcuts.ResolveUrl("~/500.aspx"), true);
                }
            }
            catch
            {
                // If the custom logging fails, use the framework logging
                base.Application_Error(ex);
            }
        }

        private void Redirect(string url, bool logError)
        {
            if (logError)
            {
                LogToSlack(HttpContext.Current);
            }

            Server.ClearError();
            Server.Transfer(url, true);
        }

        private static void LogToSlack(HttpContext context)
        {
            UserEntity currentUser = CmsSessionHelper.CurrentUser;
            SlackMessage slackMessage = new SlackMessage(CreateMessageText(context), currentUser.Username, null, null);
            SlackClient slackClient = new SlackClient(SlackWebhookEndpoints.DevSupportNotifications);
            slackClient.SendMessage(slackMessage);
        }

        private static string CreateMessageText(HttpContext httpContext)
        {
            MessageFromExceptionFactory messageFactory = new MessageFromExceptionFactory();
            return messageFactory.Create(httpContext);
        }

        protected void Session_End(object sender, EventArgs e)
        {

        }

        protected void Application_End(object sender, EventArgs e)
        {

        }
    }
}
