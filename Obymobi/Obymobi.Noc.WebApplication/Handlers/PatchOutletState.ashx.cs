﻿using System;
using System.IO;
using System.Net;
using System.Web;
using Obymobi.Data.EntityClasses;

namespace Obymobi.Noc.WebApplication.Handlers
{
    public class PatchOutletVisibility : IHttpHandler
    {
        public bool IsReusable => false;

        public void ProcessRequest(HttpContext context)
        {
            if (!int.TryParse(context.Request.QueryString["id"], out int id))
            {
                context.Response.StatusCode = (int)HttpStatusCode.NotFound;
                context.Response.Write("Not found");
                return;
            }

            OutletEntity outletEntity = new OutletEntity(id);

            if (outletEntity.IsNew)
            {
                context.Response.StatusCode = (int)HttpStatusCode.NotFound;
                context.Response.Write("Not found");
                return;
            }

            if (!CraveMobileNocHelper.CheckAuthorization(outletEntity.CompanyId))
            {
                context.Response.StatusCode = (int)HttpStatusCode.Unauthorized;
                context.Response.Write("Unauthorised");
                return;
            }

            outletEntity.OutletOperationalStateEntity.OrderIntakeDisabled = !outletEntity.OutletOperationalStateEntity.OrderIntakeDisabled;
            outletEntity.OutletOperationalStateEntity.UpdatedBy = CraveMobileNocHelper.CurrentUser?.UserId ?? -1;
            _ = outletEntity.OutletOperationalStateEntity.Save();

            WriteAuditToFile(outletEntity);

            string response = outletEntity.OutletOperationalStateEntity.OrderIntakeDisabled ? "Switch on" : "Switch off";

            context.Response.StatusCode = (int)HttpStatusCode.OK;
            context.Response.ContentType = "text/plain";
            context.Response.Write(response);
        }

        private void WriteAuditToFile(OutletEntity outletEntity)
        {
            try
            {
                string username = CraveMobileNocHelper.CurrentUser?.Username ?? "NULL";
                string auditString = $"{DateTime.UtcNow:s},{outletEntity.CompanyId},{outletEntity.OutletId},{outletEntity.Name},OrderIntakeDisabled,{outletEntity.OutletOperationalStateEntity.OrderIntakeDisabled},{username}";
                File.AppendAllLines(HttpContext.Current.Server.MapPath($"~/App_Data/Audit/outlet_{DateTime.UtcNow:yyyyMMdd}.txt"), new[] { auditString });
            }
            catch
            {
                // ignored
            }
        }
    }
}
