﻿using System;
using System.IO;
using System.Net;
using System.Web;
using System.Web.Hosting;
using Obymobi.Data.EntityClasses;
using Obymobi.Enums;

namespace Obymobi.Noc.WebApplication.Handlers
{
    public class PatchProductVisibility : IHttpHandler
    {
        #region Properties

        public bool IsReusable => false;

        #endregion

        #region Methods

        public void ProcessRequest(HttpContext context)
        {
            ProductEntity productEntity = null;

            if (int.TryParse(context.Request.QueryString["id"], out int id))
            {
                productEntity = new ProductEntity(id);

                if (productEntity == null || productEntity.IsNew)
                {
                    context.Response.StatusCode = (int)HttpStatusCode.NotFound;
                    context.Response.Write("Not found");
                    return;
                }
            }

            if (productEntity.VisibilityType == VisibilityType.Never)
            {
                productEntity.VisibilityType = VisibilityType.Always;
            }
            else
            {
                productEntity.VisibilityType = VisibilityType.Never;
            }

            productEntity.Save();

            WriteAuditToFile(productEntity);

            string response = productEntity.VisibilityType == VisibilityType.Always ? "HIDE" : "UNHIDE";

            context.Response.StatusCode = (int)HttpStatusCode.OK;
            context.Response.ContentType = "text/plain";
            context.Response.Write(response);
        }

        private void WriteAuditToFile(ProductEntity productEntity)
        {
            try
            { 
                string username = CraveMobileNocHelper.CurrentUser?.Username ?? "NULL";
                string auditString = $"{DateTime.UtcNow:s},{productEntity.CompanyId},{productEntity.ProductId},{productEntity.Name},VisibilityType,{productEntity.VisibilityType},{username}";
                File.AppendAllLines(HttpContext.Current.Server.MapPath($"~/App_Data/Audit/product_{DateTime.UtcNow:yyyyMMdd}.txt"), new[] { auditString });
            }
            catch
            {
                // ignored
            }
        }

        #endregion
    }
}
