﻿using System;
using System.Web;
using Dionysos.Web;

public partial class SignOn : System.Web.UI.Page
{
    protected bool EmbeddedMode { get; private set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        Title = string.Format("{0} - {1}", Resources.strings.SignOn, Resources.strings.Crave_NOC);

        if (QueryStringHelper.HasValue("msg"))
        {
            plhSignOnInvalid.Visible = true;
            lblSignOnInvalid.Text = Resources.strings.Crave_NOC_invalid_credentials;
        }

        CheckEmbeddedMode();
    }

    protected void Page_Unload(object sender, EventArgs e)
    {
        if (QueryStringHelper.HasValue("SignOut"))
        {
            var response = HttpContext.Current.Response;

            if (response.Cookies["currentUser"] != null)
            {
                response.Cookies["currentUser"].Expires = DateTime.Now.AddDays(-1);
            }
        }
    }

    private void CheckEmbeddedMode()
    {
        if (QueryStringHelper.HasValue("embeddedMode"))
        {
            if (!Page.IsPostBack)
            {
                bool embeddedMode = QueryStringHelper.GetBool("embeddedMode");
                Session["embeddedMode"] = embeddedMode.ToString().ToLower();
                EmbeddedMode = embeddedMode;
            }
        }
        else
        {
            var sessionKey = Session["embeddedMode"];
            if (sessionKey != null)
            {
                EmbeddedMode = (string)sessionKey == "true";
            }
            else
            {
                Session["embeddedMode"] = "false";
            }
        }
    }
}