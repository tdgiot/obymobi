﻿using System;
using Obymobi.Data.EntityClasses;
using Dionysos;
using Obymobi.Enums;
using System.Web;

public partial class OrderRoutestephandlerHistory : PageEntity<OrderRoutestephandlerHistoryEntity>
{
    protected void Page_Load(object sender, EventArgs e)
    {
        this.InitializeEntity();

        if (this.Entity != null)
        {
            switch (this.Entity.StatusAsEnum)
            {
                case OrderRoutestephandlerStatus.WaitingForPreviousStep:
                    this.imgOrderRoutestephandlerHistory.ImageUrl = ResolveUrl("~/Images/Icons/hourglass.png");
                    this.lblTitle.Text = string.Format(Resources.strings.OrderRoutestephandler_waiting_for_previous_step, this.Entity.Number);
                    break;
                case OrderRoutestephandlerStatus.WaitingToBeRetrieved:
                    this.imgOrderRoutestephandlerHistory.ImageUrl = ResolveUrl("~/Images/Icons/hourglass.png");
                    this.lblTitle.Text = string.Format(Resources.strings.OrderRoutestephandler_waiting_to_be_retrieved, this.Entity.Number);
                    break;
                case OrderRoutestephandlerStatus.RetrievedByHandler:
                    this.imgOrderRoutestephandlerHistory.ImageUrl = ResolveUrl("~/Images/Icons/hourglass.png");
                    this.lblTitle.Text = string.Format(Resources.strings.OrderRoutestephandler_retrieved_by_handler, this.Entity.Number);
                    break;
                case OrderRoutestephandlerStatus.BeingHandled:
                    this.imgOrderRoutestephandlerHistory.ImageUrl = ResolveUrl("~/Images/Icons/hourglass.png");
                    this.lblTitle.Text = string.Format(Resources.strings.OrderRoutestephandler_being_handled, this.Entity.Number);
                    break;
                case OrderRoutestephandlerStatus.Completed:
                    this.imgOrderRoutestephandlerHistory.ImageUrl = ResolveUrl("~/Images/Icons/checkmark.png");
                    this.lblTitle.Text = string.Format(Resources.strings.OrderRoutestephandler_completed, this.Entity.Number);
                    break;
                case OrderRoutestephandlerStatus.CompletedByOtherStep:
                    this.imgOrderRoutestephandlerHistory.ImageUrl = ResolveUrl("~/Images/Icons/checkmark.png");
                    this.lblTitle.Text = string.Format(Resources.strings.OrderRoutestephandler_completed_by_other_step, this.Entity.Number);
                    break;
                case OrderRoutestephandlerStatus.CancelledDueToOtherStepFailure:
                    this.imgOrderRoutestephandlerHistory.ImageUrl = ResolveUrl("~/Images/Icons/error.png");
                    this.lblTitle.Text = string.Format(Resources.strings.OrderRoutestephandler_cancelled_due_to_other_step_failure, this.Entity.Number);
                    this.lblStatus.CssClass = "error";
                    break;
                case OrderRoutestephandlerStatus.Failed:
                    this.imgOrderRoutestephandlerHistory.ImageUrl = ResolveUrl("~/Images/Icons/error.png");
                    this.lblTitle.Text = string.Format(Resources.strings.OrderRoutestephandler_failed, this.Entity.Number);
                    this.lblStatus.CssClass = "error";
                    break;
            }

            if (this.Entity.ErrorCodeAsEnum != OrderProcessingError.None || !this.Entity.ErrorText.Equals(OrderProcessingError.None.ToString(), StringComparison.InvariantCultureIgnoreCase))
            {
                this.lblErrorCode.CssClass = "error";
                this.lblErrorText.CssClass = "error";
            }

            this.lblNumber.Text = this.Entity.Number.ToString();
            this.lblGuid.Text = this.Entity.Guid;
            this.hlOrderId.Text = this.Entity.OrderId.ToString();
            this.hlOrderId.NavigateUrl = ResolveUrl(string.Format("~/Order.aspx?id={0}", this.Entity.OrderId));
            if (this.Entity.TerminalId.HasValue)
            {
                this.hlTerminalId.Text = this.Entity.TerminalEntity.Name;
                this.hlTerminalId.NavigateUrl = ResolveUrl(string.Format("~/Terminal.aspx?id={0}", this.Entity.TerminalId.Value));
            }
            this.lblHandlerType.Text = this.Entity.HandlerTypeAsEnum.ToString();
            this.lblStatus.Text = string.Format("{0} ({1})", this.Entity.Status, this.Entity.StatusAsEnum);
            this.lblErrorCode.Text = string.Format("{0} ({1})", this.Entity.ErrorCode, this.Entity.ErrorCodeAsEnum);
            this.lblErrorText.Text = HttpUtility.HtmlEncode(this.Entity.ErrorText);
            this.lblRoutingLog.Text = HttpUtility.HtmlEncode(this.Entity.OrderEntity.RoutingLog).ReplaceLineBreakWithBR();
        }
    }
}