﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/CraveMobileNocBase.master" AutoEventWireup="true" Inherits="Incidents" CodeBehind="Incidents.aspx.cs"  %>
<%@ Reference VirtualPath="~/UserControls/Listitems/Order.ascx" %>
<%@ Reference VirtualPath="~/UserControls/Listitems/Client.ascx" %>
<%@ Reference VirtualPath="~/UserControls/Listitems/Terminal.ascx" %>
<%@ Reference VirtualPath="~/UserControls/Listitems/ClientsSummary.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    
<D:PlaceHolder ID="plhNoResults" runat="server" Visible="false">
    <div class="noResultsFound">
        <D:LabelTextOnly runat="server" ID="lblNoResults" Text="<%$ Resources:strings, No_issues_found %>"> </D:LabelTextOnly>
    </div>
</D:PlaceHolder>

<!-- Non routed orders -->
<D:PlaceHolder ID="plhNonRoutedOrders" runat="server" Visible="false">
<h1 class="error">
    <span>
        <D:LabelTextOnly ID="lblNonRoutedOrders" runat="server" Text="<%$ Resources:strings, Non_routed_orders %>"></D:LabelTextOnly> (<D:LabelTextOnly ID="lblNonRoutedOrderCount" runat="server"></D:LabelTextOnly>)
    </span>
</h1>
<ul class="list">
    <D:PlaceHolder ID="plhNonRoutedOrdersList" runat="server"></D:PlaceHolder>
</ul>
</D:PlaceHolder>

<!-- Non started routes orders -->
<D:PlaceHolder ID="plhNonStartedRoutesOrders" runat="server" Visible="false">
<h1 class="error">
    <span>
        <D:LabelTextOnly ID="lblNonStartedRoutesOrders" runat="server" Text="<%$ Resources:strings, Non_started_routes_orders %>"></D:LabelTextOnly> (<D:LabelTextOnly ID="lblNonStartedRoutesOrderCount" runat="server"></D:LabelTextOnly>)
    </span>
</h1>
<ul class="list">
    <D:PlaceHolder ID="plhNonStartedRoutesOrdersList" runat="server"></D:PlaceHolder>
</ul>
</D:PlaceHolder>

<!-- Expired orders -->
<D:PlaceHolder ID="plhExpiredOrders" runat="server" Visible="false">
<h1 class="error">
    <span>
        <D:LabelTextOnly ID="lblExpiredOrders" runat="server" Text="<%$ Resources:strings, Expired_orders %>"></D:LabelTextOnly> (<D:LabelTextOnly ID="lblExpiredOrderCount" runat="server"></D:LabelTextOnly>)
    </span>
</h1>
<ul class="list">
    <D:PlaceHolder ID="plhExpiredOrdersList" runat="server"></D:PlaceHolder>
</ul>
</D:PlaceHolder>
    
<!-- Expired retrieved orders -->
<D:PlaceHolder ID="plhExpiredRetrievedOrders" runat="server" Visible="false">
<h1 class="error">
    <span>
        <D:LabelTextOnly ID="lblExpiredRetrievedOrders" runat="server" Text="<%$ Resources:strings, Expired_retrieved_orders %>"></D:LabelTextOnly> (<D:LabelTextOnly ID="lblExpiredRetrievedOrdersCount" runat="server"></D:LabelTextOnly>)
    </span>
</h1>
<ul class="list">
    <D:PlaceHolder ID="plhExpiredRetrievedOrdersList" runat="server"></D:PlaceHolder>
</ul>
</D:PlaceHolder>

<!-- Unprocessable orders -->
<D:PlaceHolder ID="plhUnprocessableOrders" runat="server" Visible="false">
<h1 class="error">
    <span>
        <D:LabelTextOnly ID="lblUnprocessableOrders" runat="server" Text="<%$ Resources:strings, Unprocessable_orders %>" ></D:LabelTextOnly> (<D:LabelTextOnly ID="lblUnprocessableOrderCount" runat="server"></D:LabelTextOnly>)
    </span>
</h1>
<ul class="list">
    <D:PlaceHolder ID="plhUnprocessableOrdersList" runat="server"></D:PlaceHolder>
</ul>
</D:PlaceHolder>

<!-- Non ordering clients summaries -->
<D:PlaceHolder ID="plhNonOrderingClientsSummaries" runat="server" Visible="false">
<h1 class="error">
    <span>
        <D:LabelTextOnly ID="lblNonOrderingClients" runat="server" Text="<%$ Resources:strings, Non_ordering_clients %>"></D:LabelTextOnly> (<D:LabelTextOnly ID="lblNonOrderingClientsSummaryCount" runat="server"></D:LabelTextOnly>)
    </span>
</h1>
<ul class="list">
    <D:PlaceHolder ID="plhNonOrderingClientsSummaryList" runat="server"></D:PlaceHolder>
</ul>
</D:PlaceHolder>

<!-- Offline terminals -->
<D:PlaceHolder ID="plhOfflineTerminals" runat="server" Visible="false">
<h1 class="error">
    <span>
        <D:LabelTextOnly ID="lblOfflineTerminals" runat="server" Text="<%$ Resources:strings, Offline_terminals %>"></D:LabelTextOnly> (<D:LabelTextOnly ID="lblOfflineTerminalCount" runat="server"></D:LabelTextOnly>)
    </span>
</h1>
<ul class="list">
    <D:PlaceHolder ID="plhOfflineTerminalsList" runat="server"></D:PlaceHolder>
</ul>
</D:PlaceHolder>

<!-- Companies with too much offline clients -->
<D:PlaceHolder ID="plhCompaniesWithTooMuchOfflineClients" runat="server" Visible="false">
<h1 class="error">
    <span>
        <D:LabelTextOnly ID="lblCompaniesWithTooMuchOfflineClients" runat="server" Text="<%$ Resources:strings, Companies_with_too_much_offline_clients %>"></D:LabelTextOnly> (<D:LabelTextOnly ID="lblCompaniesWithTooMuchOfflineClientsCount" runat="server"></D:LabelTextOnly>)
    </span>
</h1>
<ul class="list">
    <D:PlaceHolder ID="plhCompaniesWithTooMuchOfflineClientsList" runat="server"></D:PlaceHolder>
</ul>
</D:PlaceHolder>

<!-- Outdated terminals -->
<D:PlaceHolder ID="plhOutdatedTerminals" runat="server" Visible="false">
<h1 class="warning">
    <span>
        <D:LabelTextOnly ID="lblOutdatedTerminals" runat="server" Text="<%$ Resources:strings, Outdated_terminals %>"></D:LabelTextOnly> (<D:LabelTextOnly ID="lblOutdatedTerminalCount" runat="server"></D:LabelTextOnly>)
    </span>
</h1>
<ul class="list">
    <D:PlaceHolder ID="plhOutdatedTerminalsList" runat="server"></D:PlaceHolder>
</ul>
</D:PlaceHolder>

</asp:Content>

