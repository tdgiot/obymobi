﻿using Dionysos.Web;
using System;
using System.Web;

namespace Obymobi.Noc.WebApplication
{
    public partial class _404 : Dionysos.Web.UI.PageDefault
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            btnBack.Attributes.Add("onClick", "javascript:history.back(); return false;");
            plhLogo.AddHtml("<a id=\"logo\" class=\"logo\" href=\"{0}\"></a>", ResolveUrl("~"));

            // Get the exception
            HttpApplicationState contents = HttpContext.Current.Application.Contents;
            Exception lastError = (Exception)contents.Get("Exception");
            if (lastError != null)
            {
                contents.Remove("Exception");
            }

            if (lastError != null && lastError.GetBaseException() != null)
            {
                lastError = lastError.GetBaseException();
            }

            if (lastError != null)
            {
                // Set the exception to the session
                SessionHelper.SetValue("LastError", lastError);
            }

            if (SessionHelper.TryGetValue("LastError", out Exception ex))
            {
                plhMessage.AddHtml(GetMessage(ex));
            }
        }

        private string GetMessage(Exception ex) => string.Format("<b>Message:</b> {0} <br /><br />", ex.Message);
    }
}