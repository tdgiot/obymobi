﻿using System;
using System.Collections.Generic;
using Obymobi.Logic.HelperClasses;
using Obymobi.Data.CollectionClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Dionysos.Web;
using Obymobi.Data;
using Obymobi.Enums;

public partial class Terminals : PageListview<TerminalEntity>
{
    #region Methods

    private List<int> GetCurrentCompanyId()
    {
        if (QueryStringHelper.HasValue("cid"))
            return new List<int> { QueryStringHelper.GetInt("cid") };

        if (CraveMobileNocHelper.AvailableCompanyIds.Count > 0)
            return CraveMobileNocHelper.AvailableCompanyIds;

        return CraveMobileNocHelper.EmptyIntList;
    }

    #region Base class methods

    protected override EntityView<TerminalEntity> GetEntityViewFromSession()
    {
        return null; //Session[this.SessionKeyEntityView] as EntityView<TerminalEntity>;
    }

    protected override void GetFilterAndSortFromSession()
    {
        object filter = Session[this.SessionKeyFilter];
        if (filter != null)
            this.ddlFilter.SelectedIndex = (int)filter;

        object sort = Session[this.SessionKeySort];
        if (sort != null)
            this.ddlSort.SelectedIndex = (int)sort;
    }

    protected override EntityView<TerminalEntity> InitializeEntityView()
    {
        if (!IsPostBack)
            this.GetFilterAndSortFromSession();

        // Get the terminals
        PredicateExpression filter = new PredicateExpression();
        filter.Add(CompanyFields.UseMonitoring == true);
        filter.Add(TerminalFields.UseMonitoring == true);
        filter.Add(TerminalFields.CompanyId == GetCurrentCompanyId());

        if (this.ddlFilter.SelectedIndex == 1)
        {
            filter.Add(DeviceFields.LastRequestUTC > DateTime.UtcNow.AddMinutes(-2));
        }
        else if (this.ddlFilter.SelectedIndex == 2)
        {
            filter.Add(DeviceFields.LastRequestUTC <= DateTime.UtcNow.AddMinutes(-2));
        }

        // Create the relations
        RelationCollection relations = new RelationCollection();
        relations.Add(TerminalEntityBase.Relations.CompanyEntityUsingCompanyId);
        relations.Add(TerminalEntityBase.Relations.DeviceEntityUsingDeviceId);

        // Create the sort
        SortExpression sort = new SortExpression();
        sort.Add(new SortClause(DeviceFields.LastRequestUTC, SortOperator.Descending));

        PrefetchPath prefetch = new PrefetchPath(EntityType.TerminalEntity);
        prefetch.Add(TerminalEntityBase.PrefetchPathDeviceEntity);

        // TODO, only certain fields?

        // Get the terminals
        TerminalCollection terminals = new TerminalCollection();
        terminals.GetMulti(filter, 0, sort, relations, prefetch);

        // Return the view
        return terminals.DefaultView;
    }

    protected override void SetFilter()
    {
        // Filter
        /*switch (this.ddlFilter.SelectedIndex)
        {
            case 0:
                // All terminals
                this.EntityView.Filter = null;
                break;
            case 1:
                // Online terminals
                this.EntityView.Filter = new PredicateExpression(new EntityProperty("LastActivityUTC") > DateTime.UtcNow.AddMinutes(-2));
                break;
            case 2:
                // Offline terminals
                PredicateExpression offlineFilter = new PredicateExpression();
                offlineFilter.Add(new EntityProperty("LastActivityUTC") <= DateTime.UtcNow.AddMinutes(-2));
                offlineFilter.AddWithOr(DeviceFields.LastRequestUTC == DBNull.Value);
                this.EntityView.Filter = offlineFilter;
                break;
        }*/

        // Store the current filter in the session
        Session[this.SessionKeyFilter] = this.ddlFilter.SelectedIndex;
    }

    protected override void SetSort()
    {
        // Sort
        switch (this.ddlSort.SelectedIndex)
        {
            case 0:
                // Last request, descending
                this.EntityView.Sorter = new SortExpression(new SortClause(DeviceFields.LastRequestUTC, SortOperator.Descending));
                break;
            case 1:
                // Status, ascending
                this.EntityView.Sorter = new SortExpression(new SortClause(TerminalFields.LastStatus, SortOperator.Ascending));
                break;
            case 2:
                // Version, descending
                this.EntityView.Sorter = new SortExpression(new SortClause(DeviceFields.ApplicationVersion, SortOperator.Descending));
                break;
        }

        // Store the current sort in the session
        Session[this.SessionKeySort] = this.ddlSort.SelectedIndex;
    }

    protected override void RenderList()
    {
        if (this.Count == 1)
            this.lblTitle.Text = Resources.strings.One_item;
        else
            this.lblTitle.Text = string.Format(Resources.strings.X_items, this.Count);

        foreach (var terminal in this.EntityView)
        {
            UserControls_Listitems_Terminal terminalControl = this.LoadControl<UserControls_Listitems_Terminal>("~/UserControls/Listitems/Terminal.ascx");
            terminalControl.Terminal = terminal;
            this.plhList.Controls.Add(terminalControl);
        }
    }

    #endregion

    #endregion

    #region Properties

    public override string SessionKey
    {
        get { return "Terminals"; }
    }

    #endregion

    #region Event handlers

    protected void Page_Load(object sender, EventArgs e)
    {
        this.Title = string.Format("{0} - {1}", Resources.strings.Terminals, Resources.strings.Crave_NOC);
        this.Initialize();
    }

    #endregion
}