﻿using System;
using Dionysos.Web.UI;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Dionysos.Data.LLBLGen;
using Dionysos.Web;
using Dionysos;
using Obymobi;
using Obymobi.Data;
using Obymobi.Data.EntityClasses;

public class PageEntity<TEntity> : PageDefault where TEntity : EntityBase, IEntity
{
    #region Fields

    private TEntity entity;

    #endregion

    #region Methods

    public virtual void InitializeEntity()
    {
        if (QueryStringHelper.HasValue("id"))
        {
            int id = QueryStringHelper.GetInt("id");

            string entityName;
            if (QueryStringHelper.HasValue("entity") && typeof(TEntity) == typeof(CommonEntityBase))
            {
                entityName = QueryStringHelper.GetString("entity");
            }
            else
            {
                Type type = typeof(TEntity);
                entityName = LLBLGenUtil.GetEntityNameFromTypeString(type.ToString());
            }

            this.entity = DataFactory.EntityFactory.GetEntity(entityName, id) as TEntity;

            if (this.entity == null || this.entity.IsNew)
            {
                Response.Redirect("~/Error.aspx?code=" + (int)ErrorCodes.NoContent);
            }
            else if (CraveMobileNocHelper.IsAuthenticated)
            {
                var relatedEntity = this.entity as ICompanyRelatedEntity;
                if (relatedEntity != null && relatedEntity.CompanyId > 0)
                {
                    // This method will redirect us to Error.aspx
                    CraveMobileNocHelper.CheckAuthorization(relatedEntity.CompanyId);
                }
            }
        }
    }

    /// <summary>
    /// Get a link to the CMS
    /// </summary>
    /// <param name="deepLink">Link to EntityPage (i.e. 'Company/Client.aspx?id=1')</param>
    /// <param name="companyId"></param>
    /// <param name="args"></param>
    /// <returns></returns>
    protected string GetCmsUrl(int companyId, String deepLink, params object[] args)
    {
        deepLink = deepLink.FormatSafe(args);
        if(deepLink.Contains("?"))
            return string.Format("{0}/{1}&switchToCompany={2}", WebEnvironmentHelper.GetManagementUrl(), deepLink, companyId);
        
        return string.Format("{0}/{1}?switchToCompany={2}", WebEnvironmentHelper.GetManagementUrl(), deepLink, companyId);
    }

    #endregion

    #region Properties

    public TEntity Entity
    {
        get
        {
            return this.entity;
        }
        set
        {
            this.entity = value;
        }
    }

    #endregion

}