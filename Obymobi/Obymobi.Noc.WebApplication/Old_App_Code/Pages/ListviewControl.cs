﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Dionysos.Web.UI;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System.Web.UI.WebControls;

public abstract class ListviewControl<TEntity> : System.Web.UI.UserControl where TEntity : EntityBase, IEntity
{
    #region Fields

    private EntityView<TEntity> entityView = null;

    #endregion

    #region Methods

    public void Initialize()
    {
        if (this.IsPostBack)
            this.EntityView = this.GetEntityViewFromSession();

        if (this.EntityView == null)
        {
            this.EntityView = this.InitializeEntityView();

            // Store the entity into the session
            this.Session[this.SessionKeyEntityView] = this.EntityView;
        }

        if (EntityView != null && this.EntityView.Count > 0)
        {
            if (!this.IsPostBack)
                this.GetFilterAndSortFromSession();

            // Set the filter
            this.SetFilter();

            // Set the sort
            this.SetSort();

            // Render the terminals
            this.RenderList();
        }
    }

    protected abstract EntityView<TEntity> GetEntityViewFromSession();

    protected abstract void GetFilterAndSortFromSession();

    protected abstract EntityView<TEntity> InitializeEntityView();

    protected abstract void SetFilter();

    protected abstract void SetSort();

    protected abstract void RenderList();

    #endregion

    #region Properties

    public EntityView<TEntity> EntityView
    {
        get
        {
            return this.entityView;
        }
        set
        {
            this.entityView = value;
        }
    }

    public abstract string SessionKey { get; }

    public string SessionKeyEntityView
    {
        get
        {
            return string.Format("{0}-EntityView", this.SessionKey);
        }
    }

    public string SessionKeyFilter
    {
        get
        {
            return string.Format("{0}-Filter", this.SessionKey);
        }
    }

    public string SessionKeySort
    {
        get
        {
            return string.Format("{0}-Sort", this.SessionKey);
        }
    }

    public int Count
    {
        get
        {
            return (this.EntityView != null ? this.EntityView.Count : 0);
        }
    }

    #endregion
}