﻿using System.Collections.Specialized;
using System.Web;
using Dionysos.Web.UI;
using SD.LLBLGen.Pro.ORMSupportClasses;

public abstract class PageListview<TEntity> : PageDefault where TEntity : EntityBase, IEntity
{
    #region Fields

    public PageListview()
    {
        EntityView = null;
    }

    #endregion

    #region Methods

    public void Initialize()
    {
        if (IsPostBack)
            this.EntityView = this.GetEntityViewFromSession();

        if (this.EntityView == null)
        {
            this.EntityView = this.InitializeEntityView();

            // Store the entity into the session
            Session[this.SessionKeyEntityView] = this.EntityView;
        }

        if (EntityView != null)
        {
            if (!IsPostBack)
                this.GetFilterAndSortFromSession();

            // Set the filter
            this.SetFilter();

            // Set the sort
            this.SetSort();

            // Render the terminals
            this.RenderList();
        }
    }

    protected abstract EntityView<TEntity> GetEntityViewFromSession();

    protected abstract void GetFilterAndSortFromSession();

    protected abstract EntityView<TEntity> InitializeEntityView();

    protected abstract void SetFilter();

    protected abstract void SetSort();

    protected abstract void RenderList();

    #endregion

    #region Properties

    public EntityView<TEntity> EntityView { get; set; }

    public abstract string SessionKey { get; }

    public string SessionKeyEntityView
    {
        get
        {
            return string.Format("{0}-EntityView", this.SessionKey);
        }
    }

    public string SessionKeyFilter
    {
        get
        {
            return string.Format("{0}-Filter", this.SessionKey);
        }
    }

    public string SessionKeySort
    {
        get
        {
            return string.Format("{0}-Sort", this.SessionKey);
        }
    }

    public int Count
    {
        get
        {
            return (this.EntityView != null ? this.EntityView.Count : 0);
        }
    }

    #endregion
}