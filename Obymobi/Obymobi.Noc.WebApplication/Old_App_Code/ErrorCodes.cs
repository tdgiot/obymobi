﻿/// <summary>
/// ErrorCodes used in NOC.
/// </summary>
/// <remarks>
/// Fun fact: The error code ids come from HTTP status codes.
/// </remarks>
public enum ErrorCodes
{
    NoContent = 204,
	Unauthorized = 401,
    NotFound = 404
}