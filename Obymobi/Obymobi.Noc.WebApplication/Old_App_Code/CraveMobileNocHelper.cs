﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Dionysos;
using Dionysos.Web;
using Dionysos.Web.Security;
using Obymobi.Data.EntityClasses;
using Obymobi.Enums;

public class CraveMobileNocHelper
{
    #region Methods

    public static void ClearSession()
    {
        SessionHelper.RemoveValue("CurrentCompanyId");
        SessionHelper.RemoveValue("AvailableCompanyIds");

        UserManager.RefreshCurrentUser();
    }

    /// <summary>
    /// Gets a System.String instance representing the timespan between the specified date and now (UTC)
    /// </summary>
    /// <param name="date">The date to get the timespan till now from</param>
    /// <returns>A System.String instance containing the timespan representation</returns>
    public static string GetSpanTextBetweenDateTimeAndUtcNow(DateTime date)
    {
        return GetSpanTextBetweenDateTimes(date, DateTime.UtcNow);
    }

    /// <summary>
    /// Gets a System.String instance representing the timespan between the specified date and now
    /// </summary>
    /// <returns>A System.String instance containing the timespan representation</returns>
    public static string GetSpanTextBetweenDateTimes(DateTime date1, DateTime date2)
    {
        string spanText;

        TimeSpan timeSpan = DateTimeUtil.GetSpanBetweenDateTimes(date1, date2);

        if (date1 > date2)
            spanText = "Future";
        else if (timeSpan.TotalDays > 1)
            spanText = string.Format("{0} {1}", (int)timeSpan.TotalDays, "days");
        else if (timeSpan.TotalHours > 1)
            spanText = string.Format("{0} {1}", (int)timeSpan.TotalHours, "hours");
        else if (timeSpan.TotalMinutes > 0)
            spanText = string.Format("{0} {1}", (int)timeSpan.TotalMinutes, "minutes");
        else
            spanText = string.Format("{0} {1}", (int)timeSpan.TotalSeconds, "seconds");

        return spanText;
    }

    public static DateTime ConvertUtcDateTimeToUserTimeZone(DateTime date)
    {
        return date.UtcToLocalTime(CurrentUser.TimeZoneInfo);
    }

    public static bool CheckAuthorization(int companyId)
    {
        if (!IsAdministrator)
        {
            var companyIdsList = AvailableCompanyIds;
            if (companyIdsList.Count > 0 && !companyIdsList.Contains(companyId))
            {
                HttpContext.Current.Response.Redirect("~/Error.aspx?code=" + (int)ErrorCodes.Unauthorized);
                return false;
            }
        }

        return true;
    }

    #endregion

    #region Properties

    public static bool IsAuthenticated
    {
        get
        {
            if (HttpContext.Current == null || HttpContext.Current.User == null || HttpContext.Current.User.Identity == null)
                return false;

            return HttpContext.Current.User.Identity.IsAuthenticated;
        }
    }

    public static UserEntity CurrentUser
    {
        get
        {
            return UserManager.CurrentUser as UserEntity;
        }
    }

    public static readonly List<int> EmptyIntList = new List<int>(0); 
    public static List<int> AvailableCompanyIds
    {
        get
        {
            if (IsAdministrator)
            {
                return EmptyIntList;
            }

            UserEntity currentUser = CurrentUser;
            if (currentUser == null)
            {
                throw new Exception("Not logged in? wtf?");
            }
            if (currentUser.AccountId <= 0 || currentUser.AccountEntity.AccountCompanyCollection.Count == 0)
            {
                throw new Exception("User is no Admin and is not assigned to an account or account is not linked to any companies. (Should've been caught during login)");
            }

            List<int> companyIds;
            if (!SessionHelper.TryGetValue("AvailableCompanyIds", out companyIds))
            {
                companyIds = CurrentUser.AccountEntity.AccountCompanyCollection.Select(companyEntity => companyEntity.CompanyId).ToList();

                SessionHelper.SetValue("AvailableCompanyIds", companyIds);
            }

            return companyIds;
        }
        set
        {
            SessionHelper.SetValue("AvailableCompanyIds", value);
        }
    }
    
    public static bool IsAdministrator
    {
        get
        {
            var currentUser = CurrentUser;
            return currentUser != null && currentUser.IsAdministrator;
        }
    }

    #endregion
}