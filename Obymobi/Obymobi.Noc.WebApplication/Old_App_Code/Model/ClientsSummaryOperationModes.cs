﻿using System.Data;

public class ClientsSummaryOperationModes : ClientsSummaryBase
{
    public ClientsSummaryOperationModes(int companyId, string companyName, DataTable operationModes) : base(companyId, companyName)
    {
        this.OperationModes = operationModes;
    }

    public DataTable OperationModes { get; set; }
}