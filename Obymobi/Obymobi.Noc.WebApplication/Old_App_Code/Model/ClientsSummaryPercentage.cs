﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

public class ClientsSummaryPercentage : ClientsSummaryBase
{
    public ClientsSummaryPercentage(int companyId, string companyName, int percentage) : base(companyId, companyName)
    {
        this.Percentage = percentage;
    }

    public int Percentage { get; set; }
}