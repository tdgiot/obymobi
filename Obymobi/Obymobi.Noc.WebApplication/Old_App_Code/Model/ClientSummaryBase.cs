﻿public class ClientsSummaryBase
{
    public ClientsSummaryBase(int companyId, string companyName)
    {
        this.CompanyId = companyId;
        this.CompanyName = companyName;
    }

    public int CompanyId { get; set; }

    public string CompanyName { get; set; }
}