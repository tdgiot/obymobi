﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using Dionysos.Web;

public static class QueryStringHelperExtensions
{
    /// <summary>
    /// Adds the range.
    /// </summary>
    /// <param name="qsh">The query string helper.</param>
    /// <param name="queryString">The query string.</param>
    /// <param name="additionalKeyWhitelist">The additional key whitelist.</param>
    /// <returns>
    /// The query string helper with all whilelisted keys of the query string added.
    /// </returns>
    public static QueryStringHelper AddRange(this QueryStringHelper qsh, NameValueCollection queryString, params string[] additionalKeyWhitelist)
    {
        if (qsh == null) throw new ArgumentNullException("qsh");
        if (queryString == null) throw new ArgumentNullException("queryString");
        string[] keyWhitelist = new string[] { "id" };

        foreach (string key in queryString.AllKeys)
        {
            if (keyWhitelist.Contains(key, StringComparer.OrdinalIgnoreCase) ||
                (additionalKeyWhitelist != null && additionalKeyWhitelist.Contains(key, StringComparer.OrdinalIgnoreCase)))
            {
                qsh.AddItem(key, queryString[key]);
            }
        }

        return qsh;
    }
}