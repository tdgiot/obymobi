﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Dionysos;
using Dionysos.Web;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data;
using Obymobi.Security;
using Obymobi.Web.HelperClasses;
using Obymobi.Noc.Logic.HelperClasses;
using Resources;

public partial class DeliveryAppless : PageListview<OutletEntity>
{
    protected override EntityView<OutletEntity> GetEntityViewFromSession() => Session[SessionKeyEntityView] as EntityView<OutletEntity>;
    protected string EmptyResultText = Resources.strings.No_Outlet_Found;

    protected override void GetFilterAndSortFromSession()
    {
    }

    protected override EntityView<OutletEntity> InitializeEntityView()
    {
        List<int> companyIds = GetCurrentCompanyId();
        var outletCollection = NocHelper.GetOutlets(companyIds);
        return outletCollection.DefaultView;
    }

    protected override void SetFilter()
    {
    }

    protected override void SetSort()
    {
    }

    protected override void RenderList()
    {
        outletApplessGrid.Visible = EntityView.Any();

        foreach (OutletEntity outlet in EntityView)
        {
            UserControls_Listitems_OutletWaitTime outletControl = LoadControl<UserControls_Listitems_OutletWaitTime>("~/UserControls/Listitems/OutletWaitTime.ascx");
            outletControl.Outlet = outlet;

            plhList.Controls.Add(outletControl);
        }
    }

    public override string SessionKey => "Outlets";

    protected void Page_Load(object sender, EventArgs e)
    {
        if(GetCurrentCompanyId().Count != 1)
        {
            Response.Redirect("~/companies.aspx");
        }

        this.AppLessSaveButton.Click += ApplessSaveClick;
        this.Title = string.Format("{0} - {1}", Resources.strings.Delivery, Resources.strings.Crave_NOC);


        this.Initialize();     
    }

    #region Rendering content
    
    private List<int> GetCurrentCompanyId()
    {
        if (QueryStringHelper.HasValue("cid"))
            return new List<int> { QueryStringHelper.GetInt("cid") };

        if (CraveMobileNocHelper.AvailableCompanyIds.Count > 0)
            return CraveMobileNocHelper.AvailableCompanyIds;

        return CraveMobileNocHelper.EmptyIntList;
    }

    #endregion

    #region Event Handlers

    public void ApplessSaveClick(object sender, EventArgs e)
    {
        plhList.Controls.OfType<UserControls_Listitems_OutletWaitTime>().ToList().ForEach(control => control.Save());
        this.AddInformator(Dionysos.Web.UI.InformatorType.Information, "Your Mobile Ordering Wait time has now been updated");
    }

    #endregion
}