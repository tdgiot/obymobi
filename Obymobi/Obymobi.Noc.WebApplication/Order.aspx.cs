﻿using System;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Crave.Ordering.Api.Client;
using Dionysos;
using Dionysos.Web;
using Obymobi;
using Obymobi.Cms.Logic.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Enums;
using Obymobi.Logic.Routing;
using Obymobi.Noc.Logic.HelperClasses;
using ServiceStack;
using CheckoutType = Obymobi.Enums.CheckoutType;
using OrderStatus = Obymobi.Enums.OrderStatus;
using PaymentTransactionStatus = Obymobi.Enums.PaymentTransactionStatus;

public partial class Order : PageEntity<OrderEntity>
{
    private HttpClient httpClient = new HttpClient();

    #region Event handlers

    protected void Page_Load(object sender, EventArgs e)
    {
        this.InitializeEntity();

        // Hookup the events
        QueryStringHelper qs = new QueryStringHelper(string.Format("?ProcessOrderId={0}", this.Entity.OrderId));
        this.hlProcessOrder.NavigateUrl = ResolveUrl(qs.MergeQuerystringWithRawUrl());

        if (this.Entity != null)
        {
            this.Title = string.Format("{0} '{1} - {2}' - {3}", Resources.strings.Order, this.Entity.CompanyName, this.Entity.DeliverypointNumber, Resources.strings.Crave_NOC);

            switch (this.Entity.StatusAsEnum)
            {
                case OrderStatus.NotRouted:
                    this.imgOrder.ImageUrl = ResolveUrl("~/Images/Icons/exclamation.png");
                    this.lblTitle.Text = string.Format(Resources.strings.Order_could_not_be_routed, this.Entity.CompanyName, this.Entity.DeliverypointNumber);
                    this.lblStatus.CssClass = "error";
                    this.hlProcessOrder.Visible = true;
                    break;
                case OrderStatus.WaitingForPayment:
                    this.imgOrder.ImageUrl = ResolveUrl("~/Images/Icons/hourglass.png");
                    this.lblTitle.Text = string.Format(Resources.strings.Order_is_waiting_for_payment, this.Entity.CompanyName, this.Entity.DeliverypointNumber);
                    this.hlProcessOrder.Visible = true;
                    break;
                case OrderStatus.Routed:
                    this.imgOrder.ImageUrl = ResolveUrl("~/Images/Icons/error.png");
                    this.lblTitle.Text = string.Format(Resources.strings.Order_is_routed_but_not_started, this.Entity.CompanyName, this.Entity.DeliverypointNumber);
                    this.lblStatus.CssClass = "error";
                    this.hlProcessOrder.Visible = true;
                    break;
                case OrderStatus.InRoute:
                    this.imgOrder.ImageUrl = ResolveUrl("~/Images/Icons/hourglass.png");
                    this.lblTitle.Text = string.Format(Resources.strings.Order_is_currently_being_processed, this.Entity.CompanyName, this.Entity.DeliverypointNumber);
                    this.hlProcessOrder.Visible = true;
                    break;
                case OrderStatus.WaitingForPaymentCompleted:
                    this.imgOrder.ImageUrl = ResolveUrl("~/Images/Icons/hourglass.png");
                    this.lblTitle.Text = string.Format(Resources.strings.Order_is_waiting_for_payment_complete, this.Entity.CompanyName, this.Entity.DeliverypointNumber);
                    this.hlProcessOrder.Visible = true;
                    break;
                case OrderStatus.Processed:
                    this.imgOrder.ImageUrl = ResolveUrl("~/Images/Icons/checkmark.png");
                    if (this.Entity.ErrorCode == 0)
                    {
                        this.lblTitle.Text = string.Format(Resources.strings.Order_is_successfully_processed, this.Entity.CompanyName, this.Entity.DeliverypointNumber);
                    }
                    else
                    {
                        this.lblTitle.Text = string.Format(Resources.strings.Order_is_manually_processed, this.Entity.CompanyName, this.Entity.DeliverypointNumber);
                        this.lblErrorCode.CssClass = "error";
                    }
                    break;
                case OrderStatus.Unprocessable:
                    this.imgOrder.ImageUrl = ResolveUrl("~/Images/Icons/error.png");
                    this.lblTitle.Text = string.Format(Resources.strings.Order_could_not_be_processed, this.Entity.CompanyName, this.Entity.DeliverypointNumber);
                    this.lblStatus.CssClass = "error";
                    this.hlProcessOrder.Visible = true;
                    this.lblErrorCode.CssClass = "error";
                    break;
            }

            if (this.hlProcessOrder.Visible)
            {
                // Set onclick warning for Manually Process
                this.hlProcessOrder.Attributes.Add("onclick", string.Format("return confirmLinkClick(event, '{0}', '{1}')", Resources.strings.Generic_are_you_sure_manually_process.Replace("'", "\'"), ResolveUrl(qs.MergeQuerystringWithRawUrl())));
            }

            if (this.Entity.MasterOrderId.HasValue)
            {
                this.plhMasterOrder.Visible = true;
                this.hlMasterOrderId.Text = this.Entity.MasterOrderId.ToString();
                this.hlMasterOrderId.NavigateUrl = ResolveUrl(string.Format("~/Order.aspx?id={0}", this.Entity.MasterOrderId));
                this.hlMasterOrderId.CssClass = "error";
            }

            this.lblTitle.Text += string.Format(" (ID: {0})", this.Entity.OrderId);
            this.lblOrderId.Text = this.Entity.OrderId.ToString();
            this.lblClientId.Text = this.Entity.ClientId.ToString();
            this.hlCompany.Text = this.Entity.CompanyName;
            this.hlCompany.NavigateUrl = ResolveUrl(string.Format("~/Company.aspx?id={0}", this.Entity.CompanyId));
            this.lblDeliverypointNumber.Text = this.Entity.DeliverypointNumber;
            this.plhDeliverypoint.Visible = !this.Entity.DeliverypointNumber.IsNullOrWhiteSpace();
            this.plhChargeToRoom.Visible = !this.Entity.ChargeToDeliverypointNumber.IsNullOrWhiteSpace();
            this.lblExpired.Text = RoutingHelper.IsOrderExpired(this.Entity, false).ToString();
            this.lblStatus.Text = string.Format("{0} ({1})", this.Entity.StatusText, this.Entity.Status);
            this.lblErrorCode.Text = string.Format("{0} ({1})", this.Entity.ErrorCodeAsEnum, this.Entity.ErrorCode);
            this.lblCreated.Text = string.Format("{0} ({1})", CraveMobileNocHelper.ConvertUtcDateTimeToUserTimeZone(this.Entity.CreatedUTC.GetValueOrDefault()), CraveMobileNocHelper.GetSpanTextBetweenDateTimeAndUtcNow(this.Entity.CreatedUTC.GetValueOrDefault()));
            if (this.Entity.UpdatedUTC.HasValue)
                this.lblUpdated.Text = string.Format("{0} ({1})", CraveMobileNocHelper.ConvertUtcDateTimeToUserTimeZone(this.Entity.UpdatedUTC.GetValueOrDefault()), CraveMobileNocHelper.GetSpanTextBetweenDateTimeAndUtcNow(this.Entity.UpdatedUTC.Value));

            if (this.Entity.CreatedUTC.HasValue && this.Entity.StatusAsEnum != OrderStatus.Processed)
            {
                this.lblProcessingTimeLabel.Text = Resources.strings.Order_age + ":";
                this.lblProcessingTime.Text = CraveMobileNocHelper.GetSpanTextBetweenDateTimeAndUtcNow(this.Entity.CreatedUTC.Value);
            }
            else if (this.Entity.CreatedUTC.HasValue && this.Entity.UpdatedUTC.HasValue)
            {
                this.lblProcessingTimeLabel.Text = Resources.strings.Order_processing_time + ":";
                this.lblProcessingTime.Text = CraveMobileNocHelper.GetSpanTextBetweenDateTimes(this.Entity.CreatedUTC.Value, this.Entity.UpdatedUTC.Value);
            }

            this.lblNotes.Text = this.Entity.Notes.Replace("\n", "<br/>");

            this.lblNumberOfCovers.Text = this.Entity.CoversCount.ToString();

            if (this.Entity.CheckoutMethodType.HasValue)
            {
                CheckoutType checkoutType = (CheckoutType)this.Entity.CheckoutMethodType.Value;

                this.lblOrderCheckoutMethodType.Text = checkoutType.GetStringValue();

                if (checkoutType == CheckoutType.ChargeToRoom)
                {
                    this.lblChargeToRoomNumber.Text = this.Entity.ChargeToDeliverypointNumber;
                    this.lblChargeToRoomDeliverypointgroup.Text = this.Entity.ChargeToDeliverypointgroupName;
                }
            }

            if (this.Entity.ServiceMethodType.HasValue)
            {
                ServiceMethodType serviceMethodType = (ServiceMethodType)this.Entity.ServiceMethodType.Value;

                this.lblOrderServiceMethodType.Text = serviceMethodType.GetStringValue();

                switch (serviceMethodType)
                {
                    case ServiceMethodType.RoomService:
                        this.lblDeliverypointNumberLabel.Text = Resources.strings.Deliverypoint_room;
                        break;
                    case ServiceMethodType.TableService:
                        this.lblDeliverypointNumberLabel.Text = Resources.strings.Deliverypoint_table;
                        break;
                }
            }

            if (this.Entity.OrderitemCollection != null && this.Entity.OrderitemCollection.Count > 0)
            {
                this.hlViewOrderitems.Visible = true;
                this.hlViewOrderitems.NavigateUrl = ResolveUrl(string.Format("~/Orderitems.aspx?oid={0}", this.Entity.OrderId));

                // Show orderitems
                foreach (var orderitem in this.Entity.OrderitemCollection)
                {
                    UserControls_Listitems_Orderitem orderitemControl = this.LoadControl<UserControls_Listitems_Orderitem>("~/UserControls/Listitems/Orderitem.ascx");
                    orderitemControl.Orderitem = orderitem;
                    this.plhOrderitemsList.Controls.Add(orderitemControl);
                }
            }
            else
            {
                this.plhOrderitems.Visible = false;
            }

            if (!this.Entity.CustomerLastname.IsNullOrWhiteSpace() || !this.Entity.CustomerPhonenumber.IsNullOrWhiteSpace() || !this.Entity.Email.IsNullOrWhiteSpace())
            {
                this.plhCustomer.Visible = true;

                this.lblCustomerName.Text = this.Entity.CustomerLastname;
                this.lblCustomerPhone.Text = this.Entity.CustomerPhonenumber;
                this.lblCustomerEmail.Text = this.Entity.Email;
            }

            if (this.Entity.DeliveryInformationId.HasValue)
            {
                this.RenderDeliveryInformation(this.Entity.DeliveryInformationEntity);
            }

            // Orderroutestephandlers
            if (this.Entity.OrderRoutestephandlerCollection.Count > 0)
            {
                foreach (OrderRoutestephandlerEntity orderRoutestephandler in this.Entity.OrderRoutestephandlerCollection)
                {
                    UserControls_Listitems_OrderRoutestephandler orderRoutestephandlerControl = this.LoadControl<UserControls_Listitems_OrderRoutestephandler>("~/UserControls/Listitems/OrderRoutestephandler.ascx");
                    orderRoutestephandlerControl.OrderRoutestephandler = orderRoutestephandler;
                    this.plhOrderRoutestephandlersList.Controls.Add(orderRoutestephandlerControl);
                }
            }
            else if (this.Entity.OrderRoutestephandlerHistoryCollection.Count > 0)
            {
                foreach (OrderRoutestephandlerHistoryEntity orderRoutestephandlerHistory in this.Entity.OrderRoutestephandlerHistoryCollection)
                {
                    UserControls_Listitems_OrderRoutestephandlerHistory orderRoutestephandlerHistoryControl = this.LoadControl<UserControls_Listitems_OrderRoutestephandlerHistory>("~/UserControls/Listitems/OrderRoutestephandlerHistory.ascx");
                    orderRoutestephandlerHistoryControl.OrderRoutestephandlerHistory = orderRoutestephandlerHistory;
                    this.plhOrderRoutestephandlersList.Controls.Add(orderRoutestephandlerHistoryControl);
                }
            }

            this.hlOpenInCms.NavigateUrl = this.GetCmsUrl(this.Entity.CompanyId, "Company/Order.aspx?id={0}", this.Entity.OrderId);

            // Routing Log
            if (this.Entity.ErrorCodeAsEnum != OrderProcessingError.None)
            {
                this.plhRoutingLog.Visible = true;
                this.lblRoutingLog.Text = this.Entity.RoutingLog.ReplaceLineBreakWithBR();
            }
            else
                this.plhRoutingLog.Visible = false;

            // Terminal Logs            
            var activities = OrderHelper.GetTerminalLogsForOrder(this.Entity.OrderId);
            this.lblTerminalsLogs.Text = string.Format(Resources.strings.Terminal_logs, activities.Count);
            foreach (var activity in activities)
            {
                UserControls_Listitems_DeviceActivityLog terminalLogControl = this.LoadControl<UserControls_Listitems_DeviceActivityLog>("~/UserControls/Listitems/DeviceActivityLog.ascx");
                terminalLogControl.Activity = activity;
                this.plhTerminalLogsList.Controls.Add(terminalLogControl);
            }

            // Master/Sub Orders
            this.hlOpenMainorder.Visible = (this.Entity.MasterOrderId.GetValueOrDefault(0) > 0);
            this.hlOpenMainorder.NavigateUrl = ResolveUrl(string.Format("~/Order.aspx?id={0}", this.Entity.MasterOrderId.GetValueOrDefault(0)));

            OrderCollection subOrders = NocHelper.GetSubOrders(this.Entity);
            if (subOrders.Count > 0)
            {
                this.plhSubOrders.Visible = true;

                foreach (var subOrder in subOrders)
                {
                    var orderControl = this.LoadControl<UserControls_Listitems_Order>("~/UserControls/Listitems/Order.ascx");
                    orderControl.ShowSubOrderWarning = false;
                    orderControl.Order = subOrder;
                    this.phlSubOrderList.Controls.Add(orderControl);
                }
            }

            RenderPaymentTransactions(this.Entity);

            if (CraveMobileNocHelper.CurrentUser.Role >= Role.Crave)
            {
                RenderPaymentTransactionSplits(this.Entity);
            }

            RenderOrderNotifications(this.Entity);
        }
    }

    private void RenderDeliveryInformation(DeliveryInformationEntity deliveryInformation)
    {
        this.lblBuildingName.Text = deliveryInformation.BuildingName;
        this.lblAddress.Text = deliveryInformation.Address;
        this.lblZipcode.Text = deliveryInformation.Zipcode;
        this.lblCity.Text = deliveryInformation.City;
        this.lblInstructions.Text = deliveryInformation.Instructions;

        this.plhDeliveryInformation.Visible = true;
    }

    private void RenderPaymentTransactions(OrderEntity entity)
    {
        PaymentTransactionCollection transactionCollection = entity.PaymentTransactionCollection;
        if (transactionCollection.Count == 0) return;

        this.plhPaymentTransactions.Visible = true;

        foreach (PaymentTransactionEntity paymentTransactionEntity in transactionCollection.OrderByDescending(e => e.CreatedUTC))
        {
            var control = this.LoadControl<UserControls_Listitems_PaymentTransaction>("~/UserControls/Listitems/PaymentTransaction.ascx");
            control.PaymentTransaction = paymentTransactionEntity;
            this.plhPaymentTransactionList.Controls.Add(control);
        }

        if (CraveMobileNocHelper.CurrentUser.Role == Role.GodMode &&
            this.Entity.StatusAsEnum == OrderStatus.WaitingForPayment &&
            transactionCollection.Any(x => x.Status == PaymentTransactionStatus.Pending))
        {
            this.lbForcePayment.Visible = true;
        }
    }

    private void RenderPaymentTransactionSplits(OrderEntity entity)
    {
        IOrderedEnumerable<PaymentTransactionSplitEntity> paymentSplits = entity.PaymentTransactionCollection
            .SelectMany(x => x.PaymentTransactionSplitCollection)
            .OrderByDescending(x => x.CreatedUTC);

        plhPaymentTransactionSplits.Visible = paymentSplits.Any();

        if (plhPaymentTransactionSplits.Visible)
        {
            foreach (PaymentTransactionSplitEntity paymentSplit in paymentSplits)
            {
                var control = this.LoadControl<UserControls_Listitems_PaymentTransactionSplit>("~/UserControls/Listitems/PaymentTransactionSplit.ascx");
                control.PaymentTransactionSplit = paymentSplit;
                this.plhPaymentTransactionSplitList.Controls.Add(control);
            }
        }
    }

    private void RenderOrderNotifications(OrderEntity entity)
    {
        OrderNotificationLogCollection logCollection = entity.OrderNotificationLogCollection;
        if (logCollection.Count == 0) return;

        this.plhOrderNotifications.Visible = true;

        foreach (OrderNotificationLogEntity orderNotificationLogEntity in logCollection)
        {
            var control = this.LoadControl<UserControls_Listitems_OrderNotificationLog>("~/UserControls/Listitems/OrderNotificationLog.ascx");
            control.OrderNotificationLog = orderNotificationLogEntity;
            this.plhOrderNotificationList.Controls.Add(control);
        }
    }

    protected void MarkOrderAsPaid(object sender, CommandEventArgs e)
    {
        Page.RegisterAsyncTask(new PageAsyncTask(UpdatePaymentStatusToPaid));
        Page.ExecuteRegisteredAsyncTasks();
    }

    public async Task UpdatePaymentStatusToPaid()
    {
        await OrderingClientFactory.GetPaymentStatusClient().UpdateStatusAsync(
            this.Entity.OrderId, Crave.Ordering.Api.Client.PaymentTransactionStatus.Paid);

        do
        {
            // Unfortunately have to wait here because the order status is not directly updated
            // after the above API call returns
            await Task.Delay(250);
            this.Entity.Refetch();
        } while (this.Entity.StatusAsEnum == OrderStatus.WaitingForPayment);

        Response.Redirect(this.Request.RawUrl);
    }

    #endregion
}