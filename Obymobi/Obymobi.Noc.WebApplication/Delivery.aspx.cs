﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Dionysos;
using Dionysos.Web;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data;
using Obymobi.Security;
using Obymobi.Web.HelperClasses;

public partial class Delivery : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        this.Title = string.Format("{0} - {1}", Resources.strings.Delivery, Resources.strings.Crave_NOC);

        this.hlSetOrderTime.Click += this.hlConfirmDeliveryTimeChangeYes_Click;

        this.RenderContent();
    }

    #region Rendering content

    public void RenderContent()
    {
        int minutes = 0;

        if (this.GetCurrentDeliverypointgroupId().Any())
        {
            DeliverypointgroupEntity deliverypointgroup = this.GetDeliverypointgroup(this.GetCurrentDeliverypointgroupId()[0]);
            if (!deliverypointgroup.IsNew)
            {
                minutes = deliverypointgroup.EstimatedDeliveryTime;
            }
        }
        else if (this.GetCurrentCompanyId().Any())
        {
            DeliverypointgroupCollection deliverypointgroups = this.GetDeliverypointgroupsByCompanyId(this.GetCurrentCompanyId());
            if (deliverypointgroups.Count > 0)
            {
                minutes = deliverypointgroups[0].EstimatedDeliveryTime;
            }
        }

        this.lblDeliveryCurrentTime.Text = string.Format(Resources.strings.Delivery_Minutes, minutes);
    }

    private List<int> GetCurrentDeliverypointgroupId()
    {
        if (QueryStringHelper.HasValue("dpgid"))
            return new List<int> { QueryStringHelper.GetInt("dpgid") };

        return CraveMobileNocHelper.EmptyIntList;
    }

    private List<int> GetCurrentCompanyId()
    {
        if (QueryStringHelper.HasValue("cid"))
            return new List<int> { QueryStringHelper.GetInt("cid") };

        if (CraveMobileNocHelper.AvailableCompanyIds.Count > 0)
            return CraveMobileNocHelper.AvailableCompanyIds;

        return CraveMobileNocHelper.EmptyIntList;
    }

    private DeliverypointgroupCollection GetDeliverypointgroupsByCompanyId(List<int> companyIds)
    {
        PredicateExpression filter = new PredicateExpression();
        filter.Add(DeliverypointgroupFields.CompanyId == companyIds);
        filter.Add(DeliverypointgroupFields.Active == true);

        PrefetchPath prefetch = new PrefetchPath(EntityType.DeliverypointgroupEntity);
        prefetch.Add(DeliverypointgroupEntityBase.PrefetchPathCompanyEntity);

        DeliverypointgroupCollection deliverypointgroups = new DeliverypointgroupCollection();
        deliverypointgroups.GetMulti(filter, 0, null, null, prefetch);

        return deliverypointgroups;
    }

    private DeliverypointgroupEntity GetDeliverypointgroup(int deliverypointgroupId)
    {
        return new DeliverypointgroupEntity(deliverypointgroupId);
    }

    #endregion

    #region Event Handlers

    private void hlConfirmDeliveryTimeChangeYes_Click(object sender, EventArgs e)
    {
        if (this.tbDeliveryTime.Text.IsNumeric() && !this.tbDeliveryTime.Text.IsNullOrWhiteSpace())
        {
            DeliverypointgroupCollection deliverypointgroups = new DeliverypointgroupCollection();
            if (this.GetCurrentDeliverypointgroupId().Any())
            {
                deliverypointgroups.Add(this.GetDeliverypointgroup(this.GetCurrentDeliverypointgroupId()[0]));
            }
            else if (this.GetCurrentCompanyId().Any())
            {
                deliverypointgroups.AddRange(this.GetDeliverypointgroupsByCompanyId(this.GetCurrentCompanyId()));
            }

            if (deliverypointgroups.Count > 0)
            {
                this.UpdateDeliveryTimeThroughApi(deliverypointgroups, int.Parse(this.tbDeliveryTime.Text));
            }

            this.RenderContent();
            this.Response.Redirect(this.Request.RawUrl);
        }
    }

    private void UpdateDeliveryTimeThroughApi(DeliverypointgroupCollection deliverypointgroups, int deliveryTime)
    {
        if (deliverypointgroups.Count > 0)
        {
            int companyId = deliverypointgroups[0].CompanyId;

            PublishHelper.Publish(companyId, true, (service, timestamp, mac, salt) =>
            {
                string hash = string.Empty;

                foreach (DeliverypointgroupEntity deliverypointgroup in deliverypointgroups)
                {
                    try
                    {
                        File.AppendAllText(Server.MapPath("~/App_Data/Push.txt"), string.Format("{0} - {1} - {2} - {3} - {4} - Start delivery time: {5}\n", DateTime.Now, service.Url, mac, companyId, deliverypointgroup.DeliverypointgroupId, deliverypointgroup.Name));

                        hash = Hasher.GetHashFromParameters(salt, timestamp, mac, deliverypointgroup.DeliverypointgroupId, deliveryTime);
                        service.SetEstimatedDeliveryTime(timestamp, mac, deliverypointgroup.DeliverypointgroupId, deliveryTime, hash);

                        File.AppendAllText(Server.MapPath("~/App_Data/Push.txt"), string.Format("{0} - {1} - {2} - {3} - {4} - End delivery time: {5}\n", DateTime.Now, service.Url, mac, companyId, deliverypointgroup.DeliverypointgroupId, deliverypointgroup.Name));
                    }
                    catch (Exception ex)
                    {
                        File.AppendAllText(Server.MapPath("~/App_Data/Push.txt"), string.Format("{0} - {1} - {2} - {3} - {4} - Exception: {5}\n", DateTime.Now, service.Url, mac, companyId, deliverypointgroup.DeliverypointgroupId, ex.ToString()));
                    }
                }
            });
        }
    }

    #endregion
}