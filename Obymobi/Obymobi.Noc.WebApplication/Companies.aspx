﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/CraveMobileNocBase.master" AutoEventWireup="true" Inherits="Companies" Codebehind="Companies.aspx.cs" %>
<%@ Reference VirtualPath="~/UserControls/Listitems/Company.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<div id="settings" class="category-group">
    <div id="filter">
        <D:DropDownList ID="ddlFilter" runat="server" AutoPostBack="true">
            <asp:ListItem Text="<%$ Resources:strings, Company_all_companies %>"></asp:ListItem>
            <asp:ListItem Text="<%$ Resources:strings, Company_companies_with_issues %>"></asp:ListItem>
            <asp:ListItem Text="<%$ Resources:strings, Company_companies_without_issues %>"></asp:ListItem>
        </D:DropDownList>
    </div>
    <div id="sort">
      
    </div>
    <div id="title"><D:LabelTextOnly ID="lblTitle" runat="server"></D:LabelTextOnly></div>
</div>
<ul class="list">
    <D:PlaceHolder ID="plhList" runat="server"></D:PlaceHolder>
</ul>    
</asp:Content>

