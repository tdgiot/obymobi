﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/CraveMobileNocBase.master" AutoEventWireup="true" Inherits="Clients" Codebehind="Clients.aspx.cs" %>
<%@ Reference VirtualPath="~/UserControls/Listitems/Client.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<div id="settings">
    <div id="filter">
        <D:DropDownList ID="ddlFilter" runat="server" AutoPostBack="true">
            <asp:ListItem Text="<%$ Resources:strings, Client_all_clients %>"></asp:ListItem>
            <asp:ListItem Text="<%$ Resources:strings, Client_online_clients %>"></asp:ListItem>
            <asp:ListItem Text="<%$ Resources:strings, Client_offline_clients %>"></asp:ListItem>
            <asp:ListItem Text="<%$ Resources:strings, Client_low_battery_clients %>"></asp:ListItem>
            <asp:ListItem Text="<%$ Resources:strings, Client_outdated_app %>"></asp:ListItem>
        </D:DropDownList>
    </div>
    <div id="sort">
        <D:DropDownList ID="ddlSort" runat="server" AutoPostBack="true">
            <asp:ListItem Text="<%$ Resources:strings, Client_last_request_desc %>"></asp:ListItem>
            <asp:ListItem Text="<%$ Resources:strings, Client_status_asc %>"></asp:ListItem>
            <asp:ListItem Text="<%$ Resources:strings, Client_version_desc %>"></asp:ListItem>
            <asp:ListItem Text="<%$ Resources:strings, Client_ip_address_asc %>"></asp:ListItem>
            <asp:ListItem Text="<%$ Resources:strings, Client_battery_level_asc %>"></asp:ListItem>
        </D:DropDownList>
    </div>
    <div id="title"><D:LabelTextOnly ID="lblTitle" runat="server"></D:LabelTextOnly></div>

</div>
<ul class="list">
    <D:PlaceHolder ID="plhList" runat="server"></D:PlaceHolder>
</ul>    
</asp:Content>

