﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated. 
// </auto-generated>
//------------------------------------------------------------------------------



public partial class Orders
{

    /// <summary>
    /// ddlFilter control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::Dionysos.Web.UI.WebControls.DropDownList ddlFilter;

    /// <summary>
    /// ddlOrderCount control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::Dionysos.Web.UI.WebControls.DropDownList ddlOrderCount;

    /// <summary>
    /// tbSearch control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::Dionysos.Web.UI.WebControls.TextBoxMobileNumeric tbSearch;

    /// <summary>
    /// btnSearch control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::Dionysos.Web.UI.WebControls.Button btnSearch;

    /// <summary>
    /// plhList control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::Dionysos.Web.UI.WebControls.PlaceHolder plhList;
}
