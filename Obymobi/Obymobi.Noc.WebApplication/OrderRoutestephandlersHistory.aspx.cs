﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Dionysos;
using Obymobi.Logic.HelperClasses;
using Obymobi.Data.CollectionClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Dionysos.Web.UI;
using Dionysos.Web;
using Obymobi.Enums;
using Obymobi.Logic.Routing;
using Obymobi.Noc.Logic.HelperClasses;

public partial class OrderRoutestephandlersHistory : PageListview<OrderRoutestephandlerHistoryEntity>
{
    #region Methods

    #region Base class methods

    protected override EntityView<OrderRoutestephandlerHistoryEntity> GetEntityViewFromSession()
    {
        return Session[this.SessionKeyEntityView] as EntityView<OrderRoutestephandlerHistoryEntity>;
    }

    protected override void GetFilterAndSortFromSession()
    {
        object filter = Session[this.SessionKeyFilter];
        if (filter != null)
            this.ddlFilter.SelectedIndex = (int)filter;

        object sort = Session[this.SessionKeySort];
        if (sort != null)
            this.ddlSort.SelectedIndex = (int)sort;
    }

    protected override EntityView<OrderRoutestephandlerHistoryEntity> InitializeEntityView()
    {
        // Get the order routestep history entities
        OrderRoutestephandlerHistoryCollection orderRoutestephandlerHistoryCollection = null;

        if (QueryStringHelper.HasValue("oid"))
            orderRoutestephandlerHistoryCollection = NocHelper.GetOrderRoutestephandlersHistory(QueryStringHelper.GetInt("oid"));
        else
            throw new FunctionalException("'OrderId' parameter is missing, could not retrieve OrderRoutestephandlersHistory.");

        // Return the view
        return orderRoutestephandlerHistoryCollection.DefaultView;
    }

    protected override void SetFilter()
    {
        // Filter
        switch (this.ddlFilter.SelectedIndex)
        {
            case 0:
                // All order routestephandlers history
                this.EntityView.Filter = null;
                break;
            case 1:
                // Processed order routestephandlers history
                PredicateExpression processedFilter = new PredicateExpression();
                processedFilter.Add(OrderRoutestephandlerHistoryFields.Status == OrderRoutestephandlerStatus.Completed);
                processedFilter.AddWithOr(OrderRoutestephandlerHistoryFields.Status == OrderRoutestephandlerStatus.CompletedByOtherStep);
                this.EntityView.Filter = processedFilter;

                break;
            case 2:
                // Unprocessed order routestephandlers history
                PredicateExpression unprocessedFilter = new PredicateExpression();
                unprocessedFilter.Add(OrderRoutestephandlerHistoryFields.Status != OrderRoutestephandlerStatus.Completed);
                unprocessedFilter.Add(OrderRoutestephandlerHistoryFields.Status != OrderRoutestephandlerStatus.CompletedByOtherStep);
                this.EntityView.Filter = unprocessedFilter;
                break;
        }

        // Store the current filter in the session
        Session[this.SessionKeyFilter] = this.ddlFilter.SelectedIndex;
    }

    protected override void SetSort()
    {
        // Sort
        switch (this.ddlSort.SelectedIndex)
        {
            case 0:
                // Number, ascending
                this.EntityView.Sorter = new SortExpression(new SortClause(OrderRoutestephandlerHistoryFields.Number, SortOperator.Ascending));
                break;
            case 1:
                // Status, ascending
                this.EntityView.Sorter = new SortExpression(new SortClause(OrderRoutestephandlerHistoryFields.Status, SortOperator.Ascending));
                break;
       }

        // Store the current sort in the session
        Session[this.SessionKeySort] = this.ddlSort.SelectedIndex;
    }

    protected override void RenderList()
    {
        if (this.Count == 1)
            this.lblTitle.Text = Resources.strings.One_item;
        else
            this.lblTitle.Text = string.Format(Resources.strings.X_items, this.Count);

        foreach (var orderRoutestephandlerHistory in this.EntityView)
        {
            UserControls_Listitems_OrderRoutestephandlerHistory orderRoutestephandlerHistoryControl = this.LoadControl<UserControls_Listitems_OrderRoutestephandlerHistory>("~/UserControls/Listitems/OrderRoutestephandlerHistory.ascx");
            orderRoutestephandlerHistoryControl.OrderRoutestephandlerHistory = orderRoutestephandlerHistory;
            this.plhList.Controls.Add(orderRoutestephandlerHistoryControl);
        }
    }

    #endregion

    #endregion

    #region Properties

    public override string SessionKey
    {
        get { return "OrderRoutestephandlersHistory"; }
    }

    #endregion

    #region Event handlers

    protected void Page_Load(object sender, EventArgs e)
    {
        this.Initialize();
    }

    #endregion
}