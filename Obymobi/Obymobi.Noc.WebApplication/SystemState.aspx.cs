﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web;
using System.Web.Services;
using System.Web.UI.HtmlControls;
using System.Threading;
using Dionysos;
using Obymobi.Logic.Comet;
using Obymobi.Logic.Comet.Netmessages;
using Obymobi.Logic.Status;
using Obymobi.Constants;
using Dionysos.Web;
using Obymobi.Enums;
using System.ServiceModel;
using Obymobi.Security;
using System.Net;
using System.IO;
using Obymobi;
using Obymobi.Data.CollectionClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.EntityClasses;
using System.Collections.Concurrent;
using Dionysos.Data.LLBLGen;
using System.Linq;
using System.Diagnostics;

public partial class SystemState : System.Web.UI.Page
{
    static readonly string applicationKeyNocServiceReportPrimary = "SystemState.nocServiceReportPrimary";
    static readonly string applicationKeyNocServiceReportSecondary = "SystemState.nocServiceReportSecondary";
    static readonly string applicationKeyNocServiceReportBeingRefreshed = "SystemState.NocServiceReportBeingRefreshed";
    static readonly string applicationKeyPollersBeingRefreshed = "SystemState.PollersBeingRefreshed";
    static readonly string applicationKeyPollersPrimary = "SystemState.PollersPrimary";
    static readonly string applicationKeyPollersSecondary = "SystemState.PollersSecondary";


    protected override void OnInit(EventArgs e)
    {        
        if (!CraveMobileNocHelper.IsAdministrator)
            throw new AuthorizationException("Not Allowed!");
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        this.Title = string.Format("{0} - {1}", Resources.strings.System_state, Resources.strings.Crave_NOC);

        if (!this.IsPostBack)
        {
            this.RenderContent();          
        }        
    }  

    public void RenderContent()
    {
        //this.RenderNocServiceStatus();
        this.RenderPollers();
        this.RenderActiveRequests();
        this.RenderClientsConnectedToComet();
        this.RenderCloudApplicationVersions();
        
    }

    private void RenderNocServiceStatus()
    {
        if (!NocServiceStatusBeingRefreshed)
        {
            var httpContext = HttpContext.Current;
            var t = new Thread(() =>
            {
                HttpContext.Current = httpContext;
                DownloadNocServiceStatus();
            });
            t.Start();
        }
    }

    private void RenderPollers()
    {
        //this.GetPollerStatus();

        bool success = true;

        // Primary Server        
        var primaryPollers = this.PollersPrimary;
        this.lblNocPoller1.Text = primaryPollers.NocPoller.ToString();
        this.RenderGenericPollers(this.plhGenericPollersPrimary, this.PollersPrimary);

        if (primaryPollers.NocPoller < 0 || primaryPollers.GetPollerTime(PollerType.DatabasePoller) < 0 || primaryPollers.GetPollerTime(PollerType.SupportNotificationPoller) < 0)
            success = false;

        if (WebEnvironmentHelper.CloudEnvironment == CloudEnvironment.ProductionPrimary ||
            WebEnvironmentHelper.CloudEnvironment == CloudEnvironment.ProductionSecondary)
        {
            // Secondary Server
            var secondaryPollers = this.PollersSecondary;
            this.lblNocPoller2.Text = secondaryPollers.NocPoller.ToString();
            this.RenderGenericPollers(this.plhGenericPollersSecondary, this.PollersSecondary);
            if (!success || secondaryPollers.NocPoller < 0 || secondaryPollers.GetPollerTime(PollerType.DatabasePoller) < 0 || secondaryPollers.GetPollerTime(PollerType.SupportNotificationPoller) < 0)
                success = false;
        }
        else
        {
            this.plhSecondaryServer.Visible = false;
        }

        // Get queue length, not 100% accurate, but good indication.
        var filter = new PredicateExpression(MediaProcessingTaskFields.Attempts == 0);
        filter.AddWithAnd(MediaProcessingTaskFields.Action == (int)MediaProcessingTaskEntity.ProcessingAction.Upload);

        try
        {
            var tasks = new MediaProcessingTaskCollection();
            this.lblMediaTaskQueueLength.Text = tasks.GetDbCount(filter).ToString();
        }
        catch { }

        if (success) this.imgPollers.ImageUrl = ResolveUrl("~/Images/Icons/checkmark.png");
        else this.imgPollers.ImageUrl = ResolveUrl("~/Images/Icons/error.png");
    }

    private void RenderActiveRequests()
    {
        try
        {
            PerformanceCounter performanceCounter = new PerformanceCounter("W3SVC_W3WP", "Active Requests", "_Total", true);
            if (performanceCounter != null)
            {
                this.lblActiveRequests.Text = performanceCounter.RawValue.ToString();
            }
        }
        catch
        {
        }
    }

    private void RenderClientsConnectedToComet()
    {
        RelationCollection relation = new RelationCollection();
        relation.Add(ClientEntityBase.Relations.DeviceEntityUsingDeviceId);

        ClientCollection clientCollection = new ClientCollection();

        PredicateExpression emenusConnectedToCometFilter = new PredicateExpression();
        emenusConnectedToCometFilter.Add(DeviceFields.LastRequestUTC >= DateTime.UtcNow.AddMinutes(-3));
        emenusConnectedToCometFilter.Add(DeviceFields.CommunicationMethod == ClientCommunicationMethod.SignalR);
        int emenusConnectedToCometCount = clientCollection.GetDbCount(emenusConnectedToCometFilter, relation);

        PredicateExpression supportToolsConnectedToCometFilter = new PredicateExpression();
        supportToolsConnectedToCometFilter.Add(DeviceFields.LastRequestUTC >= DateTime.UtcNow.AddMinutes(-3));
        supportToolsConnectedToCometFilter.Add(DeviceFields.LastSupportToolsRequestUTC >= DateTime.UtcNow.AddMinutes(-3));
        int supportToolsConnectedToCometCount = clientCollection.GetDbCount(supportToolsConnectedToCometFilter, relation);

        PredicateExpression totalFilter = new PredicateExpression();
        totalFilter.Add(DeviceFields.LastRequestUTC >= DateTime.UtcNow.AddMinutes(-3));
        int totalCount = clientCollection.GetDbCount(totalFilter, relation);

        int emenusConnectedToCometPercentage = Dionysos.Math.Percentage(emenusConnectedToCometCount, totalCount, true);
        int supportToolsConnectedToCometPercentage = Dionysos.Math.Percentage(supportToolsConnectedToCometCount, totalCount, true);

        this.lblClientsTotal.Value = totalCount.ToString();
        this.lblEmenusConnectedToSignalR.Value = string.Format("{0} ({1}%)", emenusConnectedToCometCount, emenusConnectedToCometPercentage);
        this.lblSupportToolsConnectedToSignalR.Value = string.Format("{0} ({1}%)", supportToolsConnectedToCometCount, supportToolsConnectedToCometPercentage);

        if (emenusConnectedToCometPercentage < 75)
        {
            this.lblEmenusConnectedToSignalR.CssClass = "error";
            this.lblEmenusConnectedToSignalR.CssClass = "error";
        }

        IncludeFieldsList includeFieldsList = new IncludeFieldsList();
        includeFieldsList.Add(ClientFields.LastApiVersion);

        ClientCollection clients = new ClientCollection();
        clients.GetMulti(totalFilter, 0, null, relation, null, includeFieldsList, 0, 0);

        int clientCount = clients.Count;
        int clientsApiV1 = clients.Count(x => x.LastApiVersion == 1);
        int clientsApiV1Percentage = (int)(((double)clientsApiV1 / clientCount) * 100.0);
        int clientsApiV2 = clients.Count(x => x.LastApiVersion == 2);
        int clientsApiV2Percentage = (int)(((double)clientsApiV2 / clientCount) * 100.0);

        this.lblApiV1Count.Text = string.Format("{0}/{1} ({2}%)", clientsApiV1, clientCount, clientsApiV1Percentage);
        this.lblApiV2Count.Text = string.Format("{0}/{1} ({2}%)", clientsApiV2, clientCount, clientsApiV2Percentage);
    }

    private void RenderGenericPollers(Dionysos.Web.UI.WebControls.PlaceHolder plh, Pollers pollers)
    {
        foreach (var keyValuePair in pollers.GenericPollers)
        {
            plh.AddHtml("<li><div class=\"label\">{0}</div><div class=\"value\">{1}</div></li>", keyValuePair.Key, keyValuePair.Value);
        }
    }
    
    private void RenderCloudApplicationVersions()
    {
        try
        {
            CloudApplicationVersionCollection cloudApplicationVersions = EntityCollection.GetMulti<CloudApplicationVersionCollection>(null);

            bool diff = false;

            foreach (string machineName in cloudApplicationVersions.Select(x => x.MachineName).Distinct())
            {
                this.plhCloudApplicationVersions.AddHtml("<li>");
                this.plhCloudApplicationVersions.AddHtml("<div class=\"subtitle\">{0}</div>", machineName);
                this.plhCloudApplicationVersions.AddHtml("</li>");

                IEnumerable<CloudApplicationVersionEntity> cloudApplicationVersionsPerMachine = cloudApplicationVersions.Where(x => x.MachineName.Equals(machineName, StringComparison.InvariantCultureIgnoreCase))
                                                                                                                        .OrderBy(x => x.CloudApplication);

                foreach (CloudApplicationVersionEntity cloudApplicationVersionEntity in cloudApplicationVersionsPerMachine)
                {
                    bool versionDifference = cloudApplicationVersions.Where(x => x.CloudApplication == cloudApplicationVersionEntity.CloudApplication)
                                                                     .Any(x => x.Version != cloudApplicationVersionEntity.Version);

                    this.plhCloudApplicationVersions.AddHtml("<li>");
                    this.plhCloudApplicationVersions.AddHtml("<div class=\"label\">{0}</div>", cloudApplicationVersionEntity.CloudApplication.ToString());

                    if (versionDifference)
                    {
                        diff = true;
                        this.plhCloudApplicationVersions.AddHtml("<div class=\"value\"><span class=\"error\">{0}</span></div>", cloudApplicationVersionEntity.Version);
                    }
                    else
                    {
                        this.plhCloudApplicationVersions.AddHtml("<div class=\"value\">{0}</div>", cloudApplicationVersionEntity.Version);
                    }

                    this.plhCloudApplicationVersions.AddHtml("</li>");
                }

                this.plhCloudApplicationVersions.AddHtml("<li class=\"spacer\"></li>");
            }

            if (diff)
                this.imgCloudApplicationVersions.ImageUrl = ResolveUrl("~/Images/Icons/error.png");
            else
                this.imgCloudApplicationVersions.ImageUrl = ResolveUrl("~/Images/Icons/checkmark.png");
        }
        catch { }
    }

    private static void DownloadNocServiceStatus()
    {
        if (NocServiceStatusBeingRefreshed)
            return;

        NocServiceStatusBeingRefreshed = true;

        try
        {
            // Download from the NocService Url
            /*var nocServiceUrls = Obymobi.WebEnvironmentHelper.GetNocServiceUrls();

            string[] reports = new string[2];
            for (int i = 0; i < nocServiceUrls.Count; i++)
            {
                string url = nocServiceUrls[i];

                // Download the Status
                HttpWebResponse response;
                try
                {
                    string cacheSaveUrl = string.Format("{0}?update={1}", url, DateTime.Now.Ticks);
                    WebRequest request = WebRequest.Create(cacheSaveUrl);
                    request.Proxy = null;
                    response = (HttpWebResponse)request.GetResponse();

                    if (response != null && response.StatusCode == HttpStatusCode.OK)
                    {
                        // Validate it's the correct type of content
                        StreamReader sr = new StreamReader(response.GetResponseStream());
                        string content = sr.ReadToEnd();

                        content += "\n At: {0}".FormatSafe(DateTime.Now);
                        content += "\n From: {0}\n\n".FormatSafe(url);

                        if (i == 0)
                            NocServiceStatusPrimary = content;
                        else
                            NocServiceStatusSecondary = content;                        
                    }

                    if (response != null)
                        response.Close();
                }
                catch (Exception)
                {
                    if (i == 0)
                        NocServiceStatusPrimary = "Primary server could not be reached.\n";
                    else
                        NocServiceStatusSecondary = "Secondary server could not be reached.\n";
                }
            }*/
        }
        finally
        {
            NocServiceStatusBeingRefreshed = false;
        }
    }

    private static string NocServiceStatusPrimary
    {
        get
        {
            if (HttpContext.Current.Application[applicationKeyNocServiceReportPrimary] == null)
                HttpContext.Current.Application[applicationKeyNocServiceReportPrimary] = "Being retrieved...\n\n";

            return HttpContext.Current.Application[applicationKeyNocServiceReportPrimary].ToString();
        }
        set
        {
            HttpContext.Current.Application[applicationKeyNocServiceReportPrimary] = value;
        }
    }

    private static string NocServiceStatusSecondary
    {
        get
        {
            if (HttpContext.Current.Application[applicationKeyNocServiceReportSecondary] == null)
                HttpContext.Current.Application[applicationKeyNocServiceReportSecondary] = "Being retrieved...\n\n";

            return HttpContext.Current.Application[applicationKeyNocServiceReportSecondary].ToString();
        }
        set
        {
            HttpContext.Current.Application[applicationKeyNocServiceReportSecondary] = value;
        }
    }

    private static bool NocServiceStatusBeingRefreshed
    {
        get
        {
            if (HttpContext.Current.Application[applicationKeyNocServiceReportBeingRefreshed] == null)
                HttpContext.Current.Application[applicationKeyNocServiceReportBeingRefreshed] = false;

            return (bool)HttpContext.Current.Application[applicationKeyNocServiceReportBeingRefreshed];
        }
        set
        {
            HttpContext.Current.Application[applicationKeyNocServiceReportBeingRefreshed] = value;
        }
    }

    private bool PollersBeingRefreshed
    {
        get
        {
            if (this.Application[applicationKeyPollersBeingRefreshed] == null)
                this.Application[applicationKeyPollersBeingRefreshed] = false;

            return (bool)this.Application[applicationKeyPollersBeingRefreshed];
        }
        set
        {
            this.Application[applicationKeyPollersBeingRefreshed] = value;
        }
    }

    private Pollers PollersPrimary
    {
        get
        {
            var pollers = this.Application[applicationKeyPollersPrimary] as Pollers;
            if (pollers == null)
            {
                pollers = new Pollers();
                this.Application[applicationKeyPollersPrimary] = pollers;
            }

            return pollers;
        }
        set
        {
            this.Application[applicationKeyPollersPrimary] = value;
        }
    }

    private Pollers PollersSecondary
    {
        get
        {
            var pollers = this.Application[applicationKeyPollersSecondary] as Pollers;
            if (pollers == null)
            {
                pollers = new Pollers();
                this.Application[applicationKeyPollersSecondary] = pollers;
            }

            return pollers;
        }
        set
        {
            this.Application[applicationKeyPollersSecondary] = value;
        }
    }    

    private class Pollers
    {
        public Pollers()
        {
            this.NocPoller = -1;
            this.GenericPollers = new ConcurrentDictionary<PollerType,long>();
        }

        public long NocPoller { get; set; }
        public ConcurrentDictionary<PollerType, long> GenericPollers { get; private set; }

        public long GetPollerTime(PollerType type)
        {
            long time;
            if (!GenericPollers.TryGetValue(type, out time))
            {
                time = -1;
            }
            return time;
        }
    }

    #region Web methods

    [WebMethod(EnableSession = true)]
    public static object[] GetAuthentication(string pokeInIdentifier)
    {
        long timestamp = DateTime.UtcNow.ToUnixTime();
        string identifier = CometConstants.CometIdentifierNoc;
        string hash = Hasher.GetHashFromParameters(CometConstants.CometInternalSalt, timestamp, identifier, pokeInIdentifier);

        return new object[] { timestamp, identifier, hash };
    }

    [WebMethod(EnableSession = true)]
    public static string NetmessageSetClientType()
    {
        var netmessage = new NetmessageSetClientType();
        netmessage.ClientType = NetmessageClientType.MobileNoc;

        return netmessage.ToJson();
    }

    [WebMethod(EnableSession = true)]
    public static string NetmessagePong()
    {
        var netmessage = new NetmessagePong();
        return netmessage.ToJson();
    }

    [WebMethod(EnableSession = true)]
    public static string NetmessageSystemState()
    {
        var netmessage = new NetmessageSystemState();
        return netmessage.ToJson();
    }

    [WebMethod(EnableSession = true)]
    public static string GetNocServiceStatus()
    {
        if (!NocServiceStatusBeingRefreshed)
        {
            var httpContext = HttpContext.Current;
            var t = new Thread(() =>
            {
                HttpContext.Current = httpContext;
                DownloadNocServiceStatus();
            });
            t.Start();
        }

        var sb = new StringBuilder();
        sb.AppendLine("<strong>Primary Server Report</strong>");
        sb.AppendLine(NocServiceStatusPrimary);

        if (WebEnvironmentHelper.CloudEnvironment == CloudEnvironment.ProductionSecondary)
        {
            sb.AppendLine("<strong>Secondary Server Report</strong>");
            sb.AppendLine(NocServiceStatusSecondary);
        }

        return sb.ToString().ReplaceLineBreakWithBR();
    }
    #endregion
}