﻿using System;
using System.Collections.Generic;
using Dionysos;
using Obymobi.Logic.HelperClasses;
using Obymobi.Data.CollectionClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Dionysos.Web;
using Obymobi.Enums;
using Obymobi.Noc.Logic.HelperClasses;

public partial class Orders : PageListview<OrderEntity>
{
    protected string EmptyResultText = Resources.strings.OrderSearch_not_found;

    private string SessionKeyOrderCount
    {
        get
        {
            return string.Format("{0}-OrderCount", this.SessionKey);
        }
    }

    #region Methods

    private List<int> GetCurrentCompanyId()
    {
        if (QueryStringHelper.HasValue("cid"))
            return new List<int> { QueryStringHelper.GetInt("cid") };

        if (CraveMobileNocHelper.AvailableCompanyIds.Count > 0)
            return CraveMobileNocHelper.AvailableCompanyIds;

        return CraveMobileNocHelper.EmptyIntList;
    }


    #region Base class methods

    protected override EntityView<OrderEntity> GetEntityViewFromSession()
    {
        object orderCount = Session[this.SessionKeyOrderCount];
        if (orderCount != null)
        {
            if (this.ddlOrderCount.SelectedIndex != (int)orderCount)
                return null;
        }

        return Session[this.SessionKeyEntityView] as EntityView<OrderEntity>;
    }

    protected override void GetFilterAndSortFromSession()
    {
        object filter = Session[this.SessionKeyFilter];
        if (filter != null)
            this.ddlFilter.SelectedIndex = (int)filter;

        object orderCount = Session[this.SessionKeyOrderCount];
        if (orderCount != null)
            this.ddlOrderCount.SelectedIndex = (int)orderCount;
    }

    protected override EntityView<OrderEntity> InitializeEntityView()
    {
        // Return the view
        return GetOrderCollection(null).DefaultView;
    }

    protected override void SetFilter()
    {
        // Filter
        switch (this.ddlFilter.SelectedIndex)
        {
            case 0:
                // All orders
                this.EntityView.Filter = null;
                break;
            case 1:
                // Processed orders
                this.EntityView.Filter = new PredicateExpression(OrderFields.Status == OrderStatus.Processed);
                break;
            case 2:
                // Unprocessed orders
                this.EntityView.Filter = new PredicateExpression(OrderFields.Status != OrderStatus.Processed);
                break;
            case 3:
                // Waiting for payment
                this.EntityView.Filter = new PredicateExpression(OrderFields.Status == OrderStatus.WaitingForPayment);
                break;
        }

        // Store the current filter in the session
        Session[this.SessionKeyFilter] = this.ddlFilter.SelectedIndex;
    }

    protected override void SetSort()
    {

    }

    private int GetOrderCount()
    {
        int orderCount;
        if (int.TryParse(this.ddlOrderCount.SelectedValue, out orderCount))
        {
            // Store the order count in the session
            Session[this.SessionKeyOrderCount] = this.ddlOrderCount.SelectedIndex;
        }
        else
        {
            orderCount = 50;
        }

        return orderCount;
    }

    protected override void RenderList()
    {
        foreach (var order in this.EntityView)
        {
            UserControls_Listitems_Order orderControl = this.LoadControl<UserControls_Listitems_Order>("~/UserControls/Listitems/Order.ascx");
            orderControl.Order = order;
            this.plhList.Controls.Add(orderControl);
        }
    }

    private void BtnSearch_Click(object sender, EventArgs e)
    {
        string searchRequest = this.tbSearch.Text;
        int? orderId = null;

        if (!string.IsNullOrWhiteSpace(searchRequest) && !TryParseSearchCriteriaAsOrderId(searchRequest, out orderId))
        {
            EmptyResultText = Resources.strings.OrderSearch_only_numbers;
            OrderCollection orderCollectionEmpty = new OrderCollection();
            this.plhList.Controls.Clear();
            this.EntityView = orderCollectionEmpty.DefaultView;
            this.RenderList();
            return;
        }

        this.plhList.Controls.Clear();

        this.EntityView = GetOrderCollection(orderId).DefaultView;
        this.RenderList();
    }

    private OrderCollection GetOrderCollection(int? orderId)
    {
        int orderCount = GetOrderCount();
        List<int> companyIds = GetCurrentCompanyId();
        int? clidInt = QueryStringHelper.HasValue("clid") ? (int?)QueryStringHelper.GetInt("clid") : null;

        return NocHelper.GetOrders(companyIds, clidInt, null, orderId, orderCount);
    }

    private bool TryParseSearchCriteriaAsOrderId(string searchRequest, out int? orderId)
    {
        orderId = null;

        if (!searchRequest.IsNumeric())
        {
            return false;
        }

        if (!int.TryParse(searchRequest, out int validOrderId))
        {
            return false;
        }

        orderId = validOrderId;
        return true;
    }

    #endregion

    #endregion

    #region Properties

    public override string SessionKey
    {
        get { return "Orders"; }
    }

    #endregion

    #region Event handlers

    protected void Page_Load(object sender, EventArgs e)
    {
        this.Title = string.Format("{0} - {1}", Resources.strings.Orders, Resources.strings.Crave_NOC);
        this.btnSearch.Click += BtnSearch_Click;

        this.Initialize();
    }

    #endregion
}