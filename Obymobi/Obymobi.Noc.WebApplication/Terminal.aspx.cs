﻿using System;
using System.Linq;
using Dionysos.Web;
using Obymobi.Data.EntityClasses;
using Obymobi.Enums;
using Obymobi.Logic.HelperClasses;
using Obymobi.Noc.Logic.HelperClasses;

public partial class Terminal : PageEntity<TerminalEntity>
{
    #region Fields

    /// <summary>
    /// The page number.
    /// </summary>
    private int? pageNumber;

    /// <summary>
    /// The log sort order.
    /// </summary>
    private LogSortOrder? sortOrder;

    #endregion

    #region Properties

    /// <summary>
    /// Gets or sets the page number.
    /// </summary>
    /// <value>
    /// The page number.
    /// </value>
    public int PageNumber
    {
        get
        {
            if (!this.pageNumber.HasValue)
                this.pageNumber = QueryStringHelper.GetValue<int>(Resources.strings.QueryStringPage) ?? 1;

            return this.pageNumber.Value;
        }
        set
        {
            this.pageNumber = value;
        }
    }

    /// <summary>
    /// Gets or sets the log sort order.
    /// </summary>
    /// <value>
    /// The log sort order.
    /// </value>
    public LogSortOrder SortOrder
    {
        get
        {
            if (!this.sortOrder.HasValue)
            {
                string value = QueryStringHelper.GetValue(Resources.strings.QueryStringSort);
                this.sortOrder = EnumUtil.ToArray<LogSortOrder>().FirstOrDefault(log => String.Equals(value, Resources.strings.ResourceManager.GetString("PagingSortOn" + log.ToString()), StringComparison.CurrentCultureIgnoreCase));
            }

            return this.sortOrder.Value;
        }
        set
        {
            this.sortOrder = value;
        }
    }

    #endregion

    #region Event handlers

    protected void Page_Load(object sender, EventArgs e)
    {
        this.InitializeEntity();

        var terminal = this.Entity;

        if (terminal == null && QueryStringHelper.HasValue("id"))
        {
            int terminalId = QueryStringHelper.GetInt("id");
            if (terminalId > 0)
                terminal = new TerminalEntity(terminalId);
        }

        if (terminal != null)
        {
            this.Title = string.Format("{0} '{1}' - {2}", Resources.strings.Terminal, terminal.Name, Resources.strings.Crave_NOC);

            UserControls_Listitems_Terminal terminalControl = this.LoadControl<UserControls_Listitems_Terminal>("~/UserControls/Listitems/Terminal.ascx");
            terminalControl.Terminal = terminal;
            terminalControl.RenderLinkable = false;
            this.plhDetails.Controls.Add(terminalControl);

            // Active orders
            var orders = NocHelper.GetActiveOrdersForTerminal(terminal.TerminalId);
            if (orders.Count > 0)
            {
                this.plhTerminalOrders.Visible = true;

                foreach (var order in orders)
                {
                    UserControls_Listitems_Order orderControl = this.LoadControl<UserControls_Listitems_Order>("~/UserControls/Listitems/Order.ascx");
                    orderControl.Order = order;
                    this.plhTerminalOrdersList.Controls.Add(orderControl);
                }
            }

            // Get the right minutes from the ddl
            int? hours = null;
            if (!this.ddlTimespan.SelectedValue.Equals("99999"))
                hours = Int32.Parse(this.ddlTimespan.SelectedValue);
            
            // Get terminal logs
            var activities = this.Entity.GetTerminalActivityLogs(null, this.cbNetmessages.Checked, this.cbTerminalLogs.Checked, this.cbTerminalStates.Checked, hours);

            this.lblTerminalsLogs.Text = string.Format(Resources.strings.Terminal_logs, activities.Count);

            if (activities.Count > 0)
            {
                foreach (var activity in activities)
                {
                    UserControls_Listitems_DeviceActivityLog terminalLogControl = this.LoadControl<UserControls_Listitems_DeviceActivityLog>("~/UserControls/Listitems/DeviceActivityLog.ascx");
                    terminalLogControl.Activity = activity;
                    terminalLogControl.FullHeight = false;
                    this.plhTerminalLogsList.Controls.Add(terminalLogControl);
                }
            }

            this.plhTerminalLogsList.Visible = (activities.Count != 0);

            this.hlOpenInCms.NavigateUrl = this.GetCmsUrl(this.Entity.CompanyId, "Company/Terminal.aspx?id={0}", this.Entity.TerminalId);
        }
    }

    #endregion
}