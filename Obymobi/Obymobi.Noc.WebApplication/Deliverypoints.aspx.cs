﻿using System;
using System.Collections.Generic;
using Obymobi.Logic.HelperClasses;
using Obymobi.Data.CollectionClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.EntityClasses;
using Dionysos.Web;
using Obymobi.Noc.Logic.HelperClasses;

public partial class Deliverypoints : PageListview<DeliverypointEntity>
{
    #region Methods

    private List<int> GetCurrentCompanyId()
    {
        if (QueryStringHelper.HasValue("cid"))
            return new List<int> {QueryStringHelper.GetInt("cid")};

        if (CraveMobileNocHelper.AvailableCompanyIds.Count > 0)
            return CraveMobileNocHelper.AvailableCompanyIds;

        return CraveMobileNocHelper.EmptyIntList;
    }

    #region Base class methods

    protected override EntityView<DeliverypointEntity> GetEntityViewFromSession()
    {
        return Session[this.SessionKeyEntityView] as EntityView<DeliverypointEntity>;
    }

    protected override void GetFilterAndSortFromSession()
    {
        object filter = Session[this.SessionKeyFilter];
        if (filter != null)
            this.ddlFilter.SelectedIndex = (int)filter;

        object sort = Session[this.SessionKeySort];
        if (sort != null)
            this.ddlSort.SelectedIndex = (int)sort;
    }

    protected override EntityView<DeliverypointEntity> InitializeEntityView()
    {
        // Get the deliverypoints
        DeliverypointCollection deliverypointCollection = NocHelper.GetDeliverypoints(GetCurrentCompanyId());

        // Return the view
        return deliverypointCollection.DefaultView;
    }

    protected override void SetFilter()
    {
        // Filter
        switch (this.ddlFilter.SelectedIndex)
        {
            case 0:
                // All deliverypoints
                this.EntityView.Filter = null;
                break;
            case 1:
                // Deliverypoints with online clients
                this.EntityView.Filter = new PredicateExpression(new EntityProperty("HasOnlineClient") == true);
                break;
            case 2:
                // Deliverypoints with offline clients
                this.EntityView.Filter = new PredicateExpression(new EntityProperty("HasClient") == true & new EntityProperty("HasOnlineClient") == false);
                break;
            case 3:
                // Deliverypoints without clients
                this.EntityView.Filter = new PredicateExpression(new EntityProperty("HasClient") == false);
                break;
        }

        // Store the current filter in the session
        Session[this.SessionKeyFilter] = this.ddlFilter.SelectedIndex;
    }

    protected override void SetSort()
    {
        // Sort
        switch (this.ddlSort.SelectedIndex)
        {
            case 0:
                // Number, Ascending
                this.EntityView.Sorter = new SortExpression(new EntityProperty("NumberAsInteger") | SortOperator.Ascending);
                break; 
            case 1:
                // Last request, descending
                this.EntityView.Sorter = new SortExpression(new EntityProperty("LastRequestUTC") | SortOperator.Descending);
                break;
        }

        // Store the current sort in the session
        Session[this.SessionKeySort] = this.ddlSort.SelectedIndex;
    }

    protected override void RenderList()
    {
        if (this.Count == 1)
            this.lblTitle.Text = Resources.strings.One_item;
        else
            this.lblTitle.Text = string.Format(Resources.strings.X_items, this.Count);

        foreach (var deliverypoint in this.EntityView)
        {
            UserControls_Listitems_Deliverypoint deliverypointControl = this.LoadControl<UserControls_Listitems_Deliverypoint>("~/UserControls/Listitems/Deliverypoint.ascx");
            deliverypointControl.Deliverypoint = deliverypoint;
            this.plhList.Controls.Add(deliverypointControl);
        }
    }

    #endregion

    #endregion

    #region Properties

    public override string SessionKey
    {
        get { return "Deliverypoints"; }
    }

    #endregion

    #region Event handlers

    protected void Page_Load(object sender, EventArgs e)
    {
        this.Title = string.Format("{0} - {1}", Resources.strings.Deliverypoints, Resources.strings.Crave_NOC);
        this.Initialize();
    }

    #endregion
}