﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="SignOn" Codebehind="SignOn.aspx.cs" %>
<%@ Register src="~/UserControls/SignOnPanel.ascx" tagname="SignOn" tagprefix="uc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="mobileoptimized" content="300" />
    <meta name="handheldfriendly" content="true" />
    <meta name="viewport" content="width=device-width; initial-scale=1.0; minimum-scale=1.0; maximum-scale=2.0; user-scalable=yes;" />
    <link href="Css/base.css" type="text/css" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <% if (!EmbeddedMode) { %>
            <div id="header">
            <a id="logo" class="logo"></a>
        </div>
        <% } %>
        <div id="signon">
            <D:PlaceHolder runat="server" ID="plhSignOnInvalid" Visible="false">
                <div id="signon-invalid"><D:LabelTextOnly ID="lblSignOnInvalid" runat="server"></D:LabelTextOnly></div>
            </D:PlaceHolder>

            <uc1:SignOn ID="SignOn1" runat="server" />
        </div>
    
    </div>
    </form>
</body>
</html>
