﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/CraveMobileNocBase.master" AutoEventWireup="true" Inherits="Tools" Codebehind="Tools.aspx.cs" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">        
    
    <D:PlaceHolder runat="server" ID="plOrderManagementOptions">
        <ul class="details">
            <li>            
                <div class="title"><D:LabelTextOnly runat="server" Text="Order management"></D:LabelTextOnly></div>
            </li>
        </ul>
        <ul class="buttonlist">
            <li><D:HyperLink ID="hlHandleOrders"  NavigateUrl="~/Tools.aspx?handleOrders=1" runat="server" Visible="true" Text="Set unprocessed orders to processed (3 days or older)" /></li>
        </ul>
    </D:PlaceHolder>
    
<D:PlaceHolder runat="server" ID="plOrderManagement" Visible="false">
    <h1><D:LabelTextOnly ID="processOrderConfirmationTitle" runat="server" Text="Are you sure you want to process all unprocessed orders older then 3 days?"></D:LabelTextOnly></h1>
    <p>&nbsp;</p>
    <ul class="buttonlist">
        <li><D:LinkButton ID="hlProcessOrderConfirmationYes" runat="server" Visible="true" NavigateUrl="~/Tools.aspx" Text="<%$ Resources:strings, Generic_Yes %>"></D:LinkButton></li>
        <li>&nbsp;<br />&nbsp;</li>
        <li><D:HyperLink runat="server" Visible="true" NavigateUrl="~/Tools.aspx" Text="No / Back"></D:HyperLink></li>
        <li><D:PlaceHolder runat="server" ID="lblProcessOrderResult"></D:PlaceHolder></li>
    </ul>
    <p>&nbsp;</p>
</D:PlaceHolder>


    <D:PlaceHolder runat="server" ID="plhCloudManagement">
        <ul class="details">
            <li>            
                <div class="title"><D:LabelTextOnly runat="server" Text="<%$ Resources:strings, Tools_Cloud_Management %>"></D:LabelTextOnly></div>
            </li>                          
            <li>
                <div class="label"><D:LabelTextOnly runat="server" Text="<%$ Resources:strings, Tools_Cloud_Environment %>"></D:LabelTextOnly></div>
                <div class="value"><D:Label ID="lblCloudCurrentEnvironment" runat="server"></D:Label></div>
            </li>
            <li>
                <div class="label"><D:LabelTextOnly runat="server" Text="<%$ Resources:strings, Tools_Cloud_Primary %>"></D:LabelTextOnly></div>
                <div class="value"><D:Label ID="lblPrimaryCloudServerStatus" runat="server"></D:Label></div>
            </li>
            <li>
                <div class="label"><D:LabelTextOnly runat="server" Text="<%$ Resources:strings, Tools_Cloud_CometHandlers %>"></D:LabelTextOnly></div>
                <div class="value"><D:Label ID="lblCloudCometHandlers" runat="server"></D:Label></div>
            </li>
            <D:PlaceHolder runat="server" ID="plhSecondaryCloudServerStatus">
            <li>
                <div class="label"><D:LabelTextOnly runat="server" Text="<%$ Resources:strings, Tools_Cloud_Secondary %>"></D:LabelTextOnly></div>
                <div class="value"><D:Label ID="lblSecondaryCloudServerStatus" runat="server"></D:Label></div>
            </li>
            </D:PlaceHolder>
            <li>
                <div class="label"><D:LabelTextOnly runat="server" Text="<%$ Resources:strings, Tools_LastRefresh %>"></D:LabelTextOnly></div>
                <div class="value"><D:Label ID="lblLastCloudRefresh" runat="server"></D:Label></div>
            </li>
        </ul>

        <D:PlaceHolder runat="server" ID="plhCloudManagementSwitching">
        <ul class="buttonlist">
            <li><D:HyperLink ID="hlResetCloudEnvironment" NavigateUrl="~/Tools.aspx?switchToCloudEnvironment=-1" runat="server" Visible="true" Text="<%$ Resources:strings, Tools_Cloud_Reset_CloudEnvironment %>"></D:HyperLink></li>
            <li><D:HyperLink ID="hlSwitchToPrimaryCloudEnvironment" NavigateUrl="~/Tools.aspx?switchToCloudEnvironment=0" runat="server" Visible="true" Text="<%$ Resources:strings, Tools_Cloud_SwitchTo_Primary %>"></D:HyperLink></li>
            <li><D:HyperLink ID="hlSwitchToSecondaryCloudEnvironment" NavigateUrl="~/Tools.aspx?switchToCloudEnvironment=1" runat="server" Visible="true" Text="<%$ Resources:strings, Tools_Cloud_SwitchTo_Secondary %>"></D:HyperLink></li>
            <li><D:HyperLink ID="hlSetCometHandlers" NavigateUrl="~/Tools.aspx?setCometHandlers=-1" runat="server" Visible="true" Text="<%$ Resources:strings, Tools_Cloud_Set_CometHandlers %>"></D:HyperLink></li>
        </ul>
        </D:PlaceHolder>

        <D:PlaceHolder runat="server" ID="plhCloudManagementUnavailable" Visible="false">
        <ul class="details">
        <li>
            <div class="label"><D:LabelTextOnly runat="server" Text="<%$ Resources:strings, Tools_Cloud_SwitchTo %>"></D:LabelTextOnly></div>
            <div class="value"><D:LabelTextOnly runat="server" Text="<%$ Resources:strings, Tools_Cloud_Unavailable %>"></D:LabelTextOnly></div>
        </li>
        </ul>    
        </D:PlaceHolder>  
    </D:PlaceHolder> 

    <D:PlaceHolder runat="server" ID="plhDatabaseManagement">
        <ul class="details">
            <li>            
                <div class="title"><D:LabelTextOnly runat="server" Text="<%$ Resources:strings, Tools_Database_Title %>"></D:LabelTextOnly></div>
            </li>                          
            <li>
                <div class="label"><D:LabelTextOnly runat="server" Text="<%$ Resources:strings, Tools_Database_PrimaryDatabase %>"></D:LabelTextOnly></div>
                <div class="value"><D:Label ID="lblPrimaryDatabaseStatus" runat="server"></D:Label></div>
            </li>
            <D:PlaceHolder runat="server" ID="plhSecondaryDatabaseStatus">
            <li>
                <div class="label"><D:LabelTextOnly runat="server" Text="<%$ Resources:strings, Tools_Database_SecondaryDatabase %>"></D:LabelTextOnly></div>
                <div class="value"><D:Label ID="lblSecondaryDatabaseStatus" runat="server"></D:Label></div>
            </li>
            </D:PlaceHolder>
            <li>
                <div class="label"><D:LabelTextOnly runat="server" Text="<%$ Resources:strings, Tools_Database_ActiveServer %>"></D:LabelTextOnly></div>
                <div class="value"><D:Label ID="lblActiveDatabaseServer" runat="server"></D:Label></div>
            </li>
            <li>
                <div class="label"><D:LabelTextOnly runat="server" Text="<%$ Resources:strings, Tools_Database_BroadcastedServer %>"></D:LabelTextOnly></div>
                <div class="value"><D:Label ID="lblBroadcastedServer" runat="server"></D:Label></div>
            </li>
            <li>
                <div class="label"><D:LabelTextOnly ID="LabelTextOnly2" runat="server" Text="<%$ Resources:strings, Tools_LastRefresh %>"></D:LabelTextOnly></div>
                <div class="value"><D:Label ID="lblLastDatabaseRefresh" runat="server"></D:Label></div>
            </li>
        </ul>

        <D:PlaceHolder runat="server" ID="plhDatabaseManagementSwitching">
        <ul class="buttonlist">
            <li><D:HyperLink ID="hlDatabaseResetConnectionString" NavigateUrl="~/Tools.aspx?switchToDb=0&breakMirror=0" runat="server" Visible="true" Text="<%$ Resources:strings, Tools_Database_Reset_ConnectionString %>" /></li>
            <li><D:HyperLink ID="hlDatabaseSwitchToPrimary" NavigateUrl="~/Tools.aspx?switchToDb=1&breakMirror=0" runat="server" Visible="true" Text="<%$ Resources:strings, Tools_Database_FailoverToPrimary %>" /></li>            
            <li><D:HyperLink ID="hlDatabaseSwitchToSecondary" NavigateUrl="~/Tools.aspx?switchToDb=2&breakMirror=0" runat="server" Visible="true" Text="<%$ Resources:strings, Tools_Database_FailoverToSecondary %>" /></li>
            <li><D:HyperLink ID="hlDatabaseBreakToPrimary" NavigateUrl="~/Tools.aspx?switchToDb=1&breakMirror=1" runat="server" Visible="true" Text="<%$ Resources:strings, Tools_Database_BreakMirrorToPrimary %>" /></li>
            <li><D:HyperLink ID="hlDatabaseBreakToSecondary" NavigateUrl="~/Tools.aspx?switchToDb=2&breakMirror=1" runat="server" Visible="true" Text="<%$ Resources:strings, Tools_Database_BreakMirrorToSecondary %>" /></li>
        </ul>
        </D:PlaceHolder>

        <D:PlaceHolder runat="server" ID="plhDatabaseManagementUnavailable" Visible="false">
        <ul class="details">
            <li>
                <div class="label"><D:LabelTextOnly runat="server" Text="<%$ Resources:strings, Tools_Cloud_SwitchTo %>"></D:LabelTextOnly></div>
                <div class="value"><D:LabelTextOnly runat="server" Text="<%$ Resources:strings, Tools_Cloud_Unavailable %>"></D:LabelTextOnly></div>
            </li>
        </ul>    
        </D:PlaceHolder>    
    </D:PlaceHolder>

    <D:PlaceHolder runat="server" ID="plhConfirmCloudEnvironmentSwitch" Visible="false">
        <h1><D:LabelTextOnly ID="lblConfirmEnvironmentSwitchTitle" runat="server" Text="<%$ Resources:strings, System_state_cloudstatus_switch_confirm %>"></D:LabelTextOnly></h1>
        <p>&nbsp;</p>
        <ul class="buttonlist">
            <li><D:HyperLink ID="hlConfirmEnvironmentSwitchNo" runat="server" Visible="true" NavigateUrl="~/Tools.aspx" Text="<%$ Resources:strings, Generic_No %>"></D:HyperLink></li>
            <li>&nbsp;<br />&nbsp;</li>
            <li><D:LinkButton ID="hlConfirmEnvironmentSwitchYes" runat="server" Visible="true" PreSubmitWarning="<%$ Resources:strings, Tools_Switch_Confirmation %>" Text="<%$ Resources:strings, Generic_Yes %>"></D:LinkButton></li>
        </ul>
        <p>&nbsp;</p>
    </D:PlaceHolder>

    <D:PlaceHolder runat="server" ID="plhCloudEnvironmentSwitchResult" Visible="false">
        <h1>Switch results</h1>   
        <p>&nbsp;</p>     
        <ul class="details">
            <li>
                <div class="label"><D:LabelTextOnly ID="lblNocStatusService1ResultLabel" runat="server" Text="<%$ Resources:strings, System_state_nocstatusservice_1 %>"></D:LabelTextOnly></div>
                <div class="value"><D:Label ID="lblNocStatusService1Result" runat="server" Text="<%$ Resources:strings, Tools_NocSwitchResult_NotAvailable %>"></D:Label></div>
            </li>        
            <li>
                <div class="label"><D:LabelTextOnly ID="lblNocStatusService2ResultLabel" runat="server" Text="<%$ Resources:strings, System_state_nocstatusservice_2 %>"></D:LabelTextOnly></div>
                <div class="value"><D:Label ID="lblNocStatusService2Result" runat="server" Text="<%$ Resources:strings, Tools_NocSwitchResult_NotAvailable %>"></D:Label></div>
            </li>
         </ul>
        <p>&nbsp;</p>
        <ul class="buttonlist">
            <li><D:HyperLink runat="server" NavigateUrl="~/Tools.aspx" Visible="true" Text="<%$ Resources:strings, Tools_Continue_To_Tools %>"></D:HyperLink></li>
        </ul>
    </D:PlaceHolder>  
        
    <D:PlaceHolder runat="server" ID="plhDatabaseSwitchResult" Visible="false">        
        <D:PlaceHolder runat="server" ID="plhDatabaseSwitchResultReport"></D:PlaceHolder>
        <p>&nbsp;</p>
        <ul class="buttonlist">
            <li><D:HyperLink runat="server" NavigateUrl="~/Tools.aspx" Visible="true" Text="<%$ Resources:strings, Tools_Continue_To_Tools %>"></D:HyperLink></li>
        </ul>
    </D:PlaceHolder>

    <D:PlaceHolder runat="server" ID="plhCacheManagement">       
        <ul class="details">
            <li>            
                <div class="title"><D:LabelTextOnly runat="server" Text="<%$ Resources:strings, Tools_Cache_Title %>"></D:LabelTextOnly></div>
            </li>  
            <li>
                <div class="label"><D:LabelTextOnly ID="lblCacheLastClearCMSLabel" runat="server" Text="<%$ Resources:strings, Generic_CMS %>"></D:LabelTextOnly></div>
                <div class="value"><D:Label ID="lblCacheLastClearCMS" runat="server"></D:Label></div>
            </li>
            <li>
                <div class="label"><D:LabelTextOnly ID="lblCacheLastClearWebserviceLabel" runat="server" Text="<%$ Resources:strings, Generic_Webservice %>"></D:LabelTextOnly></div>
                <div class="value"><D:Label ID="lblCacheLastClearWebservice" runat="server"></D:Label></div>
            </li>
            <li>
                <div class="label"><D:LabelTextOnly ID="lblCacheLastClearCometServerLabel" runat="server" Text="<%$ Resources:strings, Generic_CometServer %>"></D:LabelTextOnly></div>
                <div class="value"><D:Label ID="lblCacheLastClearCometServer" runat="server"></D:Label></div>
            </li>
            <li>
                <div class="label"><D:LabelTextOnly ID="lblCacheLastClearNOCLabel" runat="server" Text="<%$ Resources:strings, Generic_NOC %>"></D:LabelTextOnly></div>
                <div class="value"><D:Label ID="lblCacheLastClearNOC" runat="server"></D:Label></div>
            </li>
        </ul>
        <ul class="buttonlist">
            <li><D:HyperLink ID="hlClearCacheCms" NavigateUrl="~/Tools.aspx?clearCache=1" runat="server" Visible="true" Text="<%$ Resources:strings, Tools_Cache_Cms %>" /></li>
            <li><D:HyperLink ID="hlClearCacheWebservice" NavigateUrl="~/Tools.aspx?clearCache=2" runat="server" Visible="true" Text="<%$ Resources:strings, Tools_Cache_Webservice %>" /></li>
            <li><D:HyperLink ID="hlClearCacheCometServer" NavigateUrl="~/Tools.aspx?clearCache=3" runat="server" Visible="true" Text="<%$ Resources:strings, Tools_Cache_CometServer %>" /></li>
            <li><D:HyperLink ID="hlClearCacheMobileNoc" NavigateUrl="~/Tools.aspx?clearCache=4" runat="server" Visible="true" Text="<%$ Resources:strings, Tools_Cache_MobileNoc %>" /></li>
        </ul>
    </D:PlaceHolder>

    <D:PlaceHolder runat="server" ID="phlSetCometHandlers" Visible="false">
        <ul class="details">
            <li>
                <div class="title"><D:LabelTextOnly runat="server" Text="<%$ Resources:strings, Tools_Cloud_Set_CometHandlers %>"></D:LabelTextOnly></div>
            </li>
            <li>
                <div class="label"><D:LabelTextOnly runat="server" Text="<%$ Resources:strings, Tools_Cloud_CometHandlers %>"></D:LabelTextOnly></div>
                <div class="value"><D:CheckBoxList ID="cblCometHandlers" runat="server" UseDataBinding="true"></D:CheckBoxList></div>
            </li>
        </ul>
        <p>&nbsp;</p>
        <ul class="buttonlist">
            <li><D:HyperLink ID="HyperLink2" runat="server" Visible="true" NavigateUrl="~/Tools.aspx" Text="<%$ Resources:strings, Generic_No %>"></D:HyperLink></li>
            <li>&nbsp;<br />&nbsp;</li>
            <li><D:LinkButton ID="btnSwitchCometHandlers" runat="server" Visible="true" PreSubmitWarning="<%$ Resources:strings, Tools_Set_CometHandler_Confirmation %>" Text="<%$ Resources:strings, Generic_Yes %>"></D:LinkButton></li>
        </ul>
        <p>&nbsp;</p>
    </D:PlaceHolder>
    
    <D:PlaceHolder runat="server" ID="phlSetCometHandlersResult" Visible="false">
         <ul class="details">
            <li>
                <div class="title"><D:LabelTextOnly ID="LabelTextOnly3" runat="server" Text="<%$ Resources:strings, Tools_Cloud_Set_CometHandlers %>"></D:LabelTextOnly></div>
            </li>
            <li>
                <div class="value"><D:LabelTextOnly ID="lblCometHandlerResult" runat="server"></D:LabelTextOnly></div>
            </li>
        </ul> 
        <ul class="buttonlist">
            <li><D:HyperLink ID="HyperLink3" runat="server" NavigateUrl="~/Tools.aspx" Visible="true" Text="<%$ Resources:strings, Tools_Continue_To_Tools %>"></D:HyperLink></li>
        </ul>
    </D:PlaceHolder>

    <D:PlaceHolder runat="server" ID="plhClearCacheResult" Visible="false">
        <ul class="details">
            <p><D:PlaceHolder runat="server" ID="plhClearCacheResultReport"></D:PlaceHolder></p>
        </ul> 
        <ul class="buttonlist">
            <li><D:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/Tools.aspx" Visible="true" Text="<%$ Resources:strings, Tools_Continue_To_Tools %>"></D:HyperLink></li>
        </ul>
    </D:PlaceHolder>

    <D:PlaceHolder ID="plhApplicationPoolManagement" runat="server">
    <ul class="details">
        <li>
            <div class="title"><D:LabelTextOnly ID="LabelTextOnly1" runat="server" Text="<%$ Resources:strings, AppPool_Management %>"></D:LabelTextOnly></div>
        </li>
        <li>
            <div class="label"><D:LabelTextOnly ID="lblApplicationPoolStateCMSLabel" runat="server" Text="<%$ Resources:strings, Generic_CMS %>"></D:LabelTextOnly></div>
            <div class="value"><D:Label ID="lblApplicationPoolStateCMS" runat="server"></D:Label></div>
        </li>
        <li>
            <div class="label"><D:LabelTextOnly ID="lblApplicationPoolStateWebserviceLabel" runat="server" Text="<%$ Resources:strings, Generic_Webservice %>"></D:LabelTextOnly></div>
            <div class="value"><D:Label ID="lblApplicationPoolStateWebservice" runat="server"></D:Label></div>
        </li>
        <li>
            <div class="label"><D:LabelTextOnly ID="lblApplicationPoolStateCometServerLabel" runat="server" Text="<%$ Resources:strings, Generic_CometServer %>"></D:LabelTextOnly></div>
            <div class="value"><D:Label ID="lblApplicationPoolStateCometServer" runat="server"></D:Label></div>
        </li>
        <li>
            <div class="label"><D:LabelTextOnly ID="lblApplicationPoolStateMobileNOCLabel" runat="server" Text="<%$ Resources:strings, Generic_NOC %>"></D:LabelTextOnly></div>
            <div class="value"><D:Label ID="lblApplicationPoolStateMobileNOC" runat="server"></D:Label></div>
        </li>
    </ul>
    <ul class="buttonlist">
        <li><D:HyperLink ID="hlRecycleAppCms" NavigateUrl="~/Tools.aspx?recycleApp=1" runat="server" Visible="true" Text="<%$ Resources:strings, Tools_Recycle_Cms %>" /></li>
        <li><D:HyperLink ID="hlRecycleAppWebservice" NavigateUrl="~/Tools.aspx?recycleApp=2" runat="server" Visible="true" Text="<%$ Resources:strings, Tools_Recycle_Webservice %>" /></li>
        <li><D:HyperLink ID="hlRecycleAppCometServer" NavigateUrl="~/Tools.aspx?recycleApp=3" runat="server" Visible="true" Text="<%$ Resources:strings, Tools_Recycle_CometServer %>" /></li>
        <li><D:HyperLink ID="hlRecycleAppMobileNoc" NavigateUrl="~/Tools.aspx?recycleApp=4" runat="server" Visible="true" Text="<%$ Resources:strings, Tools_Recycle_NOC %>" /></li>
    </ul>
    </D:PlaceHolder>
    
    <D:PlaceHolder runat="server" ID="plhStaggeredOutput">

    </D:PlaceHolder>

</asp:Content>