﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Net.Sockets;

namespace Crave.RoomControl.Web
{
    public class WebRequest
    {
        private readonly TcpClient clientSocket;

        private StreamReader inputStream;
        private StreamWriter outputStream;

        public string RequestUrl { get; set; }
        public string Page { get; set; }
        public Dictionary<string, string> Arguments { get; set; }
        public Dictionary<string, string> Headers { get; set; }
        public string IpAddress { get; set;  }

        public bool Connected
        {
            get
            {
                try
                {
                    if (clientSocket != null && clientSocket.Client != null && clientSocket.Client.Connected)
                    {
                        /* pear to the documentation on Poll:
                         * When passing SelectMode.SelectRead as a parameter to the Poll method it will return 
                         * -either- true if Socket.Listen(Int32) has been called and a connection is pending;
                         * -or- true if data is available for reading; 
                         * -or- true if the connection has been closed, reset, or terminated; 
                         * otherwise, returns false
                         */

                        // Detect if client disconnected
                        if (clientSocket.Client.Poll(0, SelectMode.SelectRead))
                        {
                            byte[] buff = new byte[1];
                            if (clientSocket.Client.Receive(buff, SocketFlags.Peek) == 0)
                            {
                                // Client disconnected
                                return false;
                            }
                            
                            return true;
                        }

                        return true;
                    }
                    
                    return false;
                }
                catch
                {
                    return false;
                }
            }
        }

        public WebRequest(TcpClient clientSocket)
        {
            this.clientSocket = clientSocket;
            this.clientSocket.LingerState = new LingerOption(false, 0);

            this.IpAddress = ((IPEndPoint)this.clientSocket.Client.RemoteEndPoint).Address.ToString();

            this.RequestUrl = string.Empty;
            this.Arguments = new Dictionary<string, string>();
            this.Headers = new Dictionary<string, string>();
        }

        public void Process()
        {
            this.inputStream = new StreamReader(new BufferedStream(this.clientSocket.GetStream()));
            this.outputStream = new StreamWriter(new BufferedStream(this.clientSocket.GetStream()));
            try
            {
                this.ParseRequest();
                this.ParseHeaders();
            }
            catch (Exception)
            {
                this.WriteFailureHeaders();
                this.Close();
            }
        }

        public void Close()
        {
            /*try
            {
                if (this.outputStream != null)
                {
                    this.outputStream.Flush();
                    this.outputStream.Close();
                }
            }
            catch (Exception e)
            {
                // Could be that the connection was already closed
                Trace.WriteLine(e.Message);
            }

            if (this.clientSocket != null)
            {
                //this.clientSocket.Client.Shutdown(SocketShutdown.Both);
                if (this.clientSocket.Connected)
                {
                    this.clientSocket.Client.Disconnect(false);
                }

                this.clientSocket.Client.Close(1000);
    
                this.clientSocket.Close();
            }

            try
            {
                if (this.inputStream != null)
                {
                    this.inputStream.Close();
                }
            }
            catch (Exception e)
            {
                // Could be that the connection was already closed
                Trace.WriteLine(e.Message);
            }*/

            if (this.outputStream != null)
            {
                this.outputStream.Flush();
            }

            this.clientSocket.GetStream().Close();
            this.clientSocket.Close();
        }

        public void CloseWithError(string error)
        {
            this.WriteFailureHeaders();
            this.outputStream.WriteLine(error);
            this.Close();
        }

        private void ParseRequest()
        {
            string request = this.inputStream.ReadLine();
            if (request != null)
            {
                string[] tokens = request.Split(' ');
                if (tokens.Length > 1)
                {
                    this.RequestUrl = tokens[1];

                    if (!string.IsNullOrWhiteSpace(this.RequestUrl))
                    {
                        int argsIndex = this.RequestUrl.IndexOf("?", StringComparison.Ordinal);

                        this.Page = this.RequestUrl.TrimStart('/');
                        if (argsIndex > 0)
                        {
                            this.Page = this.Page.Substring(0, argsIndex - 1);

                            string allArguments = this.RequestUrl.Substring(argsIndex + 1);
                            string[] arguments = allArguments.Split('&');
                            foreach (string argument in arguments)
                            {
                                string[] argumentTokens = argument.Split('=');
                                if (argumentTokens.Length > 1)
                                {
                                    this.Arguments[argumentTokens[0]] = argumentTokens[1];
                                }
                            }
                        }
                    }
                }    
            }
        }

        private void ParseHeaders()
        {
            string line;
            while ((line = this.inputStream.ReadLine()) != null)
            {
                if (line.Equals(""))
                {
                    break;
                }

                int separator = line.IndexOf(":", StringComparison.Ordinal);
                if (separator >= 0)
                {
                    string name = line.Substring(0, separator);
                    string value = line.Substring(separator + 1);
                    value = value.Trim();

                    this.Headers[name] = value;
                }
            }
        }

        public void WriteOkHeaders(string message = "")
        {
            this.outputStream.WriteLine("HTTP/1.0 200 OK");
            this.outputStream.WriteLine("Content-Type: text/html");
            this.outputStream.WriteLine("Connection: close");
            this.outputStream.WriteLine("");
            this.outputStream.WriteLine(message);
        }

        public void WriteFailureHeaders()
        {
            this.outputStream.WriteLine("HTTP/1.0 500 Internal Server Error");
            this.outputStream.WriteLine("Connection: close");
            this.outputStream.WriteLine("");
        }

        public void WriteJson(string json)
        {
            this.outputStream.WriteLine("HTTP/1.0 200 OK");
            this.outputStream.WriteLine("Content-Length: {0}", json.Length);
            this.outputStream.WriteLine("Content-Type: text/json; charset=utf-8");
            this.outputStream.WriteLine("Connection: close");
            this.outputStream.WriteLine("");
            this.outputStream.Write(json);
        }
    }
}
