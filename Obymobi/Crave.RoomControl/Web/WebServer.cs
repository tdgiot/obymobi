﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;

namespace Crave.RoomControl.Web
{
    public class WebServer
    {
        private readonly TcpListener serverSocket;

        private CancellationTokenSource listenTokenSource;

        public event Action<WebRequest> NewRequest;

        private bool listening;

        public WebServer(int port)
        {
            IPEndPoint endPoint = new IPEndPoint(IPAddress.Any, port);
            this.serverSocket = new TcpListener(endPoint);
        }


        public void Start()
        {
            this.serverSocket.Start();

            this.listenTokenSource = new CancellationTokenSource();
            Task.Factory.StartNew(this.ServerListen, this.listenTokenSource.Token);
	    }

        public void Stop()
        {
            this.listening = false;
            if (this.listenTokenSource != null)
            {
                this.listenTokenSource.Cancel();
            }

            if (this.serverSocket != null)
            {
                this.serverSocket.Stop();
            }
        }

	    private void ServerListen()
	    {
	        this.listening = true;
	        while (this.listening)
	        {
	            try
	            {
	                TcpClient clientSocket = this.serverSocket.AcceptTcpClient();
	                Task.Factory.StartNew(() => this.ProcessNewRequest(clientSocket));
	            }
	            catch (Exception)
	            {
	                if (!this.listening)
	                    return;
	            }
	        }
	    }

        private void ProcessNewRequest(TcpClient clientSocket)
        {
            WebRequest webRequest = new WebRequest(clientSocket);
            webRequest.Process();
            if (this.NewRequest != null && !string.IsNullOrWhiteSpace(webRequest.RequestUrl))
            {
                this.NewRequest(webRequest);
            }
            else
            {
                webRequest.WriteOkHeaders("Crave Room Control Server (" + DateTime.Now + ")");
                webRequest.Close();
            }
        }
    }
}
