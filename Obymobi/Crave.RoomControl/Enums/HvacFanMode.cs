﻿using Dionysos;

namespace Crave.RoomControl.Enums
{
    public enum HvacFanMode
    {
        /// <summary>
        /// Unknown
        /// </summary>
        [StringValue("Unknown")]
        Unknown = 0,

        /// <summary>
        /// Auto
        /// </summary>
        [StringValue("Auto")]
        Auto = 1,

        /// <summary>
        /// Low
        /// </summary>
        [StringValue("Low")]
        Low = 2,

        /// <summary>
        /// High
        /// </summary>
        [StringValue("High")]
        High = 3,

        /// <summary>
        /// Medium
        /// </summary>
        [StringValue("Medium")]
        Medium = 4,

        /// <summary>
        /// Off
        /// </summary>
        [StringValue("Off")]
        Off = 5
    }
}
