﻿using Dionysos;

namespace Crave.RoomControl.Enums
{
    public enum HvacScale
    {
        /// <summary>
        /// Kelvin
        /// </summary>
        [StringValue("Kelvin")]
        Kelvin = 0,

        /// <summary>
        /// Celsius
        /// </summary>
        [StringValue("Celsius")]
        Celsius = 1,

        /// <summary>
        /// Fahrenheit
        /// </summary>
        [StringValue("Fahrenheit")]
        Fahrenheit = 2
    }
}
