﻿using Dionysos;

namespace Crave.RoomControl.Enums
{
    public enum HvacMode
    {
        /// <summary>
        /// Unknown
        /// </summary>
        [StringValue("Unknown")]
        Unknown = 0,

        /// <summary>
        /// Off
        /// </summary>
        [StringValue("Off")]
        Off = 1,

        /// <summary>
        /// Auto
        /// </summary>
        [StringValue("Auto")]
        Auto = 2,

        /// <summary>
        /// Heat
        /// </summary>
        [StringValue("Heat")]
        Heat = 3,

        /// <summary>
        /// Cool
        /// </summary>
        [StringValue("Cool")]
        Cool = 4
    }
}
