﻿namespace Crave.RoomControl.Enums
{
    public enum CommandType
    {
        HvacSetTargetTemperature = 1,
        HvacSetMode = 2,
        HvacSetFanMode = 3,
        HvacSetScale = 4,
        BlindOpen = 5,
        BlindClose = 6,
        BlindToggle = 7,
        BlindStop = 8,
        LightSetLevel = 9,
        SceneActivate = 10,
        SceneDeactivate = 11,
        DoNotDisturbSetState = 12,
        MakeUpRoomSetState = 13,
        ToggleLight = 14,
    }
}
