﻿namespace Crave.RoomControl.Enums
{
    public enum AttributeType
    {
        HvacScale = 1,
        HvacTargetTemperature = 2,
        HvacMode = 3,
        HvacFanMode = 4,
        HvacTemperature = 5,
        DoNotDisturbState = 6,
        MakeUpRoomState = 7,
        LightLevel = 8
    }
}
