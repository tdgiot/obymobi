﻿using System;
using System.Threading.Tasks;
using Crave.RoomControl.Enums;
using Crave.RoomControl.Models;
using Crave.RoomControl.Web;

namespace Crave.RoomControl
{
    public interface IIntegration
    {
        bool Start();
        void Stop();

        void Synchronize();

        Room GetRoom(string roomId);
        Task<Room> RequestRoomAsync(string roomId);

        event Action<RoomUpdate> OnRoomAttributeUpdated;

        void HandleDebugRequest(WebRequest request);
        void HandleCommandRequest(Room room, CommandType command, string parameter1, string parameter2, int deviceAddress);

        void BroadcastAttributeUpdate(string roomId, string componentId, AttributeType attributeType, int value, int parameter = 0);
    }
}
