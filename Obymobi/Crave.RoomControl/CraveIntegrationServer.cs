﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Crave.RoomControl.Enums;
using Crave.RoomControl.Models;
using Crave.RoomControl.Web;
using Dionysos;
using Newtonsoft.Json;
using Obymobi.Security;
using Serilog;

namespace Crave.RoomControl
{
    public class CraveIntegrationServer
    {
        private readonly ILogger logger;
        private readonly int port;
        private readonly IIntegration integration;

        private readonly object roomListenersLock = new object();
        private readonly List<RoomListener> roomListeners = new List<RoomListener>();
        
        private Timer roomListenerCheckTimer;

        private const string TOKEN = "smdepy5zehj9z3hiy942iff4tdaalq9pmw3kz5dq";

        private WebServer webServer;
        
        public CraveIntegrationServer(ILogger logger, int port, IIntegration integration)
        {
            this.logger = logger;
            this.port = port;
            this.integration = integration;
            this.integration.OnRoomAttributeUpdated += OnRoomAttributeUpdated;
        }

        public void Start()
        {
            if (this.webServer != null)
            {
                return;
            }

            this.logger.Information("[CIS] Service listening on port {port}", this.port);
            
            this.webServer = new WebServer(port);
            this.webServer.NewRequest += WebServer_NewRequest;
            this.webServer.Start();

            this.roomListenerCheckTimer = new Timer(state =>
                                                    {
                                                        lock(this.roomListeners)
                                                        {
                                                            for (int i = this.roomListeners.Count - 1; i >= 0; i--)
                                                            {
                                                                if (!this.roomListeners[i].CheckConnection())
                                                                {
                                                                    this.roomListeners[i].CloseWithJson("{}");
                                                                    this.roomListeners.RemoveAt(i);
                                                                }
                                                            }
                                                        }
                                                    }, null, TimeSpan.Zero, TimeSpan.FromMinutes(1));
        }

        public void Stop()
        {
            if (this.roomListenerCheckTimer != null)
            {
                this.roomListenerCheckTimer.Dispose();
                this.roomListenerCheckTimer = null;
            }

            if (this.webServer != null)
            {
                this.webServer.Stop();
                this.webServer = null;
            }
        }

        private void WebServer_NewRequest(WebRequest request)
        {
            this.logger.Debug("[CIS] ({ip}) New request: {url}", request.IpAddress, request.RequestUrl);

            if (request.Page.IsNullOrWhiteSpace())
            {
                request.WriteOkHeaders("Crave Room Control Server (" + DateTime.Now + ")");
                request.Close();
                return;
            }
            
            if (request.Page.Equals("debug", StringComparison.InvariantCultureIgnoreCase))
            {
                if (request.Arguments.ContainsKey("notify"))
                {
                    HandleDebugNotifyRequest(request);
                }
                else
                {
                    this.integration.HandleDebugRequest(request);    
                }
                
                request.WriteOkHeaders();
                request.Close();

                return;
            }

            string roomId;
            if (!request.Arguments.TryGetValue("roomId", out roomId) || string.IsNullOrWhiteSpace(roomId))
            {
                request.CloseWithError("Request is missing roomId argument.");
                return;
            }

            if (request.Page.Equals("items", StringComparison.InvariantCultureIgnoreCase))
            {
                this.HandleItemsRequest(roomId, request);
            }
            else if (request.Page.Equals("listen", StringComparison.InvariantCultureIgnoreCase))
            {
                this.HandleListenRequest(roomId, request);
            }
            else if (request.Page.Equals("command", StringComparison.InvariantCultureIgnoreCase))
            {
                if (this.HandleSecurity(roomId, request))
                {
                    this.HandleCommandRequest(roomId, request);
                }
                else
                {
                    request.WriteFailureHeaders();
                    request.Close();
                }
            }
            else
            {
                request.CloseWithError("Unknown command");
            }
        }

        private void HandleDebugNotifyRequest(WebRequest request)
        {
            string roomId;
            if (!request.Arguments.TryGetValue("roomId", out roomId) || string.IsNullOrWhiteSpace(roomId))
            {
                return;
            }

            Room room = this.integration.RequestRoomAsync(roomId).Result;
            if (room == null)
            {
                return;
            }

            string commandStr;
            CommandType command;
            if (request.Arguments.TryGetValue("command", out commandStr) && Enum.TryParse(commandStr, out command))
            {
                string parameter1;
                request.Arguments.TryGetValue("parameter1", out parameter1);
                int parameter1AsInt;
                int.TryParse(parameter1, out parameter1AsInt);

                string parameter2;
                request.Arguments.TryGetValue("parameter2", out parameter2);
                int parameter2AsInt;
                int.TryParse(parameter2, out parameter2AsInt);

                switch (command)
                {
                    case CommandType.HvacSetTargetTemperature:
                        this.integration.BroadcastAttributeUpdate(roomId, "1", AttributeType.HvacTargetTemperature, parameter1AsInt);
                        break;
                    case CommandType.HvacSetMode:
                        this.integration.BroadcastAttributeUpdate(roomId, "1", AttributeType.HvacMode, parameter1AsInt);
                        break;
                    case CommandType.HvacSetFanMode:
                        this.integration.BroadcastAttributeUpdate(roomId, "1", AttributeType.HvacFanMode, parameter1AsInt);
                        break;
                    case CommandType.HvacSetScale:
                        this.integration.BroadcastAttributeUpdate(roomId, "1", AttributeType.HvacScale, parameter1AsInt);
                        break;
                    case CommandType.LightSetLevel:
                        this.integration.BroadcastAttributeUpdate(roomId, parameter1, AttributeType.LightLevel, parameter1AsInt);
                        break;
                    case CommandType.DoNotDisturbSetState:
                        this.integration.BroadcastAttributeUpdate(roomId, roomId, AttributeType.DoNotDisturbState, parameter1AsInt);
                        break;
                    case CommandType.MakeUpRoomSetState:
                        this.integration.BroadcastAttributeUpdate(roomId, roomId, AttributeType.MakeUpRoomState, parameter1AsInt);
                        break;
                    case CommandType.ToggleLight:
                        this.integration.BroadcastAttributeUpdate(roomId, parameter1, AttributeType.LightLevel, room.GetLight(parameter1AsInt).Level);
                        break;
                }
            }
        }

        private bool HandleSecurity(string roomId, WebRequest request)
        {
            string hash;
            if (!request.Arguments.TryGetValue("hash", out hash))
            {
                this.logger.Warning("[CIS] ({ip}) [{roomId}] No security hash found", request.IpAddress, roomId);
                return false;
            }

            string commandStr;
            request.Arguments.TryGetValue("command", out commandStr);
            
            string parameter1;
            if (!request.Arguments.TryGetValue("parameter1", out parameter1))
            {
                parameter1 = string.Empty;
            }

            string parameter2;
            if (!request.Arguments.TryGetValue("parameter2", out parameter2))
            {
                parameter2 = string.Empty;
            }
            
            string argumentStr;
            if (!request.Arguments.TryGetValue("deviceAddress", out argumentStr))
            {
                argumentStr = string.Empty;
            }

            bool isValid = Hasher.IsHashValidForParameters(hash, TOKEN, roomId, commandStr, parameter1, parameter2, argumentStr);
            if (!isValid)
            {
                this.logger.Warning("[CIS] ({ip}) [{roomId}] Security hash is not valid", request.IpAddress, roomId);
                return false;
            }
            return true;
        }

        private void HandleCommandRequest(string roomId, WebRequest request)
        {
            Room room = this.integration.GetRoom(roomId);

            if (room != null)
            {
                string commandStr;
                CommandType command;
                if (request.Arguments.TryGetValue("command", out commandStr) && Enum.TryParse(commandStr, out command))
                {
                    string parameter1;
                    request.Arguments.TryGetValue("parameter1", out parameter1);

                    if (!parameter1.IsNullOrWhiteSpace() && 
                        parameter1.Contains("-", StringComparison.InvariantCultureIgnoreCase) &&
                        !parameter1.StartsWith("-", StringComparison.InvariantCultureIgnoreCase))
                    {
                        parameter1 = parameter1.Split('-')[1];
                    }

                    string parameter2;
                    request.Arguments.TryGetValue("parameter2", out parameter2);

                    string argumentStr;
                    int deviceAddress;
                    request.Arguments.TryGetValue("deviceAddress", out argumentStr);
                    int.TryParse(argumentStr, out deviceAddress);

                    this.logger.Information("[CIS] ({ip}) Command Request (Room: {roomId}, Command: {command}, Param1: {parameter1}, Param2: {parameter2}, DeviceAddress: {address})", request.IpAddress, room.RoomId, command, parameter1, parameter2, deviceAddress);

                    this.integration.HandleCommandRequest(room, command, parameter1, parameter2, deviceAddress);
                }
            }

            request.WriteOkHeaders("OK");
            request.Close();
        }

        private void HandleItemsRequest(string roomIds, WebRequest request)
        {
            string[] roomIdParts = roomIds.Split(new [] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            if (roomIdParts.Length > 0)
            {
                List<Room> rooms = new List<Room>();
                foreach (string roomId in roomIdParts)
                {
                    Room result = this.integration.RequestRoomAsync(roomId).Result;
                    if (result != null)
                    {
                        rooms.Add(result);
                    }
                }

                if (rooms.Count > 0)
                {
                    string json = JsonConvert.SerializeObject(rooms.ToArray());
                    request.WriteJson(json);
                }
                else
                {
                    request.WriteOkHeaders();
                }
            }
            request.Close();
        }

        private void HandleListenRequest(string roomIds, WebRequest request)
        {
            string[] roomIdParts = roomIds.Split(new [] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            if (roomIdParts.Length > 0)
            {
                string tokenStr;
                long token = 0;
                if (request.Arguments.TryGetValue("token", out tokenStr))
                {
                    long.TryParse(tokenStr, out token);
                }

                RoomUpdate[] updates = null;
                if (token > 0)
                {
                    updates = this.GetUpdatesForToken(roomIdParts, token);
                }
                if (updates != null && updates.Length > 0)
                {
                    long timestamp = updates.Max(x => x.Timestamp);
                    string json = JsonConvert.SerializeObject(new RoomUpdates(timestamp, updates));

                    request.WriteJson(json);
                    request.Close();
                }
                else
                {
                    this.AddListener(roomIdParts, request);
                }
            }
        }

        private RoomUpdate[] GetUpdatesForToken(string[] roomIds, long token)
        {
            List<RoomUpdate> updates = new List<RoomUpdate>();
            foreach (string roomId in roomIds)
            {
                Room room = this.integration.GetRoom(roomId);
                if (room != null)
                {
                    updates.AddRange(room.GetUpdatesForToken(token));
                }
            }
            return updates.ToArray();
        }

        private void AddListener(string[] roomIds, WebRequest request)
        {
            RoomListener listener = new RoomListener(request);
            listener.AddRoomIds(roomIds);

            lock(this.roomListenersLock)
            {
                this.roomListeners.Add(listener);
            }
        }

        private List<RoomListener> GetListenersForRoom(string roomId)
        {
            List<RoomListener> listeners = new List<RoomListener>();

            lock(this.roomListenersLock)
            {
                for (int i = this.roomListeners.Count - 1; i >= 0; i--)
                {
                    if (this.roomListeners[i].ContainsRoomId(roomId))
                    {
                        listeners.Add(this.roomListeners[i]);
                        this.roomListeners.RemoveAt(i);
                    }
                }
            }

            return listeners;
        }

        private void OnRoomAttributeUpdated(RoomUpdate update)
        {
            Task.Factory.StartNew(() =>
                                  {
                                      Room room = this.integration.GetRoom(update.RoomId);
                                      if (room != null)
                                      {
                                          room.AddUpdate(update);

                                          List<RoomListener> listeners = this.GetListenersForRoom(update.RoomId);
                                          if (listeners.Count > 0)
                                          {
                                              string json = JsonConvert.SerializeObject(new RoomUpdates(update.Timestamp, update));
                                              foreach (RoomListener listener in listeners)
                                              {
                                                  listener.CloseWithJson(json);
                                              }
                                          }
                                      }
                                  });
        }
    }
}