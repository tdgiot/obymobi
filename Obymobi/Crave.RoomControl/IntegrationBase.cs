﻿using System;
using System.Collections.Concurrent;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using Crave.RoomControl.Enums;
using Crave.RoomControl.Models;
using Crave.RoomControl.Web;

namespace Crave.RoomControl
{
    public abstract class IntegrationBase : IIntegration
    {
        private readonly ConcurrentDictionary<string, Room> rooms = new ConcurrentDictionary<string, Room>();

        public event Action<RoomUpdate> OnRoomAttributeUpdated;

        public abstract bool Start();
        public abstract void Stop();
        public abstract void Synchronize();
        public abstract Task<Room> RequestRoomAsync(string roomId);
        public abstract void HandleDebugRequest(WebRequest request);
        public abstract void HandleCommandRequest(Room room, CommandType command, string parameter1, string parameter2, int deviceAddress);

        protected abstract void RoomCreated(string roomId);

        public Room GetRoom(string roomId)
        {
            Room room;
            if (!this.rooms.TryGetValue(roomId, out room))
            {
                room = new Room(roomId);
                this.rooms.TryAdd(roomId, room);

                RoomCreated(roomId);
            }

            return room;
        }

        public bool RoomExists(string roomId)
        {
            return this.rooms.ContainsKey(roomId);
        }

        public void CreateEmptyRoom(string roomId)
        {
            this.rooms.AddOrUpdate(roomId, new Room(roomId), (s, room) => room);
        }

        public void BroadcastAttributeUpdate(string roomId, string componentId, AttributeType attributeType, double value, int parameter = 0)
        {
            RoomUpdate update = new RoomUpdate
            {
                RoomId = roomId,
                ComponentId = componentId,
                AttributeType = (int)attributeType,
                AttributeValue = value,
                AttributeParameter = parameter
            };
            RoomAttributeUpdated(update);
        }

        public void BroadcastAttributeUpdate(string roomId, string componentId, AttributeType attributeType, int value, int parameter = 0)
        {
            BroadcastAttributeUpdate(roomId, componentId, attributeType, (double)value, parameter);
        }
        
        protected void RoomAttributeUpdated(RoomUpdate update)
        {
            if (this.OnRoomAttributeUpdated != null)
            {
                this.OnRoomAttributeUpdated.Invoke(update);
            }
        }

        protected int GetIntArgument(string name, WebRequest request)
        {
            string argumentStr;
            int argument;
            request.Arguments.TryGetValue(name, out argumentStr);
            int.TryParse(argumentStr, out argument);
            return argument;
        }
    }
}