﻿using Crave.RoomControl.Enums;

namespace Crave.RoomControl.Models
{
    public class RoomHvac
    {
        public RoomHvac()
        {

        }

        public RoomHvac(string id)
        {
            this.Id = id;
        }

        public string Id { get; set; }

        /// <summary>
        /// Temperature in Kelvin
        /// </summary>
        public double RoomTemperature { get; set; }

        public HvacScale HvacScale { get; set; }

        /// <summary>
        /// Temperature in Kelvin
        /// </summary>
        public double HvacTemperature { get; set; }

        public HvacMode HvacMode { get; set; }
        public HvacFanMode HvacFanMode { get; set; }
    }
}