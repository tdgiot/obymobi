﻿using System;
using System.Collections.Generic;
using Crave.RoomControl.Web;

namespace Crave.RoomControl.Models
{
    internal class RoomListener
    {
        private readonly WebRequest webRequest;
        private readonly List<string> roomIds = new List<string>();
        private readonly DateTime created = DateTime.UtcNow;

        public RoomListener(WebRequest webRequest)
        {
            this.webRequest = webRequest;
        }

        public void AddRoomIds(params string[] roomId)
        {
            this.roomIds.AddRange(roomId);
        }

        public bool ContainsRoomId(string roomId)
        {
            return roomIds.Contains(roomId);
        }

        public void CloseWithJson(string json)
        {
            if (this.webRequest != null)
            {
                this.webRequest.WriteJson(json);
                this.webRequest.Close();
            }
        }

        public bool CheckConnection()
        {
            if (DateTime.UtcNow.Subtract(created).TotalMinutes >= 1)
            {
                return false;
            }

            return true;
        }
    }
}