﻿using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;

namespace Crave.RoomControl.Models
{
    public class Room
    {
        [JsonIgnore]
        private const int UPDATE_QUEUE_SIZE = 1000;

        [JsonIgnore]
        private readonly List<RoomUpdate> updates = new List<RoomUpdate>();

        [JsonIgnore]
        private readonly object updatesLock = new object();

        public string RoomId { get; set; }
        public bool DoNotDisturb { get; set; }
        public bool MakeUpRoom { get; set; }

        public List<RoomHvac> Hvacs { get; set; }
        public List<RoomLight> Lights { get; set; }

        [JsonIgnore]
        public int RoomIdAsInt { get; set; }

        public Room()
        {

        }

        public Room(string roomId)
        {
            this.RoomId = roomId;
            this.Hvacs = new List<RoomHvac>();
            this.Lights = new List<RoomLight>();

            int id;
            if (int.TryParse(this.RoomId, out id))
            {
                this.RoomIdAsInt = id;
            }
        }

        public void AddUpdate(RoomUpdate update)
        {
            lock (this.updatesLock)
            {
                this.updates.Insert(0, update);
                if (this.updates.Count > UPDATE_QUEUE_SIZE)
                {
                    this.updates.RemoveAt(this.updates.Count - 1);
                }
            }
        }

        public RoomUpdate[] GetUpdatesForToken(long token)
        {
            List<RoomUpdate> relevantUpdates = new List<RoomUpdate>();
            lock (this.updatesLock)
            {
                foreach (RoomUpdate update in this.updates)
                {
                    if (update.Timestamp > token)
                    {
                        relevantUpdates.Insert(0, update);
                    }
                    else
                    {
                        break;
                    }
                }
            }

            return relevantUpdates.ToArray();
        }

        public RoomLight GetLight(int id)
        {
            return GetLight(id.ToString());
        }

        public virtual RoomLight GetLight(string id)
        {
            return this.Lights.FirstOrDefault(x => x.Id == id);
        }

        public virtual RoomHvac GetHvac(string id)
        {
            return this.Hvacs.FirstOrDefault(x => x.Id == id);
        }
    }
}