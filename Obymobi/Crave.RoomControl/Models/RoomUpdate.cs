﻿using System;

namespace Crave.RoomControl.Models
{
    public class RoomUpdate
    {
        public RoomUpdate()
        {
            this.Timestamp = DateTime.UtcNow.Ticks;
        }

        public long Timestamp { get; set; }

        public string RoomId { get; set; }

        public string ComponentId { get; set; }

        public int AttributeType { get; set; }

        public double AttributeValue { get; set; }

        public int AttributeParameter { get; set; }
    }

    public class RoomUpdates
    {
        public RoomUpdates(long timestamp, params RoomUpdate[] updates)
        {
            this.Timestamp = timestamp;
            this.Updates = updates;
        }

        public long Timestamp { get; set; }

        public RoomUpdate[] Updates { get; set; }
    }
}