﻿using Crave.RoomControl.Enums;

namespace Crave.RoomControl.Models
{
    public class DummyRoom : Room
    {
        public DummyRoom(string id) : base(id)
        {
        }

        public override RoomHvac GetHvac(string id)
        {
            RoomHvac hvac = base.GetHvac(id);
            if (hvac == null)
            {
                hvac = new RoomHvac(id)
                {
                    HvacFanMode = HvacFanMode.Auto,
                    HvacMode = HvacMode.Auto,
                    HvacScale = HvacScale.Celsius,
                    HvacTemperature = 294.15,
                    RoomTemperature = 296.15
                };
                this.Hvacs.Add(hvac);
            }

            return hvac;
        }

        public override RoomLight GetLight(string id)
        {
            RoomLight light = base.GetLight(id);
            if (light == null)
            {
                light = new RoomLight("id", 0);
            }

            return light;
        }
    }
}