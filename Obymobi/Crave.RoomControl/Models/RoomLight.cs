﻿namespace Crave.RoomControl.Models
{
    public class RoomLight
    {
        public RoomLight()
        {

        }

        public RoomLight(int id, int level)
        {
            this.Id = id.ToString();
            this.Level = level;
        }

        public RoomLight(string id, int level)
        {
            this.Id = id;
            this.Level = level;
        }

        public string Id { get; set; }
        public int Level { get; set; }
    }
}