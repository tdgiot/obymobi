﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Crave.RoomControl.Enums;
using Crave.RoomControl.Models;
using Serilog;

namespace Crave.RoomControl
{
    public class DummyIntegration : IntegrationBase
    {
        private readonly ILogger logger;
        private readonly Dictionary<string, DummyRoom> rooms = new Dictionary<string, DummyRoom>();

        public DummyIntegration(ILogger logger)
        {
            this.logger = logger;
        }

        public override bool Start()
        {
            this.logger.Information("[Dummy] Start");
            return true;
        }

        public override void Stop()
        {
            this.logger.Information("[Dummy] Stop");
        }

        public override void Synchronize()
        {
            this.logger.Information("[Dummy] Synchronize");
        }

        public override Task<Room> RequestRoomAsync(string roomId)
        {
            DummyRoom room;
            if (!this.rooms.TryGetValue(roomId, out room))
            {
                this.logger.Information("[Dummy] Request new room {roomId}", roomId);

                room = new DummyRoom(roomId);
                room.Hvacs.Add(new RoomHvac("14")
                {
                    HvacFanMode = HvacFanMode.Auto,
                    HvacMode = HvacMode.Auto,
                    HvacScale = HvacScale.Celsius,
                    HvacTemperature = 294.15,
                    RoomTemperature = 296.15
                });
                /*room.Hvacs.Add(new RoomHvac("1")
                {
                    HvacFanMode = HvacFanMode.Auto,
                    HvacMode = HvacMode.Auto,
                    HvacScale = HvacScale.Celsius,
                    HvacTemperature = (decimal)294.15,
                    RoomTemperature = (decimal)296.15
                });
                room.Hvacs.Add(new RoomHvac("2")
                {
                    HvacFanMode = HvacFanMode.Auto,
                    HvacMode = HvacMode.Auto,
                    HvacScale = HvacScale.Celsius,
                    HvacTemperature = (decimal)294.15,
                    RoomTemperature = (decimal)296.15
                });
                room.Hvacs.Add(new RoomHvac("14")
                {
                    HvacFanMode = HvacFanMode.Auto,
                    HvacMode = HvacMode.Auto,
                    HvacScale = HvacScale.Celsius,
                    HvacTemperature = (decimal)294.15,
                    RoomTemperature = (decimal)296.15
                });

                room.Lights.Add(new RoomLight("1", 0));
                room.Lights.Add(new RoomLight("2", 50));
                room.Lights.Add(new RoomLight("3", 100));
                room.Lights.Add(new RoomLight("4", 0));*/
                room.Lights.Add(new RoomLight("16", 0));

                this.rooms.Add(roomId, room);
            }

            return Task.FromResult((Room)room);
        }

        protected override void RoomCreated(string roomId)
        {
            this.logger.Information("[Dummy] New room created: {roomId}", roomId);
        }

        public override void HandleDebugRequest(Web.WebRequest request)
        {
        }

        public override void HandleCommandRequest(Room room, CommandType command, string parameter1, string parameter2, int deviceAddress)
        {
            int parameter1AsInt;
            if (!int.TryParse(parameter1, out parameter1AsInt))
            {
                parameter1AsInt = 0;
            }

            int parameter2AsInt;
            if (!int.TryParse(parameter2, out parameter2AsInt))
            {
                parameter2AsInt = 0;
            }

            DummyRoom roomServer;
            if (this.rooms.TryGetValue(room.RoomId, out roomServer))
            {
                RoomHvac hvac;
                
                switch (command)
                {
                    case CommandType.HvacSetTargetTemperature:
                        hvac = roomServer.GetHvac(parameter2);
                        hvac.HvacTemperature = parameter1AsInt;
                        BroadcastAttributeUpdate(room.RoomId, hvac.Id, AttributeType.HvacTargetTemperature, parameter1AsInt);
                        break;
                    case CommandType.HvacSetMode:
                        hvac = roomServer.GetHvac(parameter2);
                        hvac.HvacMode = (HvacMode)parameter1AsInt;
                        BroadcastAttributeUpdate(room.RoomId, hvac.Id, AttributeType.HvacMode, parameter1AsInt);
                        break;
                    case CommandType.HvacSetFanMode:
                        hvac = roomServer.GetHvac(parameter2);
                        hvac.HvacFanMode = (HvacFanMode)parameter1AsInt;
                        BroadcastAttributeUpdate(room.RoomId, hvac.Id, AttributeType.HvacFanMode, parameter1AsInt);
                        break;
                    case CommandType.HvacSetScale:
                        hvac = roomServer.GetHvac(parameter2);
                        hvac.HvacScale = (HvacScale)parameter1AsInt;
                        BroadcastAttributeUpdate(room.RoomId, hvac.Id, AttributeType.HvacScale, parameter1AsInt);
                        break;
                    case CommandType.BlindOpen:
                        break;
                    case CommandType.BlindClose:
                        break;
                    case CommandType.BlindToggle:
                        break;
                    case CommandType.BlindStop:
                        break;
                    case CommandType.LightSetLevel:
                        roomServer.GetLight(parameter1).Level = parameter2AsInt;
                        BroadcastAttributeUpdate(room.RoomId, parameter1, AttributeType.LightLevel, parameter2AsInt);
                        break;
                    case CommandType.SceneActivate:
                        break;
                    case CommandType.SceneDeactivate:
                        break;
                    case CommandType.DoNotDisturbSetState:
                        roomServer.DoNotDisturb = parameter1AsInt == 1;
                        roomServer.MakeUpRoom = false;

                        BroadcastAttributeUpdate(room.RoomId, room.RoomId, AttributeType.DoNotDisturbState, room.DoNotDisturb ? 1 : 0);
                        BroadcastAttributeUpdate(room.RoomId, room.RoomId, AttributeType.MakeUpRoomState, room.MakeUpRoom ? 1 : 0);
                        break;
                    case CommandType.MakeUpRoomSetState:
                        roomServer.MakeUpRoom = parameter1AsInt == 1;
                        roomServer.DoNotDisturb = false;

                        BroadcastAttributeUpdate(room.RoomId, room.RoomId, AttributeType.DoNotDisturbState, room.DoNotDisturb ? 1 : 0);
                        BroadcastAttributeUpdate(room.RoomId, room.RoomId, AttributeType.MakeUpRoomState, room.MakeUpRoom ? 1 : 0);
                        break;
                    case CommandType.ToggleLight:
                        int level = roomServer.GetLight(parameter1).Level;
                        roomServer.GetLight(parameter1).Level = level == 0 ? 100 : 0;
                        BroadcastAttributeUpdate(room.RoomId, parameter1, AttributeType.LightLevel, roomServer.GetLight(parameter1).Level);
                        break;
                    default:
                        throw new ArgumentOutOfRangeException("command", command, null);
                }
            }
        }
    }
}