using NUnit.Framework;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Enums;
using Obymobi.Web.Analytics.DomainTypes;
using Obymobi.Web.Analytics.Model.TransactionsAppLess;
using Obymobi.Web.Analytics.Reports.TransactionsAppLess;
using Obymobi.Web.Tests.Builders;
using System.Linq;

namespace Obymobi.Web.Tests
{
    [TestFixture]
    public class OrderReportTest
    {
        private static class A
        {
            public static TransactionsAppLessRequestBuilder TransactionsAppLessRequest => new TransactionsAppLessRequestBuilder();
            public static TransactionsAppLessSheetFilterBuilder TransactionsAppLessSheetFilter => new TransactionsAppLessSheetFilterBuilder();
            public static OrderEntityBuilder OrderEntity => new OrderEntityBuilder();
            public static PaymentTransactionEntityBuilder PaymentTransactionEntity => new PaymentTransactionEntityBuilder();
            public static CompanyEntityBuilder CompanyEntity => new CompanyEntityBuilder();
            public static OrderRepositoryMockBuilder OrderRepositoryMock => new OrderRepositoryMockBuilder();
            public static CompanyRepositoryMockBuilder CompanyRepositoryMock => new CompanyRepositoryMockBuilder();
            public static OrderitemEntityBuilder OrderitemEntity => new OrderitemEntityBuilder();
            public static OrderitemAlterationitemEntityBuilder OrderitemAlterationitemEntity => new OrderitemAlterationitemEntityBuilder();
        }

        [Test]
        public void CreateReport_WithDefaultOptions_CreatesDefaultReport()
        {
            // Arrange
            var orderCollection = new OrderCollection
            {
                A.OrderEntity.Build(),
                A.OrderEntity.Build(),
                A.OrderEntity.Build(),
                A.OrderEntity.Build(),
                A.OrderEntity.Build()
            };

            var orderRepositoryMock = A.OrderRepositoryMock
                .W(orderCollection)
                .Build();

            var companyEntity = A.CompanyEntity.Build();

            var companyRepositoryMock = A.CompanyRepositoryMock
                .W(companyEntity)
                .Build();

            var request = A.TransactionsAppLessRequest.Build();

            var sut = new OrderReport(request, orderRepositoryMock, companyRepositoryMock);

            var totalOrderRevenue = orderCollection.Sum(o => o.Total);
            var totalProductRevenue = orderCollection.Sum(order => order.OrderitemCollection
                .Sum(orderItem => orderItem.PriceTotalInTax + orderItem.OrderitemAlterationitemCollection.Sum(alterationItem => alterationItem.PriceTotalInTax)));

            // Act
            var result = sut.CreateReport(out var spreadsheet);
            try
            {
                var sheetFilter = request.TransactionsAppLessSheetFilters.First();

                // Assert
                Assert.True(result);
                OrderReportValidation.AssertEqualTitleSection(string.Empty, "Orders Report", spreadsheet);
                OrderReportValidation.AssertEqualVendorName(companyEntity.Name, spreadsheet);
                //OrderReportValidation.AssertEqualReportingPeriod(request.FromDateUtc, request.UntilDateUtc, spreadsheet);
                OrderReportValidation.AssertOrderSubViewVisible(request, spreadsheet);
                OrderReportValidation.AssertOrderItemSubViewVisible(sheetFilter, spreadsheet);
                OrderReportValidation.AssertNotesSubViewVisible(sheetFilter, spreadsheet);
                OrderReportValidation.AssertExpectedOrderHeaderBackgroundAndBorder(spreadsheet);
                OrderReportValidation.AssertCorrectOrderDetailValues(sheetFilter, companyEntity, orderCollection, spreadsheet);
                //spreadsheet.SaveAs(@"G:\My Drive\OrderReport.xlsx");
            }
            finally
            {
                spreadsheet.Dispose();
            }
        }

        [Test]
        public void CreateReport_WithoutProductsAndWithSystemProducts_ShowsTheSystemProducts()
        {
            // Arrange
            var orderCollection = new OrderCollection
            {
                A.OrderEntity
                    .W(new[]
                    {
                        A.OrderitemEntity.W(OrderitemType.Product).Build(),
                        A.OrderitemEntity.W(OrderitemType.DeliveryCharge).Build(),
                        A.OrderitemEntity.W(OrderitemType.ServiceCharge).Build(),
                        A.OrderitemEntity.W(OrderitemType.Tip).Build()
                    })
                    .Build()
            };

            var orderRepositoryMock = A.OrderRepositoryMock
                .W(orderCollection)
                .Build();

            var companyEntity = A.CompanyEntity.Build();

            var companyRepositoryMock = A.CompanyRepositoryMock
                .W(companyEntity)
                .Build();

            var request = A.TransactionsAppLessRequest
                .W(A.TransactionsAppLessSheetFilter
                    .W((ShowProductsOption)false)
                    .W((ShowSystemProductsOption)true))
                .Build();

            var sut = new OrderReport(request, orderRepositoryMock, companyRepositoryMock);

            // Act
            var result = sut.CreateReport(out var spreadsheet);
            try
            {
                var sheetFilter = request.TransactionsAppLessSheetFilters.First();

                // Assert
                Assert.True(result);
                OrderReportValidation.AssertCorrectOrderDetailValues(sheetFilter, companyEntity, orderCollection, spreadsheet);
                //spreadsheet.SaveAs(@"G:\My Drive\OrderReport.xlsx");
            }
            finally
            {
                spreadsheet.Dispose();
            }
        }

        [Test]
        public void CreateReport_WithSystemProductTippingFilter_ShowsOnlyTheTipping()
        {
            // Arrange
            var orderCollection = new OrderCollection
            {
                A.OrderEntity
                    .W(new[]
                    {
                        A.OrderitemEntity.W(OrderitemType.Product).Build(),
                        A.OrderitemEntity.W(OrderitemType.DeliveryCharge).Build(),
                        A.OrderitemEntity.W(OrderitemType.ServiceCharge).Build(),
                        A.OrderitemEntity.W(OrderitemType.Tip).Build()
                    })
                    .Build()
            };

            var orderRepositoryMock = A.OrderRepositoryMock
                .W(orderCollection)
                .Build();

            var companyEntity = A.CompanyEntity.Build();

            var companyRepositoryMock = A.CompanyRepositoryMock
                .W(companyEntity)
                .Build();

            var request = A.TransactionsAppLessRequest
                .W(A.TransactionsAppLessSheetFilter
                    .W((ShowProductsOption)false)
                    .W((ShowSystemProductsOption)true)
                    .W((IncludedSystemProductTypes)new[] { (int)OrderitemType.Tip }))
                .Build();

            var sut = new OrderReport(request, orderRepositoryMock, companyRepositoryMock);

            // Act
            var result = sut.CreateReport(out var spreadsheet);
            try
            {
                var sheetFilter = request.TransactionsAppLessSheetFilters.First();

                // Assert
                Assert.True(result);
                OrderReportValidation.AssertCorrectOrderDetailValues(sheetFilter, companyEntity, orderCollection, spreadsheet);
                //spreadsheet.SaveAs(@"G:\My Drive\OrderReport.xlsx");
            }
            finally
            {
                spreadsheet.Dispose();
            }
        }

        [Test]
        public void CreateReport_WithServiceMethodFilter_ShowOnlyOrdersWithIncludedServiceMethod()
        {
            // Arrange
            ServiceMethodId serviceMethodIdRoomService = 1;
            ServiceMethodName serviceMethodNameRoomService = "Room service";
            ServiceMethodId serviceMethodIdPickup = 2;
            ServiceMethodName serviceMethodNamePickup = "Pick up";
            OutletId outletId = 1;

            var orderCollection = new OrderCollection
            {
                A.OrderEntity
                    .W(outletId)
                    .W(serviceMethodIdPickup)
                    .W(serviceMethodNamePickup)
                    .W(new[]
                    {
                        A.OrderitemEntity.W(OrderitemType.Product).Build(),
                        A.OrderitemEntity.W(OrderitemType.DeliveryCharge).Build(),
                        A.OrderitemEntity.W(OrderitemType.ServiceCharge).Build(),
                        A.OrderitemEntity.W(OrderitemType.Tip).Build()
                    })
                    .Build(),
                A.OrderEntity
                    .W(outletId)
                    .W(serviceMethodIdRoomService)
                    .W(serviceMethodNameRoomService)
                    .W(new[]
                    {
                        A.OrderitemEntity.W(OrderitemType.Product).Build(),
                        A.OrderitemEntity.W(OrderitemType.DeliveryCharge).Build(),
                        A.OrderitemEntity.W(OrderitemType.ServiceCharge).Build(),
                        A.OrderitemEntity.W(OrderitemType.Tip).Build()
                    })
                    .Build()
            };

            var request = A.TransactionsAppLessRequest
                .W(A.TransactionsAppLessSheetFilter
                    .W(outletId)
                    .W((IncludedServiceMethods)new int[] { serviceMethodIdPickup }))
                .Build();

            // Act
            var filteredOrders = orderCollection.ToOrderModels((TransactionsAppLessSheetFilter)request.SheetFilters[0]);

            // Assert
            Assert.AreEqual(1, filteredOrders.Count);
            Assert.AreEqual((string)serviceMethodNamePickup, filteredOrders.First().ServiceMethodName);
        }

        [Test]
        public void CreateReport_WithCheckoutMethodFilter_ShowOnlyOrdersWithIncludedCheckoutMethod()
        {
            // Arrange
            CheckoutMethodId checkoutMethodIdChargeToRoom = 1;
            CheckoutMethodName checkoutMethodNameChargeToRoom = "Charge To Room";
            CheckoutMethodId checkoutMethodIdAdyen = 2;
            CheckoutMethodName checkoutMethodNameAdyen = "Adyen";
            OutletId outletId = 1;

            var orderCollection = new OrderCollection
            {
                A.OrderEntity
                    .W(outletId)
                    .W(checkoutMethodIdAdyen)
                    .W(checkoutMethodNameAdyen)
                    .W(new[]
                    {
                        A.OrderitemEntity.W(OrderitemType.Product).Build(),
                        A.OrderitemEntity.W(OrderitemType.DeliveryCharge).Build(),
                        A.OrderitemEntity.W(OrderitemType.ServiceCharge).Build(),
                        A.OrderitemEntity.W(OrderitemType.Tip).Build()
                    })
                    .Build(),
                A.OrderEntity
                    .W(outletId)
                    .W(checkoutMethodIdChargeToRoom)
                    .W(checkoutMethodNameChargeToRoom)
                    .W(new[]
                    {
                        A.OrderitemEntity.W(OrderitemType.Product).Build(),
                        A.OrderitemEntity.W(OrderitemType.DeliveryCharge).Build(),
                        A.OrderitemEntity.W(OrderitemType.ServiceCharge).Build(),
                        A.OrderitemEntity.W(OrderitemType.Tip).Build()
                    })
                    .Build()
            };

            var request = A.TransactionsAppLessRequest
                .W(A.TransactionsAppLessSheetFilter
                    .W(outletId)
                    .W((IncludedCheckoutMethods)new int[] { checkoutMethodIdAdyen }))
                .Build();

            // Act
            var filteredOrders = orderCollection.ToOrderModels(request.TransactionsAppLessSheetFilters.First());

            // Assert
            Assert.AreEqual(1, filteredOrders.Count);
            Assert.AreEqual((string)checkoutMethodNameAdyen, filteredOrders.First().CheckoutMethodName);
        }

        [Test]
        public void CreateReport_WithServiceMethodCheckoutMethodFilter_ShowOnlyOrdersWithBothServiceAndCheckoutMethod()
        {
            // Arrange
            ServiceMethodId serviceMethodIdRoomService = 1;
            ServiceMethodName serviceMethodNameRoomService = "Room service";
            ServiceMethodId serviceMethodIdPickup = 2;
            ServiceMethodName serviceMethodNamePickup = "Pick up";
            CheckoutMethodId checkoutMethodIdChargeToRoom = 1;
            CheckoutMethodName checkoutMethodNameChargeToRoom = "Charge To Room";
            CheckoutMethodId checkoutMethodIdAdyen = 2;
            CheckoutMethodName checkoutMethodNameAdyen = "Adyen";
            OutletId outletId = 1;

            var orderCollection = new OrderCollection
            {
                A.OrderEntity
                    .W(outletId)
                    .W(serviceMethodIdPickup)
                    .W(serviceMethodNamePickup)
                    .W(checkoutMethodIdAdyen)
                    .W(checkoutMethodNameAdyen)
                    .W(new[]
                    {
                        A.OrderitemEntity.W(OrderitemType.Product).Build(),
                        A.OrderitemEntity.W(OrderitemType.DeliveryCharge).Build(),
                        A.OrderitemEntity.W(OrderitemType.ServiceCharge).Build(),
                        A.OrderitemEntity.W(OrderitemType.Tip).Build()
                    })
                    .Build(),
                A.OrderEntity
                    .W(outletId)
                    .W(serviceMethodIdRoomService)
                    .W(serviceMethodNameRoomService)
                    .W(checkoutMethodIdChargeToRoom)
                    .W(checkoutMethodNameChargeToRoom)
                    .W(new[]
                    {
                        A.OrderitemEntity.W(OrderitemType.Product).Build(),
                        A.OrderitemEntity.W(OrderitemType.DeliveryCharge).Build(),
                        A.OrderitemEntity.W(OrderitemType.ServiceCharge).Build(),
                        A.OrderitemEntity.W(OrderitemType.Tip).Build()
                    })
                    .Build()
            };

            var request = A.TransactionsAppLessRequest
                .W(A.TransactionsAppLessSheetFilter
                    .W(outletId)
                    .W((IncludedServiceMethods)new int[] { serviceMethodIdPickup })
                    .W((IncludedCheckoutMethods)new int[] { checkoutMethodIdAdyen }))
                .Build();

            // Act
            var filteredOrders = orderCollection.ToOrderModels(request.TransactionsAppLessSheetFilters.First());

            // Assert
            Assert.AreEqual(1, filteredOrders.Count);
            var order = filteredOrders.First();
            Assert.AreEqual((string)serviceMethodNamePickup, order.ServiceMethodName);
            Assert.AreEqual((string)checkoutMethodNameAdyen, order.CheckoutMethodName);
        }

        [Test]
        public void CreateReport_WithServiceMethodCheckoutMethodFilterAndNoOrdersWithBotl_ShowNone()
        {
            // Arrange
            ServiceMethodId serviceMethodIdRoomService = 1;
            ServiceMethodName serviceMethodNameRoomService = "Room service";
            ServiceMethodId serviceMethodIdPickup = 2;
            ServiceMethodName serviceMethodNamePickup = "Pick up";
            CheckoutMethodId checkoutMethodIdChargeToRoom = 1;
            CheckoutMethodName checkoutMethodNameChargeToRoom = "Charge To Room";
            CheckoutMethodId checkoutMethodIdAdyen = 2;
            CheckoutMethodName checkoutMethodNameAdyen = "Adyen";
            OutletId outletId = 1;

            var orderCollection = new OrderCollection
            {
                A.OrderEntity
                    .W(outletId)
                    .W(serviceMethodIdPickup)
                    .W(serviceMethodNamePickup)
                    .W(checkoutMethodIdChargeToRoom)
                    .W(checkoutMethodNameChargeToRoom)
                    .W(new[]
                    {
                        A.OrderitemEntity.W(OrderitemType.Product).Build(),
                        A.OrderitemEntity.W(OrderitemType.DeliveryCharge).Build(),
                        A.OrderitemEntity.W(OrderitemType.ServiceCharge).Build(),
                        A.OrderitemEntity.W(OrderitemType.Tip).Build()
                    })
                    .Build(),
                A.OrderEntity
                    .W(outletId)
                    .W(serviceMethodIdRoomService)
                    .W(serviceMethodNameRoomService)
                    .W(checkoutMethodIdAdyen)
                    .W(checkoutMethodNameAdyen)
                    .W(new[]
                    {
                        A.OrderitemEntity.W(OrderitemType.Product).Build(),
                        A.OrderitemEntity.W(OrderitemType.DeliveryCharge).Build(),
                        A.OrderitemEntity.W(OrderitemType.ServiceCharge).Build(),
                        A.OrderitemEntity.W(OrderitemType.Tip).Build()
                    })
                    .Build()
            };

            var request = A.TransactionsAppLessRequest
                .W(A.TransactionsAppLessSheetFilter
                    .W(outletId)
                    .W((IncludedServiceMethods)new int[] { serviceMethodIdPickup })
                    .W((IncludedCheckoutMethods)new int[] { checkoutMethodIdAdyen }))
                .Build();

            // Act
            var filteredOrders = orderCollection.ToOrderModels(request.TransactionsAppLessSheetFilters.First());

            // Assert
            Assert.AreEqual(0, filteredOrders.Count);
        }

        [Test]
        public void CreateReport_WithoutProductCategory_HidesTheProductCategory()
        {
            // Arrange
            var orderCollection = new OrderCollection
            {
                A.OrderEntity.Build()
            };

            var orderRepositoryMock = A.OrderRepositoryMock
                .W(orderCollection)
                .Build();

            var companyEntity = A.CompanyEntity.Build();

            var companyRepositoryMock = A.CompanyRepositoryMock
                .W(companyEntity)
                .Build();

            var request = A.TransactionsAppLessRequest
                .W(A.TransactionsAppLessSheetFilter
                    .W((ShowProductCategoryOption)false))
                .Build();

            var sut = new OrderReport(request, orderRepositoryMock, companyRepositoryMock);

            // Act
            var result = sut.CreateReport(out var spreadsheet);
            try
            {
                // Assert
                Assert.True(result);
                OrderReportValidation.AssertOrderItemCategoryInvisible(request, spreadsheet);
                //spreadsheet.SaveAs(@"G:\My Drive\OrderReport.xlsx");
            }
            finally
            {
                spreadsheet.Dispose();
            }
        }

        [Test]
        public void CreateReport_WithProductCategory_ShowsTheProductCategory()
        {
            // Arrange
            var orderCollection = new OrderCollection
            {
                A.OrderEntity.Build()
            };

            var orderRepositoryMock = A.OrderRepositoryMock
                .W(orderCollection)
                .Build();

            var companyEntity = A.CompanyEntity.Build();

            var companyRepositoryMock = A.CompanyRepositoryMock
                .W(companyEntity)
                .Build();

            var request = A.TransactionsAppLessRequest
                .W(A.TransactionsAppLessSheetFilter
                    .W((ShowProductCategoryOption)true))
                .Build();

            var sut = new OrderReport(request, orderRepositoryMock, companyRepositoryMock);

            // Act
            var result = sut.CreateReport(out var spreadsheet);
            try
            {
                // Assert
                Assert.True(result);
                OrderReportValidation.AssertOrderItemCategoryVisible(request, spreadsheet);
                //spreadsheet.SaveAs(@"G:\My Drive\OrderReport.xlsx");
            }
            finally
            {
                spreadsheet.Dispose();
            }
        }

        [Test]
        public void CreateReport_WithoutProductQuantity_HidesTheProductQuantity()
        {
            // Arrange
            var orderCollection = new OrderCollection
            {
                A.OrderEntity.Build()
            };

            var orderRepositoryMock = A.OrderRepositoryMock
                .W(orderCollection)
                .Build();

            var companyEntity = A.CompanyEntity.Build();

            var companyRepositoryMock = A.CompanyRepositoryMock
                .W(companyEntity)
                .Build();

            var request = A.TransactionsAppLessRequest
                .W(A.TransactionsAppLessSheetFilter
                    .W((ShowProductQuantityOption)false))
                .Build();

            var sut = new OrderReport(request, orderRepositoryMock, companyRepositoryMock);

            // Act
            var result = sut.CreateReport(out var spreadsheet);
            try
            {
                var sheetFilter = request.TransactionsAppLessSheetFilters.First();

                // Assert
                Assert.True(result);
                OrderReportValidation.AssertOrderItemQuantityInvisible(sheetFilter, spreadsheet);
                OrderReportValidation.AssertCorrectOrderDetailValues(sheetFilter, companyEntity, orderCollection, spreadsheet);
                //spreadsheet.SaveAs(@"G:\My Drive\OrderReport.xlsx");
            }
            finally
            {
                spreadsheet.Dispose();
            }
        }

        [Test]
        public void CreateReport_WithoutProductPrice_HidesTheProductPrice()
        {
            // Arrange
            var orderCollection = new OrderCollection
            {
                A.OrderEntity.Build()
            };

            var orderRepositoryMock = A.OrderRepositoryMock
                .W(orderCollection)
                .Build();

            var companyEntity = A.CompanyEntity.Build();

            var companyRepositoryMock = A.CompanyRepositoryMock
                .W(companyEntity)
                .Build();

            var request = A.TransactionsAppLessRequest
                .W(A.TransactionsAppLessSheetFilter
                    .W((ShowProductPriceOption)false))
                .Build();

            var sut = new OrderReport(request, orderRepositoryMock, companyRepositoryMock);

            // Act
            var result = sut.CreateReport(out var spreadsheet);
            try
            {
                var sheetFilter = request.TransactionsAppLessSheetFilters.First();

                // Assert
                Assert.True(result);
                OrderReportValidation.AssertOrderItemPriceInvisible(sheetFilter, spreadsheet);
                OrderReportValidation.AssertCorrectOrderDetailValues(sheetFilter, companyEntity, orderCollection, spreadsheet);
                //spreadsheet.SaveAs(@"G:\My Drive\OrderReport.xlsx");
            }
            finally
            {
                spreadsheet.Dispose();
            }
        }

        [Test]
        public void CreateReport_WithoutProductQuantityPrice_HidesTheProductQuantityPrice()
        {
            // Arrange
            var orderCollection = new OrderCollection
            {
                A.OrderEntity.Build()
            };

            var orderRepositoryMock = A.OrderRepositoryMock
                .W(orderCollection)
                .Build();

            var companyEntity = A.CompanyEntity.Build();

            var companyRepositoryMock = A.CompanyRepositoryMock
                .W(companyEntity)
                .Build();

            var request = A.TransactionsAppLessRequest
                .W(A.TransactionsAppLessSheetFilter
                    .W((ShowProductPriceOption)false)
                    .W((ShowProductQuantityOption)false))
                .Build();

            var sut = new OrderReport(request, orderRepositoryMock, companyRepositoryMock);

            // Act
            var result = sut.CreateReport(out var spreadsheet);
            try
            {
                var sheetFilter = request.TransactionsAppLessSheetFilters.First();

                // Assert
                Assert.True(result);
                OrderReportValidation.AssertOrderItemQuantityInvisible(sheetFilter, spreadsheet);
                OrderReportValidation.AssertOrderItemPriceInvisible(sheetFilter, spreadsheet);
                OrderReportValidation.AssertCorrectOrderDetailValues(sheetFilter, companyEntity, orderCollection, spreadsheet);
                //spreadsheet.SaveAs(@"G:\My Drive\OrderReport.xlsx");
            }
            finally
            {
                spreadsheet.Dispose();
            }
        }

        [Test]
        public void CreateReport_WithoutProduct_HidesTheProducts()
        {
            // Arrange
            var orderCollection = new OrderCollection
            {
                A.OrderEntity.Build()
            };

            var orderRepositoryMock = A.OrderRepositoryMock
                .W(orderCollection)
                .Build();

            var companyEntity = A.CompanyEntity.Build();

            var companyRepositoryMock = A.CompanyRepositoryMock
                .W(companyEntity)
                .Build();

            var request = A.TransactionsAppLessRequest
                .W(A.TransactionsAppLessSheetFilter
                    .W((ShowProductsOption)false))
                .Build();

            var sut = new OrderReport(request, orderRepositoryMock, companyRepositoryMock);

            // Act
            var result = sut.CreateReport(out var spreadsheet);
            try
            {
                var sheetFilter = request.TransactionsAppLessSheetFilters.First();

                // Assert
                Assert.True(result);
                OrderReportValidation.AssertOrderItemSubViewInvisible(sheetFilter, spreadsheet);
                OrderReportValidation.AssertCorrectOrderDetailValues(sheetFilter, companyEntity, orderCollection, spreadsheet);
                //spreadsheet.SaveAs(@"G:\My Drive\OrderReport.xlsx");
            }
            finally
            {
                spreadsheet.Dispose();
            }
        }

        [Test]
        public void CreateReport_WithDeliverypointName_ShowsTheDeliverypointName()
        {
            DeliverypointName deliverypointName = (DeliverypointName)"Room 1A";

            // Arrange
            var orderCollection = new OrderCollection
            {
                A.OrderEntity
                    .W(deliverypointName)
                    .Build()
            };

            var orderRepositoryMock = A.OrderRepositoryMock
                .W(orderCollection)
                .Build();

            var companyEntity = A.CompanyEntity.Build();

            var companyRepositoryMock = A.CompanyRepositoryMock
                .W(companyEntity)
                .Build();

            var request = A.TransactionsAppLessRequest
                .W(A.TransactionsAppLessSheetFilter
                    .W((ShowDeliveryPoint)true))
                .Build();

            var sut = new OrderReport(request, orderRepositoryMock, companyRepositoryMock);

            // Act
            var result = sut.CreateReport(out var spreadsheet);
            try
            {
                var sheetFilter = request.TransactionsAppLessSheetFilters.First();

                // Assert
                Assert.True(result);
                OrderReportValidation.AssertCorrectOrderDetailHeaders(sheetFilter, spreadsheet);
                OrderReportValidation.AssertCorrectOrderDetailValues(sheetFilter, companyEntity, orderCollection, spreadsheet);
                //spreadsheet.SaveAs(@"G:\My Drive\OrderReport.xlsx");
            }
            finally
            {
                spreadsheet.Dispose();
            }
        }

        [Test]
        public void CreateReport_WithPaymentMethod_ShowsThePaymentMethod()
        {
            PaymentTransactionEntity paymentTransaction = A.PaymentTransactionEntity
                .W("iDeal")
                .Build();

            // Arrange
            var orderCollection = new OrderCollection
            {
                A.OrderEntity
                    .W(paymentTransaction)
                    .Build()
            };

            var orderRepositoryMock = A.OrderRepositoryMock
                .W(orderCollection)
                .Build();

            var companyEntity = A.CompanyEntity.Build();

            var companyRepositoryMock = A.CompanyRepositoryMock
                .W(companyEntity)
                .Build();

            var request = A.TransactionsAppLessRequest
                .W(A.TransactionsAppLessSheetFilter
                    .W((ShowPaymentMethod)true))
                .Build();

            var sut = new OrderReport(request, orderRepositoryMock, companyRepositoryMock);

            // Act
            var result = sut.CreateReport(out var spreadsheet);
            try
            {
                var sheetFilter = request.TransactionsAppLessSheetFilters.First();

                // Assert
                Assert.True(result);
                OrderReportValidation.AssertCorrectOrderDetailHeaders(sheetFilter, spreadsheet);
                OrderReportValidation.AssertCorrectOrderDetailValues(sheetFilter, companyEntity, orderCollection, spreadsheet);
                //spreadsheet.SaveAs(@"G:\My Drive\OrderReport.xlsx");
            }
            finally
            {
                spreadsheet.Dispose();
            }
        }

        [Test]
        public void CreateReport_WithCardSummary_ShowsTheCardSummary()
        {
            PaymentTransactionEntity paymentTransaction = A.PaymentTransactionEntity
                .W("iDeal")
                .Build();

            // Arrange
            var orderCollection = new OrderCollection
            {
                A.OrderEntity
                    .W(paymentTransaction)
                    .Build()
            };

            var orderRepositoryMock = A.OrderRepositoryMock
                .W(orderCollection)
                .Build();

            var companyEntity = A.CompanyEntity.Build();

            var companyRepositoryMock = A.CompanyRepositoryMock
                .W(companyEntity)
                .Build();

            var request = A.TransactionsAppLessRequest
                .W(A.TransactionsAppLessSheetFilter
                    .W((ShowCardSummary)true))
                .Build();

            var sut = new OrderReport(request, orderRepositoryMock, companyRepositoryMock);

            // Act
            var result = sut.CreateReport(out var spreadsheet);
            try
            {
                var sheetFilter = request.TransactionsAppLessSheetFilters.First();

                // Assert
                Assert.True(result);
                OrderReportValidation.AssertCorrectOrderDetailHeaders(sheetFilter, spreadsheet);
                OrderReportValidation.AssertCorrectOrderDetailValues(sheetFilter, companyEntity, orderCollection, spreadsheet);
            }
            finally
            {
                spreadsheet.Dispose();
            }
        }

        [Test]
        public void CreateReport_WithoutCardSummary_DoesNotShowTheCardSummary()
        {
            PaymentTransactionEntity paymentTransaction = A.PaymentTransactionEntity
                .W("iDeal")
                .Build();

            // Arrange
            var orderCollection = new OrderCollection
            {
                A.OrderEntity
                    .W(paymentTransaction)
                    .Build()
            };

            var orderRepositoryMock = A.OrderRepositoryMock
                .W(orderCollection)
                .Build();

            var companyEntity = A.CompanyEntity.Build();

            var companyRepositoryMock = A.CompanyRepositoryMock
                .W(companyEntity)
                .Build();

            var request = A.TransactionsAppLessRequest
                .W(A.TransactionsAppLessSheetFilter
                    .W((ShowCardSummary)false))
                .Build();

            var sut = new OrderReport(request, orderRepositoryMock, companyRepositoryMock);

            // Act
            var result = sut.CreateReport(out var spreadsheet);
            try
            {
                var sheetFilter = request.TransactionsAppLessSheetFilters.First();

                // Assert
                Assert.True(result);
                OrderReportValidation.AssertCorrectOrderDetailHeaders(sheetFilter, spreadsheet);
                OrderReportValidation.AssertCorrectOrderDetailValues(sheetFilter, companyEntity, orderCollection, spreadsheet);
            }
            finally
            {
                spreadsheet.Dispose();
            }
        }

        [Test]
        public void CreateReport_WithCoversCount_ShowsTheCoversCount()
        {
            PaymentTransactionEntity paymentTransaction = A.PaymentTransactionEntity
                .W("iDeal")
                .Build();

            // Arrange
            var orderCollection = new OrderCollection
            {
                A.OrderEntity
                    .W(paymentTransaction)
                    .Build()
            };

            var orderRepositoryMock = A.OrderRepositoryMock
                .W(orderCollection)
                .Build();

            var companyEntity = A.CompanyEntity.Build();

            var companyRepositoryMock = A.CompanyRepositoryMock
                .W(companyEntity)
                .Build();

            var request = A.TransactionsAppLessRequest
                .W(A.TransactionsAppLessSheetFilter
                    .W((ShowCoversCount)true))
                .Build();

            var sut = new OrderReport(request, orderRepositoryMock, companyRepositoryMock);

            // Act
            var result = sut.CreateReport(out var spreadsheet);
            try
            {
                var sheetFilter = request.TransactionsAppLessSheetFilters.First();

                // Assert
                Assert.True(result);
                OrderReportValidation.AssertCorrectOrderDetailHeaders(sheetFilter, spreadsheet);
                OrderReportValidation.AssertCorrectOrderDetailValues(sheetFilter, companyEntity, orderCollection, spreadsheet);
            }
            finally
            {
                spreadsheet.Dispose();
            }
        }

        [Test]
        public void CreateReport_WithoutCoversCount_DoesNotShowTheCoversCount()
        {
            PaymentTransactionEntity paymentTransaction = A.PaymentTransactionEntity
                .W("iDeal")
                .Build();

            // Arrange
            var orderCollection = new OrderCollection
            {
                A.OrderEntity
                    .W(paymentTransaction)
                    .Build()
            };

            var orderRepositoryMock = A.OrderRepositoryMock
                .W(orderCollection)
                .Build();

            var companyEntity = A.CompanyEntity.Build();

            var companyRepositoryMock = A.CompanyRepositoryMock
                .W(companyEntity)
                .Build();

            var request = A.TransactionsAppLessRequest
                .W(A.TransactionsAppLessSheetFilter
                    .W((ShowCoversCount)false))
                .Build();

            var sut = new OrderReport(request, orderRepositoryMock, companyRepositoryMock);

            // Act
            var result = sut.CreateReport(out var spreadsheet);
            try
            {
                var sheetFilter = request.TransactionsAppLessSheetFilters.First();

                // Assert
                Assert.True(result);
                OrderReportValidation.AssertCorrectOrderDetailHeaders(sheetFilter, spreadsheet);
                OrderReportValidation.AssertCorrectOrderDetailValues(sheetFilter, companyEntity, orderCollection, spreadsheet);
            }
            finally
            {
                spreadsheet.Dispose();
            }
        }

        [Test]
        public void CreateReport_WithTaxTariffFilterAndProductWithRequestedTariff_ShowsOnlyThatProduct()
        {
            // Arrange
            int? taxTariffId = 156;

            var orderItemWithRequestedTariff = A.OrderitemEntity
                .W(taxTariffId)
                .Build();

            var productName = orderItemWithRequestedTariff.ProductName;

            var orderCollection = new OrderCollection
            {
                A.OrderEntity
                    .W(new[]
                    {
                        orderItemWithRequestedTariff,
                        A.OrderitemEntity
                            .W((int?) 203)
                            .Build()
                    })
                    .Build(),
            };

            var orderRepositoryMock = A.OrderRepositoryMock
                .W(orderCollection)
                .Build();

            var companyEntity = A.CompanyEntity.Build();

            var companyRepositoryMock = A.CompanyRepositoryMock
                .W(companyEntity)
                .Build();

            var request = A.TransactionsAppLessRequest
                .W(A.TransactionsAppLessSheetFilter
                    .W((IncludedTaxTariffIds)new[] { taxTariffId.Value })
                    .W((ShowAlterationsOption)true))
                .Build();

            var sut = new OrderReport(request, orderRepositoryMock, companyRepositoryMock);

            // Act
            var result = sut.CreateReport(out var spreadsheet);
            try
            {
                var sheetFilter = request.TransactionsAppLessSheetFilters.First();

                // Assert
                Assert.True(result);
                OrderReportValidation.AssertOrderItemCount(1, sheetFilter, spreadsheet);
                OrderReportValidation.AssertContainsOrderItemWithName(productName, sheetFilter, spreadsheet);
                //spreadsheet.SaveAs(@"G:\My Drive\OrderReport.xlsx");
            }
            finally
            {
                spreadsheet.Dispose();
            }
        }

        [Test]
        public void CreateReport_WithTaxTariffFilterAndProductWithRequestedTariffAndAlterationWithOtherTariff_ShowsOnlyTheProductAndNotTheAlteration()
        {
            // Arrange
            int? taxTariffId = 156;

            var orderItemWithRequestedTariff = A.OrderitemEntity
                .W(taxTariffId)
                .W(new[] { A.OrderitemAlterationitemEntity.W(123).Build() })
                .Build();

            var productName = orderItemWithRequestedTariff.ProductName;

            var orderCollection = new OrderCollection
            {
                A.OrderEntity
                    .W(new[]
                    {
                        orderItemWithRequestedTariff,
                    })
                    .Build(),
            };

            var orderRepositoryMock = A.OrderRepositoryMock
                .W(orderCollection)
                .Build();

            var companyEntity = A.CompanyEntity.Build();

            var companyRepositoryMock = A.CompanyRepositoryMock
                .W(companyEntity)
                .Build();

            var request = A.TransactionsAppLessRequest
                .W(A.TransactionsAppLessSheetFilter
                    .W((IncludedTaxTariffIds)new[] { taxTariffId.Value })
                    .W((ShowAlterationsOption)true))
                .Build();

            var sut = new OrderReport(request, orderRepositoryMock, companyRepositoryMock);

            // Act
            var result = sut.CreateReport(out var spreadsheet);
            try
            {
                var sheetFilter = request.TransactionsAppLessSheetFilters.First();

                // Assert
                Assert.True(result);
                OrderReportValidation.AssertOrderItemCount(1, sheetFilter, spreadsheet);
                OrderReportValidation.AssertContainsOrderItemWithName(productName, sheetFilter, spreadsheet);
                OrderReportValidation.AssertDoesNotContainAterationItemWithName(productName, sheetFilter, spreadsheet);
                //spreadsheet.SaveAs(@"G:\My Drive\OrderReport.xlsx");
            }
            finally
            {
                spreadsheet.Dispose();
            }
        }

        [Test]
        public void CreateReport_WithTaxTariffFilterAndProductAndAlterationWithRequestedTariff_ShowsBothProductAndAlteration()
        {
            // Arrange
            int? taxTariffId = 156;

            var alterationOption = A.OrderitemAlterationitemEntity
                .W(taxTariffId)
                .Build();

            var orderItemWithRequestedTariff = A.OrderitemEntity
                .W(taxTariffId)
                .W(new[] { alterationOption })
                .Build();

            var productName = orderItemWithRequestedTariff.ProductName;
            var alterationOptionName = $@"{alterationOption.AlterationName} ({alterationOption.AlterationoptionName})";

            var orderCollection = new OrderCollection
            {
                A.OrderEntity
                    .W(new[]
                    {
                        orderItemWithRequestedTariff,
                    })
                    .Build(),
            };

            var orderRepositoryMock = A.OrderRepositoryMock
                .W(orderCollection)
                .Build();

            var companyEntity = A.CompanyEntity.Build();

            var companyRepositoryMock = A.CompanyRepositoryMock
                .W(companyEntity)
                .Build();

            var request = A.TransactionsAppLessRequest
                .W(A.TransactionsAppLessSheetFilter
                    .W((IncludedTaxTariffIds)new[] { taxTariffId.Value })
                    .W((ShowAlterationsOption)true))
                .Build();

            var sut = new OrderReport(request, orderRepositoryMock, companyRepositoryMock);

            // Act
            var result = sut.CreateReport(out var spreadsheet);
            try
            {
                var sheetFilter = request.TransactionsAppLessSheetFilters.First();

                // Assert
                Assert.True(result);
                OrderReportValidation.AssertOrderItemCount(2, sheetFilter, spreadsheet);
                OrderReportValidation.AssertContainsOrderItemWithName(productName, sheetFilter, spreadsheet);
                OrderReportValidation.AssertContainsAlterationItemWithName(alterationOptionName, sheetFilter, spreadsheet);
                //spreadsheet.SaveAs(@"G:\My Drive\OrderReport.xlsx");
            }
            finally
            {
                spreadsheet.Dispose();
            }
        }

        [Test]
        public void CreateReport_WithTaxTariffFilterAndNoProductOrAlterationWithRequestedTariff_DoesNotShowAnyItem()
        {
            // Arrange
            int? taxTariffId = 156;

            var orderCollection = new OrderCollection
            {
                A.OrderEntity
                    .W(new[]
                    {
                        A.OrderitemEntity
                            .W((int?) 123)
                            .W(new[]
                            {
                                A.OrderitemAlterationitemEntity
                                    .W(123)
                                    .Build()
                            }).Build(),
                    }).Build(),
            };

            var orderRepositoryMock = A.OrderRepositoryMock
                .W(orderCollection)
                .Build();

            var companyEntity = A.CompanyEntity.Build();

            var companyRepositoryMock = A.CompanyRepositoryMock
                .W(companyEntity)
                .Build();

            var request = A.TransactionsAppLessRequest
                .W(A.TransactionsAppLessSheetFilter
                    .W((IncludedTaxTariffIds)new[] { taxTariffId.Value })
                    .W((ShowAlterationsOption)true))
                .Build();

            var sut = new OrderReport(request, orderRepositoryMock, companyRepositoryMock);

            // Act
            var result = sut.CreateReport(out var spreadsheet);
            try
            {
                var sheetFilter = request.TransactionsAppLessSheetFilters.First();

                // Assert
                Assert.True(result);
                OrderReportValidation.AssertOrderItemCount(0, sheetFilter, spreadsheet);
                //spreadsheet.SaveAs(@"G:\My Drive\OrderReport.xlsx");
            }
            finally
            {
                spreadsheet.Dispose();
            }
        }

        [Test]
        public void CreateReport_WithTaxTariffFilterAndNoProductAndInheritedAlteration_DoesNotShowAnyItem()
        {
            // Arrange
            int? taxTariffId = 156;

            var orderCollection = new OrderCollection
            {
                A.OrderEntity
                    .W(new[]
                    {
                        A.OrderitemEntity
                            .W((int?) 123)
                            .W(new[]
                            {
                                A.OrderitemAlterationitemEntity
                                    .W((int?)null)
                                    .Build()
                            }).Build(),
                    }).Build(),
            };

            var orderRepositoryMock = A.OrderRepositoryMock
                .W(orderCollection)
                .Build();

            var companyEntity = A.CompanyEntity.Build();

            var companyRepositoryMock = A.CompanyRepositoryMock
                .W(companyEntity)
                .Build();

            var request = A.TransactionsAppLessRequest
                .W(A.TransactionsAppLessSheetFilter
                    .W((IncludedTaxTariffIds)new[] { taxTariffId.Value })
                    .W((ShowAlterationsOption)true))
                .Build();

            var sut = new OrderReport(request, orderRepositoryMock, companyRepositoryMock);

            // Act
            var result = sut.CreateReport(out var spreadsheet);
            try
            {
                var sheetFilter = request.TransactionsAppLessSheetFilters.First();

                // Assert
                Assert.True(result);
                OrderReportValidation.AssertOrderItemCount(0, sheetFilter, spreadsheet);
                //spreadsheet.SaveAs(@"G:\My Drive\OrderReport.xlsx");
            }
            finally
            {
                spreadsheet.Dispose();
            }
        }

        [Test]
        public void CreateReport_WithTaxTariffFilterAndProductAndInheritedAlteration_ShowsBothProductAndAlteration()
        {
            // Arrange
            int? taxTariffId = 156;

            var alterationOption = A.OrderitemAlterationitemEntity
                .W((int?)null)
                .Build();

            var orderItemWithRequestedTariff = A.OrderitemEntity
                .W(taxTariffId)
                .W(new[] { alterationOption })
                .Build();

            var productName = orderItemWithRequestedTariff.ProductName;
            var alterationOptionName = $@"{alterationOption.AlterationName} ({alterationOption.AlterationoptionName})";

            var orderCollection = new OrderCollection
            {
                A.OrderEntity
                    .W(new[]
                    {
                        orderItemWithRequestedTariff,
                    })
                    .Build(),
            };

            var orderRepositoryMock = A.OrderRepositoryMock
                .W(orderCollection)
                .Build();

            var companyEntity = A.CompanyEntity.Build();

            var companyRepositoryMock = A.CompanyRepositoryMock
                .W(companyEntity)
                .Build();

            var request = A.TransactionsAppLessRequest
                .W(A.TransactionsAppLessSheetFilter
                    .W((IncludedTaxTariffIds)new[] { taxTariffId.Value })
                    .W((ShowAlterationsOption)true))
                .Build();

            var sut = new OrderReport(request, orderRepositoryMock, companyRepositoryMock);

            // Act
            var result = sut.CreateReport(out var spreadsheet);
            try
            {
                var sheetFilter = request.TransactionsAppLessSheetFilters.First();

                // Assert
                Assert.True(result);
                OrderReportValidation.AssertOrderItemCount(2, sheetFilter, spreadsheet);
                OrderReportValidation.AssertContainsOrderItemWithName(productName, sheetFilter, spreadsheet);
                OrderReportValidation.AssertContainsAlterationItemWithName(alterationOptionName, sheetFilter, spreadsheet);
                //spreadsheet.SaveAs(@"G:\My Drive\OrderReport.xlsx");
            }
            finally
            {
                spreadsheet.Dispose();
            }
        }

        [Test]
        public void CreateReport_WithTaxTariffFilterAndProductWithOtherTaxTariffAndAlterationWithRequestedTariff_ShowsBothProductAndAlteration()
        {
            // Arrange
            int? taxTariffId = 156;

            var alterationOption = A.OrderitemAlterationitemEntity
                .W(taxTariffId)
                .Build();

            var orderItemWithRequestedTariff = A.OrderitemEntity
                .W((int?)123)
                .W(new[] { alterationOption })
                .Build();

            var productName = orderItemWithRequestedTariff.ProductName;
            var alterationOptionName = $@"{alterationOption.AlterationName} ({alterationOption.AlterationoptionName})";

            var orderCollection = new OrderCollection
            {
                A.OrderEntity
                    .W(new[]
                    {
                        orderItemWithRequestedTariff,
                    })
                    .Build(),
            };

            var orderRepositoryMock = A.OrderRepositoryMock
                .W(orderCollection)
                .Build();

            var companyEntity = A.CompanyEntity.Build();

            var companyRepositoryMock = A.CompanyRepositoryMock
                .W(companyEntity)
                .Build();

            var request = A.TransactionsAppLessRequest
                .W(A.TransactionsAppLessSheetFilter
                    .W((IncludedTaxTariffIds)new[] { taxTariffId.Value })
                    .W((ShowAlterationsOption)true))
                .Build();

            var sut = new OrderReport(request, orderRepositoryMock, companyRepositoryMock);

            // Act
            var result = sut.CreateReport(out var spreadsheet);
            try
            {
                var sheetFilter = request.TransactionsAppLessSheetFilters.First();

                // Assert
                Assert.True(result);
                OrderReportValidation.AssertOrderItemCount(2, sheetFilter, spreadsheet);
                OrderReportValidation.AssertContainsOrderItemWithName(productName, sheetFilter, spreadsheet);
                OrderReportValidation.AssertContainsAlterationItemWithName(alterationOptionName, sheetFilter, spreadsheet);
                //spreadsheet.SaveAs(@"G:\My Drive\OrderReport.xlsx");
            }
            finally
            {
                spreadsheet.Dispose();
            }
        }

        [Test]
        public void CreateReport_WithMultipleFilters_AddsMultipleWorksheetsToExcel()
        {
            // Arrange
            var orderCollection = new OrderCollection
            {
                A.OrderEntity.Build(),
                A.OrderEntity.Build(),
                A.OrderEntity.Build(),
                A.OrderEntity.Build(),
                A.OrderEntity.Build()
            };

            var orderRepositoryMock = A.OrderRepositoryMock
                .W(orderCollection)
                .Build();

            var companyEntity = A.CompanyEntity.Build();

            var companyRepositoryMock = A.CompanyRepositoryMock
                .W(companyEntity)
                .Build();

            var request = A.TransactionsAppLessRequest
                .W(A.TransactionsAppLessSheetFilter.W("Sheet 1", true),
                    A.TransactionsAppLessSheetFilter.W("Sheet 2", true),
                    A.TransactionsAppLessSheetFilter.W("Sheet 3", true))
                .Build();

            var sut = new OrderReport(request, orderRepositoryMock, companyRepositoryMock);

            // Act
            var result = sut.CreateReport(out var spreadsheet);
            try
            {
                // Assert
                Assert.True(result);

                foreach (var sheetFilter in request.SheetFilters)
                {
                    OrderReportValidation.AssertDocumentContainsWorksheet(spreadsheet, sheetFilter.Name);
                }
            }
            finally
            {
                spreadsheet.Dispose();
            }
        }

        [Test]
        public void CreateReport_WithTaxTariffFilterAndProductWithOtherTaxTariffAndAlterationWithRequestedTariff_LeavesTheProductPriceBlank()
        {
            // Arrange
            int? taxTariffId = 156;

            var alterationOption = A.OrderitemAlterationitemEntity
                .W(taxTariffId)
                .Build();

            var orderItemWithRequestedTariff = A.OrderitemEntity
                .W((int?)123)
                .W(new[] { alterationOption })
                .Build();

            var orderCollection = new OrderCollection
            {
                A.OrderEntity
                    .W(new[]
                    {
                        orderItemWithRequestedTariff,
                    })
                    .Build(),
            };

            var orderRepositoryMock = A.OrderRepositoryMock
                .W(orderCollection)
                .Build();

            var companyEntity = A.CompanyEntity.Build();

            var companyRepositoryMock = A.CompanyRepositoryMock
                .W(companyEntity)
                .Build();

            var request = A.TransactionsAppLessRequest
                .W(A.TransactionsAppLessSheetFilter
                    .W((IncludedTaxTariffIds)new[] { taxTariffId.Value })
                    .W((ShowAlterationsOption)true))
                .Build();

            var sut = new OrderReport(request, orderRepositoryMock, companyRepositoryMock);

            // Act
            var result = sut.CreateReport(out var spreadsheet);
            try
            {
                // Assert
                Assert.True(result);

                foreach (var sheetFilter in request.SheetFilters)
                {
                    OrderReportValidation.AssertDocumentContainsWorksheet(spreadsheet, sheetFilter.Name);
                }
            }
            finally
            {
                spreadsheet.Dispose();
            }
        }

        [Test]
        public void CreateReport_WithSplitOrder_ShowsTheOrderItemsOfAllOrders()
        {
            // Arrange
            var masterOrder = A.OrderEntity
                .W(1)
                .W(new[]
                {
                    A.OrderitemEntity
                        .W("Byker Brown Ale - Cask")
                        .W(0M)
                        .W(1)
                        .W(new[]
                        {
                            A.OrderitemAlterationitemEntity
                                .W("Byker Brown Keg", "Half")
                                .W(2M, 2.55M)
                                .Build()
                        })
                        .Build(),
                    A.OrderitemEntity
                        .W("Carbonated Drinks")
                        .W(1.6M)
                        .W(1)
                        .W(new[]
                        {
                            A.OrderitemAlterationitemEntity
                                .W("Carbonated Drinks", "Coke Zero")
                                .W(0M, 0M)
                                .Build()
                        })
                        .Build(),
                    A.OrderitemEntity
                        .W("Service charge")
                        .W(OrderitemType.ServiceCharge)
                        .W(.5M)
                        .W(1)
                        .W(new OrderitemAlterationitemEntity[0])
                        .Build()
                })
                .W(29.15M)
                .Build();

            var subOrder = A.OrderEntity
                .W(2)
                .W((int?)masterOrder.OrderId)
                .W(new[]
                {
                    A.OrderitemEntity
                        .W("Panko Chicken")
                        .W(11M)
                        .W(1)
                        .W(new OrderitemAlterationitemEntity[0])
                        .Build(),
                    A.OrderitemEntity
                        .W("The Brinkburger")
                        .W(12M)
                        .W(1)
                        .W(new[]
                        {
                            A.OrderitemAlterationitemEntity
                                .W("Upgrade Fries", "Parmesan Truffle Fries")
                                .W(1M, 1.5M)
                                .Build()
                        })
                        .Build()
                })
                .W(24.5M)
                .Build();


            var orderCollection = new OrderCollection
            {
                masterOrder,
                subOrder
            };

            var orderRepositoryMock = A.OrderRepositoryMock
                .W(orderCollection)
                .Build();

            var companyEntity = A.CompanyEntity.Build();

            var companyRepositoryMock = A.CompanyRepositoryMock
                .W(companyEntity)
                .Build();

            var request = A.TransactionsAppLessRequest
                .W(A.TransactionsAppLessSheetFilter
                    .W((ShowAlterationsOption)true)
                    .W((ShowProductsOption)true)
                    .W((ShowSystemProductsOption)true))
                .Build();

            var sut = new OrderReport(request, orderRepositoryMock, companyRepositoryMock);

            // Act
            var result = sut.CreateReport(out var spreadsheet);
            try
            {
                // Assert
                Assert.True(result);

                foreach (var sheetFilter in request.SheetFilters)
                {
                    OrderReportValidation.AssertDocumentContainsWorksheet(spreadsheet, sheetFilter.Name);
                }

                //spreadsheet.SaveAs(@"G:\My Drive\OrderReport.xlsx");
            }
            finally
            {
                spreadsheet.Dispose();
            }
        }

        [Test]
        public void CreateReport_WithCategoryFilterAndSystemProductFilter_ShowsBothOrderitemsOfTheOrder()
        {
            // Arrange

            var orderCollection = new OrderCollection
            {
                A.OrderEntity
                    .W(new[]
                    {
                        A.OrderitemEntity
                            .W((CategoryId) 53038)
                            .W((CategoryName) "Food")
                            .W("Burger")
                            .W(4.45M)
                            .W(1)
                            .W(new[]
                            {
                                A.OrderitemAlterationitemEntity
                                    .W("Options_Min_2_Max_3", "Large")
                                    .W(0M, 0M)
                                    .Build()
                            })
                            .Build(),
                        A.OrderitemEntity
                            .W("Tip Price")
                            .W(OrderitemType.Tip)
                            .W(.45M)
                            .W(1)
                            .Build()
                    })
                    .W(5.03M)
                    .Build(),
            };

            var request = A.TransactionsAppLessRequest
                .W(A.TransactionsAppLessSheetFilter
                    .W((ShowAlterationsOption)true)
                    .W((ShowProductsOption)true)
                    .W((ShowSystemProductsOption)true)
                    .W((IncludedSystemProductTypes)new[] { (int)OrderitemType.Tip })
                    .W((ShowProductCategoryOption)true)
                    .W(IncludeExcludeFilter.Include)
                    .W((CategoryFilterIds)new[] { 53038, 53039 })
                )
                .Build();

            // Act
            var filteredOrders = orderCollection.ToOrderModels(request.TransactionsAppLessSheetFilters.First());

            // Assert
            Assert.AreEqual(1, filteredOrders.Count);
            Assert.AreEqual(2, filteredOrders.First().OrderItems.Length);
        }

        [Test]
        public void CreateReport_WithCategoryFilterOnOneCategoryTwoProductsWithDifferentCategories_ShowsOnlyTheProductsWithTheFilteredCategory()
        {
            // Arrange
            var expectedCategoryName = "Drinks";
            var orderCollection = new OrderCollection
            {
                A.OrderEntity
                    .W(new[]
                    {
                        A.OrderitemEntity
                            .W((CategoryId) 53038)
                            .W((CategoryName) "Food")
                            .W("Burger")
                            .W(4.45M)
                            .W(1)
                            .W(new[]
                            {
                                A.OrderitemAlterationitemEntity
                                    .W("Options_Min_2_Max_3", "Large")
                                    .W(0M, 0M)
                                    .Build()
                            })
                            .Build(),
                        A.OrderitemEntity
                            .W((CategoryId) 53039)
                            .W((CategoryName) expectedCategoryName)
                            .W("Beer")
                            .W(3.66M)
                            .W(1)
                            .Build()
                    })
                    .W(5.03M)
                    .Build(),
            };

            var request = A.TransactionsAppLessRequest
                .W(A.TransactionsAppLessSheetFilter
                    .W((ShowAlterationsOption)true)
                    .W((ShowProductsOption)true)
                    .W((ShowProductCategoryOption)true)
                    .W(IncludeExcludeFilter.Include)
                    .W((CategoryFilterIds)new[] { 53039 })
                )
                .Build();

            // Act
            var filteredOrders = orderCollection.ToOrderModels(request.TransactionsAppLessSheetFilters.First());

            // Assert
            Assert.AreEqual(1, filteredOrders.Count);
            var order = filteredOrders.First();
            Assert.AreEqual(1, order.OrderItems.Length);
            Assert.AreEqual($"CN-{expectedCategoryName}", order.OrderItems.First().CategoryName);
        }

        [TestCase(false, true, true, true, true, true, true, false, true, true, false, false)]
        [TestCase(true, false, true, true, true, true, true, false, true, true, false, false)]
        [TestCase(true, true, false, true, true, true, true, false, true, true, false, false)]
        [TestCase(true, true, true, false, true, true, true, false, true, true, false, false)]
        [TestCase(true, true, true, true, false, true, true, false, true, true, false, false)]
        [TestCase(true, true, true, true, true, false, true, false, true, true, false, false)]
        [TestCase(true, true, true, true, true, true, true, false, false, true, false, false)]
        [TestCase(true, true, true, true, true, true, true, false, true, false, false, false)]
        [TestCase(false, false, false, false, false, false, true, false, false, false, false, false)]
        [TestCase(true, true, true, true, true, true, true, false, true, true, false, false)]
        [TestCase(true, true, false, false, true, true, true, false, true, true, false, false)]
        [TestCase(false, false, true, true, true, true, true, false, true, true, false, false)]
        [TestCase(false, true, true, true, true, false, true, false, true, true, false, false)]
        [TestCase(true, true, true, true, true, true, false, true, true, true, false, false)]
        [TestCase(true, true, true, true, true, true, true, true, true, true, false, false)]
        [TestCase(true, false, true, true, false, false, false, false, false, true, false, false)]
        [TestCase(true, false, true, true, false, false, false, false, false, true, true, false)]
        [TestCase(true, false, true, true, false, false, false, false, false, true, false, true)]
        [TestCase(true, false, true, true, false, false, false, false, false, true, true, true)]
        public void CreateReport_WithSpecificSetOfOptions_AddsTheCorrectInformationToExcel(
            bool showProducts,
            bool showSystemProducts,
            bool showProductQuantity,
            bool showProductPrice,
            bool showProductCategory,
            bool showAlterations,
            bool showServiceMethod,
            bool showCheckoutMethod,
            bool showCustomerInfo,
            bool showOrderNotesOption,
            bool showPaymentMethod,
            bool showDeliverypoint)
        {
            // Arrange
            var orderCollection = new OrderCollection
            {
                A.OrderEntity
                    .W(1)
                    .W(new[]
                    {
                        A.OrderitemEntity.W(OrderitemType.Product).Build(),
                        A.OrderitemEntity.W(OrderitemType.Product).Build(),
                        A.OrderitemEntity.W(OrderitemType.Product).Build(),
                        A.OrderitemEntity.W(OrderitemType.DeliveryCharge)
                            .W(new OrderitemAlterationitemEntity[0])
                            .Build(),
                        A.OrderitemEntity.W(OrderitemType.ServiceCharge)
                            .W(new OrderitemAlterationitemEntity[0])
                            .Build(),
                        A.OrderitemEntity.W(OrderitemType.Tip)
                            .W(new OrderitemAlterationitemEntity[0])
                            .Build()
                    })
                    .Build(),

                A.OrderEntity
                    .W(2)
                    .W(new[]
                    {
                        A.OrderitemEntity.W(OrderitemType.Product).Build(),
                        A.OrderitemEntity.W(OrderitemType.Product).Build(),
                        A.OrderitemEntity.W(OrderitemType.DeliveryCharge)
                            .W(new OrderitemAlterationitemEntity[0])
                            .Build(),
                        A.OrderitemEntity.W(OrderitemType.Tip)
                            .W(new OrderitemAlterationitemEntity[0])
                            .Build()
                    })
                    .Build(),
                A.OrderEntity
                    .W(3)
                    .W(new[]
                    {
                        A.OrderitemEntity.W(OrderitemType.Product).Build(),
                        A.OrderitemEntity.W(OrderitemType.Product).Build(),
                        A.OrderitemEntity.W(OrderitemType.Product).Build(),
                        A.OrderitemEntity.W(OrderitemType.ServiceCharge)
                            .W(new OrderitemAlterationitemEntity[0])
                            .Build(),
                        A.OrderitemEntity.W(OrderitemType.Tip)
                            .W(new OrderitemAlterationitemEntity[0])
                            .Build()
                    })
                    .Build(),
                A.OrderEntity
                    .W(4)
                    .W(new[]
                    {
                        A.OrderitemEntity.W(OrderitemType.Product).Build(),
                        A.OrderitemEntity.W(OrderitemType.Tip)
                            .W(new OrderitemAlterationitemEntity[0])
                            .Build()
                    })
                    .Build(),
                A.OrderEntity
                    .W(5)
                    .W(new[]
                    {
                        A.OrderitemEntity.W(OrderitemType.Product).Build(),
                        A.OrderitemEntity.W(OrderitemType.Product).Build(),
                        A.OrderitemEntity.W(OrderitemType.Product).Build(),
                        A.OrderitemEntity.W(OrderitemType.DeliveryCharge)
                            .W(new OrderitemAlterationitemEntity[0])
                            .Build(),
                        A.OrderitemEntity.W(OrderitemType.Tip)
                            .W(new OrderitemAlterationitemEntity[0])
                            .Build()
                    })
                    .Build(),
                A.OrderEntity
                    .W(6)
                    .W(new[]
                    {
                        A.OrderitemEntity.W(OrderitemType.Product).Build(),
                        A.OrderitemEntity.W(OrderitemType.Product).Build(),
                        A.OrderitemEntity.W(OrderitemType.Product).Build(),
                        A.OrderitemEntity.W(OrderitemType.DeliveryCharge)
                            .W(new OrderitemAlterationitemEntity[0])
                            .Build(),
                        A.OrderitemEntity.W(OrderitemType.ServiceCharge)
                            .W(new OrderitemAlterationitemEntity[0])
                            .Build()
                    })
                    .Build(),
                A.OrderEntity
                    .W(7)
                    .W(new[]
                    {
                        A.OrderitemEntity.W(OrderitemType.Product).Build(),
                        A.OrderitemEntity.W(OrderitemType.ServiceCharge)
                            .W(new OrderitemAlterationitemEntity[0])
                            .Build(),
                        A.OrderitemEntity.W(OrderitemType.Tip)
                            .W(new OrderitemAlterationitemEntity[0])
                            .Build()
                    })
                    .Build(),
                A.OrderEntity
                    .W(8)
                    .W(new[]
                    {
                        A.OrderitemEntity.W(OrderitemType.Product).Build(),
                        A.OrderitemEntity.W(OrderitemType.Product).Build()
                    })
                    .Build()
            };

            var orderRepositoryMock = A.OrderRepositoryMock
                .W(orderCollection)
                .Build();

            var companyEntity = A.CompanyEntity.Build();

            var companyRepositoryMock = A.CompanyRepositoryMock
                .W(companyEntity)
                .Build();

            var request = A.TransactionsAppLessRequest
                .W(A.TransactionsAppLessSheetFilter
                    .W((ShowProductsOption)showProducts)
                    .W((ShowSystemProductsOption)showSystemProducts)
                    .W((ShowProductQuantityOption)showProductQuantity)
                    .W((ShowProductPriceOption)showProductPrice)
                    .W((ShowProductCategoryOption)showProductCategory)
                    .W((ShowAlterationsOption)showAlterations)
                    .W((ShowServiceMethod)showServiceMethod)
                    .W((ShowCheckoutMethod)showCheckoutMethod)
                    .W((ShowCustomerInfoOption)showCustomerInfo)
                    .W((ShowOrderNotesOption)showOrderNotesOption)
                    .W((ShowPaymentMethod)showPaymentMethod)
                    .W((ShowDeliveryPoint)showDeliverypoint))
                .Build();

            var sut = new OrderReport(request, orderRepositoryMock, companyRepositoryMock);

            // Act
            var result = sut.CreateReport(out var spreadsheet);

            // Assert
            Assert.True(result);
            try
            {
                var sheetFilter = request.TransactionsAppLessSheetFilters.First();

                OrderReportValidation.AssertCorrectOrderDetailHeaders(sheetFilter, spreadsheet);
                OrderReportValidation.AssertCorrectOrderDetailValues(sheetFilter, companyEntity, orderCollection, spreadsheet);
                //spreadsheet.SaveAs(@"G:\My Drive\OrderReport.xlsx");
            }
            finally
            {
                spreadsheet.Dispose();
            }
        }
    }
}
