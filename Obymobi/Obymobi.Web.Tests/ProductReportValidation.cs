using NUnit.Framework;
using Obymobi.Data.CollectionClasses;
using Obymobi.Web.Analytics.DomainTypes;
using Obymobi.Web.Analytics.Extensions;
using Obymobi.Web.Analytics.Model.Orders;
using SpreadsheetLight;
using System;
using System.Drawing;
using System.Linq;

namespace Obymobi.Web.Tests
{
    internal static class ProductReportValidation
    {
        private static class DataLocations
        {
            private static int ReportInformationValueColumnIndex => 2;
            public static int ProductSalesHeaderRowIndex => 9;
            public static int IndividualPriceColumnIndex = 3;

            public static string VendorName => SLConvert.ToCellReference(1, ReportInformationValueColumnIndex);
            public static string FromDate => SLConvert.ToCellReference(2, ReportInformationValueColumnIndex);
            public static string UntilDate => SLConvert.ToCellReference(3, ReportInformationValueColumnIndex);
            public static string TotalProductRevenue => SLConvert.ToCellReference(5, ReportInformationValueColumnIndex);
            public static string ItemHeaderCell => SLConvert.ToCellReference(ProductSalesHeaderRowIndex, 1);
            public static string QuantityHeaderCell => SLConvert.ToCellReference(ProductSalesHeaderRowIndex, 2);
            public static string IndividualPriceHeaderCell => SLConvert.ToCellReference(ProductSalesHeaderRowIndex, IndividualPriceColumnIndex);
            public static string IndividualPriceCell(int rowOffset, int columnOffset) => SLConvert.ToCellReference(ProductSalesHeaderRowIndex + rowOffset, 3);
            public static string TotalPriceHeaderCell => SLConvert.ToCellReference(ProductSalesHeaderRowIndex, 4);
        }

        public static void AssertEqualVendorName(string expectedName, SLDocument spreadsheet) => Assert.AreEqual(expectedName, spreadsheet.GetCellValueAsString(DataLocations.VendorName));

        public static void AssertEqualReportingPeriod(FromDateTime expectedFrom, UntilDateTime expectedUntil, SLDocument spreadsheet)
        {
            const string format = @"yyyy-mm-dd";
            Assert.AreEqual(DateTimeToString(expectedFrom), DateTimeToString(spreadsheet.GetCellValueAsDateTime(DataLocations.FromDate)));
            Assert.AreEqual(DateTimeToString(expectedUntil), DateTimeToString(spreadsheet.GetCellValueAsDateTime(DataLocations.UntilDate)));

            string DateTimeToString(DateTime dateTime) => dateTime.ToString(format);
        }

        public static void AssertEqualOrderTotals(decimal expectedTotalProductRevenue, SLDocument spreadsheet)
        {
            Assert.AreEqual(expectedTotalProductRevenue, spreadsheet.GetCellValueAsDecimal(DataLocations.TotalProductRevenue), "Total product revenue");

            var row = DataLocations.ProductSalesHeaderRowIndex + 1;
            while (!string.IsNullOrEmpty(spreadsheet.GetCellValueAsString(DataLocations.IndividualPriceCell(row, DataLocations.IndividualPriceColumnIndex))))
            {
                ++row;
            }
        }

        public static void AssertCorrectOrderItems(OrderCollection orders, SLDocument spreadsheet)
        {
            var currentRow = DataLocations.ProductSalesHeaderRowIndex + 1;

            foreach (ProductSale orderItem in orders.SelectMany(order
                    => order.OrderitemCollection.SelectMany(selector: orderItem
                        => orderItem.OrderitemAlterationitemCollection.Select(alterationItem
                            => new ProductSale(alterationItem.AlterationoptionName, string.Empty, orderItem.Quantity, alterationItem.PriceInTax)).Concat(new[] { orderItem.ToProduct() }))).GroupBy(i => new
                            {
                                i.Name,
                                i.Price
                            })
                .Select(x => new ProductSale(x.Key.Name, string.Empty, x.Sum(k => k.Quantity), x.Key.Price))
                .OrderBy(i => i.Name)
                .ThenBy(i => i.TotalPrice)

            )
            {
                Assert.AreEqual(orderItem.Name, spreadsheet.GetCellValueAsString(SLConvert.ToCellReference(currentRow, 1)));
                Assert.AreEqual(orderItem.Quantity, spreadsheet.GetCellValueAsInt32(SLConvert.ToCellReference(currentRow, 2)));
                Assert.AreEqual(orderItem.Price, spreadsheet.GetCellValueAsDecimal(SLConvert.ToCellReference(currentRow, 3)));
                Assert.AreEqual(orderItem.TotalPrice, spreadsheet.GetCellValueAsDecimal(SLConvert.ToCellReference(currentRow, 4))
                );
                currentRow++;
            }
        }

        public static void AssertProductSubViewVisible(SLDocument spreadsheet)
        {
            Assert.AreEqual("Item", spreadsheet.GetCellValueAsString(DataLocations.ItemHeaderCell));
            Assert.AreEqual("Quantity", spreadsheet.GetCellValueAsString(DataLocations.QuantityHeaderCell));
            Assert.AreEqual("Individual Price", spreadsheet.GetCellValueAsString(DataLocations.IndividualPriceHeaderCell));
            Assert.AreEqual("Total Price", spreadsheet.GetCellValueAsString(DataLocations.TotalPriceHeaderCell));
        }

        public static void AssertExpectedOrderHeaderBackgroundAndBorder(SLDocument spreadsheet)
        {
            var columnIndex = SLConvert.ToColumnIndex(DataLocations.ItemHeaderCell);
            var rowIndex = DataLocations.ProductSalesHeaderRowIndex;

            string value;
            do
            {
                var cellReference = SLConvert.ToCellReference(rowIndex, columnIndex);
                columnIndex++;

                value = spreadsheet.GetCellValueAsString(cellReference);
                if (string.IsNullOrEmpty(value))
                {
                    continue;
                }

                var style = spreadsheet.GetCellStyle(cellReference);
                Assert.IsTrue(style.Font.Bold);
                Assert.AreEqual(Color.FromArgb(147, 196, 125), style.Fill.PatternBackgroundColor);
            } while (!string.IsNullOrEmpty(value));
        }
    }
}
