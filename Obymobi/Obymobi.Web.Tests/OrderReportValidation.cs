using NUnit.Framework;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Enums;
using Obymobi.Web.Analytics.DomainTypes;
using Obymobi.Web.Analytics.Model.TransactionsAppLess;
using Obymobi.Web.Analytics.Requests;
using SpreadsheetLight;
using System;
using System.Drawing;
using System.Linq;

namespace Obymobi.Web.Tests
{
    internal static class OrderReportValidation
    {
        private static class DataLocations
        {
            private static int ReportInformationValueColumnIndex => 2;
            public static int OrderHeaderRowIndex => 11;

            public static string Title => SLConvert.ToCellReference(1, 1);
            public static string SubTitle => SLConvert.ToCellReference(2, 1);
            public static string FromDate => SLConvert.ToCellReference(5, ReportInformationValueColumnIndex);
            public static string UntilDate => SLConvert.ToCellReference(6, ReportInformationValueColumnIndex);
            public static string NumberOfOrders => SLConvert.ToCellReference(8, ReportInformationValueColumnIndex);
            public static string TotalOrderRevenue => SLConvert.ToCellReference(9, ReportInformationValueColumnIndex);
            public static string TotalProductRevenue => SLConvert.ToCellReference(10, ReportInformationValueColumnIndex);
            public static string VendorName => SLConvert.ToCellReference(4, ReportInformationValueColumnIndex);
            public static string OrderDateHeaderCell => SLConvert.ToCellReference(OrderHeaderRowIndex, 1);
            public static string OrderNumberHeaderCell => SLConvert.ToCellReference(OrderHeaderRowIndex, 2);
            public static string ServiceMethodHeaderCell => SLConvert.ToCellReference(OrderHeaderRowIndex, 3);
            public static string OrderTotalHeaderCell => SLConvert.ToCellReference(OrderHeaderRowIndex, 4);
            public static string ProductCategoryHeaderCell => SLConvert.ToCellReference(OrderHeaderRowIndex, 5);
            public static string OrderHeaderCell(int offset) => SLConvert.ToCellReference(OrderHeaderRowIndex, 6 + offset);
            public static string OrderItemNameCell(int rowOffset, int columnOffset) => SLConvert.ToCellReference(OrderHeaderRowIndex + rowOffset, 6 + columnOffset);
            public static string QuantityHeaderCell(int offset) => SLConvert.ToCellReference(OrderHeaderRowIndex, 7 + offset);
            public static string PriceHeaderCell(int offset) => SLConvert.ToCellReference(OrderHeaderRowIndex, 8 + offset);
            public static string NotesHeaderCell(int offset) => SLConvert.ToCellReference(OrderHeaderRowIndex, 8 + offset);
            public static string OrderHeaderColumn(int column) => SLConvert.ToCellReference(DataLocations.OrderHeaderRowIndex, column);
        }

        public static void AssertEqualTitleSection(string expectedTitle, string expectedSubTitle, SLDocument spreadsheet)
        {
            Assert.AreEqual(expectedTitle, spreadsheet.GetCellValueAsString(DataLocations.Title));
            Assert.AreEqual(expectedSubTitle, spreadsheet.GetCellValueAsString(DataLocations.SubTitle));
        }

        public static void AssertEqualReportingPeriod(FromDateTime expectedFrom, UntilDateTime expectedUntil, SLDocument spreadsheet)
        {
            const string format = @"yyyy-mm-dd";
            Assert.AreEqual(DateTimeToString(expectedFrom), DateTimeToString(spreadsheet.GetCellValueAsDateTime(DataLocations.FromDate)));
            Assert.AreEqual(DateTimeToString(expectedUntil), DateTimeToString(spreadsheet.GetCellValueAsDateTime(DataLocations.UntilDate)));

            string DateTimeToString(DateTime dateTime) => dateTime.ToString(format);
        }

        public static void AssertEqualOrderTotals(int expectedNumberOfOrders, decimal expectedTotalOrderRevenue, decimal expectedTotalProductRevenue, SLDocument spreadsheet)
        {
            Assert.AreEqual(expectedNumberOfOrders, spreadsheet.GetCellValueAsInt32(DataLocations.NumberOfOrders));
            Assert.AreEqual(expectedTotalOrderRevenue, spreadsheet.GetCellValueAsDecimal(DataLocations.TotalOrderRevenue));
            Assert.AreEqual(expectedTotalProductRevenue, spreadsheet.GetCellValueAsDecimal(DataLocations.TotalProductRevenue));
        }

        public static void AssertEqualVendorName(string expectedName, SLDocument spreadsheet) => Assert.AreEqual(expectedName, spreadsheet.GetCellValueAsString(DataLocations.VendorName));

        public static void AssertOrderSubViewVisible(TransactionsAppLessRequest request, SLDocument spreadsheet)
        {
            Assert.AreEqual("Order Date", spreadsheet.GetCellValueAsString(DataLocations.OrderDateHeaderCell));
            Assert.AreEqual("Order #", spreadsheet.GetCellValueAsString(DataLocations.OrderNumberHeaderCell));
            Assert.AreEqual("Service Method", spreadsheet.GetCellValueAsString(DataLocations.ServiceMethodHeaderCell));
            Assert.AreEqual("Order Total", spreadsheet.GetCellValueAsString(DataLocations.OrderTotalHeaderCell));
        }

        public static void AssertCorrectOrderDetailHeaders(TransactionsAppLessSheetFilter sheetFilter, SLDocument spreadsheet)
        {
            Assert.AreEqual("Order Date", spreadsheet.GetCellValueAsString(DataLocations.OrderDateHeaderCell));
            Assert.AreEqual("Order #", spreadsheet.GetCellValueAsString(DataLocations.OrderNumberHeaderCell));

            var currentColumn = 2;
            if (sheetFilter.ShowServiceMethod)
            {
                Assert.AreEqual("Service Method", spreadsheet.GetCellValueAsString(DataLocations.OrderHeaderColumn(++currentColumn)));
            }

            if (sheetFilter.ShowCheckoutMethod)
            {
                Assert.AreEqual("Checkout Method", spreadsheet.GetCellValueAsString(DataLocations.OrderHeaderColumn(++currentColumn)));
            }

            if (sheetFilter.ShowPaymentMethod)
            {
                Assert.AreEqual("Payment Method", spreadsheet.GetCellValueAsString(DataLocations.OrderHeaderColumn(++currentColumn)));
            }

            if (sheetFilter.ShowCardSummary)
            {
                Assert.AreEqual("Last 4 Card Digits", spreadsheet.GetCellValueAsString(DataLocations.OrderHeaderColumn(++currentColumn)));
            }

            if (sheetFilter.ShowDeliveryPoint)
            {
                Assert.AreEqual("Delivery Point", spreadsheet.GetCellValueAsString(DataLocations.OrderHeaderColumn(++currentColumn)));
            }

            if (sheetFilter.ShowCoversCount)
            {
                Assert.AreEqual("Cover Count", spreadsheet.GetCellValueAsString(DataLocations.OrderHeaderColumn(++currentColumn)));
            }

            Assert.AreEqual("Order Total", spreadsheet.GetCellValueAsString(DataLocations.OrderHeaderColumn(++currentColumn)));

            if (sheetFilter.ShowProductCategory)
            {
                Assert.AreEqual("Category", spreadsheet.GetCellValueAsString(DataLocations.OrderHeaderColumn(++currentColumn)));
            }

            if (sheetFilter.ShowProducts || sheetFilter.ShowSystemProducts || sheetFilter.ShowAlterations)
            {
                Assert.AreEqual("Order", spreadsheet.GetCellValueAsString(DataLocations.OrderHeaderColumn(++currentColumn)));

                if (sheetFilter.ShowProductQuantity)
                {
                    Assert.AreEqual("Quantity", spreadsheet.GetCellValueAsString(DataLocations.OrderHeaderColumn(++currentColumn)));
                }

                if (sheetFilter.ShowProductPrice)
                {
                    Assert.AreEqual("Price", spreadsheet.GetCellValueAsString(DataLocations.OrderHeaderColumn(++currentColumn)));
                }
            }

            if (sheetFilter.ShowCustomerInfo)
            {
                Assert.AreEqual("Customer info", spreadsheet.GetCellValueAsString(DataLocations.OrderHeaderColumn(++currentColumn)));
            }

            if (sheetFilter.ShowOrderNotes)
            {
                Assert.AreEqual("Notes & instructions", spreadsheet.GetCellValueAsString(DataLocations.OrderHeaderColumn(++currentColumn)));
            }
        }

        public static void AssertOrderItemSubViewVisible(TransactionsAppLessSheetFilter sheetFilter, SLDocument spreadsheet)
        {
            var offset = 0;
            if (sheetFilter.ShowProductCategory)
            {
                Assert.AreEqual("Category", spreadsheet.GetCellValueAsString(DataLocations.ProductCategoryHeaderCell));
            }
            else
            {
                offset--;
            }

            Assert.AreEqual("Order", spreadsheet.GetCellValueAsString(DataLocations.OrderHeaderCell(offset)));

            if (sheetFilter.ShowProductQuantity)
            {
                Assert.AreEqual("Quantity", spreadsheet.GetCellValueAsString(DataLocations.QuantityHeaderCell(offset)));
            }
            else
            {
                offset--;
            }

            if (sheetFilter.ShowProductPrice)
            {
                Assert.AreEqual("Price", spreadsheet.GetCellValueAsString(DataLocations.PriceHeaderCell(offset)));
            }
        }

        public static void AssertOrderItemSubViewInvisible(TransactionsAppLessSheetFilter sheetFilter, SLDocument spreadsheet)
        {
            var offset = 0;
            if (sheetFilter.ShowProductCategory)
            {
                Assert.AreNotEqual("Category", spreadsheet.GetCellValueAsString(DataLocations.ProductCategoryHeaderCell));
            }
            else
            {
                offset--;
            }

            Assert.AreNotEqual("Order", spreadsheet.GetCellValueAsString(DataLocations.OrderHeaderCell(offset)));

            if (sheetFilter.ShowProductQuantity)
            {
                Assert.AreNotEqual("Quantity", spreadsheet.GetCellValueAsString(DataLocations.QuantityHeaderCell(offset)));
            }
            else
            {
                offset--;
            }

            if (sheetFilter.ShowProductPrice)
            {
                Assert.AreNotEqual("Price", spreadsheet.GetCellValueAsString(DataLocations.PriceHeaderCell(offset)));
            }
        }

        public static void AssertOrderItemCategoryVisible(TransactionsAppLessRequest request, SLDocument spreadsheet) => Assert.AreEqual("Category", spreadsheet.GetCellValueAsString(DataLocations.ProductCategoryHeaderCell));
        public static void AssertOrderItemCategoryInvisible(TransactionsAppLessRequest request, SLDocument spreadsheet) => Assert.AreNotEqual("Category", spreadsheet.GetCellValueAsString(DataLocations.ProductCategoryHeaderCell));

        public static void AssertOrderItemQuantityInvisible(TransactionsAppLessSheetFilter sheetFilter, SLDocument spreadsheet)
        {
            var offset = 0;
            if (!sheetFilter.ShowProductCategory)
            {
                offset--;
            }

            Assert.AreNotEqual("Quantity", spreadsheet.GetCellValueAsString(DataLocations.QuantityHeaderCell(offset)));
        }

        public static void AssertOrderItemCount(int expectedProductCount, TransactionsAppLessSheetFilter sheetFilter, SLDocument spreadsheet)
        {
            var productCount = 0;

            var columnOffset = 0;
            if (!sheetFilter.ShowProductCategory)
            {
                columnOffset--;
            }

            var rowOffset = 1;

            var productName = spreadsheet.GetCellValueAsString(DataLocations.OrderItemNameCell(rowOffset, columnOffset));

            while (!string.IsNullOrEmpty(productName))
            {
                productCount++;
                productName = spreadsheet.GetCellValueAsString(DataLocations.OrderItemNameCell(++rowOffset, columnOffset));
            }

            Assert.AreEqual(expectedProductCount, productCount);
        }

        public static void AssertContainsOrderItemWithName(string expectedName, TransactionsAppLessSheetFilter sheetFilter, SLDocument spreadsheet)
        {
            var columnOffset = 0;
            if (!sheetFilter.ShowProductCategory)
            {
                columnOffset--;
            }

            var rowOffset = 1;

            var actualName = spreadsheet.GetCellValueAsString(DataLocations.OrderItemNameCell(rowOffset, columnOffset));

            while (!string.IsNullOrEmpty(actualName))
            {
                if (actualName == expectedName)
                {
                    return;
                }

                actualName = spreadsheet.GetCellValueAsString(DataLocations.OrderItemNameCell(++rowOffset, columnOffset));
            }

            Assert.Fail($@"The report did not contain a product with the name {expectedName}");
        }

        public static void AssertContainsAlterationItemWithName(string expectedName, TransactionsAppLessSheetFilter sheetFilter, SLDocument spreadsheet)
        {
            var columnOffset = 0;
            if (!sheetFilter.ShowProductCategory)
            {
                columnOffset--;
            }

            var rowOffset = 1;

            var actualName = spreadsheet.GetCellValueAsString(DataLocations.OrderItemNameCell(rowOffset, columnOffset));

            while (!string.IsNullOrEmpty(actualName))
            {
                if (actualName == $@"- {expectedName}")
                {
                    return;
                }

                actualName = spreadsheet.GetCellValueAsString(DataLocations.OrderItemNameCell(++rowOffset, columnOffset));
            }

            Assert.Fail($@"The report did not contain a product with the name {expectedName}");
        }

        public static void AssertDoesNotContainAterationItemWithName(string expectedName, TransactionsAppLessSheetFilter sheetFilter, SLDocument spreadsheet)
        {
            var columnOffset = 0;
            if (!sheetFilter.ShowProductCategory)
            {
                columnOffset--;
            }

            var rowOffset = 1;

            var actualName = spreadsheet.GetCellValueAsString(DataLocations.OrderItemNameCell(rowOffset, columnOffset));

            while (!string.IsNullOrEmpty(actualName))
            {
                Assert.AreNotEqual($@"- {expectedName}", actualName);
                actualName = spreadsheet.GetCellValueAsString(DataLocations.OrderItemNameCell(++rowOffset, columnOffset));
            }
        }


        public static void AssertOrderItemPriceInvisible(TransactionsAppLessSheetFilter sheetFilter, SLDocument spreadsheet)
        {
            var offset = 0;
            if (sheetFilter.ShowProductCategory)
            {
                offset--;
            }

            if (sheetFilter.ShowProductQuantity)
            {
                offset--;
            }

            if (sheetFilter.ShowProductPrice)
            {
                Assert.AreNotEqual("Price", spreadsheet.GetCellValueAsString(DataLocations.PriceHeaderCell(offset)));
            }
        }

        public static void AssertNotesSubViewVisible(TransactionsAppLessSheetFilter sheetFilter, SLDocument spreadsheet)
        {
            var offset = 0;

            if (sheetFilter.ShowProductCategory)
            {
                offset++;
            }

            if (sheetFilter.ShowCustomerInfo)
            {
                offset++;
            }

            if (!sheetFilter.ShowProductQuantity)
            {
                offset--;
            }

            if (!sheetFilter.ShowProductPrice)
            {
                offset--;
            }

            Assert.AreEqual("Notes & instructions", spreadsheet.GetCellValueAsString(DataLocations.NotesHeaderCell(offset)));
        }

        public static void AssertExpectedOrderHeaderBackgroundAndBorder(SLDocument spreadsheet)
        {
            var columnIndex = SLConvert.ToColumnIndex(DataLocations.OrderDateHeaderCell);
            var rowIndex = DataLocations.OrderHeaderRowIndex;

            string value;
            do
            {
                var cellReference = SLConvert.ToCellReference(rowIndex, columnIndex);
                columnIndex++;

                value = spreadsheet.GetCellValueAsString(cellReference);
                if (string.IsNullOrEmpty(value))
                {
                    continue;
                }

                var style = spreadsheet.GetCellStyle(cellReference);
                Assert.IsTrue(style.Font.Bold);
                Assert.AreEqual(Color.FromArgb(147, 196, 125), style.Fill.PatternBackgroundColor);
            } while (!string.IsNullOrEmpty(value));

        }

        public static void AssertDocumentContainsWorksheet(SLDocument spreadsheet, string name) => CollectionAssert.Contains(spreadsheet.GetWorksheetNames(), name);

        public static void AssertCorrectOrderDetailValues(TransactionsAppLessSheetFilter sheetFilter, CompanyEntity companyEntity, OrderCollection orders, SLDocument spreadsheet)
        {
            var currentRow = DataLocations.OrderHeaderRowIndex + 1;
            foreach (var orderEntity in orders.OrderBy(x => x.CreatedUTC))
            {
                var currentColumn = 1;
                var previousRow = currentRow;

                if (sheetFilter.ShowProducts || sheetFilter.ShowSystemProducts)
                {
                    if (!orderEntity.OrderitemCollection.Any())
                    {
                        continue;
                    }

                    var containsProductItems = orderEntity.OrderitemCollection.Any(orderItem => orderItem.Type == OrderitemType.Product);
                    var systemProductItems = orderEntity.OrderitemCollection.Where(orderItem => orderItem.Type != OrderitemType.Product);

                    if (sheetFilter.IncludedSystemProductTypes.Any())
                    {
                        systemProductItems = systemProductItems.Where(orderItem => sheetFilter.IncludedSystemProductTypes.Contains((int)orderItem.Type));
                    }

                    var containsSystemProductItems = systemProductItems.Any();

                    if ((!sheetFilter.ShowProducts || !containsProductItems) && (!sheetFilter.ShowSystemProducts || !containsSystemProductItems))
                    {
                        continue;
                    }
                }

                var expected = DateTimeToString(orderEntity.CreatedUTC);
                var actual = DateTimeToString(spreadsheet.GetCellValueAsDateTime(SLConvert.ToCellReference(currentRow, currentColumn)));

                // Next commented out lines of code are useful for debugging failing tests and therefor not removed.
                //if (expected != actual)
                //{
                //    spreadsheet.SaveAs(@"G:\My Drive\OrderReport.xlsx");
                //}

                Assert.AreEqual(expected, actual,
                    "Incorrect order date. Expected: {0}, Actual: {1}, Row: {2}, Column: {3}",
                    expected,
                    actual,
                    currentRow,
                    currentColumn);

                Assert.AreEqual(orderEntity.OrderId, spreadsheet.GetCellValueAsInt64(SLConvert.ToCellReference(currentRow, ++currentColumn)));

                if (sheetFilter.ShowServiceMethod)
                {
                    Assert.AreEqual(orderEntity.ServiceMethodName, spreadsheet.GetCellValueAsString(SLConvert.ToCellReference(currentRow, ++currentColumn)));
                }

                if (sheetFilter.ShowCheckoutMethod)
                {
                    Assert.AreEqual(orderEntity.CheckoutMethodName, spreadsheet.GetCellValueAsString(SLConvert.ToCellReference(currentRow, ++currentColumn)));
                }

                if (sheetFilter.ShowPaymentMethod)
                {
                    PaymentTransactionEntity paymentTransaction = orderEntity.PaymentTransactionCollection.FirstOrDefault();

                    Assert.AreEqual(paymentTransaction?.PaymentMethod, spreadsheet.GetCellValueAsString(SLConvert.ToCellReference(currentRow, ++currentColumn)));
                }

                if (sheetFilter.ShowCardSummary)
                {
                    PaymentTransactionEntity paymentTransaction = orderEntity.PaymentTransactionCollection.FirstOrDefault();

                    Assert.AreEqual(paymentTransaction.CardSummary, spreadsheet.GetCellValueAsString(SLConvert.ToCellReference(currentRow, ++currentColumn)));
                }

                if (sheetFilter.ShowDeliveryPoint)
                {
                    Assert.AreEqual(orderEntity.DeliverypointName, spreadsheet.GetCellValueAsString(SLConvert.ToCellReference(currentRow, ++currentColumn)));
                }

                if (sheetFilter.ShowCoversCount)
                {
                    Assert.AreEqual(orderEntity.CoversCount ?? 0, spreadsheet.GetCellValueAsInt64(SLConvert.ToCellReference(currentRow, ++currentColumn)));
                }

                Assert.AreEqual(orderEntity.Total, spreadsheet.GetCellValueAsDecimal(SLConvert.ToCellReference(currentRow, ++currentColumn)));

                if (!ShowProducts(sheetFilter))
                {
                    currentRow++;
                    continue;
                }

                var productCategoryColumnIndex = sheetFilter.ShowProductCategory ? ++currentColumn : -1;
                var productNameColumnIndex = ++currentColumn;
                var quantityColumnIndex = sheetFilter.ShowProductQuantity ? ++currentColumn : -1;
                var priceColumnIndex = sheetFilter.ShowProductPrice ? ++currentColumn : -1;
                var customerInformationColumnIndex = sheetFilter.ShowCustomerInfo ? ++currentColumn : -1;
                var notesColumnIndex = ++currentColumn;

                foreach (var orderItemEntity in orderEntity.OrderitemCollection)
                {
                    if (!sheetFilter.ShowProducts && orderItemEntity.Type == OrderitemType.Product)
                    {
                        continue;
                    }

                    if (!sheetFilter.ShowSystemProducts && orderItemEntity.Type != OrderitemType.Product)
                    {
                        continue;
                    }

                    if (sheetFilter.ShowSystemProducts && sheetFilter.IncludedSystemProductTypes.Any() && !sheetFilter.IncludedSystemProductTypes.Contains((int)orderItemEntity.Type))
                    {
                        continue;
                    }

                    if (sheetFilter.ShowProductCategory)
                    {
                        Assert.AreEqual(orderItemEntity.CategoryPath, spreadsheet.GetCellValueAsString(SLConvert.ToCellReference(currentRow, productCategoryColumnIndex)));
                    }

                    Assert.AreEqual(orderItemEntity.ProductName, spreadsheet.GetCellValueAsString(SLConvert.ToCellReference(currentRow, productNameColumnIndex)));

                    if (sheetFilter.ShowProductQuantity)
                    {
                        if (orderItemEntity.Type == OrderitemType.Product)
                        {
                            Assert.AreEqual(orderItemEntity.Quantity, spreadsheet.GetCellValueAsInt64(SLConvert.ToCellReference(currentRow, quantityColumnIndex)));
                        }
                        else
                        {
                            Assert.AreEqual(string.Empty, spreadsheet.GetCellValueAsString(SLConvert.ToCellReference(currentRow, quantityColumnIndex)));
                        }
                    }

                    if (sheetFilter.ShowProductPrice)
                    {
                        Assert.AreEqual(orderItemEntity.PriceTotalInTax, spreadsheet.GetCellValueAsDecimal(SLConvert.ToCellReference(currentRow, priceColumnIndex)));

                    }

                    currentRow++;

                    if (sheetFilter.ShowAlterations)
                    {
                        foreach (var orderitemAlterationitemEntity in orderItemEntity.OrderitemAlterationitemCollection)
                        {
                            Assert.AreEqual($"- {orderitemAlterationitemEntity.AlterationName} ({orderitemAlterationitemEntity.AlterationoptionName})", spreadsheet.GetCellValueAsString(SLConvert.ToCellReference(currentRow, productNameColumnIndex)));

                            if (sheetFilter.ShowProductQuantity)
                            {
                                Assert.AreEqual(string.Empty, spreadsheet.GetCellValueAsString(SLConvert.ToCellReference(currentRow, quantityColumnIndex)));
                            }

                            if (sheetFilter.ShowProductPrice)
                            {
                                Assert.AreEqual(orderitemAlterationitemEntity.PriceTotalInTax, spreadsheet.GetCellValueAsDecimal(SLConvert.ToCellReference(currentRow, priceColumnIndex)));
                            }

                            currentRow++;

                        }
                    }

                }

                if (sheetFilter.ShowCustomerInfo)
                {
                    var customerInfoRow = previousRow;
                    if (!string.IsNullOrEmpty(orderEntity.CustomerLastname))
                    {
                        Assert.AreEqual(orderEntity.CustomerLastname, spreadsheet.GetCellValueAsString(SLConvert.ToCellReference(customerInfoRow, customerInformationColumnIndex)));
                        customerInfoRow++;
                    }

                    if (!string.IsNullOrEmpty(orderEntity.Email) || !string.IsNullOrEmpty(orderEntity.CustomerPhonenumber))
                    {
                        var emailPhone = CombineEmailAndPhoneNumber(orderEntity);

                        Assert.AreEqual(emailPhone, spreadsheet.GetCellValueAsString(SLConvert.ToCellReference(customerInfoRow, customerInformationColumnIndex)));
                        customerInfoRow++;
                    }

                    if (!string.IsNullOrEmpty(orderEntity.DeliveryInformationEntity.Address))
                    {
                        Assert.AreEqual(orderEntity.DeliveryInformationEntity.Address, spreadsheet.GetCellValueAsString(SLConvert.ToCellReference(customerInfoRow, customerInformationColumnIndex)));
                        customerInfoRow++;
                    }

                    if (!string.IsNullOrEmpty(orderEntity.DeliveryInformationEntity.Zipcode) || !string.IsNullOrEmpty(orderEntity.DeliveryInformationEntity.City))
                    {
                        var postCodeAndCity = $"{orderEntity.DeliveryInformationEntity.Zipcode} {orderEntity.DeliveryInformationEntity.City}";

                        Assert.AreEqual(postCodeAndCity, spreadsheet.GetCellValueAsString(SLConvert.ToCellReference(customerInfoRow, customerInformationColumnIndex)));
                        customerInfoRow++;
                    }

                    currentRow = Math.Max(currentRow, customerInfoRow);
                }

                if (sheetFilter.ShowOrderNotes)
                {
                    var notesRow = previousRow;
                    var expectedCellContent = string.Empty;

                    var linesAdded = 0;

                    if (!string.IsNullOrWhiteSpace(orderEntity.Notes))
                    {
                        expectedCellContent = $"{orderEntity.Notes}\n\n";
                        linesAdded += 2;
                    }

                    if (!string.IsNullOrWhiteSpace(orderEntity.DeliveryInformationEntity.Instructions))
                    {

                        expectedCellContent += $"{orderEntity.DeliveryInformationEntity.Instructions}\n";
                        linesAdded++;
                    }

                    if (!string.IsNullOrWhiteSpace(orderEntity.Notes) && string.IsNullOrWhiteSpace(orderEntity.DeliveryInformationEntity.Instructions))
                    {
                        // The empty line at the end will be removed by the trim call, so decrease the lines added by one.
                        linesAdded--;
                    }

                    if (expectedCellContent.Length > 0)
                    {
                        Assert.AreEqual(expectedCellContent.ToString().Trim(), spreadsheet.GetCellValueAsString(SLConvert.ToCellReference(notesRow, notesColumnIndex)));
                    }

                    currentRow = Math.Max(notesRow + linesAdded - 1, currentRow);
                }
            }

            string DateTimeToString(DateTime? dateTime) => dateTime?.ToString(@"yyyy-MM-dd h:mm:ss");
        }
        private static bool ShowProducts(TransactionsAppLessSheetFilter sheetFilter) => sheetFilter.ShowProducts || sheetFilter.ShowSystemProducts || sheetFilter.ShowAlterations;

        private static string CombineEmailAndPhoneNumber(OrderEntity order)
        {
            var emailPhone = order.CustomerPhonenumber;

            if (!string.IsNullOrWhiteSpace(emailPhone) && !string.IsNullOrWhiteSpace(order.Email))
            {
                emailPhone += @" / ";
            }

            if (!string.IsNullOrWhiteSpace(order.Email))
            {
                emailPhone += order.Email;
            }

            return emailPhone;
        }

    }
}
