using NUnit.Framework;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Web.Analytics.Model.ModelBase;
using Obymobi.Web.Analytics.Model.ProductReport;
using Obymobi.Web.Analytics.Reports.ProductSummaryReport;
using Obymobi.Web.Analytics.Repositories;
using Obymobi.Web.Analytics.Repositories.TransactionsAppLess;
using Obymobi.Web.Analytics.Requests;
using Obymobi.Web.Tests.Builders;
using SpreadsheetLight;
using System;
using System.Linq;

namespace Obymobi.Web.Tests
{
    [TestFixture]
    public class ProductReportTest
    {
        private static class A
        {
            public static ProductReportRequestBuilder ProductReportRequest => new ProductReportRequestBuilder();
            public static OrderEntityBuilder OrderEntity => new OrderEntityBuilder();
            public static CompanyEntityBuilder CompanyEntity => new CompanyEntityBuilder();
            public static OrderRepositoryMockBuilder OrderRepositoryMock => new OrderRepositoryMockBuilder();
            public static CompanyRepositoryMockBuilder CompanyRepositoryMock => new CompanyRepositoryMockBuilder();
            public static TagRepositoryMockBuilder TagRepositoryMock => new TagRepositoryMockBuilder();
        }

        [Test]
        public void CreateReport_WithDefaultOptions_CreatesDefaultReport()
        {
            // Arrange
            OrderCollection orderCollection = new OrderCollection
            {
                A.OrderEntity.Build(),
                A.OrderEntity.Build(),
                A.OrderEntity.Build(),
                A.OrderEntity.Build(),
                A.OrderEntity.Build()
            };

            IOrderRepository orderRepositoryMock = A.OrderRepositoryMock
                .W(orderCollection)
                .Build();

            CompanyEntity companyEntity = A.CompanyEntity.Build();

            ICompanyRepository companyRepositoryMock = A.CompanyRepositoryMock
                .W(companyEntity)
                .Build();

            ITagRepository tagRepository = A.TagRepositoryMock.Build();

            ProductReportRequest request = A.ProductReportRequest.Build();

            ProductReport sut = new ProductReport(request, orderRepositoryMock, companyRepositoryMock, tagRepository);

            decimal totalProductRevenue = orderCollection.Sum(order => order.OrderitemCollection
                .Sum(orderItem => orderItem.PriceInTax * orderItem.Quantity + orderItem.OrderitemAlterationitemCollection.Sum(alterationItem => alterationItem.PriceInTax * orderItem.Quantity)));

            // Act
            bool result = sut.CreateReport(out SLDocument spreadsheet);
            try
            {
                SpreadSheetFilter sheetFilter = request.SheetFilters.First();

                // Assert
                Assert.True(result);
                ProductReportValidation.AssertEqualVendorName(companyEntity.Name, spreadsheet);
                ProductReportValidation.AssertEqualReportingPeriod(request.FromDateTimeUtc, request.UntilDateTimeUtc, spreadsheet);
                ProductReportValidation.AssertEqualOrderTotals(totalProductRevenue, spreadsheet);
                ProductReportValidation.AssertProductSubViewVisible(spreadsheet);
                ProductReportValidation.AssertExpectedOrderHeaderBackgroundAndBorder(spreadsheet);
                ProductReportValidation.AssertCorrectOrderItems(orderCollection, spreadsheet);
                //spreadsheet.SaveAs(@"G:\My Drive\ProductReport.xlsx");
            }
            finally
            {
                spreadsheet.Dispose();
            }
        }

        [Test]
        public void CreateReport_ProductReport()
        {
            ProductReportRequest request = new ProductReportRequest
            {
                CompanyId = 860,
                LeadTimeOffset = new TimeSpan(-14, -1, 0, 0),
                UntilDateTimeUtc = DateTime.UtcNow,
                ShowProducts = true,
                ShowAlterations = true,
                ShowSystemProducts = true,
                ShowCustomerInfo = true,
                SheetFilters = new[]
                {
                    new ProductReportSheetFilter
                    {
                        Id = 1,
                        Name = "Sheet",
                        Active = true,
                        SortOrder = 1,
                        OutletId = null,
                        ServiceMethods = new int[0],
                        CheckoutMethods = new int[0],
                        ShowSystemProducts = false,
                        CategoryFilterType = 0,
                        CategoryFilterIds = new int[0],
                    }
                }
            };
            OrderRepository orderRepository = new OrderRepository();
            CompanyRepository companyRepository = new CompanyRepository();
            TagRepository tagRepository = new TagRepository();
            ProductReport sut = new ProductReport(request, orderRepository, companyRepository, tagRepository);
            // Act
            bool result = sut.CreateReport(out SLDocument spreadsheet);
            try
            {
                SpreadSheetFilter sheetFilter = request.SheetFilters.First();
                // Assert
                Assert.True(result);
                //OrderReportValidation.AssertEqualTitleSection(string.Empty, "Orders Report", spreadsheet);
                //OrderReportValidation.AssertEqualVendorName(companyEntity.Name, spreadsheet);
                ////OrderReportValidation.AssertEqualReportingPeriod(request.FromDateUtc, request.UntilDateUtc, spreadsheet);
                //OrderReportValidation.AssertEqualOrderTotals(orderCollection.Count, totalOrderRevenue, totalProductRevenue, spreadsheet);
                //OrderReportValidation.AssertOrderSubViewVisible(request, spreadsheet);
                //OrderReportValidation.AssertOrderItemSubViewVisible(sheetFilter, spreadsheet);
                //OrderReportValidation.AssertNotesSubViewVisible(sheetFilter, spreadsheet);
                //OrderReportValidation.AssertExpectedOrderHeaderBackgroundAndBorder(spreadsheet);
                //OrderReportValidation.AssertCorrectOrderDetailValues(sheetFilter, companyEntity, orderCollection, spreadsheet);
                //spreadsheet.SaveAs(@"G:\My Drive\ProductReport.xlsx");
            }
            finally
            {
                spreadsheet.Dispose();
            }
        }
    }
}
