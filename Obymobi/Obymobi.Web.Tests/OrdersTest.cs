﻿using NUnit.Framework;
using Obymobi.Web.Analytics.Model.Orders;
using Obymobi.Web.Tests.Builders;

namespace Obymobi.Web.Tests
{
	[TestFixture]
	public class OrdersTest
	{
		private static class A
		{
			public static OrderBuilder Order => new OrderBuilder();
			public static OrderItemBuilder OrderItem => new OrderItemBuilder();
		}

		[Test]
		public void SetOrders_WithAndOrderWithoutOrderItems_ReturnsZeroQuantity()
		{
			// Arrange
			Orders sut = new Orders();
			Order[] orders = new[] {A.Order.Build()};

			// Act
			sut.SetOrders(orders);

			// Assert
			Assert.AreEqual(0, sut.TotalReportQuantity);
		}

		[Test]
		public void SetOrders_WithAndOrderWithoutOrderItems_ReturnsZeroTotalReportRevenue()
		{
			// Arrange
			Orders sut = new Orders();
			Order[] orders = new[] {A.Order.Build()};

			// Act
			sut.SetOrders(orders);

			// Assert
			Assert.AreEqual(0M, sut.TotalReportRevenue);
		}

		[Test]
		public void SetOrders_WithAndOrderWithoutOrderItems_ReturnsZeroTotalRevenue()
		{
			// Arrange
			Orders sut = new Orders();
			const decimal orderTotal = 232323.23M;
			Order[] orders = new[]
			{
				A.Order.W(orderTotal).Build(),
				A.Order.W(orderTotal).Build()
			};

			// Act
			sut.SetOrders(orders);

			// Assert
			Assert.AreEqual(orderTotal * 2, sut.TotalRevenue);
		}

		[Test]
		public void SetOrders_WithOneOrderWithOneOrderItem_ReturnsOneQuantity()
		{
			// Arrange
			Orders sut = new Orders();
			const int quantity = 7;
			Order[] orders = new[]
			{
				A.Order
					.W(new[]
					{
						A.OrderItem
							.W(quantity)
							.Build()
					})
					.Build()
			};

			// Act
			sut.SetOrders(orders);

			// Assert
			Assert.AreEqual(7, sut.TotalReportQuantity);
		}

		[Test]
		public void SetOrders_WithOneOrderWithOneOrderItem_ReturnsThePriceOfTheOneProduct()
		{
			// Arrange
			Orders sut = new Orders();
			const int quantity = 1;
			const decimal price = 123.56M;
			Order[] orders = new[]
			{
				A.Order
					.W(new[]
					{
						A.OrderItem
							.W(quantity)
							.W(price)
							.Build()
					})
					.Build()
			};

			// Act
			sut.SetOrders(orders);

			// Assert
			Assert.AreEqual(123.56M, sut.TotalReportRevenue);
		}

		[Test]
		public void SetOrders_WithMultipleOrdersAndMultipleOrderItems_ReturnsTheExpectedQuantity()
		{
			// Arrange
			Orders sut = new Orders();
			const int quantity1 = 7;
			const int quantity2 = 3;
			const int quantity3 = 19;
			const int expectedQuantity = 38;

			Order[] orders = new[]
			{
				A.Order
					.W(new[]
					{
						A.OrderItem.W(quantity1).Build(),
						A.OrderItem.W(quantity2).Build(),
						A.OrderItem.W(quantity3).Build()
					})
					.Build(),
				A.Order
					.W(new[]
					{
						A.OrderItem.W(quantity2).Build(),
						A.OrderItem.W(quantity2).Build(),
						A.OrderItem.W(quantity2).Build()
					})
					.Build(),
				A.Order
					.W(new OrderItemSale[] { })
					.Build()
			};

			// Act
			sut.SetOrders(orders);

			// Assert
			Assert.AreEqual(expectedQuantity, sut.TotalReportQuantity);
		}

		[Test]
		public void SetOrders_WithMultipleOrdersAndMultipleOrderItems_ReturnsTheExpectedReportRevenue()
		{
			// Arrange
			Orders sut = new Orders();
			const int quantity1 = 7;
			const int quantity2 = 3;
			const int quantity3 = 19;
			const decimal price1 = 1.11M;
			const decimal price2 = 2.22M;
			const decimal price3 = 3.33M;
			const decimal expectedTotalReportRevenue = 13.32M;


			Order[] orders = new[]
			{
				A.Order
					.W(new[]
					{
						A.OrderItem.W(quantity1).W(price1).Build(),
						A.OrderItem.W(quantity2).W(price2).Build(),
						A.OrderItem.W(quantity3).W(price3).Build()
					})
					.Build(),
				A.Order
					.W(new[]
					{
						A.OrderItem.W(quantity2).W(price2).Build(),
						A.OrderItem.W(quantity2).W(price2).Build(),
						A.OrderItem.W(quantity2).W(price2).Build()
					})
					.Build(),
				A.Order
					.W(new OrderItemSale[] { })
					.Build()
			};

			// Act
			sut.SetOrders(orders);

			// Assert
			Assert.AreEqual(expectedTotalReportRevenue, sut.TotalReportRevenue);
		}
	}
}
