using Google.Apis.Manual.Util;
using NUnit.Framework;
using Obymobi.Data;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using Obymobi.Web.Analytics.Model.ModelBase;
using Obymobi.Web.Analytics.Model.TransactionsAppLess;
using Obymobi.Web.Analytics.Reports.TransactionsAppLess;
using Obymobi.Web.Analytics.Repositories.TransactionsAppLess;
using Obymobi.Web.Analytics.Requests;
using SD.LLBLGen.Pro.ORMSupportClasses;
using SpreadsheetLight;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Obymobi.Web.Tests
{
    [TestFixture, Explicit]
    public class ManualReports
    {
        // private readonly string OUTPUT_DIR = @"G:\My Drive\Reports\DK";
        private string OUTPUT_DIR = @"D:\Temp\Reports";

        [TestCase(744, "TheFountain")]
        [Category("TestAutoReport")]
        public void Test(int companyId, string companyName)
        {
            var request = new TransactionsAppLessRequest
            {
                CompanyId = companyId,
                LeadTimeOffset = new TimeSpan(-1, -1, 0, 0),
                UntilDateTimeUtc = DateTime.UtcNow.Date.AddSeconds(-1).AddHours(-1),
                ShowProducts = true,
                ShowProductQuantity = true,
                ShowProductPrice = true,
                ShowAlterations = true,
                ShowSystemProducts = true,
                ShowOrderNotes = true,
                ShowCustomerInfo = true
            };

            var sut = new OrderReport(request, new OrderRepository(), new CompanyRepository());
            var result = sut.CreateReport(out var spreadsheet);

            Assert.True(result);

            SaveOutput(spreadsheet, request.UntilDateTimeUtc.Add(request.LeadTimeOffset), request.UntilDateTimeUtc, companyName, "");
        }

        [TestCase(744, "TheFountain")]
        [Category("TestAutoReport")]
        public void TestProductReport(int companyId, string companyName)
        {
            var request = new TransactionsAppLessRequest
            {
                CompanyId = companyId,
                LeadTimeOffset = new TimeSpan(-1, -1, 0, 0),
                UntilDateTimeUtc = DateTime.UtcNow.Date.AddSeconds(-1).AddHours(-1),
                ShowProducts = true,
                ShowProductQuantity = true,
                ShowProductPrice = true,
                ShowAlterations = true,
                ShowSystemProducts = true,
                ShowOrderNotes = true,
                ShowCustomerInfo = true
            };

            var sut = new OrderReport(request, new OrderRepository(), new CompanyRepository());
            var result = sut.CreateReport(out var spreadsheet);

            Assert.True(result);

            SaveOutput(spreadsheet, request.UntilDateTimeUtc.Add(request.LeadTimeOffset), request.UntilDateTimeUtc, companyName, "");
        }


        [TestCase(636, "TannersArms")]
        [TestCase(739, "MorningStar")]
        [TestCase(734, "ArmyNavy")]
        [TestCase(648, "OldTalbot")]
        [TestCase(751, "AnchorInn")]
        [TestCase(744, "TheFountain")]
        [TestCase(741, "Jolly")]
        [TestCase(743, "Tipsy")]
        [TestCase(740, "BarBroadway")]
        [TestCase(744, "TheFountain")]
        [Explicit("Only for manual activation")]
        public void FullReport_Weekly(int companyId, string companyName)
        {
            var request = new TransactionsAppLessRequest
            {
                CompanyId = companyId,
                //FromDateUtc = DateTime.UtcNow.Date.AddDays(-1).AddHours(-1),
                //UntilDateUtc = DateTime.UtcNow.Date.AddSeconds(-1).AddHours(-1),
                LeadTimeOffset = new TimeSpan(-15, 0, 0, 0),
                UntilDateTimeUtc = new DateTime(2020, 7, 15, 22, 59, 0),
                ShowProducts = true,
                SheetFilters = new List<SpreadSheetFilter>
                {
                    new TransactionsAppLessSheetFilter
                    {
                        Active = true,
                        Name = "Transactions"
                    }
                },
                ShowProductQuantity = true,
                ShowProductPrice = true,
                ShowAlterations = true,
                ShowSystemProducts = true,
                ShowOrderNotes = true,
                ShowCustomerInfo = true,
                ShowPaymentMethod = true,
                ShowDeliveryPoint = true
            };

            var sut = new OrderReport(request, new OrderRepository(), new CompanyRepository());
            var result = sut.CreateReport(out var spreadsheet);

            Assert.True(result);

            SaveOutput(spreadsheet, request.UntilDateTimeUtc.Add(request.LeadTimeOffset), request.UntilDateTimeUtc, companyName, "Bulk");
        }

        [Test]
        public void CustomerReport()
        {
            var request = new TransactionsAppLessRequest
            {
                CompanyId = 732,
                //FromDateUtc = DateTime.UtcNow.Date.AddDays(-1).AddHours(-1),
                //UntilDateUtc = DateTime.UtcNow.Date.AddSeconds(-1).AddHours(-1),
                LeadTimeOffset = new TimeSpan(-15, 0, 0, 0),
                UntilDateTimeUtc = new DateTime(2020, 7, 15, 22, 59, 0),
                ShowProducts = false,
                ShowProductQuantity = false,
                ShowProductPrice = false,
                ShowAlterations = false,
                ShowSystemProducts = false,
                ShowOrderNotes = false,
                ShowCustomerInfo = true
            };

            var sut = new OrderReport(request, new OrderRepository(), new CompanyRepository());
            var result = sut.CreateReport(out var spreadsheet);

            Assert.True(result);

            SaveOutput(spreadsheet, request.UntilDateTimeUtc.Add(request.LeadTimeOffset), request.UntilDateTimeUtc, "Lincoln", "Customers");
        }

        private void CreateDirectory(string fileName)
        {
            FileInfo fileInfo = new FileInfo(fileName);
            if (!(fileInfo?.Directory?.Exists ?? true))
            {
                fileInfo.Directory.Create();
            }
        }

        private void SaveOutput(SLDocument spreadsheet, DateTime fromDate, DateTime untilDate, string companyName, string reportName, string outputDir = null)
        {
            if (outputDir == null)
            {
                outputDir = OUTPUT_DIR;
            }

            string fileName = $"{outputDir}/{untilDate:yyyyMMdd}/{companyName}-{fromDate.AddDays(1):yyyyMMdd}-{untilDate:yyyyMMdd}-{reportName}.xlsx";
            CreateDirectory(fileName);
            spreadsheet.SaveAs(fileName);
        }

        [Test, TestCaseSource(nameof(DailyReports))]
        [Category("ManualReports")]
        public void CreateCombinedReport(ReportConfig config)
        {
            TransactionsAppLessRequest request = config.Build();
            OrderReport sut = new OrderReport(request, new OrderRepository(), new CompanyRepository());
            SLDocument mainReport = new SLDocument();

            foreach (TransactionsAppLessSheetFilter sheetFilter in request.SheetFilters)
            {
                sut.CreateReportManual(mainReport, sheetFilter);
            }
            mainReport.SelectWorksheet(request.SheetFilters.First().Name);

            SaveOutput(mainReport, config.FromDateUtc, config.UntilDateUtc, config.CompanyName, "Daily");

            Assert.True(true);
        }

        [Test, TestCaseSource(nameof(DailyReports_Test))]
        public void CreateCombinedReport_ManualTest(ReportConfig config)
        {
            TransactionsAppLessRequest request = config.Build();
            OrderReport sut = new OrderReport(request, new OrderRepository(), new CompanyRepository());
            SLDocument mainReport = new SLDocument();

            foreach (TransactionsAppLessSheetFilter sheetFilter in request.SheetFilters)
            {
                sut.CreateReportManual(mainReport, sheetFilter);
            }
            mainReport.SelectWorksheet(request.SheetFilters.First().Name);

            SaveOutput(mainReport, config.FromDateUtc, config.UntilDateUtc, config.CompanyName, "Manual", "D:\\Temp\\Reports\\");

            Assert.True(true);
        }

        private static List<int> GetCategoryIdsForSplit(int[] mainCategoryIds)
        {
            PredicateExpression filter = new PredicateExpression();
            filter.Add(CategoryFields.ParentCategoryId == mainCategoryIds);
            filter.AddWithOr(CategoryFields.CategoryId == mainCategoryIds);

            PrefetchPath prefetch = new PrefetchPath(EntityType.CategoryEntity);
            prefetch.Add(CategoryEntityBase.PrefetchPathChildCategoryCollection)
                .SubPath.Add(CategoryEntityBase.PrefetchPathChildCategoryCollection);

            CategoryCollection categoryCollection = new CategoryCollection();
            categoryCollection.GetMulti(filter, prefetch);

            List<int> categoryIds = new List<int>();
            categoryIds.AddRange(mainCategoryIds);
            foreach (CategoryEntity categoryEntity in categoryCollection)
            {
                if (categoryEntity.ChildCategoryCollection.Count == 0)
                {
                    categoryIds.Add(categoryEntity.CategoryId);
                }

                foreach (CategoryEntity entity2 in categoryEntity.ChildCategoryCollection)
                {
                    if (entity2.ChildCategoryCollection.Count == 0)
                    {
                        categoryIds.Add(entity2.CategoryId);
                    }

                    foreach (CategoryEntity entity3 in categoryEntity.ChildCategoryCollection)
                    {
                        if (entity3.ChildCategoryCollection.Count == 0)
                        {
                            categoryIds.Add(entity3.CategoryId);
                        }
                    }
                }
            }

            return categoryIds;
        }

        public enum ReportSubType
        {
            [StringValue("Full")]
            Full,
            [StringValue("Tipping")]
            Tipping,
            [StringValue("Split")]
            Split,
            [StringValue("EatOutToHelpOut")]
            EatOut,
        }

        public class ReportConfig
        {
            public ReportConfig(int companyId, string companyName)
            {
                FromDateUtc = DateTime.UtcNow.Date.AddDays(-1).AddHours(-1);
                UntilDateUtc = DateTime.UtcNow.Date.AddSeconds(-1).AddHours(-1);

                this.CompanyId = companyId;
                this.CompanyName = companyName;
            }

            public int CompanyId { get; set; }
            public string CompanyName { get; set; }
            public DateTime FromDateUtc { get; set; }
            public DateTime UntilDateUtc { get; set; }
            public TimeSpan LeadTimeOffset => FromDateUtc.Subtract(UntilDateUtc);
            public List<ReportSheet> Sheets { get; } = new List<ReportSheet>();

            public ReportConfig W(string name, ReportSubType type)
            {
                this.Sheets.Add(new ReportSheet(name, type));
                return this;
            }

            public ReportConfig W(string name, params int[] categoryIds)
            {
                this.Sheets.Add(new ReportSheet(name, ReportSubType.Split)
                {
                    CategoryIds = categoryIds
                });
                return this;
            }

            public TransactionsAppLessRequest Build()
            {
                int sortOrder = 1;

                return new TransactionsAppLessRequest
                {
                    CompanyId = this.CompanyId,
                    LeadTimeOffset = this.LeadTimeOffset,
                    UntilDateTimeUtc = this.UntilDateUtc,
                    SheetFilters = Sheets.Select(x => (SpreadSheetFilter)x.Build(sortOrder++)).ToList()
                };
            }
        }

        public class ReportSheet
        {
            public ReportSheet(string name, ReportSubType type)
            {
                this.Name = name;
                this.ReportType = type;
            }

            public string Name { get; }
            public ReportSubType ReportType { get; }
            public int[] CategoryIds { get; set; }

            public TransactionsAppLessSheetFilter Build(int sortOrder)
            {
                if (ReportType == ReportSubType.Full)
                {
                    return new TransactionsAppLessSheetFilter
                    {
                        Name = this.Name,
                        Active = true,
                        SortOrder = sortOrder,
                        ShowProducts = true,
                        ShowProductQuantity = true,
                        ShowProductPrice = true,
                        ShowAlterations = true,
                        ShowSystemProducts = true,
                        ShowOrderNotes = true,
                        ShowCustomerInfo = true
                    };
                }

                if (ReportType == ReportSubType.Tipping)
                {
                    return new TransactionsAppLessSheetFilter
                    {
                        Name = this.Name,
                        Active = true,
                        SortOrder = sortOrder,
                        ShowProducts = false,
                        ShowProductQuantity = false,
                        ShowProductPrice = true,
                        ShowAlterations = false,
                        ShowSystemProducts = true,
                        ShowOrderNotes = false,
                        ShowCustomerInfo = false,
                        IncludedSystemProductTypes = new List<int> { (int)OrderitemType.Tip }
                    };
                }

                if (ReportType == ReportSubType.Split)
                {
                    List<int> categoryIds = GetCategoryIdsForSplit(this.CategoryIds.Distinct().ToArray());
                    return new TransactionsAppLessSheetFilter
                    {
                        Name = this.Name,
                        Active = true,
                        ShowProducts = true,
                        ShowProductQuantity = true,
                        ShowProductPrice = true,
                        ShowAlterations = true,
                        ShowSystemProducts = false,
                        ShowOrderNotes = false,
                        ShowCustomerInfo = false,
                        CategoryFilterType = IncludeExcludeFilter.Include,
                        CategoryFilterIds = categoryIds,
                    };
                }

                if (ReportType == ReportSubType.EatOut)
                {
                    return new TransactionsAppLessSheetFilter
                    {
                        Name = this.Name,
                        Active = true,
                        SortOrder = sortOrder,
                        ShowProducts = false,
                        ShowProductQuantity = false,
                        ShowProductPrice = true,
                        ShowAlterations = true,
                        ShowSystemProducts = true,
                        ShowOrderNotes = false,
                        ShowCustomerInfo = true,
                        IncludedSystemProductTypes = new List<int> { (int)OrderitemType.EatOutToHelpOutDiscount }
                    };
                }

                return new TransactionsAppLessSheetFilter { Active = false };
            }
        }

        public static object[] DailyReports_Test =
        {
            /*new ReportConfig(743, "Tipsy")
                {
                    FromDateUtc = new DateTime(2020, 8, 2, 23, 0, 0),
                    UntilDateUtc = new DateTime(2020, 8, 9, 22, 59, 59),
                }
                .W("Full", ReportSubType.Full)
                .W("Tipping", ReportSubType.Tipping)
                .W("EatOutToHelpOut", ReportSubType.EatOut)
                .W("FoodMenu", 57582, 57584, 57585, 57586, 57587, 57588, 57589, 58397, 58398)
                .W("AlcoholicBeverages", 57733, 57746, 57744, 57745, 57739, 57740, 57747, 58050, 57736, 57734, 57737, 57738, 57735, 57741)
                .W("NonAlcoholicBeverages", 57742, 57743),
            new ReportConfig(744, "TheFountain")
                {
                    FromDateUtc = new DateTime(2020, 8, 2, 23, 0, 0),
                    UntilDateUtc = new DateTime(2020, 8, 9, 22, 59, 59),
                }
                .W("Full", ReportSubType.Full)
                .W("Tipping", ReportSubType.Tipping)
                .W("EatOutToHelpOut", ReportSubType.EatOut)
                .W("FoodMenu", 57898, 57901, 57899, 57900, 57902, 58960, 57976)
                .W("AlcoholicBeverages", 57970, 57971, 57972, 57974)
                .W("NonAlcoholicBeverages", 57975),*/
            
            new ReportConfig(758, "BombedOutChurch")
                .W("Full", ReportSubType.Full)
                .W("Tipping", ReportSubType.Tipping)
                .W("AlcoholicBeverages", 57876, 57877, 57878, 57879)
                .W("NonAlcoholicBeverages", 57880, 58177),
        };

        public static object[] DailyReports =
        {
            new ReportConfig(706, "TheEngine")
                .W("Full", ReportSubType.Full)
                .W("Tipping", ReportSubType.Tipping),
            new ReportConfig(707, "Bird&Bush")
                .W("Full", ReportSubType.Full)
                .W("Tipping", ReportSubType.Tipping),
            new ReportConfig(732, "Lincoln")
                .W("Full", ReportSubType.Full)
                .W("Tipping", ReportSubType.Tipping),
            new ReportConfig(748, "GoldenEagle")
                .W("Full", ReportSubType.Full)
                .W("Tipping", ReportSubType.Tipping),
            new ReportConfig(769, "SobarBirmingham")
                .W("Full", ReportSubType.Full)
                .W("Tipping", ReportSubType.Tipping),
            new ReportConfig(775, "CuriosityShop")
                .W("Full", ReportSubType.Full)
                .W("Tipping", ReportSubType.Tipping),
            new ReportConfig(785, "HomeLincoln")
                .W("Full", ReportSubType.Full)
                .W("Tipping", ReportSubType.Tipping),
            new ReportConfig(727, "ThePelican")
                .W("Full", ReportSubType.Full)
                .W("Tipping", ReportSubType.Tipping)
                .W("EatOutToHelpOut", ReportSubType.EatOut),

            new ReportConfig(759, "LucusLive")
                .W("Full", ReportSubType.Full)
                .W("Bar", 59477, 59478)
                .W("Thai", 59487)
                .W("Burger", 59526)
                .W("Coffee", 59521)
                .W("Wraps", 59937)
                .W("Cocktails", 61460),
            new ReportConfig(729, "LaRazaSS")
                .W("Full", ReportSubType.Full)
                .W("Tipping", ReportSubType.Tipping)
                .W("EatOutToHelpOut", ReportSubType.EatOut)
                .W("FoodMenu", 57185, 57186, 57187, 57188, 57189, 57190, 57191, 57192, 57193, 57194)
                .W("AlcoholicBeverages", 57195, 57253, 57447, 57205, 57749, 57750, 57430, 57751, 62437)
                .W("NonAlcoholicBeverages", 57255, 57198, 60301, 59501 , 59935 ,  59936),
            new ReportConfig(728, "TaBoucheSS")
                .W("Full", ReportSubType.Full)
                .W("Tipping", ReportSubType.Tipping)
                .W("EatOutToHelpOut", ReportSubType.EatOut)
                .W("FoodMenu", 56949, 56950, 56951, 56952, 56953, 60062, 60056, 60057, 60058, 60059, 60060, 60061)
                .W("AlcoholicBeverages", 57159, 57161, 57164, 57163, 57169, 57131, 57429, 57158)
                .W("NonAlcoholicBeverages", 57165, 57143, 58662, 58179, 60001,   59935),
            new ReportConfig(739, "MorningStar")
                .W("Full", ReportSubType.Full)
                .W("Tipping", ReportSubType.Tipping)
                .W("EatOutToHelpOut", ReportSubType.EatOut)
                .W("FoodMenu", 57334, 57335, 57336, 57337, 57338, 57339, 57340, 57341, 57386, 57385, 57381, 57382, 57634, 57528)
                .W("BeveragesMenu", 57333)
                .W("AlcoholicBeverages", 57409, 57413, 57414, 57415, 57416, 57418, 57419, 57421, 57422, 57423)
                .W("NonAlcoholicBeverages", 57424, 58761, 57636),
            new ReportConfig(758, "BombedOutChurch")
                .W("Full", ReportSubType.Full)
                .W("Tipping", ReportSubType.Tipping)
                .W("AlcoholicBeverages", 57876, 57877, 57878, 57879)
                .W("NonAlcoholicBeverages", 57880, 58177),
            new ReportConfig(718, "MilanSS")
                .W("Full", ReportSubType.Full)
                .W("Tipping", ReportSubType.Tipping)
                .W("EatOutToHelpOut", ReportSubType.EatOut)
                .W("FoodMenu", 57172, 57174, 57175,   57176 ,  58727,   61908,   59588 ,  60689,   61910 ,  61909 ,  61921, 62116)
                .W("BeveragesMenu", 57177)
                .W("AlcoholicBeverages", 57178 , 57179  , 57180 ,  57182  , 57183 ,  59587, 62115)
                .W("NonAlcoholicBeverages", 57181),
            new ReportConfig(742, "Tregenna")
                .W("Full", ReportSubType.Full)
                .W("Tipping", ReportSubType.Tipping)
                .W("EatOutToHelpOut", ReportSubType.EatOut)
                .W("FoodMenu", 58081, 58153, 57481, 59025 ,  59026,   59027)
                .W("BeveragesMenu", 58113)
                .W("AlcoholicBeverages", 58114, 58133, 58134, 58135, 58136, 58137, 58138, 58139, 58140, 58141, 58142, 57577, 58126, 58147, 58994)
                .W("NonAlcoholicBeverages", 58143, 58144, 58149, 58150),
            new ReportConfig(746, "Oddfellows")
                .W("Full", ReportSubType.Full)
                .W("Tipping", ReportSubType.Tipping)
                .W("FoodMenu", 57614, 57615, 57616, 57618, 57619, 57626, 57627, 57628)
                .W("AlcoholicBeverages", 57916, 57917, 57918, 57919, 57920, 57921, 57922, 57924, 58313, 58314, 58315, 58316, 57608, 57613, 57953, 57954, 57955, 57956, 57957, 57958, 57959, 57960, 57961)
                .W("NonAlcoholicBeverages", 57964, 57950, 57607, 57951, 57962),
            new ReportConfig(735, "MamaFeelgood")
                .W("Full", ReportSubType.Full)
                .W("Tipping", ReportSubType.Tipping)
                .W("EatOutToHelpOut", ReportSubType.EatOut)
                .W("FoodMenu", 57232, 57631, 57233, 57235, 57506)
                .W("AlcoholicBeverages", 57243, 57244)
                .W("NonAlcoholicBeverages", 57245, 57246, 57247, 57248, 57249),
            new ReportConfig(636, "TannersArms")
                .W("Full", ReportSubType.Full)
                .W("Tipping", ReportSubType.Tipping)
                .W("EatOutToHelpOut", ReportSubType.EatOut)
                .W("FoodMenu", 53629, 53825, 57535, 57541)
                .W("AlcoholicBeverages", 58797, 57547, 57548, 57549, 57550, 57551, 57552, 57553, 57555, 57675, 57568)
                .W("NonAlcoholicBeverages", 57554, 57574, 57573),
            new ReportConfig(717, "Courtyard")
                .W("Full", ReportSubType.Full)
                .W("Tipping", ReportSubType.Tipping)
                .W("EatOutToHelpOut", ReportSubType.EatOut)
                .W("FoodMenu", 58800, 56123, 56115, 56120, 56117, 56118, 56119, 56360, 56113, 59491, 56116, 58800, 56441, 58808, 59475)
                .W("AlcoholicBeverages", 56128, 56129, 56130, 56358, 56359, 58947, 56127)
                .W("NonAlcoholicBeverages", 56383, 57888, 56116),
            new ReportConfig(767, "Sub13")
                .W("Full", ReportSubType.Full)
                .W("Tipping", ReportSubType.Tipping)
                .W("EatOutToHelpOut", ReportSubType.EatOut)
                .W("FoodMenu", 58659)
                .W("AlcoholicBeverages", 58644, 58645, 58646, 58648, 58649, 58650, 58651, 58652, 58653, 58654, 58655, 58656, 58658)
                .W("NonAlcoholicBeverages", 58657, 58647),
            new ReportConfig(740, "BarBroadway")
                .W("Full", ReportSubType.Full)
                .W("Tipping", ReportSubType.Tipping)
                .W("AlcoholicBeverages", 57462, 57463, 57464, 57465, 57461)
                .W("NonAlcoholicBeverages", 57488, 57466, 59142),
            new ReportConfig(743, "Tipsy")
                .W("Full", ReportSubType.Full)
                .W("Tipping", ReportSubType.Tipping)
                .W("EatOutToHelpOut", ReportSubType.EatOut)
                .W("FoodMenu", 57582, 57584, 57585, 57586, 57587, 57588, 57589, 58397, 58398, 60719)
                .W("AlcoholicBeverages", 57733, 57746, 57744, 57745, 57739, 57740, 57747, 58050, 57736, 57734, 57737, 57738, 57735, 57741)
                .W("NonAlcoholicBeverages", 57742, 57743),
            new ReportConfig(744, "TheFountain")
                .W("Full", ReportSubType.Full)
                .W("Tipping", ReportSubType.Tipping)
                .W("EatOutToHelpOut", ReportSubType.EatOut)
                .W("FoodMenu", 57898, 57901, 57899, 57900, 57902, 58960, 57976)
                .W("AlcoholicBeverages", 57970, 57971, 57972, 57974)
                .W("NonAlcoholicBeverages", 57975),
            new ReportConfig(741, "Jolly")
                .W("Full", ReportSubType.Full)
                .W("Tipping", ReportSubType.Tipping)
                .W("FoodMenu", 57491, 57492, 57497, 57503, 57504)
                .W("AlcoholicBeverages", 57521, 57522, 57531, 57532, 57533, 57534, 57595, 57596, 57597, 57598)
                .W("NonAlcoholicBeverages", 57600, 57601, 57602, 57526),
            new ReportConfig(751, "AnchorInn")
                .W("Full", ReportSubType.Full)
                .W("Tipping", ReportSubType.Tipping)
                .W("EatOutToHelpOut", ReportSubType.EatOut)
                .W("FoodMenu", 57908, 57909, 57910, 57911, 58230, 57912, 57913, 57914)
                .W("AlcoholicBeverages", 57718, 57708, 57719)
                .W("NonAlcoholicBeverages", 57769, 58145),
            new ReportConfig(766, "BrinkburnSt")
                .W("Full", ReportSubType.Full)
                .W("Tipping", ReportSubType.Tipping)
                .W("EatOutToHelpOut", ReportSubType.EatOut)
                .W("FoodMenu", 58244, 58245, 58991, 58246, 58248, 58249, 58250, 58251, 58413, 58261, 58262, 58989, 58265, 58266, 58267, 58269, 58268, 60385, 60384, 59503, 59507, 59508, 59508, 59510, 59511, 59512, 60530, 60531, 60532, 62031)
                .W("AlcoholicBeverages", 58275, 58276, 58277, 58278, 58279, 58247, 58272, 58271, 58273, 58257, 58258, 58962, 58963, 58964, 58965, 58988, 59592, 59514, 59515, 59516, 59517, 59518, 59519, 59520, 60767, 60768, 60769, 60770, 60771, 60772, 60773, 60774, 60776, 60777, 61928, 61923)
                .W("NonAlcoholicBeverages", 59589, 60778, 59591, 59590),
            new ReportConfig(781, "TannersArmsSS")
                .W("Full", ReportSubType.Full)
                .W("Tipping", ReportSubType.Tipping)
                .W("EatOutToHelpOut", ReportSubType.EatOut)
                .W("FoodMenu", 59397)
                .W("AlcoholicBeverages", 59404, 59405, 59406, 59407, 59408, 59409, 59410, 59411, 59412, 59413, 59426, 59428, 59429, 59430)
                .W("NonAlcoholicBeverages", 59412, 59435, 59432),
            new ReportConfig(786, "CharSS")
                .W("Full", ReportSubType.Full)
                .W("Tipping", ReportSubType.Tipping)
                .W("EatOutToHelpOut", ReportSubType.EatOut)
                .W("FoodMenu", 60513, 60514, 60515, 60516, 60517, 60745, 60746, 60747)
                .W("AlcoholicBeverages", 60534, 60520, 60521, 60523, 60524)
                .W("NonAlcoholicBeverages", 60522, 60526),
            new ReportConfig(787, "BankSS")
                .W("Full", ReportSubType.Full)
                .W("Tipping", ReportSubType.Tipping)
                .W("EatOutToHelpOut", ReportSubType.EatOut)
                .W("FoodMenu", 60547, 60548, 60549, 60550, 60551, 60239, 62430)
                .W("AlcoholicBeverages", 60540, 60541, 60542, 60544, 60545, 62431)
                .W("NonAlcoholicBeverages", 60543, 60546)
        };
    }
}
