﻿using Moq;
using Obymobi.Data.CollectionClasses;
using Obymobi.Web.Analytics.Repositories;
using System.Collections.Generic;

namespace Obymobi.Web.Tests.Builders
{
    internal class OutletSellerInformationRepositoryMockBuilder
    {
        private readonly OutletSellerInformationCollection OutletSellerInformationCollection;

        public OutletSellerInformationRepositoryMockBuilder() : this(new OutletSellerInformationCollection())
        { }

        private OutletSellerInformationRepositoryMockBuilder(OutletSellerInformationCollection outletSellerInfoCollection) => OutletSellerInformationCollection = outletSellerInfoCollection;

        public IOutletSellerInformationRepository Build()
        {
            Mock<IOutletSellerInformationRepository> sellerInfoRepositoryMock = new Mock<IOutletSellerInformationRepository>();
            sellerInfoRepositoryMock.Setup(repository => repository.Get(It.IsAny<IEnumerable<int>>())).Returns(OutletSellerInformationCollection);
            return sellerInfoRepositoryMock.Object;
        }

        public OutletSellerInformationRepositoryMockBuilder W(OutletSellerInformationCollection sellerInfoCollection) 
            => new OutletSellerInformationRepositoryMockBuilder(sellerInfoCollection);
    }
}
