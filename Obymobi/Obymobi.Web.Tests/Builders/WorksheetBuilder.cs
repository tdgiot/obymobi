﻿using Obymobi.Web.Analytics.Reports.ReportBase;
using SpreadsheetLight;

namespace Obymobi.Web.Tests.Builders
{
    internal class WorksheetBuilder
    {
        private readonly string name;
        private readonly Column[] columns;

        public WorksheetBuilder() : this(new WorksheetNameBuilder().W(SLDocument.DefaultFirstSheetName).Build(), new ColumnsBuilder().Build())
        {

        }

        private WorksheetBuilder(string name, Column[] columns)
        {
            this.name = name;
            this.columns = columns;
        }

        public Worksheet Build()
        {
            return new Worksheet(name, columns);
        }

        public WorksheetBuilder W(WorksheetNameBuilder worksheetNameBuilder) => W(worksheetNameBuilder.Build());

        public WorksheetBuilder W(string worksheetName) => new WorksheetBuilder(worksheetName, this.columns);

        public WorksheetBuilder W(ColumnsBuilder columnsBuilder) => new WorksheetBuilder(this.name, columnsBuilder.Build());
    }
}
