﻿using Obymobi.Data.EntityClasses;

namespace Obymobi.Web.Tests.Builders
{
	internal class OrderitemAlterationitemTagEntityBuilder
	{
		public OrderitemAlterationitemTagEntityBuilder() : this(new OrderitemAlterationitemEntityBuilder().Build(), new TagEntityBuilder().Build())
		{
		}

		private OrderitemAlterationitemTagEntityBuilder(OrderitemAlterationitemEntity orderitemAlterationitemEntity, TagEntity tagEntity)
		{
			OrderitemAlterationitemEntity = orderitemAlterationitemEntity;
			TagEntity = tagEntity;
		}

		private OrderitemAlterationitemEntity OrderitemAlterationitemEntity { get; }
		private TagEntity TagEntity { get; }

		public OrderitemAlterationitemTagEntity Build() =>
			new OrderitemAlterationitemTagEntity
			{
				OrderitemAlterationitemEntity = OrderitemAlterationitemEntity,
				TagEntity = TagEntity,
				IsNew = false
			};

		public OrderitemAlterationitemTagEntityBuilder W(OrderitemAlterationitemEntityBuilder orderitemAlterationitemEntityBuilder) => W(orderitemAlterationitemEntityBuilder.Build());

		public OrderitemAlterationitemTagEntityBuilder W(OrderitemAlterationitemEntity orderitemAlterationitemEntity)
			=> new OrderitemAlterationitemTagEntityBuilder(orderitemAlterationitemEntity, TagEntity);

		public OrderitemAlterationitemTagEntityBuilder W(TagEntityBuilder tagEntityBuilder) => W(tagEntityBuilder.Build());

		public OrderitemAlterationitemTagEntityBuilder W(TagEntity tagEntity)
			=> new OrderitemAlterationitemTagEntityBuilder(OrderitemAlterationitemEntity, tagEntity);
	}
}
