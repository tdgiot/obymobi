﻿using Obymobi.Logic.Cms;
using Obymobi.Web.Analytics.Repositories;
using Obymobi.Web.Analytics.Repositories.TransactionsAppLess;

namespace Obymobi.Web.Tests.Builders
{
    internal class DatabaseClearerBuilder
    {
        public DatabaseClearerBuilder(IActionRepository actionRepository = null, IOutletSellerInformationRepository outletSellerInformationRepository = null, 
            IOutletRepository outletRepository = null, ICompanyRepository companyRepository = null, IPaymentIntegrationConfigurationRepository paymentIntegrationConfigurationRepository = null,
            IRoutestephandlerRepository routestephandlerRepository = null)
        {
            ActionRepository = actionRepository;
            OutletSellerInformationRepository = outletSellerInformationRepository;
            OutletRepository = outletRepository;
            CompanyRepository = companyRepository;
            PaymentIntegrationConfigurationRepository = paymentIntegrationConfigurationRepository;
            RoutestepHandlerRepository = routestephandlerRepository;
        }

        public DatabaseClearerBuilder W(IActionRepository actionRepository) => new DatabaseClearerBuilder(actionRepository);

        public DatabaseClearerBuilder W(IOutletSellerInformationRepository outletSellerInformationRepo, IOutletRepository outletRepo) 
            => new DatabaseClearerBuilder(outletSellerInformationRepository: outletSellerInformationRepo, outletRepository: outletRepo);

        public DatabaseClearerBuilder W(ICompanyRepository companyRepo) => new DatabaseClearerBuilder(companyRepository: companyRepo);

        public DatabaseClearerBuilder W(IPaymentIntegrationConfigurationRepository paymentIntegrationConfigurationRepo) 
            => new DatabaseClearerBuilder(paymentIntegrationConfigurationRepository: paymentIntegrationConfigurationRepo);

        public DatabaseClearerBuilder W(IRoutestephandlerRepository routestephandlerRepo)
            => new DatabaseClearerBuilder(routestephandlerRepository: routestephandlerRepo);

        public DatabaseClearer Build() =>
            new DatabaseClearer(
                ActionRepository,
                OutletRepository,
                OutletSellerInformationRepository,
                CompanyRepository,
                PaymentIntegrationConfigurationRepository,
                RoutestepHandlerRepository);

        private readonly IActionRepository ActionRepository;
        private readonly IOutletSellerInformationRepository OutletSellerInformationRepository;
        private readonly IOutletRepository OutletRepository;
        private readonly ICompanyRepository CompanyRepository;
        private readonly IPaymentIntegrationConfigurationRepository PaymentIntegrationConfigurationRepository;
        private readonly IRoutestephandlerRepository RoutestepHandlerRepository;
    }
}
