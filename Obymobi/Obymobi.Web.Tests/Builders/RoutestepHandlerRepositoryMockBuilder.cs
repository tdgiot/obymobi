﻿using Moq;
using Obymobi.Data.CollectionClasses;
using Obymobi.Web.Analytics.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Obymobi.Web.Tests.Builders
{
    internal class RoutestepHandlerRepositoryMockBuilder
    {
        private readonly RoutestephandlerCollection RoutestephandlerCollection;

        public RoutestepHandlerRepositoryMockBuilder() : this(new RoutestephandlerCollection())
        { }

        private RoutestepHandlerRepositoryMockBuilder(RoutestephandlerCollection routestephandlerCollection) => RoutestephandlerCollection = routestephandlerCollection;

        public IRoutestephandlerRepository Build()
        {
            Mock<IRoutestephandlerRepository> routestepHandlerRepositoryMock = new Mock<IRoutestephandlerRepository>();
            routestepHandlerRepositoryMock.Setup(repository => repository.Get(It.IsAny<IEnumerable<int>>())).Returns(RoutestephandlerCollection);
            return routestepHandlerRepositoryMock.Object;
        }

        public RoutestepHandlerRepositoryMockBuilder W(RoutestephandlerCollection routestephandlerCollection)
            => new RoutestepHandlerRepositoryMockBuilder(routestephandlerCollection);
    }
}
