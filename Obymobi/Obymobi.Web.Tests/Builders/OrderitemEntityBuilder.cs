﻿using System.Collections.Generic;
using AutoFixture;
using AutoFixture.Kernel;
using Obymobi.Data.EntityClasses;
using Obymobi.Enums;
using Obymobi.Web.Analytics.DomainTypes;

namespace Obymobi.Web.Tests.Builders
{
	internal class OrderitemEntityBuilder
	{
		public OrderitemEntityBuilder() : this(IgnoreVirtualMembersSpecimenBuilder.CreateBuilder())
		{
		}

		private OrderitemEntityBuilder(OrderItemEntityData orderItemEntityData) => Data = orderItemEntityData;

		private OrderitemEntityBuilder(ISpecimenBuilder specimenBuilder) : this(new OrderItemEntityData
		{
			ProductName = specimenBuilder.Create<string>().Substring(0, 20),
			Quantity = specimenBuilder.Create<int>(),
			PriceTotalInTax = specimenBuilder.Create<decimal>(),
			Type = OrderitemType.Product,
			OrderitemAlterationitemEntities = new[]
			{
				new OrderitemAlterationitemEntityBuilder().Build(), new OrderitemAlterationitemEntityBuilder().Build(), new OrderitemAlterationitemEntityBuilder().Build()
			},
			CategoryName = specimenBuilder.Create<string>().Substring(0, 10),
			TaxTariffId = specimenBuilder.Create<int?>(),
			PriceInTax = specimenBuilder.Create<decimal>(),
			ProductPriceIn = specimenBuilder.Create<decimal>()
		})
		{
		}

		private OrderItemEntityData Data { get; }

		public OrderitemEntity Build()
		{
			string productName = $"PN-[{(int) Data.Type}]-{Data.ProductName}";
			string categoryName = $"CN-{Data.CategoryName}";
			OrderitemEntity orderitemEntity = new OrderitemEntity
			{
				ProductName = productName,
				Quantity = Data.Quantity,
				PriceTotalInTax = Data.PriceTotalInTax,
				Type = Data.Type,
				CategoryPath = categoryName,
				CategoryId = Data.CategoryId,
				TaxTariffId = Data.TaxTariffId,
				PriceInTax = Data.PriceInTax,
				ProductPriceIn = Data.ProductPriceIn,
				IsNew = false
			};

			orderitemEntity.OrderitemAlterationitemCollection.AddRange(Data.OrderitemAlterationitemEntities);

			return orderitemEntity;
		}

		public OrderitemEntityBuilder W(OrderitemType type) => new OrderitemEntityBuilder(new OrderItemEntityData(Data) {Type = type});

		public OrderitemEntityBuilder W(params OrderitemAlterationitemEntity[] orderitemAlterationitemEntities) =>
			new OrderitemEntityBuilder(new OrderItemEntityData(Data) {OrderitemAlterationitemEntities = orderitemAlterationitemEntities});

		public OrderitemEntityBuilder W(int? taxTariffId) => new OrderitemEntityBuilder(new OrderItemEntityData(Data) {TaxTariffId = taxTariffId});

		public OrderitemEntityBuilder W(string productName) => new OrderitemEntityBuilder(new OrderItemEntityData(Data) {ProductName = productName});

		public OrderitemEntityBuilder W(decimal priceTotalInTax) => new OrderitemEntityBuilder(new OrderItemEntityData(Data) {PriceTotalInTax = priceTotalInTax});

		public OrderitemEntityBuilder W(int quantity) => new OrderitemEntityBuilder(new OrderItemEntityData(Data) {Quantity = quantity});

		public OrderitemEntityBuilder W(CategoryName categoryName) => new OrderitemEntityBuilder(new OrderItemEntityData(Data) {CategoryName = categoryName});

		public OrderitemEntityBuilder W(CategoryId categoryId) => new OrderitemEntityBuilder(new OrderItemEntityData(Data) {CategoryId = categoryId});

		private class OrderItemEntityData
		{
			internal OrderItemEntityData()
			{
			}

			internal OrderItemEntityData(OrderItemEntityData orderItemEntityData)
			{
				ProductName = orderItemEntityData.ProductName;
				Quantity = orderItemEntityData.Quantity;
				PriceTotalInTax = orderItemEntityData.PriceTotalInTax;
				Type = orderItemEntityData.Type;
				OrderitemAlterationitemEntities = orderItemEntityData.OrderitemAlterationitemEntities;
				CategoryName = orderItemEntityData.CategoryName;
				CategoryId = orderItemEntityData.CategoryId;
				TaxTariffId = orderItemEntityData.TaxTariffId;
				PriceInTax = orderItemEntityData.PriceInTax;
				ProductPriceIn = orderItemEntityData.ProductPriceIn;
			}

			public string ProductName { get; set; }
			public int Quantity { get; set; }
			public decimal PriceTotalInTax { get; set; }
			public OrderitemType Type { get; set; }
			public IEnumerable<OrderitemAlterationitemEntity> OrderitemAlterationitemEntities { get; set; }
			public int? CategoryId { get; set; }
			public string CategoryName { get; set; }
			public int? TaxTariffId { get; set; }
			public decimal PriceInTax { get; set; }
			public decimal ProductPriceIn { get; set; }
		}
	}
}
