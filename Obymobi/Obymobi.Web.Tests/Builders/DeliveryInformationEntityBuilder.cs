﻿using AutoFixture;
using AutoFixture.Kernel;
using Obymobi.Data.EntityClasses;

namespace Obymobi.Web.Tests.Builders
{
	internal class DeliveryInformationEntityBuilder
	{
		private string BuildingName { get; }
		private string Address { get; }
		private string ZipCode { get; }
		private string Instructions { get; }

		public DeliveryInformationEntityBuilder() : this(new Fixture())
		{
			
		}

		private DeliveryInformationEntityBuilder(ISpecimenBuilder specimenBuilder) : this(
			$"BN-{specimenBuilder.Create<string>().Substring(0, 25)}",
			$"Addr-{specimenBuilder.Create<string>().Substring(0, 25)}",
			$"ZC-{specimenBuilder.Create<string>().Substring(0, 10)}",
			$"Instr-{specimenBuilder.Create<string>().Substring(0, 30)}")
		{
		}

		private DeliveryInformationEntityBuilder(string buildingName, string address, string zipCode, string instructions)
		{
			BuildingName = buildingName;
			Address = address;
			ZipCode = zipCode;
			Instructions = instructions;
		}

		public DeliveryInformationEntity Build() => new DeliveryInformationEntity
		{
			BuildingName = BuildingName,
			Address = Address,
			Zipcode =ZipCode,
			Instructions = Instructions
		};
	}
}