﻿using AutoFixture;
using AutoFixture.Kernel;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;

namespace Obymobi.Web.Tests.Builders
{
	internal class OrderitemAlterationitemEntityBuilder
	{
		private OrderitemAlterationitemEntityBuilder(OrderitemAlterationitemEntityData orderitemAlterationitemEntityData) => Data = orderitemAlterationitemEntityData;

		public OrderitemAlterationitemEntityBuilder() : this(new Fixture())
		{
		}

		private OrderitemAlterationitemEntityBuilder(ISpecimenBuilder specimenBuilder) : this(new OrderitemAlterationitemEntityData
		{
			AlterationName = $"AN-{specimenBuilder.Create<string>().Substring(0, 20)}",
			AlterationOptionName = $"AON-{specimenBuilder.Create<string>().Substring(0, 20)}",
			PriceInTax = specimenBuilder.Create<decimal>(),
			PriceExTax = specimenBuilder.Create<decimal>(),
			TaxTariffId = specimenBuilder.Create<int?>(),
			PriceTotalExTax = specimenBuilder.Create<decimal>(),
			PriceTotalInTax = specimenBuilder.Create<decimal>(),
			AlterationoptionProductId = specimenBuilder.Create<int?>(),
			OrderitemAlterationitemTagCollection = new OrderitemAlterationitemTagCollection()
		})
		{
		}

		private OrderitemAlterationitemEntityData Data { get; }

		public OrderitemAlterationitemEntity Build()
		{
			OrderitemAlterationitemEntity orderitemAlterationitemEntity = new OrderitemAlterationitemEntity
			{
				AlterationName = Data.AlterationName,
				AlterationoptionName = Data.AlterationOptionName,
				PriceInTax = Data.PriceInTax,
				PriceExTax = Data.PriceExTax,
				PriceTotalInTax = Data.PriceTotalInTax,
				PriceTotalExTax = Data.PriceTotalExTax,
				TaxTariffId = Data.TaxTariffId,
				AlterationoptionProductId = Data.AlterationoptionProductId,
				IsNew = false
			};

			orderitemAlterationitemEntity.OrderitemAlterationitemTagCollection.AddRange(Data.OrderitemAlterationitemTagCollection);

			return orderitemAlterationitemEntity;
		}

		public OrderitemAlterationitemEntityBuilder W(string alterationName, string alterationOptionName) => new OrderitemAlterationitemEntityBuilder(new OrderitemAlterationitemEntityData(Data)
		{
			AlterationName = alterationName,
			AlterationOptionName = alterationOptionName
		});

		public OrderitemAlterationitemEntityBuilder W(decimal priceExTax, decimal priceInTax) => new OrderitemAlterationitemEntityBuilder(new OrderitemAlterationitemEntityData(Data)
		{
			PriceExTax = priceExTax,
			PriceInTax = priceInTax
		});

		public OrderitemAlterationitemEntityBuilder W(int? taxTariffId) => new OrderitemAlterationitemEntityBuilder(new OrderitemAlterationitemEntityData(Data) {TaxTariffId = taxTariffId});

		public OrderitemAlterationitemEntityBuilder W(params OrderitemAlterationitemTagEntity[] orderitemAlterationitemTagEntities) =>
			new OrderitemAlterationitemEntityBuilder(new OrderitemAlterationitemEntityData(Data)
			{
				OrderitemAlterationitemTagCollection = new OrderitemAlterationitemTagCollection(orderitemAlterationitemTagEntities)
			});

		public OrderitemAlterationitemEntityBuilder WithAlterationOptionProductId(int? id) =>
			new OrderitemAlterationitemEntityBuilder(new OrderitemAlterationitemEntityData(Data) {AlterationoptionProductId = id});

		private class OrderitemAlterationitemEntityData
		{
			public OrderitemAlterationitemEntityData()
			{
			}

			public OrderitemAlterationitemEntityData(OrderitemAlterationitemEntityData data)
			{
				AlterationName = data.AlterationName;
				AlterationOptionName = data.AlterationOptionName;
				PriceExTax = data.PriceExTax;
				PriceInTax = data.PriceInTax;
				TaxTariffId = data.TaxTariffId;
				OrderitemAlterationitemTagCollection = data.OrderitemAlterationitemTagCollection;
			}

			public string AlterationName { get; set; }
			public string AlterationOptionName { get; set; }
			public decimal PriceInTax { get; set; }
			public decimal PriceExTax { get; set; }
			public decimal PriceTotalInTax { get; set; }
			public decimal PriceTotalExTax { get; set; }
			public int? TaxTariffId { get; set; }
			public int? AlterationoptionProductId { get; set; }
			public OrderitemAlterationitemTagCollection OrderitemAlterationitemTagCollection { get; set; }
		}
	}
}
