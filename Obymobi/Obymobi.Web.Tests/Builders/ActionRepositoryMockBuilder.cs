﻿using Moq;
using Obymobi.Data.CollectionClasses;
using Obymobi.Web.Analytics.Repositories;
using System.Linq;

namespace Obymobi.Web.Tests.Builders
{
    internal class ActionRepositoryMockBuilder
    {
        private readonly ActionCollection ActionCollection;

        public ActionRepositoryMockBuilder() : this(new ActionCollection())
        { }

        private ActionRepositoryMockBuilder(ActionCollection actionCollection) => ActionCollection = actionCollection;

        public IActionRepository Build()
        {
            Mock<IActionRepository> actionRepositoryMock = new Mock<IActionRepository>();
            actionRepositoryMock.Setup(repository => repository.Get(It.IsAny<int>())).Returns(ActionCollection.FirstOrDefault());
            return actionRepositoryMock.Object;
        }

        public ActionRepositoryMockBuilder W(ActionCollection actionCollection) => new ActionRepositoryMockBuilder(actionCollection);
    }
}
