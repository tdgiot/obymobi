﻿using Obymobi.Web.Analytics.Reports.ReportBase;

namespace Obymobi.Web.Tests.Builders
{
    internal class ColumnsBuilder
    {
        private readonly Column[] columns;

        public ColumnsBuilder() : this(new[] { new ColumnBuilder().Build() })
        {

        }

        private ColumnsBuilder(Column[] columns) => this.columns = columns;

        public Column[] Build()
        {
            return columns;
        }

        public ColumnsBuilder W(ColumnBuilder columnBuilder) => new ColumnsBuilder(new[] { columnBuilder.Build() });

        public ColumnsBuilder W(Column[] columns) => new ColumnsBuilder(columns);
    }

}
