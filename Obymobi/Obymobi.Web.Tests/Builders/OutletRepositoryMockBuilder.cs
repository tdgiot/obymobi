﻿using Moq;
using Obymobi.Data.CollectionClasses;
using Obymobi.Web.Analytics.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Obymobi.Web.Tests.Builders
{
    internal class OutletRepositoryMockBuilder
    {
        private readonly OutletCollection OutletCollection;

        public OutletRepositoryMockBuilder() : this(new OutletCollection())
        { }

        private OutletRepositoryMockBuilder(OutletCollection outletCollection) => OutletCollection = outletCollection;

        public IOutletRepository Build()
        {
            Mock<IOutletRepository> outletRepositoryMock = new Mock<IOutletRepository>();
            outletRepositoryMock.Setup(repository => repository.GetOutletsByCompanyId(It.IsAny<IEnumerable<int>>())).Returns(OutletCollection);
            return outletRepositoryMock.Object;
        }

        public OutletRepositoryMockBuilder W(OutletCollection outletCollection)
            => new OutletRepositoryMockBuilder(outletCollection);
    }
}
