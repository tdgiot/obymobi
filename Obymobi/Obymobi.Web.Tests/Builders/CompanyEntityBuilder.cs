﻿using System;
using System.Globalization;
using Obymobi.Data.EntityClasses;

namespace Obymobi.Web.Tests.Builders
{
	internal class CompanyEntityBuilder
	{
		public CompanyEntityBuilder() : this($"CN-{Guid.NewGuid()}", new RegionInfo("en-GB"))
		{
			
		}

		private CompanyEntityBuilder(string name, RegionInfo regionInfo)
		{
			Name = name;
			RegionInfo = regionInfo;
		}

		private string Name { get; }

		private RegionInfo RegionInfo { get; } 

		public CompanyEntity Build() => new CompanyEntity
		{
			Name = Name,
			CurrencyCode = RegionInfo.ISOCurrencySymbol
		};
	}
}