﻿using AutoFixture;
using Obymobi.Data.EntityClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Obymobi.Web.Tests.Builders
{
    internal class RoutestephandlerEntityBuilder
    {
        private readonly RoutestephandlerData routestephandlerData;

        public RoutestephandlerEntityBuilder() : this(new RoutestephandlerData())
        {
        }

        public RoutestephandlerEntity Build() => new RoutestephandlerEntity(routestephandlerData.RoutestephandlerId)
        {
            FieldValue1 = routestephandlerData.FieldValue1,
            FieldValue2 = routestephandlerData.FieldValue2
        };

        private RoutestephandlerEntityBuilder(RoutestephandlerData routestephandlerData) => this.routestephandlerData = routestephandlerData;

        public RoutestephandlerEntityBuilder W() => new RoutestephandlerEntityBuilder(new RoutestephandlerData(routestephandlerData));

        private class RoutestephandlerData
        {
            internal int RoutestephandlerId { get; set; }
            internal string FieldValue1 { get; set; }
            internal string FieldValue2 { get; set; }

            public RoutestephandlerData()
            {
                Fixture fixture = new Fixture();

                RoutestephandlerId = fixture.Create<int>();
                FieldValue1 = fixture.Create<string>();
                FieldValue2 = fixture.Create<string>();
            }

            public RoutestephandlerData(RoutestephandlerData routestephandlerData)
            {
                RoutestephandlerId = routestephandlerData.RoutestephandlerId;
                FieldValue1 = routestephandlerData.FieldValue1;
                FieldValue2 = routestephandlerData.FieldValue2;
            }
        }
    }
}
