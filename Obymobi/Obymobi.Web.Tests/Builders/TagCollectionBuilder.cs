﻿using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;

namespace Obymobi.Web.Tests.Builders
{
	internal class TagCollectionBuilder
	{
		private readonly TagEntity[] tagEntities;

		public TagCollectionBuilder() : this(new TagEntity[0])
		{
			
		}

		private TagCollectionBuilder(TagEntity[] tagEntities) => this.tagEntities = tagEntities;

		public TagCollection Build() => new TagCollection(tagEntities);

		public TagCollectionBuilder W(TagEntity[] tags) => new TagCollectionBuilder(tags);

		public TagCollectionBuilder WithRandomEntities(int amountOfEntities)
		{
			TagEntity[] entities = new TagEntity[amountOfEntities];

			for (int i = 0; i < amountOfEntities; i++)
			{
				entities[i] = new TagEntityBuilder().Build();
			}

			return new TagCollectionBuilder(entities);
		}
	}
}
