﻿using AutoFixture;
using Obymobi.Data.EntityClasses;
using Obymobi.Enums;
using System;

namespace Obymobi.Web.Tests.Builders
{
    internal class ActionEntityBuilder
    {
        private readonly ActionData actionData;

        public ActionEntityBuilder() => actionData = new ActionData();

        public ActionEntity Build() => new ActionEntity(actionData.ActionId)
        {
            ActionId = actionData.ActionId,
            Resource = actionData.Resource
        };

        private ActionEntityBuilder(ActionData actionData) => this.actionData = actionData;

        public ActionEntityBuilder W() => new ActionEntityBuilder(new ActionData(actionData));

        private class ActionData
        {
            internal int ActionId { get; }
            internal string Resource { get; set; }

            public ActionData()
            {
                Fixture fixture = new Fixture();

                ActionId = fixture.Create<int>();
                Resource = fixture.Create<string>();
            }

            public ActionData(ActionData actionData)
            {
                ActionId = actionData.ActionId;
                Resource = actionData.Resource;
            }
        }
    }
}
