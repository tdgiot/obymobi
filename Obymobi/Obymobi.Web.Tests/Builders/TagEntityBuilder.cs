﻿using System.Collections.Generic;
using AutoFixture;
using Obymobi.Data.EntityClasses;

namespace Obymobi.Web.Tests.Builders
{
	internal class TagEntityBuilder
	{
		private readonly TagEntityData tagEntityData;

		public TagEntityBuilder() : this(BuildRandomTagEntityData())
		{
		}

		private TagEntityBuilder(TagEntityData tagEntityData) => this.tagEntityData = tagEntityData;

		private static TagEntityData BuildRandomTagEntityData()
		{
			Fixture fixture = new Fixture();
			fixture.Customizations.Add(new ElementsBuilder<string>(TagNames.Values));
			return new TagEntityData
			{
				TagId = fixture.Create<int>(),
				TagName = fixture.Create<string>(),
				CompanyId = fixture.Create<int?>()
			};
		}

		public TagEntity Build()
		{
			TagEntity tagEntity = new TagEntity
			{
				Name = tagEntityData.TagName,
				CompanyId = tagEntityData.CompanyId,
				TagId = tagEntityData.TagId,
				IsNew = false
			};
			return tagEntity;
		}

		public TagEntityBuilder W(int tagId) => new TagEntityBuilder(new TagEntityData(tagEntityData) { TagId = tagId});
		public TagEntityBuilder W(string tagName) => new TagEntityBuilder(new TagEntityData(tagEntityData) { TagName = tagName});
		public TagEntityBuilder W(int? companyId) => new TagEntityBuilder(new TagEntityData(tagEntityData) { CompanyId = companyId});

		public static class TagNames
		{
			public static IEnumerable<string> Values = new[]
			{
				"beer",
				"soda",
				"alcohol",
				"non-alcoholic",
				"spirits",
				"vodka",
				"whisky",
				"rum",
				"gin",
				"cocktails",
				"shots",
				"liqueurs",
				"cider",
				"ale",
				"lager",
				"wine",
				"champagne",
				"soft-drinks",
				"vegan",
				"vegetarian",
				"dairy",
				"gluten",
				"breakfast",
				"lunch",
				"dinner",
				"appetisers",
				"starters",
				"dine-in",
				"takeaway",
				"in-room-dining",
				"restaurant",
				"desserts",
				"pizza",
				"burger",
				"salad",
				"soup",
				"sandwiches",
				"steak",
				"fish",
				"kids",
				"water",
				"spa",
				"sides",
				"specials",
				"nuts",
				"shellfish",
				"downstairs",
				"upstairs"
			};
		}

		private protected class TagEntityData
		{
			internal int TagId {get; set; }
			internal string TagName {get; set; }
			internal int? CompanyId {get; set; }

			internal TagEntityData()
			{
				
			}

			internal TagEntityData(TagEntityData tagEntityData)
			{
				TagId = tagEntityData.TagId;
				TagName = tagEntityData.TagName;
				CompanyId = tagEntityData.CompanyId;
			}
		}
	}
}
