﻿using Obymobi.Web.Analytics.Reports.ReportBase;

namespace Obymobi.Web.Tests.Builders
{
    internal class ColumnBuilder
    {
        private readonly double? width;

        public ColumnBuilder()
        {

        }

        private ColumnBuilder(double? width) => this.width = width;

        public Column Build()
        {
            return this.width == null ? new Column() : new Column(width.Value);
        }

        public ColumnBuilder W(double columnWidth) => new ColumnBuilder(columnWidth);
    }
}
