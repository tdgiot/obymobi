﻿using Moq;
using Obymobi.Data.CollectionClasses;
using Obymobi.Web.Analytics.Repositories;

namespace Obymobi.Web.Tests.Builders
{
    internal class PaymentIntegrationConfigurationRepositoryMockBuilder
    {
        private readonly PaymentIntegrationConfigurationCollection PaymentIntegrationConfigurationCollection;

        public PaymentIntegrationConfigurationRepositoryMockBuilder() : this(new PaymentIntegrationConfigurationCollection())
        { }

        private PaymentIntegrationConfigurationRepositoryMockBuilder(PaymentIntegrationConfigurationCollection paymentIntegrationConfigurationCollection) 
            => PaymentIntegrationConfigurationCollection = paymentIntegrationConfigurationCollection;

        public IPaymentIntegrationConfigurationRepository Build()
        {
            Mock<IPaymentIntegrationConfigurationRepository> paymentIntegrationRepoMock = new Mock<IPaymentIntegrationConfigurationRepository>();
            paymentIntegrationRepoMock.Setup(repository => repository.GetByAdyenMerchantCode(It.IsAny<string>())).Returns(PaymentIntegrationConfigurationCollection);
            return paymentIntegrationRepoMock.Object;
        }

        public PaymentIntegrationConfigurationRepositoryMockBuilder W(PaymentIntegrationConfigurationCollection paymentIntegrationConfigurationCollection)
            => new PaymentIntegrationConfigurationRepositoryMockBuilder(paymentIntegrationConfigurationCollection);
    }
}
