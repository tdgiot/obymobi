﻿using AutoFixture;
using Obymobi.Data.EntityClasses;
using System.Collections.Generic;

namespace Obymobi.Web.Tests.Builders
{
    internal class OutletSellerInformationEntityBuilder
    {
        private readonly OutletSellerInformationData outletSellerInfoData;

        public OutletSellerInformationEntityBuilder() => outletSellerInfoData = new OutletSellerInformationData();

        public OutletSellerInformationEntity Build()
        {
            OutletSellerInformationEntity entity = new OutletSellerInformationEntity(outletSellerInfoData.OutletSellerInformationId)
            {
                ReceiptReceiversEmail = outletSellerInfoData.ReceiptReceiversEmail,
            };

            entity.OutletCollection.AddRange(outletSellerInfoData.OutletEntities);

            return entity;
        }

        private OutletSellerInformationEntityBuilder(OutletSellerInformationData outletSellerInfoData) => this.outletSellerInfoData = outletSellerInfoData;

        public OutletSellerInformationEntityBuilder W() => new OutletSellerInformationEntityBuilder(new OutletSellerInformationData(outletSellerInfoData));

        private class OutletSellerInformationData
        {
            internal int OutletSellerInformationId { get; }
            internal IEnumerable<OutletEntity> OutletEntities { get; set; }
            internal string ReceiptReceiversEmail { get; }

            public OutletSellerInformationData()
            {
                Fixture fixture = new Fixture();

                OutletSellerInformationId = fixture.Create<int>();
                OutletEntities = new[]
                {
                    new OutletEntityBuilder().Build(),
                    new OutletEntityBuilder().Build(),
                    new OutletEntityBuilder().Build()
                };
                ReceiptReceiversEmail = fixture.Create<string>();
            }

            public OutletSellerInformationData(OutletSellerInformationData outletSellerInfoData)
            {
                OutletSellerInformationId = outletSellerInfoData.OutletSellerInformationId;
                OutletEntities = outletSellerInfoData.OutletEntities;
                ReceiptReceiversEmail = outletSellerInfoData.ReceiptReceiversEmail;
            }
        }
    }
}
