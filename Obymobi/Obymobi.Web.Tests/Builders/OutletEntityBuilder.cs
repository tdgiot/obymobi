﻿using AutoFixture;
using Obymobi.Data.EntityClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Obymobi.Web.Tests.Builders
{
    internal class OutletEntityBuilder
    {
        private readonly OutletData outletData;

        public OutletEntityBuilder() => outletData = new OutletData();

        public OutletEntity Build() => new OutletEntity(outletData.OutletId)
        {
            CompanyId = outletData.CompanyId,
            OutletSellerInformationId = outletData.OutletSellerInformationId
        };

        private OutletEntityBuilder(OutletData outletData) => this.outletData = outletData;

        public OutletEntityBuilder W() => new OutletEntityBuilder(new OutletData(outletData));

        private class OutletData
        {
            internal int OutletId { get; }
            internal int CompanyId { get; }
            internal int OutletSellerInformationId { get; }

            public OutletData()
            {
                Fixture fixture = new Fixture();

                OutletId = fixture.Create<int>();
                CompanyId = fixture.Create<int>();
                OutletSellerInformationId = fixture.Create<int>();
            }

            public OutletData(OutletData outletData)
            {
                OutletId = outletData.OutletId;
                CompanyId = outletData.CompanyId;
                OutletSellerInformationId = outletData.OutletSellerInformationId;
            }
        }
    }
}
