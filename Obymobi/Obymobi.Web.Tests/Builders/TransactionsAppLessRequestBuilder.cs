﻿using System;
using System.Collections.Generic;
using System.Linq;
using Obymobi.Web.Analytics.DomainTypes;
using Obymobi.Web.Analytics.Model.ModelBase;
using Obymobi.Web.Analytics.Requests;

namespace Obymobi.Web.Tests.Builders
{
	internal class TransactionsAppLessRequestBuilder
	{
		public TransactionsAppLessRequestBuilder() : this(new RequestData())
		{
		}

		private TransactionsAppLessRequestBuilder(RequestData requestData) => Data = requestData;
		private RequestData Data { get; }

		public TransactionsAppLessRequestBuilder W(params TransactionsAppLessSheetFilterBuilder[] sheetFilterBuilder) => new TransactionsAppLessRequestBuilder(new RequestData(Data)
		{
			Sheets = sheetFilterBuilder.ToList()
		});

		public TransactionsAppLessRequest Build() => new TransactionsAppLessRequest
		{
			CompanyId = Data.CompanyId,
			CompanyTimeZone = Data.CompanyTimeZone,
			LeadTimeOffset = ((DateTime) Data.FromDate).Subtract(((DateTime) Data.UntilDate).Date),
			UntilDateTimeUtc = Data.UntilDate,
			SheetFilters = Data.Sheets.Select(builder => (SpreadSheetFilter)builder.Build()).ToList()
		};

		private class RequestData
		{
			public RequestData()
			{
				CompanyId = 550;
				CompanyTimeZone = string.Empty;
				FromDate = DateTime.UtcNow;
				UntilDate = DateTime.UtcNow;
				Sheets = new List<TransactionsAppLessSheetFilterBuilder>
				{
					new TransactionsAppLessSheetFilterBuilder()
				};
			}

			public RequestData(RequestData old)
			{
				CompanyId = old.CompanyId;
				CompanyTimeZone = old.CompanyTimeZone;
				FromDate = old.FromDate;
				UntilDate = old.UntilDate;
			}

			public int CompanyId { get; }
			public string CompanyTimeZone { get; }
			public FromDateTime FromDate { get; }
			public UntilDateTime UntilDate { get; }
			public IEnumerable<TransactionsAppLessSheetFilterBuilder> Sheets { get; set; } = new List<TransactionsAppLessSheetFilterBuilder>();
		}
	}
}
