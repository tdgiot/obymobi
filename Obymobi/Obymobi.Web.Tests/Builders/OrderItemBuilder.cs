﻿using AutoFixture;
using Obymobi.Enums;
using Obymobi.Web.Analytics.Model.Orders;

namespace Obymobi.Web.Tests.Builders
{
	internal class OrderItemBuilder
	{
		private readonly OrderItemData orderItemData;

		public OrderItemBuilder() => orderItemData = new OrderItemData();

		private OrderItemBuilder(OrderItemData orderItemData) => this.orderItemData = orderItemData;

		public OrderItemSale Build() =>
			new OrderItemSale(
				orderItemData.Name,
				orderItemData.ExternalIdentifier,
				orderItemData.Quantity,
				orderItemData.Price,
				orderItemData.Type,
				orderItemData.CategoryName,
				orderItemData.OrderItemAlterationItems);

		public OrderItemBuilder W(int quantity) => new OrderItemBuilder(new OrderItemData(orderItemData) {Quantity = quantity});

		public OrderItemBuilder W(decimal price) => new OrderItemBuilder(new OrderItemData(orderItemData) {Price = price});

		private class OrderItemData
		{
			public OrderItemData()
			{
				var fixture = new Fixture();

				Name = $"N-{fixture.Create<string>()}";
				ExternalIdentifier = $"EXT-{fixture.Create<string>()}";
				Quantity = fixture.Create<int>();
				Price = fixture.Create<decimal>();
				Type = fixture.Create<OrderitemType>();
				CategoryName = $"CN-{fixture.Create<string>()}";
				OrderItemAlterationItems = new OrderItemAlterationItem[0];
			}

			public OrderItemData(OrderItemData orderItemData)
			{
				Name = orderItemData.Name;
                ExternalIdentifier = orderItemData.ExternalIdentifier;
				Quantity = orderItemData.Quantity;
				Price = orderItemData.Price;
				Type = orderItemData.Type;
				CategoryName = orderItemData.CategoryName;
				OrderItemAlterationItems = orderItemData.OrderItemAlterationItems;
			}

			public string Name { get; }
			public string ExternalIdentifier { get; }
			public int Quantity { get; internal set; }
			public decimal Price { get; internal set; }
			public OrderitemType Type { get; }
			public string CategoryName { get; }
			public OrderItemAlterationItem[] OrderItemAlterationItems { get; }
		}
	}
}
