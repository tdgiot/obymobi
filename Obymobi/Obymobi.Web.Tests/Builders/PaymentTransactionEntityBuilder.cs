using Obymobi.Data.EntityClasses;
using Obymobi.Enums;

namespace Obymobi.Web.Tests.Builders
{
    public class PaymentTransactionEntityBuilder
    {
        public PaymentTransactionEntityBuilder() : this(new MockData()) { }

        private PaymentTransactionEntityBuilder(MockData data) { Data = data; }

        private MockData Data { get; }

        public PaymentTransactionEntityBuilder W(int id) => new PaymentTransactionEntityBuilder(new MockData(Data) { Id = id });
        public PaymentTransactionEntityBuilder W(PaymentTransactionStatus status) => new PaymentTransactionEntityBuilder(new MockData(Data) { Status = status });
        public PaymentTransactionEntityBuilder W(string paymentMethod) => new PaymentTransactionEntityBuilder(new MockData(Data) { PaymentMethod = paymentMethod });

        public PaymentTransactionEntity Build() => new PaymentTransactionEntity
        {
            PaymentTransactionId = Data.Id,
            PaymentMethod = Data.PaymentMethod,
            Status = Data.Status,
            IsNew = false
        };

        private class MockData
        {
            // Default values
            public MockData()
            {
                Id = 1;
                Status = PaymentTransactionStatus.Paid;
                PaymentMethod = "Credit card";
            }

            public MockData(MockData data)
            {
                Id = data.Id;
                Status = data.Status;
                PaymentMethod = data.PaymentMethod;
            }

            public int Id { get; set; }
            public PaymentTransactionStatus Status { get; set; }
            public string PaymentMethod { get; set; }
        }
    }
}