﻿using System.Collections.Generic;
using Moq;
using Obymobi.Data.CollectionClasses;
using Obymobi.Web.Analytics.Repositories;

namespace Obymobi.Web.Tests.Builders
{
	internal class TagRepositoryMockBuilder
	{
		private readonly TagCollection tagCollection;

		public TagRepositoryMockBuilder() : this(new TagCollectionBuilder().WithRandomEntities(3).Build())
		{
			
		}

		private TagRepositoryMockBuilder(TagCollection tagCollection) => this.tagCollection = tagCollection;

		public ITagRepository Build()
		{
			Mock<ITagRepository> tagRepositoryMock = new Mock<ITagRepository>();
			tagRepositoryMock.Setup(repository => repository.GetByCompanyId(It.IsAny<int>())).Returns(tagCollection);
			tagRepositoryMock.Setup(repository => repository.GetTags(It.IsAny<IEnumerable<int>>())).Returns(tagCollection);
			return tagRepositoryMock.Object;
		}

		public TagRepositoryMockBuilder W(TagCollectionBuilder tagCollectionBuilder) => new TagRepositoryMockBuilder(tagCollectionBuilder.Build());
	}
}
