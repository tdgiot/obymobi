﻿namespace Obymobi.Web.Tests.Builders
{
    internal class WorksheetNameBuilder
    {
        private readonly string sheetName;

        public WorksheetNameBuilder() : this("Lang may yer lum reek")
        {

        }

        private WorksheetNameBuilder(string sheetName) => this.sheetName = sheetName;

        public string Build()
        {
            return sheetName;
        }

        public WorksheetNameBuilder W(string name) => new WorksheetNameBuilder(name);
    }
}
