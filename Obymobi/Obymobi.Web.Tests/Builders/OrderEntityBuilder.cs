using AutoFixture;
using AutoFixture.Kernel;
using Obymobi.Data.EntityClasses;
using Obymobi.Web.Analytics.DomainTypes;
using System;
using System.Collections.Generic;

namespace Obymobi.Web.Tests.Builders
{
    internal class OrderEntityBuilder
    {
        private OrderEntityData Data { get; }

        private class OrderEntityData
        {
            internal OrderEntityData()
            {

            }

            internal OrderEntityData(OrderEntityData data)
            {
                OrderId = data.OrderId;
                CompanyId = data.CompanyId;
                CreatedUtc = data.CreatedUtc;
                ServiceMethodName = data.ServiceMethodName;
                CheckoutMethodName = data.CheckoutMethodName;
                Total = data.Total;
                CustomerPhoneNumber = data.CustomerPhoneNumber;
                CustomerLastName = data.CustomerLastName;
                Email = data.Email;
                Notes = data.Notes;
                DeliveryInformationEntity = data.DeliveryInformationEntity;
                OrderitemEntities = data.OrderitemEntities;
                MasterOrderId = data.MasterOrderId;
                ServiceMethodId = data.ServiceMethodId;
                CheckoutMethodId = data.CheckoutMethodId;
                OutletId = data.OutletId;
                PaymentTransactionEntities = data.PaymentTransactionEntities;
                DeliverypointName = data.DeliverypointName;
            }

            internal int? OutletId { get; set; }
            internal int? MasterOrderId { get; set; }
            internal int OrderId { get; set; }
            internal int CompanyId { get; set; }
            internal DateTime? CreatedUtc { get; set; }
            internal string ServiceMethodName { get; set; }
            internal string CheckoutMethodName { get; set; }
            internal decimal Total { get; set; }
            internal string CustomerPhoneNumber { get; set; }
            internal string CustomerLastName { get; set; }
            internal string Email { get; set; }
            internal string Notes { get; set; }
            internal DeliveryInformationEntity DeliveryInformationEntity { get; set; }
            internal IEnumerable<OrderitemEntity> OrderitemEntities { get; set; }
            internal int ServiceMethodId { get; set; }
            internal int CheckoutMethodId { get; set; }
            internal IEnumerable<PaymentTransactionEntity> PaymentTransactionEntities { get; set; }
            internal string DeliverypointName { get; set; }
        }

        public OrderEntityBuilder() : this(IgnoreVirtualMembersSpecimenBuilder.CreateBuilder())
        {

        }

        private OrderEntityBuilder(OrderEntityData orderEntityData) => Data = orderEntityData;

        private OrderEntityBuilder(ISpecimenBuilder specimenBuilder) : this(new OrderEntityData()
        {
            OrderId = specimenBuilder.Create<int>(),
            CompanyId = specimenBuilder.Create<int>(),
            CreatedUtc = DateTime.UtcNow,
            ServiceMethodName = $"SMN-{specimenBuilder.Create<string>().Substring(0, 3)}",
            CheckoutMethodName = $"CMN-{specimenBuilder.Create<string>().Substring(0, 3)}",
            Total = (decimal)12.10,
            CustomerPhoneNumber = $"CPN-{specimenBuilder.Create<string>().Substring(0, 3)}",
            CustomerLastName = $"CLN-{specimenBuilder.Create<string>().Substring(0, 20)}",
            Email = $"Email-{specimenBuilder.Create<string>().Substring(0, 30)}",
            Notes = $"Notes-{specimenBuilder.Create<string>().Substring(0, 30)}",
            DeliverypointName = $"DPN-{specimenBuilder.Create<string>().Substring(0, 30)}",
            DeliveryInformationEntity = new DeliveryInformationEntityBuilder().Build(),
            OrderitemEntities = new[]
            {
                new OrderitemEntityBuilder().Build(),
                new OrderitemEntityBuilder().Build(),
                new OrderitemEntityBuilder().Build()
            },
            PaymentTransactionEntities = new[]
            {
                new PaymentTransactionEntityBuilder().Build()
            }
        })
        {

        }

        public OrderEntity Build()
        {
            var orderEntity = new OrderEntity
            {
                OrderId = Data.OrderId,
                CompanyId = Data.CompanyId,
                CreatedUTC = Data.CreatedUtc,
                ServiceMethodName = Data.ServiceMethodName,
                CheckoutMethodName = Data.CheckoutMethodName,
                Total = Data.Total,
                CustomerPhonenumber = Data.CustomerPhoneNumber,
                CustomerLastname = Data.CustomerLastName,
                Email = Data.Email,
                Notes = Data.Notes,
                DeliveryInformationEntity = Data.DeliveryInformationEntity,
                MasterOrderId = Data.MasterOrderId,
                ServiceMethodId = Data.ServiceMethodId,
                CheckoutMethodId = Data.CheckoutMethodId,
                OutletId = Data.OutletId,
                DeliverypointName = Data.DeliverypointName,
                IsNew = false
            };

            orderEntity.OrderitemCollection.AddRange(Data.OrderitemEntities);
            orderEntity.PaymentTransactionCollection.AddRange(Data.PaymentTransactionEntities);

            return orderEntity;
        }

        public OrderEntityBuilder W(params OrderitemEntity[] orderitemEntities) => new OrderEntityBuilder(new OrderEntityData(Data) { OrderitemEntities = orderitemEntities });

        public OrderEntityBuilder W(decimal totalPrice) => new OrderEntityBuilder(new OrderEntityData(Data) { Total = totalPrice });

        public OrderEntityBuilder W(int orderId) => new OrderEntityBuilder(new OrderEntityData(Data) { OrderId = orderId });

        public OrderEntityBuilder W(int? masterOrderId) => new OrderEntityBuilder(new OrderEntityData(Data) { MasterOrderId = masterOrderId });

        public OrderEntityBuilder W(OutletId outletId) => new OrderEntityBuilder(new OrderEntityData(Data) { OutletId = outletId });

        public OrderEntityBuilder W(ServiceMethodId serviceMethodId) => new OrderEntityBuilder(new OrderEntityData(Data) { ServiceMethodId = serviceMethodId });

        public OrderEntityBuilder W(ServiceMethodName serviceMethodName) => new OrderEntityBuilder(new OrderEntityData(Data) { ServiceMethodName = serviceMethodName });

        public OrderEntityBuilder W(CheckoutMethodId checkoutMethodId) => new OrderEntityBuilder(new OrderEntityData(Data) { CheckoutMethodId = checkoutMethodId });

        public OrderEntityBuilder W(CheckoutMethodName checkoutMethodName) => new OrderEntityBuilder(new OrderEntityData(Data) { CheckoutMethodName = checkoutMethodName });

        public OrderEntityBuilder W(DeliverypointName deliverypointName) => new OrderEntityBuilder(new OrderEntityData(Data) { DeliverypointName = deliverypointName });

        public OrderEntityBuilder W(params PaymentTransactionEntity[] paymentTransactionEntities) => new OrderEntityBuilder(new OrderEntityData(Data) { PaymentTransactionEntities = paymentTransactionEntities });
    }
}