﻿using Moq;
using Obymobi.Data.EntityClasses;
using Obymobi.Web.Analytics.Repositories.TransactionsAppLess;

namespace Obymobi.Web.Tests.Builders
{
	internal class CompanyRepositoryMockBuilder
	{
		public CompanyRepositoryMockBuilder() : this(new CompanyEntityBuilder().Build())
		{
			
		}

		private CompanyRepositoryMockBuilder(CompanyEntity companyEntity) => CompanyEntity = companyEntity;

		private CompanyEntity CompanyEntity { get; }

		public ICompanyRepository Build()
		{
			var companyRepositoryMock = new Mock<ICompanyRepository>();
			_ = companyRepositoryMock.Setup(repository => repository.Get(It.IsAny<int>())).Returns(new[]
			{
				CompanyEntity
			});
			return companyRepositoryMock.Object;
		}

		public CompanyRepositoryMockBuilder W(CompanyEntity companyEntity) => new CompanyRepositoryMockBuilder(companyEntity);
	}
}