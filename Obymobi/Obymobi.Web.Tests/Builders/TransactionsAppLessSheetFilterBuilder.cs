using Obymobi.Enums;
using Obymobi.Web.Analytics.DomainTypes;
using Obymobi.Web.Analytics.Model.TransactionsAppLess;
using System.Linq;

namespace Obymobi.Web.Tests.Builders
{
    internal class TransactionsAppLessSheetFilterBuilder
    {
        private RequestData Data { get; }

        public TransactionsAppLessSheetFilterBuilder() : this(new TransactionsAppLessSheetFilter())
        {

        }

        private TransactionsAppLessSheetFilterBuilder(TransactionsAppLessSheetFilter defaultRequest) : this(new RequestData(defaultRequest))
        {

        }

        private TransactionsAppLessSheetFilterBuilder(RequestData requestData) => Data = requestData;

        public TransactionsAppLessSheetFilterBuilder W(string name, bool active) => new TransactionsAppLessSheetFilterBuilder(new RequestData(Data)
        {
            Name = name,
            Active = active
        });

        public TransactionsAppLessSheetFilterBuilder W(ShowProductsOption showProductsOption) => new TransactionsAppLessSheetFilterBuilder(new RequestData(Data)
        {
            ShowProducts = showProductsOption
        });

        public TransactionsAppLessSheetFilterBuilder W(ShowSystemProductsOption showSystemProductsOption) => new TransactionsAppLessSheetFilterBuilder(new RequestData(Data)
        {
            ShowSystemProducts = showSystemProductsOption
        });

        public TransactionsAppLessSheetFilterBuilder W(ShowProductQuantityOption showProductQuantity) => new TransactionsAppLessSheetFilterBuilder(new RequestData(Data)
        {
            ShowProductQuantity = showProductQuantity
        });

        public TransactionsAppLessSheetFilterBuilder W(ShowProductPriceOption showProductPrice) => new TransactionsAppLessSheetFilterBuilder(new RequestData(Data)
        {
            ShowProductPrice = showProductPrice
        });

        public TransactionsAppLessSheetFilterBuilder W(ShowAlterationsOption showAlterations) => new TransactionsAppLessSheetFilterBuilder(new RequestData(Data)
        {
            ShowAlterations = showAlterations
        });

        public TransactionsAppLessSheetFilterBuilder W(ShowServiceMethod showServiceMethod) => new TransactionsAppLessSheetFilterBuilder(new RequestData(Data)
        {
            ShowServiceMethod = showServiceMethod
        });

        public TransactionsAppLessSheetFilterBuilder W(ShowCheckoutMethod showCheckoutMethod) => new TransactionsAppLessSheetFilterBuilder(new RequestData(Data)
        {
            ShowCheckoutMethod = showCheckoutMethod
        });

        public TransactionsAppLessSheetFilterBuilder W(ShowCustomerInfoOption showCustomerInfo) => new TransactionsAppLessSheetFilterBuilder(new RequestData(Data)
        {
            ShowCustomerInfo = showCustomerInfo
        });

        public TransactionsAppLessSheetFilterBuilder W(ShowOrderNotesOption showOrderNotes) => new TransactionsAppLessSheetFilterBuilder(new RequestData(Data)
        {
            ShowOrderNotes = showOrderNotes
        });

        public TransactionsAppLessSheetFilterBuilder W(ShowProductCategoryOption showProductCategory) => new TransactionsAppLessSheetFilterBuilder(new RequestData(Data)
        {
            ShowProductCategory = showProductCategory
        });

        public TransactionsAppLessSheetFilterBuilder W(IncludedTaxTariffIds includedTaxTariffIds) => new TransactionsAppLessSheetFilterBuilder(new RequestData(Data)
        {
            IncludeTaxTariffIds = includedTaxTariffIds
        });

        public TransactionsAppLessSheetFilterBuilder W(IncludedSystemProductTypes includedSystemProductTypes) => new TransactionsAppLessSheetFilterBuilder(new RequestData(Data)
        {
            IncludedSystemProductTypes = includedSystemProductTypes
        });

        public TransactionsAppLessSheetFilterBuilder W(IncludedServiceMethods includedServiceMethods) => new TransactionsAppLessSheetFilterBuilder(new RequestData(Data)
        {
            IncludedServiceMethods = includedServiceMethods
        });

        public TransactionsAppLessSheetFilterBuilder W(IncludedCheckoutMethods includedCheckoutMethods) => new TransactionsAppLessSheetFilterBuilder(new RequestData(Data)
        {
            IncludedCheckoutMethods = includedCheckoutMethods
        });

        public TransactionsAppLessSheetFilterBuilder W(OutletId outletId) => new TransactionsAppLessSheetFilterBuilder(new RequestData(Data)
        {
            OutletId = outletId
        });

        public TransactionsAppLessSheetFilterBuilder W(CategoryFilterIds categoryFilterIds) => new TransactionsAppLessSheetFilterBuilder(new RequestData(Data)
        {
            CategoryFilterIds = categoryFilterIds
        });

        public TransactionsAppLessSheetFilterBuilder W(IncludeExcludeFilter categoryFilterType) => new TransactionsAppLessSheetFilterBuilder(new RequestData(Data)
        {
            CategoryFilterType = categoryFilterType
        });

        public TransactionsAppLessSheetFilterBuilder W(ShowPaymentMethod showPaymentMethod) => new TransactionsAppLessSheetFilterBuilder(new RequestData(Data)
        {
            ShowPaymentMethod = showPaymentMethod
        });

        public TransactionsAppLessSheetFilterBuilder W(ShowCardSummary showCardSummary) => new TransactionsAppLessSheetFilterBuilder(new RequestData(Data)
        {
            ShowCardSummary = showCardSummary
        });

        public TransactionsAppLessSheetFilterBuilder W(ShowDeliveryPoint showDeliveryPoint) => new TransactionsAppLessSheetFilterBuilder(new RequestData(Data)
        {
            ShowDeliveryPoint = showDeliveryPoint
        });

        public TransactionsAppLessSheetFilterBuilder W(ShowCoversCount showCoversCount) => new TransactionsAppLessSheetFilterBuilder(new RequestData(Data)
        {
            ShowCoversCount = showCoversCount
        });

        public TransactionsAppLessSheetFilter Build() => new TransactionsAppLessSheetFilter
        {
            Name = Data.Name,
            Active = Data.Active,
            ShowProducts = Data.ShowProducts,
            ShowProductQuantity = Data.ShowProductQuantity,
            ShowProductPrice = Data.ShowProductPrice,
            ShowAlterations = Data.ShowAlterations,
            ShowServiceMethod = Data.ShowServiceMethod,
            ShowCheckoutMethod = Data.ShowCheckoutMethod,
            ShowCustomerInfo = Data.ShowCustomerInfo,
            ShowOrderNotes = Data.ShowOrderNotes,
            ShowSystemProducts = Data.ShowSystemProducts,
            ShowProductCategory = Data.ShowProductCategory,
            ShowCoversCount = Data.ShowCoversCount,
            IncludeTaxTariffIds = (int[])Data.IncludeTaxTariffIds,
            IncludedSystemProductTypes = (int[])Data.IncludedSystemProductTypes,
            ServiceMethods = (int[])Data.IncludedServiceMethods,
            CheckoutMethods = (int[])Data.IncludedCheckoutMethods,
            OutletId = Data.OutletId,
            CategoryFilterIds = (int[])Data.CategoryFilterIds,
            CategoryFilterType = Data.CategoryFilterType
        };

        internal class RequestData
        {
            public RequestData(TransactionsAppLessSheetFilter sheetFilter)
            {
                Name = "Order Report";
                Active = true;
                ShowProducts = sheetFilter.ShowProducts;
                ShowProductQuantity = sheetFilter.ShowProductQuantity;
                ShowProductPrice = sheetFilter.ShowProductPrice;
                ShowAlterations = sheetFilter.ShowAlterations;
                ShowServiceMethod = sheetFilter.ShowServiceMethod;
                ShowCheckoutMethod = sheetFilter.ShowCheckoutMethod;
                ShowCustomerInfo = sheetFilter.ShowCustomerInfo;
                ShowOrderNotes = sheetFilter.ShowOrderNotes;
                ShowSystemProducts = sheetFilter.ShowSystemProducts;
                ShowProductCategory = sheetFilter.ShowProductCategory;
                IncludeTaxTariffIds = sheetFilter.IncludeTaxTariffIds.ToArray();
                IncludedSystemProductTypes = sheetFilter.IncludedSystemProductTypes.ToArray();
                IncludedServiceMethods = sheetFilter.ServiceMethods.ToArray();
                IncludedCheckoutMethods = sheetFilter.CheckoutMethods.ToArray();
                CategoryFilterIds = sheetFilter.CategoryFilterIds.ToArray();
                CategoryFilterType = sheetFilter.CategoryFilterType;
                OutletId = sheetFilter.OutletId;
                ShowPaymentMethod = sheetFilter.ShowPaymentMethod;
                ShowDeliveryPoint = sheetFilter.ShowDeliveryPoint;
                ShowCoversCount = sheetFilter.ShowCoversCount;
            }

            public RequestData(RequestData old)
            {
                Name = old.Name;
                Active = old.Active;
                ShowProducts = old.ShowProducts;
                ShowProductQuantity = old.ShowProductQuantity;
                ShowProductPrice = old.ShowProductPrice;
                ShowAlterations = old.ShowAlterations;
                ShowServiceMethod = old.ShowServiceMethod;
                ShowCheckoutMethod = old.ShowCheckoutMethod;
                ShowCustomerInfo = old.ShowCustomerInfo;
                ShowOrderNotes = old.ShowOrderNotes;
                ShowSystemProducts = old.ShowSystemProducts;
                ShowProductCategory = old.ShowProductCategory;
                IncludeTaxTariffIds = old.IncludeTaxTariffIds;
                IncludedSystemProductTypes = old.IncludedSystemProductTypes;
                IncludedServiceMethods = old.IncludedServiceMethods;
                IncludedCheckoutMethods = old.IncludedCheckoutMethods;
                CategoryFilterIds = old.CategoryFilterIds;
                CategoryFilterType = old.CategoryFilterType;
                OutletId = old.OutletId;
                ShowPaymentMethod = old.ShowPaymentMethod;
                ShowDeliveryPoint = old.ShowDeliveryPoint;
                ShowCoversCount = old.ShowCoversCount;
            }

            public string Name { get; set; }
            public bool Active { get; set; }

            public ShowProductsOption ShowProducts { get; set; }
            public ShowProductQuantityOption ShowProductQuantity { get; set; }
            public ShowProductPriceOption ShowProductPrice { get; set; }
            public ShowAlterationsOption ShowAlterations { get; set; }
            public ShowServiceMethod ShowServiceMethod { get; set; }
            public ShowCheckoutMethod ShowCheckoutMethod { get; set; }
            public ShowCustomerInfoOption ShowCustomerInfo { get; set; }
            public ShowOrderNotesOption ShowOrderNotes { get; set; }
            public ShowSystemProductsOption ShowSystemProducts { get; set; }
            public ShowProductCategoryOption ShowProductCategory { get; set; }
            public IncludedTaxTariffIds IncludeTaxTariffIds { get; set; }
            public IncludedSystemProductTypes IncludedSystemProductTypes { get; set; }
            public IncludedServiceMethods IncludedServiceMethods { get; set; }
            public IncludedCheckoutMethods IncludedCheckoutMethods { get; set; }
            public CategoryFilterIds CategoryFilterIds { get; set; }
            public IncludeExcludeFilter CategoryFilterType { get; set; }
            public OutletId OutletId { get; set; }
            public ShowPaymentMethod ShowPaymentMethod { get; set; }
            public ShowCardSummary ShowCardSummary { get; set; }

            public ShowDeliveryPoint ShowDeliveryPoint { get; set; }
            public ShowCoversCount ShowCoversCount { get; set; }
        }
    }
}