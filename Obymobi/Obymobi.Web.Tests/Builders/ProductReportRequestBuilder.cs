﻿using System;
using System.Collections.Generic;
using System.Linq;
using Obymobi.Web.Analytics.DomainTypes;
using Obymobi.Web.Analytics.Model.ModelBase;
using Obymobi.Web.Analytics.Requests;

namespace Obymobi.Web.Tests.Builders
{
	internal class ProductReportRequestBuilder
	{
		public ProductReportRequestBuilder() : this(new RequestData())
		{
		}

		private ProductReportRequestBuilder(RequestData requestData) => Data = requestData;
		private RequestData Data { get; }

		public ProductReportRequest Build() => new ProductReportRequest
		{
			CompanyId = Data.CompanyId,
			LeadTimeOffset = ((DateTime) Data.FromDate).Subtract(((DateTime) Data.UntilDate).Date),
			UntilDateTimeUtc = Data.UntilDate,
			SheetFilters = Data.Sheets.Select(builder => (SpreadSheetFilter)builder.Build()).ToList()
		};

		internal class RequestData
		{
			public RequestData()
			{
				CompanyId = 550;
				CompanyTimeZone = string.Empty;
				FromDate = DateTime.UtcNow;
				UntilDate = DateTime.UtcNow;
				Sheets = new List<ProductReportSheetFilterBuilder>
				{
					new ProductReportSheetFilterBuilder()
				};
			}

			public RequestData(RequestData old)
			{
				CompanyId = old.CompanyId;
				CompanyTimeZone = old.CompanyTimeZone;
				FromDate = old.FromDate;
				UntilDate = old.UntilDate;
			}

			public int CompanyId { get; }
			public string CompanyTimeZone { get; }
			public FromDateTime FromDate { get; }
			public UntilDateTime UntilDate { get; }
			public IEnumerable<ProductReportSheetFilterBuilder> Sheets { get; set; } = new List<ProductReportSheetFilterBuilder>();
		}
	}
}
