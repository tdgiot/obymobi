using AutoFixture;
using Obymobi.Web.Analytics.Model.Orders;
using System;

namespace Obymobi.Web.Tests.Builders
{
    internal class OrderBuilder
    {
        private readonly OrderData orderData;

        public OrderBuilder() => this.orderData = new OrderData();

        public Order Build() =>
            new Order(
                orderData.DateTime,
                orderData.OrderNumber,
                orderData.ServiceMethodName,
                orderData.CheckoutMethodName,
                orderData.OrderTotal,
                orderData.OrderItems,
                orderData.CustomerName,
                orderData.CustomerPhoneNumber,
                orderData.CustomerEmail,
                orderData.Notes,
                orderData.Building,
                orderData.Address,
                orderData.PostCode,
                orderData.City,
                orderData.Instructions,
                orderData.PaymentMethod,
                orderData.CardSummary,
                orderData.DeliveryPoint,
                orderData.CoversCount
            );

        private OrderBuilder(OrderData orderData) => this.orderData = orderData;

        public OrderBuilder W(OrderItemSale[] orderItems) =>
            new OrderBuilder(new OrderData(this.orderData)
            {
                OrderItems = orderItems
            });

        public OrderBuilder W(decimal orderTotal) => new OrderBuilder(new OrderData(orderData) { OrderTotal = orderTotal });

        private class OrderData
        {
            public DateTime? DateTime { get; }
            public int OrderNumber { get; }
            public string ServiceMethodName { get; }
            public string CheckoutMethodName { get; }
            public decimal OrderTotal { get; internal set; }
            public OrderItemSale[] OrderItems { get; internal set; }
            public string CustomerName { get; }
            public string CustomerPhoneNumber { get; }
            public string CustomerEmail { get; }
            public string Notes { get; }
            public string Building { get; }
            public string Address { get; }
            public string PostCode { get; }
            public string City { get; }
            public string Instructions { get; }
            public string PaymentMethod { get; }
            public string CardSummary { get; }
            public string DeliveryPoint { get; }
            public string CoversCount { get; }

            public OrderData()
            {
                Fixture fixture = new Fixture();

                DateTime = fixture.Create<DateTime>();
                OrderNumber = fixture.Create<int>();
                ServiceMethodName = $"SM-{fixture.Create<string>()}";
                CheckoutMethodName = $"CM-{fixture.Create<string>()}";
                OrderTotal = fixture.Create<decimal>();
                OrderItems = new OrderItemSale[0];
                CustomerName = $"CN-{fixture.Create<string>()}";
                CustomerPhoneNumber = $"CP-{fixture.Create<string>()}";
                CustomerEmail = $"CE-{fixture.Create<string>()}";
                Notes = $"N-{fixture.Create<string>()}";
                Building = $"B-{fixture.Create<string>()}";
                Address = $"A-{fixture.Create<string>()}";
                PostCode = $"PC-{fixture.Create<string>()}";
                City = $"C-{fixture.Create<string>()}";
                Instructions = $"I-{fixture.Create<string>()}";
                PaymentMethod = $"PM-{fixture.Create<string>()}";
                CardSummary = $"CS-{fixture.Create<string>()}";
                DeliveryPoint = $"DP-{fixture.Create<string>()}";
                CoversCount = $"CC-{fixture.Create<string>()}";
            }

            public OrderData(OrderData orderData)
            {
                DateTime = orderData.DateTime;
                OrderNumber = orderData.OrderNumber;
                ServiceMethodName = orderData.ServiceMethodName;
                CheckoutMethodName = orderData.CheckoutMethodName;
                OrderTotal = orderData.OrderTotal;
                OrderItems = orderData.OrderItems;
                CustomerName = orderData.CustomerName;
                CustomerPhoneNumber = orderData.CustomerPhoneNumber;
                CustomerEmail = orderData.CustomerEmail;
                Notes = orderData.Notes;
                Building = orderData.Building;
                Address = orderData.Address;
                PostCode = orderData.PostCode;
                City = orderData.City;
                Instructions = orderData.Instructions;
                PaymentMethod = orderData.PaymentMethod;
                CardSummary = orderData.CardSummary;
                DeliveryPoint = orderData.DeliveryPoint;
                CoversCount = orderData.CoversCount;
            }
        }
    }
}
