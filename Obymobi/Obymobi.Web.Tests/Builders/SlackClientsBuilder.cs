﻿using Obymobi.Web.Slack.Clients;
using Obymobi.Web.Slack.Interfaces;

namespace Obymobi.Web.Tests.Builders
{
    internal class SlackClientsBuilder
    {
        private readonly IWebContext webContext;

        public SlackClientsBuilder() : this(new WebContextMockBuilder().Build())
        {

        }

        private SlackClientsBuilder(IWebContext webContext)
        {
            this.webContext = webContext;
        }

        public ISlackClients Build()
        {
            return new SlackClients(webContext, new ISlackClient[]
            {
                new ProductionChannelSlackClient(),
                new TestChannelSlackClient(),
                new SupportNotificationsSlackClient(),
                new DevSupportNotificationsSlackClient()
            });
        }

        public SlackClientsBuilder W(WebContextMockBuilder webContextMockBuilder) => W(webContextMockBuilder.Build());

        public SlackClientsBuilder W(IWebContext webContext) => new SlackClientsBuilder(webContext);
    }
}
