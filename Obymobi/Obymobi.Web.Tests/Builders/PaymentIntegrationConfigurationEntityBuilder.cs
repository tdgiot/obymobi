﻿using AutoFixture;
using Obymobi.Data.EntityClasses;

namespace Obymobi.Web.Tests.Builders
{
    internal class PaymentIntegrationConfigurationEntityBuilder
    {
        private readonly PaymentIntegrationConfigurationData paymentIntegrationConfigData;

        public PaymentIntegrationConfigurationEntityBuilder() => paymentIntegrationConfigData = new PaymentIntegrationConfigurationData();

        public PaymentIntegrationConfigurationEntity Build() => new PaymentIntegrationConfigurationEntity()
        {
            AdyenMerchantCode = paymentIntegrationConfigData.AdyenMerchantCode
        };

        private PaymentIntegrationConfigurationEntityBuilder(PaymentIntegrationConfigurationData paymentIntegrationConfigData) 
            => this.paymentIntegrationConfigData = paymentIntegrationConfigData;

        public PaymentIntegrationConfigurationEntityBuilder W() 
            => new PaymentIntegrationConfigurationEntityBuilder(new PaymentIntegrationConfigurationData(paymentIntegrationConfigData));

        private class PaymentIntegrationConfigurationData
        {
            internal string AdyenMerchantCode { get; }

            public PaymentIntegrationConfigurationData() => AdyenMerchantCode = new Fixture().Create<string>();

            public PaymentIntegrationConfigurationData(PaymentIntegrationConfigurationData paymentIntegrationConfigData) 
                => AdyenMerchantCode = paymentIntegrationConfigData.AdyenMerchantCode;
        }
    }
}
