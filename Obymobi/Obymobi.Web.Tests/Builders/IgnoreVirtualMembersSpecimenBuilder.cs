﻿using System.Reflection;
using AutoFixture;
using AutoFixture.Kernel;

namespace Obymobi.Web.Tests.Builders
{
    public class IgnoreVirtualMembersSpecimenBuilder : ISpecimenBuilder
    {
        public object Create(object request, ISpecimenContext context)
        {
            var propertyInfo = request as PropertyInfo;
            if (propertyInfo == null)
            {
                return new NoSpecimen();
            }

            if (propertyInfo.GetGetMethod().IsVirtual)
            {
                return null;
            }

            return new NoSpecimen();
        }

        public static ISpecimenBuilder CreateBuilder()
        {
            var fixture = new Fixture();
            fixture.Customizations.Add(new IgnoreVirtualMembersSpecimenBuilder());
            return fixture;
        }
    }
}
