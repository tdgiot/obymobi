﻿using System.Linq;
using Obymobi.Web.Analytics.DomainTypes;
using Obymobi.Web.Analytics.Model.ProductReport;

namespace Obymobi.Web.Tests.Builders
{
	internal class ProductReportSheetFilterBuilder
	{
		public ProductReportSheetFilterBuilder() : this(new ProductReportSheetFilter())
		{
		}

		private ProductReportSheetFilterBuilder(ProductReportSheetFilter defaultRequest) : this(new RequestData(defaultRequest))
		{
		}

		private ProductReportSheetFilterBuilder(RequestData requestData) => Data = requestData;
		private RequestData Data { get; }


		public ProductReportSheetFilter Build() => new ProductReportSheetFilter
		{
			Name = Data.Name,
			Active = Data.Active,
			ServiceMethods = (int[]) Data.IncludedServiceMethods,
			CheckoutMethods = (int[]) Data.IncludedCheckoutMethods,
			OutletId = Data.OutletId,
		};

		internal class RequestData
		{
			internal RequestData(ProductReportSheetFilter sheetFilter)
			{
				Name = "Product Report";
				Active = true;
				IncludedServiceMethods = sheetFilter.ServiceMethods.ToArray();
				IncludedCheckoutMethods = sheetFilter.CheckoutMethods.ToArray();
				OutletId = sheetFilter.OutletId;
			}

			internal RequestData(RequestData old)
			{
				Name = old.Name;
				Active = old.Active;
				IncludedServiceMethods = old.IncludedServiceMethods;
				IncludedCheckoutMethods = old.IncludedCheckoutMethods;
				OutletId = old.OutletId;
			}

			public string Name { get; set; }
			public bool Active { get; set; }

			public IncludedServiceMethods IncludedServiceMethods { get; set; }
			public IncludedCheckoutMethods IncludedCheckoutMethods { get; set; }
			public OutletId OutletId { get; set; }
		}
	}
}
