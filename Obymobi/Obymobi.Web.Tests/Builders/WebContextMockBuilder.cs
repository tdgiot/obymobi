﻿using Moq;
using Obymobi.Enums;
using Obymobi.Web.Slack.Interfaces;

namespace Obymobi.Web.Tests.Builders
{
    internal class WebContextMockBuilder
    {
        private readonly CloudEnvironment cloudEnvironment;

        public WebContextMockBuilder() : this(CloudEnvironment.Development)
        {

        }

        private WebContextMockBuilder(CloudEnvironment cloudEnvironment)
        {
            this.cloudEnvironment = cloudEnvironment;
        }

        public IWebContext Build()
        {
            Mock<IWebContext> webContexMock = new Mock<IWebContext>();

            webContexMock.SetupGet(mock => mock.Environment).Returns(cloudEnvironment);

            return webContexMock.Object;
        }

        public WebContextMockBuilder W(CloudEnvironment cloudEnvironment) => new WebContextMockBuilder(cloudEnvironment);
    }
}
