﻿using Moq;
using Obymobi.Data.CollectionClasses;
using Obymobi.Web.Analytics.Repositories.TransactionsAppLess;

namespace Obymobi.Web.Tests.Builders
{
	internal class OrderRepositoryMockBuilder
	{
		public OrderRepositoryMockBuilder() : this(
			new OrderCollection
			{
				new OrderEntityBuilder().Build(),
				new OrderEntityBuilder().Build(),
				new OrderEntityBuilder().Build()
			})
		{
		}

		private OrderRepositoryMockBuilder(OrderCollection orderCollection) => OrderCollection = orderCollection;

		private OrderCollection OrderCollection { get; }

		public IOrderRepository Build()
		{
			var orderRepositoryMock = new Mock<IOrderRepository>();
			_ = orderRepositoryMock.Setup(repository => repository.Get(It.IsAny<OrdersRequest>()))
				.Returns(OrderCollection);
			return orderRepositoryMock.Object;
		}

		public OrderRepositoryMockBuilder W(OrderCollection orderCollection) => new OrderRepositoryMockBuilder(orderCollection);
	}
}
