﻿using NUnit.Framework;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Enums;
using Obymobi.Logic.Cms;
using Obymobi.Web.Analytics.Repositories;
using Obymobi.Web.Analytics.Repositories.TransactionsAppLess;
using Obymobi.Web.Tests.Builders;
using System.Collections.Generic;
using System.Linq;

namespace Obymobi.Web.Tests
{
    [TestFixture]
    public class DatabaseClearerTest
    {
        private static class A
        {
            public static ActionEntityBuilder ActionEntity => new ActionEntityBuilder();
            public static ActionRepositoryMockBuilder ActionRepositoryMock => new ActionRepositoryMockBuilder();
            public static OutletSellerInformationEntityBuilder OutletSellerInformationEntity => new OutletSellerInformationEntityBuilder();
            public static OutletSellerInformationRepositoryMockBuilder OutletSellerInformationRepositoryMock => new OutletSellerInformationRepositoryMockBuilder();
            public static OutletEntityBuilder OutletEntity => new OutletEntityBuilder();
            public static OutletRepositoryMockBuilder OutletRepositoryMock => new OutletRepositoryMockBuilder();
            public static CompanyEntityBuilder CompanyEntity => new CompanyEntityBuilder();
            public static CompanyRepositoryMockBuilder CompanyRepositoryMock => new CompanyRepositoryMockBuilder();
            public static PaymentIntegrationConfigurationEntityBuilder PaymentIntegrationConfigurationEntity => new PaymentIntegrationConfigurationEntityBuilder();
            public static PaymentIntegrationConfigurationRepositoryMockBuilder paymentIntegrationConfigurationRepositoryMock => new PaymentIntegrationConfigurationRepositoryMockBuilder();
            public static RoutestephandlerEntityBuilder RoutestephandlerEntity => new RoutestephandlerEntityBuilder();
            public static RoutestepHandlerRepositoryMockBuilder RoutestepHandlerRepositoryMock => new RoutestepHandlerRepositoryMockBuilder();
            public static DatabaseClearerBuilder DatabaseClearerBuilder => new DatabaseClearerBuilder();
        }

        [Test]
        public void UpdateEnterTheNocLink_ShouldUpdateActionResource()
        {
            // Arrange
            ActionEntity actionEntity = A.ActionEntity.Build();
            actionEntity.ActionId = 11561;

            IActionRepository actionRepositoryMock = A.ActionRepositoryMock.W(new ActionCollection() { actionEntity }).Build();

            DatabaseClearer sut = A.DatabaseClearerBuilder.W(actionRepositoryMock).Build();

            // Act
            sut.UpdateEnterTheNocLink(CloudEnvironment.Development);

            // Assert
            Assert.AreEqual(actionRepositoryMock.Get(11561).Resource, string.Format(DatabaseClearer.NocLinkTemplate, "dev"));
        }

        [TestCase(443)]
        [TestCase(550)]
        [TestCase(222)]
        [TestCase(595)]
        public void AddTestEmailsToOutlets_ShouldUpdateReceiptReceiversEmail(int companyID)
        {
            // Arrange
            OutletEntity outletEntity = A.OutletEntity.Build();
            OutletSellerInformationEntity sellerInfoEntity = A.OutletSellerInformationEntity.Build();

            outletEntity.CompanyId = companyID;
            outletEntity.OutletSellerInformationId = sellerInfoEntity.OutletSellerInformationId;

            OutletCollection outletcollection = new OutletCollection()
            { outletEntity };

            sellerInfoEntity.OutletCollection.AddRange(outletcollection);

            OutletSellerInformationCollection sellerInfoCollection = new OutletSellerInformationCollection
            { sellerInfoEntity };

            IOutletSellerInformationRepository outletSellerInfoRepositoryMock = A.OutletSellerInformationRepositoryMock.W(sellerInfoCollection).Build();
            IOutletRepository outletRepositoryMock = A.OutletRepositoryMock.W(outletcollection).Build();

            DatabaseClearer sut = A.DatabaseClearerBuilder.W(outletSellerInfoRepositoryMock, outletRepositoryMock).Build();

            // Act
            sut.AddTestEmailsToOutlets(CloudEnvironment.Development);

            // Assert
            OutletSellerInformationEntity entity = outletSellerInfoRepositoryMock.Get(new List<int>() { sellerInfoEntity.OutletSellerInformationId }).FirstOrDefault();
            Assert.AreEqual(entity.ReceiptReceiversEmail, DatabaseClearer.TestEmailAddresses);
        }

        [TestCase(443)]
        [TestCase(550)]
        [TestCase(222)]
        [TestCase(595)]
        public void SwitchOnMonitoring_ShouldUpdateCompany(int companyID)
        {
            // Arrange
            CompanyEntity companyEntity = A.CompanyEntity.Build();
            companyEntity.CompanyId = companyID;

            ICompanyRepository companyRepositoryMock = A.CompanyRepositoryMock.W(companyEntity).Build();

            DatabaseClearer sut = A.DatabaseClearerBuilder.W(companyRepositoryMock).Build();

            // Act
            sut.SwitchOnMonitoring(CloudEnvironment.Development);

            // Assert
            Assert.IsTrue(companyRepositoryMock.Get(companyID).FirstOrDefault().UseMonitoring);
        }

        [Test]
        public void UpdatePaymentConfigurations_ShouldUpdateAdyenMerchantCode()
        {
            // Arrange
            PaymentIntegrationConfigurationEntity entity = A.PaymentIntegrationConfigurationEntity.Build();
            entity.AdyenMerchantCode = DatabaseClearer.ProdAdyenMerchantCode;

            IPaymentIntegrationConfigurationRepository paymentIntegrationConfigRepo = A.paymentIntegrationConfigurationRepositoryMock.W(new PaymentIntegrationConfigurationCollection() { entity }).Build();

            DatabaseClearer sut = A.DatabaseClearerBuilder.W(paymentIntegrationConfigRepo).Build();

            // Act
            sut.UpdatePaymentConfigurations(CloudEnvironment.Development);

            // Assert
            Assert.IsTrue(paymentIntegrationConfigRepo.GetByAdyenMerchantCode(DatabaseClearer.TestAdyenMerchantCode).Count == 1);
        }

        [TestCase(2685)]
        public void AddTestEmailsToRouteStepHandlers_ShouldUpdateFieldValue1(int routeStepHandlerId)
        {
            // Arrange
            RoutestephandlerEntity routestephandlerEntity = A.RoutestephandlerEntity.Build();
            routestephandlerEntity.RoutestephandlerId = routeStepHandlerId;

            RoutestephandlerCollection routestephandlerCollection = new RoutestephandlerCollection()
            { routestephandlerEntity };

            IRoutestephandlerRepository routestepHandlerRepoMock = A.RoutestepHandlerRepositoryMock.W(routestephandlerCollection).Build();

            DatabaseClearer sut = A.DatabaseClearerBuilder.W(routestepHandlerRepoMock).Build();

            // Act
            sut.AddTestEmailsToRouteStepHandlers(CloudEnvironment.Development);

            // Assert
            RoutestephandlerEntity entity = routestepHandlerRepoMock.Get(new List<int>() { routeStepHandlerId }).FirstOrDefault();
            Assert.AreEqual(entity.FieldValue1, DatabaseClearer.TestEmailAddresses);
        }

        [TestCase(2682)]
        [TestCase(2683)]
        [TestCase(2686)]
        [TestCase(5006)]
        [TestCase(5041)]
        public void AddTestEmailsToRouteStepHandlers_ShouldUpdateFieldValues(int routeStepHandlerId)
        {
            // Arrange
            RoutestephandlerEntity routestephandlerEntity = A.RoutestephandlerEntity.Build();
            routestephandlerEntity.RoutestephandlerId = routeStepHandlerId;

            RoutestephandlerCollection routestephandlerCollection = new RoutestephandlerCollection()
            { routestephandlerEntity };

            IRoutestephandlerRepository routestepHandlerRepoMock = A.RoutestepHandlerRepositoryMock.W(routestephandlerCollection).Build();

            DatabaseClearer sut = A.DatabaseClearerBuilder.W(routestepHandlerRepoMock).Build();

            // Act
            sut.AddTestEmailsToRouteStepHandlers(CloudEnvironment.Development);

            // Assert
            RoutestephandlerEntity entity = routestepHandlerRepoMock.Get(new List<int>() { routeStepHandlerId }).FirstOrDefault();
            Assert.AreEqual(entity.FieldValue1, DatabaseClearer.TestEmailAddresses);
            Assert.AreEqual(entity.FieldValue2, DatabaseClearer.TestEmailFromAddress);
        }
    }
}
