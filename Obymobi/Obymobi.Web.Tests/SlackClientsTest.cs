﻿using NUnit.Framework;
using Obymobi.Enums;
using Obymobi.Web.Slack.Clients;
using Obymobi.Web.Slack.Interfaces;
using Obymobi.Web.Tests.Builders;

namespace Obymobi.Web.Tests
{
    [TestFixture]
    public class SlackClientsTest
    {
        private static class A
        {
            public static SlackClientsBuilder SlackClients => new SlackClientsBuilder();
            public static WebContextMockBuilder WebContextMock => new WebContextMockBuilder();
        }

        [TestCase(CloudEnvironment.ProductionPrimary)]
        [TestCase(CloudEnvironment.ProductionSecondary)]
        [TestCase(CloudEnvironment.ProductionOverwrite)]
        public void GetDefaultSlackClient_WithProductionEnvironment_ReturnsProductionChannel(CloudEnvironment cloudEnvironment)
        {
            // Arrange
            ISlackClients sut = A.SlackClients
                .W(A.WebContextMock
                .W(cloudEnvironment))
                .Build();

            // Act
            ISlackClient result = sut.GetDefaultSlackClient();

            // Assert
            Assert.IsInstanceOf<ProductionChannelSlackClient>(result);
        }

        [Test]
        public void GetDefaultSlackClient_WithTestEnvironment_ReturnsTestChannel()
        {
            // Arrange
            ISlackClients sut = A.SlackClients
                .W(A.WebContextMock
                .W(CloudEnvironment.Test))
                .Build();

            // Act
            ISlackClient result = sut.GetDefaultSlackClient();

            // Assert
            Assert.IsInstanceOf<TestChannelSlackClient>(result);
        }

        [Test]
        public void GetDefaultSlackClient_WithDevelopmentEnvironment_ReturnsEmptyClient()
        {
            // Arrange
            ISlackClients sut = A.SlackClients
                .W(A.WebContextMock
                .W(CloudEnvironment.Development))
                .Build();

            // Act
            ISlackClient result = sut.GetDefaultSlackClient();

            // Assert
            Assert.IsInstanceOf<EmptySlackClient>(result);
        }

        [TestCase(CloudEnvironment.ProductionPrimary)]
        [TestCase(CloudEnvironment.ProductionSecondary)]
        [TestCase(CloudEnvironment.ProductionOverwrite)]
        public void GetSupportNotificationsSlackClient_WithProductionEnvironment_ReturnsSupportNotificationsSlackClient(CloudEnvironment cloudEnvironment)
        {
            // Arrange
            ISlackClients sut = A.SlackClients
                .W(A.WebContextMock
                .W(cloudEnvironment))
                .Build();

            // Act
            ISlackClient result = sut.GetSupportNotificationsSlackClient();

            // Assert
            Assert.IsInstanceOf<SupportNotificationsSlackClient>(result);
        }

        [TestCase(CloudEnvironment.Test)]
        [TestCase(CloudEnvironment.Development)]
        public void GetSupportNotificationsSlackClient_WithNonProductionEnvironment_ReturnsDevSupportNotificationsSlackClient(CloudEnvironment cloudEnvironment)
        {
            // Arrange
            ISlackClients sut = A.SlackClients
                .W(A.WebContextMock
                .W(cloudEnvironment))
                .Build();

            // Act
            ISlackClient result = sut.GetSupportNotificationsSlackClient();

            // Assert
            Assert.IsInstanceOf<DevSupportNotificationsSlackClient>(result);
        }

        [TestCase(CloudEnvironment.ProductionPrimary)]
        [TestCase(CloudEnvironment.ProductionSecondary)]
        [TestCase(CloudEnvironment.ProductionOverwrite)]
        [TestCase(CloudEnvironment.Test)]
        [TestCase(CloudEnvironment.Development)]
        public void GetDevelopmentNotificationsSlackClient_WithAnyEnvironment_ReturnsDevSupportNotificationsSlackClient(CloudEnvironment cloudEnvironment)
        {
            // Arrange
            ISlackClients sut = A.SlackClients
                .W(A.WebContextMock
                .W(cloudEnvironment))
                .Build();

            // Act
            ISlackClient result = sut.GetDevelopmentNotificationsSlackClient();

            // Assert
            Assert.IsInstanceOf<DevSupportNotificationsSlackClient>(result);
        }
    }
}
