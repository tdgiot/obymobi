﻿using System.Linq;
using NUnit.Framework;
using Obymobi.Web.Analytics.Reports.ReportBase;
using Obymobi.Web.Tests.Builders;
using SpreadsheetLight;

namespace Obymobi.Web.Tests
{
    [TestFixture]
    public class WorksheetTest
    {
        private static class A
        {
            public static WorksheetNameBuilder WorksheetName => new WorksheetNameBuilder();
            public static ColumnBuilder Column => new ColumnBuilder();
            public static ColumnsBuilder Columns => new ColumnsBuilder();
            public static WorksheetBuilder Worksheet => new WorksheetBuilder();
        }

        [Test]
        public void Render_WithDefaultWorksheetName_KeepsTheWorksheet()
        {
            // Arrange
            string sheetName = A.WorksheetName
                .W(SLDocument.DefaultFirstSheetName)
                .Build();

            Worksheet sut = A.Worksheet
                .W(sheetName)
                .Build();

            SLDocument spreadsheet = new SLDocument();

            // Act
            sut.Render(spreadsheet);

            // Assert
            Assert.AreEqual(sheetName, spreadsheet.GetSheetNames().Single());
        }

        [Test]
        public void Render_WithNonDefaultWorksheetName_RemovesTheDefaultWorksheet()
        {
            // Arrange
            Worksheet sut = A.Worksheet
                .W(A.WorksheetName)
                .Build();

            SLDocument spreadsheet = new SLDocument();

            // Act
            sut.Render(spreadsheet);

            // Assert
            Assert.IsFalse(spreadsheet.GetSheetNames().Any(name => name == SLDocument.DefaultFirstSheetName));
        }

        [Test]
        public void Render_WithNonDefaultWorksheetName_HasTheSheetWithNonDefaultName()
        {
            // Arrange
            string sheetName = A.WorksheetName.Build();

            Worksheet sut = A.Worksheet
                .W(sheetName)
                .Build();

            SLDocument spreadsheet = new SLDocument();

            // Act
            sut.Render(spreadsheet);

            // Assert
            Assert.AreEqual(sheetName, spreadsheet.GetSheetNames().Single());
        }

        [Test]
        public void Render_WithThreeColumns_SetTheCorrectColumnWidths()
        {
            // Arrange
            Column[] columns = (new double[] { 30d, 140d, 301d })
                .Select(width => A.Column.W(width).Build())
                .ToArray();

            Worksheet sut = A.Worksheet
                .W(A.Columns.W(columns))
                .Build();

            SLDocument spreadsheet = new SLDocument();

            // Act
            sut.Render(spreadsheet);

            // Assert
            for (int columnIndex = 0; columnIndex < columns.Length; columnIndex++)
            {
                Assert.AreEqual(columns[columnIndex].Width, spreadsheet.GetColumnWidth(columnIndex + 1), .1d);
            }
        }
    }
}
