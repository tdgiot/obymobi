﻿using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Web.Analytics.Model.InventoryReport;
using Obymobi.Web.Analytics.Model.Orders;
using Obymobi.Web.Analytics.Model.ProductReport;
using Obymobi.Web.Analytics.Model.TransactionsAppLess;
using Obymobi.Web.Tests.Builders;

namespace Obymobi.Web.Tests
{
	[TestFixture]
	public class OrderCollectionTraitsTest
	{
		private static class A
		{
			public static OrderEntityBuilder OrderEntity => new OrderEntityBuilder();
			public static OrderitemEntityBuilder OrderItemEntity => new OrderitemEntityBuilder();
			public static OrderitemAlterationitemEntityBuilder OrderitemAlterationitemEntity => new OrderitemAlterationitemEntityBuilder();
			public static TagEntityBuilder TagEntity => new TagEntityBuilder();
			public static OrderitemAlterationitemTagEntityBuilder OrderitemAlterationitemTagEntity => new OrderitemAlterationitemTagEntityBuilder();
		}

		[Test]
		public void ToOrderModels_WithProductReportSheetFilterAndSelectedTagOnAlterationOnly_PullsThroughTheOrderItem()
		{
			// Arrange
			TagEntity tag = A.TagEntity.W(int.MaxValue).Build();
			const int productId = 123;

			OrderCollection sut = new OrderCollection(new[] {
				A.OrderEntity
					.W(A.OrderItemEntity
						.W(A.OrderitemAlterationitemEntity
							.W(A.OrderitemAlterationitemTagEntity
								.W(tag)
								.Build())
							.WithAlterationOptionProductId(productId)
							.Build())
						.Build())
					.Build()
			});
			
			ProductReportSheetFilter filter = new ProductReportSheetFilter
			{
				IncludedTagIds = new[]
				{
					tag.TagId
				}
			};

			// Act
			IEnumerable<ProductSale> result = sut.ToOrderModels(filter);

			// Assert
			Assert.AreEqual(1, result.Count());
		}

		[Test]
		public void ToOrderModels_WithInventoryReportSheetFilterAndSelectedTagOnAlterationOnly_PullsThroughTheOrderItem()
		{
			// Arrange
			TagEntity tag = A.TagEntity.W(int.MaxValue).Build();
			const int productId = 123;

			OrderCollection sut = new OrderCollection(new[] {
				A.OrderEntity
					.W(A.OrderItemEntity
						.W(A.OrderitemAlterationitemEntity
							.W(A.OrderitemAlterationitemTagEntity
								.W(tag)
								.Build())
							.WithAlterationOptionProductId(productId)
							.Build())
						.Build())
					.Build()
			});
			
			InventoryReportSheetFilter filter = new InventoryReportSheetFilter
			{
				IncludedTagIds = new[]
				{
					tag.TagId
				}
			};

			// Act
			IEnumerable<InventoryItem> result = sut.ToOrderModels(filter);

			// Assert
			Assert.AreEqual(1, result.Count());
		}
	}
}
