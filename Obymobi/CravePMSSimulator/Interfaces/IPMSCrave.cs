﻿using System.Collections.Generic;

namespace CravePMSSimulator.Interfaces
{
    public interface IPMSCrave
    {
        void CheckIn(string deliverypointNumber, string firstName, string lastName, bool allowViewBill, bool allowExpressCheckout, string language);

        void CheckoutPMS(string deliverypointNumber);

        void RoomMove(string deliverypointNumberOld, string deliverypointNumberNew);

        void EditGuestInformation(string deliverypointNumber, string firstName, string lastName, bool allowViewBill, bool allowExpressCheckout, string language);

        void EditRoomData(string deliverypointNumber, bool hasVoicemail);

        void SetWakeUp(string deliverypointNumber);

        void ClearWakeUp(string deliverypointNumber);

        void CreateOrder(string deliverypointNumber, List<string> productNames);

        void CheckedInForDevelopment(bool checkedIn);
    }
}