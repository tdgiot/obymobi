﻿namespace CravePMSSimulator.Interfaces
{
    public interface ILog
    {
        void Log(string message, params object[] args);
    }
}