﻿namespace CravePMSSimulator.Interfaces
{
    public interface ICravePMS
    {
        void Checkout(string deliverypointNumber, decimal balance);
        void GetFolio(string deliverypointNumber);
        void GetGuestInformation(string deliverypointNumber);
        void AddToFolio(string deliverypointNumber, string productName, decimal productPrice);
    }
}