﻿using Obymobi.Enums;
using Obymobi.Integrations.Interfaces;
using Obymobi.Integrations.PMS.Tigertms;

namespace CravePMSSimulator.PMSPanels
{
    public partial class UcTiger : BasePmsPanel
    {
        public UcTiger()
        {
            InitializeComponent();
        }

        public override PMSConnectorType ConnectorType
        {
            get { return PMSConnectorType.Tigertms; }
        }

        public override IConfigurationAdapter GetConfiguration()
        {
            string externalWsUrl = this.tbPmsTigerExternalWebserviceUrl.Text;

            return new TigertmsConfigurationAdapter
                   {
                       TestMode = this.cbPmsTigerEmulateMessageInService.Checked,
                       WebserviceUserkey = this.tbPmsTigerUserKey.Text,
                       WebserviceToCallToUrl = this.tbPmsTigerMessageInServiceUrl.Text,
                       WebserviceToBeCalledOnUrl = externalWsUrl,
                       ExternalNotUsed = !this.cbPmsTigerEmulateExternalWebservice.Checked
                   };
        }
    }
}
