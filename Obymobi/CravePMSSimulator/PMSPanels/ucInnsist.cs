﻿using Obymobi.Enums;
using Obymobi.Integrations.Interfaces;
using Obymobi.Integrations.PMS.Innsist;

namespace CravePMSSimulator.PMSPanels
{
    public partial class UcInnsist : BasePmsPanel
    {
        public UcInnsist()
        {
            InitializeComponent();
        }

        public override PMSConnectorType ConnectorType
        {
            get { return PMSConnectorType.Innsist; }
        }

        public override IConfigurationAdapter GetConfiguration()
        {
            return new TCAPosConfgurationAdapter
                   {
                       FromSystemId = this.tbInnsistFromId.Text,
                       Username = this.tbInnsistUsername.Text,
                       Password = this.tbInnsistPassword.Text,
                       ToSystemId = this.tbInnsistToId.Text,
                       WebserviceUrl = this.tbInnsistOutgoingUrl.Text,
                       WebserviceToBeCalledOnUrl = this.tbInnsistIncommingUrl.Text,
                       PosId = this.tbInnsistPosId.Text
                   };
        }
    }
}
