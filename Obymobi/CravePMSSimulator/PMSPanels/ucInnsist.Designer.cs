﻿namespace CravePMSSimulator.PMSPanels
{
    partial class UcInnsist
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label45 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.tbInnsistWakeUpCode = new System.Windows.Forms.TextBox();
            this.label42 = new System.Windows.Forms.Label();
            this.tbInnsistToId = new System.Windows.Forms.TextBox();
            this.tbInnsistFromId = new System.Windows.Forms.TextBox();
            this.label41 = new System.Windows.Forms.Label();
            this.tbInnsistPassword = new System.Windows.Forms.TextBox();
            this.tbInnsistUsername = new System.Windows.Forms.TextBox();
            this.label40 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.tbInnsistOutgoingUrl = new System.Windows.Forms.TextBox();
            this.tbInnsistIncommingUrl = new System.Windows.Forms.TextBox();
            this.label38 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tbInnsistPosId = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Location = new System.Drawing.Point(1024, 49);
            this.label45.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(116, 20);
            this.label45.TabIndex = 35;
            this.label45.Text = "(PMS -> Crave)";
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Location = new System.Drawing.Point(1024, 9);
            this.label44.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(116, 20);
            this.label44.TabIndex = 34;
            this.label44.Text = "(Crave -> PMS)";
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Location = new System.Drawing.Point(304, 89);
            this.label43.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(113, 20);
            this.label43.TabIndex = 33;
            this.label43.Text = "WakeUp Code";
            // 
            // tbInnsistWakeUpCode
            // 
            this.tbInnsistWakeUpCode.Location = new System.Drawing.Point(430, 85);
            this.tbInnsistWakeUpCode.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tbInnsistWakeUpCode.Name = "tbInnsistWakeUpCode";
            this.tbInnsistWakeUpCode.Size = new System.Drawing.Size(148, 26);
            this.tbInnsistWakeUpCode.TabIndex = 32;
            this.tbInnsistWakeUpCode.Text = "C1";
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Location = new System.Drawing.Point(304, 49);
            this.label42.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(48, 20);
            this.label42.TabIndex = 31;
            this.label42.Text = "To ID";
            // 
            // tbInnsistToId
            // 
            this.tbInnsistToId.Location = new System.Drawing.Point(430, 45);
            this.tbInnsistToId.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tbInnsistToId.Name = "tbInnsistToId";
            this.tbInnsistToId.Size = new System.Drawing.Size(148, 26);
            this.tbInnsistToId.TabIndex = 30;
            this.tbInnsistToId.Text = "001";
            // 
            // tbInnsistFromId
            // 
            this.tbInnsistFromId.Location = new System.Drawing.Point(430, 5);
            this.tbInnsistFromId.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tbInnsistFromId.Name = "tbInnsistFromId";
            this.tbInnsistFromId.Size = new System.Drawing.Size(148, 26);
            this.tbInnsistFromId.TabIndex = 29;
            this.tbInnsistFromId.Text = "ACME";
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(304, 9);
            this.label41.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(67, 20);
            this.label41.TabIndex = 28;
            this.label41.Text = "From ID";
            // 
            // tbInnsistPassword
            // 
            this.tbInnsistPassword.Location = new System.Drawing.Point(94, 45);
            this.tbInnsistPassword.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tbInnsistPassword.Name = "tbInnsistPassword";
            this.tbInnsistPassword.Size = new System.Drawing.Size(148, 26);
            this.tbInnsistPassword.TabIndex = 27;
            this.tbInnsistPassword.Text = "WEEkpNJPhAwK";
            // 
            // tbInnsistUsername
            // 
            this.tbInnsistUsername.Location = new System.Drawing.Point(94, 5);
            this.tbInnsistUsername.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tbInnsistUsername.Name = "tbInnsistUsername";
            this.tbInnsistUsername.Size = new System.Drawing.Size(148, 26);
            this.tbInnsistUsername.TabIndex = 26;
            this.tbInnsistUsername.Text = "SandBox";
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(6, 49);
            this.label40.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(78, 20);
            this.label40.TabIndex = 25;
            this.label40.Text = "Password";
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(3, 9);
            this.label39.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(83, 20);
            this.label39.TabIndex = 24;
            this.label39.Text = "Username";
            // 
            // tbInnsistOutgoingUrl
            // 
            this.tbInnsistOutgoingUrl.Location = new System.Drawing.Point(788, 5);
            this.tbInnsistOutgoingUrl.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tbInnsistOutgoingUrl.Name = "tbInnsistOutgoingUrl";
            this.tbInnsistOutgoingUrl.Size = new System.Drawing.Size(226, 26);
            this.tbInnsistOutgoingUrl.TabIndex = 23;
            this.tbInnsistOutgoingUrl.Text = "http://localhost:8586/";
            // 
            // tbInnsistIncommingUrl
            // 
            this.tbInnsistIncommingUrl.Location = new System.Drawing.Point(788, 45);
            this.tbInnsistIncommingUrl.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tbInnsistIncommingUrl.Name = "tbInnsistIncommingUrl";
            this.tbInnsistIncommingUrl.Size = new System.Drawing.Size(226, 26);
            this.tbInnsistIncommingUrl.TabIndex = 22;
            this.tbInnsistIncommingUrl.Text = "http://localhost:8587/";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(654, 49);
            this.label38.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(124, 20);
            this.label38.TabIndex = 21;
            this.label38.Text = "Incomming URL";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(654, 9);
            this.label37.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(111, 20);
            this.label37.TabIndex = 20;
            this.label37.Text = "Outgoing URL";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 89);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(57, 20);
            this.label1.TabIndex = 36;
            this.label1.Text = "Pos ID";
            // 
            // tbInnsistPosId
            // 
            this.tbInnsistPosId.Location = new System.Drawing.Point(94, 85);
            this.tbInnsistPosId.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tbInnsistPosId.Name = "tbInnsistPosId";
            this.tbInnsistPosId.Size = new System.Drawing.Size(148, 26);
            this.tbInnsistPosId.TabIndex = 37;
            this.tbInnsistPosId.Text = "01";
            // 
            // UcInnsist
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tbInnsistPosId);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label45);
            this.Controls.Add(this.label44);
            this.Controls.Add(this.label43);
            this.Controls.Add(this.tbInnsistWakeUpCode);
            this.Controls.Add(this.label42);
            this.Controls.Add(this.tbInnsistToId);
            this.Controls.Add(this.tbInnsistFromId);
            this.Controls.Add(this.label41);
            this.Controls.Add(this.tbInnsistPassword);
            this.Controls.Add(this.tbInnsistUsername);
            this.Controls.Add(this.label40);
            this.Controls.Add(this.label39);
            this.Controls.Add(this.tbInnsistOutgoingUrl);
            this.Controls.Add(this.tbInnsistIncommingUrl);
            this.Controls.Add(this.label38);
            this.Controls.Add(this.label37);
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "UcInnsist";
            this.Size = new System.Drawing.Size(1158, 135);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.TextBox tbInnsistWakeUpCode;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.TextBox tbInnsistToId;
        private System.Windows.Forms.TextBox tbInnsistFromId;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.TextBox tbInnsistPassword;
        private System.Windows.Forms.TextBox tbInnsistUsername;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.TextBox tbInnsistOutgoingUrl;
        private System.Windows.Forms.TextBox tbInnsistIncommingUrl;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbInnsistPosId;
    }
}
