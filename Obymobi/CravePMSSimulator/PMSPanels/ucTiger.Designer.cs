﻿namespace CravePMSSimulator.PMSPanels
{
    partial class UcTiger
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cbPmsTigerEmulateExternalWebservice = new System.Windows.Forms.CheckBox();
            this.label5 = new System.Windows.Forms.Label();
            this.tbPmsTigerUserKey = new System.Windows.Forms.TextBox();
            this.cbPmsTigerEmulateMessageInService = new System.Windows.Forms.CheckBox();
            this.label12 = new System.Windows.Forms.Label();
            this.tbPmsTigerExternalWebserviceUrl = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.tbPmsTigerMessageInServiceUrl = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // cbPmsTigerEmulateExternalWebservice
            // 
            this.cbPmsTigerEmulateExternalWebservice.AutoSize = true;
            this.cbPmsTigerEmulateExternalWebservice.Checked = true;
            this.cbPmsTigerEmulateExternalWebservice.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbPmsTigerEmulateExternalWebservice.Location = new System.Drawing.Point(440, 5);
            this.cbPmsTigerEmulateExternalWebservice.Name = "cbPmsTigerEmulateExternalWebservice";
            this.cbPmsTigerEmulateExternalWebservice.Size = new System.Drawing.Size(162, 17);
            this.cbPmsTigerEmulateExternalWebservice.TabIndex = 36;
            this.cbPmsTigerEmulateExternalWebservice.Text = "Emulate ExternalWebservice";
            this.cbPmsTigerEmulateExternalWebservice.UseVisualStyleBackColor = true;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(3, 60);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(49, 13);
            this.label5.TabIndex = 35;
            this.label5.Text = "User key";
            // 
            // tbPmsTigerUserKey
            // 
            this.tbPmsTigerUserKey.Location = new System.Drawing.Point(164, 57);
            this.tbPmsTigerUserKey.Name = "tbPmsTigerUserKey";
            this.tbPmsTigerUserKey.Size = new System.Drawing.Size(264, 20);
            this.tbPmsTigerUserKey.TabIndex = 34;
            this.tbPmsTigerUserKey.Text = "USERKEY";
            // 
            // cbPmsTigerEmulateMessageInService
            // 
            this.cbPmsTigerEmulateMessageInService.AutoSize = true;
            this.cbPmsTigerEmulateMessageInService.Checked = true;
            this.cbPmsTigerEmulateMessageInService.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbPmsTigerEmulateMessageInService.Location = new System.Drawing.Point(440, 33);
            this.cbPmsTigerEmulateMessageInService.Name = "cbPmsTigerEmulateMessageInService";
            this.cbPmsTigerEmulateMessageInService.Size = new System.Drawing.Size(158, 17);
            this.cbPmsTigerEmulateMessageInService.TabIndex = 33;
            this.cbPmsTigerEmulateMessageInService.Text = "Emulate MessageIn Service";
            this.cbPmsTigerEmulateMessageInService.UseVisualStyleBackColor = true;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(614, 6);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(168, 13);
            this.label12.TabIndex = 32;
            this.label12.Text = "(Url for TigerTMS to call Crave on)";
            // 
            // tbPmsTigerExternalWebserviceUrl
            // 
            this.tbPmsTigerExternalWebserviceUrl.Location = new System.Drawing.Point(164, 3);
            this.tbPmsTigerExternalWebserviceUrl.Name = "tbPmsTigerExternalWebserviceUrl";
            this.tbPmsTigerExternalWebserviceUrl.Size = new System.Drawing.Size(264, 20);
            this.tbPmsTigerExternalWebserviceUrl.TabIndex = 31;
            this.tbPmsTigerExternalWebserviceUrl.Text = "http://localhost:9654/externalWebservice";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(3, 6);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(130, 13);
            this.label11.TabIndex = 30;
            this.label11.Text = "Url of ExternalWebservice";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(614, 34);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(168, 13);
            this.label10.TabIndex = 29;
            this.label10.Text = "(Url for Crave to call TigerTMS on)";
            // 
            // tbPmsTigerMessageInServiceUrl
            // 
            this.tbPmsTigerMessageInServiceUrl.Location = new System.Drawing.Point(164, 31);
            this.tbPmsTigerMessageInServiceUrl.Name = "tbPmsTigerMessageInServiceUrl";
            this.tbPmsTigerMessageInServiceUrl.Size = new System.Drawing.Size(264, 20);
            this.tbPmsTigerMessageInServiceUrl.TabIndex = 28;
            this.tbPmsTigerMessageInServiceUrl.Text = "http://localhost:9655/messageInService";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(3, 34);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(126, 13);
            this.label8.TabIndex = 27;
            this.label8.Text = "Url of MessageIn Service";
            // 
            // ucTiger
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.cbPmsTigerEmulateExternalWebservice);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.tbPmsTigerUserKey);
            this.Controls.Add(this.cbPmsTigerEmulateMessageInService);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.tbPmsTigerExternalWebserviceUrl);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.tbPmsTigerMessageInServiceUrl);
            this.Controls.Add(this.label8);
            this.Name = "UcTiger";
            this.Size = new System.Drawing.Size(792, 84);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckBox cbPmsTigerEmulateExternalWebservice;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox tbPmsTigerUserKey;
        private System.Windows.Forms.CheckBox cbPmsTigerEmulateMessageInService;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox tbPmsTigerExternalWebserviceUrl;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox tbPmsTigerMessageInServiceUrl;
        private System.Windows.Forms.Label label8;
    }
}
