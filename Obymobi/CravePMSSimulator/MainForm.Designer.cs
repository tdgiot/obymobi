﻿namespace CravePMSSimulator
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tbPmsLog = new System.Windows.Forms.TextBox();
            this.tabControlMain = new System.Windows.Forms.TabControl();
            this.tabPmsConfig = new System.Windows.Forms.TabPage();
            this.btnStopPms = new System.Windows.Forms.Button();
            this.btnInitializePms = new System.Windows.Forms.Button();
            this.tabControlPmsConfig = new System.Windows.Forms.TabControl();
            this.tabPmsTiger = new System.Windows.Forms.TabPage();
            this.ucTiger = new CravePMSSimulator.PMSPanels.UcTiger();
            this.tabPmsInnsist = new System.Windows.Forms.TabPage();
            this.ucInnsist = new CravePMSSimulator.PMSPanels.UcInnsist();
            this.tabPmsControls = new System.Windows.Forms.TabPage();
            this.tabControlPmsControl = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.ucCravePMS = new CravePMSSimulator.ControlPanels.UcCravePMS();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.ucPMSCrave = new CravePMSSimulator.ControlPanels.UcPMSCrave();
            this.tabControlMain.SuspendLayout();
            this.tabPmsConfig.SuspendLayout();
            this.tabControlPmsConfig.SuspendLayout();
            this.tabPmsTiger.SuspendLayout();
            this.tabPmsInnsist.SuspendLayout();
            this.tabPmsControls.SuspendLayout();
            this.tabControlPmsControl.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.SuspendLayout();
            // 
            // tbPmsLog
            // 
            this.tbPmsLog.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.tbPmsLog.Location = new System.Drawing.Point(0, 359);
            this.tbPmsLog.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tbPmsLog.Multiline = true;
            this.tbPmsLog.Name = "tbPmsLog";
            this.tbPmsLog.ReadOnly = true;
            this.tbPmsLog.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tbPmsLog.Size = new System.Drawing.Size(1317, 692);
            this.tbPmsLog.TabIndex = 10;
            this.tbPmsLog.TextChanged += new System.EventHandler(this.tbPmsLog_TextChanged);
            // 
            // tabControlMain
            // 
            this.tabControlMain.Controls.Add(this.tabPmsConfig);
            this.tabControlMain.Controls.Add(this.tabPmsControls);
            this.tabControlMain.Dock = System.Windows.Forms.DockStyle.Top;
            this.tabControlMain.Location = new System.Drawing.Point(0, 0);
            this.tabControlMain.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tabControlMain.Name = "tabControlMain";
            this.tabControlMain.SelectedIndex = 0;
            this.tabControlMain.Size = new System.Drawing.Size(1317, 348);
            this.tabControlMain.TabIndex = 11;
            // 
            // tabPmsConfig
            // 
            this.tabPmsConfig.Controls.Add(this.btnStopPms);
            this.tabPmsConfig.Controls.Add(this.btnInitializePms);
            this.tabPmsConfig.Controls.Add(this.tabControlPmsConfig);
            this.tabPmsConfig.Location = new System.Drawing.Point(4, 29);
            this.tabPmsConfig.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tabPmsConfig.Name = "tabPmsConfig";
            this.tabPmsConfig.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tabPmsConfig.Size = new System.Drawing.Size(1309, 315);
            this.tabPmsConfig.TabIndex = 0;
            this.tabPmsConfig.Text = "Config PMS";
            this.tabPmsConfig.UseVisualStyleBackColor = true;
            // 
            // btnStopPms
            // 
            this.btnStopPms.Enabled = false;
            this.btnStopPms.Location = new System.Drawing.Point(224, 263);
            this.btnStopPms.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnStopPms.Name = "btnStopPms";
            this.btnStopPms.Size = new System.Drawing.Size(112, 35);
            this.btnStopPms.TabIndex = 2;
            this.btnStopPms.Text = "Stop";
            this.btnStopPms.UseVisualStyleBackColor = true;
            this.btnStopPms.Click += new System.EventHandler(this.btnStopPms_Click);
            // 
            // btnInitializePms
            // 
            this.btnInitializePms.Location = new System.Drawing.Point(12, 263);
            this.btnInitializePms.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnInitializePms.Name = "btnInitializePms";
            this.btnInitializePms.Size = new System.Drawing.Size(202, 35);
            this.btnInitializePms.TabIndex = 1;
            this.btnInitializePms.Text = "Initialize PMS";
            this.btnInitializePms.UseVisualStyleBackColor = true;
            this.btnInitializePms.Click += new System.EventHandler(this.btnInitializePms_Click);
            // 
            // tabControlPmsConfig
            // 
            this.tabControlPmsConfig.Controls.Add(this.tabPmsTiger);
            this.tabControlPmsConfig.Controls.Add(this.tabPmsInnsist);
            this.tabControlPmsConfig.Dock = System.Windows.Forms.DockStyle.Top;
            this.tabControlPmsConfig.Location = new System.Drawing.Point(4, 5);
            this.tabControlPmsConfig.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tabControlPmsConfig.Name = "tabControlPmsConfig";
            this.tabControlPmsConfig.SelectedIndex = 0;
            this.tabControlPmsConfig.Size = new System.Drawing.Size(1301, 249);
            this.tabControlPmsConfig.TabIndex = 0;
            this.tabControlPmsConfig.Selecting += new System.Windows.Forms.TabControlCancelEventHandler(this.tabControlPmsConfig_Selecting);
            // 
            // tabPmsTiger
            // 
            this.tabPmsTiger.Controls.Add(this.ucTiger);
            this.tabPmsTiger.Location = new System.Drawing.Point(4, 29);
            this.tabPmsTiger.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tabPmsTiger.Name = "tabPmsTiger";
            this.tabPmsTiger.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tabPmsTiger.Size = new System.Drawing.Size(1293, 216);
            this.tabPmsTiger.TabIndex = 0;
            this.tabPmsTiger.Text = "Tiger";
            this.tabPmsTiger.UseVisualStyleBackColor = true;
            // 
            // ucTiger
            // 
            this.ucTiger.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ucTiger.Location = new System.Drawing.Point(4, 5);
            this.ucTiger.Margin = new System.Windows.Forms.Padding(6, 8, 6, 8);
            this.ucTiger.Name = "ucTiger";
            this.ucTiger.Size = new System.Drawing.Size(1285, 206);
            this.ucTiger.TabIndex = 0;
            // 
            // tabPmsInnsist
            // 
            this.tabPmsInnsist.Controls.Add(this.ucInnsist);
            this.tabPmsInnsist.Location = new System.Drawing.Point(4, 29);
            this.tabPmsInnsist.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tabPmsInnsist.Name = "tabPmsInnsist";
            this.tabPmsInnsist.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tabPmsInnsist.Size = new System.Drawing.Size(1293, 216);
            this.tabPmsInnsist.TabIndex = 2;
            this.tabPmsInnsist.Text = "Innsist";
            this.tabPmsInnsist.UseVisualStyleBackColor = true;
            // 
            // ucInnsist
            // 
            this.ucInnsist.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ucInnsist.Location = new System.Drawing.Point(4, 5);
            this.ucInnsist.Margin = new System.Windows.Forms.Padding(6, 8, 6, 8);
            this.ucInnsist.Name = "ucInnsist";
            this.ucInnsist.Size = new System.Drawing.Size(1285, 206);
            this.ucInnsist.TabIndex = 0;
            // 
            // tabPmsControls
            // 
            this.tabPmsControls.Controls.Add(this.tabControlPmsControl);
            this.tabPmsControls.Location = new System.Drawing.Point(4, 29);
            this.tabPmsControls.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tabPmsControls.Name = "tabPmsControls";
            this.tabPmsControls.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tabPmsControls.Size = new System.Drawing.Size(1309, 315);
            this.tabPmsControls.TabIndex = 1;
            this.tabPmsControls.Text = "Controls";
            this.tabPmsControls.UseVisualStyleBackColor = true;
            // 
            // tabControlPmsControl
            // 
            this.tabControlPmsControl.Controls.Add(this.tabPage1);
            this.tabControlPmsControl.Controls.Add(this.tabPage3);
            this.tabControlPmsControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControlPmsControl.Location = new System.Drawing.Point(4, 5);
            this.tabControlPmsControl.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tabControlPmsControl.Name = "tabControlPmsControl";
            this.tabControlPmsControl.SelectedIndex = 0;
            this.tabControlPmsControl.Size = new System.Drawing.Size(1301, 305);
            this.tabControlPmsControl.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.ucCravePMS);
            this.tabPage1.Location = new System.Drawing.Point(4, 29);
            this.tabPage1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tabPage1.Size = new System.Drawing.Size(1293, 272);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Crave > PMS";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // ucCravePMS
            // 
            this.ucCravePMS.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ucCravePMS.Location = new System.Drawing.Point(4, 5);
            this.ucCravePMS.Margin = new System.Windows.Forms.Padding(6, 8, 6, 8);
            this.ucCravePMS.Name = "ucCravePMS";
            this.ucCravePMS.Size = new System.Drawing.Size(1285, 262);
            this.ucCravePMS.TabIndex = 0;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.ucPMSCrave);
            this.tabPage3.Location = new System.Drawing.Point(4, 29);
            this.tabPage3.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tabPage3.Size = new System.Drawing.Size(1288, 265);
            this.tabPage3.TabIndex = 1;
            this.tabPage3.Text = "PMS > Crave";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // ucPMSCrave
            // 
            this.ucPMSCrave.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ucPMSCrave.Location = new System.Drawing.Point(4, 5);
            this.ucPMSCrave.Margin = new System.Windows.Forms.Padding(6, 8, 6, 8);
            this.ucPMSCrave.Name = "ucPMSCrave";
            this.ucPMSCrave.Size = new System.Drawing.Size(1280, 255);
            this.ucPMSCrave.TabIndex = 0;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1317, 1051);
            this.Controls.Add(this.tabControlMain);
            this.Controls.Add(this.tbPmsLog);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.MaximizeBox = false;
            this.Name = "MainForm";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Crave PMS Simulator";
            this.tabControlMain.ResumeLayout(false);
            this.tabPmsConfig.ResumeLayout(false);
            this.tabControlPmsConfig.ResumeLayout(false);
            this.tabPmsTiger.ResumeLayout(false);
            this.tabPmsInnsist.ResumeLayout(false);
            this.tabPmsControls.ResumeLayout(false);
            this.tabControlPmsControl.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage3.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tbPmsLog;
        private System.Windows.Forms.TabControl tabControlMain;
        private System.Windows.Forms.TabPage tabPmsConfig;
        private System.Windows.Forms.TabPage tabPmsControls;
        private System.Windows.Forms.Button btnStopPms;
        private System.Windows.Forms.Button btnInitializePms;
        private System.Windows.Forms.TabControl tabControlPmsConfig;
        private System.Windows.Forms.TabPage tabPmsTiger;
        private System.Windows.Forms.TabPage tabPmsInnsist;
        private PMSPanels.UcTiger ucTiger;
        private PMSPanels.UcInnsist ucInnsist;
        private System.Windows.Forms.TabControl tabControlPmsControl;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage3;
        private ControlPanels.UcCravePMS ucCravePMS;
        private ControlPanels.UcPMSCrave ucPMSCrave;
    }
}

