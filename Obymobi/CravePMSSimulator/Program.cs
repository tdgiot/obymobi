﻿using System;
using System.IO;
using System.Windows.Forms;
using Dionysos.Configuration;
using Obymobi.Integrations.PMS.Configuration;

namespace CravePMSSimulator
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            XmlConfigurationProvider provider = new Dionysos.Configuration.XmlConfigurationProvider { ManualConfigFilePath = Path.Combine(Path.GetDirectoryName(Application.StartupPath), "PMSSimulator.config") };

            Dionysos.Global.ConfigurationProvider = provider;
            Dionysos.Global.ConfigurationInfo.Add(new PmsConfigurationInfo());

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new MainForm());
        }
    }
}
