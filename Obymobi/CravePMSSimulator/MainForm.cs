﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using CravePMSSimulator.Integrations;
using CravePMSSimulator.Interfaces;
using CravePMSSimulator.PMSPanels;

namespace CravePMSSimulator
{
    public partial class MainForm : Form, ILog, ICravePMS, IPMSCrave
    {
        [DllImport("User32.dll", CharSet = CharSet.Auto, EntryPoint = "SendMessage")]
        static extern IntPtr SendMessage(IntPtr hWnd, uint msg, IntPtr wParam, IntPtr lParam);

        private BaseIntegration integration;

        public MainForm()
        {
            InitializeComponent();

            this.tbPmsLog.Text = string.Empty;
            tabControlPmsConfig_Selecting(null, null);

            this.ucCravePMS.SetHandler(this);
            this.ucPMSCrave.SetHandler(this);
        }

        public void Log(string format, params object[] args)
        {
            if (this.tbPmsLog.InvokeRequired)
            {
                this.tbPmsLog.Invoke((MethodInvoker)(() => Log(format, args)));
            }
            else
            {
                this.tbPmsLog.AppendText(string.Format(format, args) + Environment.NewLine);
            }
        }

        private void CreatePmsIntegration()
        {
            BasePmsPanel basePanel = GetBasePmsPanel();   
            this.integration = BaseIntegration.Create(this, basePanel.ConnectorType, basePanel.GetConfiguration());
            bool result = this.integration.Start();

            ToggleControlButtons(result);

            this.tabControlMain.SelectedIndex = 1;
        }

        private void StopPmsIntegration()
        {
            if (this.integration != null)
            {
                bool result = this.integration.Stop();
                this.integration = null;

                ToggleControlButtons(!result);
                tabControlPmsConfig_Selecting(null, null);
            }
        }

        private BasePmsPanel GetBasePmsPanel()
        {
            return this.tabControlPmsConfig.SelectedTab.Controls[0] as BasePmsPanel;
        }

        private void ToggleControlButtons(bool pmsActive)
        {
            this.btnInitializePms.Enabled = !pmsActive;
            this.btnStopPms.Enabled = pmsActive;
            this.tabControlPmsConfig.Enabled = !pmsActive;
        }

        #region UI Events

        private void btnInitializePms_Click(object sender, EventArgs e)
        {
            CreatePmsIntegration();
        }

        private void btnStopPms_Click(object sender, EventArgs e)
        {
            StopPmsIntegration();
        }

        private void tabControlPmsConfig_Selecting(object sender, TabControlCancelEventArgs e)
        {
            this.btnInitializePms.Text = string.Format("Initialize PMS - {0}", GetBasePmsPanel().ConnectorType);
        }

        private void tbPmsLog_TextChanged(object sender, EventArgs e)
        {
            MainForm.SendMessage(this.tbPmsLog.Handle, 277, new IntPtr(7), new IntPtr(0));
        }

        #endregion

        #region ICravePMS

        public void Checkout(string deliverypointNumber, decimal balance)
        {
            if (this.integration != null)
            {
                this.integration.Checkout(deliverypointNumber, balance);
            }
        }

        public void GetFolio(string deliverypointNumber)
        {
            if (this.integration != null)
            {
                this.integration.GetFolio(deliverypointNumber);
            }
        }

        public void GetGuestInformation(string deliverypointNumber)
        {
            if (this.integration != null)
            {
                this.integration.GetGuestInformation(deliverypointNumber);
            }
        }

        public void AddToFolio(string deliverypointNumber, string productName, decimal productPrice)
        {
            if (this.integration != null)
            {
                this.integration.AddToFolio(deliverypointNumber, productName, productPrice);
            }
        }

        #endregion

        #region IPMSCrave

        public void CheckIn(string deliverypointNumber, string firstName, string lastName, bool allowViewBill, bool allowExpressCheckout, string language)
        {
            if (this.integration != null)
            {
                this.integration.CheckIn(deliverypointNumber, firstName, lastName, allowViewBill, allowExpressCheckout, language);
            }
        }

        public void CheckoutPMS(string deliverypointNumber)
        {
            if (this.integration != null)
            {
                this.integration.CheckoutPMS(deliverypointNumber);
            }
        }

        public void RoomMove(string deliverypointNumberOld, string deliverypointNumberNew)
        {
            if (this.integration != null)
            {
                this.integration.RoomMove(deliverypointNumberOld, deliverypointNumberNew);
            }
        }

        public void EditGuestInformation(string deliverypointNumber, string firstName, string lastName, bool allowViewBill, bool allowExpressCheckout, string language)
        {
            if (this.integration != null)
            {
                this.integration.EditGuestInformation(deliverypointNumber, firstName, lastName, allowViewBill, allowExpressCheckout, language);
            }
        }

        public void EditRoomData(string deliverypointNumber, bool hasVoicemail)
        {
            if (this.integration != null)
            {
                this.integration.EditRoomData(deliverypointNumber, hasVoicemail);
            }
        }

        public void SetWakeUp(string deliverypointNumber)
        {
            if (this.integration != null)
            {
                this.integration.SetWakeUp(deliverypointNumber);
            }
        }

        public void ClearWakeUp(string deliverypointNumber)
        {
            if (this.integration != null)
            {
                this.integration.ClearWakeUp(deliverypointNumber);
            }
        }

        public void CreateOrder(string deliverypointNumber, List<string> productNames)
        {
            if (this.integration != null)
            {
                this.integration.CreateOrder(deliverypointNumber, productNames);
            }
        }

        public void CheckedInForDevelopment(bool checkedIn)
        {
            if (this.integration != null)
            {
                this.integration.CheckedInForDevelopment(checkedIn);
            }
        }

        #endregion
    }
}
