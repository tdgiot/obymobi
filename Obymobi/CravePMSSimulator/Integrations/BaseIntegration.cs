﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using CravePMSSimulator.Interfaces;
using Dionysos;
using Obymobi.Enums;
using Obymobi.Integrations.Interfaces;
using Obymobi.Integrations.PMS;
using Obymobi.Logic.Model;

namespace CravePMSSimulator.Integrations
{
    public abstract class BaseIntegration
    {
        protected PmsConnector Connector { get; set; }
        private ILog Logger { get; set; }

        #region Ctor

        public static BaseIntegration Create(ILog log, PMSConnectorType type, IConfigurationAdapter config)
        {
            BaseIntegration integration;
            switch (type)
            {
                case PMSConnectorType.Tigertms:
                    integration = TigerIntegration.Create(config);
                    break;
                case PMSConnectorType.Innsist:
                    integration = InnsistIntegration.Create(config);
                    break;
                default:
                    throw new ArgumentOutOfRangeException("type", type, null);
            }

            integration.Logger = log;

            return integration;
        }

        protected BaseIntegration(PMSConnectorType type)
        {
            this.ConnectorType = type;
        }

        #endregion

        public PMSConnectorType ConnectorType { get; private set; }

        public bool Start()
        {
            if (this.Connector != null)
            {
                Log("Error: There already seems to be an active PMS connector");
            }
            else
            {
                Log("Starting PMS connector: {0}", this.ConnectorType);

                if (OnStart())
                {
                    HookUpEvents();
                }
            }
            Log("--------------------------");

            return (this.Connector != null);
        }

        public bool Stop()
        {
            if (this.Connector == null)
            {
                Log("Error: There is no active PMS connector");
            }
            else
            {
                Log("Stopping PMS connector: {0}", this.ConnectorType);

                if (OnStop())
                {
                    this.Connector.StopReceiving();
                    this.Connector = null;
                }
            }
            Log("--------------------------");

            return (this.Connector == null);
        }

        #region Crave to PMS

        /// <summary>
        /// Checkout from Crave to PMS
        /// </summary>
        public void Checkout(string deliverypointNumber, decimal balance)
        {
            if (!IsConnectorActive())
                return;

            Checkout checkoutRequest = new Checkout();
            checkoutRequest.DeliverypointNumber = deliverypointNumber;
            checkoutRequest.ChargePriceIn = balance;

            Task.Factory.StartNew(() => this.Connector.Checkout(checkoutRequest)).ContinueWith(t =>
            {
                if (!t.IsFaulted)
                {
                    Log(t.Result ? "Express Checkout succeeded." : "Express Checkout failed.");
                }
                else
                {
                    string exception = (t.Exception != null) ? t.Exception.Message : "Unknown";
                    Log("Express Checkout failed: {0}", exception);
                }
                Log("--------------------------");
            }, TaskScheduler.FromCurrentSynchronizationContext());
        }

        public void GetFolio(string deliverypointNumber)
        {
            if (!IsConnectorActive())
                return;

            Task.Factory.StartNew(() => this.Connector.GetFolio(deliverypointNumber)).ContinueWith(t =>
            {
                if (!t.IsFaulted)
                {
                    if (t.Result != null)
                    {
                        foreach (Folio folio in t.Result)
                        {
                            Log("Folio received: Room '{0}'", folio.DeliverypointNumber);
                            if (!folio.Error.IsNullOrWhiteSpace())
                            {
                                Log("Error: '{0}'", folio.Error);
                            }
                            else
                            {
                                foreach (FolioItem item in folio.FolioItems)
                                {
                                    Log("Folio item: '{0}', '{1}', '{2}'",
                                        item.Description, item.PriceIn, item.Created);
                                }
                            }
                        }
                    }
                    else
                    {
                        Log("Folio received: No result.");
                    }
                }
                else
                {
                    string exception = (t.Exception != null) ? t.Exception.Message : "Unknown";
                    Log("GetFolio failed: {0}", exception);
                }
                Log("--------------------------");
            }, TaskScheduler.FromCurrentSynchronizationContext());
        }

        public void GetGuestInformation(string deliverypointNumber)
        {
            if (!IsConnectorActive())
                return;

            Task.Factory.StartNew(() => this.Connector.GetGuestInformation(deliverypointNumber)).ContinueWith(t =>
            {
                if (!t.IsFaulted)
                {
                    if (t.Result != null)
                    {
                        foreach (GuestInformation guestInfo in t.Result)
                        {
                            Log(guestInfo);
                        }
                    }
                    else
                    {
                        Log("GetGuestInformation returned nothing.");
                    }
                }
                else
                {
                    string exception = (t.Exception != null) ? t.Exception.Message : "Unknown";
                    Log("GetGuestInformation failed: {0}", exception);
                }
                Log("--------------------------");
            }, TaskScheduler.FromCurrentSynchronizationContext());
        }

        public void AddToFolio(string deliverypointNumber, string description, decimal price)
        {
            if (!IsConnectorActive())
                return;

            FolioItem fi = new FolioItem();
            fi.Created = DateTime.Now;
            fi.Description = description;
            fi.PriceIn = price;
            List<FolioItem> folioItems = new List<FolioItem> { fi };

            Task.Factory.StartNew(() => this.Connector.AddToFolio(deliverypointNumber, folioItems)).ContinueWith(t =>
            {
                if (!t.IsFaulted)
                {
                    Log(t.Result ? "Add to Folio succeeded." : "Add to Folio failed.");
                }
                else
                {
                    string exception = (t.Exception != null) ? t.Exception.Message : "Unknown";
                    Log("AddToFolio failed: {0}", exception);
                }
                Log("--------------------------");
            }, TaskScheduler.FromCurrentSynchronizationContext());            
        }

        #endregion

        #region PMS to Crave

        public abstract void CheckIn(string deliverypointNumber, string firstName, string lastName, bool allowViewBill, bool allowExpressCheckout, string language);
        
        /// <summary>
        /// Checkout from PMS to Crave
        /// </summary>
        public abstract void CheckoutPMS(string deliverypointNumber);

        public abstract void RoomMove(string deliverypointNumberOld, string deliverypointNumberNew);

        public abstract void EditGuestInformation(string deliverypointNumber, string firstName, string lastName, bool allowViewBill, bool allowExpressCheckout, string language);

        public abstract void EditRoomData(string deliverypointNumber, bool hasVoicemail);

        public abstract void SetWakeUp(string deliverypointNumber);

        public abstract void ClearWakeUp(string deliverypointNumber);

        public abstract void CreateOrder(string deliverypointNumber, List<string> productNames);

        public virtual void CheckedInForDevelopment(bool checkedIn)
        {
        }

        #endregion

        protected abstract bool OnStart();
        protected abstract bool OnStop();

        protected void Log(string message, params object[] args)
        {
            this.Logger.Log(message, args);
        }

        protected void Log(GuestInformation guestInformation)
        {
            Log("Account: '{0}'", guestInformation.AccountNumber);
            Log("Group: '{0}' (Reference: '{1}')", guestInformation.GroupName, guestInformation.GroupReference);
            Log("Company: '{0}'", guestInformation.Company);
            Log("Name: First: '{0}', Middle: '{1}', Last: '{2}'", guestInformation.CustomerFirstname, guestInformation.CustomerLastnamePrefix, guestInformation.CustomerLastname);
            Log("Zipcode: '{0}'", guestInformation.Zipcode);
            Log("Language: '{0}'", guestInformation.LanguageCode);
            Log("Password: '{0}'", guestInformation.Password);
            Log("Error: '{0}'", guestInformation.Error);

            if (guestInformation.AllowFolioPosting)
            {
                Log("Folio posting: Allowed, Credit Limit: '{0}'", guestInformation.CreditLimit.ToString("N2"));
            }
            else
            {
                Log("Folio posting: Not allowed");
            }
        }

        private bool IsConnectorActive()
        {
            if (this.Connector == null)
            {
                Log("ERROR: PMS Connector is not initialized!");
                return false;
            }

            return true;
        }

        private void HookUpEvents()
        {
            this.Connector.CheckedIn += connector_CheckedIn;
            this.Connector.CheckedOut += connector_CheckedOut;
            this.Connector.CreditLimitUpdated += connector_CreditLimitUpdated;
            this.Connector.DoNotDisturbStateUpdated += connector_DoNotDisturbStateUpdated;
            this.Connector.GuestInformationUpdated += connector_GuestInformationUpdated;
            this.Connector.Moved += connector_Moved;
            this.Connector.RoomStateUpdated += connector_RoomStateUpdated;
        }

        protected void connector_Logged(object sender, LoggingLevel level, string log)
        {
            Log(log);
        }

        private void connector_CheckedIn(PmsConnector sender, GuestInformation guestInformation)
        {
            Log("Room checked in: Room '{0}'", guestInformation.DeliverypointNumber);
            Log(guestInformation);
            Log("--------------------------");
        }

        private void connector_CheckedOut(PmsConnector sender, GuestInformation guestInformation)
        {
            Log("Room checked out: Room '{0}'", guestInformation.DeliverypointNumber);
            Log("--------------------------");
        }

        private void connector_CreditLimitUpdated(PmsConnector sender, string deliverypointNumber, decimal limit)
        {
            Log("Credit limit updated: Room '{0}' to state '{1:N2}'", deliverypointNumber, limit);
            Log("--------------------------");
        }

        private void connector_DoNotDisturbStateUpdated(PmsConnector sender, string deliverypointNumber, DoNotDisturbState state)
        {
            Log("Do not disturb updated: Room '{0}' to state '{1}'", deliverypointNumber, state);
            Log("--------------------------");
        }

        private void connector_GuestInformationUpdated(PmsConnector sender, GuestInformation guestInformation)
        {
            Log("GuestInformation Updated for Room: {0}", guestInformation.DeliverypointNumber);
            Log(guestInformation);
            Log("--------------------------");
        }

        private void connector_Moved(PmsConnector sender, string fromDeliverypointNumber, string fromStationNumber, string toDeliverypointNumber, string toStationNumber)
        {
            Log("RoomMoved - From: {0} (Station: {1}), To: {2} (Station: {3})", fromDeliverypointNumber, fromStationNumber, toDeliverypointNumber, toDeliverypointNumber, toStationNumber);
            Log("--------------------------");
        }

        private void connector_RoomStateUpdated(PmsConnector sender, string deliverypointNumber, RoomState state)
        {
            Log("RoomStateUpdated - Room: {0}, New state: {1}", deliverypointNumber, state.ToString());
            Log("--------------------------");
        }
    }
}