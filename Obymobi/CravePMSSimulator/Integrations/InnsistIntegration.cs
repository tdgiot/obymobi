﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using CravePMSSimulator.InnsistClientService;
using Obymobi.Enums;
using Obymobi.Integrations.Interfaces;
using Obymobi.Integrations.PMS.Innsist;

namespace CravePMSSimulator.Integrations
{
    public class InnsistIntegration : BaseIntegration
    {
        private readonly TCAPosConfgurationAdapter config;
        private ExternalWebserviceClient externalWebservice;

        private InnsistIntegration(TCAPosConfgurationAdapter config) : base(PMSConnectorType.Innsist)
        {
            this.config = config;
        }

        public static InnsistIntegration Create(IConfigurationAdapter config)
        {
            return new InnsistIntegration((TCAPosConfgurationAdapter)config);
        }

        public override void CheckIn(string deliverypointNumber, string firstName, string lastName, bool allowViewBill, bool allowExpressCheckout, string language)
        {
            HTNG_HotelCheckInNotifRQ request = new HTNG_HotelCheckInNotifRQ
            {
                EchoToken = Guid.NewGuid().ToString(),
                TimeStamp = DateTime.UtcNow,
                Room = new HTNG_RoomElementType
                {
                    RoomType = new RoomTypeType
                    {
                        RoomID = deliverypointNumber
                    }
                },
                HotelReservations = new HotelReservationsType
                {
                    HotelReservation = new HotelReservationsTypeHotelReservation[]
                    {
                        new HotelReservationsTypeHotelReservation
                        {
                            ResGuests = new ResGuestType[]
                            {
                                new ResGuestType
                                {
                                    GroupEventCode = "CraveGroupCode",
                                    ArrivalTimeSpecified = true,
                                    ArrivalTime = DateTime.UtcNow,
                                    DepartureTimeSpecified = true,
                                    DepartureTime = DateTime.UtcNow.AddDays(5),
                                    Profiles = new ArrayOfProfilesTypeProfileInfoProfileInfo[]
                                    {
                                        new ArrayOfProfilesTypeProfileInfoProfileInfo
                                        {
                                            UniqueID = new[]
                                            {
                                                new UniqueID_Type
                                                {
                                                    ID_Context = "GST123"
                                                }
                                            },
                                            Profile = new ProfileType
                                            {
                                                Customer = new CustomerType
                                                {
                                                    VIP_Indicator = false,
                                                    Language = language,
                                                    PersonName = new PersonNameType[]
                                                    {
                                                        new PersonNameType
                                                        {
                                                            NamePrefix = new[] {"Mr."},
                                                            GivenName = new[] {firstName},
                                                            Surname = lastName
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            },
                            RoomStays = new ArrayOfRoomStaysTypeRoomStayRoomStay[]
                            {
                                new ArrayOfRoomStaysTypeRoomStayRoomStay
                                {
                                    TimeSpan = new DateTimeSpanType
                                    {
                                        Start = DateTime.UtcNow.ToString("yyyy-MM-dd"),
                                        End = DateTime.UtcNow.AddDays(5).ToString("yyyy-MM-dd")
                                    }
                                }
                            }
                        }
                    }
                }
            };
            HTNG_ResponseBaseType response = this.externalWebservice.CheckedIn(request);
            bool test = response.TimeStampSpecified;
        }

        public override void CheckoutPMS(string deliverypointNumber)
        {
            HTNG_HotelCheckOutNotifRQ request = new HTNG_HotelCheckOutNotifRQ
            {
                EchoToken = Guid.NewGuid().ToString(),
                TimeStamp = DateTime.UtcNow,
                Room = new HTNG_RoomElementType
                {
                    RoomType = new RoomTypeType
                    {
                        RoomID = deliverypointNumber
                    }
                },
                HotelReservations = new HotelReservationsType
                {
                    HotelReservation = new HotelReservationsTypeHotelReservation[]
                    {
                        new HotelReservationsTypeHotelReservation
                        {
                            ResGuests = new ResGuestType[]
                            {
                                new ResGuestType
                                {
                                    GroupEventCode = "CraveGroupCode",
                                    ArrivalTimeSpecified = true,
                                    ArrivalTime = DateTime.UtcNow,
                                    DepartureTimeSpecified = true,
                                    DepartureTime = DateTime.UtcNow.AddDays(5)
                                }
                            }
                        }
                    }
                }
            };

            this.externalWebservice.CheckedOut(request);
        }

        public override void RoomMove(string deliverypointNumberOld, string deliverypointNumberNew)
        {
            HTNG_HotelRoomMoveNotifRQ request = new HTNG_HotelRoomMoveNotifRQ
            {
                EchoToken = Guid.NewGuid().ToString(),
                TimeStamp = DateTime.UtcNow,
                SourceRoomInformation = new HTNG_HotelRoomMoveNotifRQSourceRoomInformation
                {
                    Room = new HTNG_RoomElementType
                    {
                        RoomType = new RoomTypeType
                        {
                            RoomID = deliverypointNumberOld
                        }
                    }
                },
                DestinationRoomInformation = new HTNG_HotelRoomMoveNotifRQDestinationRoomInformation
                {
                    Room = new HTNG_RoomElementType
                    {
                        RoomType = new RoomTypeType
                        {
                            RoomID = deliverypointNumberNew
                        }
                    }
                }
            };

            this.externalWebservice.RoomMoved(request);
        }

        public override void EditGuestInformation(string deliverypointNumber, string firstName, string lastName, bool allowViewBill, bool allowExpressCheckout, string language)
        {
            HTNG_HotelStayUpdateNotifRQ request = new HTNG_HotelStayUpdateNotifRQ
            {
                EchoToken = Guid.NewGuid().ToString(),
                TimeStamp = DateTime.UtcNow,
                Room = new HTNG_RoomElementType
                {
                    RoomType = new RoomTypeType
                    {
                        RoomID = deliverypointNumber
                    }
                },
                HotelReservations = new HotelReservationsType
                {
                    HotelReservation = new HotelReservationsTypeHotelReservation[]
                    {
                        new HotelReservationsTypeHotelReservation
                        {
                            ResStatus = "In-house",
                            ResGuests = new ResGuestType[]
                            {
                                new ResGuestType
                                {
                                    GroupEventCode = "CraveGroupCode",
                                    ArrivalTimeSpecified = true,
                                    ArrivalTime = DateTime.UtcNow,
                                    DepartureTimeSpecified = true,
                                    DepartureTime = DateTime.UtcNow.AddDays(5),
                                    Profiles = new ArrayOfProfilesTypeProfileInfoProfileInfo[]
                                    {
                                        new ArrayOfProfilesTypeProfileInfoProfileInfo
                                        {
                                            UniqueID = new[]
                                            {
                                                new UniqueID_Type
                                                {
                                                    ID_Context = "GST123"
                                                }
                                            },
                                            Profile = new ProfileType
                                            {
                                                Customer = new CustomerType
                                                {
                                                    VIP_Indicator = false,
                                                    Language = language,
                                                    PersonName = new PersonNameType[]
                                                    {
                                                        new PersonNameType
                                                        {
                                                            NamePrefix = new[] {"Mr."},
                                                            GivenName = new[] {firstName},
                                                            Surname = lastName
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            },
                            RoomStays = new ArrayOfRoomStaysTypeRoomStayRoomStay[]
                            {
                                new ArrayOfRoomStaysTypeRoomStayRoomStay
                                {
                                    TimeSpan = new DateTimeSpanType
                                    {
                                        Start = DateTime.UtcNow.ToString("yyyy-MM-dd"),
                                        End = DateTime.UtcNow.AddDays(5).ToString("yyyy-MM-dd")
                                    }
                                }
                            }
                        }
                    }
                }
            };

            HTNG_ResponseBaseType response = this.externalWebservice.StayUpdated(request);
            bool test = response.TimeStampSpecified;
        }

        public override void EditRoomData(string deliverypointNumber, bool hasVoicemail)
        {
            Log("Error: Not implemented");
        }

        public override void SetWakeUp(string deliverypointNumber)
        {
            Log("Error: Not implemented");
        }

        public override void ClearWakeUp(string deliverypointNumber)
        {
            Log("Error: Not implemented");
        }

        public override void CreateOrder(string deliverypointNumber, List<string> productNames)
        {
            Log("Error: Not implemented");
        }

        protected override bool OnStart()
        {
            try
            {
                this.Connector = new TCAPosConnector(this.config, connector_Logged);
                this.externalWebservice = new InnsistClientService.ExternalWebserviceClient();
                this.externalWebservice.Endpoint.Address = new EndpointAddress(new Uri(this.config.WebserviceUrl));
            }
            catch (Exception ex)
            {
                Log("Exception: {0}", ex.Message);
                this.Connector = null;
            }

            return (this.Connector != null);
        }

        protected override bool OnStop()
        {
            Log("Stopping external webservice");

            this.externalWebservice.Close();
            this.externalWebservice = null;

            return true;
        }
    }
}