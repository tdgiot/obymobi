﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using System.Threading.Tasks;
using Dionysos;
using Obymobi.Enums;
using Obymobi.Integrations.Interfaces;
using Obymobi.Integrations.PMS.Tigertms;
using Obymobi.Integrations.PMS.Tigertms.Messages;

namespace CravePMSSimulator.Integrations
{
    public sealed class TigerIntegration : BaseIntegration
    {
        private readonly TigertmsConfigurationAdapter config;

        private TigertmsToBeCalledOn.ExternalWebserviceClient externalWebservice;

        private string wakeUpDateSent;
        private string wakeUpTimeSent;

        private TigerIntegration(TigertmsConfigurationAdapter config) : base(PMSConnectorType.Tigertms)
        {
            this.config = config;
        }

        public static TigerIntegration Create(IConfigurationAdapter config)
        {
            return new TigerIntegration((TigertmsConfigurationAdapter)config);
        }

        public override void CheckIn(string deliverypointNumber, string firstName, string lastName, bool allowViewBill, bool allowExpressCheckout, string language)
        {
            Checkinresults checkinresults = new Checkinresults();
            checkinresults.Title = "Mr.";
            checkinresults.Room = deliverypointNumber;
            checkinresults.First = firstName;
            checkinresults.Last = lastName;
            checkinresults.Viewbill = allowViewBill.ToString();
            checkinresults.ExpressCheckout = allowExpressCheckout.ToString();
            checkinresults.Language = language;

            SendMessageToExternal(checkinresults);
        }

        public override void CheckoutPMS(string deliverypointNumber)
        {
            Checkoutresults checkoutresults = new Checkoutresults();
            checkoutresults.Room = deliverypointNumber;

            SendMessageToExternal(checkoutresults);
        }

        public override void RoomMove(string deliverypointNumberOld, string deliverypointNumberNew)
        {
            Roommoveresults roommove = new Roommoveresults();
            roommove.RoomOld = deliverypointNumberOld;
            roommove.Room = deliverypointNumberNew;

            SendMessageToExternal(roommove);
        }

        public override void EditGuestInformation(string deliverypointNumber, string firstName, string lastName, bool allowViewBill, bool allowExpressCheckout, string language)
        {
            Editguestresults editguestresults = new Editguestresults();
            editguestresults.Room = deliverypointNumber;
            editguestresults.Title = "Mr.";
            editguestresults.First = firstName;
            editguestresults.Last = lastName;
            editguestresults.Viewbill = allowViewBill.ToString();
            editguestresults.ExpressCheckout = allowExpressCheckout.ToString();
            editguestresults.Language = language;

            SendMessageToExternal(editguestresults);
        }

        public override void EditRoomData(string deliverypointNumber, bool hasVoicemail)
        {
            Roomdataresults roomdataResults = new Roomdataresults();
            roomdataResults.Room = deliverypointNumber;
            roomdataResults.MessageWaitingLamp = hasVoicemail ? "Y" : "N";

            SendMessageToExternal(roomdataResults);
        }

        public override void SetWakeUp(string deliverypointNumber)
        {
            DateTime today = DateTime.Now.AddMinutes(5); // 5 minutes ahead so we have time to clear it, if necessary

            Wakeupsetresults wakeupsetresults = new Wakeupsetresults();
            wakeupsetresults.Room = deliverypointNumber;
            wakeupsetresults.WakeUpDate = string.Format("{0:dd/MM/yyyy}", today).Replace("-", "/"); // Format: 07/04/2010. I know, strange replace but took too long, fuck the police
            wakeupsetresults.WakeUpTime = string.Format("{0:HH:mm:ss}", today); // Format: 07:30:00

            this.wakeUpDateSent = wakeupsetresults.WakeUpDate;
            this.wakeUpTimeSent = wakeupsetresults.WakeUpTime;

            SendMessageToExternal(wakeupsetresults);
        }

        public override void ClearWakeUp(string deliverypointNumber)
        {
            if (this.wakeUpDateSent.IsNullOrWhiteSpace() || this.wakeUpTimeSent.IsNullOrWhiteSpace())
            {
                Log("ERROR: You must set a wake-up first before you can clear it");
            }

            Wakeupclearresults wakeupclearresults = new Wakeupclearresults();
            wakeupclearresults.Room = deliverypointNumber;
            wakeupclearresults.WakeUpDate = this.wakeUpDateSent; // 07/04/2010
            wakeupclearresults.WakeUpTime = this.wakeUpTimeSent; // 07:30:00

            this.wakeUpDateSent = "";
            this.wakeUpTimeSent = "";

            SendMessageToExternal(wakeupclearresults);
        }

        public override void CreateOrder(string deliverypointNumber, List<string> productNames)
        {
            Roombillresults roombillresults = new Roombillresults();
            roombillresults.Room = deliverypointNumber;

            int numberOfProducts = RandomUtil.GetRandomNumber(productNames.Count) + 2;
            roombillresults.Items = new List<BillItem>();
            decimal balance = 0;
            for (int i = 1; i <= numberOfProducts; i++)
            {
                decimal price = (decimal)((RandomUtil.GetRandomNumber(1000) + 100.0) / 100.0);
                balance += price;

                BillItem bi1 = new BillItem();
                bi1.Charge = price.ToStringInvariantCulture("N2");
                bi1.Code = "CODE" + i;
                bi1.Description = string.Format("Charge - {0}", productNames.Pick());
                bi1.DateTime = MessageBase.DateTimeToString(DateTime.Now.AddDays(0 - RandomUtil.GetRandomNumber(8)).AddMinutes(RandomUtil.GetRandomNumber(1440)));

                roombillresults.Items.Add(bi1);
            }
            roombillresults.Balance = balance.ToStringInvariantCulture("N2");

            SendMessageToExternal(roombillresults);
        }

        public override void CheckedInForDevelopment(bool checkedIn)
        {
            ((TigertmsConnector)this.Connector).CheckedInForDevelopmentMode = checkedIn;
        }

        protected override bool OnStart()
        {
            string originalWebserviceToBeCalledOnUrl = this.config.WebserviceToBeCalledOnUrl;
            if(this.config.ExternalNotUsed)
            {
                this.config.WebserviceToBeCalledOnUrl += "/NotToBeUsed";
            }

            this.Connector = new TigertmsConnector(this.config, connector_Logged);

            this.config.WebserviceToBeCalledOnUrl = originalWebserviceToBeCalledOnUrl;

            this.externalWebservice = new TigertmsToBeCalledOn.ExternalWebserviceClient();
            this.externalWebservice.Endpoint.Address = new EndpointAddress(new Uri(this.config.WebserviceToBeCalledOnUrl),
                                                                           this.externalWebservice.Endpoint.Address.Identity,
                                                                           this.externalWebservice.Endpoint.Address.Headers);     
            return true;
        }

        protected override bool OnStop()
        {
            Log("Stopping external webservice");

            this.externalWebservice.Close();
            this.externalWebservice = null;

            return true;
        }

        private void SendMessageToExternal(MessageBase message)
        {
            Task.Factory.StartNew(() =>
            {
                this.externalWebservice.SendMessageToExternalInterface(message.ToXml());
            });
        }
    }
}