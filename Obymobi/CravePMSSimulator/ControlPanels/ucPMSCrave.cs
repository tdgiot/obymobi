﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using CravePMSSimulator.Interfaces;

namespace CravePMSSimulator.ControlPanels
{
    public partial class UcPMSCrave : UserControl
    {
        private IPMSCrave handler;

        public UcPMSCrave()
        {
            InitializeComponent();
        }

        public void SetHandler(IPMSCrave pmsCraveInterface)
        {
            this.handler = pmsCraveInterface;
        }

        private void btPmsToCraveCheckIn_Click(object sender, EventArgs e)
        {
            if (this.handler != null)
            {
                this.handler.CheckIn(this.tbPmsToCraveRoomNumber.Text,
                                     this.tbPmsToCraveFirstName.Text,
                                     this.tbPmsToCraveLastname.Text,
                                     this.cbPmsToCraveAllowViewBill.Checked,
                                     this.cbPmsToCraveAllowExpressCheckout.Checked,
                                     this.tbPmsToCraveLanguage.Text);

                this.cbPmsToCraveCheckedIn.Checked = true;
            }
        }

        private void btPmsToCraveCheckout_Click(object sender, EventArgs e)
        {
            if (this.handler != null)
            {
                this.handler.CheckoutPMS(this.tbPmsToCraveRoomNumber.Text);

                this.cbPmsToCraveCheckedIn.Checked = false;
            }
        }

        private void btPmsToCraveRoomMove_Click(object sender, EventArgs e)
        {
            if (this.handler != null)
            {
                this.handler.RoomMove(this.tbPmsToCraveOldRoomNumber.Text, this.tbPmsToCraveRoomNumber.Text);
            }
        }

        private void btnCreateOrder_Click(object sender, EventArgs e)
        {
            if (this.handler != null)
            {
                List<string> productNames = new List<string> { "Club Sandwich", "Cuba Libre Cocktail", "Champagne Afternoon Tea", "Continental Breakfast", "60 Minute Massage", "Skin Radiance Facials", "Chauffeur Hire" };
                this.handler.CreateOrder(this.tbPmsToCraveRoomNumber.Text, productNames);
            }
        }

        private void btnEditRoomData_Click(object sender, EventArgs e)
        {
            if (this.handler != null)
            {
                this.handler.EditRoomData(this.tbPmsToCraveRoomNumber.Text, this.cbHasVoicemail.Checked);
            }
        }

        private void btnEditGuestInformation_Click(object sender, EventArgs e)
        {
            if (this.handler != null)
            {
                this.handler.EditGuestInformation(this.tbPmsToCraveRoomNumber.Text,
                                                  this.tbPmsToCraveFirstName.Text,
                                                  this.tbPmsToCraveLastname.Text,
                                                  this.cbPmsToCraveAllowViewBill.Checked,
                                                  this.cbPmsToCraveAllowExpressCheckout.Checked,
                                                  this.tbPmsToCraveLanguage.Text);
            }
        }

        private void btnSetWakeUp_Click(object sender, EventArgs e)
        {
            if (this.handler != null)
            {
                this.handler.SetWakeUp(this.tbPmsToCraveRoomNumber.Text);
            }
        }

        private void btnClearWakeUp_Click(object sender, EventArgs e)
        {
            if (this.handler != null)
            {
                this.handler.ClearWakeUp(this.tbPmsToCraveRoomNumber.Text);
            }
        }
    }
}
