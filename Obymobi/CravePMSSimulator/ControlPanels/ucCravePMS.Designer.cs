﻿namespace CravePMSSimulator.ControlPanels
{
    partial class UcCravePMS
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label13 = new System.Windows.Forms.Label();
            this.btCraveToPmsGetGuestInformation = new System.Windows.Forms.Button();
            this.btCraveToPmsGetFolio = new System.Windows.Forms.Button();
            this.tbCraveToPmsRoom = new System.Windows.Forms.MaskedTextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.tbCraveToPmsCheckoutBalance = new System.Windows.Forms.MaskedTextBox();
            this.btCraveToPmsCheckout = new System.Windows.Forms.Button();
            this.tbCraveToPmsAddToFolioPrice = new System.Windows.Forms.MaskedTextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.tbCraveToPmsAddToFolioDescription = new System.Windows.Forms.TextBox();
            this.btCraveToPmsAddToFolio = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(3, 35);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(94, 13);
            this.label13.TabIndex = 34;
            this.label13.Text = "Checkout balance";
            // 
            // btCraveToPmsGetGuestInformation
            // 
            this.btCraveToPmsGetGuestInformation.Location = new System.Drawing.Point(239, 62);
            this.btCraveToPmsGetGuestInformation.Name = "btCraveToPmsGetGuestInformation";
            this.btCraveToPmsGetGuestInformation.Size = new System.Drawing.Size(121, 23);
            this.btCraveToPmsGetGuestInformation.TabIndex = 33;
            this.btCraveToPmsGetGuestInformation.Text = "Get Guest Info";
            this.btCraveToPmsGetGuestInformation.UseVisualStyleBackColor = true;
            this.btCraveToPmsGetGuestInformation.Click += new System.EventHandler(this.btCraveToPmsGetGuestInformation_Click);
            // 
            // btCraveToPmsGetFolio
            // 
            this.btCraveToPmsGetFolio.Location = new System.Drawing.Point(239, 32);
            this.btCraveToPmsGetFolio.Name = "btCraveToPmsGetFolio";
            this.btCraveToPmsGetFolio.Size = new System.Drawing.Size(121, 23);
            this.btCraveToPmsGetFolio.TabIndex = 32;
            this.btCraveToPmsGetFolio.Text = "Get Folio";
            this.btCraveToPmsGetFolio.UseVisualStyleBackColor = true;
            this.btCraveToPmsGetFolio.Click += new System.EventHandler(this.btCraveToPmsGetFolio_Click);
            // 
            // tbCraveToPmsRoom
            // 
            this.tbCraveToPmsRoom.Location = new System.Drawing.Point(103, 7);
            this.tbCraveToPmsRoom.Name = "tbCraveToPmsRoom";
            this.tbCraveToPmsRoom.Size = new System.Drawing.Size(40, 20);
            this.tbCraveToPmsRoom.TabIndex = 31;
            this.tbCraveToPmsRoom.Text = "100";
            this.tbCraveToPmsRoom.ValidatingType = typeof(int);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(3, 9);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(45, 13);
            this.label9.TabIndex = 30;
            this.label9.Text = "Room #";
            // 
            // tbCraveToPmsCheckoutBalance
            // 
            this.tbCraveToPmsCheckoutBalance.Location = new System.Drawing.Point(103, 32);
            this.tbCraveToPmsCheckoutBalance.Name = "tbCraveToPmsCheckoutBalance";
            this.tbCraveToPmsCheckoutBalance.Size = new System.Drawing.Size(67, 20);
            this.tbCraveToPmsCheckoutBalance.TabIndex = 29;
            this.tbCraveToPmsCheckoutBalance.Text = "10.00";
            this.tbCraveToPmsCheckoutBalance.ValidatingType = typeof(int);
            // 
            // btCraveToPmsCheckout
            // 
            this.btCraveToPmsCheckout.Location = new System.Drawing.Point(239, 4);
            this.btCraveToPmsCheckout.Name = "btCraveToPmsCheckout";
            this.btCraveToPmsCheckout.Size = new System.Drawing.Size(121, 23);
            this.btCraveToPmsCheckout.TabIndex = 28;
            this.btCraveToPmsCheckout.Text = "Request Check-out";
            this.btCraveToPmsCheckout.UseVisualStyleBackColor = true;
            this.btCraveToPmsCheckout.Click += new System.EventHandler(this.btCraveToPmsCheckout_Click);
            // 
            // tbCraveToPmsAddToFolioPrice
            // 
            this.tbCraveToPmsAddToFolioPrice.Location = new System.Drawing.Point(471, 32);
            this.tbCraveToPmsAddToFolioPrice.Name = "tbCraveToPmsAddToFolioPrice";
            this.tbCraveToPmsAddToFolioPrice.Size = new System.Drawing.Size(100, 20);
            this.tbCraveToPmsAddToFolioPrice.TabIndex = 27;
            this.tbCraveToPmsAddToFolioPrice.Text = "10.00";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(405, 9);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(60, 13);
            this.label3.TabIndex = 25;
            this.label3.Text = "Description";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(405, 35);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(34, 13);
            this.label4.TabIndex = 26;
            this.label4.Text = "Value";
            // 
            // tbCraveToPmsAddToFolioDescription
            // 
            this.tbCraveToPmsAddToFolioDescription.Location = new System.Drawing.Point(471, 6);
            this.tbCraveToPmsAddToFolioDescription.Name = "tbCraveToPmsAddToFolioDescription";
            this.tbCraveToPmsAddToFolioDescription.Size = new System.Drawing.Size(227, 20);
            this.tbCraveToPmsAddToFolioDescription.TabIndex = 23;
            this.tbCraveToPmsAddToFolioDescription.Text = "Continental breakfast";
            // 
            // btCraveToPmsAddToFolio
            // 
            this.btCraveToPmsAddToFolio.Location = new System.Drawing.Point(577, 32);
            this.btCraveToPmsAddToFolio.Name = "btCraveToPmsAddToFolio";
            this.btCraveToPmsAddToFolio.Size = new System.Drawing.Size(121, 23);
            this.btCraveToPmsAddToFolio.TabIndex = 24;
            this.btCraveToPmsAddToFolio.Text = "Add to Folio";
            this.btCraveToPmsAddToFolio.UseVisualStyleBackColor = true;
            this.btCraveToPmsAddToFolio.Click += new System.EventHandler(this.btCraveToPmsAddToFolio_Click);
            // 
            // UcCravePMS
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.label13);
            this.Controls.Add(this.btCraveToPmsGetGuestInformation);
            this.Controls.Add(this.btCraveToPmsGetFolio);
            this.Controls.Add(this.tbCraveToPmsRoom);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.tbCraveToPmsCheckoutBalance);
            this.Controls.Add(this.btCraveToPmsCheckout);
            this.Controls.Add(this.tbCraveToPmsAddToFolioPrice);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.tbCraveToPmsAddToFolioDescription);
            this.Controls.Add(this.btCraveToPmsAddToFolio);
            this.Name = "UcCravePMS";
            this.Size = new System.Drawing.Size(710, 94);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Button btCraveToPmsGetGuestInformation;
        private System.Windows.Forms.Button btCraveToPmsGetFolio;
        private System.Windows.Forms.MaskedTextBox tbCraveToPmsRoom;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.MaskedTextBox tbCraveToPmsCheckoutBalance;
        private System.Windows.Forms.Button btCraveToPmsCheckout;
        private System.Windows.Forms.MaskedTextBox tbCraveToPmsAddToFolioPrice;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox tbCraveToPmsAddToFolioDescription;
        private System.Windows.Forms.Button btCraveToPmsAddToFolio;
    }
}
