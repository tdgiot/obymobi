﻿using System;
using System.Windows.Forms;
using CravePMSSimulator.Interfaces;

namespace CravePMSSimulator.ControlPanels
{
    public partial class UcCravePMS : UserControl
    {
        private ICravePMS handler;

        public UcCravePMS()
        {
            InitializeComponent();
        }

        public void SetHandler(ICravePMS cravePmsInterface)
        {
            this.handler = cravePmsInterface;
        }

        private void btCraveToPmsCheckout_Click(object sender, EventArgs e)
        {
            if (this.handler != null)
            {
                decimal balance = NumericsUtil.ParseInvariantCulture(this.tbCraveToPmsCheckoutBalance.Text);
                this.handler.Checkout(this.tbCraveToPmsRoom.Text, balance);
            }
        }

        private void btCraveToPmsGetFolio_Click(object sender, EventArgs e)
        {
            if (this.handler != null)
            {
                this.handler.GetFolio(this.tbCraveToPmsRoom.Text); 
            }
        }

        private void btCraveToPmsGetGuestInformation_Click(object sender, EventArgs e)
        {
            if (this.handler != null)
            {
                this.handler.GetGuestInformation(this.tbCraveToPmsRoom.Text);
            }
        }

        private void btCraveToPmsAddToFolio_Click(object sender, EventArgs e)
        {
            if (this.handler != null)
            {
                decimal price = NumericsUtil.ParseInvariantCulture(this.tbCraveToPmsAddToFolioPrice.Text);
                this.handler.AddToFolio(this.tbCraveToPmsRoom.Text, this.tbCraveToPmsAddToFolioDescription.Text, price);
            }
        }
    }
}
