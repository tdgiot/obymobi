﻿namespace CravePMSSimulator.ControlPanels
{
    partial class UcPMSCrave
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnEditRoomData = new System.Windows.Forms.Button();
            this.btnCreateOrder = new System.Windows.Forms.Button();
            this.btnClearWakeUp = new System.Windows.Forms.Button();
            this.btnSetWakeUp = new System.Windows.Forms.Button();
            this.btnEditGuestInformation = new System.Windows.Forms.Button();
            this.cbHasVoicemail = new System.Windows.Forms.CheckBox();
            this.cbPmsToCraveCheckedIn = new System.Windows.Forms.CheckBox();
            this.tbPmsToCraveLanguage = new System.Windows.Forms.TextBox();
            this.tbPmsToCraveLastname = new System.Windows.Forms.TextBox();
            this.tbPmsToCraveFirstName = new System.Windows.Forms.TextBox();
            this.cbPmsToCraveAllowExpressCheckout = new System.Windows.Forms.CheckBox();
            this.cbPmsToCraveAllowViewBill = new System.Windows.Forms.CheckBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tbPmsToCraveOldRoomNumber = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.btPmsToCraveRoomMove = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.tbPmsToCraveRoomNumber = new System.Windows.Forms.TextBox();
            this.btPmsToCraveCheckIn = new System.Windows.Forms.Button();
            this.btPmsToCraveCheckout = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnEditRoomData
            // 
            this.btnEditRoomData.Location = new System.Drawing.Point(260, 100);
            this.btnEditRoomData.Name = "btnEditRoomData";
            this.btnEditRoomData.Size = new System.Drawing.Size(121, 23);
            this.btnEditRoomData.TabIndex = 61;
            this.btnEditRoomData.Text = "Edit Room Data";
            this.btnEditRoomData.UseVisualStyleBackColor = true;
            this.btnEditRoomData.Click += new System.EventHandler(this.btnEditRoomData_Click);
            // 
            // btnCreateOrder
            // 
            this.btnCreateOrder.Location = new System.Drawing.Point(133, 129);
            this.btnCreateOrder.Name = "btnCreateOrder";
            this.btnCreateOrder.Size = new System.Drawing.Size(121, 23);
            this.btnCreateOrder.TabIndex = 60;
            this.btnCreateOrder.Text = "Create Order\r\n";
            this.btnCreateOrder.UseVisualStyleBackColor = true;
            this.btnCreateOrder.Click += new System.EventHandler(this.btnCreateOrder_Click);
            // 
            // btnClearWakeUp
            // 
            this.btnClearWakeUp.Location = new System.Drawing.Point(387, 129);
            this.btnClearWakeUp.Name = "btnClearWakeUp";
            this.btnClearWakeUp.Size = new System.Drawing.Size(121, 23);
            this.btnClearWakeUp.TabIndex = 59;
            this.btnClearWakeUp.Text = "Clear Wake-up";
            this.btnClearWakeUp.UseVisualStyleBackColor = true;
            this.btnClearWakeUp.Click += new System.EventHandler(this.btnClearWakeUp_Click);
            // 
            // btnSetWakeUp
            // 
            this.btnSetWakeUp.Location = new System.Drawing.Point(387, 100);
            this.btnSetWakeUp.Name = "btnSetWakeUp";
            this.btnSetWakeUp.Size = new System.Drawing.Size(121, 23);
            this.btnSetWakeUp.TabIndex = 58;
            this.btnSetWakeUp.Text = "Set Wake-up";
            this.btnSetWakeUp.UseVisualStyleBackColor = true;
            this.btnSetWakeUp.Click += new System.EventHandler(this.btnSetWakeUp_Click);
            // 
            // btnEditGuestInformation
            // 
            this.btnEditGuestInformation.Location = new System.Drawing.Point(260, 129);
            this.btnEditGuestInformation.Name = "btnEditGuestInformation";
            this.btnEditGuestInformation.Size = new System.Drawing.Size(121, 23);
            this.btnEditGuestInformation.TabIndex = 57;
            this.btnEditGuestInformation.Text = "Edit Guest Info";
            this.btnEditGuestInformation.UseVisualStyleBackColor = true;
            this.btnEditGuestInformation.Click += new System.EventHandler(this.btnEditGuestInformation_Click);
            // 
            // cbHasVoicemail
            // 
            this.cbHasVoicemail.AutoSize = true;
            this.cbHasVoicemail.Location = new System.Drawing.Point(523, 9);
            this.cbHasVoicemail.Name = "cbHasVoicemail";
            this.cbHasVoicemail.Size = new System.Drawing.Size(116, 17);
            this.cbHasVoicemail.TabIndex = 56;
            this.cbHasVoicemail.Text = "Voicemail message";
            this.cbHasVoicemail.UseVisualStyleBackColor = true;
            // 
            // cbPmsToCraveCheckedIn
            // 
            this.cbPmsToCraveCheckedIn.AutoSize = true;
            this.cbPmsToCraveCheckedIn.Location = new System.Drawing.Point(523, 35);
            this.cbPmsToCraveCheckedIn.Name = "cbPmsToCraveCheckedIn";
            this.cbPmsToCraveCheckedIn.Size = new System.Drawing.Size(81, 17);
            this.cbPmsToCraveCheckedIn.TabIndex = 55;
            this.cbPmsToCraveCheckedIn.Text = "Checked In";
            this.cbPmsToCraveCheckedIn.UseVisualStyleBackColor = true;
            // 
            // tbPmsToCraveLanguage
            // 
            this.tbPmsToCraveLanguage.Location = new System.Drawing.Point(198, 58);
            this.tbPmsToCraveLanguage.Name = "tbPmsToCraveLanguage";
            this.tbPmsToCraveLanguage.Size = new System.Drawing.Size(151, 20);
            this.tbPmsToCraveLanguage.TabIndex = 54;
            this.tbPmsToCraveLanguage.Text = "EN";
            // 
            // tbPmsToCraveLastname
            // 
            this.tbPmsToCraveLastname.Location = new System.Drawing.Point(198, 32);
            this.tbPmsToCraveLastname.Name = "tbPmsToCraveLastname";
            this.tbPmsToCraveLastname.Size = new System.Drawing.Size(151, 20);
            this.tbPmsToCraveLastname.TabIndex = 53;
            this.tbPmsToCraveLastname.Text = "Norris";
            // 
            // tbPmsToCraveFirstName
            // 
            this.tbPmsToCraveFirstName.Location = new System.Drawing.Point(198, 6);
            this.tbPmsToCraveFirstName.Name = "tbPmsToCraveFirstName";
            this.tbPmsToCraveFirstName.Size = new System.Drawing.Size(151, 20);
            this.tbPmsToCraveFirstName.TabIndex = 52;
            this.tbPmsToCraveFirstName.Text = "Chuck";
            // 
            // cbPmsToCraveAllowExpressCheckout
            // 
            this.cbPmsToCraveAllowExpressCheckout.AutoSize = true;
            this.cbPmsToCraveAllowExpressCheckout.Checked = true;
            this.cbPmsToCraveAllowExpressCheckout.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbPmsToCraveAllowExpressCheckout.Location = new System.Drawing.Point(377, 35);
            this.cbPmsToCraveAllowExpressCheckout.Name = "cbPmsToCraveAllowExpressCheckout";
            this.cbPmsToCraveAllowExpressCheckout.Size = new System.Drawing.Size(140, 17);
            this.cbPmsToCraveAllowExpressCheckout.TabIndex = 51;
            this.cbPmsToCraveAllowExpressCheckout.Text = "Allow Express Checkout";
            this.cbPmsToCraveAllowExpressCheckout.UseVisualStyleBackColor = true;
            // 
            // cbPmsToCraveAllowViewBill
            // 
            this.cbPmsToCraveAllowViewBill.AutoSize = true;
            this.cbPmsToCraveAllowViewBill.Checked = true;
            this.cbPmsToCraveAllowViewBill.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbPmsToCraveAllowViewBill.Location = new System.Drawing.Point(377, 9);
            this.cbPmsToCraveAllowViewBill.Name = "cbPmsToCraveAllowViewBill";
            this.cbPmsToCraveAllowViewBill.Size = new System.Drawing.Size(93, 17);
            this.cbPmsToCraveAllowViewBill.TabIndex = 50;
            this.cbPmsToCraveAllowViewBill.Text = "Allow View Bill";
            this.cbPmsToCraveAllowViewBill.UseVisualStyleBackColor = true;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(137, 61);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(55, 13);
            this.label14.TabIndex = 49;
            this.label14.Text = "Language";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(139, 35);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 13);
            this.label2.TabIndex = 48;
            this.label2.Text = "Lastname";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(140, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(52, 13);
            this.label1.TabIndex = 47;
            this.label1.Text = "Firstname";
            // 
            // tbPmsToCraveOldRoomNumber
            // 
            this.tbPmsToCraveOldRoomNumber.Location = new System.Drawing.Point(72, 32);
            this.tbPmsToCraveOldRoomNumber.Name = "tbPmsToCraveOldRoomNumber";
            this.tbPmsToCraveOldRoomNumber.Size = new System.Drawing.Size(55, 20);
            this.tbPmsToCraveOldRoomNumber.TabIndex = 46;
            this.tbPmsToCraveOldRoomNumber.Text = "15";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(3, 35);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(59, 13);
            this.label7.TabIndex = 45;
            this.label7.Text = "Old room #";
            // 
            // btPmsToCraveRoomMove
            // 
            this.btPmsToCraveRoomMove.Location = new System.Drawing.Point(133, 100);
            this.btPmsToCraveRoomMove.Name = "btPmsToCraveRoomMove";
            this.btPmsToCraveRoomMove.Size = new System.Drawing.Size(121, 23);
            this.btPmsToCraveRoomMove.TabIndex = 44;
            this.btPmsToCraveRoomMove.Text = "Room Move";
            this.btPmsToCraveRoomMove.UseVisualStyleBackColor = true;
            this.btPmsToCraveRoomMove.Click += new System.EventHandler(this.btPmsToCraveRoomMove_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(17, 9);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(45, 13);
            this.label6.TabIndex = 43;
            this.label6.Text = "Room #";
            // 
            // tbPmsToCraveRoomNumber
            // 
            this.tbPmsToCraveRoomNumber.Location = new System.Drawing.Point(72, 6);
            this.tbPmsToCraveRoomNumber.Name = "tbPmsToCraveRoomNumber";
            this.tbPmsToCraveRoomNumber.Size = new System.Drawing.Size(55, 20);
            this.tbPmsToCraveRoomNumber.TabIndex = 42;
            this.tbPmsToCraveRoomNumber.Text = "1";
            // 
            // btPmsToCraveCheckIn
            // 
            this.btPmsToCraveCheckIn.Location = new System.Drawing.Point(6, 100);
            this.btPmsToCraveCheckIn.Name = "btPmsToCraveCheckIn";
            this.btPmsToCraveCheckIn.Size = new System.Drawing.Size(121, 23);
            this.btPmsToCraveCheckIn.TabIndex = 41;
            this.btPmsToCraveCheckIn.Text = "Check-in";
            this.btPmsToCraveCheckIn.UseVisualStyleBackColor = true;
            this.btPmsToCraveCheckIn.Click += new System.EventHandler(this.btPmsToCraveCheckIn_Click);
            // 
            // btPmsToCraveCheckout
            // 
            this.btPmsToCraveCheckout.Location = new System.Drawing.Point(6, 129);
            this.btPmsToCraveCheckout.Name = "btPmsToCraveCheckout";
            this.btPmsToCraveCheckout.Size = new System.Drawing.Size(121, 23);
            this.btPmsToCraveCheckout.TabIndex = 40;
            this.btPmsToCraveCheckout.Text = "Check-out";
            this.btPmsToCraveCheckout.UseVisualStyleBackColor = true;
            this.btPmsToCraveCheckout.Click += new System.EventHandler(this.btPmsToCraveCheckout_Click);
            // 
            // UcPMSCrave
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.btnEditRoomData);
            this.Controls.Add(this.btnCreateOrder);
            this.Controls.Add(this.btnClearWakeUp);
            this.Controls.Add(this.btnSetWakeUp);
            this.Controls.Add(this.btnEditGuestInformation);
            this.Controls.Add(this.cbHasVoicemail);
            this.Controls.Add(this.cbPmsToCraveCheckedIn);
            this.Controls.Add(this.tbPmsToCraveLanguage);
            this.Controls.Add(this.tbPmsToCraveLastname);
            this.Controls.Add(this.tbPmsToCraveFirstName);
            this.Controls.Add(this.cbPmsToCraveAllowExpressCheckout);
            this.Controls.Add(this.cbPmsToCraveAllowViewBill);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tbPmsToCraveOldRoomNumber);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.btPmsToCraveRoomMove);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.tbPmsToCraveRoomNumber);
            this.Controls.Add(this.btPmsToCraveCheckIn);
            this.Controls.Add(this.btPmsToCraveCheckout);
            this.Name = "UcPMSCrave";
            this.Size = new System.Drawing.Size(931, 166);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnEditRoomData;
        private System.Windows.Forms.Button btnCreateOrder;
        private System.Windows.Forms.Button btnClearWakeUp;
        private System.Windows.Forms.Button btnSetWakeUp;
        private System.Windows.Forms.Button btnEditGuestInformation;
        private System.Windows.Forms.CheckBox cbHasVoicemail;
        private System.Windows.Forms.CheckBox cbPmsToCraveCheckedIn;
        private System.Windows.Forms.TextBox tbPmsToCraveLanguage;
        private System.Windows.Forms.TextBox tbPmsToCraveLastname;
        private System.Windows.Forms.TextBox tbPmsToCraveFirstName;
        private System.Windows.Forms.CheckBox cbPmsToCraveAllowExpressCheckout;
        private System.Windows.Forms.CheckBox cbPmsToCraveAllowViewBill;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbPmsToCraveOldRoomNumber;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button btPmsToCraveRoomMove;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox tbPmsToCraveRoomNumber;
        private System.Windows.Forms.Button btPmsToCraveCheckIn;
        private System.Windows.Forms.Button btPmsToCraveCheckout;
    }
}
