﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Mvc;

using Google.Apis.Auth.OAuth2.Mvc;
using Google.Apis.Drive.v2;
using Google.Apis.Services;
using Google.Apis.Analytics.v3;
using System.Diagnostics;
using AnalyticsDemoWeb.Analytics;
using AnalyticsDemoWeb.Models.Home;

namespace AnalyticsDemoWeb.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        public async Task<object> Index(CancellationToken cancellationToken)
        {
            // These actions can take long. Might want this a background task or at least do something in UI to ask the user for some ptience. But you could also just fire a Task in the background and tell 
            // the user it might take up to 5 minutes before it's completed. (Easiest)
            this.HttpContext.Server.ScriptTimeout = 300;
            
            var apiUtil = new GoogleApiUtil(this);
                
            ViewBag.GoogleLinked = await apiUtil.IsAuthenticated(cancellationToken);

            if ((bool)ViewBag.GoogleLinked)
            {
                AnalyticsOverviewViewModel model = new AnalyticsOverviewViewModel();

                GoogleAnalyticsApiUtil gaApiUtil = new GoogleAnalyticsApiUtil(this);

                foreach (var account in await gaApiUtil.GetAccounts(cancellationToken))
                {
                    var accountModel = new AnalyticsOverviewViewModel.Account { Id = account.Id, Name = account.Name };

                    foreach (var property in await gaApiUtil.GetProperties(account.Id, cancellationToken))
                    {
                        bool isValid = await gaApiUtil.ValidateCustomDimensions(property.AccountId, property.Id, cancellationToken, false);
                        accountModel.Properties.Add(new AnalyticsOverviewViewModel.Property { Id = property.Id, Name = property.Name, AccountId = account.Id, ValidCustomDimensions = isValid });
                    }

                    model.Accounts.Add(accountModel);
                }

                // Also it would probably make sense to cache the results with an automated refresh of X minutes + offering the user a button to do a hard refresh on demand.

                return View(model);
            }
            else
            {
                return View();
            }
        }

        public async Task<ActionResult> FixCustomDimensions(string propertyId, string accountId, CancellationToken cancellationtoken)
        {
            // These actions can take long. Might want this a background task or at least do something in UI to ask the user for some ptience. But you could also just fire a Task in the background and tell 
            // the user it might take up to 5 minutes before it's completed. (Easiest)
            this.HttpContext.Server.ScriptTimeout = 300;
            GoogleAnalyticsApiUtil gaApiUtil = new GoogleAnalyticsApiUtil(this);
            await gaApiUtil.ValidateCustomDimensions(accountId, propertyId, cancellationtoken, true);

            return RedirectToAction("Index");
        }
    }
}