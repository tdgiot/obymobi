﻿using AnalyticsDemoWeb.Analytics;
using Google.Apis.Auth.OAuth2.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace AnalyticsDemoWeb.Controllers
{
    public class OAuthController : Controller
    {
        // GET: OAuth
        public async Task<object> Index(CancellationToken cancellationToken)
        {
            var apiUtil = new GoogleApiUtil(this);
            ViewBag.GoogleLinked = await apiUtil.IsAuthenticated();

            if ((bool)ViewBag.GoogleLinked)
                ViewBag.GoogleEmail = await apiUtil.GetEmail(cancellationToken);

            return View();
        }

        public async Task<object> LinkGoogle(CancellationToken cancellationToken)
        {
            var apiUtil = new GoogleApiUtil(this);
            return await apiUtil.GetLinkRedirect(cancellationToken);
        }

        public async Task<ActionResult> UnlinkGoogle(CancellationToken cancellationToken)
        {
            var apiUtil = new GoogleApiUtil(this);
            await apiUtil.Unlink(cancellationToken);
            return RedirectToAction("index");
        }
    }
}