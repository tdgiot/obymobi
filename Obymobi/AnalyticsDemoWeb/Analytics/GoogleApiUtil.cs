﻿using Google.Apis.Analytics.v3;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Auth.OAuth2.Flows;
using Google.Apis.Auth.OAuth2.Mvc;
using Google.Apis.Auth.OAuth2.Web;
using Google.Apis.Drive.v2;
using Google.Apis.Oauth2.v2;
using Google.Apis.Plus.v1;
using Google.Apis.Services;
using Google.Apis.Util.Store;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace AnalyticsDemoWeb.Analytics
{
    public class GoogleApiUtil
    {
        private const string ApplicationName = "Crave Platform";
        private Controller controller;        
        private AuthorizationCodeWebApp authorizor;

        public GoogleApiUtil(Controller controller, string authenticationCallBackFullUrl = "", string postLinkRedirectFullUrl = "")
        {
            if (!controller.User.Identity.IsAuthenticated)
            {
                Debug.WriteLine("Can't use the GoogleApiUtil when not authenticated.");
                throw new InvalidOperationException("Can't use the GoogleApiUtil when not authenticated.");
            }

            this.controller = controller;

            // In the future this could be done using config values or something but: YAGNI, and most probably only used once
            // per web application and specific to that application to could be constants without a problem. Or just stick with
            // convention of this class :)
            if(string.IsNullOrWhiteSpace(authenticationCallBackFullUrl))
                authenticationCallBackFullUrl = this.controller.Url.Action("IndexAsync", "AuthCallback", null, this.controller.Url.RequestContext.HttpContext.Request.Url.Scheme); // http://stackoverflow.com/a/6784004/2104

            if (string.IsNullOrWhiteSpace(postLinkRedirectFullUrl))
                postLinkRedirectFullUrl = this.controller.Url.Action("Index", "OAuth", null, this.controller.Url.RequestContext.HttpContext.Request.Url.Scheme); // http://stackoverflow.com/a/6784004/2104

            this.authorizor = new AuthorizationCodeWebApp(new AppFlowMetadata().Flow, authenticationCallBackFullUrl, postLinkRedirectFullUrl);
        }

        public async Task<string> GetEmail(CancellationToken cancellationToken)
        {
            var authResult = await GetAuthResult(cancellationToken);

            var service = new Oauth2Service(await this.GetBaseClientService(cancellationToken));
            var userInfo = await service.Userinfo.Get().ExecuteAsync();

            return userInfo.Email;
        }

        public async Task<bool> IsAuthenticated(CancellationToken cancellationToken = new CancellationToken())
        {
            var authResult = await GetAuthResult(cancellationToken);
            return (authResult.Credential != null);
        }

        public async Task<RedirectResult> GetLinkRedirect(CancellationToken cancellationToken)
        {
            AuthorizationCodeWebApp.AuthResult result = await authorizor.AuthorizeAsync(controller.User.Identity.Name, cancellationToken);
            return new RedirectResult(result.RedirectUri);
        }

        public async Task Unlink(CancellationToken cancellationToken)
        {
            var authResult = await GetAuthResult(cancellationToken);

            if (authResult.Credential != null && controller.User.Identity.IsAuthenticated)
                await new AuthorizationCodeMvcApp(controller, new AppFlowMetadata()).Flow.RevokeTokenAsync(controller.User.Identity.Name, authResult.Credential.Token.AccessToken, cancellationToken);
        }

        public async Task<BaseClientService.Initializer> GetBaseClientService(CancellationToken cancellationToken)
        {
            var authResult = await this.GetAuthResult(cancellationToken);

            if (authResult.Credential == null)
                throw new InvalidOperationException("Can't use GetBaseClientService when not linked to a Google Account");

            return new BaseClientService.Initializer
            {
                HttpClientInitializer = authResult.Credential,
                ApplicationName = GoogleApiUtil.ApplicationName
            };
        }

        private async Task<AuthorizationCodeWebApp.AuthResult> GetAuthResult(CancellationToken cancellationToken)
        {        
            return await authorizor.AuthorizeAsync(controller.User.Identity.Name, cancellationToken);
        }                

        public class AppFlowMetadata : FlowMetadata
        {
            private static readonly IAuthorizationCodeFlow flow =
                new GoogleAuthorizationCodeFlow(new GoogleAuthorizationCodeFlow.Initializer
                {
                    ClientSecrets = new ClientSecrets
                    {                     
                        ClientId = ConfigurationManager.AppSettings["GoogleApisClientId"],
                        ClientSecret = ConfigurationManager.AppSettings["GoogleApisClientSecret"]
                    },
                    Scopes = new[] { AnalyticsService.Scope.Analytics, AnalyticsService.Scope.AnalyticsEdit, Oauth2Service.Scope.UserinfoEmail, PlusService.Scope.UserinfoEmail }, // YAGNI: Configureable. (https://developers.google.com/identity/protocols/OAuth2WebServer#incrementalAuth)
                    
                    // TODO: Normal storage & investigate why and how.
                    DataStore = new FileDataStore(System.Web.Hosting.HostingEnvironment.MapPath("~/App_Data/Google.Api.Auth.Store"), true),
                });

            public override string GetUserId(Controller controller)
            {
                return controller.User.Identity.Name;
            }

            public override IAuthorizationCodeFlow Flow
            {
                get { return flow; }
            }
        }

    }
}