﻿using Google.Apis.Analytics.v3;
using Google.Apis.Analytics.v3.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace AnalyticsDemoWeb.Analytics
{
    public class GoogleAnalyticsApiUtil
    {
        GoogleApiUtil apiUtil;

        public GoogleAnalyticsApiUtil(Controller controller)
        {
            apiUtil = new GoogleApiUtil(controller);
        }

        public async Task<IList<Account>> GetAccounts(CancellationToken cancellationToken)
        {
            var service = await this.GetAnalyticsService(cancellationToken);            
            var accountsResult = await service.Management.Accounts.List().ExecuteAsync(cancellationToken);
            return accountsResult.Items;
        }

        // It's a design choice to use strings instead of the full 'Account' object, as this means on a postback
        // you don't have to re-retrieve an acocunt just to get it's properties.
        public async Task<IList<Webproperty>> GetProperties(string accountId, CancellationToken cancellationToken)
        {            
            var service = await this.GetAnalyticsService(cancellationToken);
            var propertiesResult = await service.Management.Webproperties.List(accountId).ExecuteAsync(cancellationToken);
            return propertiesResult.Items;
        }

        public async Task<IList<CustomDimension>> GetCustomDimensions(string accountId, string webPropertyId, CancellationToken cancellationToken)
        {
            var service = await this.GetAnalyticsService(cancellationToken);
            var dimensionsResult = await service.Management.CustomDimensions.List(accountId, webPropertyId).ExecuteAsync(cancellationToken);
            return dimensionsResult.Items ?? new List<CustomDimension>();
        }

        public async Task<bool> ValidateCustomDimensions(string accountId, string webPropertyId, CancellationToken cancellationToken, bool fixIssues = false)
        {
            // The assumption that we make is that the Dimensions are always configured in a continous sequence of id's (i.e. 1, 2, 3, 4, etc.)            
            bool isValid = true;

            // Hardcoded prevention to break shit on Crave Interactive Production - Crave World Live
            var dimensionTemplates = this.GetCustomDimensionTemplates();
            var service = await this.GetAnalyticsService(cancellationToken);
            IList<CustomDimension> dimensions = await this.GetCustomDimensions(accountId, webPropertyId, cancellationToken);

            for (int i = dimensions.Count - 1; i >= 0; i--)
            {
                CustomDimension dimension = dimensions[i];

                if (!dimension.Index.HasValue)
                    continue; // Should never happen, and we can't handle it - leave it.

                // Do we need this dimension?
                bool needsUpdate = false;
                bool dimensionActive = dimension.Active ?? false;

                CustomDimensionTemplate template = dimensionTemplates.FirstOrDefault(x => x.Index == dimension.Index);
                if (template == null)
                {
                    // This dimension is configured but not Template exists for it, so deactivate. (But it's not invalid, as this gives no issues - just an additional dimension)
                    if (dimensionActive)
                    {
                        dimension.Active = false;
                        needsUpdate = true;
                    }
                }
                else if (!(dimension.Name.Equals(template.Name) && dimensionActive))
                {
                    // This dimension is configured but not according to our Template
                    dimension.Name = template.Name;
                    dimension.Active = true;
                    needsUpdate = true;

                    // If we need to fix it means we are not valid: 
                    isValid = false;
                }

                if (needsUpdate && fixIssues)
                {
                    await service.Management.CustomDimensions.Update(dimension, accountId, webPropertyId, dimension.Id).ExecuteAsync();                    
                }

                // Remove a 'used' template because it's taken care of, this means we keep a remainder that we
                // still need to set at Google 
                if (template != null)
                    dimensionTemplates.Remove(template);
            }

            // All Template that are still in our collection were not found at Google, so must be created
            if (dimensionTemplates.Count > 0)
                isValid = false; // We still need to add items, so invalid.

            if (fixIssues)
            {
                foreach (var template in dimensionTemplates.OrderBy(x => x.Index))
                {
                    CustomDimension cd = new CustomDimension();
                    cd.Name = template.Name;
                    cd.Index = template.Index;
                    cd.Active = true;
                    cd.Scope = "HIT";
                    await service.Management.CustomDimensions.Insert(cd, accountId, webPropertyId).ExecuteAsync();
                }
            }

            return isValid;
        }

        //public async Task ApplyCustomDimensions(string accountId, string webPropertyId, CancellationToken cancellationToken)
        //{
        //    // The assumption that we make is that the Dimensions are always configured in a continous sequence of id's (i.e. 1, 2, 3, 4, etc.)            

        //    // Hardcoded prevention to break shit on Crave Interactive Production - Crave World Live
        //    var dimensionTemplates = this.GetCustomDimensionTemplates();
        //    var service = await this.GetAnalyticsService(cancellationToken);
        //    IList<CustomDimension> dimensions = await this.GetCustomDimensions(accountId, webPropertyId, cancellationToken);

        //    for (int i = dimensions.Count - 1; i >= 0; i--)
        //    {
        //        CustomDimension dimension = dimensions[i];

        //        if (!dimension.Index.HasValue)
        //            continue; // Should never happen, and we can't handle it - leave it.

        //        // Do we need this dimension?
        //        bool needsUpdate = false;
        //        bool dimensionActive = dimension.Active ?? false;

        //        CustomDimensionTemplate template = dimensionTemplates.FirstOrDefault(x => x.Index == dimension.Index);
        //        if (template == null && dimensionActive)
        //        {
        //            // This dimension is configured but not Template exists for it, so deactivate.
        //            dimension.Active = false;
        //            needsUpdate = true;
        //        }
        //        else if (!(dimension.Name.Equals(template.Name) && dimensionActive))
        //        {
        //            // This dimension is configured but not according to our Template
        //            dimension.Name = template.Name;
        //            dimension.Active = true;
        //            needsUpdate = true;
        //        }

        //        if (needsUpdate)
        //        {
        //            await service.Management.CustomDimensions.Update(dimension, accountId, webPropertyId, dimension.Id).ExecuteAsync();
        //        }

        //        // Remove a 'used' template because it's taken care of, this means we keep a remainder that we
        //        // still need to set at Google 
        //        if (template != null)
        //            dimensionTemplates.Remove(template);
        //    }

        //    // All Template that are still in our collection were not found at Google, so must be created
        //    foreach (var template in dimensionTemplates.OrderBy(x => x.Index))
        //    {
        //        CustomDimension cd = new CustomDimension();
        //        cd.Name = template.Name;
        //        cd.Index = template.Index;
        //        cd.Active = true;
        //        cd.Scope = "HIT";
        //        await service.Management.CustomDimensions.Insert(cd, accountId, webPropertyId).ExecuteAsync();
        //    }
        //}

        private AnalyticsService analyticsService = null;
        private async Task<AnalyticsService> GetAnalyticsService(CancellationToken cancellationToken)
        {
            if (this.analyticsService == null)
            {
                var baseClient = await this.apiUtil.GetBaseClientService(cancellationToken);
                this.analyticsService = new AnalyticsService(baseClient);
            }                

            return this.analyticsService;
        }

        private class CustomDimensionTemplate
        {
            public CustomDimensionTemplate(int index, string Name)
            {
                this.Index = index;
                this.Name = Name;
            }

            public int Index;
            public string Name;

        }

        private List<CustomDimensionTemplate> GetCustomDimensionTemplates()
        {
            List<CustomDimensionTemplate> toReturn = new List<CustomDimensionTemplate>();
            toReturn.Add(new CustomDimensionTemplate(1, "LegacyCustomDimension1"));
            toReturn.Add(new CustomDimensionTemplate(10, "EntertainmentName"));
            toReturn.Add(new CustomDimensionTemplate(11, "ApplicationType"));
            toReturn.Add(new CustomDimensionTemplate(12, "DeliverypointNumber"));
            toReturn.Add(new CustomDimensionTemplate(13, "DeliverypointGroupName"));
            toReturn.Add(new CustomDimensionTemplate(14, "PrimaryKeys"));
            toReturn.Add(new CustomDimensionTemplate(15, "IsTablet"));
            toReturn.Add(new CustomDimensionTemplate(16, "OperatingSystem"));
            toReturn.Add(new CustomDimensionTemplate(17, "CompanyName"));
            toReturn.Add(new CustomDimensionTemplate(18, "LegacyApplicationVersion"));
            toReturn.Add(new CustomDimensionTemplate(19, "NavigationSource"));
            toReturn.Add(new CustomDimensionTemplate(2, "LegacyCustomDimension2"));
            toReturn.Add(new CustomDimensionTemplate(20, "LegacyApplicationName"));
            toReturn.Add(new CustomDimensionTemplate(21, "HitVersion"));
            toReturn.Add(new CustomDimensionTemplate(22, "CompanyId"));
            toReturn.Add(new CustomDimensionTemplate(23, "ProductId"));
            toReturn.Add(new CustomDimensionTemplate(24, "CategoryId"));
            toReturn.Add(new CustomDimensionTemplate(25, "EntertainmentId"));
            toReturn.Add(new CustomDimensionTemplate(26, "DeliverypointId"));
            toReturn.Add(new CustomDimensionTemplate(27, "DeliverypointName"));
            toReturn.Add(new CustomDimensionTemplate(28, "AdvertisementId"));
            toReturn.Add(new CustomDimensionTemplate(29, "AdvertisementName"));
            toReturn.Add(new CustomDimensionTemplate(3, "MacAddress"));
            toReturn.Add(new CustomDimensionTemplate(30, "AdvertisementLocation"));
            toReturn.Add(new CustomDimensionTemplate(31, "SiteId"));
            toReturn.Add(new CustomDimensionTemplate(32, "SiteName"));
            toReturn.Add(new CustomDimensionTemplate(33, "PageId"));
            toReturn.Add(new CustomDimensionTemplate(34, "PageName"));
            toReturn.Add(new CustomDimensionTemplate(35, "TabId"));
            toReturn.Add(new CustomDimensionTemplate(36, "TabName"));
            toReturn.Add(new CustomDimensionTemplate(37, "MessageId"));
            toReturn.Add(new CustomDimensionTemplate(38, "MessageName"));
            toReturn.Add(new CustomDimensionTemplate(39, "RoomControlAreaId"));
            toReturn.Add(new CustomDimensionTemplate(4, "DeviceType"));
            toReturn.Add(new CustomDimensionTemplate(40, "RoomControlAreaName"));
            toReturn.Add(new CustomDimensionTemplate(41, "WidgetId"));
            toReturn.Add(new CustomDimensionTemplate(42, "WidgetName"));
            toReturn.Add(new CustomDimensionTemplate(43, "WidgetCaption"));
            toReturn.Add(new CustomDimensionTemplate(44, "DeliverypointGroupId"));
            toReturn.Add(new CustomDimensionTemplate(45, "ClientId"));
            toReturn.Add(new CustomDimensionTemplate(46, "OrderSource"));
            toReturn.Add(new CustomDimensionTemplate(47, "Action"));
            toReturn.Add(new CustomDimensionTemplate(48, "AttachmentName"));
            toReturn.Add(new CustomDimensionTemplate(49, "AttachmentId"));
            toReturn.Add(new CustomDimensionTemplate(5, "TimeStamp"));
            toReturn.Add(new CustomDimensionTemplate(50, "RoomControlAreaSectionId"));
            toReturn.Add(new CustomDimensionTemplate(51, "RoomControlAreaSectionName"));
            toReturn.Add(new CustomDimensionTemplate(52, "RoomControlAreaSectionItemId"));
            toReturn.Add(new CustomDimensionTemplate(53, "RoomControlAreaSectionItemName"));
            toReturn.Add(new CustomDimensionTemplate(54, "RoomControlSceneName"));
            toReturn.Add(new CustomDimensionTemplate(55, "RoomControlComponentName"));
            toReturn.Add(new CustomDimensionTemplate(56, "RoomControlComponentId"));
            toReturn.Add(new CustomDimensionTemplate(57, "RoomControlStationListingId"));
            toReturn.Add(new CustomDimensionTemplate(58, "RoomControlStationListingName"));
            toReturn.Add(new CustomDimensionTemplate(59, "RoomControlThermostatMode"));
            toReturn.Add(new CustomDimensionTemplate(6, "ProductName"));
            toReturn.Add(new CustomDimensionTemplate(60, "RoomControlThermostatScale"));
            toReturn.Add(new CustomDimensionTemplate(61, "RoomControlThermostatTemperature"));
            toReturn.Add(new CustomDimensionTemplate(62, "RoomControlThermostatFanMode"));
            toReturn.Add(new CustomDimensionTemplate(63, "RoomControlStationId"));
            toReturn.Add(new CustomDimensionTemplate(64, "RoomControlStationName"));
            toReturn.Add(new CustomDimensionTemplate(65, "RoomControlTvRemoteAction"));
            toReturn.Add(new CustomDimensionTemplate(66, "MediaId"));
            toReturn.Add(new CustomDimensionTemplate(67, "OrderId"));
            toReturn.Add(new CustomDimensionTemplate(68, "OrderitemId"));
            toReturn.Add(new CustomDimensionTemplate(69, "TestPrefix"));
            toReturn.Add(new CustomDimensionTemplate(7, "AlterationName"));
            toReturn.Add(new CustomDimensionTemplate(8, "ServiceRequestName"));
            toReturn.Add(new CustomDimensionTemplate(9, "CategoryName"));            

            return toReturn;
        }

        // TODO: Later we should have a central place for the Custom Dimenions and use the Code Generator to generate Enums or something like that for Java (Android) and .NET (ASP.NET / Xamarin)
      
    }
}