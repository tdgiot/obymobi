﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AnalyticsDemoWeb.Models.Home
{
    public class AnalyticsOverviewViewModel
    {            
        public List<Account> Accounts { get; set; } = new List<Account>();
            
            
        public class Account
        {
            public string Id { get; set; }
            public string Name { get; set; }
            public List<Property> Properties { get; set; } = new List<Property>();
        }

        public class Property
        {
            public string Id { get; set; }
            public string Name { get; set; }
            public string AccountId { get; set; }
            public bool ValidCustomDimensions { get; set; }
        }
    }
}