﻿using NUnit.Framework;

namespace Obymobi.Integrations.Service.Alice.Tests
{
    [TestFixture, Explicit]
    [Category("Integration"), Category("Alice")]
    public class AliceApiTests
    {
        [Test]
        public void GetFacilities()
        {
            StaffApi staffApi = CreateStaffApi();

            var facilities = staffApi.GetFacilities();
        }

        [Test]
        public void GetServices()
        {
            StaffApi staffApi = CreateStaffApi();

            var services = staffApi.GetServices();
        }

        [Test]
        public void GetLocations()
        {
            StaffApi staffApi = CreateStaffApi();

            var locations = staffApi.GetLocations();
        }

        private StaffApi CreateStaffApi()
        {
            string baseUrl = "https://rapi.aliceapp.com";

            string apiKey = "67d98fca-0ccb-458b-bf20-497e4886e51f";
            string hotelId = "437";
            string username = "crave_sand_api";
            string password = "12NZXIatKvKt";

            return new StaffApi(baseUrl, apiKey, username, password, hotelId);
        }
    }
}
