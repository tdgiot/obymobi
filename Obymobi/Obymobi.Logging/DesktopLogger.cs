﻿using System;
using System.Collections.Generic;
using System.IO;
using Dionysos;

namespace Obymobi.Logging
{
    /// <summary>
    /// Logger for Desktop / Service applications
    /// </summary>
    public static class DesktopLogger
    {
        private static LoggingLevel loggingLevel = LoggingLevel.Verbose;
        private static readonly object mutex = new object();
        private const string LogsDir = "Logs";

        /// <summary>
        /// Prefix for the log file. Usefull when you want to create multiple log files.
        /// </summary>
        public static string FilePrefix = "";

        /// <summary>
        /// Gets the filename.
        /// </summary>
        /// <param name="prefix">The prefix.</param>
        /// <returns></returns>
        public static string GetFilename(string prefix = "")
        {
            DateTime utcNow = DateTime.UtcNow;
            string dir = DesktopLogger.CreateDirectory(DesktopLogger.LogsDir);
            return Path.Combine(dir, string.Format("{0}{1}{2}{3}.log", prefix, utcNow.Year.ToString("0000"), utcNow.Month.ToString("00"), utcNow.Day.ToString("00")));
        }

        /// <summary>
        /// Set loggin level value
        /// </summary>
        /// <param name="level"></param>
        public static void SetLoggingLevel(LoggingLevel level)
        {
            loggingLevel = level;
        }

        /// <summary>
        /// Writes the specified contents to the log file with logging level 'Verbose'
        /// </summary>
        /// <param name="contents">The contents to write to the logfile</param>
        /// <param name="args">The args.</param>
        public static void Error(string contents, params object[] args)
        {
            DesktopLogger.Log(contents, LoggingLevel.Error, args);
        }

        /// <summary>
        /// Writes the specified contents to the log file with logging level 'Verbose'
        /// </summary>
        /// <param name="contents">The contents to write to the logfile</param>
        /// <param name="args">The args.</param>
        public static void Warning(string contents, params object[] args)
        {
            DesktopLogger.Log(contents, LoggingLevel.Warning, args);
        }

        /// <summary>
        /// Writes the specified contents to the log file with logging level 'Verbose'
        /// </summary>
        /// <param name="contents">The contents to write to the logfile</param>
        /// <param name="args">The args.</param>
        public static void Info(string contents, params object[] args)
        {
            DesktopLogger.Log(contents, LoggingLevel.Info, args);
        }

        /// <summary>
        /// Writes the specified contents to the log file with logging level 'Verbose'
        /// </summary>
        /// <param name="contents">The contents to write to the logfile</param>
        /// <param name="args">The args.</param>
        public static void Debug(string contents, params object[] args)
        {
            DesktopLogger.Log(contents, LoggingLevel.Debug, args);
        }

        /// <summary>
        /// Writes the specified contents to the log file with logging level 'Verbose'
        /// </summary>
        /// <param name="contents">The contents to write to the logfile</param>
        /// <param name="args">The args.</param>
        public static void Verbose(string contents, params object[] args)
        {
            DesktopLogger.Log(contents, LoggingLevel.Verbose, args);
        }

        /// <summary>
        /// Writes the specified contents to the log file
        /// </summary>
        /// <param name="contents">The contents to write to the logfile</param>
        /// <param name="level">The level.</param>
        /// <param name="args">The args.</param>
        private static void Log(string contents, LoggingLevel level, params object[] args)
        {
            if (level >= DesktopLogger.loggingLevel)
            {
                string formattedText = string.Empty;
                try
                {
                    formattedText = string.Format(contents, args);
                }
                catch
                {
                    formattedText = string.Format("String Format Failed. Format: '{0}', Args: {1}", contents, StringUtil.CombineWithSeperator(", ", args));
                }

                if (TestUtil.IsPcGabriel)
                    Dionysos.Diagnostics.Debug.WriteLine(formattedText);
                else
                    System.Diagnostics.Debug.WriteLine(formattedText);

                // Create the Logs directory if it doesn't exists yet
                string dir = DesktopLogger.CreateDirectory(DesktopLogger.LogsDir);

                DateTime now = DateTime.Now;

                lock (mutex)
                {
                    // Set up a filestream
                    FileStream fs = new FileStream(Path.Combine(dir, string.Format("{0}{1}{2}{3}.log", FilePrefix, now.Year.ToString("0000"), now.Month.ToString("00"), now.Day.ToString("00"))), FileMode.OpenOrCreate, FileAccess.Write);

                    // Set up a streamwriter for adding text
                    StreamWriter sw = new StreamWriter(fs);

                    // Find the end of the underlying filestream
                    sw.BaseStream.Seek(0, SeekOrigin.End);

                    // Add the text
                    try
                    {
                        sw.WriteLine(string.Format("{0}:{1}:{2}\t{3}", now.Hour.ToString("00"), now.Minute.ToString("00"), now.Second.ToString("00"), string.Format(contents, args)));
                    }
                    catch
                    {
                        sw.WriteLine("String.Format failed");
                        sw.WriteLine(string.Format("{0}:{1}:{2}", now.Hour.ToString("00"), now.Minute.ToString("00"), now.Second.ToString("00")));
                        sw.WriteLine(contents);
                        sw.WriteLine("Args: ", StringUtil.CombineWithSeperator(", ", args));
                    }
                    // Add the text to the underlying filestream
                    sw.Flush();
                    // Close the writer
                    sw.Close();
                }
            }
        }

        public static string ReadLog(string path)
        {
            lock (mutex)
            {
                return File.ReadAllText(path);
            }
        }

        /// <summary>
        /// Creates a directory relative to the application directory
        /// </summary>
        /// <param name="relativeDir">The directory relative to the application directory</param>
        /// <returns>
        /// The full path to the relative directory
        /// </returns>
        public static string CreateDirectory(string relativeDir)
        {
            string directory = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
            string dir = Path.Combine(directory, relativeDir);
            if (!Directory.Exists(dir))
            {
                Directory.CreateDirectory(dir);
            }

            return dir;
        }

        public static List<string> GetLogFiles(DateTime since)
        {
            string dir = DesktopLogger.CreateDirectory(DesktopLogger.LogsDir);
            DirectoryInfo dirInfo = new DirectoryInfo(dir);
            FileInfo[] files = dirInfo.GetFiles();

            List<string> toReturn = new List<string>();
            if (files.Length > 0)
            {
                foreach (var file in files)
                {
                    if (file.LastWriteTime > since)
                    {
                        toReturn.Add(file.FullName);
                    }
                }
            }

            return toReturn;
        }

        public static void Cleanup(DateTime before)
        {
            string dir = DesktopLogger.CreateDirectory(DesktopLogger.LogsDir);
            DirectoryInfo dirInfo = new DirectoryInfo(dir);
            FileInfo[] files = dirInfo.GetFiles();
            if (files.Length > 0)
            {
                foreach (var file in files)
                {
                    if (file.LastWriteTime <= before)
                    {
                        file.Delete();
                    }
                }
            }
        }
    }
}
