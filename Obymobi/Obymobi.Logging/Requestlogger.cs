﻿using System;
using System.IO;
using System.Text;
using System.Web;
using System.Web.Hosting;
using Dionysos;
using Dionysos.Logging;
using Dionysos.Web;
using Obymobi.Data.EntityClasses;
using Obymobi.Enums;
using Obymobi.Logic;

namespace Obymobi.Logging
{
    /// <summary>
    /// Requestlogger class
    /// </summary>
    public class Requestlogger
    {
        private static AsyncLoggingProvider fileLoggingProvider = null;

        #region Static methods

        /// <summary>
        /// Creates an exceptionlog instance and saves it to the database
        /// </summary>
        /// <param name="terminalId">The id of the terminal</param>
        /// <param name="methodname">The message of the exception</param>
        /// <param name="parameters">The stacktrace of the exception</param>
        /// <returns>True if creating the requestlog was successful, False if not</returns>
        public static bool CreateTerminalRequestlog(int? terminalId, string methodname, params object[] parameters)
        {
            return CreateActualRequestlog(null, string.Empty, terminalId, methodname, parameters);
        }

        /// <summary>
        /// Creates an exceptionlog instance and saves it to the database
        /// </summary>
        /// <param name="customerId">The customerId</param>
        /// <param name="methodname">The message of the exception</param>
        /// <param name="parameters">The stacktrace of the exception</param>
        /// <returns>True if creating the requestlog was successful, False if not</returns>
        public static bool CreateWebmethodJavascriptRequestlog(int? customerId, string methodname, params object[] parameters)
        {
            return CreateActualRequestlog(customerId, string.Empty, null, methodname, parameters);
        }

        /// <summary>
        /// Creates an exceptionlog instance and saves it to the database
        /// </summary>
        /// <param name="methodname">The message of the exception</param>
        /// <param name="parameters">The stacktrace of the exception</param>
        /// <returns>True if creating the requestlog was successful, False if not</returns>
        public static bool CreateRequestlog(string methodname, params object[] parameters)
        {
            return CreateActualRequestlog(null, string.Empty, null, methodname, parameters);
        }

        /// <summary>
        /// Creates an exceptionlog instance and saves it to the database
        /// </summary>
        /// <param name="customerId">The id of the customer to create the request log for</param>
        /// <param name="methodname">The message of the exception</param>
        /// <param name="parameters">The stacktrace of the exception</param>
        /// <returns>True if creating the requestlog was successful, False if not</returns>
        public static bool CreateRequestlog(int? customerId, string methodname, params object[] parameters)
        {
            return CreateActualRequestlog(customerId, string.Empty, null, methodname, parameters);
        }

        /// <summary>
        /// Creates an exceptionlog instance and saves it to the database
        /// </summary>
        /// <param name="customerId">The id of the customer to create the request log for</param>
        /// <param name="phonenumber">The phonenumber.</param>
        /// <param name="terminalId">The terminal id.</param>
        /// <param name="methodname">The message of the exception</param>
        /// <param name="parameters">The stacktrace of the exception</param>
        /// <returns>
        /// True if creating the requestlog was successful, False if not
        /// </returns>
        private static bool CreateActualRequestlog(int? customerId, string phonenumber, int? terminalId, string methodname, params object[] parameters)
        {
            bool success = false;

            try
            {
                RequestlogEntity requestlog = Requestlogger.CurrentRequestLog;
                requestlog.SourceApplication = Dionysos.Global.ApplicationInfo.ApplicationName;
                requestlog.ResultCode = "100";
                requestlog.ResultEnumValueName = "InitialValue";
                requestlog.ResultEnumTypeName = "InitialValue";
                requestlog.MethodName = methodname;
                requestlog.TerminalId = terminalId;

                for (int i = 0; i < parameters.Length && i < 6; i++)
                {
                    requestlog.SetNewFieldValue(string.Format("Parameter{0}", (i + 1)), parameters[i].ToString());
                }

                // GK Saving happens in Application_EndRequest
                // success = requestlog.Save();
                success = true;
            }
            catch (Exception ex)
            {
                Exceptionlogger.CreateExceptionlog(ex, "Request log failed", string.Empty, string.Empty, string.Empty);
            }

            return success;
        }

        /// <summary>
        /// Updates the request log.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="result">The result.</param>
        public static void UpdateRequestLog<T>(ObyTypedResult<T> result)
        {
            if (result.ResultCode != (int)GenericWebserviceCallResult.Success)
                UpdateRequestLog(result.ToObyresult());
        }

        /// <summary>
        /// Updates the request log.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="result">The result.</param>
        public static void UpdateRequestLog<T>(ObyTypedMobileResult<T> result)
        {
            if (result.ResultCode != (int)GenericWebserviceCallResult.Success)
                UpdateRequestLog(result.ToObyresult());
        }

        /// <summary>
        /// Updates the request log.
        /// </summary>
        /// <param name="result">The result.</param>
        public static void UpdateRequestLog(Obyresult result)
        {
            try
            {
                RequestlogEntity requestlog = Requestlogger.CurrentRequestLog;
                requestlog.ResultCode = result.code.ToString();

                if (result.message.Length > 255)
                    requestlog.ResultMessage = result.message.Substring(0, 255);
                else
                    requestlog.ResultMessage = result.message;

                requestlog.ResultBody = result.data;
                
                // GK Saving happens in Application_EndRequest
                //requestlog.Save();
            }
            catch (Exception ex)
            {
                Exceptionlogger.CreateExceptionlog(ex, "Request log failed", string.Empty, string.Empty, string.Empty);
            }
        }

        /// <summary>
        /// Updates the request log.
        /// </summary>
        /// <param name="resultCode">The result code.</param>
        /// <param name="resultMessage">The result message.</param>
        /// <param name="exception">The exception.</param>
        public static void UpdateRequestLog(Enum resultCode, string resultMessage, Exception exception)
        {
            try
            {
                RequestlogEntity requestlog = Requestlogger.CurrentRequestLog;
                requestlog.ResultCode = resultCode.ToIntString();
                requestlog.ResultMessage = resultMessage;
                if (exception != null)
                {
                    requestlog.ErrorMessage = exception.Message;
                    requestlog.ErrorStackTrace = exception.StackTrace;
                }
                // GK Saving happens in Application_EndRequest
                //requestlog.Save();
            }
            catch (Exception ex)
            {
                Exceptionlogger.CreateExceptionlog(ex, "Request log failed", string.Empty, string.Empty, string.Empty);
            }
        }

        /// <summary>
        /// Updates the request log on end request.
        /// </summary>
        public static void UpdateRequestLogOnEndRequest()
        {
            if (Requestlogger.CurrentRequestLog == null)
                return;

            // Save only when ALL calls should be saved or it's FAILED.            
            if (Dionysos.ConfigurationManager.GetBool(ObymobiConfigConstants.RequestlogLogSuccessfulCalls) ||
                (!Requestlogger.CurrentRequestLog.ResultCode.Equals("100") ||
                Requestlogger.CurrentRequestLog.ResultEnumValueName.Equals("InitialValue")))
            {
                bool shouldSave = Requestlogger.ShouldSaveRequestLog(Requestlogger.CurrentRequestLog);
        
                if(shouldSave)
                    Requestlogger.CurrentRequestLog.Save();   
            }
        }

        private static bool LogToFile(RequestlogEntity log)
        { 
            if (Requestlogger.fileLoggingProvider == null)
            { 
                string logPath = Path.Combine(HostingEnvironment.ApplicationPhysicalPath, "App_Data", "Logs");
                Directory.CreateDirectory(logPath);
                Requestlogger.fileLoggingProvider = new AsyncLoggingProvider(logPath, "RequestLogger", 14, Dionysos.Global.ApplicationInfo.ApplicationName);
            }

            Requestlogger.fileLoggingProvider.Verbose("{0};{1};{2};{3};{4};{5};{6};{7};{8};{9};{10};{11};{12};{13};{14}".FormatSafe( 
                log.SourceApplication, 
                log.MethodName, 
                log.ResultEnumTypeName, 
                log.ResultEnumValueName, 
                log.ResultCode, 
                log.ResultMessage, 
                log.Parameter1, 
                log.Parameter2,
                log.Parameter3, 
                log.Parameter4, 
                log.Parameter5, 
                log.Parameter6, 
                log.ErrorMessage, 
                log.ErrorStackTrace, 
                log.RawRequest));

            return true;
        }

        public static bool ShouldSaveRequestLog(RequestlogEntity log)
        {
            bool shouldSave = true;

            try
            {
                if (log == null)
                {
                    shouldSave = false;
                }
                else if (!log.IsNew && !log.IsDirty)
                {
                    shouldSave = false;
                }
                else if (log.MethodName.IsNullOrWhiteSpace())
                {                   
                    // We log this to a file, since it's not really valuable information, but we want to be able to find these issues any way.
                    shouldSave = false;
                    Requestlogger.LogToFile(log);
                }
                else if (log.SourceApplication.IsNullOrWhiteSpace() &&
                                   log.MethodName.IsNullOrWhiteSpace() &&
                                   !log.ErrorMessage.IsNullOrWhiteSpace())
                {
                    // Unknown errors, to too often
                    if (HttpContext.Current != null && HttpContext.Current.Cache != null)
                    {
                        string cacheKey = log.ErrorMessage;
                        if (!Dionysos.Web.CacheHelper.HasValue(cacheKey, false))
                        {
                            Dionysos.Web.CacheHelper.AddAbsoluteExpire(false, cacheKey, "bla", 30);
                        }
                        else
                            shouldSave = false;
                    }
                }
                else if (log.ResultEnumValueName.Equals("RequestHashIsInvalid"))
                {
                    // Only log invalid credentials once every 30 minutes.
                    // Concat the cache key
                    string cacheKey = log.ResultEnumValueName + log.MethodName + log.Parameter2;
                    if (!Dionysos.Web.CacheHelper.HasValue(cacheKey, false))
                    {
                        Dionysos.Web.CacheHelper.AddAbsoluteExpire(false, cacheKey, "bla", 30);
                    }
                    else
                        shouldSave = false;
                }
            }
            catch
            {
                if (TestUtil.IsPcDeveloper)
                    throw;
            }

            return shouldSave;
        }

        /// <summary>
        /// Creates an exceptionlog instance and saves it to the database
        /// </summary>
        /// <param name="ex">The exception instance to log</param>
        /// <returns>True if creating the exceptionlog was successful, False if not</returns>
        public static bool LogException(Exception ex)
        {
            return LogException(ex, string.Empty, string.Empty, string.Empty, string.Empty);
        }

        /// <summary>
        /// Creates an exceptionlog instance and saves it to the database
        /// </summary>
        /// <param name="ex">The exception</param>
        /// <param name="message">The message of the exception</param>
        /// <param name="stacktrace">The stacktrace of the exception</param>
        /// <param name="deviceinfo">The device information on which the error occurred</param>
        /// <param name="log">The log.</param>
        /// <param name="objs">The objs.</param>
        /// <returns>
        /// True if creating the exceptionlog was successful, False if not
        /// </returns>
        public static bool LogException(Exception ex, string message, string stacktrace, string deviceinfo, string log, params object[] objs)
        {
            bool success = false;

            try
            {
                RequestlogEntity requestLog = Requestlogger.CurrentRequestLog;

                // Cast to Oby and perform logic if it's a obymobi exception
                ObymobiException obyex = ex as ObymobiException;
                if (obyex != null)
                {
                    requestLog.ErrorStackTrace = obyex.StackTrace;
                    requestLog.ErrorMessage = obyex.Message;
                    requestLog.ResultCode = obyex.ErrorEnumValueInt.ToString();
                    requestLog.ResultEnumTypeName = obyex.ErrorEnumType;
                    requestLog.ResultEnumValueName = obyex.ErrorEnumValueText;
                    requestLog.DeviceInfo = deviceinfo;
                    requestLog.Log = string.Format(log, objs);
                }
                else
                {
                    requestLog.ResultCode = UnspecifiedError.Unknown.ToIntString();
                    requestLog.ResultEnumTypeName = UnspecifiedError.Unknown.GetType().Name;
                    requestLog.ResultEnumValueName = UnspecifiedError.Unknown.ToString();
                    requestLog.ErrorMessage = (ex != null ? ex.Message : (message.Length > 0 ? message : string.Empty));
                    requestLog.ErrorStackTrace = (ex != null ? ex.StackTrace : (stacktrace.Length > 0 ? stacktrace : string.Empty));
                    requestLog.DeviceInfo = deviceinfo;
                    requestLog.Log = string.Format(log, objs);
                }

                bool shouldSave = Requestlogger.ShouldSaveRequestLog(requestLog);

                if(shouldSave)
                    success = requestLog.Save();
                else
                    success = true;
            }
            catch (Exception exceptionDuringLog)
            {
                Requestlogger.ExceptionToFileHandler(ex, exceptionDuringLog);
            }

            return success;
        }

        /// <summary>
        /// Exceptions to file handler.
        /// </summary>
        /// <param name="exceptionToLog">The exception to log.</param>
        /// <param name="exceptionDuringLog">The exception during log.</param>
        public static void ExceptionToFileHandler(Exception exceptionToLog, Exception exceptionDuringLog)
        {
            ExceptionToFileHandler(exceptionToLog, exceptionDuringLog, false);
        }

        /// <summary>
        /// Exceptions to file handler.
        /// </summary>
        /// <param name="exceptionToLog">The exception to log.</param>
        /// <param name="exceptionDuringLog">The exception during log.</param>
        /// <param name="test">if set to <c>true</c> [test].</param>
        public static void ExceptionToFileHandler(Exception exceptionToLog, Exception exceptionDuringLog, bool test)
        {
            try
            {
                DateTime now = DateTime.Now;
                // GK 'Could be breaking change'
                string path = System.Web.Hosting.HostingEnvironment.MapPath("~/App_Data/Error");
                Directory.CreateDirectory(path);
                string fileName = Path.Combine(path, (Dionysos.TimeStamp.CreateTimeStamp(now) + now.Ticks.ToString() + RandomUtil.GetRandomLowerCaseString(6) + ".txt"));

                string exceptionMessage = FormatExceptionMessage(exceptionToLog, exceptionDuringLog);
                File.WriteAllText(fileName, exceptionMessage);

                if (test)
                    File.Delete(fileName);
            }
            catch (Exception ex)
            {
                throw new Dionysos.FunctionalException("ExceptionToFileHandler failure: {0},\r\n\r\n<br><br> {1}", ex.Message, ex.StackTrace);
            }
        }

        private static string FormatExceptionMessage(Exception exceptionToLog, Exception exceptionDuringLog)
        {
            StringBuilder stringBuilder = new StringBuilder(DateTime.Now.ToString() + Environment.NewLine);

            if (exceptionToLog != null)
            {
                stringBuilder.AppendLine($"Error to log: {Environment.NewLine}");

                stringBuilder.AppendLine($"{exceptionToLog.Message}{Environment.NewLine}");
                stringBuilder.AppendLine(exceptionToLog.StackTrace);

                if (exceptionToLog.InnerException != null)
                {
                    stringBuilder.AppendLine(exceptionToLog.InnerException.Message);
                    stringBuilder.AppendLine(exceptionToLog.InnerException.StackTrace);
                }
            }

            if (exceptionDuringLog != null)
            {
                stringBuilder.AppendLine($"Error during log: {Environment.NewLine}");

                stringBuilder.AppendLine(exceptionDuringLog.Message);
                stringBuilder.AppendLine(exceptionDuringLog.StackTrace);
            }

            return stringBuilder.ToString();
        }

        private const string requestLogId = "WebserviceHelper.RequestLogId";
        /// <summary>
        /// Gets the current request log if a HttpContext.Current is available.
        /// </summary>
        public static RequestlogEntity CurrentRequestLog
        {
            get
            {
                RequestlogEntity requestlog = null;
                try
                {
                    if (HttpContext.Current == null)
                    {
                        // Don't.
                    }
                    else
                    {
                        if (HttpContext.Current.Items[requestLogId] != null)
                        {
                            requestlog = (RequestlogEntity)HttpContext.Current.Items[requestLogId];
                        }
                        else
                        {
                            requestlog = new RequestlogEntity();
                            requestlog.ServerName = Environment.MachineName;
                            requestlog.RawRequest = Puker.BarfRawHttpRequest();
                            HttpContext.Current.Items[requestLogId] = requestlog;
                        }
                    }
                }
                catch (Exception exceptionDuringLog)
                {
                    ExceptionToFileHandler(null, exceptionDuringLog);
                }

                return requestlog;
            }
        }     

        #endregion
    }
}
