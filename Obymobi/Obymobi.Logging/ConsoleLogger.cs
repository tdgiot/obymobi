﻿using System;
using Dionysos;

namespace Obymobi.Logging
{
    public class ConsoleLogger
    {
        private static readonly object mutex = new object();
        public static bool IsConsoleMode { get; set; }

        public static string GetFilename(string prefix = "")
        {
            return DesktopLogger.GetFilename(prefix);
        }

        /// <summary>
        /// Writes the specified contents to the log file with logging level normal
        /// </summary>
        /// <param name="contents">The contents to write to the logfile</param>
        /// <param name="args"/>
        public static void WriteToLog(string contents, params object[] args)
        {
            WriteToLog(contents, LoggingLevel.Debug, args);
        }

        /// <summary>
        /// Writes the specified contents to the log file
        /// </summary>
        /// <param name="contents">The contents to write to the logfile</param>
        /// <param name="level"/>
        /// <param name="args"/>
        public static void WriteToLog(string contents, LoggingLevel level, params object[] args)
        {
            WriteToLog(contents, level, "", true, args);
        }

        /// <summary>
        /// Writes the specified contents to the log file
        /// </summary>
        /// <param name="contents">The contents to write to the logfile</param>
        /// <param name="level"></param>
        /// <param name="filePrefix">Filename prefix</param>
        /// <param name="showInConsole"></param>
        /// <param name="args"></param>
        public static void WriteToLog(string contents, LoggingLevel level, string filePrefix, bool showInConsole, params object[] args)
        {
            lock (mutex)
            {
                DesktopLogger.FilePrefix = filePrefix;

                if (IsConsoleMode && showInConsole)
                {
                    DateTime now = DateTime.Now;
                    string message = StringUtil.FormatSafe(contents, args);
                    Console.WriteLine("[{0}] {1}", now.ToLongTimeString(), message);
                }

                switch (level)
                {
                    case LoggingLevel.Verbose:
                        DesktopLogger.Verbose(contents, args);
                        break;
                    case LoggingLevel.Debug:
                        DesktopLogger.Debug(contents, args);
                        break;
                    case LoggingLevel.Info:
                        DesktopLogger.Info(contents, args);
                        break;
                    case LoggingLevel.Warning:
                        DesktopLogger.Warning(contents, args);
                        break;
                    case LoggingLevel.Error:
                        DesktopLogger.Error(contents, args);
                        break;
                    default:
                        DesktopLogger.Info(contents, args);
                        break;
                }

                DesktopLogger.FilePrefix = "";
            }
        }

        public static void Cleanup(int daysOld)
        {
            DateTime before = DateTime.Now;
            if (daysOld > 0)
                before = before.AddDays(-daysOld);

            DesktopLogger.Cleanup(before);
        }
    }
}
