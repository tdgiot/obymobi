﻿using System;
using Obymobi.Data.EntityClasses;

namespace Obymobi.Logging
{
    /// <summary>
    /// Exceptionlogger class
    /// </summary>
    public class Exceptionlogger
    {
		/// <summary>
		/// Creates an exceptionlog instance and saves it to the database
		/// </summary>
        /// <param name="ex">The exception instance to log</param>
		/// <returns>True if creating the exceptionlog was successful, False if not</returns>
		public static bool CreateExceptionlog(Exception ex)
		{
			return CreateExceptionlog(ex, string.Empty, string.Empty, string.Empty, string.Empty);
		}

        /// <summary>
        /// Creates an exceptionlog instance and saves it to the database
        /// </summary>
        /// <param name="ex">The ex.</param>
        /// <param name="message">The message of the exception</param>
        /// <param name="stacktrace">The stacktrace of the exception</param>
        /// <param name="deviceinfo">The device information on which the error occurred</param>
        /// <param name="log">The log.</param>
        /// <param name="objs">The objs.</param>
        /// <returns>
        /// True if creating the exceptionlog was successful, False if not
        /// </returns>
		public static bool CreateExceptionlog(Exception ex, string message, string stacktrace, string deviceinfo, string log, params object[] objs)
		{
			bool success = false;
			
			try
			{
				RequestlogEntity requestLog = Requestlogger.CurrentRequestLog;

                if (requestLog != null)
                {
                    // Cast to Oby and perform logic if it's a obymobi exception
                    ObymobiException obyex = ex as ObymobiException;
                    if (obyex != null)
                    {
                        requestLog.ErrorStackTrace = obyex.StackTrace;
                        requestLog.ErrorMessage = obyex.Message;
                        requestLog.ResultCode = obyex.ErrorEnumValueInt.ToString();
                        requestLog.ResultEnumTypeName = obyex.ErrorEnumType;
                        requestLog.ResultEnumValueName = obyex.ErrorEnumValueText;
                        requestLog.DeviceInfo = deviceinfo;
                        requestLog.Log = log;
                    }
                    else
                    {
                        requestLog.ErrorMessage = (ex != null ? ex.Message : (message.Length > 0 ? message : string.Empty));
                        requestLog.ErrorStackTrace = (ex != null ? ex.StackTrace : (stacktrace.Length > 0 ? stacktrace : string.Empty));
                        requestLog.DeviceInfo = deviceinfo;
                        requestLog.Log = log;
                    }

                    bool shouldSave = Requestlogger.ShouldSaveRequestLog(Requestlogger.CurrentRequestLog);

                    if (shouldSave)
                    {
                        success = requestLog.Save();
                    }
                }
                else
                {
                    Requestlogger.ExceptionToFileHandler(ex, null);
                }
			}
			catch (Exception exceptionDuringLog)
			{
				Requestlogger.ExceptionToFileHandler(ex, exceptionDuringLog);
			}

			return success;
		}

    }
}
