﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="_Pollers" Codebehind="Pollers.aspx.cs" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style>
        body {
            font-family: Consolas;
            font-size: 10pt;
        }
    </style>
</head>
<body>
    The Answer to the Great Question, of Life, the Universe and Pollers<br />
    <br />
    <asp:Label runat="server" ID="lblOutput"></asp:Label>
</body>
</html>
