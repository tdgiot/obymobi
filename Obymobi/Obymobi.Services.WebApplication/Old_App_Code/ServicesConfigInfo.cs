﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dionysos.Configuration;

namespace Obymobi.Configuration
{
    public class ServicesConfigInfo : ConfigurationItemCollection
    {
        const string SECTION_NAME = "Services";

        public ServicesConfigInfo()
        {
            // General            
            Add(new ConfigurationItem(SECTION_NAME, ServicesConfigConstants.FirstWarningDiskspaceGb, "First disk space level in gigabytes at which a first warning must be send, default: 5Gb", 5, typeof(int), true));
            Add(new ConfigurationItem(SECTION_NAME, ServicesConfigConstants.SecondWarningDiskspaceGb, "Second disk space level in gigabytes at which a first warning must be send, default: 3Gb", 3, typeof(int), true));
            Add(new ConfigurationItem(SECTION_NAME, ServicesConfigConstants.ThirdWarningDiskspaceGb, "Third disk space level in gigabytes at which a first warning must be send, default: 1Gb", 1, typeof(int), true));
        }
    }
}
