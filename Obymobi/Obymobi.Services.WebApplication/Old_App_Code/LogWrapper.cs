﻿using Dionysos;
using Dionysos.Logging;
using System;

/// <summary>
/// Mainly used to have a ILoggingProvider to supply to CloudeStorageProviders which does include the LogType.
/// </summary>
public class LogWrapper : ILoggingProvider
{
    private readonly IMultiLoggingProvider loggingProvider;
    private readonly Enum loggingType;

    public LogWrapper(IMultiLoggingProvider loggingProvider, Enum type)
    {
        this.loggingProvider = loggingProvider;
        this.loggingType = type;
    }

    public void Log(LoggingLevel level, string text, params object[] args)
    {
        this.loggingProvider.Log(this.loggingType, level, text, args);
    }

    public void Verbose(string format, params object[] args)
    {
        Log(LoggingLevel.Verbose, format, args);
    }

    public void Debug(string format, params object[] args)
    {
        Log(LoggingLevel.Debug, format, args);
    }

    public void Information(string format, params object[] args)
    {
        Log(LoggingLevel.Info, format, args);
    }

    public void Warning(string format, params object[] args)
    {
        Log(LoggingLevel.Warning, format, args);
    }

    public void Error(string format, params object[] args)
    {
        Log(LoggingLevel.Error, format, args);
    }
}