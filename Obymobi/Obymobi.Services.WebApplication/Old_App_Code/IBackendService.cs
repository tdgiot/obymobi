﻿using System.ServiceModel;
using Obymobi.Logic.Model;

[ServiceContract]
public interface IBackendService
{
    [OperationContract]
    bool IsAlive();

    [OperationContract]
    bool IsDatabaseAlive();

    [OperationContract]
    long GetSecondsSinceLastPollerExecution(string pollerName, long timestamp, string hash);

    [OperationContract]
    ServiceBackendWebserviceResult GetSystemSupportpoolInfo(long timestamp, string hash);

    [OperationContract]
    string[] GetSystemSupportpoolPhonenumbers(long timestamp, string hash);

    [OperationContract]
    string[] GetSystemSupportpoolEmailaddresses(long timestamp, string hash);
}
