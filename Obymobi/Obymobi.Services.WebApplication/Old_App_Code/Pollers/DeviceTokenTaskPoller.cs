﻿using System;
using System.Data;
using Dionysos.Logging;
using Obymobi.Constants;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using Obymobi.Logic.Comet;
using Obymobi.Web.Pollers;
using Obymobi.Web.Slack.Interfaces;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Pollers
{
    /// <summary>
    /// Summary description for DeviceTokenTaskPoller
    /// </summary>
    public class DeviceTokenTaskPoller : BasePoller
    {
        #region Constants

        private const int BATCH_SIZE = 50;
        private const int RETRY_TIMEOUT_MINUTES = 60;

        #endregion

        #region Fields

        private readonly DeviceDataSource deviceDataSource = new DeviceDataSource();
        private readonly DeviceTokenTaskDataSource deviceTokenTaskDataSource = new DeviceTokenTaskDataSource();
        private readonly DeviceTokenHistoryDataSource deviceTokenHistoryDataSource = new DeviceTokenHistoryDataSource();

        #endregion

        public DeviceTokenTaskPoller(ISlackClient slackClient) : base(slackClient)
        {
        }

        #region Base Overrides

        public override Enum LoggingType
        {
            get { return LoggingTypes.DeviceTokenTaskPoller; }
        }

        public override PollerType PollerType
        {
            get { return PollerType.DeviceTokenTaskPoller; }
        }

        public override IMultiLoggingProvider LoggingProvider
        {
            get { return Global.LoggingProvider; }
        }

        public override int PrimaryPollerTimeoutSeconds
        {
            get { return ObymobiIntervals.SERVICES_DEVICETOKENTASKPOLLER_INTERVAL; }
        }

        protected override TimeSpan WorkInterval
        {
            get { return TimeSpan.FromMilliseconds(ObymobiIntervals.SERVICES_DEVICETOKENTASKPOLLER_INTERVAL); }
        }

        protected override TimeSpan FirstWorkInterval
        {
            get { return WorkInterval; }
        }

        #endregion

        protected override void ExecuteWork(bool force = false)
        {
            LastStarted = DateTime.Now;
            
            this.HandleAcknowledgedDeviceTokenCollection();
            this.HandleQueuedDeviceTokenTaskCollection();
            this.HandleWaitingToBeRetrievedDeviceTokenTaskCollection();
        }

        /// <summary>
        /// Handles 'Queued' device token tasks, these device token tasks are awaiting to be send to the device.
        /// </summary>
        private void HandleQueuedDeviceTokenTaskCollection()
        {
            DeviceTokenTaskCollection deviceTokenTaskCollection = this.deviceTokenTaskDataSource.GetDeviceTokenTaskCollection(DeviceTokenTaskPoller.BATCH_SIZE, DeviceTokenState.Queued);
            this.HandleDeviceTokenTaskCollection(deviceTokenTaskCollection);
        }

        /// <summary>
        /// Handles 'WaitingToBeRetrieved' device token tasks, these device token tasks will be send to the device until it's acknowledged.
        /// </summary>
        private void HandleWaitingToBeRetrievedDeviceTokenTaskCollection()
        {
            DeviceTokenTaskCollection deviceTokenTaskCollection = this.deviceTokenTaskDataSource.GetDeviceTokenTaskCollection(DeviceTokenTaskPoller.BATCH_SIZE, DeviceTokenState.WaitingToBeRetrieved, DateTime.UtcNow.AddMinutes(-DeviceTokenTaskPoller.RETRY_TIMEOUT_MINUTES));
            this.HandleDeviceTokenTaskCollection(deviceTokenTaskCollection);
        }

        /// <summary>
        /// Handles 'Acknowledged' device token tasks, these device token are received on the device.
        /// </summary>
        private void HandleAcknowledgedDeviceTokenCollection()
        {
            DeviceTokenTaskCollection deviceTokenTaskCollection = this.deviceTokenTaskDataSource.GetDeviceTokenTaskCollection(DeviceTokenTaskPoller.BATCH_SIZE, DeviceTokenState.Acknowledged);
            this.HandleDeviceTokenTaskCollection(deviceTokenTaskCollection);
        }

        /// <summary>
        /// Handles the device token tasks based on the state of the tasks.
        /// </summary>
        /// <param name="deviceTokenTaskCollection">The device token tasks to handle.</param>
        private void HandleDeviceTokenTaskCollection(DeviceTokenTaskCollection deviceTokenTaskCollection)
        {
            foreach (DeviceTokenTaskEntity deviceTokenTaskEntity in deviceTokenTaskCollection)
            {
                this.HandleDeviceTokenTask(deviceTokenTaskEntity);
            }
        }

        /// <summary>
        /// Handles the device token task based on the state of the task.
        /// </summary>
        /// <param name="deviceTokenTaskEntity">The device token task to handle.</param>
        private void HandleDeviceTokenTask(DeviceTokenTaskEntity deviceTokenTaskEntity)
        {
            using (Transaction transaction = new Transaction(IsolationLevel.ReadUncommitted, string.Format("DeviceTokenTask-{0}", deviceTokenTaskEntity.DeviceTokenTaskId)))
            {
                try
                {
                    switch (deviceTokenTaskEntity.State)
                    {
                        case DeviceTokenState.Queued:
                            this.HandleQueuedDeviceTokenTaskEntity(deviceTokenTaskEntity, transaction);
                            break;
                        case DeviceTokenState.WaitingToBeRetrieved:
                            this.HandleWaitingToBeRetrievedDeviceTokenTaskEntity(deviceTokenTaskEntity, transaction);
                            break;
                        case DeviceTokenState.Acknowledged:
                            this.HandleAcknowledgedDeviceTokenTaskEntity(deviceTokenTaskEntity, transaction);
                            break;
                    }

                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();

                    this.LogError("ProcessDeviceTokenTask", "Processing task '{0}' failed: {1}\r\n{2}", deviceTokenTaskEntity.DeviceTokenTaskId, ex.Message, ex.StackTrace);
                }
            }
        }

        /// <summary>
        /// Handles a 'Queued' device token task, this device token task is awaiting to be send to the device.
        /// </summary>
        /// <param name="deviceTokenTaskEntity">The device token to handle.</param>
        /// <param name="transaction">The transaction to use.</param>
        private void HandleQueuedDeviceTokenTaskEntity(DeviceTokenTaskEntity deviceTokenTaskEntity, ITransaction transaction)
        {
            this.SendUpdateDeviceTokenNetmessage(deviceTokenTaskEntity, transaction);

            if (deviceTokenTaskEntity.Action == DeviceTokenAction.Renew)
            {
                this.deviceTokenTaskDataSource.UpdateDeviceTokenTaskState(deviceTokenTaskEntity, DeviceTokenState.WaitingToBeRetrieved, transaction);
            }
            else if (deviceTokenTaskEntity.Action == DeviceTokenAction.Revoke)
            {
                this.deviceDataSource.RevokeDeviceToken(deviceTokenTaskEntity.DeviceEntity, deviceTokenTaskEntity.Token, transaction);
                this.deviceTokenHistoryDataSource.CreateDeviceTokenHistoryEntity(deviceTokenTaskEntity, transaction);
                this.deviceTokenTaskDataSource.DeleteDeviceTokenTaskEntity(deviceTokenTaskEntity, transaction);
            }
        }

        /// <summary>
        /// Handles a 'WaitingToBeRetrieved' device token task, this device token task will keep getting send to the device until it's acknowledged.
        /// </summary>
        /// <param name="deviceTokenTaskEntity"></param>
        /// <param name="transaction"></param>
        private void HandleWaitingToBeRetrievedDeviceTokenTaskEntity(DeviceTokenTaskEntity deviceTokenTaskEntity, ITransaction transaction)
        {
            this.SendUpdateDeviceTokenNetmessage(deviceTokenTaskEntity, transaction);
            this.deviceTokenTaskDataSource.UpdateDeviceTokenTaskStateUpdated(deviceTokenTaskEntity, transaction);
        }

        /// <summary>
        /// Handles a 'Acknowledged' device token task, this device token is received on the device.
        /// </summary>
        /// <param name="deviceTokenTaskEntity">The device token task to handle.</param>
        /// <param name="transaction">The transaction to use.</param>
        private void HandleAcknowledgedDeviceTokenTaskEntity(DeviceTokenTaskEntity deviceTokenTaskEntity, ITransaction transaction)
        {
            this.deviceTokenTaskDataSource.DeleteDeviceTokenTaskEntity(deviceTokenTaskEntity, transaction);
        }

        /// <summary>
        /// Creates an 'UpdateDeviceToken' net message.
        /// </summary>
        /// <param name="deviceTokenTaskEntity">The device token task entity.</param>
        /// <param name="transaction">The transaction to use.</param>
        private void SendUpdateDeviceTokenNetmessage(DeviceTokenTaskEntity deviceTokenTaskEntity, ITransaction transaction)
        {
            CometHelper.UpdateDeviceToken(deviceTokenTaskEntity, transaction);
        }
    }
}