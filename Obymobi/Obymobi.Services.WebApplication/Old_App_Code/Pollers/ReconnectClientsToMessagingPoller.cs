﻿using System;
using System.Diagnostics;
using System.Threading;
using Dionysos.Logging;
using Obymobi.Enums;
using Obymobi.Web.Pollers;
using Obymobi.Web.Slack.Interfaces;

namespace Pollers
{
    /// <summary>
    /// Summary description for ReconnectClientsToMessagingPoller
    /// </summary>
    public class ReconnectClientsToMessagingPoller : BasePoller
    {
        private static DateTime nextReconnect;

        public ReconnectClientsToMessagingPoller(ISlackClient slackClient) : base(slackClient)
        {
        }

        #region Base Overrides

        public override Enum LoggingType
        {
            get { return LoggingTypes.ReconnectClientsToMessagingPoller; }
        }

        public override PollerType PollerType
        {
            get { return PollerType.ReconnectClientsToMessagingPoller; }
        }

        public override IMultiLoggingProvider LoggingProvider
        {
            get { return Global.LoggingProvider; }
        }

        public override int PrimaryPollerTimeoutSeconds
        {
            get { return 30 * 60; } // 30 mins
        }

        /// <summary>
        /// NOT USED! This poller calculates its own <see cref="TimeTillNextWork"/> TimeSpan
        /// </summary>
        protected override TimeSpan WorkInterval
        {
            get { return new TimeSpan(1, 0, 0, 0); }
        }

        /// <summary>
        /// NOT USED! This poller calculates its own <see cref="TimeTillNextWork"/> TimeSpan
        /// </summary>
        protected override TimeSpan FirstWorkInterval
        {
            get { return new TimeSpan(1, 0, 0, 0); }
        }

        #endregion

        public override void Initialize(CancellationToken cancellationToken)
        {
            // By added hours to "Today", we can let this poller run each day at specific time
            ReconnectClientsToMessagingPoller.nextReconnect = DateTime.Today.AddHours(10);

            this.SetNextReconnectTime();

            base.Initialize(cancellationToken);
        }

        public override TimeSpan TimeTillNextWork()
        {
            TimeSpan timeTillNextWork = ReconnectClientsToMessagingPoller.nextReconnect.Subtract(DateTime.Now);
            return timeTillNextWork;
        }

        protected override void ExecuteWork(bool force = false)
        {
            if (DateTime.Now < ReconnectClientsToMessagingPoller.nextReconnect)
                return;

            this.DoReconnect();
            this.SetNextReconnectTime();
        }

        private void SetNextReconnectTime()
        {
            if (DateTime.Now >= ReconnectClientsToMessagingPoller.nextReconnect)
            {
                // Set next cleanup time to tomorrow
                ReconnectClientsToMessagingPoller.nextReconnect = ReconnectClientsToMessagingPoller.nextReconnect.AddHours(12);
            }

            LogInfo("SetNextCleanupTime", "Next reconnect scheduled at: {0}", ReconnectClientsToMessagingPoller.nextReconnect.ToString("dd/MM/yyyy HH:mm:ss"));
        }

        private void DoReconnect()
        {
            try
            {
                LogWrapper logWrapper = new LogWrapper(this.LoggingProvider, this.LoggingType);

                Stopwatch sw = new Stopwatch();
                sw.Start();

                this.LogInfo("DoReconnect", "Started reconnect");

                ClientsToMessagingReconnector reconnector = new ClientsToMessagingReconnector(logWrapper);
                reconnector.Reconnect();

                sw.Stop();
                this.LogInfo("DoCleanup", "Completed reconnect in {0}", sw.Elapsed);
            }
            catch (Exception ex)
            {
                this.LogError("DoCleanup", "Failed to do the reconnect: {0}", ex.Message);
            }
        }
    }
}