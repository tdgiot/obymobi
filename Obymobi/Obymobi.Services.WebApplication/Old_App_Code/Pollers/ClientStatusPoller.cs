﻿using System;
using System.Data;
using System.Text;
using System.Threading;
using Dionysos;
using Dionysos.Logging;
using Obymobi.Data;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using Obymobi.Web.Pollers;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Logic.HelperClasses;
using Obymobi.Constants;
using System.Web;
using System.IO;
using Obymobi.Web.Slack.Interfaces;

namespace Pollers
{
    public class ClientStatusPoller : BasePoller
    {
        private string csvDirPath;

        private static DateTime nextCleanup;

        public ClientStatusPoller(ISlackClient slackClient) : base(slackClient)
        {
        }

        #region Base Overrides

        public override Enum LoggingType
        {
            get { return LoggingTypes.ClientStatusPoller; }
        }

        public override PollerType PollerType
        {
            get { return PollerType.ClientStatusPoller; }
        }

        public override IMultiLoggingProvider LoggingProvider
        {
            get { return Global.LoggingProvider; }
        }

        public override int PrimaryPollerTimeoutSeconds
        {
            get { return 60; }
        }

        protected override TimeSpan WorkInterval
        {
            get { return new TimeSpan(0, 0, 1, 0); }
        }

        protected override TimeSpan FirstWorkInterval
        {
            get { return this.WorkInterval; }
        }

        #endregion

        public override void Initialize(CancellationToken cancellationToken)
        {
            this.csvDirPath = HttpContext.Current.Server.MapPath("~/App_Data/Csv");
            if (!Directory.Exists(this.csvDirPath))
                Directory.CreateDirectory(this.csvDirPath);

            base.Initialize(cancellationToken);
        }

        protected override void ExecuteWork(bool force = false)
        {
            // Global online / offline / out of battery clients
            this.WriteGlobalClientStatuses();

            // Online / offline / out of battery per company
            this.WriteCompanyClientStatuses();
        }

        private void WriteGlobalClientStatuses()
        {
            // Online
            PredicateExpression onlineFilter = new PredicateExpression();
            onlineFilter.Add(DeviceFields.LastRequestUTC >= DateTime.UtcNow.AddMilliseconds(ObymobiIntervals.DEVICE_OFFLINE_THRESHOLD * -1));
            RelationCollection onlineRelations = new RelationCollection(ClientEntity.Relations.DeviceEntityUsingDeviceId);

            // Offline
            PredicateExpression offlineFilter = new PredicateExpression();
            offlineFilter.Add(DeviceFields.LastRequestUTC < DateTime.UtcNow.AddMilliseconds(ObymobiIntervals.DEVICE_OFFLINE_THRESHOLD * -1));
            offlineFilter.Add(DeviceFields.BatteryLevel > DeliverypointgroupFields.OutOfChargeLevel);
            RelationCollection offlineRelations = new RelationCollection();
            offlineRelations.Add(ClientEntity.Relations.DeliverypointgroupEntityUsingDeliverypointGroupId);
            offlineRelations.Add(ClientEntity.Relations.DeviceEntityUsingDeviceId);

            // Out of battery
            PredicateExpression outOfBatteryFilter = new PredicateExpression();
            outOfBatteryFilter.Add(DeviceFields.LastRequestUTC < DateTime.UtcNow.AddMilliseconds(ObymobiIntervals.DEVICE_OFFLINE_THRESHOLD * -1));
            outOfBatteryFilter.Add(DeviceFields.BatteryLevel <= DeliverypointgroupFields.OutOfChargeLevel);
            RelationCollection outOfBattteryRelations = new RelationCollection();
            outOfBattteryRelations.Add(ClientEntity.Relations.DeliverypointgroupEntityUsingDeliverypointGroupId);
            outOfBattteryRelations.Add(ClientEntity.Relations.DeviceEntityUsingDeviceId);

            ClientCollection clientCollection = new ClientCollection();
            int onlineClientCount = clientCollection.GetDbCount(onlineFilter, onlineRelations);
            int offlineClientCount = clientCollection.GetDbCount(offlineFilter, offlineRelations);
            int outOfBatteryClientCount = clientCollection.GetDbCount(outOfBatteryFilter, outOfBattteryRelations);

            LogInfo("WriteGlobalClientStatuses", "Writing to csv. Online: {0}, offline: {1}, out of battery {2}", onlineClientCount, offlineClientCount, outOfBatteryClientCount);

            string fileName = string.Format("GlobalClientStatus-{0}{1}{2}.csv", DateTime.Now.Year.ToString("0000"), DateTime.Now.Month.ToString("00"), DateTime.Now.Day.ToString("00"));
            this.WriteToCsv(fileName, this.csvDirPath, onlineClientCount, offlineClientCount, outOfBatteryClientCount);
        }

        private void WriteCompanyClientStatuses()
        {
            PredicateExpression companyFilter = new PredicateExpression();
            companyFilter.Add(CompanyFields.UseMonitoring == true);

            CompanyCollection companyCollection = new CompanyCollection();
            companyCollection.GetMulti(companyFilter, new IncludeFieldsList(CompanyFields.UseMonitoring, CompanyFields.Name), null);

            foreach (CompanyEntity companyEntity in companyCollection)
            {
                string companyCsvDirPath = Path.Combine(this.csvDirPath, companyEntity.CompanyId.ToString());
                if (!Directory.Exists(companyCsvDirPath))
                    Directory.CreateDirectory(companyCsvDirPath);

                string fileName = string.Format("ClientStatus-{0}{1}{2}.csv", DateTime.Now.Year.ToString("0000"), DateTime.Now.Month.ToString("00"), DateTime.Now.Day.ToString("00"));

                // Online
                PredicateExpression onlineFilter = new PredicateExpression();
                onlineFilter.Add(ClientFields.CompanyId == companyEntity.CompanyId);
                onlineFilter.Add(DeviceFields.LastRequestUTC >= DateTime.UtcNow.AddMilliseconds(ObymobiIntervals.DEVICE_OFFLINE_THRESHOLD * -1));
                RelationCollection onlineRelations = new RelationCollection(ClientEntity.Relations.DeviceEntityUsingDeviceId);

                // Offline
                PredicateExpression offlineFilter = new PredicateExpression();
                offlineFilter.Add(ClientFields.CompanyId == companyEntity.CompanyId);
                offlineFilter.Add(DeviceFields.LastRequestUTC < DateTime.UtcNow.AddMilliseconds(ObymobiIntervals.DEVICE_OFFLINE_THRESHOLD * -1));
                offlineFilter.Add(DeviceFields.BatteryLevel > DeliverypointgroupFields.OutOfChargeLevel);
                RelationCollection offlineRelations = new RelationCollection();
                offlineRelations.Add(ClientEntity.Relations.DeliverypointgroupEntityUsingDeliverypointGroupId);
                offlineRelations.Add(ClientEntity.Relations.DeviceEntityUsingDeviceId);

                // Out of battery
                PredicateExpression outOfBatteryFilter = new PredicateExpression();
                outOfBatteryFilter.Add(ClientFields.CompanyId == companyEntity.CompanyId);
                outOfBatteryFilter.Add(DeviceFields.LastRequestUTC < DateTime.UtcNow.AddMilliseconds(ObymobiIntervals.DEVICE_OFFLINE_THRESHOLD * -1));
                outOfBatteryFilter.Add(DeviceFields.BatteryLevel <= DeliverypointgroupFields.OutOfChargeLevel);
                RelationCollection outOfBatteryRelations = new RelationCollection();
                outOfBatteryRelations.Add(ClientEntity.Relations.DeliverypointgroupEntityUsingDeliverypointGroupId);
                outOfBatteryRelations.Add(ClientEntity.Relations.DeviceEntityUsingDeviceId);

                ClientCollection clientCollection = new ClientCollection();
                int onlineClientCount = clientCollection.GetDbCount(onlineFilter, onlineRelations);
                int offlineClientCount = clientCollection.GetDbCount(offlineFilter, offlineRelations);
                int outOfBatteryClientCount = clientCollection.GetDbCount(outOfBatteryFilter, outOfBatteryRelations);

                LogInfo("WriteCompanyClientStatuses", "Writing company {0} ({1}) to csv. Online: {2}, offline: {3}, out of battery {4}", companyEntity.Name, companyEntity.CompanyId, onlineClientCount, offlineClientCount, outOfBatteryClientCount);
                this.WriteToCsv(fileName, companyCsvDirPath, onlineClientCount, offlineClientCount, outOfBatteryClientCount);
            }
        }

        private void WriteToCsv(string fileName, string csvDirPath, int online, int offline, int outOfBattery)
        {
            string content = string.Format("{0},{1},{2},{3}\r\n", TimeStamp.CreateTimeStamp(false), online, offline, outOfBattery);

            string filePath = Path.Combine(csvDirPath, fileName);
            File.AppendAllText(filePath, content);
        }
    }
}