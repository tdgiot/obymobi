﻿using System;
using System.Collections.Generic;
using System.Threading;
using Dionysos;
using Dionysos.Logging;
using Obymobi;
using Obymobi.Enums;
using Obymobi.Web.Pollers;
using Obymobi.Data.EntityClasses;
using Obymobi.Logic.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Web.Slack.Interfaces;

namespace Pollers
{
    /// <summary>
    /// Summary description for ExternalLinkCheckPoller
    /// </summary>
    public class ExternalLinkCheckPoller : BasePoller
    {
        private static DateTime nextScheduledRun;
        
        private SupportNotificationSendHelper notificationSendHelper;

        private readonly List<ReportProcessingTaskEntity> currentTasks = new List<ReportProcessingTaskEntity>();

        public ExternalLinkCheckPoller(ISlackClient slackClient) : base(slackClient)
        {
        }

        #region Base Override

        public override IMultiLoggingProvider LoggingProvider
        {
            get { return Global.LoggingProvider; }
        }

        public override PollerType PollerType
        {
            get { return PollerType.ExternalLinkCheckPoller; }
        }

        public override Enum LoggingType
        {
            get { return LoggingTypes.ExternalLinkCheckPoller; }
        }

        public override int PrimaryPollerTimeoutSeconds
        {
            get { return 60; }
        }

        protected override TimeSpan WorkInterval
        {
            get { return new TimeSpan(0, 0, 0, 30); }
        }

        protected override TimeSpan FirstWorkInterval
        {
            get { return new TimeSpan(0, 0, 1, 0); }
        }

        public override void Initialize(CancellationToken cancellationToken)
        {
            string emailSenderAddress, emailSenderName, smsSender;
            WebEnvironmentHelper.GetSupportNotificationSenderInformation(out emailSenderAddress, out emailSenderName, out smsSender);
            this.notificationSendHelper = new SupportNotificationSendHelper(emailSenderAddress, emailSenderName, smsSender);

            // Calculate time till next Sunday
            DateTime sundayDateTime = DateTimeUtil.GetNextWeekday(DateTime.Today, DayOfWeek.Sunday);
            nextScheduledRun = sundayDateTime.AddHours(3).AddMinutes(30); // Sunday 3:30AM

            SetNextCleanupTime();

            base.Initialize(cancellationToken);
        }

        public override TimeSpan TimeTillNextWork()
        {
            return (currentTasks.Count > 0) ? base.TimeTillNextWork() : nextScheduledRun.Subtract(DateTime.Now);
        }

        #endregion

        protected override void ExecuteWork(bool force = false)
        {
            HandleCurrentTasks();

            if (DateTime.Now >= nextScheduledRun || force)
            {
                CreateReportTask();

                if (!force)
                    SetNextCleanupTime();
            }
        }

        private void SetNextCleanupTime()
        {
            if (DateTime.Now >= nextScheduledRun)
            {
                // Set next cleanup time
                nextScheduledRun = nextScheduledRun.AddDays(7);
            }

            LogInfo("SetNextCleanupTime", "Next run scheduled for: {0}", nextScheduledRun.ToString("dd/MM/yyyy HH:mm:ss"));
        }

        private void HandleCurrentTasks()
        {
            var taskListCopy = new List<ReportProcessingTaskEntity>(currentTasks);
            foreach (var currentTask in taskListCopy)
            {
                currentTask.Refetch();

                if (currentTask.Processed)
                {
                    LogInfo("HandleCurrentTasks", "ReportProcessingTask '{0}' finished procssing.", currentTask.ReportProcessingTaskId);

                    if (currentTask.ReportProcessingTaskFileEntity == null)
                    {
                        // Task completed without creating report. 
                        // Nothing to report, delete task from database
                        currentTask.Delete();

                        LogVerbose("HandleCurrentTasks", "ReportProcessingTask '{0}' - No file attached, nothing to report.", currentTask.ReportProcessingTaskId);
                    }
                    else
                    {
                        // Send email notification
                        SendMailNotification(currentTask);

                        currentTask.ExternalHandled = true;
                        currentTask.Save();
                    }

                    lock(this.currentTasks)
                    {
                        currentTasks.Remove(currentTask);
                    }
                }
            }
        }

        private void CreateReportTask()
        {
            var reportTaskEntity = new ReportProcessingTaskEntity();
            reportTaskEntity.AnalyticsReportType = AnalyticsReportType.BrokenLinks;
            reportTaskEntity.ExternalTask = true;
            reportTaskEntity.FromUTC = DateTime.UtcNow;
            reportTaskEntity.TillUTC = DateTime.UtcNow;
            reportTaskEntity.Save();
            LogInfo("CreateReportTask", "Created new ReportProcessingTask '{0}'. Further handled by ReportingPoller.", reportTaskEntity.ReportProcessingTaskId);

            lock (this.currentTasks)
            {
                currentTasks.Add(reportTaskEntity);
            }
        }

        private void SendMailNotification(ReportProcessingTaskEntity taskEntity)
        {
            var filter = new PredicateExpression();
            filter.Add(SupportpoolFields.ContentSupportPool == true);

            var supportPools = new SupportpoolCollection();
            supportPools.GetMulti(filter);

            if (supportPools.Count == 0)
            {
                LogWarning("SendMailNotification", "No content supportpools found!");
            }

            foreach (var suppportPoolEntity in supportPools)
            {
                var url = string.Format("{0}/Analytics/ReportProcessingTask.aspx?Id={1}", WebEnvironmentHelper.GetManagementUrl(), taskEntity.ReportProcessingTaskId);
                url = url.Replace("http://", "https://");
                var emailRecipients = suppportPoolEntity.GetSupportpoolEmailaddresses(false, null);
                string title = string.Format("Weekly Content Report: {0:D}", DateTime.UtcNow);
                string message = string.Format("Weekly content check has found some broken links.<br/>" +
                                               "Click on the link below to view the report for a breakdown of which objects have faulty or dead urls.<br/><br/>" +
                                               "<a href='{0}'>{0}</a>",
                                               url);

                LogVerbose("SendMailNotification", "SupportPool={0}, Recipients={1}", suppportPoolEntity.Name, String.Join(", ", emailRecipients));

                string errorReport;
                if (!this.notificationSendHelper.SendSupportNotification(null, emailRecipients, title, message, out errorReport, true))
                {
                    LogError("SendMailNotification", "Failed to send e-mail: {0}", errorReport);
                }
            }
        }
    }
}