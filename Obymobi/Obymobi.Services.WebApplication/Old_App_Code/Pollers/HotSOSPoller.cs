﻿//using System;
//using System.Collections.Generic;
//using Dionysos.Logging;
//using Obymobi.Constants;
//using Obymobi.Data.CollectionClasses;
//using Obymobi.Data.EntityClasses;
//using Obymobi.Data.HelperClasses;
//using Obymobi.Web.Pollers;
//using SD.LLBLGen.Pro.ORMSupportClasses;
//using Obymobi.Logic.Integrations.Other.HotSOS;

//namespace Pollers
//{
//    public class HotSOSPoller : BasePoller<HotSOSPoller>
//    {
//        #region Base Overrides

//        public override Enum LoggingType
//        {
//            get { return LoggingTypes.HotSOSPoller; }
//        }

//        public override Obymobi.Enums.PollerType PollerType
//        {
//            get { return Obymobi.Enums.PollerType.HotSOSPoller; }
//        }

//        public override IMultiLoggingProvider LoggingProvider
//        {
//            get { return Global.LoggingProvider; }
//        }

//        public override int PrimaryPollerTimeoutSeconds
//        {
//            get { return 60; }
//        }

//        protected override TimeSpan WorkInterval
//        {
//            get { return TimeSpan.FromMilliseconds(ObymobiIntervals.API_HOTSOSPOLLER_INTERVAL); }
//        }

//        protected override TimeSpan FirstWorkInterval
//        {
//            get { return WorkInterval; }
//        }

//        #endregion

//        protected override void ExecuteWork(bool force = false)
//        {
//            this.LogVerbose("ExecuteWork", "Do some crazy funky stuff jo!");

//            var filter = new PredicateExpression();
//            //filter.Add(CompanyFields.HotSOSUsername != string.Empty);
//            //filter.Add(CompanyFields.HotSOSPassword != string.Empty);

//            this.LogVerbose("ExecuteWork", "Getting companies enabled for HotSOS.");
        
//            var companyCollection = new CompanyCollection();
//            companyCollection.GetMulti(filter);

//            this.LogVerbose("ExecuteWork", "{0} companies found that are enabled for HotSOS.", companyCollection.Count);

//            foreach (CompanyEntity companyEntity in companyCollection)
//            {
//                if (this.CancellationToken.IsCancellationRequested)
//                {
//                    this.LogInfo("ExecuteWork", "Cancellation is requested, breaking loop.");
//                    break;
//                }

//                this.LogVerbose("ExecuteWork", "Checking for service order updates for company '{0}'.", companyEntity.Name);

//                try
//                {
//                    // Initialize the HotSOS connector for the current company
//                    var connector = new HotSOSConnector(companyEntity.CompanyId);

//                    // Get the service order updates
//                    Dictionary<string, Obymobi.Logic.MTechAPI.ServiceOrder> serviceOrders = connector.GetServiceOrderUpdates();

//                    if (serviceOrders.Count == 0)
//                    {
//                        this.LogVerbose("ExecuteWork", "No service order updates were found.");
//                    }
//                    else
//                    {
//                        // Do something awesome
//                        foreach (var serviceOrder in serviceOrders.Values)
//                        {
//                            this.LogVerbose("ExecuteWork", "Service order with ID '{0}' updated.", serviceOrder.ID);
//                        }
//                    }
//                }
//                catch (Exception ex)
//                {
//                    this.LogVerbose("ExecuteWork", "An exception occurred when trying to get the service order updates. Message: {0}", ex.Message);
//                }

//                try
//                {
//                    // Initialize the HotSOS connector for the current company
//                    var connector = new HotSOSConnector(companyEntity);

//                    // Get the reservations
//                    Dictionary<string, Obymobi.Logic.MTechAPI.Reservation> reservations = connector.GetReservations("424");

//                    if (reservations.Count == 0)
//                    {
//                        this.LogVerbose("ExecuteWork", "No reservations were found.");
//                    }
//                    else
//                    {
//                        // Do something awesome
//                        foreach (var reservation in reservations.Values)
//                        {
//                            this.LogVerbose("ExecuteWork", "Reservation found with ID '{0}'.", reservation.ID);
//                        }
//                    }
//                }
//                catch (Exception ex)
//                {
//                    this.LogVerbose("ExecuteWork", "An exception occurred when trying to get the reservations. Message: {0}", ex.Message);
//                }
//            }
//        }
//    }
//}