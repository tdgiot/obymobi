﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Dionysos;
using Dionysos.Logging;
using Obymobi.Constants;
using Obymobi.Data;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using Obymobi.Integrations.Service.HotSOS;
using Obymobi.Logic.Comet;
using Obymobi.Logic.HelperClasses;
using Obymobi.Logic.Routing;
using Obymobi.Logic.ScheduledCommands;
using Obymobi.Logic.ScheduledCommands.Client;
using Obymobi.Logic.ScheduledCommands.Terminal;
using Obymobi.Web.Pollers;
using Obymobi.Web.Slack.Interfaces;
using SD.LLBLGen.Pro.ORMSupportClasses;
using SD.LLBLGen.Pro.QuerySpec;

namespace Pollers
{
    /// <summary>
    /// Summary description for DatabasePoller
    /// </summary>
    public class DatabasePoller : BasePoller
    {
        public DatabasePoller(ISlackClient slackClient) : base(slackClient)
        {
        }

        #region Base Overrides

        public override Enum LoggingType => LoggingTypes.DatabasePoller;

        public override PollerType PollerType => PollerType.DatabasePoller;

        public override IMultiLoggingProvider LoggingProvider => Global.LoggingProvider;

        public override int PrimaryPollerTimeoutSeconds => ObymobiIntervals.SERVICES_DATABASEPOLLER_INTERVAL;

        protected override TimeSpan WorkInterval => TimeSpan.FromMilliseconds(ObymobiIntervals.SERVICES_DATABASEPOLLER_INTERVAL);

        protected override TimeSpan FirstWorkInterval => WorkInterval;

        #endregion

        protected override void ExecuteWork(bool force = false)
        {
            // TODO Check if this server is primary, if not, don't do this work.

            // Check all Routestephandlers for overriding their Time-out
            FailExpiredRoutestephandlers();

            // Start Checking the Operation Modes for Clients.             
            List<int> nonOrderingClients = new List<int>();

            // Update Clients that have Unprocessable Orders            
            this.UpdateClientsWithUnprocessableOrders(ref nonOrderingClients);

            // Update Clients that have Expiring Orders
            this.UpdateClientsWithExpiringOrders(ref nonOrderingClients);

            // Update Client without a Deliverypoint
            this.UpdateClientsWithoutDeliverypoint(ref nonOrderingClients);

            // All clients NOT in nonOrderingClients are OK, so update those to 'Ordering', if they're not already
            this.SwitchHealthyClientsBackOnline(ref nonOrderingClients);

            // Update ClientStates for clients that have gone offline            
            this.UpdateClientStatesForOfflineClients();

            // Update TerminalStates for terminal that have gone offline
            this.UpdateTerminalStatesForOfflineTerminals();
            this.UpdateTerminalStatesForOnlineTerminals();

            // Send Battery Low Notifications for Clients that are below the OutOfCharge level & have less notifications sent than configured to be sent.                                    
            this.HandleBatteryLevelLogic();

            // Check for clients that need to be woken up because the device
            this.HandleClientWakeUpLogic();

            // Auto update
            this.CreateScheduledCommandTasksForUpdates();
        }

        private void FailExpiredRoutestephandlers()
        {
            try
            {
                string errors = RoutingHelper.FailExpiredRoutestephandlers();
                if (errors.Length > 0)
                {
                    LogError("FailExpiredRoutestephandlers", errors);
                }
            }
            catch (Exception ex)
            {
                LogError("FailExpiredRoutestephandlers", "Exception: {0}", ex.Message);
            }
        }

        #region Push Logic

        private void UpdateClientsWithUnprocessableOrders(ref List<int> nonOrderingClients)
        {
            // Check for Unprocessable Orders
            PredicateExpression filter = new PredicateExpression();
            filter.Add(OrderFields.Status == (int) OrderStatus.Unprocessable);
            filter.Add(DeviceFields.LastRequestUTC >= DateTime.UtcNow.AddMilliseconds(-ObymobiIntervals.DEVICE_OFFLINE_THRESHOLD));

            RelationCollection relations = new RelationCollection();
            relations.Add(ClientEntityBase.Relations.OrderEntityUsingClientId);
            relations.Add(ClientEntityBase.Relations.DeviceEntityUsingDeviceId);

            IncludeFieldsList includeFields = new IncludeFieldsList(ClientFields.OperationMode);
            
            ClientCollection clients = new ClientCollection();
            clients.GetMulti(filter, 0, null, relations, null, includeFields, 0, 0);

            this.UpdateAndSaveClients(clients, ClientOperationMode.KioskAutomaticUnprocessableOrders);

            nonOrderingClients.AddRange(clients.Select(c => c.ClientId));
        }

        private void UpdateClientsWithExpiringOrders(ref List<int> nonOrderingClients)
        {
            // Get the basics for joining and filtering to get expired orders
            RoutingHelper.GetExpiredOrdersRetrievalSet(out PredicateExpression filter, out RelationCollection relations);
            filter.Add(DeviceFields.LastRequestUTC >= DateTime.UtcNow.AddMilliseconds(-ObymobiIntervals.DEVICE_OFFLINE_THRESHOLD));

            relations.Add(ClientEntityBase.Relations.OrderEntityUsingClientId);
            relations.Add(ClientEntityBase.Relations.DeviceEntityUsingDeviceId);

            IncludeFieldsList includeFields = new IncludeFieldsList(ClientFields.OperationMode);

            ClientCollection clients = new ClientCollection();
            clients.GetMulti(filter, 0, null, relations, null, includeFields, 0, 0);
            
            List<int> clientIdsToSkip = new List<int>(nonOrderingClients);
            List<ClientEntity> clientList = clients.Where(c => !clientIdsToSkip.Contains(c.ClientId)).ToList();

            this.UpdateAndSaveClients(clientList, ClientOperationMode.KioskAutomaticUnprocessedOrders);

            nonOrderingClients.AddRange(clientList.Select(c => c.ClientId));
        }

        private void UpdateClientsWithoutDeliverypoint(ref List<int> nonOrderingClients)
        {
            // Check for Clients without a DeliverypointId
            PredicateExpression filter = new PredicateExpression(ClientFields.DeliverypointId == DBNull.Value);
            filter.Add(DeviceFields.LastRequestUTC >= DateTime.UtcNow.AddMilliseconds(-ObymobiIntervals.DEVICE_OFFLINE_THRESHOLD));

            RelationCollection relations = new RelationCollection();
            relations.Add(ClientEntityBase.Relations.DeviceEntityUsingDeviceId);

            IncludeFieldsList includeFields = new IncludeFieldsList(ClientFields.OperationMode);

            ClientCollection clients = new ClientCollection();
            clients.GetMulti(filter, 0, null, relations, null, includeFields, 0, 0);

            List<int> clientIdsToSkip = new List<int>(nonOrderingClients);
            List<ClientEntity> clientList = clients.Where(c => !clientIdsToSkip.Contains(c.ClientId)).ToList();

            this.UpdateAndSaveClients(clientList, ClientOperationMode.KioskNoDeliverypointConfigured);

            nonOrderingClients.AddRange(clientList.Select(c => c.ClientId));
        }

        private void SwitchHealthyClientsBackOnline(ref List<int> nonOrderingClients)
        {
            PredicateExpression filter = new PredicateExpression();
            filter.Add(DeviceFields.LastRequestUTC >= DateTime.UtcNow.AddMilliseconds(-ObymobiIntervals.DEVICE_OFFLINE_THRESHOLD));

            PredicateExpression operationModeFilter = new PredicateExpression();
            operationModeFilter.AddWithOr(ClientFields.OperationMode == (int)ClientOperationMode.KioskAutomaticUnprocessableOrders);
            operationModeFilter.AddWithOr(ClientFields.OperationMode == (int)ClientOperationMode.KioskAutomaticUnprocessedOrders);
            operationModeFilter.AddWithOr(ClientFields.OperationMode == (int)ClientOperationMode.KioskNoDeliverypointConfigured);
            operationModeFilter.AddWithOr(ClientFields.OperationMode == (int)ClientOperationMode.KioskDisconnectedFromWebservice);
            operationModeFilter.AddWithOr(ClientFields.OperationMode == (int)ClientOperationMode.KioskAutomaticNoTerminalOnline);
            operationModeFilter.AddWithOr(ClientFields.OperationMode == (int)ClientOperationMode.KioskDisconnectedFromWifi);
            operationModeFilter.AddWithOr(ClientFields.OperationMode == (int)ClientOperationMode.KioskDisconnectedFromPokeIn);
            operationModeFilter.AddWithOr(ClientFields.OperationMode == (int)ClientOperationMode.Unknown);
            filter.Add(operationModeFilter);

            RelationCollection relations = new RelationCollection();
            relations.Add(ClientEntityBase.Relations.DeviceEntityUsingDeviceId);

            IncludeFieldsList includeFields = new IncludeFieldsList(ClientFields.OperationMode);

            ClientCollection clients = new ClientCollection();
            clients.GetMulti(filter, 0, null, relations, null, includeFields, 0, 0);

            List<int> clientIdsToSkip = new List<int>(nonOrderingClients);
            List<ClientEntity> filteredCollection = clients.Where(c => !clientIdsToSkip.Contains(c.ClientId)).ToList();

            this.UpdateAndSaveClients(filteredCollection, ClientOperationMode.Ordering);
        }

        private void UpdateClientStatesForOfflineClients()
        {
            PredicateExpression filter = new PredicateExpression();
            filter.Add(DeviceFields.LastRequestUTC != DBNull.Value);
            filter.Add(DeviceFields.LastRequestUTC > DateTime.UtcNow.AddMilliseconds(ObymobiIntervals.DEVICE_OFFLINE_THRESHOLD * -4));
            filter.Add(DeviceFields.LastRequestUTC < DateTime.UtcNow.AddMilliseconds(ObymobiIntervals.DEVICE_OFFLINE_THRESHOLD * -1));
            filter.Add(ClientFields.LastStateOnline != false);

            RelationCollection relation = new RelationCollection(ClientEntity.Relations.DeviceEntityUsingDeviceId);

            ClientCollection clients = new ClientCollection();
            clients.GetMulti(filter, relation);

            if (!clients.Any())
            {
                return;
            }

            using (Transaction transaction = new Transaction(IsolationLevel.ReadUncommitted, nameof(UpdateClientStatesForOfflineClients)))
            {
                try
                {
                    foreach (ClientEntity clientEntity in clients)
                    {
                        ClientStateHelper.UpdateClientState(clientEntity, false, transaction);
                    }

                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    LogError(nameof(UpdateClientStatesForOfflineClients), "Transaction error: {0}", ex.Message);
                    transaction.Rollback();
                }
            }
        }

        private void UpdateTerminalStatesForOfflineTerminals()
        {
            PredicateExpression filter = new PredicateExpression();
            filter.Add(DeviceFields.LastRequestUTC != DBNull.Value);
            filter.Add(DeviceFields.LastRequestUTC < DateTime.UtcNow.AddMilliseconds(ObymobiIntervals.DEVICE_OFFLINE_THRESHOLD * -1));
            filter.Add(TerminalFields.LastStateOnline == true);

            RelationCollection relation = new RelationCollection();
            relation.Add(TerminalEntityBase.Relations.DeviceEntityUsingDeviceId);

            TerminalCollection terminals = new TerminalCollection();
            terminals.GetMulti(filter, relation);

            foreach (TerminalEntity terminalEntity in terminals)
            {                
                TerminalStateHelper.UpdateTerminalState(terminalEntity, false);
            }
        }

        private void UpdateTerminalStatesForOnlineTerminals()
        {
            PredicateExpression filter = new PredicateExpression();
            filter.Add(DeviceFields.LastRequestUTC != DBNull.Value);
            filter.Add(DeviceFields.LastRequestUTC > DateTime.UtcNow.AddMilliseconds(ObymobiIntervals.DEVICE_OFFLINE_THRESHOLD * -1));
            filter.Add(TerminalFields.LastStateOnline == false);

            RelationCollection relation = new RelationCollection();
            relation.Add(TerminalEntityBase.Relations.DeviceEntityUsingDeviceId);

            TerminalCollection terminals = new TerminalCollection();
            terminals.GetMulti(filter, relation);

            foreach (TerminalEntity terminalEntity in terminals)
            {
                TerminalStateHelper.UpdateTerminalState(terminalEntity, true);
            }
        }

        private void HandleBatteryLevelLogic()
        {
            // Only to it for Clients seen maximum of 5 minutes ago (otherwise it might just be 'dead' clients)
            // Also we only remind maximum of once every 5 minutes.
            ClientCollection clientCollection = new ClientCollection();
            RelationCollection joins = new RelationCollection();
            joins.Add(ClientEntityBase.Relations.DeliverypointgroupEntityUsingDeliverypointGroupId);
            joins.Add(ClientEntityBase.Relations.DeviceEntityUsingDeviceId);

            DateTime minimumLastOnline = DateTime.UtcNow.AddMinutes(-5);

            PredicateExpression filter = new PredicateExpression();
            filter.Add(new FieldLikePredicate(DeviceFields.Identifier, "%OTOUCHO%", true)); // Exclude Otoucho clients
            filter.Add(DeviceFields.LastRequestUTC > minimumLastOnline);
            filter.Add(DeviceFields.IsCharging == false);
            filter.Add(ClientFields.DeliverypointGroupId != DBNull.Value);
            filter.Add(DeliverypointgroupFields.OutOfChargeLevel > 0);
            filter.Add(DeliverypointgroupFields.EnableBatteryLowConsoleNotifications == true);
            filter.Add(DeviceFields.BatteryLevel <= DeliverypointgroupFields.OutOfChargeLevel);
            filter.Add(ClientFields.OutOfChargeNotificationsSent < DeliverypointgroupFields.OutOfChargeNotificationAmount);

            PredicateExpression subfilter = new PredicateExpression();
            subfilter.AddWithOr(ClientFields.OutOfChargeNotificationLastSentUTC == DBNull.Value);
            subfilter.AddWithOr(ClientFields.OutOfChargeNotificationLastSentUTC < minimumLastOnline);
            filter.Add(subfilter);

            // Create a (large) prefetch path for the routes, because we need to detect if it will pass a POS connector, which would require a product in  the order.                        
            // Client
            PrefetchPath prefetchClient = new PrefetchPath(EntityType.ClientEntity);

            // Client > Deliverypointgroup
            IPrefetchPathElement prefetchClientDeliverypointgroup = prefetchClient.Add(ClientEntityBase.PrefetchPathDeliverypointgroupEntity);
            IncludeFieldsList dpgFields = new IncludeFieldsList();
            dpgFields.Add(DeliverypointgroupFields.OutOfChargeLevel);
            dpgFields.Add(DeliverypointgroupFields.HotSOSBatteryLowProductId);
            prefetchClientDeliverypointgroup.ExcludedIncludedFields = dpgFields;

            IncludeFieldsList dontLoadFields = new IncludeFieldsList(); // We don't need the fields of the Route related entites, FKs are always included.            

            // Client > Deliverypointgroup > Route
            IPrefetchPathElement prefetchClientDeliverypointgroupRoute = prefetchClientDeliverypointgroup.SubPath.Add(DeliverypointgroupEntityBase.PrefetchPathSystemMessageRouteEntity);
            prefetchClientDeliverypointgroupRoute.ExcludedIncludedFields = dontLoadFields;

            // Client > Deliverypointgroup > Route > Routestep
            IPrefetchPathElement prefetchClientDeliverypointgroupRouteRoutestep = prefetchClientDeliverypointgroupRoute.SubPath.Add(RouteEntityBase.PrefetchPathRoutestepCollection);
            prefetchClientDeliverypointgroupRouteRoutestep.ExcludedIncludedFields = dontLoadFields;

            // Client > Deliverypointgroup > Route > Routestep > Routestephandler
            IPrefetchPathElement prefetchClientDeliverypointgroupRouteRoutestepRoutestephandler = prefetchClientDeliverypointgroupRouteRoutestep.SubPath.Add(RoutestepEntityBase.PrefetchPathRoutestephandlerCollection);
            prefetchClientDeliverypointgroupRouteRoutestepRoutestephandler.ExcludedIncludedFields = dontLoadFields;

            IncludeFieldsList terminalFields = new IncludeFieldsList();
            terminalFields.Add(TerminalFields.POSConnectorType);
            terminalFields.Add(TerminalFields.BatteryLowProductId);

            // Client > Deliverypointgroup > Route > Routestep > Routestephandler > Terminal
            IPrefetchPathElement prefetchClientDeliverypointgroupRouteRoutestepRoutestephandlerTerminal = prefetchClientDeliverypointgroupRouteRoutestepRoutestephandler.SubPath.Add(RoutestephandlerEntityBase.PrefetchPathTerminalEntity);
            prefetchClientDeliverypointgroupRouteRoutestepRoutestephandlerTerminal.ExcludedIncludedFields = terminalFields;

            clientCollection.GetMulti(filter, 0, null, joins, prefetchClient, 0, 0);

            foreach (ClientEntity client in clientCollection)
            {
                if (HotSOSHelper.WriteOutOfChargeNotification(client))
                {
                    client.OutOfChargeNotificationsSent++;
                    client.Save();
                }
            }

            // Reset the OutOfChargeNotificationsSent value to 0 for all Clients that are on a good level.
            // A update multi is used to prevent loading all the clients.
            joins = new RelationCollection();
            joins.Add(ClientEntity.Relations.DeliverypointgroupEntityUsingDeliverypointGroupId);
            joins.Add(ClientEntity.Relations.DeviceEntityUsingDeviceId);

            PredicateExpression filterGoodBatteryLevelClients = new PredicateExpression();
            filterGoodBatteryLevelClients.Add(ClientFields.OutOfChargeNotificationsSent > 0);
            filterGoodBatteryLevelClients.Add(DeviceFields.BatteryLevel > DeliverypointgroupFields.OutOfChargeLevel);

            ClientEntity clientUpdateValues = new ClientEntity();
            clientUpdateValues.OutOfChargeNotificationsSent = 0;
            clientUpdateValues.OutOfChargeNotificationLastSentUTC = new DateTime(2000, 1, 1);

            clientCollection.UpdateMulti(clientUpdateValues, filterGoodBatteryLevelClients, joins);
        }

        private void HandleClientWakeUpLogic()
        {
            DateTime sixtyMinutesBeforeUtcNow = DateTime.UtcNow.AddMinutes(-60);
            DateTime twentyMinutesAfterUtcNow = DateTime.UtcNow.AddMinutes(20);

            PredicateExpression filter = new PredicateExpression();
            filter.Add(ClientFields.WakeUpTimeUtc != DBNull.Value);                     // Wake up time utc is set
            filter.Add(ClientFields.WakeUpTimeUtc > sixtyMinutesBeforeUtcNow);          // And wake up time utc bigger than 60 minutes in the past
            filter.Add(ClientFields.WakeUpTimeUtc <= twentyMinutesAfterUtcNow);         // And wake up time utc smaller or equal than 20 minutes from now
            filter.Add(ClientFields.DeliverypointId != DBNull.Value);

            RelationCollection relations = new RelationCollection();
            relations.Add(ClientEntity.Relations.DeviceEntityUsingDeviceId);

            PrefetchPath prefetch = new PrefetchPath(EntityType.ClientEntity);
            prefetch.Add(ClientEntityBase.PrefetchPathDeviceEntity);

            ClientCollection clients = new ClientCollection();
            clients.GetMulti(filter, 0, null, relations, prefetch);

            LogVerbose("HandleClientWakeUpLogic", "Clients between: {0} and {1}", sixtyMinutesBeforeUtcNow, twentyMinutesAfterUtcNow);

            DateTime offlineThreshold = DateTime.UtcNow.AddMilliseconds(ObymobiIntervals.DEVICE_OFFLINE_THRESHOLD * -1);

            foreach (ClientEntity clientEntity in clients)
            {
                if (!clientEntity.DeviceId.HasValue)
                    continue;

                DateTime convertedDateTime = clientEntity.WakeUpTimeUtc.Value.UtcToLocalTime(clientEntity.CompanyEntity.TimeZoneInfo);
                LogVerbose("HandleClientWakeUpLogic", "Client: {0} | WakeUp UTC: {1} | WakeUp Local: {2} | Battery: {3} | Last Request UTC: {4}", clientEntity.ClientId, clientEntity.WakeUpTimeUtc, convertedDateTime, clientEntity.DeviceEntity.BatteryLevel, clientEntity.DeviceEntity.LastRequestUTC);

                bool writeIt = false;
                string wakeUpReason = "";
                if (clientEntity.DeviceEntity.BatteryLevel <= 20 && !clientEntity.DeviceEntity.IsCharging)
                {
                    LogVerbose("HandleClientWakeUpLogic", "Due to battery: {0}", clientEntity.ClientId);
                    writeIt = true;
                    wakeUpReason = string.Format("Low battery level ({0}%)", clientEntity.DeviceEntity.BatteryLevel);
                }
                else if (clientEntity.DeviceEntity.LastRequestUTC < offlineThreshold)
                {
                    LogVerbose("HandleClientWakeUpLogic", "Due to being offline: {0}", clientEntity.ClientId);
                    writeIt = true;
                    wakeUpReason = string.Format("Client is offline. Last request: {0} (UTC)", clientEntity.DeviceEntity.LastRequestUTC);
                }

                if (writeIt)
                {
                    ClientLogEntity logEntity = new ClientLogEntity();
                    logEntity.ClientId = clientEntity.ClientId;
                    logEntity.TypeEnum = ClientLogType.ClientWakeUpSet;
                    logEntity.Message = string.Format("Sending wakeup notification.\n\rReason: {0}\n\rWakeUp UTC: {1}\n\rWakeUp Local: {2}", wakeUpReason, clientEntity.WakeUpTimeUtc, convertedDateTime);
                    logEntity.CreatedUTC = DateTime.UtcNow;
                    logEntity.Save();

                    ClientHelper.WriteWakeUpNotification(clientEntity);
                    clientEntity.WakeUpTimeUtc = null;
                    clientEntity.Save();
                }                
            }
        }

        private void UpdateAndSaveClients(IList<ClientEntity> clients, ClientOperationMode operationMode)
        {
            if (!clients.Any())
            {
                return;
            }

            ClientEntity clientEntityWithNewOperationMode = new ClientEntity();
            clientEntityWithNewOperationMode.OperationMode = (int)operationMode;

            IList<ClientEntity> clientsToUpdate = clients.Where(x => x.OperationModeEnum != operationMode &&
                                                                           x.OperationModeEnum != ClientOperationMode.KioskManual &&
                                                                           x.OperationModeEnum != ClientOperationMode.Disabled).ToList();

            int batchSize = 1000;
            int totalBatches = (int)System.Math.Ceiling(clientsToUpdate.Count / (double)batchSize);
            for (int batch = 0; batch < totalBatches; batch++)
            {
                IList<ClientEntity> bucket = clientsToUpdate.Skip(batch * batchSize).Take(batchSize).ToList();
                if (bucket.Count == 0) break;

                PredicateExpression filter = new PredicateExpression();
                filter.Add(ClientFields.ClientId == bucket.Select(x => x.ClientId).ToList());
                int affectedEntities = new ClientCollection().UpdateMulti(clientEntityWithNewOperationMode, filter);

                LogVerbose("UpdateAndSaveClients", "Batch #{0} - Updating {1} clients (affected: {2}) to operation mode {3}", batch, bucket.Count, affectedEntities, operationMode.ToString());
            }
            
            foreach (ClientEntity client in clientsToUpdate)
            {
                CometHelper.SetClientOperationMode(client.ClientId, operationMode, client.GetCurrentTransaction());
            }
        }

        private void CreateScheduledCommandTasksForUpdates()
        {
            PredicateExpression companyReleaseFilter = new PredicateExpression();
            companyReleaseFilter.Add(CompanyReleaseFields.AutoUpdate == true);

            PrefetchPath prefetch = new PrefetchPath(EntityType.CompanyEntity);
            prefetch.Add(CompanyEntity.PrefetchPathCompanyReleaseCollection)
                    .SubPath.Add(CompanyReleaseEntity.PrefetchPathReleaseEntity, new IncludeFieldsList { ReleaseFields.Version })
                    .SubPath.Add(ReleaseEntity.PrefetchPathApplicationEntity, new IncludeFieldsList { ApplicationFields.Code });
            
            RelationCollection relations = new RelationCollection();
            relations.Add(CompanyEntity.Relations.CompanyReleaseEntityUsingCompanyId);

            CompanyCollection companyCollection = new CompanyCollection();
            companyCollection.GetMulti(companyReleaseFilter, 0, null, relations, prefetch, new IncludeFieldsList(CompanyFields.CompanyId), 0, 0); 

            if (!companyCollection.Any())
            {
                LogVerbose("CreateScheduledCommandTasksForUpdates", "No company were found that have automatic updates enabled.");
                return;
            }

            foreach (CompanyEntity companyEntity in companyCollection)
            {
                // Check whether a scheduled command task is already running for the company
                PredicateExpression activefilter = new PredicateExpression();
                activefilter.Add(ScheduledCommandTaskFields.CompanyId == companyEntity.CompanyId);
                activefilter.Add(ScheduledCommandTaskFields.Active == true);
                activefilter.Add(ScheduledCommandTaskFields.ExpiresUTC > DateTime.UtcNow);

                PredicateExpression scheduledCommandTaskStatusFilter = new PredicateExpression();
                scheduledCommandTaskStatusFilter.Add(ScheduledCommandTaskFields.Status == ScheduledCommandTaskStatus.Started);
                scheduledCommandTaskStatusFilter.AddWithOr(ScheduledCommandTaskFields.Status == ScheduledCommandTaskStatus.Pending);
                activefilter.Add(scheduledCommandTaskStatusFilter);
                
                PredicateExpression inactivefilter = new PredicateExpression();
                inactivefilter.Add(ScheduledCommandTaskFields.CompanyId == companyEntity.CompanyId);
                inactivefilter.Add(ScheduledCommandTaskFields.Active == false);
                inactivefilter.Add(ScheduledCommandTaskFields.ExpiresUTC.Between(DateTime.UtcNow.AddHours(-4), DateTime.UtcNow.AddHours(4)));
                
                PredicateExpression filter = new PredicateExpression();
                filter.Add(activefilter);
                filter.AddWithOr(inactivefilter);
                
                ScheduledCommandTaskCollection scheduledCommandTaskCollection = new ScheduledCommandTaskCollection();
                scheduledCommandTaskCollection.GetMulti(filter);

                if (scheduledCommandTaskCollection.Any())
                {
                    LogVerbose("CreateScheduledCommandTasksForUpdates", "A scheduled command task is already running for company '{0}'.", companyEntity.CompanyId);
                    continue;
                }

                ScheduledCommandCollection commands = new ScheduledCommandCollection();

                foreach (CompanyReleaseEntity companyReleaseEntity in companyEntity.CompanyReleaseCollection.Where(x => x.AutoUpdate))
                {
                    commands.AddRange(this.CreateScheduledCommandsForClients(companyReleaseEntity));
                    commands.AddRange(this.CreateScheduledCommandsForTerminals(companyReleaseEntity));
                }

                if (commands.Count <= 0)
                {
                    LogVerbose("CreateScheduledCommandTasksForUpdates", "No clients and terminals found for company '{0}' that are outdated and online.", companyEntity.CompanyId);
                    continue;
                }

                ScheduledCommandTaskEntity scheduledCommandTaskEntity = new ScheduledCommandTaskEntity();
                scheduledCommandTaskEntity.CompanyId = companyEntity.CompanyId;
                scheduledCommandTaskEntity.Active = true;
                scheduledCommandTaskEntity.StartUTC = DateTime.UtcNow.AddMinutes(2);
                scheduledCommandTaskEntity.ExpiresUTC = DateTime.UtcNow.AddHours(4);
                scheduledCommandTaskEntity.ScheduledCommandCollection.AddRange(commands);                

                if (scheduledCommandTaskEntity.Save(true))
                {
                    LogVerbose("CreateScheduledCommandTasksForUpdates", "Scheduled command task created for company '{0}'.", companyEntity.CompanyId);
                }
                else
                {
                    LogVerbose("CreateScheduledCommandTasksForUpdates", "Something went wrong whilst trying to save the scheduled command task for company '{0}'.", companyEntity.CompanyId);
                    continue;
                }
            }
        }

        private ScheduledCommandCollection CreateScheduledCommandsForClients(CompanyReleaseEntity companyReleaseEntity)
        {
            ScheduledCommandCollection scheduledCommandCollection = new ScheduledCommandCollection();

            ReleaseEntity releaseEntity = companyReleaseEntity.ReleaseEntity;
            string applicationCode = releaseEntity.ApplicationEntity.Code;
            string releaseVersion = releaseEntity.Version;
            int companyId = companyReleaseEntity.CompanyId;
            Dictionary<ClientCommand, ClientCommandSchedulerBase> clientCommands;

            EntityField versionField = null;
            if (applicationCode == ApplicationCode.Emenu)
            {
                versionField = DeviceFields.ApplicationVersion;
                clientCommands = new Dictionary<ClientCommand, ClientCommandSchedulerBase>
                    {
                        { ClientCommand.DownloadEmenuUpdate, CommandSchedulers.GetClientCommandScheduler(ClientCommand.DownloadEmenuUpdate) },
                        { ClientCommand.InstallEmenuUpdate, CommandSchedulers.GetClientCommandScheduler(ClientCommand.InstallEmenuUpdate) }
                    };
            }
            else if (applicationCode == ApplicationCode.Agent)
            {
                versionField = DeviceFields.AgentVersion;
                clientCommands = new Dictionary<ClientCommand, ClientCommandSchedulerBase>
                    {
                        { ClientCommand.DownloadAgentUpdate, CommandSchedulers.GetClientCommandScheduler(ClientCommand.DownloadAgentUpdate) },
                        { ClientCommand.InstallAgentUpdate, CommandSchedulers.GetClientCommandScheduler(ClientCommand.InstallAgentUpdate) }
                    };
            }
            else if (applicationCode == ApplicationCode.SupportTools)
            {
                versionField = DeviceFields.SupportToolsVersion;
                clientCommands = new Dictionary<ClientCommand, ClientCommandSchedulerBase>
                    {
                        { ClientCommand.DownloadSupportToolsUpdate, CommandSchedulers.GetClientCommandScheduler(ClientCommand.DownloadSupportToolsUpdate) },
                        { ClientCommand.InstallSupportToolsUpdate, CommandSchedulers.GetClientCommandScheduler(ClientCommand.InstallSupportToolsUpdate) }
                    };
            }
            else if (applicationCode == ApplicationCode.MessagingService)
            {
                versionField = DeviceFields.MessagingServiceVersion;
                clientCommands = new Dictionary<ClientCommand, ClientCommandSchedulerBase>
                {
                    { ClientCommand.DownloadInstallMessagingServiceUpdate, CommandSchedulers.GetClientCommandScheduler(ClientCommand.DownloadInstallMessagingServiceUpdate) }
                };
            }
            else
                return scheduledCommandCollection;

            // Get the outdated client devices for this release
            PredicateExpression clientFilter = new PredicateExpression();
            clientFilter.Add(versionField < releaseVersion);
            clientFilter.Add(ClientFields.CompanyId == companyId);
            clientFilter.Add(DeviceFields.LastRequestUTC >= DateTime.UtcNow.AddMinutes(-3));

            RelationCollection clientRelations = new RelationCollection();
            clientRelations.Add(ClientEntity.Relations.DeviceEntityUsingDeviceId);

            ClientCollection clientCollection = new ClientCollection();
            clientCollection.GetMulti(clientFilter, 0, null, clientRelations, null, new IncludeFieldsList { ClientFields.CompanyId }, 0, 0);

            if (!clientCollection.Any())
            {
                return scheduledCommandCollection;
            }

            foreach (ClientCommand clientCommand in clientCommands.Keys)
            {
                ClientCommandSchedulerBase scheduler = clientCommands[clientCommand];

                foreach (ClientEntity clientEntity in clientCollection)
                {
                    if (!scheduler.CanCommandBeAdded(clientEntity))
                    {
                        LogVerbose("CreateScheduledCommandsForClients", "Command '{0}' could not be added for client '{1}' of company '{2}'.", clientCommand, clientEntity.ClientId, companyId);
                        continue;
                    }

                    ScheduledCommandEntity scheduledCommandEntity = new ScheduledCommandEntity();
                    scheduledCommandEntity.ParentCompanyId = clientEntity.CompanyId;
                    scheduledCommandEntity.Validator = null;
                    scheduledCommandEntity.ClientCommand = clientCommand;
                    scheduledCommandEntity.ClientId = clientEntity.ClientId;
                    scheduledCommandEntity.Status = ScheduledCommandStatus.Pending;
                    scheduledCommandCollection.Add(scheduledCommandEntity);
                }
            }

            return scheduledCommandCollection;            
        }

        private ScheduledCommandCollection CreateScheduledCommandsForTerminals(CompanyReleaseEntity companyReleaseEntity)
        {
            ScheduledCommandCollection scheduledCommandCollection = new ScheduledCommandCollection();

            ReleaseEntity releaseEntity = companyReleaseEntity.ReleaseEntity;
            string applicationCode = releaseEntity.ApplicationEntity.Code;
            string releaseVersion = releaseEntity.Version;
            int companyId = companyReleaseEntity.CompanyId;
            Dictionary<TerminalCommand, TerminalCommandSchedulerBase> terminalCommands;

            EntityField versionField = null;
            if (applicationCode == ApplicationCode.Console)
            {
                versionField = DeviceFields.ApplicationVersion;
                terminalCommands = new Dictionary<TerminalCommand, TerminalCommandSchedulerBase>
                    {
                        { TerminalCommand.DownloadConsoleUpdate, CommandSchedulers.GetTerminalCommandScheduler(TerminalCommand.DownloadConsoleUpdate) },
                        { TerminalCommand.InstallConsoleUpdate, CommandSchedulers.GetTerminalCommandScheduler(TerminalCommand.InstallConsoleUpdate) }
                    };
            }
            else
                return scheduledCommandCollection;

            // Get the outdated terminal devices for this release
            PredicateExpression terminalFilter = new PredicateExpression();
            terminalFilter.Add(versionField < releaseVersion);
            terminalFilter.Add(TerminalFields.CompanyId == companyId);
            terminalFilter.Add(DeviceFields.LastRequestUTC >= DateTime.UtcNow.AddMinutes(-3));

            RelationCollection terminalRelations = new RelationCollection();
            terminalRelations.Add(TerminalEntity.Relations.DeviceEntityUsingDeviceId);

            TerminalCollection terminalCollection = new TerminalCollection();
            terminalCollection.GetMulti(terminalFilter, 0, null, terminalRelations, null, new IncludeFieldsList { TerminalFields.CompanyId }, 0, 0);

            if (!terminalCommands.Any())
            {
                return scheduledCommandCollection;
            }

            foreach (TerminalCommand terminalCommand in terminalCommands.Keys)
            {
                TerminalCommandSchedulerBase scheduler = terminalCommands[terminalCommand];

                foreach (TerminalEntity terminalEntity in terminalCollection)
                {
                    if (!scheduler.CanCommandBeAdded(terminalEntity))
                    {
                        LogVerbose("CreateScheduledCommandsForTerminals", "Command '{0}' could not be added for terminal '{1}' of company '{2}'.", terminalCommand, terminalEntity.TerminalId, companyId);
                        continue;
                    }

                    // Add commands for client commands
                    ScheduledCommandEntity scheduledCommandEntity = new ScheduledCommandEntity();
                    scheduledCommandEntity.ParentCompanyId = terminalEntity.CompanyId;
                    scheduledCommandEntity.Validator = null;                    
                    scheduledCommandEntity.TerminalCommand = terminalCommand;
                    scheduledCommandEntity.TerminalId = terminalEntity.TerminalId;
                    scheduledCommandEntity.Status = ScheduledCommandStatus.Pending;
                    scheduledCommandCollection.Add(scheduledCommandEntity);
                }
            }

            return scheduledCommandCollection;
        }

        #endregion
    }
}