﻿using DevExpress.XtraScheduler;
using Dionysos;
using Dionysos.Logging;
using Obymobi.Data;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using Obymobi.Logic;
using Obymobi.Web.Pollers;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System;
using System.Diagnostics;
using System.Threading;
using Obymobi.Web.Slack.Interfaces;

namespace Pollers
{
    /// <summary>
    /// Deletes media linked to a scheduled item when the schedule ends.
    /// </summary>
    public class CleanScheduledMediaPoller : BasePoller
    {
        private static DateTime nextCleanup;

        public CleanScheduledMediaPoller(ISlackClient slackClient) : base(slackClient)
        {
        }

        #region Base Overrides

        public override Enum LoggingType
        {
            get { return LoggingTypes.CleanScheduledMediaPoller; }
        }

        public override PollerType PollerType
        {
            get { return PollerType.CleanScheduledMediaPoller; }
        }

        public override IMultiLoggingProvider LoggingProvider
        {
            get { return Global.LoggingProvider; }
        }

        public override int PrimaryPollerTimeoutSeconds
        {
            get { return 30 * 60; } // 30 mins
        }

        /// <summary>
        /// NOT USED! This poller calculates its own <see cref="TimeTillNextWork"/> TimeSpan
        /// </summary>
        protected override TimeSpan WorkInterval
        {
            get { return new TimeSpan(1, 0, 0, 0); }
        }

        /// <summary>
        /// NOT USED! This poller calculates its own <see cref="TimeTillNextWork"/> TimeSpan
        /// </summary>
        protected override TimeSpan FirstWorkInterval
        {
            get { return new TimeSpan(1, 0, 0, 0); }
        }

        #endregion

        public override void Initialize(CancellationToken cancellationToken)
        {
            // By added hours to "Today", we can let this poller run each day at specific time
            nextCleanup = DateTime.Today.AddHours(4);            

            SetNextCleanupTime();

            base.Initialize(cancellationToken);
        }

        public override TimeSpan TimeTillNextWork()
        {
            var timeTillNextWork = nextCleanup.Subtract(DateTime.Now);
            return timeTillNextWork;
        }

        protected override void ExecuteWork(bool force = false)
        {
            if (DateTime.Now < nextCleanup)
                return;

            DoCleanup();
            SetNextCleanupTime();
        }

        private void SetNextCleanupTime()
        {
            if (DateTime.Now >= nextCleanup)
            {
                // Set next cleanup time to tomorrow
                nextCleanup = nextCleanup.AddDays(1);                
            }

            LogInfo("SetNextCleanupTime", "Next clean up scheduled at: {0}", nextCleanup.ToString("dd/MM/yyyy HH:mm:ss"));
        }

        private void DoCleanup()
        {
            try
            {
                Stopwatch sw = new Stopwatch();
                sw.Start();
                
                this.LogInfo("DoCleanup", "Started cleaning");

                UIScheduleItemCollection scheduledMediaItems = this.FetchScheduledMediaItems();
                UIScheduleItemCollection expiredItems = this.FilterExpiredItems(scheduledMediaItems);
                this.DeleteExpiredItems(expiredItems);

                sw.Stop();
                this.LogInfo("DoCleanup", "Completed cleaning in {0}", sw.Elapsed);            
            }
            catch (Exception ex)
            {
                this.LogError("DoCleanup", "Failed to do the clean up: {0}", ex.Message);
            }
        }

        private UIScheduleItemCollection FilterExpiredItems(UIScheduleItemCollection scheduledItems)
        {
            UIScheduleItemCollection expiredItems = new UIScheduleItemCollection();
            foreach (UIScheduleItemEntity scheduledItem in scheduledItems)
            {
                if (!this.IsScheduledInFuture(scheduledItem))
                {
                    expiredItems.Add(scheduledItem);                    
                }
            }
            return expiredItems;
        }

        private void DeleteExpiredItems(UIScheduleItemCollection expiredItems)
        {
            MediaCollection mediaToDelete = new MediaCollection();

            foreach (UIScheduleItemEntity expiredItem in expiredItems)
            {
                this.LogInfo("DoCleanup", "Deleting expired item {0} for company {1} on schedule {2}", expiredItem.UIScheduleItemId, expiredItem.UIScheduleEntity.CompanyEntity.Name, expiredItem.UIScheduleEntity.Name);

                MediaEntity mediaEntity = expiredItem.MediaEntity;
                mediaEntity.UIScheduleItemCollection.Remove(expiredItem);
                if (mediaEntity.UIScheduleItemCollection.Count <= 0)
                {
                    this.LogInfo("DoCleanup", "Deleting related media first {0}", mediaEntity.Name);
                    mediaToDelete.Add(mediaEntity);
                }
            }

            mediaToDelete.DeleteMulti();
            expiredItems.DeleteMulti();
        }

        private UIScheduleItemCollection FetchScheduledMediaItems()
        {
            PredicateExpression filter = new PredicateExpression();
            filter.Add(UIScheduleItemFields.MediaId != DBNull.Value);

            RelationCollection relations = new RelationCollection();
            relations.Add(UIScheduleItemEntityBase.Relations.MediaEntityUsingMediaId);
            relations.Add(UIScheduleItemEntityBase.Relations.UIScheduleEntityUsingUIScheduleId);
            relations.Add(UIScheduleEntityBase.Relations.CompanyEntityUsingCompanyId);

            PrefetchPath prefetch = new PrefetchPath(EntityType.UIScheduleItemEntity);
            IPrefetchPathElement prefetchOccurrences = prefetch.Add(UIScheduleItemEntityBase.PrefetchPathUIScheduleItemOccurrenceCollection);
            IPrefetchPathElement prefetchUiSchedule = prefetch.Add(UIScheduleItemEntityBase.PrefetchPathUIScheduleEntity, new IncludeFieldsList(UIScheduleFields.Name, UIScheduleFields.CompanyId));
            IPrefetchPathElement prefetchUiScheduleCompany = prefetchUiSchedule.SubPath.Add(UIScheduleEntityBase.PrefetchPathCompanyEntity, new IncludeFieldsList(CompanyFields.Name, CompanyFields.TimeZoneOlsonId));

            SortExpression sort = new SortExpression(new SortClause(UIScheduleItemFields.UIScheduleItemId, SortOperator.Ascending));
            sort.Add(new SortClause(CompanyFields.Name, SortOperator.Ascending));

            UIScheduleItemCollection items = new UIScheduleItemCollection();
            items.GetMulti(filter, 0, sort, relations, prefetch);

            return items;
        }

        private bool IsScheduledInFuture(UIScheduleItemEntity scheduledItem)
        {
            foreach (UIScheduleItemOccurrenceEntity occurrenceEntity in scheduledItem.UIScheduleItemOccurrenceCollection)
            {
                if (this.IsScheduledInFuture(occurrenceEntity))
                {
                    return true;
                }
            }
            return false;
        }

        private bool IsScheduledInFuture(UIScheduleItemOccurrenceEntity occurrenceEntity)
        {
            DateTime companyNow = DateTime.UtcNow.UtcToLocalTime(occurrenceEntity.UIScheduleItemEntity.UIScheduleEntity.CompanyEntity.TimeZoneInfo);
            if (!occurrenceEntity.Recurring)
            {
                return companyNow < occurrenceEntity.EndTime.GetValueOrDefault();
            }
            else
            {
                Appointment appointment = new DevExpressAppointment(AppointmentType.Pattern);
                appointment.RecurrenceInfo.Type = (DevExpress.XtraScheduler.RecurrenceType)occurrenceEntity.RecurrenceType;
                appointment.RecurrenceInfo.Range = (DevExpress.XtraScheduler.RecurrenceRange)occurrenceEntity.RecurrenceRange;
                appointment.RecurrenceInfo.Start = occurrenceEntity.RecurrenceStart.GetValueOrDefault();
                appointment.RecurrenceInfo.End = occurrenceEntity.RecurrenceEnd.GetValueOrDefault();
                appointment.RecurrenceInfo.OccurrenceCount = occurrenceEntity.RecurrenceOccurrenceCount;
                appointment.RecurrenceInfo.Periodicity = occurrenceEntity.RecurrencePeriodicity;
                appointment.RecurrenceInfo.DayNumber = occurrenceEntity.RecurrenceDayNumber;
                appointment.RecurrenceInfo.WeekDays = (WeekDays)occurrenceEntity.RecurrenceWeekDays;
                appointment.RecurrenceInfo.WeekOfMonth = (WeekOfMonth)occurrenceEntity.RecurrenceWeekOfMonth;
                appointment.RecurrenceInfo.Month = occurrenceEntity.RecurrenceMonth;

                OccurrenceCalculator occurrenceCalculator = OccurrenceCalculator.CreateInstance(appointment.RecurrenceInfo);
                DateTime nextStart = occurrenceCalculator.FindNextOccurrenceTimeAfter(companyNow, appointment);
                return nextStart != DateTime.MaxValue;                
            }            
        }        
    }
}