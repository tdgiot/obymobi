﻿using System;
using System.Threading;
using Dionysos.Logging;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using Obymobi.Web.Pollers;
using Obymobi.Web.Slack.Interfaces;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Pollers
{
    public class NetmessageCleaner : BasePoller
    {
        private const int NETMESSAGE_CLEANUP_DAYS = 2;

        public NetmessageCleaner(ISlackClient slackClient) : base(slackClient)
        {
        }

        #region Base Overrides

        public override Enum LoggingType
        {
            get { return LoggingTypes.NetmessageCleaner; }
        }

        public override PollerType PollerType
        {
            get { return PollerType.NetmessageCleanerPoller; }
        }

        public override IMultiLoggingProvider LoggingProvider
        {
            get { return Global.LoggingProvider; }
        }

        public override int PrimaryPollerTimeoutSeconds
        {
            get { return 5 * 60; } // 5 mins
        }

        protected override TimeSpan WorkInterval
        {
            get { return new TimeSpan(0, 1, 0, 0); }
        }

        protected override TimeSpan FirstWorkInterval
        {
            get { return new TimeSpan(0, 0, 30, 0); }
        }

        #endregion

        protected override void ExecuteWork(bool force = false)
        {
            PredicateExpression filter = new PredicateExpression();
            filter.Add(NetmessageFields.CreatedUTC < DateTime.UtcNow.AddDays(-NETMESSAGE_CLEANUP_DAYS));

            NetmessageCollection netmessages = new NetmessageCollection();
            netmessages.DeleteMulti(filter);
        }
    }
}