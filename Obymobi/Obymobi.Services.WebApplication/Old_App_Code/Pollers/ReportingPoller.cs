﻿using System;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text;
using Dionysos;
using Dionysos.Net.Mail;
using Obymobi;
using Obymobi.Constants;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using Obymobi.Web.Pollers;
using Obymobi.Data.EntityClasses;
using Obymobi.Services.Logic.Analytics;
using Obymobi.Services.Logic.Analytics.ReportBuilders;
using Obymobi.Services.WebApplication.Properties;
using Obymobi.Web.Analytics.Repositories;
using Obymobi.Web.Analytics.Repositories.TransactionsAppLess;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Web.Slack.Interfaces;

namespace Pollers
{
    /// <summary>
    /// Summary description for ReportingPoller
    /// </summary>
    public class ReportingPoller : BasePoller
    {
        private const string ORIGINATOR_NAME = "Crave Interactive";
        private const string ORIGINATOR_EMAIL = "support@crave-emenu.com";
        private const string EMAIL_ATTACHMENT_CONTENT_TYPE = "application/vnd.ms-excel";
        private const string EMAIL_SUBJECT = "[Crave Report] {0}";

        public override Dionysos.Logging.IMultiLoggingProvider LoggingProvider => Global.LoggingProvider;
        public override Enum LoggingType => LoggingTypes.ReportingPoller;
        public override PollerType PollerType => PollerType.ReportingPoller;
        public override int PrimaryPollerTimeoutSeconds => 240;
        protected override TimeSpan WorkInterval => TimeSpan.FromMilliseconds(ObymobiIntervals.SERVICES_REPORTINGTASKPOLLER_INTERVAL);
        protected override TimeSpan FirstWorkInterval => WorkInterval;

        private IOrderRepository OrderRepository { get; }
        private ICompanyRepository CompanyRepository { get; }
        private TagRepository TagRepository { get; }

        public ReportingPoller(ISlackClient slackClient) : base(slackClient)
        {
            this.OrderRepository = new OrderRepository();
            this.CompanyRepository = new CompanyRepository();
            this.TagRepository = new TagRepository();
        }


        protected override void ExecuteWork(bool force = false)
        {
            ReportProcessingTaskCollection reportProcessingTasks = this.FetchReportProcessingTasks();

            if (!reportProcessingTasks.Any())
            {
                this.LogVerbose("ExecuteWork", "No ReportProcessingTasks to process");
                return;
            }

            this.LogVerbose("ExecuteWork", $"About to process {reportProcessingTasks.Count} ReportProcessingTask(s).");

            foreach (ReportProcessingTaskEntity task in reportProcessingTasks)
            {
                if (this.CancellationToken.IsCancellationRequested)
                {
                    this.LogInfo("ExecuteWork", "Cancellation is requested, breaking loop.");
                    break;
                }

                // Because of potententially long runs, update in between:
                this.LastStarted = DateTime.Now;

                Stopwatch sw = Stopwatch.StartNew();
                this.LogVerbose("ExecuteWork", "Starting with ReportProcessingTask '{0}' - Id: {1}", task.Name, task.ReportProcessingTaskId);

                bool exception = false;
                bool success = false;
                try
                {
                    success = ProcessTask(task);
                }
                catch (Exception ex)
                {
                    exception = true;
                    this.LogError("ExecuteWork", "Unexpected Exception: {0}", ex.Message);
                    this.LogError("ExecuteWork", "All Exceptions: {0}", ex.GetAllMessages(true));
                    sw.Stop();
                }

                sw.Stop();
                if (!exception && success)
                {
                    this.LogVerbose("ExecuteWork", "Finished ReportProcessingTask '{0}' - Id: {1} successfully in {2}ms", task.Name, task.ReportProcessingTaskId, sw.ElapsedMilliseconds);
                }
                else
                {
                    this.LogWarning("ExecuteWork", "Finished ReportProcessingTask '{0}' - Id: {1} unsuccessfully in {2}ms", task.Name, task.ReportProcessingTaskId, sw.ElapsedMilliseconds);
                }

                // Because of potententially long runs, update in between:
                this.LastCompleted = DateTime.Now;
            }

            this.LogVerbose("ExecuteWork", "Completed run");
        }

        private ReportProcessingTaskCollection FetchReportProcessingTasks()
        {
            PredicateExpression filter = new PredicateExpression(ReportProcessingTaskFields.Processed == false);
            SortExpression sort = new SortExpression(ReportProcessingTaskFields.ReportProcessingTaskId | SortOperator.Ascending);

            ReportProcessingTaskCollection tasks = new ReportProcessingTaskCollection();
            tasks.GetMulti(filter, 0, sort);

            return tasks;
        }

        private bool ProcessTask(ReportProcessingTaskEntity task)
        {
            bool success = this.CreateReport(task, out ReportFile reportFile);

            if (success)
            {
                this.SendMailIfSet(task, reportFile);
            }

            return success;
        }

        private bool CreateReport(ReportProcessingTaskEntity task, out ReportFile reportFile)
        {
            reportFile = null;

            StringBuilder sb = new StringBuilder();

            bool success = false;
            try
            {
                IReportBuilder reportBuilder;

                switch (task.AnalyticsReportType)
                {
                    case AnalyticsReportType.Batch:
                        reportBuilder = new BatchReportBuilder();
                        break;
                    case AnalyticsReportType.Kpi:
                        reportBuilder = new KpiReportBuilder(WebEnvironmentHelper.GoogleApisJwt);
                        break;
                    case AnalyticsReportType.Transactions:
                        reportBuilder = new TransactionsReportBuilder();
                        break;
                    case AnalyticsReportType.PageViews:
                        reportBuilder = new PageViewsReportBuilder(WebEnvironmentHelper.CloudEnvironment);
                        break;
                    case AnalyticsReportType.Marketing:
                        reportBuilder = new MarketingReportBuilder(WebEnvironmentHelper.CloudEnvironment);
                        break;
                    case AnalyticsReportType.BrokenLinks:
                        reportBuilder = new BrokenLinksReportBuilder(this);
                        break;
                    case AnalyticsReportType.TransactionsAppLess:
                        reportBuilder = new TransactionsAppLessReportBuilder(OrderRepository, CompanyRepository);
                        break;
                    case AnalyticsReportType.ProductSales:
                        reportBuilder = new ProductSalesReportBuilder(OrderRepository, CompanyRepository, TagRepository);
                        break;
                    case AnalyticsReportType.Inventory:
                        reportBuilder = new InventoryReportBuilder(OrderRepository, CompanyRepository, TagRepository);
                        break;
                    default:
                        throw new NotImplementedException("No implementation for AnalyticsReportType: " + task.AnalyticsReportType);
                }

                if (!reportBuilder.TryBuild(task, out reportFile))
                {
                    sb.AppendLine($"Failed to create report. (ReportProcessingTaskId={task.ReportProcessingTaskId})");
                }
                else
                {
                    ReportProcessingTaskFileEntity taskFile = new ReportProcessingTaskFileEntity();
                    taskFile.ParentCompanyId = task.CompanyId.GetValueOrDefault(-1);
                    taskFile.File = reportFile.File;

                    task.ReportProcessingTaskFileEntity = taskFile;
                }
            }
            catch (Exception ex)
            {
                sb.AppendFormatLine("Failed to run report for ReportProcessingTask: {0} - Exception: {1}", task.ReportProcessingTaskId, ex.Message);
                sb.AppendFormatLine("Full exceptions: {0}", ex.GetAllMessages(true));
            }

            // Record failure
            if (sb.Length > 0)
            {
                task.ExceptionText += "Exceptions for {0} attempt: \r\n".FormatSafe(DateTime.Now.ToString(CultureInfo.InvariantCulture));
                task.ExceptionText += sb.ToString();
                task.Failed = true;
            }
            else
            {
                success = true;
            }

            // It's always marked a processed & saved
            task.Processed = true;
            task.Save(true);

            return success;
        }

        private void SendMailIfSet(ReportProcessingTaskEntity task, ReportFile reportFile)
        {
            this.LogVerbose("SendMailIfSet", "Task Email: {0}", task.Email);

            if (task.Email.IsNullOrWhiteSpace())
            {
                return;
            }

            string[] recipients = task.Email.Split(new string[] { ",", "\r", "\n" }, StringSplitOptions.RemoveEmptyEntries)
                                           .Select(x => x.Trim())
                                           .Where(y => !y.IsNullOrWhiteSpace())
                                           .ToArray();

            foreach (string recipient in recipients)
            {
                try
                {
                    MailAddress from = new MailAddress(ORIGINATOR_EMAIL, ORIGINATOR_NAME);
                    MailAddress to = new MailAddress(recipient);

                    string subject = string.Format(EMAIL_SUBJECT, task.Name);
                    string filename = task.FilenameAsSpreadsheet;

                    string emailTemplate = Resource.mail_template;
                    string emailBody = emailTemplate.Replace("[[REPORT_NAME]]", task.Name);

                    MemoryStream stream = new MemoryStream(reportFile.File);
                    Attachment attachment = new Attachment(stream, filename, EMAIL_ATTACHMENT_CONTENT_TYPE);

                    this.LogVerbose("SendMailIfSet", "Subject: {0}, To: {1}, From: {2}", subject, to, from);

                    const int timeout = 30000;
                    MailUtilV2 mailUtil = new MailUtilV2(from, to, subject);
                    mailUtil.Attachments.Add(attachment);
                    mailUtil.SmtpClient.Timeout = timeout;
                    mailUtil.BodyHtml = emailBody;
                    mailUtil.SendMail().Wait(timeout);
                }
                catch (Exception ex)
                {
                    this.LogError("SendMailIfSet", $"Exception trying to send report through email. (ReportProcessingTaskId={task.ReportProcessingTaskId}, Message={ex.Message})");
                }
            }
        }
    }
}