﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Dionysos;
using Dionysos.Logging;
using Obymobi.Constants;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using Obymobi.Web.CloudStorage;
using Obymobi.Web.Pollers;
using Obymobi.Web.Slack.Interfaces;
using Obymobi.Web.Tasks;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Pollers
{
    public class CloudTaskPoller : BasePoller
    {
        private CloudStorageProvider amazonStorageProvider;

        private List<CloudStorageProvider> storageProviders;

        public CloudTaskPoller(ISlackClient slackClient) : base(slackClient)
        {
        }

        #region Base Overrides

        public override Enum LoggingType
        {
            get { return LoggingTypes.CloudTaskPoller; }
        }

        public override PollerType PollerType
        {
            get { return PollerType.CloudTaskPoller; }
        }

        public override IMultiLoggingProvider LoggingProvider
        {
            get { return Global.LoggingProvider; }
        }

        public override int PrimaryPollerTimeoutSeconds
        {
            get { return ObymobiIntervals.SERVICES_CLOUDTASKPOLLER_INTERVAL; }
        }

        protected override TimeSpan WorkInterval
        {
            get { return TimeSpan.FromMilliseconds(ObymobiIntervals.SERVICES_CLOUDTASKPOLLER_INTERVAL); }
        }

        protected override TimeSpan FirstWorkInterval
        {
            get { return WorkInterval; }
        }

        #endregion

        public override void Initialize(CancellationToken cancellationToken)
        {
            storageProviders = InitializeAndGetStorageProviders(true);

            base.Initialize(cancellationToken);
        }

        protected override void ExecuteWork(bool force = false)
        {
            LastStarted = DateTime.Now;

            // Create filter
            var firstFilter = true;
            var filter = new PredicateExpression();
            if (amazonStorageProvider != null)
            {
                filter.Add(CloudProcessingTaskFields.CompletedOnAmazonUTC == DBNull.Value);
            }

            var tasks = new CloudProcessingTaskCollection();
            tasks.GetMulti(filter);

            if (tasks.Count > 0)
            {
                this.LogVerbose("ExecuteWork", "Pending tasks: {0}", tasks.Count);

                // Now process each processing queue item of the batch.             
                var loopOptions = new ParallelOptions();
                loopOptions.MaxDegreeOfParallelism = 4;
                loopOptions.CancellationToken = this.CancellationToken;

                if (TestUtil.IsPcDeveloper)
                    loopOptions.MaxDegreeOfParallelism = 1;

                Parallel.ForEach(tasks, loopOptions, task =>
                                                     {
                                                         var cloudTask = CreateCloudTask(task);
                                                         try
                                                         {
                                                             if (cloudTask.TaskType != CloudProcessingTaskType.Unknown)
                                                             {
                                                                 switch (cloudTask.TaskEntity.ActionAsEnum)
                                                                 {
                                                                     case CloudProcessingTaskEntity.ProcessingAction.Upload:
                                                                         cloudTask.Upload(storageProviders);
                                                                         break;
                                                                     case CloudProcessingTaskEntity.ProcessingAction.Delete:
                                                                         cloudTask.Delete(storageProviders);
                                                                         break;
                                                                     default:
                                                                         this.LogWarning("ExecuteWork", "Unknown action '{0}' for CloudProcessingTask Id '{1}'", task.Action, task.CloudProcessingTaskId);
                                                                         cloudTask.Cancel("Unknown action '{0}' for CloudProcessingTask Id '{1}'\n", task.Action, task.CloudProcessingTaskId);
                                                                         break;
                                                                 }
                                                             }

                                                             HandleAttempts(cloudTask);
                                                         }
                                                         catch (Exception ex)
                                                         {
                                                             this.LogError("ExecuteWork", "Exception for task '{0}'. Message: {1}, StackTrace: {2}", task.CloudProcessingTaskId, ex.Message, ex.ProcessStackTrace(true));
                                                         }
                                                     });
            }
        }

        private List<CloudStorageProvider> InitializeAndGetStorageProviders(bool resetProviders)
        {
            if (resetProviders)
            {
                amazonStorageProvider = null;
            }

            // Create logging wrapper for cloud storage providers
            var logWrapper = new LogWrapper(this.LoggingProvider, this.LoggingType);


            // Initialize Amazon S3 storage provider if null
            if (amazonStorageProvider == null)
            {
                try
                {
                    amazonStorageProvider = new AmazonCloudStorageProvider(logWrapper);
                }
                catch (Exception ex)
                {
                    amazonStorageProvider = null;
                    this.LogError("InitializeAndGetStorageProviders", "Amazon Storage Provider Exception: {0}", ex.Message);
                }
            }

            // Create list of active storage providers
            storageProviders = new List<CloudStorageProvider>();
            if (this.amazonStorageProvider != null)
            {
                var available = this.amazonStorageProvider.IsStorageProviderAvailable(out var details);
                this.LogInfo("InitializeAndGetStorageProviders", "Amazon Storage Provider Available: {0} - {1}", available, details);
                if (!available)
                    this.amazonStorageProvider = null;
                else
                    storageProviders.Add(this.amazonStorageProvider);
            }

            return storageProviders;
        }

        private void HandleAttempts(CloudBaseTask task)
        {
            var entity = task.TaskEntity;

            // Always set the Next Attempt field (it's not important if it succeeeded or not because the StatusFilter will only retrieve tasks that still need work)
            entity.Attempts = entity.Attempts + 1;
            entity.LastAttemptUTC = DateTime.UtcNow;
            if (entity.Attempts < 10)
                entity.NextAttemptUTC = DateTime.UtcNow.AddMinutes(1);
            else if (entity.Attempts < 30)
                entity.NextAttemptUTC = DateTime.UtcNow.AddMinutes(entity.Attempts);
            else
            {
                bool isFailed = false;
                if (!entity.CompletedOnAmazonUTC.HasValue)
                {
                    isFailed = true;
                    entity.CompletedOnAmazonUTC = DateTime.MinValue;
                }

                if (isFailed)
                {
                    this.LogWarning("HandleAttempts", "Task for CloudProcessingTaskEntity: '{0}' failed after '{1}' attempts.".FormatSafe(entity.CloudProcessingTaskId, entity.Attempts));
                    entity.Errors += "Failed after {0} attempts.\n".FormatSafe(entity.Attempts);
                }
            }

            entity.Save();
        }

        private CloudBaseTask CreateCloudTask(CloudProcessingTaskEntity task)
        {
            CloudBaseTask cloudTask;
            switch (task.TypeAsEnum)
            {
                case CloudProcessingTaskType.Entertainment:
                    cloudTask = new EntertainmentCloudTask(task);
                    break;
                case CloudProcessingTaskType.Weather:
                    cloudTask = new WeatherCloudTask(task);
                    break;
                case CloudProcessingTaskType.Release:
                    cloudTask = new ReleaseCloudTask(new LogWrapper(this.LoggingProvider, this.LoggingType), task);
                    break;
                case CloudProcessingTaskType.DeliveryTime:
                    cloudTask = new DeliveryTimeCloudTask(task);
                    break;
                default:
                    {
                        this.LogWarning("CreateCloudTask", "Unknown CloudProcessingTaskType '{0}' for id '{1}'", task.Type, task.CloudProcessingTaskId);
                        cloudTask = new CloudBaseTask(CloudProcessingTaskType.Unknown, task);
                        cloudTask.Cancel("Unknown CloudProcessingTaskType");
                        break;
                    }
            }

            return cloudTask;
        }
    }
}