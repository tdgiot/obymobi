﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Dionysos;
using Dionysos.Logging;
using Obymobi;
using Obymobi.Constants;
using Obymobi.Data;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using Obymobi.Logic.HelperClasses;
using Obymobi.Web.CloudStorage;
using Obymobi.Web.Pollers;
using Obymobi.Web.Slack.Interfaces;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Pollers
{
    /// <summary>
    /// Summary description for MediaTaskPoller
    /// </summary>
    public class MediaTaskPoller : BasePoller
    {
        private AmazonCloudStorageProvider amazonStorageProvider;
        private const string MEDIA_CONTAINER = ObymobiConstants.CloudContainerMediaFiles;

        /// <summary>
        /// Indicates if the database used as actual connectionstring was available during the last check
        /// </summary>
        public bool IsDatabaseAvailable = true; // Be postive, of course we assume all is good.

        public MediaTaskPoller(ISlackClient slackClient) : base(slackClient)
        {
        }

        #region Base Overrides

        public override Enum LoggingType
        {
            get { return LoggingTypes.MediaTaskPoller; }
        }

        public override PollerType PollerType
        {
            get { return PollerType.MediaTaskPoller; }
        }

        public override IMultiLoggingProvider LoggingProvider
        {
            get { return Global.LoggingProvider; }
        }

        public override int PrimaryPollerTimeoutSeconds
        {
            get { return ObymobiIntervals.SERVICES_MEDIATASKPOLLER_INTERVAL; }
        }

        protected override TimeSpan WorkInterval
        {
            get { return TimeSpan.FromMilliseconds(ObymobiIntervals.SERVICES_MEDIATASKPOLLER_INTERVAL); }
        }

        protected override TimeSpan FirstWorkInterval
        {
            get { return WorkInterval; }
        }

        #endregion

        protected override void ExecuteWork(bool force = false)
        {
            LastStarted = DateTime.Now;

            // Clear the Storage Providers (before, not after, because you'll never know :)
            this.amazonStorageProvider = null;        

            // Initalize storage providers.
            // Design choice: Init for each interation as there's not real benefit in keeping them alive
            // and this allows each iteration to work with the storage providers that are available (and skip the unavailable).
            var storageProviders = this.InitializeAndGetStorageProviders();

            // Process the possible actions
            MediaProcessingTaskEntity.ProcessingAction[] actionsToProcess = { MediaProcessingTaskEntity.ProcessingAction.Upload, MediaProcessingTaskEntity.ProcessingAction.Delete };
            foreach (var action in actionsToProcess)
            {
                if (this.CancellationToken.IsCancellationRequested)
                {
                    this.LogInfo("ExecuteWork", "Cancellation is requested, breaking loop.");
                    break;
                }

                try
                {
                    var sw = new Stopwatch();
                    sw.Start();
                    int items = this.ProcessTasksForAction(action, storageProviders);
                    sw.Stop();

                    if (items > 0)
                    {
                        if (sw.Elapsed.TotalSeconds > 120)
                            this.LogVerbose("ExecuteWork", "Handling of action '{0}' completed in {1}", action, sw.Elapsed.ToString());
                        else if (sw.Elapsed.TotalSeconds > 5)
                            this.LogVerbose("ExecuteWork", "Handling of action '{0}' completed in {1:0.00}s", action, sw.Elapsed.TotalSeconds);
                        else
                            this.LogVerbose("ExecuteWork", "Handling of action '{0}' completed in {1:0.00}ms", action, sw.ElapsedMilliseconds);
                    }
                }
                catch (Exception ex)
                {
                    this.LogError("ExecuteWork", "ProcessTasksForAction '{0}' failed: {1}\r\n{2}", action, ex.Message, ex.StackTrace);
                }
            }

            // Delete old sheit
            var filter = new PredicateExpression();
            var timeFilter = new PredicateExpression();
            DateTime sixHoursAgo = DateTime.UtcNow.AddHours(-6);
            var usedStorageProviders = this.GetUsedStorageProviders();
            if (usedStorageProviders.Any(x => x == CloudStorageProviderIdentifier.Amazon))
            {
                filter.Add(MediaProcessingTaskFields.CompletedOnAmazonUTC != DBNull.Value);
                timeFilter.AddWithOr(MediaProcessingTaskFields.CompletedOnAmazonUTC < sixHoursAgo);
            }        

            filter.Add(timeFilter);

            // Require at least one filter (+ the timefilter that's al
            if (filter.Count > 1)
            {
                var tasks = new MediaProcessingTaskCollection();
                tasks.GetMulti(filter);

                if (tasks.Count > 0)
                {
                    this.LogInfo("ExecuteWork", "Deleting {0} old Media Processing Tasks", tasks.Count);
                    foreach (var task in tasks)
                        task.Delete();
                }
                //else
                //    this.LogVerbose("ExecuteWork", "No old completed MediaProcessingTasks to clean up.");
            }
            else
                this.LogWarning("ExecuteWork", "MediaProcessingTasks can't be cleaned: No valid Storage Providers");
        }

        private int ProcessTasksForAction(MediaProcessingTaskEntity.ProcessingAction action, IEnumerable<ICloudStorageProvider> storageProviders)
        {
            int items;
            // Get the work to be done, the batches.
            var batches = this.GetBatches(action, out items);

            if (batches.Count > 0)
            {
                this.LogVerbose("ProcessTasksForAction", "Batches for action '{0}' to process '{1}', total items: '{2}'", action, batches.Count, items);
            }
            else
            {
                this.LogVerbose("ProcessTasksForAction", "No tasks for action '{0}', queue is empty.", action);
                return 0;
            }

            int batchNo = 0;
            // Keep track of the MediaRatioTypeMediaIds we handled, because only 1 action is required per such an Id
            // Dictionary<int, MediaProcessingTaskEntity> processedMediaRatioTypeMediaIds = new Dictionary<int, MediaProcessingTaskEntity>();
            foreach (var batch in batches)
            {
                // Because a Do Work can take long, see every batch as a need start.
                LastStarted = DateTime.Now;
                var batchStopWatch = new Stopwatch();
                batchStopWatch.Start();
                batchNo++;

                this.LogVerbose("ProcessTasksForAction", "Batch {0} of {1} started", batchNo, batches.Count);

                // Retrieve the MediaRatioTypeMedia if required for the action
                Dictionary<int, MediaRatioTypeMediaEntity> mrtms = null;
                if (action != MediaProcessingTaskEntity.ProcessingAction.Delete)
                    mrtms = this.GetRelatedMediaRatioTypeMediaForBatch(batch);

                // Now process each processing queue item of the batch.             
                var loopOptions = new ParallelOptions();
                loopOptions.MaxDegreeOfParallelism = WebEnvironmentHelper.CloudEnvironment == CloudEnvironment.ProductionPrimary ? 4 : 1;
                if (TestUtil.IsPcGabriel)
                    loopOptions.MaxDegreeOfParallelism = 1;
                Parallel.ForEach(batch, loopOptions, item =>
                                                     {
                                                         // Actual processing with the first item of the list (we can have multiple tasks for the same MediaRatioTypeMediaId, but only 1 needs processing then)
                                                         var firstItem = item.Value[0];
                                                         if (action == MediaProcessingTaskEntity.ProcessingAction.Delete)
                                                             ProcessDeleteItem(storageProviders, firstItem);
                                                         else if (action == MediaProcessingTaskEntity.ProcessingAction.Upload)
                                                             this.ProcessUploadItem(storageProviders, mrtms, firstItem);
                                                         else
                                                         {
                                                             this.LogWarning("ProcessTasksForAction", "Action not implemented: {0}", action);
                                                             //break;
                                                         }

                                                         // Update any other ProcessingTasks for the same MediaRatioTypeMediaId
                                                         for (int i = 1; i < item.Value.Count; i++)
                                                         {
                                                             var earlierProcessedItem = item.Value[0];
                                                             item.Value[i].CompletedOnAmazonUTC = earlierProcessedItem.CompletedOnAmazonUTC;
                                                             item.Value[i].Save();
                                                         }
                                                     });

                batchStopWatch.Stop();
                LastCompleted = DateTime.Now;
                this.LogVerbose("ProcessTasksForAction", "Batch {0} of {1} completed in {2}.", batchNo, batches.Count, batchStopWatch.Elapsed.ToString());
            }

            return items;
        }

        private void ProcessDeleteItem(IEnumerable<ICloudStorageProvider> storageProviders, MediaProcessingTaskEntity item)
        {
            try
            {
                // Delete the item in each storage provider
                foreach (var storageProvider in storageProviders)
                {
                    try
                    {
                        storageProvider.DeleteFile(MEDIA_CONTAINER, item.PathFormat, true);
                        item.SetNewFieldValue(this.GetRelatedCompletedMediaProcessingTaskField(storageProvider), DateTime.UtcNow);

                        this.LogVerbose("ProcessDeleteItem", "{0} - Deleted from {1}.", item.MediaRatioTypeMediaId, storageProvider.Identifier);
                    }
                    catch
                    {
                        this.LogVerbose("ProcessDeleteItem", "Delete failed for: '{0}' on provider: '{1}'", item.PathFormat, storageProvider.Identifier);
                    }
                }

                // Check if we have attempts left 
                if (!this.HandleAttempts(item))
                    return;

                // Log it's success
                this.LogVerbose("ProcessDeleteItem", "Deleted '{0}', Amazon (UTC): '{1}'", item.MediaRatioTypeMediaId ?? item.MediaRatioTypeMediaIdNonRelationCopy, item.CompletedOnAmazonUTC.HasValue);

                item.Save();
            }
            catch (Exception ex)
            {
                this.LogError("ProcessDeleteItem", "Processing of item failed: {0}", ex.Message);
                this.LogError("ProcessDeleteItem", ex.StackTrace);
            }
        }    

        private void ProcessUploadItem(IEnumerable<ICloudStorageProvider> storageProviders, Dictionary<int, MediaRatioTypeMediaEntity> mrtms, MediaProcessingTaskEntity item)
        {
            try
            {
                string error = string.Empty;

                // Look up the MediaTypeRatioMedia Entity                        
                MediaRatioTypeMediaEntity mtmr = null;
                if (!item.MediaRatioTypeMediaId.HasValue || !mrtms.TryGetValue(item.MediaRatioTypeMediaId.Value, out mtmr))
                {
                    error = string.Format("{0} - Couldn't find MediaRatioTypeMedia with id: '{0}'", item.MediaRatioTypeMediaId);
                }

                // Find it's file
                MediaRatioTypeMediaFileEntity file = null;
                if (mtmr != null)
                {
                    file = mtmr.MediaRatioTypeMediaFileEntity;
                    if (file == null)
                        error = "{0} - Couldn't find MediaRatioTypeMediaFile for MediaRatioTypeMediaId: '{0}'".FormatSafe(item.MediaRatioTypeMediaId);
                }

                if (!error.IsNullOrWhiteSpace())
                {
                    // These errors don't need a re-attempt, because they are fucked for good.
                    this.LogWarning("ProcessUploadItem", error);
                    item.Errors = error;
                    item.CompletedOnAmazonUTC = new DateTime(2000, 1, 1);
                    item.Save();
                    return;
                }

                // Prepare the filename on the current version of the data
                string fileName;
                if (file != null && file.UpdatedUTC.HasValue)
                {
                    fileName = item.PathFormat.Replace(MediaRatioTypeMediaEntity.FILENAME_FORMAT_TICKS, file.UpdatedUTC.Value.Ticks.ToString(CultureInfo.InvariantCulture));
                }
                else if (file != null && file.CreatedUTC.HasValue)
                {
                    fileName = item.PathFormat.Replace(MediaRatioTypeMediaEntity.FILENAME_FORMAT_TICKS, file.CreatedUTC.Value.Ticks.ToString(CultureInfo.InvariantCulture));
                }
                else
                {
                    this.LogVerbose("ProcessUploadItem", "{0} - 'Changed' field empty for MediaRatioTypeMedia with id '{0}'", item.MediaRatioTypeMediaId);
                    fileName = item.PathFormat.Replace(MediaRatioTypeMediaEntity.FILENAME_FORMAT_TICKS, "0");
                }

                // Upload file to each available storage provider
                try
                {
                    mtmr.MediaEntity.FileDownloadDelegateAsync = () => mtmr.MediaEntity.DownloadAsync();
                    using (MemoryStream imageStream = mtmr.ResizeAndGetFile(mtmr.MediaEntity)) // If it's a PDF ResizeAndGetFile won't resize but just return the file.
                    {
                        string fileMd5 = Dionysos.Security.Cryptography.Hasher.GetHash(imageStream, Dionysos.Security.Cryptography.HashType.MD5);
                        foreach (var storageProvider in storageProviders)
                        {
                            try
                            {
                                // Ensure we only upload for necessary proviers
                                if ((storageProvider.Identifier == CloudStorageProviderIdentifier.Amazon && !item.CompletedOnAmazonUTC.HasValue))
                                {
                                    var sw = new Stopwatch();
                                    sw.Start();
                                    DateTime? result = this.UploadFile(storageProvider, fileName, imageStream, fileMd5);
                                    item.SetNewFieldValue(this.GetRelatedCompletedMediaProcessingTaskField(storageProvider), result);
                                    mtmr.SetNewFieldValue(this.GetRelatedLastDistributedField(storageProvider), file.UpdatedUTC.HasValue ? file.UpdatedUTC.Value.Ticks : file.CreatedUTC.Value.Ticks);
                                    sw.Stop();
                                    if (result.HasValue)
                                    {
                                        this.LogVerbose("ProcessUploadItem", "{0} - Upload to {1} in {2}ms - Result: '{3}'",
                                                        item.MediaRatioTypeMediaId, storageProvider.Identifier, sw.ElapsedMilliseconds, result);
                                    }
                                    else
                                    {
                                        this.LogWarning("ProcessUploadItem", "{0} - Upload to {1} failed.",
                                                        item.MediaRatioTypeMediaId, storageProvider.Identifier);
                                    }
                                }
                                else
                                    this.LogVerbose("ProcessUploadItem", "{0} - Already uploaded on {1}", item.MediaRatioTypeMediaId, storageProvider.Identifier);
                            }
                            catch (Exception ex)
                            {
                                this.LogError("ProcessUploadItem", "{0} - Failed: {1}", item.MediaRatioTypeMediaId, ex.Message);
                            }
                        }
                    }
                }
                catch(Exception ex)
                {
                    this.LogError("ProcessUploadItem", "{0} - Failed: {1}", item.MediaRatioTypeMediaId, ex.Message);
                }

                //if (TestUtil.IsPcGabriel)
                //{ 
                //    // Force re-attempts
                //    item.CompletedOnAzure = null;
                //    item.CompletedOnPrimaryServer = null;
                //    item.CompletedOnSecondaryServer = null;
                //    this.Log("IsPcGabriel forces new attempts - IsPcGabriel forces new attempts - IsPcGabriel forces new attempts");
                //}

                // Check if we have attempts left             
                if (!this.HandleAttempts(item))
                    return;

                this.LogVerbose("ProcessUploadItem", "{0} - Uploaded '{0}', Amazon (UTC): '{1}'", item.MediaRatioTypeMediaId, item.CompletedOnAmazonUTC.HasValue);

                // Log it's success in a Transaction to be sure the mtmr gets updated
                var t = new Transaction(System.Data.IsolationLevel.ReadUncommitted, "MediaTaskHelper-" + item.MediaRatioTypeMediaId);
                try
                {
                    item.AddToTransaction(t);
                    item.Save();

                    // Report the upload to the CDN to the DB if at least on Azure or Amazon
                    if (item.CompletedOnAmazonUTC.HasValue)
                    {
                        mtmr.AddToTransaction(t);
                        mtmr.LastDistributedVersionTicks = file.UpdatedUTC.HasValue ? file.UpdatedUTC.Value.Ticks : file.CreatedUTC.Value.Ticks;
                        mtmr.Save();
                    }

                    t.Commit();
                }
                catch (Exception ex)
                {
                    this.LogError("ProcessUploadItem", "{0} - Failed to store progress in database: {1}\r\n{2}", item.MediaRatioTypeMediaId, ex.Message, ex.StackTrace);
                    t.Rollback();
                }
                finally
                {
                    t.Dispose();
                }
            }
            catch (Exception ex)
            {
                this.LogError("ProcessUploadItem", "{0} - Processing of item failed: {1}\r\n{2}", item.MediaRatioTypeMediaId, ex.Message, ex.StackTrace);
            }
        }

        private bool HandleAttempts(MediaProcessingTaskEntity item)
        {
            bool toReturn = true;
            // Always set the Next Attempt field (it's not important if it succeeeded or not because the StatusFilter will only retrieve tasks that still need work)
            item.Attempts = item.Attempts + 1;
            item.LastAttemptUTC = DateTime.UtcNow;
            if (item.Attempts < 10)
                item.NextAttemptUTC = DateTime.UtcNow.AddMinutes(1);
            else if (item.Attempts < 30)
                item.NextAttemptUTC = DateTime.UtcNow.AddMinutes(item.Attempts);
            else
            {
                // Fail it.
                this.LogWarning("HandleAttempts", "Task for MediaRatioTypeMedia: '{0}' failed after '{1}' attempts.".FormatSafe(item.MediaRatioTypeMediaId, item.Attempts));
                item.Errors = "Failed after {0} attempts.".FormatSafe(item.Attempts);
                item.CompletedOnAmazonUTC = new DateTime(2000, 1, 1);
                item.Save();
                toReturn = false;
            }
            return toReturn;
        }


        /// <summary>
        /// Get batches of MediaProcessingTasks
        /// </summary>
        /// <param name="action"></param>
        /// <param name="totalItems"></param>
        /// <returns></returns>
        private List<Dictionary<int, List<MediaProcessingTaskEntity>>> GetBatches(MediaProcessingTaskEntity.ProcessingAction action, out int totalItems)
        {
            // The Tasks collection should be sorted by MediaRatioTypeMediaId.
            var toReturn = new List<Dictionary<int, List<MediaProcessingTaskEntity>>>();

            // Fetch all Queue items to still need processing.
            var filterQueue = new PredicateExpression();
            if (action == MediaProcessingTaskEntity.ProcessingAction.Upload)
            {
                filterQueue.Add(MediaProcessingTaskFields.Action == (int)MediaProcessingTaskEntity.ProcessingAction.Upload);
            }
            else if (action == MediaProcessingTaskEntity.ProcessingAction.Delete)
            {
                filterQueue.Add(MediaProcessingTaskFields.Action == (int)MediaProcessingTaskEntity.ProcessingAction.Delete);

                // Deletes are delayed since a device might still have an old menu, and therefore link to old files. There's a grace period
                if (!TestUtil.IsPcGabriel)
                    filterQueue.Add(MediaProcessingTaskFields.CreatedUTC < DateTime.UtcNow.AddDays(ObymobiIntervals.MEDIA_TASK_POLLER_DELETE_DELAY_DAYS * -1) | MediaProcessingTaskFields.CreatedUTC == DBNull.Value);
            }
            else
                this.LogWarning("GetBatches", "Action not implemented to create batches: {0}", action);

            // The Status filter is the filter to get only non-processed tasks
            // this also deals with re-attempts
            filterQueue.Add(this.GetStatusFilter());

            // GK Previously we always processed ALL tasks available in the database, but now we do smaller 
            // batches, reason is that we don't want to block all processing for manual tasks when a batch 
            // of multiple minutes (or even hours) is running.
            // On average it takes ca. 10s to process 50 images, decided to do max 500 per 'batch' (between quotes)
            // as those batches will be broken down in 10 batches of 50.
        
            // We sort by priority, as bulk actions are intented to have a lower priority than manual actions.
            var sort = new SortExpression();
            sort.Add(MediaProcessingTaskFields.Priority | SortOperator.Descending);
            sort.Add(MediaProcessingTaskFields.MediaRatioTypeMediaId | SortOperator.Ascending);        

            var tasks = new MediaProcessingTaskCollection();
            tasks.GetMulti(filterQueue, 500, sort);
            totalItems = tasks.Count;

            Dictionary<int, List<MediaProcessingTaskEntity>> batch = null;
            // Do batches to limit memory usage.            
            // Get all the MediaRatioTypeMediaFiles to be uploaded in batches of 50 to limit memory usage
            // Assumption: 50 * 600kB = 30mb should be doable.                
            for (int i = tasks.Count - 1; i >= 0; i--)
            {
                if (batch == null)
                {
                    batch = new Dictionary<int, List<MediaProcessingTaskEntity>>();
                    toReturn.Add(batch);
                }

                if (!batch.ContainsKey(tasks[i].MediaRatioTypeMediaIdNonRelationCopy))
                    batch.Add(tasks[i].MediaRatioTypeMediaIdNonRelationCopy, new List<MediaProcessingTaskEntity>());

                batch[tasks[i].MediaRatioTypeMediaIdNonRelationCopy].Add(tasks[i]);

                if (batch.Count >= 50)
                {
                    // Check if the next item is from a different MediaRatioTypeMediaId, otherwise we need to continue)
                    if (i > 0 && !tasks[i - 1].MediaRatioTypeMediaId.Equals(tasks[i].MediaRatioTypeMediaId))
                    {
                        batch = null;
                    }
                }
            }

            return toReturn;
        }

        private Dictionary<int, MediaRatioTypeMediaEntity> GetRelatedMediaRatioTypeMediaForBatch(Dictionary<int, List<MediaProcessingTaskEntity>> batch)
        {
            var toReturn = new Dictionary<int, MediaRatioTypeMediaEntity>();

            // Get the MediaRatioTypeMediaFiles related to this batch (at once so save a lot of round trips to the DB server).
            var mediaRatioTypeMediaIds = batch.Keys.ToList();
            var mrtms = new MediaRatioTypeMediaCollection();

            var fileFilter = new PredicateExpression();
            fileFilter.Add(MediaRatioTypeMediaFields.MediaRatioTypeMediaId == mediaRatioTypeMediaIds);

            var path = new PrefetchPath(EntityType.MediaRatioTypeMediaEntity);
            path.Add(MediaRatioTypeMediaEntityBase.PrefetchPathMediaRatioTypeMediaFileEntity);

            mrtms.GetMulti(fileFilter, path);

            // Add to the lookup dictionary
            foreach (var mrtm in mrtms)
            {
                toReturn.Add(mrtm.MediaRatioTypeMediaId, mrtm);
            }

            return toReturn;
        }

        private DateTime? UploadFile(ICloudStorageProvider provider, string fileName, MemoryStream imageStream, string fileMd5)
        {
            DateTime? toReturn = null;

            try
            {
                // Verify the file was written                
                if (provider.FileRequiresUpload(MEDIA_CONTAINER, fileName, fileMd5))
                {
                    using (var imageStreamCopy = new MemoryStream())
                    {
                        imageStream.Position = 0;
                        imageStream.Copy(imageStreamCopy);
                        imageStreamCopy.Position = 0;

                        provider.WriteFile(MEDIA_CONTAINER, fileName, imageStreamCopy, ObymobiIntervals.MEDIA_STORAGE_CACHE_IN_SECONDS, false);
                    }
                }
                else
                {
                    this.LogVerbose("UploadFile", "File ({0}) already uploaded, skipping", fileName);
                }

                if (provider.FileExists(MEDIA_CONTAINER, fileName, true))
                    toReturn = DateTime.UtcNow;
                if (toReturn.HasValue)
                    this.LogVerbose("UploadFile", "Upload with Provider '{0}' of '{1}' is completed", fileName, provider.Identifier);
                else
                    this.LogVerbose("UploadFile", "Upload with Provider '{0}' of '{1}' is failed, FileExists call failed.", fileName, provider.Identifier);
            }
            catch (Exception ex)
            {
                // Some failure, handle as not good!
                this.LogError("UploadFile", "Upload with Provider '{0}' failed: '{1}', '{2}'", provider.Identifier, ex.Message, ex.StackTrace);
            }

            return toReturn;
        }

        private List<CloudStorageProviderIdentifier> GetUsedStorageProviders()
        {
            var toReturn = new List<CloudStorageProviderIdentifier>();
            toReturn.Add(CloudStorageProviderIdentifier.Amazon);
            return toReturn;
        }

        private List<ICloudStorageProvider> InitializeAndGetStorageProviders()
        {
            var toReturn = new List<ICloudStorageProvider>();

            var logWrapper = new LogWrapper(this.LoggingProvider, this.LoggingType);

            // If at least one Storage Provider is initialized this function was already called
            // during this Iteration, so then it can be skipped
            if (this.amazonStorageProvider == null)
            {
                try
                {
                    this.amazonStorageProvider = new AmazonCloudStorageProvider(logWrapper);
                }
                catch (Exception ex)
                {
                    this.LogError("InitializeAndGetStorageProviders", "Failed to initialize Amazon Cloud Storage Provider: {0}", ex.Message);
                }            

                bool available;
                string details;
                List<string> activeStorageProviders = new List<string>();
                List<string> nonactiveStorageProviders = new List<string>();
                
                if (this.amazonStorageProvider != null)
                {
                    available = this.amazonStorageProvider.IsStorageProviderAvailable(out details);                
                    if (!available)
                        this.amazonStorageProvider = null;
                    activeStorageProviders.Add("Amazon");
                }
                else
                    nonactiveStorageProviders.Add("Amazon");

                this.LogInfo("InitializeAndGetStorageProviders", "Active Storage Providers: '{0}', Non-Active Storage Providers: '{1}'", string.Join(", ", activeStorageProviders), string.Join(", ", nonactiveStorageProviders));

            }

            if (this.amazonStorageProvider != null)
                toReturn.Add(this.amazonStorageProvider);

            return toReturn;
        }

        private int GetRelatedCompletedMediaProcessingTaskField(ICloudStorageProvider storageProvider)
        {
            if (storageProvider is AmazonCloudStorageProvider)
                return MediaProcessingTaskFields.CompletedOnAmazonUTC.FieldIndex;
        
            throw new NotImplementedException("Non-supported Storage Provider: {0}".FormatSafe(storageProvider.GetType()));
        }

        private int GetRelatedLastDistributedField(ICloudStorageProvider storageProvider)
        {
            if (storageProvider is AmazonCloudStorageProvider)
                return MediaRatioTypeMediaFields.LastDistributedVersionTicksAmazon.FieldIndex;
        
            throw new NotImplementedException("Non-supported Storage Provider: {0}".FormatSafe(storageProvider.GetType()));
        }

        private PredicateExpression GetStatusFilter()
        {
            // Status Filter
            var statusFilter = new PredicateExpression();
            statusFilter.AddWithOr(MediaProcessingTaskFields.CompletedOnAmazonUTC == DBNull.Value);        

            // Attempt Filter
            var attemptFilter = new PredicateExpression();
            attemptFilter.Add(MediaProcessingTaskFields.Attempts == 0); // First attempt
            attemptFilter.AddWithOr(MediaProcessingTaskFields.NextAttemptUTC <= DateTime.UtcNow); // Ready for next attempt

            statusFilter.Add(attemptFilter);

            return statusFilter;
        }
    }
}