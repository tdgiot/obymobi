﻿using System;
using System.Collections.Generic;
using Dionysos;
using Dionysos.Logging;
using Obymobi.Constants;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Web.Pollers;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Enums;
using Obymobi.Logic.HelperClasses;
using Obymobi.Web.Slack.Interfaces;

namespace Pollers
{
    public class GameSessionReportPoller : BasePoller
    {
        public GameSessionReportPoller(ISlackClient slackClient) : base(slackClient)
        {
        }

        #region Base Overrides

        public override Enum LoggingType
        {
            get { return LoggingTypes.GameSessionReportPoller; }
        }

        public override Obymobi.Enums.PollerType PollerType
        {
            get { return Obymobi.Enums.PollerType.GameSessionReportPoller; }
        }

        public override IMultiLoggingProvider LoggingProvider
        {
            get { return Global.LoggingProvider; }
        }

        public override int PrimaryPollerTimeoutSeconds
        {
            get { return 60; }
        }

        protected override TimeSpan WorkInterval
        {
            get { return TimeSpan.FromMilliseconds(ObymobiIntervals.SERVICES_GAMESESSIONREPORTPOLLER_INTERVAL); }
        }

        protected override TimeSpan FirstWorkInterval
        {
            get { return WorkInterval; }
        }

        #endregion

        protected override void ExecuteWork(bool force = false)
        {
            this.LogVerbose("ExecuteWork", "Getting active game session report configurations.");
            GameSessionReportConfigurationCollection gameSessionReportConfigurationCollection = new GameSessionReportConfigurationCollection();
            gameSessionReportConfigurationCollection.GetMulti(GameSessionReportConfigurationFields.Active == true);
            this.LogVerbose("ExecuteWork", "{0} game session report configurations found.", gameSessionReportConfigurationCollection.Count);

            foreach (GameSessionReportConfigurationEntity gameSessionReportConfigurationEntity in gameSessionReportConfigurationCollection)
            {
                if (this.CancellationToken.IsCancellationRequested)
                {
                    this.LogInfo("ExecuteWork", "Cancellation is requested, breaking loop.");
                    break;
                }

                this.LogVerbose("ExecuteWork", "Processing game session report configuration '{0}'.", gameSessionReportConfigurationEntity.Name);

                TimeZoneInfo timeZoneInfo = gameSessionReportConfigurationEntity.CompanyEntity.TimeZoneInfo;

                DateTime utcNow = DateTime.UtcNow;
                DateTime localNow = utcNow.UtcToLocalTime(timeZoneInfo);
                DateTime dailyResetTime = gameSessionReportConfigurationEntity.DailyResetTime ?? new DateTime(2018, 3, 9, 11, 0, 0);
                DateTime start = DateTime.MinValue;
                DateTime end = DateTime.MinValue;
                bool sendReport = false;

                CompanyEntity companyEntity = gameSessionReportConfigurationEntity.CompanyEntity;

                if (gameSessionReportConfigurationEntity.RecurrenceType == GameSessionReportRecurrenceType.Daily)
                {
                    DateTime? dailySendTime = gameSessionReportConfigurationEntity.SendTime;
                    if (!dailySendTime.HasValue)
                    {
                        this.LogVerbose("ExecuteWork", "Game session report configuration '{0}' doesn't have a daily send time specified.", gameSessionReportConfigurationEntity.Name);
                    }
                    else if (dailySendTime.Value.Hour != localNow.Hour || dailySendTime.Value.Minute != localNow.Minute)
                    {
                        this.LogVerbose("ExecuteWork", "Daily send time is '{0}:{1}', current local time is '{2}:{3}'. Report not being sent.", dailySendTime.Value.Hour.ToString("00"), dailySendTime.Value.Minute.ToString("00"), localNow.Hour.ToString("00"), localNow.Minute.ToString("00"));
                    }
                    else
                    {
                        int hourDifference = localNow.Hour - dailyResetTime.Hour;
                        if (hourDifference <= 0)
                            hourDifference += 24;

                        start = utcNow.AddHours(-hourDifference);
                        end = utcNow;

                        sendReport = true;
                        this.LogVerbose("ExecuteWork", "Daily send time is '{0}:{1}', current local time is '{2}:{3}'. Report being sent.", dailySendTime.Value.Hour.ToString("00"), dailySendTime.Value.Minute.ToString("00"), localNow.Hour.ToString("00"), localNow.Minute.ToString("00"));
                    }
                }
                else if (gameSessionReportConfigurationEntity.RecurrenceType == GameSessionReportRecurrenceType.Hourly)
                {
                    if (localNow.Minute != 0)
                    {
                        this.LogVerbose("ExecuteWork", "Current local time is '{0}:{1}'. Report not being sent.", localNow.Hour.ToString("00"), localNow.Minute.ToString("00"));
                    }
                    else
                    {
                        int hourDifference = localNow.Hour - dailyResetTime.Hour;
                        if (hourDifference <= 0)
                            hourDifference += 24;

                        start = utcNow.AddHours(-hourDifference);
                        end = utcNow;

                        this.LogVerbose("ExecuteWork", "Current local time is '{0}:{1}'. Report being sent.", localNow.Hour.ToString("00"), localNow.Minute.ToString("00"));
                        sendReport = true;
                    }
                }
                
                if (sendReport)
                {
                    this.LogVerbose("ExecuteWork", "Creating and sending report for game session report configuration '{0}', interval '{1} - {2}' (UTC).", gameSessionReportConfigurationEntity.Name, start.ToString(), end.ToString());
                    GameSessionReportHelper.CreateAndSendGameSessionReport(gameSessionReportConfigurationEntity, start, end);
                }
            }
        }
    }
}