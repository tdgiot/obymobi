﻿using System;
using System.Threading;
using Dionysos.Logging;
using Obymobi.Enums;
using Obymobi.Web.Pollers;
using Obymobi.Web.Slack.Interfaces;

namespace Pollers
{
    /// <summary>
    /// Summary description for TestPoller
    /// </summary>
    public class TestPoller : BasePoller
    {
        public TestPoller(ISlackClient slackClient) : base(slackClient)
        {
        }

        public override IMultiLoggingProvider LoggingProvider
        {
            get { return Global.LoggingProvider; }
        }

        public override PollerType PollerType
        {
            get { return PollerType.TestPoller; }
        }

        public override int PrimaryPollerTimeoutSeconds
        {
            get { return 120; }
        }

        protected override TimeSpan WorkInterval
        {
            get { return TimeSpan.FromSeconds(15); }
        }

        protected override TimeSpan FirstWorkInterval
        {
            get { return TimeSpan.FromSeconds(5); }
        }

        public override Enum LoggingType
        {
            get { return LoggingTypes.TestPoller; }
        }

        protected override void ExecuteWork(bool force = false)
        {
            LogVerbose("ExecuteWork", "Going to do some work now...");

            for (int i = 0; i < 1000; i++)
            {
                if (this.CancellationToken.IsCancellationRequested)
                {
                    LogWarning("ExecuteWork", "Cancellation is requested, bye bye");
                    break;
                }

                LogVerbose("ExecuteWork", "i={0}", i);

                var currentDate = DateTime.Now;
                if (currentDate > DateTime.Now)
                {
                    LogInfo("ExecuteWork", "Very interesting...");
                }

                Thread.Sleep(15);
            }

            LogVerbose("ExecuteWork", "I'm done, peace out");
        }
    }
}