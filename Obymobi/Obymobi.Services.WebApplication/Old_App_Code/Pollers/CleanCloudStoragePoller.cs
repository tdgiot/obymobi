﻿using System;
using System.Diagnostics;
using System.Threading;
using Dionysos.Logging;
using Obymobi.Enums;
using Obymobi.Web.Pollers;
using Obymobi.Web.Slack.Interfaces;

namespace Pollers
{
    /// <summary>
    /// Summary description for CleanOfflineClientsPoller
    /// </summary>
    public class CleanCloudStoragePoller : BasePoller
    {
        private static DateTime nextCleanup;

        public CleanCloudStoragePoller(ISlackClient slackClient) : base(slackClient)
        {
        }

        #region Base Overrides

        public override Enum LoggingType
        {
            get { return LoggingTypes.CleanCloudStoragePoller; }
        }

        public override PollerType PollerType
        {
            get { return PollerType.CleanCloudStoragePoller; }
        }

        public override IMultiLoggingProvider LoggingProvider
        {
            get { return Global.LoggingProvider; }
        }

        public override int PrimaryPollerTimeoutSeconds
        {
            get { return 30 * 60; } // 30 mins
        }

        /// <summary>
        /// NOT USED! This poller calculates its own <see cref="TimeTillNextWork"/> TimeSpan
        /// </summary>
        protected override TimeSpan WorkInterval
        {
            get { return new TimeSpan(1, 0, 0, 0); }
        }

        /// <summary>
        /// NOT USED! This poller calculates its own <see cref="TimeTillNextWork"/> TimeSpan
        /// </summary>
        protected override TimeSpan FirstWorkInterval
        {
            get { return new TimeSpan(1, 0, 0, 0); }
        }

        #endregion

        public override void Initialize(CancellationToken cancellationToken)
        {
            // By added hours to "Today", we can let this poller run each day at specific time
            nextCleanup = DateTime.Today.AddHours(4);

            SetNextCleanupTime();

            base.Initialize(cancellationToken);
        }

        public override TimeSpan TimeTillNextWork()
        {
            var timeTillNextWork = nextCleanup.Subtract(DateTime.Now);
            return timeTillNextWork;
        }

        protected override void ExecuteWork(bool force = false)
        {
            if (DateTime.Now < nextCleanup)
                return;

            DoCleanup();

            SetNextCleanupTime();
        }

        private void SetNextCleanupTime()
        {
            if (DateTime.Now >= nextCleanup)
            {
                // Set next cleanup time to tomorrow
                nextCleanup = nextCleanup.AddDays(1);
            }

            LogInfo("SetNextCleanupTime", "Next clean up scheduled at: {0}", nextCleanup.ToString("dd/MM/yyyy HH:mm:ss"));
        }

        private void DoCleanup()
        {
            try
            {
                var sw = new Stopwatch();
                sw.Start();
                var logWrapper = new LogWrapper(this.LoggingProvider, this.LoggingType);
                this.LogInfo("DoCleanup", "Started cleaning");
                var cleaner = new CloudStorageCleanerMedia(logWrapper, false);
                cleaner.Clean();
                sw.Stop();
                this.LogInfo("DoCleanup", "Completed cleaning in {0}", sw.Elapsed);            
            }
            catch (Exception ex)
            {
                this.LogError("DoCleanup", "Failed to do the clean up: {0}", ex.Message);
            }
        }
    }
}