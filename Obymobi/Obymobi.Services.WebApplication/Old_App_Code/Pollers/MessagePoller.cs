﻿using System;
using System.Collections.Generic;
using System.Linq;
using Dionysos.Logging;
using Obymobi.Constants;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using Obymobi.Logic.Comet;
using Obymobi.Logic.Model;
using Obymobi.Web.Pollers;
using Obymobi.Web.Slack.Interfaces;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Pollers
{
    public class MessagePoller : BasePoller
    {
        private const int MESSAGE_BATCH_SIZE = 200;

        private int currentWorkInterval = ObymobiIntervals.SERVICES_MESSAGEPOLLER_NORMAL_INTERVAL;

        public MessagePoller(ISlackClient slackClient) : base(slackClient)
        {
        }

        #region Base Overrides

        public override Enum LoggingType
        {
            get { return LoggingTypes.MessagePoller; }
        }

        public override PollerType PollerType
        {
            get { return PollerType.MessagePoller; }
        }

        public override IMultiLoggingProvider LoggingProvider
        {
            get { return Global.LoggingProvider; }
        }

        /// <summary>
        /// NOT USED! This poller calculates its own <see cref="TimeTillNextWork"/> TimeSpan
        /// </summary>
        protected override TimeSpan WorkInterval
        {
            get { return new TimeSpan(1, 0, 0, 0); }
        }

        /// <summary>
        /// NOT USED! This poller calculates its own <see cref="TimeTillNextWork"/> TimeSpan
        /// </summary>
        protected override TimeSpan FirstWorkInterval
        {
            get { return new TimeSpan(1, 0, 0, 0); }
        }

        public override int PrimaryPollerTimeoutSeconds
        {
            get { return ObymobiIntervals.SERVICES_MESSAGEPOLLER_NORMAL_INTERVAL; }
        }

        public override TimeSpan TimeTillNextWork()
        {
             DateTime lastStarted = LastStarted;
            if (lastStarted == DateTime.MinValue)
                lastStarted = this.instanceCreated;

            var nextWorkTime = lastStarted.Add(TimeSpan.FromMilliseconds(this.currentWorkInterval));
            var timeTillNextWork = nextWorkTime.Subtract(DateTime.Now);

            return timeTillNextWork;
        }

        #endregion

        protected override void ExecuteWork(bool force = false)
        {
            PredicateExpression filter = new PredicateExpression();
            filter.Add(MessageRecipientFields.Queued == true);

            MessageRecipientCollection recipients = new MessageRecipientCollection();
            recipients.GetMulti(filter, MessagePoller.MESSAGE_BATCH_SIZE, null, null, null);

            this.currentWorkInterval = ObymobiIntervals.SERVICES_MESSAGEPOLLER_NORMAL_INTERVAL;
            if (recipients.Count > 0)
            {
                Dictionary<int, Message> messageModels = recipients.GroupBy(x => x.MessageId).Select(y => y.First()).ToDictionary(x => x.MessageId, x => Obymobi.Logic.HelperClasses.MessageHelper.CreateMessageModelFromEntity(x.MessageEntity));

                foreach (MessageRecipientEntity recipient in recipients)
                {
                    if (!messageModels.ContainsKey(recipient.MessageId))
                    {
                        continue;
                    }

                    try
                    {
                        Message message = Obymobi.Logic.HelperClasses.MessageHelper.SetMessageRecipientInfo(messageModels[recipient.MessageId], recipient);

                        recipient.Queued = false;
                        recipient.SentUTC = DateTime.UtcNow;
                        recipient.Save();

                        LogInfo("ExecuteWork", "Sending message {0}({1}), {2}", message.Title, recipient.MessageId, recipient.MessageRecipientId);

                        CometHelper.AnnounceNewMessage(message);
                    }
                    catch (Exception ex)
                    {
                        LogError("ExecuteWork", "An exception occurred whilst trying to send message '{0}'! Exception message: {1}", recipient.MessageId, ex.Message);
                    }
                }

                if (recipients.Count >= MessagePoller.MESSAGE_BATCH_SIZE)
                {
                    this.currentWorkInterval = ObymobiIntervals.SERVICES_MESSAGEPOLLER_BATCH_INTERVAL;
                }
            }
        }        
    }
}