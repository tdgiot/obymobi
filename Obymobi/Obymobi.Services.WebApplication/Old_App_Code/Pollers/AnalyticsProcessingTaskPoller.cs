using Dionysos.Logging;
using Obymobi.Analytics.Google;
using Obymobi.Analytics.Google.Recording;
using Obymobi.Constants;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Enums;
using Obymobi.Logic.HelperClasses;
using Obymobi.Web.Pollers;
using System;
using System.Collections.Generic;
using Obymobi.Web.Slack.Interfaces;

namespace Pollers
{
    public class AnalyticsProcessingTaskPoller : BasePoller
    {
        public AnalyticsProcessingTaskPoller(ISlackClient slackClient) : base(slackClient)
        {
        }

        #region Base Overrides

        public override Enum LoggingType
        {
            get { return LoggingTypes.AnalyticsProcessingTaskPoller; }
        }

        public override PollerType PollerType
        {
            get { return PollerType.AnalyticsProcessingTaskPoller; }
        }

        public override IMultiLoggingProvider LoggingProvider
        {
            get { return Global.LoggingProvider; }
        }

        public override int PrimaryPollerTimeoutSeconds
        {
            get { return 60; }
        }

        protected override TimeSpan WorkInterval
        {
            get { return TimeSpan.FromMilliseconds(ObymobiIntervals.SERVICES_ANALYTICSPROCESSINGTASKPOLLER_INTERVAL); }
        }

        protected override TimeSpan FirstWorkInterval
        {
            get { return WorkInterval; }
        }

        #endregion

        protected override void ExecuteWork(bool force = false)
        {
            AnalyticsProcessingTaskCollection tasks = AnalyticsProcessingTaskHelper.GetTasks();

            this.LogInfo("ExecuteWork", "{0} analytic tasks to process", tasks.Count);

            foreach (AnalyticsProcessingTaskEntity task in tasks)
            {
                try
                {
                    this.ProcessTask(task);
                }
                catch (Exception ex)
                {
                    this.LogError("ExecuteWork", "Error occurred while processing task for order {0}, StackTrace: {1}", task.OrderId, ex.StackTrace);
                }
                finally
                {
                    task.Delete();
                }
            }

            this.LogInfo("ExecuteWork", "All tasks processed");
        }

        private void ProcessTask(AnalyticsProcessingTaskEntity task)
        {
            List<string> trackingIds = AnalyticsHelper.GetTrackingIds(task.OrderEntity);
            Hit hit = AnalyticsHelper.CreateHitForOrder(task.OrderEntity, task.EventAction, task.EventLabel);

            AsyncHitRecorder.RecordHit(hit, trackingIds);
        }
    }
}
