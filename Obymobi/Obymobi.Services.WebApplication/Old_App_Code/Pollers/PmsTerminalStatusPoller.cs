﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Dionysos.Logging;
using Obymobi.Constants;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Enums;
using Obymobi.Web.Pollers;
using Obymobi.Services.Logic.HelperClasses;
using Obymobi.Services.Logic.Models;
using Obymobi.Logic.Comet;
using Dionysos;
using Obymobi.Web.Slack.Interfaces;

namespace Pollers
{
    /// <summary>
    /// Summary description for PmsTerminalStatusPoller
    /// </summary>
    public class PmsTerminalStatusPoller : BasePoller
    {
        #region Fields

        private readonly TerminalDataSource terminalDataSource = new TerminalDataSource();
        private Dictionary<int, PmsTerminalStatus> pmsTerminalStatuses = new Dictionary<int, PmsTerminalStatus>();

        #endregion

        public PmsTerminalStatusPoller(ISlackClient slackClient) : base(slackClient)
        {
        }

        #region Base Overrides

        public override Enum LoggingType
        {
            get { return LoggingTypes.PmsTerminalStatusPoller; }
        }

        public override PollerType PollerType
        {
            get { return PollerType.PmsTerminalStatusPoller; }
        }
         
        public override IMultiLoggingProvider LoggingProvider
        {
            get { return Global.LoggingProvider; }
        }

        public override int PrimaryPollerTimeoutSeconds
        {
            get { return ObymobiIntervals.SERVICES_PMSTERMINALSTATUSPOLLER_INTERVAL; }
        }
      
        protected override TimeSpan WorkInterval
        {
            get { return TimeSpan.FromMilliseconds(ObymobiIntervals.SERVICES_PMSTERMINALSTATUSPOLLER_INTERVAL); }
        }

        protected override TimeSpan FirstWorkInterval
        {
            get { return WorkInterval; }
        }

        #endregion        

        protected override void ExecuteWork(bool force = false)
        {
            TerminalCollection pmsTerminals = this.terminalDataSource.GetPmsTerminals();

            Dictionary<int, PmsTerminalStatus> pmsTerminalStatuses = pmsTerminals.ToDictionary(x => x.TerminalId, x => new PmsTerminalStatus
            {
                TerminalId = x.TerminalId,
                CompanyId = x.CompanyId,
                IsOnline = x.IsOnline,
                LastSyncAsString = Obymobi.Logic.HelperClasses.TerminalHelper.GetLastSyncFromTerminal(x)
            });

            IEnumerable<PmsTerminalStatus> modifiedPmsTerminalStatuses = this.FindModifiedPmsTerminalStatuses(this.pmsTerminalStatuses, pmsTerminalStatuses);

            this.SendPmsTerminalStatusNotifications(modifiedPmsTerminalStatuses);
            this.SetPmsTerminalStatuses(pmsTerminalStatuses);
        }

        private IEnumerable<PmsTerminalStatus> FindModifiedPmsTerminalStatuses(Dictionary<int, PmsTerminalStatus> oldPmsTerminalStatuses, Dictionary<int, PmsTerminalStatus> newPmsTerminalStatuses)
        {
            List<PmsTerminalStatus> modifiedStatuses = new List<PmsTerminalStatus>();
            foreach (KeyValuePair<int, PmsTerminalStatus> entry in newPmsTerminalStatuses)
            {
                int terminalId = entry.Key;
                PmsTerminalStatus newPmsTerminalStatus = entry.Value;

                bool isNew = !oldPmsTerminalStatuses.ContainsKey(terminalId);
                PmsTerminalStatus oldPmsTerminalStatus = oldPmsTerminalStatuses.GetValueOrDefault(terminalId);
                
                if (isNew)
                {
                    LogVerbose("FindModifiedPmsTerminalStatuses", "New PMS Terminal Status for terminal: {0}", terminalId);
                    modifiedStatuses.Add(newPmsTerminalStatus);
                }
                else if (!oldPmsTerminalStatus.Equals(newPmsTerminalStatus))
                {
                    LogVerbose("FindModifiedPmsTerminalStatuses", "Modified PMS Terminal Status for terminal: {0}", terminalId);
                    modifiedStatuses.Add(newPmsTerminalStatus);
                }               
            }
            return modifiedStatuses;
        }

        private void SendPmsTerminalStatusNotifications(IEnumerable<PmsTerminalStatus> modifiedPmsTerminalStatuses)
        {
            if (modifiedPmsTerminalStatuses.Count() <= 0)
            {
                return;
            }

            TerminalCollection consoles = this.terminalDataSource.GetTerminalsWithPmsTerminalOfflineNotificationEnabled();

            LogVerbose("SendPmsTerminalStatusNotifications", "{0} Consoles with PmsTerminalOfflineNotificationEnabled set to true", consoles.Count);

            foreach (PmsTerminalStatus terminalStatus in modifiedPmsTerminalStatuses)
            {
                foreach (TerminalEntity console in consoles.Where(x => x.CompanyId == terminalStatus.CompanyId))
                {
                    LogInfo("SendPmsTerminalStatusNotifications", "Sending PmsTerminalStatusUpdated netmessage to: {0}, PMS terminal online: {1}, LastPmsSync: {2}", console.TerminalId, terminalStatus.IsOnline, terminalStatus.LastSyncAsString);
                    CometHelper.PmsTerminalStatusUpdated(console.TerminalId, terminalStatus.IsOnline, terminalStatus.LastSyncAsString);
                }
            }
        }        

        private void SetPmsTerminalStatuses(Dictionary<int, PmsTerminalStatus> pmsTerminalStatuses)
        {
            this.pmsTerminalStatuses = pmsTerminalStatuses;
        }
    }
}