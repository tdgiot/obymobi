﻿using System;
using System.Collections.Generic;
using Dionysos;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Obymobi;
using Obymobi.Constants;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using Obymobi.Logic.HelperClasses;
using Obymobi.Logic.Model.Weather;
using Obymobi.Web.Pollers;
using Obymobi.Web.Slack.Interfaces;
using Obymobi.Web.Weather;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Pollers
{
    public class WeatherPoller : BasePoller
    {
        private const int FORECAST_DAYS = 6;

        private readonly List<IWeatherProvider> weatherProviders = new List<IWeatherProvider>();
        private bool firstRun = true;
        private DateTime nextFullUpdateTime = DateTime.UtcNow.Date;

        public WeatherPoller(ISlackClient slackClient) : base(slackClient)
        {
            if (WebEnvironmentHelper.CloudEnvironmentIsProduction)
            {
                this.weatherProviders.Add(new AccuWeatherProvider(ObymobiConstants.GetAccuWeatherAppId(WebEnvironmentHelper.CloudEnvironment)));
            }
            //this.weatherProviders.Add(new DarkSkyWeatherProvider(ObymobiConstants.DarkSkySecretKey));
            this.weatherProviders.Add(new OpenWeatherMapProvider(ObymobiConstants.GetOpenWeatherMapAppId(WebEnvironmentHelper.CloudEnvironment)));
        }

        #region BasePoller overrides

        public override PollerType PollerType
        {
            get { return PollerType.WeatherPoller; }
        }

        public override Dionysos.Logging.IMultiLoggingProvider LoggingProvider
        {
            get { return Global.LoggingProvider; }
        }

        public override Enum LoggingType
        {
            get { return LoggingTypes.WeatherPoller; }
        }

        public override int PrimaryPollerTimeoutSeconds
        {
            get { return ObymobiIntervals.SERVICES_WEATHERPOLLER_INTERVAL; }
        }

        protected override TimeSpan WorkInterval
        {
            get { return TimeSpan.FromMilliseconds(ObymobiIntervals.SERVICES_WEATHERPOLLER_INTERVAL); }
        }

        protected override TimeSpan FirstWorkInterval
        {
            get { return TimeSpan.FromSeconds(0); }
        }

        #endregion

        protected override void ExecuteWork(bool force = false)
        {
            var weatherList = GetAvailableWeather();
            var cityList = GetListOfCities();

            // Don't use UtcNow here because time in the database is not UTC
            var oneHourAgo = DateTime.UtcNow.AddHours(-1);
            var currentTimeUtc = DateTime.UtcNow;

            // Full run, update all cities
            if (firstRun || currentTimeUtc >= this.nextFullUpdateTime || (currentTimeUtc.Hour == 0 && currentTimeUtc.Minute == 0))
            {
                LogVerbose("ExecuteWork", "Full weather update for {0} cities.", cityList.Count);
                foreach (var city in cityList)
                {
                    WeatherEntity weatherEntity = null;
                    bool shouldDownload = true;

                    foreach (var weather in weatherList)
                    {
                        if (weather.City == city.ToString())
                        {
                            weatherEntity = weather;

                            // Only do these extra checks on first run so we don't spam our weather provider
                            // in case Services gets restarted a couple of times.
                            if (firstRun && weather.UpdatedUTC.GetValueOrDefault(DateTime.MinValue) >= oneHourAgo)
                            {
                                LogInfo("ExecuteWork", "First run, city '{0}' was updated less then 1 hour ago. Skipping.", city);
                                shouldDownload = false;
                            }

                            break;
                        }
                    }

                    if (shouldDownload)
                    {
                        DownloadWeatherForCity(city, weatherEntity);
                    }
                }

                // Schedule full update for over one hour
                this.nextFullUpdateTime = new DateTime(currentTimeUtc.Year, currentTimeUtc.Month, currentTimeUtc.Day, currentTimeUtc.Hour, 0, 0).AddHours(1);

                if (!WebEnvironmentHelper.CloudEnvironmentIsProduction)
                {
                    // When running manual cloud environment (development pc) add extra hour
                    // Do this because of the extra weather spam from our office due to multiple machines
                    // If we would still get problems (eg. hitting the api call limits), this value can be increased
                    this.nextFullUpdateTime = this.nextFullUpdateTime.AddHours(1);
                }

                LogInfo("ExecuteWork", "Next full update scheduled for: {0} UTC", this.nextFullUpdateTime.ToString("g"));

                firstRun = false;
            }
            else // Otherwise, incremental check
            {
                LogVerbose("ExecuteWork", "Incremental city weather check");
                foreach (var city in cityList)
                {
                    WeatherEntity weatherEntity = null;
                    bool isNew = true;
                    foreach (var weather in weatherList)
                    {
                        if (weather.City != city.ToString()) 
                            continue;

                        isNew = false;

                        if (weather.UpdatedUTC > oneHourAgo)
                            break;

                        weatherEntity = weather;
                    }

                    // Found new city, download the data
                    if (isNew || weatherEntity != null)
                    {
                        DownloadWeatherForCity(city, weatherEntity);
                    }
                }
            }
        }

        private WeatherCollection GetAvailableWeather()
        {
            var weatherCollection = new WeatherCollection();
            weatherCollection.GetMulti(null);

            return weatherCollection;
        }

        private List<CityEntry> GetListOfCities()
        {
            var includeFieldsList = new IncludeFieldsList();
            includeFieldsList.Add(CompanyFields.Name);
            includeFieldsList.Add(CompanyFields.CountryCode);
            includeFieldsList.Add(CompanyFields.City);
            includeFieldsList.Add(CompanyFields.Longitude);
            includeFieldsList.Add(CompanyFields.Latitude);
            
            var filter = new PredicateExpression();
            filter.Add(CompanyFields.SystemType == SystemType.Crave);

            if (!WebEnvironmentHelper.CloudEnvironmentIsProduction)
            {
                var companyFilter = new PredicateExpression();
                companyFilter.Add(CompanyFields.CompanyId == 199); // Chewton
                companyFilter.AddWithOr(CompanyFields.CompanyId == 222); // Testing Co.
                companyFilter.AddWithOr(CompanyFields.CompanyId == 443); // Katalonia
                companyFilter.AddWithOr(CompanyFields.CompanyId == 343); // Aria
                companyFilter.AddWithOr(CompanyFields.CompanyId == 430); // GHBM

                filter.Add(companyFilter);
            }

            var companyCollection = new CompanyCollection();
            companyCollection.GetMulti(filter, includeFieldsList, 0);

            var cityList = new List<CityEntry>();
            foreach (var companyEntity in companyCollection)
            {
                if (companyEntity.City.Length > 0 && !companyEntity.CountryCode.IsNullOrWhiteSpace())
                {
                    Country country;
                    try
                    {
                        country = (Country)companyEntity.CountryCode;
                    }
                    catch (Exception e)
                    {
                        LogInfo("GetListOfCities", "Failed to convert CountryCode '{0}' to object.", companyEntity.CountryCode);
                        continue;
                    }

                    if (cityList.Find(c => (c.Name == companyEntity.City && c.Country.CodeAlpha3 == companyEntity.CountryCode)) == null)
                    {
                        CityEntry entry = new CityEntry
                        {
                            Name = companyEntity.City,
                            Country = country,
                            Latitude = companyEntity.Latitude,
                            Longitude = companyEntity.Longitude
                        };
                        cityList.Add(entry);
                    }
                }
                else
                {
                    LogInfo("GetListOfCities", "Skipping '{0}', no city or country defined.", companyEntity.Name);
                }
            }

            return cityList;
        }

        private void DownloadWeatherForCity(CityEntry city, WeatherEntity weatherEntity)
        {
            var weather = new Weather();
            weather.City = city.ToString();
            weather.Date = DateTime.UtcNow;

            if (!city.Latitude.HasValue || !city.Longitude.HasValue)
            {
                LogWarning("DownloadWeatherForCity", "Skipping city '{0}'. No coordinates specified.", city);
                return;
            }

            foreach (var weatherProvider in this.weatherProviders)
            {
                LogVerbose("DownloadWeatherForCity", "Downloading weather data from '{0}' for: {1} (Lat: {2}, Lon: {3})", weatherProvider.Name, city, city.Latitude.GetValueOrDefault(0), city.Longitude.GetValueOrDefault(0));

                try
                {
                    weather.Current = weatherProvider.GetCurrentWeatherForLocation(city.Latitude.Value, city.Longitude.Value);
                    weather.Forecast = weatherProvider.GetForecastForLocation(city.Latitude.Value, city.Longitude.Value, FORECAST_DAYS);

                    if (weather.Current != null && weather.Forecast != null)
                    {
                        break;
                    }
                }
                catch (Exception ex)
                {
                    LogWarning("DownloadWeatherForCity", "Failed to download from '{0}' for '{1}': {2}.", weatherProvider.Name, city, ex.Message);
                    LogWarning("DownloadWeatherForCity", ex.ProcessStackTrace(true));
                } 
            }
        
            UploadWeatherData(weather, weatherEntity);
        }

        private void UploadWeatherData(Weather weather, WeatherEntity weatherEntity)
        {
            if (weather.Current != null && weather.Forecast != null)
            {
                var settings = new JsonSerializerSettings();
                settings.NullValueHandling = NullValueHandling.Ignore;
                settings.ContractResolver = new CamelCasePropertyNamesContractResolver();

                try
                {
                    LogVerbose("UploadWeatherData", "Saving weather data '{0}' to database.", weather.City);

                    if (weatherEntity == null)
                        weatherEntity = new WeatherEntity();

                    weatherEntity.City = weather.City;
                    weatherEntity.LastWeatherData = JsonConvert.SerializeObject(weather, new Formatting(), settings);
                    weatherEntity.UpdatedUTC = DateTime.UtcNow;

                    if (weatherEntity.Save())
                    {
                        LogVerbose("UploadWeatherData", "Creating task for uploading weather data '{0}' to CDN.", weather.City);
                        CloudTaskHelper.UploadWeather(weatherEntity);
                    }
                }
                catch (Exception ex)
                {
                    LogError("UploadWeatherData", "Failed to save/upload '{0}' - {1}", weather.City, ex.Message);
                }
            }
            else
            {
                LogWarning("UploadWeatherData", "Not uploading weather data for '{0}', missing current ({1}) and/or forecast ({2})", weather.City, weather.Current == null, weather.Forecast == null);
            }
        }

        private class CityEntry
        {
            public string Name { get; set; }
            public Country Country { get; set; }
            public double? Latitude { get; set; }
            public double? Longitude { get; set; }

            public override string ToString()
            {
                return string.Format("{0},{1}", Name, Country.CodeAlpha2).ToLower();
            }
        }
    }
}