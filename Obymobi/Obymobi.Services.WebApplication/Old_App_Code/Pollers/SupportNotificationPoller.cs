using Dionysos;
using Dionysos.Data.LLBLGen;
using Dionysos.Logging;
using Newtonsoft.Json;
using Obymobi;
using Obymobi.Configuration;
using Obymobi.Constants;
using Obymobi.Data;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using Obymobi.Logic.Model;
using Obymobi.Services.Logic.HelperClasses;
using Obymobi.Services.WebApplication;
using Obymobi.Web.Pollers;
using Obymobi.Web.Slack;
using Obymobi.Web.Slack.Interfaces;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using Math = Dionysos.Math;

namespace Pollers
{
    /// <summary>
    ///     Summary description for SupportNotificationPoller
    /// </summary>
    public class SupportNotificationPoller : BasePoller
    {
        private const int BOUNCED_EMAILS_CHECK_MINUTES = 15;
        private const int OFFLINE_DEVICE_PERCENTAGE_THRESHOLD_ON_PLATFORM_OUTAGE = 70;

        private static DateTime nextBouncedEmailsCheck = DateTime.Now.Subtract(TimeSpan.FromMinutes(20));

        private Obymobi.Logic.HelperClasses.SupportNotificationSendHelper notificationSendHelper;

        private readonly DeviceDataSource deviceDataSource = new DeviceDataSource();
        private readonly CompanyDataSource companyDataSource = new CompanyDataSource();
        private readonly TerminalDataSource terminalDataSource = new TerminalDataSource();

        private readonly ISlackClients slackClients;

        private readonly IMessageFactory messageFactory;

        public SupportNotificationPoller(ISlackClient slackClient, ISlackClients slackClients, IMessageFactory messageFactory) : base(slackClient)
        {
            this.slackClients = slackClients;
            this.messageFactory = messageFactory;
        }

        public override Enum LoggingType
        {
            get { return LoggingTypes.SupportNotificationPoller; }
        }

        public override PollerType PollerType
        {
            get { return PollerType.SupportNotificationPoller; }
        }

        public override IMultiLoggingProvider LoggingProvider
        {
            get { return Global.LoggingProvider; }
        }

        public override int PrimaryPollerTimeoutSeconds
        {
            get { return ObymobiIntervals.SERVICES_SUPPORTNOTIFICATIONPOLLER_INTERVAL; }
        }

        protected override TimeSpan WorkInterval
        {
            get { return TimeSpan.FromMilliseconds(ObymobiIntervals.SERVICES_SUPPORTNOTIFICATIONPOLLER_INTERVAL); }
        }

        protected override TimeSpan FirstWorkInterval
        {
            get { return WorkInterval; }
        }

        public override void Initialize(CancellationToken cancellationToken)
        {
            string emailSenderAddress, emailSenderName, smsSender;
            WebEnvironmentHelper.GetSupportNotificationSenderInformation(out emailSenderAddress, out emailSenderName, out smsSender);
            notificationSendHelper = new Obymobi.Logic.HelperClasses.SupportNotificationSendHelper(emailSenderAddress, emailSenderName, smsSender);

            base.Initialize(cancellationToken);
        }

        protected override void ExecuteWork(bool force = false)
        {
            // WARNING: If you want to change any of the intervals, update those in the document as well
            // Software Development > Architecture
            // https://docs.google.com/a/crave-emenu.com/document/d/1hrd79Jh43bMxWxvBiAg6lhETmwBKQdxiUVgV5I6mxZI/edit#

            // Offline Terminals
            try
            {
                SendSupportNotificationsForOfflineTerminals();
            }
            catch (Exception ex)
            {
                LogError("SendSupportNotificationsForOfflineTerminals", "Message: {0}\n{1}", ex.Message, ex.StackTrace);
            }

            // Offline clients
            try
            {
                SendSupportNotificationForOfflineClients();
            }
            catch (Exception ex)
            {
                LogError("SendSupportNotificationsForOfflineClients", "Message: {0}\n{1}", ex.Message, ex.StackTrace);
            }

            // MB: Disabled because this was causing way too much messages
            // DK: Enabled again, but only for companies which have terminal with POS connector
            // Unprocessable Orders
            try
            {
                SendSupportNotificationsForUnprocessableOrders_POSOnly();
            }
            catch (Exception ex)
            {
                LogError("SendSupportNotificationsForUnprocessableOrders", "Message: {0}\n{1}", ex.Message, ex.StackTrace);
            }

            // Orderroutesteps support notifications for 'not being retrieved' and 'not being handled'            
            try
            {
                SendSupportNotificationsForExpiredSteps();
            }
            catch (Exception ex)
            {
                LogError("SendSupportNotificationsForExpiredSteps", "Message {0}\n{1}", ex.Message, ex.StackTrace);
            }

            /*try
            {
                this.SendSupportNotificationsForNocServer();
            }
            catch (Exception ex)
            {
                this.LogError("SendSupportNotificationsForNocServer", "Message: {0}\n{1}", ex.Message, ex.StackTrace);
            }*/

            // Comet messages from webservice
            try
            {
                SendSupportNotificationForWebserviceCometConnection();
            }
            catch (Exception ex)
            {
                LogError("SendSupportNotificationForWebserviceCometConnection", "Message: {0}\n{1}", ex.Message, ex.StackTrace);
            }

            // Too much offline clients (jump)
            try
            {
                SendSupportNotificationForTooMuchOfflineClientsJump();
            }
            catch (Exception ex)
            {
                LogError("SendSupportNotificationForTooMuchOfflineClientsJump", "Message: {0}\n{1}", ex.Message, ex.StackTrace);
            }

            // Too much active requests
            try
            {
                SendSupportNotificationForTooMuchActiveRequests();
            }
            catch (Exception ex)
            {
                LogError("SendSupportNotificationForTooMuchActiveRequests", "Message: {0}\n{1}", ex.Message, ex.StackTrace);
            }

            // Too much clients on webservice mode
            try
            {
                SendSupportNotificationForTooMuchClientsOnWebserviceMode();
            }
            catch (Exception ex)
            {
                LogError("SendSupportNotificationForTooMuchClientsOnWebserviceMode", "Message: {0}\n{1}", ex.Message, ex.StackTrace);
            }

            try
            {
                //CheckFreeDiskSpace();
            }
            catch (Exception ex)
            {
                LogError("CheckFreeDiskSpace - ERROR: {0}\n{1}", ex.Message, ex.StackTrace);
            }

            try
            {
                SendNotificationIfMessagingOutage();
            }
            catch (Exception ex)
            {
                LogError("SendNotificationIfMessagingOutage", "Message: {0}\n{1}", ex.Message, ex.StackTrace);
            }

            // Check for bounced emails and report if any emails were bounced in the last 15 minutes
            if (DateTime.Now > SupportNotificationPoller.nextBouncedEmailsCheck)
            {
                try
                {
                    LogInfo("SendSupportNotificationsForBouncedEmails", "Going to check");
                    SendSupportNotificationsForBouncedEmails();
                    SupportNotificationPoller.nextBouncedEmailsCheck = DateTime.Now.AddMinutes(SupportNotificationPoller.BOUNCED_EMAILS_CHECK_MINUTES);
                }
                catch (Exception ex)
                {
                    LogError("CheckBouncedEmails", "Message: {0}\n{1}", ex.Message, ex.StackTrace);
                }
            }
        }

        private DateTime lastActiveRequests80kWarningSent = DateTime.MinValue;
        private DateTime lastActiveRequests100kWarningSent = DateTime.MinValue;
        private bool lastActiveRequestWarningSent = false;

        private void SendSupportNotificationForTooMuchActiveRequests()
        {
            PerformanceCounter performanceCounter = new PerformanceCounter("W3SVC_W3WP", "Active Requests", "_Total", true);
            long value = performanceCounter.RawValue;

            LogVerbose("CheckActiveRequests", "Active requests: {0}", value);

            SlackAttachmentField attachmentField = new SlackAttachmentField("Server", Environment.MachineName);

            if (value > 100000)
            {
                if ((DateTime.Now - lastActiveRequests100kWarningSent).TotalMinutes > 60)
                {
                    lastActiveRequests100kWarningSent = DateTime.Now;
                    lastActiveRequestWarningSent = true;

                    SendSlackMessage(Obymobi.Logic.HelperClasses.SupportpoolHelper.GetSystemSupportpool(), "Danger: Active requests are greater than 100k. The Messaging connections will now automatically be reset", SlackAttachmentColor.Danger, null, true, attachmentField);
                }
            }
            else if (value > 80000)
            {
                if ((DateTime.Now - lastActiveRequests80kWarningSent).TotalMinutes > 60)
                {
                    lastActiveRequests80kWarningSent = DateTime.Now;
                    lastActiveRequestWarningSent = true;

                    //this.SendSupportNotification(SupportpoolHelper.GetSystemSupportpool(), "WARNING - ACTION REQUIRED: The number is active requests ({0}) is greater than 50k.".FormatSafe(Environment.MachineName), null, false);
                    SendSlackMessage(Obymobi.Logic.HelperClasses.SupportpoolHelper.GetSystemSupportpool(), "Warning: Active requests are greater than 80k. This doesnt have to be a problem, but please monitor the number of connections", SlackAttachmentColor.Warning, null, true, attachmentField);
                }
            }
            else if (value < 30000)
            {
                if (lastActiveRequestWarningSent)
                {
                    lastActiveRequestWarningSent = false;
                    SendSlackMessage(Obymobi.Logic.HelperClasses.SupportpoolHelper.GetSystemSupportpool(), "Closed: Active requests too high.", SlackAttachmentColor.Good, null, false, attachmentField);
                }
            }
        }

        private DateTime lastTooMuchClientsOnWebserviceModeWarningSent = DateTime.MinValue;

        private void SendSupportNotificationForTooMuchClientsOnWebserviceMode()
        {
            if (!WebEnvironmentHelper.CloudEnvironmentIsProduction)
            {
                return;
            }

            ClientCollection clientCollection = new ClientCollection();

            RelationCollection relation = new RelationCollection(ClientEntity.Relations.DeviceEntityUsingDeviceId);

            PredicateExpression connectedToSignalRFilter = new PredicateExpression();
            connectedToSignalRFilter.Add(DeviceFields.LastRequestUTC >= DateTime.UtcNow.AddMinutes(-3));
            connectedToSignalRFilter.Add(DeviceFields.CommunicationMethod == ClientCommunicationMethod.SignalR);
            int connectedToSignalRCount = clientCollection.GetDbCount(connectedToSignalRFilter, relation);

            PredicateExpression totalOnlineFilter = new PredicateExpression();
            totalOnlineFilter.Add(DeviceFields.LastRequestUTC >= DateTime.UtcNow.AddMinutes(-3));
            int totalOnlineCount = clientCollection.GetDbCount(totalOnlineFilter, relation);

            if (totalOnlineCount == 0)
            {
                return;
            }

            int connectedToSignalRPercentage = Dionysos.Math.Percentage(connectedToSignalRCount, totalOnlineCount, true);

            if (connectedToSignalRPercentage < 75)
            {
                LogWarning("SendSupportNotificationForTooMuchClientsOnWebserviceMode", string.Format("Too many clients are running on webservice mode. {0} clients connected to SignalR of {1} clients online ({2}%).", connectedToSignalRCount, totalOnlineCount, connectedToSignalRPercentage));

                if ((DateTime.Now - lastTooMuchClientsOnWebserviceModeWarningSent).TotalMinutes > 60)
                {
                    LogWarning("SendSupportNotificationForTooMuchClientsOnWebserviceMode", "Last warning sent was more than an hour ago, sending Slack message.");

                    SlackAttachmentField attachmentField = new SlackAttachmentField("Server", Environment.MachineName);

                    lastTooMuchClientsOnWebserviceModeWarningSent = DateTime.Now;
                    SendSlackMessage(Obymobi.Logic.HelperClasses.SupportpoolHelper.GetSystemSupportpool(), string.Format("Too many clients are running on webservice mode. {0} clients connected to SignalR of {1} clients online ({2}%).", connectedToSignalRCount, totalOnlineCount, connectedToSignalRPercentage), SlackAttachmentColor.Danger, null, true, attachmentField);
                }
                else
                {
                    LogWarning("SendSupportNotificationForTooMuchClientsOnWebserviceMode", "Last warning sent was within an hour ago.");
                }
            }
        }


        private const int DELAY_BETWEEN_MESSAGE_OUTAGE_NOTIFICATIONS_MINUTES = 5;
        private const int TERMINALS_NOT_PICKING_UP_ORDERS_BEFORE_SENDING_NOTIFICATION_AMOUNT = 5;

        private static DateTime lastMessagingOutageNotificationSentUtc = DateTime.MinValue;

        private void SendNotificationIfMessagingOutage()
        {
            if ((DateTime.UtcNow - lastMessagingOutageNotificationSentUtc).TotalMinutes < DELAY_BETWEEN_MESSAGE_OUTAGE_NOTIFICATIONS_MINUTES)
            {
                return;
            }

            int terminalsNotPickingUpOrders = terminalDataSource.GetNumberOfTerminalsNotPickingUpOrders();
            if (terminalsNotPickingUpOrders < TERMINALS_NOT_PICKING_UP_ORDERS_BEFORE_SENDING_NOTIFICATION_AMOUNT)
            {
                return;
            }

            lastMessagingOutageNotificationSentUtc = DateTime.UtcNow;

            SlackMessage slackMessage = new SlackMessage(this.messageFactory.Create($"{terminalsNotPickingUpOrders} terminals are not retrieving their pending orders. Possible messaging outage detected."));
            slackClients.GetDevelopmentNotificationsSlackClient().SendMessage(slackMessage);
        }

        private DateTime lastFreeDiskSpace50WarningSent = DateTime.MinValue;
        private DateTime lastFreeDiskSpace25WarningSent = DateTime.MinValue;
        private DateTime lastFreeDiskSpace5WarningSent = DateTime.MinValue;
        private bool lastFreeDiskSpaceWarningSent = false;

        private void CheckFreeDiskSpace()
        {
            int firstWarningTreshold = ConfigurationManager.GetInt(ServicesConfigConstants.FirstWarningDiskspaceGb);
            int secondWarningTreshold = ConfigurationManager.GetInt(ServicesConfigConstants.SecondWarningDiskspaceGb);
            int thirdWarningTreshold = ConfigurationManager.GetInt(ServicesConfigConstants.ThirdWarningDiskspaceGb);

            foreach (DriveInfo drive in DriveInfo.GetDrives())
            {
                if (drive.IsReady && drive.Name.Equals("C:\\", StringComparison.CurrentCultureIgnoreCase))
                {
                    // Found the C disk, get freespace in gigabytes
                    long availableSpaceGb = drive.AvailableFreeSpace / (1024 * 1024 * 1024);

                    if (availableSpaceGb < thirdWarningTreshold)
                    {
                        if ((DateTime.Now - lastFreeDiskSpace5WarningSent).TotalHours > 24)
                        {
                            lastFreeDiskSpace5WarningSent = DateTime.Now;
                            lastFreeDiskSpaceWarningSent = true;
                            SendSlackMessage(Obymobi.Logic.HelperClasses.SupportpoolHelper.GetSystemSupportpool(), "The available disk space on the server is less than {0} gb.".FormatSafe(thirdWarningTreshold), SlackAttachmentColor.Danger);
                        }
                    }
                    else if (availableSpaceGb < secondWarningTreshold)
                    {
                        if ((DateTime.Now - lastFreeDiskSpace25WarningSent).TotalHours > 24)
                        {
                            lastFreeDiskSpace25WarningSent = DateTime.Now;
                            lastFreeDiskSpaceWarningSent = true;
                            SendSlackMessage(Obymobi.Logic.HelperClasses.SupportpoolHelper.GetSystemSupportpool(), "The available disk space on the server is less than {0} gb.".FormatSafe(secondWarningTreshold), SlackAttachmentColor.Warning);
                        }
                    }
                    else if (availableSpaceGb < firstWarningTreshold)
                    {
                        if ((DateTime.Now - lastFreeDiskSpace50WarningSent).TotalHours > 24)
                        {
                            lastFreeDiskSpace50WarningSent = DateTime.Now;
                            lastFreeDiskSpaceWarningSent = true;
                            SendSlackMessage(Obymobi.Logic.HelperClasses.SupportpoolHelper.GetSystemSupportpool(), "The available disk space on the server is less than {0} gb.".FormatSafe(firstWarningTreshold), SlackAttachmentColor.Warning);
                        }
                    }
                    else if (lastFreeDiskSpaceWarningSent)
                    {
                        lastFreeDiskSpaceWarningSent = false;
                        SendSlackMessage(Obymobi.Logic.HelperClasses.SupportpoolHelper.GetSystemSupportpool(), "Closed: The available disk space on the server is too low.", SlackAttachmentColor.Good);
                    }
                }
            }
        }

        private DateTime lastSupportNotificationForWebserviceCometConnectionSent = DateTime.MinValue;

        private void SendSupportNotificationForWebserviceCometConnection()
        {
            DateTime lastMessage = ConfigurationManager.GetDateTimeRefreshed(CraveCometConfigConstants.LAST_COMET_MESSAGE_FROM_WEBSERVICE);

            bool sendNotificationRequired = (DateTime.Now - lastSupportNotificationForWebserviceCometConnectionSent).TotalHours >= 1;

            if ((DateTime.UtcNow - lastMessage).TotalSeconds > ObymobiIntervals.SUPPORT_NOTIFICATIONS_COMET_CONNECTION_DOWN_THRESHOLD)
            {
                if (sendNotificationRequired)
                {
                    SendSupportNotification(Obymobi.Logic.HelperClasses.SupportpoolHelper.GetSystemSupportpool(), "The last comet message received from the Webservice has been longer than {0} seconds ago.".FormatSafe(ObymobiIntervals.SUPPORT_NOTIFICATIONS_COMET_CONNECTION_DOWN_THRESHOLD / 1000), null);
                    lastSupportNotificationForWebserviceCometConnectionSent = DateTime.Now;
                }
            }
            else
            {
                lastSupportNotificationForWebserviceCometConnectionSent = DateTime.MinValue;
            }
        }

        private readonly Dictionary<int, int> offlineClientPercentagePerCompany = new Dictionary<int, int>();
        private DateTime lastSupportNotificationCheckForTooMuchOfflineClientsJump = DateTime.MinValue;

        private void SendSupportNotificationForTooMuchOfflineClientsJump()
        {
            if (lastSupportNotificationCheckForTooMuchOfflineClientsJump >= DateTime.UtcNow.AddMilliseconds(ObymobiIntervals.DEVICE_OFFLINE_THRESHOLD * -1))
            {
                return;
            }
            lastSupportNotificationCheckForTooMuchOfflineClientsJump = DateTime.UtcNow;

            PredicateExpression filter = new PredicateExpression();
            filter.Add(CompanyFields.UseMonitoring == true);
            filter.Add(CompanyFields.SystemType == SystemType.Crave);

            CompanyCollection companyCollection = EntityCollection.GetMulti<CompanyCollection>(filter);
            foreach (CompanyEntity companyEntity in companyCollection)
            {
                int offlineClients = SupportNotificationPoller.GetOfflineClientCount(companyEntity);
                int totalClients = SupportNotificationPoller.GetTotalClientCount(companyEntity);
                int percentageOffline = Math.Percentage(offlineClients, totalClients, true);

                if (!offlineClientPercentagePerCompany.ContainsKey(companyEntity.CompanyId))
                {
                    // No offline percentage was known yet, so we can't determine the jump
                    offlineClientPercentagePerCompany.Add(companyEntity.CompanyId, percentageOffline);
                }
                else
                {
                    int previousPercentageOffline = offlineClientPercentagePerCompany[companyEntity.CompanyId];
                    if (percentageOffline > previousPercentageOffline)
                    {
                        int jump = percentageOffline - previousPercentageOffline;
                        if (jump > companyEntity.PercentageDownJumpForNotification)
                        {
                            bool send = false;

                            if (!companyEntity.SupportpoolId.HasValue)
                            {
                                LogWarning("SendSupportNotificationForTooMuchOfflineClientsJump", "No support pool was configured for company '{0}'.", companyEntity.Name);
                            }
                            else if (!companyEntity.PercentageDownJumpNotificationLastSentUTC.HasValue)
                            {
                                send = true;
                            }
                            else if (companyEntity.PercentageDownJumpNotificationLastSentUTC < DateTime.UtcNow.AddHours(-1))
                            {
                                send = true;
                            }
                            else
                            {
                                LogVerbose("SendSupportNotificationForTooMuchOfflineClientsJump", "A support notification for the clients that have gone offline was already sent in the last hour for company '{0}'.", companyEntity.Name);
                            }

                            if (send)
                            {
                                if (SendSupportNotification(companyEntity.SupportpoolEntity, string.Format("{0} percent of the clients of company {1} have gone offline", jump, companyEntity.Name), SupportNotification.TooMuchOfflineClientsJump))
                                {
                                    companyEntity.PercentageDownJumpNotificationLastSentUTC = DateTime.UtcNow;
                                    companyEntity.Save();
                                }

                                SendSlackMessage(companyEntity.SupportpoolEntity, string.Format("{0} Percent of the clients of company {1} have gone offline", jump, companyEntity.Name), SlackAttachmentColor.Warning);
                            }
                        }
                    }

                    offlineClientPercentagePerCompany[companyEntity.CompanyId] = percentageOffline;
                }
            }
        }

        private static int GetTotalClientCount(CompanyEntity companyEntity)
        {
            PredicateExpression clientFilter = new PredicateExpression();
            clientFilter.Add(CompanyFields.UseMonitoring == true);
            clientFilter.Add(CompanyFields.SystemType == SystemType.Crave);
            clientFilter.Add(ClientFields.CompanyId == companyEntity.CompanyId);
            clientFilter.Add(ClientFields.DeviceId != DBNull.Value);

            RelationCollection clientRelations = new RelationCollection();
            clientRelations.Add(ClientEntityBase.Relations.CompanyEntityUsingCompanyId);

            return EntityCollection.GetDbCount<ClientCollection>(clientFilter, clientRelations);
        }

        private static int GetOfflineClientCount(CompanyEntity companyEntity)
        {
            PredicateExpression offlineFilter = new PredicateExpression();
            offlineFilter.Add(CompanyFields.UseMonitoring == true);
            offlineFilter.Add(CompanyFields.SystemType == SystemType.Crave);
            offlineFilter.Add(ClientFields.CompanyId == companyEntity.CompanyId);
            offlineFilter.Add(ClientFields.DeviceId != DBNull.Value);
            offlineFilter.Add(DeviceFields.LastRequestUTC <= DateTime.UtcNow.AddMilliseconds(-ObymobiIntervals.DEVICE_OFFLINE_THRESHOLD));

            RelationCollection offlineRelations = new RelationCollection();
            offlineRelations.Add(ClientEntityBase.Relations.CompanyEntityUsingCompanyId);
            offlineRelations.Add(ClientEntityBase.Relations.DeviceEntityUsingDeviceId);

            int offlineClients = EntityCollection.GetDbCount<ClientCollection>(offlineFilter, offlineRelations);
            return offlineClients;
        }

        private DateTime lastSupportNotificationForNocServerSent = DateTime.MinValue;

        private void SendSupportNotificationsForOfflineTerminals()
        {
            PrefetchPath path = new PrefetchPath(EntityType.OrderRoutestephandlerEntity);
            path.Add(OrderRoutestephandlerEntityBase.PrefetchPathTerminalEntity).SubPath.Add(TerminalEntityBase.PrefetchPathCompanyEntity).SubPath.Add(CompanyEntityBase.PrefetchPathSupportpoolEntity);

            RelationCollection joins = new RelationCollection();
            joins.Add(OrderRoutestephandlerEntityBase.Relations.TerminalEntityUsingTerminalId);
            joins.Add(TerminalEntityBase.Relations.CompanyEntityUsingCompanyId);
            joins.Add(TerminalEntityBase.Relations.DeviceEntityUsingDeviceId);

            // Others - Threshold: 15 minutes
            PredicateExpression filter = new PredicateExpression();
            filter.Add(OrderRoutestephandlerFields.Status >= OrderRoutestephandlerStatus.WaitingToBeRetrieved);
            filter.Add(OrderRoutestephandlerFields.Status < OrderRoutestephandlerStatus.Completed);
            filter.Add(DeviceFields.LastRequestUTC < DateTime.UtcNow.AddMilliseconds(ObymobiIntervals.SUPPORT_NOTIFICATIONS_TERMINAL_OFFLINE_THRESHOLD * -1));
            filter.Add(CompanyFields.SystemType == SystemType.Crave);
            filter.Add(DeviceFields.LastRequestUTC != DeviceFields.LastRequestNotifiedBySmsUTC);

            OrderRoutestephandlerCollection orderRoutesteps = new OrderRoutestephandlerCollection();
            orderRoutesteps.GetMulti(filter, 0, null, joins, path);

            SendSupportNotificationsForOrderRoutestephandlers(orderRoutesteps);

            // Offline Terminals (not having pending orders)
            int minutesOfflineBeforeWarning = 15;
            TerminalCollection terminals = TerminalHelper.GetOfflineTerminals(minutesOfflineBeforeWarning, false, true);
            foreach (TerminalEntity terminal in terminals)
            {
                if (terminal.CompanyEntity.SystemType == SystemType.Otoucho)
                {
                    continue;
                }
                if (!terminal.CompanyEntity.UseMonitoring)
                {
                    continue;
                }
                if (!terminal.UseMonitoring)
                {
                    continue;
                }
                if (!terminal.DeviceId.HasValue)
                {
                    continue;
                }

                DeviceEntity deviceEntity = terminal.DeviceEntity;

                // Send a text message if required
                if (deviceEntity.LastRequestUTC == null)
                {
                    deviceEntity.LastRequestUTC = DateTime.MinValue;
                }

                if (deviceEntity.LastRequestUTC != deviceEntity.LastRequestNotifiedBySmsUTC)
                {
                    // Send Text Message
                    string unitName = "Terminal";
                    if (terminal.TerminalType == (int)TerminalType.Console)
                    {
                        unitName = "Console";
                    }
                    else if (terminal.TerminalType == (int)TerminalType.OnSiteServer)
                    {
                        unitName = "OnSiteServer";
                    }

                    string message = "{0} '{1}' of Company '{2}' is offline since: '{3}'.".FormatSafe(unitName, terminal.Name, terminal.CompanyEntity.Name, deviceEntity.LastRequestUTC);
                    if (SendSupportNotification(terminal.CompanyEntity.SupportpoolEntity, message, SupportNotification.OfflineTerminals))
                    {
                        deviceEntity.LastRequestNotifiedBySmsUTC = deviceEntity.LastRequestUTC;
                        deviceEntity.Save();
                    }
                }
            }
        }

        private void SendSupportNotificationForOfflineClients()
        {
            double percentageOfDevicesOffline = deviceDataSource.GetPercentageOfDevicesOfflineForMonitoredCompanies();
            if (percentageOfDevicesOffline > OFFLINE_DEVICE_PERCENTAGE_THRESHOLD_ON_PLATFORM_OUTAGE)
            {
                // There is a platform outage, dont send support notifications for offline clients
                LogVerbose("SendSupportNotificationOfflineClients", "Not sending support notifications because it looks like there is a platform outage with '{0}%' devices offline.", percentageOfDevicesOffline);
                return;
            }

            DateTime currentTimeUtc = DateTime.UtcNow;
            DateTime clientLastRequest = currentTimeUtc.AddMinutes(-15);

            CompanyCollection companyCollection = companyDataSource.GetCompaniesThatUseMonitoring();

            foreach (CompanyEntity companyEntity in companyCollection)
            {
                if (companyEntity.CorrespondenceEmail.IsNullOrWhiteSpace())
                {
                    continue;
                }

                // Convert the current UTC time to the local time of the company
                DateTime currentTimeConverted = TimeZoneInfo.ConvertTime(currentTimeUtc, companyEntity.TimeZoneInfo);
                // Daily report send time, if not set default to 08:00AM
                DateTime dailyReportSendTime = companyEntity.DailyReportSendTime.GetValueOrDefault(new DateTime(currentTimeConverted.Year, currentTimeConverted.Month, currentTimeConverted.Day, 8, 0, 0));
                TimeSpan timeDiff = currentTimeUtc.Subtract(companyEntity.LastDailyReportSendUTC.GetValueOrDefault(DateTime.MinValue));

                if (currentTimeConverted.Hour == dailyReportSendTime.Hour && currentTimeConverted.Minute == dailyReportSendTime.Minute && timeDiff.TotalHours > 1)
                {
                    List<string> companyMail = companyEntity.CorrespondenceEmail.SplitLines().ToList();
                    companyMail.Add("dk@crave-emenu.com");

                    StringBuilder messageBuilder = new StringBuilder();
                    messageBuilder.AppendFormat("<strong>{0}</strong><br/><br/>", companyEntity.Name);

                    StringBuilder clientBuilder = new StringBuilder();
                    clientBuilder.Append("<table border='1' cellspacing='0' cellpadding='4' style='border-collapse: collapse'>" + "<tr><td><strong>Client Id</strong></td><td><strong>Identifier</strong></td><td><strong>IP Address (public)</strong></td><td><strong>Room</strong></td>" + "<td><strong>Battery</strong></td><td><strong>Last Online</strong></td><td><strong>Offline Period</strong></td><td><strong>Deliverypoint Group</strong></td><td><strong>SSID</strong></td><td><strong>WiFi Strength</strong></td></tr>");

                    int offlineCount = 0;

                    DeviceCollection deviceCollection = deviceDataSource.GetOfflineDevicesForCompany(companyEntity.CompanyId);
                    foreach (DeviceEntity deviceEntity in deviceCollection)
                    {
                        if (deviceEntity.ClientEntitySingle == null)
                            continue;

                        ClientEntity clientEntity = deviceEntity.ClientEntitySingle;

                        offlineCount++;
                        string identifier = deviceEntity.Identifier;

                        TimeSpan offlinePeriod = currentTimeUtc.Subtract(deviceEntity.LastRequestUTC.GetValueOrDefault());
                        string format = offlinePeriod.Days >= 1 ? "d' day 'h' hour 'mm' min'" : "hh' hour 'mm' min'";

                        string deliverypoint = "";
                        if (clientEntity.DeliverypointId.HasValue)
                        {
                            deliverypoint = clientEntity.DeliverypointEntity.DeliverypointgroupNameDeliverypointNumber;
                        }

                        clientBuilder.AppendFormat("<tr><td>{0}</td><td>{1}</td><td>{2}</td><td>{3}</td><td>{4}%</td><td>{5}</td><td>{6}</td><td>{7}</td><td>{8}</td><td>{9}</td></tr>", clientEntity.ClientId, identifier, deviceEntity.PublicIpAddress, deliverypoint, deviceEntity.BatteryLevel, deviceEntity.LastRequestUTC, offlinePeriod.ToString(format), clientEntity.DeliverypointgroupEntity?.Name, clientEntity.LastWifiSsid, deviceEntity.WifiStrength);
                    }
                    clientBuilder.Append("</table>");

                    messageBuilder.AppendFormat("<strong>Total offline clients:</strong> {0}<br/><br/>", offlineCount);
                    messageBuilder.Append(clientBuilder);

                    LogVerbose("SendSupportNotificationOfflineClients", "Sending offline clients report for {0}. Offline count: {1}", companyEntity.Name, offlineCount);

                    string errorReport;
                    notificationSendHelper.SendSupportNotification(null, companyMail, string.Format("Offline Clients for {0}", companyEntity.Name), messageBuilder.ToString(), out errorReport, true);

                    companyEntity.LastDailyReportSendUTC = currentTimeUtc;
                    companyEntity.Save();
                }
            }
        }

        private void SendSupportNotificationsForOrderRoutestephandlers(OrderRoutestephandlerCollection orderRoutesteps)
        {
            PrefetchPath path = new PrefetchPath(EntityType.OrderRoutestephandlerEntity);
            path.Add(OrderRoutestephandlerEntityBase.PrefetchPathTerminalEntity).SubPath.Add(TerminalEntityBase.PrefetchPathCompanyEntity).SubPath.Add(CompanyEntityBase.PrefetchPathSupportpoolEntity);

            OrderRoutestephandlerCollection orderRoutestepLookup = new OrderRoutestephandlerCollection();
            foreach (OrderRoutestephandlerEntity step in orderRoutesteps)
            {
                if (!step.TerminalEntity.CompanyEntity.UseMonitoring)
                {
                    continue;
                }
                if (!step.TerminalEntity.UseMonitoring) // Check if monitoring is enabled for this terminal
                {
                    continue;
                }
                if (!step.TerminalEntity.DeviceId.HasValue)
                {
                    continue;
                }

                // Don't notify this quick when another parallel console is available 
                PredicateExpression parallelSteps = new PredicateExpression();
                parallelSteps.Add(OrderRoutestephandlerFields.OrderRoutestephandlerId != step.OrderRoutestephandlerId);
                parallelSteps.Add(OrderRoutestephandlerFields.OrderId == step.OrderId);
                parallelSteps.Add(OrderRoutestephandlerFields.Number == step.Number);
                parallelSteps.Add(OrderRoutestephandlerFields.Status >= OrderRoutestephandlerStatus.RetrievedByHandler);
                orderRoutestepLookup.GetMulti(parallelSteps, path);

                if (orderRoutestepLookup.Count > 0)
                {
                    // Check if the Terminal of the parallel step is online or that step is already being handled
                    bool parallelTerminalOnline = orderRoutestepLookup.Any(o => o.TerminalId.HasValue && o.TerminalEntity.DeviceEntity.LastRequestUTC > DateTime.UtcNow.AddMilliseconds(ObymobiIntervals.SUPPORT_NOTIFICATIONS_TERMINAL_OFFLINE_THRESHOLD * -1));
                    if (parallelTerminalOnline)
                    {
                        continue;
                    }
                }

                // The orderRoutestepLookup just retrieved above contains parallel steps, but not the step itself, so add that step and notify for all:
                orderRoutestepLookup.Add(step);

                string terminalNames = "";
                string terminalLastRequestTimes = "";
                string debugText = "";
                foreach (OrderRoutestephandlerEntity stepToNotifyAbout in orderRoutestepLookup)
                {
                    TerminalEntity terminal = stepToNotifyAbout.TerminalEntity;
                    string unitName = "Terminal";
                    if (terminal.TerminalType == (int)TerminalType.Console)
                    {
                        unitName = "Console";
                    }
                    else if (terminal.TerminalType == (int)TerminalType.OnSiteServer)
                    {
                        unitName = "OnSiteServer";
                    }

                    terminalNames = StringUtil.CombineWithCommaSpace(terminalNames, unitName + terminal.Name);
                    terminalLastRequestTimes = StringUtil.CombineWithCommaSpace(terminalLastRequestTimes, StringUtil.FormatSafe("{0}: {1}", terminal.Name, terminal.DeviceEntity.LastRequestUTC.ToString()));

                    debugText += "{0} '{1}' offline since: {2}, ".FormatSafe(unitName, terminal.Name, terminal.DeviceEntity.LastRequestUTC ?? DateTime.MinValue);
                }

                string message;
                if (orderRoutestepLookup.Count > 1)
                {
                    message = "Order '{0}' might not get processed, handling terminals are offline: '{1}' of Company '{2}'. Step: '{3}'";
                    message = message.FormatSafe(step.OrderId, terminalNames, step.TerminalEntity.CompanyEntity.Name, step.Number);
                }
                else
                {
                    message = "Order '{0}' might not get processed, handling terminal is offline: '{1}' of Company '{2}'. Step: '{3}'";
                    message = message.FormatSafe(step.OrderId, terminalNames, step.TerminalEntity.CompanyEntity.Name, step.Number);
                }

                if (SendSupportNotification(step.TerminalEntity.CompanyEntity.SupportpoolEntity, message, null))
                {
                    foreach (OrderRoutestephandlerEntity stepToNotifyAbout in orderRoutestepLookup)
                    {
                        if (!stepToNotifyAbout.TerminalEntity.DeviceId.HasValue)
                            continue;

                        DeviceEntity deviceEntity = stepToNotifyAbout.TerminalEntity.DeviceEntity;

                        deviceEntity.LastRequestNotifiedBySmsUTC = deviceEntity.LastRequestUTC;
                        deviceEntity.Save();
                    }
                }
            }
        }

        private void SendSupportNotificationsForUnprocessableOrders_POSOnly()
        {
            PredicateExpression handlerTypeFilter = new PredicateExpression();
            handlerTypeFilter.AddWithOr(OrderRoutestephandlerFields.HandlerType == RoutestephandlerType.POS);
            handlerTypeFilter.AddWithOr(OrderRoutestephandlerFields.HandlerType == RoutestephandlerType.ExternalSystem);
            handlerTypeFilter.AddWithOr(OrderRoutestephandlerHistoryFields.HandlerType == RoutestephandlerType.POS);
            handlerTypeFilter.AddWithOr(OrderRoutestephandlerHistoryFields.HandlerType == RoutestephandlerType.ExternalSystem);

            PredicateExpression handlerStatusFilter = new PredicateExpression();
            handlerStatusFilter.AddWithOr(OrderRoutestephandlerFields.Status == OrderRoutestephandlerStatus.Failed);
            handlerStatusFilter.AddWithOr(OrderRoutestephandlerHistoryFields.Status == OrderRoutestephandlerStatus.Failed);

            PredicateExpression filter = new PredicateExpression();
            filter.Add(OrderFields.CreatedUTC > DateTime.UtcNow.AddHours(-1));
            filter.Add(OrderFields.ErrorSentBySMS == false);
            filter.Add(CompanyFields.UseMonitoring == true);
            filter.Add(handlerTypeFilter);
            filter.Add(handlerStatusFilter);

            RelationCollection relation = new RelationCollection();
            relation.Add(OrderEntity.Relations.CompanyEntityUsingCompanyId);
            relation.Add(OrderEntity.Relations.OrderRoutestephandlerEntityUsingOrderId, JoinHint.Left);
            relation.Add(OrderEntity.Relations.OrderRoutestephandlerHistoryEntityUsingOrderId, JoinHint.Left);

            PrefetchPath path = new PrefetchPath(EntityType.OrderEntity);
            path.Add(OrderEntityBase.PrefetchPathOrderRoutestephandlerCollection, new IncludeFieldsList(OrderRoutestephandlerFields.Status, OrderRoutestephandlerFields.ErrorText));
            path.Add(OrderEntityBase.PrefetchPathOrderRoutestephandlerHistoryCollection, new IncludeFieldsList(OrderRoutestephandlerHistoryFields.Status, OrderRoutestephandlerHistoryFields.ErrorText));
            path.Add(OrderEntityBase.PrefetchPathCompanyEntity)
                .SubPath.Add(CompanyEntityBase.PrefetchPathSupportpoolEntity);

            OrderCollection orders = new OrderCollection();
            orders.GetMulti(filter, 0, null, relation, path);

            foreach (OrderEntity order in orders)
            {
                string reason = order.ErrorText;

                OrderRoutestephandlerEntity failedHandler = order.OrderRoutestephandlerCollection.FirstOrDefault(handler => handler.Status == (int)OrderRoutestephandlerStatus.Failed);
                if (failedHandler != null)
                {
                    reason = failedHandler.ErrorText;
                }

                OrderRoutestephandlerHistoryEntity failedHandlerHistory = order.OrderRoutestephandlerHistoryCollection.FirstOrDefault(handler => handler.Status == (int)OrderRoutestephandlerStatus.Failed);
                if (failedHandlerHistory != null)
                {
                    reason = failedHandlerHistory.ErrorText;
                }

                string message = $"[EXTERNAL FAIL] Order (ID: {order.OrderId}) unprocessable for company: '{order.CompanyEntity.Name}', room: '{order.DeliverypointNumber}', reason: '{reason}'";

                if (SendSupportNotification(order.CompanyEntity.SupportpoolEntity, message, SupportNotification.UnprocessableOrders))
                {
                    order.ErrorSentByEmail = true;
                    order.ErrorSentBySMS = true;
                    order.Save();
                }
            }
        }

        private void SendSupportNotificationsForExpiredSteps()
        {
            // OrderRoutestephandler
            // OrderRoutestephandler > Terminal
            // OrderRoutestephandler > Order
            // OrderRoutestephandler > Order > Company
            // OrderRoutestephandler > Order > Company > SupportPool
            // OrderRoutestephandler > Order > Orderitem
            // OrderRoutestephandler > Order > Orderitem > Product
            // OrderRoutestephandler > Order > Orderitem > Category
            // OrderRoutestephandler > Order > Orderitem > Category > Category > Category > Category > Category
            OrderRoutestephandlerCollection orderRoutesteps = new OrderRoutestephandlerCollection();
            PrefetchPath prefetchOrderRoutestephandler = new PrefetchPath(EntityType.OrderRoutestephandlerEntity);
            //IPrefetchPathElement prefetchOrderRoutestephandlerTerminal = prefetchOrderRoutestephandler.Add(OrderRoutestephandlerEntityBase.PrefetchPathTerminalEntity);
            IPrefetchPathElement prefetchOrderRoutestephandlerOrder = prefetchOrderRoutestephandler.Add(OrderRoutestephandlerEntityBase.PrefetchPathOrderEntity);
            IPrefetchPathElement prefetchOrderRoutestephandlerOrderCompany = prefetchOrderRoutestephandlerOrder.SubPath.Add(OrderEntityBase.PrefetchPathCompanyEntity);
            //IPrefetchPathElement prefetchOrderRoutestephandlerOrderCompanySupportPool = prefetchOrderRoutestephandlerOrderCompany.SubPath.Add(CompanyEntityBase.PrefetchPathSupportpoolEntity);
            IPrefetchPathElement prefetchOrderRoutestephandlerOrderOrderitem = prefetchOrderRoutestephandlerOrder.SubPath.Add(OrderEntityBase.PrefetchPathOrderitemCollection);
            //IPrefetchPathElement prefetchOrderRoutestephandlerOrderOrderitemProduct = prefetchOrderRoutestephandlerOrderOrderitem.SubPath.Add(OrderitemEntityBase.PrefetchPathProductEntity);
            IPrefetchPathElement prefetchOrderRoutestephandlerOrderOrderitemCategory = prefetchOrderRoutestephandlerOrderOrderitem.SubPath.Add(OrderitemEntityBase.PrefetchPathCategoryEntity);

            List<IPrefetchPathElement> categoryPaths = new List<IPrefetchPathElement>();
            for (int i = 0; i < 4; i++)
            {
                IPrefetchPathElement prefetchCategory = CategoryEntityBase.PrefetchPathParentCategoryEntity;
                categoryPaths.Add(prefetchCategory);
            }
            prefetchOrderRoutestephandlerOrderOrderitemCategory.SubPath.Add(categoryPaths[0]).SubPath.Add(categoryPaths[1]).SubPath.Add(categoryPaths[2]).SubPath.Add(categoryPaths[3]);

            PredicateExpression filter = new PredicateExpression();

            PredicateExpression filterPotentialRetrievalExpired = new PredicateExpression();
            filterPotentialRetrievalExpired.Add(OrderRoutestephandlerFields.Status == (int)OrderRoutestephandlerStatus.WaitingToBeRetrieved);
            filterPotentialRetrievalExpired.Add(OrderRoutestephandlerFields.RetrievalSupportNotificationTimeoutUTC != DBNull.Value);
            filterPotentialRetrievalExpired.Add(OrderRoutestephandlerFields.RetrievalSupportNotificationSent == false);
            filterPotentialRetrievalExpired.Add(CompanyFields.UseMonitoring == true);
            filter.Add(filterPotentialRetrievalExpired);

            PredicateExpression filterPotentialBeingHandledExpired = new PredicateExpression();
            filterPotentialBeingHandledExpired.Add(OrderRoutestephandlerFields.Status < (int)OrderRoutestephandlerStatus.BeingHandled);
            filterPotentialBeingHandledExpired.Add(OrderRoutestephandlerFields.BeingHandledSupportNotificationTimeoutUTC != DBNull.Value);
            filterPotentialBeingHandledExpired.Add(OrderRoutestephandlerFields.BeingHandledSupportNotificationSent == false);
            filterPotentialBeingHandledExpired.Add(CompanyFields.UseMonitoring == true);
            filter.AddWithOr(filterPotentialBeingHandledExpired);

            RelationCollection relations = new RelationCollection();
            relations.Add(OrderRoutestephandlerEntityBase.Relations.OrderEntityUsingOrderId);
            relations.Add(OrderEntityBase.Relations.CompanyEntityUsingCompanyId);

            orderRoutesteps.GetMulti(filter, 0, null, relations, prefetchOrderRoutestephandler);

            // We just assume there's not a real large amount of steps, so load them all for in memory lookups
            OrderRoutestephandlerCollection orderRoutestepLookup = new OrderRoutestephandlerCollection();

            // Not retrieved in time & no parallel step that did get it 
            List<OrderRoutestephandlerEntity> retrievalTimedOutSteps = new List<OrderRoutestephandlerEntity>();

            // Not being handled in time & no parallel step that dit handle it.
            List<OrderRoutestephandlerEntity> beingHandledTimedOutSteps = new List<OrderRoutestephandlerEntity>();

            foreach (OrderRoutestephandlerEntity step in orderRoutesteps)
            {
                // Retrieval check
                if (step.RetrievalSupportNotificationTimeoutUTC.HasValue && step.RetrievalSupportNotificationTimeoutUTC.Value < DateTime.UtcNow && step.Status == (int)OrderRoutestephandlerStatus.WaitingToBeRetrieved && step.RetrievalSupportNotificationSent == false)
                {
                    // Don't notify if there is a parallel handler that did receive it                
                    PredicateExpression parallelStepRetrieved = new PredicateExpression();
                    parallelStepRetrieved.Add(OrderRoutestephandlerFields.OrderRoutestephandlerId != step.OrderRoutestephandlerId);
                    parallelStepRetrieved.Add(OrderRoutestephandlerFields.OrderId == step.OrderId);
                    parallelStepRetrieved.Add(OrderRoutestephandlerFields.Number == step.Number);
                    parallelStepRetrieved.Add(OrderRoutestephandlerFields.Status >= OrderRoutestephandlerStatus.RetrievedByHandler);

                    if (orderRoutestepLookup.GetDbCount(parallelStepRetrieved) == 0)
                    {
                        if (ShouldSendNotificationForStep(step, SupportNotificationType.RetrievalTimeout))
                        {
                            // No parallel step, so need to notify
                            retrievalTimedOutSteps.Add(step);
                        }
                    }
                }

                // Being handled check
                if (step.BeingHandledSupportNotificationTimeoutUTC.HasValue && step.BeingHandledSupportNotificationTimeoutUTC.Value < DateTime.UtcNow && step.Status < (int)OrderRoutestephandlerStatus.BeingHandled && step.BeingHandledSupportNotificationSent == false)
                {
                    if (ShouldSendNotificationForStep(step, SupportNotificationType.OnTheCaseTimeout))
                    {
                        beingHandledTimedOutSteps.Add(step);
                    }
                }
            }

            // 'Retrieval' timed out for orderRoutestephandler
            if (retrievalTimedOutSteps.Any())
            {
                SendSupportNotificationsTextsSteps(retrievalTimedOutSteps.ToList(), orderRoutesteps, OrderRoutestephandlerFields.RetrievalSupportNotificationSent.Name, "Order '{0}' for Company '{1}' Room '{2}' is not being retrieved in time for step '{3}' by it's handler(s) '{4}'.", SupportNotification.NotRetrievedInTime);
            }

            // 'On the case' timed out for orderRoutestephandler
            if (beingHandledTimedOutSteps.Count > 0)
            {
                SendSupportNotificationsTextsSteps(beingHandledTimedOutSteps, orderRoutesteps, OrderRoutestephandlerFields.BeingHandledSupportNotificationSent.Name, "Order '{0}' for Company '{1}' Room '{2}' is not being handled in time for step '{3}' by it's handler(s) '{4}'.", SupportNotification.NotHandledInTime);
            }
        }

        private void SendSupportNotificationsTextsSteps(IEnumerable<OrderRoutestephandlerEntity> stepsToSendNotificationFor, OrderRoutestephandlerCollection allSteps, string fieldToSetAsSent, string message, SupportNotification supportNotification)
        {
            List<int> orderIds = new List<int>();
            foreach (OrderRoutestephandlerEntity step in stepsToSendNotificationFor)
            {
                if (!step.OrderEntity.CompanyEntity.UseMonitoring)
                {
                    continue;
                }
                if (step.TerminalEntity != null && !step.TerminalEntity.UseMonitoring && step.TerminalId.GetValueOrDefault() > 0)
                {
                    continue;
                }

                // Prevent multiple notifications for 1 orders
                if (!orderIds.Contains(step.OrderId))
                {
                    orderIds.Add(step.OrderId);

                    // Check if we have multiple handlers for this step                    
                    string handlers = string.Empty;
                    List<OrderRoutestephandlerEntity> parallelSteps = allSteps.Where(x => x.OrderId == step.OrderId && x.Number == step.Number).ToList();
                    foreach (OrderRoutestephandlerEntity parallelStep in parallelSteps)
                    {
                        switch (parallelStep.HandlerTypeAsEnum)
                        {
                            case RoutestephandlerType.Console:
                                handlers = StringUtil.CombineWithSeperator(", ", handlers, parallelStep.TerminalEntity.Name);
                                break;
                            case RoutestephandlerType.Printer:
                                handlers = StringUtil.CombineWithSeperator(", ", handlers, parallelStep.TerminalEntity.Name);
                                break;
                            case RoutestephandlerType.POS:
                                handlers = StringUtil.CombineWithSeperator(", ", handlers, parallelStep.TerminalEntity.Name);
                                break;
                            case RoutestephandlerType.EmailOrder:
                                handlers = StringUtil.CombineWithSeperator(", ", handlers, "Cloud:Mail");
                                break;
                            case RoutestephandlerType.SMS:
                                handlers = StringUtil.CombineWithSeperator(", ", handlers, "Cloud:Sms");
                                break;
                            default:
                                handlers = StringUtil.CombineWithSeperator(", ", handlers, "UNKNOWN");
                                break;
                        }
                    }

                    string formattedMessage = message.FormatSafe(step.OrderId, step.OrderEntity.CompanyEntity.Name, step.OrderEntity.DeliverypointName, step.Number, handlers);

                    LoggingProvider.Verbose(LoggingTypes.SentSupportNotifications, "SendSupportNotificationsTextsStep: OrderId: {0}, OrderRoutestepHandlerId: {1}", step.OrderId, step.OrderRoutestephandlerId);
                    LoggingProvider.Verbose(LoggingTypes.SentSupportNotifications, "Message: {0}", formattedMessage);

                    SupportpoolEntity supportpoolEntity = step.OrderEntity.CompanyEntity.SupportpoolEntity;
                    if (step.SupportpoolId.HasValue)
                    {
                        supportpoolEntity = step.SupportpoolEntity;
                        LoggingProvider.Verbose(LoggingTypes.SentSupportNotifications, "Supportpool (Step): {0}({1}) Step: {2}", supportpoolEntity.SupportpoolId, supportpoolEntity.Name, step.OrderRoutestephandlerId);
                    }
                    else
                    {
                        LoggingProvider.Verbose(LoggingTypes.SentSupportNotifications, "Supportpool (Company): {0}({1})", supportpoolEntity.SupportpoolId, supportpoolEntity.Name);
                    }

                    if (!SendSupportNotification(supportpoolEntity, formattedMessage, supportNotification))
                    {
                        LoggingProvider.Warning(LoggingTypes.SentSupportNotifications, "Send support notification failed");
                        //this.LogWarning("SendSupportNotificationsTextsSteps", "Send Support notification failed");
                    }

                    foreach (OrderRoutestephandlerEntity parallelStep in parallelSteps)
                    {
                        Transaction t = new Transaction(IsolationLevel.ReadCommitted, "SupportNotificationHelper" + parallelStep.OrderRoutestephandlerId);
                        try
                        {
                            parallelStep.AddToTransaction(t);
                            parallelStep.SetNewFieldValue(fieldToSetAsSent, true);
                            parallelStep.Save();
                            t.Commit();
                        }
                        catch
                        {
                            t.Rollback();
                        }
                        finally
                        {
                            t.Dispose();
                        }
                    }
                }
            }
        }

        private void SendSupportNotificationsForBouncedEmails()
        {
            try
            {
                string json = GetBouncedEmailsInJson();
                if (!json.IsNullOrWhiteSpace())
                {
                    DateTime dateTimeRange = DateTime.Now.AddMinutes(-SupportNotificationPoller.BOUNCED_EMAILS_CHECK_MINUTES);

                    List<Bounce> todaysEmailBounces = JsonConvert.DeserializeObject<List<Bounce>>(json);

                    List<Bounce> bouncesInRange = todaysEmailBounces.Where(b => b.CreatedAsDateTime >= dateTimeRange).ToList();
                    if (bouncesInRange.Count > 0)
                    {
                        LogInfo("SendSupportNotificationsForBouncedEmails", "Bounces in range: {0}", todaysEmailBounces.Count);

                        foreach (Bounce bounce in bouncesInRange)
                        {
                            SendNotificationForBouncedEmail(bounce);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LogError("SendSupportNotificationsForBouncedEmails", "Something went wrong trying to send a support notification for bounced emails: {0}", ex.Message);
                LogError("SendSupportNotificationsForBouncedEmails", ex.StackTrace);
            }
        }

        private string GetBouncedEmailsInJson()
        {
            string apiKey = System.Configuration.ConfigurationManager.AppSettings[DionysosConfigurationConstants.SendGridApiKey];

            DateTime startDate = DateTime.UtcNow.AddDays(-1);

            string url = string.Format(ObymobiConstants.SENDGRID_BOUNCED_EMAILS_URL, startDate.ToUnixTime());

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.PreAuthenticate = true;
            request.Headers.Add(HttpRequestHeader.Authorization, "Bearer " + apiKey);
            request.Accept = "application/json";
            request.Method = "GET";

            string bouncesInJson;
            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            using (StreamReader reader = new StreamReader(response.GetResponseStream()))
            {
                bouncesInJson = reader.ReadToEnd();
            }

            return bouncesInJson;
        }

        private void SendNotificationForBouncedEmail(Bounce bounce)
        {
            if (bounce.Email.IsNullOrWhiteSpace())
                return;

            RoutestephandlerEntity routestephandler = RoutestephandlerHelper.GetRoutestephandlerByEmail(bounce.Email);
            if (routestephandler != null && routestephandler.ParentCompanyId > 0)
            {
                CompanyEntity companyWithSupportpool = CompanyHelper.GetCompanyWithSupportpool(routestephandler.ParentCompanyId);
                if (companyWithSupportpool != null && companyWithSupportpool.SupportpoolEntity != null)
                {
                    string message = string.Format("Bounced Emailaddress '{0}' for Company '{1}' and Route '{2}'. Reason: {3}",
                        bounce.Email, companyWithSupportpool.Name, routestephandler.RoutestepEntity.RouteEntity.Name, bounce.Reason);

                    if (!SendSupportNotification(companyWithSupportpool.SupportpoolEntity, message, SupportNotification.BouncedEmails))
                    {
                        LogWarning("SendSupportNotificationsForBouncedEmails", "Send Support notification failed");
                    }
                }
            }
        }

        private DateTime lastSupportNotificationSendingFailedReport = DateTime.MinValue;
        private DateTime lastSupportNotificationSendingFailedNotified = DateTime.MinValue;

        public bool SendSupportNotification(SupportpoolEntity supportpool, string message, SupportNotification? supportNotification)
        {
            bool toReturn = true;

            LoggingProvider.Verbose(LoggingTypes.SentSupportNotifications, "Send Support notification");
            LoggingProvider.Verbose(LoggingTypes.SentSupportNotifications, "  " + message);

            LogVerbose("SendSupportNotification", "Send Support notification");
            LogVerbose("SendSupportNotification", "  " + message);

            if (supportpool == null || supportpool.IsNew)
            {
                LogVerbose("SendSupportNotification", "  Supportpool is null or new, therefore no message is sent.");
            }
            else
            {
                List<string> phoneNumbers = Obymobi.Logic.HelperClasses.SupportpoolHelper.GetSupportpoolPhonenumbers(supportpool, false, supportNotification);
                List<string> emails = Obymobi.Logic.HelperClasses.SupportpoolHelper.GetSupportpoolEmailaddresses(supportpool, false, supportNotification);

                string joinedPhoneNumbers = string.Join(", ", phoneNumbers.ToArray());
                string joinedEmails = string.Join(", ", emails.ToArray());

                if (phoneNumbers.Count > 0)
                {
                    LogVerbose("SendSupportNotification", "  To Phones: {0}", joinedPhoneNumbers);
                }
                else
                {
                    LogVerbose("SendSupportNotification", "  To Phones: None");
                }

                if (emails.Count > 0)
                {
                    LogVerbose("SendSupportNotification", "  To Emails: {0}", joinedEmails);
                }
                else
                {
                    LogVerbose("SendSupportNotification", "  To Emails: None");
                }

                LoggingProvider.Verbose(LoggingTypes.SentSupportNotifications, "Message: {0}", message);
                LoggingProvider.Verbose(LoggingTypes.SentSupportNotifications, "Phonenumbers: {0}", joinedPhoneNumbers);
                LoggingProvider.Verbose(LoggingTypes.SentSupportNotifications, "Emails: {0}", joinedEmails);

                string errorReport = string.Empty;
                toReturn = notificationSendHelper.SendSupportNotification(phoneNumbers, emails, message, out errorReport);

                try
                {
                    if (errorReport.IsNullOrWhiteSpace())
                    {
                        LogVerbose("SendSupportNotification", "Succesfully Sent.");

                        string phonenumbers = string.Join(", ", phoneNumbers);
                        string emailaddresses = string.Join(", ", emails);

                        RequestlogEntity requestLog = new RequestlogEntity();
                        requestLog.SourceApplication = "ObymobiWebservice";
                        requestLog.MethodName = "DatabasePoller.SendSupportNotifications";
                        requestLog.ResultEnumValueName = "SupportNotificationSent";
                        requestLog.ResultEnumTypeName = "SupportNotificationSent";
                        requestLog.ErrorMessage = "Notification '{0}' sent to '{1}' and {2}".FormatSafe(message, phonenumbers, emailaddresses);
                        requestLog.ResultCode = "101";
                        requestLog.ResultMessage = "Success";
                        requestLog.Save();
                    }
                    else
                    {
                        LogWarning("SendSupportNotification", "Failed to Send: {0}", errorReport);

                        // At least one notification failed to send - Log to RequestLog (but maximum of once every 10 minutes - prevent filling of RequestLog)
                        if ((DateTime.Now - lastSupportNotificationSendingFailedReport).TotalMinutes > 10)
                        {
                            RequestlogEntity requestLog = new RequestlogEntity();
                            requestLog.SourceApplication = "ObymobiWebservice";
                            requestLog.MethodName = "DatabasePoller.SendSupportNotifications";
                            requestLog.ResultEnumValueName = "SupportNotificationFailed";
                            requestLog.ResultEnumTypeName = "SupportNotificationFailed";
                            requestLog.ErrorMessage = errorReport;
                            requestLog.ResultMessage = "Failed";
                            requestLog.ResultCode = "9999";
                            requestLog.Save();
                        }
                        lastSupportNotificationSendingFailedReport = DateTime.Now;

                        // Try to send a notification about the notifications failing (since one channel might still work) once very 4 hours
                        if ((DateTime.Now - lastSupportNotificationSendingFailedNotified).TotalHours > 4)
                        {
                            // Set this first, so we can't get in a loop if it fails again.
                            lastSupportNotificationSendingFailedNotified = DateTime.Now;

                            SendSupportNotification(supportpool, "Support Notifications might not arrive, please check requestlog.", supportNotification);
                        }
                    }
                }
                catch (Exception ex)
                {
                    // Saving to Requestlog failed (could be if the database fails)
                    LogError("SendSupportNotification", "Failed to record a sent support notification to the Requestlog with error: {0}", ex.Message);
                    LogError("SendSupportNotification", ex.StackTrace);
                }
            }

            return toReturn;
        }

        private void SendSlackMessage(SupportpoolEntity supportpool, string message, string color, SupportNotification? supportNotification = null, bool mentionChannel = false, params SlackAttachmentField[] attachmentFields)
        {
            string formattedMessage = message + "\n";

            List<string> slackUsernames = Obymobi.Logic.HelperClasses.SupportpoolHelper.GetSlackUsernames(supportpool, supportNotification);
            foreach (string slackUsername in slackUsernames)
            {
                formattedMessage += $"@{slackUsername} ";
            }

            if (mentionChannel)
            {
                formattedMessage += "@channel";
            }

            List<SlackAttachmentField> fields = new List<SlackAttachmentField>(attachmentFields);
            SlackMessage slackMessage = new SlackMessage
            {
                Attachments = new List<SlackAttachment>
                {
                    new SlackAttachment(message, fields, formattedMessage, null, color)
                }
            };

            if (!slackClients.GetSupportNotificationsSlackClient().SendMessage(slackMessage))
            {
                LogError("SendSlackMessage", "Failed to send message to Slack");
            }
        }

        private bool ShouldSendNotificationForStep(OrderRoutestephandlerEntity step, SupportNotificationType notificationType)
        {
            SupportNotificationType type = GetSupportNotificationTypeForOrder(step.OrderEntity);
            return (type == notificationType || type == SupportNotificationType.All);
        }

        private SupportNotificationType GetSupportNotificationTypeForOrder(OrderEntity order)
        {
            bool retrievalNotification = false;
            bool onTheCaseNotification = false;

            foreach (OrderitemEntity orderitem in order.OrderitemCollection)
            {
                SupportNotificationType supportNotificationType = GetSupportNotificationTypeForProduct(orderitem.ProductEntity, orderitem.CategoryEntity);
                if (supportNotificationType == SupportNotificationType.RetrievalTimeout)
                {
                    retrievalNotification = true;
                }
                else if (supportNotificationType == SupportNotificationType.OnTheCaseTimeout)
                {
                    onTheCaseNotification = true;
                }
                else if (supportNotificationType == SupportNotificationType.All)
                {
                    retrievalNotification = onTheCaseNotification = true;
                }

                if (retrievalNotification && onTheCaseNotification)
                {
                    break;
                }
            }

            if (retrievalNotification && onTheCaseNotification)
            {
                return SupportNotificationType.All;
            }
            if (retrievalNotification)
            {
                return SupportNotificationType.RetrievalTimeout;
            }
            if (onTheCaseNotification)
            {
                return SupportNotificationType.OnTheCaseTimeout;
            }
            return SupportNotificationType.None;
        }

        private SupportNotificationType GetSupportNotificationTypeForProduct(ProductEntity product, CategoryEntity category)
        {
            int type;

            if (product.SupportNotificationType > 0)
            {
                type = (int)product.SupportNotificationType;
            }
            else
            {
                type = GetSupportNotificationTypeFromParent(category);
            }

            return type.ToEnum<SupportNotificationType>();
        }

        private int GetSupportNotificationTypeFromParent(CategoryEntity category)
        {
            int type = (int)SupportNotificationType.All;

            if (category != null)
            {
                if (category.SupportNotificationType > 0)
                {
                    type = (int)category.SupportNotificationType;
                }
                else if (category.ParentCategoryId.HasValue)
                {
                    type = GetSupportNotificationTypeFromParent(category.ParentCategoryEntity);
                }
            }

            return type;
        }
    }
}
