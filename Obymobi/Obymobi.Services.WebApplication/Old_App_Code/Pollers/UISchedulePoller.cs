﻿using System;
using System.Linq;
using DevExpress.XtraScheduler;
using Dionysos.Logging;
using Obymobi.Constants;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Enums;
using Obymobi.Services.Logic.Converters;
using Obymobi.Services.Logic.Scheduling;
using Obymobi.Services.Logic.Scheduling.Handlers;
using Obymobi.Web.Pollers;
using Obymobi.Web.Slack.Interfaces;

namespace Pollers
{
    /// <summary>
    /// Summary description for UISchedulePoller
    /// </summary>
    public class UISchedulePoller : BasePoller
    {
        public override Enum LoggingType => LoggingTypes.UISchedulePoller;
        public override PollerType PollerType => PollerType.UISchedulePoller;
        public override IMultiLoggingProvider LoggingProvider => Global.LoggingProvider;
        public override int PrimaryPollerTimeoutSeconds => ObymobiIntervals.SERVICES_UISCHEDULEPOLLER_INTERVAL;
        protected override TimeSpan WorkInterval => TimeSpan.FromMilliseconds(ObymobiIntervals.SERVICES_UISCHEDULEPOLLER_INTERVAL);
        protected override TimeSpan FirstWorkInterval => WorkInterval;

        private readonly CompanyDataSource companyDataSource;
        private readonly UIScheduleItemOccurrenceDataSource uiScheduleItemOccurrenceDataSource;
        private readonly UIScheduleItemOccurrenceHandler handler;

        public UISchedulePoller(ISlackClient slackClient) : base(slackClient)
        {
            companyDataSource = new CompanyDataSource();
            uiScheduleItemOccurrenceDataSource = new UIScheduleItemOccurrenceDataSource();
            handler = new UIScheduleItemOccurrenceHandler();
        }

        protected override void ExecuteWork(bool force = false)
        {
            CompanyCollection companiesWithTimeZone = companyDataSource.FetchCompaniesWithTimeZones();
            UIScheduleItemOccurrenceCollection allOccurrences = uiScheduleItemOccurrenceDataSource.FetchOccurrences(DateTime.UtcNow);

            CompanyTimeZoneConverter timeZoneConverter = new CompanyTimeZoneConverter(companiesWithTimeZone);
            UIScheduleItemOccurrenceCollection triggeredOccurrences = FilterOnNewlyTriggeredOccurrences(allOccurrences, timeZoneConverter);

            LogVerbose("ExecuteWork", $"Checked {allOccurrences.Count} occurrences. {triggeredOccurrences.Count} new occurrences to handle.");

            if (triggeredOccurrences.Any())
            {
                HandleOccurrences(triggeredOccurrences);
                LogVerbose("ExecuteWork", $"Handled {triggeredOccurrences.Count} occurrences.");
            }
        }

        private UIScheduleItemOccurrenceCollection FilterOnNewlyTriggeredOccurrences(UIScheduleItemOccurrenceCollection occurrences, CompanyTimeZoneConverter timeZoneConverter)
        {
            UIScheduleItemOccurrenceCollection triggered = new UIScheduleItemOccurrenceCollection();

            foreach (UIScheduleItemOccurrenceEntity occurrence in occurrences)
            {
                DateTime companyNow = timeZoneConverter
                    .UtcToLocalTime(DateTime.UtcNow, occurrence.ParentCompanyId);

                Appointment appointment = new AppointmentConverter(timeZoneConverter)
                    .CreateAppointment(occurrence);

                Appointment activeAppointment = new AppointmentCalculator()
                    .GetActiveAppointment(appointment, companyNow);

                if (activeAppointment == null)
                {
                    continue;
                }

                if (IsAppointmentAlreadyHandled(occurrence, activeAppointment))
                {
                    continue;
                }

                occurrence.LastTriggeredUTC = activeAppointment.Start;
                triggered.Add(occurrence);
            }

            return triggered;
        }

        private static bool IsAppointmentAlreadyHandled(UIScheduleItemOccurrenceEntity occurrence, Appointment appointment)
            => occurrence.LastTriggeredUTC.HasValue && occurrence.LastTriggeredUTC.Value == appointment.Start;

        private void HandleOccurrences(UIScheduleItemOccurrenceCollection occurrences)
        {
            foreach (UIScheduleItemOccurrenceEntity occurrence in occurrences)
            {
                handler.Handle(occurrence);
                occurrence.Save();
            }
        }
    }
}