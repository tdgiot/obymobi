﻿using System;
using Dionysos.Logging;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using Obymobi.Web.Pollers;
using Obymobi.Web.Slack.Interfaces;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Pollers
{
    public class CleanLogPoller : BasePoller
    {
        private const int REQUEST_LOG_CLEANUP_DAYS = 7;
        private const int CLIENT_LOG_CLEANUP_DAYS = 30;
        private const int TERMINAL_LOG_CLEANUP_DAYS = 30;

        private DateTime nextCleanup = DateTime.Today; // Midnight

        public CleanLogPoller(ISlackClient slackClient) : base(slackClient)
        {
        }

        public override Enum LoggingType => LoggingTypes.CleanLogPoller;

        public override PollerType PollerType => PollerType.CleanLogPoller;

        public override IMultiLoggingProvider LoggingProvider => Global.LoggingProvider;

        public override int PrimaryPollerTimeoutSeconds => 300; // 5 minutes

        protected override TimeSpan WorkInterval => new TimeSpan(0, 1, 0, 0);

        protected override TimeSpan FirstWorkInterval => new TimeSpan(0, 1, 0, 0);

        protected override void ExecuteWork(bool force = false)
        {
            LogInfo("ExecuteWork", "Starting clean up");

            CleanupRequestLogs(REQUEST_LOG_CLEANUP_DAYS);
            CleanupClientLogs(CLIENT_LOG_CLEANUP_DAYS);
            CleanupTerminalLogs(TERMINAL_LOG_CLEANUP_DAYS);

            LogInfo("ExecuteWork", "Completed clean up. Next cleanup scheduled at: {0}", nextCleanup.ToString("dd/MM/yyyy HH:mm:ss"));
        }

        private void CleanupRequestLogs(int numberOfDays)
        {
            LogInfo("CleanupRequestLogs", $"Cleaning request logs older than {numberOfDays} days ago");

            IEntityCollection requestLogCollection = new RequestlogCollection();
            IPredicate filter = new PredicateExpression(RequestlogFields.CreatedUTC < DateTime.UtcNow.AddDays(-numberOfDays));

            DeleteLogs(requestLogCollection, filter);
        }

        private void CleanupClientLogs(int numberOfDays)
        {
            LogInfo("CleanupClientLogs", $"Cleaning client logs older than {numberOfDays} days ago");

            IEntityCollection clientLogCollection = new ClientLogCollection();
            IPredicate filter = new PredicateExpression(ClientLogFields.CreatedUTC < DateTime.UtcNow.AddDays(-numberOfDays));

            DeleteLogs(clientLogCollection, filter);
        }

        private void CleanupTerminalLogs(int numberOfDays)
        {
            LogInfo("CleanupTerminalLogs", $"Cleaning terminal logs older than {numberOfDays} days ago");

            IEntityCollection terminalLogCollection = new TerminalLogCollection();
            IPredicate filter = new PredicateExpression(TerminalLogFields.CreatedUTC < DateTime.UtcNow.AddDays(-numberOfDays));

            DeleteLogs(terminalLogCollection, filter);
        }

        private void DeleteLogs(IEntityCollection entityCollection, IPredicate filter)
        {
            try
            {
                entityCollection.DeleteMulti(filter);
            }
            catch (Exception ex)
            {
                LogError(nameof(DeleteLogs), $"Exception occurred while deleting logs. (Message={ex.Message}, StackTrace={ex.StackTrace})");
                SendNotificationOnSlack(ex);
            }
        }
    }
}