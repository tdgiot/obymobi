using Dionysos.Logging;
using Obymobi.Constants;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using Obymobi.Logic.HelperClasses;
using Obymobi.Logic.ScheduledCommands;
using Obymobi.Web.Pollers;
using Obymobi.Web.Slack.Interfaces;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;

namespace Pollers
{
    public class ScheduledCommandTaskPoller : BasePoller
    {
        #region Fields

        private readonly Dictionary<int, CommandSchedulerBase> RunningSchedulers = new Dictionary<int, CommandSchedulerBase>();

        #endregion

        public ScheduledCommandTaskPoller(ISlackClient slackClient) : base(slackClient)
        {
        }

        #region Base Overrides

        public override Enum LoggingType => LoggingTypes.ScheduledCommandTaskPoller;
        public override PollerType PollerType => PollerType.ScheduledCommandTaskPoller;
        public override IMultiLoggingProvider LoggingProvider => Global.LoggingProvider;
        public override int PrimaryPollerTimeoutSeconds => 60;
        protected override TimeSpan WorkInterval => TimeSpan.FromMilliseconds(ObymobiIntervals.SERVICES_SCHEDULEDCOMMANDTASKPOLLER_INTERVAL);
        protected override TimeSpan FirstWorkInterval => WorkInterval;

        #endregion

        protected override void ExecuteWork(bool force = false)
        {
            this.CheckForRunningSchedulers();
            this.CheckForScheduledCommands();
        }

        public void CheckForScheduledCommands()
        {
            // Get the scheduled command tasks which are:
            // Active and with status 'Started' or 'Pending'
            ScheduledCommandTaskCollection scheduledCommandTaskCollection = ScheduledCommandTaskHelper.GetActiveScheduledCommandTasks();

            this.LogVerbose("ScheduledCommands", "Active scheduled command tasks: {0}.", scheduledCommandTaskCollection.Count);

            // Walk through the scheduled command tasks
            foreach (ScheduledCommandTaskEntity scheduledCommandTaskEntity in scheduledCommandTaskCollection)
            {
                if (scheduledCommandTaskEntity.ExpiresUTC < DateTime.UtcNow)
                {
                    this.LogWarning("ScheduledCommands", "Scheduled command task with ID '{0}' is expired.", scheduledCommandTaskEntity.ScheduledCommandTaskId);

                    scheduledCommandTaskEntity.Status = ScheduledCommandTaskStatus.Expired;
                    scheduledCommandTaskEntity.CompletedUTC = DateTime.UtcNow;
                    scheduledCommandTaskEntity.Active = false;

                    // The validator sets the status to inactive after disabling the SCT.. so we don't want to use the validator.
                    scheduledCommandTaskEntity.Validator = null;
                    scheduledCommandTaskEntity.Save();

                    ScheduledCommandTaskHelper.SendEmail(scheduledCommandTaskEntity);

                    this.LogVerbose("ScheduledCommands", "Removing running schedulers from scheduled command task with ID '{0}'.", scheduledCommandTaskEntity.ScheduledCommandTaskId);

                    ConcurrentStack<int> keyStack = new ConcurrentStack<int>(this.RunningSchedulers.Keys);
                    while (true)
                    {
                        // Split keys into multiple batched. It's possible to get above 2100, which SQL server doesn't like.
                        int maxSize = keyStack.Count > 2000 ? 2000 : keyStack.Count;
                        if (maxSize == 0)
                        {
                            break;
                        }

                        int[] keys = new int[maxSize];
                        keyStack.TryPopRange(keys);

                        // Remove the schedulers from the running schedulers
                        PredicateExpression filter = new PredicateExpression();
                        filter.Add(ScheduledCommandFields.ScheduledCommandTaskId == scheduledCommandTaskEntity.ScheduledCommandTaskId);
                        filter.Add(ScheduledCommandFields.ScheduledCommandId == keys);

                        ScheduledCommandCollection scheduledCommandCollection = new ScheduledCommandCollection();
                        scheduledCommandCollection.GetMulti(filter);

                        foreach (ScheduledCommandEntity scheduledCommandEntity in scheduledCommandCollection)
                        {
                            if (RunningSchedulers.ContainsKey(scheduledCommandEntity.ScheduledCommandId))
                                RunningSchedulers.Remove(scheduledCommandEntity.ScheduledCommandId);
                        }
                    }
                }
                else
                {
                    this.LogVerbose("ScheduledCommands", "Scheduled command task with ID '{0}' is still active. Retrieving commands for task.", scheduledCommandTaskEntity.ScheduledCommandTaskId);

                    // Get the scheduled commands for this task 
                    PredicateExpression filter = new PredicateExpression();
                    filter.Add(ScheduledCommandFields.ScheduledCommandTaskId == scheduledCommandTaskEntity.ScheduledCommandTaskId);
                    filter.Add(ScheduledCommandFields.Status == ScheduledCommandStatus.Started);

                    ScheduledCommandCollection scheduledCommandCollection = new ScheduledCommandCollection();
                    scheduledCommandCollection.GetMulti(filter);

                    int maxSlotCount = scheduledCommandTaskEntity.CompanyEntity.MaxDownloadConnections;

                    if (scheduledCommandCollection.Count == maxSlotCount)
                    {
                        // There are currently no free slots to execute commands
                        this.LogVerbose("ScheduledCommands", "Scheduled command task with ID '{0}' has no more free slots left. {1} slots are currently in use.", scheduledCommandTaskEntity.ScheduledCommandTaskId, maxSlotCount);
                    }
                    else if (scheduledCommandCollection.Count < maxSlotCount)
                    {
                        // Get the amount of free slots
                        int freeSlotCount = maxSlotCount - scheduledCommandCollection.Count;

                        this.LogVerbose("ScheduledCommands", "Scheduled command task with ID '{0}' has currently {1} free slots left. Executing scheduled commands.", scheduledCommandTaskEntity.ScheduledCommandTaskId, freeSlotCount);

                        // Execute the scheduled commands for the amount of free slots
                        ExecuteScheduledCommands(scheduledCommandTaskEntity, freeSlotCount);
                    }
                    else
                    {
                        // There are some slots used than allowed!!
                        this.LogVerbose("ScheduledCommands", "Scheduled command task with ID '{0}' has currently {1} slots in use, which is more than allowed!!", scheduledCommandTaskEntity.ScheduledCommandTaskId, scheduledCommandCollection.Count);
                    }
                }
            }
        }

        public void CheckForRunningSchedulers()
        {
            this.LogVerbose("ScheduledCommands", "NetmessagePoller - Running schedulers: {0}", this.RunningSchedulers.Count);

            if (this.RunningSchedulers.Count == 0) return;

            var temp = new Dictionary<int, CommandSchedulerBase>(this.RunningSchedulers);
            foreach (CommandSchedulerBase scheduler in temp.Values)
            {
                try
                {
                    scheduler.ScheduledCommand.Refetch();

                    if (scheduler.ScheduledCommand.Status != ScheduledCommandStatus.Started)
                    {
                        continue;
                    }

                    if (scheduler.HasCommandExecutedSuccessfully())
                    {
                        this.LogVerbose("ScheduledCommands", "Scheduled command with ID '{0}' has been executed successfully.", scheduler.ScheduledCommand.ScheduledCommandId);

                        // Set the status of the command to 'Succeeded'
                        scheduler.ScheduledCommand.Status = ScheduledCommandStatus.Succeeded;
                        scheduler.ScheduledCommand.CompletedUTC = DateTime.UtcNow;
                        scheduler.ScheduledCommand.Save();

                        // Update the status of the scheduled command task
                        UpdateScheduledCommandTaskStatus(scheduler);

                        if (this.RunningSchedulers.ContainsKey(scheduler.ScheduledCommand.ScheduledCommandId))
                        {
                            this.LogVerbose("ScheduledCommands", "Scheduled command with ID '{0}' removed from schedulers, finished.", scheduler.ScheduledCommand.ScheduledCommandId);

                            // Remove the scheduler from the running schedulers
                            this.RunningSchedulers.Remove(scheduler.ScheduledCommand.ScheduledCommandId);
                        }
                    }
                    else if (scheduler.ScheduledCommand.ExpiresUTC.HasValue && scheduler.ScheduledCommand.ExpiresUTC.Value < DateTime.UtcNow)
                    {
                        this.LogVerbose("ScheduledCommands", "Scheduled command with ID '{0}' has been expired.", scheduler.ScheduledCommand.ScheduledCommandId);

                        // Set the status of the command to 'Expired'
                        scheduler.ScheduledCommand.Status = ScheduledCommandStatus.Expired;
                        scheduler.ScheduledCommand.CompletedUTC = DateTime.UtcNow;
                        scheduler.ScheduledCommand.Save();

                        // Update the status of the scheduled command task
                        UpdateScheduledCommandTaskStatus(scheduler);

                        if (this.RunningSchedulers.ContainsKey(scheduler.ScheduledCommand.ScheduledCommandId))
                        {
                            this.LogVerbose("ScheduledCommands", "Scheduled command with ID '{0}' removed from schedulers, expired.", scheduler.ScheduledCommand.ScheduledCommandId);

                            // Remove the scheduler from the running schedulers
                            this.RunningSchedulers.Remove(scheduler.ScheduledCommand.ScheduledCommandId);
                        }
                    }
                }
                catch (Exception ex)
                {
                    this.LogError("CheckForRunningSchedulers", "Exception: {0}", ex.Message);
                }
            }
        }

        private void ExecuteScheduledCommands(ScheduledCommandTaskEntity scheduledCommandTaskEntity, int freeSlotCount)
        {
            // Create the filter for the pending scheduled commands
            var filter = new PredicateExpression();
            filter.Add(ScheduledCommandFields.Status == ScheduledCommandStatus.Pending);
            filter.Add(ScheduledCommandFields.ScheduledCommandTaskId == scheduledCommandTaskEntity.ScheduledCommandTaskId);

            var sort = new SortExpression();
            sort.Add(new SortClause(ScheduledCommandFields.Sort, SortOperator.Ascending));

            // Apply the filter and the sort to the scheduled command view
            ScheduledCommandCollection scheduledCommandCollection = new ScheduledCommandCollection();
            scheduledCommandCollection.GetMulti(filter, 0, sort);

            this.LogVerbose("ScheduledCommands", "Executing max {0} commands for scheduled command task with ID '{1}'", freeSlotCount, scheduledCommandTaskEntity.ScheduledCommandTaskId);

            // Walk through the scheduled commands
            foreach (ScheduledCommandEntity scheduledCommandEntity in scheduledCommandCollection)
            {
                if (freeSlotCount == 0) break;

                CommandSchedulerBase commandScheduler = null;

                // Get the command scheduler for the specified client or terminal command
                if (scheduledCommandEntity.ClientCommand.HasValue)
                {
                    commandScheduler = CommandSchedulers.GetClientCommandScheduler(scheduledCommandEntity);
                }
                else if (scheduledCommandEntity.TerminalCommand.HasValue)
                {
                    commandScheduler = CommandSchedulers.GetTerminalCommandScheduler(scheduledCommandEntity);
                }

                // Check whether the command scheduler was found
                if (commandScheduler != null)
                {
                    if (commandScheduler.CanExecuteCommand() && commandScheduler.IsOnline)
                    {
                        this.LogVerbose("ScheduledCommands", "Command can be executed and device is online. Executing scheduled command with ID '{0}'.", commandScheduler.ScheduledCommand.ScheduledCommandId);

                        // Update the status for the scheduled command task
                        if (scheduledCommandTaskEntity.Status == ScheduledCommandTaskStatus.Pending)
                        {
                            scheduledCommandTaskEntity.Status = ScheduledCommandTaskStatus.Started;
                            scheduledCommandTaskEntity.Save();
                        }

                        // Execute the command
                        commandScheduler.ExecuteCommand();

                        // Decrement the free slot count
                        freeSlotCount--;
                    }

                    if (!this.RunningSchedulers.ContainsKey(scheduledCommandEntity.ScheduledCommandId))
                    {
                        // Add the scheduler to the running schedulers
                        this.RunningSchedulers.Add(scheduledCommandEntity.ScheduledCommandId, commandScheduler);
                    }
                }
                else
                {
                    this.LogVerbose("ScheduledCommands", "Failed to get scheduler for scheduled command ID: {0}", scheduledCommandEntity.ScheduledCommandId);
                }
            }
        }

        private void UpdateScheduledCommandTaskStatus(CommandSchedulerBase scheduler)
        {
            // Get the scheduled command task entity from the scheduler
            ScheduledCommandTaskEntity scheduledCommandTaskEntity = scheduler.ScheduledCommand.ScheduledCommandTaskEntity;

            // Create the subfilter for the statuses
            var subfilter = new PredicateExpression();
            subfilter.Add(ScheduledCommandFields.Status == ScheduledCommandStatus.Pending);
            subfilter.AddWithOr(ScheduledCommandFields.Status == ScheduledCommandStatus.Started);

            // Create the filter
            var filter = new PredicateExpression();
            filter.Add(ScheduledCommandFields.ScheduledCommandTaskId == scheduledCommandTaskEntity.ScheduledCommandTaskId);
            filter.Add(subfilter);

            // Check whether there still are scheduled commands with status 'Pending' or 'Started'
            var scheduledCommandCollection = new ScheduledCommandCollection();

            int scheduledCommandCount = scheduledCommandCollection.GetDbCount(filter);
            if (scheduledCommandCount == 0) // No more scheduled commands
            {
                this.LogVerbose("ScheduledCommands", "Scheduled command task with ID '{0}' has no more scheduled commands.", scheduledCommandTaskEntity.ScheduledCommandTaskId);

                // Recreate the filter
                filter = new PredicateExpression();
                filter.Add(ScheduledCommandFields.ScheduledCommandTaskId == scheduledCommandTaskEntity.ScheduledCommandTaskId);
                int totalNumberOfScheduledCommands = new ScheduledCommandCollection().GetDbCount(filter);

                filter = new PredicateExpression();
                filter.Add(ScheduledCommandFields.ScheduledCommandTaskId == scheduledCommandTaskEntity.ScheduledCommandTaskId);
                filter.Add(ScheduledCommandFields.Status == ScheduledCommandStatus.Succeeded);
                int totalNumberOfSucceededScheduledCommands = new ScheduledCommandCollection().GetDbCount(filter);

                filter = new PredicateExpression();
                filter.Add(ScheduledCommandFields.ScheduledCommandTaskId == scheduledCommandTaskEntity.ScheduledCommandTaskId);
                PredicateExpression statusFilter = new PredicateExpression();
                statusFilter.Add(ScheduledCommandFields.Status == ScheduledCommandStatus.Failed);
                statusFilter.AddWithOr(ScheduledCommandFields.Status == ScheduledCommandStatus.Expired);
                filter.Add(statusFilter);
                int totalNumberOfFailedOrExpiredScheduledCommands = new ScheduledCommandCollection().GetDbCount(filter);

                // Check whether all of the items have succeeded
                if (totalNumberOfScheduledCommands == totalNumberOfSucceededScheduledCommands)
                {
                    this.LogVerbose("ScheduledCommands", "All of the commands for scheduled command task with ID '{0}' have succeeded. Updating status to 'Completed'.", scheduledCommandTaskEntity.ScheduledCommandTaskId);

                    scheduledCommandTaskEntity.Status = ScheduledCommandTaskStatus.Completed;
                    scheduledCommandTaskEntity.CompletedUTC = DateTime.UtcNow;
                    scheduledCommandTaskEntity.Active = false;

                    // The validator sets the status to inactive after disabling the SCT.. so we don't want to use the validator.
                    scheduledCommandTaskEntity.Validator = null;
                    scheduledCommandTaskEntity.Save();

                    ScheduledCommandTaskHelper.SendEmail(scheduledCommandTaskEntity);
                }
                else if (totalNumberOfFailedOrExpiredScheduledCommands > 0)
                {
                    this.LogVerbose("ScheduledCommands", "Some of the commands for scheduled command task with ID '{0}' have failed or have expired. Updating status to 'CompletedWithErrors'.", scheduledCommandTaskEntity.ScheduledCommandTaskId);

                    scheduledCommandTaskEntity.Status = ScheduledCommandTaskStatus.CompletedWithErrors;
                    scheduledCommandTaskEntity.CompletedUTC = DateTime.UtcNow;
                    scheduledCommandTaskEntity.Active = false;

                    // The validator sets the status to inactive after disabling the SCT.. so we don't want to use the validator.
                    scheduledCommandTaskEntity.Validator = null;
                    scheduledCommandTaskEntity.Save();

                    ScheduledCommandTaskHelper.SendEmail(scheduledCommandTaskEntity);
                }
            }
            else
            {
                this.LogVerbose("ScheduledCommands", "Scheduled command task with ID '{0}' still has {1} scheduled commands left.", scheduledCommandTaskEntity.ScheduledCommandTaskId, scheduledCommandCount);
            }
        }

        public void CleanExpiredCommands()
        {
            try
            {
                this.LogVerbose("CleanExpiredCommands", "Start");

                ScheduledCommandTaskCollection scheduledCommandTaskCollection = ScheduledCommandTaskHelper.GetActiveScheduledCommandTasks();

                this.LogVerbose("CleanExpiredCommands", "Active scheduled command tasks: {0}.", scheduledCommandTaskCollection.Count);

                foreach (ScheduledCommandTaskEntity scheduledCommandTaskEntity in scheduledCommandTaskCollection)
                {
                    if (scheduledCommandTaskEntity.ExpiresUTC < DateTime.UtcNow)
                    {
                        // Task is expired, dont care
                    }
                    else
                    {
                        PredicateExpression filter = new PredicateExpression();
                        filter.Add(ScheduledCommandFields.ScheduledCommandTaskId == scheduledCommandTaskEntity.ScheduledCommandTaskId);
                        filter.Add(ScheduledCommandFields.Status == ScheduledCommandStatus.Started);

                        ScheduledCommandCollection scheduledCommandCollection = new ScheduledCommandCollection();
                        scheduledCommandCollection.GetMulti(filter);

                        foreach (ScheduledCommandEntity scheduledCommandEntity in scheduledCommandCollection)
                        {
                            CommandSchedulerBase scheduler = null;

                            // Get the command scheduler for the specified client or terminal command
                            if (scheduledCommandEntity.ClientCommand.HasValue)
                            {
                                scheduler = CommandSchedulers.GetClientCommandScheduler(scheduledCommandEntity);
                            }
                            else if (scheduledCommandEntity.TerminalCommand.HasValue)
                            {
                                scheduler = CommandSchedulers.GetTerminalCommandScheduler(scheduledCommandEntity);
                            }

                            // Check whether the command scheduler was found
                            if (scheduler != null)
                            {
                                if (scheduler.HasCommandExecutedSuccessfully())
                                {
                                    this.LogVerbose("ScheduledCommands", "Scheduled command with ID '{0}' has been executed successfully.", scheduler.ScheduledCommand.ScheduledCommandId);

                                    // Set the status of the command to 'Succeeded'
                                    scheduler.ScheduledCommand.Status = ScheduledCommandStatus.Succeeded;
                                    scheduler.ScheduledCommand.CompletedUTC = DateTime.UtcNow;
                                    scheduler.ScheduledCommand.Save();

                                    // Update the status of the scheduled command task
                                    UpdateScheduledCommandTaskStatus(scheduler);

                                    if (this.RunningSchedulers.ContainsKey(scheduler.ScheduledCommand.ScheduledCommandId))
                                    {
                                        this.LogVerbose("ScheduledCommands", "Scheduled command with ID '{0}' removed from schedulers, finished.", scheduler.ScheduledCommand.ScheduledCommandId);

                                        // Remove the scheduler from the running schedulers
                                        this.RunningSchedulers.Remove(scheduler.ScheduledCommand.ScheduledCommandId);
                                    }
                                }
                                else if (scheduler.ScheduledCommand.ExpiresUTC.HasValue && scheduler.ScheduledCommand.ExpiresUTC.Value < DateTime.UtcNow)
                                {
                                    this.LogVerbose("ScheduledCommands", "Scheduled command with ID '{0}' has been expired.", scheduler.ScheduledCommand.ScheduledCommandId);

                                    // Set the status of the command to 'Expired'
                                    scheduler.ScheduledCommand.Status = ScheduledCommandStatus.Expired;
                                    scheduler.ScheduledCommand.CompletedUTC = DateTime.UtcNow;
                                    scheduler.ScheduledCommand.Save();

                                    // Update the status of the scheduled command task
                                    UpdateScheduledCommandTaskStatus(scheduler);

                                    if (this.RunningSchedulers.ContainsKey(scheduler.ScheduledCommand.ScheduledCommandId))
                                    {
                                        this.LogVerbose("ScheduledCommands", "Scheduled command with ID '{0}' removed from schedulers, expired.", scheduler.ScheduledCommand.ScheduledCommandId);

                                        // Remove the scheduler from the running schedulers
                                        this.RunningSchedulers.Remove(scheduler.ScheduledCommand.ScheduledCommandId);
                                    }
                                }
                                else
                                {
                                    if (!this.RunningSchedulers.ContainsKey(scheduledCommandEntity.ScheduledCommandId))
                                    {
                                        // Add the scheduler to the running schedulers
                                        this.RunningSchedulers.Add(scheduledCommandEntity.ScheduledCommandId, scheduler);
                                    }
                                }
                            }
                            else
                            {
                                this.LogVerbose("CleanExpiredCommands", "Failed to get scheduler for scheduled command ID: {0}", scheduledCommandEntity.ScheduledCommandId);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                this.LogVerbose("CleanExpiredCommands", "Exception: {0}", ex.Message);
            }
        }
    }
}