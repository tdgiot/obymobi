﻿using System;
using System.Data;
using System.Text;
using System.Threading;
using Dionysos;
using Dionysos.Logging;
using Obymobi.Data;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using Obymobi.Web.Pollers;
using Obymobi.Web.Slack.Interfaces;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Pollers
{
    /// <summary>
    /// Summary description for CleanScheduledCommandTaskPoller
    /// </summary>
    public class CleanScheduledCommandTaskPoller : BasePoller
    {
        private static DateTime nextCleanup;

        public CleanScheduledCommandTaskPoller(ISlackClient slackClient) : base(slackClient)
        {
        }

        #region Base Overrides

        public override Enum LoggingType
        {
            get { return LoggingTypes.CleanScheduledCommandTaskPoller; }
        }

        public override PollerType PollerType
        {
            get { return PollerType.CleanScheduledCommandTaskPoller; }
        }

        public override IMultiLoggingProvider LoggingProvider
        {
            get { return Global.LoggingProvider; }
        }

        public override int PrimaryPollerTimeoutSeconds
        {
            get { return 60000; }
        }

        /// <summary>
        /// NOT USED! This poller calculates its own <see cref="TimeTillNextWork"/> TimeSpan
        /// </summary>
        protected override TimeSpan WorkInterval
        {
            get { return new TimeSpan(1, 0, 0, 0); }
        }

        /// <summary>
        /// NOT USED! This poller calculates its own <see cref="TimeTillNextWork"/> TimeSpan
        /// </summary>
        protected override TimeSpan FirstWorkInterval
        {
            get { return new TimeSpan(1, 0, 0, 0); }
        }

        #endregion

        public override void Initialize(CancellationToken cancellationToken)
        {
            // By added hours to "Today", we can let this poller run each day at specific time
            CleanScheduledCommandTaskPoller.nextCleanup = DateTime.Today.AddHours(5);            

            SetNextCleanupTime();

            base.Initialize(cancellationToken);
        }

        public override TimeSpan TimeTillNextWork()
        {
            TimeSpan timeTillNextWork = CleanScheduledCommandTaskPoller.nextCleanup.Subtract(DateTime.Now);
            return timeTillNextWork;
        }

        protected override void ExecuteWork(bool force = false)
        {
            if (DateTime.Now < CleanScheduledCommandTaskPoller.nextCleanup)
                return;

            DoCleanup();
            SetNextCleanupTime();
        }

        private void DoCleanup()
        {
            DateTime fourteenDaysAgo = DateTime.UtcNow.AddDays(-7); // All SCT which expired more than 7 days ago

            PredicateExpression filter = new PredicateExpression();
            filter.Add(ScheduledCommandTaskFields.ExpiresUTC <= fourteenDaysAgo);

            ScheduledCommandTaskCollection scheduledCommandTasks = new ScheduledCommandTaskCollection();
            scheduledCommandTasks.GetMulti(filter);

            LogInfo("DoCleanup", "Found {0} SCT(s) for cleanup.", scheduledCommandTasks.Count);

            foreach (ScheduledCommandTaskEntity scheduledCommandTask in scheduledCommandTasks)
            {
                if (this.CancellationToken.IsCancellationRequested)
                {
                    LogInfo("DoCleanup", "Cancellation is requested, breaking cleanup loop.");
                    break;
                }

                string message = string.Format("ID: {0} | CompanyId: {1}", scheduledCommandTask.ScheduledCommandTaskId, scheduledCommandTask.CompanyId);

                try
                {
                    if (scheduledCommandTask.Delete())
                        LogVerbose("DoCleanup", "OK - {0}", message);
                }
                catch (Exception ex)
                {
                    LogError("DoCleanup", "FAILED - {0}", message);
                    LogError("DoCleanup", ex.ProcessStackTrace(true));
                }
            }
        }

        private void SetNextCleanupTime()
        {
            if (DateTime.Now >= CleanScheduledCommandTaskPoller.nextCleanup)
            {
                // Set next cleanup time to tomorrow
                CleanScheduledCommandTaskPoller.nextCleanup = CleanScheduledCommandTaskPoller.nextCleanup.AddDays(1);
            }

            LogInfo("SetLastCleanup", "Next clean up scheduled at: {0}", CleanScheduledCommandTaskPoller.nextCleanup.ToString("dd/MM/yyyy HH:mm:ss"));
        }
    }
}