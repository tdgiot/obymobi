﻿using System;
using System.Collections.Generic;
using System.Data;
using Dionysos;
using Obymobi.Constants;
using Obymobi.Data;
using Obymobi.Data.DaoClasses;
using Obymobi.Web.Pollers;
using System.Threading;
using Obymobi.Web.Slack.Interfaces;

namespace Pollers
{
    /// <summary>
    /// Summary description for PerformancePoller
    /// </summary>
    public class PerformancePoller : BasePoller
    {
        List<PerformanceCounter> performanceCounters;

        public PerformancePoller(ISlackClient slackClient) : base(slackClient)
        {
        }

        #region Base Overrides

        public override Obymobi.Enums.PollerType PollerType
        {
            get { return Obymobi.Enums.PollerType.PerformancePoller; }
        }

        public override Dionysos.Logging.IMultiLoggingProvider LoggingProvider
        {
            get { return Global.LoggingProvider; }
        }

        public override Enum LoggingType
        {
            get { return LoggingTypes.PerformancePoller; }
        }

        public override int PrimaryPollerTimeoutSeconds
        {
            get { return 60; }
        }

        protected override TimeSpan WorkInterval
        {
            get { return TimeSpan.FromMilliseconds(ObymobiIntervals.SERVICES_PERFORMANCEPOLLER_INTERVAL); }
        }

        protected override TimeSpan FirstWorkInterval
        {
            get { return WorkInterval; }
        }

        #endregion

        public override void Initialize(CancellationToken cancellationToken)
        {
            this.performanceCounters = new List<PerformanceCounter>();
            InitializePerformanceCounters();

            base.Initialize(cancellationToken);
        }

        private void InitializePerformanceCounters()
        {
            // Number of batch requests received per second, and is a good general indicator for the activity level of the SQL Server. This counter is highly 
            // dependent on the hardware and quality of code running on the server. The more powerful the hardware, the higher this number can be, even 
            // on poorly coded applications. A value of 1000 batch requests/sec is easily attainable though a typical 100Mbs NIC can only handle about 3000 
            // batch requests/sec.Many other counter thresholds depend upon batch requests/sec while, in some cases, a low (or high) number does not point to 
            // poor processing power. You should frequently use this counter in combination with other counters, such as processor utilization or user connections.
            this.performanceCounters.Add(new PerformanceCounter("SQLServer:SQL Statistics", "Batch Requests/sec", string.Empty, NewRelicConstants.NEWRELIC_SQLSERVER_BATCH_REQUESTS_SEC));

            // Number of times that SQL Server escalated locks from page- or row-level to table-level. This number should,
            // generally, be low. Frequent of even occasional spiking in this value may indicated poorly coded transactions.
            this.performanceCounters.Add(new PerformanceCounter("SQLServer:Access Methods", "Table Lock Escalations/sec", string.Empty, NewRelicConstants.NEWRELIC_SQLSERVER_LOCK_ESCALATION_SEC));

            // Number of new locks and locks converted per second. This metric's value should generally correspond to "Batch Request/sec". 
            // Values > 1000 may indicate queries are accessing very large numbers of rows and may benefit from tuning
            this.performanceCounters.Add(new PerformanceCounter("SQLServer:Locks", "Lock Requests/sec", "_Total", NewRelicConstants.NEWRELIC_SQLSERVER_LOCK_REQUESTS_SEC));
        
            // Shows the number of lock requests per second that timed out, including internal requests for NOWAIT locks. 
            // A value greater than zero might indicate that user queries are not completing. The lower this value is, the better.
            this.performanceCounters.Add(new PerformanceCounter("SQLServer:Locks", "Lock Timeouts/sec", "_Total", NewRelicConstants.NEWRELIC_SQLSERVER_LOCK_TIMEOUTS_SEC));

            // Percentage of space in the log that is in use. Since all work in an OLTP database stops until writes can occur to the transaction log, it's a
            // very good idea to ensure thta the log never fills completely. Hence, the recommendation to keep the log under 80% full.
            this.performanceCounters.Add(new PerformanceCounter("SQLServer:Databases", "Percent Log Used", DbUtils.GetCurrentCatalogName(), NewRelicConstants.NEWRELIC_SQLSERVER_DATABASES_PERCENTLOGUSED));
        }

        protected override void ExecuteWork(bool force = false)
        {
            IDbConnection conn = null;
            try
            {
                var dao = new TypedListDAO();
                conn = dao.CreateConnection(CommonDaoBase.ActualConnectionString);
                conn.Open();

                HarvestSqlServerPerformanceData(conn);
            }
            catch (Exception ex)
            {
                LogError("TryOpenDbConnection", "Failed to open connection: {0}", ex.Message);
            }
            finally
            {
                if (conn != null)
                {
                    conn.Close();
                    conn.Dispose();
                }
            }
        }

        private void HarvestSqlServerPerformanceData(IDbConnection conn)
        {
            LogVerbose("HarvestSqlServerPerformanceData", "Gathering data");

            try
            {
                foreach (PerformanceCounter counter in this.performanceCounters)
                {
                    counter.UpdateCounter(this, ref conn);

                    if (TestUtil.IsPcBattleStationDanny)
                    {
                        LogVerbose("HarvestSqlServerPerformanceData", "Counter: {0}/{1}, Value: {2}", counter.ObjectName, counter.CounterName, counter.LastValue);
                    }
                }
            }
            catch (Exception ex)
            {
                LogError("HarvestSqlServerPerformanceData", "Error while gathering data: {0}", ex.Message);
            }
        }

        #region Nested Class PerformanceCounter

        private class PerformanceCounter
        {
            public string ObjectName { get; private set; }
            public string CounterName { get; private set; }
            public long LastValue { get; private set; }

            private bool IsFirst { get; set; }
            private string InstanceName { get; set; }
            private string NewRelicMetric { get; set; }
        
            private PerformanceCounter()
            {
                this.LastValue = 0;
                this.IsFirst = true;
            }

            public PerformanceCounter(string objectName, string counterName, string instanceName, string newRelicMetric) : this()
            {
                this.ObjectName = objectName;
                this.CounterName = counterName;
                this.InstanceName = instanceName;
                this.NewRelicMetric = newRelicMetric;
            }

            public void UpdateCounter(PerformancePoller parent, ref IDbConnection conn)
            {
                long newValue = 0;
                try
                {
                    // Fetch current locks on database
                    IDbCommand command = conn.CreateCommand();
                    command.CommandText = string.Format("SELECT cntr_value FROM sys.dm_os_performance_counters WHERE object_name = '{0}' AND counter_name = '{1}' AND instance_name = '{2}'", this.ObjectName, this.CounterName, this.InstanceName);
                    command.CommandTimeout = 2; // Seconds
                    command.CommandType = CommandType.Text;

                    object result = command.ExecuteScalar();
                    if (result != null)
                    {
                        newValue = (long)result;
                    }
                    command.Dispose();
                }
                catch (Exception ex)
                {
                    parent.LogError("UpdateCounter", "Error while gathering counter ({0}/{1}): {2}", this.ObjectName, this.CounterName, ex.Message);
                }

                PushData(newValue);
            }

            private void PushData(long newValue)
            {
                if (newValue == 0)
                    return;

                if (this.IsFirst || this.NewRelicMetric.IsNullOrWhiteSpace())
                {
                    this.LastValue = newValue;
                    this.IsFirst = false;
                    return;
                }

                long diffValue = newValue - this.LastValue;
                this.LastValue = newValue;
            }
        }

        #endregion
    }
}