﻿using System;
using Obymobi.Constants;
using Obymobi.Enums;
using Obymobi.Logging;
using Obymobi.Logic.HelperClasses;
using Obymobi.Logic.Model;
using Obymobi.Web.Pollers;

/// <summary>
/// Summary description for BackendServiceHandler
/// </summary>
public static class BackendServiceHandler
{
    internal static long GetSecondsSinceLastPollerExecution(string pollerName, long timestamp, string hash)
    {
        long secondsSinceLastExecution = -1;
        try
        {
            WebserviceHelper.ValidateRequestNoc(timestamp, hash, pollerName);            
            PollerType pollerType;
            if (EnumUtil.TryParse(pollerName, out pollerType))
            {
                secondsSinceLastExecution = PollerManager.GetSecondsSinceLastExecution(pollerType);
            }
            else
                throw new Exception("Could not parse: " + pollerName);
        }
        catch(Exception ex)
        {
            secondsSinceLastExecution = -787; // Keep this, as it's used to
            Exceptionlogger.CreateExceptionlog(ex);            
        }

        return secondsSinceLastExecution;
    }

    internal static string[] GetSystemSupportpoolPhonenumbers(long timestamp, string hash)
    {
        string[] ret;
        try
        {
            WebserviceHelper.ValidateRequestNoc(timestamp, hash);

            ret = SupportpoolHelper.GetSystemSupportpoolPhonenumbers().ToArray();
        }
        catch (Exception ex)
        {
            Exceptionlogger.CreateExceptionlog(ex);
            ret = ObymobiConstants.SupportpoolFallbackPhonenumbers.ToArray();
        }

        return ret;
    }

    internal static string[] GetSystemSupportpoolEmailaddresses(long timestamp, string hash)
    {
        string[] ret;
        try
        {
            WebserviceHelper.ValidateRequestNoc(timestamp, hash);

            ret = SupportpoolHelper.GetSystemSupportpoolEmailaddresses().ToArray();
        }
        catch (Exception ex)
        {
            Exceptionlogger.CreateExceptionlog(ex);
            ret = ObymobiConstants.SupportpoolFallbackEmailaddresses.ToArray();
        }

        return ret;
    }

    internal static ServiceBackendWebserviceResult GetSystemSupportpoolInfo(long timestamp, string hash)
    {
        ServiceBackendWebserviceResult result = new ServiceBackendWebserviceResult(true);
        try
        {
            WebserviceHelper.ValidateRequestNoc(timestamp, hash);

            result.SupportpoolPhonenumbers = SupportpoolHelper.GetSystemSupportpoolPhonenumbers();
            result.SupportpoolEmailaddresses = SupportpoolHelper.GetSystemSupportpoolEmailaddresses();
        }
        catch (Exception ex)
        {
            Exceptionlogger.CreateExceptionlog(ex);

            result.Success = false;

            // Use fallback phonenumbers and emailaddresses when all else fails
            if (result.SupportpoolPhonenumbers == null || result.SupportpoolPhonenumbers.Count == 0)
            {
                result.SupportpoolPhonenumbers = ObymobiConstants.SupportpoolFallbackPhonenumbers;
            }

            if (result.SupportpoolEmailaddresses == null || result.SupportpoolEmailaddresses.Count == 0)
            {
                result.SupportpoolEmailaddresses = ObymobiConstants.SupportpoolFallbackEmailaddresses;
            }
        }

        return result;
    }
}