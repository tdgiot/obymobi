﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using Dionysos;
using Dionysos.Logging;
using Obymobi;
using Obymobi.Enums;
using Obymobi.Services.WebApplication;
using Obymobi.Web.Pollers;
using Obymobi.Web.Slack;
using Obymobi.Web.Slack.Clients;
using Obymobi.Web.Slack.Interfaces;
using Pollers;

/// <summary>
/// Woof, woof
/// </summary>
public class Watchdog
{

    private readonly HttpContext httpContext;
    private readonly IMultiLoggingProvider loggingProvider;

    private InfiniteLooper watchdogLooper;

    private readonly Dictionary<IPoller, DateTime> pollerList;
    private readonly Dictionary<PollerType, Task> runningPollers;

    private readonly CancellationTokenSource cancellationTokenSource;
    private readonly CancellationToken cancellationToken;

    private readonly ISlackClient slackClient;

    public Watchdog(HttpContext httpContext, IMultiLoggingProvider loggingProvider)
    {
        HttpContext.Current = this.httpContext = httpContext;
        this.loggingProvider = loggingProvider;

        this.cancellationTokenSource = new CancellationTokenSource();
        this.cancellationToken = this.cancellationTokenSource.Token;

        this.pollerList = new Dictionary<IPoller, DateTime>();
        this.runningPollers = new Dictionary<PollerType, Task>();

        ISlackClients slackClients = CreateSlackClients();

        this.slackClient = slackClients.GetDefaultSlackClient();

        Init(slackClients);
    }

    private void Init(ISlackClients slackClients)
    {
        loggingProvider.Verbose(LoggingTypes.Services, "Watchdog.Init - Initializing pollers");

        IMessageFactory messageFactory = CreateMessageFactory();

        AddPoller(new CloudTaskPoller(slackClient));
        AddPoller(new DatabasePoller(slackClient));
        AddPoller(new ExternalLinkCheckPoller(slackClient));
        AddPoller(new MediaTaskPoller(slackClient));
        AddPoller(new ReportingPoller(slackClient));
        AddPoller(new SupportNotificationPoller(slackClient, slackClients, messageFactory));
        AddPoller(new MessagePoller(slackClient));
        AddPoller(new CleanScheduledCommandTaskPoller(slackClient));
        AddPoller(new NetmessageCleaner(slackClient));
        AddPoller(new CleanScheduledMediaPoller(slackClient));
        AddPoller(new GameSessionReportPoller(slackClient));
        AddPoller(new ScheduledCommandTaskPoller(slackClient));
        AddPoller(new WeatherPoller(slackClient));
        AddPoller(new PmsTerminalStatusPoller(slackClient));
        AddPoller(new AnalyticsProcessingTaskPoller(slackClient));
        AddPoller(new DeviceTokenTaskPoller(slackClient));
        AddPoller(new UISchedulePoller(slackClient));
        AddPoller(new CleanLogPoller(slackClient));

        foreach (IPoller poller in pollerList.Keys)
        {
            // CancellationToken is given to each poller, so that when the app is stopped any long-running pollers can also be stopped (halfway)
            poller.Initialize(cancellationToken);
        }
    }

    private static IMessageFactory CreateMessageFactory() => new EnvironmentSpecificMessageFactory(WebEnvironmentHelper.CloudEnvironment);

    private static ISlackClients CreateSlackClients() => new SlackClients(new WebContext(), new ISlackClient[]
    {
        new ProductionChannelSlackClient(),
        new TestChannelSlackClient(),
        new SupportNotificationsSlackClient(),
        new DevSupportNotificationsSlackClient()
    });

    private void AddPoller(IPoller poller)
    {
        PollerManager.SetPoller(poller);

        this.pollerList.Add(poller, DateTime.MinValue);
    }

    public void Start()
    {
        loggingProvider.Verbose(LoggingTypes.Services, "Watchdog.Start");

        if (this.watchdogLooper != null)
        {
            loggingProvider.Warning(LoggingTypes.Services, "Watchdog.Start - Watchdog is already awake. Skipping.");
            return;
        }

        this.watchdogLooper = InfiniteLooper.StartNew(() => HttpContext.Current = httpContext,
                                                      Run, 1000, 5000, null, OnExceptionHandler);
    }

    public void Stop()
    {
        loggingProvider.Verbose(LoggingTypes.Services, "Watchdog.Stop");

        if (this.watchdogLooper == null || !this.watchdogLooper.IsRunning)
        {
            loggingProvider.Warning(LoggingTypes.Services, "Watchdog.Stop - Watchdog not awake or already put to sleep. Skipping.");
            return;
        }

        loggingProvider.Verbose(LoggingTypes.Services, "Watchdog.Stop - Calling cancel on cancellationToken");
        this.cancellationTokenSource.Cancel();

        loggingProvider.Verbose(LoggingTypes.Services, "Watchdog.Stop - Stopping main watchdog looper");
        this.watchdogLooper.Stop();

        loggingProvider.Verbose(LoggingTypes.Services, "Watchdog.Stop - Stopped");
    }

    private void OnExceptionHandler(Exception exception)
    {
        if (!this.cancellationToken.IsCancellationRequested)
        {
            loggingProvider.Error(LoggingTypes.Services, "Watchdog.OnExceptionHandler: {0}", exception.Message);
            loggingProvider.Error(LoggingTypes.Services, "Watchdog.OnExceptionHandler: {0}", exception.ProcessStackTrace(true));

            this.watchdogLooper.Stop();
            this.watchdogLooper.Start();
        }
        else
        {
            loggingProvider.Information(LoggingTypes.Services, "Watchdog.OnExceptionHandler - Exception was thrown but cancellation was requested. Ignoring.");
        }
    }

    private void Run()
    {
        if (this.cancellationTokenSource.IsCancellationRequested)
        {
            loggingProvider.Verbose(LoggingTypes.Services, "Watchdog.Run - Cancellation requested. Skipping Run.");
            return;
        }

        var dateTimeUtcNow = DateTime.UtcNow;
        var pollerListCopy = new Dictionary<IPoller, DateTime>(this.pollerList);

        foreach (var pollerEntry in pollerListCopy)
        {
            IPoller poller = pollerEntry.Key;

            var nextWork = poller.TimeTillNextWork();
            if (nextWork.TotalSeconds > 0)
                continue;

            lock (this.runningPollers)
            {
                if (this.runningPollers.ContainsKey(poller.PollerType))
                {
                    // Skip if poller is already running. Print warning every 5 seconds
                    if (dateTimeUtcNow.Subtract(pollerEntry.Value).TotalSeconds >= 5)
                    {
                        loggingProvider.Verbose(LoggingTypes.Services, "Watchdog.Run - Want to start '{0}', but it's still running. Skipping for now.", poller.PollerType);
                        this.pollerList[poller] = dateTimeUtcNow;
                    }
                    continue;
                }

                loggingProvider.Debug(LoggingTypes.Services, "Watchdog.Run - Start: '{0}'", poller.PollerType);

                IPoller runPoller = poller;
                var t = Task.Factory.StartNew(() =>
                                              {
                                                  // Copy http context
                                                  HttpContext.Current = this.httpContext;

                                                  // Do actual work
                                                  runPoller.DoWork();

                                                  // Report task is complete
                                                  PollerWorkCompleted(runPoller.PollerType);
                                              }, this.cancellationToken, TaskCreationOptions.LongRunning, TaskScheduler.Default);
                this.runningPollers.Add(poller.PollerType, t);
            }
        }
    }

    private void PollerWorkCompleted(PollerType pollerType)
    {
        lock (this.runningPollers)
        {
            loggingProvider.Debug(LoggingTypes.Services, "Watchdog.Run - Complete: '{0}'", pollerType);
            this.runningPollers.Remove(pollerType);
        }
    }
}
