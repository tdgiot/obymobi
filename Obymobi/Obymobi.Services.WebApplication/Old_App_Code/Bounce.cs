﻿using System;
using System.Globalization;
using Newtonsoft.Json;

[Serializable]
public class Bounce
{
    [JsonProperty]
    private string email;

    [JsonProperty]
    private string status;

    [JsonProperty]
    private string reason;

    [JsonProperty]
    private string created;

    [JsonIgnore]
    public string Email { get { return this.email; } }

    [JsonIgnore]
    public string Status { get { return this.status; } }

    [JsonIgnore]
    public string Reason { get { return this.reason; } }

    [JsonIgnore]
    public DateTime CreatedAsDateTime
    {
        get
        {
            return Bounce.StringToDateTime(this.created);
        }
    }

    private static DateTime StringToDateTime(string datetimeString)
    {
        const string datetimeFormat = "yyyy-MM-dd HH:mm:ss";

        DateTime toReturn;
        try
        {
            toReturn = DateTime.ParseExact(datetimeString, datetimeFormat, CultureInfo.InvariantCulture);
        }
        catch
        {
            toReturn = DateTime.MinValue;
        }

        return toReturn;
    }
}