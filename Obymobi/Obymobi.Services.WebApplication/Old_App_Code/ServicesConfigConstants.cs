﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dionysos.Configuration;

namespace Obymobi.Configuration
{
    public class ServicesConfigConstants : ConfigurationItemCollection
    {
        public const string FirstWarningDiskspaceGb = "FirstWarningDiskspaceGb";
        public const string SecondWarningDiskspaceGb = "SecondWarningDiskspaceGb";
        public const string ThirdWarningDiskspaceGb = "ThirdWarningDiskspaceGb";        
    }
}
