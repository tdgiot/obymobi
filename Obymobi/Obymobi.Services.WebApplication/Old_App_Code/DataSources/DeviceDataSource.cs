using Obymobi.Data;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System;

public class DeviceDataSource
{
    public int GetOfflineDevicesCountForMonitoredCompanies()
    {
        PredicateExpression filter = new PredicateExpression();
        filter.Add(CompanyFields.SystemType == SystemType.Crave);
        filter.Add(CompanyFields.UseMonitoring == true);
        filter.Add(DeviceFields.LastRequestUTC <= DateTime.UtcNow.AddMinutes(-15));
        filter.Add(DeviceFields.IsDevelopment == false);

        RelationCollection relations = new RelationCollection();
        relations.Add(DeviceEntity.Relations.ClientEntityUsingDeviceId);
        relations.Add(ClientEntity.Relations.CompanyEntityUsingCompanyId);

        return new DeviceCollection().GetDbCount(filter, relations);
    }

    public int GetTotalDevicesCountForMonitoredCompanies()
    {
        PredicateExpression filter = new PredicateExpression();
        filter.Add(CompanyFields.SystemType == SystemType.Crave);
        filter.Add(CompanyFields.UseMonitoring == true);
        filter.Add(DeviceFields.IsDevelopment == false);

        RelationCollection relations = new RelationCollection();
        relations.Add(DeviceEntity.Relations.ClientEntityUsingDeviceId);
        relations.Add(ClientEntity.Relations.CompanyEntityUsingCompanyId);

        return new DeviceCollection().GetDbCount(filter, relations);
    }

    public double GetPercentageOfDevicesOfflineForMonitoredCompanies()
    {
        int totalNumberOfDevices = GetTotalDevicesCountForMonitoredCompanies();
        int totalNumberOfDevicesOffline = GetOfflineDevicesCountForMonitoredCompanies();

        return (double)totalNumberOfDevicesOffline / totalNumberOfDevices * 100;
    }

    public DeviceCollection GetOfflineDevicesForCompany(int companyId)
    {
        PredicateExpression filter = new PredicateExpression();
        filter.Add(ClientFields.CompanyId == companyId);
        filter.Add(DeviceFields.LastRequestUTC <= DateTime.UtcNow.AddMinutes(-15));
        filter.Add(DeviceFields.IsDevelopment == false);

        RelationCollection relations = new RelationCollection(DeviceEntity.Relations.ClientEntityUsingDeviceId);

        SortExpression sort = new SortExpression();
        sort.Add(ClientFields.DeliverypointGroupId | SortOperator.Ascending);
        sort.Add(ClientFields.DeliverypointId | SortOperator.Ascending);

        PrefetchPath prefetch = new PrefetchPath(EntityType.DeviceEntity);
        IPrefetchPathElement clientPrefetch = prefetch.Add(DeviceEntity.PrefetchPathClientCollection, new IncludeFieldsList { ClientFields.ClientId });
        clientPrefetch.SubPath.Add(ClientEntityBase.PrefetchPathDeliverypointEntity, new IncludeFieldsList(DeliverypointFields.Name, DeliverypointFields.DeliverypointgroupId)).SubPath.Add(DeliverypointEntityBase.PrefetchPathDeliverypointgroupEntity, new IncludeFieldsList(DeliverypointgroupFields.Name));

        IncludeFieldsList deviceIncludedFieldsList = new IncludeFieldsList(DeviceFields.Identifier, DeviceFields.LastRequestUTC, DeviceFields.BatteryLevel, DeviceFields.PublicIpAddress);

        DeviceCollection deviceCollection = new DeviceCollection();
        deviceCollection.GetMulti(filter, 0, sort, relations, prefetch, deviceIncludedFieldsList, 0, 0);

        return deviceCollection;
    }

    /// <summary>
    /// Updates a device token.
    /// </summary>
    /// <param name="deviceEntity">The device entity.</param>
    /// <param name="token">The token to use.</param>
    /// <param name="transaction">The transaction to use.</param>
    public void RevokeDeviceToken(DeviceEntity deviceEntity, string token, ITransaction transaction)
    {
        deviceEntity.AddToTransaction(transaction);
        deviceEntity.Token = token;
        deviceEntity.SandboxMode = true;
        deviceEntity.Save();
    }
}