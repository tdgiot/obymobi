﻿using Obymobi.Data.CollectionClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using SD.LLBLGen.Pro.ORMSupportClasses;

public class CompanyDataSource
{
    public CompanyCollection GetCompaniesThatUseMonitoring()
    {
        PredicateExpression filter = new PredicateExpression();
        filter.Add(CompanyFields.SystemType == SystemType.Crave);
        filter.Add(CompanyFields.UseMonitoring == true);

        CompanyCollection companyCollection = new CompanyCollection();
        companyCollection.GetMulti(filter);

        return companyCollection;
    }

    public CompanyCollection FetchCompaniesWithTimeZones()
    {
        IncludeFieldsList includes = new IncludeFieldsList(CompanyFields.TimeZoneOlsonId);

        CompanyCollection companyCollection = new CompanyCollection();
        companyCollection.GetMulti(null, includes, 0);

        return companyCollection;
    }
}