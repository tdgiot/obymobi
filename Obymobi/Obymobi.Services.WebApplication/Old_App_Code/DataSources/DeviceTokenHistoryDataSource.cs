﻿using Obymobi.Data.EntityClasses;
using Obymobi.Enums;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System;

public class DeviceTokenHistoryDataSource
{
    /// <summary>
    /// Creates a new device token history entity.
    /// </summary>
    /// <param name="deviceTokenTaskEntity">The device token task entity.</param>
    /// <param name="transaction">The transaction to use.</param>
    public void CreateDeviceTokenHistoryEntity(DeviceTokenTaskEntity deviceTokenTaskEntity, ITransaction transaction)
    {
        DeviceTokenHistoryEntity deviceTokenHistoryEntity = new DeviceTokenHistoryEntity();
        deviceTokenHistoryEntity.AddToTransaction(transaction);
        deviceTokenHistoryEntity.DeviceId = deviceTokenTaskEntity.DeviceId;
        deviceTokenHistoryEntity.Identifier = deviceTokenTaskEntity.DeviceEntity.Identifier;
        deviceTokenHistoryEntity.UserId = deviceTokenTaskEntity.UserId;
        deviceTokenHistoryEntity.Username = deviceTokenTaskEntity.UserEntity.Username;
        deviceTokenHistoryEntity.Token = deviceTokenTaskEntity.Token;
        deviceTokenHistoryEntity.Action = deviceTokenTaskEntity.Action;
        deviceTokenHistoryEntity.Notes = deviceTokenTaskEntity.Notes;
        deviceTokenHistoryEntity.Save();
    }
}