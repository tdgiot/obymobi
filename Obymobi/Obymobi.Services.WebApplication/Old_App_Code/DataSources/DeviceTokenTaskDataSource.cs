﻿using System;
using System.Linq;
using Obymobi.Data;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using SD.LLBLGen.Pro.ORMSupportClasses;

public class DeviceTokenTaskDataSource
{
    /// <summary>
    /// Returns a device token task collection.
    /// </summary>
    /// <param name="amountOfEntities">The amount of device token tasks to return.</param>
    /// <param name="state">The state of the tasks to return.</param>
    /// <param name="stateUpdatedUtc">The state updated utc of the tasks to return before.</param>
    /// <returns></returns>
    public DeviceTokenTaskCollection GetDeviceTokenTaskCollection(int amountOfEntities, DeviceTokenState state, DateTime? stateUpdatedUtc = null)
    {
        PredicateExpression filter = new PredicateExpression(DeviceTokenTaskFields.State == state);

        if (stateUpdatedUtc.HasValue)
        {
            filter.Add(DeviceTokenTaskFields.StateUpdatedUTC < stateUpdatedUtc);
        }

        SortExpression sort = new SortExpression(DeviceTokenTaskFields.DeviceTokenTaskId | SortOperator.Ascending);

        PrefetchPath prefetchPath = new PrefetchPath(EntityType.DeviceTokenTaskEntity);
        prefetchPath.Add(DeviceTokenTaskEntity.PrefetchPathDeviceEntity, new IncludeFieldsList(DeviceFields.DeviceId, DeviceFields.Identifier));
        prefetchPath.Add(DeviceTokenTaskEntity.PrefetchPathUserEntity, new IncludeFieldsList(UserFields.UserId, UserFields.Username));

        DeviceTokenTaskCollection deviceTokenTaskCollection = new DeviceTokenTaskCollection();
        deviceTokenTaskCollection.GetMulti(filter, amountOfEntities, sort);

        return deviceTokenTaskCollection;
    }

    /// <summary>
    /// Deletes a device token task entity.
    /// </summary>
    /// <param name="deviceTokenTaskEntity">The device token task entity.</param>
    /// <param name="transaction">The transaction to use.</param>
    public void DeleteDeviceTokenTaskEntity(DeviceTokenTaskEntity deviceTokenTaskEntity, ITransaction transaction)
    {
        deviceTokenTaskEntity.AddToTransaction(transaction);
        deviceTokenTaskEntity.Delete();
    }

    /// <summary>
    /// Updates the device token state state.
    /// </summary>
    /// <param name="deviceTokenTaskEntity">The device token task entity.</param>
    /// <param name="state">The new device token task state.</param>
    /// <param name="transaction">The transaction to use.</param>
    public void UpdateDeviceTokenTaskState(DeviceTokenTaskEntity deviceTokenTaskEntity, DeviceTokenState state, ITransaction transaction)
    {
        deviceTokenTaskEntity.AddToTransaction(transaction);
        deviceTokenTaskEntity.State = state;
        deviceTokenTaskEntity.StateUpdatedUTC = DateTime.UtcNow;
        deviceTokenTaskEntity.Save();
    }

    /// <summary>
    /// Updates the state updated.
    /// </summary>
    /// <param name="deviceTokenTaskEntity">The device token task entity.</param>
    /// <param name="state">The new device token task state.</param>
    /// <param name="transaction">The transaction to use.</param>
    public void UpdateDeviceTokenTaskStateUpdated(DeviceTokenTaskEntity deviceTokenTaskEntity, ITransaction transaction)
    {
        deviceTokenTaskEntity.AddToTransaction(transaction);
        deviceTokenTaskEntity.StateUpdatedUTC = DateTime.UtcNow;
        deviceTokenTaskEntity.Save();
    }
}