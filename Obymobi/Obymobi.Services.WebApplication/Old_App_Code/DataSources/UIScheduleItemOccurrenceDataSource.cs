﻿using Obymobi.Data.CollectionClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System;
using DevExpress.XtraScheduler;
using Obymobi.Data;
using Obymobi.Data.EntityClasses;

public class UIScheduleItemOccurrenceDataSource
{
    public UIScheduleItemOccurrenceCollection FetchOccurrences(DateTime dateTimeUtc)
    {
        UIScheduleItemOccurrenceCollection adHocOccurrenceCollection = FetchAdHocOccurrences(dateTimeUtc);
        UIScheduleItemOccurrenceCollection recurringOccurrenceCollection = FetchRecurringOccurrences();

        UIScheduleItemOccurrenceCollection occurrenceCollection = new UIScheduleItemOccurrenceCollection();

        occurrenceCollection.AddRange(adHocOccurrenceCollection);
        occurrenceCollection.AddRange(recurringOccurrenceCollection);

        return occurrenceCollection;
    }

    private static UIScheduleItemOccurrenceCollection FetchAdHocOccurrences(DateTime dateTimeUtc)
    {
        IPredicateExpression filter = CreateAdHocFilter(dateTimeUtc);
        IRelationCollection relationCollection = CreateRelationCollection();

        return FetchOccurrences(filter, relationCollection, null);
    }

    private static UIScheduleItemOccurrenceCollection FetchRecurringOccurrences()
    {
        IPredicateExpression filter = CreateRecurringFilter();
        IRelationCollection relationCollection = CreateRelationCollection();
        IPrefetchPath prefetchPath = CreatePrefetchPath();

        return FetchOccurrences(filter, relationCollection, prefetchPath);
    }

    private static UIScheduleItemOccurrenceCollection FetchOccurrences(IPredicateExpression filter, IRelationCollection relationCollection, IPrefetchPath prefetch)
    {
        UIScheduleItemOccurrenceCollection uiScheduleItemOccurrenceCollection = new UIScheduleItemOccurrenceCollection();
        uiScheduleItemOccurrenceCollection.GetMulti(filter, 0, null, relationCollection, prefetch);

        return uiScheduleItemOccurrenceCollection;
    }

    private static IPredicateExpression CreateAdHocFilter(DateTime dateTimeUtc)
    {
        return new PredicateExpression()
               .Add(UIScheduleItemOccurrenceFields.Recurring == false)
               .Add(UIScheduleItemOccurrenceFields.StartTimeUTC <= dateTimeUtc)
               .Add(UIScheduleItemOccurrenceFields.EndTimeUTC > dateTimeUtc)
               .Add(CreateTypeFilter())
               .Add(CreateLinkedItemFilter());
    }

    private static IPredicateExpression CreateRecurringFilter()
    {
        return new PredicateExpression()
                .Add(UIScheduleItemOccurrenceFields.Recurring == true)
                .Add(CreateTypeFilter())
                .Add(CreateLinkedItemFilter());
    }

    private static IPredicateExpression CreateTypeFilter()
    {
        return new PredicateExpression()
                .Add(UIScheduleItemOccurrenceFields.Type != (int)AppointmentType.DeletedOccurrence);
    }

    private static IPredicateExpression CreateLinkedItemFilter()
    {
        return new PredicateExpression()
                .Add(UIScheduleItemFields.ScheduledMessageId != DBNull.Value)
                .AddWithOr(UIScheduleItemOccurrenceFields.ReportProcessingTaskTemplateId != DBNull.Value);
    }

    private static IRelationCollection CreateRelationCollection()
    {
        IRelationCollection relationCollection = new RelationCollection();
        relationCollection.Add(UIScheduleItemOccurrenceEntityBase.Relations.UIScheduleItemEntityUsingUIScheduleItemId, JoinHint.Left);

        return relationCollection;
    }

    private static IPrefetchPath CreatePrefetchPath()
    {
        PrefetchPath prefetch = new PrefetchPath(EntityType.UIScheduleItemOccurrenceEntity);
        prefetch.Add(UIScheduleItemOccurrenceEntityBase.PrefetchPathUIScheduleItemOccurrenceCollection);

        return prefetch;
    }
}