﻿using Obymobi.Constants;
using Obymobi.Data;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using Obymobi.Logic.Model;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System;

public class TerminalDataSource
{
    public TerminalCollection GetPmsTerminals()
    {
        PredicateExpression filter = new PredicateExpression();
        filter.Add(TerminalFields.HandlingMethod == TerminalType.OnSiteServer);
        filter.Add(TerminalFields.PMSConnectorType > 0);

        PrefetchPath prefetch = new PrefetchPath(EntityType.TerminalEntity);
        prefetch.Add(TerminalEntity.PrefetchPathDeviceEntity, new IncludeFieldsList(DeviceFields.LastRequestUTC));
        prefetch.Add(TerminalEntity.PrefetchPathCompanyEntity, new IncludeFieldsList(CompanyFields.TimeZoneOlsonId));

        IncludeFieldsList includes = new IncludeFieldsList();
        includes.Add(TerminalFields.TerminalId);
        includes.Add(TerminalFields.DeviceId);
        includes.Add(TerminalFields.LastSyncUTC);        

        TerminalCollection terminals = new TerminalCollection();
        terminals.GetMulti(filter, 0, null, null, prefetch, includes, 0, 0);

        return terminals;
    }

    public TerminalCollection GetTerminalsWithPmsTerminalOfflineNotificationEnabled()
    {
        PredicateExpression filter = new PredicateExpression();
        filter.Add(TerminalFields.PmsTerminalOfflineNotificationEnabled == true);        
        filter.Add(DeviceFields.LastRequestUTC > DateTime.UtcNow.AddMilliseconds(ObymobiIntervals.DEVICE_OFFLINE_THRESHOLD * -1));

        PrefetchPath prefetch = new PrefetchPath(EntityType.TerminalEntity);
        prefetch.Add(TerminalEntity.PrefetchPathDeviceEntity, new IncludeFieldsList(DeviceFields.Identifier, DeviceFields.LastRequestUTC));        

        RelationCollection relations = new RelationCollection();
        relations.Add(TerminalEntity.Relations.DeviceEntityUsingDeviceId);

        IncludeFieldsList includes = new IncludeFieldsList();
        includes.Add(TerminalFields.TerminalId);
        includes.Add(TerminalFields.DeviceId);
        includes.Add(TerminalFields.CompanyId);
        includes.Add(TerminalFields.PmsTerminalOfflineNotificationEnabled);

        TerminalCollection terminals = new TerminalCollection();
        terminals.GetMulti(filter, 0, null, relations, prefetch, includes, 0, 0);

        return terminals;
    }   

    public int GetNumberOfTerminalsNotPickingUpOrders()
    {
        DateTime threeMinutesAgoUtc = DateTime.UtcNow.AddMinutes(-3);

        IPredicateExpression filter = new PredicateExpression();
        filter.Add(DeviceFields.LastRequestUTC > threeMinutesAgoUtc);                                           // Terminal is online
        filter.Add(DeviceFields.CommunicationMethod == ClientCommunicationMethod.SignalR);                      // Terminal communicates via messaging
        filter.Add(OrderRoutestephandlerFields.HandlerType == RoutestephandlerType.Console);                    // Order to be picked up by console
        filter.Add(OrderRoutestephandlerFields.Status == OrderRoutestephandlerStatus.WaitingToBeRetrieved);     // Order waiting to be retrieved
        filter.Add(OrderRoutestephandlerFields.WaitingToBeRetrievedUTC < threeMinutesAgoUtc);                   // Been waiting for at least 3 minutes

        IRelationCollection relations = new RelationCollection();
        relations.Add(TerminalEntityBase.Relations.DeviceEntityUsingDeviceId);
        relations.Add(TerminalEntityBase.Relations.OrderRoutestephandlerEntityUsingTerminalId);

        TerminalCollection terminals = new TerminalCollection();
        return terminals.GetDbCount(filter, relations);
    }
}