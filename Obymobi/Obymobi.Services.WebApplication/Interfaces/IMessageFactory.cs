﻿namespace Obymobi.Services.WebApplication
{
    public interface IMessageFactory
    {
        string Create(string message);
    }
}
