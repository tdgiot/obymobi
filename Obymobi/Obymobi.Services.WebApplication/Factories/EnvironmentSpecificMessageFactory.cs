﻿using Obymobi.Enums;

namespace Obymobi.Services.WebApplication
{
    public class EnvironmentSpecificMessageFactory : IMessageFactory
    {
        private readonly CloudEnvironment cloudEnvironment;

        public EnvironmentSpecificMessageFactory(CloudEnvironment cloudEnvironment) => this.cloudEnvironment = cloudEnvironment;

        public string Create(string message) => $"{GetPrefix(cloudEnvironment)}{message}";

        private static string GetPrefix(CloudEnvironment cloudEnvironment) => IsProductionEnvironment(cloudEnvironment) ? string.Empty : $"[{cloudEnvironment}] ";

        private static bool IsProductionEnvironment(CloudEnvironment cloudEnvironment) =>
            cloudEnvironment == CloudEnvironment.ProductionPrimary ||
            cloudEnvironment == CloudEnvironment.ProductionSecondary ||
            cloudEnvironment == CloudEnvironment.ProductionOverwrite;
    }
}
