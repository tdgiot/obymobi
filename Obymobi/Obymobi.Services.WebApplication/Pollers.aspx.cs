﻿using System.Globalization;
using System.Linq;
using System.Text;
using Obymobi.Enums;
using Obymobi.Web.Pollers;
using System;
using System.Threading.Tasks;
using Dionysos.Web;
using Obymobi;

public partial class _Pollers : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        var sb = new StringBuilder();

        var pollerTypes = Enum.GetValues(typeof(PollerType)).Cast<PollerType>().ToList();
        pollerTypes.Sort((type, pollerType) => String.Compare(type.ToString(), pollerType.ToString(), StringComparison.Ordinal));

        sb.AppendFormat("{0,-32} | {1, -7} | {2, -21} | {3}<br/>", "Poller", "Status", "Last Start", "Next Run");
        sb.AppendFormat("{0}|{1}|{2}|{3}<br/>", new string('-', 33), new string('-', 9), new string('-', 23), new string('-', 12));
        foreach(PollerType pollerType in pollerTypes)
        {
            string status = "OFFLINE";
            string lastStarted = "Never";
            string nextRun = "Unknown";
            IPoller poller;
            if (PollerManager.TryGetPoller(pollerType, out poller))
            {
                status = "ONLINE";
                if (poller.LastStarted != DateTime.MinValue)
                    lastStarted = poller.LastStarted.ToString(CultureInfo.InvariantCulture);

                nextRun = poller.TimeTillNextWork().ToString(@"d\.hh\:mm\:ss");
            }

            sb.AppendFormat("{0,-32} | {1, -7} | {2, -21} | {3}<br/>", pollerType, status, lastStarted, nextRun);
        }

        sb.Replace(" ", "&nbsp;");
        this.lblOutput.Text = sb.ToString();

        RunPoller();
    }

    private void RunPoller()
    {
        if (!QueryStringHelper.HasValue("Run"))
            return;

        if (WebEnvironmentHelper.CloudEnvironmentIsProduction)
        {
            this.lblOutput.Text += "<br/><br/>This is a production environment. FUCKYOU!";
            return;
        }

        IPoller poller = null;
        PollerType pollerType;
        if (Enum.TryParse(QueryStringHelper.GetString("Run"), out pollerType))
        {
            if (PollerManager.TryGetPoller(pollerType, out poller))
            {
                this.lblOutput.Text += "<br/><br/>Manually running poller: " + poller.PollerType;
                Task.Factory.StartNew(() => poller.DoWork(true), TaskCreationOptions.LongRunning);
            }
        }

        if (poller == null)
        {
            this.lblOutput.Text += "<br/><br/>Unknown PollerType";
        }
    }
}