﻿using System;
using Obymobi.Logic.HelperClasses;
using Obymobi.Logic.Model;

public class BackendService : IBackendService
{
    public bool IsAlive()
    {
        return true;
    }

    public bool IsDatabaseAlive()
    {
        return DatabaseHelper.IsDatabaseReady(Obymobi.Data.DaoClasses.CommonDaoBase.ActualConnectionString);
    }

    public long GetSecondsSinceLastPollerExecution(string pollerName, long timestamp, string hash)
    {
        return BackendServiceHandler.GetSecondsSinceLastPollerExecution(pollerName, timestamp, hash);
    }

    public ServiceBackendWebserviceResult GetSystemSupportpoolInfo(long timestamp, string hash)
    {
        return BackendServiceHandler.GetSystemSupportpoolInfo(timestamp, hash);
    }

    #region Backwards-Compatible (Obsolete since v21+)

    [Obsolete("Use GetSystemSupportpoolInfo instead")]
    public string[] GetSystemSupportpoolPhonenumbers(long timestamp, string hash)
    {
        return BackendServiceHandler.GetSystemSupportpoolPhonenumbers(timestamp, hash);
    }

    [Obsolete("Use GetSystemSupportpoolInfo instead")]
    public string[] GetSystemSupportpoolEmailaddresses(long timestamp, string hash)
    {
        return BackendServiceHandler.GetSystemSupportpoolEmailaddresses(timestamp, hash);
    }

    #endregion
}
