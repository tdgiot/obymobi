using Amazon.Runtime;
using AWS.Logger;
using AWS.Logger.SeriLog;
using Dionysos;
using Dionysos.Logging;
using Dionysos.SMS;
using Dionysos.Verification;
using Dionysos.Web;
using Obymobi;
using Obymobi.Configuration;
using Obymobi.Enums;
using Obymobi.Logic.Comet;
using Obymobi.Logic.HelperClasses;
using Obymobi.Logic.Verifiers;
using Obymobi.Web;
using Obymobi.Web.CloudStorage;
using Serilog;
using Serilog.Events;
using System;
using System.Web;

/// <summary>
/// Summary description for Global
/// </summary>
public class Global : HttpApplicationSystemBased
{
    #region Fields

    public static IMultiLoggingProvider LoggingProvider { get; private set; }

    private static Watchdog watchdog;

    #endregion

    public Global() : base("Services")
    { }

    #region Initialization

    public override void DatabaseIndependentPreInitialization()
    {
        LoggingProvider = InitializeLoggingProvider(ApplicationName);
        LoggingProvider.Information(LoggingTypes.Services, "DatabaseIndependentPreInitialization");

        // Set the application information
        Dionysos.Global.ApplicationInfo = new WebApplicationInfo();
        Dionysos.Global.ApplicationInfo.BasePath = Server.MapPath("~");
        Dionysos.Global.ApplicationInfo.ApplicationName = "BackendPollers";
        Dionysos.Global.ApplicationInfo.ApplicationVersion = "1.2019103101";

        // Set the configuration provivder
        Dionysos.Global.ConfigurationProvider = new Dionysos.Configuration.LLBLGenConfigurationProvider();

        // Set the data providers
        DataFactory.EntityCollectionFactory = new Dionysos.Data.LLBLGen.LLBLGenEntityCollectionFactory();
        DataFactory.EntityFactory = new Dionysos.Data.LLBLGen.LLBLGenEntityFactory();
        DataFactory.EntityUtil = new Dionysos.Data.LLBLGen.LLBLGenEntityUtil();

        // Set the configuration definitions
        Dionysos.Global.ConfigurationInfo.Add(new DionysosConfigurationInfo());
        Dionysos.Global.ConfigurationInfo.Add(new DionysosWebConfigurationInfo());
        Dionysos.Global.ConfigurationInfo.Add(new ObymobiConfigInfo());
        Dionysos.Global.ConfigurationInfo.Add(new ObymobiDataConfigInfo());
        Dionysos.Global.ConfigurationInfo.Add(new DionysosSMSConfigurationInfo());
        Dionysos.Global.ConfigurationInfo.Add(new CraveCometConfigInfo());
        Dionysos.Global.ConfigurationInfo.Add(new ServicesConfigInfo());

        // Initialize the assembly information
        Dionysos.Global.AssemblyInfo.Add(Server.MapPath("~/Bin/Obymobi.Data.dll"), "Data");

        // Comet Helper (comm method = none == use redis)
        CometHelper.SetCometProvider(new CometDesktopProvider(ClientCommunicationMethod.SignalR, CometConstants.CometIdentifierServices, NetmessageClientType.Webservice));

        var verifiers = new VerifierCollection();
        verifiers.Add(new Dionysos.Data.DependencyInjectionVerifier());
        verifiers.Add(new EntityVerifier());
        verifiers.Add(new LoggingVerifier());
        verifiers.Add(new MiniTixVerifier());
        verifiers.Add(new SMSVerifier());
        verifiers.Add(new AmazonCloudStorageVerifier());

        verifiers.Add(new WritePermissionVerifier(System.Web.Hosting.HostingEnvironment.MapPath("~/App_Data/Logs/")));

        verifiers.Add(new AppSettingsVerifier(DionysosConfigurationConstants.BaseUrl));
        verifiers.Add(new AppSettingsVerifier(DionysosConfigurationConstants.CloudEnvironment));
        verifiers.Add(new AppSettingsVerifier(DionysosConfigurationConstants.SendGridApiKey));

        if (!TestUtil.IsPcDeveloper)
        {
            // How to get this token? https://docs.google.com/document/d/1mxh2qw_Mp3Z_aJf8W9cLQi1LLY2vOwgdZOYwW84nqh4/edit
            verifiers.Add(new AppSettingsVerifier("GoogleApisJsonWebTokenBase64"));

        }
        if (!verifiers.Verify())
        {
            throw new VerificationException(verifiers.ErrorMessage);
        }
    }

    public override void DatabaseDependentInitialization()
    {
        LoggingProvider.Information(LoggingTypes.Services, "DatabaseDependentInitialization");

        // Set locking method
        SD.LLBLGen.Pro.DQE.SqlServer.DynamicQueryEngine.UseNoLockHintOnSelects = true;

        // Update the version
        CloudApplicationVersionHelper.SetCloudApplicationVersion(CloudApplication.Services, Dionysos.Global.ApplicationInfo.ApplicationVersion);

        VerifierCollection verifiers = new VerifierCollection();
        verifiers.Add(new CloudEnvironmentConfigValidator());
        if (!verifiers.Verify())
        {
            throw new VerificationException(verifiers.ErrorMessage);
        }

        if (WebEnvironmentHelper.ORMProfilerEnabled)
        {
            EnableDatabaseProfiler();
        }

        if (watchdog == null)
        {
            watchdog = new Watchdog(HttpContext.Current, LoggingProvider);
            watchdog.Start();
        }
    }

    public void Application_End()
    {
        if (watchdog != null)
        {
            watchdog.Stop();
        }
        else
        {
            LoggingProvider.Information(LoggingTypes.Services, "Unable to stop Watchdog, never initialized");
        }

        LoggingProvider.Verbose(LoggingTypes.Services, "Application_End");
        LoggingProvider.Verbose(LoggingTypes.Services, "=============================================");
    }

    private IMultiLoggingProvider InitializeLoggingProvider(string applicationName)
    {
        string cloudWatchAccessKey = System.Configuration.ConfigurationManager.AppSettings["CloudWatch:AccessKey"];
        string cloudWatchSecretKey = System.Configuration.ConfigurationManager.AppSettings["CloudWatch:SecretKey"];
        string cloudWatchLogGroup = System.Configuration.ConfigurationManager.AppSettings["CloudWatch:LogGroup"];
        string cloudWatchRegion = System.Configuration.ConfigurationManager.AppSettings["CloudWatch:Region"];
        string cloudWatchMinimumLevel = System.Configuration.ConfigurationManager.AppSettings["CloudWatch:Minimumlevel"];

        if (cloudWatchAccessKey.IsNullOrWhiteSpace() || cloudWatchSecretKey.IsNullOrWhiteSpace())
        {
            return InitializeFileLoggingProvider(applicationName);
        }

        if (cloudWatchLogGroup.IsNullOrWhiteSpace())
        {
            throw new InvalidOperationException("Configuration is not valid. CloudWatch:LogGroup is required.");
        }

        if (cloudWatchRegion.IsNullOrWhiteSpace())
        {
            throw new InvalidOperationException("Configuration is not valid. CloudWatch:Region is required.");
        }

        LogEventLevel minimumLevel = !cloudWatchMinimumLevel.IsNullOrWhiteSpace()
            ? cloudWatchMinimumLevel.ToEnum<LogEventLevel>()
            : LogEventLevel.Information;

        LoggerConfiguration loggerConfiguration = new LoggerConfiguration()
                .MinimumLevel.Is(minimumLevel)
                .WriteTo.AWSSeriLog(new AWSLoggerConfig
                {
                    Credentials = new BasicAWSCredentials(cloudWatchAccessKey, cloudWatchSecretKey),
                    LogGroup = cloudWatchLogGroup,
                    Region = cloudWatchRegion
                });

        return InitializeCloudWatchLoggingProvider(applicationName, loggerConfiguration);
    }

    private IMultiLoggingProvider InitializeFileLoggingProvider(string applicationName)
        => new AsyncWebAppLoggingProvider(System.Web.Hosting.HostingEnvironment.MapPath("~/App_Data/Logs/"), applicationName, 7);

    private IMultiLoggingProvider InitializeCloudWatchLoggingProvider(string applicationName, LoggerConfiguration loggerConfiguration)
        => new SerilogProvider(loggerConfiguration.CreateLogger(), applicationName);

    #endregion
}
