﻿using Obymobi.Charts.Enumerators;

namespace Obymobi.Charts.Dtos
{
    public class ChartOptionLegendDto
    {
        #region Constructors

        public ChartOptionLegendDto()
        {
            Display = false;
        }

        public ChartOptionLegendDto(LegendPosition legendPosition, bool reverse = false)
        {
            Display = true;
            Position = legendPosition.ToString().ToLower();
            Reverse = reverse;
        }

        #endregion

        #region Properties

        public bool Display { get; set; }

        public string Position { get; set; }

        public bool Reverse { get; set; }

        #endregion
    }
}
