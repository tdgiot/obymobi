﻿using System.Collections.Generic;

namespace Obymobi.Charts.Dtos
{
    public class ChartWrapperDto
    {
        #region Constructors

        public ChartWrapperDto(ChartDto chart, bool applyTotalsToLegend = false, bool hideSlices = false)
        {
            Chart = chart;
            ApplyTotalsToLegend = applyTotalsToLegend;
            HideSlices = hideSlices;
        }

        #endregion

        #region Properties

        public ChartDto Chart { get; set; }

        public bool ApplyTotalsToLegend { get; set; }

        public bool HideSlices { get; set; }

        #endregion
    }
}
