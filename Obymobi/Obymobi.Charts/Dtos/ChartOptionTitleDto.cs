﻿namespace Obymobi.Charts.Dtos
{
    public class ChartOptionTitleDto
    {
        #region Constructors

        public ChartOptionTitleDto()
        {
            Display = false;
        }

        public ChartOptionTitleDto(string title)
        {
            Display = true;
            Text = title;
        }

        #endregion

        #region Properties

        public bool Display { get; private set; }

        public string Text { get; private set; }

        #endregion
    }
}
