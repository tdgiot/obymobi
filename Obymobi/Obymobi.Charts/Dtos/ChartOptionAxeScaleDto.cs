﻿namespace Obymobi.Charts.Dtos
{
    public class ChartOptionAxeScaleDto
    {
        public ChartOptionAxeScaleTicksDto Ticks { get; set; }
    }
}
