﻿using System.Collections.Generic;

namespace Obymobi.Charts.Dtos
{
    public class ChartOptionScalesDto
    {
        #region Constructors

        public ChartOptionScalesDto(bool beginAtZero = false, int stepSize = 0, int min = 0)
        {
            YAxes.Add(new ChartOptionAxeScaleDto
            {
                Ticks = new ChartOptionAxeScaleTicksDto
                {
                    BeginAtZero = beginAtZero,
                    Min = min,
                    StepSize = stepSize,
                    SuggestedMin = 1,
                    SuggestedMax = 10
                }
            });
        }

        #endregion

        #region Properties

        public List<ChartOptionAxeScaleDto> YAxes { get; set; } = new List<ChartOptionAxeScaleDto>();

        #endregion
    }
}
