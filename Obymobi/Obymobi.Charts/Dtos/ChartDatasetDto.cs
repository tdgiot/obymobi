﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace Obymobi.Charts.Dtos
{
    public class ChartDatasetDto
    {
        public string Label { get; set; }

        public List<int> Data { get; set; } = new List<int>();

        public bool Fill { get; set; }

        public IEnumerable<string> BackgroundColor { get; set; }

        public IEnumerable<string> BorderColor { get; set; }

        public int BorderWidth { get; set; }

        [JsonIgnore]
        public List<string> DataLabels { get; set; } = new List<string>();
    }
}
