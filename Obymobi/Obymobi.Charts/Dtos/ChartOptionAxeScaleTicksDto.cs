﻿namespace Obymobi.Charts.Dtos
{
    public class ChartOptionAxeScaleTicksDto
    {
        public bool BeginAtZero { get; set; }

        public int Min { get; set; }

        public int StepSize { get; set; }

        public int Precision { get; set; }

        public int SuggestedMax { get; set; }

        public int SuggestedMin { get; set; }
    }
}