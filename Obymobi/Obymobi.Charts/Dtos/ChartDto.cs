﻿using Obymobi.Charts.Enumerators;

namespace Obymobi.Charts.Dtos
{
    public class ChartDto
    {
        #region Constructors

        public ChartDto(ChartType type, ChartDataDto data, ChartOptionsDto options = null)
        {
            Type = type.ToString().ToLower();
            Data = data;
            Options = options ?? new ChartOptionsDto();
        }

        #endregion

        #region Properties

        public string Type { get; set; }

        public ChartDataDto Data { get; set; }

        public ChartOptionsDto Options { get; set; }

        #endregion
    }
}
