﻿namespace Obymobi.Charts.Dtos
{
    public class ChartOptionsDto
    {
        public ChartOptionTitleDto Title { get; set; }

        public ChartOptionLegendDto Legend { get; set; }

        public ChartOptionScalesDto Scales { get; set; }
    }
}
