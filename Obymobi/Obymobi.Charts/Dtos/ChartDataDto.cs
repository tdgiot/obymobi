﻿using System.Collections.Generic;
using System.Linq;
using Obymobi.Charts.Enumerators;

namespace Obymobi.Charts.Dtos
{ 
    public class ChartDataDto
    {
        #region Constuctors

        public ChartDataDto(ChartDatasetDto dataset, string label = "")
        {
            Labels = new List<string> { label };
            Datasets = new List<ChartDatasetDto> { dataset };
        }

        public ChartDataDto(List<ChartDatasetDto> datasets, List<string> labels, IEnumerable<int> hiddenSlices = null)
        {
            Labels = labels;
            Datasets = datasets;
            HiddenSlices = hiddenSlices;
        }

        #endregion

        #region Properties

        public List<string> Labels { get; set; } = new List<string>();

        public List<ChartDatasetDto> Datasets { get; set; } = new List<ChartDatasetDto>();

        public IEnumerable<int> HiddenSlices { get; set; }

        #endregion
    }
}
