﻿namespace Obymobi.Charts.Enumerators
{
    public enum LegendPosition
    {
        None = 0,
        Top = 1,
        Right = 2,
        Bottom = 3,
        Left = 4
    }
}
