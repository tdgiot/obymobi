﻿namespace Obymobi.Charts.Enumerators
{
    public enum ChartType
    {
        Bar = 1,
        Pie = 2,
        Line = 3
    }
}
