using NUnit.Framework;

namespace Obymobi.Core.Tests
{
    public class VersionNumberTest
    {
        [Test]
        public void CompareVersions_WithVersions2021092203And2021120101_ReturnsOlder()
        {
            // Arrange
            const string version1 = "2021092203";
            const string version2 = "2021120101";

            // Act
            VersionNumber.VersionState result = VersionNumber.CompareVersions(version1, version2);

            // Assert
            Assert.AreEqual(VersionNumber.VersionState.Older, result);
        }

        [Test]
        public void CompareVersions_WithVersions2021120101And2021092203_ReturnsNewer()
        {
            // Arrange
            const string version1 = "2021120101";
            const string version2 = "2021092203";

            // Act
            VersionNumber.VersionState result = VersionNumber.CompareVersions(version1, version2);

            // Assert
            Assert.AreEqual(VersionNumber.VersionState.Newer, result);
        }

        [Test]
        public void CompareVersions_WithVersions2021120101And2021120101_ReturnsEqual()
        {
            // Arrange
            const string version1 = "2021120101";
            const string version2 = "2021120101";

            // Act
            VersionNumber.VersionState result = VersionNumber.CompareVersions(version1, version2);

            // Assert
            Assert.AreEqual(VersionNumber.VersionState.Equal, result);
        }
    }
}
