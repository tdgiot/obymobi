﻿using Obymobi.Enums;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Dionysos;
using System.Globalization;
using Newtonsoft.Json;
using Obymobi.Data.EntityClasses;
using Obymobi.Analytics.KpiFunctions;
using Obymobi.Analytics.Reports;

namespace Obymobi.Analytics
{
    public class KpiFunctionHelper
    {
        private static string functionPattern = @"(?<FunctionName>.*)\((?<ParameterSets>.*)\)";
        
        // Exampel input: GoogleScreenViews(CompanyIds[343]; SiteIds[10,15]; TabIds[]; ScreenNames[Aria]; ProductIds[]; FriendlyName[All Aria Pages Views])
        // Match full functions (?<FunctionName>.*)\((?<ParameterSets>.*)\)
        // Match the Sets in the Parameters: (?<ParameterName>.*?)\[(?<Values>.*?)\];?
        // Split the Parameters in the Set
        
        private int? companyId;
        private List<IKpiFunctionProcessor> processors = null;

        public KpiFunctionHelper(List<IKpiFunctionProcessor> processors, int? companyId = null)
        {
            this.processors = processors;

            // GAKR I didn't know a better way (for now) to secure that these report didn't allow people to include CompanyIds to which they had no access (especially with Google)
            this.companyId = companyId;            
        }

        public bool IsValid(string input)
        {
            return Regex.IsMatch(input, KpiFunctionHelper.functionPattern);
        }

        //public string GetValueAsString(string kpiFunction, DateTime fromUtc, DateTime tillUtc)
        //{
        //    return this.GetValue<string>(kpiFunction, fromUtc, tillUtc);
        //}

        //public string GetValueAsString(string kpiFunction, DateTime fromUtc, DateTime tillUtc, out string actualFilterValues)
        //{
        //    return this.GetValue<string>(kpiFunction, fromUtc, tillUtc, out actualFilterValues);
        //}        

        //public T GetValue<T>(string kpiFunction, DateTime fromUtc, DateTime tillUtc)
        //{
        //    string notInterested = string.Empty;
        //    return this.GetValue<T>(kpiFunction, fromUtc, tillUtc, out notInterested);
        //}

        public List<object[]> GetValue(string kpiFunction, ReportProcessingTaskEntity processingTask)
        {
            string dummy;
            return GetValue(kpiFunction, processingTask, out dummy);
        }

        public List<object[]> GetValue(string kpiFunction, ReportProcessingTaskEntity processingTask, out string actualFilterValues)
        {
            var metaData = JsonConvert.DeserializeObject<KpiReportMetadata>(processingTask.Filter);
            DateTime zonedFrom = processingTask.FromUTC.Value.AddHours(metaData.UtcOffset);
            DateTime zonedTill = processingTask.TillUTC.Value.AddHours(metaData.UtcOffset);
            return GetValue(kpiFunction, processingTask.FromUTC.Value, processingTask.TillUTC.Value, zonedFrom, zonedTill, out actualFilterValues);
        }

        public List<object[]> GetValue(string kpiFunction, DateTime fromUtc, DateTime tillUtc, DateTime fromTimezoned, DateTime tillTimezoned, out string actualFilterValues)
        {
            actualFilterValues = "Filter values were never completly parsed";

            var toReturn = new List<object[]>();

            // Ensure we have a function
            string functionName, parameterSets;
            ParseKpiFunction(kpiFunction, out functionName, out parameterSets);

            try
            {
                bool handled = false;
                foreach (var processor in processors)
                {
                    IAnalyticsFilter filter;
                    if (processor.CanHandleFunction(functionName))
                    {
                        if (processor is Google.GoogleAnalyticsKpiFunctionProcessor)
                        {
                            // We need to process using the timezoned time. Since there's no intra-day query possibility it will always go 
                            // wrong. So we just have to assume the user chose a View in the right timezone
                            toReturn = processor.PrepareAndExecuteFunction(functionName, parameterSets, fromTimezoned, tillTimezoned, out filter);
                        }
                        else
                        {
                            toReturn = processor.PrepareAndExecuteFunction(functionName, parameterSets, fromUtc, tillUtc, out filter);
                        }
                        actualFilterValues = JsonConvert.SerializeObject(filter);
                        handled = true;
                    }
                }

                if (!handled)
                    throw new NotSupportedException("The following function is not supported: " + functionName);

            }
            catch (Exception ex)
            {
                throw new Exception("Failed to process function: " + kpiFunction, ex);
            }

            return toReturn;
        }

        private static void ParseKpiFunction(string kpiFunction, out string functionName, out string parameterSets)
        {
            MatchCollection functionsMatches = Regex.Matches(kpiFunction, KpiFunctionHelper.functionPattern);
            if (functionsMatches.Count == 0)
                throw new ArgumentException("Function syntax incorrect: " + kpiFunction);

            if (functionsMatches.Count > 1)
                throw new ArgumentException("Multiple functions found, this is not supported: " + kpiFunction);

            Match functionMatch = functionsMatches[0];

            // GAKR This could be made fully generic with reflection - but for now I think this is sufficient
            functionName = functionMatch.Groups["FunctionName"].Value;
            parameterSets = functionMatch.Groups["ParameterSets"].Value;
        }

        public IAnalyticsFilter GetFilter(string kpiFunction, DateTime fromUtc, DateTime tillUtc)
        {
            string functionName, parameterSets;
            ParseKpiFunction(kpiFunction, out functionName, out parameterSets);
            foreach (var processor in processors)
            {                
                if (processor.CanHandleFunction(functionName))
                {
                    return processor.PrepareFilter(parameterSets, fromUtc, tillUtc);
                }
            }

            throw new NotSupportedException("The following function is not supported: " + functionName);
        }
    }
}
