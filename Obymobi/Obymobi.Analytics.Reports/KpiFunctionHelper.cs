﻿//using Obymobi.Analytics.GoogleAnalytics;
//using Obymobi.Analytics.Transactions;
//using Obymobi.Enums;
//using System;
//using System.Collections;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Text.RegularExpressions;
//using System.Threading.Tasks;
//using Dionysos;
//using System.Globalization;
//using Newtonsoft.Json;
//using Obymobi.Data.EntityClasses;

//namespace Obymobi.Analytics
//{
//    public class KpiFunctionHelper
//    {
//        private static string functionPattern = @"(?<FunctionName>.*)\((?<ParameterSets>.*)\)";
//        private static string parameterSetPattern = @"\s?(?<ParameterName>.*?)\[(?<Values>.*?)\];?";
//        // Exampel input: GoogleScreenViews(CompanyIds[343]; SiteIds[10,15]; TabIds[]; ScreenNames[Aria]; ProductIds[]; FriendlyName[All Aria Pages Views])
//        // Match full functions (?<FunctionName>.*)\((?<ParameterSets>.*)\)
//        // Match the Sets in the Parameters: (?<ParameterName>.*?)\[(?<Values>.*?)\];?
//        // Split the Parameters in the Set

//        // GAKR Not very proud of using these strings here and again below - but couldn't really think of a better implementation. Maybe an Enum? Or make retrieval methods match the keywords and use reflection
//        // But I already spent an evening on these kinds of details - need to move forward as well. Feel free to refactor
//        private static List<string> GoogleFunctions = new List<string> { "GoogleScreenViewCount", "GoogleEventCount" };
//        private static List<string> TransactionsFunctions = new List<string> { "OrderCount", "Revenue", "OrderedQuantity", "TopXProductsByRevenue", "TopXProductsByQuantity" };

//        private GoogleAnalyticsKpiRetriever googleReportingApiHelper;
//        private int companyId;
//        private bool includeChildCategories;

//        public KpiFunctionHelper(int companyId, bool includeChildCategories, GoogleAnalyticsKpiRetriever googleReportingApiHelper = null)
//        {
//            // GAKR I didn't know a better way (for now) to secure that these report didn't allow people to include CompanyIds to which they had no access (especially with Google)
//            this.companyId = companyId;
//            this.includeChildCategories = includeChildCategories;

//            this.googleReportingApiHelper = googleReportingApiHelper;
//        }

//        public static bool IsValid(string input)
//        {
//            return Regex.IsMatch(input, KpiFunctionHelper.functionPattern);
//        }

//        public string GetValueAsString(string kpiFunction, DateTime fromUtc, DateTime tillUtc)
//        {
//            return this.GetValue<string>(kpiFunction, fromUtc, tillUtc);
//        }

//        public string GetValueAsString(string kpiFunction, DateTime fromUtc, DateTime tillUtc, out string actualFilterValues)
//        {
//            return this.GetValue<string>(kpiFunction, fromUtc, tillUtc, out actualFilterValues);
//        }        

//        public T GetValue<T>(string kpiFunction, DateTime fromUtc, DateTime tillUtc)
//        {
//            string notInterested = string.Empty;
//            return this.GetValue<T>(kpiFunction, fromUtc, tillUtc, out notInterested);
//        }

//        public T GetValue<T>(string kpiFunction, DateTime fromUtc, DateTime tillUtc, out string actualFilterValues)
//        {
//            actualFilterValues = "Filter values were never completly parsed";
            
//            T toReturn = default(T);

//            // Ensure we have a function
//            MatchCollection functionsMatches = Regex.Matches(kpiFunction, KpiFunctionHelper.functionPattern);
//            if (functionsMatches.Count == 0)
//                throw new ArgumentException("Function syntax incorrect: " + kpiFunction);

//            if(functionsMatches.Count > 1)
//                throw new ArgumentException("Multiple functions found, this is not supported: " + kpiFunction);

//            Match functionMatch = functionsMatches[0];

//            // GAKR This could be made fully generic with reflection - but for now I think this is sufficient
//            string functionName = functionMatch.Groups["FunctionName"].Value;
//            string parameterSets = functionMatch.Groups["ParameterSets"].Value;

//            object nonTypedResult = null;
//            try
//            {
//                if (KpiFunctionHelper.GoogleFunctions.Contains(functionName))
//                {
//                    if (this.googleReportingApiHelper == null)
//                        throw new Exception("Google function can't be executed because the Google Reporting Api Helper is not initialized");

//                    var googleFilter = new GoogleAnalyticsKpiRetriever.Filter { FromUtc = fromUtc, TillUtc = tillUtc };                    
//                    this.SetFilterValues(googleFilter, parameterSets);                                        
//                    actualFilterValues = JsonConvert.SerializeObject(googleFilter);

//                    switch (functionName)
//                    {
//                        case "GoogleScreenViewCount":
//                            nonTypedResult = this.googleReportingApiHelper.GetScreenViewCount(googleFilter).Result;
//                            break;
//                        case "GoogleEventCount":
//                            nonTypedResult = this.googleReportingApiHelper.GetEventCount(googleFilter).Result;
//                            break;
//                        default:
//                            throw new NotSupportedException("The following function is not supported: " + functionName);
//                    }                        
//                }
//                else if (KpiFunctionHelper.TransactionsFunctions.Contains(functionName))
//                {
//                    var transactionFilter = new TransactionsKpiRetriever.Filter { FromUtc = fromUtc, TillUtc = tillUtc };
//                    this.SetFilterValues(transactionFilter, parameterSets);                    
//                    actualFilterValues = JsonConvert.SerializeObject(transactionFilter);

//                    switch (functionName)
//                    {
//                        case "OrderCount":
//                            nonTypedResult = TransactionsKpiRetriever.GetOrderCount(transactionFilter);
//                            break;
//                        case "Revenue":
//                            nonTypedResult = TransactionsKpiRetriever.GetRevenue(transactionFilter);
//                            break;
//                        case "OrderedQuantity":
//                            nonTypedResult = TransactionsKpiRetriever.GetOrderedQuantity(transactionFilter);
//                            break;
//                        case "TopXProductsByRevenue":
//                            nonTypedResult = TransactionsKpiRetriever.GetTopXProducts(transactionFilter, TransactionsKpiRetriever.TopXProductOrder.Revenue);
//                            nonTypedResult = this.ConvertTopXProductsToString((List<TransactionsKpiRetriever.TopXProduct>)nonTypedResult);
//                            break;
//                        case "TopXProductsByQuantity":
//                            nonTypedResult = TransactionsKpiRetriever.GetTopXProducts(transactionFilter, TransactionsKpiRetriever.TopXProductOrder.Quantity);
//                            nonTypedResult = this.ConvertTopXProductsToString((List<TransactionsKpiRetriever.TopXProduct>)nonTypedResult);
//                            break;
//                        default:
//                            throw new NotSupportedException("The following function is not supported: " + functionName);
//                    }                    
//                }
//                else
//                    throw new NotImplementedException("No implementation for function: " + functionName);

//                toReturn = (T)Convert.ChangeType(nonTypedResult, typeof(T));
//            }
//            catch (Exception ex)
//            {
//                throw new Exception("Failed to process function: " + kpiFunction, ex);
//            }            

//            return toReturn;
//        }

//        private void SetFilterValues(IAnalyticsFilter filterObject, string parameterSets)
//        {
//            // Match ParameterSets
//            try
//            {
//                MatchCollection matches = Regex.Matches(parameterSets, KpiFunctionHelper.parameterSetPattern);

//                string name = string.Empty;
//                foreach (Match match in matches)
//                {
//                    try
//                    {
//                        name = "Couldn't parse name";
//                        name = match.Groups["ParameterName"].Value;
//                        string values = match.Groups["Values"].Value;

//                        if (name.Equals("FriendlyName"))
//                            continue; // Was only implemented for the Temp Analytics Gatherer.

//                        var property = filterObject.GetType().GetProperty(name);
//                        if (property == null)
//                            throw new ArgumentException("Unknown ParameterName (or invalid syntax): " + name);

//                        var propertyValue = property.GetValue(filterObject);

//                        if (propertyValue == null)
//                            throw new Exception("All properties of a Filter object must be initialized, the following was null: " + name);

//                        if (propertyValue is IList && propertyValue.GetType().IsGenericType)
//                        {
//                            Type listType = ListOfWhat(propertyValue);
//                            if (listType == typeof(int))
//                            {
//                                this.AddValuesToList(values, (List<int>)propertyValue);
//                            }
//                            else if (listType == typeof(string))
//                                this.AddValuesToList(values, (List<string>)propertyValue);
//                            else if (listType == typeof(OrderType))
//                                this.AddOrderTypesToList(values, (List<OrderType>)propertyValue);
//                            else
//                                throw new NotImplementedException(string.Format("Lists of type '{0}' are not supported.", listType.ToString()));
//                        }
//                        else
//                        {
//                            property.SetValue(filterObject, Convert.ChangeType(values, property.PropertyType, CultureInfo.InvariantCulture));
//                        }
//                    }
//                    catch (Exception ex)
//                    {
//                        throw new Exception("Failed to parse ParameterSet '{0}': {1}".FormatSafe(name, ex.Message), ex);
//                    }
//                }

//                // Add childeren of categories if requested
//                if (this.includeChildCategories && filterObject.CategoryIds.Count > 0)
//                {
//                    List<int> childCategoryIds = new List<int>();
//                    foreach (int categoryId in filterObject.CategoryIds)
//                    {
//                        var category = new CategoryEntity(categoryId);
//                        childCategoryIds.AddRange(category.GetChildrenCategoryIds());
//                    }
//                    filterObject.CategoryIds.AddRange(childCategoryIds);
//                }

//                // Validate that it's only for the Company for which the report was requested
//                if (filterObject.CompanyIds.Count != 1 || filterObject.CompanyIds[0] != this.companyId)
//                {
//                    throw new NotSupportedException("Currently you can only get results for one Company at a time and only for the one that's currently active for your session.");
//                }
//            }
//            catch (Exception ex)
//            {
//                throw new Exception("Failed to SetFilterValues: " + ex.Message, ex);
//            }
//        }        

//        private static Type ListOfWhat(Object list)
//        {
//            return ListOfWhat2((dynamic)list);
//        }

//        private static Type ListOfWhat2<T>(IList<T> list)
//        {
//            return typeof(T);
//        }

//        private void AddValuesToList<T>(string values, List<T> list)
//        {
//            if (values.Length == 0)
//                return;

//            var valuesSplit = values.Split(new char[] { ',' });
//            Type type = list.GetType().GetGenericArguments()[0];
//            foreach (var value in valuesSplit)
//            {
//                try
//                {
//                    list.Add((T)Convert.ChangeType(value, type, CultureInfo.InvariantCulture));
//                }
//                catch(Exception ex)
//                {
//                    throw new Exception(string.Format("Value '{0}' can't be parsed to {1}: '{2}'", value, type, ex.Message));
//                }
//            }            
//        }
                
//        //private void AddValuesToList(string values, List<string> list)
//        //{
//        //    if (values.Length == 0)
//        //        return;

//        //    list.AddRange(values.Split(new char[] { ',' }).Select(x => x));
//        //}

//        private void AddOrderTypesToList(string values, List<OrderType> list)
//        {
//            if (values.Length == 0)
//                return;

//            list.AddRange(values.Split(new char[] { ',' }).Select(x => (OrderType)Enum.Parse(typeof(OrderType), x))); // Don't you love one-liners?
//        }


//    }
//}
