﻿using Obymobi.Data.EntityClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Obymobi.Analytics.Reports
{
    public class ChildCategoryFetcher : IChildCategoryFetcher
    {
        public List<int> GetChildCategoryIds(List<int> parentCategories)
        {
            List<int> childCategoryIds = new List<int>();
            for (int i = 0; i < parentCategories.Count; i++)
            {
                CategoryEntity cat = new CategoryEntity(parentCategories[i]);
                childCategoryIds.AddRange(cat.GetChildrenCategoryIds());
            }
            return childCategoryIds;
        }
    }
}
