﻿using Obymobi.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Obymobi.Analytics.Reports
{
    public class KpiReportMetadata
    {
        public string Name { get; set; }
        public int? CompanyId { get; set; }
        public int UtcOffset { get; set; }
        public bool IncludeStackTracesWithErrors { get; set; }
        public bool IncludeActualFilters { get; set; }
        public string GoogleAnalyticsViewId { get; set; }

    }
}
