using NUnit.Framework;
using Obymobi.Logic.HelperClasses;

namespace Obymobi.Logic.Tests.HelperClasses
{
    [TestFixture]
    public class TagValidatorTests
    {
        [TestCase(" ,  BreAkfast  driNks34", "breakfast-drinks34")]
        [TestCase(" ,  BreAkfast driNks34 !//& ", "breakfast-drinks34")]
        [TestCase("BreAkfast / driNks34!", "breakfast-drinks34")]
        [TestCase("Drink   it4 tonight !/", "drink-it4-tonight")]
        public void FormatTagName(string input, string expected)
        {
            var result = TagHelper.FormatTagName(input);

            Assert.AreEqual(expected, result);
        }
    }
}
