using Dionysos.Web;
using NUnit.Framework;
using Obymobi.Analytics;
using Obymobi.Analytics.Google;
using Obymobi.Data.EntityClasses;
using Obymobi.Enums;
using Obymobi.Logic.HelperClasses;
using Obymobi.Logic.Tests.Builders;
using System.Collections.Generic;

namespace Obymobi.Logic.Tests.HelperClasses
{
    [TestFixture]
    public class AnalyticsHelperTests
    {
        private static class A
        {
            public static OrderEntityBuilder OrderEntity => new OrderEntityBuilder();
            public static DeliverypointEntityBuilder DeliverypointEntity => new DeliverypointEntityBuilder();
        }

        [SetUp]
        public void Init()
        {
            Dionysos.Global.ApplicationInfo = new WebApplicationInfo
            {
                ApplicationName = "CMS Unit Test",
                ApplicationVersion = "0.1"
            };

        }

        [Test]
        public void CreateHitForOrder_WithOrder_ReturnsHit()
        {
            OrderEntity orderEntity = A.OrderEntity
                .CompanyId(1)
                .CompanyName("The Hyde")
                .OrderId(10)
                .Type((int)OrderType.Standard)
                .DeliverypointEntity(A.DeliverypointEntity
                    .DeliverypointId(2)
                    .DeliverypointgroupId(100)
                    .Build())
                .DeliverypointNumber("2")
                .DeliverypointName("Room 2")
                .DeliverypointgroupName("Floor 1")
                .ClientId(1000)
                .Build();

            Hit result = AnalyticsHelper.CreateHitForOrder(orderEntity, string.Empty, string.Empty);

            Assert.NotNull(result);
            Assert.AreEqual(1, result.CompanyId);
            Assert.AreEqual("The Hyde", result.CompanyName);
            Assert.AreEqual(10, result.OrderId);
            Assert.AreEqual(2, result.DeliverypointId);
            Assert.AreEqual(2, result.DeliverypointNumber);
            Assert.AreEqual("Room 2", result.DeliverypointName);
            Assert.AreEqual(100, result.DeliverypointGroupId);
            Assert.AreEqual("Floor 1", result.DeliverypointGroupName);
            Assert.AreEqual(EventCategory.Transactions, result.EventCategory);
            Assert.AreEqual("The Hyde/Floor 1", result.EventLabel);
            Assert.AreEqual(1000, result.ClientId);
        }

        [Test]
        public void CreateHitForOrder_OrderWithPayload_ReturnsHitWithPayloadMappedToCustomDimensions()
        {
            OrderEntity orderEntity = A.OrderEntity
                .AnalyticsPayLoad("ApplicationInstallerId|-|net.craveinteractive.android.client|--|ApplicationName|-|CraveEmenu|--|ApplicationVersion|-|2021061403|--|ApplicationType|-|IrtApp|--|")
                .Build();

            Hit result = AnalyticsHelper.CreateHitForOrder(orderEntity, string.Empty, string.Empty);

            Assert.NotNull(result);
            Assert.AreEqual("CraveEmenu", result.ApplicationName);
            Assert.AreEqual("net.craveinteractive.android.client", result.ApplicationInstallerId);
            Assert.AreEqual("2021061403", result.ApplicationVersion);
            Assert.AreEqual("IrtApp", result.ApplicationType);
        }

        [Test]
        public void CreateHitForOrder_OrderWithoutDeliverypoint_DoesNotThrow()
        {
            OrderEntity orderEntity = A.OrderEntity
                .DeliverypointEntity(null)
                .Build();

            Hit result = AnalyticsHelper.CreateHitForOrder(orderEntity, string.Empty, string.Empty);

            Assert.NotNull(result);
        }

        [Test]
        public void CreateHitForOrder_OrderWithoutDeliverypoint_ReturnsWithEventLabel()
        {
            OrderEntity orderEntity = A.OrderEntity
                .CompanyName("The Hyde")
                .DeliverypointgroupName(null)
                .Build();

            Hit result = AnalyticsHelper.CreateHitForOrder(orderEntity, string.Empty, string.Empty);

            Assert.NotNull(result);
            Assert.AreEqual("The Hyde/", result.EventLabel);
        }

        [Test]
        public void CreateHitForOrder_OrderWithUnexpectedDeliverypointNumber_ReturnsHitWithDeliverypointNumberZero()
        {
            OrderEntity orderEntity = A.OrderEntity
                .DeliverypointNumber("1A")
                .DeliverypointEntity(null)
                .Build();

            Hit result = AnalyticsHelper.CreateHitForOrder(orderEntity, string.Empty, string.Empty);

            Assert.NotNull(result);
            Assert.AreEqual(0, result.DeliverypointNumber);
        }

        [Test]
        public void GetTrackingIds_WithoutTrackingId_ReturnsEmptyList()
        {
            OrderEntity orderEntity = A.OrderEntity
                .AnalyticsTrackingIds("")
                .Build();

            List<string> result = AnalyticsHelper.GetTrackingIds(orderEntity);

            Assert.IsEmpty(result);
        }

        [Test]
        public void GetTrackingIds_WithSingleTrackingId_ReturnsListWithSingleTrackingId()
        {
            OrderEntity orderEntity = A.OrderEntity
                .AnalyticsTrackingIds("1234abcd")
                .Build();

            List<string> result = AnalyticsHelper.GetTrackingIds(orderEntity);

            Assert.AreEqual(1, result.Count);
            Assert.Contains("1234abcd", result);
        }

        [Test]
        public void GetTrackingIds_WithMultipleTrackingIds_ReturnsListWithMultipleTrackingIds()
        {
            OrderEntity orderEntity = A.OrderEntity
                .AnalyticsTrackingIds("1234abcd;5678efgh;")
                .Build();

            List<string> result = AnalyticsHelper.GetTrackingIds(orderEntity);

            Assert.AreEqual(2, result.Count);
            Assert.Contains("1234abcd", result);
            Assert.Contains("5678efgh", result);
        }
    }
}
