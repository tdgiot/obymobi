using Obymobi.Data.EntityClasses;

namespace Obymobi.Logic.Tests.Builders
{
    public class OrderEntityBuilder
    {
        public OrderEntityBuilder() : this(new MockData()) { }

        private OrderEntityBuilder(MockData data) { Data = data; }

        private MockData Data { get; }

        public OrderEntityBuilder OrderId(int orderId) => new OrderEntityBuilder(new MockData(Data) { OrderId = orderId });
        public OrderEntityBuilder CompanyId(int companyId) => new OrderEntityBuilder(new MockData(Data) { CompanyId = companyId });
        public OrderEntityBuilder CompanyName(string companyName) => new OrderEntityBuilder(new MockData(Data) { CompanyName = companyName });
        public OrderEntityBuilder Type(int orderType) => new OrderEntityBuilder(new MockData(Data) { Type = orderType });
        public OrderEntityBuilder DeliverypointNumber(string deliverypointNumber) => new OrderEntityBuilder(new MockData(Data) { DeliverypointNumber = deliverypointNumber });
        public OrderEntityBuilder DeliverypointName(string deliverypointName) => new OrderEntityBuilder(new MockData(Data) { DeliverypointName = deliverypointName });
        public OrderEntityBuilder DeliverypointgroupName(string deliverypointgroupName) => new OrderEntityBuilder(new MockData(Data) { DeliverypointgroupName = deliverypointgroupName });
        public OrderEntityBuilder AnalyticsTrackingIds(string analyticsTrackingIds) => new OrderEntityBuilder(new MockData(Data) { AnalyticsTrackingIds = analyticsTrackingIds });
        public OrderEntityBuilder AnalyticsPayLoad(string analyticsPayLoad) => new OrderEntityBuilder(new MockData(Data) { AnalyticsPayLoad = analyticsPayLoad });
        public OrderEntityBuilder ClientId(int clientId) => new OrderEntityBuilder(new MockData(Data) { ClientId = clientId });
        public OrderEntityBuilder DeliverypointEntity(DeliverypointEntity deliverypointEntity) => new OrderEntityBuilder(new MockData(Data) { DeliverypointEntity = deliverypointEntity });


        public OrderEntity Build()
        {
            var order = new OrderEntity
            {
                CompanyId = this.Data.CompanyId,
                CompanyName = this.Data.CompanyName,
                OrderId = this.Data.OrderId,
                Type = this.Data.Type,
                DeliverypointNumber = this.Data.DeliverypointNumber,
                DeliverypointName = this.Data.DeliverypointName,
                DeliverypointgroupName = this.Data.DeliverypointgroupName,
                AnalyticsTrackingIds = this.Data.AnalyticsTrackingIds,
                AnalyticsPayLoad = this.Data.AnalyticsPayLoad,
                ClientId = this.Data.ClientId
            };

            if (Data.DeliverypointEntity != null)
            {
                order.DeliverypointEntity = Data.DeliverypointEntity;
                order.DeliverypointId = Data.DeliverypointEntity.DeliverypointId;
            }

            order.IsNew = false;
            order.IsDirty = false;

            return order;
        }

        private class MockData
        {
            // Default values
            public MockData()
            {

            }

            public MockData(MockData data)
            {
                OrderId = data.OrderId;
                CompanyId = data.CompanyId;
                CompanyName = data.CompanyName;
                Type = data.Type;
                DeliverypointNumber = data.DeliverypointNumber;
                DeliverypointName = data.DeliverypointName;
                DeliverypointEntity = data.DeliverypointEntity;
                DeliverypointgroupName = data.DeliverypointgroupName;
                AnalyticsTrackingIds = data.AnalyticsTrackingIds;
                AnalyticsPayLoad = data.AnalyticsPayLoad;
                ClientId = data.ClientId;
            }

            public int OrderId { get; set; }
            public int CompanyId { get; set; }
            public string CompanyName { get; set; }
            public int Type { get; set; }
            public string DeliverypointNumber { get; set; }
            public string DeliverypointName { get; set; }
            public DeliverypointEntity DeliverypointEntity { get; set; }
            public string DeliverypointgroupName { get; set; }
            public string AnalyticsTrackingIds { get; set; }
            public string AnalyticsPayLoad { get; set; }
            public int ClientId { get; set; }
        }
    }
}