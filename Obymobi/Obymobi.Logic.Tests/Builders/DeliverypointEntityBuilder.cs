using Obymobi.Data.EntityClasses;

namespace Obymobi.Logic.Tests.Builders
{
    public class DeliverypointEntityBuilder
    {
        public DeliverypointEntityBuilder() : this(new MockData()) { }

        private DeliverypointEntityBuilder(MockData data) { Data = data; }

        private MockData Data { get; }

        public DeliverypointEntityBuilder DeliverypointId(int deliverypointId) => new DeliverypointEntityBuilder(new MockData(Data) { DeliverypointId = deliverypointId });
        public DeliverypointEntityBuilder DeliverypointgroupId(int deliverypointgroupId) => new DeliverypointEntityBuilder(new MockData(Data) { DeliverypointgroupId = deliverypointgroupId });

        public DeliverypointEntity Build()
        {
            DeliverypointEntity deliverypointEntity = new DeliverypointEntity
            {
                DeliverypointId = this.Data.DeliverypointId,
                DeliverypointgroupId = this.Data.DeliverypointgroupId
            };

            deliverypointEntity.IsNew = false;
            deliverypointEntity.IsDirty = false;

            return deliverypointEntity;
        }

        private class MockData
        {
            // Default values
            public MockData()
            {

            }

            public MockData(MockData data)
            {
                DeliverypointId = data.DeliverypointId;
                DeliverypointgroupId = data.DeliverypointgroupId;
            }

            public int DeliverypointId { get; set; }
            public int DeliverypointgroupId { get; set; }
        }
    }
}