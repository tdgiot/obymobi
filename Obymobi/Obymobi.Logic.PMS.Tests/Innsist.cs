using Dionysos;
using NUnit.Framework;
using Obymobi.Integrations.PMS.Innsist;
using Obymobi.Logic.Model;
using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace Obymobi.Logic.PMS.Tests
{
    [TestFixture, Explicit]
    public class Innsist
    {
        private TCAPosConnector connector;

        private string lastErrorMessage = string.Empty;

        [OneTimeSetUp]
        public void Initialize()
        {
            TCAPosConfgurationAdapter config = new TCAPosConfgurationAdapter
            {
                FromSystemId = "ACME",
                Username = "SandBox",
                Password = "WEEkpNJPhAwK",
                ToSystemId = "001",
                WebserviceUrl = "http://216.87.172.219:8587/",
                WebserviceToBeCalledOnUrl = "http://localhost:1234/"
            };
            this.connector = new TCAPosConnector(config);
            this.connector.Logged += connector_Logged;
        }

        void connector_Logged(object sender, LoggingLevel level, string log)
        {
            Debug.WriteLine(log);

            if (level == LoggingLevel.Error)
                this.lastErrorMessage = log;
        }

        [SetUp]
        public void PreTestReset()
        {
            this.lastErrorMessage = string.Empty;
        }

        [Test]
        public void Ping()
        {
            string result = this.connector.Ping();
            Assert.AreEqual(result, "Hello Crave!", this.lastErrorMessage);
        }

        [Test]
        public void GuestInformation()
        {
            List<Model.GuestInformation> guestInfoList = this.connector.GetGuestInformation("1001");
            Assert.Greater(guestInfoList.Count, 0, this.lastErrorMessage);
            Assert.IsNull(guestInfoList[0].Error, "Error message: {0}", guestInfoList[0].Error);
        }

        [Test]
        public void GetFolio()
        {
            List<Model.Folio> folioList = this.connector.GetFolio("1001");
            Assert.Greater(folioList.Count, 0, this.lastErrorMessage);
            Assert.IsNull(folioList[0].Error, "Error message: {0}", folioList[0].Error);
        }

        [Test]
        public void GetBalance()
        {
            decimal balance = this.connector.GetBalance("1001");
            Assert.Greater(balance, 0, this.lastErrorMessage);
        }

        [Test]
        public void Checkout()
        {
            bool result = this.connector.Checkout(new Checkout { DeliverypointNumber = "2204" });
            Assert.IsTrue(result, this.lastErrorMessage);
        }

        [Test]
        public void AddToFolio()
        {
            bool result = this.connector.AddToFolio("1001", new List<FolioItem>
                                                            {
                                                                new FolioItem
                                                                {
                                                                    Code = "Crave1",
                                                                    Description = "Added by Crave test",
                                                                    Created = DateTime.Now,
                                                                    PriceIn = (decimal)9.00,
                                                                    CurrencyCode = "MXP"
                                                                }
                                                            });
            Assert.IsTrue(result, this.lastErrorMessage);
        }

        [Test]
        public void SetWakeUpStatus()
        {
            bool result = this.connector.SetWakeUpStatus(new WakeUpStatus
            {
                DeliverypointNumber = "1001",
                WakeUpDate = new DateTime(2016, 04, 31),
                WakeUpTime = new DateTime(2016, 04, 31, 6, 40, 0)
            });
            Assert.True(result, this.lastErrorMessage);
        }
    }
}
