﻿using System;
using Dionysos.Web;

public partial class _Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (QueryStringHelper.HasValue("perfcheck") && TestUtil.IsPcDeveloper)
        {
            Response.Clear();
            Response.ContentType = "text/plain";
            Response.StatusCode = 200;

            var perf = new Test.PongPerformance();

            int clients = 0;
            var elapsed = perf.RegularThreaded(ref clients);
            Response.Write(string.Format("Regular: {0} | Clients: {1}\n", elapsed, clients));

            clients = 0;
            elapsed = perf.ProducerConsumer(ref clients);
            Response.Write(string.Format("P/C: {0} | Clients: {1}\n", elapsed, clients));
        }
        else if (QueryStringHelper.HasValue("restart-scale-out-queue"))
        {
            CometServer.Instance.RecreateScaleOutQueue();
            this.Response.Write("ScaleOutQueue recreated, check logs if success");
        }
    }
}