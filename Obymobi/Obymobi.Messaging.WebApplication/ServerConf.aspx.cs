﻿using Dionysos;
using System;
using System.Text;
using System.Text.RegularExpressions;
using System.Web.UI;
using Dionysos.Web;
using Obymobi.Configuration;

public partial class ServerConf : Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!QueryStringHelper.HasValue("showmeall"))
        {
            Response.Clear();
            Response.StatusCode = 404;
            Response.End();
        }

        if (QueryStringHelper.HasValue("identifier") && QueryStringHelper.HasValue("delete"))
        {
            string identifier = QueryStringHelper.GetString("identifier");
            if (ValidateMac(ref identifier))
            {
                Tracer.Instance.StopTracing(identifier);
                Response.Redirect("ServerConf.aspx?showmeall=1");
            }
        }
        
        this.btnUpdateLoggingLevel.Click += btnUpdateLoggingLevel_Click;
        
        this.btnPongQueue.Click += btnPongQueue_Click;
        this.btnPongRegular.Click += btnPongRegular_Click;
        this.btnAddTrace.Click += btnAddTrace_Click;

        if (!IsPostBack)
        {
            UpdatePongButtons();
            UpdateLogginLevel();
        }

        ShowTraces();
    }

    private void UpdatePongButtons()
    {
        bool usePongQueue = ConfigurationManager.GetBool(CraveCometConfigConstants.USE_PONG_QUEUE);
        this.btnPongQueue.Enabled = !usePongQueue;
        this.btnPongRegular.Enabled = usePongQueue;
    }

    private void SwitchPongHandler(bool useQueue)
    {
        ConfigurationManager.SetValue(CraveCometConfigConstants.USE_PONG_QUEUE, useQueue);
        Global.Log("CometServer", "Switched pong handler. Using Queue: {0}", useQueue);
        UpdatePongButtons();
    }

    private void UpdateLogginLevel()
    {
        int loggingLevel = ConfigurationManager.GetInt(CraveCometConfigConstants.LOGGING_LEVEL);
        if (loggingLevel == (int)LoggingLevel.Verbose)
            this.ddlLoggingLevel.SelectedIndex = 1;
        else if (loggingLevel == (int)LoggingLevel.Info)
            this.ddlLoggingLevel.SelectedIndex = 2;
        else
            this.ddlLoggingLevel.SelectedIndex = 0;
    }

    private void ShowTraces()
    {
        var traces = Tracer.Instance.TraceList;
        var sb = new StringBuilder();
        if (traces.Count == 0)
            sb.Append("<li>None</li>");

        foreach (var identifier in traces)
        {
            sb.Append(string.Format("<li>{0} <a href='?showmeall=1&identifier={0}&delete=1'>[Delete]</a></li>", identifier));
        }

        this.plhTraces.Controls.Clear();
        this.plhTraces.Controls.Add(new LiteralControl(sb.ToString()));
    }

    private bool ValidateMac(ref string input)
    {
        input = input.Replace(" ", "").Replace(":", "").Replace("-", "");
        Regex r = new Regex("^[a-fA-F0-9]{12}$");
        if (r.IsMatch(input))
        {
            string newMac = input;
            for (int i = 2; i < 17; i += 3)
            {
                newMac = newMac.Insert(i, ":");
            }
            input = newMac;

            return true;
        }

        return false;
    }

    #region EventHandlers

    void btnUpdateLoggingLevel_Click(object sender, EventArgs e)
    {
        LoggingLevel loggingLevel;
        if (Enum.TryParse(this.ddlLoggingLevel.SelectedValue, out loggingLevel))
        {
            ConfigurationManager.SetValue(CraveCometConfigConstants.LOGGING_LEVEL, (int)loggingLevel);
            UpdateLogginLevel();
        }
    }

    void btnPongRegular_Click(object sender, EventArgs e)
    {
        SwitchPongHandler(false);
    }

    void btnPongQueue_Click(object sender, EventArgs e)
    {
        SwitchPongHandler(true);
    }

    void btnAddTrace_Click(object sender, EventArgs e)
    {
        if (this.tbTraceIdentifier.Value.IsNullOrWhiteSpace())
            return;

        string macAddr = this.tbTraceIdentifier.Value;
        if (!ValidateMac(ref macAddr))
            return;

        Tracer.Instance.StartTracing(macAddr);
        ShowTraces();
    }

    #endregion
}