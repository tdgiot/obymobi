﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="ServerConf" Codebehind="ServerConf.aspx.cs" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style>
        div {
            padding: 10px;
        }
        .aspNetHidden {
            padding: 0;
        }
    </style>
</head>
<body>
    
    <form id="form1" runat="server">
        <h2>Messaging Config</h2>
        <div>
            Logging Level: <asp:DropDownList runat="server" ID="ddlLoggingLevel" AppendDataBoundItems="true">
                <asp:ListItem Text="Debug" Value="400" />
                <asp:ListItem Text="Verbose" Value="300" />
                <asp:ListItem Text="Info" Value="200" />
            </asp:DropDownList>&nbsp;<asp:Button runat="server" ID="btnUpdateLoggingLevel" Text="Update"/>
        </div>
        <div>
            Pong Message Handling: <asp:Button id="btnPongRegular" Text="Regular" runat="server" />&nbsp;<asp:Button id="btnPongQueue" Text="Queue" runat="server" />
        </div>
        
        <h2>Tracing</h2>
        <ul>
            <asp:PlaceHolder ID="plhTraces" runat="server"/>
        </ul>
        <div>
            Identifier: <input type="text" id="tbTraceIdentifier" runat="server"/>&nbsp;<asp:Button runat="server" ID="btnAddTrace" Text="Start Tracing"/>
        </div>
    </form>
</body>
</html>
