using System;
using System.Diagnostics;
using System.IO;
using System.Threading.Tasks;
using System.Web;
using System.Web.Hosting;
using Dionysos;
using Dionysos.Configuration;
using Dionysos.Data;
using Dionysos.Data.LLBLGen;
using Dionysos.Verification;
using Dionysos.Web;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Transports;
using Obymobi;
using Obymobi.Configuration;
using Obymobi.Constants;
using Obymobi.Data.EntityClasses;
using Obymobi.Enums;
using Obymobi.Logic.Comet;
using Obymobi.Logic.HelperClasses;
using Obymobi.Web;

/// <summary>
///     Summary description for Global
/// </summary>
public class Global : HttpApplicationSystemBased
{
    #region  Fields

    private static InfiniteLooper pingPongThread;
    private static InfiniteLooper loggingCleanupThread;
    private static InfiniteLooper netmessagePollerThread;
    //private static InfiniteLooper newRelicDataGathererThread;
    private static readonly InfiniteLooper pongerThread;
    //private static InfiniteLooper signalRCleanThread;

    #endregion

    public override void DatabaseIndependentPreInitialization()
    {
        Global.Log("CometServer", "DatabaseIndependentPreInitialization");

        TaskScheduler.UnobservedTaskException += ((s, arg) =>
                                                  {
                                                      // Marks the Exception as "observed," thus preventing it from triggering exception escalation policy which, by default, terminates the process.
                                                      arg.SetObserved();
                                                      (arg.Exception).Handle(ex =>
                                                                             {
                                                                                 // Write exception to seperate log file
                                                                                 Global.Log(string.Format("Exception-{0:HH:mm:ss.fff}", DateTime.Now), "{0}\n\r{1}", ex.Message, ex.ProcessStackTrace(true));
                                                                                 return true;
                                                                             });
                                                  });

        // Set the application information
        Dionysos.Global.ApplicationInfo = new WebApplicationInfo
        {
            ApplicationName = "CometServer",
            BasePath = this.Server.MapPath("~"),
            ApplicationVersion = "1.2019103101"
        };

        // Set the configuration provivder
        Dionysos.Global.ConfigurationProvider = new LLBLGenConfigurationProvider();

        // Set the data providers
        DataFactory.EntityCollectionFactory = new LLBLGenEntityCollectionFactory();
        DataFactory.EntityFactory = new LLBLGenEntityFactory();
        DataFactory.EntityUtil = new LLBLGenEntityUtil();

        // Set the configuration definitions
        Dionysos.Global.ConfigurationInfo.Add(new DionysosConfigurationInfo());
        Dionysos.Global.ConfigurationInfo.Add(new DionysosWebConfigurationInfo());
        Dionysos.Global.ConfigurationInfo.Add(new ObymobiConfigInfo());
        Dionysos.Global.ConfigurationInfo.Add(new ObymobiDataConfigInfo());
        Dionysos.Global.ConfigurationInfo.Add(new CraveCometConfigInfo());

        // Initialize the assembly information
        Dionysos.Global.AssemblyInfo.Add(this.Server.MapPath("~/Bin/Obymobi.Data.dll"), "Data");

        VerifierCollection verifiers = new VerifierCollection
        {
            new DependencyInjectionVerifier(),
            new AppSettingsVerifier(DionysosConfigurationConstants.BaseUrl),
            new AppSettingsVerifier(DionysosConfigurationConstants.CloudEnvironment),

            new WritePermissionVerifier(HostingEnvironment.MapPath("~/App_Data/Logs/")),
            new WritePermissionVerifier(HostingEnvironment.MapPath("~/App_Data/Logs/Traces/"))
        };

        if (!verifiers.Verify())
        {
            throw new VerificationException(verifiers.ErrorMessage);
        }

        // Comet Helper
        CometHelper.SetCometProvider(new CometServerProvider());

        #region SignalR

        GlobalHost.Configuration.DisconnectTimeout = new TimeSpan(0, 0, 60);
        GlobalHost.Configuration.KeepAlive = new TimeSpan(0, 0, 20);

        GlobalHost.Configuration.TransportConnectTimeout = new TimeSpan(0, 0, 15);
        GlobalHost.Configuration.MaxIncomingWebSocketMessageSize = 64 * 1024;

        if (GlobalHost.DependencyResolver.Resolve<ITransportManager>() is TransportManager manager)
        {
            Global.Log("CometServer", "DatabaseIndependentPreInitialization - Removing transport methods");

            manager.Remove("foreverFrame");
            manager.Remove("serverSentEvents");
            manager.Remove("longPolling");
        }

        Global.Log("CometServer", "DatabaseIndependentPreInitialization - Done");

        #endregion

        // Call instance to create it
        CometServer.Instance.TotalActiveDeviceConnections();
    }

    public override void DatabaseDependentInitialization()
    {
        Global.Log("CometServer", "DatabaseDependentInitialization");

        // Set locking method
        SD.LLBLGen.Pro.DQE.SqlServer.DynamicQueryEngine.UseNoLockHintOnSelects = true;

        // Update the version
        CloudApplicationVersionHelper.SetCloudApplicationVersion(CloudApplication.Messaging, Dionysos.Global.ApplicationInfo.ApplicationVersion);

        if (WebEnvironmentHelper.ORMProfilerEnabled)
        {
            EnableDatabaseProfiler();
        }

        // Initialize various loopers
        Global.InitializeLoopers();
    }

    protected override void Application_Error(Exception ex)
    {
        string errorMessage = ex.ProcessStackTrace(true);

        try
        {
            // Report to RequestLog
            RequestlogEntity requestLog = new RequestlogEntity
            {
                SourceApplication = "CometServer",
                MethodName = "Application Error",
                ResultEnumValueName = "UnhandledException",
                ResultEnumTypeName = "UnhandledException",
                ErrorMessage = errorMessage,
                ResultMessage = "Error"
            };
            requestLog.Save();
        }
        catch
        {
            // ignored
        }

        // Write error to disk
        Global.Log(string.Format("UnhandledException-{0:HH:mm:ss.fff}", DateTime.Now), "{0}\n\r{1}", ex.Message, errorMessage);
    }

    public void Application_End()
    {
        Global.Log("CometServer", "Application_End - START");

        if (Global.pingPongThread != null)
        {
            Global.Log("CometServer", "Pinger - Stopping");
            Global.pingPongThread.Stop();
            Global.pingPongThread = null;
        }

        if (Global.loggingCleanupThread != null)
        {
            Global.Log("CometServer", "Stats Logging Cleanup - Stopping");
            Global.loggingCleanupThread.Stop();
            Global.loggingCleanupThread = null;
        }

        if (Global.netmessagePollerThread != null)
        {
            Global.Log("CometServer", "NetmessagePoller - Stopping");
            Global.netmessagePollerThread.Stop();
            Global.netmessagePollerThread = null;
        }

        /*if (Global.pongerThread != null)
        {
            Global.Log("CometServer", "Ponger - Stopping");
            Global.pongerThread.Stop();
            Global.pongerThread = null;
        }*/

        CometServer.Instance.Dispose();

        Global.Log("CometServer", "Application_End - FINISHED\n==================================================");
    }

    private static void InitializeLoopers()
    {
        HttpContext httpContext = HttpContext.Current;

        if (httpContext == null)
        {
            Global.Log("CometServer", "HttpContext is null");
        }

        // Initialize Ping-Pong Looper
        if (Global.pingPongThread == null)
        {
            Global.Log("CometServer", "PingPong - Initializing");
            Global.pingPongThread = InfiniteLooper.StartNew(() => HttpContext.Current = httpContext,
                                                            () =>
                                                            {
                                                                Global.Log("CometServer", "PingPong - Starting Run");
                                                                CometServer.Instance.PingClients();
                                                            },
                                                            10000,
                                                            15000,
                                                            Global.PingPong_OnExecutionRunCompleted,
                                                            Global.PingPong_ExceptionHandler);
        }

        /*if (Global.pongerThread == null)
        {
            Global.Log("CometServer", "Ponger - Initializing");
            Global.pongerThread = InfiniteLooper.StartNew(() => HttpContext.Current = httpContext,
                                                          () =>
                                                          {
                                                              Global.Log("CometServer", "Ponger - Starting");
                                                              CometServer.Instance.HandlePongs();
                                                          },
                                                          15000,
                                                          60000,
                                                          Global.Ponger_OnExecutionRunCompleted,
                                                          Global.Ponger_ExceptionHandler);
        }*/

        // Netmessage Poller
        if (Global.netmessagePollerThread == null)
        {
            Global.Log("CometServer", "NetmessagePoller - Initializing");
            Global.netmessagePollerThread = InfiniteLooper.StartNew(() => HttpContext.Current = httpContext,
                                                                    () =>
                                                                    {
                                                                        NetmessagePoller.CheckNonHandledNetmessages();
                                                                    },
                                                                    ObymobiIntervals.COMETSERVER_NETMESSAGEPOLLER_INTERVAL,
                                                                    ObymobiIntervals.COMETSERVER_NETMESSAGEPOLLER_TIMEOUT,
                                                                    Global.NetmessagePoller_OnExecutionRunCompleted,
                                                                    Global.NetmessagePoller_OnExceptionHandler);
        }
    }

    #region Infinite looper Handlers

    private static void PingPong_ExceptionHandler(Exception exception)
    {
        Global.Log("CometServer", "Pinger - Exception: {0}", exception.Message);
        Global.Log("CometServer", "Pinger - StackTrace:\n{0}", exception.ProcessStackTrace(true));

        //NewRelic.Api.Agent.NewRelic.NoticeError(exception);

        try
        {
            // Report to RequestLog
            RequestlogEntity requestLog = new RequestlogEntity
            {
                SourceApplication = "CometServer",
                MethodName = "CometServer.Instance.PingClients",
                ResultEnumValueName = "Exception",
                ResultEnumTypeName = "Exception",
                ErrorMessage = string.Format("{0}\n\r{1}", exception.Message, exception.ProcessStackTrace(true)),
                ResultMessage = "Failed"
            };
            requestLog.Save();
        }
        catch
        {
        }

        // Restart
        Global.pingPongThread.Stop();
        Global.pingPongThread.Start();
    }

    private static void PingPong_OnExecutionRunCompleted(TimeSpan executionTime)
    {
        Global.Log("CometServer", "Pinger - Finished in {0} milliseconds.", executionTime.TotalMilliseconds);
        //NewRelic.Api.Agent.NewRelic.RecordResponseTimeMetric(NewRelicConstants.NEWRELIC_MESSAGING_PING_METRIC, (long)executionTime.TotalMilliseconds);

        if (executionTime.TotalSeconds > 5)
        {
            try
            {
                // Report to RequestLog
                RequestlogEntity requestLog = new RequestlogEntity
                {
                    SourceApplication = "CometServer",
                    MethodName = "CometServer.Instance.PingClients",
                    ResultEnumValueName = "LongExecution",
                    ResultEnumTypeName = "LongExecution",
                    ErrorMessage = string.Format("Execution took: {0} seconds.", executionTime.TotalSeconds),
                    ResultMessage = "Completed"
                };
                requestLog.Save();
            }
            catch
            {
            }
        }
    }

    private static void Ponger_ExceptionHandler(Exception exception)
    {
        Global.Log("CometServer", "Ponger - Exception: {0}", exception.Message);
        Global.Log("CometServer", "Ponger - StackTrace:\n{0}", exception.ProcessStackTrace(true));

        //NewRelic.Api.Agent.NewRelic.NoticeError(exception);

        try
        {
            // Report to RequestLog
            RequestlogEntity requestLog = new RequestlogEntity
            {
                SourceApplication = "CometServer",
                MethodName = "CometServer.Instance.Ponger",
                ResultEnumValueName = "Exception",
                ResultEnumTypeName = "Exception",
                ErrorMessage = string.Format("{0}\n\r{1}", exception.Message, exception.ProcessStackTrace(true)),
                ResultMessage = "Failed"
            };
            requestLog.Save();
        }
        catch
        {
        }

        // Restart
        Global.pongerThread.Stop();
        Global.pongerThread.Start();
    }

    private static void Ponger_OnExecutionRunCompleted(TimeSpan executionTime)
    {
        Global.Log("CometServer", "Ponger - Finished in {0} milliseconds.", executionTime.TotalMilliseconds);

        if (executionTime.TotalSeconds > 50)
        {
            try
            {
                // Report to RequestLog
                RequestlogEntity requestLog = new RequestlogEntity
                {
                    SourceApplication = "CometServer",
                    MethodName = "CometServer.Instance.Ponger",
                    ResultEnumValueName = "LongExecution",
                    ResultEnumTypeName = "LongExecution",
                    ErrorMessage = string.Format("Execution took: {0} seconds.", executionTime.TotalSeconds),
                    ResultMessage = "Completed"
                };
                requestLog.Save();
            }
            catch
            {
            }
        }
    }

    private static void NetmessagePoller_OnExecutionRunCompleted(TimeSpan executionTime)
    {
        Global.Log("CometServer", "NetmessagePoller - Finished in {0} milliseconds.", executionTime.TotalMilliseconds);
        //NewRelic.Api.Agent.NewRelic.RecordResponseTimeMetric(NewRelicConstants.NEWRELIC_MESSAGING_NETMESSAGE_METRIC, (long)executionTime.TotalMilliseconds);

        if (executionTime.TotalSeconds > 5)
        {
            try
            {
                // Report to RequestLog
                RequestlogEntity requestLog = new RequestlogEntity
                {
                    SourceApplication = "CometServer",
                    MethodName = "NetmessagePoller.CheckNonHandledNetmessages",
                    ResultEnumValueName = "LongExecution",
                    ResultEnumTypeName = "LongExecution",
                    ErrorMessage = string.Format("Execution took: {0} seconds.", executionTime.TotalSeconds),
                    ResultMessage = "Completed"
                };
                requestLog.Save();
            }
            catch
            {
            }
        }
    }

    private static void NetmessagePoller_OnExceptionHandler(Exception exception)
    {
        Global.Log("CometServer", "NetmessagePoller - Exception: {0}", exception.Message);
        Global.Log("CometServer", "NetmessagePoller - StackTrace:\n{0}", exception.ProcessStackTrace(true));

        try
        {
            // Report to RequestLog
            RequestlogEntity requestLog = new RequestlogEntity
            {
                SourceApplication = "CometServer",
                MethodName = "NetmessagePoller.CheckNonHandledNetmessages",
                ResultEnumValueName = "Exception",
                ResultEnumTypeName = "Exception",
                ErrorMessage = string.Format("{0}\n\r{1}", exception.Message, exception.ProcessStackTrace(true)),
                ResultMessage = "Failed"
            };
            requestLog.Save();
        }
        catch
        {
        }

        // Restart
        Global.netmessagePollerThread.Stop();
        Global.netmessagePollerThread.Start();
    }

    #endregion

    #region Dirty Loggger

    private static DateTime lastCleanup = DateTime.MinValue;
    private static readonly object logLock = new object();

    public static void Log(string logname, string format, params object[] args)
    {
        string textToWrite = format.FormatSafe(args);
        textToWrite = DateTime.Now.ToString("[HH:mm:ss] ") + textToWrite;

        try
        {
            lock (Global.logLock)
            {
                string fileName = logname + "-" + DateTime.Now.ToString("yyyyMMdd") + ".log";
                string path = HostingEnvironment.MapPath("~/App_Data/Logs/");
                string filepath = Path.Combine(path, fileName);

                using (StreamWriter writer = File.AppendText(filepath))
                {
                    writer.Write(textToWrite);
                    writer.Write("\n");
                }

                if (Global.lastCleanup.TotalSecondsToNow() > 600)
                {
                    // Clean up
                    string[] files = Directory.GetFiles(path, logname + "*");
                    foreach (string file in files)
                    {
                        FileInfo fi = new FileInfo(file);
                        if ((DateTime.Now - fi.LastWriteTime).Days > 14)
                        {
                            File.Delete(file);
                        }
                    }

                    Global.lastCleanup = DateTime.Now;
                }
            }
        }
        catch (Exception ex)
        {
            try
            {
                // Report to RequestLog
                RequestlogEntity requestLog = new RequestlogEntity
                {
                    SourceApplication = "CometServer",
                    MethodName = "Global.Log",
                    ResultEnumValueName = "LogWriteFailed",
                    ResultEnumTypeName = "LogWriteFailed",
                    ErrorMessage = string.Format("Message: {0}, Exception: {1}", textToWrite, ex.Message),
                    ResultMessage = "Exception"
                };
                requestLog.Save();
            }
            catch
            {
            }

            Debug.WriteLine("Writing to log file failed: {0}".FormatSafe(ex.Message));
        }
    }

    #endregion
}
