﻿using System.IO;
using Dionysos;
using Dionysos.Web;
using Obymobi;
using Obymobi.Constants;
using Obymobi.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Obymobi.Logging;

public partial class ClearCache : System.Web.UI.Page
{
    private const bool DEBUG_MODE = false;

    public enum ClearCacheResult
    {
        MissingParameters = 200,
        RequestHashIsInvalid = 300,
        TimestampIsOutOfRange = 400
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        Response.Clear();
        Response.ContentType = "text/plain";

        if (QueryStringHelper.HasValue("lastcacheclear"))
        {
            Response.Write(CacheHelper.GetLastCacheCleared());
            Response.StatusCode = 200;
        }
        else if (QueryStringHelper.HasValue("appstarted"))
        {
            string appStarted;
            if (HttpContext.Current.Application["APPLICATION_STARTED"] == null)
                appStarted = "Never";
            else
                appStarted = (string)HttpContext.Current.Application["APPLICATION_STARTED"];

            Response.Write(appStarted);
            Response.StatusCode = 200;
        }
        else if (ProcessRequest())
        {
            Response.Write("OK");
            Response.StatusCode = 200;
        }
        else
        {
            Response.Write("FAILED");
            Response.StatusCode = 500;
        }

        Response.End();
    }

    private bool ProcessRequest()
    {
        int timestamp = 0;
        string request = "";
        string hash = string.Empty;

        if (!QueryStringHelper.TryGetValue("timestamp", out timestamp))
        {
            if (DEBUG_MODE)
                throw new ObymobiException(ClearCacheResult.MissingParameters, "timestamp");
            else
                return false;
        }

        QueryStringHelper.TryGetValue("request", out request);
        if (request == null)
            request = "";

        if (!QueryStringHelper.TryGetValue("hash", out hash))
        {
            if (DEBUG_MODE)
                throw new ObymobiException(ClearCacheResult.MissingParameters, "hash");
            else
                return false;
        }

        if (Hasher.IsHashValidForParameters(hash, ObymobiConstants.GenericSalt, timestamp, request))
        {
            DateTime requestDateTime = DateTimeUtil.FromUnixTime(timestamp);
            TimeSpan requestAge = (DateTime.UtcNow - requestDateTime);
            if (requestAge.TotalMinutes > 5 || requestAge.TotalMinutes < -5)
            {
                if (DEBUG_MODE)
                    throw new ObymobiException(ClearCacheResult.TimestampIsOutOfRange, "Timestamp is: '{0}', Age: '{1}m' - Server time: '{2}'", requestDateTime, requestAge.ToString(), DateTime.UtcNow);

                return false;
            }

            if (request.IsNullOrWhiteSpace())
            {
                Requestlogger.CreateRequestlog("ClearCache", timestamp, hash);
                CacheHelper.Clear();
            }
            else if (request.Equals("recycle"))
            {
                Requestlogger.CreateRequestlog("RecycleApplication", timestamp, request, hash);

                return RecycleApplication();
            }
        }
        else
        {
            if (DEBUG_MODE)
                throw new ObymobiException(ClearCacheResult.RequestHashIsInvalid, "The supplied hash is invalid: '{0}' for '{1}'.", hash, timestamp);

            return false;
        }

        return true;
    }

    private bool RecycleApplication()
    {
        bool error = false;
        try
        {
            // This requires full trust so this will fail in many scenarios
            HttpRuntime.UnloadAppDomain();
        }
        catch
        {
            error = true;
        }

        if (error)
        {
            // Couldn't unload with Runtime - let's try modifying web.config
            string configPath = HttpContext.Current.Request.PhysicalApplicationPath + "\\web.config";
            try
            {
                File.SetLastWriteTimeUtc(configPath, DateTime.UtcNow);
            }
            catch
            {
                error = true;
            }
        }

        return !error;
    }
}
