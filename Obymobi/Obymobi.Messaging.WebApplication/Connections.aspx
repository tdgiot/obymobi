﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="Connections" Codebehind="Connections.aspx.cs" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
<h1>Clients</h1>
    <table>
       <tr>
            <td width="400"><strong>MacAddress</strong></td>
            <td width="100"><strong>Comet ID</strong></td>
            <td><strong>IP Address</strong></td>
           <td><strong>Client Id</strong></td>
           <td><strong>Terminal Id</strong></td>
           <td><strong>Deliverypoint Id</strong></td>
        </tr>
        <asp:PlaceHolder ID="plhClients" runat="server" />
    </table>
</body>
</html>
