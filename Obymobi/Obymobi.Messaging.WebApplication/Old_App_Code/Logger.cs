﻿using System;
using Dionysos;
using Obymobi.Configuration;

/// <summary>
///     Log stuff
/// </summary>
public class Logger
{
    #region  Fields

    private static DateTime lastRefresh = DateTime.MinValue;

    #endregion

    public static LoggingLevel GetLoggingLevel()
    {
        int loggingLevelInt;
        if (DateTime.UtcNow.Subtract(Logger.lastRefresh) >= new TimeSpan(0, 0, 10))
        {
            try
            {
                loggingLevelInt = ConfigurationManager.GetIntRefreshed(CraveCometConfigConstants.LOGGING_LEVEL);
            }
            catch (Exception)
            {
                loggingLevelInt = ConfigurationManager.GetInt(CraveCometConfigConstants.LOGGING_LEVEL);
            }

            Logger.lastRefresh = DateTime.UtcNow;
        }
        else
        {
            loggingLevelInt = ConfigurationManager.GetInt(CraveCometConfigConstants.LOGGING_LEVEL);
        }

        return loggingLevelInt.ToEnum<LoggingLevel>();
    }

    public static void Debug(CometClient client, string message, params object[] args)
    {
        Logger.Log(LoggingLevel.Debug, client, message, "[D]", args);
    }

    public static void Verbose(CometClient client, string message, params object[] args)
    {
        Logger.Log(LoggingLevel.Error, client, message, "[V]", args);
    }

    public static void Information(CometClient client, string message, params object[] args)
    {
        Logger.Log(LoggingLevel.Error, client, message, "[I]", args);
    }

    public static void Error(CometClient client, string message, params object[] args)
    {
        Logger.Log(LoggingLevel.Error, client, message, "[E]", args);
    }

    private static void Log(LoggingLevel level, CometClient client, string message, string prefix, params object[] args)
    {
        string messageFormatted = message.FormatSafe(args);

        if (client != null)
        {
            Tracer.Instance.AddTrace(client, messageFormatted);
        }

        if (Logger.GetLoggingLevel() >= level)
        {
            string clientMessage = "";
            if (client != null)
            {
                clientMessage = string.Format("[Identifier: {0}, IdentifierComet: {1}] ", client.Identifier, client.IdentifierComet);
            }

            Global.Log("CometServerClient", "{0} CometServer - {1}{2}", prefix, clientMessage, messageFormatted);
        }
    }
}