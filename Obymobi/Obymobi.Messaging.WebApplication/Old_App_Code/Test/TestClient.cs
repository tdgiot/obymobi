﻿using Obymobi.Enums;

namespace Test
{
    /// <summary>
    /// Summary description for TestClient
    /// </summary>
    public class TestClient : CometClient
    {
        public TestClient(string macAddress, NetmessageClientType clientType) :
            base("", ClientCommunicationMethod.SignalR)
        {
            Identifier = macAddress;
            IsAuthenticated = true;
            ClientType = clientType;
        }

        public override bool TransmitMessage(string[] receiversIdentifierComet, Obymobi.Logic.Model.Netmessage message)
        {
            return true;
        }

        public override bool VerifyMessage(string guid)
        {
            return true;
        }

        public override void ScheduleForGarbageCollect(string message)
        {

        }
    }
}