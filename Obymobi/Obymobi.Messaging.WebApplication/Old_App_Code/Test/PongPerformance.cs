﻿using System.Collections.Generic;
using System.Diagnostics;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Enums;
using System.Threading;
using Obymobi.Logic.Comet.Netmessages;
using System;
using System.Collections.Concurrent;
using System.Threading.Tasks;
using Obymobi.Data.HelperClasses;

namespace Test
{
    /// <summary>
    /// Summary description for PongPerformance
    /// </summary>
    public class PongPerformance
    {
        public PongPerformance()
        {
		
        }

        private const int multiplier = 100;
        CountdownEvent countdown;
        private BlockingCollection<TestClient> queue = new BlockingCollection<TestClient>();

        public TimeSpan RegularThreaded(ref int numberOfClients)
        {
            var clients = GetClients();

            numberOfClients += clients.Count * multiplier;
            countdown = new CountdownEvent(numberOfClients);

            Stopwatch sw = new Stopwatch();
            sw.Start();

            for (int i = 0; i < multiplier; i++)
            {
                Parallel.ForEach(clients, Pong);
            }

            countdown.Wait();

            sw.Stop();

            return sw.Elapsed;
        }

        public TimeSpan ProducerConsumer(ref int numberOfClients)
        {
            var clients = GetClients();

            countdown = new CountdownEvent(clients.Count * multiplier);

            Task consumer = Task.Factory.StartNew(() =>
                                                  {
                                                      foreach (TestClient value in queue.GetConsumingEnumerable())
                                                      {
                                                          Pong(value);
                                                      }
                                                  }, TaskCreationOptions.LongRunning);

            Stopwatch sw = new Stopwatch();
            sw.Start();

            for (int i = 0; i < multiplier; i++)
            {
                foreach (var testClient in clients)
                {
                    numberOfClients++;
                    queue.Add(testClient);
                }
            }

            queue.CompleteAdding();

            Task.WaitAll(consumer);

            sw.Stop();

            return sw.Elapsed;
        }

        private void Pong(object client)
        {
            var netmessage = new NetmessagePong
                             {
                                 AgentIsRunning = false,
                                 BatteryLevel = 100,
                                 BluetoothKeyboardConnected = false,
                                 BluetoothPrinterConnected = false,
                                 ClientOperationMode = 200,
                                 CurrentCloudEnvironment = CloudEnvironment.Manual,
                                 IsCharging = true,
                                 PrivateIpAddresses = "192.168.58.0",
                                 PublicIpAddress = "127.0.0.1",
                                 LastWifiStrength = -2,
                                 SupportToolsIsRunning = false
                             };
            ((TestClient)client).Pong(netmessage);

            countdown.Signal();
        }

        private List<TestClient> GetClients()
        {
            var outputList = new List<TestClient>();

            var prefetch = new PrefetchPath(EntityType.ClientEntity);
            prefetch.Add(ClientEntityBase.PrefetchPathDeviceEntity);

            var filter = new PredicateExpression(ClientFields.CompanyId > 0);

            var clientCollection = new ClientCollection();
            clientCollection.GetMulti(filter, prefetch);

            foreach (var clientEntity in clientCollection)
            {
                if (clientEntity.DeviceId.HasValue)
                    outputList.Add(new TestClient(clientEntity.MacAddress, NetmessageClientType.Emenu) {ClientId = clientEntity.ClientId});
            }

            prefetch = new PrefetchPath(EntityType.TerminalEntity);
            prefetch.Add(TerminalEntityBase.PrefetchPathDeviceEntity);

            filter = new PredicateExpression(TerminalFields.CompanyId > 0);

            var terminalCollection = new TerminalCollection();
            terminalCollection.GetMulti(filter, prefetch);

            foreach (var terminalEntity in terminalCollection)
            {
                if (terminalEntity.DeviceId.HasValue)
                    outputList.Add(new TestClient(terminalEntity.DeviceEntity.Identifier, NetmessageClientType.Console){TerminalId = terminalEntity.TerminalId});
            }

            return outputList;
        }
    }
}