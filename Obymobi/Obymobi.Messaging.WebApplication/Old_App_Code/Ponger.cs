﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Obymobi.Data;
using Obymobi.Data.EntityClasses;
using Obymobi.Enums;

public class Ponger
{
    public void WritePongUpdates(Dictionary<string, List<CometClient>> connectedClients)
    {
        Global.Log("Ponger", "Writing full pong data to database");

        List<CometClient> cometClients = connectedClients.Values.SelectMany(x => x).ToList();

        Global.Log("Ponger", "Looping through {0} comet clients", cometClients.Count);

        List<CometClient> emenus = cometClients.Where(x => x.ClientType == NetmessageClientType.Emenu && x.ClientEntity != null && x.FullUpdateReady).Take(2000).ToList();
        List<CometClient> consoles = cometClients.Where(x => x.ClientType == NetmessageClientType.Console && x.TerminalEntity != null && x.FullUpdateReady).Take(500).ToList();        

        Global.Log("Ponger", "Updating {0} emenus", emenus.Count);

        int clientsUpdated = 0;
        int terminalsUpdated = 0;

        foreach (CometClient emenu in emenus)
        {
            ClientEntity clientEntity = emenu.ClientEntity;
            if (clientEntity == null || clientEntity.IsNew)
                continue;

            if (clientEntity.Fields[(int)ClientFieldIndex.RoomControlConnected].IsChanged)
            {
                ClientLogEntity logEntity = new ClientLogEntity();
                logEntity.ClientId = clientEntity.ClientId;
                logEntity.TypeEnum = clientEntity.RoomControlConnected ? ClientLogType.RoomControlConnected : ClientLogType.RoomControlDisconnected;
                logEntity.Save();
            }

            try
            {
                if (clientEntity.Save())
                {
                    emenu.FullUpdateReady = false;
                    clientsUpdated++;
                }
            }
            catch (Exception ex)
            {
                if (clientEntity.LastDeliverypointId.HasValue)
                    Global.Log("Ponger", "An exception occurred whilst trying to save the client (ID: {0})!\r\nMessage: {1}\r\nStack: {2}\r\nLastDeliverypointId = {3}", clientEntity.ClientId, ex.Message, ex.StackTrace, clientEntity.LastDeliverypointId.Value);
                else
                    Global.Log("Ponger", "An exception occurred whilst trying to save the client (ID: {0})!\r\nMessage: {1}\r\nStack: {2}\r\nLastDeliverypointId = null", clientEntity.ClientId, ex.Message, ex.StackTrace);
            }
        }

        Global.Log("Ponger", "{0} clients updated", clientsUpdated);
        Global.Log("Ponger", "Updating {0} consoles", consoles.Count);

        foreach (CometClient console in consoles)
        {
            TerminalEntity terminalEntity = console.TerminalEntity;
            if (terminalEntity == null || terminalEntity.IsNew)
                continue;

            try
            {
                if (terminalEntity.Save())
                {
                    console.FullUpdateReady = false;
                    terminalsUpdated++;
                }
            }
            catch (Exception ex)
            {
                Global.Log("Ponger", "An exception occurred whilst trying to save the terminal (ID: {0})!\r\nMessage: {1}\r\nStack: {2}", terminalEntity.TerminalId, ex.Message, ex.StackTrace);
            }
        }

        Global.Log("Ponger", "{0} terminals updated", terminalsUpdated);
        Global.Log("Ponger", "Finished");        
    }
}

