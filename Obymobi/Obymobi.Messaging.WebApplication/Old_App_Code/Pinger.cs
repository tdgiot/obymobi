﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Dionysos;
using Microsoft.AspNet.SignalR;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Obymobi.Enums;
using Obymobi.Logic.Comet.Netmessages;
using Obymobi.Logic.Model;
using SignalR;

public class Pinger
{
    public void Ping(Dictionary<string, List<CometClient>> connectedClients, Dictionary<string, List<CometClient>> sandboxClients)
    {
        List<CometClient> scheduledForDispose = new List<CometClient>();
        List<string> scheduledForPing = new List<string>();        

        // Ping regular clients 
        PingClients_Normal(connectedClients, ref scheduledForPing, ref scheduledForDispose);

        // Ping clients connected in sandbox mode
        PingClients_Sandbox(sandboxClients, ref scheduledForPing, ref scheduledForDispose);

        if (scheduledForPing.Count > 0)
        {
            Global.Log("CometServer", "Pinger - Sending ping to {0} clients", scheduledForPing.Count);

            Stopwatch sw = new Stopwatch();
            sw.Start();

            NetmessagePing pingMessage = new NetmessagePing();
            pingMessage.UnixTime = DateTime.UtcNow.ToUnixTime();
            try
            {
                JsonSerializerSettings settings = new JsonSerializerSettings();
                settings.NullValueHandling = NullValueHandling.Ignore;
                settings.ContractResolver = new CamelCasePropertyNamesContractResolver();
                string netmessage = JsonConvert.SerializeObject(pingMessage, new Formatting(), settings);

                IHubContext hubContext = GlobalHost.ConnectionManager.GetHubContext<SignalRInterface>();
                if (hubContext == null)
                {
                    Global.Log("CometServer", "Pinger - SignalR - Unable to find HubContext for SignalRInterface");
                }
                else
                {
                    hubContext.Clients.Clients(scheduledForPing).ReceiveMessage(netmessage);
                }
            }
            catch (Exception ex)
            {
                Global.Log("CometServer", "Pinger - Exception: {0}\n{1}", ex.Message, ex.StackTrace);
            }

            sw.Stop();
            Global.Log("CometServer", "Pinger - Send ping looping time: {0}ms", sw.Elapsed.TotalMilliseconds);
        }

        if (scheduledForDispose.Count > 0)
        {
            try
            {
                Global.Log("CometServer", "PingPong - Disposing {0} inactive clients", scheduledForDispose.Count);
                var hubContext = GlobalHost.ConnectionManager.GetHubContext<SignalR.SignalRInterface>();
                if (hubContext == null)
                {
                    Global.Log("CometServer", "PingPong - SignalR - Unable to find HubContext for SignalRInterface");
                }
                else
                {
                    // Batch send disconnect message
                    List<string> disposeIds = scheduledForDispose.Select(cometClient => cometClient.IdentifierComet).ToList();
                    hubContext.Clients.Clients(disposeIds).ReceiveMessage(new Netmessage(NetmessageType.Disconnect) {FieldValue1 = "Not responding to ping"});
                }

                List<string> disposedCometIdentifiers = new List<string>();
                foreach (var cometClient in scheduledForDispose)
                {
                    disposedCometIdentifiers.Add(cometClient.IdentifierComet);

                    // Don't supply message, otherwise it will to a DB call to write to client/terminal
                    cometClient.Dispose("", false);
                }
                
                Microsoft.AspNet.SignalR.Transports.ITransportHeartbeat heartbeat = GlobalHost.DependencyResolver.Resolve<Microsoft.AspNet.SignalR.Transports.ITransportHeartbeat>();
                foreach (Microsoft.AspNet.SignalR.Transports.ITrackingConnection connection in heartbeat.GetConnections())
                {
                    if (connection.IsAlive && disposedCometIdentifiers.Contains(connection.ConnectionId))
                    {                        
                        heartbeat.RemoveConnection(connection);
                        connection.Disconnect();                        
                    }
                }

                Global.Log("CometServer", "PingPong - Disposing finished");
            }
            catch (Exception ex)
            {
                Global.Log("CometServer", "Pinger - Failed to dispose clients: {0}\n{1}", ex.Message, ex.StackTrace);
            }
        }
    }

    private void PingClients_Normal(Dictionary<string, List<CometClient>> connectedClients, ref List<string> scheduledForPing, ref List<CometClient> scheduledForDispose)
    {
        Global.Log("CometServer", "Pinger - Looping total {0} clients", connectedClients.Count);

        if (connectedClients.Count > 0)
        {
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();

            DateTime nowUtc = DateTime.UtcNow;
            DateTime fourMinutesAgo = DateTime.UtcNow.Subtract(new TimeSpan(0, 0, 4, 0));
            DateTime thirtySecondsAgo = DateTime.UtcNow.Subtract(new TimeSpan(0, 0, 0, 30));

            foreach (List<CometClient> clientValue in connectedClients.Values)
            {
                int clientCountEmenu = clientValue.Count(x => x.ClientType == NetmessageClientType.Emenu);
                int clientCountSupportTools = clientValue.Count(x => x.ClientType == NetmessageClientType.SupportTools);
                int clientCountUnknown = clientValue.Count(x => x.ClientType == NetmessageClientType.Unknown);
                int clientDisposed = 0;

                string identifier = "";

                foreach (CometClient cometClient in clientValue)
                {
                    identifier = cometClient.Identifier;

                    bool shouldPing = false;
                    bool shouldDispose = false;

                    if (cometClient.LastPong > thirtySecondsAgo)
                    {
                        // Last ping within 30 seconds
                    }
                    else if (cometClient.LastPong >= fourMinutesAgo)
                    {
                        // Last ping within between 30 seconds and 4 minutes
                        if (cometClient.ClientType != NetmessageClientType.Unknown &&
                            (cometClient.LastPingSent == null || cometClient.LastPingSent < thirtySecondsAgo))
                        {
                            shouldPing = true;
                        }
                    }
                    else
                    {
                        // Last ping longer than 4 minutes ago
                        shouldDispose = true;                        
                    }

                    if (shouldDispose)
                    {
                        clientDisposed++;
                        scheduledForDispose.Add(cometClient);
                    }
                    else if (shouldPing)
                    {
                        cometClient.LastPingSent = nowUtc;
                        scheduledForPing.Add(cometClient.IdentifierComet);
                    }
                }

                if (clientCountEmenu > 1 || clientCountSupportTools > 1 || clientCountUnknown > 1)
                {
                    Global.Log("Ping", "Client: {0, -15}, Emenu: {1, -3}, SupportTools: {2, -3}, Unknown: {3, -3}, Disposed: {4}", identifier, clientCountEmenu, clientCountSupportTools, clientCountUnknown, clientDisposed);
                }
            }

            stopwatch.Stop();
            Global.Log("CometServer", "Pinger - Looping time: {0}ms", stopwatch.Elapsed.TotalMilliseconds);
        }
    }

    private void PingClients_Sandbox(Dictionary<string, List<CometClient>> sandboxClients, ref List<string> scheduledForPing, ref List<CometClient> scheduledForDispose)
    {
        Global.Log("CometServer", "Pinger - Looping total {0} sandbox clients", sandboxClients.Count);

        if (sandboxClients.Count > 0)
        {
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();

            DateTime twoMinutesAgo = DateTime.UtcNow.Subtract(new TimeSpan(0, 0, 2, 0));
            DateTime thirtySecondsAgo = DateTime.UtcNow.Subtract(new TimeSpan(0, 0, 0, 30));

            foreach (List<CometClient> clientValue in sandboxClients.Values)
            {
                foreach (CometClient cometClient in clientValue)
                {
                    // Check if the clients last pong was 30 seconds or longer ago
                    if (cometClient.LastPong <= thirtySecondsAgo)
                    {
                        if (cometClient.LastPong >= twoMinutesAgo)
                        {
                            // Check if the client has registered itself with a ClientType
                            if (cometClient.ClientType != NetmessageClientType.Unknown)
                            {
                                // Send ping to client
                                scheduledForPing.Add(cometClient.IdentifierComet);
                            }

                            //NewRelic.Api.Agent.NewRelic.IncrementCounter(NewRelicConstants.NEWRELIC_MESSAGING_NETMESSAGE_PINGCOUNTER);
                        }
                        else
                        {
                            // Clients last pong was 2+ minutes ago, schedule client for disconnect/dispose
                            scheduledForDispose.Add(cometClient);
                        }
                    }
                }
            }

            stopwatch.Stop();
            Global.Log("CometServer", "Pinger - Sandbox looping time: {0}ms", stopwatch.Elapsed.TotalMilliseconds);
        }
    }
}