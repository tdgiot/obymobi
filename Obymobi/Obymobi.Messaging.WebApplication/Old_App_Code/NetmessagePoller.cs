﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using Dionysos;
using Obymobi.Constants;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using Obymobi.Logic.HelperClasses;
using Obymobi.Logic.Model;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Logic.ScheduledCommands;

/// <summary>
/// Summary description for NetmessagePoller
/// </summary>
public class NetmessagePoller
{
	public static void CheckNonHandledNetmessages()
	{
	    var stopwatch = Stopwatch.StartNew();

        // Fetch all netmessages - which are non verified and created more than 15 seconds ago - from database.
        var filter = new PredicateExpression();
        filter.Add(NetmessageFields.Status == NetmessageStatus.WaitingForVerification);
        filter.Add(NetmessageFields.CreatedUTC > DateTime.UtcNow.AddDays(-1));
        filter.Add(NetmessageFields.CreatedUTC <= DateTime.UtcNow.AddSeconds(-15));

        var collection = new NetmessageCollection();
        collection.GetMulti(filter);

        var clientDisposeList = new List<CometClient>();
	    int timeoutCounter = 0;

        Global.Log("CometServer", "NetmessagePoller - Found {0} non-verified netmessages. Elapsed: {1}ms", collection.Count, stopwatch.ElapsedMilliseconds);
        stopwatch.Restart();

        foreach (NetmessageEntity netmessageEntity in collection)
        {
            Netmessage netmessage = NetmessageHelper.CreateNetmessageModelFromEntity(netmessageEntity);
            CometServer.Instance.ScaleOutMessageQueue.PublishMessage(netmessage);

            var clientList = new List<CometClient>();

            // Check if the receiving client is online
            if (netmessageEntity.ReceiverClientId.HasValue && netmessageEntity.ReceiverClientId.Value > 0)
            {
	            clientList = CometServer.Instance.GetCometClientsByClientId(netmessageEntity.ReceiverClientId.Value);
            }
            else if (netmessageEntity.ReceiverTerminalId.HasValue && netmessageEntity.ReceiverTerminalId.Value > 0)
            {
	            int terminalId = netmessageEntity.ReceiverTerminalId.Value;
	            clientList = CometServer.Instance.GetCometClientsByProperty(c => c.TerminalId == terminalId);
            }

            bool forceComplete = (netmessageEntity.CreatedUTC <= DateTime.UtcNow.AddMinutes(-NetmessageHelper.NETMESSAGE_TIMEOUT));

            if (!forceComplete && clientList.Count > 0)
            {
                netmessageEntity.MessageLog += string.Format("[{0}] Sending message from NetmessagePoller (CometServer) to client\n", DateTimeUtil.GetTimeString());
                netmessageEntity.Save();

                CometServer.Instance.SendNetmessage(clientList, netmessage);
            }
            else if (forceComplete)
            {
                try
                {
                    timeoutCounter++;

                    netmessageEntity.Status = (int)NetmessageStatus.TimedOut;
                    netmessageEntity.Save();
                }
                catch (Exception exception)
                {
                    Global.Log("CometServer", "NetmessagePoller - Unable to save NetmessageEntity. Id {0}. Exception: {1}", netmessageEntity.NetmessageId, exception.ProcessStackTrace(true));

                    var requestLog = new RequestlogEntity();
                    requestLog.SourceApplication = "CometServer";
                    requestLog.MethodName = "CometServer.NetmessagePoller";
                    requestLog.ResultEnumValueName = "Exception";
                    requestLog.ResultEnumTypeName = "Exception";
                    requestLog.ErrorMessage = string.Format("Unable to save NetmessageEntity. Id: {0}. Exception: {1}\n\r{2}", netmessageEntity.NetmessageId, exception.Message, exception.StackTrace);
                    requestLog.ResultMessage = "Failed";
                    requestLog.Save();
                }

                if (clientList.Count > 0)
                {
                    clientDisposeList.AddRange(clientList);
                }
            }
        }

        Global.Log("CometServer", "NetmessagePoller - Processed all Netmessages. Elapsed: {0}ms", stopwatch.ElapsedMilliseconds);
        stopwatch.Restart();

        // Dispose clients
        foreach (CometClient cometClient in clientDisposeList)
        {
            cometClient.Dispose("Not responding to messages");
        }

        Global.Log("CometServer", "NetmessagePoller - Disposed clients. Elapsed: {0}ms", stopwatch.ElapsedMilliseconds);
	}
}