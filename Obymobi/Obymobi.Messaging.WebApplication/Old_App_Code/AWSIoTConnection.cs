﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Security.Authentication;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using Google.Apis.Manual.Util;
using Models;
using MQTTnet;
using MQTTnet.Client;
using MQTTnet.Client.Connecting;
using MQTTnet.Client.Disconnecting;
using MQTTnet.Client.Options;
using MQTTnet.Client.Publishing;
using MQTTnet.Client.Subscribing;
using MQTTnet.Formatter;
using MQTTnet.Protocol;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Obymobi.Enums;
using Obymobi.Logic.Model;

namespace Obymobi.Messaging.WebApplication.Old_App_Code
{
    public class AWSIoTConnection : IDisposable
    {
        private const string TAG = "AWSIoTConnection";

        private readonly DummyCometClient dummyClient;
        private readonly CancellationTokenSource cancellationTokenSource;
        private readonly CancellationToken cancellationToken;

        private readonly MqttFactory clientFactory;
        private IMqttClient client;
        private IMqttClientOptions clientOptions;

        private readonly JsonSerializerSettings jsonSettingsCamelCase;
        private readonly BlockingCollection<NetmessageContainer> messageTxQueue;

        private string clientId;

        public AWSIoTConnection()
        {
            this.dummyClient = new DummyCometClient();
            this.messageTxQueue = new BlockingCollection<NetmessageContainer>();

            this.jsonSettingsCamelCase = new JsonSerializerSettings
            {
                NullValueHandling = NullValueHandling.Ignore,
                ContractResolver = new CamelCasePropertyNamesContractResolver()
            };

            this.cancellationTokenSource = new CancellationTokenSource();
            this.cancellationToken = this.cancellationTokenSource.Token;

            this.clientFactory = new MqttFactory();

            InitClient();

            Task.Factory.StartNew(ConsumeQueue, TaskCreationOptions.LongRunning);
            Task.Factory.StartNew(Connect, this.cancellationToken);
        }

        private string IoTEndpoint
        {
            get
            {
                if (WebEnvironmentHelper.CloudEnvironmentIsProduction)
                {
                    return "a18pry60ja66ut-ats.iot.ap-southeast-1.amazonaws.com";
                }

                return "a1sxiz33z6s37h-ats.iot.ap-southeast-1.amazonaws.com"; // dev
            }
        }

        private byte[] ClientCertificate
        {
            get
            {
                if (WebEnvironmentHelper.CloudEnvironmentIsProduction)
                {
                    return Properties.Resources.LegacyMessaging_production_apsoutheast1;
                }

                return Properties.Resources.LegacyMessaging_development_apsoutheast1; // dev
            }
        }

        private void InitClient()
        {
            X509Certificate caCert = new X509Certificate(Properties.Resources.root_ca_development);
            X509Certificate2 clientCert = new X509Certificate2(this.ClientCertificate, "fu7280U6iqHJda3Fr9bR");

            this.clientId = "LegacyMessaging_" + Guid.NewGuid().ToString("N");

            this.clientOptions = new MqttClientOptionsBuilder()
                                         .WithClientId(this.clientId)
                                         .WithTcpServer(this.IoTEndpoint, 8883)
                                         .WithTls(new MqttClientOptionsBuilderTlsParameters
                                         {
                                             Certificates = new[] { caCert, clientCert },
                                             UseTls = true,
                                             SslProtocol = SslProtocols.Tls12
                                         })
                                         .WithCleanSession()
                                         .WithProtocolVersion(MqttProtocolVersion.V311)
                                         .WithKeepAlivePeriod(TimeSpan.FromSeconds(60))
                                         .Build();

            this.client = this.clientFactory.CreateMqttClient();
            this.client.UseApplicationMessageReceivedHandler(MqttMessageHandler);
            this.client.UseConnectedHandler(MqttConnectedHandler);
            this.client.UseDisconnectedHandler(MqttDisconnectedHandler);
        }

        private async Task MqttDisconnectedHandler(MqttClientDisconnectedEventArgs arg)
        {
            Global.Log(TAG, "Disconnected from server, reconnecting in a couple of seconds...");
            await Task.Delay(TimeSpan.FromSeconds(30), this.cancellationToken);

            try
            {
                await Connect();
            }
            catch (Exception ex)
            {
                Global.Log(TAG, "Reconnect failed - {0}", ex.Message);
            }
        }

        private async Task MqttConnectedHandler(MqttClientConnectedEventArgs arg)
        {
            Global.Log(TAG, "Subscribing to agent response topics: supporttools/response/#");
            await this.client.SubscribeAsync(new MqttClientSubscribeOptionsBuilder()
                                             .WithTopicFilter("supporttools/response/#", MqttQualityOfServiceLevel.AtLeastOnce)
                                             .WithTopicFilter("device/#", MqttQualityOfServiceLevel.AtLeastOnce)
                                             .Build());
        }

        private Task MqttMessageHandler(MqttApplicationMessageReceivedEventArgs arg)
        {
            if (arg.ApplicationMessage.Payload.Length == 0)
            {
                return Task.CompletedTask;
            }

            string payload = Encoding.UTF8.GetString(arg.ApplicationMessage.Payload);

            NetmessageContainer container = null;
            try
            {
                container = JsonConvert.DeserializeObject(payload, typeof(NetmessageContainer)) as NetmessageContainer;
            }
            catch (Exception ex)
            {
                Global.Log(TAG, "Exception while deserializing JSON to NetmessageContainer.\n{ 'json': '{0}', 'Exception': '{1}' }", payload, ex.Message);
            }

            if (container != null && (container.Instance == null || !container.Instance.Equals("Legacy")))
            {
                Global.Log(TAG, "Topic: {0}, Payload: {1}", arg.ApplicationMessage.Topic, payload);

                try
                {
                    ProcessMessageFn(container);
                }
                catch (Exception ex)
                {
                    Global.Log(TAG, "Exception while processing container.\n{ 'json': '{0}', 'Exception': '{1}' }", payload, ex.Message);
                }
            }

            return Task.CompletedTask;
        }

        public void Dispose()
        {
            Global.Log(TAG, "Dispose has been initiated");

            if (this.messageTxQueue != null && !this.messageTxQueue.IsAddingCompleted)
            {
                this.messageTxQueue.CompleteAdding();
            }

            this.cancellationTokenSource.Cancel();

            Task.Run(() => { this.client.DisconnectAsync(); });

            this.client.Dispose();
        }

        public void PublishMessage(Netmessage netmessage)
        {
            if (this.messageTxQueue.IsAddingCompleted || this.cancellationToken.IsCancellationRequested)
            {
                return;
            }

            if (netmessage.FromAwsIot)
            {
                return;
            }

            if (Utilities.IsNullOrEmpty(netmessage.ReceiverIdentifier))
            {
                return;
            }

            NetmessageContainer container = new NetmessageContainer
            {
                Guid = netmessage.Guid,
                Instance = "Legacy",
                Sender = netmessage.SenderIdentifier,
                Receiver = netmessage.ReceiverIdentifier,
                Type = netmessage.MessageType,
                Data = JsonConvert.SerializeObject(netmessage, this.jsonSettingsCamelCase)
            };
            this.messageTxQueue.Add(container, this.cancellationToken);
        }

        private async Task Connect()
        {
            Global.Log(TAG, "Connecting to... {0} (ClientId: {1})", this.IoTEndpoint, this.clientId);

            try
            {
                await this.client.ConnectAsync(this.clientOptions, this.cancellationToken);
            }
            catch (Exception ex)
            {
                Global.Log(TAG, "Connect failed - {0}", ex.Message);
            }
        }

        private async Task ConsumeQueue()
        {
            while (!this.messageTxQueue.IsAddingCompleted && !this.cancellationToken.IsCancellationRequested)
            {
                NetmessageContainer container = this.messageTxQueue.Take(this.cancellationToken);
                if (container.CreatedUtc < DateTime.UtcNow.Subtract(TimeSpan.FromMinutes(15)))
                {
                    Global.Log(TAG, "Dropping netmessage (guid={0}). Reason: Message older than 15 minutes", container.Guid);
                    continue;
                }

                string topic = $"device/{container.Receiver.Replace(":", "").ToLower()}";
                if (container.Type == NetmessageType.AgentCommandRequest)
                {
                    topic = $"supporttools/request/{container.Receiver.Replace(":", "").ToLower()}";
                }

                container.Topic = topic;

                string json = JsonConvert.SerializeObject(container, this.jsonSettingsCamelCase);

                if (this.client.IsConnected)
                {
                    MqttApplicationMessage message = new MqttApplicationMessageBuilder()
                                                     .WithTopic(topic)
                                                     .WithPayload(json)
                                                     .Build();

                    MqttClientPublishResult result = await this.client.PublishAsync(message, this.cancellationToken);

                    if (result.ReasonCode != MqttClientPublishReasonCode.Success)
                    {
                        Global.Log(TAG, "Failed to publish message. Result: {0} - {1}", result.ReasonCode, result.ReasonString);
                    }
                }
                else
                {
                    await Task.Delay(10000, this.cancellationToken);
                    this.messageTxQueue.Add(container, this.cancellationToken);
                }
            }
        }


        private void ProcessMessageFn(NetmessageContainer container)
        {
            Netmessage netmessage;
            try
            {
                if (container.Data.Length == 0)
                {
                    Global.Log(TAG, "Container data length is 0");
                    return;
                }

                netmessage = (Netmessage)JsonConvert.DeserializeObject(container.Data, typeof(Netmessage));
            }
            catch (Exception ex)
            {
                Global.Log(TAG, "Exception: {0}", ex.Message);
                return;
            }

            // Mark netmessage as received from IoT
            netmessage.FromAwsIot = true;

            List<CometClient> cometClients = new List<CometClient>();
            if (container.Groups.IsNullOrEmpty())
            {
                if (netmessage.ReceiverClientId > 0)
                {
                    cometClients = CometServer.Instance.GetCometClientsByClientId(netmessage.ReceiverClientId);
                }
                else if (netmessage.ReceiverTerminalId > 0)
                {
                    cometClients = CometServer.Instance.GetCometClientsByProperty(c => c.TerminalId == netmessage.ReceiverTerminalId);
                }
                else if (container.Receiver.IsNotNullOrEmpty())
                {
                    CometServer.Instance.TryGetCometClients(container.Receiver, out cometClients);
                }
                else if (netmessage.ReceiverIdentifier.IsNotNullOrEmpty())
                {
                    CometServer.Instance.TryGetCometClients(netmessage.ReceiverIdentifier, out cometClients);
                }

                if (cometClients == null)
                {
                    cometClients = new List<CometClient>();
                }
            }
            else
            {
                foreach (string groupName in container.Groups)
                {
                    if (groupName.StartsWith("Agent-"))
                    {
                        NetmessageHandler.HandleNetmessage(this.dummyClient, netmessage);
                    }
                    else
                    {
                        cometClients.AddRange(CometServer.Groups.GetCometClientsForGroup(ConvertFromNewApiGroupName(groupName)));
                    }
                }
            }

            Global.Log(TAG, "Sending netmessage to {0} clients.", cometClients.Count);

            foreach (CometClient cometClient in cometClients)
            {
                netmessage.ReceiverClientId = 0;
                netmessage.ReceiverTerminalId = 0;
                netmessage.ReceiverDeliverypointId = 0;
                netmessage.ReceiverCompanyId = 0;

                if (cometClient.ClientId > 0)
                {
                    netmessage.ReceiverClientId = cometClient.ClientId;
                }

                if (cometClient.TerminalId > 0)
                {
                    netmessage.ReceiverTerminalId = cometClient.TerminalId;
                }

                if (cometClient.DeliverypointId > 0)
                {
                    netmessage.ReceiverDeliverypointId = cometClient.DeliverypointId;
                }

                if (cometClient.SubscribedCompany != null)
                {
                    netmessage.ReceiverCompanyId = cometClient.SubscribedCompany.CompanyId;
                }

                NetmessageHandler.HandleNetmessage(this.dummyClient, netmessage);
            }
        }

        private string ConvertFromNewApiGroupName(string groupName)
        {
            string originalGroupName = groupName;

            Regex companyGroup = new Regex(MessagingConstants.GroupCompanyRegex);
            Match companyGroupMatch = companyGroup.Match(groupName);

            Regex terminalGroup = new Regex(MessagingConstants.GroupCompanyTerminalsRegex);
            Match terminalGroupMatch = terminalGroup.Match(groupName);

            Regex deliverypointgroupGroup = new Regex(MessagingConstants.GroupDeliverypointgroupRegex);
            Match deliverypointgroupGroupMatch = deliverypointgroupGroup.Match(groupName);

            if (companyGroupMatch.Success && companyGroupMatch.Groups.Count > 1)
            {
                groupName = string.Format("Company-Clients-{0}", companyGroupMatch.Groups[1]);
                Global.Log(TAG, "Converted new api groupname from {0} to {1}", originalGroupName, groupName);
            }
            else if (terminalGroupMatch.Success && terminalGroupMatch.Groups.Count > 1)
            {
                groupName = string.Format("Company-Consoles-{0}", terminalGroupMatch.Groups[1]);
                Global.Log(TAG, "Converted new api groupname from {0} to {1}", originalGroupName, groupName);
            }
            else if (deliverypointgroupGroupMatch.Success && deliverypointgroupGroupMatch.Groups.Count > 1)
            {
                groupName = string.Format("Deliverypointgroup-Clients-{0}", deliverypointgroupGroupMatch.Groups[1]);
                Global.Log(TAG, "Converted new api groupname from {0} to {1}", originalGroupName, groupName);
            }

            return groupName;
        }
    }
}
