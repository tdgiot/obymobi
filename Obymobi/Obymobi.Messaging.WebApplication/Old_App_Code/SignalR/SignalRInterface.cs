﻿using System;
using System.Collections.Concurrent;
using System.Threading.Tasks;
using Microsoft.AspNet.SignalR;
using Dionysos;

namespace SignalR
{
    /// <summary>
    /// Summary description for SignalRInterface
    /// </summary>
    /// <remarks>
    /// SignalR creates a new interface instance for each request
    /// therefore we store key/value pair of identifiers and SignalRClient objects
    /// </remarks>
    public class SignalRInterface : Hub
    {
        private static readonly ConcurrentDictionary<string, ConcurrentDictionary<string, SignalRClient>> SignalClients = new ConcurrentDictionary<string, ConcurrentDictionary<string, SignalRClient>>(StringComparer.InvariantCultureIgnoreCase);

        //private int GetTotalConnections()
        //{
        //    int count = 0;

        //    foreach (ConcurrentDictionary<string, SignalRClient> entry in SignalClients.Values)
        //    {
        //        count += entry.Count;
        //    }

        //    return count;
        //}

        public override Task OnDisconnected(bool stopCalled)
        {
            var client = GetClientByHeader();
            if (client != null)
            {
                client.Dispose();
            }
            return base.OnDisconnected(stopCalled);
        }

        public void Authenticate(int timestamp, string identifier, string hash)
        {
            var client = GetAddClient(identifier);
            if (client != null)
            {
                client.Authenticate(timestamp, identifier, hash);
            }
        }

        public void SandboxLogin(string identifier, int lastCompanyId, string localIp, string externalIp)
        {
            var client = GetAddClient(identifier);
            if (client != null)
            {
                client.SandboxLogin(identifier, lastCompanyId, localIp, externalIp);
            }
        }

        public void ReceiveMessage(string message)
        {
            var client = GetClientByHeader();
            if (client != null)
            {
                client.ReceiveMessage(message);
            }

            //Global.Log("SignalRInterface", "Total connections: {0}", this.GetTotalConnections());
        }

        private SignalRClient GetClientByHeader()
        {
            string identifier = Context.Headers.Get("deviceIdentifier");
            if (identifier == null)
            {
                identifier = Context.QueryString["deviceIdentifier"];
            }

            SignalRClient client = null;
            if (!string.IsNullOrEmpty(identifier))
            {
                ConcurrentDictionary<string, SignalRClient> clientList;
                if (SignalClients.TryGetValue(identifier, out clientList) && clientList.ContainsKey(Context.ConnectionId))
                {
                    clientList.TryGetValue(Context.ConnectionId, out client);
                }
            }

            return client;
        }

        private SignalRClient GetAddClient(string identifier)
        {
            ConcurrentDictionary<string, SignalRClient> clientList;
            if (!SignalClients.TryGetValue(identifier, out clientList))
            {
                clientList = new ConcurrentDictionary<string, SignalRClient>(StringComparer.InvariantCultureIgnoreCase);
                SignalClients[identifier] = clientList;
            }
            return clientList.GetOrAdd(Context.ConnectionId, _ => new SignalRClient(this, Context.ConnectionId));
        }

        public void RemoveClient(SignalRClient client)
        {
            if (client.Identifier.IsNullOrWhiteSpace() || client.IdentifierComet.IsNullOrWhiteSpace())
            {
                return;
            }            

            ConcurrentDictionary<string, SignalRClient> clientList;
            if (!SignalClients.TryGetValue(client.Identifier, out clientList))
            {
                return;
            }                

            SignalRClient oldClient;
            if (!clientList.TryRemove(client.IdentifierComet, out oldClient))
            {
                return;
            }                

            if (clientList.Count <= 0)
            {
                ConcurrentDictionary<string, SignalRClient> oldClients;
                SignalClients.TryRemove(client.Identifier, out oldClients);
            }
        }
    }
}