﻿using Microsoft.AspNet.SignalR;
using Microsoft.Owin;
using Microsoft.Owin.Cors;
using Owin;

[assembly: OwinStartup(typeof(SignalR.Startup))]
namespace SignalR
{
    /// <summary>
    /// SignalR startup class
    /// </summary>
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            // Allow cross-domain connections
            app.UseCors(CorsOptions.AllowAll);

            var configuration = new HubConfiguration();
            configuration.EnableDetailedErrors = true;
            configuration.EnableJSONP = true;
            app.MapSignalR("/signalr", configuration);
        }
    }
}