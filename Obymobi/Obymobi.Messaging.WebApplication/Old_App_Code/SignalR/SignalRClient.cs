﻿using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Obymobi.Enums;
using Obymobi.Logic.Model;

namespace SignalR
{
    public class SignalRClient : CometClient
    {
        private SignalRInterface signalHub;

        public SignalRClient(SignalRInterface hub, string connectionId) 
            : base(connectionId, ClientCommunicationMethod.SignalR)
        {
            signalHub = hub;
        }

        public void ReceiveMessage(string message)
        {
            Netmessage netmessage = null;
            try
            {
                // Convert message json back to Netmessage object
                netmessage = (Netmessage)JsonConvert.DeserializeObject(message, typeof(Netmessage));
            }
            catch (Exception ex)
            {
                Logger.Error(this, "Exception while deserializing JSON to Netmessage.\n{ 'json': '{0}', 'Exception': '{1}' }", message, ex.Message);
            }

            if (netmessage != null)
            {
                ReceiveMessage(netmessage);
            }
        }

        public override bool TransmitMessage(string[] receiversIdentifierComet, Netmessage message)
        {
            var settings = new JsonSerializerSettings();
            settings.NullValueHandling = NullValueHandling.Ignore;
            settings.ContractResolver = new CamelCasePropertyNamesContractResolver();
            var netmessage = JsonConvert.SerializeObject(message, new Formatting(), settings);

            if (signalHub == null)
            {
                Logger.Error(this, "signalHub is NULL");
                return false;
            }
            if (signalHub.Clients == null)
            {
                Logger.Error(this, "signalHub.Clients is NULL");
                return false;
            }

            var receiverClients = signalHub.Clients.Clients(receiversIdentifierComet);
            if (receiverClients == null)
            {
                Logger.Error(this, "signalHub.Client.Clients({0}) is NULL", string.Join(", ", receiversIdentifierComet));
                return false;
            }
            
            receiverClients.ReceiveMessage(netmessage);

            return true;
        }

        public override bool VerifyMessage(string guid)
        {
            try
            {
                signalHub.Clients.Caller.MessageVerified(guid);
            }
            catch (Exception ex)
            {
                Logger.Error(this, "VerifyMessage Error: {0}", ex.Message);
                return false;
            }

            return true;
        }

        public override void Dispose()
        {
            base.Dispose();            
            signalHub = null;
        }

        public override void ScheduleForGarbageCollect(string message)
        {
            // Remove client from server, this will also unpin the client from all groups
            CometServer.Instance.RemoveClient(this, message);

            if (this.signalHub != null)
            {
                this.signalHub.RemoveClient(this);
            }
        }
    }
}