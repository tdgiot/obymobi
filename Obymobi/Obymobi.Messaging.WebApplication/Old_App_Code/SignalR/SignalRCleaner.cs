﻿using System;
using System.Linq;
using System.Collections.Generic;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Transports;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;

/// <summary>
/// Summary description for SignalRCleaner
/// </summary>
public class SignalRCleaner
{
    public static void DoWork()
    {
        Stopwatch sw = new Stopwatch();
        sw.Start();

        Global.Log("SignalR", "-- Starting work --");

        ITransportHeartbeat heartbeat = GlobalHost.DependencyResolver.Resolve<ITransportHeartbeat>();
        IList<ITrackingConnection> connections = heartbeat.GetConnections();

        int signalRConnectionsAlive = connections.Count(c => c.IsAlive);

        Global.Log("ActiveConnections", "Connections (Unique): {0, -4} | Connections (Total): {1, -4} | SignalR (Total): {2, -4} | SignalR (Alive): {3, -4}",
            CometServer.Instance.TotalActiveDeviceConnections(),
            CometServer.Instance.TotalActiveConnections(),
            connections.Count,
            signalRConnectionsAlive);

        TimeSpan countTime = sw.Elapsed;
        Global.Log("SignalR", "Done counting active connections: {0}", countTime);

        int connectionsRemoved = 0;
        List<string> connectionIds = CometServer.Instance.GetClientCometIdentifiers();
        try
        {
            // Force removal of clients internal SignalR connection
            Global.Log("SignalR", "Forcing removal of unused SignalR connections (Total connections: {0})", connections.Count);
            Parallel.ForEach(connections,
                             new ParallelOptions {MaxDegreeOfParallelism = 2},
                             connection =>
                             {
                                 if (connectionIds.Contains(connection.ConnectionId))
                                 {
                                     return;
                                 }

                                 try
                                 {
                                     //Global.Log("SignalR", "Trying to remove: {0}", connection.ConnectionId);

                                     heartbeat.RemoveConnection(connection);
                                     connection.Disconnect();

                                     Interlocked.Increment(ref connectionsRemoved);
                                 }
                                 catch (Exception iex)
                                 {
                                     Global.Log("SignalR", "An exception occurred while trying to forced remove SignalR connection '{0}'. Message: {1}", connection.ConnectionId, iex.Message);
                                 }
                             });
        }
        catch (Exception ex)
        {
            Global.Log("SignalR", "An exception occurred while trying to forced remove the SignalR connections. Message: {0}", ex.Message);
        }
        finally
        {
            sw.Stop();

            Global.Log("SignalR", "Work is done: {0} (Total: {1}). Removed {2} connections", sw.Elapsed.Subtract(countTime), sw.Elapsed, connectionsRemoved);
        }
    }
}