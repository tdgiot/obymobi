﻿using Models;

namespace Crave.Backend.MessageQueue.Requests
{
    /// <summary>
    /// Model for sending a netmessage request on the message bus
    /// </summary>
    public class SendNetmessageRequest
    {
        /// <summary>
        /// Gets or sets the netmessage container
        /// </summary>
        public NetmessageContainer Container { get; set; }
    }
}