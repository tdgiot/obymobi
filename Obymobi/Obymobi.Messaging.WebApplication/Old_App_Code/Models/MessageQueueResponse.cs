﻿namespace Models
{
    /// <summary>
    /// Base message queue response
    /// </summary>
    public class MessageQueueReponse
    {
        /// <summary>
        /// Gets or sets whether the request was executed succesfully
        /// </summary>
        public bool Success { get; set; }

        /// <summary>
        /// Gets or sets the error message
        /// </summary>
        public string ErrorMessage { get; set; }
    }
}