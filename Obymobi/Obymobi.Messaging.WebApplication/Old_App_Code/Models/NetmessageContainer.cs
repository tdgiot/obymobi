﻿using System;
using System.Text;
using Newtonsoft.Json;
using Obymobi.Enums;

namespace Models
{
    /// <summary>
    /// Netmessage wrapper containing receiver information and the actual data to send
    /// </summary>
    public class NetmessageContainer
    {
        public NetmessageContainer()
        {
            this.Sender = string.Empty;
            this.Receiver = string.Empty;
            this.Groups = new string[] { };
            this.Type = NetmessageType.Unknown;
            this.Data = string.Empty;
            this.CreatedUtc = DateTime.UtcNow;
        }

        /// <summary>
        /// Unique identifier to verify message on sender
        /// </summary>
        public string Guid { get; set; }

        /// <summary>
        /// Topic on which this message was published (AWS IoT only)
        /// </summary>
        public string Topic { get; set; }

        /// <summary>
        /// Gets or sets the sender of this netmessage
        /// </summary>
        public string Sender { get; set; }

        /// <summary>
        /// Gets or sets the receiver
        /// </summary>
        public string Receiver { get; set; }

        /// <summary>
        /// Gets or sets the group names to which to send this message
        /// </summary>
        public string[] Groups { get; set; }

        /// <summary>
        /// Netmessage type <see cref="NetmessageType"/>
        /// </summary>
        public NetmessageType Type { get; set; }

        /// <summary>
        /// Data to send to the receiver
        /// </summary>
        public string Data { get; set; }

        /// <summary>
        /// Server instance name from which message is send (only used for scale out messages)
        /// </summary>
        public string Instance { get; set; }

        [JsonIgnore]
        public DateTime CreatedUtc { get; }

        /// <inheritdoc />
        public override string ToString()
        {
            return new StringBuilder()
                   .AppendFormat("Topic={0},", this.Topic)
                   .AppendFormat("Sender={0},", this.Sender)
                   .AppendFormat("Receiver={0},", this.Receiver)
                   .AppendFormat("Groups={0},", this.Groups != null ? string.Join(",", this.Groups) : string.Empty)
                   .AppendFormat("Type={0},", this.Type)
                   .AppendFormat("Instance={0},", this.Instance)
                   .AppendFormat("Data={0}", this.Data)
                   .ToString();
        }
    }
}
