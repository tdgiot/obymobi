﻿using System;
using System.Collections.Generic;
using Obymobi.Data.EntityClasses;

/// <summary>
/// CometCompany
/// </summary>
public class CometCompany
{
	#region Fields

	/// <summary>
	/// Company id
	/// </summary>
	public int CompanyId { get; private set; }

	/// <summary>
    /// The linked <see cref="CompanyEntity"/> model
	/// </summary>
	public CompanyEntity ObymobiCompany { get; private set; }

	public List<CometDeliverypointgroup> Deliverypointgroups { get; private set; }

	#endregion

	#region Company Groups

	public string GroupNameConsoles
	{
		get { return "Company-Consoles-" + this.CompanyId; }
	}

	public string GroupNameClients
	{
		get { return "Company-Clients-" + this.CompanyId; }
	}

	public string GroupNameExternal
	{
		get { return "Company-External-" + this.CompanyId; }
	}

	public static string GroupNameTerminal(int terminalId)
	{
		return "Terminal-" + terminalId;
	}

    public string GroupNameMasterTerminals
    {
        get { return "Company-MasterTerminals-" + this.CompanyId; }
    }

	#endregion

	/// <summary>
	/// <para>Creates a new CometCompany object. Will automatically be linked to <see cref="Obymobi.Logic.Model.Company"/> model.</para>
	/// <para>Exception will be thrown if there is no such company with supplied id.</para>
	/// </summary>
	/// <param name="companyId">Id of the company</param>
	public CometCompany(int companyId)
	{
	    var companyEntity = new CompanyEntity(companyId);
        if (!companyEntity.IsNew)
        {
            this.ObymobiCompany = companyEntity;
        }
        else
        {
            throw new Exception(string.Format("Company with ID {0} does not exist in the database.", companyId));
        }

		this.CompanyId = companyId;
		this.Deliverypointgroups = new List<CometDeliverypointgroup>();
	}

	/// <summary>
	/// Adds a new deliverypointgroup to a comet company
	/// </summary>
	/// <param name="deliverypointgroup">Deliverypointgroup to add</param>
	public void AddDeliverypointgroup(CometDeliverypointgroup deliverypointgroup)
	{
		this.Deliverypointgroups.Add(deliverypointgroup);
	}

	/// <summary>
	/// Subscribe a CometClient to this CometCompany
	/// </summary>
	/// <param name="client">CometClient object to subscribe</param>
	public void PinClient(CometClient client)
	{
		client.SubscribedCompany = this;
		CometServer.Groups.PinClient(client, this.GroupNameClients);
	}

	/// <summary>
	/// Unsubscribe a CometClient from this CometCompany
	/// </summary>
	/// <param name="client">CometClient object to unsubscribe</param>
	public void UnpinClient(CometClient client)
	{
		client.SubscribedCompany = null;
		CometServer.Groups.UnpinClientFromGroup(client, this.GroupNameClients);
	}

	/// <summary>
	/// Subscribe a CometClient to this CometCompany
	/// </summary>
	/// <param name="client">CometClient object to subscribe</param>
	public void PinConsole(CometClient client)
	{
		client.SubscribedCompany = this;
		CometServer.Groups.PinClient(client, this.GroupNameConsoles);
	}

	/// <summary>
	/// Unsubscribe a CometClient from this CometCompany
	/// </summary>
	/// <param name="client">CometClient object to unsubscribe</param>
	public void UnpinConsole(CometClient client)
	{
		client.SubscribedCompany = null;
		CometServer.Groups.UnpinClientFromGroup(client, this.GroupNameConsoles);
	}

	/// <summary>
	/// Add terminal to company. If terminal already exists the client will be added to the list.
	/// </summary>
	/// <param name="terminalId">Terminal Id</param>
	/// <param name="client">Client to link to terminal</param>
	internal void PinTerminal(int terminalId, CometClient client)
	{
		CometServer.Groups.PinClient(client, GroupNameTerminal(terminalId));
	}

	/// <summary>
	/// Remove a client from terminal list.
	/// </summary>
	/// <param name="client"></param>
	internal void UnpinTerminal(CometClient client)
	{
		if (client.TerminalId > 0)
		{
			CometServer.Groups.UnpinClientFromGroup(client, GroupNameTerminal(client.TerminalId));
		}
	}

    /// <summary>
    /// Add master terminal to company. If terminal already exists the client will be added to the list.
    /// </summary>
    /// <param name="client">Client to link to terminal</param>
    internal void PinMasterTerminal(CometClient client)
    {
        CometServer.Groups.PinClient(client, this.GroupNameMasterTerminals);
    }

    /// <summary>
    /// Remove a client from master terminal list.
    /// </summary>
    internal void UnpinMasterTerminal(CometClient client)
    {
        CometServer.Groups.UnpinClientFromGroup(client, this.GroupNameMasterTerminals);
    }
}