﻿using System.Collections.Concurrent;
using System.Collections.Generic;

/// <summary>
/// Summary description for CometGroups
/// </summary>
public class CometGroups
{
	#region Group Names

	public const string GROUP_AGENTS = "Agents";
	public const string GROUP_MOBILECLIENTS = "MobileClients";
	public const string GROUP_TERMINALCLIENTS = "TerminalClients"; // Emenu Console
	public const string GROUP_EXTERNALLISTENERS = "ExternalListeners";
	public const string GROUP_ONSITESERVER = "OnSiteServers";

	#endregion

	/// <summary>
	/// List of groups with assigned CometClient objects
	/// </summary>
	/// <remarks>Key: Group name | Value: [Key: Identifier (Comet) | Value: CometClient]</remarks>
    private readonly ConcurrentDictionary<string, ConcurrentDictionary<string, CometClient>> groups = new ConcurrentDictionary<string, ConcurrentDictionary<string, CometClient>>();

	/// <summary>
	/// Constructor
	/// </summary>
	internal CometGroups()
	{
        groups = new ConcurrentDictionary<string, ConcurrentDictionary<string, CometClient>>();
	}

	/// <summary>
	/// Subscribe a client to a group. If the group is new it will be created.
	/// </summary>
	/// <param name="client">CometClient object to add</param>
	/// <param name="groupName">Name of the group</param>
	public void PinClient(CometClient client, string groupName)
	{
		// Fetch group
		var groupList = GetGroupByName(groupName);

        // Add client or update it
        groupList.AddOrUpdate(client.IdentifierComet, client, (key, oldClient) => client);

        Logger.Debug(client, "Pinned to group (groupName={0}, groupSize={1})", groupName, groupList.Count);
	}

	/// <summary>
	/// Unsubscribe a client from all the subscribed groups
	/// </summary>
	/// <param name="client">CometClient to unsubscribe</param>
	public void UnpinClient(CometClient client)
	{
        foreach (ConcurrentDictionary<string, CometClient> groupList in groups.Values)
		{
            CometClient removedClient;
            if (groupList.TryRemove(client.IdentifierComet, out removedClient))
            {
                Logger.Debug(client, "Unpinned from groups");
            }
		}
	}

	/// <summary>
	/// Unsubscribe a client form a specific group
	/// </summary>
	/// <param name="client">CometClient to unsubscribe</param>
	/// <param name="groupName">Name of the group to unsubscribe the client from</param>
	public void UnpinClientFromGroup(CometClient client, string groupName)
	{
        ConcurrentDictionary<string, CometClient> groupList;
		if (this.groups.TryGetValue(groupName, out groupList))
		{
		    CometClient removedClient;
		    groupList.TryRemove(client.IdentifierComet, out removedClient);

            Logger.Debug(client, "Unpinned from group (groupName={0})", groupName);
		}
	}

	/// <summary>
	/// Get whether a group has any members or not
	/// </summary>
	/// <param name="groupName">Name of the group</param>
	/// <returns>True if this group contains 1 or more members.</returns>
	public bool GroupHasMembers(string groupName)
	{
		var ret = false;

        ConcurrentDictionary<string, CometClient> groupList;
		if (this.groups.TryGetValue(groupName, out groupList))
		{
			ret = (groupList.Count > 0);
		}

		return ret;
	}

	/// <summary>
	/// Get all associated CometClient objects with specified group.
	/// </summary>
	/// <param name="groupName">Name of the group</param>
	/// <returns>List of CometClient objects. An empty list is returned when group doesn't exists.</returns>
	public List<CometClient> GetCometClientsForGroup(string groupName)
	{
        ConcurrentDictionary<string, CometClient> groupList;
		if (this.groups.TryGetValue(groupName, out groupList))
		{
            Logger.Debug(null, "CometGroups - Found requested group. (groupName={0}, groupSize={1}, groupValuesSize={2})", groupName, groupList.Count, groupList.Values.Count);
			return new List<CometClient>(groupList.Values);
		}

        Logger.Debug(null, "CometGroups - Could not find group. (groupName={0})", groupName);

		return new List<CometClient>();
	}

	/// <summary>
	/// Get group by groupname. If there is no such group the group will be created.
	/// </summary>
	/// <param name="groupName">Name of the group you want to retrieve</param>
    private ConcurrentDictionary<string, CometClient> GetGroupByName(string groupName)
	{
	    var groupList = this.groups.GetOrAdd(groupName, new ConcurrentDictionary<string, CometClient>());

		return groupList;
	}
}