﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

/// <summary>
/// Summary description for ConnectionMapping
/// </summary>
public class ConnectionMapping
{
    private readonly Dictionary<string, List<CometClient>> connections = new Dictionary<string, List<CometClient>>();
    private volatile int totalCount = 0;

    public int Count
    {
        get
        {
            return connections.Count;
        }
    }

    public int TotalConnections
    {
        get
        {
            int count = 0;
            lock(connections)
            {
                foreach (var connection in connections.Values)
                {
                    count += connection.Count;
                }
            }
            return count;
        }
    }

    public int TotalCount
    {
        get
        {
            return this.totalCount;
        }
    }

    public Dictionary<string, List<CometClient>> ShallowCopy()
    {
        lock(connections)
        {
            Dictionary<string, List<CometClient>> copy = new Dictionary<string, List<CometClient>>();
            foreach(KeyValuePair<string, List<CometClient>> connection in connections)
            {
                copy.Add(connection.Key, new List<CometClient>(connection.Value));
            }
            return copy;
        }
    }

    public void Add(string identifier, CometClient cometClient)
    {
        lock (connections)
        {
            List<CometClient> users;
            if (!this.connections.TryGetValue(identifier, out users))
            {
                users = new List<CometClient>();
                this.connections.Add(identifier, users);
            }
            Interlocked.Increment(ref this.totalCount);
            users.Add(cometClient);    
        }
    }

    public List<CometClient> GetConnections(string key)
    {
        lock(connections)
        {
            List<CometClient> users;
            if (this.connections.TryGetValue(key, out users))
            {
                return users;
            }
        }

        return Enumerable.Empty<CometClient>().ToList();
    }

    public List<CometClient> GetConnections(Predicate<CometClient> predicate)
    {
        var clientList = new List<CometClient>();

        lock(connections)
        {
            foreach (List<CometClient> clients in connections.Values)
            {
                clientList.AddRange(clients.FindAll(predicate));
            }
        }

        return clientList;
    }

    public void Remove(string identifier, CometClient cometClient)
    {
        lock(connections)
        {
            List<CometClient> users;
            if (!this.connections.TryGetValue(identifier, out users))
            {
                return;
            }

            users.RemoveAll(c => c.IdentifierComet == cometClient.IdentifierComet);
            if (users.Count == 0)
            {
                this.connections.Remove(identifier);
            }
            Interlocked.Decrement(ref this.totalCount);
        }
    }
}