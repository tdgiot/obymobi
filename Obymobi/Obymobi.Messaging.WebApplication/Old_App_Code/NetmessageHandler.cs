﻿using System;
using System.Collections.Generic;
using System.Linq;
using Dionysos;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using Obymobi.Logic.Comet;
using Obymobi.Logic.Comet.Netmessages;
using Obymobi.Logic.HelperClasses;
using Obymobi.Logic.Model;
using SD.LLBLGen.Pro.ORMSupportClasses;

/// <summary>
/// Summary description for NetmessageHandler
/// </summary>
public class NetmessageHandler
{
    /// <summary>
    /// Call this method when you're unable to parse all needed fields
    /// </summary>
    /// <param name="message">Failed Netmessage</param>
    /// <param name="exception">Optional exception thrown</param>
    private static void FailedToParseNetmessage(Netmessage message, Exception exception = null) => Logger.Error(null, "Unable to parse Netmessage. Probably missing some FieldValues.\nException: {0}\n{1}", ((exception != null) ? exception.Message : "NULL"), message.ToJson());

    private static bool TryConvertMessage<T>(Netmessage input, out T output) where T : Netmessage, new()
    {
        try
        {
            output = input.ConvertTo<T>();
        }
        catch (ObymobiNetmessageException ex)
        {
            NetmessageHandler.FailedToParseNetmessage(input, ex);

            output = null;
        }

        return (output != null);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="client"></param>
    /// <param name="message"></param>
    internal static void HandleNetmessage(CometClient client, Netmessage message)
    {
        // Only log this if we're DEBUG logging
        if (Logger.GetLoggingLevel() == LoggingLevel.Debug && message.GetMessageType() != NetmessageType.Pong && message.GetMessageType() != NetmessageType.SystemState)
        {
            string typedMsg = NetmessageHelper.CreateTypedNetmessageModelFromModel(message).ToString();
            Logger.Debug(client, "Received message from client.\nType: {0} - {1}.", message.MessageType, typedMsg);
        }

        message.AddTraceMessage("Message received on CometServer");

        switch (message.GetMessageType())
        {
            case NetmessageType.AnnounceNewMessage:
                NetmessageHandler.AnnounceNewMessage(message);
                break;
            case NetmessageType.OrderStatusUpdated:
                NetmessageHandler.OrderStatusUpdated(message);
                break;
            //case NetmessageType.SetMobileOrdering:
            //    SetMobileOrdering(client, message);
            //    break;
            case NetmessageType.BroadcastMessage:
                NetmessageHandler.BroadcastMessage(message);
                break;
            case NetmessageType.NewSocialMediaMessage:
                NetmessageHandler.NewSocialMediaMessage(message);
                break;
            case NetmessageType.RoutestephandlerStatusUpdated:
                NetmessageHandler.RoutestephandlerStatusUpdated(client, message);
                break;
            case NetmessageType.GetPmsFolio:
            case NetmessageType.GetPmsGuestInformation:
            case NetmessageType.GetPmsExpressCheckout:
                NetmessageHandler.ForwardPmsRequest(message);
                break;
            case NetmessageType.SetPmsCheckedOut:
            case NetmessageType.SetPmsCheckedIn:
            case NetmessageType.SetPmsFolio:
            case NetmessageType.SetPmsGuestInformation:
            case NetmessageType.SetPmsExpressCheckedOut:
                NetmessageHandler.ForwardPmsResponse(message);
                break;
            case NetmessageType.SetForwardTerminal:
                NetmessageHandler.SetForwardTerminal(message);
                break;
            case NetmessageType.SetNetworkInformation:
                NetmessageHandler.SetNetworkInformation(client, message);
                break;
            case NetmessageType.SetClientOperationMode:
                NetmessageHandler.SetClientOperationMode(message);
                break;
            case NetmessageType.SetClientType:
                NetmessageHandler.SetClientType(client, message);
                break;
            case NetmessageType.ConnectToAgent:
                NetmessageHandler.ConnectToAgent(client, message);
                break;
            case NetmessageType.AgentCommandResponse:
                NetmessageHandler.AgentCommandResponse(client, message);
                break;
            case NetmessageType.AgentCommandRequest:
                NetmessageHandler.AgentCommandRequest(client, message);
                break;
            case NetmessageType.NewEventExternalListener:
                NetmessageHandler.NewEventExternalListener(message);
                break;
            case NetmessageType.GetConnectedClients:
                NetmessageHandler.GetConnectedClients(client, message);
                break;
            case NetmessageType.Pong:
                NetmessageHandler.Pong(client, message);
                break;
            case NetmessageType.NewSurveyResult:
                NetmessageHandler.NewSurveyResult(message);
                break;
            case NetmessageType.SetDeliverypointId:
                NetmessageHandler.SetDeliverypointId(message);
                break;
            case NetmessageType.ClientCommand:
                NetmessageHandler.ClientCommand(message);
                break;
            case NetmessageType.TerminalCommand:
                NetmessageHandler.TerminalCommand(message);
                break;
            case NetmessageType.DeviceUnlink:
                NetmessageHandler.DeviceUnlink(message);
                break;
            case NetmessageType.SandboxConfig:
                NetmessageHandler.SandboxConfig(message);
                break;
            case NetmessageType.Test:
                NetmessageHandler.Test();
                break;
            case NetmessageType.DeviceCommandExecute:
                NetmessageHandler.DeviceCommandExecute(message);
                break;
            case NetmessageType.SetCloudEnvironment:
                NetmessageHandler.SetCloudEnvironment(message);
                break;
            case NetmessageType.SystemState:
                NetmessageHandler.SystemState(client);
                break;
            case NetmessageType.SetMasterTab:
                NetmessageHandler.SetMasterTab(client, message);
                break;
            case NetmessageType.DownloadUpdate:
                NetmessageHandler.DownloadUpdate(message);
                break;
            case NetmessageType.SwitchCometHandler:
                NetmessageHandler.SwitchCometHandler(message);
                break;
            case NetmessageType.MenuUpdated:
                NetmessageHandler.MenuUpdated(message);
                break;
            case NetmessageType.SwitchTab:
                NetmessageHandler.SwitchTab(message);
                break;
            case NetmessageType.SendJsonObject:
                NetmessageHandler.SendJsonObject(message);
                break;
            case NetmessageType.RefreshContent:
                NetmessageHandler.RefreshContent(message);
                break;
            case NetmessageType.PmsTerminalStatusUpdated:
                NetmessageHandler.PmsTerminalStatusUpdated(message);
                break;
            case NetmessageType.UpdateDeviceToken:
            case NetmessageType.ClearCompanyCredentials:
                NetmessageHandler.SendNetmessageToDevice(message);
                break;
            default:
                Logger.Error(client, "Received unknown message type in Netmessage.\n{0}", message.ToJson());
                break;
        }
    }

    private static void SwitchCometHandler(Netmessage message)
    {
        if (message.ReceiverClientId > 0)
        {
            CometServer.Instance.SendNetmessage(CometServer.Instance.GetCometClientsByClientId(message.ReceiverClientId), message);
        }
    }

    private static void DownloadUpdate(Netmessage message)
    {
        if (NetmessageHandler.TryConvertMessage(message, out NetmessageDownloadUpdate netmessage))
        {
            if (message.ReceiverClientId > 0)
            {
                CometServer.Instance.SendNetmessage(CometServer.Instance.GetCometClientsByClientId(message.ReceiverClientId), message);
            }
            else if (message.ReceiverTerminalId > 0)
            {
                CometServer.Instance.SendNetmessage(CometServer.Groups.GetCometClientsForGroup(CometCompany.GroupNameTerminal(message.ReceiverTerminalId)), message);
            }
        }
    }

    private static void SetMasterTab(CometClient client, Netmessage message)
    {
        if (NetmessageHandler.TryConvertMessage(message, out NetmessageSetMasterTab netmessage))
        {
            if (netmessage.IsSetFromClient)
            {
                CometServer.Instance.TerminalActAsMaster(client, netmessage.MasterTabEnabled);
            }
            else
            {
                List<CometClient> clientList = CometServer.Groups.GetCometClientsForGroup(CometCompany.GroupNameTerminal(message.ReceiverTerminalId));
                CometServer.Instance.SendNetmessage(clientList, message);
            }
        }
    }

    private static void SandboxConfig(Netmessage message)
    {
        if (NetmessageHandler.TryConvertMessage(message, out NetmessageSandboxConfig netmessage))
        {
            if (CometServer.Instance.TryGetCometSandboxClients(netmessage.ReceiverIdentfier, out List<CometClient> clientList))
            {
                CometServer.Instance.SendNetmessage(clientList, netmessage);
            }
        }
    }

    private static void DeviceUnlink(Netmessage netmessage)
    {
        if (NetmessageHandler.TryConvertMessage(netmessage, out NetmessageDeviceUnlink message))
        {
            if (!message.DeviceMacAddress.IsNullOrWhiteSpace())
            {
                if (CometServer.Instance.TryGetCometClients(message.DeviceMacAddress, out List<CometClient> clientList))
                {
                    CometServer.Instance.SendNetmessage(clientList, netmessage);
                }
            }
            else if (message.ReceiverClientId > 0)
            {
                CometServer.Instance.SendNetmessage(CometServer.Instance.GetCometClientsByClientId(message.ReceiverClientId), message);
            }
            else if (message.ReceiverTerminalId > 0)
            {
                CometServer.Instance.SendNetmessage(CometServer.Groups.GetCometClientsForGroup(CometCompany.GroupNameTerminal(message.ReceiverTerminalId)), message);
            }
        }
    }

    private static void SystemState(CometClient client)
    {
        Dictionary<NetmessageClientType, int[]> totals = CometServer.Instance.ConnectionTotals();

        NetmessageSystemState netmessage = new NetmessageSystemState
        {
            IsWebserviceConnected = (CometServer.Instance.GetCometClientsByProperty(c => c.ClientType == NetmessageClientType.Webservice).Count > 0),
            IsCmsConnected = (CometServer.Instance.GetCometClientsByProperty(c => c.ClientType == NetmessageClientType.Cms).Count > 0),
            IsNocConnected = (CometServer.Instance.GetCometClientsByProperty(c => c.ClientType == NetmessageClientType.MobileNoc).Count > 0),
            ConnectedSupportTools = totals[NetmessageClientType.SupportTools],
            ConnectedTerminals = totals[NetmessageClientType.Console],
            ConnectedEmenu = totals[NetmessageClientType.Emenu],
            ConnectedOnSiteServers = totals[NetmessageClientType.OnsiteServer],
            ConnectedSandboxClients = CometServer.Instance.TotalSandboxConnections()
        };

        int[] totalConnections = new[] { 0, 0, 0 };
        totalConnections[0] = (netmessage.ConnectedEmenu[0] + netmessage.ConnectedTerminals[0] + netmessage.ConnectedSupportTools[0] + netmessage.ConnectedOnSiteServers[0]);
        totalConnections[0] += netmessage.ConnectedSandboxClients;
        totalConnections[0] += (netmessage.IsCmsConnected) ? 1 : 0;
        totalConnections[0] += (netmessage.IsWebserviceConnected) ? 1 : 0;
        totalConnections[0] += (netmessage.IsNocConnected) ? 1 : 0;

        // PokeIn
        totalConnections[1] = (netmessage.ConnectedEmenu[1] + netmessage.ConnectedTerminals[1] + netmessage.ConnectedSupportTools[1] + netmessage.ConnectedOnSiteServers[1]);
        // SignalR
        totalConnections[2] = (netmessage.ConnectedEmenu[2] + netmessage.ConnectedTerminals[2] + netmessage.ConnectedSupportTools[2] + netmessage.ConnectedOnSiteServers[2]);

        netmessage.TotalConnectedClients = totalConnections;

        CometServer.Instance.SendNetmessageToClient(client, netmessage);
    }

    private static void SetCloudEnvironment(Netmessage message)
    {
        List<CometClient> clientList = new List<CometClient>();
        if (message.ReceiverClientId > 0)
        {
            clientList.AddRange(CometServer.Instance.GetCometClientsByClientId(message.ReceiverClientId));
        }
        else if (message.ReceiverTerminalId > 0)
        {
            clientList.AddRange(CometServer.Instance.GetCometClientsByClientId(message.ReceiverTerminalId));
        }

        CometServer.Instance.SendNetmessage(clientList, message);
    }

    private static void DeviceCommandExecute(Netmessage message)
    {
        if (message.ReceiverClientId > 0)
        {
            CometServer.Instance.SendNetmessage(CometServer.Instance.GetCometClientsByClientId(message.ReceiverClientId), message);
        }
        else if (message.ReceiverTerminalId > 0)
        {
            CometServer.Instance.SendNetmessage(CometServer.Groups.GetCometClientsForGroup(CometCompany.GroupNameTerminal(message.ReceiverTerminalId)), message);
        }
    }

    private static void NewSurveyResult(Netmessage netmessage)
    {
        if (NetmessageHandler.TryConvertMessage(netmessage, out NetmessageNewSurveyResult message))
        {
            if (CometServer.Instance.TryGetCometCompanyById(message.ReceiverCompanyId, out CometCompany company))
            {
                List<CometClient> clientList = CometServer.Groups.GetCometClientsForGroup(company.GroupNameConsoles);
                CometServer.Instance.SendNetmessage(clientList, message);
            }
        }
    }

    private static void MenuUpdated(Netmessage message)
    {
        List<CometClient> clientList = new List<CometClient>();
        if (message.ReceiverClientId > 0)
        {
            clientList.AddRange(CometServer.Instance.GetCometClientsByClientId(message.ReceiverClientId));
        }
        CometServer.Instance.SendNetmessage(clientList, message);
    }

    private static void SwitchTab(Netmessage message)
    {
        List<CometClient> clientList = new List<CometClient>();
        if (message.ReceiverClientId > 0)
        {
            clientList.AddRange(CometServer.Instance.GetCometClientsByClientId(message.ReceiverClientId));
        }
        else if (message.ReceiverTerminalId > 0)
        {
            clientList.AddRange(CometServer.Groups.GetCometClientsForGroup(CometCompany.GroupNameTerminal(message.ReceiverTerminalId)));
        }
        CometServer.Instance.SendNetmessage(clientList, message);
    }

    private static void SendJsonObject(Netmessage message)
    {
        List<CometClient> clientList = new List<CometClient>();
        if (message.ReceiverClientId > 0)
        {
            clientList.AddRange(CometServer.Instance.GetCometClientsByClientId(message.ReceiverClientId));
        }
        else if (message.ReceiverTerminalId > 0)
        {
            clientList.AddRange(CometServer.Groups.GetCometClientsForGroup(CometCompany.GroupNameTerminal(message.ReceiverTerminalId)));
        }
        CometServer.Instance.SendNetmessage(clientList, message);
    }

    private static void RefreshContent(Netmessage message)
    {
        if (message.ReceiverClientId > 0)
        {
            CometServer.Instance.SendNetmessage(CometServer.Instance.GetCometClientsByClientId(message.ReceiverClientId), message);
        }
    }

    private static void PmsTerminalStatusUpdated(Netmessage message)
    {
        if (message.ReceiverTerminalId > 0)
        {
            CometServer.Instance.SendNetmessage(CometServer.Groups.GetCometClientsForGroup(CometCompany.GroupNameTerminal(message.ReceiverTerminalId)), message);
        }
    }

    private static void SendNetmessageToDevice(Netmessage message)
    {
        List<CometClient> cometClients = new List<CometClient>();

        if (!message.ReceiverIdentifier.IsNullOrWhiteSpace())
        {
            cometClients.AddRange(CometServer.Instance.GetCometClientsByIdentifier(message.ReceiverIdentifier));
        }

        CometServer.Instance.SendNetmessage(cometClients, message);
    }

    private static void Test()
    {
        //client.Dispose();
    }

    private static DateTime clientListClearedUtc = DateTime.UtcNow;
    private static DateTime clientSupportToolsListClearedUtc = DateTime.UtcNow;
    private static DateTime terminalListClearedUtc = DateTime.UtcNow;
    private static DateTime terminalSupportToolsListClearedUtc = DateTime.UtcNow;

    private static readonly List<int> clientUpdateList = new List<int>();
    private static readonly List<int> clientSupportToolsUpdateList = new List<int>();
    private static readonly List<int> terminalUpdateList = new List<int>();
    private static readonly List<int> terminalSupportToolsUpdateList = new List<int>();

    private static readonly int MAX_UPDATE_CLIENTS = 1250;
    private static readonly int MAX_UPDATE_TERMINALS = 40;

    private static void Pong(CometClient client, Netmessage netmessage)
    {
        bool usingHeartbeatService = false;
        NetmessagePong message = null;
        if (netmessage != null && NetmessageHandler.TryConvertMessage(netmessage, out message))
        {
            usingHeartbeatService = message.HeartbeatServiceActive;
        }

        List<int> copyClientList = null;
        List<int> copyTerminalList = null;

        if (client.ClientType == NetmessageClientType.Emenu && client.ClientId > 0 && client.IncrementalUpdateCount > 0)
        {
            client.UpdateLastPong();

            if (!usingHeartbeatService)
            {
                client.IncrementalUpdateCount--;

                lock (clientUpdateList)
                {
                    if (!clientUpdateList.Contains(client.ClientId) && client.ClientEntity != null)
                    {
                        clientUpdateList.Add(client.ClientId);
                    }

                    if (clientUpdateList.Count >= MAX_UPDATE_CLIENTS || clientListClearedUtc < DateTime.UtcNow.AddMinutes(-1))
                    {
                        copyClientList = new List<int>(clientUpdateList);
                        clientUpdateList.Clear();
                        clientListClearedUtc = DateTime.UtcNow;
                    }
                }
            }
        }
        else if (client.ClientType == NetmessageClientType.SupportTools && client.SupportToolsClientId > 0 && client.IncrementalUpdateCountSupportTools > 0)
        {
            client.UpdateLastPong();

            if (!usingHeartbeatService)
            {
                client.IncrementalUpdateCountSupportTools--;

                lock (clientSupportToolsUpdateList)
                {
                    if (!clientSupportToolsUpdateList.Contains(client.SupportToolsClientId) && client.ClientEntity != null)
                    {
                        clientSupportToolsUpdateList.Add(client.SupportToolsClientId);
                    }

                    if (clientSupportToolsUpdateList.Count >= MAX_UPDATE_CLIENTS || clientSupportToolsListClearedUtc < DateTime.UtcNow.AddMinutes(-1))
                    {
                        copyClientList = new List<int>(clientSupportToolsUpdateList);
                        clientSupportToolsUpdateList.Clear();
                        clientSupportToolsListClearedUtc = DateTime.UtcNow;
                    }
                }
            }
        }
        else if (client.ClientType == NetmessageClientType.Console && client.TerminalId > 0 && client.IncrementalUpdateCount > 0)
        {
            client.UpdateLastPong();

            if (!usingHeartbeatService)
            {
                client.IncrementalUpdateCount--;

                lock (terminalUpdateList)
                {
                    if (!terminalUpdateList.Contains(client.TerminalId) && client.TerminalEntity != null)
                    {
                        terminalUpdateList.Add(client.TerminalId);
                    }

                    if (terminalUpdateList.Count >= MAX_UPDATE_TERMINALS || terminalListClearedUtc < DateTime.UtcNow.AddMinutes(-1))
                    {
                        copyTerminalList = new List<int>(terminalUpdateList);
                        terminalUpdateList.Clear();
                        terminalListClearedUtc = DateTime.UtcNow;
                    }
                }
            }
        }
        else if (client.ClientType == NetmessageClientType.SupportTools && client.IncrementalUpdateCountSupportTools > 0)
        {
            client.UpdateLastPong();

            if (!usingHeartbeatService)
            {
                client.IncrementalUpdateCountSupportTools--;

                lock (terminalSupportToolsUpdateList)
                {
                    if (!terminalSupportToolsUpdateList.Contains(client.TerminalEntity.TerminalId) && client.TerminalEntity != null && client.TerminalEntity.TerminalId > 0)
                    {
                        terminalSupportToolsUpdateList.Add(client.TerminalEntity.TerminalId);
                    }

                    if (terminalSupportToolsUpdateList.Count >= MAX_UPDATE_TERMINALS || terminalSupportToolsListClearedUtc < DateTime.UtcNow.AddMinutes(-1))
                    {
                        copyTerminalList = new List<int>(terminalSupportToolsUpdateList);
                        terminalSupportToolsUpdateList.Clear();
                        terminalSupportToolsListClearedUtc = DateTime.UtcNow;
                    }
                }
            }
        }
        else
        {
            if (client.ClientType == NetmessageClientType.Emenu)
            {
                client.IncrementalUpdateCount = 10;
            }
            else if (client.ClientType == NetmessageClientType.SupportTools)
            {
                client.IncrementalUpdateCountSupportTools = 10;
            }
            else if (client.ClientType == NetmessageClientType.Console)
            {
                client.IncrementalUpdateCount = 10;
            }

            if (!usingHeartbeatService && (message != null || client.ClientType == NetmessageClientType.OnsiteServer))
            {
                client.Pong(message);
            }
        }

        if (copyClientList != null)
        {
            Logger.Verbose(client, "Handling '{0}' {1} pongs", copyClientList.Count, client.ClientType == NetmessageClientType.Emenu ? "emenu" : "(emenu) supporttools");

            PredicateExpression filter = new PredicateExpression(ClientFields.ClientId == copyClientList);
            RelationCollection relations = new RelationCollection(ClientEntity.Relations.DeviceEntityUsingDeviceId);

            if (client.ClientType == NetmessageClientType.Emenu)
            {
                DeviceEntity deviceEntity = new DeviceEntity { LastRequestUTC = DateTime.UtcNow };
                new DeviceCollection().UpdateMulti(deviceEntity, filter, relations);
            }
            else
            {
                DeviceEntity deviceEntity = new DeviceEntity
                {
                    LastSupportToolsRequestUTC = DateTime.UtcNow,
                    SupportToolsRunning = true
                };
                new DeviceCollection().UpdateMulti(deviceEntity, filter, relations);
            }
        }

        if (copyTerminalList != null)
        {
            Logger.Verbose(client, "Handling '{0}' {1} pongs", copyTerminalList.Count, client.ClientType == NetmessageClientType.Console ? "console" : "(console) supporttools");

            PredicateExpression filter = new PredicateExpression(TerminalFields.TerminalId == copyTerminalList);
            RelationCollection relations = new RelationCollection(TerminalEntity.Relations.DeviceEntityUsingDeviceId);

            if (client.ClientType == NetmessageClientType.Console)
            {
                DeviceEntity deviceEntity = new DeviceEntity { LastRequestUTC = DateTime.UtcNow };
                new DeviceCollection().UpdateMulti(deviceEntity, filter, relations);
            }
            else
            {
                DeviceEntity deviceEntity = new DeviceEntity
                {
                    LastSupportToolsRequestUTC = DateTime.UtcNow,
                    SupportToolsRunning = true
                };
                new DeviceCollection().UpdateMulti(deviceEntity, filter, relations);
            }
        }
    }

    private static void GetConnectedClients(CometClient client, Netmessage netmessage)
    {
        if (NetmessageHandler.TryConvertMessage(netmessage, out NetmessageGetConnectedClients message))
        {
            if (message.CompanyId > 0)
            {
                List<CometClient> clientList = CometServer.Instance.GetCometClientsByProperty(c => c.SubscribedCompany != null && c.SubscribedCompany.CompanyId == message.CompanyId);
                List<CometClient> agentList = CometServer.Groups.GetCometClientsForGroup(CometGroups.GROUP_AGENTS);

                List<string> emenuList = new List<string>();
                List<string> terminalList = new List<string>();
                List<string> supportToolsList = new List<string>();

                foreach (CometClient cometClient in clientList)
                {
                    if (cometClient.ClientType == NetmessageClientType.Emenu)
                    {
                        if (!emenuList.Contains(cometClient.Identifier))
                        {
                            emenuList.Add(cometClient.Identifier);
                        }
                    }
                    else if (cometClient.ClientType == NetmessageClientType.Console)
                    {
                        if (!terminalList.Contains(cometClient.Identifier))
                        {
                            terminalList.Add(cometClient.Identifier);
                        }
                    }

                    if (!supportToolsList.Contains(cometClient.Identifier))
                    {
                        // Check if client has supportools
                        if (agentList.Exists(agent => agent.Identifier == cometClient.Identifier))
                        {
                            supportToolsList.Add(cometClient.Identifier);
                        }
                    }
                }

                foreach (CometClient cometClient in agentList)
                {
                    if (cometClient.ClientType == NetmessageClientType.SupportTools)
                    {
                        if (!supportToolsList.Contains(cometClient.Identifier) && cometClient.SubscribedCompany != null && cometClient.SubscribedCompany.CompanyId == message.CompanyId)
                        {
                            supportToolsList.Add(cometClient.Identifier);
                        }
                    }
                }

                // Convert client list to mac array
                message.ClientIdentifiers = emenuList.ToArray();

                // Convert terminal list to mac array
                message.TerminalIdentifiers = terminalList.ToArray();

                // Convert supporttools list to mac array
                message.SupportToolsIdentifiers = supportToolsList.ToArray();
            }

            string sandboxClients = CometServer.Instance.GetSandboxClientIdentifiers();
            message.SandboxIdentifiers = sandboxClients;

            // Send message back to client
            client.TransmitMessage(message);
        }
    }

    private static void NewEventExternalListener(Netmessage netmessage)
    {
        if (NetmessageHandler.TryConvertMessage(netmessage, out NetmessageNewEventExternalListener message))
        {
            if (message.CompanyId > 0)
            {
                if (CometServer.Instance.TryGetCometCompanyById(message.CompanyId, out CometCompany company))
                {
                    List<CometClient> clientList = CometServer.Groups.GetCometClientsForGroup(company.GroupNameExternal);
                    CometServer.Instance.SendNetmessage(clientList, message);
                }
            }

            CometServer.Instance.SendNetmessage(CometServer.Groups.GetCometClientsForGroup("Company-External-0"), netmessage);
        }
    }

    private static void AgentCommandRequest(CometClient client, Netmessage netmessage)
    {
        if (NetmessageHandler.TryConvertMessage(netmessage, out NetmessageAgentCommandRequest message))
        {
            List<CometClient> clientList = CometServer.Groups.GetCometClientsForGroup("Agent-" + message.AgentMacAddress);
            clientList.RemoveAll(cometClient => cometClient.IdentifierComet == client.IdentifierComet);

            // Send command to other connected pages
            NetmessageAgentCommandResponse cmdMessage = new NetmessageAgentCommandResponse
            {
                ReponseMessage = "> " + message.Command
            };
            CometServer.Instance.SendNetmessage(clientList, cmdMessage);

            // Send command to agent
            List<CometClient> agentList = CometServer.Groups.GetCometClientsForGroup(CometGroups.GROUP_AGENTS).FindAll(agent => agent.Identifier == message.AgentMacAddress);
            CometServer.Instance.SendNetmessage(agentList, message);
        }
    }

    private static void AgentCommandResponse(CometClient client, Netmessage netmessage)
    {
        if (NetmessageHandler.TryConvertMessage(netmessage, out NetmessageAgentCommandResponse message))
        {
            List<CometClient> clientList;
            if (message.SenderMacAddress.Length > 0)
            {
                clientList = CometServer.Groups.GetCometClientsForGroup("Agent-" + message.SenderMacAddress);
            }
            else
            {
                clientList = CometServer.Groups.GetCometClientsForGroup("Agent-" + client.Identifier);
            }

            CometServer.Instance.SendNetmessage(clientList, message);
        }
    }

    private static void ConnectToAgent(CometClient client, Netmessage netmessage)
    {
        if (client == null)
        {
            throw new ArgumentNullException("client");
        }
        if (NetmessageHandler.TryConvertMessage(netmessage, out NetmessageConnectToAgent message))
        {
            CometServer.Instance.SubscribeToAgent(client, message.AgentMacAddress);
        }
    }

    private static void AnnounceNewMessage(Netmessage netmessage)
    {
        if (NetmessageHandler.TryConvertMessage(netmessage, out NetmessageAnnounceNewMessage message))
        {
            if (message.ReceiverClientId > 0)
            {
                List<CometClient> clientList = CometServer.Instance.GetCometClientsByClientId(message.ReceiverClientId);

                // Send message to all clients
                CometServer.Instance.SendNetmessage(clientList, message);
            }
        }
    }

    private static void SetDeliverypointId(Netmessage netmessage)
    {
        if (NetmessageHandler.TryConvertMessage(netmessage, out NetmessageSetDeliverypointId message))
        {
            List<CometClient> clientList = new List<CometClient>();
            if (message.ReceiverClientId > 0)
            {
                clientList.AddRange(CometServer.Instance.GetCometClientsByClientId(message.ReceiverClientId));
            }

            CometServer.Instance.SendNetmessage(clientList, netmessage);
        }
    }

    private static void ClientCommand(Netmessage netmessage)
    {
        if (NetmessageHandler.TryConvertMessage(netmessage, out NetmessageClientCommand message))
        {
            List<CometClient> clientList = new List<CometClient>();
            if (message.ReceiverClientId > 0)
            {
                clientList.AddRange(CometServer.Instance.GetCometClientsByClientId(message.ReceiverClientId));
            }

            if (message.ToSupportTools && clientList.Count > 0)
            {
                List<string> usedIdentifiers = new List<string>();
                foreach (CometClient cometClient in clientList)
                {
                    if (!usedIdentifiers.Contains(cometClient.Identifier))
                    {
                        usedIdentifiers.Add(cometClient.Identifier);

                        List<CometClient> agentList = CometServer.Groups.GetCometClientsForGroup(CometGroups.GROUP_AGENTS).FindAll(agent => agent.Identifier == cometClient.Identifier);
                        CometServer.Instance.SendNetmessage(agentList, netmessage);
                    }
                }
            }
            else
            {
                CometServer.Instance.SendNetmessage(clientList, netmessage);
            }
        }
    }

    private static void TerminalCommand(Netmessage netmessage)
    {
        if (NetmessageHandler.TryConvertMessage(netmessage, out NetmessageTerminalCommand message))
        {
            List<CometClient> clientList = new List<CometClient>();
            if (message.ReceiverTerminalId > 0)
            {
                clientList.AddRange(CometServer.Groups.GetCometClientsForGroup(CometCompany.GroupNameTerminal(message.ReceiverTerminalId)));
            }

            if (message.ToSupportTools && clientList.Count > 0)
            {
                List<string> usedIdentifiers = new List<string>();
                foreach (CometClient cometClient in clientList)
                {
                    if (!usedIdentifiers.Contains(cometClient.Identifier))
                    {
                        usedIdentifiers.Add(cometClient.Identifier);

                        List<CometClient> agentList = CometServer.Groups.GetCometClientsForGroup(CometGroups.GROUP_AGENTS).FindAll(agent => agent.Identifier == cometClient.Identifier);
                        CometServer.Instance.SendNetmessage(agentList, netmessage);
                    }
                }
            }
            else
            {
                CometServer.Instance.SendNetmessage(clientList, netmessage);
            }
        }
    }

    private static void OrderStatusUpdated(Netmessage netmessage)
    {
        if (NetmessageHandler.TryConvertMessage(netmessage, out NetmessageOrderStatusUpdated message))
        {
            int receiverClientId = message.ReceiverClientId;
            int receiverTerminalId = message.ReceiverTerminalId;
            List<CometClient> clientList = new List<CometClient>();

            if (receiverClientId > 0)
            {
                clientList.AddRange(CometServer.Instance.GetCometClientsByClientId(receiverClientId));
            }

            if (receiverTerminalId > 0)
            {
                clientList.AddRange(NetmessageHandler.GetCometClientsForTerminalWithMasterTerminals(receiverTerminalId, netmessage.SenderCompanyId));
            }

            CometServer.Instance.SendNetmessage(clientList, message);
        }
    }

    private static void BroadcastMessage(Netmessage netmessage)
    {
        if (NetmessageHandler.TryConvertMessage(netmessage, out NetmessageBroadcastMessage message))
        {
            List<CometClient> clientList = new List<CometClient>();
            if (CometServer.Instance.TryGetCometCompanyById(message.ReceiverCompanyId, out CometCompany company))
            {
                List<CometClient> companyClients = CometServer.Groups.GetCometClientsForGroup(company.GroupNameClients);

                if (message.ReceiverDeliverypointId > 0)
                {
                    foreach (CometClient companyClient in companyClients)
                    {
                        if (companyClient.DeliverypointId == message.ReceiverDeliverypointId)
                        {
                            clientList.Add(companyClient);
                        }
                    }
                }
                else
                {
                    clientList.AddRange(companyClients);
                }
            }

            CometServer.Instance.SendNetmessage(clientList, message);
        }
    }

    private static void NewSocialMediaMessage(Netmessage netmessage)
    {
        if (NetmessageHandler.TryConvertMessage(netmessage, out NetmessageNewSocialMediaMessage message))
        {
            if (CometServer.Instance.TryGetCometCompanyById(message.ReceiverCompanyId, out CometCompany company))
            {
                List<CometClient> clientList = CometServer.Groups.GetCometClientsForGroup(company.GroupNameConsoles);
                CometServer.Instance.SendNetmessage(clientList, message);
            }
        }
    }

    private static void RoutestephandlerStatusUpdated(CometClient client, Netmessage netmessage)
    {
        if (NetmessageHandler.TryConvertMessage(netmessage, out NetmessageRoutestephandlerStatusUpdated message))
        {
            List<CometClient> clientList = new List<CometClient>();
            if (message.ReceiverTerminalId > 0)
            {
                clientList.AddRange(NetmessageHandler.GetCometClientsForTerminalWithMasterTerminals(message.ReceiverTerminalId, message.SenderCompanyId));
            }

            CometServer.Instance.SendNetmessage(clientList, message);
        }
    }

    private static void ForwardPmsRequest(Netmessage message)
    {
        message.SaveToDatabase = true;
        if (message.ReceiverTerminalId > 0)
        {
            List<CometClient> clientList = CometServer.Groups.GetCometClientsForGroup(CometGroups.GROUP_ONSITESERVER)
                .FindAll(terminal => terminal.TerminalId == message.ReceiverTerminalId);

            CometServer.Instance.SendNetmessage(clientList, message);
        }
    }

    private static void ForwardPmsResponse(Netmessage message)
    {
        message.SaveToDatabase = true;
        if (message.ReceiverClientId > 0)
        {
            List<CometClient> clientList = CometServer.Instance.GetCometClientsByClientId(message.ReceiverClientId);

            CometServer.Instance.SendNetmessage(clientList, message);
        }
    }

    private static void SetForwardTerminal(Netmessage netmessage)
    {
        if (NetmessageHandler.TryConvertMessage(netmessage, out NetmessageSetForwardedTerminal message))
        {
            List<CometClient> clientList = CometServer.Groups.GetCometClientsForGroup(CometCompany.GroupNameTerminal(message.ReceiverTerminalId));
            CometServer.Instance.SendNetmessage(clientList, message);
        }
    }

    private static void SetNetworkInformation(CometClient client, Netmessage netmessage)
    {
        if (NetmessageHandler.TryConvertMessage(netmessage, out NetmessageSetNetworkInformation message))
        {
            if (message.InternalIP.Length > 0)
            {
                client.IpAddressLocal = message.InternalIP;
            }

            if (message.ExternalIP.Length > 0)
            {
                client.IpAddressExtern = message.ExternalIP;
            }
        }
    }

    private static void SetClientOperationMode(Netmessage netmessage)
    {
        if (NetmessageHandler.TryConvertMessage(netmessage, out NetmessageSetClientOperationMode message) && message.ReceiverClientId > 0)
        {
            List<CometClient> clients = CometServer.Instance.GetCometClientsByClientId(message.ReceiverClientId);
            Global.Log("NetmessageHandler", $"SetClientOperationMode for netmessage '{netmessage.NetmessageId}' to {clients.Count} clients. (ReceiverClientId={message.ReceiverClientId})");
            CometServer.Instance.SendNetmessage(clients, message);
        }
    }

    private static void SetClientType(CometClient client, Netmessage netmessage)
    {
        if (NetmessageHandler.TryConvertMessage(netmessage, out NetmessageSetClientType message))
        {
            // Check if client type is something else
            if (client.ClientType != NetmessageClientType.Unknown && client.ClientType != message.ClientType)
            {
                // Unpin from all groups
                CometServer.Groups.UnpinClient(client);

                // Reset variables
                client.SubscribedCompany = null;
                client.SubscribedDeliverypointgroup = null;
                client.TerminalId = 0;
                client.DeliverypointId = 0;
            }

            client.ClientType = message.ClientType;
            Logger.Verbose(client, "Client '{0}' registered as {1}", client.Identifier, client.ClientType);

            if (message.ClientType == NetmessageClientType.Emenu)
            {
                client.ClientId = message.ClientId;
                client.DeliverypointId = message.DeliverypointId;

                // Pin to mobile clients group
                CometServer.Groups.PinClient(client, CometGroups.GROUP_MOBILECLIENTS);

                if (message.CompanyId > 0)
                {
                    client.SubscribeToCompany(message.CompanyId);
                }

                if (message.DeliverypointgroupId > 0)
                {
                    client.SubscribeToDeliverypointgroup(message.DeliverypointgroupId);
                }
            }
            else if (message.ClientType == NetmessageClientType.Console)
            {
                // Pin the PokeIn client to the terminals group and add it to the list of terminals
                CometServer.Groups.PinClient(client, CometGroups.GROUP_TERMINALCLIENTS);

                CometServer.Instance.SubscribeAsTerminal(client, message.TerminalId);

                // TODO: Keep this for backwards compatability with Android version July 2013
                if (message.MessageVersion == 0)
                {
                    CometServer.Instance.TerminalActAsMaster(client, message.IsMasterConsole);
                }
            }
            else if (message.ClientType == NetmessageClientType.OnsiteServer)
            {
                // Pin the PokeIn client to the terminals group and add it to the list of OSS
                CometServer.Groups.PinClient(client, CometGroups.GROUP_ONSITESERVER);

                CometServer.Instance.SubscribeAsTerminal(client, message.TerminalId);
                CometServer.Instance.TerminalActAsMaster(client, true);
            }
            else if (message.ClientType == NetmessageClientType.SupportTools)
            {
                CometServer.Groups.PinClient(client, CometGroups.GROUP_AGENTS);

                if (message.CompanyId > 0)
                {
                    if (CometServer.Instance.TryGetCometCompanyById(message.CompanyId, out CometCompany company))
                    {
                        client.SubscribedCompany = company;
                    }
                }

                // Send "Agent Online" message to connected external listeners
                List<CometClient> clientList = CometServer.Groups.GetCometClientsForGroup("Agent-" + client.Identifier);
                if (clientList.Count > 0)
                {
                    NetmessageAgentCommandResponse onlineMsg = new NetmessageAgentCommandResponse
                    {
                        ReponseMessage = "NOTICE: Crave Agent is (back) online!"
                    };
                    CometServer.Instance.SendNetmessage(clientList, onlineMsg);
                }
            }
            else if (message.ClientType == NetmessageClientType.External)
            {
                CometServer.Groups.PinClient(client, CometGroups.GROUP_EXTERNALLISTENERS);

                if (message.CompanyId > 0)
                {
                    if (CometServer.Instance.TryGetCometCompanyById(message.CompanyId, out CometCompany company))
                    {
                        CometServer.Groups.PinClient(client, company.GroupNameExternal);
                    }
                }
                else
                {
                    CometServer.Groups.PinClient(client, "Company-External-0");
                }
            }
            else if (message.ClientType == NetmessageClientType.Cms || message.ClientType == NetmessageClientType.Webservice || message.ClientType == NetmessageClientType.MobileNoc)
            {
                // Nothing here...
            }

            if (client.ClientType == NetmessageClientType.Unknown)
            {
                client.Dispose();
            }
            else
            {
                // Send pending messages if we have a terminal or client id
                if (client.TerminalId > 0)
                {
                    List<Netmessage> pendingMessages = NetmessageHelper.GetNetmessagesForTerminal(client.TerminalId);
                    foreach (Netmessage pendingMessage in pendingMessages)
                    {
                        client.TransmitMessage(pendingMessage);
                    }
                }
                else if (client.ClientId > 0)
                {
                    List<Netmessage> pendingMessages = NetmessageHelper.GetNetmessagesForClient(client.ClientId);
                    foreach (Netmessage pendingMessage in pendingMessages)
                    {
                        client.TransmitMessage(pendingMessage);
                    }
                }
            }

            // Since this is (normally) the first message send after authenticating, we call Pong 
            // so the LastCommunicationMethod will be upated for this client
            Pong(client, null);
        }
    }

    private static List<CometClient> GetCometClientsForTerminalWithMasterTerminals(int terminalId, int companyId)
    {
        List<CometClient> clientList = new List<CometClient>();
        clientList.AddRange(CometServer.Groups.GetCometClientsForGroup(CometCompany.GroupNameTerminal(terminalId)));

        if (clientList.Count > 0)
        {
            CometClient terminalClient = clientList[clientList.Count - 1];
            if (terminalClient.SubscribedCompany != null)
            {
                clientList.AddRange(CometServer.Groups.GetCometClientsForGroup(terminalClient.SubscribedCompany.GroupNameMasterTerminals));
                clientList = clientList.Distinct().ToList();
            }
        }
        else if (companyId > 0)
        {
            if (CometServer.Instance.TryGetCometCompanyById(companyId, out CometCompany company))
            {
                clientList.AddRange(CometServer.Groups.GetCometClientsForGroup(company.GroupNameMasterTerminals));
                clientList = clientList.Distinct().ToList();
            }
        }
        return clientList;
    }
}
