﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

public class MessagingConstants
{
    public const string GroupCompanyFormat = "Company-{0}";
    public const string GroupCompanyTerminalsFormat = "Company-{0}-Terminals";
    public const string GroupDeliverypointFormat = "Deliverypoint-{0}";
    public const string GroupDeliverypointgroupFormat = "Deliverypointgroup-{0}";

    public const string GroupCompanyRegex = @"Company-(\d+)$";
    public const string GroupCompanyTerminalsRegex = @"Company-(\d+)-Terminals$";
    public const string GroupDeliverypointRegex = @"Deliverypoint-(\d+)$";
    public const string GroupDeliverypointgroupRegex = @"Deliverypointgroup-(\d+)$";
}