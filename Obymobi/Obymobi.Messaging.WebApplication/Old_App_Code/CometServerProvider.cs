﻿using Obymobi.Logic.Comet.Interfaces;
using Obymobi.Logic.Model;
using System.Web;

/// <summary>
/// Summary description for CometServerProvider
/// </summary>
public class CometServerProvider : ICometProvider
{
    readonly CometClient fakeClient;
	public CometServerProvider()
	{
        fakeClient = new DummyCometClient();
	}

    public void Initialize()
    {
        
    }

    public void SendMessage(Netmessage netmessage)
    {
        NetmessageHandler.HandleNetmessage(fakeClient, netmessage);
    }

    public void AddHandler(int key, IMessageHandler handler, HttpContext context)
    {
        
    }

    public void RemoveHandler(int key)
    {

    }
}