﻿using Obymobi.Enums;
using Obymobi.Logic.Comet.Netmessages;
using Obymobi.Logic.HelperClasses;
using Obymobi.Logic.Model;

/// <summary>
/// Container for Deliverypointgroup information in CometServer
/// </summary>
public class CometDeliverypointgroup
{
	#region Fields

	/// <summary>
	/// Deliverypointgroup id
	/// </summary>
	public int DeliverypointgroupId { get; private set; }

	/// <summary>
	/// Company id this deliverypointgroup belongs to
	/// </summary>
	public int CompanyId { get; private set; }

	/// <summary>
	/// The linked <see cref="Obymobi.Logic.Model.Deliverypointgroup"/> model
	/// </summary>
	public Deliverypointgroup ObymobiDeliverypointgroup { get; private set; }

	/// <summary>
	/// Linked <see cref="CometCompany"/>
	/// </summary>
	public CometCompany Company { get; private set; }

	#endregion

	#region Deliverypointgroup Groups
	
	/// <summary>
	/// Gets the group name clients.
	/// </summary>
	public string GroupNameClients
	{
		get { return "Deliverypointgroup-Clients-" + this.DeliverypointgroupId; }
	}

	#endregion

	/// <summary>
	/// <para>Creates a new CometDeliverypointgroup object. Will automatically be linked to <see cref="Obymobi.Logic.Model.Deliverypointgroup"/> model.</para>
	/// <para>Exception will be thrown if there is no deliverypointgroup with supplied id.</para>
	/// </summary>
	/// <param name="deliverypointgroupId">The deliverypointgroup id.</param>
	public CometDeliverypointgroup(int deliverypointgroupId)
	{
		this.DeliverypointgroupId = deliverypointgroupId;
		this.ObymobiDeliverypointgroup = DeliverypointgroupHelper.GetDeliverypointgroupModelByDeliverypointgroupId(deliverypointgroupId);
		this.CompanyId = this.ObymobiDeliverypointgroup.CompanyId;

		CometCompany company;
		if (CometServer.Instance.TryGetCometCompanyById(this.CompanyId, out company))
		{
			Company = company;
			Company.AddDeliverypointgroup(this);
		}
	}

    /// <summary>
    /// Subscribe a CometClient to this CometDeliverypointgroup
    /// </summary>
    /// <param name="client">CometClient object to subscribe</param>
    public void PinClient(CometClient client)
    {
        client.SubscribedDeliverypointgroup = this;
        CometServer.Groups.PinClient(client, this.GroupNameClients);

        //// Send ordering state of this deliverypointgroup to the client
        //var message = new NetmessageSetMobileOrdering();
        //message.ReceiverClientId = client.ClientId;
        //message.IsOrderingStateActive = IsOrderingEnabled();

        //CometServer.Instance.SendNetmessageToClient(client, message);
    }

	/// <summary>
	/// Unsubscribe a CometClient from this CometDeliverypointgroup
	/// </summary>
	/// <param name="client">CometClient object to unsubscribe</param>
	public void UnpinClient(CometClient client)
	{
		client.SubscribedDeliverypointgroup = null;
		CometServer.Groups.PinClient(client, this.GroupNameClients);
	}

	/// <summary>
	/// Checks if it's possible to order anything using this deliverypointgroup.
	/// </summary>
	/// <returns>True if ordering is enabled, otherwise false.</returns>
	public bool IsOrderingEnabled()
	{
		return DeliverypointgroupHelper.GetOrderingEnabled(this.DeliverypointgroupId);
	}

    ///// <summary>
    ///// Send current ordering state to all connected clients
    ///// </summary>
    //public void SendOrderingState()
    //{
    //    var message = new NetmessageSetMobileOrdering();
    //    message.IsOrderingStateActive = IsOrderingEnabled();

    //    var clientList = CometServer.Groups.GetCometClientsForGroup(this.GroupNameClients);
    //    if (clientList.Count > 0)
    //    {
    //        CometServer.Instance.SendNetmessage(clientList, message);
    //    }
    //}
}