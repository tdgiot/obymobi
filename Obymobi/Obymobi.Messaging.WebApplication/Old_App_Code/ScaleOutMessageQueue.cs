﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using Crave.Backend.MessageQueue.Requests;
using Google.Apis.Manual.Util;
using Models;
using Newtonsoft.Json;
using Obymobi;
using Obymobi.Enums;
using Obymobi.Logic.Model;
using ServiceStack.Messaging;
using ServiceStack.Messaging.Redis;
using ServiceStack.Redis;

public class ScaleOutMessageQueue : IDisposable
{
    private readonly DummyCometClient dummyClient;
    private RedisMqServer mqServer;

    private readonly CancellationTokenSource cancellationTokenSource;
    private readonly CancellationToken cancellationToken;

    public class PingPong
    {
        public PingPong() => this.Ticks = DateTime.UtcNow.Ticks;

        public long Ticks { get; set; }
    }

    private PingPong lastPingPong = new PingPong();
    private long pingPongCounter = 0;

    public ScaleOutMessageQueue()
    {
        this.dummyClient = new DummyCometClient();

        this.cancellationTokenSource = new CancellationTokenSource();
        this.cancellationToken = this.cancellationTokenSource.Token;

        string redisConnString = System.Configuration.ConfigurationManager.AppSettings["RedisHost"];

        if (redisConnString.IsNullOrEmpty())
        {
            redisConnString = "localhost";
        }

        Global.Log("MessageQueueReceiver", "Connecting to Redis on {0}", redisConnString);

        PooledRedisClientManager redisClientManager = new PooledRedisClientManager(redisConnString)
        {
            ConnectTimeout = 12,
            IdleTimeOutSecs = 30,
            PoolTimeout = 3
        };

        using (redisClientManager.GetClient())
        {
            Global.Log("MessageQueueReceiver", "Test client success");
        }

        this.mqServer = new RedisMqServer(redisClientManager, 1, TimeSpan.FromSeconds(5));
        this.mqServer.ErrorHandler += HandleMqServerError;
        this.mqServer.RegisterHandler<SendNetmessageRequest>(SendNetmessage);
        this.mqServer.RegisterHandler<PingPong>(HandlePingPong);
        this.mqServer.RedisPubSub.OnError += OnRedisPubSubError;

        Global.Log("MessageQueueReceiver", "Starting MQ");
        this.mqServer.Start();
        Global.Log("MessageQueueReceiver", "Starting MQ complete");

        Task.Factory.StartNew(PingPongLoop, CancellationToken.None, TaskCreationOptions.LongRunning, TaskScheduler.Default);
    }

    private void OnRedisPubSubError(Exception exception) => Global.Log("MessageQueueReceiver", "RedisPubSub ErrorHandler: {0}", exception.Message);

    private static void HandleMqServerError(Exception exception) => Global.Log("MessageQueueReceiver", "ErrorHandler: {0}", exception.Message);

    private MessageQueueReponse SendNetmessage(IMessage<SendNetmessageRequest> message)
    {
        NetmessageContainer container = message.GetBody().Container;

        ProcessMessageFn(container);

        return new MessageQueueReponse { Success = true };
    }

    private MessageQueueReponse HandlePingPong(IMessage<PingPong> message)
    {
        PingPong smolPp = message.GetBody();

        Global.Log("MessageQueueReceiver", "Received ping: {0} (Local: {1})", smolPp.Ticks, this.lastPingPong.Ticks);

        //Global.Log("MessageQueueReceiver", "Reset ping counter");
        Interlocked.Exchange(ref this.pingPongCounter, 0);

        return new MessageQueueReponse { Success = true };
    }

    public void PublishMessage(Netmessage netmessage)
    {
        //if (this.messageTxQueue.IsAddingCompleted || this.cancellationToken.IsCancellationRequested)
        //{
        //    return;
        //}

        //if (netmessage.ReceiverIdentifier.IsNullOrEmpty())
        //{
        //    return;
        //}

        //NetmessageContainer container = new NetmessageContainer
        //                                {
        //                                    Guid = netmessage.Guid,
        //                                    Instance = "Legacy",
        //                                    Sender = netmessage.SenderIdentifier,
        //                                    Receiver = netmessage.ReceiverIdentifier,
        //                                    Type = netmessage.MessageType,
        //                                    Data = JsonConvert.SerializeObject(netmessage, this.jsonSettingsCamelCase)
        //                                };
        //this.messageTxQueue.Add(container, this.cancellationToken);
    }

    private void ProcessMessageFn(NetmessageContainer container)
    {
        Netmessage netmessage;
        try
        {
            if (container.Data.Length == 0)
            {
                Global.Log("MessageQueueReceiver", "Container data length is 0");
                return;
            }

            netmessage = (Netmessage)JsonConvert.DeserializeObject(container.Data, typeof(Netmessage));
            if (netmessage == null)
            {
                Global.Log("MessageQueueReceiver", "Invalid container data");
                return;
            }
        }
        catch (Exception ex)
        {
            Global.Log("MessageQueueReceiver", "Exception: {0}", ex.Message);
            return;
        }

        Global.Log("MessageQueueReceiver", "Received message: {0}", container.ToString());

        List<CometClient> cometClients = new List<CometClient>();
        if (container.Groups.IsNullOrEmpty())
        {
            if (container.Receiver.IsNotNullOrEmpty())
            {
                CometServer.Instance.TryGetCometClients(container.Receiver, out cometClients);
            }
            else if (netmessage.ReceiverIdentifier.IsNotNullOrEmpty())
            {
                CometServer.Instance.TryGetCometClients(netmessage.ReceiverIdentifier, out cometClients);
            }

            if (cometClients == null)
            {
                cometClients = new List<CometClient>();
            }
        }
        else
        {
            foreach (string groupName in container.Groups)
            {
                cometClients.AddRange(CometServer.Groups.GetCometClientsForGroup(ConvertFromNewApiGroupName(groupName)));
            }
        }

        Global.Log("MessageQueueReceiver", "Sending netmessage to {0} clients.", cometClients.Count);

        if (cometClients.Count == 0 && CometServer.Instance.AwsIoTConnection != null)
        {
            // Handle even if there are no clients so the message is published to AWS IoT
            if (string.IsNullOrWhiteSpace(netmessage.Guid))
            {
                netmessage.Guid = Guid.NewGuid().ToString("N");
            }

            if (string.IsNullOrWhiteSpace(netmessage.ReceiverIdentifier))
            {
                netmessage.ReceiverIdentifier = container.Receiver;
                netmessage.SenderIdentifier = container.Sender;
            }

            Global.Log("MessageQueueReceiver", $"Sending message to IoT on topic '{container.Topic}':{Environment.NewLine}" +
                                               $"Guid: {container.Guid}{Environment.NewLine}" +
                                               $"Sender: {container.Sender}{Environment.NewLine}" +
                                               $"Receiver: {container.Receiver}{Environment.NewLine}" +
                                               $"Instance: {container.Instance}{Environment.NewLine}" +
                                               $"Created UTC: {container.CreatedUtc}{Environment.NewLine}" +
                                               $"Groups: {string.Join(",", container.Groups)}{Environment.NewLine}" +
                                               $"Type: {container.Type}{Environment.NewLine}" +
                                               $"Data: {container.Data}");

            CometServer.Instance.AwsIoTConnection.PublishMessage(netmessage);
            return;
        }

        foreach (CometClient cometClient in cometClients)
        {
            netmessage.ReceiverClientId = 0;
            netmessage.ReceiverTerminalId = 0;
            netmessage.ReceiverDeliverypointId = 0;
            netmessage.ReceiverCompanyId = 0;

            if (cometClient.ClientId > 0)
            {
                netmessage.ReceiverClientId = cometClient.ClientId;
            }

            if (cometClient.TerminalId > 0)
            {
                netmessage.ReceiverTerminalId = cometClient.TerminalId;
            }

            if (cometClient.DeliverypointId > 0)
            {
                netmessage.ReceiverDeliverypointId = cometClient.DeliverypointId;
            }

            if (cometClient.SubscribedCompany != null)
            {
                netmessage.ReceiverCompanyId = cometClient.SubscribedCompany.CompanyId;
            }

            NetmessageHandler.HandleNetmessage(this.dummyClient, netmessage);
        }
    }

    private string ConvertFromNewApiGroupName(string groupName)
    {
        string originalGroupName = groupName;

        Regex companyGroup = new Regex(MessagingConstants.GroupCompanyRegex);
        Match companyGroupMatch = companyGroup.Match(groupName);

        Regex terminalGroup = new Regex(MessagingConstants.GroupCompanyTerminalsRegex);
        Match terminalGroupMatch = terminalGroup.Match(groupName);

        Regex deliverypointgroupGroup = new Regex(MessagingConstants.GroupDeliverypointgroupRegex);
        Match deliverypointgroupGroupMatch = deliverypointgroupGroup.Match(groupName);

        if (companyGroupMatch.Success && companyGroupMatch.Groups.Count > 1)
        {
            groupName = string.Format("Company-Clients-{0}", companyGroupMatch.Groups[1]);
            Global.Log("MessageQueueReceiver", "Converted new api groupname from {0} to {1}", originalGroupName, groupName);
        }
        else if (terminalGroupMatch.Success && terminalGroupMatch.Groups.Count > 1)
        {
            groupName = string.Format("Company-Consoles-{0}", terminalGroupMatch.Groups[1]);
            Global.Log("MessageQueueReceiver", "Converted new api groupname from {0} to {1}", originalGroupName, groupName);
        }
        else if (deliverypointgroupGroupMatch.Success && deliverypointgroupGroupMatch.Groups.Count > 1)
        {
            groupName = string.Format("Deliverypointgroup-Clients-{0}", deliverypointgroupGroupMatch.Groups[1]);
            Global.Log("MessageQueueReceiver", "Converted new api groupname from {0} to {1}", originalGroupName, groupName);
        }

        return groupName;
    }

    public void Dispose()
    {
        Global.Log("MessageQueueReceiver", "Dispose");

        this.cancellationTokenSource.Cancel();

        if (this.mqServer != null)
        {
            this.mqServer.Stop();
            this.mqServer.ErrorHandler -= HandleMqServerError;
            this.mqServer.Dispose();
            this.mqServer = null;
        }
    }

    private void PingPongLoop()
    {
        Global.Log("MessageQueueReceiver", "=====> PingPongLoop Enter");

        while (!this.cancellationToken.IsCancellationRequested)
        {
            try
            {
                Thread.Sleep(15000);
            }
            catch (ThreadInterruptedException threadInterruptedException)
            {
                Global.Log("MessageQueueReceiver", "ThreadInterruptedException while putting the thread to sleep. Exception: {0}", threadInterruptedException);
            }

            try
            {
                if (this.cancellationToken.IsCancellationRequested)
                {
                    return;
                }

                long counter = Interlocked.Increment(ref this.pingPongCounter);

                // This loop runs every 15 seconds, but WTF is happening?
                // The pingPongCounter var is used to track how many iterations have happened and to trigger any
                // logic that needs to be executed. The counter is reset (to 0) when a PingPong message is received
                // on the MQ. See the HandlePingPong method at the top.
                //
                // 15s (count 1) - PingPong is published on MQ. If the message is received on the other end the counter reset to 0
                // 30s (count 2) - Nothing happens, just extra wait time
                // 45s (count 3) - Still not received the PingPong, perhaps message got lost? Try sending a new message (benefit of the doubt)
                // 60s (count 4) - Still nothing... shit's gone wrong probably. Reset the counter and restart the MQ
                if (counter == 1 || counter == 3)
                {
                    this.lastPingPong = new PingPong();
                    Global.Log("MessageQueueReceiver", "Sending ping: {0} (counter: {1})", this.lastPingPong.Ticks, counter);

                    // Send ping pong
                    using (IMessageQueueClient client = this.mqServer.CreateMessageQueueClient())
                    {
                        try
                        {
                            client.Publish(this.lastPingPong);
                        }
                        catch (ThreadInterruptedException exception)
                        {
                            Global.Log("MessageQueueReceiver", "ThreadInterruptedException while to publishing Ping Pong message. Exception: {0}", exception);
                        }
                        catch (ThreadAbortException exception)
                        {
                            Global.Log("MessageQueueReceiver", "ThreadAbortException while to publishing Ping Pong message. Exception: {0}", exception);
                        }
                        catch (RedisException exception)
                        {
                            Global.Log("MessageQueueReceiver", "Failed to publish Ping Pong message. Exception: {0}", exception);
                        }
                        catch (Exception exception)
                        {
                            Global.Log("MessageQueueReceiver", "Unexpected exception while publishing Ping Pong message. Exception: {0}", exception);
                        }
                    }
                }
                else if (counter > 3)
                {
                    // Something is going wrong, restart MQServer
                    Global.Log("MessageQueueReceiver", "------- Restarting MQServer ------");
                    Interlocked.Exchange(ref this.pingPongCounter, 0);
                    this.mqServer.Restart();
                }
            }
            catch (ThreadInterruptedException threadInterruptedException)
            {
                Global.Log("MessageQueueReceiver", "ThreadInterruptedException while playing ping pong. Exception: {0}", threadInterruptedException);
            }
        }

        Global.Log("MessageQueueReceiver", "=====> PingPongLoop Exit");
    }
}
