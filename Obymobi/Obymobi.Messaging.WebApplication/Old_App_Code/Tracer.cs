﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using Dionysos;

/// <summary>
/// Summary description for Tracer
/// </summary>
public class Tracer : IDisposable
{
    private readonly Object tracingLock = new Object();
    private readonly List<string> traceList = new List<string>();

    private readonly CancellationTokenSource cancellationTokenSource;
    private readonly BlockingCollection<Trace> traceCollection;

    private readonly string baseTraceLogPath;

    private Task processingTask;

	private Tracer()
	{
        cancellationTokenSource = new CancellationTokenSource();
        traceCollection = new BlockingCollection<Trace>();

        baseTraceLogPath = System.Web.Hosting.HostingEnvironment.MapPath("~/App_Data/Logs/Traces/");

	    StartProcessing();
	}

    private void StartProcessing()
    {
        if (processingTask == null && false)
        {
            processingTask = Task.Factory.StartNew(ProcessTraces,
                                                   cancellationTokenSource.Token,
                                                   TaskCreationOptions.LongRunning,
                                                   TaskScheduler.Default);
        }
    }

    private static Tracer instance;
    public static Tracer Instance
    {
        get
        {
            return instance ?? (instance = new Tracer());
        }
    }

    public List<string> TraceList
    {
        get
        {
            lock(tracingLock)
            {
                return new List<string>(traceList);
            }
        }
    }

    public void StartTracing(string identifier)
    {
        lock(tracingLock)
        {
            StartProcessing();

            if (!traceList.Contains(identifier))
            {
                traceList.Add(identifier);
            }
        }
    }

    public void StopTracing(string identifier)
    {
        lock (tracingLock)
        {
            traceList.Remove(identifier);
        }
    }

    public void AddTrace(CometClient client, string message, params object[] args)
    {
        if (true)
            return;

        if (client == null || message.IsNullOrWhiteSpace())
            return;

        lock (tracingLock)
        {
            if (!traceList.Contains(client.Identifier))
                return;
        }

        if (traceCollection.IsAddingCompleted)
            return;

        traceCollection.Add(new Trace
                            {
                                Timestamp = DateTime.Now,
                                Identifier = client.Identifier,
                                CometIdentifier = client.IdentifierComet,
                                Message = message.FormatSafe(args)
                            });
    }

    public void Dispose()
    {
        traceCollection.CompleteAdding();
        cancellationTokenSource.Cancel();
    }

    private void ProcessTraces()
    {
        foreach (var trace in traceCollection.GetConsumingEnumerable(cancellationTokenSource.Token))
        {
            try
            {
                string identifier = trace.Identifier.Replace(":", ""); // Remove semi-column for invalid file path exception
                string fileName = string.Format("Trace-{0}-{1}.log", identifier, trace.Timestamp.ToString("yyyyMMdd"));
                string filepath = Path.Combine(baseTraceLogPath, fileName);

                using (StreamWriter writer = File.AppendText(filepath))
                {
                    string message = string.Format("[{0}] {1}", trace.CometIdentifier, trace.Message);
                    writer.Write(message);
                    writer.Write(Environment.NewLine);
                }
            }
            catch (Exception ex)
            {
                Global.Log("CometServer", "Tracer Exception: {0}", ex.Message);
            }
        }

        processingTask = null;
    }

    public class Trace
    {
        public DateTime Timestamp { get; set; }
        public string Identifier { get; set; }
        public string CometIdentifier { get; set; }
        public string Message { get; set; }
    }

}