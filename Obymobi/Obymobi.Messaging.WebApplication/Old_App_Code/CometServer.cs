﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Globalization;
using Dionysos;
using Obymobi.Data.EntityClasses;
using Obymobi.Enums;
using Obymobi.Logic.Comet.Netmessages;
using Obymobi.Logic.HelperClasses;
using Obymobi.Logic.Model;
using Obymobi.Messaging.WebApplication.Old_App_Code;
using ServiceStack;

/// <summary>
///     Summary description for CometServer
/// </summary>
public class CometServer
{
    #region  Fields

    private static CometServer cometInstance;
    private static CometGroups cometGroups = new CometGroups();

    private static readonly ConnectionMapping connectedClients = new ConnectionMapping();
    private static readonly ConnectionMapping sandboxClients = new ConnectionMapping();

    private static readonly Pinger pinger = new Pinger();
    private static readonly Ponger ponger = new Ponger();

    // Key: Company Id | Value: CometCompany object
    private readonly ConcurrentDictionary<int, CometCompany> cometCompanies = new ConcurrentDictionary<int, CometCompany>();

    // Key: Deliverypointgroup Id | Value: CometDeliverypointgroup object
    private readonly ConcurrentDictionary<int, CometDeliverypointgroup> cometDeliverypointgroups = new ConcurrentDictionary<int, CometDeliverypointgroup>();

    private static readonly List<string> authenticatedDevices = new List<string>();

    #endregion

    #region Constructors

    /// <summary>
    ///     CometServer Constructor. Use CometServer.Instance to access CometServer class.
    /// </summary>
    private CometServer()
    {
        if (this.ScaleOutMessageQueue == null)
        {
            this.ScaleOutMessageQueue = new ScaleOutMessageQueue();
        }

        if ("True".EqualsIgnoreCase(System.Configuration.ConfigurationManager.AppSettings["EnableIotBridge"]))
        {
            Global.Log("CometServer", "Initializing AWS IoT Bridge");
            if (this.AwsIoTConnection == null)
            {
                this.AwsIoTConnection = new AWSIoTConnection();
            }
        }
        else
        {
            Global.Log("CometServer", "AWS IoT Bridge disabled in AppSettings");
        }
    }

    public void Dispose()
    {
        this.ScaleOutMessageQueue.Dispose();

        if (this.AwsIoTConnection != null) 
        {
            this.AwsIoTConnection.Dispose();
        }
    }

    #endregion

    #region Properties

    public ScaleOutMessageQueue ScaleOutMessageQueue { get; private set; }

    public AWSIoTConnection AwsIoTConnection { get; private set; }

    /// <summary>
    ///     Returns instance object of CometServer
    /// </summary>
    public static CometServer Instance
    {
        get
        {
            return CometServer.cometInstance ?? (CometServer.cometInstance = new CometServer());
        }
    }

    /// <summary>
    ///     Group messaging support
    /// </summary>
    internal static CometGroups Groups
    {
        get { return CometServer.cometGroups; }
    }

    #endregion

    #region Methods

    #endregion

    public int TotalActiveDeviceConnections()
    {
        return CometServer.connectedClients.Count;
    }

    public int TotalActiveConnections()
    {
        return CometServer.connectedClients.TotalConnections;
    }

    public int TotalSandboxConnections()
    {
        return CometServer.sandboxClients.Count;
    }

    public List<string> GetClientCometIdentifiers()
    {
        List<string> identifiers = new List<string>();
        foreach (List<CometClient> clientValue in connectedClients.ShallowCopy().Values)
        {
            foreach(CometClient c in clientValue)
            {
                identifiers.Add(c.IdentifierComet);
            }
        }

        return identifiers;
    }
     
    public Dictionary<string, List<CometClient>> GetConnectedClients()
    {
        return CometServer.connectedClients.ShallowCopy();
    }

    #region Client Pinger

    /// <summary>
    ///     Send ping to all connected clients
    /// </summary>
    internal void PingClients()
    {
        CometServer.pinger.Ping(CometServer.connectedClients.ShallowCopy(), CometServer.sandboxClients.ShallowCopy());
    }

    internal void HandlePongs()
    {
        ponger.WritePongUpdates(CometServer.connectedClients.ShallowCopy());
    }
    
    #endregion

    #region Autenticate Skipper

    internal bool IsPreviouslyAuthenticated(string identifier)
    {
        lock(authenticatedDevices)
        {
            return authenticatedDevices.Contains(identifier);
        }
    }

    #endregion

    /// <summary>
    ///     Add new <see cref="CometClient" /> to CometServer
    /// </summary>
    /// <param name="client">CometClient object to add</param>
    internal void AddClient(CometClient client)
    {
        if (client.SandboxMode)
        {
            CometServer.sandboxClients.Add(client.Identifier, client);

            Logger.Information(client, "Sandbox client added.");
        }
        else
        {
            CometServer.connectedClients.Add(client.Identifier, client);

	        lock(authenticatedDevices)
	        {
	            if (!authenticatedDevices.Contains(client.Identifier))
	            {
	                authenticatedDevices.Add(client.Identifier);
	            }
	        }

            // Remove from sandbox client (if exists)
            CometServer.sandboxClients.Remove(client.Identifier, client);

            //Logger.Information(client, "New client added.");
        }
    }

    /// <summary>
    ///     Remove client from CometServer
    /// </summary>
    /// <param name="client">CometClient to remove from server</param>
    /// <param name="message">Optional message</param>
    internal void RemoveClient(CometClient client, string message = "")
    {
        if (client == null || client.Identifier.IsNullOrWhiteSpace())
        {
            return;
        }

        if (client.SandboxMode)
        {
            CometServer.sandboxClients.Remove(client.Identifier, client);

            //Logger.Information(client, "Sandbox client removed");

            // Write disconnect to NewRelic
            //NewRelic.Api.Agent.NewRelic.IncrementCounter(NewRelicConstants.NEWRELIC_MESSAGING_CONNECTIONS_DISCONNECTS.FormatSafe("Sandbox"));
        }
        else
        {
            CometServer.connectedClients.Remove(client.Identifier, client);

            //Logger.Information(client, "Client removed. Message={0}", message);

            // Write disconnect message to client or terminal log
            if (message.Length > 0)
            {
                if (client.ClientId > 0)
                {
                    ClientLogEntity clientLog = new ClientLogEntity();
                    clientLog.ClientId = client.ClientId;
                    clientLog.Type = (int)ClientLogType.CometMessage;
                    clientLog.Message = message;
                    clientLog.Save();
                }
                else if (client.TerminalId > 0)
                {
                    TerminalLogEntity terminalLog = new TerminalLogEntity();
                    terminalLog.TerminalId = client.TerminalId;
                    terminalLog.Type = (int)ClientLogType.CometMessage;
                    terminalLog.Message = message;
                    terminalLog.Save();
                }
            }            
        }

        // Unpin from all groups
        CometServer.Groups.UnpinClient(client);
    }

    /// <summary>
    ///     Try and get a list of active CometClient objects for supplied identifier
    /// </summary>
    /// <param name="identifier">Client identifier (MAC)</param>
    /// <param name="cometClient">
    ///     When this method returns, contains the value associated with the specified
    ///     key, if the key is found; otherwise, the default value for the type of the
    ///     value parameter. This parameter is passed uninitialized.
    /// </param>
    /// <returns>Will return true if key is found otherwise false.</returns>
    internal bool TryGetCometClients(string identifier, out List<CometClient> cometClient)
    {
        cometClient = CometServer.connectedClients.GetConnections(identifier);

        return cometClient.Count > 0;
    }

    /// <summary>
    ///     Tries to get a <see cref="CometCompany" /> by company id. If there is no company with supplied company id it will
    ///     be created.
    /// </summary>
    /// <param name="companyId">The company id</param>
    /// <param name="company">
    ///     When this method returns, contains the value associated with the specified
    ///     key, if the key is found; otherwise, the default value for the type of the
    ///     value parameter. This parameter is passed uninitialized.
    /// </param>
    /// <returns>True if a company has been found or created.</returns>
    internal bool TryGetCometCompanyById(int companyId, out CometCompany company)
    {
        if (!cometCompanies.TryGetValue(companyId, out company))
        {
            try
            {
                company = new CometCompany(companyId);
                this.cometCompanies.TryAdd(companyId, company);
            }
            catch (Exception ex)
            {
                Logger.Error(null, "Exception when creating CometCompany.\n{ 'companyId': '{0}', 'Exception': '{1}' }", companyId, ex.Message);
                company = null;

                return false;
            }
        }

        return true;
    }

    /// <summary>
    ///     Tries to get a <see cref="CometDeliverypointgroup" /> by deliverypointgroup id. If there is no deliverypointgroup
    ///     with supplied id it will be created.
    /// </summary>
    /// <param name="deliverypointgroupId">The deliverypointgroup id</param>
    /// <param name="deliverypointgroup">
    ///     When this method returns, contains the value associated with the specified
    ///     key, if the key is found; otherwise, the default value for the type of the
    ///     value parameter. This parameter is passed uninitialized.
    /// </param>
    /// <returns>True if a deliverypointgroup has been found or created.</returns>
    internal bool TryGetCometDeliverypointgroupById(int deliverypointgroupId, out CometDeliverypointgroup deliverypointgroup)
    {
        if (!cometDeliverypointgroups.TryGetValue(deliverypointgroupId, out deliverypointgroup))
        {
            try
            {
                deliverypointgroup = new CometDeliverypointgroup(deliverypointgroupId);
                this.cometDeliverypointgroups.TryAdd(deliverypointgroupId, deliverypointgroup);
            }
            catch (Exception ex)
            {
                Logger.Error(null, "Exception when creating CometDeliverypointgroup.\n{ 'deliverypointgroupId': '{0}', 'Exception': '{1}' }", deliverypointgroupId, ex.Message);
                deliverypointgroup = null;

                return false;
            }
        }

        return true;
    }

    /// <summary>
    ///     Try and get a list of active Sandbox mode CometClient objects for supplied identifier
    /// </summary>
    /// <param name="identifier">Client identifier (MAC)</param>
    /// <param name="cometClient">
    ///     When this method returns, contains the value associated with the specified
    ///     key, if the key is found; otherwise, the default value for the type of the
    ///     value parameter. This parameter is passed uninitialized.
    /// </param>
    /// <returns>Will return true if key is found otherwise false.</returns>
    internal bool TryGetCometSandboxClients(string identifier, out List<CometClient> cometClient)
    {
        cometClient = CometServer.sandboxClients.GetConnections(identifier);

        return cometClient.Count > 0;
    }

    /// <summary>
    ///     Gets a list of connected <see cref="CometClient" /> objects with supplied client id.
    /// </summary>
    /// <param name="identifier">Identifier (mac) to find clients for</param>
    /// <returns>List of CometClient objects. Empty list if none are found.</returns>
    internal List<CometClient> GetCometClientsByIdentifier(string identifier)
    {
        return GetCometClientsByProperty(c => c.Identifier.Equals(identifier, StringComparison.InvariantCultureIgnoreCase));
    }

    /// <summary>
    ///     Gets a list of connected <see cref="CometClient" /> objects with supplied client id.
    /// </summary>
    /// <param name="clientId">Client id to find clients for</param>
    /// <returns>List of CometClient objects. Empty list if none are found.</returns>
    internal List<CometClient> GetCometClientsByClientId(int clientId)
    {
        return GetCometClientsByProperty(c => c.ClientId == clientId);
    }

    /// <summary>
    ///     Gets a list of connected <see cref="CometClient" /> objects with supplied deliverypoint id
    /// </summary>
    /// <param name="deliverypointId">Deliverypoint id to find clients for</param>
    /// <returns>List of CometClient objects. Empty list if none are found.</returns>
    internal List<CometClient> GetCometClientsByDeliverypointId(int deliverypointId)
    {
        return GetCometClientsByProperty(c => c.DeliverypointId == deliverypointId);
    }

    /// <summary>
    ///     Gets a list of connected <see cref="CometClient" /> objects using <see cref="System.Predicate{CometClient}" />.
    /// </summary>
    /// <param name="predicate">Predicate to use for filtering.</param>
    /// <returns>List of CometClient objects. Empty list if none are found.</returns>
    internal List<CometClient> GetCometClientsByProperty(Predicate<CometClient> predicate)
    {
        return CometServer.connectedClients.GetConnections(predicate);
    }

    /// <summary>
    ///     Send a <see cref="Obymobi.Logic.Model.Netmessage" /> to supplied clients. By default all messages are saved in
    ///     database for verification.
    /// </summary>
    /// <param name="clients">List of receiving CometCliets</param>
    /// <param name="message"><see cref="Obymobi.Logic.Model.Netmessage" /> to send.</param>
    internal void SendNetmessage(List<CometClient> clients, Netmessage message)
    {
        message.AddTraceMessage("Sending message from CometServer to client(s)");

        // Message is not saved, just send the message to clients
        foreach (CometClient cometClient in clients)
        {
            SendNetmessageToClient(cometClient, message);
        }

        if (this.AwsIoTConnection != null)
        {
            this.AwsIoTConnection.PublishMessage(message);
        }        
    }

    /// <summary>
    ///     Send a <see cref="Obymobi.Logic.Model.Netmessage" /> to supplied client. By default all messages are saved in
    ///     database for verification.
    /// </summary>
    /// <param name="client">Receiver CometClient</param>
    /// <param name="message"><see cref="Obymobi.Logic.Model.Netmessage" /> to send.</param>
    internal void SendNetmessageToClient(CometClient client, Netmessage message)
    {
        if (client != null)
        {
            // Send message to all connections from this clients MAC address.
            List<CometClient> clientList;
            List<string> cometIdentifers = new List<string>();
            if (client.ClientType == NetmessageClientType.Cms || client.ClientType == NetmessageClientType.MobileNoc || client.ClientType == NetmessageClientType.Webservice)
            {
                cometIdentifers.Add(client.IdentifierComet);
            }
            else if (client.SandboxMode && TryGetCometSandboxClients(client.Identifier, out clientList))
            {
                foreach (CometClient cometClient in clientList)
                {
                    cometIdentifers.Add(cometClient.IdentifierComet);
                }
            }
            else if (TryGetCometClients(client.Identifier, out clientList))
            {
                foreach (CometClient cometClient in clientList)
                {
                    cometIdentifers.Add(cometClient.IdentifierComet);
                }
            }

            this.ScaleOutMessageQueue.PublishMessage(message);

            if (cometIdentifers.Count > 0)
            {
                //Logger.Verbose(client, "Sending message '{0}' to client(s) with identifier '{1}'", message.MessageType, client.Identifier);

                foreach (string identifier in cometIdentifers)
                {
                    bool sendSuccess;
                    try
                    {
                        //Logger.Verbose(client, " - Transmitting message '{0}' to client with Comet identifier '{1}'", message.MessageType, identifier);
                        sendSuccess = client.TransmitMessage(new[] {identifier}, message);
                    }
                    catch (Exception ex)
                    {
                        Logger.Error(client, " - Failed to send message to '{0}'. Error: {1}", identifier, ex.Message);
                        sendSuccess = false;
                    }

                    if (!sendSuccess)
                    {
                        client.Dispose("Failed to send message to client");
                    }
                }
            }
        }
    }

    /// <summary>
    ///     Subscribe a client as a terminal (can be a Console or OSS)
    /// </summary>
    /// <param name="client">Client of the terminal</param>
    /// <param name="terminalId">Terminal Id</param>
    internal void SubscribeAsTerminal(CometClient client, int terminalId)
    {
        TerminalEntity terminalEntity = new TerminalEntity(terminalId);
        if (!terminalEntity.IsNew)
        {
            client.TerminalId = terminalId;

            // Subscribe to terminal company
            CometCompany company = client.SubscribeToCompany(terminalEntity.CompanyId);
            if (company != null)
            {
                // Pin client as console to company
                company.PinConsole(client);

                // Pin client to terminal
                company.PinTerminal(terminalId, client);
            }
        }
    }

    internal void TerminalActAsMaster(CometClient client, bool isMaster)
    {
        if ((client.ClientType == NetmessageClientType.Console || client.ClientType == NetmessageClientType.OnsiteServer) && client.SubscribedCompany != null)
        {
            bool wasMaster = client.IsMasterTerminal;
            client.IsMasterTerminal = isMaster;

            if (isMaster)
            {
                client.SubscribedCompany.PinMasterTerminal(client);
            }
            else if (wasMaster)
            {
                client.SubscribedCompany.UnpinMasterTerminal(client);
            }

            try
            {
                // Update Terminal in database
                TerminalEntity terminalEntity = new TerminalEntity(client.TerminalId);
                if (!terminalEntity.IsNew)
                {
                    terminalEntity.MasterTab = isMaster;
                    terminalEntity.ValidatorOverwriteSendMasterTabChangedNetmessage = true;
                    terminalEntity.Save();
                }
            }
            catch (Exception ex)
            {
                Global.Log("CometServer", "Failed to save terminal entity (id: {0}). Exception: {1}", client.TerminalId, ex.Message);
            }
        }
    }

    /// <summary>
    ///     Subscribed a client a to an Agent so it'll receive output from any commands send to the agent.
    /// </summary>
    /// <param name="client"></param>
    /// <param name="macAddress"></param>
    internal void SubscribeToAgent(CometClient client, string macAddress)
    {
        // Add this client to agent connected list
        CometServer.Groups.PinClient(client, "Agent-" + macAddress);

        // Return message
        NetmessageAgentCommandResponse message = new NetmessageAgentCommandResponse();

        /*// Check if there is an Agent online with this MAC
        if (CometServer.Groups.GetCometClientsForGroup(CometGroups.GROUP_AGENTS).Find(agent => agent.Identifier == macAddress) != null)
        {
            message.ReponseMessage = "NOTICE: Device shell is connected to the Crave SupportTools!";
        }
        else
        {
            message.ReponseMessage = "WARNING: This device is not connected with the Crave SupportTools!";
        }

        SendNetmessageToClient(client, message);*/
    }

    #region Hacky-de-Peppie

    public Dictionary<NetmessageClientType, int[]> ConnectionTotals()
    {
        Dictionary<NetmessageClientType, int[]> total = new Dictionary<NetmessageClientType, int[]> {{NetmessageClientType.OnsiteServer, new[] {0, 0, 0}}, {NetmessageClientType.Emenu, new[] {0, 0, 0}}, {NetmessageClientType.Console, new[] {0, 0, 0}}, {NetmessageClientType.SupportTools, new[] {0, 0, 0}}};

        Dictionary<string, List<CometClient>> clients = CometServer.connectedClients.ShallowCopy();
        foreach (KeyValuePair<string, List<CometClient>> clientList in clients)
        {
            bool emenu = false;
            bool supporttools = false;
            bool console = false;
            bool onsiteserver = false;
            foreach (CometClient cometClient in clientList.Value)
            {
                if (cometClient.ClientType == NetmessageClientType.Emenu)
                {
                    if (!emenu)
                    {
                        if (cometClient.CommunicationMethod == ClientCommunicationMethod.SignalR)
                        {
                            total[NetmessageClientType.Emenu][2]++;
                        }
                        emenu = true;
                    }

                    total[NetmessageClientType.Emenu][0]++;
                }
                else if (cometClient.ClientType == NetmessageClientType.SupportTools)
                {
                    if (!supporttools)
                    {
                        if (cometClient.CommunicationMethod == ClientCommunicationMethod.SignalR)
                        {
                            total[NetmessageClientType.SupportTools][2]++;
                        }
                        supporttools = true;
                    }

                    total[NetmessageClientType.SupportTools][0]++;
                }
                else if (cometClient.ClientType == NetmessageClientType.Console)
                {
                    if (!console)
                    {
                        if (cometClient.CommunicationMethod == ClientCommunicationMethod.SignalR)
                        {
                            total[NetmessageClientType.Console][2]++;
                        }
                        console = true;
                    }

                    total[NetmessageClientType.Console][0]++;
                }
                else if (cometClient.ClientType == NetmessageClientType.OnsiteServer)
                {
                    if (!onsiteserver)
                    {
                        if (cometClient.CommunicationMethod == ClientCommunicationMethod.SignalR)
                        {
                            total[NetmessageClientType.OnsiteServer][2]++;
                        }
                        onsiteserver = true;
                    }

                    total[NetmessageClientType.OnsiteServer][0]++;
                }
            }
        }

        return total;
    }

    public Dictionary<string, int> TotalConnectionsPerCompany()
    {
        Dictionary<string, int> dictionary = new Dictionary<string, int>();
        foreach (CometCompany cometCompany in cometCompanies.Values)
        {
            int connections = CometServer.Groups.GetCometClientsForGroup(cometCompany.GroupNameClients).Count;
            connections += CometServer.Groups.GetCometClientsForGroup(cometCompany.GroupNameConsoles).Count;
            connections += CometServer.Groups.GetCometClientsForGroup(cometCompany.GroupNameExternal).Count;

            dictionary.Add(cometCompany.ObymobiCompany.Name, connections);
        }

        return dictionary;
    }

    internal string GetSandboxClientIdentifiers()
    {
        string clientList = "";
        foreach (KeyValuePair<string, List<CometClient>> sandboxClient in CometServer.sandboxClients.ShallowCopy())
        {
            string[] clientInfo = new string[4];
            clientInfo[0] = sandboxClient.Key; // Identifier
            clientInfo[1] = "0"; // company id
            clientInfo[2] = ""; // localip
            clientInfo[3] = ""; // externalip

            foreach (CometClient client in sandboxClient.Value)
            {
                if (client.SandboxLastCompanyId > 0)
                {
                    clientInfo[1] = client.SandboxLastCompanyId.ToString(CultureInfo.InvariantCulture);
                }

                if (client.IpAddressLocal.Length > 0)
                {
                    clientInfo[2] = client.IpAddressLocal;
                }

                if (client.IpAddressExtern.Length > 0)
                {
                    clientInfo[3] = client.IpAddressExtern;
                }
            }

            clientList += string.Format("{0},{1},{2},{3}|", clientInfo[0], clientInfo[1], clientInfo[2], clientInfo[3]);
        }

        if (clientList.Length > 0)
        {
            clientList = clientList.Remove(clientList.Length - 1);
        }

        return clientList;
    }

    public void RecreateScaleOutQueue()
    {
        this.ScaleOutMessageQueue.Dispose();
        this.ScaleOutMessageQueue = new ScaleOutMessageQueue();
    }

    #endregion
}