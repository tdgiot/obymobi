﻿using System;
using System.Collections.Generic;
using Dionysos;
using Obymobi;
using Obymobi.Configuration;
using Obymobi.Constants;
using Obymobi.Data;
using Obymobi.Data.EntityClasses;
using Obymobi.Enums;
using Obymobi.Logic.Comet;
using Obymobi.Logic.Comet.Netmessages;
using Obymobi.Logic.HelperClasses;
using Obymobi.Logic.Model;
using Obymobi.Security;
using SD.LLBLGen.Pro.ORMSupportClasses;

/// <summary>
///     Summary description for CometClient
/// </summary>
public abstract class CometClient : IDisposable
{
    private enum AuthenticateResult
    {
        AuthenticateHashFailed = 200,
        AlreadyAuthenticated = 201
    }

    #region Constructors

    internal CometClient(string identifierComet, ClientCommunicationMethod communicationMethod)
    {
        this.Identifier = "";
        this.IdentifierComet = identifierComet;
        this.CommunicationMethod = communicationMethod;
        this.ClientType = NetmessageClientType.Unknown;

        this.IncrementalUpdateCount = 1;
        this.IncrementalUpdateCountSupportTools = 0;
        this.FullUpdateReady = false;
    }

    #endregion

    #region Properties

    public bool IsDisposed { get; private set; }

    /// <summary>
    ///     Client MAC address
    /// </summary>
    public string Identifier { get; internal set; }

    /// <summary>
    ///     Auto-generated identifier by the comet server
    /// </summary>
    public string IdentifierComet { get; internal set; }

    /// <summary>
    ///     The server type which this client uses to communicate
    /// </summary>
    public ClientCommunicationMethod CommunicationMethod { get; set; }

    /// <summary>
    ///     Client type
    /// </summary>
    public NetmessageClientType ClientType { get; internal set; }

    /// <summary>
    ///     True if the client has authenticated itself with valid credentials
    /// </summary>
    public bool IsAuthenticated { get; internal set; }

    /// <summary>
    ///     Last time the server received ping-pong from the client
    /// </summary>
    public DateTime LastPong { get; protected set; }

    /// <summary>
    ///     Last time the server has sent a ping to the client
    /// </summary>
    public DateTime LastPingSent { get; set; }

    public int IncrementalUpdateCount { get; set; }
    public int IncrementalUpdateCountSupportTools { get; set; }

    /// <summary>
    ///     Database client id this device is assigned to
    /// </summary>
    public int ClientId { get; set; }

    public int SupportToolsClientId
    {
        get
        {
            if (this.ClientId > 0)
                return this.ClientId;

            return  this.ClientEntity != null ? this.ClientEntity.ClientId : 0;
        }
    }

    public int SupportToolsTerminalId
    {
        get { return this.TerminalEntity != null ? this.TerminalEntity.TerminalId : 0; }
    }

    /// <summary>
    ///     Deliverypoint id this device is assigned to
    /// </summary>
    public int DeliverypointId { get; set; }

    /// <summary>
    ///     Terminal id this device is assigned to
    /// </summary>
    public int TerminalId { get; set; }

    /// <summary>
    ///     The <see cref="CometCompany" /> object this client is subscribe to. Value can be NULL if not subscribed to any
    ///     company.
    /// </summary>
    public CometCompany SubscribedCompany { get; set; }

    /// <summary>
    ///     The <see cref="CometDeliverypointgroup" /> object this client is subscribe to. Value can be NULL if not subscribed
    ///     to any deliverypointgroup.
    /// </summary>
    public CometDeliverypointgroup SubscribedDeliverypointgroup { get; set; }

    /// <summary>
    ///     Local IP address of this client
    /// </summary>
    public string IpAddressLocal { get; set; }

    /// <summary>
    ///     External IP address of this client
    /// </summary>
    public string IpAddressExtern { get; set; }

    /// <summary>
    ///     Flag client as master terminal
    /// </summary>
    public bool IsMasterTerminal { get; set; }

    /// <summary>
    ///     Only used on console
    /// </summary>
    public bool IsFirstTimeInitialize { get; set; }

    public ClientEntity ClientEntity { get; set; }
    public TerminalEntity TerminalEntity { get; set; }

    public bool FullUpdateReady { get; set; }    

    #endregion

    #region Sandbox

    /// <summary>
    ///     True if the client is in sandbox mode
    /// </summary>
    public bool SandboxMode { get; internal set; }

    /// <summary>
    ///     Last known company id before entering sandbox mode
    /// </summary>
    public int SandboxLastCompanyId { get; internal set; }

    #endregion

    /// <summary>
    ///     Authenticate new client. If the client is not authenticated it's impossible to send or receive net messages.
    /// </summary>
    /// <param name="timestamp">Device timestamp</param>
    /// <param name="identifier">Device identifier (MAC)</param>
    /// <param name="hash">Paramaters hash value</param>
    internal void Authenticate(long timestamp, string identifier, string hash)
    {
        // Return message
        NetmessageAuthenticateResult returnMessage = new NetmessageAuthenticateResult();

        if (this.Identifier != identifier)
        {
            this.IsAuthenticated = false;
        }

        //Tracer.Instance.AddTrace(this, "Authenticating");

        if (!this.IsAuthenticated)
        {
            // Set fields
            this.Identifier = identifier;

            // Check authentication
            try
            {
                this.IsAuthenticated = CometServer.Instance.IsPreviouslyAuthenticated(identifier) || this.ValidateAuthentication(this, timestamp, hash);
            }
            catch (ObymobiException ex)
            {
                returnMessage.AuthenticateErrorMessage = ex.ErrorEnumValueText + " - " + ex.Message;

                Logger.Error(this, "Failed to authenticate: {0}", returnMessage.AuthenticateErrorMessage);

                //NewRelic.Api.Agent.NewRelic.NoticeError(ex);
            }

            returnMessage.IsAuthenticated = this.IsAuthenticated;

            if (this.IsAuthenticated)
            {
                // Update last active Date/Time
                // Add a random amount of seconds, in case of a mass connect
                this.LastPong = DateTime.UtcNow.Add(new TimeSpan(0, 0, 0, RandomUtil.Next(1, 60)));

                // Add client to CometServer
                CometServer.Instance.AddClient(this);
            }
        }
        else
        {
            returnMessage.IsAuthenticated = true;
        }

        if (!this.IsAuthenticated)
        {
            Logger.Verbose(this, "Authenticating client. Authenticated={0}, ErrorMessage: {1}", returnMessage.IsAuthenticated, returnMessage.AuthenticateErrorMessage);
        }

        // Send login result message
        this.TransmitMessage(returnMessage);

        if (!returnMessage.IsAuthenticated)
        {
            this.Dispose();
        }
    }

    internal void SandboxLogin(string identifier, int lastCompanyId, string localIp, string externalIp)
    {
        Tracer.Instance.AddTrace(this, "Sandbox Login");

        if (this.IsAuthenticated)
        {
            CometServer.Instance.RemoveClient(this);
            this.IsAuthenticated = false;
        }

        this.Identifier = identifier;
        this.SandboxMode = true;
        this.SandboxLastCompanyId = lastCompanyId;
        this.IpAddressLocal = localIp;
        this.IpAddressExtern = externalIp;        

        // Update last active Date/Time
        this.LastPong = DateTime.UtcNow;

        CometServer.Instance.AddClient(this);
    }

    /// <summary>
    ///     Send message to this client
    /// </summary>
    /// <param name="message"><see cref="Obymobi.Logic.Model.Netmessage" /> object to send.</param>
    internal void TransmitMessage(Netmessage message)
    {
        try
        {
            this.TransmitMessage(new[] {this.IdentifierComet}, message);
        }
        catch (Exception ex)
        {
            // Catch-all, happens when client gets disconnected   
            Logger.Error(this, "Failed to transmit message, client is probably disposed. {0}", ex.ToString());

            this.Dispose("Failed to transmit message, client probably disconnected");
        }
    }

    /// <summary>
    ///     Receive method for Netmessages send from the client.
    ///     Netmessages will only be handled if the client is authenticated.
    /// </summary>
    /// <param name="message">Received Netmessage</param>
    internal void ReceiveMessage(Netmessage message)
    {
        if (this.IsAuthenticated)
        {
            if (message.IsVerifyNeeded() && !string.IsNullOrEmpty(message.Guid))
            {
                if (!this.VerifyMessage(message.Guid))
                {
                    this.Dispose("Failed to send verify message to client");
                    return;
                }
            }

            // Update last received message. ONLY FOR WEBSERVICE!!
            if (this.ClientType == NetmessageClientType.Webservice)
            {
                try
                {
                    ConfigurationManager.SetValue(CraveCometConfigConstants.LAST_COMET_MESSAGE_FROM_WEBSERVICE, DateTime.UtcNow);
                }
                catch (Exception)
                {
                    Logger.Error(this, "Failed to write last comet message from webservice to configuration");
                }
            }

            NetmessageHandler.HandleNetmessage(this, message);
        }
        else if (this.SandboxMode)
        {
            if (message.MessageType == NetmessageType.Pong || message.MessageType == NetmessageType.AgentCommandResponse)
            {
                NetmessageHandler.HandleNetmessage(this, message);
            }
            else
            {
                Logger.Verbose(this, "Received non-valid message '{0}' for sanbox mode", message.MessageType);
            }
        }
        else
        {
            Logger.Verbose(this, "Received message but client is not authenticated!");
        }
    }

    internal void UpdateLastPong()
    {
        if (this.IsAuthenticated || this.SandboxMode)
        {
            DateTime now = DateTime.UtcNow;
            if (this.LastPong < now)
            {
                this.LastPong = now;
            }
        }
    }

    /// <summary>
    ///     Ping/Pong. Update last received pong date and time.
    ///     LastPong value will only be updated if it's before DateTime.UtcNow, so if it's manually set in the future this will
    ///     not change.
    /// </summary>
    internal void Pong(NetmessagePong messageTyped, bool updateLastPong = true)
    {
        Tracer.Instance.AddTrace(this, "Pong");        

        if (updateLastPong)
        {
            this.UpdateLastPong();
        }

        if (this.IsAuthenticated)
        {
            try
            {
                if (this.ClientType == NetmessageClientType.Emenu)
                {
                    if (this.ClientEntity == null)
                    {
                        // Record this value to to the clients
                        this.ClientEntity = new ClientEntity(this.ClientId);
                        if (this.ClientEntity.IsNew)
                        {
                            this.ClientEntity = null;
                        }
                    }

                    if (this.ClientEntity != null)
                    {
                        this.ClientEntity.LastStatus = (int)ClientStatusCode.OK;
                        this.ClientEntity.LastStatusText = ClientStatusCode.OK.ToString();
                        this.ClientEntity.DeviceEntity.CommunicationMethod = (int)this.CommunicationMethod;

                        this.ClientEntity.DeviceEntity.CloudEnvironment = string.Format("{0} ({1})", WebEnvironmentHelper.CloudEnvironment, TestUtil.MachineName);

                        if (messageTyped != null)
                        {
                            this.ClientEntity.DeviceEntity.BatteryLevel = messageTyped.BatteryLevel;
                            this.ClientEntity.DeviceEntity.PrivateIpAddresses = messageTyped.PrivateIpAddresses;

                            if (!messageTyped.PublicIpAddress.IsNullOrWhiteSpace() && messageTyped.PublicIpAddress.Length > 255)
                            {
                                this.ClientEntity.DeviceEntity.PublicIpAddress = messageTyped.PublicIpAddress.Substring(0, 255);
                            }
                            else
                            {
                                this.ClientEntity.DeviceEntity.PublicIpAddress = messageTyped.PublicIpAddress;
                            }

                            this.ClientEntity.DeviceEntity.IsCharging = messageTyped.IsCharging;

                            this.ClientEntity.LastOperationMode = messageTyped.ClientOperationMode;
                            this.ClientEntity.BluetoothKeyboardConnected = messageTyped.BluetoothKeyboardConnected;
                            if (this.ClientEntity.RoomControlConnected != messageTyped.RoomControlConnected)
                            {
                                this.ClientEntity.RoomControlConnected = messageTyped.RoomControlConnected;
                            }
                            this.ClientEntity.DoNotDisturbActive = messageTyped.DoNotDisturbActive;
                            this.ClientEntity.ServiceRoomActive = messageTyped.ServiceRoomActive;

                            if (messageTyped.DeliverypointId > 0)
                            {
                                this.ClientEntity.LastDeliverypointId = messageTyped.DeliverypointId;
                            }
                            else
                            {
                                this.ClientEntity.LastDeliverypointId = null;
                            }

                            this.ClientEntity.LastWifiSsid = messageTyped.LastWifiSsid;

                            this.ClientEntity.DeviceEntity.WifiStrength = messageTyped.LastWifiStrength;
                            this.ClientEntity.DeviceEntity.AgentRunning = messageTyped.AgentIsRunning;
                            this.ClientEntity.DeviceEntity.SupportToolsRunning = messageTyped.SupportToolsIsRunning;
                        }

                        this.FullUpdateReady = true;

                        // Client state
                        if (!this.ClientEntity.LastStateOnline.HasValue || !this.ClientEntity.LastStateOnline.Value) // LastStateOnline is null or false
                        {
                            // Update the client state to online
                            ClientStateHelper.UpdateClientState(this.ClientEntity, true);
                            this.ClientEntity.LastStateOnline = true;
                        }
                    }
                }
                else if (this.ClientType == NetmessageClientType.Console)
                {
                    if (this.TerminalEntity == null)
                    {
                        // Record this value to to the clients
                        this.TerminalEntity = new TerminalEntity(this.TerminalId);
                        if (this.TerminalEntity.IsNew)
                        {
                            this.TerminalEntity = null;
                        }
                    }

                    if (this.TerminalEntity != null)
                    {
                        this.TerminalEntity.DeviceEntity.CommunicationMethod = (int)this.CommunicationMethod;
                        this.TerminalEntity.DeviceEntity.CloudEnvironment = string.Format("{0} ({1})", WebEnvironmentHelper.CloudEnvironment, TestUtil.MachineName);

                        if (messageTyped != null)
                        {
                            this.TerminalEntity.DeviceEntity.BatteryLevel = messageTyped.BatteryLevel;
                            this.TerminalEntity.DeviceEntity.PrivateIpAddresses = messageTyped.PrivateIpAddresses;

                            if (!messageTyped.PublicIpAddress.IsNullOrWhiteSpace() && messageTyped.PublicIpAddress.Length > 255)
                            {
                                this.TerminalEntity.DeviceEntity.PublicIpAddress = messageTyped.PublicIpAddress.Substring(0, 255);
                            }
                            else
                            {
                                this.TerminalEntity.DeviceEntity.PublicIpAddress = messageTyped.PublicIpAddress;
                            }

                            this.TerminalEntity.DeviceEntity.IsCharging = messageTyped.IsCharging;
                            this.TerminalEntity.DeviceEntity.WifiStrength = messageTyped.LastWifiStrength;
                            this.TerminalEntity.DeviceEntity.AgentRunning = messageTyped.AgentIsRunning;
                            this.TerminalEntity.DeviceEntity.SupportToolsRunning = messageTyped.SupportToolsIsRunning;
                            this.TerminalEntity.PrinterConnected = messageTyped.BluetoothPrinterConnected;
                        }

                        this.FullUpdateReady = true;
                    }
                }
                else if (this.ClientType == NetmessageClientType.SupportTools)
                {
                    if (this.ClientEntity != null)
                    {
                        this.ClientEntity.DeviceEntity.LastSupportToolsRequestUTC = DateTime.UtcNow;
                        this.ClientEntity.DeviceEntity.SupportToolsRunning = true;
                    }
                    else if (this.TerminalEntity != null)
                    {
                        this.TerminalEntity.DeviceEntity.LastSupportToolsRequestUTC = DateTime.UtcNow;
                        this.TerminalEntity.DeviceEntity.SupportToolsRunning = true;
                    }
                    else
                    {
                        PrefetchPath prefetch = new PrefetchPath(EntityType.DeviceEntity);
                        prefetch.Add(DeviceEntityBase.PrefetchPathClientCollection);
                        prefetch.Add(DeviceEntityBase.PrefetchPathTerminalCollection);

                        DeviceEntity deviceEntity = DeviceHelper.GetDeviceEntityByIdentifier(this.Identifier, false, prefetch, DeviceType.SamsungP5110);
                        if (deviceEntity != null && !deviceEntity.IsNew)
                        {
                            if (deviceEntity.ClientEntitySingle != null)
                            {
                                this.ClientEntity = deviceEntity.ClientEntitySingle;

                                this.ClientEntity.DeviceEntity.LastSupportToolsRequestUTC = DateTime.UtcNow;
                                this.ClientEntity.DeviceEntity.SupportToolsRunning = true;
                            }
                            else if (deviceEntity.TerminalEntitySingle != null)
                            {
                                this.TerminalEntity = deviceEntity.TerminalEntitySingle;

                                this.TerminalEntity.DeviceEntity.LastSupportToolsRequestUTC = DateTime.UtcNow;
                                this.TerminalEntity.DeviceEntity.SupportToolsRunning = true;
                            }
                        }
                    }                    
                }
                else if (this.ClientType == NetmessageClientType.OnsiteServer)
                {
                    if (this.TerminalEntity == null)
                    {
                        // Record this value to to the clients
                        this.TerminalEntity = new TerminalEntity(this.TerminalId);
                        if (this.TerminalEntity.IsNew)
                        {
                            this.TerminalEntity = null;
                        }
                    }

                    if (this.TerminalEntity != null)
                    {
                        this.TerminalEntity.DeviceEntity.CommunicationMethod = (int)this.CommunicationMethod;
                        this.TerminalEntity.DeviceEntity.LastRequestUTC = DateTime.UtcNow;
                        this.TerminalEntity.DeviceEntity.CloudEnvironment = string.Format("{0} ({1})", WebEnvironmentHelper.CloudEnvironment, TestUtil.MachineName);

                        if (messageTyped != null)
                        {
                            this.TerminalEntity.DeviceEntity.BatteryLevel = messageTyped.BatteryLevel;
                            this.TerminalEntity.DeviceEntity.PrivateIpAddresses = messageTyped.PrivateIpAddresses;

                            if (!messageTyped.PublicIpAddress.IsNullOrWhiteSpace() && messageTyped.PublicIpAddress.Length > 255)
                            {
                                this.TerminalEntity.DeviceEntity.PublicIpAddress = messageTyped.PublicIpAddress.Substring(0, 255);
                            }
                            else
                            {
                                this.TerminalEntity.DeviceEntity.PublicIpAddress = messageTyped.PublicIpAddress;
                            }

                            this.TerminalEntity.DeviceEntity.WifiStrength = messageTyped.LastWifiStrength;
                            this.TerminalEntity.DeviceEntity.AgentRunning = messageTyped.AgentIsRunning;
                        }

                        this.TerminalEntity.DeviceEntity.Save();
                        this.TerminalEntity.Save();
                    }
                }
            }
            catch (Exception ex)
            {
                Global.Log("CometClient", "CometClient.Pong - Exception: {0} - Stack trace: {1}", ex.Message, ex.StackTrace);
            }
        }
    }

    /// <summary>
    ///     Subscribe client to company.
    /// </summary>
    /// <param name="companyId">Company id to subscribe to</param>
    internal CometCompany SubscribeToCompany(int companyId)
    {
        Tracer.Instance.AddTrace(this, "Subscribe to company: {0}", companyId);

        CometCompany company;
        if (this.SubscribedCompany == null || this.SubscribedCompany.CompanyId != companyId)
        {
            // Client is already subscribed to a company but wants to change. Unsubscribe the old one first.
            if (this.SubscribedCompany != null)
            {
                this.SubscribedCompany.UnpinClient(this);
            }

            // Get CometCompany object
            if (CometServer.Instance.TryGetCometCompanyById(companyId, out company))
            {
                company.PinClient(this);
            }
        }
        else
        {
            company = this.SubscribedCompany;
        }

        return company;
    }

    /// <summary>
    ///     Subscribe client to deliverypointgroup.
    /// </summary>
    /// <param name="deliverypointgroupId">Deliverypointgroup id to subscribe to</param>
    internal void SubscribeToDeliverypointgroup(int deliverypointgroupId)
    {
        Tracer.Instance.AddTrace(this, "Subscribe to deliverypointgroup: {0}", deliverypointgroupId);

        if (this.SubscribedDeliverypointgroup == null || this.SubscribedDeliverypointgroup.DeliverypointgroupId != deliverypointgroupId)
        {
            // Client is already subscribed to a deliverypiontgroup but wants to change. Unsubscribe from the old one.
            if (this.SubscribedDeliverypointgroup != null)
            {
                this.SubscribedDeliverypointgroup.UnpinClient(this);
            }

            // Get the CometDeliverypointgroup object
            CometDeliverypointgroup deliverypointgroup;
            if (CometServer.Instance.TryGetCometDeliverypointgroupById(deliverypointgroupId, out deliverypointgroup))
            {
                deliverypointgroup.PinClient(this);
            }
        }
    }

    /// <summary>
    ///     Send message to one or multiple clients
    /// </summary>
    /// <param name="receiversIdentifierComet">Array of receiver comet identifiers</param>
    /// <param name="message">Netmesage object to send</param>
    public abstract bool TransmitMessage(string[] receiversIdentifierComet, Netmessage message);

    /// <summary>
    ///     Verification method for when a client has sent a Netmessage to the server. Once received the server will
    ///     immediately respond by calling this method.
    /// </summary>
    /// <param name="guid">Global Unique ID of the message</param>
    public abstract bool VerifyMessage(string guid);

    /// <summary>
    ///     Validate authentication credentials. Method will only return true when authentication is successfull.
    ///     If authenticaion fails an Exception whill be thrown.
    /// </summary>
    /// <param name="timestamp">Device timestamp</param>
    /// <param name="hash">Hash of all parameters + salt</param>
    /// <param name="client">CometClient object which is authenticated</param>
    /// <returns>True if authentication is successful. Otherwise exception will be thrown</returns>
    internal bool ValidateAuthentication(CometClient client, long timestamp, string hash)
    {
        if (client.Identifier.StartsWith(CometConstants.CometIdentifierWebservice) || 
            client.Identifier.StartsWith(CometConstants.CometIdentifierCms) || 
            client.Identifier.StartsWith(CometConstants.CometIdentifierNoc) || 
            client.Identifier.StartsWith(CometConstants.CometIdentifierServices) || 
            client.Identifier.StartsWith(CometConstants.CometIdentifierControlCenter))
        {
            if (!Hasher.IsHashValidForParameters(hash, CometConstants.CometInternalSalt, timestamp, client.Identifier, client.IdentifierComet))
            {
                throw new ObymobiException(AuthenticateResult.AuthenticateHashFailed, "Hash: {0} | Timestamp: {1} | Identifier: {2} | Comet Identifier: {3}", hash, timestamp, client.Identifier, client.IdentifierComet);
            }
        }
        else
        {
            WebserviceHelper.ValidateRequest(timestamp, client.Identifier, hash, client.IdentifierComet);

            List<CometClient> cometClients;
            if (CometServer.Instance.TryGetCometClients(client.Identifier, out cometClients))
            {
                if (cometClients.Find(c => c.IdentifierComet == client.IdentifierComet) != null)
                {
                    throw new ObymobiException(AuthenticateResult.AlreadyAuthenticated, "Identifier: {0} | Comet Identifier: {1}", client.Identifier, client.IdentifierComet);
                }
            }
        }

        return true;
    }

    #region IDisposable Implementation

    /// <summary>
    ///     Dispose CometClient object. This will remove the client from the CometServer and from any pinned groups.
    /// </summary>
    public void Dispose(string message, bool sendDisconnect = true)
    {
        if (!this.IsDisposed)
        {
            this.IsDisposed = true;
            this.IsAuthenticated = false;

            this.ScheduleForGarbageCollect(message);
        }
    }

    /// <summary>
    ///     Dispose CometClient object. This will remove the client from the CometServer and from any pinned groups.
    /// </summary>
    public virtual void Dispose()
    {
        this.Dispose("");
    }

    public abstract void ScheduleForGarbageCollect(string message);    

    #endregion
}