﻿    using Obymobi.Enums;
    using Obymobi.Logic.Model;

public class DummyCometClient : CometClient
{
    public DummyCometClient()
        : base("FAKE-CLIENT", ClientCommunicationMethod.Webservice)
    {
        this.IsAuthenticated = true;
        this.ClientType = NetmessageClientType.Unknown;
    }

    public override bool TransmitMessage(string[] receiversIdentifierComet, Netmessage message)
    {
        return true;
    }

    public override bool VerifyMessage(string guid)
    {
        return true;
    }

    public override void ScheduleForGarbageCollect(string message)
    {

    }
}