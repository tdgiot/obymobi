﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI;
using Dionysos.Web;

public partial class Connections : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        List<CometClient> connectedClients = new List<CometClient>();

        if (QueryStringHelper.GetInt("clientId", out int clientId))
        {
            connectedClients = CometServer.Instance.GetCometClientsByClientId(clientId);
        }
        else if (QueryStringHelper.GetString("identifier", out string identifier))
        {
            connectedClients = CometServer.Instance.GetCometClientsByIdentifier(identifier);
        }


        StringBuilder clientBuilder = new StringBuilder();

        foreach (CometClient cometClient in connectedClients)
        {
            string row = string.Format("<tr><td>{0}</td><td>{1}</td><td>{2} ({3})</td><td>{4}</td><td>{5}</td><td>{6}</td><tr>", cometClient.Identifier, cometClient.IdentifierComet, cometClient.IpAddressExtern, cometClient.IpAddressLocal, cometClient.ClientId, cometClient.TerminalId, cometClient.DeliverypointId);
            clientBuilder.AppendLine(row);
        }

        /*
        var terminalBuilder = new StringBuilder();
        var supporttoolsBuilder = new StringBuilder();
        var sandboxBuilder = new StringBuilder();
        var unknownBuilder = new StringBuilder();
        var internalBuilder = new StringBuilder();

        List<string> clientMacs = new List<string>();

        PredicateExpression filter = new PredicateExpression(ClientFields.CompanyId == 251); // Lainston house
        filter.Add(ClientFields.DeviceId != DBNull.Value);

        PrefetchPath prefetch = new PrefetchPath(EntityType.ClientEntity);
        prefetch.Add(ClientEntityBase.PrefetchPathDeviceEntity);

        ClientCollection clients = new ClientCollection();
        clients.GetMulti(filter, prefetch);

        List<string> identifiers = new List<string>();
        foreach (ClientEntity client in clients)
        {
            if (client.DeviceId.HasValue && client.DeviceEntity != null)
            {
                identifiers.Add(client.DeviceEntity.Identifier);
            }
        }

        var connectedClients = new List<KeyValuePair<string, List<CometClient>>>(); // CometServer.Instance.GetClientsByIdentifiers(identifiers.ToArray());
        foreach (KeyValuePair<string, List<CometClient>> clientEntry in connectedClients)
        {
            foreach (CometClient cometClient in clientEntry.Value)
            {
                string row = string.Empty;

                if (cometClient.ClientType == NetmessageClientType.Emenu && clientMacs.Contains(cometClient.Identifier))
                    row = string.Format("<tr><td><strong>{0}</strong></td><td>{1}</td><td>{2} ({3})</td><tr>", cometClient.Identifier, cometClient.IdentifierComet, cometClient.IpAddressExtern, cometClient.IpAddressLocal);
                else
                    row = string.Format("<tr><td>{0}</td><td>{1}</td><td>{2} ({3})</td><tr>", cometClient.Identifier, cometClient.IdentifierComet, cometClient.IpAddressExtern, cometClient.IpAddressLocal);

                if (cometClient.SandboxMode)
                    sandboxBuilder.Append(row);
                else if (cometClient.ClientType == NetmessageClientType.Emenu)
                {
                    clientBuilder.Append(row);
                    clientMacs.Add(cometClient.Identifier);
                }
                else if (cometClient.ClientType == NetmessageClientType.Console || cometClient.ClientType == NetmessageClientType.OnsiteServer)
                    terminalBuilder.Append(row);
                else if (cometClient.ClientType == NetmessageClientType.SupportTools)
                    supporttoolsBuilder.Append(row);
                else if (cometClient.ClientType == NetmessageClientType.Cms || cometClient.ClientType == NetmessageClientType.MobileNoc || cometClient.ClientType == NetmessageClientType.Webservice)
                    internalBuilder.Append(row);
                else
                    unknownBuilder.Append(row);
            }
        }*/

        this.plhClients.Controls.Add(new LiteralControl(clientBuilder.ToString()));
        //this.plhTerminals.Controls.Add(new LiteralControl(terminalBuilder.ToString()));
        //this.plhSupportTools.Controls.Add(new LiteralControl(supporttoolsBuilder.ToString()));
        //this.plhSandboxes.Controls.Add(new LiteralControl(sandboxBuilder.ToString()));
        //this.plhUnknown.Controls.Add(new LiteralControl(unknownBuilder.ToString()));
        //this.plhInternal.Controls.Add(new LiteralControl(internalBuilder.ToString()));
    }
}
