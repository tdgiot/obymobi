﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using Dionysos;
using Dionysos.Drawing;
using Dionysos.Web;
using Obymobi;
using Obymobi.Data;
using Obymobi.Data.EntityClasses;
using Obymobi.Enums;
using Obymobi.Logic.HelperClasses;
using Obymobi.Web.CloudStorage;
using PdfSharp.Drawing;
using PdfSharp.Pdf;
using PdfSharp.Pdf.IO;

using foxit.common;
using foxit.common.fxcrt;
using foxit.pdf;
using System.Runtime.InteropServices;
using Path = System.IO.Path;

/// <summary>
/// Summary description for PdfConverter
/// </summary>
public class PdfHelper
{
    private const string SessionKey = "PdfConverter.SessionKey";

    public enum PdfHelperResult
    {
        PdfContainsZeroPages = 201,
    }

    private PdfHelper()
    {
    }

    /// <summary>
    /// This is not a Singleton, but single instance per Session class
    /// </summary>
    public static PdfHelper Instance
    {
        get
        {

            PdfHelper instance = SessionHelper.GetValue<PdfHelper>(PdfHelper.SessionKey);
            if (instance == null)
            {
                instance = new PdfHelper();
                SessionHelper.SetValue(PdfHelper.SessionKey, instance);
            }

            return instance;
        }
    }

    public void AttachPdfAsMedia(IMediaContainingEntity entity, string fileName, byte[] fileBytes, List<MediaRatioType> mediaRatioTypes, out int pages, Action<string> deleteFromCdn, Action<MediaEntity, string, byte[]> uploadToCdn)
    {
        // Retrieve the Entity PK and PK Fieldname, so the entity can go out of scope during the processing.
        // these are used on the MediaEntity as FK.
        string fkFieldName = entity.PrimaryKeyFields[0].Name;
        int fkId = (int)entity.PrimaryKeyFields[0].CurrentValue;

        // Run in background.
        int sortOrder = 1;

        if (entity.MediaCollection.Count > 0)
        {
            sortOrder = entity.MediaCollection.Max(x => x.SortOrder ?? 1);
        }

        pages = 3;

        this.ProcessPdf(fkFieldName, fkId, fileName, sortOrder, fileBytes, mediaRatioTypes, out pages, deleteFromCdn, uploadToCdn);
    }

    private void ProcessPdf(string fkFieldName, int fkValue, string fileName, int sortOrderMax, byte[] fileBytes, List<MediaRatioType> mediaRatioTypes, out int pages, Action<string> deleteFromCdn, Action<MediaEntity, string, byte[]> uploadToCdn)
    {
        MemoryStream bmpStream = null;
        System.Drawing.Bitmap image = null;
        try
        {

            string sn = "tEsr9cv4K+3+yL5zF1oUDuNB6sZSTlS9Q9vsdqR5oklS2qIoniKQ2w==";
            string key = "8f3gFcFtvWsN+jePnKBLSE+0EGpkHBiRfwGAS7gfZbXJu5jSaB+F5Ovnvl11UH61Kd5930y7vF4l4a3LrNDgiJm3viJFnSmQfAwXpEU7HU+Zl9yhi+GM+IDixegN+TpED/UUOEaQ3rK0L8ouxSFSapFGg2SxdyVFDaa3UMKEYWIAPa4EkEdJeIOQdnRz2OIR1Xg81oHFc5IZzuPDwRucV7XoKTw5A2Xrdr3KCzKjyRGvqziV1MHt+8Jjgx/2x/tycqYjIePv6V1pQti7+PjbtHuwN+KrdLkg201IVseOaK48ex+4o8qM38L+D4amDcCqUHm/iOWdqz4s+iUIA/HSbi8VEGHXUVsHniUAcc7YryIb7pGUiMmQ9V+OJW3ygIa7tqT1TTUFuDmlj7GuTpo5Tcr7WpdIP7dWOYzykzZDHbSWO7FWcAB6lDQyrA2Kob3jiqk5vxCCbtLKLoiNl2PqvyH0LHBTsK1LAoNbrvqAP0O5Q7lnlZHPvm8E8ep/V4YjQ2Ew4fSSnQSRfIkJ2kx8KW/kKk4O76fLyOFOkeCFpfmZGWnKYxa1utABLfKDFKGW0h2og4/nJnnudcNWJq1F54BxwZec/BN30aoWSeztWN2iASIUe/tBpm+i0mKWttiDWXNdbO2YnKK8tf3ZgR8vt89bWmQWMJTSPZ8IGN0pXt/s2QGeyMWPPe3h5O8v1kWzoxZKkX9XxmvbP9K90CBv1jj9dLIlim42RC+f+OCyX3j0vBKcJsi3RMRy/LhE/NyOZhJf2IAOQqGl+X15STKC+z+Mf37I0W1baWVk5QsaAHfKlKKuQTU4hS3LyPuMPuYcCwSecYh8p00fnY1wKzpGHWuemDajkHDAQf3n9H0VwaR0s3nfaGY6SiQ+2sPUtLK2m1kGblj1bdsJL/6buu9PdYwDF7GQatl7PzFrwuojfE5tBRf69pa7OasLIzoxq7rn6fnX/DqB4yY2Lxcx6J2BbtNVGQLOY3qsgQM3KOIs5eB4KQZRCLbGtQn3OF9nI417j1boVoWPAjDDxqNf6ffQ72hEWqrEpzphXNIEsPuQXOGXzcz+5QdUvk5C2GxrQhk6X83f40W53FKRIQkj72iJ70ivj4DvxlWym6BEsaN/dXMW8+zCS7M6ZlLvoEyZYyuS3SUqnuCeay0E5JjHcjDmYjrxxE93TNx7TOg41M75b84eJEjocUPsPMXt1zl1S//7pYoMavMcNkbmIikcbEbzU0vuqd6UWWoWpLblkv33aFXWG3oGGaOQJRiO2cVQUUVEFUFhfZnkm2dRiKmSe/fMMoh8LauV1UoAxa7sqYFXN/7DR3FT9Nn9LqKUo2x7hMUVyUd6gnDy/k6o7IQOuw==";
            // Initialize library
            ErrorCode error_code = Library.Initialize(sn, key);
            if (error_code != ErrorCode.e_ErrSuccess)
            {
                Console.WriteLine("Library Initialize Error: {0}\n", error_code);
                pages = 0;
                return;
            }

            IntPtr buffer_intptr = Marshal.AllocHGlobal(fileBytes.Length);
            Marshal.Copy(fileBytes, 0, buffer_intptr, fileBytes.Length);

            using (PDFDoc doc = new PDFDoc(buffer_intptr, (uint)fileBytes.Length))
            {
                error_code = doc.LoadW(null);
                if (error_code != ErrorCode.e_ErrSuccess)
                {
                    pages = 0;
                    return;
                }

                // JPG Quality Parameter
                ImageCodecInfo jpgEncoder = ImageCodecInfo.GetImageDecoders().FirstOrDefault(x => x.FormatID.Equals(ImageFormat.Jpeg.Guid));
                Encoder myEncoder = Encoder.Quality;
                EncoderParameters encoderParams = new EncoderParameters(1);

                EncoderParameter encoderParam = new EncoderParameter(myEncoder, 70L);
                encoderParams.Param[0] = encoderParam;

                // Add each page as a Media Entity
                pages = doc.GetPageCount();
                for (int i = 0; i < pages; i++)
                {
                    using (PDFPage page = doc.GetPage(i))
                    {
                        byte[] bytes = null;
                        // Parse page.
                        page.StartParse((int)foxit.pdf.PDFPage.ParseFlags.e_ParsePageNormal, null, false);

                        int width = (int)(page.GetWidth());
                        int height = (int)(page.GetHeight());
                        Matrix2D matrix = page.GetDisplayMatrix(0, 0, width, height, page.GetRotation());

                        // Prepare a bitmap for rendering.
                        image = new System.Drawing.Bitmap(width, height, System.Drawing.Imaging.PixelFormat.Format32bppArgb);
                        Graphics draw = Graphics.FromImage(image);
                        draw.Clear(System.Drawing.Color.White);

                        // Render page
                        Renderer render = new Renderer(image, false);
                        render.StartRender(page, matrix, null);

                        // Resize if required
                        if (image.Size.Width > 4000 || image.Size.Height > 2000)
                        {
                            // Crop is full area
                            Rectangle cropArea = new Rectangle(0, 0, image.Size.Width, image.Size.Height);
                            Size size = ImageUtil.GetFittingDimenions(image.Size.Width, image.Size.Height, 4000, 2000);

                            image = ImageUtil.Resize(image, cropArea, size, ImageFormat.Jpeg, System.Drawing.Color.White);
                        }

                        using (MemoryStream ms = new MemoryStream())
                        {
                            image.Save(ms, jpgEncoder, encoderParams);
                            bytes = ms.ToArray();
                        }

                        // Apply Jpeg compression
                        bytes = ImageUtil.ConvertToJpg(bytes, 70);

                        MediaEntity mediaEnt = new MediaEntity();
                        mediaEnt.Name = Path.GetFileNameWithoutExtension(fileName) + " " + (i + 1);
                        mediaEnt.Fields[fkFieldName].CurrentValue = fkValue;
                        mediaEnt.FilePathRelativeToMediaPath = Path.GetFileNameWithoutExtension(fileName) + ".jpg"; // We convert it to a JPG.
                        mediaEnt.MediaType = -1;
                        mediaEnt.SetMimeTypeAndExtension(mediaEnt.FilePathRelativeToMediaPath);
                        mediaEnt.SortOrder = sortOrderMax + i + 1;
                        mediaEnt.SizeWidth = image.Width;
                        mediaEnt.SizeHeight = image.Height;

                        // Save the size of the image                
                        if (bytes.Length > 0)
                        {
                            mediaEnt.SizeKb = Convert.ToInt32(bytes.Length) / 1024;
                        }

                        // Save the media entity
                        if (mediaEnt.Save())
                        {
                            if (mediaEnt.LastDistributedVersionTicks.HasValue)
                            {
                                string mediaPathOld = mediaEnt.GetMediaPath(FileNameType.Format);
                                mediaPathOld = mediaPathOld.Replace(MediaEntity.FILENAME_FORMAT_TICKS, mediaEnt.LastDistributedVersionTicks.Value.ToString(CultureInfo.InvariantCulture));

                                deleteFromCdn(mediaPathOld);
                            }

                            mediaEnt.Refetch();

                            // Update last distributed ticks
                            mediaEnt.LastDistributedVersionTicks = mediaEnt.UpdatedUTC.GetValueOrDefault(mediaEnt.CreatedUTC.Value).Ticks;

                            mediaEnt.Upload(bytes);
                        }

                        // Save MediaRatioTypes if any
                        if (mediaRatioTypes != null)
                        {
                            foreach (var mediaRatioType in mediaRatioTypes)
                            {
                                MediaRatioTypeMediaEntity mrtm = new MediaRatioTypeMediaEntity();
                                mediaEnt.MediaRatioTypeMediaCollection.Add(mrtm);
                                mediaEnt.FileDownloadDelegate = () => mrtm.MediaEntity.Download();
                                mrtm.MediaTypeAsEnum = mediaRatioType.MediaType;
                                mrtm.SetDefaultCrop(image.Size);
                                mrtm.Save();
                                mrtm.Refetch();
                                mrtm.ResizeAndPublishFile(mediaEnt, true);
                            }
                        }
                    }
                }

            }
        }
        finally
        {
            if (bmpStream != null)
            {
                bmpStream.Dispose();
            }

            if (image != null)
            {
                image.Dispose();
            }
        }
    }

    public static byte[] MergeWithCoverPage(HttpPostedFile postedFile, ClientEntity client)
    {
        string roomCaption = !client.DeliverypointgroupEntity.DeliverypointCaption.IsNullOrWhiteSpace() ? client.DeliverypointgroupEntity.DeliverypointCaption : "Room";
        string roomAndNumberCaption = string.Format("{0} {1}", roomCaption, client.DeliverypointEntity.Number);

        string pagesCaption = "Pages";
        Dictionary<CustomTextType, string> customTexts = client.DeliverypointgroupEntity.CustomTextCollection.ToCultureCodeSpecificDictionary(client.CompanyEntity.CultureCode);
        string defaultPagesCaption = customTexts.GetValueOrDefault(CustomTextType.DeliverypointgroupPagesCaption);
        if (!defaultPagesCaption.IsNullOrWhiteSpace())
        {
            pagesCaption = defaultPagesCaption;
        }

        return PdfHelper.AddCoverPage(postedFile, roomAndNumberCaption, pagesCaption);
    }

    public static byte[] AddCoverPage(HttpPostedFile postedFile, string roomAndNumberCaption, string pagesCaption)
    {
        // Combined byte array
        byte[] combinedFile = null;

        // Pdf file        
        using (PdfDocument contentPdf = PdfReader.Open(HttpContext.Current.Request.Files[0].InputStream, PdfDocumentOpenMode.Import))
        using (PdfDocument newPdf = new PdfDocument())
        {
            string totalPages = string.Format("{0}: {1}", pagesCaption, contentPdf.PageCount);

            int width = 793;
            int height = 1122;

            System.Drawing.Bitmap textImage = new System.Drawing.Bitmap(width, height);

            StringFormat format = new StringFormat();
            format.Alignment = StringAlignment.Center;
            format.LineAlignment = StringAlignment.Center;

            Graphics g = Graphics.FromImage(textImage);
            g.SmoothingMode = SmoothingMode.AntiAlias;
            g.DrawString(roomAndNumberCaption, new System.Drawing.Font("Verdana", 18, FontStyle.Bold), Brushes.Black, new RectangleF(0, (height / 2) - 50, width, 50), format);
            g.DrawString(totalPages, new System.Drawing.Font("Verdana", 12, FontStyle.Regular), Brushes.Black, new System.Drawing.PointF((width / 2), (height / 2)), format);
            g.Flush();

            XImage image = XImage.FromGdiPlusImage(textImage);

            // Front page
            PdfPage frontPage = new PdfPage();
            newPdf.AddPage(frontPage);

            XGraphics gfx = XGraphics.FromPdfPage(frontPage);
            gfx.SmoothingMode = XSmoothingMode.AntiAlias;
            gfx.DrawImage(image, 0, 0);

            // Add the pages from the content pdf
            PdfHelper.CopyPages(contentPdf, newPdf);

            using (MemoryStream stream = new MemoryStream())
            {
                newPdf.Save(stream, true);
                combinedFile = stream.ToArray();
            }
        }

        return combinedFile;
    }

    private static void CopyPages(PdfDocument from, PdfDocument to)
    {
        for (int i = 0; i < from.PageCount; i++)
        {
            PdfPage toAdd = from.Pages[i];
            toAdd.Orientation = PdfSharp.PageOrientation.Landscape;

            to.AddPage(toAdd);
        }
    }
}