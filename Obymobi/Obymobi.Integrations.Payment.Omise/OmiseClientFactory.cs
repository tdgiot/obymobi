﻿namespace Obymobi.Integrations.Payment.Omise
{

    public class OmiseClientFactory : IClientFactory
    {
        private readonly IPaymentProviderConfiguration paymentProviderConfiguration;

        public OmiseClientFactory(IPaymentProviderConfiguration paymentProviderConfiguration) => this.paymentProviderConfiguration = paymentProviderConfiguration;

        public IClient Create() => new OmiseClient(paymentProviderConfiguration.ApiKey);
    }
}
