﻿using System.Threading.Tasks;
using Obymobi.Integrations.Payment.Models;
using Omise;
using Omise.Models;

namespace Obymobi.Integrations.Payment.Omise
{
    public class OmiseClient : IClient
    {
        private readonly string secretKey;

        public OmiseClient(string publicKey) => secretKey = publicKey;

        public bool IsAuthenticated() => Task.Run(() => GetCapabilityAsync(secretKey)).Result != null;

        public GetPaymentMethodsResponse GetPaymentMethods() => throw new System.NotImplementedException();

        private static async Task<Capabilities> GetCapabilityAsync(string secretKey)
        {
            if (string.IsNullOrWhiteSpace(secretKey))
            {
                return null;
            }

            try
            {
                Client client = CreateClient(secretKey);
                return await client.Requester.Request<Capabilities>(Endpoint.Api, "GET", "/capability");
            }
            catch (OmiseError omiseError)
            {
                Logging.Exceptionlogger.CreateExceptionlog(omiseError);
            }
            catch (OmiseException omiseException)
            {
                Logging.Exceptionlogger.CreateExceptionlog(omiseException);
            }

            return null;
        }

        private static Client CreateClient(string secretKey) => new Client(skey: secretKey);

        private class Capabilities : ModelBase
        {
        }
    }
}
