﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Dionysos.Data.LLBLGen;
using Dionysos;

namespace Obymobi.Data.Master
{
	/// <summary>
	/// Helping methods for local refreshing of the data
	/// </summary>
	public class RefreshLocalHelper
	{
		private IEntityCollection entityCollection;

		public RefreshLocalHelper(IEntityCollection entityCollection)
		{
			this.entityCollection = entityCollection;
			this.entityCollection.EntityAdded += new EventHandler<CollectionChangedEventArgs>(entityCollection_EntityAdded);
			this.MarkItemsAsNotUpdated();
		}

		/// <summary>
		/// Marks all items False for field ForLocalUseWhileSyncingIsUpdated
		/// </summary>
		/// <param name="entity">Entity to mark as updated</param>
		public void MarkItemsAsNotUpdated()
		{
			foreach (IEntity entity in this.entityCollection)
			{
				if (entity.Fields["ForLocalUseWhileSyncingIsUpdated"] == null)
				{
					throw new Dionysos.TechnicalException("Het veld '{0}' is niet beschikbaar op '{1}', haal de laatste versie van DATA op", "ForLocalUseWhileSyncingIsUpdated", entity.LLBLGenProEntityName);
				}
				else
				{
					entity.Fields["ForLocalUseWhileSyncingIsUpdated"].CurrentValue = false;
					entity.PropertyChanged += new System.ComponentModel.PropertyChangedEventHandler(entity_PropertyChanged);
					entity.AfterSave += new EventHandler(entity_AfterSave);
				}
			}			
		}

		void entity_AfterSave(object sender, EventArgs e)
		{
			this.MarkItemAsUpdated((IEntity)sender);
		}

		/// <summary>
		/// Mark an item as updated
		/// </summary>
		/// <param name="entity">Entity to mark as updated</param>
		public static void MarkItemAsUpdatedStatic(IEntity entity)
		{
			entity.Fields["ForLocalUseWhileSyncingIsUpdated"].CurrentValue = true;
		}

		/// <summary>
		/// Mark an item as updated
		/// </summary>
		/// <param name="entity">Entity to mark as updated</param>
		public void MarkItemAsUpdated(IEntity entity)
		{
			entity.Fields["ForLocalUseWhileSyncingIsUpdated"].CurrentValue = true;
		}

		public void DeleteAllNonUpdatedItems()
		{
            if (this.entityCollection.Count > 0)
            {
                // Delete all non updated entites
                PredicateExpression filter = LLBLGenFilterUtil.GetFieldCompareValueEqualPredicateExpression(this.entityCollection[0].LLBLGenProEntityName, "ForLocalUseWhileSyncingIsUpdated", false);

                foreach (IEntity entity in this.entityCollection)
                {
                    if ((bool)entity.Fields["ForLocalUseWhileSyncingIsUpdated"].CurrentValue == false)
                    {
                        try
                        {
                            entity.Delete();
                        }
                        catch(Exception ex)
                        {
                            throw new Exception(string.Format("Delete failed for entity of type: '{0}', PK: '{1}' - Error: {2}", entity.LLBLGenProEntityName, entity.PrimaryKeyFields[0].CurrentValue, ex.Message));
                        }
                    }
                }
            }
		}

		void entity_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
		{
			this.MarkItemAsUpdated((IEntity)sender);
		}

		void entityCollection_EntityAdded(object sender, CollectionChangedEventArgs e)
		{
			this.MarkItemAsUpdated((IEntity)e.InvolvedEntity);
		}
	}
}
