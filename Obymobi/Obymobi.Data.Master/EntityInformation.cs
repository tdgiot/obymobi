﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using Dionysos;
using Dionysos.Data.LLBLGen;
using Dionysos.Interfaces.Data;
using Obymobi.Data.Master.CollectionClasses;
using Obymobi.Data.Master.EntityClasses;
using Obymobi.Data.Master.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.Master
{
    public class EntityInformation
    {
        #region Fields

        static EntityInformation instance = null;

        #endregion

        #region Constructors

        /// <summary>
        /// Constructs an instance of the EntityInformationUtil class if the instance has not been initialized yet
        /// </summary>
        public EntityInformation()
        {
            if (instance == null)
            {
                // first create the instance
                instance = new EntityInformation();
                // Then init the instance (the init needs the instance to fill it)
                instance.Init();
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Initializes the static EntityInformationUtil instance
        /// </summary>
        private void Init()
        {
        }

        #region Creation of entity information in Master

        /// <summary>
        /// Creates EntityInformation entities for the ones that do not exist already
        /// </summary>
        public static void CreateEntityInformationEntities()
        {
            // Preload EntityInformationCollection instance
            // and initialize the EntityView
            EntityInformationCollection entityInformationCollection = new EntityInformationCollection();
            entityInformationCollection.GetMulti(null);

            // Get the default entity view from the collection
            EntityView<EntityInformationEntity> entityInformationView = entityInformationCollection.DefaultView;

            // Get the entity names
            string[] entityNames = LLBLGenUtil.GetEntityNames();

            // Walk through the entity names
            for (int i = 0; i < entityNames.Length; i++)
            {
                string entityName = LLBLGenUtil.GetEntityName(entityNames[i]);

                // Create filter for the view
                // and apply the filter to the view
                PredicateExpression filterEntityName = new PredicateExpression(EntityInformationFields.EntityName == entityName);
                entityInformationView.Filter = filterEntityName;

                // check count to see if we need to add
                if (entityInformationView.Count > 1)
                {
                    throw new Dionysos.TechnicalException("Multiple EntityInformation records found for: {0}", entityName);
                }
                else if (entityInformationView.Count == 0)
                {
                    // Not found, add:
                    EntityInformationEntity entityToInsert = new EntityInformationEntity();

                    // Set field values & save entity
                    entityToInsert.EntityName = entityName;

                    // Load an instance of the type we are adding to the database to set a default display field
                    IEntity entityOfTypeToAdd = DataFactory.EntityFactory.GetEntity(entityName) as IEntity;

                    // Try to set a default ShowFieldName
                    if (entityOfTypeToAdd.Fields["Name"] != null)
                        entityToInsert.ShowFieldName = "Name";
                    else if (entityOfTypeToAdd.Fields["Title"] != null)
                        entityToInsert.ShowFieldName = "Title";
                    else if (entityOfTypeToAdd.Fields["Subject"] != null)
                        entityToInsert.ShowFieldName = "Subject";
                    else if (entityOfTypeToAdd.Fields["Company"] != null)
                        entityToInsert.ShowFieldName = "Company";
                    else if (entityOfTypeToAdd.Fields["Number"] != null)
                        entityToInsert.ShowFieldName = "Number";
                    else if (entityOfTypeToAdd.Fields["Description"] != null)
                        entityToInsert.ShowFieldName = "Description";

                    entityToInsert.Save();

                    // Add to collection for viewing/searchgin
                    entityInformationView.RelatedCollection.Add(entityToInsert);
                }
                else
                {
                    // Found, nothing to add							
                }
            }
        }

        /// <summary>
        /// Creates EntityFieldInformation entities for the ones that do not exist already
        /// </summary>
        /// <param name="update">Flag which indicates whether existing field information should be updated</param>
        public static void CreateEntityFieldInformationEntities(bool update)
        {
            EntityFieldInformationCollection entityFieldInformationCollection = new EntityFieldInformationCollection();
            entityFieldInformationCollection.GetMulti(null);

            // Get the default entity view from the collection
            EntityView<EntityFieldInformationEntity> entityFieldInformationView = entityFieldInformationCollection.DefaultView;

            // Get the entity names
            string[] entityNames = LLBLGenUtil.GetEntityNames();

            // Walk through the entity names
            RefreshLocalHelper refreshHelper = new RefreshLocalHelper(entityFieldInformationCollection);
            for (int i = 0; i < entityNames.Length; i++)
            {
                string entityName = LLBLGenUtil.GetEntityName(entityNames[i]);
                CreateEntityFieldInformationEntities(entityName, ref entityFieldInformationView, update);
            }
            refreshHelper.DeleteAllNonUpdatedItems();
        }

        /// <summary>
        /// Creates EntityFieldInformation entities for the database fields, properties and related fields
        /// that do not exists already for the specified entity name
        /// </summary>
        /// <param name="entityName">The name of the entity to create the fields for</param>
        /// <param name="entityFieldInformationView">The EntityView instance to perform the search operations with</param>
        /// <param name="update">Flag which indicates whether existing field information should be updated</param>
        private static void CreateEntityFieldInformationEntities(string entityName, ref EntityView<EntityFieldInformationEntity> entityFieldInformationView, bool update)
        {
            if (Instance.ArgumentIsEmpty(entityName, "entityName"))
            {
                // Parameter 'entityName' is empty
            }
            else
            {
                // Get an entity to get access to its fields
                IEntity entity = DataFactory.EntityFactory.GetEntity(entityName) as IEntity;
                if (Instance.Empty(entity))
                {
                    throw new EmptyException("Variable 'entity' is empty.");
                }
                else
                {
                    CreateEntityFieldInformationEntitiesForDatabaseFields(entity, ref entityFieldInformationView, update);
                    CreateEntityFieldInformationEntitiesForProperties(entity, ref entityFieldInformationView, update);
                }
            }
        }

        /// <summary>
        /// Creates EntityFieldInformation entities for the database fields of the specified entity
        /// </summary>
        /// <param name="entity">The entity to create the EntityFieldInformation for</param>
        /// <param name="entityFieldInformationView">The EntityView instance to perform the search operations with</param>
        /// <param name="update">Flag which indicates whether existing field information should be updated</param>
        private static void CreateEntityFieldInformationEntitiesForDatabaseFields(IEntity entity, ref EntityView<EntityFieldInformationEntity> entityFieldInformationView, bool update)
        {
            if (Instance.ArgumentIsEmpty(entity, "entity"))
            {
                // Parameter 'entity' is empty
            }
            else
            {
                string entityName = LLBLGenUtil.GetEntityName(entity.LLBLGenProEntityName);

                // Get the field names for the current entity
                string[] fields = DataFactory.EntityUtil.GetFields(entityName);

                // Walk through the field names
                for (int j = 0; j < fields.Length; j++)
                {
                    string fieldName = fields[j];
                    if (fieldName.Contains("_"))
                    {
                        // Field names which contain an "_" are 
                        // inherited fieldnames using entity inheritance
                    }
                    else
                    {
                        // Get the corresponding IEntityField instance from the IEntity instance
                        IEntityField entityField = entity.Fields[fieldName];
                        if (Instance.Empty(entityField))
                        {
                            throw new EmptyException("Variable 'entityField' is empty for <{0}>.Fields[<{1}>].", entity.LLBLGenProEntityName, fieldName);
                        }
                        else
                        {
                            // Create a filter to set on the view
                            // and apply the filter
                            PredicateExpression filterFieldInformation = new PredicateExpression();
                            filterFieldInformation.Add(EntityFieldInformationFields.EntityName == entityName);
                            filterFieldInformation.Add(EntityFieldInformationFields.FieldName == fieldName);
                            entityFieldInformationView.Filter = filterFieldInformation;

                            // Check count for exitence
                            if (entityFieldInformationView.Count > 1)
                            {
                                throw new Dionysos.TechnicalException("Multiple EntityFieldInformation records found for: {0}.{1}", entityName, fieldName);
                            }
                            else if (entityFieldInformationView.Count == 0)
                            {
                                // Need to add an entity
                                EntityFieldInformationEntity entityToInsert = new EntityFieldInformationEntity();

                                // Set values and save
                                entityToInsert.EntityName = entityName;
                                entityToInsert.FieldName = fieldName;
                                entityToInsert.ExcludeForUpdateCompanyDataVersionOnChange = true;
                                entityToInsert.ExcludeForUpdateMenuDataVersionOnChange = true;
                                entityToInsert.ExcludeForUpdatePosIntegrationInformationVersionOnChange = true;
                                entityToInsert.ExcludeForUpdateWmsCacheDateOnChange = true;
                                entityToInsert.ExcludeForUpdateDeliverypointDataVersionOnChange = true;
                                entityToInsert.ExcludeForUpdateSurveyDataVersionOnChange = true;
                                entityToInsert.ExcludeForUpdateAnnouncementDataVersionOnChange = true;
                                entityToInsert.ExcludeForUpdateEntertainmentDataVersionOnChange = true;
                                entityToInsert.ExcludeForUpdateAdvertisementDataVersionOnChange = true;
                                // MDB TODO
                                //entityToInsert.EntityFieldType = Dionysos.Data.EntityInformationUtil.EntityFieldTypeDatabase;
                                entityToInsert.Type = entityField.ActualDotNetType.ToString();
                                entityToInsert.Save();

                                // Add to collection for viewing/searchgin
                                entityFieldInformationView.RelatedCollection.Add(entityToInsert);
                            }
                            else if (entityFieldInformationView.Count == 1 && update)
                            {
                                EntityFieldInformationEntity entityToUpdate = entityFieldInformationView[0];

                                entityToUpdate.EntityName = entityName;
                                entityToUpdate.FieldName = fieldName;
                                // MDB TODO
                                //entityToUpdate.EntityFieldType = Dionysos.Data.EntityInformationUtil.EntityFieldTypeDatabase;
                                entityToUpdate.Type = entityField.ActualDotNetType.ToString();
                                entityToUpdate.Save();

                                RefreshLocalHelper.MarkItemAsUpdatedStatic(entityFieldInformationView[0]);
                            }
                            else if (entityFieldInformationView.Count == 1)
                            {
                                RefreshLocalHelper.MarkItemAsUpdatedStatic(entityFieldInformationView[0]);
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Creates EntityFieldInformation entities for the properties of the specified entity
        /// </summary>
        /// <param name="entity">The entity to create the EntityFieldInformation for</param>
        /// <param name="entityFieldInformationView">The EntityView instance to perform the search operations with</param>
        /// <param name="update">Flag which indicates whether existing field information should be updated</param>
        private static void CreateEntityFieldInformationEntitiesForProperties(IEntity entity, ref EntityView<EntityFieldInformationEntity> entityFieldInformationView, bool update)
        {
            if (Instance.ArgumentIsEmpty(entity, "entity"))
            {
                // Parameter 'entity' is empty
            }
            else
            {
                // Get the name of the entity
                string entityName = LLBLGenUtil.GetEntityName(entity.LLBLGenProEntityName);

                // Get the property information for the current entity
                PropertyInfo[] propertyInfo = Dionysos.Reflection.Member.GetPropertyInfo(entity);
                for (int j = 0; j < propertyInfo.Length; j++)
                {
                    PropertyInfo property = propertyInfo[j];

                    // Check whether the property has a DataGridViewColumnVisibleAttribute
                    if (property.GetCustomAttributes(typeof(Dionysos.Data.DataGridViewColumnVisibleAttribute), false).Length > 0)
                    {
                        string propertyName = property.Name;

                        // Create a filter to set on the view
                        // and apply the filter
                        PredicateExpression filterFieldInformation = new PredicateExpression();
                        filterFieldInformation.Add(EntityFieldInformationFields.EntityName == entityName);
                        filterFieldInformation.Add(EntityFieldInformationFields.FieldName == propertyName);
                        entityFieldInformationView.Filter = filterFieldInformation;

                        // Check count for exitence
                        if (entityFieldInformationView.Count > 1)
                        {
                            throw new Dionysos.TechnicalException("Multiple EntityFieldInformation records found for: {0}.{1}", entityName, propertyName);
                        }
                        else if (entityFieldInformationView.Count == 0)
                        {
                            // Need to add an entity
                            EntityFieldInformationEntity entityToInsert = new EntityFieldInformationEntity();

                            // Set values and save
                            entityToInsert.EntityName = entityName;
                            entityToInsert.FieldName = propertyName;
                            entityToInsert.EntityFieldType = Dionysos.Data.EntityInformationUtil.EntityFieldTypeCode;
                            entityToInsert.Type = property.PropertyType.ToString();
                            entityToInsert.Save();

                            // Add to collection for viewing/searchin
                            entityFieldInformationView.RelatedCollection.Add(entityToInsert);
                        }
                        else if (entityFieldInformationView.Count == 1 && update)
                        {
                            EntityFieldInformationEntity entityToUpdate = entityFieldInformationView[0];

                            entityToUpdate.EntityName = entityName;
                            entityToUpdate.FieldName = propertyName;
                            entityToUpdate.EntityFieldType = Dionysos.Data.EntityInformationUtil.EntityFieldTypeCode;
                            entityToUpdate.Type = property.PropertyType.ToString();
                            entityToUpdate.Save();

                            RefreshLocalHelper.MarkItemAsUpdatedStatic(entityFieldInformationView[0]);
                        }
                        else if (entityFieldInformationView.Count == 1)
                        {
                            RefreshLocalHelper.MarkItemAsUpdatedStatic(entityFieldInformationView[0]);
                        }
                    }
                }
            }
        }

        #endregion

        #region Creation of entity information in local database

        /// <summary>
        /// Refreshes EntityInformation entities from the master database to the local database
        /// </summary>
        /// <param name="update">Flag which indicates if found entity information should be updated</param>
        public static void RefreshEntityInformationEntities(bool update)
        {
            // Preload EntityInformationCollection instance
            // and initialize the EntityView
            IEntityCollection entityInformationCollection = LLBLGenEntityCollectionUtil.GetEntityCollection("EntityInformation");
            if (Instance.Empty(entityInformationCollection))
            {
                throw new EmptyException("Variable 'entityInformationCollection' is empty.");
            }
            else
            {
                // Get the default entity view from the collection
                IEntityView entityInformationView = entityInformationCollection.DefaultView;

                EntityInformationCollection entityInformationCollectionMaster = new EntityInformationCollection();
                entityInformationCollectionMaster.GetMulti(null);

                // Walk through the entity names
                for (int i = 0; i < entityInformationCollectionMaster.Count; i++)
                {
                    EntityInformationEntity entityInformationEntityMaster = entityInformationCollectionMaster[i];

                    string entityName = entityInformationEntityMaster.EntityName;

                    // Create filter for the view
                    // and apply the filter to the view
                    PredicateExpression filterEntityName = LLBLGenFilterUtil.GetFieldCompareValueEqualPredicateExpression("EntityInformation", new string[] { "EntityName" }, new object[] { entityName });
                    entityInformationView.Filter = filterEntityName;

                    // check count to see if we need to add
                    if (entityInformationView.Count > 1)
                    {
                        throw new Dionysos.TechnicalException("Multiple EntityInformation records found for: {0}", entityName);
                    }
                    else if (entityInformationView.Count == 0)
                    {
                        // Not found, add:
                        IEntity entityToInsert = DataFactory.EntityFactory.GetEntity("EntityInformation") as IEntity;
                        if (Instance.Empty(entityToInsert))
                        {
                            throw new EmptyException("Variable 'EntityInformation' is empty.");
                        }
                        else
                        {
                            // Set field values & save entity
                            entityToInsert.SetNewFieldValue("EntityName", entityName);

                            try
                            {
                                // Load an instance of the type we are adding to the database to set a default display field
                                IEntity entityOfTypeToAdd = DataFactory.EntityFactory.GetEntity(entityName) as IEntity;

                                // Try to set a default ShowFieldName
                                if (entityInformationEntityMaster.ShowFieldName.Length > 0)
                                    entityToInsert.SetNewFieldValue("ShowFieldName", entityInformationEntityMaster.ShowFieldName);
                                else if (entityOfTypeToAdd.Fields["Name"] != null)
                                    entityToInsert.SetNewFieldValue("ShowFieldName", "Name");
                                else if (entityOfTypeToAdd.Fields["Title"] != null)
                                    entityToInsert.SetNewFieldValue("ShowFieldName", "Title");
                                else if (entityOfTypeToAdd.Fields["Subject"] != null)
                                    entityToInsert.SetNewFieldValue("ShowFieldName", "Subject");
                                else if (entityOfTypeToAdd.Fields["Company"] != null)
                                    entityToInsert.SetNewFieldValue("ShowFieldName", "Company");
                                else if (entityOfTypeToAdd.Fields["Number"] != null)
                                    entityToInsert.SetNewFieldValue("ShowFieldName", "Number");
                                else if (entityOfTypeToAdd.Fields["Description"] != null)
                                    entityToInsert.SetNewFieldValue("ShowFieldName", "Description");

                                entityToInsert.SetNewFieldValue("FriendlyName", entityInformationEntityMaster.FriendlyName);
                                entityToInsert.SetNewFieldValue("FriendlyNamePlural", entityInformationEntityMaster.FriendlyNamePlural);
                                entityToInsert.SetNewFieldValue("HelpText", entityInformationEntityMaster.HelpText);
                                entityToInsert.SetNewFieldValue("ShowFieldNameBreadCrumb", entityInformationEntityMaster.ShowFieldNameBreadCrumb);
                                entityToInsert.SetNewFieldValue("TitleAsBreadCrumbHierarchy", entityInformationEntityMaster.TitleAsBreadCrumbHierarchy);
                                entityToInsert.SetNewFieldValue("DefaultEntityEditPage", entityInformationEntityMaster.DefaultEntityEditPage);
                                entityToInsert.SetNewFieldValue("DefaultEntityCollectionPage", entityInformationEntityMaster.DefaultEntityCollectionPage);
                                entityToInsert.SetNewFieldValue("UpdateMenuDataVersionOnChange", entityInformationEntityMaster.UpdateMenuDataVersionOnChange);
                                entityToInsert.SetNewFieldValue("UpdateCompanyDataVersionOnChange", entityInformationEntityMaster.UpdateCompanyDataVersionOnChange);
                                entityToInsert.SetNewFieldValue("UpdatePosIntegrationInformationOnChange", entityInformationEntityMaster.UpdatePosIntegrationInformationOnChange);
                                entityToInsert.SetNewFieldValue("UpdateDeliverypointDataVersionOnChange", entityInformationEntityMaster.UpdateDeliverypointDataVersionOnChange);
                                entityToInsert.SetNewFieldValue("UpdateSurveyDataVersionOnChange", entityInformationEntityMaster.UpdateSurveyDataVersionOnChange);
                                entityToInsert.SetNewFieldValue("UpdateAnnouncementDataVersionOnChange", entityInformationEntityMaster.UpdateAnnouncementDataVersionOnChange);
                                entityToInsert.SetNewFieldValue("UpdateSurveyDataVersionOnChange", entityInformationEntityMaster.UpdateSurveyDataVersionOnChange);
                                entityToInsert.SetNewFieldValue("UpdateEntertainmentDataVersionOnChange", entityInformationEntityMaster.UpdateEntertainmentDataVersionOnChange);
                                entityToInsert.SetNewFieldValue("UpdateAdvertisementDataVersionOnChange", entityInformationEntityMaster.UpdateAdvertisementDataVersionOnChange);

                                entityToInsert.Save();

                                // Add to collection for viewing/searchgin
                                entityInformationView.RelatedCollection.Add(entityToInsert);
                            }
                            catch
                            {
                                // we come here if we couldn't cat the entity because it was deleted. Need logic to delete
                            }
                        }
                    }
                    else if (entityInformationView.Count == 1 && update)
                    {
                        IEntity entityToUpdate = entityInformationView[0];

                        entityToUpdate.SetNewFieldValue("ShowFieldName", entityInformationEntityMaster.ShowFieldName);
                        entityToUpdate.SetNewFieldValue("FriendlyName", entityInformationEntityMaster.FriendlyName);
                        entityToUpdate.SetNewFieldValue("FriendlyNamePlural", entityInformationEntityMaster.FriendlyNamePlural);
                        entityToUpdate.SetNewFieldValue("HelpText", entityInformationEntityMaster.HelpText);
                        entityToUpdate.SetNewFieldValue("TitleAsBreadCrumbHierarchy", entityInformationEntityMaster.TitleAsBreadCrumbHierarchy);
                        entityToUpdate.SetNewFieldValue("DefaultEntityEditPage", entityInformationEntityMaster.DefaultEntityEditPage);
                        entityToUpdate.SetNewFieldValue("DefaultEntityCollectionPage", entityInformationEntityMaster.DefaultEntityCollectionPage);
                        entityToUpdate.SetNewFieldValue("ShowFieldNameBreadCrumb", entityInformationEntityMaster.ShowFieldNameBreadCrumb);

                        entityToUpdate.SetNewFieldValue("UpdateMenuDataVersionOnChange", entityInformationEntityMaster.UpdateMenuDataVersionOnChange);
                        entityToUpdate.SetNewFieldValue("UpdateCompanyDataVersionOnChange", entityInformationEntityMaster.UpdateCompanyDataVersionOnChange);
                        entityToUpdate.SetNewFieldValue("UpdatePosIntegrationInformationOnChange", entityInformationEntityMaster.UpdatePosIntegrationInformationOnChange);
                        entityToUpdate.SetNewFieldValue("UpdateDeliverypointDataVersionOnChange", entityInformationEntityMaster.UpdateDeliverypointDataVersionOnChange);
                        entityToUpdate.SetNewFieldValue("UpdateSurveyDataVersionOnChange", entityInformationEntityMaster.UpdateSurveyDataVersionOnChange);
                        entityToUpdate.SetNewFieldValue("UpdateAnnouncementDataVersionOnChange", entityInformationEntityMaster.UpdateAnnouncementDataVersionOnChange);
                        entityToUpdate.SetNewFieldValue("UpdateEntertainmentDataVersionOnChange", entityInformationEntityMaster.UpdateEntertainmentDataVersionOnChange);
                        entityToUpdate.SetNewFieldValue("UpdateAdvertisementDataVersionOnChange", entityInformationEntityMaster.UpdateAdvertisementDataVersionOnChange);

                        entityToUpdate.Save();
                    }
                }
            }
        }

        /// <summary>
        /// Refreshes EntityFieldInformation entities from the master database to the local database
        /// </summary>
        /// <param name="update">Flag which indicates whether existing field information should be updated</param>
        public static void RefreshEntityFieldInformationEntities(bool update)
        {
            IEntityCollection entityFieldInformationCollection = LLBLGenEntityCollectionUtil.GetEntityCollection("EntityFieldInformation");
            if (Instance.Empty(entityFieldInformationCollection))
            {
                throw new EmptyException("Variable 'entityFieldInformationCollection' is empty.");
            }
            else
            {
                // Get the default entity view from the collection
                IEntityView entityFieldInformationView = entityFieldInformationCollection.DefaultView;

                // Get the entity information collection from the master database
                EntityFieldInformationCollection entityFieldInformationCollectionMaster = new EntityFieldInformationCollection();
                entityFieldInformationCollectionMaster.GetMulti(null);

                // Create a refresh helper for deleting non updated entities
                RefreshLocalHelper refreshHelper = null;
                if (update)
                    refreshHelper = new RefreshLocalHelper(entityFieldInformationCollection);

                PredicateExpression filter = null;

                // Walk through the entity names
                for (int i = 0; i < entityFieldInformationCollectionMaster.Count; i++)
                {
                    EntityFieldInformationEntity entityFieldInformationEntityMaster = entityFieldInformationCollectionMaster[i];

                    string entityName = entityFieldInformationEntityMaster.EntityName;
                    string fieldName = entityFieldInformationEntityMaster.FieldName;

                    filter = LLBLGenFilterUtil.GetFieldCompareValueEqualPredicateExpression("EntityFieldInformation", new string[] { "EntityName", "FieldName" }, new object[] { entityName, fieldName });

                    entityFieldInformationView.Filter = filter;

                    // check count to see if we need to add
                    if (entityFieldInformationView.Count > 1)
                    {
                        throw new Dionysos.TechnicalException("Multiple EntityFieldInformation records found for entity name {0} and field name {1}", entityName, fieldName);
                    }
                    else if (entityFieldInformationView.Count == 0)
                    {
                        // Not found, add:
                        IEntity entityToInsert = DataFactory.EntityFactory.GetEntity("EntityFieldInformation") as IEntity;
                        if (Instance.Empty(entityToInsert))
                        {
                            throw new EmptyException("Could not initialize entity of entity name 'EntityFieldInformation'.");
                        }
                        else
                        {
                            // Set field values & save entity
                            entityToInsert.SetNewFieldValue("EntityName", entityName);
                            entityToInsert.SetNewFieldValue("FieldName", fieldName);
                            entityToInsert.SetNewFieldValue("FriendlyName", entityFieldInformationEntityMaster.FriendlyName);
                            entityToInsert.SetNewFieldValue("ShowOnDefaultGridView", entityFieldInformationEntityMaster.ShowOnDefaultGridView);
                            entityToInsert.SetNewFieldValue("DefaultDisplayPosition", entityFieldInformationEntityMaster.DefaultDisplayPosition);
                            entityToInsert.SetNewFieldValue("AllowShowOnGridView", entityFieldInformationEntityMaster.AllowShowOnGridView);
                            entityToInsert.SetNewFieldValue("HelpText", entityFieldInformationEntityMaster.HelpText);
                            entityToInsert.SetNewFieldValue("DisplayFormatString", entityFieldInformationEntityMaster.DisplayFormatString);
                            entityToInsert.SetNewFieldValue("EntityFieldType", entityFieldInformationEntityMaster.EntityFieldType);
                            entityToInsert.SetNewFieldValue("Type", entityFieldInformationEntityMaster.Type);
                            entityToInsert.SetNewFieldValue("ExcludeForUpdateCompanyDataVersionOnChange", entityFieldInformationEntityMaster.ExcludeForUpdateCompanyDataVersionOnChange);
                            entityToInsert.SetNewFieldValue("ExcludeForUpdateMenuDataVersionOnChange", entityFieldInformationEntityMaster.ExcludeForUpdateMenuDataVersionOnChange);
                            entityToInsert.SetNewFieldValue("ExcludeForUpdatePosIntegrationInformationVersionOnChange", entityFieldInformationEntityMaster.ExcludeForUpdatePosIntegrationInformationVersionOnChange);
                            entityToInsert.SetNewFieldValue("ExcludeForUpdateDeliverypointDataVersionOnChange", entityFieldInformationEntityMaster.ExcludeForUpdateDeliverypointDataVersionOnChange);
                            entityToInsert.SetNewFieldValue("ExcludeForUpdateSurveyDataVersionOnChange", entityFieldInformationEntityMaster.ExcludeForUpdateSurveyDataVersionOnChange);
                            entityToInsert.SetNewFieldValue("ExcludeForUpdateAnnouncementDataVersionOnChange", entityFieldInformationEntityMaster.ExcludeForUpdateAnnouncementDataVersionOnChange);
                            entityToInsert.SetNewFieldValue("ExcludeForUpdateEntertainmentDataVersionOnChange", entityFieldInformationEntityMaster.ExcludeForUpdateEntertainmentDataVersionOnChange);
                            entityToInsert.SetNewFieldValue("ExcludeForUpdateAdvertisementDataVersionOnChange", entityFieldInformationEntityMaster.ExcludeForUpdateAdvertisementDataVersionOnChange);
                            entityToInsert.Save();

                            // Add to collection for viewing/searchgin
                            entityFieldInformationView.RelatedCollection.Add(entityToInsert);
                        }
                    }
                    else if (entityFieldInformationView.Count == 1 && update)
                    {
                        IEntity entityToUpdate = entityFieldInformationView[0];

                        entityToUpdate.SetNewFieldValue("FriendlyName", entityFieldInformationEntityMaster.FriendlyName);
                        entityToUpdate.SetNewFieldValue("ShowOnDefaultGridView", entityFieldInformationEntityMaster.ShowOnDefaultGridView);
                        entityToUpdate.SetNewFieldValue("DefaultDisplayPosition", entityFieldInformationEntityMaster.DefaultDisplayPosition);
                        entityToUpdate.SetNewFieldValue("AllowShowOnGridView", entityFieldInformationEntityMaster.AllowShowOnGridView);
                        entityToUpdate.SetNewFieldValue("HelpText", entityFieldInformationEntityMaster.HelpText);
                        entityToUpdate.SetNewFieldValue("DisplayFormatString", entityFieldInformationEntityMaster.DisplayFormatString);
                        entityToUpdate.SetNewFieldValue("EntityFieldType", entityFieldInformationEntityMaster.EntityFieldType);
                        entityToUpdate.SetNewFieldValue("Type", entityFieldInformationEntityMaster.Type);
                        entityToUpdate.SetNewFieldValue("ExcludeForUpdateCompanyDataVersionOnChange", entityFieldInformationEntityMaster.ExcludeForUpdateCompanyDataVersionOnChange);
                        entityToUpdate.SetNewFieldValue("ExcludeForUpdateMenuDataVersionOnChange", entityFieldInformationEntityMaster.ExcludeForUpdateMenuDataVersionOnChange);
                        entityToUpdate.SetNewFieldValue("ExcludeForUpdatePosIntegrationInformationVersionOnChange", entityFieldInformationEntityMaster.ExcludeForUpdatePosIntegrationInformationVersionOnChange);
                        entityToUpdate.SetNewFieldValue("ExcludeForUpdateDeliverypointDataVersionOnChange", entityFieldInformationEntityMaster.ExcludeForUpdateDeliverypointDataVersionOnChange);
                        entityToUpdate.SetNewFieldValue("ExcludeForUpdateSurveyDataVersionOnChange", entityFieldInformationEntityMaster.ExcludeForUpdateSurveyDataVersionOnChange);
                        entityToUpdate.SetNewFieldValue("ExcludeForUpdateAnnouncementDataVersionOnChange", entityFieldInformationEntityMaster.ExcludeForUpdateAnnouncementDataVersionOnChange);
                        entityToUpdate.SetNewFieldValue("ExcludeForUpdateEntertainmentDataVersionOnChange", entityFieldInformationEntityMaster.ExcludeForUpdateEntertainmentDataVersionOnChange);
                        entityToUpdate.SetNewFieldValue("ExcludeForUpdateAdvertisementDataVersionOnChange", entityFieldInformationEntityMaster.ExcludeForUpdateAdvertisementDataVersionOnChange);
                        entityToUpdate.Save();

                        // Because if an entity hasn't changed it won't fire AfterSave or PropertyChanged we need to mark it 
                        // As updated to prevent it form being deleted by the refreshHelper
                        refreshHelper.MarkItemAsUpdated(entityToUpdate);
                    }
                }

                // Delete non updated items
                if (update)
                    refreshHelper.DeleteAllNonUpdatedItems();
            }
        }

        #endregion

        #endregion
    }
}