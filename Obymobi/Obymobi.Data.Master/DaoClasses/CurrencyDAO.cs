﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using Obymobi.Data.Master.EntityClasses;
using Obymobi.Data.Master.FactoryClasses;
using Obymobi.Data.Master.CollectionClasses;
using Obymobi.Data.Master.HelperClasses;
using Obymobi.Data.Master;
using SD.LLBLGen.Pro.ORMSupportClasses;
using SD.LLBLGen.Pro.DQE.SqlServer;


namespace Obymobi.Data.Master.DaoClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>General DAO class for the Currency Entity. It will perform database oriented actions for a entity of type 'CurrencyEntity'.</summary>
	public partial class CurrencyDAO : CommonDaoBase
	{
		/// <summary>CTor</summary>
		public CurrencyDAO() : base(InheritanceHierarchyType.None, "CurrencyEntity", new CurrencyEntityFactory())
		{
		}








		
		#region Custom DAO code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomDAOCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion
		
		#region Included Code

		#endregion
	}
}
