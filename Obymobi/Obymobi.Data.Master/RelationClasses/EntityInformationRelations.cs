﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data.Master;
using Obymobi.Data.Master.FactoryClasses;
using Obymobi.Data.Master.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.Master.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: EntityInformation. </summary>
	public partial class EntityInformationRelations
	{
		/// <summary>CTor</summary>
		public EntityInformationRelations()
		{
		}

		/// <summary>Gets all relations of the EntityInformationEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.RoleEntityRightsEntityUsingEntityInformationId);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between EntityInformationEntity and RoleEntityRightsEntity over the 1:n relation they have, using the relation between the fields:
		/// EntityInformation.EntityInformationId - RoleEntityRights.EntityInformationId
		/// </summary>
		public virtual IEntityRelation RoleEntityRightsEntityUsingEntityInformationId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "RoleEntityRights" , true);
				relation.AddEntityFieldPair(EntityInformationFields.EntityInformationId, RoleEntityRightsFields.EntityInformationId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("EntityInformationEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RoleEntityRightsEntity", false);
				return relation;
			}
		}


		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticEntityInformationRelations
	{
		internal static readonly IEntityRelation RoleEntityRightsEntityUsingEntityInformationIdStatic = new EntityInformationRelations().RoleEntityRightsEntityUsingEntityInformationId;

		/// <summary>CTor</summary>
		static StaticEntityInformationRelations()
		{
		}
	}
}
