﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data.Master;
using Obymobi.Data.Master.FactoryClasses;
using Obymobi.Data.Master.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.Master.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: RoleEntityRights. </summary>
	public partial class RoleEntityRightsRelations
	{
		/// <summary>CTor</summary>
		public RoleEntityRightsRelations()
		{
		}

		/// <summary>Gets all relations of the RoleEntityRightsEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.EntityInformationEntityUsingEntityInformationId);
			toReturn.Add(this.RoleEntityUsingRoleId);
			return toReturn;
		}

		#region Class Property Declarations



		/// <summary>Returns a new IEntityRelation object, between RoleEntityRightsEntity and EntityInformationEntity over the m:1 relation they have, using the relation between the fields:
		/// RoleEntityRights.EntityInformationId - EntityInformation.EntityInformationId
		/// </summary>
		public virtual IEntityRelation EntityInformationEntityUsingEntityInformationId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "EntityInformation", false);
				relation.AddEntityFieldPair(EntityInformationFields.EntityInformationId, RoleEntityRightsFields.EntityInformationId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("EntityInformationEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RoleEntityRightsEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between RoleEntityRightsEntity and RoleEntity over the m:1 relation they have, using the relation between the fields:
		/// RoleEntityRights.RoleId - Role.RoleId
		/// </summary>
		public virtual IEntityRelation RoleEntityUsingRoleId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Role", false);
				relation.AddEntityFieldPair(RoleFields.RoleId, RoleEntityRightsFields.RoleId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RoleEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("RoleEntityRightsEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticRoleEntityRightsRelations
	{
		internal static readonly IEntityRelation EntityInformationEntityUsingEntityInformationIdStatic = new RoleEntityRightsRelations().EntityInformationEntityUsingEntityInformationId;
		internal static readonly IEntityRelation RoleEntityUsingRoleIdStatic = new RoleEntityRightsRelations().RoleEntityUsingRoleId;

		/// <summary>CTor</summary>
		static StaticRoleEntityRightsRelations()
		{
		}
	}
}
