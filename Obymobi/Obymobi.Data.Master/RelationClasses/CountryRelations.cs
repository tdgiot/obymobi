﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using Obymobi.Data.Master;
using Obymobi.Data.Master.FactoryClasses;
using Obymobi.Data.Master.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.Master.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: Country. </summary>
	public partial class CountryRelations
	{
		/// <summary>CTor</summary>
		public CountryRelations()
		{
		}

		/// <summary>Gets all relations of the CountryEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.CurrencyEntityUsingCurrencyId);
			return toReturn;
		}

		#region Class Property Declarations



		/// <summary>Returns a new IEntityRelation object, between CountryEntity and CurrencyEntity over the m:1 relation they have, using the relation between the fields:
		/// Country.CurrencyId - Currency.CurrencyId
		/// </summary>
		public virtual IEntityRelation CurrencyEntityUsingCurrencyId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Currency", false);
				relation.AddEntityFieldPair(CurrencyFields.CurrencyId, CountryFields.CurrencyId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CurrencyEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("CountryEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticCountryRelations
	{
		internal static readonly IEntityRelation CurrencyEntityUsingCurrencyIdStatic = new CountryRelations().CurrencyEntityUsingCurrencyId;

		/// <summary>CTor</summary>
		static StaticCountryRelations()
		{
		}
	}
}
