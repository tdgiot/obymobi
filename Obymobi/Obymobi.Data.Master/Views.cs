﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using Dionysos;
using Dionysos.Data;
using Dionysos.Data.LLBLGen;
using Dionysos.Interfaces.Data;
using Obymobi.Data.Master.CollectionClasses;
using Obymobi.Data.Master.EntityClasses;
using Obymobi.Data.Master.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.Master
{
    public class Views
    {
        #region Fields

        static Views instance = null;

        #endregion

        #region Constructors

        /// <summary>
        /// Constructs an instance of the Views class if the instance has not been initialized yet
        /// </summary>
        public Views()
        {
            if (instance == null)
            {
                // first create the instance
                instance = new Views();
                // Then init the instance (the init needs the instance to fill it)
                instance.Init();
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Initializes the static Views instance
        /// </summary>
        private void Init()
        {
        }

        ///// <summary>
        ///// Creates Views for the EntityInformation entities 
        ///// </summary>
        //public static void CreateDefaultViews()
        //{
        //    ViewCollection viewCollection = new ViewCollection();
        //    viewCollection.GetMulti(null);

        //    RefreshLocalHelper refreshHelper = new RefreshLocalHelper(viewCollection);

        //    // Get the default entity view from the collection
        //    EntityView<ViewEntity> viewView = viewCollection.DefaultView;

        //    // Get the collection of EntityInformation
        //    EntityInformationCollection entityInformationCollection = new EntityInformationCollection();
        //    entityInformationCollection.GetMulti(null);

        //    for (int i = 0; i < entityInformationCollection.Count; i++)
        //    {
        //        // Get an EntityInformation entity from the collection
        //        EntityInformationEntity entityInformationEntity = entityInformationCollection[i];

        //        // Get the EntityInformationId 
        //        string entityName = entityInformationEntity.EntityName;

        //        // Create a filter for the ui element
        //        // and retrieve the ui elements
        //        PredicateExpression uiElementFilter = new PredicateExpression(UIElementFields.EntityName == entityName);
        //        UIElementCollection uiElementCollection = new UIElementCollection();
        //        uiElementCollection.GetMulti(uiElementFilter);

        //        for (int j = 0; j < uiElementCollection.Count; j++)
        //        {
        //            UIElementEntity uiElement = uiElementCollection[j];

        //            // Create and apply a filter in order to check 
        //            // whether a default View entity already exists
        //            PredicateExpression viewFilter = new PredicateExpression();
        //            viewFilter.Add(ViewFields.EntityName == entityName);
        //            viewFilter.Add(ViewFields.UiElementTypeNameFull == uiElement.TypeNameFull);
        //            viewView.Filter = viewFilter;

        //            if (viewView.Count == 0)
        //            {
        //                // No view found for the specified EntityInformation item
        //                // A default view needs to be created
        //                ViewEntity viewEntity = new ViewEntity();

        //                viewEntity.Name = "Standaard";
        //                viewEntity.EntityName = entityName;
        //                viewEntity.UiElementTypeNameFull = uiElement.TypeNameFull;
        //                viewEntity.Save();

        //                // Add to collection for viewing/searchgin
        //                viewView.RelatedCollection.Add(viewEntity);
        //            }
        //            else if (viewView.Count > 0)
        //            {
        //                refreshHelper.MarkItemAsUpdated(viewView[0]);
        //            }
        //        }
        //    }

        //    refreshHelper.DeleteAllNonUpdatedItems();
        //}

        ///// <summary>
        ///// Creates ViewItems for the EntityFieldInformation entities
        ///// </summary>
        ///// <param name="update">Flag which indicates whether existing view items should be updated</param>
        //public static void CreateDefaultViewItems(bool update)
        //{
        //    ViewItemCollection viewItemCollection = new ViewItemCollection();
        //    viewItemCollection.GetMulti(null);

        //    RefreshLocalHelper refreshHelper = new RefreshLocalHelper(viewItemCollection);

        //    // Get the default entity view from the collection
        //    EntityView<ViewItemEntity> viewItemView = viewItemCollection.DefaultView;

        //    // Get a collection of View entities
        //    ViewCollection viewCollection = new ViewCollection();
        //    viewCollection.GetMulti(null);

        //    // Loop through the views
        //    for (int j = 0; j < viewCollection.Count; j++)
        //    {
        //        // Get a ViewEntity from the collection
        //        ViewEntity viewEntity = viewCollection[j];
        //        string entityName = viewEntity.EntityName;

        //        IEntityCollection entityFieldInformationCollection;

        //        PredicateExpression filterEntityFieldsToShow = new PredicateExpression();
        //        filterEntityFieldsToShow.Add(LLBLGenFilterUtil.GetFieldCompareValueEqualPredicate("EntityFieldInformation", "ShowOnDefaultGridView", true));
        //        filterEntityFieldsToShow.Add(LLBLGenFilterUtil.GetFieldCompareValueEqualPredicate("EntityFieldInformation", "AllowShowOnGridView", true));
        //        SortExpression sort = new SortExpression(new SortClause(EntityFieldInformationFields.DefaultDisplayPosition, SortOperator.Ascending));

        //        // First retrieve all info for the entity, then apply the filter since some items might be altered by Custom items
        //        entityFieldInformationCollection = EntityInformationUtil.GetEntityFieldInformationEntityCollection(entityName) as IEntityCollection;
        //        IEntityView entityFieldInformationView = entityFieldInformationCollection.DefaultView;
        //        entityFieldInformationView.Sorter = sort;
        //        entityFieldInformationView.Filter = filterEntityFieldsToShow;

        //        // GK Old: entityFieldInformationCollection.GetMulti(filter2, 0, sort);

        //        PredicateExpression filter = null;

        //        // Retrieve an entity
        //        // and the entity information
        //        IEntity entity = DataFactory.EntityFactory.GetEntity(entityName) as IEntity;
        //        IEntityInformation entityInformation = EntityInformationUtil.GetEntityInformation(entityName);

        //        // Walk through the EntityFieldInformation in order
        //        // to check whether view items exists for the EntityFieldInformation items
        //        for (int i = 0; i < entityFieldInformationView.Count; i++)
        //        {
        //            // Retrieve the entity field information from the collection
        //            IEntity entityFieldInformation = entityFieldInformationView[i];

        //            // Get an entity field instance for the current field
        //            IEntityField entityField = entity.Fields[entityFieldInformation.Fields["FieldName"].CurrentValue.ToString()];

        //            string fieldName = string.Empty;

        //            // Friendly Name is set here, so it can be overridden later for related fields
        //            string friendlyName = entityFieldInformation.Fields["FriendlyName"].CurrentValue.ToString();

        //            if (entityField == null)
        //            {
        //                fieldName = entityFieldInformation.Fields["FieldName"].CurrentValue.ToString();
        //            }
        //            else if (entityField.IsForeignKey)
        //            {
        //                IEntityRelation relation = LLBLGenRelationUtil.GetRelation(entity, entityField.Name);
        //                if (Instance.Empty(relation))
        //                {
        //                    // This just means to much information is added in the database, a field that's deleted or renamed, no hard error needed
        //                }
        //                else
        //                {
        //                    IFieldPersistenceInfo pkPersistenceInfo = relation.GetPKFieldPersistenceInfo(0);

        //                    IEntityInformation relatedEntityInformation = EntityInformationUtil.GetEntityInformation(pkPersistenceInfo.SourceObjectName);
        //                    if (!Instance.Empty(relatedEntityInformation))
        //                    {
        //                        fieldName = string.Format("{0}.{1}", relation.MappedFieldName, relatedEntityInformation.ShowFieldName);
        //                    }

        //                    if (relatedEntityInformation.FriendlyName.Length > 0)
        //                        friendlyName = relatedEntityInformation.FriendlyName;
        //                }
        //            }
        //            else
        //            {
        //                fieldName = entityFieldInformation.Fields["FieldName"].CurrentValue.ToString();
        //            }

        //            filter = new PredicateExpression();
        //            filter.Add(ViewItemFields.EntityName == entityName);
        //            filter.Add(ViewItemFields.FieldName == fieldName);
        //            viewItemView.Filter = filter;

        //            if (viewItemView.Count == 0)
        //            {
        //                // No view found for the specified EntityInformation item
        //                // A default view needs to be created
        //                ViewItemEntity viewItemEntity = new ViewItemEntity();
        //                viewItemEntity.EntityName = entityName;
        //                viewItemEntity.FieldName = fieldName;
        //                viewItemEntity.FriendlyName = friendlyName;
        //                viewItemEntity.ShowOnGridView = (bool)entityFieldInformation.Fields["ShowOnDefaultGridView"].CurrentValue;
        //                viewItemEntity.DisplayPosition = (int)entityFieldInformation.Fields["DefaultDisplayPosition"].CurrentValue;
        //                viewItemEntity.Type = (string)entityFieldInformation.Fields["Type"].CurrentValue;
        //                viewItemEntity.UiElementTypeNameFull = viewEntity.UiElementTypeNameFull;
        //                viewItemEntity.Save();

        //                // Add to collection for viewing/searchgin
        //                viewItemView.RelatedCollection.Add(viewItemEntity);
        //            }
        //            else if (viewItemView.Count == 1 && update)
        //            {
        //                ViewItemEntity viewItemEntity = viewItemView[0];

        //                viewItemEntity.FriendlyName = friendlyName;
        //                viewItemEntity.ShowOnGridView = (bool)entityFieldInformation.Fields["ShowOnDefaultGridView"].CurrentValue;
        //                viewItemEntity.DisplayPosition = (int)entityFieldInformation.Fields["DefaultDisplayPosition"].CurrentValue;
        //                viewItemEntity.Type = (string)entityFieldInformation.Fields["Type"].CurrentValue;
        //                viewItemEntity.Save();

        //                refreshHelper.MarkItemAsUpdated(viewItemEntity);
        //            }
        //        }
        //    }

        //    if (update)
        //        refreshHelper.DeleteAllNonUpdatedItems();
        //}

        ///// <summary>
        ///// Refreshes the default views in the local database
        ///// </summary>
        //public static void RefreshDefaultViews()
        //{
        //    // Preload ViewCollection instance
        //    // and initialize the EntityView
        //    IEntityCollection viewCollection = LLBLGenEntityCollectionUtil.GetEntityCollection("View");
        //    if (Instance.Empty(viewCollection))
        //    {
        //        throw new EmptyException("Could not initialize entity collection for entity name 'View'.");
        //    }
        //    else
        //    {
        //        // Get the default entity view from the collection
        //        IEntityView viewView = viewCollection.DefaultView;

        //        // Get the referential constraints from the master database
        //        ViewCollection viewCollectionMaster = new ViewCollection();
        //        viewCollectionMaster.GetMulti(null);

        //        // Create a refresh helper for deleting non updated entities
        //        RefreshLocalHelper refreshHelper = null;
        //        refreshHelper = new RefreshLocalHelper(viewCollection);

        //        PredicateExpression filter = null;

        //        // Walk through the referential constraints
        //        for (int i = 0; i < viewCollectionMaster.Count; i++)
        //        {
        //            ViewEntity viewEntityMaster = viewCollectionMaster[i];

        //            string entityName = viewEntityMaster.EntityName;
        //            string uiElementTypeNameFull = viewEntityMaster.UiElementTypeNameFull;

        //            filter = LLBLGenFilterUtil.GetFieldCompareValueEqualPredicateExpression("View", new string[] { "EntityName", "UiElementTypeNameFull" }, new object[] { entityName, uiElementTypeNameFull });
        //            viewView.Filter = filter;

        //            // check count to see if we need to add
        //            if (viewView.Count > 1)
        //            {
        //                throw new Dionysos.TechnicalException("Multiple View records found for entity name {0} and UI element {1}", entityName, uiElementTypeNameFull);
        //            }
        //            else if (viewView.Count == 1)
        //            {
        //                viewView[0].SetNewFieldValue("Name", viewEntityMaster.Name);
        //                viewView[0].SetNewFieldValue("EntityName", entityName);
        //                viewView[0].SetNewFieldValue("UiElementTypeNameFull", uiElementTypeNameFull);
        //                viewView[0].Save();

        //                refreshHelper.MarkItemAsUpdated(viewView[0]);
        //            }
        //            else if (viewView.Count == 0)
        //            {
        //                // Not found, add:
        //                IEntity entityToInsert = DataFactory.EntityFactory.GetEntity("View") as IEntity;
        //                if (Instance.Empty(entityToInsert))
        //                {
        //                    throw new EmptyException("Could not initialize entity of entity name 'View'.");
        //                }
        //                else
        //                {
        //                    // Set field values & save entity
        //                    entityToInsert.SetNewFieldValue("Name", viewEntityMaster.Name);
        //                    entityToInsert.SetNewFieldValue("EntityName", entityName);
        //                    entityToInsert.SetNewFieldValue("UiElementTypeNameFull", uiElementTypeNameFull);
        //                    entityToInsert.Save();

        //                    // Add to collection for viewing/searchgin
        //                    viewView.RelatedCollection.Add(entityToInsert);
        //                }
        //            }
        //        }

        //        refreshHelper.DeleteAllNonUpdatedItems();
        //    }
        //}

        ///// <summary>
        ///// Refreshes the default views in the local database
        ///// </summary>
        //public static void RefreshDefaultViewItems(bool update)
        //{
        //    // Preload ViewItemCollection instance
        //    // and initialize the EntityView
        //    IEntityCollection viewItemCollection = LLBLGenEntityCollectionUtil.GetEntityCollection("ViewItem");
        //    if (Instance.Empty(viewItemCollection))
        //    {
        //        throw new EmptyException("Could not initialize entity collection for entity name 'ViewItem'.");
        //    }
        //    else
        //    {
        //        // Get the default entity view from the collection
        //        IEntityView viewItemView = viewItemCollection.DefaultView;

        //        // Get the view items from the master database
        //        ViewItemCollection viewItemCollectionMaster = new ViewItemCollection();
        //        viewItemCollectionMaster.GetMulti(null);

        //        // Create a refresh helper for deleting non updated entities
        //        RefreshLocalHelper refreshHelper = null;
        //        if (update)
        //            refreshHelper = new RefreshLocalHelper(viewItemCollection);

        //        PredicateExpression filter = null;

        //        // Walk through the view items
        //        for (int i = 0; i < viewItemCollectionMaster.Count; i++)
        //        {
        //            ViewItemEntity viewItemEntityMaster = viewItemCollectionMaster[i];

        //            string entityName = viewItemEntityMaster.EntityName;
        //            string fieldName = viewItemEntityMaster.FieldName;
        //            string uiElementTypeNameFull = viewItemEntityMaster.UiElementTypeNameFull;

        //            filter = LLBLGenFilterUtil.GetFieldCompareValueEqualPredicateExpression("ViewItem", new string[] { "EntityName", "FieldName", "UiElementTypeNameFull" }, new object[] { entityName, fieldName, uiElementTypeNameFull });
        //            viewItemView.Filter = filter;

        //            if (viewItemView.Count == 0)
        //            {
        //                // No view found for the specified EntityInformation item
        //                // A default view needs to be created
        //                IEntity viewItemEntity = DataFactory.EntityFactory.GetEntity("ViewItem") as IEntity;
        //                if (Instance.Empty(viewItemEntity))
        //                {
        //                    throw new EmptyException("Could not initialize entity of entity name 'ViewItem'.");
        //                }
        //                else
        //                {
        //                    viewItemEntity.SetNewFieldValue("EntityName", viewItemEntityMaster.EntityName);
        //                    viewItemEntity.SetNewFieldValue("FieldName", viewItemEntityMaster.FieldName);
        //                    viewItemEntity.SetNewFieldValue("FriendlyName", viewItemEntityMaster.FriendlyName);
        //                    viewItemEntity.SetNewFieldValue("ShowOnGridView", viewItemEntityMaster.ShowOnGridView);
        //                    viewItemEntity.SetNewFieldValue("DisplayPosition", viewItemEntityMaster.DisplayPosition);
        //                    viewItemEntity.SetNewFieldValue("Type", viewItemEntityMaster.Type);
        //                    viewItemEntity.SetNewFieldValue("UiElementTypeNameFull", viewItemEntityMaster.UiElementTypeNameFull);
        //                    viewItemEntity.Save();

        //                    // Add to collection for viewing/searchin
        //                    viewItemView.RelatedCollection.Add(viewItemEntity);
        //                }
        //            }
        //            else if (viewItemView.Count == 1 && update)
        //            {
        //                IEntity viewItemEntity = viewItemView[0];

        //                viewItemEntity.SetNewFieldValue("FriendlyName", viewItemEntityMaster.FriendlyName);
        //                viewItemEntity.SetNewFieldValue("ShowOnGridView", viewItemEntityMaster.ShowOnGridView);
        //                viewItemEntity.SetNewFieldValue("DisplayPosition", viewItemEntityMaster.DisplayPosition);
        //                viewItemEntity.SetNewFieldValue("UiElementTypeNameFull", viewItemEntityMaster.UiElementTypeNameFull);
        //                viewItemEntity.SetNewFieldValue("Type", viewItemEntityMaster.Type);
        //                viewItemEntity.Save();

        //                // Because if an entity hasn't changed it won't fire AfterSave or PropertyChanged we need to mark it 
        //                // As updated to prevent it form being deleted by the refreshHelper
        //                refreshHelper.MarkItemAsUpdated(viewItemEntity);
        //            }
        //        }

        //        // Delete non updated items
        //        if (update)
        //            refreshHelper.DeleteAllNonUpdatedItems();
        //    }
        //}

        /// <summary>
        /// Creates Views for the EntityInformation entities 
        /// </summary>
        public static void CreateDefaultViews()
        {
            IEntityCollection viewCollection = DataFactory.EntityCollectionFactory.GetEntityCollection("View") as IEntityCollection;
            Trace.Assert(!Instance.Empty(viewCollection), "Entity collection of type 'ViewCollection' could not be initialized.");

            viewCollection.GetMulti(null);

            RefreshLocalHelper refreshHelper = new RefreshLocalHelper(viewCollection);

            // Get the default entity view from the collection
            IEntityView viewView = viewCollection.DefaultView;
            Trace.Assert(!Instance.Empty(viewView), "EntityView instance could not be retrieved from ViewCollection instance.");

            // Get the collection of EntityInformation
            IEntityCollection entityInformationCollection = DataFactory.EntityCollectionFactory.GetEntityCollection("EntityInformation") as IEntityCollection;
            Trace.Assert(!Instance.Empty(entityInformationCollection), "Entity collection of type 'EntityInformation' could not be initialized.");

            entityInformationCollection.GetMulti(null);

            for (int i = 0; i < entityInformationCollection.Count; i++)
            {
                // Get an EntityInformation entity from the collection
                IEntity entityInformationEntity = entityInformationCollection[i];

                // Get the entity name 
                string entityName = entityInformationEntity.Fields["EntityName"].CurrentValue.ToString();

				#region GENERATE FOR UIELEMENT (NOT SUBELEMEMENTS)
				// GENERATE FOR UIELEMENT (NOT SUBELEMEMENTS)
                // Create a filter for the ui element
                // and retrieve the ui elements
                PredicateExpression uiElementFilter = LLBLGenFilterUtil.GetFieldCompareValueEqualPredicateExpression("UIElement", "EntityName", entityName);
                Trace.Assert(!Instance.Empty(uiElementFilter), string.Format("PredicateExpression could not be initialized for filter 'UIElement.EntityName == {0}'.", entityName));

                IEntityCollection uiElementCollection = DataFactory.EntityCollectionFactory.GetEntityCollection("UIElement") as IEntityCollection;
                Trace.Assert(!Instance.Empty(uiElementCollection), "Entity collection of type 'UIElementCollection' could not be initialized.");

                uiElementCollection.GetMulti(uiElementFilter);

				
                for (int j = 0; j < uiElementCollection.Count; j++)
                {
                    IEntity uiElement = uiElementCollection[j];

                    string typeNameFull = uiElement.Fields["TypeNameFull"].CurrentValue.ToString();

                    // Create and apply a filter in order to check 
                    // whether a default View entity already exists
                    PredicateExpression viewFilter = LLBLGenFilterUtil.GetFieldCompareValueEqualPredicateExpression("View", new string[] { "EntityName", "UiElementTypeNameFull" }, new object[] { entityName, typeNameFull });
                    viewView.Filter = viewFilter;

                    if (viewView.Count == 0)
                    {
                        // No view found for the specified EntityInformation item
                        // A default view needs to be created
                        IEntity viewEntity = DataFactory.EntityFactory.GetEntity("View") as IEntity;
                        Trace.Assert(!Instance.Empty(viewEntity), "Entity of type 'ViewEntity' could not be initialized.");

                        viewEntity.SetNewFieldValue("Name", "Standaard");
                        viewEntity.SetNewFieldValue("EntityName", entityName);
                        viewEntity.SetNewFieldValue("UiElementTypeNameFull", typeNameFull);
                        viewEntity.Save();

                        // Add to collection for viewing/searchgin
                        viewView.RelatedCollection.Add(viewEntity);
                    }
                    else if (viewView.Count > 0)
                    {
                        refreshHelper.MarkItemAsUpdated(viewView[0]);
                    }
                }
				#endregion

				#region GENERATE FOR SUBELEMENT

				// GENERATE FOR SUBELEMENT
                PredicateExpression uiElementSubPanelFilter = LLBLGenFilterUtil.GetFieldCompareValueEqualPredicateExpression("UIElementSubPanel", "EntityName", entityName);                
                IEntityCollection uiElementSubPanelCollection = DataFactory.EntityCollectionFactory.GetEntityCollection("UIElementSubPanel") as IEntityCollection;
				uiElementSubPanelCollection.GetMulti(uiElementSubPanelFilter);

				for (int j = 0; j < uiElementSubPanelCollection.Count; j++)
				{
					IEntity uiElementSubPanel = uiElementSubPanelCollection[j];
					string typeNameFull = uiElementSubPanel.Fields["TypeNameFull"].CurrentValue.ToString();
					PredicateExpression viewFilter = LLBLGenFilterUtil.GetFieldCompareValueEqualPredicateExpression("View", new string[] { "EntityName", "UiElementTypeNameFull" }, new object[] { entityName, typeNameFull });
					viewView.Filter = viewFilter;

					if (viewView.Count == 0)
					{
						IEntity viewEntity = DataFactory.EntityFactory.GetEntity("View") as IEntity;
						Trace.Assert(!Instance.Empty(viewEntity), "Entity of type 'ViewEntity' could not be initialized.");

						viewEntity.SetNewFieldValue("Name", "Standaard");
						viewEntity.SetNewFieldValue("EntityName", entityName);
						viewEntity.SetNewFieldValue("UiElementTypeNameFull", typeNameFull);
						viewEntity.Save();

						// Add to collection for viewing/searchgin
						viewView.RelatedCollection.Add(viewEntity);
					}
					else if (viewView.Count > 0)
					{
						refreshHelper.MarkItemAsUpdated(viewView[0]);
					}
				}

				#endregion
            }

            refreshHelper.DeleteAllNonUpdatedItems();
        }		

        /// <summary>
        /// Creates ViewItems for the EntityFieldInformation entities
        /// </summary>
        /// <param name="update">Flag which indicates whether existing view items should be updated</param>
        public static void CreateDefaultViewItems(bool update)
        {
            IEntityCollection viewItemCollection = DataFactory.EntityCollectionFactory.GetEntityCollection("ViewItem") as IEntityCollection;
            Trace.Assert(!Instance.Empty(viewItemCollection), "Entity collection of type 'ViewItemCollection' could not be initialized.");

            viewItemCollection.GetMulti(null);

			RefreshLocalHelper refreshHelper = new RefreshLocalHelper(viewItemCollection);

            // Get the default entity view from the collection
            IEntityView viewItemView = viewItemCollection.DefaultView;

            // Get a collection of View entities
            IEntityCollection viewCollection = DataFactory.EntityCollectionFactory.GetEntityCollection("View") as IEntityCollection;
            Trace.Assert(!Instance.Empty(viewCollection), "Entity collection of type 'ViewCollection' could not be initialized.");

            viewCollection.GetMulti(null);

			// Loop through the views
            for (int j = 0; j < viewCollection.Count; j++)
            {
                // Get a ViewEntity from the collection
                IEntity viewEntity = viewCollection[j];
                string entityName = viewEntity.Fields["EntityName"].CurrentValue.ToString();
                string uiElementTypeNameFull = viewEntity.Fields["UiElementTypeNameFull"].CurrentValue.ToString();

				IEntityCollection entityFieldInformationCollection;

                PredicateExpression filterEntityFieldsToShow = LLBLGenFilterUtil.GetFieldCompareValueEqualPredicateExpression("EntityFieldInformation", new string[] { "ShowOnDefaultGridView", "AllowShowOnGridView" }, new object[] { true, true });
                SortExpression sort = new SortExpression(new SortClause(EntityFieldInformationFields.DefaultDisplayPosition, SortOperator.Ascending));

				// First retrieve all info for the entity, then apply the filter since some items might be altered by Custom items
				entityFieldInformationCollection = EntityInformationUtil.GetEntityFieldInformationEntityCollection(entityName) as IEntityCollection;

				IEntityView entityFieldInformationView = entityFieldInformationCollection.DefaultView;
				entityFieldInformationView.Sorter = sort;
				entityFieldInformationView.Filter = filterEntityFieldsToShow;

                // GK Old: entityFieldInformationCollection.GetMulti(filter2, 0, sort);
                
                PredicateExpression filter = null;

                // Retrieve an entity
                // and the entity information
                IEntity entity = DataFactory.EntityFactory.GetEntity(entityName) as IEntity;
                IEntityInformation entityInformation = EntityInformationUtil.GetEntityInformation(entityName);

                // Walk through the EntityFieldInformation in order
                // to check whether view items exists for the EntityFieldInformation items
				for (int i = 0; i < entityFieldInformationView.Count; i++)
                {
                    // Retrieve the entity field information from the collection
					IEntity entityFieldInformation = entityFieldInformationView[i];

                    // Get an entity field instance for the current field
					IEntityField entityField = entity.Fields[entityFieldInformation.Fields["FieldName"].CurrentValue.ToString()];

                    string fieldName = string.Empty;
					string fieldType = string.Empty;			

                    if (entityField == null)
                    {
						fieldName = entityFieldInformation.Fields["FieldName"].CurrentValue.ToString();
						fieldType = entityFieldInformation.Fields["Type"].CurrentValue.ToString();
                    }
                    else if (entityField.IsForeignKey)
                    {
                        IEntityRelation relation = LLBLGenRelationUtil.GetRelation(entity, entityField.Name);
                        if (Instance.Empty(relation))
                        {
                            // This just means to much information is added in the database, a field that's deleted or renamed, no hard error needed
                        }
                        else
                        {
                            IFieldPersistenceInfo pkPersistenceInfo = relation.GetPKFieldPersistenceInfo(0);

                            IEntityInformation relatedEntityInformation = EntityInformationUtil.GetEntityInformation(pkPersistenceInfo.SourceObjectName);
                            if (!Instance.Empty(relatedEntityInformation))
                            {
                                if (!relatedEntityInformation.AllFields.ContainsKey(relatedEntityInformation.ShowFieldName))
                                {
                                    throw new Exception($"ShowFieldName '{relatedEntityInformation.ShowFieldName}' not present in dictionary for entity '{entityName}'");
                                }

                                fieldName = string.Format("{0}.{1}", relation.MappedFieldName, relatedEntityInformation.ShowFieldName);
								fieldType = relatedEntityInformation.AllFields[relatedEntityInformation.ShowFieldName].Type;
                            }						
                        }
                    }
                    else
                    {
						fieldName = entityFieldInformation.Fields["FieldName"].CurrentValue.ToString();
						fieldType = entityFieldInformation.Fields["Type"].CurrentValue.ToString();
                    }

                    filter = LLBLGenFilterUtil.GetFieldCompareValueEqualPredicateExpression("ViewItem",
						new string[] { "EntityName", "FieldName", "UiElementTypeNameFull" }, new object[] { entityName, fieldName, uiElementTypeNameFull });
                    viewItemView.Filter = filter;

                    if (viewItemView.Count == 0)
                    {
                        // No view found for the specified EntityInformation item
                        // A default view needs to be created
                        IEntity viewItemEntity = DataFactory.EntityFactory.GetEntity("ViewItem") as IEntity;
                        Trace.Assert(!Instance.Empty(viewItemEntity), "Entity of type 'ViewItemEntity' could not be initialized.");

						viewItemEntity.SetNewFieldValue("ViewId", viewEntity.Fields["ViewId"].CurrentValue);
                        viewItemEntity.SetNewFieldValue("EntityName", entityName);
                        viewItemEntity.SetNewFieldValue("FieldName", fieldName);                        
						viewItemEntity.SetNewFieldValue("ShowOnGridView", entityFieldInformation.Fields["ShowOnDefaultGridView"].CurrentValue);						
						viewItemEntity.SetNewFieldValue("DisplayPosition",entityFieldInformation.Fields["DefaultDisplayPosition"].CurrentValue);
						viewItemEntity.SetNewFieldValue("Type", fieldType);
                        viewItemEntity.SetNewFieldValue("UiElementTypeNameFull", uiElementTypeNameFull);
						viewItemEntity.SetNewFieldValue("DisplayFormatString", entityFieldInformation.Fields["DisplayFormatString"].CurrentValue);
                        viewItemEntity.Save();

                        // Add to collection for viewing/searchgin
                        viewItemView.RelatedCollection.Add(viewItemEntity);
                    }
                    else if (viewItemView.Count == 1 && update)
                    {
                        IEntity viewItemEntity = viewItemView[0];
						viewItemEntity.SetNewFieldValue("ViewId", viewEntity.Fields["ViewId"].CurrentValue);
                        viewItemEntity.SetNewFieldValue("ShowOnGridView", entityFieldInformation.Fields["ShowOnDefaultGridView"].CurrentValue);
                        viewItemEntity.SetNewFieldValue("DisplayPosition", entityFieldInformation.Fields["DefaultDisplayPosition"].CurrentValue);
                        viewItemEntity.SetNewFieldValue("Type", fieldType);
						viewItemEntity.SetNewFieldValue("DisplayFormatString", entityFieldInformation.Fields["DisplayFormatString"].CurrentValue);
                        viewItemEntity.Save();

						refreshHelper.MarkItemAsUpdated(viewItemEntity);
                    }
                }
            }

			if(update)
				refreshHelper.DeleteAllNonUpdatedItems();
        }        

        ///// <summary>
        ///// Refreshes the default views in the local database
        ///// </summary>
        //public static void RefreshDefaultViews()
        //{
        //    // Preload ViewCollection instance
        //    // and initialize the EntityView
        //    IEntityCollection viewCollection = DataFactory.EntityCollectionFactory.GetEntityCollection("View") as IEntityCollection;
        //    Trace.Assert(!Instance.Empty(viewCollection), "Entity collection of type 'ViewCollection' could not be initialized.");

        //    viewCollection.GetMulti(null);

        //    // Get the default entity view from the collection
        //    IEntityView viewView = viewCollection.DefaultView;

        //    // Get the referential constraints from the master database
        //    ViewCollection viewCollectionMaster = new ViewCollection();
        //    viewCollectionMaster.GetMulti(null);

        //    // Create a refresh helper for deleting non updated entities
        //    RefreshLocalHelper refreshHelper = null;				
        //    refreshHelper = new RefreshLocalHelper(viewCollection);

        //    PredicateExpression filter = null;

        //    // Walk through the referential constraints
        //    for (int i = 0; i < viewCollectionMaster.Count; i++)
        //    {
        //        ViewEntity viewEntityMaster = viewCollectionMaster[i];

        //        string entityName = viewEntityMaster.EntityName;
        //        string uiElementTypeNameFull = viewEntityMaster.UiElementTypeNameFull;

        //        filter = LLBLGenFilterUtil.GetFieldCompareValueEqualPredicateExpression("View", new string[] { "EntityName", "UiElementTypeNameFull" }, new object[] { entityName, uiElementTypeNameFull });
        //        viewView.Filter = filter;

        //        // check count to see if we need to add
        //        if (viewView.Count > 1)
        //        {
        //            throw new Dionysos.TechnicalException("Multiple View records found for entity name {0} and UI element {1}", entityName, uiElementTypeNameFull);
        //        }
        //        else if (viewView.Count == 1)
        //        {
        //            viewView[0].SetNewFieldValue("Name", viewEntityMaster.Name);
        //            viewView[0].SetNewFieldValue("EntityName", entityName);
        //            viewView[0].SetNewFieldValue("UiElementTypeNameFull", uiElementTypeNameFull);
        //            viewView[0].Save();

        //            refreshHelper.MarkItemAsUpdated(viewView[0]);
        //        }
        //        else if (viewView.Count == 0)
        //        {
        //            // Not found, add:
        //            IEntity entityToInsert = DataFactory.EntityFactory.GetEntity("View") as IEntity;
        //            if (Instance.Empty(entityToInsert))
        //            {
        //                throw new EmptyException("Could not initialize entity of entity name 'View'.");
        //            }
        //            else
        //            {
        //                // Set field values & save entity
        //                entityToInsert.SetNewFieldValue("Name", viewEntityMaster.Name);
        //                entityToInsert.SetNewFieldValue("EntityName", entityName);
        //                entityToInsert.SetNewFieldValue("UiElementTypeNameFull", uiElementTypeNameFull);
        //                entityToInsert.Save();

        //                // Add to collection for viewing/searchgin
        //                viewView.RelatedCollection.Add(entityToInsert);
        //            }
        //        }
        //    }

        //    refreshHelper.DeleteAllNonUpdatedItems();
        //}

        ///// <summary>
        ///// Refreshes the default views in the local database
        ///// </summary>
        //public static void RefreshDefaultViewItems(bool update)
        //{
        //    // Preload ViewItemCollection instance
        //    // and initialize the EntityView
        //    IEntityCollection viewItemCollection = LLBLGenEntityCollectionUtil.GetEntityCollection("ViewItem");
        //    if (Instance.Empty(viewItemCollection))
        //    {
        //        throw new EmptyException("Could not initialize entity collection for entity name 'ViewItem'.");
        //    }
        //    else
        //    {
        //        // Get the default entity view from the collection
        //        IEntityView viewItemView = viewItemCollection.DefaultView;

        //        // Get the view items from the master database
        //        ViewItemCollection viewItemCollectionMaster = new ViewItemCollection();
        //        viewItemCollectionMaster.GetMulti(null);

        //        // Create a refresh helper for deleting non updated entities
        //        RefreshLocalHelper refreshHelper = null;
        //        if(update)
        //            refreshHelper = new RefreshLocalHelper(viewItemCollection);

        //        PredicateExpression filter = null;

        //        // Walk through the view items
        //        for (int i = 0; i < viewItemCollectionMaster.Count; i++)
        //        {
        //            ViewItemEntity viewItemEntityMaster = viewItemCollectionMaster[i];

        //            string entityName = viewItemEntityMaster.EntityName;
        //            string fieldName = viewItemEntityMaster.FieldName;
        //            string uiElementTypeNameFull = viewItemEntityMaster.UiElementTypeNameFull;

        //            filter = LLBLGenFilterUtil.GetFieldCompareValueEqualPredicateExpression("ViewItem", new string[] { "EntityName", "FieldName", "UiElementTypeNameFull" }, new object[] { entityName, fieldName, uiElementTypeNameFull });
        //            viewItemView.Filter = filter;

        //            if (viewItemView.Count == 0)
        //            {
        //                // No view found for the specified EntityInformation item
        //                // A default view needs to be created
        //                IEntity viewItemEntity = DataFactory.EntityFactory.GetEntity("ViewItem") as IEntity;
        //                if (Instance.Empty(viewItemEntity))
        //                {
        //                    throw new EmptyException("Could not initialize entity of entity name 'ViewItem'.");
        //                }
        //                else
        //                {
        //                    viewItemEntity.SetNewFieldValue("EntityName", viewItemEntityMaster.EntityName);
        //                    viewItemEntity.SetNewFieldValue("FieldName", viewItemEntityMaster.FieldName);
        //                    viewItemEntity.SetNewFieldValue("FriendlyName", viewItemEntityMaster.FriendlyName);
        //                    viewItemEntity.SetNewFieldValue("ShowOnGridView", viewItemEntityMaster.ShowOnGridView);
        //                    viewItemEntity.SetNewFieldValue("DisplayPosition", viewItemEntityMaster.DisplayPosition);
        //                    viewItemEntity.SetNewFieldValue("Type", viewItemEntityMaster.Type);
        //                    viewItemEntity.SetNewFieldValue("UiElementTypeNameFull", viewItemEntityMaster.UiElementTypeNameFull);
        //                    viewItemEntity.Save();

        //                    // Add to collection for viewing/searchin
        //                    viewItemView.RelatedCollection.Add(viewItemEntity);
        //                }
        //            }
        //            else if (viewItemView.Count == 1 && update)
        //            {
        //                IEntity viewItemEntity = viewItemView[0];

        //                viewItemEntity.SetNewFieldValue("FriendlyName", viewItemEntityMaster.FriendlyName);
        //                viewItemEntity.SetNewFieldValue("ShowOnGridView", viewItemEntityMaster.ShowOnGridView);
        //                viewItemEntity.SetNewFieldValue("DisplayPosition", viewItemEntityMaster.DisplayPosition);
        //                viewItemEntity.SetNewFieldValue("UiElementTypeNameFull", viewItemEntityMaster.UiElementTypeNameFull);
        //                viewItemEntity.SetNewFieldValue("Type", viewItemEntityMaster.Type);						
        //                viewItemEntity.Save();

        //                // Because if an entity hasn't changed it won't fire AfterSave or PropertyChanged we need to mark it 
        //                // As updated to prevent it form being deleted by the refreshHelper
        //                refreshHelper.MarkItemAsUpdated(viewItemEntity);
        //            }
        //        }

        //        // Delete non updated items
        //        if(update)
        //            refreshHelper.DeleteAllNonUpdatedItems();
        //    }
        //}

        #endregion
    }
}