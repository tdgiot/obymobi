﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dionysos;
using Dionysos.Data.LLBLGen;
using Obymobi.Data.Master.CollectionClasses;
using Obymobi.Data.Master.EntityClasses;
using Obymobi.Data.Master.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System.Collections;
using System.Diagnostics;

namespace Obymobi.Data.Master
{
    /// <summary>
    /// Class for mutating Globalization Data	
    /// </summary>
    public class GlobalizationDataHelper
    {
        private string languageCode;

        // Generate Globalization (Generates all items for specified language code), NL = System Language for Meta Data)		
        public void UpdateGlobalizationDataInMaster(string languageCode)
        {
            if (languageCode.Length != 2)
                throw new Dionysos.TechnicalException("languageCode must be a 2 character string, '{0}' is invalid", languageCode);
            else
                this.languageCode = languageCode;

            // All 'Auto-Learned' translations from this local instance should be transferred to the MAster
            this.SynchronizeTranslations();
            this.UpdateDataForModulesInMaster();
            this.UpdateDataForUIElementsInMaster();
            this.UpdateDataForEntityInformationInMaster();
            this.UpdateDataForEntityFieldInformationInMaster();
        }

        public void SynchronizeTranslations()
        {
            // Retrieve all Translations from the Master
            PredicateExpression filterMasterTranslation = new PredicateExpression();
            filterMasterTranslation.Add(TranslationFields.LanguageCode == this.languageCode);

            TranslationCollection masterTranslations = new TranslationCollection();
            masterTranslations.GetMulti(null);
            var masterTranslationsView = masterTranslations.DefaultView;

            // Retrieve all local translations that are created locally (not originated from Master).            
            PredicateExpression filterLocalTranslations = new PredicateExpression();
            filterLocalTranslations.Add(LLBLGenFilterUtil.GetFieldCompareValueEqualPredicate("Translation", TranslationFields.LocallyCreated.Name, true));
            filterLocalTranslations.Add(LLBLGenFilterUtil.GetFieldCompareValueEqualPredicate("Translation", TranslationFields.LanguageCode.Name, this.languageCode));

            IEntityCollection localTranslations = LLBLGenEntityCollectionUtil.GetEntityCollection("Translation", filterLocalTranslations);

            foreach (IEntity localTranslation in localTranslations)
            {
                // Check it's already available in the master:
                filterMasterTranslation = new PredicateExpression();

                // GK: This is a LIKE operator!!! Just so you know, not case sensitive.
                filterMasterTranslation.Add(TranslationFields.TranslationKey % localTranslation.Fields[TranslationFields.TranslationKey.Name].CurrentValue.ToString());

                masterTranslationsView.Filter = filterMasterTranslation;

                // Check if it's already available
                try
                {
                    if (masterTranslationsView.Count == 0)
                    {
                        // Create
                        TranslationEntity masterTranslation = new TranslationEntity();
                        LLBLGenEntityUtil.CopyFields(localTranslation, masterTranslation);
                        masterTranslation.LocallyCreated = false;
                        masterTranslation.Save();
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception(string.Format("Problem saving to Master: {0}", localTranslation.Fields[TranslationFields.TranslationKey.Name].CurrentValue.ToString()), ex);
                }

                // Remove the local translation, will be retrieved from master back again later.
                localTranslation.Delete();
            }

        }

        public void UpdateGlobalizationDataLocallyFromMaster(string languageCode)
        {
            // Retrieve all Translations from the Master
            this.languageCode = languageCode;
            PredicateExpression filterMasterTranslation = new PredicateExpression();
            filterMasterTranslation.Add(TranslationFields.LanguageCode == this.languageCode);

            TranslationCollection masterTranslations = new TranslationCollection();
            masterTranslations.GetMulti(filterMasterTranslation);

            // Retrieve all Translations from Local
            // Retrieve all local translations that are created locally (not originated from Master).            
            PredicateExpression filterLocalTranslations = new PredicateExpression();
            filterLocalTranslations.Add(LLBLGenFilterUtil.GetFieldCompareValueEqualPredicate("Translation", TranslationFields.LanguageCode.Name, this.languageCode));
            IEntityCollection localTranslations = LLBLGenEntityCollectionUtil.GetEntityCollection("Translation", filterLocalTranslations);
            var localTranslationsView = localTranslations.DefaultView;

            foreach (var masterTranslation in masterTranslations)
            {
                IEntity localTranslation = null;

                filterLocalTranslations = new PredicateExpression();
                filterLocalTranslations.Add(LLBLGenFilterUtil.GetFieldCompareValueEqualPredicate("Translation", TranslationFields.TranslationKey.Name, masterTranslation.TranslationKey));

                localTranslationsView.Filter = filterLocalTranslations;

                if (localTranslationsView.Count == 0)
                {
                    localTranslation = DataFactory.EntityFactory.GetEntity("Translation") as IEntity;
                    LLBLGenEntityUtil.CopyFields(masterTranslation, localTranslation, new ArrayList());
                }
                else if (localTranslationsView.Count == 1)
                {
                    localTranslation = localTranslationsView[0];
                }
                else
                    throw new Dionysos.FunctionalException("Multiple local Translations found for TranslationKey: '{0}'", masterTranslation.TranslationKey);

                //for (int i = 0; i < localTranslations.Count; i++)
                //{
                //    localTranslation = localTranslations[i] as IEntity;
                //    if (masterTranslation.TranslationKey == localTranslation.Fields[TranslationFields.TranslationKey.Name].CurrentValue.ToString())
                //    {
                //        // Found!				
                //        break;
                //    }
                //    else
                //        localTranslation = null;
                //}

                //// Not found, create
                //if (localTranslation == null)
                //{
                //    localTranslation = DataFactory.EntityFactory.GetEntity("Translation") as IEntity;
                //    LLBLGenEntityUtil.CopyFields(masterTranslation, localTranslation, new ArrayList());
                //}

                if (masterTranslation.TranslationValue.Length > 0)
                    localTranslation.SetNewFieldValue("TranslationValue", masterTranslation.TranslationValue);
                else
                    localTranslation.SetNewFieldValue("TranslationValue", null);

                // Just to be sure:
                localTranslation.SetNewFieldValue("LocallyCreated", false);

                // If IsNew or  new value, update & save
                localTranslation.Save();

            }
        }

        // Generate for Modules
        private void UpdateDataForModulesInMaster()
        {
            // Retrieve all Modules
            ModuleCollection modules = new ModuleCollection();
            modules.GetMulti(null);

            // Retrieve all Module translations
            var translations = this.GetTranslations("Module.%." + ModuleFields.NameFull.Name);

            // Verify for each module if a translation is available, if not create, if found, remove from collection.
            this.UpdateTranslations(translations, modules, ModuleFields.NameFull);
        }

        // Generate for UIElements
        private void UpdateDataForUIElementsInMaster()
        {
            // Retrieve all UIElements
            UIElementCollection uielements = new UIElementCollection();

            // GK couldnt get this working
            //PredicateExpression filterTodos = new PredicateExpression();
            //filterTodos.Add(UIElementFields.ModuleNameSystem % "TODO:");
            //filterTodos.AddWithOr(UIElementFields.NameSystem % "TODO:");

            //PredicateExpression filter = new PredicateExpression(filterTodos);
            //filter.Negate = true;

            uielements.GetMulti(null);

            // GK Pragmatic
            for (int i = uielements.Count - 1; i >= 0; i--)
            {
                if (uielements[i].ModuleNameSystem.Contains("TODO") || uielements[i].NameSystem.Contains("TODO"))
                    uielements.RemoveAt(i);
            }

            //foreach (var ui in uielements)
            //{
            //    Debug.WriteLine(string.Format("{0}.{1}", ui.ModuleNameSystem, ui.NameSystem));
            //}

            // Retrieve all UIElement Translations
            var translations = this.GetTranslations("UIElement.%." + UIElementFields.NameFull.Name);

            //foreach (var t in translations)
            //{
            //    Debug.WriteLine(string.Format("{0}", t.TranslationKey));
            //}

            this.UpdateTranslations(translations, uielements, UIElementFields.NameFull);
        }

        // Generate for EntityInformation
        private void UpdateDataForEntityInformationInMaster()
        {
            // Retrieve all UIElements
            EntityInformationCollection entityInformations = new EntityInformationCollection();
            entityInformations.GetMulti(null);

            // Retrieve all UIElement Translations
            var translations = this.GetTranslations("EntityInformation.%." + EntityInformationFields.FriendlyName.Name);
            this.UpdateTranslations(translations, entityInformations, EntityInformationFields.FriendlyName);

            translations = this.GetTranslations("EntityInformation.%." + EntityInformationFields.FriendlyNamePlural.Name);
            this.UpdateTranslations(translations, entityInformations, EntityInformationFields.FriendlyNamePlural);
        }

        // Generate for EntityFieldInformation
        private void UpdateDataForEntityFieldInformationInMaster()
        {
            EntityFieldInformationCollection entityFields = new EntityFieldInformationCollection();

            // Exclude all Created, Updated, CreatedBy, UpdatedBy, Archived, etc. Fields
            PredicateExpression filter = new PredicateExpression();
            filter.Add(new FieldLikePredicate(EntityFieldInformationFields.FieldName, "Created", true));
            filter.Add(new FieldLikePredicate(EntityFieldInformationFields.FieldName, "CreatedBy", true));
            filter.Add(new FieldLikePredicate(EntityFieldInformationFields.FieldName, "Updated", true));
            filter.Add(new FieldLikePredicate(EntityFieldInformationFields.FieldName, "UpdatedBy", true));
            filter.Add(new FieldLikePredicate(EntityFieldInformationFields.FieldName, "Deleted", true));
            filter.Add(new FieldLikePredicate(EntityFieldInformationFields.FieldName, "Archived", true));
            entityFields.GetMulti(filter);

            // Retrieve all EntityField Translations
            var translations = this.GetTranslations("EntityFieldInformation.%." + EntityFieldInformationFields.FriendlyName.Name);
            this.UpdateTranslations(translations, entityFields, EntityFieldInformationFields.FriendlyName);
        }

        private void UpdateTranslations(EntityView<TranslationEntity> translations, IEntityCollection entityCollection, IEntityField field)
        {
            PredicateExpression filter;
            string translationkey;
            foreach (var entity in entityCollection)
            {
                translationkey = this.GetTranslationKey((IEntity)entity, field);
                filter = new PredicateExpression();
                filter.Add(TranslationFields.TranslationKey == translationkey);
                filter.Add(TranslationFields.LanguageCode == this.languageCode);
                translations.Filter = filter;

                if (translations.Count == 1)
                {
                    // Remove, already exists
                    //Debug.WriteLine("translations.Count == 1 for: " + translationkey);
                    translations.RelatedCollection.Remove(translations[0]);
                }
                else if (translations.Count == 0)
                {
                    // Create, is new
                    //Debug.WriteLine("translations.Count == 0 for: " + translationkey);
                    try
                    {
                        TranslationEntity translation = this.CreateTranslation((IEntity)entity, field, this.languageCode, string.Empty);
                        if (translation != null)
                        {
                            translation.Refetch();
                            translations.RelatedCollection.Add(translation);
                        }
                    }
                    catch (Exception ex)
                    {
                        string message = string.Format("The inner exception most likely caused by a (errornous) double entry of '{0}' in de source datatable in de Master db.", translationkey);
                        Exception ex2 = new Exception(message, ex);
                        throw ex2;
                    }
                }
                else
                {
                    // Multiple found, error!
                    //Debug.WriteLine("translations.Count > 1 for: " + translationkey);
                    throw new Dionysos.TechnicalException("It's not allowed to have multiple translations for one translationkey: '{0}'", translationkey);
                }
            }

            foreach (var translation in translations)
                translation.Delete();
        }

        private EntityView<TranslationEntity> GetTranslations(string likePredicate)
        {
            TranslationCollection translations = new TranslationCollection();

            PredicateExpression filterTranslations = new PredicateExpression();
            filterTranslations.Add(TranslationFields.TranslationKey % likePredicate);
            filterTranslations.Add(TranslationFields.LanguageCode % this.languageCode);

            translations.GetMulti(filterTranslations);

            return translations.DefaultView;
        }

        private TranslationEntity CreateTranslation(IEntity entity, IEntityField field, string languageCode, string translationValue)
        {
            TranslationEntity t = new TranslationEntity();
            try
            {
                t.TranslationKey = GetTranslationKey(entity, field);

                // Generic properties, non entitytype specific
                t.LanguageCode = languageCode;
                if (translationValue.Length > 0)
                    t.TranslationValue = translationValue;

                t.Save();
            }
            catch (Exception ex)
            {
                throw new Dionysos.TechnicalException(ex, "Error while saving Translation for EntityType '{0}', Postfix '{1}', TranslationKey '{2}', Language '{3}', Value '{4}'\r\n\r\nError: {5}",
                            entity.GetType().ToString(), field.Name, t.TranslationKey, languageCode, translationValue, ex.Message);
            }

            return t;
        }

        /// <summary>
        /// Keep this method in sync with EVAdmin/App_Data/Globalization/GlobalizationHelper
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="postFix"></param>
        /// <returns></returns>
        private string GetTranslationKey(IEntity entity, IEntityField field)
        {
            string toReturn = string.Empty;
            if (entity is ModuleEntity)
            {
                ModuleEntity module = entity as ModuleEntity;
                toReturn = string.Format("Module.{0}", module.NameSystem);
            }
            else if (entity is UIElementEntity)
            {
                UIElementEntity uielement = entity as UIElementEntity;
                toReturn = string.Format("UIElement.{0}.{1}", uielement.ModuleNameSystem, uielement.NameSystem);
            }
            else if (entity is EntityInformationEntity)
            {
                EntityInformationEntity einfo = entity as EntityInformationEntity;
                toReturn = string.Format("EntityInformation.{0}", einfo.EntityName);
            }
            else if (entity is EntityFieldInformationEntity)
            {
                EntityFieldInformationEntity efinfo = entity as EntityFieldInformationEntity;
                toReturn = string.Format("EntityFieldInformation.{0}.{1}", efinfo.EntityName, efinfo.FieldName);
            }
            else
                throw new Dionysos.TechnicalException("GlobalizationHelper.GetTranslationKey has no implementation for Entities of the type: " + entity.GetType().ToString());

            toReturn += "." + field.Name;

            return toReturn;
        }
    }
}
