﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.Master.FactoryClasses;
using Obymobi.Data.Master;

namespace Obymobi.Data.Master.HelperClasses
{
	/// <summary>Field Creation Class for entity CountryEntity</summary>
	public partial class CountryFields
	{
		/// <summary>Creates a new CountryEntity.CountryId field instance</summary>
		public static EntityField CountryId
		{
			get { return (EntityField)EntityFieldFactory.Create(CountryFieldIndex.CountryId);}
		}
		/// <summary>Creates a new CountryEntity.Name field instance</summary>
		public static EntityField Name
		{
			get { return (EntityField)EntityFieldFactory.Create(CountryFieldIndex.Name);}
		}
		/// <summary>Creates a new CountryEntity.Code field instance</summary>
		public static EntityField Code
		{
			get { return (EntityField)EntityFieldFactory.Create(CountryFieldIndex.Code);}
		}
		/// <summary>Creates a new CountryEntity.CodeAlpha3 field instance</summary>
		public static EntityField CodeAlpha3
		{
			get { return (EntityField)EntityFieldFactory.Create(CountryFieldIndex.CodeAlpha3);}
		}
		/// <summary>Creates a new CountryEntity.CultureName field instance</summary>
		public static EntityField CultureName
		{
			get { return (EntityField)EntityFieldFactory.Create(CountryFieldIndex.CultureName);}
		}
		/// <summary>Creates a new CountryEntity.CurrencyId field instance</summary>
		public static EntityField CurrencyId
		{
			get { return (EntityField)EntityFieldFactory.Create(CountryFieldIndex.CurrencyId);}
		}
		/// <summary>Creates a new CountryEntity.Created field instance</summary>
		public static EntityField Created
		{
			get { return (EntityField)EntityFieldFactory.Create(CountryFieldIndex.Created);}
		}
		/// <summary>Creates a new CountryEntity.CreatedBy field instance</summary>
		public static EntityField CreatedBy
		{
			get { return (EntityField)EntityFieldFactory.Create(CountryFieldIndex.CreatedBy);}
		}
		/// <summary>Creates a new CountryEntity.Updated field instance</summary>
		public static EntityField Updated
		{
			get { return (EntityField)EntityFieldFactory.Create(CountryFieldIndex.Updated);}
		}
		/// <summary>Creates a new CountryEntity.UpdatedBy field instance</summary>
		public static EntityField UpdatedBy
		{
			get { return (EntityField)EntityFieldFactory.Create(CountryFieldIndex.UpdatedBy);}
		}
		/// <summary>Creates a new CountryEntity.CreatedUTC field instance</summary>
		public static EntityField CreatedUTC
		{
			get { return (EntityField)EntityFieldFactory.Create(CountryFieldIndex.CreatedUTC);}
		}
		/// <summary>Creates a new CountryEntity.UpdatedUTC field instance</summary>
		public static EntityField UpdatedUTC
		{
			get { return (EntityField)EntityFieldFactory.Create(CountryFieldIndex.UpdatedUTC);}
		}
	}

	/// <summary>Field Creation Class for entity CurrencyEntity</summary>
	public partial class CurrencyFields
	{
		/// <summary>Creates a new CurrencyEntity.CurrencyId field instance</summary>
		public static EntityField CurrencyId
		{
			get { return (EntityField)EntityFieldFactory.Create(CurrencyFieldIndex.CurrencyId);}
		}
		/// <summary>Creates a new CurrencyEntity.Name field instance</summary>
		public static EntityField Name
		{
			get { return (EntityField)EntityFieldFactory.Create(CurrencyFieldIndex.Name);}
		}
		/// <summary>Creates a new CurrencyEntity.Symbol field instance</summary>
		public static EntityField Symbol
		{
			get { return (EntityField)EntityFieldFactory.Create(CurrencyFieldIndex.Symbol);}
		}
		/// <summary>Creates a new CurrencyEntity.CodeIso4217 field instance</summary>
		public static EntityField CodeIso4217
		{
			get { return (EntityField)EntityFieldFactory.Create(CurrencyFieldIndex.CodeIso4217);}
		}
		/// <summary>Creates a new CurrencyEntity.Created field instance</summary>
		public static EntityField Created
		{
			get { return (EntityField)EntityFieldFactory.Create(CurrencyFieldIndex.Created);}
		}
		/// <summary>Creates a new CurrencyEntity.CreatedBy field instance</summary>
		public static EntityField CreatedBy
		{
			get { return (EntityField)EntityFieldFactory.Create(CurrencyFieldIndex.CreatedBy);}
		}
		/// <summary>Creates a new CurrencyEntity.Updated field instance</summary>
		public static EntityField Updated
		{
			get { return (EntityField)EntityFieldFactory.Create(CurrencyFieldIndex.Updated);}
		}
		/// <summary>Creates a new CurrencyEntity.UpdatedBy field instance</summary>
		public static EntityField UpdatedBy
		{
			get { return (EntityField)EntityFieldFactory.Create(CurrencyFieldIndex.UpdatedBy);}
		}
		/// <summary>Creates a new CurrencyEntity.CreatedUTC field instance</summary>
		public static EntityField CreatedUTC
		{
			get { return (EntityField)EntityFieldFactory.Create(CurrencyFieldIndex.CreatedUTC);}
		}
		/// <summary>Creates a new CurrencyEntity.UpdatedUTC field instance</summary>
		public static EntityField UpdatedUTC
		{
			get { return (EntityField)EntityFieldFactory.Create(CurrencyFieldIndex.UpdatedUTC);}
		}
	}

	/// <summary>Field Creation Class for entity EntityFieldInformationEntity</summary>
	public partial class EntityFieldInformationFields
	{
		/// <summary>Creates a new EntityFieldInformationEntity.EntityFieldInformationId field instance</summary>
		public static EntityField EntityFieldInformationId
		{
			get { return (EntityField)EntityFieldFactory.Create(EntityFieldInformationFieldIndex.EntityFieldInformationId);}
		}
		/// <summary>Creates a new EntityFieldInformationEntity.EntityName field instance</summary>
		public static EntityField EntityName
		{
			get { return (EntityField)EntityFieldFactory.Create(EntityFieldInformationFieldIndex.EntityName);}
		}
		/// <summary>Creates a new EntityFieldInformationEntity.FieldName field instance</summary>
		public static EntityField FieldName
		{
			get { return (EntityField)EntityFieldFactory.Create(EntityFieldInformationFieldIndex.FieldName);}
		}
		/// <summary>Creates a new EntityFieldInformationEntity.FriendlyName field instance</summary>
		public static EntityField FriendlyName
		{
			get { return (EntityField)EntityFieldFactory.Create(EntityFieldInformationFieldIndex.FriendlyName);}
		}
		/// <summary>Creates a new EntityFieldInformationEntity.ShowOnDefaultGridView field instance</summary>
		public static EntityField ShowOnDefaultGridView
		{
			get { return (EntityField)EntityFieldFactory.Create(EntityFieldInformationFieldIndex.ShowOnDefaultGridView);}
		}
		/// <summary>Creates a new EntityFieldInformationEntity.DefaultDisplayPosition field instance</summary>
		public static EntityField DefaultDisplayPosition
		{
			get { return (EntityField)EntityFieldFactory.Create(EntityFieldInformationFieldIndex.DefaultDisplayPosition);}
		}
		/// <summary>Creates a new EntityFieldInformationEntity.AllowShowOnGridView field instance</summary>
		public static EntityField AllowShowOnGridView
		{
			get { return (EntityField)EntityFieldFactory.Create(EntityFieldInformationFieldIndex.AllowShowOnGridView);}
		}
		/// <summary>Creates a new EntityFieldInformationEntity.HelpText field instance</summary>
		public static EntityField HelpText
		{
			get { return (EntityField)EntityFieldFactory.Create(EntityFieldInformationFieldIndex.HelpText);}
		}
		/// <summary>Creates a new EntityFieldInformationEntity.DisplayFormatString field instance</summary>
		public static EntityField DisplayFormatString
		{
			get { return (EntityField)EntityFieldFactory.Create(EntityFieldInformationFieldIndex.DisplayFormatString);}
		}
		/// <summary>Creates a new EntityFieldInformationEntity.DefaultSortPosition field instance</summary>
		public static EntityField DefaultSortPosition
		{
			get { return (EntityField)EntityFieldFactory.Create(EntityFieldInformationFieldIndex.DefaultSortPosition);}
		}
		/// <summary>Creates a new EntityFieldInformationEntity.DefaultSortOperator field instance</summary>
		public static EntityField DefaultSortOperator
		{
			get { return (EntityField)EntityFieldFactory.Create(EntityFieldInformationFieldIndex.DefaultSortOperator);}
		}
		/// <summary>Creates a new EntityFieldInformationEntity.Type field instance</summary>
		public static EntityField Type
		{
			get { return (EntityField)EntityFieldFactory.Create(EntityFieldInformationFieldIndex.Type);}
		}
		/// <summary>Creates a new EntityFieldInformationEntity.IsRequired field instance</summary>
		public static EntityField IsRequired
		{
			get { return (EntityField)EntityFieldFactory.Create(EntityFieldInformationFieldIndex.IsRequired);}
		}
		/// <summary>Creates a new EntityFieldInformationEntity.EntityFieldType field instance</summary>
		public static EntityField EntityFieldType
		{
			get { return (EntityField)EntityFieldFactory.Create(EntityFieldInformationFieldIndex.EntityFieldType);}
		}
		/// <summary>Creates a new EntityFieldInformationEntity.ExcludeForUpdateWmsCacheDateOnChange field instance</summary>
		public static EntityField ExcludeForUpdateWmsCacheDateOnChange
		{
			get { return (EntityField)EntityFieldFactory.Create(EntityFieldInformationFieldIndex.ExcludeForUpdateWmsCacheDateOnChange);}
		}
		/// <summary>Creates a new EntityFieldInformationEntity.ExcludeForUpdateCompanyDataVersionOnChange field instance</summary>
		public static EntityField ExcludeForUpdateCompanyDataVersionOnChange
		{
			get { return (EntityField)EntityFieldFactory.Create(EntityFieldInformationFieldIndex.ExcludeForUpdateCompanyDataVersionOnChange);}
		}
		/// <summary>Creates a new EntityFieldInformationEntity.ExcludeForUpdateMenuDataVersionOnChange field instance</summary>
		public static EntityField ExcludeForUpdateMenuDataVersionOnChange
		{
			get { return (EntityField)EntityFieldFactory.Create(EntityFieldInformationFieldIndex.ExcludeForUpdateMenuDataVersionOnChange);}
		}
		/// <summary>Creates a new EntityFieldInformationEntity.ExcludeForUpdatePosIntegrationInformationVersionOnChange field instance</summary>
		public static EntityField ExcludeForUpdatePosIntegrationInformationVersionOnChange
		{
			get { return (EntityField)EntityFieldFactory.Create(EntityFieldInformationFieldIndex.ExcludeForUpdatePosIntegrationInformationVersionOnChange);}
		}
		/// <summary>Creates a new EntityFieldInformationEntity.ForLocalUseWhileSyncingIsUpdated field instance</summary>
		public static EntityField ForLocalUseWhileSyncingIsUpdated
		{
			get { return (EntityField)EntityFieldFactory.Create(EntityFieldInformationFieldIndex.ForLocalUseWhileSyncingIsUpdated);}
		}
		/// <summary>Creates a new EntityFieldInformationEntity.Archived field instance</summary>
		public static EntityField Archived
		{
			get { return (EntityField)EntityFieldFactory.Create(EntityFieldInformationFieldIndex.Archived);}
		}
		/// <summary>Creates a new EntityFieldInformationEntity.Created field instance</summary>
		public static EntityField Created
		{
			get { return (EntityField)EntityFieldFactory.Create(EntityFieldInformationFieldIndex.Created);}
		}
		/// <summary>Creates a new EntityFieldInformationEntity.CreatedBy field instance</summary>
		public static EntityField CreatedBy
		{
			get { return (EntityField)EntityFieldFactory.Create(EntityFieldInformationFieldIndex.CreatedBy);}
		}
		/// <summary>Creates a new EntityFieldInformationEntity.Updated field instance</summary>
		public static EntityField Updated
		{
			get { return (EntityField)EntityFieldFactory.Create(EntityFieldInformationFieldIndex.Updated);}
		}
		/// <summary>Creates a new EntityFieldInformationEntity.UpdatedBy field instance</summary>
		public static EntityField UpdatedBy
		{
			get { return (EntityField)EntityFieldFactory.Create(EntityFieldInformationFieldIndex.UpdatedBy);}
		}
		/// <summary>Creates a new EntityFieldInformationEntity.Deleted field instance</summary>
		public static EntityField Deleted
		{
			get { return (EntityField)EntityFieldFactory.Create(EntityFieldInformationFieldIndex.Deleted);}
		}
		/// <summary>Creates a new EntityFieldInformationEntity.ExcludeForUpdateDeliverypointDataVersionOnChange field instance</summary>
		public static EntityField ExcludeForUpdateDeliverypointDataVersionOnChange
		{
			get { return (EntityField)EntityFieldFactory.Create(EntityFieldInformationFieldIndex.ExcludeForUpdateDeliverypointDataVersionOnChange);}
		}
		/// <summary>Creates a new EntityFieldInformationEntity.ExcludeForUpdateSurveyDataVersionOnChange field instance</summary>
		public static EntityField ExcludeForUpdateSurveyDataVersionOnChange
		{
			get { return (EntityField)EntityFieldFactory.Create(EntityFieldInformationFieldIndex.ExcludeForUpdateSurveyDataVersionOnChange);}
		}
		/// <summary>Creates a new EntityFieldInformationEntity.ExcludeForUpdateAnnouncementDataVersionOnChange field instance</summary>
		public static EntityField ExcludeForUpdateAnnouncementDataVersionOnChange
		{
			get { return (EntityField)EntityFieldFactory.Create(EntityFieldInformationFieldIndex.ExcludeForUpdateAnnouncementDataVersionOnChange);}
		}
		/// <summary>Creates a new EntityFieldInformationEntity.ExcludeForUpdateEntertainmentDataVersionOnChange field instance</summary>
		public static EntityField ExcludeForUpdateEntertainmentDataVersionOnChange
		{
			get { return (EntityField)EntityFieldFactory.Create(EntityFieldInformationFieldIndex.ExcludeForUpdateEntertainmentDataVersionOnChange);}
		}
		/// <summary>Creates a new EntityFieldInformationEntity.ExcludeForUpdateAdvertisementDataVersionOnChange field instance</summary>
		public static EntityField ExcludeForUpdateAdvertisementDataVersionOnChange
		{
			get { return (EntityField)EntityFieldFactory.Create(EntityFieldInformationFieldIndex.ExcludeForUpdateAdvertisementDataVersionOnChange);}
		}
		/// <summary>Creates a new EntityFieldInformationEntity.CreatedUTC field instance</summary>
		public static EntityField CreatedUTC
		{
			get { return (EntityField)EntityFieldFactory.Create(EntityFieldInformationFieldIndex.CreatedUTC);}
		}
		/// <summary>Creates a new EntityFieldInformationEntity.UpdatedUTC field instance</summary>
		public static EntityField UpdatedUTC
		{
			get { return (EntityField)EntityFieldFactory.Create(EntityFieldInformationFieldIndex.UpdatedUTC);}
		}
	}

	/// <summary>Field Creation Class for entity EntityInformationEntity</summary>
	public partial class EntityInformationFields
	{
		/// <summary>Creates a new EntityInformationEntity.EntityInformationId field instance</summary>
		public static EntityField EntityInformationId
		{
			get { return (EntityField)EntityFieldFactory.Create(EntityInformationFieldIndex.EntityInformationId);}
		}
		/// <summary>Creates a new EntityInformationEntity.EntityName field instance</summary>
		public static EntityField EntityName
		{
			get { return (EntityField)EntityFieldFactory.Create(EntityInformationFieldIndex.EntityName);}
		}
		/// <summary>Creates a new EntityInformationEntity.ShowFieldName field instance</summary>
		public static EntityField ShowFieldName
		{
			get { return (EntityField)EntityFieldFactory.Create(EntityInformationFieldIndex.ShowFieldName);}
		}
		/// <summary>Creates a new EntityInformationEntity.ShowFieldNameBreadCrumb field instance</summary>
		public static EntityField ShowFieldNameBreadCrumb
		{
			get { return (EntityField)EntityFieldFactory.Create(EntityInformationFieldIndex.ShowFieldNameBreadCrumb);}
		}
		/// <summary>Creates a new EntityInformationEntity.FriendlyName field instance</summary>
		public static EntityField FriendlyName
		{
			get { return (EntityField)EntityFieldFactory.Create(EntityInformationFieldIndex.FriendlyName);}
		}
		/// <summary>Creates a new EntityInformationEntity.FriendlyNamePlural field instance</summary>
		public static EntityField FriendlyNamePlural
		{
			get { return (EntityField)EntityFieldFactory.Create(EntityInformationFieldIndex.FriendlyNamePlural);}
		}
		/// <summary>Creates a new EntityInformationEntity.HelpText field instance</summary>
		public static EntityField HelpText
		{
			get { return (EntityField)EntityFieldFactory.Create(EntityInformationFieldIndex.HelpText);}
		}
		/// <summary>Creates a new EntityInformationEntity.TitleAsBreadCrumbHierarchy field instance</summary>
		public static EntityField TitleAsBreadCrumbHierarchy
		{
			get { return (EntityField)EntityFieldFactory.Create(EntityInformationFieldIndex.TitleAsBreadCrumbHierarchy);}
		}
		/// <summary>Creates a new EntityInformationEntity.DefaultEntityEditPage field instance</summary>
		public static EntityField DefaultEntityEditPage
		{
			get { return (EntityField)EntityFieldFactory.Create(EntityInformationFieldIndex.DefaultEntityEditPage);}
		}
		/// <summary>Creates a new EntityInformationEntity.DefaultEntityCollectionPage field instance</summary>
		public static EntityField DefaultEntityCollectionPage
		{
			get { return (EntityField)EntityFieldFactory.Create(EntityInformationFieldIndex.DefaultEntityCollectionPage);}
		}
		/// <summary>Creates a new EntityInformationEntity.UpdateWmsCacheDateOnChange field instance</summary>
		public static EntityField UpdateWmsCacheDateOnChange
		{
			get { return (EntityField)EntityFieldFactory.Create(EntityInformationFieldIndex.UpdateWmsCacheDateOnChange);}
		}
		/// <summary>Creates a new EntityInformationEntity.UpdateCompanyDataVersionOnChange field instance</summary>
		public static EntityField UpdateCompanyDataVersionOnChange
		{
			get { return (EntityField)EntityFieldFactory.Create(EntityInformationFieldIndex.UpdateCompanyDataVersionOnChange);}
		}
		/// <summary>Creates a new EntityInformationEntity.UpdateMenuDataVersionOnChange field instance</summary>
		public static EntityField UpdateMenuDataVersionOnChange
		{
			get { return (EntityField)EntityFieldFactory.Create(EntityInformationFieldIndex.UpdateMenuDataVersionOnChange);}
		}
		/// <summary>Creates a new EntityInformationEntity.UpdatePosIntegrationInformationOnChange field instance</summary>
		public static EntityField UpdatePosIntegrationInformationOnChange
		{
			get { return (EntityField)EntityFieldFactory.Create(EntityInformationFieldIndex.UpdatePosIntegrationInformationOnChange);}
		}
		/// <summary>Creates a new EntityInformationEntity.ForLocalUseWhileSyncingIsUpdated field instance</summary>
		public static EntityField ForLocalUseWhileSyncingIsUpdated
		{
			get { return (EntityField)EntityFieldFactory.Create(EntityInformationFieldIndex.ForLocalUseWhileSyncingIsUpdated);}
		}
		/// <summary>Creates a new EntityInformationEntity.Archived field instance</summary>
		public static EntityField Archived
		{
			get { return (EntityField)EntityFieldFactory.Create(EntityInformationFieldIndex.Archived);}
		}
		/// <summary>Creates a new EntityInformationEntity.Created field instance</summary>
		public static EntityField Created
		{
			get { return (EntityField)EntityFieldFactory.Create(EntityInformationFieldIndex.Created);}
		}
		/// <summary>Creates a new EntityInformationEntity.CreatedBy field instance</summary>
		public static EntityField CreatedBy
		{
			get { return (EntityField)EntityFieldFactory.Create(EntityInformationFieldIndex.CreatedBy);}
		}
		/// <summary>Creates a new EntityInformationEntity.Updated field instance</summary>
		public static EntityField Updated
		{
			get { return (EntityField)EntityFieldFactory.Create(EntityInformationFieldIndex.Updated);}
		}
		/// <summary>Creates a new EntityInformationEntity.UpdatedBy field instance</summary>
		public static EntityField UpdatedBy
		{
			get { return (EntityField)EntityFieldFactory.Create(EntityInformationFieldIndex.UpdatedBy);}
		}
		/// <summary>Creates a new EntityInformationEntity.Deleted field instance</summary>
		public static EntityField Deleted
		{
			get { return (EntityField)EntityFieldFactory.Create(EntityInformationFieldIndex.Deleted);}
		}
		/// <summary>Creates a new EntityInformationEntity.UpdateDeliverypointDataVersionOnChange field instance</summary>
		public static EntityField UpdateDeliverypointDataVersionOnChange
		{
			get { return (EntityField)EntityFieldFactory.Create(EntityInformationFieldIndex.UpdateDeliverypointDataVersionOnChange);}
		}
		/// <summary>Creates a new EntityInformationEntity.UpdateSurveyDataVersionOnChange field instance</summary>
		public static EntityField UpdateSurveyDataVersionOnChange
		{
			get { return (EntityField)EntityFieldFactory.Create(EntityInformationFieldIndex.UpdateSurveyDataVersionOnChange);}
		}
		/// <summary>Creates a new EntityInformationEntity.UpdateAnnouncementDataVersionOnChange field instance</summary>
		public static EntityField UpdateAnnouncementDataVersionOnChange
		{
			get { return (EntityField)EntityFieldFactory.Create(EntityInformationFieldIndex.UpdateAnnouncementDataVersionOnChange);}
		}
		/// <summary>Creates a new EntityInformationEntity.UpdateEntertainmentDataVersionOnChange field instance</summary>
		public static EntityField UpdateEntertainmentDataVersionOnChange
		{
			get { return (EntityField)EntityFieldFactory.Create(EntityInformationFieldIndex.UpdateEntertainmentDataVersionOnChange);}
		}
		/// <summary>Creates a new EntityInformationEntity.UpdateAdvertisementDataVersionOnChange field instance</summary>
		public static EntityField UpdateAdvertisementDataVersionOnChange
		{
			get { return (EntityField)EntityFieldFactory.Create(EntityInformationFieldIndex.UpdateAdvertisementDataVersionOnChange);}
		}
		/// <summary>Creates a new EntityInformationEntity.CreatedUTC field instance</summary>
		public static EntityField CreatedUTC
		{
			get { return (EntityField)EntityFieldFactory.Create(EntityInformationFieldIndex.CreatedUTC);}
		}
		/// <summary>Creates a new EntityInformationEntity.UpdatedUTC field instance</summary>
		public static EntityField UpdatedUTC
		{
			get { return (EntityField)EntityFieldFactory.Create(EntityInformationFieldIndex.UpdatedUTC);}
		}
	}

	/// <summary>Field Creation Class for entity ModuleEntity</summary>
	public partial class ModuleFields
	{
		/// <summary>Creates a new ModuleEntity.ModuleId field instance</summary>
		public static EntityField ModuleId
		{
			get { return (EntityField)EntityFieldFactory.Create(ModuleFieldIndex.ModuleId);}
		}
		/// <summary>Creates a new ModuleEntity.NameSystem field instance</summary>
		public static EntityField NameSystem
		{
			get { return (EntityField)EntityFieldFactory.Create(ModuleFieldIndex.NameSystem);}
		}
		/// <summary>Creates a new ModuleEntity.NameFull field instance</summary>
		public static EntityField NameFull
		{
			get { return (EntityField)EntityFieldFactory.Create(ModuleFieldIndex.NameFull);}
		}
		/// <summary>Creates a new ModuleEntity.NameShort field instance</summary>
		public static EntityField NameShort
		{
			get { return (EntityField)EntityFieldFactory.Create(ModuleFieldIndex.NameShort);}
		}
		/// <summary>Creates a new ModuleEntity.IconPath field instance</summary>
		public static EntityField IconPath
		{
			get { return (EntityField)EntityFieldFactory.Create(ModuleFieldIndex.IconPath);}
		}
		/// <summary>Creates a new ModuleEntity.DefaultUiElementTypeNameFull field instance</summary>
		public static EntityField DefaultUiElementTypeNameFull
		{
			get { return (EntityField)EntityFieldFactory.Create(ModuleFieldIndex.DefaultUiElementTypeNameFull);}
		}
		/// <summary>Creates a new ModuleEntity.SortOrder field instance</summary>
		public static EntityField SortOrder
		{
			get { return (EntityField)EntityFieldFactory.Create(ModuleFieldIndex.SortOrder);}
		}
		/// <summary>Creates a new ModuleEntity.LicensingFree field instance</summary>
		public static EntityField LicensingFree
		{
			get { return (EntityField)EntityFieldFactory.Create(ModuleFieldIndex.LicensingFree);}
		}
		/// <summary>Creates a new ModuleEntity.UserRightsFree field instance</summary>
		public static EntityField UserRightsFree
		{
			get { return (EntityField)EntityFieldFactory.Create(ModuleFieldIndex.UserRightsFree);}
		}
		/// <summary>Creates a new ModuleEntity.ForLocalUseWhileSyncingIsUpdated field instance</summary>
		public static EntityField ForLocalUseWhileSyncingIsUpdated
		{
			get { return (EntityField)EntityFieldFactory.Create(ModuleFieldIndex.ForLocalUseWhileSyncingIsUpdated);}
		}
		/// <summary>Creates a new ModuleEntity.Archived field instance</summary>
		public static EntityField Archived
		{
			get { return (EntityField)EntityFieldFactory.Create(ModuleFieldIndex.Archived);}
		}
		/// <summary>Creates a new ModuleEntity.Created field instance</summary>
		public static EntityField Created
		{
			get { return (EntityField)EntityFieldFactory.Create(ModuleFieldIndex.Created);}
		}
		/// <summary>Creates a new ModuleEntity.CreatedBy field instance</summary>
		public static EntityField CreatedBy
		{
			get { return (EntityField)EntityFieldFactory.Create(ModuleFieldIndex.CreatedBy);}
		}
		/// <summary>Creates a new ModuleEntity.Updated field instance</summary>
		public static EntityField Updated
		{
			get { return (EntityField)EntityFieldFactory.Create(ModuleFieldIndex.Updated);}
		}
		/// <summary>Creates a new ModuleEntity.UpdatedBy field instance</summary>
		public static EntityField UpdatedBy
		{
			get { return (EntityField)EntityFieldFactory.Create(ModuleFieldIndex.UpdatedBy);}
		}
		/// <summary>Creates a new ModuleEntity.Deleted field instance</summary>
		public static EntityField Deleted
		{
			get { return (EntityField)EntityFieldFactory.Create(ModuleFieldIndex.Deleted);}
		}
		/// <summary>Creates a new ModuleEntity.CreatedUTC field instance</summary>
		public static EntityField CreatedUTC
		{
			get { return (EntityField)EntityFieldFactory.Create(ModuleFieldIndex.CreatedUTC);}
		}
		/// <summary>Creates a new ModuleEntity.UpdatedUTC field instance</summary>
		public static EntityField UpdatedUTC
		{
			get { return (EntityField)EntityFieldFactory.Create(ModuleFieldIndex.UpdatedUTC);}
		}
	}

	/// <summary>Field Creation Class for entity ReferentialConstraintEntity</summary>
	public partial class ReferentialConstraintFields
	{
		/// <summary>Creates a new ReferentialConstraintEntity.ReferentialConstraintId field instance</summary>
		public static EntityField ReferentialConstraintId
		{
			get { return (EntityField)EntityFieldFactory.Create(ReferentialConstraintFieldIndex.ReferentialConstraintId);}
		}
		/// <summary>Creates a new ReferentialConstraintEntity.EntityName field instance</summary>
		public static EntityField EntityName
		{
			get { return (EntityField)EntityFieldFactory.Create(ReferentialConstraintFieldIndex.EntityName);}
		}
		/// <summary>Creates a new ReferentialConstraintEntity.ForeignKeyName field instance</summary>
		public static EntityField ForeignKeyName
		{
			get { return (EntityField)EntityFieldFactory.Create(ReferentialConstraintFieldIndex.ForeignKeyName);}
		}
		/// <summary>Creates a new ReferentialConstraintEntity.DeleteRule field instance</summary>
		public static EntityField DeleteRule
		{
			get { return (EntityField)EntityFieldFactory.Create(ReferentialConstraintFieldIndex.DeleteRule);}
		}
		/// <summary>Creates a new ReferentialConstraintEntity.UpdateRule field instance</summary>
		public static EntityField UpdateRule
		{
			get { return (EntityField)EntityFieldFactory.Create(ReferentialConstraintFieldIndex.UpdateRule);}
		}
		/// <summary>Creates a new ReferentialConstraintEntity.ArchiveRule field instance</summary>
		public static EntityField ArchiveRule
		{
			get { return (EntityField)EntityFieldFactory.Create(ReferentialConstraintFieldIndex.ArchiveRule);}
		}
		/// <summary>Creates a new ReferentialConstraintEntity.ForLocalUseWhileSyncingIsUpdated field instance</summary>
		public static EntityField ForLocalUseWhileSyncingIsUpdated
		{
			get { return (EntityField)EntityFieldFactory.Create(ReferentialConstraintFieldIndex.ForLocalUseWhileSyncingIsUpdated);}
		}
		/// <summary>Creates a new ReferentialConstraintEntity.Archived field instance</summary>
		public static EntityField Archived
		{
			get { return (EntityField)EntityFieldFactory.Create(ReferentialConstraintFieldIndex.Archived);}
		}
		/// <summary>Creates a new ReferentialConstraintEntity.Created field instance</summary>
		public static EntityField Created
		{
			get { return (EntityField)EntityFieldFactory.Create(ReferentialConstraintFieldIndex.Created);}
		}
		/// <summary>Creates a new ReferentialConstraintEntity.CreatedBy field instance</summary>
		public static EntityField CreatedBy
		{
			get { return (EntityField)EntityFieldFactory.Create(ReferentialConstraintFieldIndex.CreatedBy);}
		}
		/// <summary>Creates a new ReferentialConstraintEntity.Updated field instance</summary>
		public static EntityField Updated
		{
			get { return (EntityField)EntityFieldFactory.Create(ReferentialConstraintFieldIndex.Updated);}
		}
		/// <summary>Creates a new ReferentialConstraintEntity.UpdatedBy field instance</summary>
		public static EntityField UpdatedBy
		{
			get { return (EntityField)EntityFieldFactory.Create(ReferentialConstraintFieldIndex.UpdatedBy);}
		}
		/// <summary>Creates a new ReferentialConstraintEntity.Deleted field instance</summary>
		public static EntityField Deleted
		{
			get { return (EntityField)EntityFieldFactory.Create(ReferentialConstraintFieldIndex.Deleted);}
		}
		/// <summary>Creates a new ReferentialConstraintEntity.CreatedUTC field instance</summary>
		public static EntityField CreatedUTC
		{
			get { return (EntityField)EntityFieldFactory.Create(ReferentialConstraintFieldIndex.CreatedUTC);}
		}
		/// <summary>Creates a new ReferentialConstraintEntity.UpdatedUTC field instance</summary>
		public static EntityField UpdatedUTC
		{
			get { return (EntityField)EntityFieldFactory.Create(ReferentialConstraintFieldIndex.UpdatedUTC);}
		}
	}

	/// <summary>Field Creation Class for entity RoleEntity</summary>
	public partial class RoleFields
	{
		/// <summary>Creates a new RoleEntity.RoleId field instance</summary>
		public static EntityField RoleId
		{
			get { return (EntityField)EntityFieldFactory.Create(RoleFieldIndex.RoleId);}
		}
		/// <summary>Creates a new RoleEntity.ParentRoleId field instance</summary>
		public static EntityField ParentRoleId
		{
			get { return (EntityField)EntityFieldFactory.Create(RoleFieldIndex.ParentRoleId);}
		}
		/// <summary>Creates a new RoleEntity.Name field instance</summary>
		public static EntityField Name
		{
			get { return (EntityField)EntityFieldFactory.Create(RoleFieldIndex.Name);}
		}
		/// <summary>Creates a new RoleEntity.IsAdministrator field instance</summary>
		public static EntityField IsAdministrator
		{
			get { return (EntityField)EntityFieldFactory.Create(RoleFieldIndex.IsAdministrator);}
		}
		/// <summary>Creates a new RoleEntity.SystemName field instance</summary>
		public static EntityField SystemName
		{
			get { return (EntityField)EntityFieldFactory.Create(RoleFieldIndex.SystemName);}
		}
		/// <summary>Creates a new RoleEntity.SystemRole field instance</summary>
		public static EntityField SystemRole
		{
			get { return (EntityField)EntityFieldFactory.Create(RoleFieldIndex.SystemRole);}
		}
		/// <summary>Creates a new RoleEntity.Created field instance</summary>
		public static EntityField Created
		{
			get { return (EntityField)EntityFieldFactory.Create(RoleFieldIndex.Created);}
		}
		/// <summary>Creates a new RoleEntity.CreatedBy field instance</summary>
		public static EntityField CreatedBy
		{
			get { return (EntityField)EntityFieldFactory.Create(RoleFieldIndex.CreatedBy);}
		}
		/// <summary>Creates a new RoleEntity.Updated field instance</summary>
		public static EntityField Updated
		{
			get { return (EntityField)EntityFieldFactory.Create(RoleFieldIndex.Updated);}
		}
		/// <summary>Creates a new RoleEntity.UpdatedBy field instance</summary>
		public static EntityField UpdatedBy
		{
			get { return (EntityField)EntityFieldFactory.Create(RoleFieldIndex.UpdatedBy);}
		}
		/// <summary>Creates a new RoleEntity.Deleted field instance</summary>
		public static EntityField Deleted
		{
			get { return (EntityField)EntityFieldFactory.Create(RoleFieldIndex.Deleted);}
		}
		/// <summary>Creates a new RoleEntity.Archived field instance</summary>
		public static EntityField Archived
		{
			get { return (EntityField)EntityFieldFactory.Create(RoleFieldIndex.Archived);}
		}
		/// <summary>Creates a new RoleEntity.CreatedUTC field instance</summary>
		public static EntityField CreatedUTC
		{
			get { return (EntityField)EntityFieldFactory.Create(RoleFieldIndex.CreatedUTC);}
		}
		/// <summary>Creates a new RoleEntity.UpdatedUTC field instance</summary>
		public static EntityField UpdatedUTC
		{
			get { return (EntityField)EntityFieldFactory.Create(RoleFieldIndex.UpdatedUTC);}
		}
	}

	/// <summary>Field Creation Class for entity RoleEntityRightsEntity</summary>
	public partial class RoleEntityRightsFields
	{
		/// <summary>Creates a new RoleEntityRightsEntity.RoleEntityRightId field instance</summary>
		public static EntityField RoleEntityRightId
		{
			get { return (EntityField)EntityFieldFactory.Create(RoleEntityRightsFieldIndex.RoleEntityRightId);}
		}
		/// <summary>Creates a new RoleEntityRightsEntity.RoleId field instance</summary>
		public static EntityField RoleId
		{
			get { return (EntityField)EntityFieldFactory.Create(RoleEntityRightsFieldIndex.RoleId);}
		}
		/// <summary>Creates a new RoleEntityRightsEntity.EntityInformationId field instance</summary>
		public static EntityField EntityInformationId
		{
			get { return (EntityField)EntityFieldFactory.Create(RoleEntityRightsFieldIndex.EntityInformationId);}
		}
		/// <summary>Creates a new RoleEntityRightsEntity.AllowReadAll field instance</summary>
		public static EntityField AllowReadAll
		{
			get { return (EntityField)EntityFieldFactory.Create(RoleEntityRightsFieldIndex.AllowReadAll);}
		}
		/// <summary>Creates a new RoleEntityRightsEntity.AllowUpdateAll field instance</summary>
		public static EntityField AllowUpdateAll
		{
			get { return (EntityField)EntityFieldFactory.Create(RoleEntityRightsFieldIndex.AllowUpdateAll);}
		}
		/// <summary>Creates a new RoleEntityRightsEntity.AllowInsert field instance</summary>
		public static EntityField AllowInsert
		{
			get { return (EntityField)EntityFieldFactory.Create(RoleEntityRightsFieldIndex.AllowInsert);}
		}
		/// <summary>Creates a new RoleEntityRightsEntity.AllowDeleteAll field instance</summary>
		public static EntityField AllowDeleteAll
		{
			get { return (EntityField)EntityFieldFactory.Create(RoleEntityRightsFieldIndex.AllowDeleteAll);}
		}
		/// <summary>Creates a new RoleEntityRightsEntity.AllowUpdateCreated field instance</summary>
		public static EntityField AllowUpdateCreated
		{
			get { return (EntityField)EntityFieldFactory.Create(RoleEntityRightsFieldIndex.AllowUpdateCreated);}
		}
		/// <summary>Creates a new RoleEntityRightsEntity.AllowDeleteCreated field instance</summary>
		public static EntityField AllowDeleteCreated
		{
			get { return (EntityField)EntityFieldFactory.Create(RoleEntityRightsFieldIndex.AllowDeleteCreated);}
		}
		/// <summary>Creates a new RoleEntityRightsEntity.Created field instance</summary>
		public static EntityField Created
		{
			get { return (EntityField)EntityFieldFactory.Create(RoleEntityRightsFieldIndex.Created);}
		}
		/// <summary>Creates a new RoleEntityRightsEntity.CreatedBy field instance</summary>
		public static EntityField CreatedBy
		{
			get { return (EntityField)EntityFieldFactory.Create(RoleEntityRightsFieldIndex.CreatedBy);}
		}
		/// <summary>Creates a new RoleEntityRightsEntity.Updated field instance</summary>
		public static EntityField Updated
		{
			get { return (EntityField)EntityFieldFactory.Create(RoleEntityRightsFieldIndex.Updated);}
		}
		/// <summary>Creates a new RoleEntityRightsEntity.UpdatedBy field instance</summary>
		public static EntityField UpdatedBy
		{
			get { return (EntityField)EntityFieldFactory.Create(RoleEntityRightsFieldIndex.UpdatedBy);}
		}
		/// <summary>Creates a new RoleEntityRightsEntity.Deleted field instance</summary>
		public static EntityField Deleted
		{
			get { return (EntityField)EntityFieldFactory.Create(RoleEntityRightsFieldIndex.Deleted);}
		}
		/// <summary>Creates a new RoleEntityRightsEntity.Archived field instance</summary>
		public static EntityField Archived
		{
			get { return (EntityField)EntityFieldFactory.Create(RoleEntityRightsFieldIndex.Archived);}
		}
		/// <summary>Creates a new RoleEntityRightsEntity.CreatedUTC field instance</summary>
		public static EntityField CreatedUTC
		{
			get { return (EntityField)EntityFieldFactory.Create(RoleEntityRightsFieldIndex.CreatedUTC);}
		}
		/// <summary>Creates a new RoleEntityRightsEntity.UpdatedUTC field instance</summary>
		public static EntityField UpdatedUTC
		{
			get { return (EntityField)EntityFieldFactory.Create(RoleEntityRightsFieldIndex.UpdatedUTC);}
		}
	}

	/// <summary>Field Creation Class for entity RoleModuleRightsEntity</summary>
	public partial class RoleModuleRightsFields
	{
		/// <summary>Creates a new RoleModuleRightsEntity.RoleModuleRightsId field instance</summary>
		public static EntityField RoleModuleRightsId
		{
			get { return (EntityField)EntityFieldFactory.Create(RoleModuleRightsFieldIndex.RoleModuleRightsId);}
		}
		/// <summary>Creates a new RoleModuleRightsEntity.RoleId field instance</summary>
		public static EntityField RoleId
		{
			get { return (EntityField)EntityFieldFactory.Create(RoleModuleRightsFieldIndex.RoleId);}
		}
		/// <summary>Creates a new RoleModuleRightsEntity.ModuleId field instance</summary>
		public static EntityField ModuleId
		{
			get { return (EntityField)EntityFieldFactory.Create(RoleModuleRightsFieldIndex.ModuleId);}
		}
		/// <summary>Creates a new RoleModuleRightsEntity.IsAllowed field instance</summary>
		public static EntityField IsAllowed
		{
			get { return (EntityField)EntityFieldFactory.Create(RoleModuleRightsFieldIndex.IsAllowed);}
		}
		/// <summary>Creates a new RoleModuleRightsEntity.Archived field instance</summary>
		public static EntityField Archived
		{
			get { return (EntityField)EntityFieldFactory.Create(RoleModuleRightsFieldIndex.Archived);}
		}
		/// <summary>Creates a new RoleModuleRightsEntity.Created field instance</summary>
		public static EntityField Created
		{
			get { return (EntityField)EntityFieldFactory.Create(RoleModuleRightsFieldIndex.Created);}
		}
		/// <summary>Creates a new RoleModuleRightsEntity.CreatedBy field instance</summary>
		public static EntityField CreatedBy
		{
			get { return (EntityField)EntityFieldFactory.Create(RoleModuleRightsFieldIndex.CreatedBy);}
		}
		/// <summary>Creates a new RoleModuleRightsEntity.Updated field instance</summary>
		public static EntityField Updated
		{
			get { return (EntityField)EntityFieldFactory.Create(RoleModuleRightsFieldIndex.Updated);}
		}
		/// <summary>Creates a new RoleModuleRightsEntity.UpdatedBy field instance</summary>
		public static EntityField UpdatedBy
		{
			get { return (EntityField)EntityFieldFactory.Create(RoleModuleRightsFieldIndex.UpdatedBy);}
		}
		/// <summary>Creates a new RoleModuleRightsEntity.Deleted field instance</summary>
		public static EntityField Deleted
		{
			get { return (EntityField)EntityFieldFactory.Create(RoleModuleRightsFieldIndex.Deleted);}
		}
		/// <summary>Creates a new RoleModuleRightsEntity.CreatedUTC field instance</summary>
		public static EntityField CreatedUTC
		{
			get { return (EntityField)EntityFieldFactory.Create(RoleModuleRightsFieldIndex.CreatedUTC);}
		}
		/// <summary>Creates a new RoleModuleRightsEntity.UpdatedUTC field instance</summary>
		public static EntityField UpdatedUTC
		{
			get { return (EntityField)EntityFieldFactory.Create(RoleModuleRightsFieldIndex.UpdatedUTC);}
		}
	}

	/// <summary>Field Creation Class for entity RoleUIElementRightsEntity</summary>
	public partial class RoleUIElementRightsFields
	{
		/// <summary>Creates a new RoleUIElementRightsEntity.RoleUIElementRightsId field instance</summary>
		public static EntityField RoleUIElementRightsId
		{
			get { return (EntityField)EntityFieldFactory.Create(RoleUIElementRightsFieldIndex.RoleUIElementRightsId);}
		}
		/// <summary>Creates a new RoleUIElementRightsEntity.RoleId field instance</summary>
		public static EntityField RoleId
		{
			get { return (EntityField)EntityFieldFactory.Create(RoleUIElementRightsFieldIndex.RoleId);}
		}
		/// <summary>Creates a new RoleUIElementRightsEntity.UiElementTypeNameFull field instance</summary>
		public static EntityField UiElementTypeNameFull
		{
			get { return (EntityField)EntityFieldFactory.Create(RoleUIElementRightsFieldIndex.UiElementTypeNameFull);}
		}
		/// <summary>Creates a new RoleUIElementRightsEntity.IsAllowed field instance</summary>
		public static EntityField IsAllowed
		{
			get { return (EntityField)EntityFieldFactory.Create(RoleUIElementRightsFieldIndex.IsAllowed);}
		}
		/// <summary>Creates a new RoleUIElementRightsEntity.IsReadOnly field instance</summary>
		public static EntityField IsReadOnly
		{
			get { return (EntityField)EntityFieldFactory.Create(RoleUIElementRightsFieldIndex.IsReadOnly);}
		}
		/// <summary>Creates a new RoleUIElementRightsEntity.Archived field instance</summary>
		public static EntityField Archived
		{
			get { return (EntityField)EntityFieldFactory.Create(RoleUIElementRightsFieldIndex.Archived);}
		}
		/// <summary>Creates a new RoleUIElementRightsEntity.Created field instance</summary>
		public static EntityField Created
		{
			get { return (EntityField)EntityFieldFactory.Create(RoleUIElementRightsFieldIndex.Created);}
		}
		/// <summary>Creates a new RoleUIElementRightsEntity.CreatedBy field instance</summary>
		public static EntityField CreatedBy
		{
			get { return (EntityField)EntityFieldFactory.Create(RoleUIElementRightsFieldIndex.CreatedBy);}
		}
		/// <summary>Creates a new RoleUIElementRightsEntity.Updated field instance</summary>
		public static EntityField Updated
		{
			get { return (EntityField)EntityFieldFactory.Create(RoleUIElementRightsFieldIndex.Updated);}
		}
		/// <summary>Creates a new RoleUIElementRightsEntity.UpdatedBy field instance</summary>
		public static EntityField UpdatedBy
		{
			get { return (EntityField)EntityFieldFactory.Create(RoleUIElementRightsFieldIndex.UpdatedBy);}
		}
		/// <summary>Creates a new RoleUIElementRightsEntity.Deleted field instance</summary>
		public static EntityField Deleted
		{
			get { return (EntityField)EntityFieldFactory.Create(RoleUIElementRightsFieldIndex.Deleted);}
		}
		/// <summary>Creates a new RoleUIElementRightsEntity.CreatedUTC field instance</summary>
		public static EntityField CreatedUTC
		{
			get { return (EntityField)EntityFieldFactory.Create(RoleUIElementRightsFieldIndex.CreatedUTC);}
		}
		/// <summary>Creates a new RoleUIElementRightsEntity.UpdatedUTC field instance</summary>
		public static EntityField UpdatedUTC
		{
			get { return (EntityField)EntityFieldFactory.Create(RoleUIElementRightsFieldIndex.UpdatedUTC);}
		}
	}

	/// <summary>Field Creation Class for entity RoleUIElementSubPanelRightsEntity</summary>
	public partial class RoleUIElementSubPanelRightsFields
	{
		/// <summary>Creates a new RoleUIElementSubPanelRightsEntity.RoleUIElementSubPanelRightsId field instance</summary>
		public static EntityField RoleUIElementSubPanelRightsId
		{
			get { return (EntityField)EntityFieldFactory.Create(RoleUIElementSubPanelRightsFieldIndex.RoleUIElementSubPanelRightsId);}
		}
		/// <summary>Creates a new RoleUIElementSubPanelRightsEntity.RoleId field instance</summary>
		public static EntityField RoleId
		{
			get { return (EntityField)EntityFieldFactory.Create(RoleUIElementSubPanelRightsFieldIndex.RoleId);}
		}
		/// <summary>Creates a new RoleUIElementSubPanelRightsEntity.UiElementTypeNameFull field instance</summary>
		public static EntityField UiElementTypeNameFull
		{
			get { return (EntityField)EntityFieldFactory.Create(RoleUIElementSubPanelRightsFieldIndex.UiElementTypeNameFull);}
		}
		/// <summary>Creates a new RoleUIElementSubPanelRightsEntity.IsAllowed field instance</summary>
		public static EntityField IsAllowed
		{
			get { return (EntityField)EntityFieldFactory.Create(RoleUIElementSubPanelRightsFieldIndex.IsAllowed);}
		}
		/// <summary>Creates a new RoleUIElementSubPanelRightsEntity.Archived field instance</summary>
		public static EntityField Archived
		{
			get { return (EntityField)EntityFieldFactory.Create(RoleUIElementSubPanelRightsFieldIndex.Archived);}
		}
		/// <summary>Creates a new RoleUIElementSubPanelRightsEntity.Created field instance</summary>
		public static EntityField Created
		{
			get { return (EntityField)EntityFieldFactory.Create(RoleUIElementSubPanelRightsFieldIndex.Created);}
		}
		/// <summary>Creates a new RoleUIElementSubPanelRightsEntity.CreatedBy field instance</summary>
		public static EntityField CreatedBy
		{
			get { return (EntityField)EntityFieldFactory.Create(RoleUIElementSubPanelRightsFieldIndex.CreatedBy);}
		}
		/// <summary>Creates a new RoleUIElementSubPanelRightsEntity.Updated field instance</summary>
		public static EntityField Updated
		{
			get { return (EntityField)EntityFieldFactory.Create(RoleUIElementSubPanelRightsFieldIndex.Updated);}
		}
		/// <summary>Creates a new RoleUIElementSubPanelRightsEntity.UpdatedBy field instance</summary>
		public static EntityField UpdatedBy
		{
			get { return (EntityField)EntityFieldFactory.Create(RoleUIElementSubPanelRightsFieldIndex.UpdatedBy);}
		}
		/// <summary>Creates a new RoleUIElementSubPanelRightsEntity.Deleted field instance</summary>
		public static EntityField Deleted
		{
			get { return (EntityField)EntityFieldFactory.Create(RoleUIElementSubPanelRightsFieldIndex.Deleted);}
		}
		/// <summary>Creates a new RoleUIElementSubPanelRightsEntity.CreatedUTC field instance</summary>
		public static EntityField CreatedUTC
		{
			get { return (EntityField)EntityFieldFactory.Create(RoleUIElementSubPanelRightsFieldIndex.CreatedUTC);}
		}
		/// <summary>Creates a new RoleUIElementSubPanelRightsEntity.UpdatedUTC field instance</summary>
		public static EntityField UpdatedUTC
		{
			get { return (EntityField)EntityFieldFactory.Create(RoleUIElementSubPanelRightsFieldIndex.UpdatedUTC);}
		}
	}

	/// <summary>Field Creation Class for entity TimeZoneEntity</summary>
	public partial class TimeZoneFields
	{
		/// <summary>Creates a new TimeZoneEntity.TimeZoneId field instance</summary>
		public static EntityField TimeZoneId
		{
			get { return (EntityField)EntityFieldFactory.Create(TimeZoneFieldIndex.TimeZoneId);}
		}
		/// <summary>Creates a new TimeZoneEntity.Name field instance</summary>
		public static EntityField Name
		{
			get { return (EntityField)EntityFieldFactory.Create(TimeZoneFieldIndex.Name);}
		}
		/// <summary>Creates a new TimeZoneEntity.NameAndroid field instance</summary>
		public static EntityField NameAndroid
		{
			get { return (EntityField)EntityFieldFactory.Create(TimeZoneFieldIndex.NameAndroid);}
		}
		/// <summary>Creates a new TimeZoneEntity.NameDotNet field instance</summary>
		public static EntityField NameDotNet
		{
			get { return (EntityField)EntityFieldFactory.Create(TimeZoneFieldIndex.NameDotNet);}
		}
		/// <summary>Creates a new TimeZoneEntity.Created field instance</summary>
		public static EntityField Created
		{
			get { return (EntityField)EntityFieldFactory.Create(TimeZoneFieldIndex.Created);}
		}
		/// <summary>Creates a new TimeZoneEntity.CreatedBy field instance</summary>
		public static EntityField CreatedBy
		{
			get { return (EntityField)EntityFieldFactory.Create(TimeZoneFieldIndex.CreatedBy);}
		}
		/// <summary>Creates a new TimeZoneEntity.Updated field instance</summary>
		public static EntityField Updated
		{
			get { return (EntityField)EntityFieldFactory.Create(TimeZoneFieldIndex.Updated);}
		}
		/// <summary>Creates a new TimeZoneEntity.UpdatedBy field instance</summary>
		public static EntityField UpdatedBy
		{
			get { return (EntityField)EntityFieldFactory.Create(TimeZoneFieldIndex.UpdatedBy);}
		}
		/// <summary>Creates a new TimeZoneEntity.CreatedUTC field instance</summary>
		public static EntityField CreatedUTC
		{
			get { return (EntityField)EntityFieldFactory.Create(TimeZoneFieldIndex.CreatedUTC);}
		}
		/// <summary>Creates a new TimeZoneEntity.UpdatedUTC field instance</summary>
		public static EntityField UpdatedUTC
		{
			get { return (EntityField)EntityFieldFactory.Create(TimeZoneFieldIndex.UpdatedUTC);}
		}
	}

	/// <summary>Field Creation Class for entity TranslationEntity</summary>
	public partial class TranslationFields
	{
		/// <summary>Creates a new TranslationEntity.TranslationId field instance</summary>
		public static EntityField TranslationId
		{
			get { return (EntityField)EntityFieldFactory.Create(TranslationFieldIndex.TranslationId);}
		}
		/// <summary>Creates a new TranslationEntity.TranslationKey field instance</summary>
		public static EntityField TranslationKey
		{
			get { return (EntityField)EntityFieldFactory.Create(TranslationFieldIndex.TranslationKey);}
		}
		/// <summary>Creates a new TranslationEntity.LanguageCode field instance</summary>
		public static EntityField LanguageCode
		{
			get { return (EntityField)EntityFieldFactory.Create(TranslationFieldIndex.LanguageCode);}
		}
		/// <summary>Creates a new TranslationEntity.TranslationValue field instance</summary>
		public static EntityField TranslationValue
		{
			get { return (EntityField)EntityFieldFactory.Create(TranslationFieldIndex.TranslationValue);}
		}
		/// <summary>Creates a new TranslationEntity.LocallyCreated field instance</summary>
		public static EntityField LocallyCreated
		{
			get { return (EntityField)EntityFieldFactory.Create(TranslationFieldIndex.LocallyCreated);}
		}
		/// <summary>Creates a new TranslationEntity.Archived field instance</summary>
		public static EntityField Archived
		{
			get { return (EntityField)EntityFieldFactory.Create(TranslationFieldIndex.Archived);}
		}
		/// <summary>Creates a new TranslationEntity.Created field instance</summary>
		public static EntityField Created
		{
			get { return (EntityField)EntityFieldFactory.Create(TranslationFieldIndex.Created);}
		}
		/// <summary>Creates a new TranslationEntity.CreatedBy field instance</summary>
		public static EntityField CreatedBy
		{
			get { return (EntityField)EntityFieldFactory.Create(TranslationFieldIndex.CreatedBy);}
		}
		/// <summary>Creates a new TranslationEntity.Updated field instance</summary>
		public static EntityField Updated
		{
			get { return (EntityField)EntityFieldFactory.Create(TranslationFieldIndex.Updated);}
		}
		/// <summary>Creates a new TranslationEntity.UpdatedBy field instance</summary>
		public static EntityField UpdatedBy
		{
			get { return (EntityField)EntityFieldFactory.Create(TranslationFieldIndex.UpdatedBy);}
		}
		/// <summary>Creates a new TranslationEntity.Deleted field instance</summary>
		public static EntityField Deleted
		{
			get { return (EntityField)EntityFieldFactory.Create(TranslationFieldIndex.Deleted);}
		}
		/// <summary>Creates a new TranslationEntity.CreatedUTC field instance</summary>
		public static EntityField CreatedUTC
		{
			get { return (EntityField)EntityFieldFactory.Create(TranslationFieldIndex.CreatedUTC);}
		}
		/// <summary>Creates a new TranslationEntity.UpdatedUTC field instance</summary>
		public static EntityField UpdatedUTC
		{
			get { return (EntityField)EntityFieldFactory.Create(TranslationFieldIndex.UpdatedUTC);}
		}
	}

	/// <summary>Field Creation Class for entity UIColorEntity</summary>
	public partial class UIColorFields
	{
		/// <summary>Creates a new UIColorEntity.UIColorId field instance</summary>
		public static EntityField UIColorId
		{
			get { return (EntityField)EntityFieldFactory.Create(UIColorFieldIndex.UIColorId);}
		}
		/// <summary>Creates a new UIColorEntity.Type field instance</summary>
		public static EntityField Type
		{
			get { return (EntityField)EntityFieldFactory.Create(UIColorFieldIndex.Type);}
		}
		/// <summary>Creates a new UIColorEntity.Color field instance</summary>
		public static EntityField Color
		{
			get { return (EntityField)EntityFieldFactory.Create(UIColorFieldIndex.Color);}
		}
		/// <summary>Creates a new UIColorEntity.CreatedUTC field instance</summary>
		public static EntityField CreatedUTC
		{
			get { return (EntityField)EntityFieldFactory.Create(UIColorFieldIndex.CreatedUTC);}
		}
		/// <summary>Creates a new UIColorEntity.CreatedBy field instance</summary>
		public static EntityField CreatedBy
		{
			get { return (EntityField)EntityFieldFactory.Create(UIColorFieldIndex.CreatedBy);}
		}
		/// <summary>Creates a new UIColorEntity.UpdatedUTC field instance</summary>
		public static EntityField UpdatedUTC
		{
			get { return (EntityField)EntityFieldFactory.Create(UIColorFieldIndex.UpdatedUTC);}
		}
		/// <summary>Creates a new UIColorEntity.UpdatedBy field instance</summary>
		public static EntityField UpdatedBy
		{
			get { return (EntityField)EntityFieldFactory.Create(UIColorFieldIndex.UpdatedBy);}
		}
		/// <summary>Creates a new UIColorEntity.GroupName field instance</summary>
		public static EntityField GroupName
		{
			get { return (EntityField)EntityFieldFactory.Create(UIColorFieldIndex.GroupName);}
		}
	}

	/// <summary>Field Creation Class for entity UIElementEntity</summary>
	public partial class UIElementFields
	{
		/// <summary>Creates a new UIElementEntity.UIElementId field instance</summary>
		public static EntityField UIElementId
		{
			get { return (EntityField)EntityFieldFactory.Create(UIElementFieldIndex.UIElementId);}
		}
		/// <summary>Creates a new UIElementEntity.ModuleNameSystem field instance</summary>
		public static EntityField ModuleNameSystem
		{
			get { return (EntityField)EntityFieldFactory.Create(UIElementFieldIndex.ModuleNameSystem);}
		}
		/// <summary>Creates a new UIElementEntity.NameSystem field instance</summary>
		public static EntityField NameSystem
		{
			get { return (EntityField)EntityFieldFactory.Create(UIElementFieldIndex.NameSystem);}
		}
		/// <summary>Creates a new UIElementEntity.NameFull field instance</summary>
		public static EntityField NameFull
		{
			get { return (EntityField)EntityFieldFactory.Create(UIElementFieldIndex.NameFull);}
		}
		/// <summary>Creates a new UIElementEntity.NameShort field instance</summary>
		public static EntityField NameShort
		{
			get { return (EntityField)EntityFieldFactory.Create(UIElementFieldIndex.NameShort);}
		}
		/// <summary>Creates a new UIElementEntity.Url field instance</summary>
		public static EntityField Url
		{
			get { return (EntityField)EntityFieldFactory.Create(UIElementFieldIndex.Url);}
		}
		/// <summary>Creates a new UIElementEntity.DisplayInMenu field instance</summary>
		public static EntityField DisplayInMenu
		{
			get { return (EntityField)EntityFieldFactory.Create(UIElementFieldIndex.DisplayInMenu);}
		}
		/// <summary>Creates a new UIElementEntity.TypeNameFull field instance</summary>
		public static EntityField TypeNameFull
		{
			get { return (EntityField)EntityFieldFactory.Create(UIElementFieldIndex.TypeNameFull);}
		}
		/// <summary>Creates a new UIElementEntity.SortOrder field instance</summary>
		public static EntityField SortOrder
		{
			get { return (EntityField)EntityFieldFactory.Create(UIElementFieldIndex.SortOrder);}
		}
		/// <summary>Creates a new UIElementEntity.FreeAccess field instance</summary>
		public static EntityField FreeAccess
		{
			get { return (EntityField)EntityFieldFactory.Create(UIElementFieldIndex.FreeAccess);}
		}
		/// <summary>Creates a new UIElementEntity.LicensingFree field instance</summary>
		public static EntityField LicensingFree
		{
			get { return (EntityField)EntityFieldFactory.Create(UIElementFieldIndex.LicensingFree);}
		}
		/// <summary>Creates a new UIElementEntity.UserRightsFree field instance</summary>
		public static EntityField UserRightsFree
		{
			get { return (EntityField)EntityFieldFactory.Create(UIElementFieldIndex.UserRightsFree);}
		}
		/// <summary>Creates a new UIElementEntity.IsConfigurationItem field instance</summary>
		public static EntityField IsConfigurationItem
		{
			get { return (EntityField)EntityFieldFactory.Create(UIElementFieldIndex.IsConfigurationItem);}
		}
		/// <summary>Creates a new UIElementEntity.EntityName field instance</summary>
		public static EntityField EntityName
		{
			get { return (EntityField)EntityFieldFactory.Create(UIElementFieldIndex.EntityName);}
		}
		/// <summary>Creates a new UIElementEntity.ForLocalUseWhileSyncingIsUpdated field instance</summary>
		public static EntityField ForLocalUseWhileSyncingIsUpdated
		{
			get { return (EntityField)EntityFieldFactory.Create(UIElementFieldIndex.ForLocalUseWhileSyncingIsUpdated);}
		}
		/// <summary>Creates a new UIElementEntity.Deleted field instance</summary>
		public static EntityField Deleted
		{
			get { return (EntityField)EntityFieldFactory.Create(UIElementFieldIndex.Deleted);}
		}
		/// <summary>Creates a new UIElementEntity.Archived field instance</summary>
		public static EntityField Archived
		{
			get { return (EntityField)EntityFieldFactory.Create(UIElementFieldIndex.Archived);}
		}
		/// <summary>Creates a new UIElementEntity.Created field instance</summary>
		public static EntityField Created
		{
			get { return (EntityField)EntityFieldFactory.Create(UIElementFieldIndex.Created);}
		}
		/// <summary>Creates a new UIElementEntity.CreatedBy field instance</summary>
		public static EntityField CreatedBy
		{
			get { return (EntityField)EntityFieldFactory.Create(UIElementFieldIndex.CreatedBy);}
		}
		/// <summary>Creates a new UIElementEntity.Updated field instance</summary>
		public static EntityField Updated
		{
			get { return (EntityField)EntityFieldFactory.Create(UIElementFieldIndex.Updated);}
		}
		/// <summary>Creates a new UIElementEntity.UpdatedBy field instance</summary>
		public static EntityField UpdatedBy
		{
			get { return (EntityField)EntityFieldFactory.Create(UIElementFieldIndex.UpdatedBy);}
		}
		/// <summary>Creates a new UIElementEntity.CreatedUTC field instance</summary>
		public static EntityField CreatedUTC
		{
			get { return (EntityField)EntityFieldFactory.Create(UIElementFieldIndex.CreatedUTC);}
		}
		/// <summary>Creates a new UIElementEntity.UpdatedUTC field instance</summary>
		public static EntityField UpdatedUTC
		{
			get { return (EntityField)EntityFieldFactory.Create(UIElementFieldIndex.UpdatedUTC);}
		}
	}

	/// <summary>Field Creation Class for entity UIElementSubPanelEntity</summary>
	public partial class UIElementSubPanelFields
	{
		/// <summary>Creates a new UIElementSubPanelEntity.UIElementSubPanelId field instance</summary>
		public static EntityField UIElementSubPanelId
		{
			get { return (EntityField)EntityFieldFactory.Create(UIElementSubPanelFieldIndex.UIElementSubPanelId);}
		}
		/// <summary>Creates a new UIElementSubPanelEntity.NameSystem field instance</summary>
		public static EntityField NameSystem
		{
			get { return (EntityField)EntityFieldFactory.Create(UIElementSubPanelFieldIndex.NameSystem);}
		}
		/// <summary>Creates a new UIElementSubPanelEntity.NameFull field instance</summary>
		public static EntityField NameFull
		{
			get { return (EntityField)EntityFieldFactory.Create(UIElementSubPanelFieldIndex.NameFull);}
		}
		/// <summary>Creates a new UIElementSubPanelEntity.NameShort field instance</summary>
		public static EntityField NameShort
		{
			get { return (EntityField)EntityFieldFactory.Create(UIElementSubPanelFieldIndex.NameShort);}
		}
		/// <summary>Creates a new UIElementSubPanelEntity.Url field instance</summary>
		public static EntityField Url
		{
			get { return (EntityField)EntityFieldFactory.Create(UIElementSubPanelFieldIndex.Url);}
		}
		/// <summary>Creates a new UIElementSubPanelEntity.TypeNameFull field instance</summary>
		public static EntityField TypeNameFull
		{
			get { return (EntityField)EntityFieldFactory.Create(UIElementSubPanelFieldIndex.TypeNameFull);}
		}
		/// <summary>Creates a new UIElementSubPanelEntity.SortOrder field instance</summary>
		public static EntityField SortOrder
		{
			get { return (EntityField)EntityFieldFactory.Create(UIElementSubPanelFieldIndex.SortOrder);}
		}
		/// <summary>Creates a new UIElementSubPanelEntity.FreeAccess field instance</summary>
		public static EntityField FreeAccess
		{
			get { return (EntityField)EntityFieldFactory.Create(UIElementSubPanelFieldIndex.FreeAccess);}
		}
		/// <summary>Creates a new UIElementSubPanelEntity.LicensingFree field instance</summary>
		public static EntityField LicensingFree
		{
			get { return (EntityField)EntityFieldFactory.Create(UIElementSubPanelFieldIndex.LicensingFree);}
		}
		/// <summary>Creates a new UIElementSubPanelEntity.EntityName field instance</summary>
		public static EntityField EntityName
		{
			get { return (EntityField)EntityFieldFactory.Create(UIElementSubPanelFieldIndex.EntityName);}
		}
		/// <summary>Creates a new UIElementSubPanelEntity.OnTabPage field instance</summary>
		public static EntityField OnTabPage
		{
			get { return (EntityField)EntityFieldFactory.Create(UIElementSubPanelFieldIndex.OnTabPage);}
		}
		/// <summary>Creates a new UIElementSubPanelEntity.ForLocalUseWhileSyncingIsUpdated field instance</summary>
		public static EntityField ForLocalUseWhileSyncingIsUpdated
		{
			get { return (EntityField)EntityFieldFactory.Create(UIElementSubPanelFieldIndex.ForLocalUseWhileSyncingIsUpdated);}
		}
		/// <summary>Creates a new UIElementSubPanelEntity.Deleted field instance</summary>
		public static EntityField Deleted
		{
			get { return (EntityField)EntityFieldFactory.Create(UIElementSubPanelFieldIndex.Deleted);}
		}
		/// <summary>Creates a new UIElementSubPanelEntity.Archived field instance</summary>
		public static EntityField Archived
		{
			get { return (EntityField)EntityFieldFactory.Create(UIElementSubPanelFieldIndex.Archived);}
		}
		/// <summary>Creates a new UIElementSubPanelEntity.Created field instance</summary>
		public static EntityField Created
		{
			get { return (EntityField)EntityFieldFactory.Create(UIElementSubPanelFieldIndex.Created);}
		}
		/// <summary>Creates a new UIElementSubPanelEntity.CreatedBy field instance</summary>
		public static EntityField CreatedBy
		{
			get { return (EntityField)EntityFieldFactory.Create(UIElementSubPanelFieldIndex.CreatedBy);}
		}
		/// <summary>Creates a new UIElementSubPanelEntity.Updated field instance</summary>
		public static EntityField Updated
		{
			get { return (EntityField)EntityFieldFactory.Create(UIElementSubPanelFieldIndex.Updated);}
		}
		/// <summary>Creates a new UIElementSubPanelEntity.UpdatedBy field instance</summary>
		public static EntityField UpdatedBy
		{
			get { return (EntityField)EntityFieldFactory.Create(UIElementSubPanelFieldIndex.UpdatedBy);}
		}
		/// <summary>Creates a new UIElementSubPanelEntity.CreatedUTC field instance</summary>
		public static EntityField CreatedUTC
		{
			get { return (EntityField)EntityFieldFactory.Create(UIElementSubPanelFieldIndex.CreatedUTC);}
		}
		/// <summary>Creates a new UIElementSubPanelEntity.UpdatedUTC field instance</summary>
		public static EntityField UpdatedUTC
		{
			get { return (EntityField)EntityFieldFactory.Create(UIElementSubPanelFieldIndex.UpdatedUTC);}
		}
	}

	/// <summary>Field Creation Class for entity UIElementSubPanelUIElementEntity</summary>
	public partial class UIElementSubPanelUIElementFields
	{
		/// <summary>Creates a new UIElementSubPanelUIElementEntity.UIElementSubPanelUIElementId field instance</summary>
		public static EntityField UIElementSubPanelUIElementId
		{
			get { return (EntityField)EntityFieldFactory.Create(UIElementSubPanelUIElementFieldIndex.UIElementSubPanelUIElementId);}
		}
		/// <summary>Creates a new UIElementSubPanelUIElementEntity.UIElementSubPanelTypeNameFull field instance</summary>
		public static EntityField UIElementSubPanelTypeNameFull
		{
			get { return (EntityField)EntityFieldFactory.Create(UIElementSubPanelUIElementFieldIndex.UIElementSubPanelTypeNameFull);}
		}
		/// <summary>Creates a new UIElementSubPanelUIElementEntity.ParentUIElementTypeNameFull field instance</summary>
		public static EntityField ParentUIElementTypeNameFull
		{
			get { return (EntityField)EntityFieldFactory.Create(UIElementSubPanelUIElementFieldIndex.ParentUIElementTypeNameFull);}
		}
		/// <summary>Creates a new UIElementSubPanelUIElementEntity.ParentUIElementSubPanelTypeNameFull field instance</summary>
		public static EntityField ParentUIElementSubPanelTypeNameFull
		{
			get { return (EntityField)EntityFieldFactory.Create(UIElementSubPanelUIElementFieldIndex.ParentUIElementSubPanelTypeNameFull);}
		}
		/// <summary>Creates a new UIElementSubPanelUIElementEntity.ForLocalUseWhileSyncingIsUpdated field instance</summary>
		public static EntityField ForLocalUseWhileSyncingIsUpdated
		{
			get { return (EntityField)EntityFieldFactory.Create(UIElementSubPanelUIElementFieldIndex.ForLocalUseWhileSyncingIsUpdated);}
		}
		/// <summary>Creates a new UIElementSubPanelUIElementEntity.Archived field instance</summary>
		public static EntityField Archived
		{
			get { return (EntityField)EntityFieldFactory.Create(UIElementSubPanelUIElementFieldIndex.Archived);}
		}
		/// <summary>Creates a new UIElementSubPanelUIElementEntity.Created field instance</summary>
		public static EntityField Created
		{
			get { return (EntityField)EntityFieldFactory.Create(UIElementSubPanelUIElementFieldIndex.Created);}
		}
		/// <summary>Creates a new UIElementSubPanelUIElementEntity.CreatedBy field instance</summary>
		public static EntityField CreatedBy
		{
			get { return (EntityField)EntityFieldFactory.Create(UIElementSubPanelUIElementFieldIndex.CreatedBy);}
		}
		/// <summary>Creates a new UIElementSubPanelUIElementEntity.Updated field instance</summary>
		public static EntityField Updated
		{
			get { return (EntityField)EntityFieldFactory.Create(UIElementSubPanelUIElementFieldIndex.Updated);}
		}
		/// <summary>Creates a new UIElementSubPanelUIElementEntity.UpdatedBy field instance</summary>
		public static EntityField UpdatedBy
		{
			get { return (EntityField)EntityFieldFactory.Create(UIElementSubPanelUIElementFieldIndex.UpdatedBy);}
		}
		/// <summary>Creates a new UIElementSubPanelUIElementEntity.CreatedUTC field instance</summary>
		public static EntityField CreatedUTC
		{
			get { return (EntityField)EntityFieldFactory.Create(UIElementSubPanelUIElementFieldIndex.CreatedUTC);}
		}
		/// <summary>Creates a new UIElementSubPanelUIElementEntity.UpdatedUTC field instance</summary>
		public static EntityField UpdatedUTC
		{
			get { return (EntityField)EntityFieldFactory.Create(UIElementSubPanelUIElementFieldIndex.UpdatedUTC);}
		}
	}

	/// <summary>Field Creation Class for entity ViewEntity</summary>
	public partial class ViewFields
	{
		/// <summary>Creates a new ViewEntity.ViewId field instance</summary>
		public static EntityField ViewId
		{
			get { return (EntityField)EntityFieldFactory.Create(ViewFieldIndex.ViewId);}
		}
		/// <summary>Creates a new ViewEntity.Name field instance</summary>
		public static EntityField Name
		{
			get { return (EntityField)EntityFieldFactory.Create(ViewFieldIndex.Name);}
		}
		/// <summary>Creates a new ViewEntity.EntityName field instance</summary>
		public static EntityField EntityName
		{
			get { return (EntityField)EntityFieldFactory.Create(ViewFieldIndex.EntityName);}
		}
		/// <summary>Creates a new ViewEntity.UiElementTypeNameFull field instance</summary>
		public static EntityField UiElementTypeNameFull
		{
			get { return (EntityField)EntityFieldFactory.Create(ViewFieldIndex.UiElementTypeNameFull);}
		}
		/// <summary>Creates a new ViewEntity.ForLocalUseWhileSyncingIsUpdated field instance</summary>
		public static EntityField ForLocalUseWhileSyncingIsUpdated
		{
			get { return (EntityField)EntityFieldFactory.Create(ViewFieldIndex.ForLocalUseWhileSyncingIsUpdated);}
		}
		/// <summary>Creates a new ViewEntity.Archived field instance</summary>
		public static EntityField Archived
		{
			get { return (EntityField)EntityFieldFactory.Create(ViewFieldIndex.Archived);}
		}
		/// <summary>Creates a new ViewEntity.Created field instance</summary>
		public static EntityField Created
		{
			get { return (EntityField)EntityFieldFactory.Create(ViewFieldIndex.Created);}
		}
		/// <summary>Creates a new ViewEntity.CreatedBy field instance</summary>
		public static EntityField CreatedBy
		{
			get { return (EntityField)EntityFieldFactory.Create(ViewFieldIndex.CreatedBy);}
		}
		/// <summary>Creates a new ViewEntity.Updated field instance</summary>
		public static EntityField Updated
		{
			get { return (EntityField)EntityFieldFactory.Create(ViewFieldIndex.Updated);}
		}
		/// <summary>Creates a new ViewEntity.UpdatedBy field instance</summary>
		public static EntityField UpdatedBy
		{
			get { return (EntityField)EntityFieldFactory.Create(ViewFieldIndex.UpdatedBy);}
		}
		/// <summary>Creates a new ViewEntity.Deleted field instance</summary>
		public static EntityField Deleted
		{
			get { return (EntityField)EntityFieldFactory.Create(ViewFieldIndex.Deleted);}
		}
		/// <summary>Creates a new ViewEntity.CreatedUTC field instance</summary>
		public static EntityField CreatedUTC
		{
			get { return (EntityField)EntityFieldFactory.Create(ViewFieldIndex.CreatedUTC);}
		}
		/// <summary>Creates a new ViewEntity.UpdatedUTC field instance</summary>
		public static EntityField UpdatedUTC
		{
			get { return (EntityField)EntityFieldFactory.Create(ViewFieldIndex.UpdatedUTC);}
		}
	}

	/// <summary>Field Creation Class for entity ViewItemEntity</summary>
	public partial class ViewItemFields
	{
		/// <summary>Creates a new ViewItemEntity.ViewItemId field instance</summary>
		public static EntityField ViewItemId
		{
			get { return (EntityField)EntityFieldFactory.Create(ViewItemFieldIndex.ViewItemId);}
		}
		/// <summary>Creates a new ViewItemEntity.ViewId field instance</summary>
		public static EntityField ViewId
		{
			get { return (EntityField)EntityFieldFactory.Create(ViewItemFieldIndex.ViewId);}
		}
		/// <summary>Creates a new ViewItemEntity.EntityName field instance</summary>
		public static EntityField EntityName
		{
			get { return (EntityField)EntityFieldFactory.Create(ViewItemFieldIndex.EntityName);}
		}
		/// <summary>Creates a new ViewItemEntity.FieldName field instance</summary>
		public static EntityField FieldName
		{
			get { return (EntityField)EntityFieldFactory.Create(ViewItemFieldIndex.FieldName);}
		}
		/// <summary>Creates a new ViewItemEntity.ShowOnGridView field instance</summary>
		public static EntityField ShowOnGridView
		{
			get { return (EntityField)EntityFieldFactory.Create(ViewItemFieldIndex.ShowOnGridView);}
		}
		/// <summary>Creates a new ViewItemEntity.DisplayPosition field instance</summary>
		public static EntityField DisplayPosition
		{
			get { return (EntityField)EntityFieldFactory.Create(ViewItemFieldIndex.DisplayPosition);}
		}
		/// <summary>Creates a new ViewItemEntity.SortPosition field instance</summary>
		public static EntityField SortPosition
		{
			get { return (EntityField)EntityFieldFactory.Create(ViewItemFieldIndex.SortPosition);}
		}
		/// <summary>Creates a new ViewItemEntity.SortOperator field instance</summary>
		public static EntityField SortOperator
		{
			get { return (EntityField)EntityFieldFactory.Create(ViewItemFieldIndex.SortOperator);}
		}
		/// <summary>Creates a new ViewItemEntity.DisplayFormatString field instance</summary>
		public static EntityField DisplayFormatString
		{
			get { return (EntityField)EntityFieldFactory.Create(ViewItemFieldIndex.DisplayFormatString);}
		}
		/// <summary>Creates a new ViewItemEntity.Width field instance</summary>
		public static EntityField Width
		{
			get { return (EntityField)EntityFieldFactory.Create(ViewItemFieldIndex.Width);}
		}
		/// <summary>Creates a new ViewItemEntity.Type field instance</summary>
		public static EntityField Type
		{
			get { return (EntityField)EntityFieldFactory.Create(ViewItemFieldIndex.Type);}
		}
		/// <summary>Creates a new ViewItemEntity.UiElementTypeNameFull field instance</summary>
		public static EntityField UiElementTypeNameFull
		{
			get { return (EntityField)EntityFieldFactory.Create(ViewItemFieldIndex.UiElementTypeNameFull);}
		}
		/// <summary>Creates a new ViewItemEntity.ForLocalUseWhileSyncingIsUpdated field instance</summary>
		public static EntityField ForLocalUseWhileSyncingIsUpdated
		{
			get { return (EntityField)EntityFieldFactory.Create(ViewItemFieldIndex.ForLocalUseWhileSyncingIsUpdated);}
		}
		/// <summary>Creates a new ViewItemEntity.Archived field instance</summary>
		public static EntityField Archived
		{
			get { return (EntityField)EntityFieldFactory.Create(ViewItemFieldIndex.Archived);}
		}
		/// <summary>Creates a new ViewItemEntity.Created field instance</summary>
		public static EntityField Created
		{
			get { return (EntityField)EntityFieldFactory.Create(ViewItemFieldIndex.Created);}
		}
		/// <summary>Creates a new ViewItemEntity.CreatedBy field instance</summary>
		public static EntityField CreatedBy
		{
			get { return (EntityField)EntityFieldFactory.Create(ViewItemFieldIndex.CreatedBy);}
		}
		/// <summary>Creates a new ViewItemEntity.Updated field instance</summary>
		public static EntityField Updated
		{
			get { return (EntityField)EntityFieldFactory.Create(ViewItemFieldIndex.Updated);}
		}
		/// <summary>Creates a new ViewItemEntity.UpdatedBy field instance</summary>
		public static EntityField UpdatedBy
		{
			get { return (EntityField)EntityFieldFactory.Create(ViewItemFieldIndex.UpdatedBy);}
		}
		/// <summary>Creates a new ViewItemEntity.Deleted field instance</summary>
		public static EntityField Deleted
		{
			get { return (EntityField)EntityFieldFactory.Create(ViewItemFieldIndex.Deleted);}
		}
		/// <summary>Creates a new ViewItemEntity.CreatedUTC field instance</summary>
		public static EntityField CreatedUTC
		{
			get { return (EntityField)EntityFieldFactory.Create(ViewItemFieldIndex.CreatedUTC);}
		}
		/// <summary>Creates a new ViewItemEntity.UpdatedUTC field instance</summary>
		public static EntityField UpdatedUTC
		{
			get { return (EntityField)EntityFieldFactory.Create(ViewItemFieldIndex.UpdatedUTC);}
		}
	}
	

}