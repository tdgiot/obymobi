﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.Master.HelperClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	
	/// <summary>Singleton implementation of the FieldInfoProvider. This class is the singleton wrapper through which the actual instance is retrieved.</summary>
	/// <remarks>It uses a single instance of an internal class. The access isn't marked with locks as the FieldInfoProviderBase class is threadsafe.</remarks>
	internal static class FieldInfoProviderSingleton
	{
		#region Class Member Declarations
		private static readonly IFieldInfoProvider _providerInstance = new FieldInfoProviderCore();
		#endregion

		/// <summary>Dummy static constructor to make sure threadsafe initialization is performed.</summary>
		static FieldInfoProviderSingleton()
		{
		}

		/// <summary>Gets the singleton instance of the FieldInfoProviderCore</summary>
		/// <returns>Instance of the FieldInfoProvider.</returns>
		public static IFieldInfoProvider GetInstance()
		{
			return _providerInstance;
		}
	}

	/// <summary>Actual implementation of the FieldInfoProvider. Used by singleton wrapper.</summary>
	internal class FieldInfoProviderCore : FieldInfoProviderBase
	{
		/// <summary>Initializes a new instance of the <see cref="FieldInfoProviderCore"/> class.</summary>
		internal FieldInfoProviderCore()
		{
			Init();
		}

		/// <summary>Method which initializes the internal datastores.</summary>
		private void Init()
		{
			this.InitClass( (19 + 0));
			InitCountryEntityInfos();
			InitCurrencyEntityInfos();
			InitEntityFieldInformationEntityInfos();
			InitEntityInformationEntityInfos();
			InitModuleEntityInfos();
			InitReferentialConstraintEntityInfos();
			InitRoleEntityInfos();
			InitRoleEntityRightsEntityInfos();
			InitRoleModuleRightsEntityInfos();
			InitRoleUIElementRightsEntityInfos();
			InitRoleUIElementSubPanelRightsEntityInfos();
			InitTimeZoneEntityInfos();
			InitTranslationEntityInfos();
			InitUIColorEntityInfos();
			InitUIElementEntityInfos();
			InitUIElementSubPanelEntityInfos();
			InitUIElementSubPanelUIElementEntityInfos();
			InitViewEntityInfos();
			InitViewItemEntityInfos();

			this.ConstructElementFieldStructures(InheritanceInfoProviderSingleton.GetInstance());
		}

		/// <summary>Inits CountryEntity's FieldInfo objects</summary>
		private void InitCountryEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(CountryFieldIndex), "CountryEntity");
			this.AddElementFieldInfo("CountryEntity", "CountryId", typeof(System.Int32), true, false, false, false,  (int)CountryFieldIndex.CountryId, 0, 0, 10);
			this.AddElementFieldInfo("CountryEntity", "Name", typeof(System.String), false, false, false, true,  (int)CountryFieldIndex.Name, 50, 0, 0);
			this.AddElementFieldInfo("CountryEntity", "Code", typeof(System.String), false, false, false, true,  (int)CountryFieldIndex.Code, 2, 0, 0);
			this.AddElementFieldInfo("CountryEntity", "CodeAlpha3", typeof(System.String), false, false, false, true,  (int)CountryFieldIndex.CodeAlpha3, 3, 0, 0);
			this.AddElementFieldInfo("CountryEntity", "CultureName", typeof(System.String), false, false, false, true,  (int)CountryFieldIndex.CultureName, 10, 0, 0);
			this.AddElementFieldInfo("CountryEntity", "CurrencyId", typeof(System.Int32), false, true, false, false,  (int)CountryFieldIndex.CurrencyId, 0, 0, 10);
			this.AddElementFieldInfo("CountryEntity", "Created", typeof(Nullable<System.DateTime>), false, false, false, true,  (int)CountryFieldIndex.Created, 0, 0, 0);
			this.AddElementFieldInfo("CountryEntity", "CreatedBy", typeof(System.Int32), false, false, false, false,  (int)CountryFieldIndex.CreatedBy, 0, 0, 10);
			this.AddElementFieldInfo("CountryEntity", "Updated", typeof(Nullable<System.DateTime>), false, false, false, true,  (int)CountryFieldIndex.Updated, 0, 0, 0);
			this.AddElementFieldInfo("CountryEntity", "UpdatedBy", typeof(System.Int32), false, false, false, false,  (int)CountryFieldIndex.UpdatedBy, 0, 0, 10);
			this.AddElementFieldInfo("CountryEntity", "CreatedUTC", typeof(Nullable<System.DateTime>), false, false, false, true,  (int)CountryFieldIndex.CreatedUTC, 0, 0, 0);
			this.AddElementFieldInfo("CountryEntity", "UpdatedUTC", typeof(Nullable<System.DateTime>), false, false, false, true,  (int)CountryFieldIndex.UpdatedUTC, 0, 0, 0);
		}
		/// <summary>Inits CurrencyEntity's FieldInfo objects</summary>
		private void InitCurrencyEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(CurrencyFieldIndex), "CurrencyEntity");
			this.AddElementFieldInfo("CurrencyEntity", "CurrencyId", typeof(System.Int32), true, false, false, false,  (int)CurrencyFieldIndex.CurrencyId, 0, 0, 10);
			this.AddElementFieldInfo("CurrencyEntity", "Name", typeof(System.String), false, false, false, true,  (int)CurrencyFieldIndex.Name, 50, 0, 0);
			this.AddElementFieldInfo("CurrencyEntity", "Symbol", typeof(System.String), false, false, false, true,  (int)CurrencyFieldIndex.Symbol, 50, 0, 0);
			this.AddElementFieldInfo("CurrencyEntity", "CodeIso4217", typeof(System.String), false, false, false, false,  (int)CurrencyFieldIndex.CodeIso4217, 3, 0, 0);
			this.AddElementFieldInfo("CurrencyEntity", "Created", typeof(Nullable<System.DateTime>), false, false, false, true,  (int)CurrencyFieldIndex.Created, 0, 0, 0);
			this.AddElementFieldInfo("CurrencyEntity", "CreatedBy", typeof(System.Int32), false, false, false, false,  (int)CurrencyFieldIndex.CreatedBy, 0, 0, 10);
			this.AddElementFieldInfo("CurrencyEntity", "Updated", typeof(Nullable<System.DateTime>), false, false, false, true,  (int)CurrencyFieldIndex.Updated, 0, 0, 0);
			this.AddElementFieldInfo("CurrencyEntity", "UpdatedBy", typeof(System.Int32), false, false, false, false,  (int)CurrencyFieldIndex.UpdatedBy, 0, 0, 10);
			this.AddElementFieldInfo("CurrencyEntity", "CreatedUTC", typeof(Nullable<System.DateTime>), false, false, false, true,  (int)CurrencyFieldIndex.CreatedUTC, 0, 0, 0);
			this.AddElementFieldInfo("CurrencyEntity", "UpdatedUTC", typeof(Nullable<System.DateTime>), false, false, false, true,  (int)CurrencyFieldIndex.UpdatedUTC, 0, 0, 0);
		}
		/// <summary>Inits EntityFieldInformationEntity's FieldInfo objects</summary>
		private void InitEntityFieldInformationEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(EntityFieldInformationFieldIndex), "EntityFieldInformationEntity");
			this.AddElementFieldInfo("EntityFieldInformationEntity", "EntityFieldInformationId", typeof(System.Int32), true, false, true, false,  (int)EntityFieldInformationFieldIndex.EntityFieldInformationId, 0, 0, 10);
			this.AddElementFieldInfo("EntityFieldInformationEntity", "EntityName", typeof(System.String), false, false, false, true,  (int)EntityFieldInformationFieldIndex.EntityName, 150, 0, 0);
			this.AddElementFieldInfo("EntityFieldInformationEntity", "FieldName", typeof(System.String), false, false, false, true,  (int)EntityFieldInformationFieldIndex.FieldName, 255, 0, 0);
			this.AddElementFieldInfo("EntityFieldInformationEntity", "FriendlyName", typeof(System.String), false, false, false, true,  (int)EntityFieldInformationFieldIndex.FriendlyName, 100, 0, 0);
			this.AddElementFieldInfo("EntityFieldInformationEntity", "ShowOnDefaultGridView", typeof(System.Boolean), false, false, false, false,  (int)EntityFieldInformationFieldIndex.ShowOnDefaultGridView, 0, 0, 0);
			this.AddElementFieldInfo("EntityFieldInformationEntity", "DefaultDisplayPosition", typeof(System.Int32), false, false, false, false,  (int)EntityFieldInformationFieldIndex.DefaultDisplayPosition, 0, 0, 10);
			this.AddElementFieldInfo("EntityFieldInformationEntity", "AllowShowOnGridView", typeof(System.Boolean), false, false, false, false,  (int)EntityFieldInformationFieldIndex.AllowShowOnGridView, 0, 0, 0);
			this.AddElementFieldInfo("EntityFieldInformationEntity", "HelpText", typeof(System.String), false, false, false, true,  (int)EntityFieldInformationFieldIndex.HelpText, 2147483647, 0, 0);
			this.AddElementFieldInfo("EntityFieldInformationEntity", "DisplayFormatString", typeof(System.String), false, false, false, true,  (int)EntityFieldInformationFieldIndex.DisplayFormatString, 100, 0, 0);
			this.AddElementFieldInfo("EntityFieldInformationEntity", "DefaultSortPosition", typeof(System.Int32), false, false, false, false,  (int)EntityFieldInformationFieldIndex.DefaultSortPosition, 0, 0, 10);
			this.AddElementFieldInfo("EntityFieldInformationEntity", "DefaultSortOperator", typeof(System.Int32), false, false, false, false,  (int)EntityFieldInformationFieldIndex.DefaultSortOperator, 0, 0, 10);
			this.AddElementFieldInfo("EntityFieldInformationEntity", "Type", typeof(System.String), false, false, false, true,  (int)EntityFieldInformationFieldIndex.Type, 100, 0, 0);
			this.AddElementFieldInfo("EntityFieldInformationEntity", "IsRequired", typeof(Nullable<System.Boolean>), false, false, false, true,  (int)EntityFieldInformationFieldIndex.IsRequired, 0, 0, 0);
			this.AddElementFieldInfo("EntityFieldInformationEntity", "EntityFieldType", typeof(System.String), false, false, false, true,  (int)EntityFieldInformationFieldIndex.EntityFieldType, 50, 0, 0);
			this.AddElementFieldInfo("EntityFieldInformationEntity", "ExcludeForUpdateWmsCacheDateOnChange", typeof(System.Boolean), false, false, false, false,  (int)EntityFieldInformationFieldIndex.ExcludeForUpdateWmsCacheDateOnChange, 0, 0, 0);
			this.AddElementFieldInfo("EntityFieldInformationEntity", "ExcludeForUpdateCompanyDataVersionOnChange", typeof(System.Boolean), false, false, false, false,  (int)EntityFieldInformationFieldIndex.ExcludeForUpdateCompanyDataVersionOnChange, 0, 0, 0);
			this.AddElementFieldInfo("EntityFieldInformationEntity", "ExcludeForUpdateMenuDataVersionOnChange", typeof(System.Boolean), false, false, false, false,  (int)EntityFieldInformationFieldIndex.ExcludeForUpdateMenuDataVersionOnChange, 0, 0, 0);
			this.AddElementFieldInfo("EntityFieldInformationEntity", "ExcludeForUpdatePosIntegrationInformationVersionOnChange", typeof(System.Boolean), false, false, false, false,  (int)EntityFieldInformationFieldIndex.ExcludeForUpdatePosIntegrationInformationVersionOnChange, 0, 0, 0);
			this.AddElementFieldInfo("EntityFieldInformationEntity", "ForLocalUseWhileSyncingIsUpdated", typeof(System.Boolean), false, false, false, false,  (int)EntityFieldInformationFieldIndex.ForLocalUseWhileSyncingIsUpdated, 0, 0, 0);
			this.AddElementFieldInfo("EntityFieldInformationEntity", "Archived", typeof(System.Boolean), false, false, false, false,  (int)EntityFieldInformationFieldIndex.Archived, 0, 0, 0);
			this.AddElementFieldInfo("EntityFieldInformationEntity", "Created", typeof(Nullable<System.DateTime>), false, false, false, true,  (int)EntityFieldInformationFieldIndex.Created, 0, 0, 0);
			this.AddElementFieldInfo("EntityFieldInformationEntity", "CreatedBy", typeof(System.Int32), false, false, false, false,  (int)EntityFieldInformationFieldIndex.CreatedBy, 0, 0, 10);
			this.AddElementFieldInfo("EntityFieldInformationEntity", "Updated", typeof(Nullable<System.DateTime>), false, false, false, true,  (int)EntityFieldInformationFieldIndex.Updated, 0, 0, 0);
			this.AddElementFieldInfo("EntityFieldInformationEntity", "UpdatedBy", typeof(System.Int32), false, false, false, false,  (int)EntityFieldInformationFieldIndex.UpdatedBy, 0, 0, 10);
			this.AddElementFieldInfo("EntityFieldInformationEntity", "Deleted", typeof(System.Boolean), false, false, false, false,  (int)EntityFieldInformationFieldIndex.Deleted, 0, 0, 0);
			this.AddElementFieldInfo("EntityFieldInformationEntity", "ExcludeForUpdateDeliverypointDataVersionOnChange", typeof(System.Boolean), false, false, false, false,  (int)EntityFieldInformationFieldIndex.ExcludeForUpdateDeliverypointDataVersionOnChange, 0, 0, 0);
			this.AddElementFieldInfo("EntityFieldInformationEntity", "ExcludeForUpdateSurveyDataVersionOnChange", typeof(System.Boolean), false, false, false, false,  (int)EntityFieldInformationFieldIndex.ExcludeForUpdateSurveyDataVersionOnChange, 0, 0, 0);
			this.AddElementFieldInfo("EntityFieldInformationEntity", "ExcludeForUpdateAnnouncementDataVersionOnChange", typeof(System.Boolean), false, false, false, false,  (int)EntityFieldInformationFieldIndex.ExcludeForUpdateAnnouncementDataVersionOnChange, 0, 0, 0);
			this.AddElementFieldInfo("EntityFieldInformationEntity", "ExcludeForUpdateEntertainmentDataVersionOnChange", typeof(System.Boolean), false, false, false, false,  (int)EntityFieldInformationFieldIndex.ExcludeForUpdateEntertainmentDataVersionOnChange, 0, 0, 0);
			this.AddElementFieldInfo("EntityFieldInformationEntity", "ExcludeForUpdateAdvertisementDataVersionOnChange", typeof(System.Boolean), false, false, false, false,  (int)EntityFieldInformationFieldIndex.ExcludeForUpdateAdvertisementDataVersionOnChange, 0, 0, 0);
			this.AddElementFieldInfo("EntityFieldInformationEntity", "CreatedUTC", typeof(Nullable<System.DateTime>), false, false, false, true,  (int)EntityFieldInformationFieldIndex.CreatedUTC, 0, 0, 0);
			this.AddElementFieldInfo("EntityFieldInformationEntity", "UpdatedUTC", typeof(Nullable<System.DateTime>), false, false, false, true,  (int)EntityFieldInformationFieldIndex.UpdatedUTC, 0, 0, 0);
		}
		/// <summary>Inits EntityInformationEntity's FieldInfo objects</summary>
		private void InitEntityInformationEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(EntityInformationFieldIndex), "EntityInformationEntity");
			this.AddElementFieldInfo("EntityInformationEntity", "EntityInformationId", typeof(System.Int32), true, false, true, false,  (int)EntityInformationFieldIndex.EntityInformationId, 0, 0, 10);
			this.AddElementFieldInfo("EntityInformationEntity", "EntityName", typeof(System.String), false, false, false, false,  (int)EntityInformationFieldIndex.EntityName, 250, 0, 0);
			this.AddElementFieldInfo("EntityInformationEntity", "ShowFieldName", typeof(System.String), false, false, false, true,  (int)EntityInformationFieldIndex.ShowFieldName, 250, 0, 0);
			this.AddElementFieldInfo("EntityInformationEntity", "ShowFieldNameBreadCrumb", typeof(System.String), false, false, false, true,  (int)EntityInformationFieldIndex.ShowFieldNameBreadCrumb, 250, 0, 0);
			this.AddElementFieldInfo("EntityInformationEntity", "FriendlyName", typeof(System.String), false, false, false, true,  (int)EntityInformationFieldIndex.FriendlyName, 150, 0, 0);
			this.AddElementFieldInfo("EntityInformationEntity", "FriendlyNamePlural", typeof(System.String), false, false, false, true,  (int)EntityInformationFieldIndex.FriendlyNamePlural, 150, 0, 0);
			this.AddElementFieldInfo("EntityInformationEntity", "HelpText", typeof(System.String), false, false, false, true,  (int)EntityInformationFieldIndex.HelpText, 2147483647, 0, 0);
			this.AddElementFieldInfo("EntityInformationEntity", "TitleAsBreadCrumbHierarchy", typeof(System.String), false, false, false, true,  (int)EntityInformationFieldIndex.TitleAsBreadCrumbHierarchy, 2147483647, 0, 0);
			this.AddElementFieldInfo("EntityInformationEntity", "DefaultEntityEditPage", typeof(System.String), false, false, false, true,  (int)EntityInformationFieldIndex.DefaultEntityEditPage, 2147483647, 0, 0);
			this.AddElementFieldInfo("EntityInformationEntity", "DefaultEntityCollectionPage", typeof(System.String), false, false, false, true,  (int)EntityInformationFieldIndex.DefaultEntityCollectionPage, 2147483647, 0, 0);
			this.AddElementFieldInfo("EntityInformationEntity", "UpdateWmsCacheDateOnChange", typeof(System.Boolean), false, false, false, false,  (int)EntityInformationFieldIndex.UpdateWmsCacheDateOnChange, 0, 0, 0);
			this.AddElementFieldInfo("EntityInformationEntity", "UpdateCompanyDataVersionOnChange", typeof(System.Boolean), false, false, false, false,  (int)EntityInformationFieldIndex.UpdateCompanyDataVersionOnChange, 0, 0, 0);
			this.AddElementFieldInfo("EntityInformationEntity", "UpdateMenuDataVersionOnChange", typeof(System.Boolean), false, false, false, false,  (int)EntityInformationFieldIndex.UpdateMenuDataVersionOnChange, 0, 0, 0);
			this.AddElementFieldInfo("EntityInformationEntity", "UpdatePosIntegrationInformationOnChange", typeof(System.Boolean), false, false, false, false,  (int)EntityInformationFieldIndex.UpdatePosIntegrationInformationOnChange, 0, 0, 0);
			this.AddElementFieldInfo("EntityInformationEntity", "ForLocalUseWhileSyncingIsUpdated", typeof(System.Boolean), false, false, false, false,  (int)EntityInformationFieldIndex.ForLocalUseWhileSyncingIsUpdated, 0, 0, 0);
			this.AddElementFieldInfo("EntityInformationEntity", "Archived", typeof(System.Boolean), false, false, false, false,  (int)EntityInformationFieldIndex.Archived, 0, 0, 0);
			this.AddElementFieldInfo("EntityInformationEntity", "Created", typeof(Nullable<System.DateTime>), false, false, false, true,  (int)EntityInformationFieldIndex.Created, 0, 0, 0);
			this.AddElementFieldInfo("EntityInformationEntity", "CreatedBy", typeof(System.Int32), false, false, false, false,  (int)EntityInformationFieldIndex.CreatedBy, 0, 0, 10);
			this.AddElementFieldInfo("EntityInformationEntity", "Updated", typeof(Nullable<System.DateTime>), false, false, false, true,  (int)EntityInformationFieldIndex.Updated, 0, 0, 0);
			this.AddElementFieldInfo("EntityInformationEntity", "UpdatedBy", typeof(System.Int32), false, false, false, false,  (int)EntityInformationFieldIndex.UpdatedBy, 0, 0, 10);
			this.AddElementFieldInfo("EntityInformationEntity", "Deleted", typeof(System.Boolean), false, false, false, false,  (int)EntityInformationFieldIndex.Deleted, 0, 0, 0);
			this.AddElementFieldInfo("EntityInformationEntity", "UpdateDeliverypointDataVersionOnChange", typeof(System.Boolean), false, false, false, false,  (int)EntityInformationFieldIndex.UpdateDeliverypointDataVersionOnChange, 0, 0, 0);
			this.AddElementFieldInfo("EntityInformationEntity", "UpdateSurveyDataVersionOnChange", typeof(System.Boolean), false, false, false, false,  (int)EntityInformationFieldIndex.UpdateSurveyDataVersionOnChange, 0, 0, 0);
			this.AddElementFieldInfo("EntityInformationEntity", "UpdateAnnouncementDataVersionOnChange", typeof(System.Boolean), false, false, false, false,  (int)EntityInformationFieldIndex.UpdateAnnouncementDataVersionOnChange, 0, 0, 0);
			this.AddElementFieldInfo("EntityInformationEntity", "UpdateEntertainmentDataVersionOnChange", typeof(System.Boolean), false, false, false, false,  (int)EntityInformationFieldIndex.UpdateEntertainmentDataVersionOnChange, 0, 0, 0);
			this.AddElementFieldInfo("EntityInformationEntity", "UpdateAdvertisementDataVersionOnChange", typeof(System.Boolean), false, false, false, false,  (int)EntityInformationFieldIndex.UpdateAdvertisementDataVersionOnChange, 0, 0, 0);
			this.AddElementFieldInfo("EntityInformationEntity", "CreatedUTC", typeof(Nullable<System.DateTime>), false, false, false, true,  (int)EntityInformationFieldIndex.CreatedUTC, 0, 0, 0);
			this.AddElementFieldInfo("EntityInformationEntity", "UpdatedUTC", typeof(Nullable<System.DateTime>), false, false, false, true,  (int)EntityInformationFieldIndex.UpdatedUTC, 0, 0, 0);
		}
		/// <summary>Inits ModuleEntity's FieldInfo objects</summary>
		private void InitModuleEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(ModuleFieldIndex), "ModuleEntity");
			this.AddElementFieldInfo("ModuleEntity", "ModuleId", typeof(System.Int32), true, false, true, false,  (int)ModuleFieldIndex.ModuleId, 0, 0, 10);
			this.AddElementFieldInfo("ModuleEntity", "NameSystem", typeof(System.String), false, false, false, true,  (int)ModuleFieldIndex.NameSystem, 50, 0, 0);
			this.AddElementFieldInfo("ModuleEntity", "NameFull", typeof(System.String), false, false, false, true,  (int)ModuleFieldIndex.NameFull, 50, 0, 0);
			this.AddElementFieldInfo("ModuleEntity", "NameShort", typeof(System.String), false, false, false, true,  (int)ModuleFieldIndex.NameShort, 50, 0, 0);
			this.AddElementFieldInfo("ModuleEntity", "IconPath", typeof(System.String), false, false, false, true,  (int)ModuleFieldIndex.IconPath, 255, 0, 0);
			this.AddElementFieldInfo("ModuleEntity", "DefaultUiElementTypeNameFull", typeof(System.String), false, false, false, true,  (int)ModuleFieldIndex.DefaultUiElementTypeNameFull, 255, 0, 0);
			this.AddElementFieldInfo("ModuleEntity", "SortOrder", typeof(Nullable<System.Int32>), false, false, false, true,  (int)ModuleFieldIndex.SortOrder, 0, 0, 10);
			this.AddElementFieldInfo("ModuleEntity", "LicensingFree", typeof(System.Boolean), false, false, false, false,  (int)ModuleFieldIndex.LicensingFree, 0, 0, 0);
			this.AddElementFieldInfo("ModuleEntity", "UserRightsFree", typeof(System.Boolean), false, false, false, false,  (int)ModuleFieldIndex.UserRightsFree, 0, 0, 0);
			this.AddElementFieldInfo("ModuleEntity", "ForLocalUseWhileSyncingIsUpdated", typeof(System.Boolean), false, false, false, false,  (int)ModuleFieldIndex.ForLocalUseWhileSyncingIsUpdated, 0, 0, 0);
			this.AddElementFieldInfo("ModuleEntity", "Archived", typeof(System.Boolean), false, false, false, false,  (int)ModuleFieldIndex.Archived, 0, 0, 0);
			this.AddElementFieldInfo("ModuleEntity", "Created", typeof(Nullable<System.DateTime>), false, false, false, true,  (int)ModuleFieldIndex.Created, 0, 0, 0);
			this.AddElementFieldInfo("ModuleEntity", "CreatedBy", typeof(System.Int32), false, false, false, false,  (int)ModuleFieldIndex.CreatedBy, 0, 0, 10);
			this.AddElementFieldInfo("ModuleEntity", "Updated", typeof(Nullable<System.DateTime>), false, false, false, true,  (int)ModuleFieldIndex.Updated, 0, 0, 0);
			this.AddElementFieldInfo("ModuleEntity", "UpdatedBy", typeof(System.Int32), false, false, false, false,  (int)ModuleFieldIndex.UpdatedBy, 0, 0, 10);
			this.AddElementFieldInfo("ModuleEntity", "Deleted", typeof(System.Boolean), false, false, false, false,  (int)ModuleFieldIndex.Deleted, 0, 0, 0);
			this.AddElementFieldInfo("ModuleEntity", "CreatedUTC", typeof(Nullable<System.DateTime>), false, false, false, true,  (int)ModuleFieldIndex.CreatedUTC, 0, 0, 0);
			this.AddElementFieldInfo("ModuleEntity", "UpdatedUTC", typeof(Nullable<System.DateTime>), false, false, false, true,  (int)ModuleFieldIndex.UpdatedUTC, 0, 0, 0);
		}
		/// <summary>Inits ReferentialConstraintEntity's FieldInfo objects</summary>
		private void InitReferentialConstraintEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(ReferentialConstraintFieldIndex), "ReferentialConstraintEntity");
			this.AddElementFieldInfo("ReferentialConstraintEntity", "ReferentialConstraintId", typeof(System.Int32), true, false, true, false,  (int)ReferentialConstraintFieldIndex.ReferentialConstraintId, 0, 0, 10);
			this.AddElementFieldInfo("ReferentialConstraintEntity", "EntityName", typeof(System.String), false, false, false, true,  (int)ReferentialConstraintFieldIndex.EntityName, 255, 0, 0);
			this.AddElementFieldInfo("ReferentialConstraintEntity", "ForeignKeyName", typeof(System.String), false, false, false, true,  (int)ReferentialConstraintFieldIndex.ForeignKeyName, 255, 0, 0);
			this.AddElementFieldInfo("ReferentialConstraintEntity", "DeleteRule", typeof(System.Int32), false, false, false, false,  (int)ReferentialConstraintFieldIndex.DeleteRule, 0, 0, 10);
			this.AddElementFieldInfo("ReferentialConstraintEntity", "UpdateRule", typeof(System.Int32), false, false, false, false,  (int)ReferentialConstraintFieldIndex.UpdateRule, 0, 0, 10);
			this.AddElementFieldInfo("ReferentialConstraintEntity", "ArchiveRule", typeof(System.Int32), false, false, false, false,  (int)ReferentialConstraintFieldIndex.ArchiveRule, 0, 0, 10);
			this.AddElementFieldInfo("ReferentialConstraintEntity", "ForLocalUseWhileSyncingIsUpdated", typeof(System.Boolean), false, false, false, false,  (int)ReferentialConstraintFieldIndex.ForLocalUseWhileSyncingIsUpdated, 0, 0, 0);
			this.AddElementFieldInfo("ReferentialConstraintEntity", "Archived", typeof(System.Boolean), false, false, false, false,  (int)ReferentialConstraintFieldIndex.Archived, 0, 0, 0);
			this.AddElementFieldInfo("ReferentialConstraintEntity", "Created", typeof(Nullable<System.DateTime>), false, false, false, true,  (int)ReferentialConstraintFieldIndex.Created, 0, 0, 0);
			this.AddElementFieldInfo("ReferentialConstraintEntity", "CreatedBy", typeof(System.Int32), false, false, false, false,  (int)ReferentialConstraintFieldIndex.CreatedBy, 0, 0, 10);
			this.AddElementFieldInfo("ReferentialConstraintEntity", "Updated", typeof(Nullable<System.DateTime>), false, false, false, true,  (int)ReferentialConstraintFieldIndex.Updated, 0, 0, 0);
			this.AddElementFieldInfo("ReferentialConstraintEntity", "UpdatedBy", typeof(System.Int32), false, false, false, false,  (int)ReferentialConstraintFieldIndex.UpdatedBy, 0, 0, 10);
			this.AddElementFieldInfo("ReferentialConstraintEntity", "Deleted", typeof(System.Boolean), false, false, false, false,  (int)ReferentialConstraintFieldIndex.Deleted, 0, 0, 0);
			this.AddElementFieldInfo("ReferentialConstraintEntity", "CreatedUTC", typeof(Nullable<System.DateTime>), false, false, false, true,  (int)ReferentialConstraintFieldIndex.CreatedUTC, 0, 0, 0);
			this.AddElementFieldInfo("ReferentialConstraintEntity", "UpdatedUTC", typeof(Nullable<System.DateTime>), false, false, false, true,  (int)ReferentialConstraintFieldIndex.UpdatedUTC, 0, 0, 0);
		}
		/// <summary>Inits RoleEntity's FieldInfo objects</summary>
		private void InitRoleEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(RoleFieldIndex), "RoleEntity");
			this.AddElementFieldInfo("RoleEntity", "RoleId", typeof(System.Int32), true, false, true, false,  (int)RoleFieldIndex.RoleId, 0, 0, 10);
			this.AddElementFieldInfo("RoleEntity", "ParentRoleId", typeof(Nullable<System.Int32>), false, false, false, true,  (int)RoleFieldIndex.ParentRoleId, 0, 0, 10);
			this.AddElementFieldInfo("RoleEntity", "Name", typeof(System.String), false, false, false, true,  (int)RoleFieldIndex.Name, 50, 0, 0);
			this.AddElementFieldInfo("RoleEntity", "IsAdministrator", typeof(System.Boolean), false, false, false, false,  (int)RoleFieldIndex.IsAdministrator, 0, 0, 0);
			this.AddElementFieldInfo("RoleEntity", "SystemName", typeof(System.String), false, false, false, true,  (int)RoleFieldIndex.SystemName, 50, 0, 0);
			this.AddElementFieldInfo("RoleEntity", "SystemRole", typeof(System.Boolean), false, false, false, false,  (int)RoleFieldIndex.SystemRole, 0, 0, 0);
			this.AddElementFieldInfo("RoleEntity", "Created", typeof(Nullable<System.DateTime>), false, false, false, true,  (int)RoleFieldIndex.Created, 0, 0, 0);
			this.AddElementFieldInfo("RoleEntity", "CreatedBy", typeof(Nullable<System.Int32>), false, false, false, true,  (int)RoleFieldIndex.CreatedBy, 0, 0, 10);
			this.AddElementFieldInfo("RoleEntity", "Updated", typeof(Nullable<System.DateTime>), false, false, false, true,  (int)RoleFieldIndex.Updated, 0, 0, 0);
			this.AddElementFieldInfo("RoleEntity", "UpdatedBy", typeof(Nullable<System.Int32>), false, false, false, true,  (int)RoleFieldIndex.UpdatedBy, 0, 0, 10);
			this.AddElementFieldInfo("RoleEntity", "Deleted", typeof(System.Boolean), false, false, false, false,  (int)RoleFieldIndex.Deleted, 0, 0, 0);
			this.AddElementFieldInfo("RoleEntity", "Archived", typeof(System.Boolean), false, false, false, false,  (int)RoleFieldIndex.Archived, 0, 0, 0);
			this.AddElementFieldInfo("RoleEntity", "CreatedUTC", typeof(Nullable<System.DateTime>), false, false, false, true,  (int)RoleFieldIndex.CreatedUTC, 0, 0, 0);
			this.AddElementFieldInfo("RoleEntity", "UpdatedUTC", typeof(Nullable<System.DateTime>), false, false, false, true,  (int)RoleFieldIndex.UpdatedUTC, 0, 0, 0);
		}
		/// <summary>Inits RoleEntityRightsEntity's FieldInfo objects</summary>
		private void InitRoleEntityRightsEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(RoleEntityRightsFieldIndex), "RoleEntityRightsEntity");
			this.AddElementFieldInfo("RoleEntityRightsEntity", "RoleEntityRightId", typeof(System.Int32), true, false, true, false,  (int)RoleEntityRightsFieldIndex.RoleEntityRightId, 0, 0, 10);
			this.AddElementFieldInfo("RoleEntityRightsEntity", "RoleId", typeof(System.Int32), false, true, false, false,  (int)RoleEntityRightsFieldIndex.RoleId, 0, 0, 10);
			this.AddElementFieldInfo("RoleEntityRightsEntity", "EntityInformationId", typeof(System.Int32), false, true, false, false,  (int)RoleEntityRightsFieldIndex.EntityInformationId, 0, 0, 10);
			this.AddElementFieldInfo("RoleEntityRightsEntity", "AllowReadAll", typeof(System.Boolean), false, false, false, false,  (int)RoleEntityRightsFieldIndex.AllowReadAll, 0, 0, 0);
			this.AddElementFieldInfo("RoleEntityRightsEntity", "AllowUpdateAll", typeof(Nullable<System.Boolean>), false, false, false, true,  (int)RoleEntityRightsFieldIndex.AllowUpdateAll, 0, 0, 0);
			this.AddElementFieldInfo("RoleEntityRightsEntity", "AllowInsert", typeof(Nullable<System.Boolean>), false, false, false, true,  (int)RoleEntityRightsFieldIndex.AllowInsert, 0, 0, 0);
			this.AddElementFieldInfo("RoleEntityRightsEntity", "AllowDeleteAll", typeof(Nullable<System.Boolean>), false, false, false, true,  (int)RoleEntityRightsFieldIndex.AllowDeleteAll, 0, 0, 0);
			this.AddElementFieldInfo("RoleEntityRightsEntity", "AllowUpdateCreated", typeof(Nullable<System.Boolean>), false, false, false, true,  (int)RoleEntityRightsFieldIndex.AllowUpdateCreated, 0, 0, 0);
			this.AddElementFieldInfo("RoleEntityRightsEntity", "AllowDeleteCreated", typeof(Nullable<System.Boolean>), false, false, false, true,  (int)RoleEntityRightsFieldIndex.AllowDeleteCreated, 0, 0, 0);
			this.AddElementFieldInfo("RoleEntityRightsEntity", "Created", typeof(Nullable<System.DateTime>), false, false, false, true,  (int)RoleEntityRightsFieldIndex.Created, 0, 0, 0);
			this.AddElementFieldInfo("RoleEntityRightsEntity", "CreatedBy", typeof(Nullable<System.Int32>), false, false, false, true,  (int)RoleEntityRightsFieldIndex.CreatedBy, 0, 0, 10);
			this.AddElementFieldInfo("RoleEntityRightsEntity", "Updated", typeof(Nullable<System.DateTime>), false, false, false, true,  (int)RoleEntityRightsFieldIndex.Updated, 0, 0, 0);
			this.AddElementFieldInfo("RoleEntityRightsEntity", "UpdatedBy", typeof(Nullable<System.Int32>), false, false, false, true,  (int)RoleEntityRightsFieldIndex.UpdatedBy, 0, 0, 10);
			this.AddElementFieldInfo("RoleEntityRightsEntity", "Deleted", typeof(System.Boolean), false, false, false, false,  (int)RoleEntityRightsFieldIndex.Deleted, 0, 0, 0);
			this.AddElementFieldInfo("RoleEntityRightsEntity", "Archived", typeof(System.Boolean), false, false, false, false,  (int)RoleEntityRightsFieldIndex.Archived, 0, 0, 0);
			this.AddElementFieldInfo("RoleEntityRightsEntity", "CreatedUTC", typeof(Nullable<System.DateTime>), false, false, false, true,  (int)RoleEntityRightsFieldIndex.CreatedUTC, 0, 0, 0);
			this.AddElementFieldInfo("RoleEntityRightsEntity", "UpdatedUTC", typeof(Nullable<System.DateTime>), false, false, false, true,  (int)RoleEntityRightsFieldIndex.UpdatedUTC, 0, 0, 0);
		}
		/// <summary>Inits RoleModuleRightsEntity's FieldInfo objects</summary>
		private void InitRoleModuleRightsEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(RoleModuleRightsFieldIndex), "RoleModuleRightsEntity");
			this.AddElementFieldInfo("RoleModuleRightsEntity", "RoleModuleRightsId", typeof(System.Int32), true, false, true, false,  (int)RoleModuleRightsFieldIndex.RoleModuleRightsId, 0, 0, 10);
			this.AddElementFieldInfo("RoleModuleRightsEntity", "RoleId", typeof(System.Int32), false, true, false, false,  (int)RoleModuleRightsFieldIndex.RoleId, 0, 0, 10);
			this.AddElementFieldInfo("RoleModuleRightsEntity", "ModuleId", typeof(System.Int32), false, true, false, false,  (int)RoleModuleRightsFieldIndex.ModuleId, 0, 0, 10);
			this.AddElementFieldInfo("RoleModuleRightsEntity", "IsAllowed", typeof(System.Boolean), false, false, false, false,  (int)RoleModuleRightsFieldIndex.IsAllowed, 0, 0, 0);
			this.AddElementFieldInfo("RoleModuleRightsEntity", "Archived", typeof(System.Boolean), false, false, false, false,  (int)RoleModuleRightsFieldIndex.Archived, 0, 0, 0);
			this.AddElementFieldInfo("RoleModuleRightsEntity", "Created", typeof(Nullable<System.DateTime>), false, false, false, true,  (int)RoleModuleRightsFieldIndex.Created, 0, 0, 0);
			this.AddElementFieldInfo("RoleModuleRightsEntity", "CreatedBy", typeof(System.Int32), false, false, false, false,  (int)RoleModuleRightsFieldIndex.CreatedBy, 0, 0, 10);
			this.AddElementFieldInfo("RoleModuleRightsEntity", "Updated", typeof(Nullable<System.DateTime>), false, false, false, true,  (int)RoleModuleRightsFieldIndex.Updated, 0, 0, 0);
			this.AddElementFieldInfo("RoleModuleRightsEntity", "UpdatedBy", typeof(System.Int32), false, false, false, false,  (int)RoleModuleRightsFieldIndex.UpdatedBy, 0, 0, 10);
			this.AddElementFieldInfo("RoleModuleRightsEntity", "Deleted", typeof(System.Boolean), false, false, false, false,  (int)RoleModuleRightsFieldIndex.Deleted, 0, 0, 0);
			this.AddElementFieldInfo("RoleModuleRightsEntity", "CreatedUTC", typeof(Nullable<System.DateTime>), false, false, false, true,  (int)RoleModuleRightsFieldIndex.CreatedUTC, 0, 0, 0);
			this.AddElementFieldInfo("RoleModuleRightsEntity", "UpdatedUTC", typeof(Nullable<System.DateTime>), false, false, false, true,  (int)RoleModuleRightsFieldIndex.UpdatedUTC, 0, 0, 0);
		}
		/// <summary>Inits RoleUIElementRightsEntity's FieldInfo objects</summary>
		private void InitRoleUIElementRightsEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(RoleUIElementRightsFieldIndex), "RoleUIElementRightsEntity");
			this.AddElementFieldInfo("RoleUIElementRightsEntity", "RoleUIElementRightsId", typeof(System.Int32), true, false, true, false,  (int)RoleUIElementRightsFieldIndex.RoleUIElementRightsId, 0, 0, 10);
			this.AddElementFieldInfo("RoleUIElementRightsEntity", "RoleId", typeof(System.Int32), false, true, false, false,  (int)RoleUIElementRightsFieldIndex.RoleId, 0, 0, 10);
			this.AddElementFieldInfo("RoleUIElementRightsEntity", "UiElementTypeNameFull", typeof(System.String), false, false, false, false,  (int)RoleUIElementRightsFieldIndex.UiElementTypeNameFull, 255, 0, 0);
			this.AddElementFieldInfo("RoleUIElementRightsEntity", "IsAllowed", typeof(System.Boolean), false, false, false, false,  (int)RoleUIElementRightsFieldIndex.IsAllowed, 0, 0, 0);
			this.AddElementFieldInfo("RoleUIElementRightsEntity", "IsReadOnly", typeof(System.Boolean), false, false, false, false,  (int)RoleUIElementRightsFieldIndex.IsReadOnly, 0, 0, 0);
			this.AddElementFieldInfo("RoleUIElementRightsEntity", "Archived", typeof(System.Boolean), false, false, false, false,  (int)RoleUIElementRightsFieldIndex.Archived, 0, 0, 0);
			this.AddElementFieldInfo("RoleUIElementRightsEntity", "Created", typeof(Nullable<System.DateTime>), false, false, false, true,  (int)RoleUIElementRightsFieldIndex.Created, 0, 0, 0);
			this.AddElementFieldInfo("RoleUIElementRightsEntity", "CreatedBy", typeof(System.Int32), false, false, false, false,  (int)RoleUIElementRightsFieldIndex.CreatedBy, 0, 0, 10);
			this.AddElementFieldInfo("RoleUIElementRightsEntity", "Updated", typeof(Nullable<System.DateTime>), false, false, false, true,  (int)RoleUIElementRightsFieldIndex.Updated, 0, 0, 0);
			this.AddElementFieldInfo("RoleUIElementRightsEntity", "UpdatedBy", typeof(System.Int32), false, false, false, false,  (int)RoleUIElementRightsFieldIndex.UpdatedBy, 0, 0, 10);
			this.AddElementFieldInfo("RoleUIElementRightsEntity", "Deleted", typeof(System.Boolean), false, false, false, false,  (int)RoleUIElementRightsFieldIndex.Deleted, 0, 0, 0);
			this.AddElementFieldInfo("RoleUIElementRightsEntity", "CreatedUTC", typeof(Nullable<System.DateTime>), false, false, false, true,  (int)RoleUIElementRightsFieldIndex.CreatedUTC, 0, 0, 0);
			this.AddElementFieldInfo("RoleUIElementRightsEntity", "UpdatedUTC", typeof(Nullable<System.DateTime>), false, false, false, true,  (int)RoleUIElementRightsFieldIndex.UpdatedUTC, 0, 0, 0);
		}
		/// <summary>Inits RoleUIElementSubPanelRightsEntity's FieldInfo objects</summary>
		private void InitRoleUIElementSubPanelRightsEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(RoleUIElementSubPanelRightsFieldIndex), "RoleUIElementSubPanelRightsEntity");
			this.AddElementFieldInfo("RoleUIElementSubPanelRightsEntity", "RoleUIElementSubPanelRightsId", typeof(System.Int32), true, false, false, false,  (int)RoleUIElementSubPanelRightsFieldIndex.RoleUIElementSubPanelRightsId, 0, 0, 10);
			this.AddElementFieldInfo("RoleUIElementSubPanelRightsEntity", "RoleId", typeof(System.Int32), false, true, false, false,  (int)RoleUIElementSubPanelRightsFieldIndex.RoleId, 0, 0, 10);
			this.AddElementFieldInfo("RoleUIElementSubPanelRightsEntity", "UiElementTypeNameFull", typeof(System.String), false, false, false, false,  (int)RoleUIElementSubPanelRightsFieldIndex.UiElementTypeNameFull, 255, 0, 0);
			this.AddElementFieldInfo("RoleUIElementSubPanelRightsEntity", "IsAllowed", typeof(System.Boolean), false, false, false, false,  (int)RoleUIElementSubPanelRightsFieldIndex.IsAllowed, 0, 0, 0);
			this.AddElementFieldInfo("RoleUIElementSubPanelRightsEntity", "Archived", typeof(System.Boolean), false, false, false, false,  (int)RoleUIElementSubPanelRightsFieldIndex.Archived, 0, 0, 0);
			this.AddElementFieldInfo("RoleUIElementSubPanelRightsEntity", "Created", typeof(Nullable<System.DateTime>), false, false, false, true,  (int)RoleUIElementSubPanelRightsFieldIndex.Created, 0, 0, 0);
			this.AddElementFieldInfo("RoleUIElementSubPanelRightsEntity", "CreatedBy", typeof(System.Int32), false, false, false, false,  (int)RoleUIElementSubPanelRightsFieldIndex.CreatedBy, 0, 0, 10);
			this.AddElementFieldInfo("RoleUIElementSubPanelRightsEntity", "Updated", typeof(Nullable<System.DateTime>), false, false, false, true,  (int)RoleUIElementSubPanelRightsFieldIndex.Updated, 0, 0, 0);
			this.AddElementFieldInfo("RoleUIElementSubPanelRightsEntity", "UpdatedBy", typeof(System.Int32), false, false, false, false,  (int)RoleUIElementSubPanelRightsFieldIndex.UpdatedBy, 0, 0, 10);
			this.AddElementFieldInfo("RoleUIElementSubPanelRightsEntity", "Deleted", typeof(System.Boolean), false, false, false, false,  (int)RoleUIElementSubPanelRightsFieldIndex.Deleted, 0, 0, 0);
			this.AddElementFieldInfo("RoleUIElementSubPanelRightsEntity", "CreatedUTC", typeof(Nullable<System.DateTime>), false, false, false, true,  (int)RoleUIElementSubPanelRightsFieldIndex.CreatedUTC, 0, 0, 0);
			this.AddElementFieldInfo("RoleUIElementSubPanelRightsEntity", "UpdatedUTC", typeof(Nullable<System.DateTime>), false, false, false, true,  (int)RoleUIElementSubPanelRightsFieldIndex.UpdatedUTC, 0, 0, 0);
		}
		/// <summary>Inits TimeZoneEntity's FieldInfo objects</summary>
		private void InitTimeZoneEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(TimeZoneFieldIndex), "TimeZoneEntity");
			this.AddElementFieldInfo("TimeZoneEntity", "TimeZoneId", typeof(System.Int32), true, false, true, false,  (int)TimeZoneFieldIndex.TimeZoneId, 0, 0, 10);
			this.AddElementFieldInfo("TimeZoneEntity", "Name", typeof(System.String), false, false, false, false,  (int)TimeZoneFieldIndex.Name, 255, 0, 0);
			this.AddElementFieldInfo("TimeZoneEntity", "NameAndroid", typeof(System.String), false, false, false, false,  (int)TimeZoneFieldIndex.NameAndroid, 255, 0, 0);
			this.AddElementFieldInfo("TimeZoneEntity", "NameDotNet", typeof(System.String), false, false, false, false,  (int)TimeZoneFieldIndex.NameDotNet, 255, 0, 0);
			this.AddElementFieldInfo("TimeZoneEntity", "Created", typeof(Nullable<System.DateTime>), false, false, false, true,  (int)TimeZoneFieldIndex.Created, 0, 0, 0);
			this.AddElementFieldInfo("TimeZoneEntity", "CreatedBy", typeof(System.Int32), false, false, false, false,  (int)TimeZoneFieldIndex.CreatedBy, 0, 0, 10);
			this.AddElementFieldInfo("TimeZoneEntity", "Updated", typeof(Nullable<System.DateTime>), false, false, false, true,  (int)TimeZoneFieldIndex.Updated, 0, 0, 0);
			this.AddElementFieldInfo("TimeZoneEntity", "UpdatedBy", typeof(System.Int32), false, false, false, false,  (int)TimeZoneFieldIndex.UpdatedBy, 0, 0, 10);
			this.AddElementFieldInfo("TimeZoneEntity", "CreatedUTC", typeof(Nullable<System.DateTime>), false, false, false, true,  (int)TimeZoneFieldIndex.CreatedUTC, 0, 0, 0);
			this.AddElementFieldInfo("TimeZoneEntity", "UpdatedUTC", typeof(Nullable<System.DateTime>), false, false, false, true,  (int)TimeZoneFieldIndex.UpdatedUTC, 0, 0, 0);
		}
		/// <summary>Inits TranslationEntity's FieldInfo objects</summary>
		private void InitTranslationEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(TranslationFieldIndex), "TranslationEntity");
			this.AddElementFieldInfo("TranslationEntity", "TranslationId", typeof(System.Int32), true, false, true, false,  (int)TranslationFieldIndex.TranslationId, 0, 0, 10);
			this.AddElementFieldInfo("TranslationEntity", "TranslationKey", typeof(System.String), false, false, false, false,  (int)TranslationFieldIndex.TranslationKey, 500, 0, 0);
			this.AddElementFieldInfo("TranslationEntity", "LanguageCode", typeof(System.String), false, false, false, false,  (int)TranslationFieldIndex.LanguageCode, 2, 0, 0);
			this.AddElementFieldInfo("TranslationEntity", "TranslationValue", typeof(System.String), false, false, false, true,  (int)TranslationFieldIndex.TranslationValue, 2147483647, 0, 0);
			this.AddElementFieldInfo("TranslationEntity", "LocallyCreated", typeof(System.Boolean), false, false, false, false,  (int)TranslationFieldIndex.LocallyCreated, 0, 0, 0);
			this.AddElementFieldInfo("TranslationEntity", "Archived", typeof(System.Boolean), false, false, false, false,  (int)TranslationFieldIndex.Archived, 0, 0, 0);
			this.AddElementFieldInfo("TranslationEntity", "Created", typeof(Nullable<System.DateTime>), false, false, false, true,  (int)TranslationFieldIndex.Created, 0, 0, 0);
			this.AddElementFieldInfo("TranslationEntity", "CreatedBy", typeof(System.Int32), false, false, false, false,  (int)TranslationFieldIndex.CreatedBy, 0, 0, 10);
			this.AddElementFieldInfo("TranslationEntity", "Updated", typeof(Nullable<System.DateTime>), false, false, false, true,  (int)TranslationFieldIndex.Updated, 0, 0, 0);
			this.AddElementFieldInfo("TranslationEntity", "UpdatedBy", typeof(System.Int32), false, false, false, false,  (int)TranslationFieldIndex.UpdatedBy, 0, 0, 10);
			this.AddElementFieldInfo("TranslationEntity", "Deleted", typeof(System.Boolean), false, false, false, false,  (int)TranslationFieldIndex.Deleted, 0, 0, 0);
			this.AddElementFieldInfo("TranslationEntity", "CreatedUTC", typeof(Nullable<System.DateTime>), false, false, false, true,  (int)TranslationFieldIndex.CreatedUTC, 0, 0, 0);
			this.AddElementFieldInfo("TranslationEntity", "UpdatedUTC", typeof(Nullable<System.DateTime>), false, false, false, true,  (int)TranslationFieldIndex.UpdatedUTC, 0, 0, 0);
		}
		/// <summary>Inits UIColorEntity's FieldInfo objects</summary>
		private void InitUIColorEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(UIColorFieldIndex), "UIColorEntity");
			this.AddElementFieldInfo("UIColorEntity", "UIColorId", typeof(System.Int32), true, false, true, false,  (int)UIColorFieldIndex.UIColorId, 0, 0, 10);
			this.AddElementFieldInfo("UIColorEntity", "Type", typeof(System.Int32), false, false, false, false,  (int)UIColorFieldIndex.Type, 0, 0, 10);
			this.AddElementFieldInfo("UIColorEntity", "Color", typeof(System.Int32), false, false, false, false,  (int)UIColorFieldIndex.Color, 0, 0, 10);
			this.AddElementFieldInfo("UIColorEntity", "CreatedUTC", typeof(Nullable<System.DateTime>), false, false, false, true,  (int)UIColorFieldIndex.CreatedUTC, 0, 0, 0);
			this.AddElementFieldInfo("UIColorEntity", "CreatedBy", typeof(Nullable<System.Int32>), false, false, false, true,  (int)UIColorFieldIndex.CreatedBy, 0, 0, 10);
			this.AddElementFieldInfo("UIColorEntity", "UpdatedUTC", typeof(Nullable<System.DateTime>), false, false, false, true,  (int)UIColorFieldIndex.UpdatedUTC, 0, 0, 0);
			this.AddElementFieldInfo("UIColorEntity", "UpdatedBy", typeof(Nullable<System.Int32>), false, false, false, true,  (int)UIColorFieldIndex.UpdatedBy, 0, 0, 10);
			this.AddElementFieldInfo("UIColorEntity", "GroupName", typeof(System.String), false, false, false, true,  (int)UIColorFieldIndex.GroupName, 255, 0, 0);
		}
		/// <summary>Inits UIElementEntity's FieldInfo objects</summary>
		private void InitUIElementEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(UIElementFieldIndex), "UIElementEntity");
			this.AddElementFieldInfo("UIElementEntity", "UIElementId", typeof(System.Int32), true, false, true, false,  (int)UIElementFieldIndex.UIElementId, 0, 0, 10);
			this.AddElementFieldInfo("UIElementEntity", "ModuleNameSystem", typeof(System.String), false, false, false, true,  (int)UIElementFieldIndex.ModuleNameSystem, 50, 0, 0);
			this.AddElementFieldInfo("UIElementEntity", "NameSystem", typeof(System.String), false, false, false, true,  (int)UIElementFieldIndex.NameSystem, 50, 0, 0);
			this.AddElementFieldInfo("UIElementEntity", "NameFull", typeof(System.String), false, false, false, true,  (int)UIElementFieldIndex.NameFull, 50, 0, 0);
			this.AddElementFieldInfo("UIElementEntity", "NameShort", typeof(System.String), false, false, false, true,  (int)UIElementFieldIndex.NameShort, 50, 0, 0);
			this.AddElementFieldInfo("UIElementEntity", "Url", typeof(System.String), false, false, false, true,  (int)UIElementFieldIndex.Url, 255, 0, 0);
			this.AddElementFieldInfo("UIElementEntity", "DisplayInMenu", typeof(System.Boolean), false, false, false, false,  (int)UIElementFieldIndex.DisplayInMenu, 0, 0, 0);
			this.AddElementFieldInfo("UIElementEntity", "TypeNameFull", typeof(System.String), false, false, false, true,  (int)UIElementFieldIndex.TypeNameFull, 255, 0, 0);
			this.AddElementFieldInfo("UIElementEntity", "SortOrder", typeof(Nullable<System.Int32>), false, false, false, true,  (int)UIElementFieldIndex.SortOrder, 0, 0, 10);
			this.AddElementFieldInfo("UIElementEntity", "FreeAccess", typeof(System.Boolean), false, false, false, false,  (int)UIElementFieldIndex.FreeAccess, 0, 0, 0);
			this.AddElementFieldInfo("UIElementEntity", "LicensingFree", typeof(System.Boolean), false, false, false, false,  (int)UIElementFieldIndex.LicensingFree, 0, 0, 0);
			this.AddElementFieldInfo("UIElementEntity", "UserRightsFree", typeof(System.Boolean), false, false, false, false,  (int)UIElementFieldIndex.UserRightsFree, 0, 0, 0);
			this.AddElementFieldInfo("UIElementEntity", "IsConfigurationItem", typeof(System.Boolean), false, false, false, false,  (int)UIElementFieldIndex.IsConfigurationItem, 0, 0, 0);
			this.AddElementFieldInfo("UIElementEntity", "EntityName", typeof(System.String), false, false, false, true,  (int)UIElementFieldIndex.EntityName, 150, 0, 0);
			this.AddElementFieldInfo("UIElementEntity", "ForLocalUseWhileSyncingIsUpdated", typeof(System.Boolean), false, false, false, false,  (int)UIElementFieldIndex.ForLocalUseWhileSyncingIsUpdated, 0, 0, 0);
			this.AddElementFieldInfo("UIElementEntity", "Deleted", typeof(System.Boolean), false, false, false, false,  (int)UIElementFieldIndex.Deleted, 0, 0, 0);
			this.AddElementFieldInfo("UIElementEntity", "Archived", typeof(System.Boolean), false, false, false, false,  (int)UIElementFieldIndex.Archived, 0, 0, 0);
			this.AddElementFieldInfo("UIElementEntity", "Created", typeof(Nullable<System.DateTime>), false, false, false, true,  (int)UIElementFieldIndex.Created, 0, 0, 0);
			this.AddElementFieldInfo("UIElementEntity", "CreatedBy", typeof(System.Int32), false, false, false, false,  (int)UIElementFieldIndex.CreatedBy, 0, 0, 10);
			this.AddElementFieldInfo("UIElementEntity", "Updated", typeof(Nullable<System.DateTime>), false, false, false, true,  (int)UIElementFieldIndex.Updated, 0, 0, 0);
			this.AddElementFieldInfo("UIElementEntity", "UpdatedBy", typeof(System.Int32), false, false, false, false,  (int)UIElementFieldIndex.UpdatedBy, 0, 0, 10);
			this.AddElementFieldInfo("UIElementEntity", "CreatedUTC", typeof(Nullable<System.DateTime>), false, false, false, true,  (int)UIElementFieldIndex.CreatedUTC, 0, 0, 0);
			this.AddElementFieldInfo("UIElementEntity", "UpdatedUTC", typeof(Nullable<System.DateTime>), false, false, false, true,  (int)UIElementFieldIndex.UpdatedUTC, 0, 0, 0);
		}
		/// <summary>Inits UIElementSubPanelEntity's FieldInfo objects</summary>
		private void InitUIElementSubPanelEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(UIElementSubPanelFieldIndex), "UIElementSubPanelEntity");
			this.AddElementFieldInfo("UIElementSubPanelEntity", "UIElementSubPanelId", typeof(System.Int32), true, false, true, false,  (int)UIElementSubPanelFieldIndex.UIElementSubPanelId, 0, 0, 10);
			this.AddElementFieldInfo("UIElementSubPanelEntity", "NameSystem", typeof(System.String), false, false, false, true,  (int)UIElementSubPanelFieldIndex.NameSystem, 50, 0, 0);
			this.AddElementFieldInfo("UIElementSubPanelEntity", "NameFull", typeof(System.String), false, false, false, true,  (int)UIElementSubPanelFieldIndex.NameFull, 50, 0, 0);
			this.AddElementFieldInfo("UIElementSubPanelEntity", "NameShort", typeof(System.String), false, false, false, true,  (int)UIElementSubPanelFieldIndex.NameShort, 50, 0, 0);
			this.AddElementFieldInfo("UIElementSubPanelEntity", "Url", typeof(System.String), false, false, false, true,  (int)UIElementSubPanelFieldIndex.Url, 255, 0, 0);
			this.AddElementFieldInfo("UIElementSubPanelEntity", "TypeNameFull", typeof(System.String), false, false, false, true,  (int)UIElementSubPanelFieldIndex.TypeNameFull, 255, 0, 0);
			this.AddElementFieldInfo("UIElementSubPanelEntity", "SortOrder", typeof(Nullable<System.Int32>), false, false, false, true,  (int)UIElementSubPanelFieldIndex.SortOrder, 0, 0, 10);
			this.AddElementFieldInfo("UIElementSubPanelEntity", "FreeAccess", typeof(System.Boolean), false, false, false, false,  (int)UIElementSubPanelFieldIndex.FreeAccess, 0, 0, 0);
			this.AddElementFieldInfo("UIElementSubPanelEntity", "LicensingFree", typeof(System.Boolean), false, false, false, false,  (int)UIElementSubPanelFieldIndex.LicensingFree, 0, 0, 0);
			this.AddElementFieldInfo("UIElementSubPanelEntity", "EntityName", typeof(System.String), false, false, false, true,  (int)UIElementSubPanelFieldIndex.EntityName, 150, 0, 0);
			this.AddElementFieldInfo("UIElementSubPanelEntity", "OnTabPage", typeof(System.Boolean), false, false, false, false,  (int)UIElementSubPanelFieldIndex.OnTabPage, 0, 0, 0);
			this.AddElementFieldInfo("UIElementSubPanelEntity", "ForLocalUseWhileSyncingIsUpdated", typeof(System.Boolean), false, false, false, false,  (int)UIElementSubPanelFieldIndex.ForLocalUseWhileSyncingIsUpdated, 0, 0, 0);
			this.AddElementFieldInfo("UIElementSubPanelEntity", "Deleted", typeof(System.Boolean), false, false, false, false,  (int)UIElementSubPanelFieldIndex.Deleted, 0, 0, 0);
			this.AddElementFieldInfo("UIElementSubPanelEntity", "Archived", typeof(System.Boolean), false, false, false, false,  (int)UIElementSubPanelFieldIndex.Archived, 0, 0, 0);
			this.AddElementFieldInfo("UIElementSubPanelEntity", "Created", typeof(Nullable<System.DateTime>), false, false, false, true,  (int)UIElementSubPanelFieldIndex.Created, 0, 0, 0);
			this.AddElementFieldInfo("UIElementSubPanelEntity", "CreatedBy", typeof(System.Int32), false, false, false, false,  (int)UIElementSubPanelFieldIndex.CreatedBy, 0, 0, 10);
			this.AddElementFieldInfo("UIElementSubPanelEntity", "Updated", typeof(Nullable<System.DateTime>), false, false, false, true,  (int)UIElementSubPanelFieldIndex.Updated, 0, 0, 0);
			this.AddElementFieldInfo("UIElementSubPanelEntity", "UpdatedBy", typeof(System.Int32), false, false, false, false,  (int)UIElementSubPanelFieldIndex.UpdatedBy, 0, 0, 10);
			this.AddElementFieldInfo("UIElementSubPanelEntity", "CreatedUTC", typeof(Nullable<System.DateTime>), false, false, false, true,  (int)UIElementSubPanelFieldIndex.CreatedUTC, 0, 0, 0);
			this.AddElementFieldInfo("UIElementSubPanelEntity", "UpdatedUTC", typeof(Nullable<System.DateTime>), false, false, false, true,  (int)UIElementSubPanelFieldIndex.UpdatedUTC, 0, 0, 0);
		}
		/// <summary>Inits UIElementSubPanelUIElementEntity's FieldInfo objects</summary>
		private void InitUIElementSubPanelUIElementEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(UIElementSubPanelUIElementFieldIndex), "UIElementSubPanelUIElementEntity");
			this.AddElementFieldInfo("UIElementSubPanelUIElementEntity", "UIElementSubPanelUIElementId", typeof(System.Int32), true, false, true, false,  (int)UIElementSubPanelUIElementFieldIndex.UIElementSubPanelUIElementId, 0, 0, 10);
			this.AddElementFieldInfo("UIElementSubPanelUIElementEntity", "UIElementSubPanelTypeNameFull", typeof(System.String), false, false, false, false,  (int)UIElementSubPanelUIElementFieldIndex.UIElementSubPanelTypeNameFull, 255, 0, 0);
			this.AddElementFieldInfo("UIElementSubPanelUIElementEntity", "ParentUIElementTypeNameFull", typeof(System.String), false, false, false, true,  (int)UIElementSubPanelUIElementFieldIndex.ParentUIElementTypeNameFull, 255, 0, 0);
			this.AddElementFieldInfo("UIElementSubPanelUIElementEntity", "ParentUIElementSubPanelTypeNameFull", typeof(System.String), false, false, false, true,  (int)UIElementSubPanelUIElementFieldIndex.ParentUIElementSubPanelTypeNameFull, 255, 0, 0);
			this.AddElementFieldInfo("UIElementSubPanelUIElementEntity", "ForLocalUseWhileSyncingIsUpdated", typeof(System.Boolean), false, false, false, false,  (int)UIElementSubPanelUIElementFieldIndex.ForLocalUseWhileSyncingIsUpdated, 0, 0, 0);
			this.AddElementFieldInfo("UIElementSubPanelUIElementEntity", "Archived", typeof(System.Boolean), false, false, false, false,  (int)UIElementSubPanelUIElementFieldIndex.Archived, 0, 0, 0);
			this.AddElementFieldInfo("UIElementSubPanelUIElementEntity", "Created", typeof(Nullable<System.DateTime>), false, false, false, true,  (int)UIElementSubPanelUIElementFieldIndex.Created, 0, 0, 0);
			this.AddElementFieldInfo("UIElementSubPanelUIElementEntity", "CreatedBy", typeof(System.Int32), false, false, false, false,  (int)UIElementSubPanelUIElementFieldIndex.CreatedBy, 0, 0, 10);
			this.AddElementFieldInfo("UIElementSubPanelUIElementEntity", "Updated", typeof(Nullable<System.DateTime>), false, false, false, true,  (int)UIElementSubPanelUIElementFieldIndex.Updated, 0, 0, 0);
			this.AddElementFieldInfo("UIElementSubPanelUIElementEntity", "UpdatedBy", typeof(System.Int32), false, false, false, false,  (int)UIElementSubPanelUIElementFieldIndex.UpdatedBy, 0, 0, 10);
			this.AddElementFieldInfo("UIElementSubPanelUIElementEntity", "CreatedUTC", typeof(Nullable<System.DateTime>), false, false, false, true,  (int)UIElementSubPanelUIElementFieldIndex.CreatedUTC, 0, 0, 0);
			this.AddElementFieldInfo("UIElementSubPanelUIElementEntity", "UpdatedUTC", typeof(Nullable<System.DateTime>), false, false, false, true,  (int)UIElementSubPanelUIElementFieldIndex.UpdatedUTC, 0, 0, 0);
		}
		/// <summary>Inits ViewEntity's FieldInfo objects</summary>
		private void InitViewEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(ViewFieldIndex), "ViewEntity");
			this.AddElementFieldInfo("ViewEntity", "ViewId", typeof(System.Int32), true, false, true, false,  (int)ViewFieldIndex.ViewId, 0, 0, 10);
			this.AddElementFieldInfo("ViewEntity", "Name", typeof(System.String), false, false, false, true,  (int)ViewFieldIndex.Name, 150, 0, 0);
			this.AddElementFieldInfo("ViewEntity", "EntityName", typeof(System.String), false, false, false, false,  (int)ViewFieldIndex.EntityName, 250, 0, 0);
			this.AddElementFieldInfo("ViewEntity", "UiElementTypeNameFull", typeof(System.String), false, false, false, true,  (int)ViewFieldIndex.UiElementTypeNameFull, 255, 0, 0);
			this.AddElementFieldInfo("ViewEntity", "ForLocalUseWhileSyncingIsUpdated", typeof(System.Boolean), false, false, false, false,  (int)ViewFieldIndex.ForLocalUseWhileSyncingIsUpdated, 0, 0, 0);
			this.AddElementFieldInfo("ViewEntity", "Archived", typeof(System.Boolean), false, false, false, false,  (int)ViewFieldIndex.Archived, 0, 0, 0);
			this.AddElementFieldInfo("ViewEntity", "Created", typeof(Nullable<System.DateTime>), false, false, false, true,  (int)ViewFieldIndex.Created, 0, 0, 0);
			this.AddElementFieldInfo("ViewEntity", "CreatedBy", typeof(System.Int32), false, false, false, false,  (int)ViewFieldIndex.CreatedBy, 0, 0, 10);
			this.AddElementFieldInfo("ViewEntity", "Updated", typeof(Nullable<System.DateTime>), false, false, false, true,  (int)ViewFieldIndex.Updated, 0, 0, 0);
			this.AddElementFieldInfo("ViewEntity", "UpdatedBy", typeof(System.Int32), false, false, false, false,  (int)ViewFieldIndex.UpdatedBy, 0, 0, 10);
			this.AddElementFieldInfo("ViewEntity", "Deleted", typeof(System.Boolean), false, false, false, false,  (int)ViewFieldIndex.Deleted, 0, 0, 0);
			this.AddElementFieldInfo("ViewEntity", "CreatedUTC", typeof(Nullable<System.DateTime>), false, false, false, true,  (int)ViewFieldIndex.CreatedUTC, 0, 0, 0);
			this.AddElementFieldInfo("ViewEntity", "UpdatedUTC", typeof(Nullable<System.DateTime>), false, false, false, true,  (int)ViewFieldIndex.UpdatedUTC, 0, 0, 0);
		}
		/// <summary>Inits ViewItemEntity's FieldInfo objects</summary>
		private void InitViewItemEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(ViewItemFieldIndex), "ViewItemEntity");
			this.AddElementFieldInfo("ViewItemEntity", "ViewItemId", typeof(System.Int32), true, false, true, false,  (int)ViewItemFieldIndex.ViewItemId, 0, 0, 10);
			this.AddElementFieldInfo("ViewItemEntity", "ViewId", typeof(Nullable<System.Int32>), false, true, false, true,  (int)ViewItemFieldIndex.ViewId, 0, 0, 10);
			this.AddElementFieldInfo("ViewItemEntity", "EntityName", typeof(System.String), false, false, false, true,  (int)ViewItemFieldIndex.EntityName, 150, 0, 0);
			this.AddElementFieldInfo("ViewItemEntity", "FieldName", typeof(System.String), false, false, false, true,  (int)ViewItemFieldIndex.FieldName, 150, 0, 0);
			this.AddElementFieldInfo("ViewItemEntity", "ShowOnGridView", typeof(System.Boolean), false, false, false, false,  (int)ViewItemFieldIndex.ShowOnGridView, 0, 0, 0);
			this.AddElementFieldInfo("ViewItemEntity", "DisplayPosition", typeof(System.Int32), false, false, false, false,  (int)ViewItemFieldIndex.DisplayPosition, 0, 0, 10);
			this.AddElementFieldInfo("ViewItemEntity", "SortPosition", typeof(System.Int32), false, false, false, false,  (int)ViewItemFieldIndex.SortPosition, 0, 0, 10);
			this.AddElementFieldInfo("ViewItemEntity", "SortOperator", typeof(System.Int32), false, false, false, false,  (int)ViewItemFieldIndex.SortOperator, 0, 0, 10);
			this.AddElementFieldInfo("ViewItemEntity", "DisplayFormatString", typeof(System.String), false, false, false, true,  (int)ViewItemFieldIndex.DisplayFormatString, 100, 0, 0);
			this.AddElementFieldInfo("ViewItemEntity", "Width", typeof(System.String), false, false, false, true,  (int)ViewItemFieldIndex.Width, 50, 0, 0);
			this.AddElementFieldInfo("ViewItemEntity", "Type", typeof(System.String), false, false, false, true,  (int)ViewItemFieldIndex.Type, 100, 0, 0);
			this.AddElementFieldInfo("ViewItemEntity", "UiElementTypeNameFull", typeof(System.String), false, false, false, true,  (int)ViewItemFieldIndex.UiElementTypeNameFull, 255, 0, 0);
			this.AddElementFieldInfo("ViewItemEntity", "ForLocalUseWhileSyncingIsUpdated", typeof(System.Boolean), false, false, false, false,  (int)ViewItemFieldIndex.ForLocalUseWhileSyncingIsUpdated, 0, 0, 0);
			this.AddElementFieldInfo("ViewItemEntity", "Archived", typeof(System.Boolean), false, false, false, false,  (int)ViewItemFieldIndex.Archived, 0, 0, 0);
			this.AddElementFieldInfo("ViewItemEntity", "Created", typeof(Nullable<System.DateTime>), false, false, false, true,  (int)ViewItemFieldIndex.Created, 0, 0, 0);
			this.AddElementFieldInfo("ViewItemEntity", "CreatedBy", typeof(System.Int32), false, false, false, false,  (int)ViewItemFieldIndex.CreatedBy, 0, 0, 10);
			this.AddElementFieldInfo("ViewItemEntity", "Updated", typeof(Nullable<System.DateTime>), false, false, false, true,  (int)ViewItemFieldIndex.Updated, 0, 0, 0);
			this.AddElementFieldInfo("ViewItemEntity", "UpdatedBy", typeof(System.Int32), false, false, false, false,  (int)ViewItemFieldIndex.UpdatedBy, 0, 0, 10);
			this.AddElementFieldInfo("ViewItemEntity", "Deleted", typeof(System.Boolean), false, false, false, false,  (int)ViewItemFieldIndex.Deleted, 0, 0, 0);
			this.AddElementFieldInfo("ViewItemEntity", "CreatedUTC", typeof(Nullable<System.DateTime>), false, false, false, true,  (int)ViewItemFieldIndex.CreatedUTC, 0, 0, 0);
			this.AddElementFieldInfo("ViewItemEntity", "UpdatedUTC", typeof(Nullable<System.DateTime>), false, false, false, true,  (int)ViewItemFieldIndex.UpdatedUTC, 0, 0, 0);
		}
		
	}
}




