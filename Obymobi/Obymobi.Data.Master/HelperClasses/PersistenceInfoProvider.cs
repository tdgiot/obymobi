﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.Master.HelperClasses
{
	/// <summary>Singleton implementation of the PersistenceInfoProvider. This class is the singleton wrapper through which the actual instance is retrieved.</summary>
	/// <remarks>It uses a single instance of an internal class. The access isn't marked with locks as the PersistenceInfoProviderBase class is threadsafe.</remarks>
	internal static class PersistenceInfoProviderSingleton
	{
		#region Class Member Declarations
		private static readonly IPersistenceInfoProvider _providerInstance = new PersistenceInfoProviderCore();
		#endregion

		/// <summary>Dummy static constructor to make sure threadsafe initialization is performed.</summary>
		static PersistenceInfoProviderSingleton()
		{
		}

		/// <summary>Gets the singleton instance of the PersistenceInfoProviderCore</summary>
		/// <returns>Instance of the PersistenceInfoProvider.</returns>
		public static IPersistenceInfoProvider GetInstance()
		{
			return _providerInstance;
		}
	}

	/// <summary>Actual implementation of the PersistenceInfoProvider. Used by singleton wrapper.</summary>
	internal class PersistenceInfoProviderCore : PersistenceInfoProviderBase
	{
		/// <summary>Initializes a new instance of the <see cref="PersistenceInfoProviderCore"/> class.</summary>
		internal PersistenceInfoProviderCore()
		{
			Init();
		}

		/// <summary>Method which initializes the internal datastores with the structure of hierarchical types.</summary>
		private void Init()
		{
			this.InitClass(19);
			InitCountryEntityMappings();
			InitCurrencyEntityMappings();
			InitEntityFieldInformationEntityMappings();
			InitEntityInformationEntityMappings();
			InitModuleEntityMappings();
			InitReferentialConstraintEntityMappings();
			InitRoleEntityMappings();
			InitRoleEntityRightsEntityMappings();
			InitRoleModuleRightsEntityMappings();
			InitRoleUIElementRightsEntityMappings();
			InitRoleUIElementSubPanelRightsEntityMappings();
			InitTimeZoneEntityMappings();
			InitTranslationEntityMappings();
			InitUIColorEntityMappings();
			InitUIElementEntityMappings();
			InitUIElementSubPanelEntityMappings();
			InitUIElementSubPanelUIElementEntityMappings();
			InitViewEntityMappings();
			InitViewItemEntityMappings();
		}

		/// <summary>Inits CountryEntity's mappings</summary>
		private void InitCountryEntityMappings()
		{
			this.AddElementMapping("CountryEntity", @"", @"dbo", "Country", 12, 0);
			this.AddElementFieldMapping("CountryEntity", "CountryId", "CountryId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("CountryEntity", "Name", "Name", true, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 1);
			this.AddElementFieldMapping("CountryEntity", "Code", "Code", true, "NVarChar", 2, 0, 0, false, "", null, typeof(System.String), 2);
			this.AddElementFieldMapping("CountryEntity", "CodeAlpha3", "CodeAlpha3", true, "VarChar", 3, 0, 0, false, "", null, typeof(System.String), 3);
			this.AddElementFieldMapping("CountryEntity", "CultureName", "CultureName", true, "NVarChar", 10, 0, 0, false, "", null, typeof(System.String), 4);
			this.AddElementFieldMapping("CountryEntity", "CurrencyId", "CurrencyId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 5);
			this.AddElementFieldMapping("CountryEntity", "Created", "Created", true, "DateTime", 0, 0, 0, false, "", null, typeof(System.DateTime), 6);
			this.AddElementFieldMapping("CountryEntity", "CreatedBy", "CreatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 7);
			this.AddElementFieldMapping("CountryEntity", "Updated", "Updated", true, "DateTime", 0, 0, 0, false, "", null, typeof(System.DateTime), 8);
			this.AddElementFieldMapping("CountryEntity", "UpdatedBy", "UpdatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 9);
			this.AddElementFieldMapping("CountryEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 10);
			this.AddElementFieldMapping("CountryEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 11);
		}

		/// <summary>Inits CurrencyEntity's mappings</summary>
		private void InitCurrencyEntityMappings()
		{
			this.AddElementMapping("CurrencyEntity", @"", @"dbo", "Currency", 10, 0);
			this.AddElementFieldMapping("CurrencyEntity", "CurrencyId", "CurrencyId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("CurrencyEntity", "Name", "Name", true, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 1);
			this.AddElementFieldMapping("CurrencyEntity", "Symbol", "Symbol", true, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 2);
			this.AddElementFieldMapping("CurrencyEntity", "CodeIso4217", "CodeIso4217", false, "Char", 3, 0, 0, false, "", null, typeof(System.String), 3);
			this.AddElementFieldMapping("CurrencyEntity", "Created", "Created", true, "DateTime", 0, 0, 0, false, "", null, typeof(System.DateTime), 4);
			this.AddElementFieldMapping("CurrencyEntity", "CreatedBy", "CreatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 5);
			this.AddElementFieldMapping("CurrencyEntity", "Updated", "Updated", true, "DateTime", 0, 0, 0, false, "", null, typeof(System.DateTime), 6);
			this.AddElementFieldMapping("CurrencyEntity", "UpdatedBy", "UpdatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 7);
			this.AddElementFieldMapping("CurrencyEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 8);
			this.AddElementFieldMapping("CurrencyEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 9);
		}

		/// <summary>Inits EntityFieldInformationEntity's mappings</summary>
		private void InitEntityFieldInformationEntityMappings()
		{
			this.AddElementMapping("EntityFieldInformationEntity", @"", @"dbo", "EntityFieldInformation", 32, 0);
			this.AddElementFieldMapping("EntityFieldInformationEntity", "EntityFieldInformationId", "EntityFieldInformationId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("EntityFieldInformationEntity", "EntityName", "EntityName", true, "NVarChar", 150, 0, 0, false, "", null, typeof(System.String), 1);
			this.AddElementFieldMapping("EntityFieldInformationEntity", "FieldName", "FieldName", true, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 2);
			this.AddElementFieldMapping("EntityFieldInformationEntity", "FriendlyName", "FriendlyName", true, "NVarChar", 100, 0, 0, false, "", null, typeof(System.String), 3);
			this.AddElementFieldMapping("EntityFieldInformationEntity", "ShowOnDefaultGridView", "ShowOnDefaultGridView", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 4);
			this.AddElementFieldMapping("EntityFieldInformationEntity", "DefaultDisplayPosition", "DefaultDisplayPosition", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 5);
			this.AddElementFieldMapping("EntityFieldInformationEntity", "AllowShowOnGridView", "AllowShowOnGridView", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 6);
			this.AddElementFieldMapping("EntityFieldInformationEntity", "HelpText", "HelpText", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 7);
			this.AddElementFieldMapping("EntityFieldInformationEntity", "DisplayFormatString", "DisplayFormatString", true, "NVarChar", 100, 0, 0, false, "", null, typeof(System.String), 8);
			this.AddElementFieldMapping("EntityFieldInformationEntity", "DefaultSortPosition", "DefaultSortPosition", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 9);
			this.AddElementFieldMapping("EntityFieldInformationEntity", "DefaultSortOperator", "DefaultSortOperator", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 10);
			this.AddElementFieldMapping("EntityFieldInformationEntity", "Type", "Type", true, "NVarChar", 100, 0, 0, false, "", null, typeof(System.String), 11);
			this.AddElementFieldMapping("EntityFieldInformationEntity", "IsRequired", "IsRequired", true, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 12);
			this.AddElementFieldMapping("EntityFieldInformationEntity", "EntityFieldType", "EntityFieldType", true, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 13);
			this.AddElementFieldMapping("EntityFieldInformationEntity", "ExcludeForUpdateWmsCacheDateOnChange", "ExcludeForUpdateWmsCacheDateOnChange", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 14);
			this.AddElementFieldMapping("EntityFieldInformationEntity", "ExcludeForUpdateCompanyDataVersionOnChange", "ExcludeForUpdateCompanyDataVersionOnChange", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 15);
			this.AddElementFieldMapping("EntityFieldInformationEntity", "ExcludeForUpdateMenuDataVersionOnChange", "ExcludeForUpdateMenuDataVersionOnChange", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 16);
			this.AddElementFieldMapping("EntityFieldInformationEntity", "ExcludeForUpdatePosIntegrationInformationVersionOnChange", "ExcludeForUpdatePosIntegrationInformationVersionOnChange", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 17);
			this.AddElementFieldMapping("EntityFieldInformationEntity", "ForLocalUseWhileSyncingIsUpdated", "ForLocalUseWhileSyncingIsUpdated", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 18);
			this.AddElementFieldMapping("EntityFieldInformationEntity", "Archived", "Archived", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 19);
			this.AddElementFieldMapping("EntityFieldInformationEntity", "Created", "Created", true, "DateTime", 0, 0, 0, false, "", null, typeof(System.DateTime), 20);
			this.AddElementFieldMapping("EntityFieldInformationEntity", "CreatedBy", "CreatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 21);
			this.AddElementFieldMapping("EntityFieldInformationEntity", "Updated", "Updated", true, "DateTime", 0, 0, 0, false, "", null, typeof(System.DateTime), 22);
			this.AddElementFieldMapping("EntityFieldInformationEntity", "UpdatedBy", "UpdatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 23);
			this.AddElementFieldMapping("EntityFieldInformationEntity", "Deleted", "Deleted", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 24);
			this.AddElementFieldMapping("EntityFieldInformationEntity", "ExcludeForUpdateDeliverypointDataVersionOnChange", "ExcludeForUpdateDeliverypointDataVersionOnChange", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 25);
			this.AddElementFieldMapping("EntityFieldInformationEntity", "ExcludeForUpdateSurveyDataVersionOnChange", "ExcludeForUpdateSurveyDataVersionOnChange", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 26);
			this.AddElementFieldMapping("EntityFieldInformationEntity", "ExcludeForUpdateAnnouncementDataVersionOnChange", "ExcludeForUpdateAnnouncementDataVersionOnChange", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 27);
			this.AddElementFieldMapping("EntityFieldInformationEntity", "ExcludeForUpdateEntertainmentDataVersionOnChange", "ExcludeForUpdateEntertainmentDataVersionOnChange", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 28);
			this.AddElementFieldMapping("EntityFieldInformationEntity", "ExcludeForUpdateAdvertisementDataVersionOnChange", "ExcludeForUpdateAdvertisementDataVersionOnChange", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 29);
			this.AddElementFieldMapping("EntityFieldInformationEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 30);
			this.AddElementFieldMapping("EntityFieldInformationEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 31);
		}

		/// <summary>Inits EntityInformationEntity's mappings</summary>
		private void InitEntityInformationEntityMappings()
		{
			this.AddElementMapping("EntityInformationEntity", @"", @"dbo", "EntityInformation", 28, 0);
			this.AddElementFieldMapping("EntityInformationEntity", "EntityInformationId", "EntityInformationId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("EntityInformationEntity", "EntityName", "EntityName", false, "VarChar", 250, 0, 0, false, "", null, typeof(System.String), 1);
			this.AddElementFieldMapping("EntityInformationEntity", "ShowFieldName", "ShowFieldName", true, "NVarChar", 250, 0, 0, false, "", null, typeof(System.String), 2);
			this.AddElementFieldMapping("EntityInformationEntity", "ShowFieldNameBreadCrumb", "ShowFieldNameBreadCrumb", true, "NVarChar", 250, 0, 0, false, "", null, typeof(System.String), 3);
			this.AddElementFieldMapping("EntityInformationEntity", "FriendlyName", "FriendlyName", true, "VarChar", 150, 0, 0, false, "", null, typeof(System.String), 4);
			this.AddElementFieldMapping("EntityInformationEntity", "FriendlyNamePlural", "FriendlyNamePlural", true, "VarChar", 150, 0, 0, false, "", null, typeof(System.String), 5);
			this.AddElementFieldMapping("EntityInformationEntity", "HelpText", "HelpText", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 6);
			this.AddElementFieldMapping("EntityInformationEntity", "TitleAsBreadCrumbHierarchy", "TitleAsBreadCrumbHierarchy", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 7);
			this.AddElementFieldMapping("EntityInformationEntity", "DefaultEntityEditPage", "DefaultEntityEditPage", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 8);
			this.AddElementFieldMapping("EntityInformationEntity", "DefaultEntityCollectionPage", "DefaultEntityCollectionPage", true, "VarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 9);
			this.AddElementFieldMapping("EntityInformationEntity", "UpdateWmsCacheDateOnChange", "UpdateWmsCacheDateOnChange", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 10);
			this.AddElementFieldMapping("EntityInformationEntity", "UpdateCompanyDataVersionOnChange", "UpdateCompanyDataVersionOnChange", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 11);
			this.AddElementFieldMapping("EntityInformationEntity", "UpdateMenuDataVersionOnChange", "UpdateMenuDataVersionOnChange", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 12);
			this.AddElementFieldMapping("EntityInformationEntity", "UpdatePosIntegrationInformationOnChange", "UpdatePosIntegrationInformationOnChange", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 13);
			this.AddElementFieldMapping("EntityInformationEntity", "ForLocalUseWhileSyncingIsUpdated", "ForLocalUseWhileSyncingIsUpdated", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 14);
			this.AddElementFieldMapping("EntityInformationEntity", "Archived", "Archived", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 15);
			this.AddElementFieldMapping("EntityInformationEntity", "Created", "Created", true, "DateTime", 0, 0, 0, false, "", null, typeof(System.DateTime), 16);
			this.AddElementFieldMapping("EntityInformationEntity", "CreatedBy", "CreatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 17);
			this.AddElementFieldMapping("EntityInformationEntity", "Updated", "Updated", true, "DateTime", 0, 0, 0, false, "", null, typeof(System.DateTime), 18);
			this.AddElementFieldMapping("EntityInformationEntity", "UpdatedBy", "UpdatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 19);
			this.AddElementFieldMapping("EntityInformationEntity", "Deleted", "Deleted", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 20);
			this.AddElementFieldMapping("EntityInformationEntity", "UpdateDeliverypointDataVersionOnChange", "UpdateDeliverypointDataVersionOnChange", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 21);
			this.AddElementFieldMapping("EntityInformationEntity", "UpdateSurveyDataVersionOnChange", "UpdateSurveyDataVersionOnChange", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 22);
			this.AddElementFieldMapping("EntityInformationEntity", "UpdateAnnouncementDataVersionOnChange", "UpdateAnnouncementDataVersionOnChange", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 23);
			this.AddElementFieldMapping("EntityInformationEntity", "UpdateEntertainmentDataVersionOnChange", "UpdateEntertainmentDataVersionOnChange", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 24);
			this.AddElementFieldMapping("EntityInformationEntity", "UpdateAdvertisementDataVersionOnChange", "UpdateAdvertisementDataVersionOnChange", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 25);
			this.AddElementFieldMapping("EntityInformationEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 26);
			this.AddElementFieldMapping("EntityInformationEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 27);
		}

		/// <summary>Inits ModuleEntity's mappings</summary>
		private void InitModuleEntityMappings()
		{
			this.AddElementMapping("ModuleEntity", @"", @"dbo", "Module", 18, 0);
			this.AddElementFieldMapping("ModuleEntity", "ModuleId", "ModuleId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("ModuleEntity", "NameSystem", "NameSystem", true, "VarChar", 50, 0, 0, false, "", null, typeof(System.String), 1);
			this.AddElementFieldMapping("ModuleEntity", "NameFull", "NameFull", true, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 2);
			this.AddElementFieldMapping("ModuleEntity", "NameShort", "NameShort", true, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 3);
			this.AddElementFieldMapping("ModuleEntity", "IconPath", "IconPath", true, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 4);
			this.AddElementFieldMapping("ModuleEntity", "DefaultUiElementTypeNameFull", "DefaultUiElementTypeNameFull", true, "VarChar", 255, 0, 0, false, "", null, typeof(System.String), 5);
			this.AddElementFieldMapping("ModuleEntity", "SortOrder", "SortOrder", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 6);
			this.AddElementFieldMapping("ModuleEntity", "LicensingFree", "LicensingFree", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 7);
			this.AddElementFieldMapping("ModuleEntity", "UserRightsFree", "UserRightsFree", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 8);
			this.AddElementFieldMapping("ModuleEntity", "ForLocalUseWhileSyncingIsUpdated", "ForLocalUseWhileSyncingIsUpdated", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 9);
			this.AddElementFieldMapping("ModuleEntity", "Archived", "Archived", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 10);
			this.AddElementFieldMapping("ModuleEntity", "Created", "Created", true, "DateTime", 0, 0, 0, false, "", null, typeof(System.DateTime), 11);
			this.AddElementFieldMapping("ModuleEntity", "CreatedBy", "CreatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 12);
			this.AddElementFieldMapping("ModuleEntity", "Updated", "Updated", true, "DateTime", 0, 0, 0, false, "", null, typeof(System.DateTime), 13);
			this.AddElementFieldMapping("ModuleEntity", "UpdatedBy", "UpdatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 14);
			this.AddElementFieldMapping("ModuleEntity", "Deleted", "Deleted", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 15);
			this.AddElementFieldMapping("ModuleEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 16);
			this.AddElementFieldMapping("ModuleEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 17);
		}

		/// <summary>Inits ReferentialConstraintEntity's mappings</summary>
		private void InitReferentialConstraintEntityMappings()
		{
			this.AddElementMapping("ReferentialConstraintEntity", @"", @"dbo", "ReferentialConstraint", 15, 0);
			this.AddElementFieldMapping("ReferentialConstraintEntity", "ReferentialConstraintId", "ReferentialConstraintId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("ReferentialConstraintEntity", "EntityName", "EntityName", true, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 1);
			this.AddElementFieldMapping("ReferentialConstraintEntity", "ForeignKeyName", "ForeignKeyName", true, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 2);
			this.AddElementFieldMapping("ReferentialConstraintEntity", "DeleteRule", "DeleteRule", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("ReferentialConstraintEntity", "UpdateRule", "UpdateRule", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("ReferentialConstraintEntity", "ArchiveRule", "ArchiveRule", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 5);
			this.AddElementFieldMapping("ReferentialConstraintEntity", "ForLocalUseWhileSyncingIsUpdated", "ForLocalUseWhileSyncingIsUpdated", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 6);
			this.AddElementFieldMapping("ReferentialConstraintEntity", "Archived", "Archived", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 7);
			this.AddElementFieldMapping("ReferentialConstraintEntity", "Created", "Created", true, "DateTime", 0, 0, 0, false, "", null, typeof(System.DateTime), 8);
			this.AddElementFieldMapping("ReferentialConstraintEntity", "CreatedBy", "CreatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 9);
			this.AddElementFieldMapping("ReferentialConstraintEntity", "Updated", "Updated", true, "DateTime", 0, 0, 0, false, "", null, typeof(System.DateTime), 10);
			this.AddElementFieldMapping("ReferentialConstraintEntity", "UpdatedBy", "UpdatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 11);
			this.AddElementFieldMapping("ReferentialConstraintEntity", "Deleted", "Deleted", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 12);
			this.AddElementFieldMapping("ReferentialConstraintEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 13);
			this.AddElementFieldMapping("ReferentialConstraintEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 14);
		}

		/// <summary>Inits RoleEntity's mappings</summary>
		private void InitRoleEntityMappings()
		{
			this.AddElementMapping("RoleEntity", @"", @"dbo", "Role", 14, 0);
			this.AddElementFieldMapping("RoleEntity", "RoleId", "RoleId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("RoleEntity", "ParentRoleId", "ParentRoleId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("RoleEntity", "Name", "Name", true, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 2);
			this.AddElementFieldMapping("RoleEntity", "IsAdministrator", "IsAdministrator", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 3);
			this.AddElementFieldMapping("RoleEntity", "SystemName", "SystemName", true, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 4);
			this.AddElementFieldMapping("RoleEntity", "SystemRole", "SystemRole", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 5);
			this.AddElementFieldMapping("RoleEntity", "Created", "Created", true, "DateTime", 0, 0, 0, false, "", null, typeof(System.DateTime), 6);
			this.AddElementFieldMapping("RoleEntity", "CreatedBy", "CreatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 7);
			this.AddElementFieldMapping("RoleEntity", "Updated", "Updated", true, "DateTime", 0, 0, 0, false, "", null, typeof(System.DateTime), 8);
			this.AddElementFieldMapping("RoleEntity", "UpdatedBy", "UpdatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 9);
			this.AddElementFieldMapping("RoleEntity", "Deleted", "Deleted", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 10);
			this.AddElementFieldMapping("RoleEntity", "Archived", "Archived", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 11);
			this.AddElementFieldMapping("RoleEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 12);
			this.AddElementFieldMapping("RoleEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 13);
		}

		/// <summary>Inits RoleEntityRightsEntity's mappings</summary>
		private void InitRoleEntityRightsEntityMappings()
		{
			this.AddElementMapping("RoleEntityRightsEntity", @"", @"dbo", "RoleEntityRights", 17, 0);
			this.AddElementFieldMapping("RoleEntityRightsEntity", "RoleEntityRightId", "RoleEntityRightId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("RoleEntityRightsEntity", "RoleId", "RoleId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("RoleEntityRightsEntity", "EntityInformationId", "EntityInformationId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("RoleEntityRightsEntity", "AllowReadAll", "AllowReadAll", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 3);
			this.AddElementFieldMapping("RoleEntityRightsEntity", "AllowUpdateAll", "AllowUpdateAll", true, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 4);
			this.AddElementFieldMapping("RoleEntityRightsEntity", "AllowInsert", "AllowInsert", true, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 5);
			this.AddElementFieldMapping("RoleEntityRightsEntity", "AllowDeleteAll", "AllowDeleteAll", true, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 6);
			this.AddElementFieldMapping("RoleEntityRightsEntity", "AllowUpdateCreated", "AllowUpdateCreated", true, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 7);
			this.AddElementFieldMapping("RoleEntityRightsEntity", "AllowDeleteCreated", "AllowDeleteCreated", true, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 8);
			this.AddElementFieldMapping("RoleEntityRightsEntity", "Created", "Created", true, "DateTime", 0, 0, 0, false, "", null, typeof(System.DateTime), 9);
			this.AddElementFieldMapping("RoleEntityRightsEntity", "CreatedBy", "CreatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 10);
			this.AddElementFieldMapping("RoleEntityRightsEntity", "Updated", "Updated", true, "DateTime", 0, 0, 0, false, "", null, typeof(System.DateTime), 11);
			this.AddElementFieldMapping("RoleEntityRightsEntity", "UpdatedBy", "UpdatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 12);
			this.AddElementFieldMapping("RoleEntityRightsEntity", "Deleted", "Deleted", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 13);
			this.AddElementFieldMapping("RoleEntityRightsEntity", "Archived", "Archived", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 14);
			this.AddElementFieldMapping("RoleEntityRightsEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 15);
			this.AddElementFieldMapping("RoleEntityRightsEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 16);
		}

		/// <summary>Inits RoleModuleRightsEntity's mappings</summary>
		private void InitRoleModuleRightsEntityMappings()
		{
			this.AddElementMapping("RoleModuleRightsEntity", @"", @"dbo", "RoleModuleRights", 12, 0);
			this.AddElementFieldMapping("RoleModuleRightsEntity", "RoleModuleRightsId", "RoleModuleRightsId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("RoleModuleRightsEntity", "RoleId", "RoleId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("RoleModuleRightsEntity", "ModuleId", "ModuleId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("RoleModuleRightsEntity", "IsAllowed", "IsAllowed", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 3);
			this.AddElementFieldMapping("RoleModuleRightsEntity", "Archived", "Archived", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 4);
			this.AddElementFieldMapping("RoleModuleRightsEntity", "Created", "Created", true, "DateTime", 0, 0, 0, false, "", null, typeof(System.DateTime), 5);
			this.AddElementFieldMapping("RoleModuleRightsEntity", "CreatedBy", "CreatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 6);
			this.AddElementFieldMapping("RoleModuleRightsEntity", "Updated", "Updated", true, "DateTime", 0, 0, 0, false, "", null, typeof(System.DateTime), 7);
			this.AddElementFieldMapping("RoleModuleRightsEntity", "UpdatedBy", "UpdatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 8);
			this.AddElementFieldMapping("RoleModuleRightsEntity", "Deleted", "Deleted", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 9);
			this.AddElementFieldMapping("RoleModuleRightsEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 10);
			this.AddElementFieldMapping("RoleModuleRightsEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 11);
		}

		/// <summary>Inits RoleUIElementRightsEntity's mappings</summary>
		private void InitRoleUIElementRightsEntityMappings()
		{
			this.AddElementMapping("RoleUIElementRightsEntity", @"", @"dbo", "RoleUIElementRights", 13, 0);
			this.AddElementFieldMapping("RoleUIElementRightsEntity", "RoleUIElementRightsId", "RoleUIElementRightsId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("RoleUIElementRightsEntity", "RoleId", "RoleId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("RoleUIElementRightsEntity", "UiElementTypeNameFull", "UiElementTypeNameFull", false, "VarChar", 255, 0, 0, false, "", null, typeof(System.String), 2);
			this.AddElementFieldMapping("RoleUIElementRightsEntity", "IsAllowed", "IsAllowed", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 3);
			this.AddElementFieldMapping("RoleUIElementRightsEntity", "IsReadOnly", "IsReadOnly", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 4);
			this.AddElementFieldMapping("RoleUIElementRightsEntity", "Archived", "Archived", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 5);
			this.AddElementFieldMapping("RoleUIElementRightsEntity", "Created", "Created", true, "DateTime", 0, 0, 0, false, "", null, typeof(System.DateTime), 6);
			this.AddElementFieldMapping("RoleUIElementRightsEntity", "CreatedBy", "CreatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 7);
			this.AddElementFieldMapping("RoleUIElementRightsEntity", "Updated", "Updated", true, "DateTime", 0, 0, 0, false, "", null, typeof(System.DateTime), 8);
			this.AddElementFieldMapping("RoleUIElementRightsEntity", "UpdatedBy", "UpdatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 9);
			this.AddElementFieldMapping("RoleUIElementRightsEntity", "Deleted", "Deleted", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 10);
			this.AddElementFieldMapping("RoleUIElementRightsEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 11);
			this.AddElementFieldMapping("RoleUIElementRightsEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 12);
		}

		/// <summary>Inits RoleUIElementSubPanelRightsEntity's mappings</summary>
		private void InitRoleUIElementSubPanelRightsEntityMappings()
		{
			this.AddElementMapping("RoleUIElementSubPanelRightsEntity", @"", @"dbo", "RoleUIElementSubPanelRights", 12, 0);
			this.AddElementFieldMapping("RoleUIElementSubPanelRightsEntity", "RoleUIElementSubPanelRightsId", "RoleUIElementSubPanelRightsId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("RoleUIElementSubPanelRightsEntity", "RoleId", "RoleId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("RoleUIElementSubPanelRightsEntity", "UiElementTypeNameFull", "UiElementTypeNameFull", false, "VarChar", 255, 0, 0, false, "", null, typeof(System.String), 2);
			this.AddElementFieldMapping("RoleUIElementSubPanelRightsEntity", "IsAllowed", "IsAllowed", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 3);
			this.AddElementFieldMapping("RoleUIElementSubPanelRightsEntity", "Archived", "Archived", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 4);
			this.AddElementFieldMapping("RoleUIElementSubPanelRightsEntity", "Created", "Created", true, "DateTime", 0, 0, 0, false, "", null, typeof(System.DateTime), 5);
			this.AddElementFieldMapping("RoleUIElementSubPanelRightsEntity", "CreatedBy", "CreatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 6);
			this.AddElementFieldMapping("RoleUIElementSubPanelRightsEntity", "Updated", "Updated", true, "DateTime", 0, 0, 0, false, "", null, typeof(System.DateTime), 7);
			this.AddElementFieldMapping("RoleUIElementSubPanelRightsEntity", "UpdatedBy", "UpdatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 8);
			this.AddElementFieldMapping("RoleUIElementSubPanelRightsEntity", "Deleted", "Deleted", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 9);
			this.AddElementFieldMapping("RoleUIElementSubPanelRightsEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 10);
			this.AddElementFieldMapping("RoleUIElementSubPanelRightsEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 11);
		}

		/// <summary>Inits TimeZoneEntity's mappings</summary>
		private void InitTimeZoneEntityMappings()
		{
			this.AddElementMapping("TimeZoneEntity", @"", @"dbo", "TimeZone", 10, 0);
			this.AddElementFieldMapping("TimeZoneEntity", "TimeZoneId", "TimeZoneId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("TimeZoneEntity", "Name", "Name", false, "VarChar", 255, 0, 0, false, "", null, typeof(System.String), 1);
			this.AddElementFieldMapping("TimeZoneEntity", "NameAndroid", "NameAndroid", false, "VarChar", 255, 0, 0, false, "", null, typeof(System.String), 2);
			this.AddElementFieldMapping("TimeZoneEntity", "NameDotNet", "NameDotNet", false, "VarChar", 255, 0, 0, false, "", null, typeof(System.String), 3);
			this.AddElementFieldMapping("TimeZoneEntity", "Created", "Created", true, "DateTime", 0, 0, 0, false, "", null, typeof(System.DateTime), 4);
			this.AddElementFieldMapping("TimeZoneEntity", "CreatedBy", "CreatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 5);
			this.AddElementFieldMapping("TimeZoneEntity", "Updated", "Updated", true, "DateTime", 0, 0, 0, false, "", null, typeof(System.DateTime), 6);
			this.AddElementFieldMapping("TimeZoneEntity", "UpdatedBy", "UpdatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 7);
			this.AddElementFieldMapping("TimeZoneEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 8);
			this.AddElementFieldMapping("TimeZoneEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 9);
		}

		/// <summary>Inits TranslationEntity's mappings</summary>
		private void InitTranslationEntityMappings()
		{
			this.AddElementMapping("TranslationEntity", @"", @"dbo", "Translation", 13, 0);
			this.AddElementFieldMapping("TranslationEntity", "TranslationId", "TranslationId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("TranslationEntity", "TranslationKey", "TranslationKey", false, "VarChar", 500, 0, 0, false, "", null, typeof(System.String), 1);
			this.AddElementFieldMapping("TranslationEntity", "LanguageCode", "LanguageCode", false, "Char", 2, 0, 0, false, "", null, typeof(System.String), 2);
			this.AddElementFieldMapping("TranslationEntity", "TranslationValue", "TranslationValue", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 3);
			this.AddElementFieldMapping("TranslationEntity", "LocallyCreated", "LocallyCreated", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 4);
			this.AddElementFieldMapping("TranslationEntity", "Archived", "Archived", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 5);
			this.AddElementFieldMapping("TranslationEntity", "Created", "Created", true, "DateTime", 0, 0, 0, false, "", null, typeof(System.DateTime), 6);
			this.AddElementFieldMapping("TranslationEntity", "CreatedBy", "CreatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 7);
			this.AddElementFieldMapping("TranslationEntity", "Updated", "Updated", true, "DateTime", 0, 0, 0, false, "", null, typeof(System.DateTime), 8);
			this.AddElementFieldMapping("TranslationEntity", "UpdatedBy", "UpdatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 9);
			this.AddElementFieldMapping("TranslationEntity", "Deleted", "Deleted", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 10);
			this.AddElementFieldMapping("TranslationEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 11);
			this.AddElementFieldMapping("TranslationEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 12);
		}

		/// <summary>Inits UIColorEntity's mappings</summary>
		private void InitUIColorEntityMappings()
		{
			this.AddElementMapping("UIColorEntity", @"", @"dbo", "UIColor", 8, 0);
			this.AddElementFieldMapping("UIColorEntity", "UIColorId", "UIColorId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("UIColorEntity", "Type", "Type", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("UIColorEntity", "Color", "Color", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("UIColorEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 3);
			this.AddElementFieldMapping("UIColorEntity", "CreatedBy", "CreatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("UIColorEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 5);
			this.AddElementFieldMapping("UIColorEntity", "UpdatedBy", "UpdatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 6);
			this.AddElementFieldMapping("UIColorEntity", "GroupName", "GroupName", true, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 7);
		}

		/// <summary>Inits UIElementEntity's mappings</summary>
		private void InitUIElementEntityMappings()
		{
			this.AddElementMapping("UIElementEntity", @"", @"dbo", "UIElement", 23, 0);
			this.AddElementFieldMapping("UIElementEntity", "UIElementId", "UIElementId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("UIElementEntity", "ModuleNameSystem", "ModuleNameSystem", true, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 1);
			this.AddElementFieldMapping("UIElementEntity", "NameSystem", "NameSystem", true, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 2);
			this.AddElementFieldMapping("UIElementEntity", "NameFull", "NameFull", true, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 3);
			this.AddElementFieldMapping("UIElementEntity", "NameShort", "NameShort", true, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 4);
			this.AddElementFieldMapping("UIElementEntity", "Url", "Url", true, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 5);
			this.AddElementFieldMapping("UIElementEntity", "DisplayInMenu", "DisplayInMenu", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 6);
			this.AddElementFieldMapping("UIElementEntity", "TypeNameFull", "TypeNameFull", true, "VarChar", 255, 0, 0, false, "", null, typeof(System.String), 7);
			this.AddElementFieldMapping("UIElementEntity", "SortOrder", "SortOrder", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 8);
			this.AddElementFieldMapping("UIElementEntity", "FreeAccess", "FreeAccess", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 9);
			this.AddElementFieldMapping("UIElementEntity", "LicensingFree", "LicensingFree", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 10);
			this.AddElementFieldMapping("UIElementEntity", "UserRightsFree", "UserRightsFree", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 11);
			this.AddElementFieldMapping("UIElementEntity", "IsConfigurationItem", "IsConfigurationItem", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 12);
			this.AddElementFieldMapping("UIElementEntity", "EntityName", "EntityName", true, "NVarChar", 150, 0, 0, false, "", null, typeof(System.String), 13);
			this.AddElementFieldMapping("UIElementEntity", "ForLocalUseWhileSyncingIsUpdated", "ForLocalUseWhileSyncingIsUpdated", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 14);
			this.AddElementFieldMapping("UIElementEntity", "Deleted", "Deleted", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 15);
			this.AddElementFieldMapping("UIElementEntity", "Archived", "Archived", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 16);
			this.AddElementFieldMapping("UIElementEntity", "Created", "Created", true, "DateTime", 0, 0, 0, false, "", null, typeof(System.DateTime), 17);
			this.AddElementFieldMapping("UIElementEntity", "CreatedBy", "CreatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 18);
			this.AddElementFieldMapping("UIElementEntity", "Updated", "Updated", true, "DateTime", 0, 0, 0, false, "", null, typeof(System.DateTime), 19);
			this.AddElementFieldMapping("UIElementEntity", "UpdatedBy", "UpdatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 20);
			this.AddElementFieldMapping("UIElementEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 21);
			this.AddElementFieldMapping("UIElementEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 22);
		}

		/// <summary>Inits UIElementSubPanelEntity's mappings</summary>
		private void InitUIElementSubPanelEntityMappings()
		{
			this.AddElementMapping("UIElementSubPanelEntity", @"", @"dbo", "UIElementSubPanel", 20, 0);
			this.AddElementFieldMapping("UIElementSubPanelEntity", "UIElementSubPanelId", "UIElementSubPanelId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("UIElementSubPanelEntity", "NameSystem", "NameSystem", true, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 1);
			this.AddElementFieldMapping("UIElementSubPanelEntity", "NameFull", "NameFull", true, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 2);
			this.AddElementFieldMapping("UIElementSubPanelEntity", "NameShort", "NameShort", true, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 3);
			this.AddElementFieldMapping("UIElementSubPanelEntity", "Url", "Url", true, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 4);
			this.AddElementFieldMapping("UIElementSubPanelEntity", "TypeNameFull", "TypeNameFull", true, "VarChar", 255, 0, 0, false, "", null, typeof(System.String), 5);
			this.AddElementFieldMapping("UIElementSubPanelEntity", "SortOrder", "SortOrder", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 6);
			this.AddElementFieldMapping("UIElementSubPanelEntity", "FreeAccess", "FreeAccess", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 7);
			this.AddElementFieldMapping("UIElementSubPanelEntity", "LicensingFree", "LicensingFree", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 8);
			this.AddElementFieldMapping("UIElementSubPanelEntity", "EntityName", "EntityName", true, "NVarChar", 150, 0, 0, false, "", null, typeof(System.String), 9);
			this.AddElementFieldMapping("UIElementSubPanelEntity", "OnTabPage", "OnTabPage", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 10);
			this.AddElementFieldMapping("UIElementSubPanelEntity", "ForLocalUseWhileSyncingIsUpdated", "ForLocalUseWhileSyncingIsUpdated", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 11);
			this.AddElementFieldMapping("UIElementSubPanelEntity", "Deleted", "Deleted", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 12);
			this.AddElementFieldMapping("UIElementSubPanelEntity", "Archived", "Archived", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 13);
			this.AddElementFieldMapping("UIElementSubPanelEntity", "Created", "Created", true, "DateTime", 0, 0, 0, false, "", null, typeof(System.DateTime), 14);
			this.AddElementFieldMapping("UIElementSubPanelEntity", "CreatedBy", "CreatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 15);
			this.AddElementFieldMapping("UIElementSubPanelEntity", "Updated", "Updated", true, "DateTime", 0, 0, 0, false, "", null, typeof(System.DateTime), 16);
			this.AddElementFieldMapping("UIElementSubPanelEntity", "UpdatedBy", "UpdatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 17);
			this.AddElementFieldMapping("UIElementSubPanelEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 18);
			this.AddElementFieldMapping("UIElementSubPanelEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 19);
		}

		/// <summary>Inits UIElementSubPanelUIElementEntity's mappings</summary>
		private void InitUIElementSubPanelUIElementEntityMappings()
		{
			this.AddElementMapping("UIElementSubPanelUIElementEntity", @"", @"dbo", "UIElementSubPanelUIElement", 12, 0);
			this.AddElementFieldMapping("UIElementSubPanelUIElementEntity", "UIElementSubPanelUIElementId", "UIElementSubPanelUIElementId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("UIElementSubPanelUIElementEntity", "UIElementSubPanelTypeNameFull", "UIElementSubPanelTypeNameFull", false, "VarChar", 255, 0, 0, false, "", null, typeof(System.String), 1);
			this.AddElementFieldMapping("UIElementSubPanelUIElementEntity", "ParentUIElementTypeNameFull", "ParentUIElementTypeNameFull", true, "VarChar", 255, 0, 0, false, "", null, typeof(System.String), 2);
			this.AddElementFieldMapping("UIElementSubPanelUIElementEntity", "ParentUIElementSubPanelTypeNameFull", "ParentUIElementSubPanelTypeNameFull", true, "VarChar", 255, 0, 0, false, "", null, typeof(System.String), 3);
			this.AddElementFieldMapping("UIElementSubPanelUIElementEntity", "ForLocalUseWhileSyncingIsUpdated", "ForLocalUseWhileSyncingIsUpdated", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 4);
			this.AddElementFieldMapping("UIElementSubPanelUIElementEntity", "Archived", "Archived", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 5);
			this.AddElementFieldMapping("UIElementSubPanelUIElementEntity", "Created", "Created", true, "DateTime", 0, 0, 0, false, "", null, typeof(System.DateTime), 6);
			this.AddElementFieldMapping("UIElementSubPanelUIElementEntity", "CreatedBy", "CreatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 7);
			this.AddElementFieldMapping("UIElementSubPanelUIElementEntity", "Updated", "Updated", true, "DateTime", 0, 0, 0, false, "", null, typeof(System.DateTime), 8);
			this.AddElementFieldMapping("UIElementSubPanelUIElementEntity", "UpdatedBy", "UpdatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 9);
			this.AddElementFieldMapping("UIElementSubPanelUIElementEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 10);
			this.AddElementFieldMapping("UIElementSubPanelUIElementEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 11);
		}

		/// <summary>Inits ViewEntity's mappings</summary>
		private void InitViewEntityMappings()
		{
			this.AddElementMapping("ViewEntity", @"", @"dbo", "View", 13, 0);
			this.AddElementFieldMapping("ViewEntity", "ViewId", "ViewId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("ViewEntity", "Name", "Name", true, "NVarChar", 150, 0, 0, false, "", null, typeof(System.String), 1);
			this.AddElementFieldMapping("ViewEntity", "EntityName", "EntityName", false, "VarChar", 250, 0, 0, false, "", null, typeof(System.String), 2);
			this.AddElementFieldMapping("ViewEntity", "UiElementTypeNameFull", "UiElementTypeNameFull", true, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 3);
			this.AddElementFieldMapping("ViewEntity", "ForLocalUseWhileSyncingIsUpdated", "ForLocalUseWhileSyncingIsUpdated", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 4);
			this.AddElementFieldMapping("ViewEntity", "Archived", "Archived", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 5);
			this.AddElementFieldMapping("ViewEntity", "Created", "Created", true, "DateTime", 0, 0, 0, false, "", null, typeof(System.DateTime), 6);
			this.AddElementFieldMapping("ViewEntity", "CreatedBy", "CreatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 7);
			this.AddElementFieldMapping("ViewEntity", "Updated", "Updated", true, "DateTime", 0, 0, 0, false, "", null, typeof(System.DateTime), 8);
			this.AddElementFieldMapping("ViewEntity", "UpdatedBy", "UpdatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 9);
			this.AddElementFieldMapping("ViewEntity", "Deleted", "Deleted", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 10);
			this.AddElementFieldMapping("ViewEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 11);
			this.AddElementFieldMapping("ViewEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 12);
		}

		/// <summary>Inits ViewItemEntity's mappings</summary>
		private void InitViewItemEntityMappings()
		{
			this.AddElementMapping("ViewItemEntity", @"", @"dbo", "ViewItem", 21, 0);
			this.AddElementFieldMapping("ViewItemEntity", "ViewItemId", "ViewItemId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("ViewItemEntity", "ViewId", "ViewId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("ViewItemEntity", "EntityName", "EntityName", true, "NVarChar", 150, 0, 0, false, "", null, typeof(System.String), 2);
			this.AddElementFieldMapping("ViewItemEntity", "FieldName", "FieldName", true, "NVarChar", 150, 0, 0, false, "", null, typeof(System.String), 3);
			this.AddElementFieldMapping("ViewItemEntity", "ShowOnGridView", "ShowOnGridView", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 4);
			this.AddElementFieldMapping("ViewItemEntity", "DisplayPosition", "DisplayPosition", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 5);
			this.AddElementFieldMapping("ViewItemEntity", "SortPosition", "SortPosition", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 6);
			this.AddElementFieldMapping("ViewItemEntity", "SortOperator", "SortOperator", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 7);
			this.AddElementFieldMapping("ViewItemEntity", "DisplayFormatString", "DisplayFormatString", true, "NVarChar", 100, 0, 0, false, "", null, typeof(System.String), 8);
			this.AddElementFieldMapping("ViewItemEntity", "Width", "Width", true, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 9);
			this.AddElementFieldMapping("ViewItemEntity", "Type", "Type", true, "NVarChar", 100, 0, 0, false, "", null, typeof(System.String), 10);
			this.AddElementFieldMapping("ViewItemEntity", "UiElementTypeNameFull", "UiElementTypeNameFull", true, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 11);
			this.AddElementFieldMapping("ViewItemEntity", "ForLocalUseWhileSyncingIsUpdated", "ForLocalUseWhileSyncingIsUpdated", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 12);
			this.AddElementFieldMapping("ViewItemEntity", "Archived", "Archived", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 13);
			this.AddElementFieldMapping("ViewItemEntity", "Created", "Created", true, "DateTime", 0, 0, 0, false, "", null, typeof(System.DateTime), 14);
			this.AddElementFieldMapping("ViewItemEntity", "CreatedBy", "CreatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 15);
			this.AddElementFieldMapping("ViewItemEntity", "Updated", "Updated", true, "DateTime", 0, 0, 0, false, "", null, typeof(System.DateTime), 16);
			this.AddElementFieldMapping("ViewItemEntity", "UpdatedBy", "UpdatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 17);
			this.AddElementFieldMapping("ViewItemEntity", "Deleted", "Deleted", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 18);
			this.AddElementFieldMapping("ViewItemEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 19);
			this.AddElementFieldMapping("ViewItemEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 20);
		}

	}
}
