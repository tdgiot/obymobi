﻿using System;
using System.Collections.Generic;
using System.Text;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Dionysos.Data.LLBLGen;
using Obymobi.Data.Master.CollectionClasses;
using Obymobi.Data.Master.EntityClasses;
using Dionysos;
using System.IO;
using Obymobi.Data.Master.HelperClasses;

namespace Obymobi.Data.Master
{
    public class UIElements
    {
        #region Fields

        static UIElements instance = null;

        #endregion

        #region Constructors

        /// <summary>
        /// Constructs an instance of the UIElements class if the instance has not been initialized yet
        /// </summary>
        public UIElements()
        {
            if (instance == null)
            {
                // first create the instance
                instance = new UIElements();
                // Then init the instance (the init needs the instance to fill it)
                instance.Init();
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Initializes the static UIElements instance
        /// </summary>
        private void Init()
        {
        }

        /// <summary>
        /// Creates UI Elements
        /// </summary>
        public static void CreateUIElements(string baseDirectory)
        {
            UIElementCollection uiElementCollection = new UIElementCollection();
            uiElementCollection.GetMulti(null);

            //RefreshLocalHelper refreshHelper = new RefreshLocalHelper(uiElementCollection);

            // Get the default entity view from the collection
            EntityView<UIElementEntity> uiElementView = uiElementCollection.DefaultView;

            string[] directories = Directory.GetDirectories(baseDirectory);
            for (int i = 0; i < directories.Length; i++)
            {
                string directory = directories[i];
                if (!directory.ToLower().Contains("cutesoft_client"))
                {
                    // Get the aspx files from the application directory and all subdirectories
                    string[] aspxFiles = Directory.GetFiles(directory, "*.aspx", SearchOption.AllDirectories);

                    for (int j = 0; j < aspxFiles.Length; j++)
                    {
                        // Get an path to a file
                        string path = aspxFiles[j];
                        string relativeUrl = path.Replace(baseDirectory, "~").Replace(@"\", "/");

                        // Create a filter for the ui element
                        // and retrieve the ui elements
                        PredicateExpression uiElementFilter = new PredicateExpression(UIElementFields.Url % relativeUrl);
                        uiElementView.Filter = uiElementFilter;

                        if (uiElementView.Count == 0)
                        {
                            // No view found for the specified EntityInformation item
                            // A default view needs to be created
                            UIElementEntity uiElementEntity = new UIElementEntity();

                            uiElementEntity.ModuleNameSystem = "TODO: MODULENAMESYSTEM";
                            uiElementEntity.NameSystem = "TODO: NAMESYSTEM";
                            uiElementEntity.NameFull = "TODO: NAMEFULL";
                            uiElementEntity.NameShort = "TODO: NAMESHORT";
                            uiElementEntity.Url = relativeUrl;
                            uiElementEntity.DisplayInMenu = false; // Default: do not display
                            uiElementEntity.TypeNameFull = "Obymobi.ObymobiCms" + relativeUrl.Replace("~", "").Replace("/", ".").Replace(".aspx", "");
                            uiElementEntity.EntityName = string.Empty;

                            try
                            {
                                uiElementEntity.Save();
                            }
                            catch
                            {
                            }

                            // Add to collection for viewing/searchgin
                            uiElementView.RelatedCollection.Add(uiElementEntity);
                        }
                        //else if (uiElementView.Count > 0)
                        //{
                        //    refreshHelper.MarkItemAsUpdated(uiElementView[0]);
                        //}
                    }
                }
            }

            //refreshHelper.DeleteAllNonUpdatedItems();
        }

        /// <summary>
        /// Refreshes the ui elements in the local database from the master database
        /// </summary>
        /// <param name="update">Flag which indicates whether existing ui elements should be updated</param>
        public static void RefreshUIElements(bool update)
        {
            // Preload UIElementCollection instance
            // and initialize the EntityView			
            IEntityCollection uielementCollection = LLBLGenEntityCollectionUtil.GetEntityCollection("UIElement");
            if (Instance.Empty(uielementCollection))
            {
                throw new EmptyException("Could not initialize entity collection for entity name 'UIElement'.");
            }
            else
            {
                // Get the default entity view from the collection
                IEntityView uielementView = uielementCollection.DefaultView;

                // Get the referential constraints from the master database
                UIElementCollection uielementCollectionMaster = new UIElementCollection();
                uielementCollectionMaster.GetMulti(null);

                // Create a refresh helper for deleting non updated entities
                RefreshLocalHelper refreshHelper = null;
                if (update)
                    refreshHelper = new RefreshLocalHelper(uielementCollection);

                PredicateExpression filter = null;

                // Walk through the referential constraints
                for (int i = 0; i < uielementCollectionMaster.Count; i++)
                {
                    UIElementEntity uielementEntityMaster = uielementCollectionMaster[i];

                    string uielementTypeNameFull = uielementEntityMaster.TypeNameFull;

                    filter = LLBLGenFilterUtil.GetFieldCompareValueEqualPredicateExpression("UIElement", "TypeNameFull", uielementTypeNameFull);
                    uielementView.Filter = filter;

                    // check count to see if we need to add
                    if (uielementView.Count > 1)
                    {
                        throw new Dionysos.TechnicalException("Multiple UIElement records found for uielement {0}.", uielementTypeNameFull);
                    }
                    else if (uielementView.Count == 0)
                    {
                        // Not found, add:
                        IEntity entityToInsert = DataFactory.EntityFactory.GetEntity("UIElement") as IEntity;
                        if (Instance.Empty(entityToInsert))
                        {
                            throw new EmptyException("Could not initialize entity of entity name 'UIElement'.");
                        }
                        else
                        {
                            // Set field values & save entity
                            entityToInsert.SetNewFieldValue("ModuleNameSystem", uielementEntityMaster.ModuleNameSystem);
                            entityToInsert.SetNewFieldValue("NameSystem", uielementEntityMaster.NameSystem);
                            entityToInsert.SetNewFieldValue("NameFull", uielementEntityMaster.NameFull);
                            entityToInsert.SetNewFieldValue("NameShort", uielementEntityMaster.NameShort);
                            entityToInsert.SetNewFieldValue("Url", uielementEntityMaster.Url);
                            entityToInsert.SetNewFieldValue("DisplayInMenu", uielementEntityMaster.DisplayInMenu);
                            entityToInsert.SetNewFieldValue("TypeNameFull", uielementEntityMaster.TypeNameFull);
                            entityToInsert.SetNewFieldValue("SortOrder", uielementEntityMaster.SortOrder);
                            entityToInsert.SetNewFieldValue("IsConfigurationItem", uielementEntityMaster.IsConfigurationItem);
                            entityToInsert.SetNewFieldValue("FreeAccess", uielementEntityMaster.FreeAccess);
                            entityToInsert.SetNewFieldValue("LicensingFree", uielementEntityMaster.LicensingFree);
                            entityToInsert.SetNewFieldValue("UserRightsFree", uielementEntityMaster.UserRightsFree);
                            entityToInsert.SetNewFieldValue("EntityName", uielementEntityMaster.EntityName);
                            entityToInsert.SetNewFieldValue("IsConfigurationItem", uielementEntityMaster.IsConfigurationItem);
                            entityToInsert.Save();

                            // Add to collection for viewing/searchgin
                            uielementView.RelatedCollection.Add(entityToInsert);
                        }
                    }
                    else if (uielementView.Count == 1 && update)
                    {
                        IEntity entityToUpdate = uielementView[0];
                        entityToUpdate.SetNewFieldValue("ModuleNameSystem", uielementEntityMaster.ModuleNameSystem);
                        entityToUpdate.SetNewFieldValue("NameSystem", uielementEntityMaster.NameSystem);
                        entityToUpdate.SetNewFieldValue("NameFull", uielementEntityMaster.NameFull);
                        entityToUpdate.SetNewFieldValue("NameShort", uielementEntityMaster.NameShort);
                        entityToUpdate.SetNewFieldValue("Url", uielementEntityMaster.Url);
                        entityToUpdate.SetNewFieldValue("DisplayInMenu", uielementEntityMaster.DisplayInMenu);
                        entityToUpdate.SetNewFieldValue("TypeNameFull", uielementEntityMaster.TypeNameFull);
                        entityToUpdate.SetNewFieldValue("SortOrder", uielementEntityMaster.SortOrder);
                        entityToUpdate.SetNewFieldValue("IsConfigurationItem", uielementEntityMaster.IsConfigurationItem);
                        entityToUpdate.SetNewFieldValue("FreeAccess", uielementEntityMaster.FreeAccess);
                        entityToUpdate.SetNewFieldValue("LicensingFree", uielementEntityMaster.LicensingFree);
                        entityToUpdate.SetNewFieldValue("UserRightsFree", uielementEntityMaster.UserRightsFree);
                        entityToUpdate.SetNewFieldValue("EntityName", uielementEntityMaster.EntityName);
                        entityToUpdate.SetNewFieldValue("IsConfigurationItem", uielementEntityMaster.IsConfigurationItem);
                        entityToUpdate.Save();

                        // Because if an entity hasn't changed it won't fire AfterSave or PropertyChanged we need to mark it 
                        // As updated to prevent it form being deleted by the refreshHelper
                        refreshHelper.MarkItemAsUpdated(entityToUpdate);
                    }
                }

                // Delete non updated items
                if (update)
                    refreshHelper.DeleteAllNonUpdatedItems();
            }


        }

        #endregion
    }
}