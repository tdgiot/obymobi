﻿using System;
using System.Collections.Generic;
using System.Text;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Dionysos.Data.LLBLGen;
using Dionysos;
using Obymobi.Data.Master.CollectionClasses;
using Obymobi.Data.Master.EntityClasses;
using Obymobi.Data.Master.HelperClasses;

namespace Obymobi.Data.Master
{
    public class Modules
    {
        #region Fields

        static Modules instance = null;

        #endregion

        #region Constructors

        /// <summary>
        /// Constructs an instance of the Modules class if the instance has not been initialized yet
        /// </summary>
        public Modules()
        {
            if (instance == null)
            {
                // first create the instance
                instance = new Modules();
                // Then init the instance (the init needs the instance to fill it)
                instance.Init();
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Initializes the static Modules instance
        /// </summary>
        private void Init()
        {
        }

        /// <summary>
        /// Refreshes the modules in the local database from the master database
        /// </summary>
        /// <param name="update">Flag which indicates whether existing modules should be updated</param>
        public static void RefreshModules(bool update)
        {
            // Preload ModuleCollection instance
            // and initialize the EntityView
            IEntityCollection moduleCollection = LLBLGenEntityCollectionUtil.GetEntityCollection("Module");
            if (Instance.Empty(moduleCollection))
            {
                throw new EmptyException("Could not initialize entity collection for entity name 'Module'.");
            }
            else
            {
                // Get the default entity view from the collection
                IEntityView moduleView = moduleCollection.DefaultView;

                // Get the referential constraints from the master database
                ModuleCollection moduleCollectionMaster = new ModuleCollection();
                moduleCollectionMaster.GetMulti(null);

				// Create a refresh helper for deleting non updated entities
				RefreshLocalHelper refreshHelper = null;
				if (update)
					refreshHelper = new RefreshLocalHelper(moduleCollection);

                PredicateExpression filter = null;

                // Walk through the referential constraints
                for (int i = 0; i < moduleCollectionMaster.Count; i++)
                {
                    ModuleEntity moduleEntityMaster = moduleCollectionMaster[i];

                    string moduleName = moduleEntityMaster.NameSystem;

                    filter = LLBLGenFilterUtil.GetFieldCompareValueEqualPredicateExpression("Module", ModuleFields.NameSystem.Name, moduleName);
                    moduleView.Filter = filter;

                    // check count to see if we need to add
                    if (moduleView.Count > 1)
                    {
                        throw new Dionysos.TechnicalException("Multiple Module records found for module name {0}.", moduleName);
                    }
                    else if (moduleView.Count == 0)
                    {
                        // Not found, add:
                        IEntity entityToInsert = DataFactory.EntityFactory.GetEntity("Module") as IEntity;
                        if (Instance.Empty(entityToInsert))
                        {
                            throw new EmptyException("Could not initialize entity of entity name 'Module'.");
                        }
                        else
                        {
                            // Set field values & save entity
                            entityToInsert.SetNewFieldValue("NameSystem", moduleName);
                            entityToInsert.SetNewFieldValue("NameFull", moduleEntityMaster.NameFull);
                            entityToInsert.SetNewFieldValue("NameShort", moduleEntityMaster.NameShort);
                            entityToInsert.SetNewFieldValue("IconPath", moduleEntityMaster.IconPath);
							entityToInsert.SetNewFieldValue("DefaultUiElementTypeNameFull", moduleEntityMaster.DefaultUiElementTypeNameFull);
                            entityToInsert.SetNewFieldValue("SortOrder", moduleEntityMaster.SortOrder);
                            entityToInsert.SetNewFieldValue("LicensingFree", moduleEntityMaster.LicensingFree);
                            entityToInsert.Save();

                            // Add to collection for viewing/searchgin
                            moduleView.RelatedCollection.Add(entityToInsert);
                        }
                    }
                    else if (moduleView.Count == 1 && update)
                    {
                        IEntity entityToUpdate = moduleView[0];
                        entityToUpdate.SetNewFieldValue("NameFull", moduleEntityMaster.NameFull);
                        entityToUpdate.SetNewFieldValue("NameShort", moduleEntityMaster.NameShort);
                        entityToUpdate.SetNewFieldValue("IconPath", moduleEntityMaster.IconPath);
						entityToUpdate.SetNewFieldValue("DefaultUiElementTypeNameFull", moduleEntityMaster.DefaultUiElementTypeNameFull);
                        entityToUpdate.SetNewFieldValue("SortOrder", moduleEntityMaster.SortOrder);
                        entityToUpdate.SetNewFieldValue("LicensingFree", moduleEntityMaster.LicensingFree);
                        entityToUpdate.Save();

						// Because if an entity hasn't changed it won't fire AfterSave or PropertyChanged we need to mark it 
						// As updated to prevent it form being deleted by the refreshHelper
						refreshHelper.MarkItemAsUpdated(entityToUpdate);
                    }
                }

				// Delete non updated items
				if (update)
					refreshHelper.DeleteAllNonUpdatedItems();
            }
        }

        #endregion
    }
}