﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
//////////////////////////////////////////////////////////////
using System;
using System.Linq;
using System.Collections.Generic;
using SD.LLBLGen.Pro.LinqSupportClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

using Obymobi.Data.Master;
using Obymobi.Data.Master.DaoClasses;
using Obymobi.Data.Master.EntityClasses;
using Obymobi.Data.Master.FactoryClasses;
using Obymobi.Data.Master.HelperClasses;
using Obymobi.Data.Master.RelationClasses;

namespace Obymobi.Data.Master.Linq
{
	/// <summary>Meta-data class for the construction of Linq queries which are to be executed using LLBLGen Pro code.</summary>
	public partial class LinqMetaData : ILinqMetaData
	{
		#region Class Member Declarations
		private ITransaction _transactionToUse;
		private FunctionMappingStore _customFunctionMappings;
		private Context _contextToUse;
		#endregion
		
		/// <summary>CTor. Using this ctor will leave the transaction object to use empty. This is ok if you're not executing queries created with this
		/// meta data inside a transaction. If you're executing the queries created with this meta-data inside a transaction, either set the Transaction property
		/// on the IQueryable.Provider instance of the created LLBLGenProQuery object prior to execution or use the ctor which accepts a transaction object.</summary>
		public LinqMetaData() : this(null, null)
		{
		}
		
		/// <summary>CTor. If you're executing the queries created with this meta-data inside a transaction, pass a live ITransaction object to this ctor.</summary>
		/// <param name="transactionToUse">the transaction to use in queries created with this meta-data</param>
		/// <remarks> Be aware that the ITransaction object set via this property is kept alive by the LLBLGenProQuery objects created with this meta data
		/// till they go out of scope.</remarks>
		public LinqMetaData(ITransaction transactionToUse) : this(transactionToUse, null)
		{
		}
		
		/// <summary>CTor. If you're executing the queries created with this meta-data inside a transaction, pass a live ITransaction object to this ctor.</summary>
		/// <param name="transactionToUse">the transaction to use in queries created with this meta-data</param>
		/// <param name="customFunctionMappings">The custom function mappings to use. These take higher precedence than the ones in the DQE to use.</param>
		/// <remarks> Be aware that the ITransaction object set via this property is kept alive by the LLBLGenProQuery objects created with this meta data
		/// till they go out of scope.</remarks>
		public LinqMetaData(ITransaction transactionToUse, FunctionMappingStore customFunctionMappings)
		{
			_transactionToUse = transactionToUse;
			_customFunctionMappings = customFunctionMappings;
		}
		
		/// <summary>returns the datasource to use in a Linq query for the entity type specified</summary>
		/// <param name="typeOfEntity">the type of the entity to get the datasource for</param>
		/// <returns>the requested datasource</returns>
		public IDataSource GetQueryableForEntity(int typeOfEntity)
		{
			IDataSource toReturn = null;
			switch((Obymobi.Data.Master.EntityType)typeOfEntity)
			{
				case Obymobi.Data.Master.EntityType.CountryEntity:
					toReturn = this.Country;
					break;
				case Obymobi.Data.Master.EntityType.CurrencyEntity:
					toReturn = this.Currency;
					break;
				case Obymobi.Data.Master.EntityType.EntityFieldInformationEntity:
					toReturn = this.EntityFieldInformation;
					break;
				case Obymobi.Data.Master.EntityType.EntityInformationEntity:
					toReturn = this.EntityInformation;
					break;
				case Obymobi.Data.Master.EntityType.ModuleEntity:
					toReturn = this.Module;
					break;
				case Obymobi.Data.Master.EntityType.ReferentialConstraintEntity:
					toReturn = this.ReferentialConstraint;
					break;
				case Obymobi.Data.Master.EntityType.RoleEntity:
					toReturn = this.Role;
					break;
				case Obymobi.Data.Master.EntityType.RoleEntityRightsEntity:
					toReturn = this.RoleEntityRights;
					break;
				case Obymobi.Data.Master.EntityType.RoleModuleRightsEntity:
					toReturn = this.RoleModuleRights;
					break;
				case Obymobi.Data.Master.EntityType.RoleUIElementRightsEntity:
					toReturn = this.RoleUIElementRights;
					break;
				case Obymobi.Data.Master.EntityType.RoleUIElementSubPanelRightsEntity:
					toReturn = this.RoleUIElementSubPanelRights;
					break;
				case Obymobi.Data.Master.EntityType.TimeZoneEntity:
					toReturn = this.TimeZone;
					break;
				case Obymobi.Data.Master.EntityType.TranslationEntity:
					toReturn = this.Translation;
					break;
				case Obymobi.Data.Master.EntityType.UIColorEntity:
					toReturn = this.UIColor;
					break;
				case Obymobi.Data.Master.EntityType.UIElementEntity:
					toReturn = this.UIElement;
					break;
				case Obymobi.Data.Master.EntityType.UIElementSubPanelEntity:
					toReturn = this.UIElementSubPanel;
					break;
				case Obymobi.Data.Master.EntityType.UIElementSubPanelUIElementEntity:
					toReturn = this.UIElementSubPanelUIElement;
					break;
				case Obymobi.Data.Master.EntityType.ViewEntity:
					toReturn = this.View;
					break;
				case Obymobi.Data.Master.EntityType.ViewItemEntity:
					toReturn = this.ViewItem;
					break;
				default:
					toReturn = null;
					break;
			}
			return toReturn;
		}

		/// <summary>returns the datasource to use in a Linq query for the entity type specified</summary>
		/// <typeparam name="TEntity">the type of the entity to get the datasource for</typeparam>
		/// <returns>the requested datasource</returns>
		public DataSource<TEntity> GetQueryableForEntity<TEntity>()
			    where TEntity : class
		{
    		return new DataSource<TEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse);
		}

		/// <summary>returns the datasource to use in a Linq query when targeting CountryEntity instances in the database.</summary>
		public DataSource<CountryEntity> Country
		{
			get { return new DataSource<CountryEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting CurrencyEntity instances in the database.</summary>
		public DataSource<CurrencyEntity> Currency
		{
			get { return new DataSource<CurrencyEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting EntityFieldInformationEntity instances in the database.</summary>
		public DataSource<EntityFieldInformationEntity> EntityFieldInformation
		{
			get { return new DataSource<EntityFieldInformationEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting EntityInformationEntity instances in the database.</summary>
		public DataSource<EntityInformationEntity> EntityInformation
		{
			get { return new DataSource<EntityInformationEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting ModuleEntity instances in the database.</summary>
		public DataSource<ModuleEntity> Module
		{
			get { return new DataSource<ModuleEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting ReferentialConstraintEntity instances in the database.</summary>
		public DataSource<ReferentialConstraintEntity> ReferentialConstraint
		{
			get { return new DataSource<ReferentialConstraintEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting RoleEntity instances in the database.</summary>
		public DataSource<RoleEntity> Role
		{
			get { return new DataSource<RoleEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting RoleEntityRightsEntity instances in the database.</summary>
		public DataSource<RoleEntityRightsEntity> RoleEntityRights
		{
			get { return new DataSource<RoleEntityRightsEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting RoleModuleRightsEntity instances in the database.</summary>
		public DataSource<RoleModuleRightsEntity> RoleModuleRights
		{
			get { return new DataSource<RoleModuleRightsEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting RoleUIElementRightsEntity instances in the database.</summary>
		public DataSource<RoleUIElementRightsEntity> RoleUIElementRights
		{
			get { return new DataSource<RoleUIElementRightsEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting RoleUIElementSubPanelRightsEntity instances in the database.</summary>
		public DataSource<RoleUIElementSubPanelRightsEntity> RoleUIElementSubPanelRights
		{
			get { return new DataSource<RoleUIElementSubPanelRightsEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting TimeZoneEntity instances in the database.</summary>
		public DataSource<TimeZoneEntity> TimeZone
		{
			get { return new DataSource<TimeZoneEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting TranslationEntity instances in the database.</summary>
		public DataSource<TranslationEntity> Translation
		{
			get { return new DataSource<TranslationEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting UIColorEntity instances in the database.</summary>
		public DataSource<UIColorEntity> UIColor
		{
			get { return new DataSource<UIColorEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting UIElementEntity instances in the database.</summary>
		public DataSource<UIElementEntity> UIElement
		{
			get { return new DataSource<UIElementEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting UIElementSubPanelEntity instances in the database.</summary>
		public DataSource<UIElementSubPanelEntity> UIElementSubPanel
		{
			get { return new DataSource<UIElementSubPanelEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting UIElementSubPanelUIElementEntity instances in the database.</summary>
		public DataSource<UIElementSubPanelUIElementEntity> UIElementSubPanelUIElement
		{
			get { return new DataSource<UIElementSubPanelUIElementEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting ViewEntity instances in the database.</summary>
		public DataSource<ViewEntity> View
		{
			get { return new DataSource<ViewEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting ViewItemEntity instances in the database.</summary>
		public DataSource<ViewItemEntity> ViewItem
		{
			get { return new DataSource<ViewItemEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
 
		#region Class Property Declarations
		/// <summary> Gets / sets the ITransaction to use for the queries created with this meta data object.</summary>
		/// <remarks> Be aware that the ITransaction object set via this property is kept alive by the LLBLGenProQuery objects created with this meta data
		/// till they go out of scope.</remarks>
		public ITransaction TransactionToUse
		{
			get { return _transactionToUse;}
			set { _transactionToUse = value;}
		}

		/// <summary>Gets or sets the custom function mappings to use. These take higher precedence than the ones in the DQE to use</summary>
		public FunctionMappingStore CustomFunctionMappings
		{
			get { return _customFunctionMappings; }
			set { _customFunctionMappings = value; }
		}
		
		/// <summary>Gets or sets the Context instance to use for entity fetches.</summary>
		public Context ContextToUse
		{
			get { return _contextToUse;}
			set { _contextToUse = value;}
		}
		#endregion
	}
}