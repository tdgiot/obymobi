﻿using System;
using System.Collections.Generic;
using System.Text;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Dionysos.Data.LLBLGen;
using Obymobi.Data.Master.CollectionClasses;
using Obymobi.Data.Master.EntityClasses;
using Dionysos;
using System.IO;
using Obymobi.Data.Master.HelperClasses;
using System.Web.UI;
using System.Web;
using Dionysos.Interfaces.Presentation;

namespace Obymobi.Data.Master
{
    public class UIElementSubPanels
    {
        #region Fields

        static UIElementSubPanels instance = null;

        #endregion

        #region Constructors

        /// <summary>
        /// Constructs an instance of the UIElementSubPanels class if the instance has not been initialized yet
        /// </summary>
        public UIElementSubPanels()
        {
            if (instance == null)
            {
                // first create the instance
                instance = new UIElementSubPanels();
                // Then init the instance (the init needs the instance to fill it)
                instance.Init();
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Initializes the static UIElements instance
        /// </summary>
        private void Init()
        {
        }

        /// <summary>
        /// Creates UI Element SubPanels
        /// </summary>
        public static void CreateUIElementSubPanels(string baseDirectory)
        {
            UIElementSubPanelCollection uiElementSubPanelCollection = new UIElementSubPanelCollection();
            uiElementSubPanelCollection.GetMulti(null);

            // Get the default entity view from the collection
            EntityView<UIElementSubPanelEntity> uiElementSubPanelView = uiElementSubPanelCollection.DefaultView;

            string[] directories = Directory.GetDirectories(baseDirectory);
            for (int i = 0; i < directories.Length; i++)
            {
                string directory = directories[i];
                if (!directory.ToLower().Contains("cutesoft_client"))
                {
                    // Get the aspx files from the application directory and all subdirectories
                    string[] ascxFiles = Directory.GetFiles(directory, "*.ascx", SearchOption.AllDirectories);

                    for (int j = 0; j < ascxFiles.Length; j++)
                    {
                        // Get an path to a file
                        string path = ascxFiles[j];
                        string relativeUrl = path.Replace(baseDirectory, "~").Replace(@"\", "/");

                        Control c = (HttpContext.Current.Handler as Page).LoadControl(relativeUrl);
                        if (c is IUielement)
                        {
                            // Create a filter for the ui element
                            // and retrieve the ui elements
                            PredicateExpression uiElementSubPanelFilter = new PredicateExpression(UIElementSubPanelFields.Url == relativeUrl);
                            uiElementSubPanelView.Filter = uiElementSubPanelFilter;

                            if (uiElementSubPanelView.Count == 0)
                            {
                                // No view found for the specified EntityInformation item
                                // A default view needs to be created
                                UIElementSubPanelEntity uiElementSubPanelEntity = new UIElementSubPanelEntity();

                                uiElementSubPanelEntity.NameSystem = "TODO: NAMESYSTEM";
                                uiElementSubPanelEntity.NameFull = "TODO: NAMEFULL";
                                uiElementSubPanelEntity.NameShort = "TODO: NAMESHORT";
                                uiElementSubPanelEntity.Url = relativeUrl;
                                uiElementSubPanelEntity.TypeNameFull = "Obymobi.ObymobiCms" + relativeUrl.Replace("~", "").Replace("/", ".").Replace(".ascx", "");
                                uiElementSubPanelEntity.EntityName = string.Empty;

                                try
                                {
                                    uiElementSubPanelEntity.Save();
                                }
                                catch
                                {
                                }

                                // Add to collection for viewing/searchgin
                                uiElementSubPanelView.RelatedCollection.Add(uiElementSubPanelEntity);
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Refreshes the ui element subpanels in the local database from the master database
        /// </summary>
        /// <param name="update">Flag which indicates whether existing ui element subpanels should be updated</param>
        public static void RefreshUIElementSubPanels(bool update)
        {
            // Preload UIElementSubPanelCollection instance
            // and initialize the EntityView			
            IEntityCollection uielementSubPanelCollection = LLBLGenEntityCollectionUtil.GetEntityCollection("UIElementSubPanel");
            if (Instance.Empty(uielementSubPanelCollection))
            {
                throw new EmptyException("Could not initialize entity collection for entity name 'UIElementSubPanel'.");
            }
            else
            {
                // Get the default entity view from the collection
                IEntityView uielementSubPanelView = uielementSubPanelCollection.DefaultView;

                // Get the ui element subpanels from the master database
                UIElementSubPanelCollection uielementSubPanelCollectionMaster = new UIElementSubPanelCollection();
                uielementSubPanelCollectionMaster.GetMulti(null);

                // Create a refresh helper for deleting non updated entities
                RefreshLocalHelper refreshHelper = null;
                if (update)
                    refreshHelper = new RefreshLocalHelper(uielementSubPanelCollection);

                PredicateExpression filter = null;

                // Walk through the referential constraints
                for (int i = 0; i < uielementSubPanelCollectionMaster.Count; i++)
                {
                    UIElementSubPanelEntity uielementSubPanelEntityMaster = uielementSubPanelCollectionMaster[i];

                    string uielementSubPanelTypeNameFull = uielementSubPanelEntityMaster.TypeNameFull;

                    filter = LLBLGenFilterUtil.GetFieldCompareValueEqualPredicateExpression("UIElementSubPanel", "TypeNameFull", uielementSubPanelTypeNameFull);
                    uielementSubPanelView.Filter = filter;

                    // check count to see if we need to add
                    if (uielementSubPanelView.Count > 1)
                    {
                        throw new Dionysos.TechnicalException("Multiple UIElementSubPanel records found for uielement {0}.", uielementSubPanelTypeNameFull);
                    }
                    else if (uielementSubPanelView.Count == 0)
                    {
                        // Not found, add:
                        IEntity entityToInsert = DataFactory.EntityFactory.GetEntity("UIElementSubPanel") as IEntity;
                        if (Instance.Empty(entityToInsert))
                        {
                            throw new EmptyException("Could not initialize entity of entity name 'UIElementSubPanel'.");
                        }
                        else
                        {
                            // Set field values & save entity
                            entityToInsert.SetNewFieldValue("NameSystem", uielementSubPanelEntityMaster.NameSystem);
                            entityToInsert.SetNewFieldValue("NameFull", uielementSubPanelEntityMaster.NameFull);
                            entityToInsert.SetNewFieldValue("NameShort", uielementSubPanelEntityMaster.NameShort);
                            entityToInsert.SetNewFieldValue("Url", uielementSubPanelEntityMaster.Url);
                            entityToInsert.SetNewFieldValue("TypeNameFull", uielementSubPanelEntityMaster.TypeNameFull);
                            entityToInsert.SetNewFieldValue("SortOrder", uielementSubPanelEntityMaster.SortOrder);
                            entityToInsert.SetNewFieldValue("FreeAccess", uielementSubPanelEntityMaster.FreeAccess);
                            entityToInsert.SetNewFieldValue("LicensingFree", uielementSubPanelEntityMaster.LicensingFree);
                            entityToInsert.SetNewFieldValue("EntityName", uielementSubPanelEntityMaster.EntityName);
                            entityToInsert.SetNewFieldValue("OnTabPage", uielementSubPanelEntityMaster.OnTabPage);
                            entityToInsert.Save();

                            // Add to collection for viewing/searchgin
                            uielementSubPanelView.RelatedCollection.Add(entityToInsert);
                        }
                    }
                    else if (uielementSubPanelView.Count == 1 && update)
                    {
                        IEntity entityToUpdate = uielementSubPanelView[0];
                        entityToUpdate.SetNewFieldValue("NameSystem", uielementSubPanelEntityMaster.NameFull);
                        entityToUpdate.SetNewFieldValue("NameFull", uielementSubPanelEntityMaster.NameFull);
                        entityToUpdate.SetNewFieldValue("NameShort", uielementSubPanelEntityMaster.NameShort);
                        entityToUpdate.SetNewFieldValue("Url", uielementSubPanelEntityMaster.Url);
                        entityToUpdate.SetNewFieldValue("TypeNameFull", uielementSubPanelEntityMaster.TypeNameFull);
                        entityToUpdate.SetNewFieldValue("SortOrder", uielementSubPanelEntityMaster.SortOrder);
                        entityToUpdate.SetNewFieldValue("FreeAccess", uielementSubPanelEntityMaster.FreeAccess);
                        entityToUpdate.SetNewFieldValue("LicensingFree", uielementSubPanelEntityMaster.LicensingFree);
                        entityToUpdate.SetNewFieldValue("EntityName", uielementSubPanelEntityMaster.EntityName);
                        entityToUpdate.SetNewFieldValue("OnTabPage", uielementSubPanelEntityMaster.OnTabPage);
                        entityToUpdate.Save();

                        // Because if an entity hasn't changed it won't fire AfterSave or PropertyChanged we need to mark it 
                        // As updated to prevent it form being deleted by the refreshHelper
                        refreshHelper.MarkItemAsUpdated(entityToUpdate);
                    }
                }

                // Delete non updated items
                if (update)
                    refreshHelper.DeleteAllNonUpdatedItems();
            }
        }

        /// <summary>
        /// Refreshes the ui element subpanel ui elements in the local database from the master database
        /// </summary>
        /// <param name="update">Flag which indicates whether existing ui element subpanel ui elements should be updated</param>
        public static void RefreshUIElementSubPanelUIElements(bool update)
        {
            // Preload UIElementSubPanelUIElementCollection instance
            // and initialize the EntityView			
            IEntityCollection uielementSubPanelUIElementCollection = LLBLGenEntityCollectionUtil.GetEntityCollection("UIElementSubPanelUIElement");
            if (Instance.Empty(uielementSubPanelUIElementCollection))
            {
                throw new EmptyException("Could not initialize entity collection for entity name 'UIElementSubPanelUIElement'.");
            }
            else
            {
                // Get the default entity view from the collection
                IEntityView uielementSubPanelUIElementView = uielementSubPanelUIElementCollection.DefaultView;

                // Get the ui element subpanels from the master database
                UIElementSubPanelUIElementCollection uielementSubPanelUIElementCollectionMaster = new UIElementSubPanelUIElementCollection();
                uielementSubPanelUIElementCollectionMaster.GetMulti(null);

                // Create a refresh helper for deleting non updated entities
                RefreshLocalHelper refreshHelper = null;
                if (update)
                    refreshHelper = new RefreshLocalHelper(uielementSubPanelUIElementCollection);

                PredicateExpression filter = null;

                // Walk through the referential constraints
                for (int i = 0; i < uielementSubPanelUIElementCollectionMaster.Count; i++)
                {
                    UIElementSubPanelUIElementEntity uielementSubPanelUIElementEntityMaster = uielementSubPanelUIElementCollectionMaster[i];

                    string uielementSubPanelTypeNameFull = uielementSubPanelUIElementEntityMaster.UIElementSubPanelTypeNameFull;

                    filter = new PredicateExpression();
                    filter.Add(LLBLGenFilterUtil.GetFieldCompareValueEqualPredicate("UIElementSubPanelUIElement", "UIElementSubPanelTypeNameFull", uielementSubPanelTypeNameFull));
                    if (uielementSubPanelUIElementEntityMaster.ParentUIElementTypeNameFull.Length > 0)
                        filter.Add(LLBLGenFilterUtil.GetFieldCompareValueEqualPredicate("UIElementSubPanelUIElement", "ParentUIElementTypeNameFull", uielementSubPanelUIElementEntityMaster.ParentUIElementTypeNameFull));
                    else if (uielementSubPanelUIElementEntityMaster.ParentUIElementSubPanelTypeNameFull.Length > 0)
                        filter.Add(LLBLGenFilterUtil.GetFieldCompareValueEqualPredicate("UIElementSubPanelUIElement", "ParentUIElementSubPanelTypeNameFull", uielementSubPanelUIElementEntityMaster.ParentUIElementSubPanelTypeNameFull));
                    else
                        throw new TechnicalException("No parent ui element for ui element subpanel '{0}'.", uielementSubPanelTypeNameFull);

                    uielementSubPanelUIElementView.Filter = filter;

                    // check count to see if we need to add
                    if (uielementSubPanelUIElementView.Count > 1)
                    {
                        throw new Dionysos.TechnicalException("Multiple UIElementSubPanel records found for ui element subpanel '{0}'.", uielementSubPanelTypeNameFull);
                    }
                    else if (uielementSubPanelUIElementView.Count == 0)
                    {
                        // Not found, add:
                        IEntity entityToInsert = DataFactory.EntityFactory.GetEntity("UIElementSubPanelUIElement") as IEntity;
                        if (Instance.Empty(entityToInsert))
                        {
                            throw new EmptyException("Could not initialize entity of entity name 'UIElementSubPanelUIElement'.");
                        }
                        else
                        {
                            // Set field values & save entity
                            entityToInsert.SetNewFieldValue("UIElementSubPanelTypeNameFull", uielementSubPanelTypeNameFull);
                            if (uielementSubPanelUIElementEntityMaster.ParentUIElementTypeNameFull.Length > 0)
                                entityToInsert.SetNewFieldValue("ParentUIElementTypeNameFull", uielementSubPanelUIElementEntityMaster.ParentUIElementTypeNameFull);
                            else if (uielementSubPanelUIElementEntityMaster.ParentUIElementSubPanelTypeNameFull.Length > 0)
                                entityToInsert.SetNewFieldValue("ParentUIElementSubPanelTypeNameFull", uielementSubPanelUIElementEntityMaster.ParentUIElementSubPanelTypeNameFull);
                            entityToInsert.Save();

                            // Add to collection for viewing/searchgin
                            uielementSubPanelUIElementView.RelatedCollection.Add(entityToInsert);
                        }
                    }
                    else if (uielementSubPanelUIElementView.Count == 1 && update)
                    {
                        IEntity entityToUpdate = uielementSubPanelUIElementView[0];
                        if (uielementSubPanelUIElementEntityMaster.ParentUIElementTypeNameFull.Length > 0)
                            entityToUpdate.SetNewFieldValue("ParentUIElementTypeNameFull", uielementSubPanelUIElementEntityMaster.ParentUIElementTypeNameFull);
                        else if (uielementSubPanelUIElementEntityMaster.ParentUIElementSubPanelTypeNameFull.Length > 0)
                            entityToUpdate.SetNewFieldValue("ParentUIElementSubPanelTypeNameFull", uielementSubPanelUIElementEntityMaster.ParentUIElementSubPanelTypeNameFull);
                        entityToUpdate.Save();

                        // Because if an entity hasn't changed it won't fire AfterSave or PropertyChanged we need to mark it 
                        // As updated to prevent it form being deleted by the refreshHelper
                        refreshHelper.MarkItemAsUpdated(entityToUpdate);
                    }
                }

                // Delete non updated items
                if (update)
                    refreshHelper.DeleteAllNonUpdatedItems();
            }
        }

        #endregion
    }
}