﻿using System;
using System.Collections.Generic;
using System.Text;
using Obymobi.Data.Master.EntityClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Dionysos;
using Obymobi.Data.Master.CollectionClasses;
using Obymobi.Data.Master.HelperClasses;
using Dionysos.Data.LLBLGen;

namespace Obymobi.Data.Master
{
    public class ReferentialConstraints
    {
        #region Fields

        static ReferentialConstraints instance = null;

        #endregion

        #region Constructors

        /// <summary>
        /// Constructs an instance of the ReferentialConstraints class if the instance has not been initialized yet
        /// </summary>
        public ReferentialConstraints()
        {
            if (instance == null)
            {
                // first create the instance
                instance = new ReferentialConstraints();
                // Then init the instance (the init needs the instance to fill it)
                instance.Init();
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Initializes the static ReferentialConstraints instance
        /// </summary>
        private void Init()
        {
        }

        /// <summary>
        /// Checks if the referential constraints are available
        /// </summary>
        public static void CheckReferentialConstraints()
        {
            // Get the entity names
            string[] entityNames = LLBLGenUtil.GetEntityNames();

            // Walk through the entity names
            for (int i = 0; i < entityNames.Length; i++)
            {
                string entityName = LLBLGenUtil.GetEntityName(entityNames[i]);

                // Get an entity to get access to its fields
                // TODO: this solution is not very elegant
                IEntity entity = DataFactory.EntityFactory.GetEntity(entityName) as IEntity;
                if (Instance.Empty(entity))
                {
                    throw new EmptyException("Variable 'entity' is empty.");
                }
                else
                {
                    // Get the field names for the current entity
                    string[] fields = DataFactory.EntityUtil.GetFields(entityName);

                    // Walk through the field names
                    for (int j = 0; j < fields.Length; j++)
                    {
                        string fieldName = fields[j];
                        if (fieldName.Contains("_"))
                        {
                            // Field names which contain an "_" are 
                            // inherited fieldnames using entity inheritance
                        }
                        else
                        {
                            // Get the corresponding IEntityField instance from the IEntity instance
                            IEntityField entityField = entity.Fields[fieldName];
                            if (Instance.Empty(entityField))
                            {
                                throw new EmptyException("Variable 'entityField' is empty.");
                            }
                            else
                            {
                                // Logic for referential constrains
                                if (entityField.IsForeignKey)
                                {
                                    // Found a foreign key
                                    // Now check if it already exists in the database
                                    ReferentialConstraintCollection entityCollection = new ReferentialConstraintCollection();

                                    PredicateExpression filter = new PredicateExpression();
                                    filter.Add(ReferentialConstraintFields.EntityName == entityName);
                                    filter.Add(ReferentialConstraintFields.ForeignKeyName == fieldName);

                                    entityCollection.GetMulti(filter);

                                    if (entityCollection.Count == 0)
                                    {
                                        ReferentialConstraintEntity entityToInsert = new ReferentialConstraintEntity();
                                        entityToInsert.EntityName = entityName;
                                        entityToInsert.ForeignKeyName = fieldName;
                                        entityToInsert.Save();
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Refreshes the referential constaints in the local database
        /// </summary>
        public static void RefreshReferentialConstraints(bool update)
        {
            // Preload EntityInformationCollection instance
            // and initialize the EntityView
            IEntityCollection referentialConstraintCollection = LLBLGenEntityCollectionUtil.GetEntityCollection("ReferentialConstraint");
            if (Instance.Empty(referentialConstraintCollection))
            {
                throw new EmptyException("Could not initialize entity collection for entity name 'ReferentialConstraint'.");
            }
            else
            {
                // Get the default entity view from the collection
                IEntityView referentialConstraintView = referentialConstraintCollection.DefaultView;

                // Get the referential constraints from the master database
                ReferentialConstraintCollection referentialConstaintCollectionMaster = new ReferentialConstraintCollection();
                referentialConstaintCollectionMaster.GetMulti(null);

				// Create a refresh helper for deleting non updated entities
				RefreshLocalHelper refreshHelper = null;
				if (update)
					refreshHelper = new RefreshLocalHelper(referentialConstraintCollection);

                PredicateExpression filter = null;

                // Walk through the referential constraints
                for (int i = 0; i < referentialConstaintCollectionMaster.Count; i++)
                {
                    ReferentialConstraintEntity referentialConstraintEntityMaster = referentialConstaintCollectionMaster[i];

                    string entityName = referentialConstraintEntityMaster.EntityName;
                    string foreignKeyName = referentialConstraintEntityMaster.ForeignKeyName;

                    filter = LLBLGenFilterUtil.GetFieldCompareValueEqualPredicateExpression("ReferentialConstraint", new string[] { "EntityName", "ForeignKeyName" }, new object[] { entityName, foreignKeyName });
                    referentialConstraintView.Filter = filter;

                    // check count to see if we need to add
                    if (referentialConstraintView.Count > 1)
                    {
                        throw new Dionysos.TechnicalException("Multiple ReferentialConstraint records found for entity name {0} and foreign key name {1}", entityName, foreignKeyName);
                    }
                    else if (referentialConstraintView.Count == 0)
                    {
                        // Not found, add:
                        IEntity entityToInsert = DataFactory.EntityFactory.GetEntity("ReferentialConstraint") as IEntity;
                        if (Instance.Empty(entityToInsert))
                        {
                            throw new EmptyException("Could not initialize entity of entity name 'ReferentialConstraint'.");
                        }
                        else
                        {
                            // Set field values & save entity
                            entityToInsert.SetNewFieldValue("EntityName", entityName);
                            entityToInsert.SetNewFieldValue("ForeignKeyName", foreignKeyName);
                            entityToInsert.SetNewFieldValue("DeleteRule", referentialConstraintEntityMaster.DeleteRule);
                            entityToInsert.SetNewFieldValue("UpdateRule", referentialConstraintEntityMaster.UpdateRule);
							entityToInsert.SetNewFieldValue("ArchiveRule", referentialConstraintEntityMaster.ArchiveRule);
                            entityToInsert.Save();

                            // Add to collection for viewing/searchgin
                            referentialConstraintView.RelatedCollection.Add(entityToInsert);
                        }
                    }
                    else if (referentialConstraintView.Count == 1 && update)
                    {
                        IEntity entityToUpdate = referentialConstraintView[0];
                        entityToUpdate.SetNewFieldValue("DeleteRule", referentialConstraintEntityMaster.DeleteRule);
                        entityToUpdate.SetNewFieldValue("UpdateRule", referentialConstraintEntityMaster.UpdateRule);
						entityToUpdate.SetNewFieldValue("ArchiveRule", referentialConstraintEntityMaster.ArchiveRule);
                        entityToUpdate.Save();

						// Because if an entity hasn't changed it won't fire AfterSave or PropertyChanged we need to mark it 
						// As updated to prevent it form being deleted by the refreshHelper
						refreshHelper.MarkItemAsUpdated(entityToUpdate);
                    }
                }

				// Delete non updated items
				if (update)
					refreshHelper.DeleteAllNonUpdatedItems();
            }
        }

        #endregion
    }
}