﻿using System;
using System.Collections.Generic;
using System.Text;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Dionysos.Data.LLBLGen;
using Obymobi.Data.Master.CollectionClasses;
using Obymobi.Data.Master.EntityClasses;
using Dionysos;
using System.IO;
using Obymobi.Data.Master.HelperClasses;

namespace Obymobi.Data.Master
{
    public class RoleUIElementRights
    {
        #region Fields

        static RoleUIElementRights instance = null;

        #endregion

        #region Constructors

        /// <summary>
        /// Constructs an instance of the RoleUIElementRights class if the instance has not been initialized yet
        /// </summary>
        public RoleUIElementRights()
        {
            if (instance == null)
            {
                // first create the instance
                instance = new RoleUIElementRights();
                // Then init the instance (the init needs the instance to fill it)
                instance.Init();
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Initializes the static UIElements instance
        /// </summary>
        private void Init()
        {
        }

        /// <summary>
        /// Creates Role UI Element rights
        /// </summary>
        public static void CreateRoleUIElementRights()
        {
            // Get the non user rights free UI elements
            PredicateExpression uiElementFilter = new PredicateExpression(UIElementFields.UserRightsFree == false);

            UIElementCollection uiElementCollection = new UIElementCollection();
            uiElementCollection.GetMulti(uiElementFilter);

            // Get the non administrator roles
            PredicateExpression roleFilter = new PredicateExpression(RoleFields.IsAdministrator == false);

            RoleCollection roleCollection = new RoleCollection();
            roleCollection.GetMulti(roleFilter);

            // Get the current rights 
            RoleUIElementRightsCollection roleUiElementRightsCollection = new RoleUIElementRightsCollection();
            roleUiElementRightsCollection.GetMulti(null);

            EntityView<RoleUIElementRightsEntity> roleUiElementRightsView = roleUiElementRightsCollection.DefaultView;

            PredicateExpression roleUiElementRightsFilter = null;

            foreach (RoleEntity roleEntity in roleCollection)
            {
                foreach (UIElementEntity uiElementEntity in uiElementCollection)
                {
                    roleUiElementRightsFilter = new PredicateExpression();
                    roleUiElementRightsFilter.Add(RoleUIElementRightsFields.RoleId == roleEntity.RoleId);
                    roleUiElementRightsFilter.Add(RoleUIElementRightsFields.UiElementTypeNameFull == uiElementEntity.TypeNameFull);

                    roleUiElementRightsView.Filter = roleUiElementRightsFilter;

                    if (roleUiElementRightsView.Count == 0)
                    {
                        // RoleUIElementRightsEntity did not exist yet, add
                        RoleUIElementRightsEntity roleUiElementRightsEntity = new RoleUIElementRightsEntity();
                        roleUiElementRightsEntity.RoleId = roleEntity.RoleId;
                        roleUiElementRightsEntity.UiElementTypeNameFull = uiElementEntity.TypeNameFull;
                        roleUiElementRightsEntity.IsAllowed = false;
                        if (roleUiElementRightsEntity.Save())
                        {
                            roleUiElementRightsView.RelatedCollection.Add(roleUiElementRightsEntity);
                        }
                    }
                }
            }            
        }

        /// <summary>
        /// Refreshes the role ui element rights in the local database from the master database
        /// </summary>
        /// <param name="update">Flag which indicates whether existing role ui element rights should be updated</param>
        public static void RefreshRoleUIElementRights(bool update)
        {
            // Preload RoleUIElementRightCollection instance
            // and initialize the EntityView			
            IEntityCollection roleUiElementRightsCollection = LLBLGenEntityCollectionUtil.GetEntityCollection("RoleUIElementRights");
            IEntityCollection roleCollection = LLBLGenEntityCollectionUtil.GetEntityCollection("Role");
            if (Instance.Empty(roleUiElementRightsCollection))
            {
                throw new EmptyException("Could not initialize entity collection for entity name 'RoleUIElementRights'.");
            }
            else if (Instance.Empty(roleCollection))
            {
                throw new EmptyException("Could not initialize entity collection for entity name 'Role'.");
            }
            else
            {
                // Get the default entity views from the collections
                IEntityView roleUiElementRightView = roleUiElementRightsCollection.DefaultView;
                IEntityView roleView = roleCollection.DefaultView;

                // Get the role UI element right from the master database
                RoleUIElementRightsCollection roleUiElementRightCollectionMaster = new RoleUIElementRightsCollection();
                roleUiElementRightCollectionMaster.GetMulti(null);

                PredicateExpression roleUiElementRightsFilter = null;
                PredicateExpression roleFilter = null;

                // Walk through the rights
                for (int i = 0; i < roleUiElementRightCollectionMaster.Count; i++)
                {
                    RoleUIElementRightsEntity roleUiEntityRightMaster = roleUiElementRightCollectionMaster[i];

                    roleFilter = LLBLGenFilterUtil.GetFieldCompareValueEqualPredicateExpression("Role", "Name", roleUiEntityRightMaster.Role.Name);
                    roleView.Filter = roleFilter;

                    if (roleView.Count == 1)
                    {
                        string uielementTypeNameFull = roleUiEntityRightMaster.UiElementTypeNameFull;
                        object roleId = roleView[0].Fields["RoleId"].CurrentValue;

                        roleUiElementRightsFilter = LLBLGenFilterUtil.GetFieldCompareValueEqualPredicateExpression("RoleUIElementRights", new string[] { "RoleId", "UiElementTypeNameFull" }, new object[] { roleId, uielementTypeNameFull });
                        roleUiElementRightView.Filter = roleUiElementRightsFilter;

                        // check count to see if we need to add
                        if (roleUiElementRightView.Count > 1)
                        {
                            throw new Dionysos.TechnicalException("Multiple RoleUIElementRight records found for role {0} and UI element type name {1}.", roleId, uielementTypeNameFull);
                        }
                        else if (roleUiElementRightView.Count == 0)
                        {
                            // Not found, add:
                            IEntity entityToInsert = DataFactory.EntityFactory.GetEntity("RoleUIElementRights") as IEntity;
                            if (Instance.Empty(entityToInsert))
                            {
                                throw new EmptyException("Could not initialize entity of entity name 'RoleUIElementRights'.");
                            }
                            else
                            {
                                // Set field values & save entity
                                entityToInsert.SetNewFieldValue("RoleId", roleId);
                                entityToInsert.SetNewFieldValue("UiElementTypeNameFull", roleUiEntityRightMaster.UiElementTypeNameFull);
                                entityToInsert.SetNewFieldValue("IsAllowed", roleUiEntityRightMaster.IsAllowed);
                                entityToInsert.SetNewFieldValue("IsReadOnly", roleUiEntityRightMaster.IsReadOnly);

                                entityToInsert.Save();

                                // Add to collection for viewing/searchgin
                                roleUiElementRightView.RelatedCollection.Add(entityToInsert);
                            }
                        }
                        else if (roleUiElementRightView.Count == 1 && update)
                        {
                            IEntity entityToUpdate = roleUiElementRightView[0];
                            entityToUpdate.SetNewFieldValue("IsAllowed", roleUiEntityRightMaster.IsAllowed);
                            entityToUpdate.SetNewFieldValue("IsReadOnly", roleUiEntityRightMaster.IsReadOnly);

                            entityToUpdate.Save();
                        }
                    }
                }
            }
        }

        #endregion
    }
}