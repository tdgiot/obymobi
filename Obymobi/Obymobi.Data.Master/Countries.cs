﻿using System;
using System.Collections;
using Dionysos;
using Dionysos.Data.LLBLGen;
using Obymobi.Data.Master.CollectionClasses;
using Obymobi.Data.Master.EntityClasses;
using Obymobi.Data.Master.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.Master
{
    public static class Countries
    {
        public static void Refresh()
        {
            RefreshCurrencies();
            RefreshCountries();
        }

        private static void RefreshCurrencies()
        {
            var currencyCollectionMaster = new CurrencyCollection();
            currencyCollectionMaster.GetMulti(null);

            IEntityCollection currencyCollectionLocal = LLBLGenEntityCollectionUtil.GetEntityCollection("Currency");
            var currencyViewLocal = currencyCollectionLocal.DefaultView;

            foreach (var currencyMaster in currencyCollectionMaster)
            {
                var filterLocal = new PredicateExpression();
                filterLocal.Add(LLBLGenFilterUtil.GetFieldCompareValueEqualPredicate("Currency", CurrencyFields.CodeIso4217.Name, currencyMaster.CodeIso4217));

                currencyViewLocal.Filter = filterLocal;

                IEntity currencyLocal = GetLocalCurrencyEntityByIsoCode(currencyMaster);

                if (currencyLocal == null)
                    continue;

                currencyLocal.SetNewFieldValue(CurrencyFields.Name.Name, currencyMaster.Name);
                currencyLocal.SetNewFieldValue(CurrencyFields.Symbol.Name, currencyMaster.Symbol);

                // If IsNew or  new value, update & save
                currencyLocal.Save();
            }
        }

        private static void RefreshCountries()
        {
            var prefetch = new PrefetchPath(EntityType.CountryEntity);
            prefetch.Add(CountryEntityBase.PrefetchPathCurrency);

            var relation = new RelationCollection(CountryEntityBase.Relations.CurrencyEntityUsingCurrencyId);

            var countryCollectionMaster = new CountryCollection();
            countryCollectionMaster.GetMulti(null, 0, null, relation, prefetch);

            IEntityCollection countryCollectionLocal = LLBLGenEntityCollectionUtil.GetEntityCollection("Country");
            var countryViewLocal = countryCollectionLocal.DefaultView;

            foreach (var countryMaster in countryCollectionMaster)
            {
                var filterLocal = new PredicateExpression();
                filterLocal.Add(LLBLGenFilterUtil.GetFieldCompareValueEqualPredicate("Country", CountryFields.CodeAlpha3.Name, countryMaster.CodeAlpha3));

                countryViewLocal.Filter = filterLocal;

                IEntity countryLocal;
                if (countryViewLocal.Count == 0)
                {
                    countryLocal = DataFactory.EntityFactory.GetEntity("Country") as IEntity;
                    LLBLGenEntityUtil.CopyFields(countryMaster, countryLocal, new ArrayList());
                }
                else if (countryViewLocal.Count == 1)
                {
                    countryLocal = countryViewLocal[0];
                }
                else
                {
                    throw new FunctionalException("Multiple local countries found for code: '{0}'", countryMaster.CodeAlpha3);
                }

                if (countryLocal == null)
                    continue;

                // Fetch real currency Id for current country
                if (countryMaster.CurrencyId > 0)
                {
                    var localCurrencyEntity = GetLocalCurrencyEntityByIsoCode(countryMaster.Currency);
                    if (localCurrencyEntity != null)
                    {
                        if (!localCurrencyEntity.IsNew)
                        {
                            countryLocal.SetNewFieldValue(CountryFields.CurrencyId.Name, localCurrencyEntity.PrimaryKeyFields[0].CurrentValue);
                        }
                        else
                        {
                            throw new FunctionalException("Failed to find currency with code '{0}' for country '{1} ({2})'", countryMaster.Currency.CodeIso4217, countryMaster.CodeAlpha3, countryMaster.CountryId);
                        }
                    }
                    else
                    {
                        countryLocal.SetNewFieldValue(CountryFields.CurrencyId.Name, DBNull.Value);
                    }
                }

                countryLocal.SetNewFieldValue(CountryFields.Name.Name, countryMaster.Name);
                countryLocal.SetNewFieldValue(CountryFields.Code.Name, countryMaster.Code);
                countryLocal.SetNewFieldValue(CountryFields.CultureName.Name, countryMaster.CultureName);

                // If IsNew or  new value, update & save
                countryLocal.Save();
            }
        }

        private static IEntity GetLocalCurrencyEntityByIsoCode(CurrencyEntity currencyMaster)
        {
            IEntityCollection currencyCollectionLocal = LLBLGenEntityCollectionUtil.GetEntityCollection("Currency");
            var currencyViewLocal = currencyCollectionLocal.DefaultView;

            var filterLocal = new PredicateExpression();
            filterLocal.Add(LLBLGenFilterUtil.GetFieldCompareValueEqualPredicate("Currency", CurrencyFields.CodeIso4217.Name, currencyMaster.CodeIso4217));

            currencyViewLocal.Filter = filterLocal;

            IEntity currencyLocal;
            if (currencyViewLocal.Count == 0)
            {
                currencyLocal = DataFactory.EntityFactory.GetEntity("Currency") as IEntity;
                LLBLGenEntityUtil.CopyFields(currencyMaster, currencyLocal, new ArrayList());
            }
            else if (currencyViewLocal.Count == 1)
            {
                currencyLocal = currencyViewLocal[0];
            }
            else
            {
                throw new FunctionalException("Multiple local currencies found with ISO Code: '{0}'", currencyMaster.CodeIso4217);
            }

            return currencyLocal;
        }
    }
}
