﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
////////////////////////////////////////////////////////////// 
using System;
using System.Linq;
using Obymobi.Data.Master.EntityClasses;
using Obymobi.Data.Master.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using SD.LLBLGen.Pro.QuerySpec;

namespace Obymobi.Data.Master.FactoryClasses
{
	/// <summary>Factory class to produce DynamicQuery instances and EntityQuery instances</summary>
	public partial class QueryFactory
	{
		private int _aliasCounter = 0;

		/// <summary>Creates a new DynamicQuery instance with no alias set.</summary>
		/// <returns>Ready to use DynamicQuery instance</returns>
		public DynamicQuery Create()
		{
			return Create(string.Empty);
		}

		/// <summary>Creates a new DynamicQuery instance with the alias specified as the alias set.</summary>
		/// <param name="alias">The alias.</param>
		/// <returns>Ready to use DynamicQuery instance</returns>
		public DynamicQuery Create(string alias)
		{
			return new DynamicQuery(new ElementCreator(), alias, this.GetNextAliasCounterValue());
		}

		/// <summary>Creates a new DynamicQuery which wraps the specified TableValuedFunction call</summary>
		/// <param name="toWrap">The table valued function call to wrap.</param>
		/// <returns>toWrap wrapped in a DynamicQuery.</returns>
		public DynamicQuery Create(TableValuedFunctionCall toWrap)
		{
			return this.Create().From(new TvfCallWrapper(toWrap)).Select(toWrap.GetFieldsAsArray().Select(f => this.Field(toWrap.Alias, f.Alias)).ToArray());
		}

		/// <summary>Creates a new EntityQuery for the entity of the type specified with no alias set.</summary>
		/// <typeparam name="TEntity">The type of the entity to produce the query for.</typeparam>
		/// <returns>ready to use EntityQuery instance</returns>
		public EntityQuery<TEntity> Create<TEntity>()
			where TEntity : IEntityCore
		{
			return Create<TEntity>(string.Empty);
		}

		/// <summary>Creates a new EntityQuery for the entity of the type specified with the alias specified as the alias set.</summary>
		/// <typeparam name="TEntity">The type of the entity to produce the query for.</typeparam>
		/// <param name="alias">The alias.</param>
		/// <returns>ready to use EntityQuery instance</returns>
		public EntityQuery<TEntity> Create<TEntity>(string alias)
			where TEntity : IEntityCore
		{
			return new EntityQuery<TEntity>(new ElementCreator(), alias, this.GetNextAliasCounterValue());
		}
				
		/// <summary>Creates a new field object with the name specified and of resulttype 'object'. Used for referring to aliased fields in another projection.</summary>
		/// <param name="fieldName">Name of the field.</param>
		/// <returns>Ready to use field object</returns>
		public EntityField Field(string fieldName)
		{
			return Field<object>(string.Empty, fieldName);
		}

		/// <summary>Creates a new field object with the name specified and of resulttype 'object'. Used for referring to aliased fields in another projection.</summary>
		/// <param name="targetAlias">The alias of the table/query to target.</param>
		/// <param name="fieldName">Name of the field.</param>
		/// <returns>Ready to use field object</returns>
		public EntityField Field(string targetAlias, string fieldName)
		{
			return Field<object>(targetAlias, fieldName);
		}

		/// <summary>Creates a new field object with the name specified and of resulttype 'TValue'. Used for referring to aliased fields in another projection.</summary>
		/// <typeparam name="TValue">The type of the value represented by the field.</typeparam>
		/// <param name="fieldName">Name of the field.</param>
		/// <returns>Ready to use field object</returns>
		public EntityField Field<TValue>(string fieldName)
		{
			return Field<TValue>(string.Empty, fieldName);
		}

		/// <summary>Creates a new field object with the name specified and of resulttype 'TValue'. Used for referring to aliased fields in another projection.</summary>
		/// <typeparam name="TValue">The type of the value.</typeparam>
		/// <param name="targetAlias">The alias of the table/query to target.</param>
		/// <param name="fieldName">Name of the field.</param>
		/// <returns>Ready to use field object</returns>
		public EntityField Field<TValue>(string targetAlias, string fieldName)
		{
			return new EntityField(fieldName, targetAlias, typeof(TValue));
		}
						
		/// <summary>Gets the next alias counter value to produce artifical aliases with</summary>
		private int GetNextAliasCounterValue()
		{
			_aliasCounter++;
			return _aliasCounter;
		}
		

		/// <summary>Creates and returns a new EntityQuery for the Country entity</summary>
		public EntityQuery<CountryEntity> Country
		{
			get { return Create<CountryEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the Currency entity</summary>
		public EntityQuery<CurrencyEntity> Currency
		{
			get { return Create<CurrencyEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the EntityFieldInformation entity</summary>
		public EntityQuery<EntityFieldInformationEntity> EntityFieldInformation
		{
			get { return Create<EntityFieldInformationEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the EntityInformation entity</summary>
		public EntityQuery<EntityInformationEntity> EntityInformation
		{
			get { return Create<EntityInformationEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the Module entity</summary>
		public EntityQuery<ModuleEntity> Module
		{
			get { return Create<ModuleEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the ReferentialConstraint entity</summary>
		public EntityQuery<ReferentialConstraintEntity> ReferentialConstraint
		{
			get { return Create<ReferentialConstraintEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the Role entity</summary>
		public EntityQuery<RoleEntity> Role
		{
			get { return Create<RoleEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the RoleEntityRights entity</summary>
		public EntityQuery<RoleEntityRightsEntity> RoleEntityRights
		{
			get { return Create<RoleEntityRightsEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the RoleModuleRights entity</summary>
		public EntityQuery<RoleModuleRightsEntity> RoleModuleRights
		{
			get { return Create<RoleModuleRightsEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the RoleUIElementRights entity</summary>
		public EntityQuery<RoleUIElementRightsEntity> RoleUIElementRights
		{
			get { return Create<RoleUIElementRightsEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the RoleUIElementSubPanelRights entity</summary>
		public EntityQuery<RoleUIElementSubPanelRightsEntity> RoleUIElementSubPanelRights
		{
			get { return Create<RoleUIElementSubPanelRightsEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the TimeZone entity</summary>
		public EntityQuery<TimeZoneEntity> TimeZone
		{
			get { return Create<TimeZoneEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the Translation entity</summary>
		public EntityQuery<TranslationEntity> Translation
		{
			get { return Create<TranslationEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the UIColor entity</summary>
		public EntityQuery<UIColorEntity> UIColor
		{
			get { return Create<UIColorEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the UIElement entity</summary>
		public EntityQuery<UIElementEntity> UIElement
		{
			get { return Create<UIElementEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the UIElementSubPanel entity</summary>
		public EntityQuery<UIElementSubPanelEntity> UIElementSubPanel
		{
			get { return Create<UIElementSubPanelEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the UIElementSubPanelUIElement entity</summary>
		public EntityQuery<UIElementSubPanelUIElementEntity> UIElementSubPanelUIElement
		{
			get { return Create<UIElementSubPanelUIElementEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the View entity</summary>
		public EntityQuery<ViewEntity> View
		{
			get { return Create<ViewEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the ViewItem entity</summary>
		public EntityQuery<ViewItemEntity> ViewItem
		{
			get { return Create<ViewItemEntity>(); }
		}


 
	}
}