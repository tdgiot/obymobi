﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;

using Obymobi.Data.Master.DaoClasses;
using Obymobi.Data.Master.HelperClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.Master.FactoryClasses
{
	/// <summary>
	/// Generic factory for DAO objects. 
	/// </summary>
	public partial class DAOFactory
	{
		/// <summary>
		/// Private CTor, no instantiation possible.
		/// </summary>
		private DAOFactory()
		{
		}

		/// <summary>Creates a new CountryDAO object</summary>
		/// <returns>the new DAO object ready to use for Country Entities</returns>
		public static CountryDAO CreateCountryDAO()
		{
			return new CountryDAO();
		}

		/// <summary>Creates a new CurrencyDAO object</summary>
		/// <returns>the new DAO object ready to use for Currency Entities</returns>
		public static CurrencyDAO CreateCurrencyDAO()
		{
			return new CurrencyDAO();
		}

		/// <summary>Creates a new EntityFieldInformationDAO object</summary>
		/// <returns>the new DAO object ready to use for EntityFieldInformation Entities</returns>
		public static EntityFieldInformationDAO CreateEntityFieldInformationDAO()
		{
			return new EntityFieldInformationDAO();
		}

		/// <summary>Creates a new EntityInformationDAO object</summary>
		/// <returns>the new DAO object ready to use for EntityInformation Entities</returns>
		public static EntityInformationDAO CreateEntityInformationDAO()
		{
			return new EntityInformationDAO();
		}

		/// <summary>Creates a new ModuleDAO object</summary>
		/// <returns>the new DAO object ready to use for Module Entities</returns>
		public static ModuleDAO CreateModuleDAO()
		{
			return new ModuleDAO();
		}

		/// <summary>Creates a new ReferentialConstraintDAO object</summary>
		/// <returns>the new DAO object ready to use for ReferentialConstraint Entities</returns>
		public static ReferentialConstraintDAO CreateReferentialConstraintDAO()
		{
			return new ReferentialConstraintDAO();
		}

		/// <summary>Creates a new RoleDAO object</summary>
		/// <returns>the new DAO object ready to use for Role Entities</returns>
		public static RoleDAO CreateRoleDAO()
		{
			return new RoleDAO();
		}

		/// <summary>Creates a new RoleEntityRightsDAO object</summary>
		/// <returns>the new DAO object ready to use for RoleEntityRights Entities</returns>
		public static RoleEntityRightsDAO CreateRoleEntityRightsDAO()
		{
			return new RoleEntityRightsDAO();
		}

		/// <summary>Creates a new RoleModuleRightsDAO object</summary>
		/// <returns>the new DAO object ready to use for RoleModuleRights Entities</returns>
		public static RoleModuleRightsDAO CreateRoleModuleRightsDAO()
		{
			return new RoleModuleRightsDAO();
		}

		/// <summary>Creates a new RoleUIElementRightsDAO object</summary>
		/// <returns>the new DAO object ready to use for RoleUIElementRights Entities</returns>
		public static RoleUIElementRightsDAO CreateRoleUIElementRightsDAO()
		{
			return new RoleUIElementRightsDAO();
		}

		/// <summary>Creates a new RoleUIElementSubPanelRightsDAO object</summary>
		/// <returns>the new DAO object ready to use for RoleUIElementSubPanelRights Entities</returns>
		public static RoleUIElementSubPanelRightsDAO CreateRoleUIElementSubPanelRightsDAO()
		{
			return new RoleUIElementSubPanelRightsDAO();
		}

		/// <summary>Creates a new TimeZoneDAO object</summary>
		/// <returns>the new DAO object ready to use for TimeZone Entities</returns>
		public static TimeZoneDAO CreateTimeZoneDAO()
		{
			return new TimeZoneDAO();
		}

		/// <summary>Creates a new TranslationDAO object</summary>
		/// <returns>the new DAO object ready to use for Translation Entities</returns>
		public static TranslationDAO CreateTranslationDAO()
		{
			return new TranslationDAO();
		}

		/// <summary>Creates a new UIColorDAO object</summary>
		/// <returns>the new DAO object ready to use for UIColor Entities</returns>
		public static UIColorDAO CreateUIColorDAO()
		{
			return new UIColorDAO();
		}

		/// <summary>Creates a new UIElementDAO object</summary>
		/// <returns>the new DAO object ready to use for UIElement Entities</returns>
		public static UIElementDAO CreateUIElementDAO()
		{
			return new UIElementDAO();
		}

		/// <summary>Creates a new UIElementSubPanelDAO object</summary>
		/// <returns>the new DAO object ready to use for UIElementSubPanel Entities</returns>
		public static UIElementSubPanelDAO CreateUIElementSubPanelDAO()
		{
			return new UIElementSubPanelDAO();
		}

		/// <summary>Creates a new UIElementSubPanelUIElementDAO object</summary>
		/// <returns>the new DAO object ready to use for UIElementSubPanelUIElement Entities</returns>
		public static UIElementSubPanelUIElementDAO CreateUIElementSubPanelUIElementDAO()
		{
			return new UIElementSubPanelUIElementDAO();
		}

		/// <summary>Creates a new ViewDAO object</summary>
		/// <returns>the new DAO object ready to use for View Entities</returns>
		public static ViewDAO CreateViewDAO()
		{
			return new ViewDAO();
		}

		/// <summary>Creates a new ViewItemDAO object</summary>
		/// <returns>the new DAO object ready to use for ViewItem Entities</returns>
		public static ViewItemDAO CreateViewItemDAO()
		{
			return new ViewItemDAO();
		}

		/// <summary>Creates a new TypedListDAO object</summary>
		/// <returns>The new DAO object ready to use for Typed Lists</returns>
		public static TypedListDAO CreateTypedListDAO()
		{
			return new TypedListDAO();
		}

		#region Included Code

		#endregion
	}
}
