﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections.Generic;
using Obymobi.Data.Master.HelperClasses;
using Obymobi.Data.Master.RelationClasses;
using Obymobi.Data.Master.DaoClasses;

using Obymobi.Data.Master.EntityClasses;
using Obymobi.Data.Master.CollectionClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.Master.FactoryClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>general base class for the generated factories</summary>
	[Serializable]
	public partial class EntityFactoryBase : EntityFactoryCore
	{
		private readonly Obymobi.Data.Master.EntityType _typeOfEntity;
		
		/// <summary>CTor</summary>
		/// <param name="entityName">Name of the entity.</param>
		/// <param name="typeOfEntity">The type of entity.</param>
		public EntityFactoryBase(string entityName, Obymobi.Data.Master.EntityType typeOfEntity) : base(entityName)
		{
			_typeOfEntity = typeOfEntity;
		}

		/// <summary>Creates a new entity instance using the GeneralEntityFactory in the generated code, using the passed in entitytype value</summary>
		/// <param name="entityTypeValue">The entity type value of the entity to create an instance for.</param>
		/// <returns>new IEntity instance</returns>
		public override IEntity CreateEntityFromEntityTypeValue(int entityTypeValue)
		{
			return GeneralEntityFactory.Create((Obymobi.Data.Master.EntityType)entityTypeValue);
		}
		
		/// <summary>Creates, using the generated EntityFieldsFactory, the IEntityFields object for the entity to create. </summary>
		/// <returns>Empty IEntityFields object.</returns>
		public override IEntityFields CreateFields()
		{
			return EntityFieldsFactory.CreateEntityFieldsObject(_typeOfEntity);
		}

		/// <summary>Creates the relations collection to the entity to join all targets so this entity can be fetched. </summary>
		/// <param name="objectAlias">The object alias to use for the elements in the relations.</param>
		/// <returns>null if the entity isn't in a hierarchy of type TargetPerEntity, otherwise the relations collection needed to join all targets together to fetch all subtypes of this entity and this entity itself</returns>
		public override IRelationCollection CreateHierarchyRelations(string objectAlias) 
		{
			return InheritanceInfoProviderSingleton.GetInstance().GetHierarchyRelations(ForEntityName, objectAlias);
		}

		/// <summary>This method retrieves, using the InheritanceInfoprovider, the factory for the entity represented by the values passed in.</summary>
		/// <param name="fieldValues">Field values read from the db, to determine which factory to return, based on the field values passed in.</param>
		/// <param name="entityFieldStartIndexesPerEntity">indexes into values where per entity type their own fields start.</param>
		/// <returns>the factory for the entity which is represented by the values passed in.</returns>
		public override IEntityFactory GetEntityFactory(object[] fieldValues, Dictionary<string, int> entityFieldStartIndexesPerEntity)
		{
			return (IEntityFactory)InheritanceInfoProviderSingleton.GetInstance().GetEntityFactory(ForEntityName, fieldValues, entityFieldStartIndexesPerEntity) ?? this;
		}
						
		/// <summary>Creates a new entity collection for the entity of this factory.</summary>
		/// <returns>ready to use new entity collection, typed.</returns>
		public override IEntityCollection CreateEntityCollection()
		{
			return GeneralEntityCollectionFactory.Create(_typeOfEntity);
		}
	}
	
	/// <summary>Factory to create new, empty CountryEntity objects.</summary>
	[Serializable]
	public partial class CountryEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public CountryEntityFactory() : base("CountryEntity", Obymobi.Data.Master.EntityType.CountryEntity) { }

		/// <summary>Creates a new, empty CountryEntity object.</summary>
		/// <returns>A new, empty CountryEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new CountryEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewCountry
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty CurrencyEntity objects.</summary>
	[Serializable]
	public partial class CurrencyEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public CurrencyEntityFactory() : base("CurrencyEntity", Obymobi.Data.Master.EntityType.CurrencyEntity) { }

		/// <summary>Creates a new, empty CurrencyEntity object.</summary>
		/// <returns>A new, empty CurrencyEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new CurrencyEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewCurrency
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty EntityFieldInformationEntity objects.</summary>
	[Serializable]
	public partial class EntityFieldInformationEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public EntityFieldInformationEntityFactory() : base("EntityFieldInformationEntity", Obymobi.Data.Master.EntityType.EntityFieldInformationEntity) { }

		/// <summary>Creates a new, empty EntityFieldInformationEntity object.</summary>
		/// <returns>A new, empty EntityFieldInformationEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new EntityFieldInformationEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewEntityFieldInformation
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty EntityInformationEntity objects.</summary>
	[Serializable]
	public partial class EntityInformationEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public EntityInformationEntityFactory() : base("EntityInformationEntity", Obymobi.Data.Master.EntityType.EntityInformationEntity) { }

		/// <summary>Creates a new, empty EntityInformationEntity object.</summary>
		/// <returns>A new, empty EntityInformationEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new EntityInformationEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewEntityInformation
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty ModuleEntity objects.</summary>
	[Serializable]
	public partial class ModuleEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public ModuleEntityFactory() : base("ModuleEntity", Obymobi.Data.Master.EntityType.ModuleEntity) { }

		/// <summary>Creates a new, empty ModuleEntity object.</summary>
		/// <returns>A new, empty ModuleEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new ModuleEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewModule
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty ReferentialConstraintEntity objects.</summary>
	[Serializable]
	public partial class ReferentialConstraintEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public ReferentialConstraintEntityFactory() : base("ReferentialConstraintEntity", Obymobi.Data.Master.EntityType.ReferentialConstraintEntity) { }

		/// <summary>Creates a new, empty ReferentialConstraintEntity object.</summary>
		/// <returns>A new, empty ReferentialConstraintEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new ReferentialConstraintEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewReferentialConstraint
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty RoleEntity objects.</summary>
	[Serializable]
	public partial class RoleEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public RoleEntityFactory() : base("RoleEntity", Obymobi.Data.Master.EntityType.RoleEntity) { }

		/// <summary>Creates a new, empty RoleEntity object.</summary>
		/// <returns>A new, empty RoleEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new RoleEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewRole
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty RoleEntityRightsEntity objects.</summary>
	[Serializable]
	public partial class RoleEntityRightsEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public RoleEntityRightsEntityFactory() : base("RoleEntityRightsEntity", Obymobi.Data.Master.EntityType.RoleEntityRightsEntity) { }

		/// <summary>Creates a new, empty RoleEntityRightsEntity object.</summary>
		/// <returns>A new, empty RoleEntityRightsEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new RoleEntityRightsEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewRoleEntityRights
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty RoleModuleRightsEntity objects.</summary>
	[Serializable]
	public partial class RoleModuleRightsEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public RoleModuleRightsEntityFactory() : base("RoleModuleRightsEntity", Obymobi.Data.Master.EntityType.RoleModuleRightsEntity) { }

		/// <summary>Creates a new, empty RoleModuleRightsEntity object.</summary>
		/// <returns>A new, empty RoleModuleRightsEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new RoleModuleRightsEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewRoleModuleRights
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty RoleUIElementRightsEntity objects.</summary>
	[Serializable]
	public partial class RoleUIElementRightsEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public RoleUIElementRightsEntityFactory() : base("RoleUIElementRightsEntity", Obymobi.Data.Master.EntityType.RoleUIElementRightsEntity) { }

		/// <summary>Creates a new, empty RoleUIElementRightsEntity object.</summary>
		/// <returns>A new, empty RoleUIElementRightsEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new RoleUIElementRightsEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewRoleUIElementRights
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty RoleUIElementSubPanelRightsEntity objects.</summary>
	[Serializable]
	public partial class RoleUIElementSubPanelRightsEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public RoleUIElementSubPanelRightsEntityFactory() : base("RoleUIElementSubPanelRightsEntity", Obymobi.Data.Master.EntityType.RoleUIElementSubPanelRightsEntity) { }

		/// <summary>Creates a new, empty RoleUIElementSubPanelRightsEntity object.</summary>
		/// <returns>A new, empty RoleUIElementSubPanelRightsEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new RoleUIElementSubPanelRightsEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewRoleUIElementSubPanelRights
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty TimeZoneEntity objects.</summary>
	[Serializable]
	public partial class TimeZoneEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public TimeZoneEntityFactory() : base("TimeZoneEntity", Obymobi.Data.Master.EntityType.TimeZoneEntity) { }

		/// <summary>Creates a new, empty TimeZoneEntity object.</summary>
		/// <returns>A new, empty TimeZoneEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new TimeZoneEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewTimeZone
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty TranslationEntity objects.</summary>
	[Serializable]
	public partial class TranslationEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public TranslationEntityFactory() : base("TranslationEntity", Obymobi.Data.Master.EntityType.TranslationEntity) { }

		/// <summary>Creates a new, empty TranslationEntity object.</summary>
		/// <returns>A new, empty TranslationEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new TranslationEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewTranslation
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty UIColorEntity objects.</summary>
	[Serializable]
	public partial class UIColorEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public UIColorEntityFactory() : base("UIColorEntity", Obymobi.Data.Master.EntityType.UIColorEntity) { }

		/// <summary>Creates a new, empty UIColorEntity object.</summary>
		/// <returns>A new, empty UIColorEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new UIColorEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewUIColor
			// __LLBLGENPRO_USER_CODE_REGION_END
			
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty UIElementEntity objects.</summary>
	[Serializable]
	public partial class UIElementEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public UIElementEntityFactory() : base("UIElementEntity", Obymobi.Data.Master.EntityType.UIElementEntity) { }

		/// <summary>Creates a new, empty UIElementEntity object.</summary>
		/// <returns>A new, empty UIElementEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new UIElementEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewUIElement
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty UIElementSubPanelEntity objects.</summary>
	[Serializable]
	public partial class UIElementSubPanelEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public UIElementSubPanelEntityFactory() : base("UIElementSubPanelEntity", Obymobi.Data.Master.EntityType.UIElementSubPanelEntity) { }

		/// <summary>Creates a new, empty UIElementSubPanelEntity object.</summary>
		/// <returns>A new, empty UIElementSubPanelEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new UIElementSubPanelEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewUIElementSubPanel
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty UIElementSubPanelUIElementEntity objects.</summary>
	[Serializable]
	public partial class UIElementSubPanelUIElementEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public UIElementSubPanelUIElementEntityFactory() : base("UIElementSubPanelUIElementEntity", Obymobi.Data.Master.EntityType.UIElementSubPanelUIElementEntity) { }

		/// <summary>Creates a new, empty UIElementSubPanelUIElementEntity object.</summary>
		/// <returns>A new, empty UIElementSubPanelUIElementEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new UIElementSubPanelUIElementEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewUIElementSubPanelUIElement
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty ViewEntity objects.</summary>
	[Serializable]
	public partial class ViewEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public ViewEntityFactory() : base("ViewEntity", Obymobi.Data.Master.EntityType.ViewEntity) { }

		/// <summary>Creates a new, empty ViewEntity object.</summary>
		/// <returns>A new, empty ViewEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new ViewEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewView
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty ViewItemEntity objects.</summary>
	[Serializable]
	public partial class ViewItemEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public ViewItemEntityFactory() : base("ViewItemEntity", Obymobi.Data.Master.EntityType.ViewItemEntity) { }

		/// <summary>Creates a new, empty ViewItemEntity object.</summary>
		/// <returns>A new, empty ViewItemEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new ViewItemEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewViewItem
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}

	/// <summary>Factory to create new entity collection objects</summary>
	[Serializable]
	public partial class GeneralEntityCollectionFactory
	{
		/// <summary>Creates a new entity collection</summary>
		/// <param name="typeToUse">The entity type to create the collection for.</param>
		/// <returns>A new entity collection object.</returns>
		public static IEntityCollection Create(Obymobi.Data.Master.EntityType typeToUse)
		{
			switch(typeToUse)
			{
				case Obymobi.Data.Master.EntityType.CountryEntity:
					return new CountryCollection();
				case Obymobi.Data.Master.EntityType.CurrencyEntity:
					return new CurrencyCollection();
				case Obymobi.Data.Master.EntityType.EntityFieldInformationEntity:
					return new EntityFieldInformationCollection();
				case Obymobi.Data.Master.EntityType.EntityInformationEntity:
					return new EntityInformationCollection();
				case Obymobi.Data.Master.EntityType.ModuleEntity:
					return new ModuleCollection();
				case Obymobi.Data.Master.EntityType.ReferentialConstraintEntity:
					return new ReferentialConstraintCollection();
				case Obymobi.Data.Master.EntityType.RoleEntity:
					return new RoleCollection();
				case Obymobi.Data.Master.EntityType.RoleEntityRightsEntity:
					return new RoleEntityRightsCollection();
				case Obymobi.Data.Master.EntityType.RoleModuleRightsEntity:
					return new RoleModuleRightsCollection();
				case Obymobi.Data.Master.EntityType.RoleUIElementRightsEntity:
					return new RoleUIElementRightsCollection();
				case Obymobi.Data.Master.EntityType.RoleUIElementSubPanelRightsEntity:
					return new RoleUIElementSubPanelRightsCollection();
				case Obymobi.Data.Master.EntityType.TimeZoneEntity:
					return new TimeZoneCollection();
				case Obymobi.Data.Master.EntityType.TranslationEntity:
					return new TranslationCollection();
				case Obymobi.Data.Master.EntityType.UIColorEntity:
					return new UIColorCollection();
				case Obymobi.Data.Master.EntityType.UIElementEntity:
					return new UIElementCollection();
				case Obymobi.Data.Master.EntityType.UIElementSubPanelEntity:
					return new UIElementSubPanelCollection();
				case Obymobi.Data.Master.EntityType.UIElementSubPanelUIElementEntity:
					return new UIElementSubPanelUIElementCollection();
				case Obymobi.Data.Master.EntityType.ViewEntity:
					return new ViewCollection();
				case Obymobi.Data.Master.EntityType.ViewItemEntity:
					return new ViewItemCollection();
				default:
					return null;
			}
		}		
	}
	
	/// <summary>Factory to create new, empty Entity objects based on the entity type specified. Uses entity specific factory objects</summary>
	[Serializable]
	public partial class GeneralEntityFactory
	{
		/// <summary>Creates a new, empty Entity object of the type specified</summary>
		/// <param name="entityTypeToCreate">The entity type to create.</param>
		/// <returns>A new, empty Entity object.</returns>
		public static IEntity Create(Obymobi.Data.Master.EntityType entityTypeToCreate)
		{
			IEntityFactory factoryToUse = null;
			switch(entityTypeToCreate)
			{
				case Obymobi.Data.Master.EntityType.CountryEntity:
					factoryToUse = new CountryEntityFactory();
					break;
				case Obymobi.Data.Master.EntityType.CurrencyEntity:
					factoryToUse = new CurrencyEntityFactory();
					break;
				case Obymobi.Data.Master.EntityType.EntityFieldInformationEntity:
					factoryToUse = new EntityFieldInformationEntityFactory();
					break;
				case Obymobi.Data.Master.EntityType.EntityInformationEntity:
					factoryToUse = new EntityInformationEntityFactory();
					break;
				case Obymobi.Data.Master.EntityType.ModuleEntity:
					factoryToUse = new ModuleEntityFactory();
					break;
				case Obymobi.Data.Master.EntityType.ReferentialConstraintEntity:
					factoryToUse = new ReferentialConstraintEntityFactory();
					break;
				case Obymobi.Data.Master.EntityType.RoleEntity:
					factoryToUse = new RoleEntityFactory();
					break;
				case Obymobi.Data.Master.EntityType.RoleEntityRightsEntity:
					factoryToUse = new RoleEntityRightsEntityFactory();
					break;
				case Obymobi.Data.Master.EntityType.RoleModuleRightsEntity:
					factoryToUse = new RoleModuleRightsEntityFactory();
					break;
				case Obymobi.Data.Master.EntityType.RoleUIElementRightsEntity:
					factoryToUse = new RoleUIElementRightsEntityFactory();
					break;
				case Obymobi.Data.Master.EntityType.RoleUIElementSubPanelRightsEntity:
					factoryToUse = new RoleUIElementSubPanelRightsEntityFactory();
					break;
				case Obymobi.Data.Master.EntityType.TimeZoneEntity:
					factoryToUse = new TimeZoneEntityFactory();
					break;
				case Obymobi.Data.Master.EntityType.TranslationEntity:
					factoryToUse = new TranslationEntityFactory();
					break;
				case Obymobi.Data.Master.EntityType.UIColorEntity:
					factoryToUse = new UIColorEntityFactory();
					break;
				case Obymobi.Data.Master.EntityType.UIElementEntity:
					factoryToUse = new UIElementEntityFactory();
					break;
				case Obymobi.Data.Master.EntityType.UIElementSubPanelEntity:
					factoryToUse = new UIElementSubPanelEntityFactory();
					break;
				case Obymobi.Data.Master.EntityType.UIElementSubPanelUIElementEntity:
					factoryToUse = new UIElementSubPanelUIElementEntityFactory();
					break;
				case Obymobi.Data.Master.EntityType.ViewEntity:
					factoryToUse = new ViewEntityFactory();
					break;
				case Obymobi.Data.Master.EntityType.ViewItemEntity:
					factoryToUse = new ViewItemEntityFactory();
					break;
			}
			IEntity toReturn = null;
			if(factoryToUse != null)
			{
				toReturn = factoryToUse.Create();
			}
			return toReturn;
		}		
	}
	
	/// <summary>Class which is used to obtain the entity factory based on the .NET type of the entity. </summary>
	[Serializable]
	public static class EntityFactoryFactory
	{
		private static readonly Dictionary<Type, IEntityFactory> _factoryPerType = new Dictionary<Type, IEntityFactory>();

		/// <summary>Initializes the <see cref="EntityFactoryFactory"/> class.</summary>
		static EntityFactoryFactory()
		{
			Array entityTypeValues = Enum.GetValues(typeof(Obymobi.Data.Master.EntityType));
			foreach(int entityTypeValue in entityTypeValues)
			{
				IEntity dummy = GeneralEntityFactory.Create((Obymobi.Data.Master.EntityType)entityTypeValue);
				_factoryPerType.Add(dummy.GetType(), dummy.GetEntityFactory());
			}
		}

		/// <summary>Gets the factory of the entity with the .NET type specified</summary>
		/// <param name="typeOfEntity">The type of entity.</param>
		/// <returns>factory to use or null if not found</returns>
		public static IEntityFactory GetFactory(Type typeOfEntity)
		{
			IEntityFactory toReturn = null;
			_factoryPerType.TryGetValue(typeOfEntity, out toReturn);
			return toReturn;
		}

		/// <summary>Gets the factory of the entity with the Obymobi.Data.Master.EntityType specified</summary>
		/// <param name="typeOfEntity">The type of entity.</param>
		/// <returns>factory to use or null if not found</returns>
		public static IEntityFactory GetFactory(Obymobi.Data.Master.EntityType typeOfEntity)
		{
			return GetFactory(GeneralEntityFactory.Create(typeOfEntity).GetType());
		}
	}
	
	/// <summary>Element creator for creating project elements from somewhere else, like inside Linq providers.</summary>
	public class ElementCreator : ElementCreatorBase, IElementCreator
	{
		/// <summary>Gets the factory of the Entity type with the Obymobi.Data.Master.EntityType value passed in</summary>
		/// <param name="entityTypeValue">The entity type value.</param>
		/// <returns>the entity factory of the entity type or null if not found</returns>
		public IEntityFactory GetFactory(int entityTypeValue)
		{
			return (IEntityFactory)this.GetFactoryImpl(entityTypeValue);
		}

		/// <summary>Gets the factory of the Entity type with the .NET type passed in</summary>
		/// <param name="typeOfEntity">The type of entity.</param>
		/// <returns>the entity factory of the entity type or null if not found</returns>
		public IEntityFactory GetFactory(Type typeOfEntity)
		{
			return (IEntityFactory)this.GetFactoryImpl(typeOfEntity);
		}

		/// <summary>Creates a new resultset fields object with the number of field slots reserved as specified</summary>
		/// <param name="numberOfFields">The number of fields.</param>
		/// <returns>ready to use resultsetfields object</returns>
		public IEntityFields CreateResultsetFields(int numberOfFields)
		{
			return new ResultsetFields(numberOfFields);
		}
		
		/// <summary>Gets an instance of the TypedListDAO class to execute dynamic lists and projections.</summary>
		/// <returns>ready to use typedlistDAO</returns>
		public IDao GetTypedListDao()
		{
			return new TypedListDAO();
		}
		
		/// <summary>Obtains the inheritance info provider instance from the singleton </summary>
		/// <returns>The singleton instance of the inheritance info provider</returns>
		public override IInheritanceInfoProvider ObtainInheritanceInfoProviderInstance()
		{
			return InheritanceInfoProviderSingleton.GetInstance();
		}


		/// <summary>Creates a new dynamic relation instance</summary>
		/// <param name="leftOperand">The left operand.</param>
		/// <returns>ready to use dynamic relation</returns>
		public override IDynamicRelation CreateDynamicRelation(DerivedTableDefinition leftOperand)
		{
			return new DynamicRelation(leftOperand);
		}

		/// <summary>Creates a new dynamic relation instance</summary>
		/// <param name="leftOperand">The left operand.</param>
		/// <param name="joinType">Type of the join. If None is specified, Inner is assumed.</param>
		/// <param name="rightOperand">The right operand.</param>
		/// <param name="onClause">The on clause for the join.</param>
		/// <returns>ready to use dynamic relation</returns>
		public override IDynamicRelation CreateDynamicRelation(DerivedTableDefinition leftOperand, JoinHint joinType, DerivedTableDefinition rightOperand, IPredicate onClause)
		{
			return new DynamicRelation(leftOperand, joinType, rightOperand, onClause);
		}

		/// <summary>Creates a new dynamic relation instance</summary>
		/// <param name="leftOperand">The left operand.</param>
		/// <param name="joinType">Type of the join. If None is specified, Inner is assumed.</param>
		/// <param name="rightOperand">The right operand.</param>
		/// <param name="aliasLeftOperand">The alias of the left operand. If you don't want to / need to alias the right operand (only alias if you have to), specify string.Empty.</param>
		/// <param name="onClause">The on clause for the join.</param>
		/// <returns>ready to use dynamic relation</returns>
		public override IDynamicRelation CreateDynamicRelation(IEntityFieldCore leftOperand, JoinHint joinType, DerivedTableDefinition rightOperand, string aliasLeftOperand, IPredicate onClause)
		{
			return new DynamicRelation(leftOperand, joinType, rightOperand, aliasLeftOperand, onClause);
		}

		/// <summary>Creates a new dynamic relation instance</summary>
		/// <param name="leftOperand">The left operand.</param>
		/// <param name="joinType">Type of the join. If None is specified, Inner is assumed.</param>
		/// <param name="rightOperandEntityName">Name of the entity, which is used as the right operand.</param>
		/// <param name="aliasRightOperand">The alias of the right operand. If you don't want to / need to alias the right operand (only alias if you have to), specify string.Empty.</param>
		/// <param name="onClause">The on clause for the join.</param>
		/// <returns>ready to use dynamic relation</returns>
		public override IDynamicRelation CreateDynamicRelation(DerivedTableDefinition leftOperand, JoinHint joinType, string rightOperandEntityName, string aliasRightOperand, IPredicate onClause)
		{
			return new DynamicRelation(leftOperand, joinType, (Obymobi.Data.Master.EntityType)Enum.Parse(typeof(Obymobi.Data.Master.EntityType), rightOperandEntityName, false), aliasRightOperand, onClause);
		}

		/// <summary>Creates a new dynamic relation instance</summary>
		/// <param name="leftOperandEntityName">Name of the entity which is used as the left operand.</param>
		/// <param name="joinType">Type of the join. If None is specified, Inner is assumed.</param>
		/// <param name="rightOperandEntityName">Name of the entity, which is used as the right operand.</param>
		/// <param name="aliasLeftOperand">The alias of the left operand. If you don't want to / need to alias the right operand (only alias if you have to), specify string.Empty.</param>
		/// <param name="aliasRightOperand">The alias of the right operand. If you don't want to / need to alias the right operand (only alias if you have to), specify string.Empty.</param>
		/// <param name="onClause">The on clause for the join.</param>
		/// <returns>ready to use dynamic relation</returns>
		public override IDynamicRelation CreateDynamicRelation(string leftOperandEntityName, JoinHint joinType, string rightOperandEntityName, string aliasLeftOperand, string aliasRightOperand, IPredicate onClause)
		{
			return new DynamicRelation((Obymobi.Data.Master.EntityType)Enum.Parse(typeof(Obymobi.Data.Master.EntityType), leftOperandEntityName, false), joinType, (Obymobi.Data.Master.EntityType)Enum.Parse(typeof(Obymobi.Data.Master.EntityType), rightOperandEntityName, false), aliasLeftOperand, aliasRightOperand, onClause);
		}
		
		/// <summary>Creates a new dynamic relation instance</summary>
		/// <param name="leftOperand">The left operand.</param>
		/// <param name="joinType">Type of the join. If None is specified, Inner is assumed.</param>
		/// <param name="rightOperandEntityName">Name of the entity, which is used as the right operand.</param>
		/// <param name="aliasLeftOperand">The alias of the left operand. If you don't want to / need to alias the right operand (only alias if you have to), specify string.Empty.</param>
		/// <param name="aliasRightOperand">The alias of the right operand. If you don't want to / need to alias the right operand (only alias if you have to), specify string.Empty.</param>
		/// <param name="onClause">The on clause for the join.</param>
		/// <returns>ready to use dynamic relation</returns>
		public override IDynamicRelation CreateDynamicRelation(IEntityFieldCore leftOperand, JoinHint joinType, string rightOperandEntityName, string aliasLeftOperand, string aliasRightOperand, IPredicate onClause)
		{
			return new DynamicRelation(leftOperand, joinType, (Obymobi.Data.Master.EntityType)Enum.Parse(typeof(Obymobi.Data.Master.EntityType), rightOperandEntityName, false), aliasLeftOperand, aliasRightOperand, onClause);
		}
		
		/// <summary>Implementation of the routine which gets the factory of the Entity type with the Obymobi.Data.Master.EntityType value passed in</summary>
		/// <param name="entityTypeValue">The entity type value.</param>
		/// <returns>the entity factory of the entity type or null if not found</returns>
		protected override IEntityFactoryCore GetFactoryImpl(int entityTypeValue)
		{
			return EntityFactoryFactory.GetFactory((Obymobi.Data.Master.EntityType)entityTypeValue);
		}
	
		/// <summary>Implementation of the routine which gets the factory of the Entity type with the .NET type passed in</summary>
		/// <param name="typeOfEntity">The type of entity.</param>
		/// <returns>the entity factory of the entity type or null if not found</returns>
		protected override IEntityFactoryCore GetFactoryImpl(Type typeOfEntity)
		{
			return EntityFactoryFactory.GetFactory(typeOfEntity);
		}

	}
}
