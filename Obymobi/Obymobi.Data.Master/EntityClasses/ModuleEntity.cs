﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using Obymobi.Data.Master.FactoryClasses;
using Obymobi.Data.Master.CollectionClasses;
using Obymobi.Data.Master.DaoClasses;
using Obymobi.Data.Master.RelationClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.Master.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'Module'. This class is used for Business Logic or for framework extension code.</summary>
	[Serializable]
	public partial class ModuleEntity : ModuleEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		/// <summary>CTor</summary>
		public ModuleEntity():base()
		{
		}

		/// <summary>CTor</summary>
		/// <param name="moduleId">PK value for Module which data should be fetched into this Module object</param>
		public ModuleEntity(System.Int32 moduleId):
			base(moduleId)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="moduleId">PK value for Module which data should be fetched into this Module object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public ModuleEntity(System.Int32 moduleId, IPrefetchPath prefetchPathToUse):
			base(moduleId, prefetchPathToUse)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="moduleId">PK value for Module which data should be fetched into this Module object</param>
		/// <param name="validator">The custom validator object for this ModuleEntity</param>
		public ModuleEntity(System.Int32 moduleId, IValidator validator):
			base(moduleId, validator)
		{
		}
		
		/// <summary>CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected ModuleEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}

		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included Code

		#endregion
	}
}
