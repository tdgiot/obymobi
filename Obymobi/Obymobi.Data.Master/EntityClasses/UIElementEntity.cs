﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using Obymobi.Data.Master.FactoryClasses;
using Obymobi.Data.Master.CollectionClasses;
using Obymobi.Data.Master.DaoClasses;
using Obymobi.Data.Master.RelationClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.Master.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'UIElement'. This class is used for Business Logic or for framework extension code.</summary>
	[Serializable]
	public partial class UIElementEntity : UIElementEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		/// <summary>CTor</summary>
		public UIElementEntity():base()
		{
		}

		/// <summary>CTor</summary>
		/// <param name="uIElementId">PK value for UIElement which data should be fetched into this UIElement object</param>
		public UIElementEntity(System.Int32 uIElementId):
			base(uIElementId)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="uIElementId">PK value for UIElement which data should be fetched into this UIElement object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public UIElementEntity(System.Int32 uIElementId, IPrefetchPath prefetchPathToUse):
			base(uIElementId, prefetchPathToUse)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="uIElementId">PK value for UIElement which data should be fetched into this UIElement object</param>
		/// <param name="validator">The custom validator object for this UIElementEntity</param>
		public UIElementEntity(System.Int32 uIElementId, IValidator validator):
			base(uIElementId, validator)
		{
		}
		
		/// <summary>CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected UIElementEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}

		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included Code

		#endregion
	}
}
