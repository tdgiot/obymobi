﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using Obymobi.Data.Master.FactoryClasses;
using Obymobi.Data.Master.CollectionClasses;
using Obymobi.Data.Master.DaoClasses;
using Obymobi.Data.Master.RelationClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.Master.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'View'. This class is used for Business Logic or for framework extension code.</summary>
	[Serializable]
	public partial class ViewEntity : ViewEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		/// <summary>CTor</summary>
		public ViewEntity():base()
		{
		}

		/// <summary>CTor</summary>
		/// <param name="viewId">PK value for View which data should be fetched into this View object</param>
		public ViewEntity(System.Int32 viewId):
			base(viewId)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="viewId">PK value for View which data should be fetched into this View object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public ViewEntity(System.Int32 viewId, IPrefetchPath prefetchPathToUse):
			base(viewId, prefetchPathToUse)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="viewId">PK value for View which data should be fetched into this View object</param>
		/// <param name="validator">The custom validator object for this ViewEntity</param>
		public ViewEntity(System.Int32 viewId, IValidator validator):
			base(viewId, validator)
		{
		}
		
		/// <summary>CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected ViewEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}

		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included Code

		#endregion
	}
}
