﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using Obymobi.Data.Master.FactoryClasses;
using Obymobi.Data.Master.CollectionClasses;
using Obymobi.Data.Master.DaoClasses;
using Obymobi.Data.Master.RelationClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.Master.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'RoleUIElementRights'. This class is used for Business Logic or for framework extension code.</summary>
	[Serializable]
	public partial class RoleUIElementRightsEntity : RoleUIElementRightsEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		/// <summary>CTor</summary>
		public RoleUIElementRightsEntity():base()
		{
		}

		/// <summary>CTor</summary>
		/// <param name="roleUIElementRightsId">PK value for RoleUIElementRights which data should be fetched into this RoleUIElementRights object</param>
		public RoleUIElementRightsEntity(System.Int32 roleUIElementRightsId):
			base(roleUIElementRightsId)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="roleUIElementRightsId">PK value for RoleUIElementRights which data should be fetched into this RoleUIElementRights object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public RoleUIElementRightsEntity(System.Int32 roleUIElementRightsId, IPrefetchPath prefetchPathToUse):
			base(roleUIElementRightsId, prefetchPathToUse)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="roleUIElementRightsId">PK value for RoleUIElementRights which data should be fetched into this RoleUIElementRights object</param>
		/// <param name="validator">The custom validator object for this RoleUIElementRightsEntity</param>
		public RoleUIElementRightsEntity(System.Int32 roleUIElementRightsId, IValidator validator):
			base(roleUIElementRightsId, validator)
		{
		}
		
		/// <summary>CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected RoleUIElementRightsEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}

		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included Code

		#endregion
	}
}
