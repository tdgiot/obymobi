﻿using System.Collections;
using Dionysos;
using Dionysos.Data.LLBLGen;
using Obymobi.Data.Master.CollectionClasses;
using Obymobi.Data.Master.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.Master
{
    public class TimeZones
    {
        public static void RefreshTimeZones()
        {
            var timeZoneCollectionMaster = new TimeZoneCollection();
            timeZoneCollectionMaster.GetMulti(null);

            IEntityCollection timeZoneCollectionLocal = LLBLGenEntityCollectionUtil.GetEntityCollection("TimeZone");
            var timeZoneViewLocal = timeZoneCollectionLocal.DefaultView;

            foreach (var timeZoneEntityMaster in timeZoneCollectionMaster)
            {
                var filterLocalTranslations = new PredicateExpression();
                filterLocalTranslations.Add(LLBLGenFilterUtil.GetFieldCompareValueEqualPredicate("TimeZone", TimeZoneFields.Name.Name, timeZoneEntityMaster.Name));

                timeZoneViewLocal.Filter = filterLocalTranslations;

                IEntity timeZoneEntityLocal;
                if (timeZoneViewLocal.Count == 0)
                {
                    timeZoneEntityLocal = DataFactory.EntityFactory.GetEntity("TimeZone") as IEntity;
                    LLBLGenEntityUtil.CopyFields(timeZoneEntityMaster, timeZoneEntityLocal, new ArrayList());
                }
                else if (timeZoneViewLocal.Count == 1)
                {
                    timeZoneEntityLocal = timeZoneViewLocal[0];
                }
                else
                {
                    throw new FunctionalException("Multiple local Time Zones found for time zone name: '{0}'", timeZoneEntityMaster.Name);
                }

                if (timeZoneEntityLocal == null)
                    continue;

                timeZoneEntityLocal.SetNewFieldValue("NameAndroid", timeZoneEntityMaster.NameAndroid);
                timeZoneEntityLocal.SetNewFieldValue("NameDotNet", timeZoneEntityMaster.NameDotNet);
                
                // If IsNew or  new value, update & save
                timeZoneEntityLocal.Save();
            }
        }
    }
}
