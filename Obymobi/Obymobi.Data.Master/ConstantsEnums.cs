﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;

namespace Obymobi.Data.Master
{
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Country.</summary>
	public enum CountryFieldIndex
	{
		///<summary>CountryId. </summary>
		CountryId,
		///<summary>Name. </summary>
		Name,
		///<summary>Code. </summary>
		Code,
		///<summary>CodeAlpha3. </summary>
		CodeAlpha3,
		///<summary>CultureName. </summary>
		CultureName,
		///<summary>CurrencyId. </summary>
		CurrencyId,
		///<summary>Created. </summary>
		Created,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>Updated. </summary>
		Updated,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Currency.</summary>
	public enum CurrencyFieldIndex
	{
		///<summary>CurrencyId. </summary>
		CurrencyId,
		///<summary>Name. </summary>
		Name,
		///<summary>Symbol. </summary>
		Symbol,
		///<summary>CodeIso4217. </summary>
		CodeIso4217,
		///<summary>Created. </summary>
		Created,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>Updated. </summary>
		Updated,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: EntityFieldInformation.</summary>
	public enum EntityFieldInformationFieldIndex
	{
		///<summary>EntityFieldInformationId. </summary>
		EntityFieldInformationId,
		///<summary>EntityName. </summary>
		EntityName,
		///<summary>FieldName. </summary>
		FieldName,
		///<summary>FriendlyName. </summary>
		FriendlyName,
		///<summary>ShowOnDefaultGridView. </summary>
		ShowOnDefaultGridView,
		///<summary>DefaultDisplayPosition. </summary>
		DefaultDisplayPosition,
		///<summary>AllowShowOnGridView. </summary>
		AllowShowOnGridView,
		///<summary>HelpText. </summary>
		HelpText,
		///<summary>DisplayFormatString. </summary>
		DisplayFormatString,
		///<summary>DefaultSortPosition. </summary>
		DefaultSortPosition,
		///<summary>DefaultSortOperator. </summary>
		DefaultSortOperator,
		///<summary>Type. </summary>
		Type,
		///<summary>IsRequired. </summary>
		IsRequired,
		///<summary>EntityFieldType. </summary>
		EntityFieldType,
		///<summary>ExcludeForUpdateWmsCacheDateOnChange. </summary>
		ExcludeForUpdateWmsCacheDateOnChange,
		///<summary>ExcludeForUpdateCompanyDataVersionOnChange. </summary>
		ExcludeForUpdateCompanyDataVersionOnChange,
		///<summary>ExcludeForUpdateMenuDataVersionOnChange. </summary>
		ExcludeForUpdateMenuDataVersionOnChange,
		///<summary>ExcludeForUpdatePosIntegrationInformationVersionOnChange. </summary>
		ExcludeForUpdatePosIntegrationInformationVersionOnChange,
		///<summary>ForLocalUseWhileSyncingIsUpdated. </summary>
		ForLocalUseWhileSyncingIsUpdated,
		///<summary>Archived. </summary>
		Archived,
		///<summary>Created. </summary>
		Created,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>Updated. </summary>
		Updated,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>Deleted. </summary>
		Deleted,
		///<summary>ExcludeForUpdateDeliverypointDataVersionOnChange. </summary>
		ExcludeForUpdateDeliverypointDataVersionOnChange,
		///<summary>ExcludeForUpdateSurveyDataVersionOnChange. </summary>
		ExcludeForUpdateSurveyDataVersionOnChange,
		///<summary>ExcludeForUpdateAnnouncementDataVersionOnChange. </summary>
		ExcludeForUpdateAnnouncementDataVersionOnChange,
		///<summary>ExcludeForUpdateEntertainmentDataVersionOnChange. </summary>
		ExcludeForUpdateEntertainmentDataVersionOnChange,
		///<summary>ExcludeForUpdateAdvertisementDataVersionOnChange. </summary>
		ExcludeForUpdateAdvertisementDataVersionOnChange,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: EntityInformation.</summary>
	public enum EntityInformationFieldIndex
	{
		///<summary>EntityInformationId. </summary>
		EntityInformationId,
		///<summary>EntityName. </summary>
		EntityName,
		///<summary>ShowFieldName. </summary>
		ShowFieldName,
		///<summary>ShowFieldNameBreadCrumb. </summary>
		ShowFieldNameBreadCrumb,
		///<summary>FriendlyName. </summary>
		FriendlyName,
		///<summary>FriendlyNamePlural. </summary>
		FriendlyNamePlural,
		///<summary>HelpText. </summary>
		HelpText,
		///<summary>TitleAsBreadCrumbHierarchy. </summary>
		TitleAsBreadCrumbHierarchy,
		///<summary>DefaultEntityEditPage. </summary>
		DefaultEntityEditPage,
		///<summary>DefaultEntityCollectionPage. </summary>
		DefaultEntityCollectionPage,
		///<summary>UpdateWmsCacheDateOnChange. </summary>
		UpdateWmsCacheDateOnChange,
		///<summary>UpdateCompanyDataVersionOnChange. </summary>
		UpdateCompanyDataVersionOnChange,
		///<summary>UpdateMenuDataVersionOnChange. </summary>
		UpdateMenuDataVersionOnChange,
		///<summary>UpdatePosIntegrationInformationOnChange. </summary>
		UpdatePosIntegrationInformationOnChange,
		///<summary>ForLocalUseWhileSyncingIsUpdated. </summary>
		ForLocalUseWhileSyncingIsUpdated,
		///<summary>Archived. </summary>
		Archived,
		///<summary>Created. </summary>
		Created,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>Updated. </summary>
		Updated,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>Deleted. </summary>
		Deleted,
		///<summary>UpdateDeliverypointDataVersionOnChange. </summary>
		UpdateDeliverypointDataVersionOnChange,
		///<summary>UpdateSurveyDataVersionOnChange. </summary>
		UpdateSurveyDataVersionOnChange,
		///<summary>UpdateAnnouncementDataVersionOnChange. </summary>
		UpdateAnnouncementDataVersionOnChange,
		///<summary>UpdateEntertainmentDataVersionOnChange. </summary>
		UpdateEntertainmentDataVersionOnChange,
		///<summary>UpdateAdvertisementDataVersionOnChange. </summary>
		UpdateAdvertisementDataVersionOnChange,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Module.</summary>
	public enum ModuleFieldIndex
	{
		///<summary>ModuleId. </summary>
		ModuleId,
		///<summary>NameSystem. </summary>
		NameSystem,
		///<summary>NameFull. </summary>
		NameFull,
		///<summary>NameShort. </summary>
		NameShort,
		///<summary>IconPath. </summary>
		IconPath,
		///<summary>DefaultUiElementTypeNameFull. </summary>
		DefaultUiElementTypeNameFull,
		///<summary>SortOrder. </summary>
		SortOrder,
		///<summary>LicensingFree. </summary>
		LicensingFree,
		///<summary>UserRightsFree. </summary>
		UserRightsFree,
		///<summary>ForLocalUseWhileSyncingIsUpdated. </summary>
		ForLocalUseWhileSyncingIsUpdated,
		///<summary>Archived. </summary>
		Archived,
		///<summary>Created. </summary>
		Created,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>Updated. </summary>
		Updated,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>Deleted. </summary>
		Deleted,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: ReferentialConstraint.</summary>
	public enum ReferentialConstraintFieldIndex
	{
		///<summary>ReferentialConstraintId. </summary>
		ReferentialConstraintId,
		///<summary>EntityName. </summary>
		EntityName,
		///<summary>ForeignKeyName. </summary>
		ForeignKeyName,
		///<summary>DeleteRule. </summary>
		DeleteRule,
		///<summary>UpdateRule. </summary>
		UpdateRule,
		///<summary>ArchiveRule. </summary>
		ArchiveRule,
		///<summary>ForLocalUseWhileSyncingIsUpdated. </summary>
		ForLocalUseWhileSyncingIsUpdated,
		///<summary>Archived. </summary>
		Archived,
		///<summary>Created. </summary>
		Created,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>Updated. </summary>
		Updated,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>Deleted. </summary>
		Deleted,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Role.</summary>
	public enum RoleFieldIndex
	{
		///<summary>RoleId. </summary>
		RoleId,
		///<summary>ParentRoleId. </summary>
		ParentRoleId,
		///<summary>Name. </summary>
		Name,
		///<summary>IsAdministrator. </summary>
		IsAdministrator,
		///<summary>SystemName. </summary>
		SystemName,
		///<summary>SystemRole. </summary>
		SystemRole,
		///<summary>Created. </summary>
		Created,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>Updated. </summary>
		Updated,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>Deleted. </summary>
		Deleted,
		///<summary>Archived. </summary>
		Archived,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: RoleEntityRights.</summary>
	public enum RoleEntityRightsFieldIndex
	{
		///<summary>RoleEntityRightId. </summary>
		RoleEntityRightId,
		///<summary>RoleId. </summary>
		RoleId,
		///<summary>EntityInformationId. </summary>
		EntityInformationId,
		///<summary>AllowReadAll. </summary>
		AllowReadAll,
		///<summary>AllowUpdateAll. </summary>
		AllowUpdateAll,
		///<summary>AllowInsert. </summary>
		AllowInsert,
		///<summary>AllowDeleteAll. </summary>
		AllowDeleteAll,
		///<summary>AllowUpdateCreated. </summary>
		AllowUpdateCreated,
		///<summary>AllowDeleteCreated. </summary>
		AllowDeleteCreated,
		///<summary>Created. </summary>
		Created,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>Updated. </summary>
		Updated,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>Deleted. </summary>
		Deleted,
		///<summary>Archived. </summary>
		Archived,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: RoleModuleRights.</summary>
	public enum RoleModuleRightsFieldIndex
	{
		///<summary>RoleModuleRightsId. </summary>
		RoleModuleRightsId,
		///<summary>RoleId. </summary>
		RoleId,
		///<summary>ModuleId. </summary>
		ModuleId,
		///<summary>IsAllowed. </summary>
		IsAllowed,
		///<summary>Archived. </summary>
		Archived,
		///<summary>Created. </summary>
		Created,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>Updated. </summary>
		Updated,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>Deleted. </summary>
		Deleted,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: RoleUIElementRights.</summary>
	public enum RoleUIElementRightsFieldIndex
	{
		///<summary>RoleUIElementRightsId. </summary>
		RoleUIElementRightsId,
		///<summary>RoleId. </summary>
		RoleId,
		///<summary>UiElementTypeNameFull. </summary>
		UiElementTypeNameFull,
		///<summary>IsAllowed. </summary>
		IsAllowed,
		///<summary>IsReadOnly. </summary>
		IsReadOnly,
		///<summary>Archived. </summary>
		Archived,
		///<summary>Created. </summary>
		Created,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>Updated. </summary>
		Updated,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>Deleted. </summary>
		Deleted,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: RoleUIElementSubPanelRights.</summary>
	public enum RoleUIElementSubPanelRightsFieldIndex
	{
		///<summary>RoleUIElementSubPanelRightsId. </summary>
		RoleUIElementSubPanelRightsId,
		///<summary>RoleId. </summary>
		RoleId,
		///<summary>UiElementTypeNameFull. </summary>
		UiElementTypeNameFull,
		///<summary>IsAllowed. </summary>
		IsAllowed,
		///<summary>Archived. </summary>
		Archived,
		///<summary>Created. </summary>
		Created,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>Updated. </summary>
		Updated,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>Deleted. </summary>
		Deleted,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: TimeZone.</summary>
	public enum TimeZoneFieldIndex
	{
		///<summary>TimeZoneId. </summary>
		TimeZoneId,
		///<summary>Name. </summary>
		Name,
		///<summary>NameAndroid. </summary>
		NameAndroid,
		///<summary>NameDotNet. </summary>
		NameDotNet,
		///<summary>Created. </summary>
		Created,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>Updated. </summary>
		Updated,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Translation.</summary>
	public enum TranslationFieldIndex
	{
		///<summary>TranslationId. </summary>
		TranslationId,
		///<summary>TranslationKey. </summary>
		TranslationKey,
		///<summary>LanguageCode. </summary>
		LanguageCode,
		///<summary>TranslationValue. </summary>
		TranslationValue,
		///<summary>LocallyCreated. </summary>
		LocallyCreated,
		///<summary>Archived. </summary>
		Archived,
		///<summary>Created. </summary>
		Created,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>Updated. </summary>
		Updated,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>Deleted. </summary>
		Deleted,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: UIColor.</summary>
	public enum UIColorFieldIndex
	{
		///<summary>UIColorId. </summary>
		UIColorId,
		///<summary>Type. </summary>
		Type,
		///<summary>Color. </summary>
		Color,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>GroupName. </summary>
		GroupName,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: UIElement.</summary>
	public enum UIElementFieldIndex
	{
		///<summary>UIElementId. </summary>
		UIElementId,
		///<summary>ModuleNameSystem. </summary>
		ModuleNameSystem,
		///<summary>NameSystem. </summary>
		NameSystem,
		///<summary>NameFull. </summary>
		NameFull,
		///<summary>NameShort. </summary>
		NameShort,
		///<summary>Url. </summary>
		Url,
		///<summary>DisplayInMenu. </summary>
		DisplayInMenu,
		///<summary>TypeNameFull. </summary>
		TypeNameFull,
		///<summary>SortOrder. </summary>
		SortOrder,
		///<summary>FreeAccess. </summary>
		FreeAccess,
		///<summary>LicensingFree. </summary>
		LicensingFree,
		///<summary>UserRightsFree. </summary>
		UserRightsFree,
		///<summary>IsConfigurationItem. </summary>
		IsConfigurationItem,
		///<summary>EntityName. </summary>
		EntityName,
		///<summary>ForLocalUseWhileSyncingIsUpdated. </summary>
		ForLocalUseWhileSyncingIsUpdated,
		///<summary>Deleted. </summary>
		Deleted,
		///<summary>Archived. </summary>
		Archived,
		///<summary>Created. </summary>
		Created,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>Updated. </summary>
		Updated,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: UIElementSubPanel.</summary>
	public enum UIElementSubPanelFieldIndex
	{
		///<summary>UIElementSubPanelId. </summary>
		UIElementSubPanelId,
		///<summary>NameSystem. </summary>
		NameSystem,
		///<summary>NameFull. </summary>
		NameFull,
		///<summary>NameShort. </summary>
		NameShort,
		///<summary>Url. </summary>
		Url,
		///<summary>TypeNameFull. </summary>
		TypeNameFull,
		///<summary>SortOrder. </summary>
		SortOrder,
		///<summary>FreeAccess. </summary>
		FreeAccess,
		///<summary>LicensingFree. </summary>
		LicensingFree,
		///<summary>EntityName. </summary>
		EntityName,
		///<summary>OnTabPage. </summary>
		OnTabPage,
		///<summary>ForLocalUseWhileSyncingIsUpdated. </summary>
		ForLocalUseWhileSyncingIsUpdated,
		///<summary>Deleted. </summary>
		Deleted,
		///<summary>Archived. </summary>
		Archived,
		///<summary>Created. </summary>
		Created,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>Updated. </summary>
		Updated,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: UIElementSubPanelUIElement.</summary>
	public enum UIElementSubPanelUIElementFieldIndex
	{
		///<summary>UIElementSubPanelUIElementId. </summary>
		UIElementSubPanelUIElementId,
		///<summary>UIElementSubPanelTypeNameFull. </summary>
		UIElementSubPanelTypeNameFull,
		///<summary>ParentUIElementTypeNameFull. </summary>
		ParentUIElementTypeNameFull,
		///<summary>ParentUIElementSubPanelTypeNameFull. </summary>
		ParentUIElementSubPanelTypeNameFull,
		///<summary>ForLocalUseWhileSyncingIsUpdated. </summary>
		ForLocalUseWhileSyncingIsUpdated,
		///<summary>Archived. </summary>
		Archived,
		///<summary>Created. </summary>
		Created,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>Updated. </summary>
		Updated,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: View.</summary>
	public enum ViewFieldIndex
	{
		///<summary>ViewId. </summary>
		ViewId,
		///<summary>Name. </summary>
		Name,
		///<summary>EntityName. </summary>
		EntityName,
		///<summary>UiElementTypeNameFull. </summary>
		UiElementTypeNameFull,
		///<summary>ForLocalUseWhileSyncingIsUpdated. </summary>
		ForLocalUseWhileSyncingIsUpdated,
		///<summary>Archived. </summary>
		Archived,
		///<summary>Created. </summary>
		Created,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>Updated. </summary>
		Updated,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>Deleted. </summary>
		Deleted,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: ViewItem.</summary>
	public enum ViewItemFieldIndex
	{
		///<summary>ViewItemId. </summary>
		ViewItemId,
		///<summary>ViewId. </summary>
		ViewId,
		///<summary>EntityName. </summary>
		EntityName,
		///<summary>FieldName. </summary>
		FieldName,
		///<summary>ShowOnGridView. </summary>
		ShowOnGridView,
		///<summary>DisplayPosition. </summary>
		DisplayPosition,
		///<summary>SortPosition. </summary>
		SortPosition,
		///<summary>SortOperator. </summary>
		SortOperator,
		///<summary>DisplayFormatString. </summary>
		DisplayFormatString,
		///<summary>Width. </summary>
		Width,
		///<summary>Type. </summary>
		Type,
		///<summary>UiElementTypeNameFull. </summary>
		UiElementTypeNameFull,
		///<summary>ForLocalUseWhileSyncingIsUpdated. </summary>
		ForLocalUseWhileSyncingIsUpdated,
		///<summary>Archived. </summary>
		Archived,
		///<summary>Created. </summary>
		Created,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>Updated. </summary>
		Updated,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>Deleted. </summary>
		Deleted,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		/// <summary></summary>
		AmountOfFields
	}



	/// <summary>Enum definition for all the entity types defined in this namespace. Used by the entityfields factory.</summary>
	public enum EntityType
	{
		///<summary>Country</summary>
		CountryEntity,
		///<summary>Currency</summary>
		CurrencyEntity,
		///<summary>EntityFieldInformation</summary>
		EntityFieldInformationEntity,
		///<summary>EntityInformation</summary>
		EntityInformationEntity,
		///<summary>Module</summary>
		ModuleEntity,
		///<summary>ReferentialConstraint</summary>
		ReferentialConstraintEntity,
		///<summary>Role</summary>
		RoleEntity,
		///<summary>RoleEntityRights</summary>
		RoleEntityRightsEntity,
		///<summary>RoleModuleRights</summary>
		RoleModuleRightsEntity,
		///<summary>RoleUIElementRights</summary>
		RoleUIElementRightsEntity,
		///<summary>RoleUIElementSubPanelRights</summary>
		RoleUIElementSubPanelRightsEntity,
		///<summary>TimeZone</summary>
		TimeZoneEntity,
		///<summary>Translation</summary>
		TranslationEntity,
		///<summary>UIColor</summary>
		UIColorEntity,
		///<summary>UIElement</summary>
		UIElementEntity,
		///<summary>UIElementSubPanel</summary>
		UIElementSubPanelEntity,
		///<summary>UIElementSubPanelUIElement</summary>
		UIElementSubPanelUIElementEntity,
		///<summary>View</summary>
		ViewEntity,
		///<summary>ViewItem</summary>
		ViewItemEntity
	}


	#region Custom ConstantsEnums Code
	
	// __LLBLGENPRO_USER_CODE_REGION_START CustomUserConstants
	// __LLBLGENPRO_USER_CODE_REGION_END
	#endregion

	#region Included code

	#endregion
}

