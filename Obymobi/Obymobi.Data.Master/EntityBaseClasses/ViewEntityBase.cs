﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
using System.Runtime.Serialization;
using System.Data;
using System.Xml.Serialization;
using Obymobi.Data.Master;
using Obymobi.Data.Master.FactoryClasses;
using Obymobi.Data.Master.DaoClasses;
using Obymobi.Data.Master.RelationClasses;
using Obymobi.Data.Master.HelperClasses;
using Obymobi.Data.Master.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.Master.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity base class which represents the base class for the entity 'View'.<br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public abstract partial class ViewEntityBase : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		public string LLBLGenProEntityName {
			get { return "ViewEntity"; }
		}
	
		#region Class Member Declarations
		private Obymobi.Data.Master.CollectionClasses.ViewItemCollection	_viewItem;
		private bool	_alwaysFetchViewItem, _alreadyFetchedViewItem;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name ViewItem</summary>
			public static readonly string ViewItem = "ViewItem";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static ViewEntityBase()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		protected ViewEntityBase() :base("ViewEntity")
		{
			InitClassEmpty(null);
		}

		/// <summary>CTor</summary>
		/// <param name="viewId">PK value for View which data should be fetched into this View object</param>
		protected ViewEntityBase(System.Int32 viewId):base("ViewEntity")
		{
			InitClassFetch(viewId, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="viewId">PK value for View which data should be fetched into this View object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		protected ViewEntityBase(System.Int32 viewId, IPrefetchPath prefetchPathToUse): base("ViewEntity")
		{
			InitClassFetch(viewId, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="viewId">PK value for View which data should be fetched into this View object</param>
		/// <param name="validator">The custom validator object for this ViewEntity</param>
		protected ViewEntityBase(System.Int32 viewId, IValidator validator):base("ViewEntity")
		{
			InitClassFetch(viewId, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected ViewEntityBase(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_viewItem = (Obymobi.Data.Master.CollectionClasses.ViewItemCollection)info.GetValue("_viewItem", typeof(Obymobi.Data.Master.CollectionClasses.ViewItemCollection));
			_alwaysFetchViewItem = info.GetBoolean("_alwaysFetchViewItem");
			_alreadyFetchedViewItem = info.GetBoolean("_alreadyFetchedViewItem");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}	

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedViewItem = (_viewItem.Count > 0);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "ViewItem":
					toReturn.Add(Relations.ViewItemEntityUsingViewId);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_viewItem", (!this.MarkedForDeletion?_viewItem:null));
			info.AddValue("_alwaysFetchViewItem", _alwaysFetchViewItem);
			info.AddValue("_alreadyFetchedViewItem", _alreadyFetchedViewItem);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "ViewItem":
					_alreadyFetchedViewItem = true;
					if(entity!=null)
					{
						this.ViewItem.Add((ViewItemEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "ViewItem":
					_viewItem.Add((ViewItemEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "ViewItem":
					this.PerformRelatedEntityRemoval(_viewItem, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_viewItem);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="viewId">PK value for View which data should be fetched into this View object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 viewId)
		{
			return FetchUsingPK(viewId, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="viewId">PK value for View which data should be fetched into this View object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 viewId, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(viewId, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="viewId">PK value for View which data should be fetched into this View object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 viewId, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(viewId, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="viewId">PK value for View which data should be fetched into this View object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 viewId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(viewId, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.ViewId, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new ViewRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'ViewItemEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ViewItemEntity'</returns>
		public Obymobi.Data.Master.CollectionClasses.ViewItemCollection GetMultiViewItem(bool forceFetch)
		{
			return GetMultiViewItem(forceFetch, _viewItem.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ViewItemEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ViewItemEntity'</returns>
		public Obymobi.Data.Master.CollectionClasses.ViewItemCollection GetMultiViewItem(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiViewItem(forceFetch, _viewItem.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ViewItemEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.Master.CollectionClasses.ViewItemCollection GetMultiViewItem(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiViewItem(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ViewItemEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.Master.CollectionClasses.ViewItemCollection GetMultiViewItem(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedViewItem || forceFetch || _alwaysFetchViewItem) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_viewItem);
				_viewItem.SuppressClearInGetMulti=!forceFetch;
				_viewItem.EntityFactoryToUse = entityFactoryToUse;
				_viewItem.GetMultiManyToOne(this, filter);
				_viewItem.SuppressClearInGetMulti=false;
				_alreadyFetchedViewItem = true;
			}
			return _viewItem;
		}

		/// <summary> Sets the collection parameters for the collection for 'ViewItem'. These settings will be taken into account
		/// when the property ViewItem is requested or GetMultiViewItem is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersViewItem(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_viewItem.SortClauses=sortClauses;
			_viewItem.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("ViewItem", _viewItem);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="viewId">PK value for View which data should be fetched into this View object</param>
		/// <param name="validator">The validator object for this ViewEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int32 viewId, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(viewId, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_viewItem = new Obymobi.Data.Master.CollectionClasses.ViewItemCollection();
			_viewItem.SetContainingEntityInfo(this, "View");
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ViewId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Name", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("EntityName", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UiElementTypeNameFull", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ForLocalUseWhileSyncingIsUpdated", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Archived", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Created", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Updated", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Deleted", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedUTC", fieldHashtable);
		}
		#endregion

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="viewId">PK value for View which data should be fetched into this View object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int32 viewId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)ViewFieldIndex.ViewId].ForcedCurrentValueWrite(viewId);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateViewDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new ViewEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static ViewRelations Relations
		{
			get	{ return new ViewRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ViewItem' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathViewItem
		{
			get { return new PrefetchPathElement(new Obymobi.Data.Master.CollectionClasses.ViewItemCollection(), (IEntityRelation)GetRelationsForField("ViewItem")[0], (int)Obymobi.Data.Master.EntityType.ViewEntity, (int)Obymobi.Data.Master.EntityType.ViewItemEntity, 0, null, null, null, "ViewItem", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The ViewId property of the Entity View<br/><br/></summary>
		/// <remarks>Mapped on  table field: "View"."ViewId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 ViewId
		{
			get { return (System.Int32)GetValue((int)ViewFieldIndex.ViewId, true); }
			set	{ SetValue((int)ViewFieldIndex.ViewId, value, true); }
		}

		/// <summary> The Name property of the Entity View<br/><br/></summary>
		/// <remarks>Mapped on  table field: "View"."Name"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 150<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Name
		{
			get { return (System.String)GetValue((int)ViewFieldIndex.Name, true); }
			set	{ SetValue((int)ViewFieldIndex.Name, value, true); }
		}

		/// <summary> The EntityName property of the Entity View<br/><br/></summary>
		/// <remarks>Mapped on  table field: "View"."EntityName"<br/>
		/// Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 250<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String EntityName
		{
			get { return (System.String)GetValue((int)ViewFieldIndex.EntityName, true); }
			set	{ SetValue((int)ViewFieldIndex.EntityName, value, true); }
		}

		/// <summary> The UiElementTypeNameFull property of the Entity View<br/><br/></summary>
		/// <remarks>Mapped on  table field: "View"."UiElementTypeNameFull"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 255<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String UiElementTypeNameFull
		{
			get { return (System.String)GetValue((int)ViewFieldIndex.UiElementTypeNameFull, true); }
			set	{ SetValue((int)ViewFieldIndex.UiElementTypeNameFull, value, true); }
		}

		/// <summary> The ForLocalUseWhileSyncingIsUpdated property of the Entity View<br/><br/></summary>
		/// <remarks>Mapped on  table field: "View"."ForLocalUseWhileSyncingIsUpdated"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean ForLocalUseWhileSyncingIsUpdated
		{
			get { return (System.Boolean)GetValue((int)ViewFieldIndex.ForLocalUseWhileSyncingIsUpdated, true); }
			set	{ SetValue((int)ViewFieldIndex.ForLocalUseWhileSyncingIsUpdated, value, true); }
		}

		/// <summary> The Archived property of the Entity View<br/><br/></summary>
		/// <remarks>Mapped on  table field: "View"."Archived"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean Archived
		{
			get { return (System.Boolean)GetValue((int)ViewFieldIndex.Archived, true); }
			set	{ SetValue((int)ViewFieldIndex.Archived, value, true); }
		}

		/// <summary> The Created property of the Entity View<br/><br/></summary>
		/// <remarks>Mapped on  table field: "View"."Created"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> Created
		{
			get { return (Nullable<System.DateTime>)GetValue((int)ViewFieldIndex.Created, false); }
			set	{ SetValue((int)ViewFieldIndex.Created, value, true); }
		}

		/// <summary> The CreatedBy property of the Entity View<br/><br/></summary>
		/// <remarks>Mapped on  table field: "View"."CreatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CreatedBy
		{
			get { return (System.Int32)GetValue((int)ViewFieldIndex.CreatedBy, true); }
			set	{ SetValue((int)ViewFieldIndex.CreatedBy, value, true); }
		}

		/// <summary> The Updated property of the Entity View<br/><br/></summary>
		/// <remarks>Mapped on  table field: "View"."Updated"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> Updated
		{
			get { return (Nullable<System.DateTime>)GetValue((int)ViewFieldIndex.Updated, false); }
			set	{ SetValue((int)ViewFieldIndex.Updated, value, true); }
		}

		/// <summary> The UpdatedBy property of the Entity View<br/><br/></summary>
		/// <remarks>Mapped on  table field: "View"."UpdatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 UpdatedBy
		{
			get { return (System.Int32)GetValue((int)ViewFieldIndex.UpdatedBy, true); }
			set	{ SetValue((int)ViewFieldIndex.UpdatedBy, value, true); }
		}

		/// <summary> The Deleted property of the Entity View<br/><br/></summary>
		/// <remarks>Mapped on  table field: "View"."Deleted"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean Deleted
		{
			get { return (System.Boolean)GetValue((int)ViewFieldIndex.Deleted, true); }
			set	{ SetValue((int)ViewFieldIndex.Deleted, value, true); }
		}

		/// <summary> The CreatedUTC property of the Entity View<br/><br/></summary>
		/// <remarks>Mapped on  table field: "View"."CreatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CreatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)ViewFieldIndex.CreatedUTC, false); }
			set	{ SetValue((int)ViewFieldIndex.CreatedUTC, value, true); }
		}

		/// <summary> The UpdatedUTC property of the Entity View<br/><br/></summary>
		/// <remarks>Mapped on  table field: "View"."UpdatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)ViewFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)ViewFieldIndex.UpdatedUTC, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'ViewItemEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiViewItem()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.Master.CollectionClasses.ViewItemCollection ViewItem
		{
			get	{ return GetMultiViewItem(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ViewItem. When set to true, ViewItem is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ViewItem is accessed. You can always execute/ a forced fetch by calling GetMultiViewItem(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchViewItem
		{
			get	{ return _alwaysFetchViewItem; }
			set	{ _alwaysFetchViewItem = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property ViewItem already has been fetched. Setting this property to false when ViewItem has been fetched
		/// will clear the ViewItem collection well. Setting this property to true while ViewItem hasn't been fetched disables lazy loading for ViewItem</summary>
		[Browsable(false)]
		public bool AlreadyFetchedViewItem
		{
			get { return _alreadyFetchedViewItem;}
			set 
			{
				if(_alreadyFetchedViewItem && !value && (_viewItem != null))
				{
					_viewItem.Clear();
				}
				_alreadyFetchedViewItem = value;
			}
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the Obymobi.Data.Master.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)Obymobi.Data.Master.EntityType.ViewEntity; }
		}

		#endregion

		
		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
