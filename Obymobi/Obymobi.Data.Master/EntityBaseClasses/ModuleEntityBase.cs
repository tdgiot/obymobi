﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
using System.Runtime.Serialization;
using System.Data;
using System.Xml.Serialization;
using Obymobi.Data.Master;
using Obymobi.Data.Master.FactoryClasses;
using Obymobi.Data.Master.DaoClasses;
using Obymobi.Data.Master.RelationClasses;
using Obymobi.Data.Master.HelperClasses;
using Obymobi.Data.Master.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.Master.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity base class which represents the base class for the entity 'Module'.<br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public abstract partial class ModuleEntityBase : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		public string LLBLGenProEntityName {
			get { return "ModuleEntity"; }
		}
	
		#region Class Member Declarations
		private Obymobi.Data.Master.CollectionClasses.RoleModuleRightsCollection	_roleModuleRights;
		private bool	_alwaysFetchRoleModuleRights, _alreadyFetchedRoleModuleRights;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name RoleModuleRights</summary>
			public static readonly string RoleModuleRights = "RoleModuleRights";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static ModuleEntityBase()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		protected ModuleEntityBase() :base("ModuleEntity")
		{
			InitClassEmpty(null);
		}

		/// <summary>CTor</summary>
		/// <param name="moduleId">PK value for Module which data should be fetched into this Module object</param>
		protected ModuleEntityBase(System.Int32 moduleId):base("ModuleEntity")
		{
			InitClassFetch(moduleId, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="moduleId">PK value for Module which data should be fetched into this Module object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		protected ModuleEntityBase(System.Int32 moduleId, IPrefetchPath prefetchPathToUse): base("ModuleEntity")
		{
			InitClassFetch(moduleId, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="moduleId">PK value for Module which data should be fetched into this Module object</param>
		/// <param name="validator">The custom validator object for this ModuleEntity</param>
		protected ModuleEntityBase(System.Int32 moduleId, IValidator validator):base("ModuleEntity")
		{
			InitClassFetch(moduleId, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected ModuleEntityBase(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_roleModuleRights = (Obymobi.Data.Master.CollectionClasses.RoleModuleRightsCollection)info.GetValue("_roleModuleRights", typeof(Obymobi.Data.Master.CollectionClasses.RoleModuleRightsCollection));
			_alwaysFetchRoleModuleRights = info.GetBoolean("_alwaysFetchRoleModuleRights");
			_alreadyFetchedRoleModuleRights = info.GetBoolean("_alreadyFetchedRoleModuleRights");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}	

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedRoleModuleRights = (_roleModuleRights.Count > 0);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "RoleModuleRights":
					toReturn.Add(Relations.RoleModuleRightsEntityUsingModuleId);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_roleModuleRights", (!this.MarkedForDeletion?_roleModuleRights:null));
			info.AddValue("_alwaysFetchRoleModuleRights", _alwaysFetchRoleModuleRights);
			info.AddValue("_alreadyFetchedRoleModuleRights", _alreadyFetchedRoleModuleRights);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "RoleModuleRights":
					_alreadyFetchedRoleModuleRights = true;
					if(entity!=null)
					{
						this.RoleModuleRights.Add((RoleModuleRightsEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "RoleModuleRights":
					_roleModuleRights.Add((RoleModuleRightsEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "RoleModuleRights":
					this.PerformRelatedEntityRemoval(_roleModuleRights, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_roleModuleRights);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="moduleId">PK value for Module which data should be fetched into this Module object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 moduleId)
		{
			return FetchUsingPK(moduleId, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="moduleId">PK value for Module which data should be fetched into this Module object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 moduleId, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(moduleId, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="moduleId">PK value for Module which data should be fetched into this Module object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 moduleId, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(moduleId, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="moduleId">PK value for Module which data should be fetched into this Module object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 moduleId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(moduleId, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.ModuleId, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new ModuleRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'RoleModuleRightsEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'RoleModuleRightsEntity'</returns>
		public Obymobi.Data.Master.CollectionClasses.RoleModuleRightsCollection GetMultiRoleModuleRights(bool forceFetch)
		{
			return GetMultiRoleModuleRights(forceFetch, _roleModuleRights.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'RoleModuleRightsEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'RoleModuleRightsEntity'</returns>
		public Obymobi.Data.Master.CollectionClasses.RoleModuleRightsCollection GetMultiRoleModuleRights(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiRoleModuleRights(forceFetch, _roleModuleRights.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'RoleModuleRightsEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.Master.CollectionClasses.RoleModuleRightsCollection GetMultiRoleModuleRights(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiRoleModuleRights(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'RoleModuleRightsEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.Master.CollectionClasses.RoleModuleRightsCollection GetMultiRoleModuleRights(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedRoleModuleRights || forceFetch || _alwaysFetchRoleModuleRights) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_roleModuleRights);
				_roleModuleRights.SuppressClearInGetMulti=!forceFetch;
				_roleModuleRights.EntityFactoryToUse = entityFactoryToUse;
				_roleModuleRights.GetMultiManyToOne(this, null, filter);
				_roleModuleRights.SuppressClearInGetMulti=false;
				_alreadyFetchedRoleModuleRights = true;
			}
			return _roleModuleRights;
		}

		/// <summary> Sets the collection parameters for the collection for 'RoleModuleRights'. These settings will be taken into account
		/// when the property RoleModuleRights is requested or GetMultiRoleModuleRights is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersRoleModuleRights(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_roleModuleRights.SortClauses=sortClauses;
			_roleModuleRights.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("RoleModuleRights", _roleModuleRights);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="moduleId">PK value for Module which data should be fetched into this Module object</param>
		/// <param name="validator">The validator object for this ModuleEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int32 moduleId, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(moduleId, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_roleModuleRights = new Obymobi.Data.Master.CollectionClasses.RoleModuleRightsCollection();
			_roleModuleRights.SetContainingEntityInfo(this, "Module");
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ModuleId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("NameSystem", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("NameFull", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("NameShort", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IconPath", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DefaultUiElementTypeNameFull", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SortOrder", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LicensingFree", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UserRightsFree", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ForLocalUseWhileSyncingIsUpdated", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Archived", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Created", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Updated", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Deleted", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedUTC", fieldHashtable);
		}
		#endregion

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="moduleId">PK value for Module which data should be fetched into this Module object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int32 moduleId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)ModuleFieldIndex.ModuleId].ForcedCurrentValueWrite(moduleId);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateModuleDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new ModuleEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static ModuleRelations Relations
		{
			get	{ return new ModuleRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'RoleModuleRights' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathRoleModuleRights
		{
			get { return new PrefetchPathElement(new Obymobi.Data.Master.CollectionClasses.RoleModuleRightsCollection(), (IEntityRelation)GetRelationsForField("RoleModuleRights")[0], (int)Obymobi.Data.Master.EntityType.ModuleEntity, (int)Obymobi.Data.Master.EntityType.RoleModuleRightsEntity, 0, null, null, null, "RoleModuleRights", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The ModuleId property of the Entity Module<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Module"."ModuleId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 ModuleId
		{
			get { return (System.Int32)GetValue((int)ModuleFieldIndex.ModuleId, true); }
			set	{ SetValue((int)ModuleFieldIndex.ModuleId, value, true); }
		}

		/// <summary> The NameSystem property of the Entity Module<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Module"."NameSystem"<br/>
		/// Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String NameSystem
		{
			get { return (System.String)GetValue((int)ModuleFieldIndex.NameSystem, true); }
			set	{ SetValue((int)ModuleFieldIndex.NameSystem, value, true); }
		}

		/// <summary> The NameFull property of the Entity Module<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Module"."NameFull"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String NameFull
		{
			get { return (System.String)GetValue((int)ModuleFieldIndex.NameFull, true); }
			set	{ SetValue((int)ModuleFieldIndex.NameFull, value, true); }
		}

		/// <summary> The NameShort property of the Entity Module<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Module"."NameShort"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String NameShort
		{
			get { return (System.String)GetValue((int)ModuleFieldIndex.NameShort, true); }
			set	{ SetValue((int)ModuleFieldIndex.NameShort, value, true); }
		}

		/// <summary> The IconPath property of the Entity Module<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Module"."IconPath"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 255<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String IconPath
		{
			get { return (System.String)GetValue((int)ModuleFieldIndex.IconPath, true); }
			set	{ SetValue((int)ModuleFieldIndex.IconPath, value, true); }
		}

		/// <summary> The DefaultUiElementTypeNameFull property of the Entity Module<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Module"."DefaultUiElementTypeNameFull"<br/>
		/// Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 255<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String DefaultUiElementTypeNameFull
		{
			get { return (System.String)GetValue((int)ModuleFieldIndex.DefaultUiElementTypeNameFull, true); }
			set	{ SetValue((int)ModuleFieldIndex.DefaultUiElementTypeNameFull, value, true); }
		}

		/// <summary> The SortOrder property of the Entity Module<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Module"."SortOrder"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> SortOrder
		{
			get { return (Nullable<System.Int32>)GetValue((int)ModuleFieldIndex.SortOrder, false); }
			set	{ SetValue((int)ModuleFieldIndex.SortOrder, value, true); }
		}

		/// <summary> The LicensingFree property of the Entity Module<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Module"."LicensingFree"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean LicensingFree
		{
			get { return (System.Boolean)GetValue((int)ModuleFieldIndex.LicensingFree, true); }
			set	{ SetValue((int)ModuleFieldIndex.LicensingFree, value, true); }
		}

		/// <summary> The UserRightsFree property of the Entity Module<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Module"."UserRightsFree"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean UserRightsFree
		{
			get { return (System.Boolean)GetValue((int)ModuleFieldIndex.UserRightsFree, true); }
			set	{ SetValue((int)ModuleFieldIndex.UserRightsFree, value, true); }
		}

		/// <summary> The ForLocalUseWhileSyncingIsUpdated property of the Entity Module<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Module"."ForLocalUseWhileSyncingIsUpdated"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean ForLocalUseWhileSyncingIsUpdated
		{
			get { return (System.Boolean)GetValue((int)ModuleFieldIndex.ForLocalUseWhileSyncingIsUpdated, true); }
			set	{ SetValue((int)ModuleFieldIndex.ForLocalUseWhileSyncingIsUpdated, value, true); }
		}

		/// <summary> The Archived property of the Entity Module<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Module"."Archived"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean Archived
		{
			get { return (System.Boolean)GetValue((int)ModuleFieldIndex.Archived, true); }
			set	{ SetValue((int)ModuleFieldIndex.Archived, value, true); }
		}

		/// <summary> The Created property of the Entity Module<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Module"."Created"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> Created
		{
			get { return (Nullable<System.DateTime>)GetValue((int)ModuleFieldIndex.Created, false); }
			set	{ SetValue((int)ModuleFieldIndex.Created, value, true); }
		}

		/// <summary> The CreatedBy property of the Entity Module<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Module"."CreatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CreatedBy
		{
			get { return (System.Int32)GetValue((int)ModuleFieldIndex.CreatedBy, true); }
			set	{ SetValue((int)ModuleFieldIndex.CreatedBy, value, true); }
		}

		/// <summary> The Updated property of the Entity Module<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Module"."Updated"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> Updated
		{
			get { return (Nullable<System.DateTime>)GetValue((int)ModuleFieldIndex.Updated, false); }
			set	{ SetValue((int)ModuleFieldIndex.Updated, value, true); }
		}

		/// <summary> The UpdatedBy property of the Entity Module<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Module"."UpdatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 UpdatedBy
		{
			get { return (System.Int32)GetValue((int)ModuleFieldIndex.UpdatedBy, true); }
			set	{ SetValue((int)ModuleFieldIndex.UpdatedBy, value, true); }
		}

		/// <summary> The Deleted property of the Entity Module<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Module"."Deleted"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean Deleted
		{
			get { return (System.Boolean)GetValue((int)ModuleFieldIndex.Deleted, true); }
			set	{ SetValue((int)ModuleFieldIndex.Deleted, value, true); }
		}

		/// <summary> The CreatedUTC property of the Entity Module<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Module"."CreatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CreatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)ModuleFieldIndex.CreatedUTC, false); }
			set	{ SetValue((int)ModuleFieldIndex.CreatedUTC, value, true); }
		}

		/// <summary> The UpdatedUTC property of the Entity Module<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Module"."UpdatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)ModuleFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)ModuleFieldIndex.UpdatedUTC, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'RoleModuleRightsEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiRoleModuleRights()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.Master.CollectionClasses.RoleModuleRightsCollection RoleModuleRights
		{
			get	{ return GetMultiRoleModuleRights(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for RoleModuleRights. When set to true, RoleModuleRights is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time RoleModuleRights is accessed. You can always execute/ a forced fetch by calling GetMultiRoleModuleRights(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchRoleModuleRights
		{
			get	{ return _alwaysFetchRoleModuleRights; }
			set	{ _alwaysFetchRoleModuleRights = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property RoleModuleRights already has been fetched. Setting this property to false when RoleModuleRights has been fetched
		/// will clear the RoleModuleRights collection well. Setting this property to true while RoleModuleRights hasn't been fetched disables lazy loading for RoleModuleRights</summary>
		[Browsable(false)]
		public bool AlreadyFetchedRoleModuleRights
		{
			get { return _alreadyFetchedRoleModuleRights;}
			set 
			{
				if(_alreadyFetchedRoleModuleRights && !value && (_roleModuleRights != null))
				{
					_roleModuleRights.Clear();
				}
				_alreadyFetchedRoleModuleRights = value;
			}
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the Obymobi.Data.Master.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)Obymobi.Data.Master.EntityType.ModuleEntity; }
		}

		#endregion

		
		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
