﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
using System.Runtime.Serialization;
using System.Data;
using System.Xml.Serialization;
using Obymobi.Data.Master;
using Obymobi.Data.Master.FactoryClasses;
using Obymobi.Data.Master.DaoClasses;
using Obymobi.Data.Master.RelationClasses;
using Obymobi.Data.Master.HelperClasses;
using Obymobi.Data.Master.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.Master.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity base class which represents the base class for the entity 'EntityInformation'.<br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public abstract partial class EntityInformationEntityBase : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		public string LLBLGenProEntityName {
			get { return "EntityInformationEntity"; }
		}
	
		#region Class Member Declarations
		private Obymobi.Data.Master.CollectionClasses.RoleEntityRightsCollection	_roleEntityRights;
		private bool	_alwaysFetchRoleEntityRights, _alreadyFetchedRoleEntityRights;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name RoleEntityRights</summary>
			public static readonly string RoleEntityRights = "RoleEntityRights";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static EntityInformationEntityBase()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		protected EntityInformationEntityBase() :base("EntityInformationEntity")
		{
			InitClassEmpty(null);
		}

		/// <summary>CTor</summary>
		/// <param name="entityInformationId">PK value for EntityInformation which data should be fetched into this EntityInformation object</param>
		protected EntityInformationEntityBase(System.Int32 entityInformationId):base("EntityInformationEntity")
		{
			InitClassFetch(entityInformationId, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="entityInformationId">PK value for EntityInformation which data should be fetched into this EntityInformation object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		protected EntityInformationEntityBase(System.Int32 entityInformationId, IPrefetchPath prefetchPathToUse): base("EntityInformationEntity")
		{
			InitClassFetch(entityInformationId, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="entityInformationId">PK value for EntityInformation which data should be fetched into this EntityInformation object</param>
		/// <param name="validator">The custom validator object for this EntityInformationEntity</param>
		protected EntityInformationEntityBase(System.Int32 entityInformationId, IValidator validator):base("EntityInformationEntity")
		{
			InitClassFetch(entityInformationId, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected EntityInformationEntityBase(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_roleEntityRights = (Obymobi.Data.Master.CollectionClasses.RoleEntityRightsCollection)info.GetValue("_roleEntityRights", typeof(Obymobi.Data.Master.CollectionClasses.RoleEntityRightsCollection));
			_alwaysFetchRoleEntityRights = info.GetBoolean("_alwaysFetchRoleEntityRights");
			_alreadyFetchedRoleEntityRights = info.GetBoolean("_alreadyFetchedRoleEntityRights");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}	

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedRoleEntityRights = (_roleEntityRights.Count > 0);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "RoleEntityRights":
					toReturn.Add(Relations.RoleEntityRightsEntityUsingEntityInformationId);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_roleEntityRights", (!this.MarkedForDeletion?_roleEntityRights:null));
			info.AddValue("_alwaysFetchRoleEntityRights", _alwaysFetchRoleEntityRights);
			info.AddValue("_alreadyFetchedRoleEntityRights", _alreadyFetchedRoleEntityRights);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "RoleEntityRights":
					_alreadyFetchedRoleEntityRights = true;
					if(entity!=null)
					{
						this.RoleEntityRights.Add((RoleEntityRightsEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "RoleEntityRights":
					_roleEntityRights.Add((RoleEntityRightsEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "RoleEntityRights":
					this.PerformRelatedEntityRemoval(_roleEntityRights, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_roleEntityRights);

			return toReturn;
		}

		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="entityName">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCEntityName(System.String entityName)
		{
			return FetchUsingUCEntityName( entityName, null, null, null);
		}

		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="entityName">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCEntityName(System.String entityName, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingUCEntityName( entityName, prefetchPathToUse, null, null);
		}
	
		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="entityName">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCEntityName(System.String entityName, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingUCEntityName( entityName, prefetchPathToUse, contextToUse, null);
		}
	
		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="entityName">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCEntityName(System.String entityName, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				((EntityInformationDAO)CreateDAOInstance()).FetchEntityInformationUsingUCEntityName(this, this.Transaction, entityName, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="entityInformationId">PK value for EntityInformation which data should be fetched into this EntityInformation object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 entityInformationId)
		{
			return FetchUsingPK(entityInformationId, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="entityInformationId">PK value for EntityInformation which data should be fetched into this EntityInformation object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 entityInformationId, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(entityInformationId, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="entityInformationId">PK value for EntityInformation which data should be fetched into this EntityInformation object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 entityInformationId, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(entityInformationId, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="entityInformationId">PK value for EntityInformation which data should be fetched into this EntityInformation object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 entityInformationId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(entityInformationId, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.EntityInformationId, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new EntityInformationRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'RoleEntityRightsEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'RoleEntityRightsEntity'</returns>
		public Obymobi.Data.Master.CollectionClasses.RoleEntityRightsCollection GetMultiRoleEntityRights(bool forceFetch)
		{
			return GetMultiRoleEntityRights(forceFetch, _roleEntityRights.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'RoleEntityRightsEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'RoleEntityRightsEntity'</returns>
		public Obymobi.Data.Master.CollectionClasses.RoleEntityRightsCollection GetMultiRoleEntityRights(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiRoleEntityRights(forceFetch, _roleEntityRights.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'RoleEntityRightsEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.Master.CollectionClasses.RoleEntityRightsCollection GetMultiRoleEntityRights(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiRoleEntityRights(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'RoleEntityRightsEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.Master.CollectionClasses.RoleEntityRightsCollection GetMultiRoleEntityRights(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedRoleEntityRights || forceFetch || _alwaysFetchRoleEntityRights) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_roleEntityRights);
				_roleEntityRights.SuppressClearInGetMulti=!forceFetch;
				_roleEntityRights.EntityFactoryToUse = entityFactoryToUse;
				_roleEntityRights.GetMultiManyToOne(this, null, filter);
				_roleEntityRights.SuppressClearInGetMulti=false;
				_alreadyFetchedRoleEntityRights = true;
			}
			return _roleEntityRights;
		}

		/// <summary> Sets the collection parameters for the collection for 'RoleEntityRights'. These settings will be taken into account
		/// when the property RoleEntityRights is requested or GetMultiRoleEntityRights is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersRoleEntityRights(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_roleEntityRights.SortClauses=sortClauses;
			_roleEntityRights.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("RoleEntityRights", _roleEntityRights);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="entityInformationId">PK value for EntityInformation which data should be fetched into this EntityInformation object</param>
		/// <param name="validator">The validator object for this EntityInformationEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int32 entityInformationId, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(entityInformationId, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_roleEntityRights = new Obymobi.Data.Master.CollectionClasses.RoleEntityRightsCollection();
			_roleEntityRights.SetContainingEntityInfo(this, "EntityInformation");
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("EntityInformationId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("EntityName", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ShowFieldName", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ShowFieldNameBreadCrumb", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FriendlyName", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FriendlyNamePlural", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("HelpText", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TitleAsBreadCrumbHierarchy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DefaultEntityEditPage", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DefaultEntityCollectionPage", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdateWmsCacheDateOnChange", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdateCompanyDataVersionOnChange", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdateMenuDataVersionOnChange", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatePosIntegrationInformationOnChange", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ForLocalUseWhileSyncingIsUpdated", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Archived", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Created", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Updated", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Deleted", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdateDeliverypointDataVersionOnChange", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdateSurveyDataVersionOnChange", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdateAnnouncementDataVersionOnChange", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdateEntertainmentDataVersionOnChange", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdateAdvertisementDataVersionOnChange", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedUTC", fieldHashtable);
		}
		#endregion

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="entityInformationId">PK value for EntityInformation which data should be fetched into this EntityInformation object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int32 entityInformationId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)EntityInformationFieldIndex.EntityInformationId].ForcedCurrentValueWrite(entityInformationId);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateEntityInformationDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new EntityInformationEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static EntityInformationRelations Relations
		{
			get	{ return new EntityInformationRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'RoleEntityRights' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathRoleEntityRights
		{
			get { return new PrefetchPathElement(new Obymobi.Data.Master.CollectionClasses.RoleEntityRightsCollection(), (IEntityRelation)GetRelationsForField("RoleEntityRights")[0], (int)Obymobi.Data.Master.EntityType.EntityInformationEntity, (int)Obymobi.Data.Master.EntityType.RoleEntityRightsEntity, 0, null, null, null, "RoleEntityRights", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The EntityInformationId property of the Entity EntityInformation<br/><br/></summary>
		/// <remarks>Mapped on  table field: "EntityInformation"."EntityInformationId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 EntityInformationId
		{
			get { return (System.Int32)GetValue((int)EntityInformationFieldIndex.EntityInformationId, true); }
			set	{ SetValue((int)EntityInformationFieldIndex.EntityInformationId, value, true); }
		}

		/// <summary> The EntityName property of the Entity EntityInformation<br/><br/></summary>
		/// <remarks>Mapped on  table field: "EntityInformation"."EntityName"<br/>
		/// Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 250<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String EntityName
		{
			get { return (System.String)GetValue((int)EntityInformationFieldIndex.EntityName, true); }
			set	{ SetValue((int)EntityInformationFieldIndex.EntityName, value, true); }
		}

		/// <summary> The ShowFieldName property of the Entity EntityInformation<br/><br/></summary>
		/// <remarks>Mapped on  table field: "EntityInformation"."ShowFieldName"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 250<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ShowFieldName
		{
			get { return (System.String)GetValue((int)EntityInformationFieldIndex.ShowFieldName, true); }
			set	{ SetValue((int)EntityInformationFieldIndex.ShowFieldName, value, true); }
		}

		/// <summary> The ShowFieldNameBreadCrumb property of the Entity EntityInformation<br/><br/></summary>
		/// <remarks>Mapped on  table field: "EntityInformation"."ShowFieldNameBreadCrumb"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 250<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ShowFieldNameBreadCrumb
		{
			get { return (System.String)GetValue((int)EntityInformationFieldIndex.ShowFieldNameBreadCrumb, true); }
			set	{ SetValue((int)EntityInformationFieldIndex.ShowFieldNameBreadCrumb, value, true); }
		}

		/// <summary> The FriendlyName property of the Entity EntityInformation<br/><br/></summary>
		/// <remarks>Mapped on  table field: "EntityInformation"."FriendlyName"<br/>
		/// Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 150<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FriendlyName
		{
			get { return (System.String)GetValue((int)EntityInformationFieldIndex.FriendlyName, true); }
			set	{ SetValue((int)EntityInformationFieldIndex.FriendlyName, value, true); }
		}

		/// <summary> The FriendlyNamePlural property of the Entity EntityInformation<br/><br/></summary>
		/// <remarks>Mapped on  table field: "EntityInformation"."FriendlyNamePlural"<br/>
		/// Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 150<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FriendlyNamePlural
		{
			get { return (System.String)GetValue((int)EntityInformationFieldIndex.FriendlyNamePlural, true); }
			set	{ SetValue((int)EntityInformationFieldIndex.FriendlyNamePlural, value, true); }
		}

		/// <summary> The HelpText property of the Entity EntityInformation<br/><br/></summary>
		/// <remarks>Mapped on  table field: "EntityInformation"."HelpText"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String HelpText
		{
			get { return (System.String)GetValue((int)EntityInformationFieldIndex.HelpText, true); }
			set	{ SetValue((int)EntityInformationFieldIndex.HelpText, value, true); }
		}

		/// <summary> The TitleAsBreadCrumbHierarchy property of the Entity EntityInformation<br/><br/></summary>
		/// <remarks>Mapped on  table field: "EntityInformation"."TitleAsBreadCrumbHierarchy"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String TitleAsBreadCrumbHierarchy
		{
			get { return (System.String)GetValue((int)EntityInformationFieldIndex.TitleAsBreadCrumbHierarchy, true); }
			set	{ SetValue((int)EntityInformationFieldIndex.TitleAsBreadCrumbHierarchy, value, true); }
		}

		/// <summary> The DefaultEntityEditPage property of the Entity EntityInformation<br/><br/></summary>
		/// <remarks>Mapped on  table field: "EntityInformation"."DefaultEntityEditPage"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String DefaultEntityEditPage
		{
			get { return (System.String)GetValue((int)EntityInformationFieldIndex.DefaultEntityEditPage, true); }
			set	{ SetValue((int)EntityInformationFieldIndex.DefaultEntityEditPage, value, true); }
		}

		/// <summary> The DefaultEntityCollectionPage property of the Entity EntityInformation<br/><br/></summary>
		/// <remarks>Mapped on  table field: "EntityInformation"."DefaultEntityCollectionPage"<br/>
		/// Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String DefaultEntityCollectionPage
		{
			get { return (System.String)GetValue((int)EntityInformationFieldIndex.DefaultEntityCollectionPage, true); }
			set	{ SetValue((int)EntityInformationFieldIndex.DefaultEntityCollectionPage, value, true); }
		}

		/// <summary> The UpdateWmsCacheDateOnChange property of the Entity EntityInformation<br/><br/></summary>
		/// <remarks>Mapped on  table field: "EntityInformation"."UpdateWmsCacheDateOnChange"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean UpdateWmsCacheDateOnChange
		{
			get { return (System.Boolean)GetValue((int)EntityInformationFieldIndex.UpdateWmsCacheDateOnChange, true); }
			set	{ SetValue((int)EntityInformationFieldIndex.UpdateWmsCacheDateOnChange, value, true); }
		}

		/// <summary> The UpdateCompanyDataVersionOnChange property of the Entity EntityInformation<br/><br/></summary>
		/// <remarks>Mapped on  table field: "EntityInformation"."UpdateCompanyDataVersionOnChange"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean UpdateCompanyDataVersionOnChange
		{
			get { return (System.Boolean)GetValue((int)EntityInformationFieldIndex.UpdateCompanyDataVersionOnChange, true); }
			set	{ SetValue((int)EntityInformationFieldIndex.UpdateCompanyDataVersionOnChange, value, true); }
		}

		/// <summary> The UpdateMenuDataVersionOnChange property of the Entity EntityInformation<br/><br/></summary>
		/// <remarks>Mapped on  table field: "EntityInformation"."UpdateMenuDataVersionOnChange"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean UpdateMenuDataVersionOnChange
		{
			get { return (System.Boolean)GetValue((int)EntityInformationFieldIndex.UpdateMenuDataVersionOnChange, true); }
			set	{ SetValue((int)EntityInformationFieldIndex.UpdateMenuDataVersionOnChange, value, true); }
		}

		/// <summary> The UpdatePosIntegrationInformationOnChange property of the Entity EntityInformation<br/><br/></summary>
		/// <remarks>Mapped on  table field: "EntityInformation"."UpdatePosIntegrationInformationOnChange"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean UpdatePosIntegrationInformationOnChange
		{
			get { return (System.Boolean)GetValue((int)EntityInformationFieldIndex.UpdatePosIntegrationInformationOnChange, true); }
			set	{ SetValue((int)EntityInformationFieldIndex.UpdatePosIntegrationInformationOnChange, value, true); }
		}

		/// <summary> The ForLocalUseWhileSyncingIsUpdated property of the Entity EntityInformation<br/><br/></summary>
		/// <remarks>Mapped on  table field: "EntityInformation"."ForLocalUseWhileSyncingIsUpdated"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean ForLocalUseWhileSyncingIsUpdated
		{
			get { return (System.Boolean)GetValue((int)EntityInformationFieldIndex.ForLocalUseWhileSyncingIsUpdated, true); }
			set	{ SetValue((int)EntityInformationFieldIndex.ForLocalUseWhileSyncingIsUpdated, value, true); }
		}

		/// <summary> The Archived property of the Entity EntityInformation<br/><br/></summary>
		/// <remarks>Mapped on  table field: "EntityInformation"."Archived"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean Archived
		{
			get { return (System.Boolean)GetValue((int)EntityInformationFieldIndex.Archived, true); }
			set	{ SetValue((int)EntityInformationFieldIndex.Archived, value, true); }
		}

		/// <summary> The Created property of the Entity EntityInformation<br/><br/></summary>
		/// <remarks>Mapped on  table field: "EntityInformation"."Created"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> Created
		{
			get { return (Nullable<System.DateTime>)GetValue((int)EntityInformationFieldIndex.Created, false); }
			set	{ SetValue((int)EntityInformationFieldIndex.Created, value, true); }
		}

		/// <summary> The CreatedBy property of the Entity EntityInformation<br/><br/></summary>
		/// <remarks>Mapped on  table field: "EntityInformation"."CreatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CreatedBy
		{
			get { return (System.Int32)GetValue((int)EntityInformationFieldIndex.CreatedBy, true); }
			set	{ SetValue((int)EntityInformationFieldIndex.CreatedBy, value, true); }
		}

		/// <summary> The Updated property of the Entity EntityInformation<br/><br/></summary>
		/// <remarks>Mapped on  table field: "EntityInformation"."Updated"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> Updated
		{
			get { return (Nullable<System.DateTime>)GetValue((int)EntityInformationFieldIndex.Updated, false); }
			set	{ SetValue((int)EntityInformationFieldIndex.Updated, value, true); }
		}

		/// <summary> The UpdatedBy property of the Entity EntityInformation<br/><br/></summary>
		/// <remarks>Mapped on  table field: "EntityInformation"."UpdatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 UpdatedBy
		{
			get { return (System.Int32)GetValue((int)EntityInformationFieldIndex.UpdatedBy, true); }
			set	{ SetValue((int)EntityInformationFieldIndex.UpdatedBy, value, true); }
		}

		/// <summary> The Deleted property of the Entity EntityInformation<br/><br/></summary>
		/// <remarks>Mapped on  table field: "EntityInformation"."Deleted"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean Deleted
		{
			get { return (System.Boolean)GetValue((int)EntityInformationFieldIndex.Deleted, true); }
			set	{ SetValue((int)EntityInformationFieldIndex.Deleted, value, true); }
		}

		/// <summary> The UpdateDeliverypointDataVersionOnChange property of the Entity EntityInformation<br/><br/></summary>
		/// <remarks>Mapped on  table field: "EntityInformation"."UpdateDeliverypointDataVersionOnChange"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean UpdateDeliverypointDataVersionOnChange
		{
			get { return (System.Boolean)GetValue((int)EntityInformationFieldIndex.UpdateDeliverypointDataVersionOnChange, true); }
			set	{ SetValue((int)EntityInformationFieldIndex.UpdateDeliverypointDataVersionOnChange, value, true); }
		}

		/// <summary> The UpdateSurveyDataVersionOnChange property of the Entity EntityInformation<br/><br/></summary>
		/// <remarks>Mapped on  table field: "EntityInformation"."UpdateSurveyDataVersionOnChange"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean UpdateSurveyDataVersionOnChange
		{
			get { return (System.Boolean)GetValue((int)EntityInformationFieldIndex.UpdateSurveyDataVersionOnChange, true); }
			set	{ SetValue((int)EntityInformationFieldIndex.UpdateSurveyDataVersionOnChange, value, true); }
		}

		/// <summary> The UpdateAnnouncementDataVersionOnChange property of the Entity EntityInformation<br/><br/></summary>
		/// <remarks>Mapped on  table field: "EntityInformation"."UpdateAnnouncementDataVersionOnChange"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean UpdateAnnouncementDataVersionOnChange
		{
			get { return (System.Boolean)GetValue((int)EntityInformationFieldIndex.UpdateAnnouncementDataVersionOnChange, true); }
			set	{ SetValue((int)EntityInformationFieldIndex.UpdateAnnouncementDataVersionOnChange, value, true); }
		}

		/// <summary> The UpdateEntertainmentDataVersionOnChange property of the Entity EntityInformation<br/><br/></summary>
		/// <remarks>Mapped on  table field: "EntityInformation"."UpdateEntertainmentDataVersionOnChange"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean UpdateEntertainmentDataVersionOnChange
		{
			get { return (System.Boolean)GetValue((int)EntityInformationFieldIndex.UpdateEntertainmentDataVersionOnChange, true); }
			set	{ SetValue((int)EntityInformationFieldIndex.UpdateEntertainmentDataVersionOnChange, value, true); }
		}

		/// <summary> The UpdateAdvertisementDataVersionOnChange property of the Entity EntityInformation<br/><br/></summary>
		/// <remarks>Mapped on  table field: "EntityInformation"."UpdateAdvertisementDataVersionOnChange"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean UpdateAdvertisementDataVersionOnChange
		{
			get { return (System.Boolean)GetValue((int)EntityInformationFieldIndex.UpdateAdvertisementDataVersionOnChange, true); }
			set	{ SetValue((int)EntityInformationFieldIndex.UpdateAdvertisementDataVersionOnChange, value, true); }
		}

		/// <summary> The CreatedUTC property of the Entity EntityInformation<br/><br/></summary>
		/// <remarks>Mapped on  table field: "EntityInformation"."CreatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CreatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)EntityInformationFieldIndex.CreatedUTC, false); }
			set	{ SetValue((int)EntityInformationFieldIndex.CreatedUTC, value, true); }
		}

		/// <summary> The UpdatedUTC property of the Entity EntityInformation<br/><br/></summary>
		/// <remarks>Mapped on  table field: "EntityInformation"."UpdatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)EntityInformationFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)EntityInformationFieldIndex.UpdatedUTC, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'RoleEntityRightsEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiRoleEntityRights()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.Master.CollectionClasses.RoleEntityRightsCollection RoleEntityRights
		{
			get	{ return GetMultiRoleEntityRights(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for RoleEntityRights. When set to true, RoleEntityRights is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time RoleEntityRights is accessed. You can always execute/ a forced fetch by calling GetMultiRoleEntityRights(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchRoleEntityRights
		{
			get	{ return _alwaysFetchRoleEntityRights; }
			set	{ _alwaysFetchRoleEntityRights = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property RoleEntityRights already has been fetched. Setting this property to false when RoleEntityRights has been fetched
		/// will clear the RoleEntityRights collection well. Setting this property to true while RoleEntityRights hasn't been fetched disables lazy loading for RoleEntityRights</summary>
		[Browsable(false)]
		public bool AlreadyFetchedRoleEntityRights
		{
			get { return _alreadyFetchedRoleEntityRights;}
			set 
			{
				if(_alreadyFetchedRoleEntityRights && !value && (_roleEntityRights != null))
				{
					_roleEntityRights.Clear();
				}
				_alreadyFetchedRoleEntityRights = value;
			}
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the Obymobi.Data.Master.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)Obymobi.Data.Master.EntityType.EntityInformationEntity; }
		}

		#endregion

		
		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
