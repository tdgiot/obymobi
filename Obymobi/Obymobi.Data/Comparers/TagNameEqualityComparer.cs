﻿using System.Collections.Generic;
using Obymobi.Interfaces;

namespace Obymobi.Data.Comparers
{
    public class TagIdEqualityComparer : EqualityComparer<ITag>
    {
        public override bool Equals(ITag x, ITag y)
        {
            return x.TagId.Equals(y?.TagId);
        }
 
        public override int GetHashCode(ITag obj) => obj.TagId.GetHashCode();
    }
}
