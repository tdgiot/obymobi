﻿using System.Collections.Generic;
using Obymobi.Interfaces;

namespace Obymobi.Data.Comparers
{
    public class TagNameEqualityComparer : EqualityComparer<ITag>
    {
        public override bool Equals(ITag x, ITag y)
        {
            return x.Name.Equals(y?.Name);
        }
 
        public override int GetHashCode(ITag obj) => obj.Name.GetHashCode();
    }
}
