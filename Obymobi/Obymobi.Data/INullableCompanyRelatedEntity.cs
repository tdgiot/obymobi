﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.Data
{
    public interface INullableCompanyRelatedEntity
    {
        int? CompanyId { get; }
    }
}
