﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.HelperClasses
{
	/// <summary>Singleton implementation of the PersistenceInfoProvider. This class is the singleton wrapper through which the actual instance is retrieved.</summary>
	/// <remarks>It uses a single instance of an internal class. The access isn't marked with locks as the PersistenceInfoProviderBase class is threadsafe.</remarks>
	internal static class PersistenceInfoProviderSingleton
	{
		#region Class Member Declarations
		private static readonly IPersistenceInfoProvider _providerInstance = new PersistenceInfoProviderCore();
		#endregion

		/// <summary>Dummy static constructor to make sure threadsafe initialization is performed.</summary>
		static PersistenceInfoProviderSingleton()
		{
		}

		/// <summary>Gets the singleton instance of the PersistenceInfoProviderCore</summary>
		/// <returns>Instance of the PersistenceInfoProvider.</returns>
		public static IPersistenceInfoProvider GetInstance()
		{
			return _providerInstance;
		}
	}

	/// <summary>Actual implementation of the PersistenceInfoProvider. Used by singleton wrapper.</summary>
	internal class PersistenceInfoProviderCore : PersistenceInfoProviderBase
	{
		/// <summary>Initializes a new instance of the <see cref="PersistenceInfoProviderCore"/> class.</summary>
		internal PersistenceInfoProviderCore()
		{
			Init();
		}

		/// <summary>Method which initializes the internal datastores with the structure of hierarchical types.</summary>
		private void Init()
		{
			this.InitClass(349);
			InitAccessCodeEntityMappings();
			InitAccessCodeCompanyEntityMappings();
			InitAccessCodePointOfInterestEntityMappings();
			InitAccountEntityMappings();
			InitAccountCompanyEntityMappings();
			InitActionButtonEntityMappings();
			InitActionButtonLanguageEntityMappings();
			InitAddressEntityMappings();
			InitAdvertisementEntityMappings();
			InitAdvertisementConfigurationEntityMappings();
			InitAdvertisementConfigurationAdvertisementEntityMappings();
			InitAdvertisementLanguageEntityMappings();
			InitAdvertisementTagEntityMappings();
			InitAdvertisementTagAdvertisementEntityMappings();
			InitAdvertisementTagCategoryEntityMappings();
			InitAdvertisementTagEntertainmentEntityMappings();
			InitAdvertisementTagGenericproductEntityMappings();
			InitAdvertisementTagLanguageEntityMappings();
			InitAdvertisementTagProductEntityMappings();
			InitAdyenPaymentMethodEntityMappings();
			InitAdyenPaymentMethodBrandEntityMappings();
			InitAffiliateCampaignEntityMappings();
			InitAffiliateCampaignAffiliatePartnerEntityMappings();
			InitAffiliatePartnerEntityMappings();
			InitAlterationEntityMappings();
			InitAlterationitemEntityMappings();
			InitAlterationitemAlterationEntityMappings();
			InitAlterationLanguageEntityMappings();
			InitAlterationoptionEntityMappings();
			InitAlterationoptionLanguageEntityMappings();
			InitAlterationoptionTagEntityMappings();
			InitAlterationProductEntityMappings();
			InitAmenityEntityMappings();
			InitAmenityLanguageEntityMappings();
			InitAnalyticsProcessingTaskEntityMappings();
			InitAnnouncementEntityMappings();
			InitAnnouncementLanguageEntityMappings();
			InitApiAuthenticationEntityMappings();
			InitApplicationEntityMappings();
			InitAttachmentEntityMappings();
			InitAttachmentLanguageEntityMappings();
			InitAuditlogEntityMappings();
			InitAvailabilityEntityMappings();
			InitAzureNotificationHubEntityMappings();
			InitBrandEntityMappings();
			InitBrandCultureEntityMappings();
			InitBusinesshoursEntityMappings();
			InitBusinesshoursexceptionEntityMappings();
			InitCategoryEntityMappings();
			InitCategoryAlterationEntityMappings();
			InitCategoryLanguageEntityMappings();
			InitCategorySuggestionEntityMappings();
			InitCategoryTagEntityMappings();
			InitCheckoutMethodEntityMappings();
			InitCheckoutMethodDeliverypointgroupEntityMappings();
			InitClientEntityMappings();
			InitClientConfigurationEntityMappings();
			InitClientConfigurationRouteEntityMappings();
			InitClientEntertainmentEntityMappings();
			InitClientLogEntityMappings();
			InitClientLogFileEntityMappings();
			InitClientStateEntityMappings();
			InitCloudApplicationVersionEntityMappings();
			InitCloudProcessingTaskEntityMappings();
			InitCloudStorageAccountEntityMappings();
			InitCompanyEntityMappings();
			InitCompanyAmenityEntityMappings();
			InitCompanyBrandEntityMappings();
			InitCompanyCultureEntityMappings();
			InitCompanyCurrencyEntityMappings();
			InitCompanyEntertainmentEntityMappings();
			InitCompanygroupEntityMappings();
			InitCompanyLanguageEntityMappings();
			InitCompanyManagementTaskEntityMappings();
			InitCompanyOwnerEntityMappings();
			InitCompanyReleaseEntityMappings();
			InitCompanyVenueCategoryEntityMappings();
			InitConfigurationEntityMappings();
			InitCountryEntityMappings();
			InitCurrencyEntityMappings();
			InitCustomerEntityMappings();
			InitCustomTextEntityMappings();
			InitDeliveryDistanceEntityMappings();
			InitDeliveryInformationEntityMappings();
			InitDeliverypointEntityMappings();
			InitDeliverypointExternalDeliverypointEntityMappings();
			InitDeliverypointgroupEntityMappings();
			InitDeliverypointgroupAdvertisementEntityMappings();
			InitDeliverypointgroupAnnouncementEntityMappings();
			InitDeliverypointgroupEntertainmentEntityMappings();
			InitDeliverypointgroupLanguageEntityMappings();
			InitDeliverypointgroupOccupancyEntityMappings();
			InitDeliverypointgroupProductEntityMappings();
			InitDeviceEntityMappings();
			InitDevicemediaEntityMappings();
			InitDeviceTokenHistoryEntityMappings();
			InitDeviceTokenTaskEntityMappings();
			InitEntertainmentEntityMappings();
			InitEntertainmentcategoryEntityMappings();
			InitEntertainmentcategoryLanguageEntityMappings();
			InitEntertainmentConfigurationEntityMappings();
			InitEntertainmentConfigurationEntertainmentEntityMappings();
			InitEntertainmentDependencyEntityMappings();
			InitEntertainmentFileEntityMappings();
			InitEntertainmenturlEntityMappings();
			InitEntityFieldInformationEntityMappings();
			InitEntityFieldInformationCustomEntityMappings();
			InitEntityInformationEntityMappings();
			InitEntityInformationCustomEntityMappings();
			InitExternalDeliverypointEntityMappings();
			InitExternalMenuEntityMappings();
			InitExternalProductEntityMappings();
			InitExternalSubProductEntityMappings();
			InitExternalSystemEntityMappings();
			InitExternalSystemLogEntityMappings();
			InitFeatureToggleAvailabilityEntityMappings();
			InitFolioEntityMappings();
			InitFolioItemEntityMappings();
			InitGameEntityMappings();
			InitGameSessionEntityMappings();
			InitGameSessionReportEntityMappings();
			InitGameSessionReportConfigurationEntityMappings();
			InitGameSessionReportItemEntityMappings();
			InitGenericalterationEntityMappings();
			InitGenericalterationitemEntityMappings();
			InitGenericalterationoptionEntityMappings();
			InitGenericcategoryEntityMappings();
			InitGenericcategoryLanguageEntityMappings();
			InitGenericproductEntityMappings();
			InitGenericproductGenericalterationEntityMappings();
			InitGenericproductLanguageEntityMappings();
			InitGuestInformationEntityMappings();
			InitIcrtouchprintermappingEntityMappings();
			InitIcrtouchprintermappingDeliverypointEntityMappings();
			InitIncomingSmsEntityMappings();
			InitInfraredCommandEntityMappings();
			InitInfraredConfigurationEntityMappings();
			InitLanguageEntityMappings();
			InitLicensedModuleEntityMappings();
			InitLicensedUIElementEntityMappings();
			InitLicensedUIElementSubPanelEntityMappings();
			InitMapEntityMappings();
			InitMapPointOfInterestEntityMappings();
			InitMediaEntityMappings();
			InitMediaCultureEntityMappings();
			InitMediaLanguageEntityMappings();
			InitMediaProcessingTaskEntityMappings();
			InitMediaRatioTypeMediaEntityMappings();
			InitMediaRatioTypeMediaFileEntityMappings();
			InitMediaRelationshipEntityMappings();
			InitMenuEntityMappings();
			InitMessageEntityMappings();
			InitMessagegroupEntityMappings();
			InitMessagegroupDeliverypointEntityMappings();
			InitMessageRecipientEntityMappings();
			InitMessageTemplateEntityMappings();
			InitMessageTemplateCategoryEntityMappings();
			InitMessageTemplateCategoryMessageTemplateEntityMappings();
			InitModuleEntityMappings();
			InitNetmessageEntityMappings();
			InitOptInEntityMappings();
			InitOrderEntityMappings();
			InitOrderHourEntityMappings();
			InitOrderitemEntityMappings();
			InitOrderitemAlterationitemEntityMappings();
			InitOrderitemAlterationitemTagEntityMappings();
			InitOrderitemTagEntityMappings();
			InitOrderNotificationLogEntityMappings();
			InitOrderRoutestephandlerEntityMappings();
			InitOrderRoutestephandlerHistoryEntityMappings();
			InitOutletEntityMappings();
			InitOutletOperationalStateEntityMappings();
			InitOutletSellerInformationEntityMappings();
			InitPageEntityMappings();
			InitPageElementEntityMappings();
			InitPageLanguageEntityMappings();
			InitPageTemplateEntityMappings();
			InitPageTemplateElementEntityMappings();
			InitPageTemplateLanguageEntityMappings();
			InitPaymentIntegrationConfigurationEntityMappings();
			InitPaymentProviderEntityMappings();
			InitPaymentProviderCompanyEntityMappings();
			InitPaymentTransactionEntityMappings();
			InitPaymentTransactionLogEntityMappings();
			InitPaymentTransactionSplitEntityMappings();
			InitPmsActionRuleEntityMappings();
			InitPmsReportColumnEntityMappings();
			InitPmsReportConfigurationEntityMappings();
			InitPmsRuleEntityMappings();
			InitPointOfInterestEntityMappings();
			InitPointOfInterestAmenityEntityMappings();
			InitPointOfInterestLanguageEntityMappings();
			InitPointOfInterestVenueCategoryEntityMappings();
			InitPosalterationEntityMappings();
			InitPosalterationitemEntityMappings();
			InitPosalterationoptionEntityMappings();
			InitPoscategoryEntityMappings();
			InitPosdeliverypointEntityMappings();
			InitPosdeliverypointgroupEntityMappings();
			InitPospaymentmethodEntityMappings();
			InitPosproductEntityMappings();
			InitPosproductPosalterationEntityMappings();
			InitPriceLevelEntityMappings();
			InitPriceLevelItemEntityMappings();
			InitPriceScheduleEntityMappings();
			InitPriceScheduleItemEntityMappings();
			InitPriceScheduleItemOccurrenceEntityMappings();
			InitProductEntityMappings();
			InitProductAlterationEntityMappings();
			InitProductAttachmentEntityMappings();
			InitProductCategoryEntityMappings();
			InitProductCategorySuggestionEntityMappings();
			InitProductCategoryTagEntityMappings();
			InitProductgroupEntityMappings();
			InitProductgroupItemEntityMappings();
			InitProductLanguageEntityMappings();
			InitProductSuggestionEntityMappings();
			InitProductTagEntityMappings();
			InitPublishingEntityMappings();
			InitPublishingItemEntityMappings();
			InitRatingEntityMappings();
			InitReceiptEntityMappings();
			InitReceiptTemplateEntityMappings();
			InitReferentialConstraintEntityMappings();
			InitReferentialConstraintCustomEntityMappings();
			InitReleaseEntityMappings();
			InitReportProcessingTaskEntityMappings();
			InitReportProcessingTaskFileEntityMappings();
			InitReportProcessingTaskTemplateEntityMappings();
			InitRequestlogEntityMappings();
			InitRoleEntityMappings();
			InitRoleModuleRightsEntityMappings();
			InitRoleUIElementRightsEntityMappings();
			InitRoleUIElementSubPanelRightsEntityMappings();
			InitRoomControlAreaEntityMappings();
			InitRoomControlAreaLanguageEntityMappings();
			InitRoomControlComponentEntityMappings();
			InitRoomControlComponentLanguageEntityMappings();
			InitRoomControlConfigurationEntityMappings();
			InitRoomControlSectionEntityMappings();
			InitRoomControlSectionItemEntityMappings();
			InitRoomControlSectionItemLanguageEntityMappings();
			InitRoomControlSectionLanguageEntityMappings();
			InitRoomControlWidgetEntityMappings();
			InitRoomControlWidgetLanguageEntityMappings();
			InitRouteEntityMappings();
			InitRoutestepEntityMappings();
			InitRoutestephandlerEntityMappings();
			InitScheduleEntityMappings();
			InitScheduledCommandEntityMappings();
			InitScheduledCommandTaskEntityMappings();
			InitScheduledMessageEntityMappings();
			InitScheduledMessageHistoryEntityMappings();
			InitScheduledMessageLanguageEntityMappings();
			InitScheduleitemEntityMappings();
			InitServiceMethodEntityMappings();
			InitServiceMethodDeliverypointgroupEntityMappings();
			InitSetupCodeEntityMappings();
			InitSiteEntityMappings();
			InitSiteCultureEntityMappings();
			InitSiteLanguageEntityMappings();
			InitSiteTemplateEntityMappings();
			InitSiteTemplateCultureEntityMappings();
			InitSiteTemplateLanguageEntityMappings();
			InitSmsInformationEntityMappings();
			InitSmsKeywordEntityMappings();
			InitStationEntityMappings();
			InitStationLanguageEntityMappings();
			InitStationListEntityMappings();
			InitSupplierEntityMappings();
			InitSupportagentEntityMappings();
			InitSupportpoolEntityMappings();
			InitSupportpoolNotificationRecipientEntityMappings();
			InitSupportpoolSupportagentEntityMappings();
			InitSurveyEntityMappings();
			InitSurveyAnswerEntityMappings();
			InitSurveyAnswerLanguageEntityMappings();
			InitSurveyLanguageEntityMappings();
			InitSurveyPageEntityMappings();
			InitSurveyQuestionEntityMappings();
			InitSurveyQuestionLanguageEntityMappings();
			InitSurveyResultEntityMappings();
			InitTagEntityMappings();
			InitTaxTariffEntityMappings();
			InitTerminalEntityMappings();
			InitTerminalConfigurationEntityMappings();
			InitTerminalLogEntityMappings();
			InitTerminalLogFileEntityMappings();
			InitTerminalMessageTemplateCategoryEntityMappings();
			InitTerminalStateEntityMappings();
			InitTimestampEntityMappings();
			InitTimeZoneEntityMappings();
			InitTranslationEntityMappings();
			InitUIElementEntityMappings();
			InitUIElementCustomEntityMappings();
			InitUIElementSubPanelEntityMappings();
			InitUIElementSubPanelCustomEntityMappings();
			InitUIElementSubPanelUIElementEntityMappings();
			InitUIFooterItemEntityMappings();
			InitUIFooterItemLanguageEntityMappings();
			InitUIModeEntityMappings();
			InitUIScheduleEntityMappings();
			InitUIScheduleItemEntityMappings();
			InitUIScheduleItemOccurrenceEntityMappings();
			InitUITabEntityMappings();
			InitUITabLanguageEntityMappings();
			InitUIThemeEntityMappings();
			InitUIThemeColorEntityMappings();
			InitUIThemeTextSizeEntityMappings();
			InitUIWidgetEntityMappings();
			InitUIWidgetAvailabilityEntityMappings();
			InitUIWidgetLanguageEntityMappings();
			InitUIWidgetTimerEntityMappings();
			InitUserEntityMappings();
			InitUserBrandEntityMappings();
			InitUserLogonEntityMappings();
			InitUserRoleEntityMappings();
			InitVattariffEntityMappings();
			InitVendorEntityMappings();
			InitVenueCategoryEntityMappings();
			InitVenueCategoryLanguageEntityMappings();
			InitViewEntityMappings();
			InitViewCustomEntityMappings();
			InitViewItemEntityMappings();
			InitViewItemCustomEntityMappings();
			InitWeatherEntityMappings();
			InitWifiConfigurationEntityMappings();
			InitActionEntityMappings();
			InitApplicationConfigurationEntityMappings();
			InitCarouselItemEntityMappings();
			InitFeatureFlagEntityMappings();
			InitLandingPageEntityMappings();
			InitLandingPageWidgetEntityMappings();
			InitNavigationMenuEntityMappings();
			InitNavigationMenuItemEntityMappings();
			InitNavigationMenuWidgetEntityMappings();
			InitThemeEntityMappings();
			InitWidgetEntityMappings();
			InitWidgetActionBannerEntityMappings();
			InitWidgetActionButtonEntityMappings();
			InitWidgetCarouselEntityMappings();
			InitWidgetGroupEntityMappings();
			InitWidgetGroupWidgetEntityMappings();
			InitWidgetHeroEntityMappings();
			InitWidgetLanguageSwitcherEntityMappings();
			InitWidgetMarkdownEntityMappings();
			InitWidgetOpeningTimeEntityMappings();
			InitWidgetPageTitleEntityMappings();
			InitWidgetWaitTimeEntityMappings();
		}

		/// <summary>Inits AccessCodeEntity's mappings</summary>
		private void InitAccessCodeEntityMappings()
		{
			this.AddElementMapping("AccessCodeEntity", @"Obymobi", @"dbo", "AccessCode", 9, 0);
			this.AddElementFieldMapping("AccessCodeEntity", "AccessCodeId", "AccessCodeId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("AccessCodeEntity", "Code", "Code", false, "VarChar", 50, 0, 0, false, "", null, typeof(System.String), 1);
			this.AddElementFieldMapping("AccessCodeEntity", "Type", "Type", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("AccessCodeEntity", "CreatedBy", "CreatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("AccessCodeEntity", "UpdatedBy", "UpdatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("AccessCodeEntity", "AnalyticsEnabled", "AnalyticsEnabled", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 5);
			this.AddElementFieldMapping("AccessCodeEntity", "LastModifiedUTC", "LastModifiedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 6);
			this.AddElementFieldMapping("AccessCodeEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 7);
			this.AddElementFieldMapping("AccessCodeEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 8);
		}

		/// <summary>Inits AccessCodeCompanyEntity's mappings</summary>
		private void InitAccessCodeCompanyEntityMappings()
		{
			this.AddElementMapping("AccessCodeCompanyEntity", @"Obymobi", @"dbo", "AccessCodeCompany", 8, 0);
			this.AddElementFieldMapping("AccessCodeCompanyEntity", "AccessCodeCompanyId", "AccessCodeCompanyId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("AccessCodeCompanyEntity", "AccessCodeId", "AccessCodeId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("AccessCodeCompanyEntity", "CompanyId", "CompanyId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("AccessCodeCompanyEntity", "CreatedBy", "CreatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("AccessCodeCompanyEntity", "UpdatedBy", "UpdatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("AccessCodeCompanyEntity", "IsHero", "IsHero", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 5);
			this.AddElementFieldMapping("AccessCodeCompanyEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 6);
			this.AddElementFieldMapping("AccessCodeCompanyEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 7);
		}

		/// <summary>Inits AccessCodePointOfInterestEntity's mappings</summary>
		private void InitAccessCodePointOfInterestEntityMappings()
		{
			this.AddElementMapping("AccessCodePointOfInterestEntity", @"Obymobi", @"dbo", "AccessCodePointOfInterest", 8, 0);
			this.AddElementFieldMapping("AccessCodePointOfInterestEntity", "AccessCodePointOfInterestId", "AccessCodePointOfInterestId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("AccessCodePointOfInterestEntity", "AccessCodeId", "AccessCodeId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("AccessCodePointOfInterestEntity", "PointOfInterestId", "PointOfInterestId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("AccessCodePointOfInterestEntity", "CreatedBy", "CreatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("AccessCodePointOfInterestEntity", "UpdatedBy", "UpdatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("AccessCodePointOfInterestEntity", "IsHero", "IsHero", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 5);
			this.AddElementFieldMapping("AccessCodePointOfInterestEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 6);
			this.AddElementFieldMapping("AccessCodePointOfInterestEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 7);
		}

		/// <summary>Inits AccountEntity's mappings</summary>
		private void InitAccountEntityMappings()
		{
			this.AddElementMapping("AccountEntity", @"Obymobi", @"dbo", "Account", 6, 0);
			this.AddElementFieldMapping("AccountEntity", "AccountId", "AccountId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("AccountEntity", "Name", "Name", false, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 1);
			this.AddElementFieldMapping("AccountEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 2);
			this.AddElementFieldMapping("AccountEntity", "CreatedBy", "CreatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("AccountEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 4);
			this.AddElementFieldMapping("AccountEntity", "UpdatedBy", "UpdatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 5);
		}

		/// <summary>Inits AccountCompanyEntity's mappings</summary>
		private void InitAccountCompanyEntityMappings()
		{
			this.AddElementMapping("AccountCompanyEntity", @"Obymobi", @"dbo", "AccountCompany", 7, 0);
			this.AddElementFieldMapping("AccountCompanyEntity", "AccountCompanyId", "AccountCompanyId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("AccountCompanyEntity", "AccountId", "AccountId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("AccountCompanyEntity", "CompanyId", "CompanyId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("AccountCompanyEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 3);
			this.AddElementFieldMapping("AccountCompanyEntity", "CreatedBy", "CreatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("AccountCompanyEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 5);
			this.AddElementFieldMapping("AccountCompanyEntity", "UpdatedBy", "UpdatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 6);
		}

		/// <summary>Inits ActionButtonEntity's mappings</summary>
		private void InitActionButtonEntityMappings()
		{
			this.AddElementMapping("ActionButtonEntity", @"Obymobi", @"dbo", "ActionButton", 7, 0);
			this.AddElementFieldMapping("ActionButtonEntity", "ActionButtonId", "ActionButtonId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("ActionButtonEntity", "Name", "Name", false, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 1);
			this.AddElementFieldMapping("ActionButtonEntity", "CreatedBy", "CreatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("ActionButtonEntity", "UpdatedBy", "UpdatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("ActionButtonEntity", "LastModifiedUTC", "LastModifiedUTC", false, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 4);
			this.AddElementFieldMapping("ActionButtonEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 5);
			this.AddElementFieldMapping("ActionButtonEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 6);
		}

		/// <summary>Inits ActionButtonLanguageEntity's mappings</summary>
		private void InitActionButtonLanguageEntityMappings()
		{
			this.AddElementMapping("ActionButtonLanguageEntity", @"Obymobi", @"dbo", "ActionButtonLanguage", 8, 0);
			this.AddElementFieldMapping("ActionButtonLanguageEntity", "ActionButtonLanguageId", "ActionButtonLanguageId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("ActionButtonLanguageEntity", "ActionButtonId", "ActionButtonId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("ActionButtonLanguageEntity", "LanguageId", "LanguageId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("ActionButtonLanguageEntity", "Name", "Name", true, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 3);
			this.AddElementFieldMapping("ActionButtonLanguageEntity", "CreatedBy", "CreatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("ActionButtonLanguageEntity", "UpdatedBy", "UpdatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 5);
			this.AddElementFieldMapping("ActionButtonLanguageEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 6);
			this.AddElementFieldMapping("ActionButtonLanguageEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 7);
		}

		/// <summary>Inits AddressEntity's mappings</summary>
		private void InitAddressEntityMappings()
		{
			this.AddElementMapping("AddressEntity", @"Obymobi", @"dbo", "Address", 10, 0);
			this.AddElementFieldMapping("AddressEntity", "AddressId", "AddressId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("AddressEntity", "Addressline1", "Addressline1", true, "NVarChar", 200, 0, 0, false, "", null, typeof(System.String), 1);
			this.AddElementFieldMapping("AddressEntity", "Addressline2", "Addressline2", true, "NVarChar", 200, 0, 0, false, "", null, typeof(System.String), 2);
			this.AddElementFieldMapping("AddressEntity", "Addressline3", "Addressline3", true, "NVarChar", 200, 0, 0, false, "", null, typeof(System.String), 3);
			this.AddElementFieldMapping("AddressEntity", "Zipcode", "Zipcode", true, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 4);
			this.AddElementFieldMapping("AddressEntity", "City", "City", true, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 5);
			this.AddElementFieldMapping("AddressEntity", "Latitude", "Latitude", true, "Float", 0, 38, 0, false, "", null, typeof(System.Double), 6);
			this.AddElementFieldMapping("AddressEntity", "Longitude", "Longitude", true, "Float", 0, 38, 0, false, "", null, typeof(System.Double), 7);
			this.AddElementFieldMapping("AddressEntity", "CreatedUTC", "CreatedUTC", false, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 8);
			this.AddElementFieldMapping("AddressEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 9);
		}

		/// <summary>Inits AdvertisementEntity's mappings</summary>
		private void InitAdvertisementEntityMappings()
		{
			this.AddElementMapping("AdvertisementEntity", @"Obymobi", @"dbo", "Advertisement", 21, 0);
			this.AddElementFieldMapping("AdvertisementEntity", "AdvertisementId", "AdvertisementId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("AdvertisementEntity", "SupplierId", "SupplierId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("AdvertisementEntity", "CompanyId", "CompanyId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("AdvertisementEntity", "Name", "Name", true, "NVarChar", 200, 0, 0, false, "", null, typeof(System.String), 3);
			this.AddElementFieldMapping("AdvertisementEntity", "GenericproductId", "GenericproductId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("AdvertisementEntity", "ProductId", "ProductId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 5);
			this.AddElementFieldMapping("AdvertisementEntity", "EntertainmentId", "EntertainmentId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 6);
			this.AddElementFieldMapping("AdvertisementEntity", "Visible", "Visible", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 7);
			this.AddElementFieldMapping("AdvertisementEntity", "UpdatedBy", "UpdatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 8);
			this.AddElementFieldMapping("AdvertisementEntity", "CreatedBy", "CreatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 9);
			this.AddElementFieldMapping("AdvertisementEntity", "Description", "Description", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 10);
			this.AddElementFieldMapping("AdvertisementEntity", "ActionUrl", "ActionUrl", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 11);
			this.AddElementFieldMapping("AdvertisementEntity", "ActionCategoryId", "ActionCategoryId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 12);
			this.AddElementFieldMapping("AdvertisementEntity", "ActionEntertainmentId", "ActionEntertainmentId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 13);
			this.AddElementFieldMapping("AdvertisementEntity", "ActionEntertainmentCategoryId", "ActionEntertainmentCategoryId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 14);
			this.AddElementFieldMapping("AdvertisementEntity", "DeliverypointgroupId", "DeliverypointgroupId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 15);
			this.AddElementFieldMapping("AdvertisementEntity", "ActionPageId", "ActionPageId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 16);
			this.AddElementFieldMapping("AdvertisementEntity", "ActionSiteId", "ActionSiteId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 17);
			this.AddElementFieldMapping("AdvertisementEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 18);
			this.AddElementFieldMapping("AdvertisementEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 19);
			this.AddElementFieldMapping("AdvertisementEntity", "ProductCategoryId", "ProductCategoryId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 20);
		}

		/// <summary>Inits AdvertisementConfigurationEntity's mappings</summary>
		private void InitAdvertisementConfigurationEntityMappings()
		{
			this.AddElementMapping("AdvertisementConfigurationEntity", @"Obymobi", @"dbo", "AdvertisementConfiguration", 7, 0);
			this.AddElementFieldMapping("AdvertisementConfigurationEntity", "AdvertisementConfigurationId", "AdvertisementConfigurationId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("AdvertisementConfigurationEntity", "CompanyId", "CompanyId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("AdvertisementConfigurationEntity", "Name", "Name", false, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 2);
			this.AddElementFieldMapping("AdvertisementConfigurationEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 3);
			this.AddElementFieldMapping("AdvertisementConfigurationEntity", "CreatedBy", "CreatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("AdvertisementConfigurationEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 5);
			this.AddElementFieldMapping("AdvertisementConfigurationEntity", "UpdatedBy", "UpdatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 6);
		}

		/// <summary>Inits AdvertisementConfigurationAdvertisementEntity's mappings</summary>
		private void InitAdvertisementConfigurationAdvertisementEntityMappings()
		{
			this.AddElementMapping("AdvertisementConfigurationAdvertisementEntity", @"Obymobi", @"dbo", "AdvertisementConfigurationAdvertisement", 9, 0);
			this.AddElementFieldMapping("AdvertisementConfigurationAdvertisementEntity", "AdvertisementConfigurationAdvertisementId", "AdvertisementConfigurationAdvertisementId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("AdvertisementConfigurationAdvertisementEntity", "ParentCompanyId", "ParentCompanyId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("AdvertisementConfigurationAdvertisementEntity", "AdvertisementConfigurationId", "AdvertisementConfigurationId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("AdvertisementConfigurationAdvertisementEntity", "AdvertisementId", "AdvertisementId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("AdvertisementConfigurationAdvertisementEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 4);
			this.AddElementFieldMapping("AdvertisementConfigurationAdvertisementEntity", "CreatedBy", "CreatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 5);
			this.AddElementFieldMapping("AdvertisementConfigurationAdvertisementEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 6);
			this.AddElementFieldMapping("AdvertisementConfigurationAdvertisementEntity", "UpdatedBy", "UpdatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 7);
			this.AddElementFieldMapping("AdvertisementConfigurationAdvertisementEntity", "SortOrder", "SortOrder", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 8);
		}

		/// <summary>Inits AdvertisementLanguageEntity's mappings</summary>
		private void InitAdvertisementLanguageEntityMappings()
		{
			this.AddElementMapping("AdvertisementLanguageEntity", @"Obymobi", @"dbo", "AdvertisementLanguage", 10, 0);
			this.AddElementFieldMapping("AdvertisementLanguageEntity", "AdvertisementLanguageId", "AdvertisementLanguageId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("AdvertisementLanguageEntity", "ParentCompanyId", "ParentCompanyId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("AdvertisementLanguageEntity", "AdvertisementId", "AdvertisementId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("AdvertisementLanguageEntity", "LanguageId", "LanguageId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("AdvertisementLanguageEntity", "Name", "Name", true, "NVarChar", 200, 0, 0, false, "", null, typeof(System.String), 4);
			this.AddElementFieldMapping("AdvertisementLanguageEntity", "Description", "Description", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 5);
			this.AddElementFieldMapping("AdvertisementLanguageEntity", "CreatedBy", "CreatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 6);
			this.AddElementFieldMapping("AdvertisementLanguageEntity", "UpdatedBy", "UpdatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 7);
			this.AddElementFieldMapping("AdvertisementLanguageEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 8);
			this.AddElementFieldMapping("AdvertisementLanguageEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 9);
		}

		/// <summary>Inits AdvertisementTagEntity's mappings</summary>
		private void InitAdvertisementTagEntityMappings()
		{
			this.AddElementMapping("AdvertisementTagEntity", @"Obymobi", @"dbo", "AdvertisementTag", 7, 0);
			this.AddElementFieldMapping("AdvertisementTagEntity", "AdvertisementTagId", "AdvertisementTagId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("AdvertisementTagEntity", "Name", "Name", true, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 1);
			this.AddElementFieldMapping("AdvertisementTagEntity", "CreatedBy", "CreatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("AdvertisementTagEntity", "UpdatedBy", "UpdatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("AdvertisementTagEntity", "FriendlyName", "FriendlyName", true, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 4);
			this.AddElementFieldMapping("AdvertisementTagEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 5);
			this.AddElementFieldMapping("AdvertisementTagEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 6);
		}

		/// <summary>Inits AdvertisementTagAdvertisementEntity's mappings</summary>
		private void InitAdvertisementTagAdvertisementEntityMappings()
		{
			this.AddElementMapping("AdvertisementTagAdvertisementEntity", @"Obymobi", @"dbo", "AdvertisementTagAdvertisement", 8, 0);
			this.AddElementFieldMapping("AdvertisementTagAdvertisementEntity", "AdvertisementTagAdvertisementId", "AdvertisementTagAdvertisementId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("AdvertisementTagAdvertisementEntity", "AdvertisementId", "AdvertisementId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("AdvertisementTagAdvertisementEntity", "AdvertisementTagId", "AdvertisementTagId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("AdvertisementTagAdvertisementEntity", "CreatedBy", "CreatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("AdvertisementTagAdvertisementEntity", "UpdatedBy", "UpdatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("AdvertisementTagAdvertisementEntity", "ParentCompanyId", "ParentCompanyId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 5);
			this.AddElementFieldMapping("AdvertisementTagAdvertisementEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 6);
			this.AddElementFieldMapping("AdvertisementTagAdvertisementEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 7);
		}

		/// <summary>Inits AdvertisementTagCategoryEntity's mappings</summary>
		private void InitAdvertisementTagCategoryEntityMappings()
		{
			this.AddElementMapping("AdvertisementTagCategoryEntity", @"Obymobi", @"dbo", "AdvertisementTagCategory", 8, 0);
			this.AddElementFieldMapping("AdvertisementTagCategoryEntity", "AdvertisementTagCategoryId", "AdvertisementTagCategoryId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("AdvertisementTagCategoryEntity", "CategoryId", "CategoryId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("AdvertisementTagCategoryEntity", "AdvertisementTagId", "AdvertisementTagId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("AdvertisementTagCategoryEntity", "CreatedBy", "CreatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("AdvertisementTagCategoryEntity", "UpdatedBy", "UpdatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("AdvertisementTagCategoryEntity", "ParentCompanyId", "ParentCompanyId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 5);
			this.AddElementFieldMapping("AdvertisementTagCategoryEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 6);
			this.AddElementFieldMapping("AdvertisementTagCategoryEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 7);
		}

		/// <summary>Inits AdvertisementTagEntertainmentEntity's mappings</summary>
		private void InitAdvertisementTagEntertainmentEntityMappings()
		{
			this.AddElementMapping("AdvertisementTagEntertainmentEntity", @"Obymobi", @"dbo", "AdvertisementTagEntertainment", 8, 0);
			this.AddElementFieldMapping("AdvertisementTagEntertainmentEntity", "AdvertisementTagEntertainmentId", "AdvertisementTagEntertainmentId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("AdvertisementTagEntertainmentEntity", "EntertainmentId", "EntertainmentId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("AdvertisementTagEntertainmentEntity", "AdvertisementTagId", "AdvertisementTagId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("AdvertisementTagEntertainmentEntity", "CreatedBy", "CreatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("AdvertisementTagEntertainmentEntity", "UpdatedBy", "UpdatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("AdvertisementTagEntertainmentEntity", "ParentCompanyId", "ParentCompanyId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 5);
			this.AddElementFieldMapping("AdvertisementTagEntertainmentEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 6);
			this.AddElementFieldMapping("AdvertisementTagEntertainmentEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 7);
		}

		/// <summary>Inits AdvertisementTagGenericproductEntity's mappings</summary>
		private void InitAdvertisementTagGenericproductEntityMappings()
		{
			this.AddElementMapping("AdvertisementTagGenericproductEntity", @"Obymobi", @"dbo", "AdvertisementTagGenericproduct", 7, 0);
			this.AddElementFieldMapping("AdvertisementTagGenericproductEntity", "AdvertisementTagGenericproductId", "AdvertisementTagGenericproductId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("AdvertisementTagGenericproductEntity", "GenericproductId", "GenericproductId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("AdvertisementTagGenericproductEntity", "AdvertisementTagId", "AdvertisementTagId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("AdvertisementTagGenericproductEntity", "CreatedBy", "CreatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("AdvertisementTagGenericproductEntity", "UpdatedBy", "UpdatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("AdvertisementTagGenericproductEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 5);
			this.AddElementFieldMapping("AdvertisementTagGenericproductEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 6);
		}

		/// <summary>Inits AdvertisementTagLanguageEntity's mappings</summary>
		private void InitAdvertisementTagLanguageEntityMappings()
		{
			this.AddElementMapping("AdvertisementTagLanguageEntity", @"Obymobi", @"dbo", "AdvertisementTagLanguage", 8, 0);
			this.AddElementFieldMapping("AdvertisementTagLanguageEntity", "AdvertisementTagLanguageId", "AdvertisementTagLanguageId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("AdvertisementTagLanguageEntity", "AdvertisementTagId", "AdvertisementTagId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("AdvertisementTagLanguageEntity", "LanguageId", "LanguageId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("AdvertisementTagLanguageEntity", "FriendlyName", "FriendlyName", true, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 3);
			this.AddElementFieldMapping("AdvertisementTagLanguageEntity", "CreatedBy", "CreatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("AdvertisementTagLanguageEntity", "UpdatedBy", "UpdatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 5);
			this.AddElementFieldMapping("AdvertisementTagLanguageEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 6);
			this.AddElementFieldMapping("AdvertisementTagLanguageEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 7);
		}

		/// <summary>Inits AdvertisementTagProductEntity's mappings</summary>
		private void InitAdvertisementTagProductEntityMappings()
		{
			this.AddElementMapping("AdvertisementTagProductEntity", @"Obymobi", @"dbo", "AdvertisementTagProduct", 8, 0);
			this.AddElementFieldMapping("AdvertisementTagProductEntity", "AdvertisementTagProductId", "AdvertisementTagProductId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("AdvertisementTagProductEntity", "ProductId", "ProductId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("AdvertisementTagProductEntity", "AdvertisementTagId", "AdvertisementTagId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("AdvertisementTagProductEntity", "CreatedBy", "CreatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("AdvertisementTagProductEntity", "UpdatedBy", "UpdatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("AdvertisementTagProductEntity", "ParentCompanyId", "ParentCompanyId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 5);
			this.AddElementFieldMapping("AdvertisementTagProductEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 6);
			this.AddElementFieldMapping("AdvertisementTagProductEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 7);
		}

		/// <summary>Inits AdyenPaymentMethodEntity's mappings</summary>
		private void InitAdyenPaymentMethodEntityMappings()
		{
			this.AddElementMapping("AdyenPaymentMethodEntity", @"Obymobi", @"dbo", "AdyenPaymentMethod", 9, 0);
			this.AddElementFieldMapping("AdyenPaymentMethodEntity", "AdyenPaymentMethodId", "AdyenPaymentMethodId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("AdyenPaymentMethodEntity", "PaymentIntegrationConfigurationId", "PaymentIntegrationConfigurationId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("AdyenPaymentMethodEntity", "ParentCompanyId", "ParentCompanyId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("AdyenPaymentMethodEntity", "Type", "Type", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("AdyenPaymentMethodEntity", "Active", "Active", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 4);
			this.AddElementFieldMapping("AdyenPaymentMethodEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 5);
			this.AddElementFieldMapping("AdyenPaymentMethodEntity", "CreatedUTC", "CreatedUTC", false, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 6);
			this.AddElementFieldMapping("AdyenPaymentMethodEntity", "CreatedBy", "CreatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 7);
			this.AddElementFieldMapping("AdyenPaymentMethodEntity", "UpdatedBy", "UpdatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 8);
		}

		/// <summary>Inits AdyenPaymentMethodBrandEntity's mappings</summary>
		private void InitAdyenPaymentMethodBrandEntityMappings()
		{
			this.AddElementMapping("AdyenPaymentMethodBrandEntity", @"Obymobi", @"dbo", "AdyenPaymentMethodBrand", 9, 0);
			this.AddElementFieldMapping("AdyenPaymentMethodBrandEntity", "AdyenPaymentMethodBrandId", "AdyenPaymentMethodBrandId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("AdyenPaymentMethodBrandEntity", "AdyenPaymentMethodId", "AdyenPaymentMethodId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("AdyenPaymentMethodBrandEntity", "ParentCompanyId", "ParentCompanyId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("AdyenPaymentMethodBrandEntity", "Brand", "Brand", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("AdyenPaymentMethodBrandEntity", "Active", "Active", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 4);
			this.AddElementFieldMapping("AdyenPaymentMethodBrandEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 5);
			this.AddElementFieldMapping("AdyenPaymentMethodBrandEntity", "CreatedUTC", "CreatedUTC", false, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 6);
			this.AddElementFieldMapping("AdyenPaymentMethodBrandEntity", "CreatedBy", "CreatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 7);
			this.AddElementFieldMapping("AdyenPaymentMethodBrandEntity", "UpdatedBy", "UpdatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 8);
		}

		/// <summary>Inits AffiliateCampaignEntity's mappings</summary>
		private void InitAffiliateCampaignEntityMappings()
		{
			this.AddElementMapping("AffiliateCampaignEntity", @"Obymobi", @"dbo", "AffiliateCampaign", 5, 0);
			this.AddElementFieldMapping("AffiliateCampaignEntity", "AffiliateCampaignId", "AffiliateCampaignId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("AffiliateCampaignEntity", "Name", "Name", false, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 1);
			this.AddElementFieldMapping("AffiliateCampaignEntity", "Code", "Code", false, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 2);
			this.AddElementFieldMapping("AffiliateCampaignEntity", "CreatedUTC", "CreatedUTC", false, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 3);
			this.AddElementFieldMapping("AffiliateCampaignEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 4);
		}

		/// <summary>Inits AffiliateCampaignAffiliatePartnerEntity's mappings</summary>
		private void InitAffiliateCampaignAffiliatePartnerEntityMappings()
		{
			this.AddElementMapping("AffiliateCampaignAffiliatePartnerEntity", @"Obymobi", @"dbo", "AffiliateCampaignAffiliatePartner", 6, 0);
			this.AddElementFieldMapping("AffiliateCampaignAffiliatePartnerEntity", "AffiliateCampaignAffiliatePartnerId", "AffiliateCampaignAffiliatePartnerId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("AffiliateCampaignAffiliatePartnerEntity", "AffiliateCampaignId", "AffiliateCampaignId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("AffiliateCampaignAffiliatePartnerEntity", "AffiliatePartnerId", "AffiliatePartnerId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("AffiliateCampaignAffiliatePartnerEntity", "PartnerCampaignCode", "PartnerCampaignCode", false, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 3);
			this.AddElementFieldMapping("AffiliateCampaignAffiliatePartnerEntity", "CreatedUTC", "CreatedUTC", false, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 4);
			this.AddElementFieldMapping("AffiliateCampaignAffiliatePartnerEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 5);
		}

		/// <summary>Inits AffiliatePartnerEntity's mappings</summary>
		private void InitAffiliatePartnerEntityMappings()
		{
			this.AddElementMapping("AffiliatePartnerEntity", @"Obymobi", @"dbo", "AffiliatePartner", 5, 0);
			this.AddElementFieldMapping("AffiliatePartnerEntity", "AffiliatePartnerId", "AffiliatePartnerId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("AffiliatePartnerEntity", "Name", "Name", false, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 1);
			this.AddElementFieldMapping("AffiliatePartnerEntity", "DomainUrl", "DomainUrl", false, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 2);
			this.AddElementFieldMapping("AffiliatePartnerEntity", "CreatedUTC", "CreatedUTC", false, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 3);
			this.AddElementFieldMapping("AffiliatePartnerEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 4);
		}

		/// <summary>Inits AlterationEntity's mappings</summary>
		private void InitAlterationEntityMappings()
		{
			this.AddElementMapping("AlterationEntity", @"Obymobi", @"dbo", "Alteration", 37, 0);
			this.AddElementFieldMapping("AlterationEntity", "AlterationId", "AlterationId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("AlterationEntity", "Name", "Name", false, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 1);
			this.AddElementFieldMapping("AlterationEntity", "MinOptions", "MinOptions", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("AlterationEntity", "MaxOptions", "MaxOptions", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("AlterationEntity", "PosalterationId", "PosalterationId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("AlterationEntity", "CompanyId", "CompanyId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 5);
			this.AddElementFieldMapping("AlterationEntity", "AvailableOnOtoucho", "AvailableOnOtoucho", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 6);
			this.AddElementFieldMapping("AlterationEntity", "AvailableOnObymobi", "AvailableOnObymobi", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 7);
			this.AddElementFieldMapping("AlterationEntity", "CreatedBy", "CreatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 8);
			this.AddElementFieldMapping("AlterationEntity", "UpdatedBy", "UpdatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 9);
			this.AddElementFieldMapping("AlterationEntity", "MinLeadMinutes", "MinLeadMinutes", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 10);
			this.AddElementFieldMapping("AlterationEntity", "MaxLeadHours", "MaxLeadHours", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 11);
			this.AddElementFieldMapping("AlterationEntity", "IntervalMinutes", "IntervalMinutes", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 12);
			this.AddElementFieldMapping("AlterationEntity", "ShowDatePicker", "ShowDatePicker", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 13);
			this.AddElementFieldMapping("AlterationEntity", "Type", "Type", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 14);
			this.AddElementFieldMapping("AlterationEntity", "GenericalterationId", "GenericalterationId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 15);
			this.AddElementFieldMapping("AlterationEntity", "Value", "Value", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 16);
			this.AddElementFieldMapping("AlterationEntity", "OrderLevelEnabled", "OrderLevelEnabled", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 17);
			this.AddElementFieldMapping("AlterationEntity", "Description", "Description", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 18);
			this.AddElementFieldMapping("AlterationEntity", "LayoutType", "LayoutType", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 19);
			this.AddElementFieldMapping("AlterationEntity", "ParentAlterationId", "ParentAlterationId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 20);
			this.AddElementFieldMapping("AlterationEntity", "SortOrder", "SortOrder", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 21);
			this.AddElementFieldMapping("AlterationEntity", "Visible", "Visible", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 22);
			this.AddElementFieldMapping("AlterationEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 23);
			this.AddElementFieldMapping("AlterationEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 24);
			this.AddElementFieldMapping("AlterationEntity", "FriendlyName", "FriendlyName", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 25);
			this.AddElementFieldMapping("AlterationEntity", "PriceAddition", "PriceAddition", false, "Money", 0, 19, 4, false, "", null, typeof(System.Decimal), 26);
			this.AddElementFieldMapping("AlterationEntity", "Version", "Version", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 27);
			this.AddElementFieldMapping("AlterationEntity", "OriginalAlterationId", "OriginalAlterationId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 28);
			this.AddElementFieldMapping("AlterationEntity", "AllowDuplicates", "AllowDuplicates", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 29);
			this.AddElementFieldMapping("AlterationEntity", "StartTime", "StartTime", true, "DateTime", 0, 0, 0, false, "", null, typeof(System.DateTime), 30);
			this.AddElementFieldMapping("AlterationEntity", "EndTime", "EndTime", true, "DateTime", 0, 0, 0, false, "", null, typeof(System.DateTime), 31);
			this.AddElementFieldMapping("AlterationEntity", "StartTimeUTC", "StartTimeUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 32);
			this.AddElementFieldMapping("AlterationEntity", "EndTimeUTC", "EndTimeUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 33);
			this.AddElementFieldMapping("AlterationEntity", "BrandId", "BrandId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 34);
			this.AddElementFieldMapping("AlterationEntity", "ExternalProductId", "ExternalProductId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 35);
			this.AddElementFieldMapping("AlterationEntity", "IsAvailable", "IsAvailable", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 36);
		}

		/// <summary>Inits AlterationitemEntity's mappings</summary>
		private void InitAlterationitemEntityMappings()
		{
			this.AddElementMapping("AlterationitemEntity", @"Obymobi", @"dbo", "Alterationitem", 13, 0);
			this.AddElementFieldMapping("AlterationitemEntity", "AlterationitemId", "AlterationitemId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("AlterationitemEntity", "AlterationId", "AlterationId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("AlterationitemEntity", "AlterationoptionId", "AlterationoptionId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("AlterationitemEntity", "SelectedOnDefault", "SelectedOnDefault", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 3);
			this.AddElementFieldMapping("AlterationitemEntity", "SortOrder", "SortOrder", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("AlterationitemEntity", "PosalterationitemId", "PosalterationitemId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 5);
			this.AddElementFieldMapping("AlterationitemEntity", "CreatedBy", "CreatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 6);
			this.AddElementFieldMapping("AlterationitemEntity", "UpdatedBy", "UpdatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 7);
			this.AddElementFieldMapping("AlterationitemEntity", "GenericalterationitemId", "GenericalterationitemId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 8);
			this.AddElementFieldMapping("AlterationitemEntity", "ParentCompanyId", "ParentCompanyId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 9);
			this.AddElementFieldMapping("AlterationitemEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 10);
			this.AddElementFieldMapping("AlterationitemEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 11);
			this.AddElementFieldMapping("AlterationitemEntity", "Version", "Version", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 12);
		}

		/// <summary>Inits AlterationitemAlterationEntity's mappings</summary>
		private void InitAlterationitemAlterationEntityMappings()
		{
			this.AddElementMapping("AlterationitemAlterationEntity", @"Obymobi", @"dbo", "AlterationitemAlteration", 9, 0);
			this.AddElementFieldMapping("AlterationitemAlterationEntity", "AlterationitemAlterationId", "AlterationitemAlterationId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("AlterationitemAlterationEntity", "ParentCompanyId", "ParentCompanyId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("AlterationitemAlterationEntity", "AlterationitemId", "AlterationitemId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("AlterationitemAlterationEntity", "AlterationId", "AlterationId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("AlterationitemAlterationEntity", "SortOrder", "SortOrder", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("AlterationitemAlterationEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 5);
			this.AddElementFieldMapping("AlterationitemAlterationEntity", "CreatedBy", "CreatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 6);
			this.AddElementFieldMapping("AlterationitemAlterationEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 7);
			this.AddElementFieldMapping("AlterationitemAlterationEntity", "UpdatedBy", "UpdatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 8);
		}

		/// <summary>Inits AlterationLanguageEntity's mappings</summary>
		private void InitAlterationLanguageEntityMappings()
		{
			this.AddElementMapping("AlterationLanguageEntity", @"Obymobi", @"dbo", "AlterationLanguage", 10, 0);
			this.AddElementFieldMapping("AlterationLanguageEntity", "AlterationLanguageId", "AlterationLanguageId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("AlterationLanguageEntity", "AlterationId", "AlterationId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("AlterationLanguageEntity", "LanguageId", "LanguageId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("AlterationLanguageEntity", "Name", "Name", true, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 3);
			this.AddElementFieldMapping("AlterationLanguageEntity", "CreatedBy", "CreatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("AlterationLanguageEntity", "UpdatedBy", "UpdatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 5);
			this.AddElementFieldMapping("AlterationLanguageEntity", "ParentCompanyId", "ParentCompanyId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 6);
			this.AddElementFieldMapping("AlterationLanguageEntity", "Description", "Description", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 7);
			this.AddElementFieldMapping("AlterationLanguageEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 8);
			this.AddElementFieldMapping("AlterationLanguageEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 9);
		}

		/// <summary>Inits AlterationoptionEntity's mappings</summary>
		private void InitAlterationoptionEntityMappings()
		{
			this.AddElementMapping("AlterationoptionEntity", @"Obymobi", @"dbo", "Alterationoption", 38, 0);
			this.AddElementFieldMapping("AlterationoptionEntity", "AlterationoptionId", "AlterationoptionId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("AlterationoptionEntity", "PosalterationoptionId", "PosalterationoptionId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("AlterationoptionEntity", "Name", "Name", false, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 2);
			this.AddElementFieldMapping("AlterationoptionEntity", "PriceIn", "PriceIn", true, "Money", 0, 19, 4, false, "", null, typeof(System.Decimal), 3);
			this.AddElementFieldMapping("AlterationoptionEntity", "Description", "Description", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 4);
			this.AddElementFieldMapping("AlterationoptionEntity", "PosproductId", "PosproductId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 5);
			this.AddElementFieldMapping("AlterationoptionEntity", "IsProductRelated", "IsProductRelated", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 6);
			this.AddElementFieldMapping("AlterationoptionEntity", "CompanyId", "CompanyId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 7);
			this.AddElementFieldMapping("AlterationoptionEntity", "CreatedBy", "CreatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 8);
			this.AddElementFieldMapping("AlterationoptionEntity", "UpdatedBy", "UpdatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 9);
			this.AddElementFieldMapping("AlterationoptionEntity", "GenericalterationoptionId", "GenericalterationoptionId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 10);
			this.AddElementFieldMapping("AlterationoptionEntity", "FriendlyName", "FriendlyName", true, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 11);
			this.AddElementFieldMapping("AlterationoptionEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 12);
			this.AddElementFieldMapping("AlterationoptionEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 13);
			this.AddElementFieldMapping("AlterationoptionEntity", "Type", "Type", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 14);
			this.AddElementFieldMapping("AlterationoptionEntity", "XPriceAddition", "xPriceAddition", true, "Money", 0, 19, 4, false, "", null, typeof(System.Decimal), 15);
			this.AddElementFieldMapping("AlterationoptionEntity", "StartTime", "StartTime", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 16);
			this.AddElementFieldMapping("AlterationoptionEntity", "EndTime", "EndTime", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 17);
			this.AddElementFieldMapping("AlterationoptionEntity", "MinLeadMinutes", "MinLeadMinutes", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 18);
			this.AddElementFieldMapping("AlterationoptionEntity", "MaxLeadHours", "MaxLeadHours", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 19);
			this.AddElementFieldMapping("AlterationoptionEntity", "IntervalMinutes", "IntervalMinutes", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 20);
			this.AddElementFieldMapping("AlterationoptionEntity", "ShowDatePicker", "ShowDatePicker", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 21);
			this.AddElementFieldMapping("AlterationoptionEntity", "Version", "Version", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 22);
			this.AddElementFieldMapping("AlterationoptionEntity", "StartTimeUTC", "StartTimeUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 23);
			this.AddElementFieldMapping("AlterationoptionEntity", "EndTimeUTC", "EndTimeUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 24);
			this.AddElementFieldMapping("AlterationoptionEntity", "BrandId", "BrandId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 25);
			this.AddElementFieldMapping("AlterationoptionEntity", "TaxTariffId", "TaxTariffId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 26);
			this.AddElementFieldMapping("AlterationoptionEntity", "IsAlcoholic", "IsAlcoholic", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 27);
			this.AddElementFieldMapping("AlterationoptionEntity", "InheritName", "InheritName", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 28);
			this.AddElementFieldMapping("AlterationoptionEntity", "InheritPrice", "InheritPrice", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 29);
			this.AddElementFieldMapping("AlterationoptionEntity", "InheritTaxTariff", "InheritTaxTariff", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 30);
			this.AddElementFieldMapping("AlterationoptionEntity", "InheritIsAlcoholic", "InheritIsAlcoholic", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 31);
			this.AddElementFieldMapping("AlterationoptionEntity", "ProductId", "ProductId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 32);
			this.AddElementFieldMapping("AlterationoptionEntity", "InheritTags", "InheritTags", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 33);
			this.AddElementFieldMapping("AlterationoptionEntity", "InheritCustomText", "InheritCustomText", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 34);
			this.AddElementFieldMapping("AlterationoptionEntity", "ExternalProductId", "ExternalProductId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 35);
			this.AddElementFieldMapping("AlterationoptionEntity", "IsAvailable", "IsAvailable", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 36);
			this.AddElementFieldMapping("AlterationoptionEntity", "ExternalIdentifier", "ExternalIdentifier", true, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 37);
		}

		/// <summary>Inits AlterationoptionLanguageEntity's mappings</summary>
		private void InitAlterationoptionLanguageEntityMappings()
		{
			this.AddElementMapping("AlterationoptionLanguageEntity", @"Obymobi", @"dbo", "AlterationoptionLanguage", 10, 0);
			this.AddElementFieldMapping("AlterationoptionLanguageEntity", "AlterationoptionLanguageId", "AlterationoptionLanguageId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("AlterationoptionLanguageEntity", "AlterationoptionId", "AlterationoptionId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("AlterationoptionLanguageEntity", "LanguageId", "LanguageId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("AlterationoptionLanguageEntity", "Name", "Name", true, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 3);
			this.AddElementFieldMapping("AlterationoptionLanguageEntity", "CreatedBy", "CreatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("AlterationoptionLanguageEntity", "UpdatedBy", "UpdatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 5);
			this.AddElementFieldMapping("AlterationoptionLanguageEntity", "ParentCompanyId", "ParentCompanyId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 6);
			this.AddElementFieldMapping("AlterationoptionLanguageEntity", "Description", "Description", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 7);
			this.AddElementFieldMapping("AlterationoptionLanguageEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 8);
			this.AddElementFieldMapping("AlterationoptionLanguageEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 9);
		}

		/// <summary>Inits AlterationoptionTagEntity's mappings</summary>
		private void InitAlterationoptionTagEntityMappings()
		{
			this.AddElementMapping("AlterationoptionTagEntity", @"Obymobi", @"dbo", "AlterationoptionTag", 6, 0);
			this.AddElementFieldMapping("AlterationoptionTagEntity", "AlterationoptionTagId", "AlterationoptionTagId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("AlterationoptionTagEntity", "AlterationoptionId", "AlterationoptionId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("AlterationoptionTagEntity", "TagId", "TagId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("AlterationoptionTagEntity", "CreatedUTC", "CreatedUTC", false, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 3);
			this.AddElementFieldMapping("AlterationoptionTagEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 4);
			this.AddElementFieldMapping("AlterationoptionTagEntity", "ParentCompanyId", "ParentCompanyId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 5);
		}

		/// <summary>Inits AlterationProductEntity's mappings</summary>
		private void InitAlterationProductEntityMappings()
		{
			this.AddElementMapping("AlterationProductEntity", @"Obymobi", @"dbo", "AlterationProduct", 10, 0);
			this.AddElementFieldMapping("AlterationProductEntity", "AlterationProductId", "AlterationProductId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("AlterationProductEntity", "ParentCompanyId", "ParentCompanyId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("AlterationProductEntity", "AlterationId", "AlterationId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("AlterationProductEntity", "ProductId", "ProductId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("AlterationProductEntity", "SortOrder", "SortOrder", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("AlterationProductEntity", "Visible", "Visible", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 5);
			this.AddElementFieldMapping("AlterationProductEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 6);
			this.AddElementFieldMapping("AlterationProductEntity", "CreatedBy", "CreatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 7);
			this.AddElementFieldMapping("AlterationProductEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 8);
			this.AddElementFieldMapping("AlterationProductEntity", "UpdatedBy", "UpdatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 9);
		}

		/// <summary>Inits AmenityEntity's mappings</summary>
		private void InitAmenityEntityMappings()
		{
			this.AddElementMapping("AmenityEntity", @"Obymobi", @"dbo", "Amenity", 7, 0);
			this.AddElementFieldMapping("AmenityEntity", "AmenityId", "AmenityId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("AmenityEntity", "Name", "Name", false, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 1);
			this.AddElementFieldMapping("AmenityEntity", "UpdatedBy", "UpdatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("AmenityEntity", "CreatedBy", "CreatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("AmenityEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 4);
			this.AddElementFieldMapping("AmenityEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 5);
			this.AddElementFieldMapping("AmenityEntity", "LastModifiedUTC", "LastModifiedUTC", false, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 6);
		}

		/// <summary>Inits AmenityLanguageEntity's mappings</summary>
		private void InitAmenityLanguageEntityMappings()
		{
			this.AddElementMapping("AmenityLanguageEntity", @"Obymobi", @"dbo", "AmenityLanguage", 8, 0);
			this.AddElementFieldMapping("AmenityLanguageEntity", "AmenityLanguageId", "AmenityLanguageId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("AmenityLanguageEntity", "AmenityId", "AmenityId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("AmenityLanguageEntity", "LanguageId", "LanguageId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("AmenityLanguageEntity", "Name", "Name", true, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 3);
			this.AddElementFieldMapping("AmenityLanguageEntity", "CreatedBy", "CreatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("AmenityLanguageEntity", "UpdatedBy", "UpdatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 5);
			this.AddElementFieldMapping("AmenityLanguageEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 6);
			this.AddElementFieldMapping("AmenityLanguageEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 7);
		}

		/// <summary>Inits AnalyticsProcessingTaskEntity's mappings</summary>
		private void InitAnalyticsProcessingTaskEntityMappings()
		{
			this.AddElementMapping("AnalyticsProcessingTaskEntity", @"Obymobi", @"dbo", "AnalyticsProcessingTask", 6, 0);
			this.AddElementFieldMapping("AnalyticsProcessingTaskEntity", "AnalyticsProcessingTaskId", "AnalyticsProcessingTaskId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("AnalyticsProcessingTaskEntity", "OrderId", "OrderId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("AnalyticsProcessingTaskEntity", "EventAction", "EventAction", false, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 2);
			this.AddElementFieldMapping("AnalyticsProcessingTaskEntity", "EventLabel", "EventLabel", false, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 3);
			this.AddElementFieldMapping("AnalyticsProcessingTaskEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 4);
			this.AddElementFieldMapping("AnalyticsProcessingTaskEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 5);
		}

		/// <summary>Inits AnnouncementEntity's mappings</summary>
		private void InitAnnouncementEntityMappings()
		{
			this.AddElementMapping("AnnouncementEntity", @"Obymobi", @"dbo", "Announcement", 30, 0);
			this.AddElementFieldMapping("AnnouncementEntity", "AnnouncementId", "AnnouncementId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("AnnouncementEntity", "CompanyId", "CompanyId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("AnnouncementEntity", "Title", "Title", true, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 2);
			this.AddElementFieldMapping("AnnouncementEntity", "Text", "Text", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 3);
			this.AddElementFieldMapping("AnnouncementEntity", "DateToShow", "DateToShow", true, "DateTime", 0, 0, 0, false, "", null, typeof(System.DateTime), 4);
			this.AddElementFieldMapping("AnnouncementEntity", "TimeToShow", "TimeToShow", true, "DateTime", 0, 0, 0, false, "", null, typeof(System.DateTime), 5);
			this.AddElementFieldMapping("AnnouncementEntity", "Recurring", "Recurring", true, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 6);
			this.AddElementFieldMapping("AnnouncementEntity", "RecurringPeriod", "RecurringPeriod", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 7);
			this.AddElementFieldMapping("AnnouncementEntity", "RecurringBeginDate", "RecurringBeginDate", true, "DateTime", 0, 0, 0, false, "", null, typeof(System.DateTime), 8);
			this.AddElementFieldMapping("AnnouncementEntity", "RecurringBeginTime", "RecurringBeginTime", true, "DateTime", 0, 0, 0, false, "", null, typeof(System.DateTime), 9);
			this.AddElementFieldMapping("AnnouncementEntity", "RecurringEndDate", "RecurringEndDate", true, "DateTime", 0, 0, 0, false, "", null, typeof(System.DateTime), 10);
			this.AddElementFieldMapping("AnnouncementEntity", "RecurringEndTime", "RecurringEndTime", true, "DateTime", 0, 0, 0, false, "", null, typeof(System.DateTime), 11);
			this.AddElementFieldMapping("AnnouncementEntity", "RecurringAmount", "RecurringAmount", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 12);
			this.AddElementFieldMapping("AnnouncementEntity", "RecurringMinutes", "RecurringMinutes", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 13);
			this.AddElementFieldMapping("AnnouncementEntity", "DialogType", "DialogType", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 14);
			this.AddElementFieldMapping("AnnouncementEntity", "OnYes", "OnYes", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 15);
			this.AddElementFieldMapping("AnnouncementEntity", "OnNo", "OnNo", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 16);
			this.AddElementFieldMapping("AnnouncementEntity", "OnYesCategory", "OnYesCategory", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 17);
			this.AddElementFieldMapping("AnnouncementEntity", "OnNoCategory", "OnNoCategory", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 18);
			this.AddElementFieldMapping("AnnouncementEntity", "OnYesEntertainmentCategory", "OnYesEntertainmentCategory", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 19);
			this.AddElementFieldMapping("AnnouncementEntity", "OnNoEntertainmentCategory", "OnNoEntertainmentCategory", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 20);
			this.AddElementFieldMapping("AnnouncementEntity", "CreatedBy", "CreatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 21);
			this.AddElementFieldMapping("AnnouncementEntity", "UpdatedBy", "UpdatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 22);
			this.AddElementFieldMapping("AnnouncementEntity", "Duration", "Duration", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 23);
			this.AddElementFieldMapping("AnnouncementEntity", "OnYesEntertainment", "OnYesEntertainment", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 24);
			this.AddElementFieldMapping("AnnouncementEntity", "MediaId", "MediaId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 25);
			this.AddElementFieldMapping("AnnouncementEntity", "DeliverypointgroupId", "DeliverypointgroupId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 26);
			this.AddElementFieldMapping("AnnouncementEntity", "OnYesProduct", "OnYesProduct", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 27);
			this.AddElementFieldMapping("AnnouncementEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 28);
			this.AddElementFieldMapping("AnnouncementEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 29);
		}

		/// <summary>Inits AnnouncementLanguageEntity's mappings</summary>
		private void InitAnnouncementLanguageEntityMappings()
		{
			this.AddElementMapping("AnnouncementLanguageEntity", @"Obymobi", @"dbo", "AnnouncementLanguage", 10, 0);
			this.AddElementFieldMapping("AnnouncementLanguageEntity", "AnnouncementLanguageId", "AnnouncementLanguageId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("AnnouncementLanguageEntity", "AnnouncementId", "AnnouncementId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("AnnouncementLanguageEntity", "LanguageId", "LanguageId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("AnnouncementLanguageEntity", "Title", "Title", true, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 3);
			this.AddElementFieldMapping("AnnouncementLanguageEntity", "Text", "Text", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 4);
			this.AddElementFieldMapping("AnnouncementLanguageEntity", "CreatedBy", "CreatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 5);
			this.AddElementFieldMapping("AnnouncementLanguageEntity", "UpdatedBy", "UpdatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 6);
			this.AddElementFieldMapping("AnnouncementLanguageEntity", "ParentCompanyId", "ParentCompanyId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 7);
			this.AddElementFieldMapping("AnnouncementLanguageEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 8);
			this.AddElementFieldMapping("AnnouncementLanguageEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 9);
		}

		/// <summary>Inits ApiAuthenticationEntity's mappings</summary>
		private void InitApiAuthenticationEntityMappings()
		{
			this.AddElementMapping("ApiAuthenticationEntity", @"Obymobi", @"dbo", "ApiAuthentication", 19, 0);
			this.AddElementFieldMapping("ApiAuthenticationEntity", "ApiAuthenticationId", "ApiAuthenticationId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("ApiAuthenticationEntity", "CompanyId", "CompanyId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("ApiAuthenticationEntity", "Type", "Type", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("ApiAuthenticationEntity", "Name", "Name", false, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 3);
			this.AddElementFieldMapping("ApiAuthenticationEntity", "FieldValue1", "FieldValue1", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 4);
			this.AddElementFieldMapping("ApiAuthenticationEntity", "FieldValue2", "FieldValue2", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 5);
			this.AddElementFieldMapping("ApiAuthenticationEntity", "FieldValue3", "FieldValue3", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 6);
			this.AddElementFieldMapping("ApiAuthenticationEntity", "FieldValue4", "FieldValue4", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 7);
			this.AddElementFieldMapping("ApiAuthenticationEntity", "FieldValue5", "FieldValue5", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 8);
			this.AddElementFieldMapping("ApiAuthenticationEntity", "FieldValue6", "FieldValue6", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 9);
			this.AddElementFieldMapping("ApiAuthenticationEntity", "FieldValue7", "FieldValue7", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 10);
			this.AddElementFieldMapping("ApiAuthenticationEntity", "FieldValue8", "FieldValue8", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 11);
			this.AddElementFieldMapping("ApiAuthenticationEntity", "FieldValue9", "FieldValue9", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 12);
			this.AddElementFieldMapping("ApiAuthenticationEntity", "FieldValue10", "FieldValue10", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 13);
			this.AddElementFieldMapping("ApiAuthenticationEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 14);
			this.AddElementFieldMapping("ApiAuthenticationEntity", "CreatedBy", "CreatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 15);
			this.AddElementFieldMapping("ApiAuthenticationEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 16);
			this.AddElementFieldMapping("ApiAuthenticationEntity", "UpdatedBy", "UpdatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 17);
			this.AddElementFieldMapping("ApiAuthenticationEntity", "ExpiryDateUTC", "ExpiryDateUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 18);
		}

		/// <summary>Inits ApplicationEntity's mappings</summary>
		private void InitApplicationEntityMappings()
		{
			this.AddElementMapping("ApplicationEntity", @"Obymobi", @"dbo", "Application", 10, 0);
			this.AddElementFieldMapping("ApplicationEntity", "ApplicationId", "ApplicationId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("ApplicationEntity", "Name", "Name", true, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 1);
			this.AddElementFieldMapping("ApplicationEntity", "Code", "Code", true, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 2);
			this.AddElementFieldMapping("ApplicationEntity", "Description", "Description", true, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 3);
			this.AddElementFieldMapping("ApplicationEntity", "ReleaseId", "ReleaseId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("ApplicationEntity", "CreatedBy", "CreatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 5);
			this.AddElementFieldMapping("ApplicationEntity", "UpdatedBy", "UpdatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 6);
			this.AddElementFieldMapping("ApplicationEntity", "DeviceModel", "DeviceModel", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 7);
			this.AddElementFieldMapping("ApplicationEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 8);
			this.AddElementFieldMapping("ApplicationEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 9);
		}

		/// <summary>Inits AttachmentEntity's mappings</summary>
		private void InitAttachmentEntityMappings()
		{
			this.AddElementMapping("AttachmentEntity", @"Obymobi", @"dbo", "Attachment", 17, 0);
			this.AddElementFieldMapping("AttachmentEntity", "AttachmentId", "AttachmentId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("AttachmentEntity", "PageId", "PageId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("AttachmentEntity", "ProductId", "ProductId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("AttachmentEntity", "Name", "Name", false, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 3);
			this.AddElementFieldMapping("AttachmentEntity", "Type", "Type", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("AttachmentEntity", "Url", "Url", true, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 5);
			this.AddElementFieldMapping("AttachmentEntity", "AllowedDomains", "AllowedDomains", true, "NVarChar", 500, 0, 0, false, "", null, typeof(System.String), 6);
			this.AddElementFieldMapping("AttachmentEntity", "AllowAllDomains", "AllowAllDomains", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 7);
			this.AddElementFieldMapping("AttachmentEntity", "CreatedBy", "CreatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 8);
			this.AddElementFieldMapping("AttachmentEntity", "UpdatedBy", "UpdatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 9);
			this.AddElementFieldMapping("AttachmentEntity", "ParentCompanyId", "ParentCompanyId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 10);
			this.AddElementFieldMapping("AttachmentEntity", "PageTemplateId", "PageTemplateId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 11);
			this.AddElementFieldMapping("AttachmentEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 12);
			this.AddElementFieldMapping("AttachmentEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 13);
			this.AddElementFieldMapping("AttachmentEntity", "GenericproductId", "GenericproductId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 14);
			this.AddElementFieldMapping("AttachmentEntity", "OpenNewWindow", "OpenNewWindow", true, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 15);
			this.AddElementFieldMapping("AttachmentEntity", "SortOrder", "SortOrder", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 16);
		}

		/// <summary>Inits AttachmentLanguageEntity's mappings</summary>
		private void InitAttachmentLanguageEntityMappings()
		{
			this.AddElementMapping("AttachmentLanguageEntity", @"Obymobi", @"dbo", "AttachmentLanguage", 9, 0);
			this.AddElementFieldMapping("AttachmentLanguageEntity", "AttachmentLanguageId", "AttachmentLanguageId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("AttachmentLanguageEntity", "AttachmentId", "AttachmentId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("AttachmentLanguageEntity", "LanguageId", "LanguageId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("AttachmentLanguageEntity", "Name", "Name", true, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 3);
			this.AddElementFieldMapping("AttachmentLanguageEntity", "CreatedBy", "CreatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("AttachmentLanguageEntity", "UpdatedBy", "UpdatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 5);
			this.AddElementFieldMapping("AttachmentLanguageEntity", "ParentCompanyId", "ParentCompanyId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 6);
			this.AddElementFieldMapping("AttachmentLanguageEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 7);
			this.AddElementFieldMapping("AttachmentLanguageEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 8);
		}

		/// <summary>Inits AuditlogEntity's mappings</summary>
		private void InitAuditlogEntityMappings()
		{
			this.AddElementMapping("AuditlogEntity", @"Obymobi", @"dbo", "Auditlog", 58, 0);
			this.AddElementFieldMapping("AuditlogEntity", "AuditLogId", "AuditLogId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("AuditlogEntity", "ActionType", "ActionType", true, "NVarChar", 30, 0, 0, false, "", null, typeof(System.String), 1);
			this.AddElementFieldMapping("AuditlogEntity", "EntityType", "EntityType", true, "NVarChar", 200, 0, 0, false, "", null, typeof(System.String), 2);
			this.AddElementFieldMapping("AuditlogEntity", "EntityId", "EntityId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("AuditlogEntity", "ShowName", "ShowName", true, "NVarChar", 150, 0, 0, false, "", null, typeof(System.String), 4);
			this.AddElementFieldMapping("AuditlogEntity", "FieldName0", "FieldName0", true, "NVarChar", 150, 0, 0, false, "", null, typeof(System.String), 5);
			this.AddElementFieldMapping("AuditlogEntity", "FieldValue0", "FieldValue0", true, "NVarChar", 150, 0, 0, false, "", null, typeof(System.String), 6);
			this.AddElementFieldMapping("AuditlogEntity", "FieldName1", "FieldName1", true, "NVarChar", 150, 0, 0, false, "", null, typeof(System.String), 7);
			this.AddElementFieldMapping("AuditlogEntity", "FieldValue1", "FieldValue1", true, "NVarChar", 150, 0, 0, false, "", null, typeof(System.String), 8);
			this.AddElementFieldMapping("AuditlogEntity", "FieldName2", "FieldName2", true, "NVarChar", 150, 0, 0, false, "", null, typeof(System.String), 9);
			this.AddElementFieldMapping("AuditlogEntity", "FieldValue2", "FieldValue2", true, "NVarChar", 150, 0, 0, false, "", null, typeof(System.String), 10);
			this.AddElementFieldMapping("AuditlogEntity", "FieldName3", "FieldName3", true, "NVarChar", 150, 0, 0, false, "", null, typeof(System.String), 11);
			this.AddElementFieldMapping("AuditlogEntity", "FieldValue3", "FieldValue3", true, "NVarChar", 150, 0, 0, false, "", null, typeof(System.String), 12);
			this.AddElementFieldMapping("AuditlogEntity", "FieldName4", "FieldName4", true, "NVarChar", 150, 0, 0, false, "", null, typeof(System.String), 13);
			this.AddElementFieldMapping("AuditlogEntity", "FieldValue4", "FieldValue4", true, "NVarChar", 150, 0, 0, false, "", null, typeof(System.String), 14);
			this.AddElementFieldMapping("AuditlogEntity", "FieldName5", "FieldName5", true, "NVarChar", 150, 0, 0, false, "", null, typeof(System.String), 15);
			this.AddElementFieldMapping("AuditlogEntity", "FieldValue5", "FieldValue5", true, "NVarChar", 150, 0, 0, false, "", null, typeof(System.String), 16);
			this.AddElementFieldMapping("AuditlogEntity", "FieldName6", "FieldName6", true, "NVarChar", 150, 0, 0, false, "", null, typeof(System.String), 17);
			this.AddElementFieldMapping("AuditlogEntity", "FieldValue6", "FieldValue6", true, "NVarChar", 150, 0, 0, false, "", null, typeof(System.String), 18);
			this.AddElementFieldMapping("AuditlogEntity", "FieldName7", "FieldName7", true, "NVarChar", 150, 0, 0, false, "", null, typeof(System.String), 19);
			this.AddElementFieldMapping("AuditlogEntity", "FieldValue7", "FieldValue7", true, "NVarChar", 150, 0, 0, false, "", null, typeof(System.String), 20);
			this.AddElementFieldMapping("AuditlogEntity", "FieldName8", "FieldName8", true, "NVarChar", 150, 0, 0, false, "", null, typeof(System.String), 21);
			this.AddElementFieldMapping("AuditlogEntity", "FieldValue8", "FieldValue8", true, "NVarChar", 150, 0, 0, false, "", null, typeof(System.String), 22);
			this.AddElementFieldMapping("AuditlogEntity", "FieldName9", "FieldName9", true, "NVarChar", 150, 0, 0, false, "", null, typeof(System.String), 23);
			this.AddElementFieldMapping("AuditlogEntity", "FieldValue9", "FieldValue9", true, "NVarChar", 150, 0, 0, false, "", null, typeof(System.String), 24);
			this.AddElementFieldMapping("AuditlogEntity", "FieldName10", "FieldName10", true, "NVarChar", 150, 0, 0, false, "", null, typeof(System.String), 25);
			this.AddElementFieldMapping("AuditlogEntity", "FieldValue10", "FieldValue10", true, "NVarChar", 150, 0, 0, false, "", null, typeof(System.String), 26);
			this.AddElementFieldMapping("AuditlogEntity", "FieldName11", "FieldName11", true, "NVarChar", 150, 0, 0, false, "", null, typeof(System.String), 27);
			this.AddElementFieldMapping("AuditlogEntity", "FieldValue11", "FieldValue11", true, "NVarChar", 150, 0, 0, false, "", null, typeof(System.String), 28);
			this.AddElementFieldMapping("AuditlogEntity", "FieldName12", "FieldName12", true, "NVarChar", 150, 0, 0, false, "", null, typeof(System.String), 29);
			this.AddElementFieldMapping("AuditlogEntity", "FieldValue12", "FieldValue12", true, "NVarChar", 150, 0, 0, false, "", null, typeof(System.String), 30);
			this.AddElementFieldMapping("AuditlogEntity", "FieldName13", "FieldName13", true, "NVarChar", 150, 0, 0, false, "", null, typeof(System.String), 31);
			this.AddElementFieldMapping("AuditlogEntity", "FieldValue13", "FieldValue13", true, "NVarChar", 150, 0, 0, false, "", null, typeof(System.String), 32);
			this.AddElementFieldMapping("AuditlogEntity", "FieldName14", "FieldName14", true, "NVarChar", 150, 0, 0, false, "", null, typeof(System.String), 33);
			this.AddElementFieldMapping("AuditlogEntity", "FieldValue14", "FieldValue14", true, "NVarChar", 150, 0, 0, false, "", null, typeof(System.String), 34);
			this.AddElementFieldMapping("AuditlogEntity", "FieldName15", "FieldName15", true, "NVarChar", 150, 0, 0, false, "", null, typeof(System.String), 35);
			this.AddElementFieldMapping("AuditlogEntity", "FieldValue15", "FieldValue15", true, "NVarChar", 150, 0, 0, false, "", null, typeof(System.String), 36);
			this.AddElementFieldMapping("AuditlogEntity", "FieldName16", "FieldName16", true, "NVarChar", 150, 0, 0, false, "", null, typeof(System.String), 37);
			this.AddElementFieldMapping("AuditlogEntity", "FieldValue16", "FieldValue16", true, "NVarChar", 150, 0, 0, false, "", null, typeof(System.String), 38);
			this.AddElementFieldMapping("AuditlogEntity", "FieldName17", "FieldName17", true, "NVarChar", 150, 0, 0, false, "", null, typeof(System.String), 39);
			this.AddElementFieldMapping("AuditlogEntity", "FieldValue17", "FieldValue17", true, "NVarChar", 150, 0, 0, false, "", null, typeof(System.String), 40);
			this.AddElementFieldMapping("AuditlogEntity", "FieldName18", "FieldName18", true, "NVarChar", 150, 0, 0, false, "", null, typeof(System.String), 41);
			this.AddElementFieldMapping("AuditlogEntity", "FieldValue18", "FieldValue18", true, "NVarChar", 150, 0, 0, false, "", null, typeof(System.String), 42);
			this.AddElementFieldMapping("AuditlogEntity", "FieldName19", "FieldName19", true, "NVarChar", 150, 0, 0, false, "", null, typeof(System.String), 43);
			this.AddElementFieldMapping("AuditlogEntity", "FieldValue19", "FieldValue19", true, "NVarChar", 150, 0, 0, false, "", null, typeof(System.String), 44);
			this.AddElementFieldMapping("AuditlogEntity", "FieldName20", "FieldName20", true, "NVarChar", 150, 0, 0, false, "", null, typeof(System.String), 45);
			this.AddElementFieldMapping("AuditlogEntity", "FieldValue20", "FieldValue20", true, "NVarChar", 150, 0, 0, false, "", null, typeof(System.String), 46);
			this.AddElementFieldMapping("AuditlogEntity", "FieldsAndValues", "FieldsAndValues", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 47);
			this.AddElementFieldMapping("AuditlogEntity", "CallStack", "CallStack", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 48);
			this.AddElementFieldMapping("AuditlogEntity", "Page", "Page", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 49);
			this.AddElementFieldMapping("AuditlogEntity", "BreadcrumbSteps", "BreadcrumbSteps", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 50);
			this.AddElementFieldMapping("AuditlogEntity", "ActionBy", "ActionBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 51);
			this.AddElementFieldMapping("AuditlogEntity", "ActionPerformed", "ActionPerformed", true, "DateTime", 0, 0, 0, false, "", null, typeof(System.DateTime), 52);
			this.AddElementFieldMapping("AuditlogEntity", "CreatedBy", "CreatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 53);
			this.AddElementFieldMapping("AuditlogEntity", "UpdatedBy", "UpdatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 54);
			this.AddElementFieldMapping("AuditlogEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 55);
			this.AddElementFieldMapping("AuditlogEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 56);
			this.AddElementFieldMapping("AuditlogEntity", "ActionPerformedUTC", "ActionPerformedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 57);
		}

		/// <summary>Inits AvailabilityEntity's mappings</summary>
		private void InitAvailabilityEntityMappings()
		{
			this.AddElementMapping("AvailabilityEntity", @"Obymobi", @"dbo", "Availability", 15, 0);
			this.AddElementFieldMapping("AvailabilityEntity", "AvailabilityId", "AvailabilityId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("AvailabilityEntity", "CompanyId", "CompanyId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("AvailabilityEntity", "Name", "Name", false, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 2);
			this.AddElementFieldMapping("AvailabilityEntity", "StatusText", "Status", false, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 3);
			this.AddElementFieldMapping("AvailabilityEntity", "UpdatedBy", "UpdatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("AvailabilityEntity", "CreatedBy", "CreatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 5);
			this.AddElementFieldMapping("AvailabilityEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 6);
			this.AddElementFieldMapping("AvailabilityEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 7);
			this.AddElementFieldMapping("AvailabilityEntity", "Status", "Status", false, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 8);
			this.AddElementFieldMapping("AvailabilityEntity", "Url", "Url", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 9);
			this.AddElementFieldMapping("AvailabilityEntity", "ActionCategoryId", "ActionCategoryId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 10);
			this.AddElementFieldMapping("AvailabilityEntity", "ActionEntertainmentId", "ActionEntertainmentId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 11);
			this.AddElementFieldMapping("AvailabilityEntity", "ActionPageId", "ActionPageId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 12);
			this.AddElementFieldMapping("AvailabilityEntity", "ActionSiteId", "ActionSiteId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 13);
			this.AddElementFieldMapping("AvailabilityEntity", "ActionProductCategoryId", "ActionProductCategoryId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 14);
		}

		/// <summary>Inits AzureNotificationHubEntity's mappings</summary>
		private void InitAzureNotificationHubEntityMappings()
		{
			this.AddElementMapping("AzureNotificationHubEntity", @"Obymobi", @"dbo", "AzureNotificationHub", 7, 0);
			this.AddElementFieldMapping("AzureNotificationHubEntity", "NotificationHubId", "NotificationHubId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("AzureNotificationHubEntity", "ConnectionString", "ConnectionString", false, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 1);
			this.AddElementFieldMapping("AzureNotificationHubEntity", "HubPath", "HubPath", false, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 2);
			this.AddElementFieldMapping("AzureNotificationHubEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 3);
			this.AddElementFieldMapping("AzureNotificationHubEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 4);
			this.AddElementFieldMapping("AzureNotificationHubEntity", "CreatedBy", "CreatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 5);
			this.AddElementFieldMapping("AzureNotificationHubEntity", "UpdatedBy", "UpdatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 6);
		}

		/// <summary>Inits BrandEntity's mappings</summary>
		private void InitBrandEntityMappings()
		{
			this.AddElementMapping("BrandEntity", @"Obymobi", @"dbo", "Brand", 7, 0);
			this.AddElementFieldMapping("BrandEntity", "BrandId", "BrandId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("BrandEntity", "Name", "Name", false, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 1);
			this.AddElementFieldMapping("BrandEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 2);
			this.AddElementFieldMapping("BrandEntity", "CreatedUTC", "CreatedUTC", false, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 3);
			this.AddElementFieldMapping("BrandEntity", "CreatedBy", "CreatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("BrandEntity", "UpdatedBy", "UpdatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 5);
			this.AddElementFieldMapping("BrandEntity", "CultureCode", "CultureCode", true, "NVarChar", 10, 0, 0, false, "", null, typeof(System.String), 6);
		}

		/// <summary>Inits BrandCultureEntity's mappings</summary>
		private void InitBrandCultureEntityMappings()
		{
			this.AddElementMapping("BrandCultureEntity", @"Obymobi", @"dbo", "BrandCulture", 7, 0);
			this.AddElementFieldMapping("BrandCultureEntity", "BrandCultureId", "BrandCultureId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("BrandCultureEntity", "BrandId", "BrandId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("BrandCultureEntity", "CultureCode", "CultureCode", false, "NVarChar", 10, 0, 0, false, "", null, typeof(System.String), 2);
			this.AddElementFieldMapping("BrandCultureEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 3);
			this.AddElementFieldMapping("BrandCultureEntity", "CreatedBy", "CreatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("BrandCultureEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 5);
			this.AddElementFieldMapping("BrandCultureEntity", "UpdatedBy", "UpdatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 6);
		}

		/// <summary>Inits BusinesshoursEntity's mappings</summary>
		private void InitBusinesshoursEntityMappings()
		{
			this.AddElementMapping("BusinesshoursEntity", @"Obymobi", @"dbo", "Businesshours", 10, 0);
			this.AddElementFieldMapping("BusinesshoursEntity", "BusinesshoursId", "BusinesshoursId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("BusinesshoursEntity", "CompanyId", "CompanyId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("BusinesshoursEntity", "DayOfWeekAndTime", "DayOfWeekAndTime", false, "Char", 5, 0, 0, false, "", null, typeof(System.String), 2);
			this.AddElementFieldMapping("BusinesshoursEntity", "Opening", "Opening", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 3);
			this.AddElementFieldMapping("BusinesshoursEntity", "CreatedBy", "CreatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("BusinesshoursEntity", "UpdatedBy", "UpdatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 5);
			this.AddElementFieldMapping("BusinesshoursEntity", "PointOfInterestId", "PointOfInterestId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 6);
			this.AddElementFieldMapping("BusinesshoursEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 7);
			this.AddElementFieldMapping("BusinesshoursEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 8);
			this.AddElementFieldMapping("BusinesshoursEntity", "OutletId", "OutletId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 9);
		}

		/// <summary>Inits BusinesshoursexceptionEntity's mappings</summary>
		private void InitBusinesshoursexceptionEntityMappings()
		{
			this.AddElementMapping("BusinesshoursexceptionEntity", @"Obymobi", @"dbo", "Businesshoursexception", 9, 0);
			this.AddElementFieldMapping("BusinesshoursexceptionEntity", "BusinesshoursexceptionId", "BusinesshoursexceptionId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("BusinesshoursexceptionEntity", "CompanyId", "CompanyId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("BusinesshoursexceptionEntity", "Opened", "Opened", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 2);
			this.AddElementFieldMapping("BusinesshoursexceptionEntity", "FromDateTime", "FromDateTime", false, "DateTime", 0, 0, 0, false, "", null, typeof(System.DateTime), 3);
			this.AddElementFieldMapping("BusinesshoursexceptionEntity", "TillDateTime", "TillDateTime", false, "DateTime", 0, 0, 0, false, "", null, typeof(System.DateTime), 4);
			this.AddElementFieldMapping("BusinesshoursexceptionEntity", "CreatedBy", "CreatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 5);
			this.AddElementFieldMapping("BusinesshoursexceptionEntity", "UpdatedBy", "UpdatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 6);
			this.AddElementFieldMapping("BusinesshoursexceptionEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 7);
			this.AddElementFieldMapping("BusinesshoursexceptionEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 8);
		}

		/// <summary>Inits CategoryEntity's mappings</summary>
		private void InitCategoryEntityMappings()
		{
			this.AddElementMapping("CategoryEntity", @"Obymobi", @"dbo", "Category", 36, 0);
			this.AddElementFieldMapping("CategoryEntity", "CategoryId", "CategoryId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("CategoryEntity", "CompanyId", "CompanyId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("CategoryEntity", "ParentCategoryId", "ParentCategoryId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("CategoryEntity", "GenericcategoryId", "GenericcategoryId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("CategoryEntity", "Name", "Name", true, "NVarChar", 100, 0, 0, false, "", null, typeof(System.String), 4);
			this.AddElementFieldMapping("CategoryEntity", "SortOrder", "SortOrder", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 5);
			this.AddElementFieldMapping("CategoryEntity", "PoscategoryId", "PoscategoryId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 6);
			this.AddElementFieldMapping("CategoryEntity", "ProductId", "ProductId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 7);
			this.AddElementFieldMapping("CategoryEntity", "AvailableOnOtoucho", "AvailableOnOtoucho", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 8);
			this.AddElementFieldMapping("CategoryEntity", "AvailableOnObymobi", "AvailableOnObymobi", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 9);
			this.AddElementFieldMapping("CategoryEntity", "Rateable", "Rateable", true, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 10);
			this.AddElementFieldMapping("CategoryEntity", "Visible", "Visible", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 11);
			this.AddElementFieldMapping("CategoryEntity", "Type", "Type", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 12);
			this.AddElementFieldMapping("CategoryEntity", "CreatedBy", "CreatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 13);
			this.AddElementFieldMapping("CategoryEntity", "UpdatedBy", "UpdatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 14);
			this.AddElementFieldMapping("CategoryEntity", "RouteId", "RouteId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 15);
			this.AddElementFieldMapping("CategoryEntity", "MenuId", "MenuId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 16);
			this.AddElementFieldMapping("CategoryEntity", "HidePrices", "HidePrices", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 17);
			this.AddElementFieldMapping("CategoryEntity", "AnnouncementAction", "AnnouncementAction", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 18);
			this.AddElementFieldMapping("CategoryEntity", "Color", "Color", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 19);
			this.AddElementFieldMapping("CategoryEntity", "Description", "Description", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 20);
			this.AddElementFieldMapping("CategoryEntity", "Geofencing", "Geofencing", true, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 21);
			this.AddElementFieldMapping("CategoryEntity", "AllowFreeText", "AllowFreeText", true, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 22);
			this.AddElementFieldMapping("CategoryEntity", "ScheduleId", "ScheduleId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 23);
			this.AddElementFieldMapping("CategoryEntity", "ViewLayoutType", "ViewLayoutType", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 24);
			this.AddElementFieldMapping("CategoryEntity", "DeliveryLocationType", "DeliveryLocationType", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 25);
			this.AddElementFieldMapping("CategoryEntity", "SupportNotificationType", "SupportNotificationType", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 26);
			this.AddElementFieldMapping("CategoryEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 27);
			this.AddElementFieldMapping("CategoryEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 28);
			this.AddElementFieldMapping("CategoryEntity", "VisibilityType", "VisibilityType", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 29);
			this.AddElementFieldMapping("CategoryEntity", "ButtonText", "ButtonText", true, "NVarChar", 100, 0, 0, false, "", null, typeof(System.String), 30);
			this.AddElementFieldMapping("CategoryEntity", "CustomizeButtonText", "CustomizeButtonText", true, "NVarChar", 100, 0, 0, false, "", null, typeof(System.String), 31);
			this.AddElementFieldMapping("CategoryEntity", "ViewType", "ViewType", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 32);
			this.AddElementFieldMapping("CategoryEntity", "MenuItemsMustBeLinkedToExternalProduct", "MenuItemsMustBeLinkedToExternalProduct", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 33);
			this.AddElementFieldMapping("CategoryEntity", "ShowName", "ShowName", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 34);
			this.AddElementFieldMapping("CategoryEntity", "CoversType", "CoversType", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 35);
		}

		/// <summary>Inits CategoryAlterationEntity's mappings</summary>
		private void InitCategoryAlterationEntityMappings()
		{
			this.AddElementMapping("CategoryAlterationEntity", @"Obymobi", @"dbo", "CategoryAlteration", 10, 0);
			this.AddElementFieldMapping("CategoryAlterationEntity", "CategoryAlterationId", "CategoryAlterationId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("CategoryAlterationEntity", "CategoryId", "CategoryId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("CategoryAlterationEntity", "AlterationId", "AlterationId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("CategoryAlterationEntity", "SortOrder", "SortOrder", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("CategoryAlterationEntity", "CreatedBy", "CreatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("CategoryAlterationEntity", "UpdatedBy", "UpdatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 5);
			this.AddElementFieldMapping("CategoryAlterationEntity", "ParentCompanyId", "ParentCompanyId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 6);
			this.AddElementFieldMapping("CategoryAlterationEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 7);
			this.AddElementFieldMapping("CategoryAlterationEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 8);
			this.AddElementFieldMapping("CategoryAlterationEntity", "Version", "Version", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 9);
		}

		/// <summary>Inits CategoryLanguageEntity's mappings</summary>
		private void InitCategoryLanguageEntityMappings()
		{
			this.AddElementMapping("CategoryLanguageEntity", @"Obymobi", @"dbo", "CategoryLanguage", 12, 0);
			this.AddElementFieldMapping("CategoryLanguageEntity", "CategoryLanguageId", "CategoryLanguageId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("CategoryLanguageEntity", "CategoryId", "CategoryId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("CategoryLanguageEntity", "LanguageId", "LanguageId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("CategoryLanguageEntity", "Name", "Name", true, "NVarChar", 200, 0, 0, false, "", null, typeof(System.String), 3);
			this.AddElementFieldMapping("CategoryLanguageEntity", "CreatedBy", "CreatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("CategoryLanguageEntity", "UpdatedBy", "UpdatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 5);
			this.AddElementFieldMapping("CategoryLanguageEntity", "Description", "Description", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 6);
			this.AddElementFieldMapping("CategoryLanguageEntity", "OrderProcessedTitle", "OrderProcessedTitle", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 7);
			this.AddElementFieldMapping("CategoryLanguageEntity", "OrderProcessedMessage", "OrderProcessedMessage", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 8);
			this.AddElementFieldMapping("CategoryLanguageEntity", "ParentCompanyId", "ParentCompanyId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 9);
			this.AddElementFieldMapping("CategoryLanguageEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 10);
			this.AddElementFieldMapping("CategoryLanguageEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 11);
		}

		/// <summary>Inits CategorySuggestionEntity's mappings</summary>
		private void InitCategorySuggestionEntityMappings()
		{
			this.AddElementMapping("CategorySuggestionEntity", @"Obymobi", @"dbo", "CategorySuggestion", 11, 0);
			this.AddElementFieldMapping("CategorySuggestionEntity", "CategorySuggestionId", "CategorySuggestionId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("CategorySuggestionEntity", "CategoryId", "CategoryId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("CategorySuggestionEntity", "ProductId", "ProductId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("CategorySuggestionEntity", "SortOrder", "SortOrder", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("CategorySuggestionEntity", "CreatedBy", "CreatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("CategorySuggestionEntity", "UpdatedBy", "UpdatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 5);
			this.AddElementFieldMapping("CategorySuggestionEntity", "Checkout", "Checkout", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 6);
			this.AddElementFieldMapping("CategorySuggestionEntity", "ParentCompanyId", "ParentCompanyId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 7);
			this.AddElementFieldMapping("CategorySuggestionEntity", "ProductCategoryId", "ProductCategoryId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 8);
			this.AddElementFieldMapping("CategorySuggestionEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 9);
			this.AddElementFieldMapping("CategorySuggestionEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 10);
		}

		/// <summary>Inits CategoryTagEntity's mappings</summary>
		private void InitCategoryTagEntityMappings()
		{
			this.AddElementMapping("CategoryTagEntity", @"Obymobi", @"dbo", "CategoryTag", 6, 0);
			this.AddElementFieldMapping("CategoryTagEntity", "CategoryTagId", "CategoryTagId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("CategoryTagEntity", "CategoryId", "CategoryId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("CategoryTagEntity", "TagId", "TagId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("CategoryTagEntity", "CreatedUTC", "CreatedUTC", false, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 3);
			this.AddElementFieldMapping("CategoryTagEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 4);
			this.AddElementFieldMapping("CategoryTagEntity", "ParentCompanyId", "ParentCompanyId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 5);
		}

		/// <summary>Inits CheckoutMethodEntity's mappings</summary>
		private void InitCheckoutMethodEntityMappings()
		{
			this.AddElementMapping("CheckoutMethodEntity", @"Obymobi", @"dbo", "CheckoutMethod", 24, 0);
			this.AddElementFieldMapping("CheckoutMethodEntity", "CheckoutMethodId", "CheckoutMethodId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("CheckoutMethodEntity", "CompanyId", "CompanyId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("CheckoutMethodEntity", "OutletId", "OutletId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("CheckoutMethodEntity", "Active", "Active", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 3);
			this.AddElementFieldMapping("CheckoutMethodEntity", "Name", "Name", true, "NVarChar", 256, 0, 0, false, "", null, typeof(System.String), 4);
			this.AddElementFieldMapping("CheckoutMethodEntity", "Label", "Label", true, "NVarChar", 256, 0, 0, false, "", null, typeof(System.String), 5);
			this.AddElementFieldMapping("CheckoutMethodEntity", "Description", "Description", true, "NVarChar", 256, 0, 0, false, "", null, typeof(System.String), 6);
			this.AddElementFieldMapping("CheckoutMethodEntity", "ConfirmationActive", "ConfirmationActive", true, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 7);
			this.AddElementFieldMapping("CheckoutMethodEntity", "ConfirmationDescription", "ConfirmationDescription", true, "NVarChar", 512, 0, 0, false, "", null, typeof(System.String), 8);
			this.AddElementFieldMapping("CheckoutMethodEntity", "CheckoutType", "CheckoutType", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 9);
			this.AddElementFieldMapping("CheckoutMethodEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 10);
			this.AddElementFieldMapping("CheckoutMethodEntity", "CreatedBy", "CreatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 11);
			this.AddElementFieldMapping("CheckoutMethodEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 12);
			this.AddElementFieldMapping("CheckoutMethodEntity", "UpdatedBy", "UpdatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 13);
			this.AddElementFieldMapping("CheckoutMethodEntity", "ReceiptTemplateId", "ReceiptTemplateId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 14);
			this.AddElementFieldMapping("CheckoutMethodEntity", "PaymentIntegrationConfigurationId", "PaymentIntegrationConfigurationId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 15);
			this.AddElementFieldMapping("CheckoutMethodEntity", "OrderConfirmationNotificationMethod", "OrderConfirmationNotificationMethod", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 16);
			this.AddElementFieldMapping("CheckoutMethodEntity", "OrderPaymentPendingNotificationMethod", "OrderPaymentPendingNotificationMethod", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 17);
			this.AddElementFieldMapping("CheckoutMethodEntity", "OrderPaymentFailedNotificationMethod", "OrderPaymentFailedNotificationMethod", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 18);
			this.AddElementFieldMapping("CheckoutMethodEntity", "OrderReceiptNotificationMethod", "OrderReceiptNotificationMethod", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 19);
			this.AddElementFieldMapping("CheckoutMethodEntity", "TermsAndConditionsRequired", "TermsAndConditionsRequired", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 20);
			this.AddElementFieldMapping("CheckoutMethodEntity", "TermsAndConditionsLabel", "TermsAndConditionsLabel", true, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 21);
			this.AddElementFieldMapping("CheckoutMethodEntity", "CurrencyCode", "CurrencyCode", true, "NVarChar", 3, 0, 0, false, "", null, typeof(System.String), 22);
			this.AddElementFieldMapping("CheckoutMethodEntity", "CountryCode", "CountryCode", true, "NVarChar", 3, 0, 0, false, "", null, typeof(System.String), 23);
		}

		/// <summary>Inits CheckoutMethodDeliverypointgroupEntity's mappings</summary>
		private void InitCheckoutMethodDeliverypointgroupEntityMappings()
		{
			this.AddElementMapping("CheckoutMethodDeliverypointgroupEntity", @"Obymobi", @"dbo", "CheckoutMethodDeliverypointgroup", 7, 0);
			this.AddElementFieldMapping("CheckoutMethodDeliverypointgroupEntity", "DeliverypointgroupId", "DeliverypointgroupId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("CheckoutMethodDeliverypointgroupEntity", "CheckoutMethodId", "CheckoutMethodId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("CheckoutMethodDeliverypointgroupEntity", "ParentCompanyId", "ParentCompanyId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("CheckoutMethodDeliverypointgroupEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 3);
			this.AddElementFieldMapping("CheckoutMethodDeliverypointgroupEntity", "CreatedBy", "CreatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("CheckoutMethodDeliverypointgroupEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 5);
			this.AddElementFieldMapping("CheckoutMethodDeliverypointgroupEntity", "UpdatedBy", "UpdatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 6);
		}

		/// <summary>Inits ClientEntity's mappings</summary>
		private void InitClientEntityMappings()
		{
			this.AddElementMapping("ClientEntity", @"Obymobi", @"dbo", "Client", 35, 0);
			this.AddElementFieldMapping("ClientEntity", "ClientId", "ClientId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("ClientEntity", "CompanyId", "CompanyId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("ClientEntity", "ImageVersion", "ImageVersion", true, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 2);
			this.AddElementFieldMapping("ClientEntity", "Notes", "Notes", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 3);
			this.AddElementFieldMapping("ClientEntity", "DeliverypointGroupId", "DeliverypointGroupId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("ClientEntity", "OperationMode", "OperationMode", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 5);
			this.AddElementFieldMapping("ClientEntity", "LastStatus", "LastStatus", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 6);
			this.AddElementFieldMapping("ClientEntity", "LastStatusText", "LastStatusText", true, "NVarChar", 250, 0, 0, false, "", null, typeof(System.String), 7);
			this.AddElementFieldMapping("ClientEntity", "LogToFile", "LogToFile", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 8);
			this.AddElementFieldMapping("ClientEntity", "CreatedBy", "CreatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 9);
			this.AddElementFieldMapping("ClientEntity", "UpdatedBy", "UpdatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 10);
			this.AddElementFieldMapping("ClientEntity", "DeviceId", "DeviceId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 11);
			this.AddElementFieldMapping("ClientEntity", "HeartbeatPoll", "HeartbeatPoll", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 12);
			this.AddElementFieldMapping("ClientEntity", "OutOfChargeNotificationsSent", "OutOfChargeNotificationsSent", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 13);
			this.AddElementFieldMapping("ClientEntity", "DeliverypointId", "DeliverypointId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 14);
			this.AddElementFieldMapping("ClientEntity", "LastDeliverypointId", "LastDeliverypointId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 15);
			this.AddElementFieldMapping("ClientEntity", "LastOperationMode", "LastOperationMode", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 16);
			this.AddElementFieldMapping("ClientEntity", "LastStateOnline", "LastStateOnline", true, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 17);
			this.AddElementFieldMapping("ClientEntity", "LastStateOperationMode", "LastStateOperationMode", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 18);
			this.AddElementFieldMapping("ClientEntity", "BluetoothKeyboardConnected", "BluetoothKeyboardConnected", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 19);
			this.AddElementFieldMapping("ClientEntity", "RoomControlAreaId", "RoomControlAreaId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 20);
			this.AddElementFieldMapping("ClientEntity", "WakeUpTimeUtc", "WakeUpTimeUTC", true, "DateTime", 0, 0, 0, false, "", null, typeof(System.DateTime), 21);
			this.AddElementFieldMapping("ClientEntity", "OutOfChargeNotificationLastSentUTC", "OutOfChargeNotificationLastSentUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 22);
			this.AddElementFieldMapping("ClientEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 23);
			this.AddElementFieldMapping("ClientEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 24);
			this.AddElementFieldMapping("ClientEntity", "RoomControlConnected", "RoomControlConnected", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 25);
			this.AddElementFieldMapping("ClientEntity", "LoadedSuccessfully", "LoadedSuccessfully", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 26);
			this.AddElementFieldMapping("ClientEntity", "WakeUpTimeUtcUTC", "StatusUpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 27);
			this.AddElementFieldMapping("ClientEntity", "DoNotDisturbActive", "DoNotDisturbActive", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 28);
			this.AddElementFieldMapping("ClientEntity", "ServiceRoomActive", "ServiceRoomActive", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 29);
			this.AddElementFieldMapping("ClientEntity", "LastUserInteraction", "LastUserInteraction", true, "BigInt", 0, 19, 0, false, "", null, typeof(System.Int64), 30);
			this.AddElementFieldMapping("ClientEntity", "StatusUpdatedUTC", "StatusUpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 31);
			this.AddElementFieldMapping("ClientEntity", "LastWifiSsid", "LastWifiSsid", true, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 32);
			this.AddElementFieldMapping("ClientEntity", "IsTestClient", "IsTestClient", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 33);
			this.AddElementFieldMapping("ClientEntity", "LastApiVersion", "LastApiVersion", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 34);
		}

		/// <summary>Inits ClientConfigurationEntity's mappings</summary>
		private void InitClientConfigurationEntityMappings()
		{
			this.AddElementMapping("ClientConfigurationEntity", @"Obymobi", @"dbo", "ClientConfiguration", 63, 0);
			this.AddElementFieldMapping("ClientConfigurationEntity", "ClientConfigurationId", "ClientConfigurationId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("ClientConfigurationEntity", "CompanyId", "CompanyId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("ClientConfigurationEntity", "Name", "Name", false, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 2);
			this.AddElementFieldMapping("ClientConfigurationEntity", "DeliverypointgroupId", "DeliverypointgroupId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("ClientConfigurationEntity", "MenuId", "MenuId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("ClientConfigurationEntity", "UIModeId", "UIModeId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 5);
			this.AddElementFieldMapping("ClientConfigurationEntity", "UIThemeId", "UIThemeId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 6);
			this.AddElementFieldMapping("ClientConfigurationEntity", "UIScheduleId", "UIScheduleId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 7);
			this.AddElementFieldMapping("ClientConfigurationEntity", "RoomControlConfigurationId", "RoomControlConfigurationId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 8);
			this.AddElementFieldMapping("ClientConfigurationEntity", "WifiConfigurationId", "WifiConfigurationId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 9);
			this.AddElementFieldMapping("ClientConfigurationEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 10);
			this.AddElementFieldMapping("ClientConfigurationEntity", "CreatedBy", "CreatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 11);
			this.AddElementFieldMapping("ClientConfigurationEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 12);
			this.AddElementFieldMapping("ClientConfigurationEntity", "UpdatedBy", "UpdatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 13);
			this.AddElementFieldMapping("ClientConfigurationEntity", "Pincode", "Pincode", true, "NVarChar", 25, 0, 0, false, "", null, typeof(System.String), 14);
			this.AddElementFieldMapping("ClientConfigurationEntity", "PincodeSU", "PincodeSU", true, "NVarChar", 25, 0, 0, false, "", null, typeof(System.String), 15);
			this.AddElementFieldMapping("ClientConfigurationEntity", "PincodeGM", "PincodeGM", true, "NVarChar", 25, 0, 0, false, "", null, typeof(System.String), 16);
			this.AddElementFieldMapping("ClientConfigurationEntity", "HidePrices", "HidePrices", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 17);
			this.AddElementFieldMapping("ClientConfigurationEntity", "ShowCurrencySymbol", "ShowCurrencySymbol", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 18);
			this.AddElementFieldMapping("ClientConfigurationEntity", "ClearSessionOnTimeout", "ClearSessionOnTimeout", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 19);
			this.AddElementFieldMapping("ClientConfigurationEntity", "ResetTimeout", "ResetTimeout", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 20);
			this.AddElementFieldMapping("ClientConfigurationEntity", "ScreenTimeoutInterval", "ScreenTimeoutInterval", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 21);
			this.AddElementFieldMapping("ClientConfigurationEntity", "DimLevelDull", "DimLevelDull", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 22);
			this.AddElementFieldMapping("ClientConfigurationEntity", "DimLevelMedium", "DimLevelMedium", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 23);
			this.AddElementFieldMapping("ClientConfigurationEntity", "DimLevelBright", "DimLevelBright", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 24);
			this.AddElementFieldMapping("ClientConfigurationEntity", "DockedDimLevelDull", "DockedDimLevelDull", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 25);
			this.AddElementFieldMapping("ClientConfigurationEntity", "DockedDimLevelMedium", "DockedDimLevelMedium", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 26);
			this.AddElementFieldMapping("ClientConfigurationEntity", "DockedDimLevelBright", "DockedDimLevelBright", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 27);
			this.AddElementFieldMapping("ClientConfigurationEntity", "PowerSaveTimeout", "PowerSaveTimeout", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 28);
			this.AddElementFieldMapping("ClientConfigurationEntity", "PowerSaveLevel", "PowerSaveLevel", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 29);
			this.AddElementFieldMapping("ClientConfigurationEntity", "OutOfChargeLevel", "OutOfChargeLevel", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 30);
			this.AddElementFieldMapping("ClientConfigurationEntity", "DailyOrderReset", "DailyOrderReset", false, "Char", 4, 0, 0, false, "", null, typeof(System.String), 31);
			this.AddElementFieldMapping("ClientConfigurationEntity", "SleepTime", "SleepTime", false, "Char", 4, 0, 0, false, "", null, typeof(System.String), 32);
			this.AddElementFieldMapping("ClientConfigurationEntity", "WakeUpTime", "WakeUpTime", false, "Char", 4, 0, 0, false, "", null, typeof(System.String), 33);
			this.AddElementFieldMapping("ClientConfigurationEntity", "HomepageSlideshowInterval", "HomepageSlideshowInterval", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 34);
			this.AddElementFieldMapping("ClientConfigurationEntity", "IsChargerRemovedDialogEnabled", "IsChargerRemovedDialogEnabled", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 35);
			this.AddElementFieldMapping("ClientConfigurationEntity", "IsChargerRemovedReminderDialogEnabled", "IsChargerRemovedReminderDialogEnabled", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 36);
			this.AddElementFieldMapping("ClientConfigurationEntity", "PowerButtonHardBehaviour", "PowerButtonHardBehaviour", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 37);
			this.AddElementFieldMapping("ClientConfigurationEntity", "PowerButtonSoftBehaviour", "PowerButtonSoftBehaviour", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 38);
			this.AddElementFieldMapping("ClientConfigurationEntity", "ScreenOffMode", "ScreenOffMode", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 39);
			this.AddElementFieldMapping("ClientConfigurationEntity", "ScreensaverMode", "ScreensaverMode", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 40);
			this.AddElementFieldMapping("ClientConfigurationEntity", "DeviceRebootMethod", "DeviceRebootMethod", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 41);
			this.AddElementFieldMapping("ClientConfigurationEntity", "RoomserviceCharge", "RoomserviceCharge", true, "Money", 0, 19, 4, false, "", null, typeof(System.Decimal), 42);
			this.AddElementFieldMapping("ClientConfigurationEntity", "AllowFreeText", "AllowFreeText", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 43);
			this.AddElementFieldMapping("ClientConfigurationEntity", "BrowserAgeVerificationEnabled", "BrowserAgeVerificationEnabled", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 44);
			this.AddElementFieldMapping("ClientConfigurationEntity", "GooglePrinterId", "GooglePrinterId", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 45);
			this.AddElementFieldMapping("ClientConfigurationEntity", "PriceScheduleId", "PriceScheduleId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 46);
			this.AddElementFieldMapping("ClientConfigurationEntity", "AdvertisementConfigurationId", "AdvertisementConfigurationId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 47);
			this.AddElementFieldMapping("ClientConfigurationEntity", "EntertainmentConfigurationId", "EntertainmentConfigurationId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 48);
			this.AddElementFieldMapping("ClientConfigurationEntity", "BrowserAgeVerificationLayout", "BrowserAgeVerificationLayout", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 49);
			this.AddElementFieldMapping("ClientConfigurationEntity", "RestartApplicationDialogEnabled", "RestartApplicationDialogEnabled", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 50);
			this.AddElementFieldMapping("ClientConfigurationEntity", "RebootDeviceDialogEnabled", "RebootDeviceDialogEnabled", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 51);
			this.AddElementFieldMapping("ClientConfigurationEntity", "RestartTimeoutSeconds", "RestartTimeoutSeconds", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 52);
			this.AddElementFieldMapping("ClientConfigurationEntity", "PmsIntegration", "PmsIntegration", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 53);
			this.AddElementFieldMapping("ClientConfigurationEntity", "PmsAllowShowBill", "PmsAllowShowBill", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 54);
			this.AddElementFieldMapping("ClientConfigurationEntity", "PmsAllowExpressCheckout", "PmsAllowExpressCheckout", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 55);
			this.AddElementFieldMapping("ClientConfigurationEntity", "PmsAllowShowGuestName", "PmsAllowShowGuestName", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 56);
			this.AddElementFieldMapping("ClientConfigurationEntity", "PmsLockClientWhenNotCheckedIn", "PmsLockClientWhenNotCheckedIn", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 57);
			this.AddElementFieldMapping("ClientConfigurationEntity", "PmsWelcomeTimeoutMinutes", "PmsWelcomeTimeoutMinutes", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 58);
			this.AddElementFieldMapping("ClientConfigurationEntity", "PmsCheckoutCompleteTimeoutMinutes", "PmsCheckoutCompleteTimeoutMinutes", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 59);
			this.AddElementFieldMapping("ClientConfigurationEntity", "IsOrderitemAddedDialogEnabled", "IsOrderitemAddedDialogEnabled", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 60);
			this.AddElementFieldMapping("ClientConfigurationEntity", "OrderCompletedNotificationEnabled", "OrderCompletedNotificationEnabled", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 61);
			this.AddElementFieldMapping("ClientConfigurationEntity", "IsClearBasketDialogEnabled", "IsClearBasketDialogEnabled", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 62);
		}

		/// <summary>Inits ClientConfigurationRouteEntity's mappings</summary>
		private void InitClientConfigurationRouteEntityMappings()
		{
			this.AddElementMapping("ClientConfigurationRouteEntity", @"Obymobi", @"dbo", "ClientConfigurationRoute", 9, 0);
			this.AddElementFieldMapping("ClientConfigurationRouteEntity", "ClientConfigurationRouteId", "ClientConfigurationRouteId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("ClientConfigurationRouteEntity", "ParentCompanyId", "ParentCompanyId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("ClientConfigurationRouteEntity", "ClientConfigurationId", "ClientConfigurationId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("ClientConfigurationRouteEntity", "RouteId", "RouteId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("ClientConfigurationRouteEntity", "Type", "Type", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("ClientConfigurationRouteEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 5);
			this.AddElementFieldMapping("ClientConfigurationRouteEntity", "CreatedBy", "CreatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 6);
			this.AddElementFieldMapping("ClientConfigurationRouteEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 7);
			this.AddElementFieldMapping("ClientConfigurationRouteEntity", "UpdatedBy", "UpdatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 8);
		}

		/// <summary>Inits ClientEntertainmentEntity's mappings</summary>
		private void InitClientEntertainmentEntityMappings()
		{
			this.AddElementMapping("ClientEntertainmentEntity", @"Obymobi", @"dbo", "ClientEntertainment", 8, 0);
			this.AddElementFieldMapping("ClientEntertainmentEntity", "ClientEntertainmentId", "ClientEntertainmentId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("ClientEntertainmentEntity", "ClientId", "ClientId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("ClientEntertainmentEntity", "EntertainmentId", "EntertainmentId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("ClientEntertainmentEntity", "CreatedBy", "CreatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("ClientEntertainmentEntity", "UpdatedBy", "UpdatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("ClientEntertainmentEntity", "ParentCompanyId", "ParentCompanyId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 5);
			this.AddElementFieldMapping("ClientEntertainmentEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 6);
			this.AddElementFieldMapping("ClientEntertainmentEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 7);
		}

		/// <summary>Inits ClientLogEntity's mappings</summary>
		private void InitClientLogEntityMappings()
		{
			this.AddElementMapping("ClientLogEntity", @"Obymobi", @"dbo", "ClientLog", 19, 0);
			this.AddElementFieldMapping("ClientLogEntity", "ClientLogId", "ClientLogId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("ClientLogEntity", "ClientId", "ClientId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("ClientLogEntity", "Type", "Type", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("ClientLogEntity", "Message", "Message", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 3);
			this.AddElementFieldMapping("ClientLogEntity", "FromStatus", "FromStatus", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("ClientLogEntity", "ToStatus", "ToStatus", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 5);
			this.AddElementFieldMapping("ClientLogEntity", "FromOperationMode", "FromOperationMode", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 6);
			this.AddElementFieldMapping("ClientLogEntity", "ToOperationMode", "ToOperationMode", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 7);
			this.AddElementFieldMapping("ClientLogEntity", "CreatedBy", "CreatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 8);
			this.AddElementFieldMapping("ClientLogEntity", "UpdatedBy", "UpdatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 9);
			this.AddElementFieldMapping("ClientLogEntity", "FromOperationModeText", "FromOperationModeText", true, "NVarChar", 250, 0, 0, false, "", null, typeof(System.String), 10);
			this.AddElementFieldMapping("ClientLogEntity", "FromStatusText", "FromStatusText", true, "NVarChar", 250, 0, 0, false, "", null, typeof(System.String), 11);
			this.AddElementFieldMapping("ClientLogEntity", "ToOperationModeText", "ToOperationModeText", true, "NVarChar", 250, 0, 0, false, "", null, typeof(System.String), 12);
			this.AddElementFieldMapping("ClientLogEntity", "ToStatusText", "ToStatusText", true, "NVarChar", 250, 0, 0, false, "", null, typeof(System.String), 13);
			this.AddElementFieldMapping("ClientLogEntity", "TypeText", "TypeText", true, "NVarChar", 250, 0, 0, false, "", null, typeof(System.String), 14);
			this.AddElementFieldMapping("ClientLogEntity", "ClientLogFileId", "ClientLogFileId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 15);
			this.AddElementFieldMapping("ClientLogEntity", "ParentCompanyId", "ParentCompanyId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 16);
			this.AddElementFieldMapping("ClientLogEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 17);
			this.AddElementFieldMapping("ClientLogEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 18);
		}

		/// <summary>Inits ClientLogFileEntity's mappings</summary>
		private void InitClientLogFileEntityMappings()
		{
			this.AddElementMapping("ClientLogFileEntity", @"Obymobi", @"dbo", "ClientLogFile", 8, 0);
			this.AddElementFieldMapping("ClientLogFileEntity", "ClientLogFileId", "ClientLogFileId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("ClientLogFileEntity", "Message", "Message", false, "Text", 2147483647, 0, 0, false, "", null, typeof(System.String), 1);
			this.AddElementFieldMapping("ClientLogFileEntity", "CreatedBy", "CreatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("ClientLogFileEntity", "UpdatedBy", "UpdatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("ClientLogFileEntity", "LogDate", "LogDate", false, "DateTime", 0, 0, 0, false, "", null, typeof(System.DateTime), 4);
			this.AddElementFieldMapping("ClientLogFileEntity", "Application", "Application", true, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 5);
			this.AddElementFieldMapping("ClientLogFileEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 6);
			this.AddElementFieldMapping("ClientLogFileEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 7);
		}

		/// <summary>Inits ClientStateEntity's mappings</summary>
		private void InitClientStateEntityMappings()
		{
			this.AddElementMapping("ClientStateEntity", @"Obymobi", @"dbo", "ClientState", 13, 0);
			this.AddElementFieldMapping("ClientStateEntity", "ClientStateId", "ClientStateId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("ClientStateEntity", "ClientId", "ClientId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("ClientStateEntity", "Online", "Online", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 2);
			this.AddElementFieldMapping("ClientStateEntity", "OperationMode", "OperationMode", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("ClientStateEntity", "CreatedBy", "CreatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("ClientStateEntity", "UpdatedBy", "UpdatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 5);
			this.AddElementFieldMapping("ClientStateEntity", "LastBatteryLevel", "LastBatteryLevel", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 6);
			this.AddElementFieldMapping("ClientStateEntity", "LastIsCharging", "LastIsCharging", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 7);
			this.AddElementFieldMapping("ClientStateEntity", "Message", "Message", true, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 8);
			this.AddElementFieldMapping("ClientStateEntity", "TimeSpanTicks", "TimeSpanTicks", false, "BigInt", 0, 19, 0, false, "", null, typeof(System.Int64), 9);
			this.AddElementFieldMapping("ClientStateEntity", "ParentCompanyId", "ParentCompanyId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 10);
			this.AddElementFieldMapping("ClientStateEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 11);
			this.AddElementFieldMapping("ClientStateEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 12);
		}

		/// <summary>Inits CloudApplicationVersionEntity's mappings</summary>
		private void InitCloudApplicationVersionEntityMappings()
		{
			this.AddElementMapping("CloudApplicationVersionEntity", @"Obymobi", @"dbo", "CloudApplicationVersion", 8, 0);
			this.AddElementFieldMapping("CloudApplicationVersionEntity", "CloudApplicationVersionId", "CloudApplicationVersionId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("CloudApplicationVersionEntity", "Version", "Version", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("CloudApplicationVersionEntity", "CloudApplication", "CloudApplication", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("CloudApplicationVersionEntity", "MachineName", "MachineName", false, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 3);
			this.AddElementFieldMapping("CloudApplicationVersionEntity", "CreatedBy", "CreatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("CloudApplicationVersionEntity", "UpdatedBy", "UpdatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 5);
			this.AddElementFieldMapping("CloudApplicationVersionEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 6);
			this.AddElementFieldMapping("CloudApplicationVersionEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 7);
		}

		/// <summary>Inits CloudProcessingTaskEntity's mappings</summary>
		private void InitCloudProcessingTaskEntityMappings()
		{
			this.AddElementMapping("CloudProcessingTaskEntity", @"Obymobi", @"dbo", "CloudProcessingTask", 26, 0);
			this.AddElementFieldMapping("CloudProcessingTaskEntity", "CloudProcessingTaskId", "CloudProcessingTaskId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("CloudProcessingTaskEntity", "Type", "Type", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("CloudProcessingTaskEntity", "PathFormat", "PathFormat", false, "NVarChar", 260, 0, 0, false, "", null, typeof(System.String), 2);
			this.AddElementFieldMapping("CloudProcessingTaskEntity", "Action", "Action", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("CloudProcessingTaskEntity", "Errors", "Errors", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 4);
			this.AddElementFieldMapping("CloudProcessingTaskEntity", "Attempts", "Attempts", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 5);
			this.AddElementFieldMapping("CloudProcessingTaskEntity", "EntertainmentFileId", "EntertainmentFileId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 6);
			this.AddElementFieldMapping("CloudProcessingTaskEntity", "CreatedBy", "CreatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 7);
			this.AddElementFieldMapping("CloudProcessingTaskEntity", "UpdatedBy", "UpdatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 8);
			this.AddElementFieldMapping("CloudProcessingTaskEntity", "Container", "Container", true, "NVarChar", 100, 0, 0, false, "", null, typeof(System.String), 9);
			this.AddElementFieldMapping("CloudProcessingTaskEntity", "EntertainmentId", "EntertainmentId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 10);
			this.AddElementFieldMapping("CloudProcessingTaskEntity", "WeatherId", "WeatherId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 11);
			this.AddElementFieldMapping("CloudProcessingTaskEntity", "ReleaseId", "ReleaseId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 12);
			this.AddElementFieldMapping("CloudProcessingTaskEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 13);
			this.AddElementFieldMapping("CloudProcessingTaskEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 14);
			this.AddElementFieldMapping("CloudProcessingTaskEntity", "LastAttemptUTC", "LastAttemptUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 15);
			this.AddElementFieldMapping("CloudProcessingTaskEntity", "NextAttemptUTC", "NextAttemptUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 16);
			this.AddElementFieldMapping("CloudProcessingTaskEntity", "CompletedOnAmazonUTC", "CompletedOnAmazonUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 17);
			this.AddElementFieldMapping("CloudProcessingTaskEntity", "Priority", "Priority", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 18);
			this.AddElementFieldMapping("CloudProcessingTaskEntity", "MediaRatioTypeMediaIdNonRelationCopy", "MediaRatioTypeMediaIdNonRelationCopy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 19);
			this.AddElementFieldMapping("CloudProcessingTaskEntity", "RelatedToCompanyId", "RelatedToCompanyId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 20);
			this.AddElementFieldMapping("CloudProcessingTaskEntity", "MediaRatioTypeMediaId", "MediaRatioTypeMediaId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 21);
			this.AddElementFieldMapping("CloudProcessingTaskEntity", "QueuedUTC", "QueuedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 22);
			this.AddElementFieldMapping("CloudProcessingTaskEntity", "FileContent", "FileContent", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 23);
			this.AddElementFieldMapping("CloudProcessingTaskEntity", "FileContentTimestamp", "FileContentTimestamp", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 24);
			this.AddElementFieldMapping("CloudProcessingTaskEntity", "FileContentTimestampKey", "FileContentTimestampKey", true, "VarChar", 255, 0, 0, false, "", null, typeof(System.String), 25);
		}

		/// <summary>Inits CloudStorageAccountEntity's mappings</summary>
		private void InitCloudStorageAccountEntityMappings()
		{
			this.AddElementMapping("CloudStorageAccountEntity", @"Obymobi", @"dbo", "CloudStorageAccount", 10, 0);
			this.AddElementFieldMapping("CloudStorageAccountEntity", "CloudStorageAccountId", "CloudStorageAccountId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("CloudStorageAccountEntity", "CompanyId", "CompanyId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("CloudStorageAccountEntity", "Type", "Type", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("CloudStorageAccountEntity", "AccountName", "AccountName", false, "NVarChar", 100, 0, 0, false, "", null, typeof(System.String), 3);
			this.AddElementFieldMapping("CloudStorageAccountEntity", "AccountPassword", "AccountPassword", false, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 4);
			this.AddElementFieldMapping("CloudStorageAccountEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 5);
			this.AddElementFieldMapping("CloudStorageAccountEntity", "CreatedBy", "CreatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 6);
			this.AddElementFieldMapping("CloudStorageAccountEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 7);
			this.AddElementFieldMapping("CloudStorageAccountEntity", "UpdatedBy", "UpdatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 8);
			this.AddElementFieldMapping("CloudStorageAccountEntity", "UserName", "UserName", false, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 9);
		}

		/// <summary>Inits CompanyEntity's mappings</summary>
		private void InitCompanyEntityMappings()
		{
			this.AddElementMapping("CompanyEntity", @"Obymobi", @"dbo", "Company", 115, 0);
			this.AddElementFieldMapping("CompanyEntity", "CompanyId", "CompanyId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("CompanyEntity", "CompanyOwnerId", "CompanyOwnerId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("CompanyEntity", "CurrencyId", "CurrencyId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("CompanyEntity", "LanguageId", "LanguageId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("CompanyEntity", "Name", "Name", true, "NVarChar", 100, 0, 0, false, "", null, typeof(System.String), 4);
			this.AddElementFieldMapping("CompanyEntity", "NameWithoutDiacritics", "NameWithoutDiacritics", true, "NVarChar", 100, 0, 0, false, "", null, typeof(System.String), 5);
			this.AddElementFieldMapping("CompanyEntity", "Code", "Code", true, "VarChar", 10, 0, 0, false, "", null, typeof(System.String), 6);
			this.AddElementFieldMapping("CompanyEntity", "Addressline1", "Addressline1", true, "NVarChar", 200, 0, 0, false, "", null, typeof(System.String), 7);
			this.AddElementFieldMapping("CompanyEntity", "Addressline2", "Addressline2", true, "NVarChar", 200, 0, 0, false, "", null, typeof(System.String), 8);
			this.AddElementFieldMapping("CompanyEntity", "Addressline3", "Addressline3", true, "NVarChar", 200, 0, 0, false, "", null, typeof(System.String), 9);
			this.AddElementFieldMapping("CompanyEntity", "Zipcode", "Zipcode", true, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 10);
			this.AddElementFieldMapping("CompanyEntity", "City", "City", true, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 11);
			this.AddElementFieldMapping("CompanyEntity", "CountryId", "CountryId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 12);
			this.AddElementFieldMapping("CompanyEntity", "Latitude", "Latitude", true, "Float", 0, 38, 0, false, "", null, typeof(System.Double), 13);
			this.AddElementFieldMapping("CompanyEntity", "Longitude", "Longitude", true, "Float", 0, 38, 0, false, "", null, typeof(System.Double), 14);
			this.AddElementFieldMapping("CompanyEntity", "Telephone", "Telephone", true, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 15);
			this.AddElementFieldMapping("CompanyEntity", "Fax", "Fax", true, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 16);
			this.AddElementFieldMapping("CompanyEntity", "Website", "Website", true, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 17);
			this.AddElementFieldMapping("CompanyEntity", "Email", "Email", true, "NVarChar", 100, 0, 0, false, "", null, typeof(System.String), 18);
			this.AddElementFieldMapping("CompanyEntity", "StandardReceiptTypes", "StandardReceiptTypes", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 19);
			this.AddElementFieldMapping("CompanyEntity", "GoogleAnalyticsId", "GoogleAnalyticsId", true, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 20);
			this.AddElementFieldMapping("CompanyEntity", "FacebookPlaceId", "FacebookPlaceId", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 21);
			this.AddElementFieldMapping("CompanyEntity", "FoursquareVenueId", "FoursquareVenueId", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 22);
			this.AddElementFieldMapping("CompanyEntity", "ConfirmationCodeDeliveryType", "ConfirmationCodeDeliveryType", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 23);
			this.AddElementFieldMapping("CompanyEntity", "StandardCheckInterval", "StandardCheckInterval", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 24);
			this.AddElementFieldMapping("CompanyEntity", "SMSOrdering", "SMSOrdering", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 25);
			this.AddElementFieldMapping("CompanyEntity", "Description", "Description", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 26);
			this.AddElementFieldMapping("CompanyEntity", "AutomaticClientOperationModes", "AutomaticClientOperationModes", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 27);
			this.AddElementFieldMapping("CompanyEntity", "AvailableOnOtoucho", "AvailableOnOtoucho", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 28);
			this.AddElementFieldMapping("CompanyEntity", "AvailableOnObymobi", "AvailableOnObymobi", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 29);
			this.AddElementFieldMapping("CompanyEntity", "UseMonitoring", "UseMonitoring", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 30);
			this.AddElementFieldMapping("CompanyEntity", "SystemType", "SystemType", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 31);
			this.AddElementFieldMapping("CompanyEntity", "AllowRequestService", "AllowRequestService", true, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 32);
			this.AddElementFieldMapping("CompanyEntity", "AllowRateProduct", "AllowRateProduct", true, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 33);
			this.AddElementFieldMapping("CompanyEntity", "AllowRateService", "AllowRateService", true, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 34);
			this.AddElementFieldMapping("CompanyEntity", "CreatedBy", "CreatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 35);
			this.AddElementFieldMapping("CompanyEntity", "UpdatedBy", "UpdatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 36);
			this.AddElementFieldMapping("CompanyEntity", "RouteId", "RouteId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 37);
			this.AddElementFieldMapping("CompanyEntity", "SupportpoolId", "SupportpoolId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 38);
			this.AddElementFieldMapping("CompanyEntity", "AllowFreeText", "AllowFreeText", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 39);
			this.AddElementFieldMapping("CompanyEntity", "UseBillButton", "UseBillButton", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 40);
			this.AddElementFieldMapping("CompanyEntity", "Salt", "Salt", true, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 41);
			this.AddElementFieldMapping("CompanyEntity", "MaxDownloadConnections", "MaxDownloadConnections", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 42);
			this.AddElementFieldMapping("CompanyEntity", "ClientApplicationVersion", "ClientApplicationVersion", true, "NVarChar", 250, 0, 0, false, "", null, typeof(System.String), 43);
			this.AddElementFieldMapping("CompanyEntity", "OsVersion", "OsVersion", true, "NVarChar", 250, 0, 0, false, "", null, typeof(System.String), 44);
			this.AddElementFieldMapping("CompanyEntity", "TerminalApplicationVersion", "TerminalApplicationVersion", true, "NVarChar", 250, 0, 0, false, "", null, typeof(System.String), 45);
			this.AddElementFieldMapping("CompanyEntity", "DeviceRebootMethod", "DeviceRebootMethod", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 46);
			this.AddElementFieldMapping("CompanyEntity", "CorrespondenceEmail", "CorrespondenceEmail", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 47);
			this.AddElementFieldMapping("CompanyEntity", "ShowCurrencySymbol", "ShowCurrencySymbol", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 48);
			this.AddElementFieldMapping("CompanyEntity", "MobileOrderingDisabled", "MobileOrderingDisabled", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 49);
			this.AddElementFieldMapping("CompanyEntity", "GeoFencingEnabled", "GeoFencingEnabled", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 50);
			this.AddElementFieldMapping("CompanyEntity", "GeoFencingRadius", "GeoFencingRadius", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 51);
			this.AddElementFieldMapping("CompanyEntity", "Pincode", "Pincode", true, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 52);
			this.AddElementFieldMapping("CompanyEntity", "PincodeSU", "PincodeSU", true, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 53);
			this.AddElementFieldMapping("CompanyEntity", "PincodeGM", "PincodeGM", true, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 54);
			this.AddElementFieldMapping("CompanyEntity", "ActionButtonId", "ActionButtonId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 55);
			this.AddElementFieldMapping("CompanyEntity", "CostIndicationType", "CostIndicationType", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 56);
			this.AddElementFieldMapping("CompanyEntity", "CostIndicationValue", "CostIndicationValue", true, "Money", 0, 19, 4, false, "", null, typeof(System.Decimal), 57);
			this.AddElementFieldMapping("CompanyEntity", "DescriptionSingleLine", "DescriptionSingleLine", true, "NVarChar", 250, 0, 0, false, "", null, typeof(System.String), 58);
			this.AddElementFieldMapping("CompanyEntity", "ActionButtonUrlMobile", "ActionButtonUrlMobile", true, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 59);
			this.AddElementFieldMapping("CompanyEntity", "ActionButtonUrlTablet", "ActionButtonUrlTablet", true, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 60);
			this.AddElementFieldMapping("CompanyEntity", "CometHandlerType", "CometHandlerType", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 61);
			this.AddElementFieldMapping("CompanyEntity", "AgentVersion", "AgentVersion", true, "NVarChar", 250, 0, 0, false, "", null, typeof(System.String), 62);
			this.AddElementFieldMapping("CompanyEntity", "SupportToolsVersion", "SupportToolsVersion", true, "NVarChar", 250, 0, 0, false, "", null, typeof(System.String), 63);
			this.AddElementFieldMapping("CompanyEntity", "PercentageDownForNotification", "PercentageDownForNotification", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 64);
			this.AddElementFieldMapping("CompanyEntity", "PercentageDownIntervalHours", "PercentageDownIntervalHours", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 65);
			this.AddElementFieldMapping("CompanyEntity", "LatitudeOverridden", "LatitudeOverridden", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 66);
			this.AddElementFieldMapping("CompanyEntity", "LongitudeOverriden", "LongitudeOverriden", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 67);
			this.AddElementFieldMapping("CompanyEntity", "SystemPassword", "SystemPassword", true, "VarChar", 50, 0, 0, false, "", null, typeof(System.String), 68);
			this.AddElementFieldMapping("CompanyEntity", "PercentageDownJumpForNotification", "PercentageDownJumpForNotification", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 69);
			this.AddElementFieldMapping("CompanyEntity", "TemperatureUnit", "TemperatureUnit", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 70);
			this.AddElementFieldMapping("CompanyEntity", "ClockMode", "ClockMode", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 71);
			this.AddElementFieldMapping("CompanyEntity", "WeatherClockWidgetMode", "WeatherClockWidgetMode", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 72);
			this.AddElementFieldMapping("CompanyEntity", "TimeZoneId", "TimeZoneId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 73);
			this.AddElementFieldMapping("CompanyEntity", "SaltPms", "SaltPms", true, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 74);
			this.AddElementFieldMapping("CompanyEntity", "AlterationDialogMode", "AlterationDialogMode", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 75);
			this.AddElementFieldMapping("CompanyEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 76);
			this.AddElementFieldMapping("CompanyEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 77);
			this.AddElementFieldMapping("CompanyEntity", "PercentageDownNotificationLastSentUTC", "PercentageDownNotificationLastSentUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 78);
			this.AddElementFieldMapping("CompanyEntity", "PercentageDownJumpNotificationLastSentUTC", "PercentageDownJumpNotificationLastSentUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 79);
			this.AddElementFieldMapping("CompanyEntity", "LastDailyReportSendUTC", "LastDailyReportSendUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 80);
			this.AddElementFieldMapping("CompanyEntity", "CompanyDataLastModifiedUTC", "CompanyDataLastModifiedUTC", false, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 81);
			this.AddElementFieldMapping("CompanyEntity", "CompanyMediaLastModifiedUTC", "CompanyMediaLastModifiedUTC", false, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 82);
			this.AddElementFieldMapping("CompanyEntity", "MenuDataLastModifiedUTC", "MenuDataLastModifiedUTC", false, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 83);
			this.AddElementFieldMapping("CompanyEntity", "MenuMediaLastModifiedUTC", "MenuMediaLastModifiedUTC", false, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 84);
			this.AddElementFieldMapping("CompanyEntity", "PosIntegrationInfoLastModifiedUTC", "PosIntegrationInfoLastModifiedUTC", false, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 85);
			this.AddElementFieldMapping("CompanyEntity", "DeliverypointDataLastModifiedUTC", "DeliverypointDataLastModifiedUTC", false, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 86);
			this.AddElementFieldMapping("CompanyEntity", "SurveyDataLastModifiedUTC", "SurveyDataLastModifiedUTC", false, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 87);
			this.AddElementFieldMapping("CompanyEntity", "SurveyMediaLastModifiedUTC", "SurveyMediaLastModifiedUTC", false, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 88);
			this.AddElementFieldMapping("CompanyEntity", "AnnouncementDataLastModifiedUTC", "AnnouncementDataLastModifiedUTC", false, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 89);
			this.AddElementFieldMapping("CompanyEntity", "AnnouncementMediaLastModifiedUTC", "AnnouncementMediaLastModifiedUTC", false, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 90);
			this.AddElementFieldMapping("CompanyEntity", "EntertainmentDataLastModifiedUTC", "EntertainmentDataLastModifiedUTC", false, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 91);
			this.AddElementFieldMapping("CompanyEntity", "EntertainmentMediaLastModifiedUTC", "EntertainmentMediaLastModifiedUTC", false, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 92);
			this.AddElementFieldMapping("CompanyEntity", "AdvertisementDataLastModifiedUTC", "AdvertisementDataLastModifiedUTC", false, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 93);
			this.AddElementFieldMapping("CompanyEntity", "AdvertisementMediaLastModifiedUTC", "AdvertisementMediaLastModifiedUTC", false, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 94);
			this.AddElementFieldMapping("CompanyEntity", "PriceFormatType", "PriceFormatType", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 95);
			this.AddElementFieldMapping("CompanyEntity", "CompanygroupId", "CompanygroupId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 96);
			this.AddElementFieldMapping("CompanyEntity", "CountryCode", "CountryCode", true, "NVarChar", 3, 0, 0, false, "", null, typeof(System.String), 97);
			this.AddElementFieldMapping("CompanyEntity", "TimeZoneOlsonId", "TimeZoneOlsonId", true, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 98);
			this.AddElementFieldMapping("CompanyEntity", "CultureCode", "CultureCode", true, "NVarChar", 10, 0, 0, false, "", null, typeof(System.String), 99);
			this.AddElementFieldMapping("CompanyEntity", "CurrencyCode", "CurrencyCode", true, "NVarChar", 3, 0, 0, false, "", null, typeof(System.String), 100);
			this.AddElementFieldMapping("CompanyEntity", "IncludeDeliveryTimeInOrderNotes", "IncludeDeliveryTimeInOrderNotes", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 101);
			this.AddElementFieldMapping("CompanyEntity", "GoogleAnalyticsProfileId", "GoogleAnalyticsProfileId", true, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 102);
			this.AddElementFieldMapping("CompanyEntity", "IncludeMenuItemsNotExternallyLinked", "IncludeMenuItemsNotExternallyLinked", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 103);
			this.AddElementFieldMapping("CompanyEntity", "DailyReportSendTime", "DailyReportSendTime", true, "DateTime", 0, 0, 0, false, "", null, typeof(System.DateTime), 104);
			this.AddElementFieldMapping("CompanyEntity", "OrganizationId", "OrganizationId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 105);
			this.AddElementFieldMapping("CompanyEntity", "ApiVersion", "ApiVersion", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 106);
			this.AddElementFieldMapping("CompanyEntity", "MessagingVersion", "MessagingVersion", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 107);
			this.AddElementFieldMapping("CompanyEntity", "LinkDefaultRoomControlArea", "LinkDefaultRoomControlArea", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 108);
			this.AddElementFieldMapping("CompanyEntity", "PricesIncludeTaxes", "PricesIncludeTaxes", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 109);
			this.AddElementFieldMapping("CompanyEntity", "UnitSystem", "UnitSystem", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 110);
			this.AddElementFieldMapping("CompanyEntity", "ReleaseGroup", "ReleaseGroup", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 111);
			this.AddElementFieldMapping("CompanyEntity", "Notes", "Notes", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 112);
			this.AddElementFieldMapping("CompanyEntity", "HotSwappableContentEnabled", "HotSwappableContentEnabled", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 113);
			this.AddElementFieldMapping("CompanyEntity", "Archived", "Archived", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 114);
		}

		/// <summary>Inits CompanyAmenityEntity's mappings</summary>
		private void InitCompanyAmenityEntityMappings()
		{
			this.AddElementMapping("CompanyAmenityEntity", @"Obymobi", @"dbo", "CompanyAmenity", 7, 0);
			this.AddElementFieldMapping("CompanyAmenityEntity", "CompanyAmenityId", "CompanyAmenityId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("CompanyAmenityEntity", "CompanyId", "CompanyId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("CompanyAmenityEntity", "AmenityId", "AmenityId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("CompanyAmenityEntity", "CreatedBy", "CreatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("CompanyAmenityEntity", "UpdatedBy", "UpdatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("CompanyAmenityEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 5);
			this.AddElementFieldMapping("CompanyAmenityEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 6);
		}

		/// <summary>Inits CompanyBrandEntity's mappings</summary>
		private void InitCompanyBrandEntityMappings()
		{
			this.AddElementMapping("CompanyBrandEntity", @"Obymobi", @"dbo", "CompanyBrand", 8, 0);
			this.AddElementFieldMapping("CompanyBrandEntity", "CompanyBrandId", "CompanyBrandId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("CompanyBrandEntity", "CompanyId", "CompanyId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("CompanyBrandEntity", "BrandId", "BrandId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("CompanyBrandEntity", "CreatedUTC", "CreatedUTC", false, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 3);
			this.AddElementFieldMapping("CompanyBrandEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 4);
			this.AddElementFieldMapping("CompanyBrandEntity", "CreatedBy", "CreatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 5);
			this.AddElementFieldMapping("CompanyBrandEntity", "UpdatedBy", "UpdatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 6);
			this.AddElementFieldMapping("CompanyBrandEntity", "Priority", "Priority", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 7);
		}

		/// <summary>Inits CompanyCultureEntity's mappings</summary>
		private void InitCompanyCultureEntityMappings()
		{
			this.AddElementMapping("CompanyCultureEntity", @"Obymobi", @"dbo", "CompanyCulture", 7, 0);
			this.AddElementFieldMapping("CompanyCultureEntity", "CompanyCultureId", "CompanyCultureId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("CompanyCultureEntity", "CompanyId", "CompanyId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("CompanyCultureEntity", "CultureCode", "CultureCode", false, "NVarChar", 10, 0, 0, false, "", null, typeof(System.String), 2);
			this.AddElementFieldMapping("CompanyCultureEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 3);
			this.AddElementFieldMapping("CompanyCultureEntity", "CreatedBy", "CreatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("CompanyCultureEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 5);
			this.AddElementFieldMapping("CompanyCultureEntity", "UpdatedBy", "UpdatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 6);
		}

		/// <summary>Inits CompanyCurrencyEntity's mappings</summary>
		private void InitCompanyCurrencyEntityMappings()
		{
			this.AddElementMapping("CompanyCurrencyEntity", @"Obymobi", @"dbo", "CompanyCurrency", 9, 0);
			this.AddElementFieldMapping("CompanyCurrencyEntity", "CompanyCurrencyId", "CompanyCurrencyId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("CompanyCurrencyEntity", "CompanyId", "CompanyId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("CompanyCurrencyEntity", "CurrencyCode", "CurrencyCode", false, "NVarChar", 3, 0, 0, false, "", null, typeof(System.String), 2);
			this.AddElementFieldMapping("CompanyCurrencyEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 3);
			this.AddElementFieldMapping("CompanyCurrencyEntity", "CreatedBy", "CreatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("CompanyCurrencyEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 5);
			this.AddElementFieldMapping("CompanyCurrencyEntity", "UpdatedBy", "UpdatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 6);
			this.AddElementFieldMapping("CompanyCurrencyEntity", "ExchangeRate", "ExchangeRate", false, "Decimal", 0, 19, 9, false, "", null, typeof(System.Decimal), 7);
			this.AddElementFieldMapping("CompanyCurrencyEntity", "OverwriteSymbol", "OverwriteSymbol", true, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 8);
		}

		/// <summary>Inits CompanyEntertainmentEntity's mappings</summary>
		private void InitCompanyEntertainmentEntityMappings()
		{
			this.AddElementMapping("CompanyEntertainmentEntity", @"Obymobi", @"dbo", "CompanyEntertainment", 7, 0);
			this.AddElementFieldMapping("CompanyEntertainmentEntity", "CompanyEntertainmentId", "CompanyEntertainmentId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("CompanyEntertainmentEntity", "CompanyId", "CompanyId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("CompanyEntertainmentEntity", "EntertainmentId", "EntertainmentId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("CompanyEntertainmentEntity", "CreatedBy", "CreatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("CompanyEntertainmentEntity", "UpdatedBy", "UpdatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("CompanyEntertainmentEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 5);
			this.AddElementFieldMapping("CompanyEntertainmentEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 6);
		}

		/// <summary>Inits CompanygroupEntity's mappings</summary>
		private void InitCompanygroupEntityMappings()
		{
			this.AddElementMapping("CompanygroupEntity", @"Obymobi", @"dbo", "Companygroup", 7, 0);
			this.AddElementFieldMapping("CompanygroupEntity", "CompanygroupId", "CompanygroupId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("CompanygroupEntity", "Name", "Name", false, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 1);
			this.AddElementFieldMapping("CompanygroupEntity", "GoogleAnalyticsId", "GoogleAnalyticsId", true, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 2);
			this.AddElementFieldMapping("CompanygroupEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 3);
			this.AddElementFieldMapping("CompanygroupEntity", "CreatedBy", "CreatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("CompanygroupEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 5);
			this.AddElementFieldMapping("CompanygroupEntity", "UpdatedBy", "UpdatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 6);
		}

		/// <summary>Inits CompanyLanguageEntity's mappings</summary>
		private void InitCompanyLanguageEntityMappings()
		{
			this.AddElementMapping("CompanyLanguageEntity", @"Obymobi", @"dbo", "CompanyLanguage", 12, 0);
			this.AddElementFieldMapping("CompanyLanguageEntity", "CompanyLanguageId", "CompanyLanguageId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("CompanyLanguageEntity", "CompanyId", "CompanyId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("CompanyLanguageEntity", "LanguageId", "LanguageId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("CompanyLanguageEntity", "Description", "Description", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 3);
			this.AddElementFieldMapping("CompanyLanguageEntity", "CreatedBy", "CreatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("CompanyLanguageEntity", "UpdatedBy", "UpdatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 5);
			this.AddElementFieldMapping("CompanyLanguageEntity", "DescriptionSingleLine", "DescriptionSingleLine", true, "NVarChar", 250, 0, 0, false, "", null, typeof(System.String), 6);
			this.AddElementFieldMapping("CompanyLanguageEntity", "VenuePageDescription", "VenuePageDescription", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 7);
			this.AddElementFieldMapping("CompanyLanguageEntity", "BrowserAgeVerificationTitle", "BrowserAgeVerificationTitle", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 8);
			this.AddElementFieldMapping("CompanyLanguageEntity", "BrowserAgeVerificationText", "BrowserAgeVerificationText", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 9);
			this.AddElementFieldMapping("CompanyLanguageEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 10);
			this.AddElementFieldMapping("CompanyLanguageEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 11);
		}

		/// <summary>Inits CompanyManagementTaskEntity's mappings</summary>
		private void InitCompanyManagementTaskEntityMappings()
		{
			this.AddElementMapping("CompanyManagementTaskEntity", @"Obymobi", @"dbo", "CompanyManagementTask", 21, 0);
			this.AddElementFieldMapping("CompanyManagementTaskEntity", "CompanyManagementTaskId", "CompanyManagementTaskId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("CompanyManagementTaskEntity", "CompanyId", "CompanyId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("CompanyManagementTaskEntity", "CreatedCompanyId", "CreatedCompanyId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("CompanyManagementTaskEntity", "PointOfInterestId", "PointOfInterestId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("CompanyManagementTaskEntity", "CreatedPointOfInterestId", "CreatedPointOfInterestId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("CompanyManagementTaskEntity", "Type", "Type", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 5);
			this.AddElementFieldMapping("CompanyManagementTaskEntity", "Status", "Status", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 6);
			this.AddElementFieldMapping("CompanyManagementTaskEntity", "Log", "Log", true, "VarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 7);
			this.AddElementFieldMapping("CompanyManagementTaskEntity", "Progress", "Progress", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 8);
			this.AddElementFieldMapping("CompanyManagementTaskEntity", "StringValue1", "StringValue1", true, "VarChar", 255, 0, 0, false, "", null, typeof(System.String), 9);
			this.AddElementFieldMapping("CompanyManagementTaskEntity", "StringValue2", "StringValue2", true, "VarChar", 255, 0, 0, false, "", null, typeof(System.String), 10);
			this.AddElementFieldMapping("CompanyManagementTaskEntity", "StringValue3", "StringValue3", true, "VarChar", 255, 0, 0, false, "", null, typeof(System.String), 11);
			this.AddElementFieldMapping("CompanyManagementTaskEntity", "StringValue4", "StringValue4", true, "VarChar", 255, 0, 0, false, "", null, typeof(System.String), 12);
			this.AddElementFieldMapping("CompanyManagementTaskEntity", "StringValue5", "StringValue5", true, "VarChar", 255, 0, 0, false, "", null, typeof(System.String), 13);
			this.AddElementFieldMapping("CompanyManagementTaskEntity", "StringValue6", "StringValue6", true, "VarChar", 255, 0, 0, false, "", null, typeof(System.String), 14);
			this.AddElementFieldMapping("CompanyManagementTaskEntity", "Created", "Created", true, "DateTime", 0, 0, 0, false, "", null, typeof(System.DateTime), 15);
			this.AddElementFieldMapping("CompanyManagementTaskEntity", "CreatedBy", "CreatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 16);
			this.AddElementFieldMapping("CompanyManagementTaskEntity", "Updated", "Updated", true, "DateTime", 0, 0, 0, false, "", null, typeof(System.DateTime), 17);
			this.AddElementFieldMapping("CompanyManagementTaskEntity", "UpdatedBy", "UpdatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 18);
			this.AddElementFieldMapping("CompanyManagementTaskEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 19);
			this.AddElementFieldMapping("CompanyManagementTaskEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 20);
		}

		/// <summary>Inits CompanyOwnerEntity's mappings</summary>
		private void InitCompanyOwnerEntityMappings()
		{
			this.AddElementMapping("CompanyOwnerEntity", @"Obymobi", @"dbo", "CompanyOwner", 9, 0);
			this.AddElementFieldMapping("CompanyOwnerEntity", "CompanyOwnerId", "CompanyOwnerId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("CompanyOwnerEntity", "Username", "Username", false, "VarChar", 50, 0, 0, false, "", null, typeof(System.String), 1);
			this.AddElementFieldMapping("CompanyOwnerEntity", "Password", "Password", false, "VarChar", 50, 0, 0, false, "", null, typeof(System.String), 2);
			this.AddElementFieldMapping("CompanyOwnerEntity", "UserId", "UserId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("CompanyOwnerEntity", "CreatedBy", "CreatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("CompanyOwnerEntity", "UpdatedBy", "UpdatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 5);
			this.AddElementFieldMapping("CompanyOwnerEntity", "IsPasswordEncrypted", "IsPasswordEncrypted", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 6);
			this.AddElementFieldMapping("CompanyOwnerEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 7);
			this.AddElementFieldMapping("CompanyOwnerEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 8);
		}

		/// <summary>Inits CompanyReleaseEntity's mappings</summary>
		private void InitCompanyReleaseEntityMappings()
		{
			this.AddElementMapping("CompanyReleaseEntity", @"Obymobi", @"dbo", "CompanyRelease", 8, 0);
			this.AddElementFieldMapping("CompanyReleaseEntity", "CompanyReleaseId", "CompanyReleaseId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("CompanyReleaseEntity", "CompanyId", "CompanyId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("CompanyReleaseEntity", "ReleaseId", "ReleaseId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("CompanyReleaseEntity", "CreatedBy", "CreatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("CompanyReleaseEntity", "UpdatedBy", "UpdatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("CompanyReleaseEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 5);
			this.AddElementFieldMapping("CompanyReleaseEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 6);
			this.AddElementFieldMapping("CompanyReleaseEntity", "AutoUpdate", "AutoUpdate", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 7);
		}

		/// <summary>Inits CompanyVenueCategoryEntity's mappings</summary>
		private void InitCompanyVenueCategoryEntityMappings()
		{
			this.AddElementMapping("CompanyVenueCategoryEntity", @"Obymobi", @"dbo", "CompanyVenueCategory", 7, 0);
			this.AddElementFieldMapping("CompanyVenueCategoryEntity", "CompanyVenueCategoryId", "CompanyVenueCategoryId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("CompanyVenueCategoryEntity", "CompanyId", "CompanyId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("CompanyVenueCategoryEntity", "VenueCategoryId", "VenueCategoryId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("CompanyVenueCategoryEntity", "CreatedBy", "CreatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("CompanyVenueCategoryEntity", "UpdatedBy", "UpdatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("CompanyVenueCategoryEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 5);
			this.AddElementFieldMapping("CompanyVenueCategoryEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 6);
		}

		/// <summary>Inits ConfigurationEntity's mappings</summary>
		private void InitConfigurationEntityMappings()
		{
			this.AddElementMapping("ConfigurationEntity", @"Obymobi", @"dbo", "Configuration", 11, 0);
			this.AddElementFieldMapping("ConfigurationEntity", "ConfigurationId", "ConfigurationId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("ConfigurationEntity", "Name", "Name", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 1);
			this.AddElementFieldMapping("ConfigurationEntity", "FriendlyName", "FriendlyName", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 2);
			this.AddElementFieldMapping("ConfigurationEntity", "Type", "Type", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 3);
			this.AddElementFieldMapping("ConfigurationEntity", "Value", "Value", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 4);
			this.AddElementFieldMapping("ConfigurationEntity", "Section", "Section", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 5);
			this.AddElementFieldMapping("ConfigurationEntity", "ApplicationName", "ApplicationName", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 6);
			this.AddElementFieldMapping("ConfigurationEntity", "CreatedBy", "CreatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 7);
			this.AddElementFieldMapping("ConfigurationEntity", "UpdatedBy", "UpdatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 8);
			this.AddElementFieldMapping("ConfigurationEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 9);
			this.AddElementFieldMapping("ConfigurationEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 10);
		}

		/// <summary>Inits CountryEntity's mappings</summary>
		private void InitCountryEntityMappings()
		{
			this.AddElementMapping("CountryEntity", @"Obymobi", @"dbo", "Country", 10, 0);
			this.AddElementFieldMapping("CountryEntity", "CountryId", "CountryId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("CountryEntity", "Name", "Name", true, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 1);
			this.AddElementFieldMapping("CountryEntity", "Code", "Code", true, "NVarChar", 2, 0, 0, false, "", null, typeof(System.String), 2);
			this.AddElementFieldMapping("CountryEntity", "CultureName", "CultureName", true, "NVarChar", 10, 0, 0, false, "", null, typeof(System.String), 3);
			this.AddElementFieldMapping("CountryEntity", "CurrencyId", "CurrencyId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("CountryEntity", "CreatedBy", "CreatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 5);
			this.AddElementFieldMapping("CountryEntity", "UpdatedBy", "UpdatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 6);
			this.AddElementFieldMapping("CountryEntity", "CodeAlpha3", "CodeAlpha3", true, "VarChar", 3, 0, 0, false, "", null, typeof(System.String), 7);
			this.AddElementFieldMapping("CountryEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 8);
			this.AddElementFieldMapping("CountryEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 9);
		}

		/// <summary>Inits CurrencyEntity's mappings</summary>
		private void InitCurrencyEntityMappings()
		{
			this.AddElementMapping("CurrencyEntity", @"Obymobi", @"dbo", "Currency", 8, 0);
			this.AddElementFieldMapping("CurrencyEntity", "CurrencyId", "CurrencyId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("CurrencyEntity", "Name", "Name", true, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 1);
			this.AddElementFieldMapping("CurrencyEntity", "Symbol", "Symbol", true, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 2);
			this.AddElementFieldMapping("CurrencyEntity", "CodeIso4217", "CodeIso4217", false, "Char", 3, 0, 0, false, "", null, typeof(System.String), 3);
			this.AddElementFieldMapping("CurrencyEntity", "CreatedBy", "CreatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("CurrencyEntity", "UpdatedBy", "UpdatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 5);
			this.AddElementFieldMapping("CurrencyEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 6);
			this.AddElementFieldMapping("CurrencyEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 7);
		}

		/// <summary>Inits CustomerEntity's mappings</summary>
		private void InitCustomerEntityMappings()
		{
			this.AddElementMapping("CustomerEntity", @"Obymobi", @"dbo", "Customer", 49, 0);
			this.AddElementFieldMapping("CustomerEntity", "CustomerId", "CustomerId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("CustomerEntity", "Phonenumber", "Phonenumber", true, "VarChar", 50, 0, 0, false, "", null, typeof(System.String), 1);
			this.AddElementFieldMapping("CustomerEntity", "Password", "Password", true, "NVarChar", 160, 0, 0, false, "", null, typeof(System.String), 2);
			this.AddElementFieldMapping("CustomerEntity", "Verified", "Verified", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 3);
			this.AddElementFieldMapping("CustomerEntity", "Blacklisted", "Blacklisted", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 4);
			this.AddElementFieldMapping("CustomerEntity", "BlacklistedNotes", "BlacklistedNotes", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 5);
			this.AddElementFieldMapping("CustomerEntity", "Firstname", "Firstname", true, "NVarChar", 100, 0, 0, false, "", null, typeof(System.String), 6);
			this.AddElementFieldMapping("CustomerEntity", "Lastname", "Lastname", true, "NVarChar", 100, 0, 0, false, "", null, typeof(System.String), 7);
			this.AddElementFieldMapping("CustomerEntity", "LastnamePrefix", "LastnamePrefix", true, "NVarChar", 100, 0, 0, false, "", null, typeof(System.String), 8);
			this.AddElementFieldMapping("CustomerEntity", "RandomValue", "RandomValue", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 9);
			this.AddElementFieldMapping("CustomerEntity", "Birthdate", "Birthdate", true, "DateTime", 0, 0, 0, false, "", null, typeof(System.DateTime), 10);
			this.AddElementFieldMapping("CustomerEntity", "Email", "Email", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 11);
			this.AddElementFieldMapping("CustomerEntity", "Gender", "Gender", true, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 12);
			this.AddElementFieldMapping("CustomerEntity", "Addressline1", "Addressline1", true, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 13);
			this.AddElementFieldMapping("CustomerEntity", "Addressline2", "Addressline2", true, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 14);
			this.AddElementFieldMapping("CustomerEntity", "Zipcode", "Zipcode", true, "NVarChar", 25, 0, 0, false, "", null, typeof(System.String), 15);
			this.AddElementFieldMapping("CustomerEntity", "City", "City", true, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 16);
			this.AddElementFieldMapping("CustomerEntity", "Notes", "Notes", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 17);
			this.AddElementFieldMapping("CustomerEntity", "SmsRequestsSent", "SmsRequestsSent", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 18);
			this.AddElementFieldMapping("CustomerEntity", "SmsCreateRequests", "SmsCreateRequests", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 19);
			this.AddElementFieldMapping("CustomerEntity", "SmsResetPincodeRequests", "SmsResetPincodeRequests", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 20);
			this.AddElementFieldMapping("CustomerEntity", "SmsUnlockAccountRequests", "SmsUnlockAccountRequests", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 21);
			this.AddElementFieldMapping("CustomerEntity", "SmsNonProcessableRequests", "SmsNonProcessableRequests", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 22);
			this.AddElementFieldMapping("CustomerEntity", "HasChangedInititialPincode", "HasChangedInititialPincode", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 23);
			this.AddElementFieldMapping("CustomerEntity", "InitialLinkIdentifier", "InitialLinkIdentifier", true, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 24);
			this.AddElementFieldMapping("CustomerEntity", "PasswordResetLinkIdentifier", "PasswordResetLinkIdentifier", true, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 25);
			this.AddElementFieldMapping("CustomerEntity", "FailedPasswordAttemptCount", "FailedPasswordAttemptCount", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 26);
			this.AddElementFieldMapping("CustomerEntity", "RememberOnMobileWebsite", "RememberOnMobileWebsite", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 27);
			this.AddElementFieldMapping("CustomerEntity", "ClientId", "ClientId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 28);
			this.AddElementFieldMapping("CustomerEntity", "CreatedBy", "CreatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 29);
			this.AddElementFieldMapping("CustomerEntity", "UpdatedBy", "UpdatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 30);
			this.AddElementFieldMapping("CustomerEntity", "CommunicationSalt", "CommunicationSalt", true, "NVarChar", 256, 0, 0, false, "", null, typeof(System.String), 31);
			this.AddElementFieldMapping("CustomerEntity", "AnonymousAccount", "AnonymousAccount", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 32);
			this.AddElementFieldMapping("CustomerEntity", "PasswordSalt", "PasswordSalt", true, "NVarChar", 256, 0, 0, false, "", null, typeof(System.String), 33);
			this.AddElementFieldMapping("CustomerEntity", "PasswordResetLinkFailedAtttempts", "PasswordResetLinkFailedAtttempts", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 34);
			this.AddElementFieldMapping("CustomerEntity", "CountryCode", "CountryCode", true, "NVarChar", 3, 0, 0, false, "", null, typeof(System.String), 35);
			this.AddElementFieldMapping("CustomerEntity", "CanSingleSignOn", "CanSingleSignOn", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 36);
			this.AddElementFieldMapping("CustomerEntity", "GUID", "GUID", true, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 37);
			this.AddElementFieldMapping("CustomerEntity", "NewEmailAddress", "NewEmailAddress", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 38);
			this.AddElementFieldMapping("CustomerEntity", "NewEmailAddressConfirmationGuid", "NewEmailAddressConfirmationGuid", true, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 39);
			this.AddElementFieldMapping("CustomerEntity", "NewEmailAddressVerified", "NewEmailAddressVerified", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 40);
			this.AddElementFieldMapping("CustomerEntity", "CampaignContent", "CampaignContent", true, "VarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 41);
			this.AddElementFieldMapping("CustomerEntity", "CampaignMedium", "CampaignMedium", true, "VarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 42);
			this.AddElementFieldMapping("CustomerEntity", "CampaignName", "CampaignName", true, "VarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 43);
			this.AddElementFieldMapping("CustomerEntity", "CampaignSource", "CampaignSource", true, "VarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 44);
			this.AddElementFieldMapping("CustomerEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 45);
			this.AddElementFieldMapping("CustomerEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 46);
			this.AddElementFieldMapping("CustomerEntity", "PasswordResetLinkGeneratedUTC", "PasswordResetLinkGeneratedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 47);
			this.AddElementFieldMapping("CustomerEntity", "LastFailedPasswordAttemptUTC", "LastFailedPasswordAttemptUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 48);
		}

		/// <summary>Inits CustomTextEntity's mappings</summary>
		private void InitCustomTextEntityMappings()
		{
			this.AddElementMapping("CustomTextEntity", @"Obymobi", @"dbo", "CustomText", 53, 0);
			this.AddElementFieldMapping("CustomTextEntity", "CustomTextId", "CustomTextId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("CustomTextEntity", "CompanyId", "CompanyId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("CustomTextEntity", "LanguageId", "LanguageId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("CustomTextEntity", "Text", "Text", false, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 3);
			this.AddElementFieldMapping("CustomTextEntity", "DeliverypointgroupId", "DeliverypointgroupId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("CustomTextEntity", "CategoryId", "CategoryId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 5);
			this.AddElementFieldMapping("CustomTextEntity", "ProductId", "ProductId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 6);
			this.AddElementFieldMapping("CustomTextEntity", "UpdatedBy", "UpdatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 7);
			this.AddElementFieldMapping("CustomTextEntity", "CreatedBy", "CreatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 8);
			this.AddElementFieldMapping("CustomTextEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 9);
			this.AddElementFieldMapping("CustomTextEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 10);
			this.AddElementFieldMapping("CustomTextEntity", "Type", "Type", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 11);
			this.AddElementFieldMapping("CustomTextEntity", "AdvertisementId", "AdvertisementId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 12);
			this.AddElementFieldMapping("CustomTextEntity", "AlterationId", "AlterationId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 13);
			this.AddElementFieldMapping("CustomTextEntity", "AlterationoptionId", "AlterationoptionId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 14);
			this.AddElementFieldMapping("CustomTextEntity", "AnnouncementId", "AnnouncementId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 15);
			this.AddElementFieldMapping("CustomTextEntity", "ActionButtonId", "ActionButtonId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 16);
			this.AddElementFieldMapping("CustomTextEntity", "PointOfInterestId", "PointOfInterestId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 17);
			this.AddElementFieldMapping("CustomTextEntity", "EntertainmentcategoryId", "EntertainmentcategoryId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 18);
			this.AddElementFieldMapping("CustomTextEntity", "GenericproductId", "GenericproductId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 19);
			this.AddElementFieldMapping("CustomTextEntity", "PageId", "PageId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 20);
			this.AddElementFieldMapping("CustomTextEntity", "SiteId", "SiteId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 21);
			this.AddElementFieldMapping("CustomTextEntity", "AmenityId", "AmenityId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 22);
			this.AddElementFieldMapping("CustomTextEntity", "AttachmentId", "AttachmentId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 23);
			this.AddElementFieldMapping("CustomTextEntity", "PageTemplateId", "PageTemplateId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 24);
			this.AddElementFieldMapping("CustomTextEntity", "GenericcategoryId", "GenericcategoryId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 25);
			this.AddElementFieldMapping("CustomTextEntity", "RoomControlAreaId", "RoomControlAreaId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 26);
			this.AddElementFieldMapping("CustomTextEntity", "VenueCategoryId", "VenueCategoryId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 27);
			this.AddElementFieldMapping("CustomTextEntity", "RoomControlComponentId", "RoomControlComponentId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 28);
			this.AddElementFieldMapping("CustomTextEntity", "RoomControlSectionId", "RoomControlSectionId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 29);
			this.AddElementFieldMapping("CustomTextEntity", "RoomControlSectionItemId", "RoomControlSectionItemId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 30);
			this.AddElementFieldMapping("CustomTextEntity", "RoomControlWidgetId", "RoomControlWidgetId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 31);
			this.AddElementFieldMapping("CustomTextEntity", "ScheduledMessageId", "ScheduledMessageId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 32);
			this.AddElementFieldMapping("CustomTextEntity", "SiteTemplateId", "SiteTemplateId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 33);
			this.AddElementFieldMapping("CustomTextEntity", "StationId", "StationId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 34);
			this.AddElementFieldMapping("CustomTextEntity", "UIFooterItemId", "UIFooterItemId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 35);
			this.AddElementFieldMapping("CustomTextEntity", "UITabId", "UITabId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 36);
			this.AddElementFieldMapping("CustomTextEntity", "UIWidgetId", "UIWidgetId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 37);
			this.AddElementFieldMapping("CustomTextEntity", "AvailabilityId", "AvailabilityId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 38);
			this.AddElementFieldMapping("CustomTextEntity", "CultureCode", "CultureCode", true, "NVarChar", 10, 0, 0, false, "", null, typeof(System.String), 39);
			this.AddElementFieldMapping("CustomTextEntity", "ParentCompanyId", "ParentCompanyId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 40);
			this.AddElementFieldMapping("CustomTextEntity", "ClientConfigurationId", "ClientConfigurationId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 41);
			this.AddElementFieldMapping("CustomTextEntity", "ProductgroupId", "ProductgroupId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 42);
			this.AddElementFieldMapping("CustomTextEntity", "RoutestephandlerId", "RoutestephandlerId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 43);
			this.AddElementFieldMapping("CustomTextEntity", "InfraredCommandId", "InfraredCommandId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 44);
			this.AddElementFieldMapping("CustomTextEntity", "ApplicationConfigurationId", "ApplicationConfigurationId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 45);
			this.AddElementFieldMapping("CustomTextEntity", "NavigationMenuItemId", "NavigationMenuItemId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 46);
			this.AddElementFieldMapping("CustomTextEntity", "CarouselItemId", "CarouselItemId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 47);
			this.AddElementFieldMapping("CustomTextEntity", "WidgetId", "WidgetId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 48);
			this.AddElementFieldMapping("CustomTextEntity", "LandingPageId", "LandingPageId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 49);
			this.AddElementFieldMapping("CustomTextEntity", "OutletId", "OutletId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 50);
			this.AddElementFieldMapping("CustomTextEntity", "ServiceMethodId", "ServiceMethodId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 51);
			this.AddElementFieldMapping("CustomTextEntity", "CheckoutMethodId", "CheckoutMethodId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 52);
		}

		/// <summary>Inits DeliveryDistanceEntity's mappings</summary>
		private void InitDeliveryDistanceEntityMappings()
		{
			this.AddElementMapping("DeliveryDistanceEntity", @"Obymobi", @"dbo", "DeliveryDistance", 8, 0);
			this.AddElementFieldMapping("DeliveryDistanceEntity", "DeliveryDistanceId", "DeliveryDistanceId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("DeliveryDistanceEntity", "ServiceMethodId", "ServiceMethodId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("DeliveryDistanceEntity", "MaximumInMetres", "MaximumInMetres", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("DeliveryDistanceEntity", "Price", "Price", false, "Money", 0, 19, 4, false, "", null, typeof(System.Decimal), 3);
			this.AddElementFieldMapping("DeliveryDistanceEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 4);
			this.AddElementFieldMapping("DeliveryDistanceEntity", "CreatedBy", "CreatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 5);
			this.AddElementFieldMapping("DeliveryDistanceEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 6);
			this.AddElementFieldMapping("DeliveryDistanceEntity", "UpdatedBy", "UpdatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 7);
		}

		/// <summary>Inits DeliveryInformationEntity's mappings</summary>
		private void InitDeliveryInformationEntityMappings()
		{
			this.AddElementMapping("DeliveryInformationEntity", @"Obymobi", @"dbo", "DeliveryInformation", 11, 0);
			this.AddElementFieldMapping("DeliveryInformationEntity", "DeliveryInformationId", "DeliveryInformationId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("DeliveryInformationEntity", "BuildingName", "BuildingName", true, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 1);
			this.AddElementFieldMapping("DeliveryInformationEntity", "Address", "Address", false, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 2);
			this.AddElementFieldMapping("DeliveryInformationEntity", "Zipcode", "Zipcode", false, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 3);
			this.AddElementFieldMapping("DeliveryInformationEntity", "City", "City", false, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 4);
			this.AddElementFieldMapping("DeliveryInformationEntity", "Instructions", "Instructions", true, "NVarChar", 500, 0, 0, false, "", null, typeof(System.String), 5);
			this.AddElementFieldMapping("DeliveryInformationEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 6);
			this.AddElementFieldMapping("DeliveryInformationEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 7);
			this.AddElementFieldMapping("DeliveryInformationEntity", "Latitude", "Latitude", false, "Float", 0, 38, 0, false, "", null, typeof(System.Double), 8);
			this.AddElementFieldMapping("DeliveryInformationEntity", "Longitude", "Longitude", false, "Float", 0, 38, 0, false, "", null, typeof(System.Double), 9);
			this.AddElementFieldMapping("DeliveryInformationEntity", "DistanceToOutletInMetres", "DistanceToOutletInMetres", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 10);
		}

		/// <summary>Inits DeliverypointEntity's mappings</summary>
		private void InitDeliverypointEntityMappings()
		{
			this.AddElementMapping("DeliverypointEntity", @"Obymobi", @"dbo", "Deliverypoint", 21, 0);
			this.AddElementFieldMapping("DeliverypointEntity", "DeliverypointId", "DeliverypointId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("DeliverypointEntity", "DeliverypointgroupId", "DeliverypointgroupId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("DeliverypointEntity", "Name", "Name", true, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 2);
			this.AddElementFieldMapping("DeliverypointEntity", "Number", "Number", false, "VarChar", 50, 0, 0, false, "", null, typeof(System.String), 3);
			this.AddElementFieldMapping("DeliverypointEntity", "PosdeliverypointId", "PosdeliverypointId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("DeliverypointEntity", "CompanyId", "CompanyId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 5);
			this.AddElementFieldMapping("DeliverypointEntity", "CreatedBy", "CreatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 6);
			this.AddElementFieldMapping("DeliverypointEntity", "UpdatedBy", "UpdatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 7);
			this.AddElementFieldMapping("DeliverypointEntity", "RoomControllerIp", "RoomControllerIp", true, "NVarChar", 250, 0, 0, false, "", null, typeof(System.String), 8);
			this.AddElementFieldMapping("DeliverypointEntity", "GooglePrinterId", "GooglePrinterId", true, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 9);
			this.AddElementFieldMapping("DeliverypointEntity", "GooglePrinterName", "GooglePrinterName", true, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 10);
			this.AddElementFieldMapping("DeliverypointEntity", "HotSOSRoomId", "HotSOSRoomId", true, "VarChar", 50, 0, 0, false, "", null, typeof(System.String), 11);
			this.AddElementFieldMapping("DeliverypointEntity", "DeviceId", "DeviceId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 12);
			this.AddElementFieldMapping("DeliverypointEntity", "EnableAnalytics", "EnableAnalytics", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 13);
			this.AddElementFieldMapping("DeliverypointEntity", "RoomControlConfigurationId", "RoomControlConfigurationId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 14);
			this.AddElementFieldMapping("DeliverypointEntity", "RoomControllerSlaveId", "RoomControllerSlaveId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 15);
			this.AddElementFieldMapping("DeliverypointEntity", "RoomControllerType", "RoomControllerType", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 16);
			this.AddElementFieldMapping("DeliverypointEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 17);
			this.AddElementFieldMapping("DeliverypointEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 18);
			this.AddElementFieldMapping("DeliverypointEntity", "ClientConfigurationId", "ClientConfigurationId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 19);
			this.AddElementFieldMapping("DeliverypointEntity", "RoomControlAreaId", "RoomControlAreaId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 20);
		}

		/// <summary>Inits DeliverypointExternalDeliverypointEntity's mappings</summary>
		private void InitDeliverypointExternalDeliverypointEntityMappings()
		{
			this.AddElementMapping("DeliverypointExternalDeliverypointEntity", @"Obymobi", @"dbo", "DeliverypointExternalDeliverypoint", 6, 0);
			this.AddElementFieldMapping("DeliverypointExternalDeliverypointEntity", "DeliverypointExternalDeliverypointId", "DeliverypointExternalDeliverypointId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("DeliverypointExternalDeliverypointEntity", "DeliverypointId", "DeliverypointId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("DeliverypointExternalDeliverypointEntity", "ExternalDeliverypointId", "ExternalDeliverypointId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("DeliverypointExternalDeliverypointEntity", "CreatedUTC", "CreatedUTC", false, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 3);
			this.AddElementFieldMapping("DeliverypointExternalDeliverypointEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 4);
			this.AddElementFieldMapping("DeliverypointExternalDeliverypointEntity", "ParentCompanyId", "ParentCompanyId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 5);
		}

		/// <summary>Inits DeliverypointgroupEntity's mappings</summary>
		private void InitDeliverypointgroupEntityMappings()
		{
			this.AddElementMapping("DeliverypointgroupEntity", @"Obymobi", @"dbo", "Deliverypointgroup", 174, 0);
			this.AddElementFieldMapping("DeliverypointgroupEntity", "DeliverypointgroupId", "DeliverypointgroupId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("DeliverypointgroupEntity", "Name", "Name", true, "NVarChar", 100, 0, 0, false, "", null, typeof(System.String), 1);
			this.AddElementFieldMapping("DeliverypointgroupEntity", "ConfirmationCodeDeliveryType", "ConfirmationCodeDeliveryType", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("DeliverypointgroupEntity", "Active", "Active", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 3);
			this.AddElementFieldMapping("DeliverypointgroupEntity", "UseAgeCheckInOtoucho", "UseAgeCheckInOtoucho", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 4);
			this.AddElementFieldMapping("DeliverypointgroupEntity", "UseManualDeliverypointsEntryInOtoucho", "UseManualDeliverypointsEntryInOtoucho", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 5);
			this.AddElementFieldMapping("DeliverypointgroupEntity", "UseStatelessInOtoucho", "UseStatelessInOtoucho", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 6);
			this.AddElementFieldMapping("DeliverypointgroupEntity", "SkipOrderOverviewAfterOrderSubmitInOtoucho", "SkipOrderOverviewAfterOrderSubmitInOtoucho", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 7);
			this.AddElementFieldMapping("DeliverypointgroupEntity", "WifiSsid", "WifiSsid", true, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 8);
			this.AddElementFieldMapping("DeliverypointgroupEntity", "WifiPassword", "WifiPassword", true, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 9);
			this.AddElementFieldMapping("DeliverypointgroupEntity", "Locale", "Locale", true, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 10);
			this.AddElementFieldMapping("DeliverypointgroupEntity", "Pincode", "Pincode", true, "NVarChar", 25, 0, 0, false, "", null, typeof(System.String), 11);
			this.AddElementFieldMapping("DeliverypointgroupEntity", "UseManualDeliverypoint", "UseManualDeliverypoint", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 12);
			this.AddElementFieldMapping("DeliverypointgroupEntity", "UseManualDeliverypointEncryption", "UseManualDeliverypointEncryption", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 13);
			this.AddElementFieldMapping("DeliverypointgroupEntity", "AddDeliverypointToOrder", "AddDeliverypointToOrder", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 14);
			this.AddElementFieldMapping("DeliverypointgroupEntity", "EncryptionSalt", "EncryptionSalt", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 15);
			this.AddElementFieldMapping("DeliverypointgroupEntity", "HideDeliverypointNumber", "HideDeliverypointNumber", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 16);
			this.AddElementFieldMapping("DeliverypointgroupEntity", "HidePrices", "HidePrices", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 17);
			this.AddElementFieldMapping("DeliverypointgroupEntity", "ClearSessionOnTimeout", "ClearSessionOnTimeout", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 18);
			this.AddElementFieldMapping("DeliverypointgroupEntity", "ResetTimeout", "ResetTimeout", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 19);
			this.AddElementFieldMapping("DeliverypointgroupEntity", "ReorderNotificationEnabled", "ReorderNotificationEnabled", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 20);
			this.AddElementFieldMapping("DeliverypointgroupEntity", "ReorderNotificationInterval", "ReorderNotificationInterval", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 21);
			this.AddElementFieldMapping("DeliverypointgroupEntity", "ReorderNotificationAnnouncementId", "ReorderNotificationAnnouncementId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 22);
			this.AddElementFieldMapping("DeliverypointgroupEntity", "ScreenTimeoutInterval", "ScreenTimeoutInterval", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 23);
			this.AddElementFieldMapping("DeliverypointgroupEntity", "DeviceActivationRequired", "DeviceActivationRequired", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 24);
			this.AddElementFieldMapping("DeliverypointgroupEntity", "DefaultProductFullView", "DefaultProductFullView", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 25);
			this.AddElementFieldMapping("DeliverypointgroupEntity", "MarketingSurveyUrl", "MarketingSurveyUrl", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 26);
			this.AddElementFieldMapping("DeliverypointgroupEntity", "HotelUrl1", "HotelUrl1", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 27);
			this.AddElementFieldMapping("DeliverypointgroupEntity", "HotelUrl1Caption", "HotelUrl1Caption", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 28);
			this.AddElementFieldMapping("DeliverypointgroupEntity", "HotelUrl1Zoom", "HotelUrl1Zoom", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 29);
			this.AddElementFieldMapping("DeliverypointgroupEntity", "HotelUrl2", "HotelUrl2", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 30);
			this.AddElementFieldMapping("DeliverypointgroupEntity", "HotelUrl2Caption", "HotelUrl2Caption", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 31);
			this.AddElementFieldMapping("DeliverypointgroupEntity", "HotelUrl2Zoom", "HotelUrl2Zoom", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 32);
			this.AddElementFieldMapping("DeliverypointgroupEntity", "HotelUrl3", "HotelUrl3", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 33);
			this.AddElementFieldMapping("DeliverypointgroupEntity", "HotelUrl3Caption", "HotelUrl3Caption", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 34);
			this.AddElementFieldMapping("DeliverypointgroupEntity", "HotelUrl3Zoom", "HotelUrl3Zoom", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 35);
			this.AddElementFieldMapping("DeliverypointgroupEntity", "DimLevelDull", "DimLevelDull", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 36);
			this.AddElementFieldMapping("DeliverypointgroupEntity", "DimLevelMedium", "DimLevelMedium", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 37);
			this.AddElementFieldMapping("DeliverypointgroupEntity", "DimLevelBright", "DimLevelBright", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 38);
			this.AddElementFieldMapping("DeliverypointgroupEntity", "PowerSaveTimeout", "PowerSaveTimeout", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 39);
			this.AddElementFieldMapping("DeliverypointgroupEntity", "PowerSaveLevel", "PowerSaveLevel", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 40);
			this.AddElementFieldMapping("DeliverypointgroupEntity", "OutOfChargeLevel", "OutOfChargeLevel", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 41);
			this.AddElementFieldMapping("DeliverypointgroupEntity", "OutOfChargeTitle", "OutOfChargeTitle", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 42);
			this.AddElementFieldMapping("DeliverypointgroupEntity", "OutOfChargeMessage", "OutOfChargeMessage", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 43);
			this.AddElementFieldMapping("DeliverypointgroupEntity", "OrderProcessedTitle", "OrderProcessedTitle", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 44);
			this.AddElementFieldMapping("DeliverypointgroupEntity", "OrderProcessedMessage", "OrderProcessedMessage", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 45);
			this.AddElementFieldMapping("DeliverypointgroupEntity", "OrderFailedTitle", "OrderFailedTitle", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 46);
			this.AddElementFieldMapping("DeliverypointgroupEntity", "OrderFailedMessage", "OrderFailedMessage", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 47);
			this.AddElementFieldMapping("DeliverypointgroupEntity", "OrderCompletedTitle", "OrderCompletedTitle", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 48);
			this.AddElementFieldMapping("DeliverypointgroupEntity", "OrderCompletedMessage", "OrderCompletedMessage", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 49);
			this.AddElementFieldMapping("DeliverypointgroupEntity", "OrderCompletedEnabled", "OrderCompletedEnabled", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 50);
			this.AddElementFieldMapping("DeliverypointgroupEntity", "OrderSavingTitle", "OrderSavingTitle", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 51);
			this.AddElementFieldMapping("DeliverypointgroupEntity", "OrderSavingMessage", "OrderSavingMessage", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 52);
			this.AddElementFieldMapping("DeliverypointgroupEntity", "ServiceSavingTitle", "ServiceSavingTitle", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 53);
			this.AddElementFieldMapping("DeliverypointgroupEntity", "ServiceSavingMessage", "ServiceSavingMessage", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 54);
			this.AddElementFieldMapping("DeliverypointgroupEntity", "ServiceProcessedTitle", "ServiceProcessedTitle", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 55);
			this.AddElementFieldMapping("DeliverypointgroupEntity", "ServiceProcessedMessage", "ServiceProcessedMessage", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 56);
			this.AddElementFieldMapping("DeliverypointgroupEntity", "RatingSavingTitle", "RatingSavingTitle", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 57);
			this.AddElementFieldMapping("DeliverypointgroupEntity", "RatingSavingMessage", "RatingSavingMessage", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 58);
			this.AddElementFieldMapping("DeliverypointgroupEntity", "RatingProcessedTitle", "RatingProcessedTitle", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 59);
			this.AddElementFieldMapping("DeliverypointgroupEntity", "RatingProcessedMessage", "RatingProcessedMessage", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 60);
			this.AddElementFieldMapping("DeliverypointgroupEntity", "DailyOrderReset", "DailyOrderReset", false, "Char", 4, 0, 0, false, "", null, typeof(System.String), 61);
			this.AddElementFieldMapping("DeliverypointgroupEntity", "SleepTime", "SleepTime", false, "Char", 4, 0, 0, false, "", null, typeof(System.String), 62);
			this.AddElementFieldMapping("DeliverypointgroupEntity", "WakeUpTime", "WakeUpTime", false, "Char", 4, 0, 0, false, "", null, typeof(System.String), 63);
			this.AddElementFieldMapping("DeliverypointgroupEntity", "OrderProcessingNotificationEnabled", "OrderProcessingNotificationEnabled", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 64);
			this.AddElementFieldMapping("DeliverypointgroupEntity", "OrderProcessingNotificationTitle", "OrderProcessingNotificationTitle", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 65);
			this.AddElementFieldMapping("DeliverypointgroupEntity", "OrderProcessingNotification", "OrderProcessingNotification", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 66);
			this.AddElementFieldMapping("DeliverypointgroupEntity", "OrderProcessedNotificationEnabled", "OrderProcessedNotificationEnabled", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 67);
			this.AddElementFieldMapping("DeliverypointgroupEntity", "OrderProcessedNotificationTitle", "OrderProcessedNotificationTitle", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 68);
			this.AddElementFieldMapping("DeliverypointgroupEntity", "OrderProcessedNotification", "OrderProcessedNotification", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 69);
			this.AddElementFieldMapping("DeliverypointgroupEntity", "FreeformMessageTitle", "FreeformMessageTitle", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 70);
			this.AddElementFieldMapping("DeliverypointgroupEntity", "ServiceProcessingNotificationTitle", "ServiceProcessingNotificationTitle", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 71);
			this.AddElementFieldMapping("DeliverypointgroupEntity", "ServiceProcessingNotification", "ServiceProcessingNotification", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 72);
			this.AddElementFieldMapping("DeliverypointgroupEntity", "ServiceProcessedNotificationTitle", "ServiceProcessedNotificationTitle", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 73);
			this.AddElementFieldMapping("DeliverypointgroupEntity", "ServiceProcessedNotification", "ServiceProcessedNotification", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 74);
			this.AddElementFieldMapping("DeliverypointgroupEntity", "PreorderingEnabled", "PreorderingEnabled", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 75);
			this.AddElementFieldMapping("DeliverypointgroupEntity", "PreorderingActiveMinutes", "PreorderingActiveMinutes", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 76);
			this.AddElementFieldMapping("DeliverypointgroupEntity", "AnalyticsOrderingVisible", "AnalyticsOrderingVisible", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 77);
			this.AddElementFieldMapping("DeliverypointgroupEntity", "AnalyticsBestsellersVisible", "AnalyticsBestsellersVisible", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 78);
			this.AddElementFieldMapping("DeliverypointgroupEntity", "CreatedBy", "CreatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 79);
			this.AddElementFieldMapping("DeliverypointgroupEntity", "UpdatedBy", "UpdatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 80);
			this.AddElementFieldMapping("DeliverypointgroupEntity", "PincodeSU", "PincodeSU", true, "NVarChar", 25, 0, 0, false, "", null, typeof(System.String), 81);
			this.AddElementFieldMapping("DeliverypointgroupEntity", "PincodeGM", "PincodeGM", true, "NVarChar", 25, 0, 0, false, "", null, typeof(System.String), 82);
			this.AddElementFieldMapping("DeliverypointgroupEntity", "DockedDimLevelDull", "DockedDimLevelDull", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 83);
			this.AddElementFieldMapping("DeliverypointgroupEntity", "DockedDimLevelMedium", "DockedDimLevelMedium", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 84);
			this.AddElementFieldMapping("DeliverypointgroupEntity", "DockedDimLevelBright", "DockedDimLevelBright", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 85);
			this.AddElementFieldMapping("DeliverypointgroupEntity", "DeliverypointCaption", "DeliverypointCaption", true, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 86);
			this.AddElementFieldMapping("DeliverypointgroupEntity", "OrderingEnabled", "OrderingEnabled", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 87);
			this.AddElementFieldMapping("DeliverypointgroupEntity", "HomepageSlideshowInterval", "HomepageSlideshowInterval", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 88);
			this.AddElementFieldMapping("DeliverypointgroupEntity", "OrderHistoryDialogEnabled", "OrderHistoryDialogEnabled", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 89);
			this.AddElementFieldMapping("DeliverypointgroupEntity", "OrderingNotAvailableMessage", "OrderingNotAvailableMessage", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 90);
			this.AddElementFieldMapping("DeliverypointgroupEntity", "GooglePrinterId", "GooglePrinterId", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 91);
			this.AddElementFieldMapping("DeliverypointgroupEntity", "PmsIntegration", "PmsIntegration", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 92);
			this.AddElementFieldMapping("DeliverypointgroupEntity", "PmsAllowShowBill", "PmsAllowShowBill", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 93);
			this.AddElementFieldMapping("DeliverypointgroupEntity", "PmsAllowExpressCheckout", "PmsAllowExpressCheckout", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 94);
			this.AddElementFieldMapping("DeliverypointgroupEntity", "PmsLockClientWhenNotCheckedIn", "PmsLockClientWhenNotCheckedIn", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 95);
			this.AddElementFieldMapping("DeliverypointgroupEntity", "WifiAnonymousIdentify", "WifiAnonymousIdentify", true, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 96);
			this.AddElementFieldMapping("DeliverypointgroupEntity", "WifiEapMode", "WifiEapMode", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 97);
			this.AddElementFieldMapping("DeliverypointgroupEntity", "WifiIdentity", "WifiIdentity", true, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 98);
			this.AddElementFieldMapping("DeliverypointgroupEntity", "WifiPhase2Authentication", "WifiPhase2Authentication", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 99);
			this.AddElementFieldMapping("DeliverypointgroupEntity", "WifiSecurityMethod", "WifiSecurityMethod", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 100);
			this.AddElementFieldMapping("DeliverypointgroupEntity", "CompanyId", "CompanyId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 101);
			this.AddElementFieldMapping("DeliverypointgroupEntity", "MenuId", "MenuId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 102);
			this.AddElementFieldMapping("DeliverypointgroupEntity", "PosdeliverypointgroupId", "PosdeliverypointgroupId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 103);
			this.AddElementFieldMapping("DeliverypointgroupEntity", "RouteId", "RouteId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 104);
			this.AddElementFieldMapping("DeliverypointgroupEntity", "SystemMessageRouteId", "SystemMessageRouteId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 105);
			this.AddElementFieldMapping("DeliverypointgroupEntity", "XTerminalId", "TerminalId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 106);
			this.AddElementFieldMapping("DeliverypointgroupEntity", "UIModeId", "UIModeId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 107);
			this.AddElementFieldMapping("DeliverypointgroupEntity", "OutOfChargeNotificationAmount", "OutOfChargeNotificationAmount", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 108);
			this.AddElementFieldMapping("DeliverypointgroupEntity", "IsChargerRemovedDialogEnabled", "IsChargerRemovedDialogEnabled", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 109);
			this.AddElementFieldMapping("DeliverypointgroupEntity", "ChargerRemovedDialogTitle", "ChargerRemovedDialogTitle", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 110);
			this.AddElementFieldMapping("DeliverypointgroupEntity", "ChargerRemovedDialogText", "ChargerRemovedDialogText", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 111);
			this.AddElementFieldMapping("DeliverypointgroupEntity", "IsChargerRemovedReminderDialogEnabled", "IsChargerRemovedReminderDialogEnabled", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 112);
			this.AddElementFieldMapping("DeliverypointgroupEntity", "ChargerRemovedReminderDialogTitle", "ChargerRemovedReminderDialogTitle", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 113);
			this.AddElementFieldMapping("DeliverypointgroupEntity", "ChargerRemovedReminderDialogText", "ChargerRemovedReminderDialogText", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 114);
			this.AddElementFieldMapping("DeliverypointgroupEntity", "PmsDeviceLockedTitle", "PmsDeviceLockedTitle", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 115);
			this.AddElementFieldMapping("DeliverypointgroupEntity", "PmsDeviceLockedText", "PmsDeviceLockedText", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 116);
			this.AddElementFieldMapping("DeliverypointgroupEntity", "PmsCheckinFailedTitle", "PmsCheckinFailedTitle", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 117);
			this.AddElementFieldMapping("DeliverypointgroupEntity", "PmsCheckinFailedText", "PmsCheckinFailedText", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 118);
			this.AddElementFieldMapping("DeliverypointgroupEntity", "PmsRestartTitle", "PmsRestartTitle", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 119);
			this.AddElementFieldMapping("DeliverypointgroupEntity", "PmsRestartText", "PmsRestartText", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 120);
			this.AddElementFieldMapping("DeliverypointgroupEntity", "PmsWelcomeTitle", "PmsWelcomeTitle", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 121);
			this.AddElementFieldMapping("DeliverypointgroupEntity", "PmsWelcomeText", "PmsWelcomeText", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 122);
			this.AddElementFieldMapping("DeliverypointgroupEntity", "PmsCheckoutApproveText", "PmsCheckoutApproveText", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 123);
			this.AddElementFieldMapping("DeliverypointgroupEntity", "PmsWelcomeTimeoutMinutes", "PmsWelcomeTimeoutMinutes", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 124);
			this.AddElementFieldMapping("DeliverypointgroupEntity", "PmsCheckoutCompleteTitle", "PmsCheckoutCompleteTitle", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 125);
			this.AddElementFieldMapping("DeliverypointgroupEntity", "PmsCheckoutCompleteText", "PmsCheckoutCompleteText", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 126);
			this.AddElementFieldMapping("DeliverypointgroupEntity", "PmsCheckoutCompleteTimeoutMinutes", "PmsCheckoutCompleteTimeoutMinutes", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 127);
			this.AddElementFieldMapping("DeliverypointgroupEntity", "AvailableOnObymobi", "AvailableOnObymobi", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 128);
			this.AddElementFieldMapping("DeliverypointgroupEntity", "MobileUIModeId", "MobileUIModeId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 129);
			this.AddElementFieldMapping("DeliverypointgroupEntity", "TabletUIModeId", "TabletUIModeId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 130);
			this.AddElementFieldMapping("DeliverypointgroupEntity", "PowerButtonHardBehaviour", "PowerButtonHardBehaviour", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 131);
			this.AddElementFieldMapping("DeliverypointgroupEntity", "PowerButtonSoftBehaviour", "PowerButtonSoftBehaviour", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 132);
			this.AddElementFieldMapping("DeliverypointgroupEntity", "ScreenOffMode", "ScreenOffMode", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 133);
			this.AddElementFieldMapping("DeliverypointgroupEntity", "UseHardKeyboard", "UseHardKeyboard", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 134);
			this.AddElementFieldMapping("DeliverypointgroupEntity", "GooglePrinterName", "GooglePrinterName", true, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 135);
			this.AddElementFieldMapping("DeliverypointgroupEntity", "RoomserviceCharge", "RoomserviceCharge", true, "Money", 0, 19, 4, false, "", null, typeof(System.Decimal), 136);
			this.AddElementFieldMapping("DeliverypointgroupEntity", "HotSOSBatteryLowProductId", "HotSOSBatteryLowProductId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 137);
			this.AddElementFieldMapping("DeliverypointgroupEntity", "EmailDocumentRouteId", "EmailDocumentRouteId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 138);
			this.AddElementFieldMapping("DeliverypointgroupEntity", "EnableBatteryLowConsoleNotifications", "EnableBatteryLowConsoleNotifications", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 139);
			this.AddElementFieldMapping("DeliverypointgroupEntity", "UIThemeId", "UIThemeId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 140);
			this.AddElementFieldMapping("DeliverypointgroupEntity", "ScreensaverMode", "ScreensaverMode", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 141);
			this.AddElementFieldMapping("DeliverypointgroupEntity", "PmsAllowShowGuestName", "PmsAllowShowGuestName", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 142);
			this.AddElementFieldMapping("DeliverypointgroupEntity", "TurnOffPrivacyTitle", "TurnOffPrivacyTitle", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 143);
			this.AddElementFieldMapping("DeliverypointgroupEntity", "TurnOffPrivacyText", "TurnOffPrivacyText", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 144);
			this.AddElementFieldMapping("DeliverypointgroupEntity", "EstimatedDeliveryTime", "EstimatedDeliveryTime", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 145);
			this.AddElementFieldMapping("DeliverypointgroupEntity", "OrderNotesRouteId", "OrderNotesRouteId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 146);
			this.AddElementFieldMapping("DeliverypointgroupEntity", "UIScheduleId", "UIScheduleId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 147);
			this.AddElementFieldMapping("DeliverypointgroupEntity", "ItemCurrentlyUnavailableText", "ItemCurrentlyUnavailableText", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 148);
			this.AddElementFieldMapping("DeliverypointgroupEntity", "ServiceFailedTitle", "ServiceFailedTitle", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 149);
			this.AddElementFieldMapping("DeliverypointgroupEntity", "ServiceFailedMessage", "ServiceFailedMessage", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 150);
			this.AddElementFieldMapping("DeliverypointgroupEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 151);
			this.AddElementFieldMapping("DeliverypointgroupEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 152);
			this.AddElementFieldMapping("DeliverypointgroupEntity", "AlarmSetWhileNotChargingTitle", "AlarmSetWhileNotChargingTitle", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 153);
			this.AddElementFieldMapping("DeliverypointgroupEntity", "AlarmSetWhileNotChargingText", "AlarmSetWhileNotChargingText", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 154);
			this.AddElementFieldMapping("DeliverypointgroupEntity", "PriceScheduleId", "PriceScheduleId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 155);
			this.AddElementFieldMapping("DeliverypointgroupEntity", "ClientConfigurationId", "ClientConfigurationId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 156);
			this.AddElementFieldMapping("DeliverypointgroupEntity", "IsOrderitemAddedDialogEnabled", "IsOrderitemAddedDialogEnabled", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 157);
			this.AddElementFieldMapping("DeliverypointgroupEntity", "BrowserAgeVerificationEnabled", "BrowserAgeVerificationEnabled", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 158);
			this.AddElementFieldMapping("DeliverypointgroupEntity", "BrowserAgeVerificationLayout", "BrowserAgeVerificationLayout", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 159);
			this.AddElementFieldMapping("DeliverypointgroupEntity", "RestartApplicationDialogEnabled", "RestartApplicationDialogEnabled", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 160);
			this.AddElementFieldMapping("DeliverypointgroupEntity", "RebootDeviceDialogEnabled", "RebootDeviceDialogEnabled", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 161);
			this.AddElementFieldMapping("DeliverypointgroupEntity", "RestartTimeoutSeconds", "RestartTimeoutSeconds", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 162);
			this.AddElementFieldMapping("DeliverypointgroupEntity", "CraveAnalytics", "CraveAnalytics", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 163);
			this.AddElementFieldMapping("DeliverypointgroupEntity", "GeoFencingEnabled", "GeoFencingEnabled", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 164);
			this.AddElementFieldMapping("DeliverypointgroupEntity", "GeoFencingRadius", "GeoFencingRadius", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 165);
			this.AddElementFieldMapping("DeliverypointgroupEntity", "HideCompanyDetails", "HideCompanyDetails", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 166);
			this.AddElementFieldMapping("DeliverypointgroupEntity", "IsClearBasketDialogEnabled", "IsClearBasketDialogEnabled", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 167);
			this.AddElementFieldMapping("DeliverypointgroupEntity", "ClearBasketTitle", "ClearBasketTitle", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 168);
			this.AddElementFieldMapping("DeliverypointgroupEntity", "ClearBasketText", "ClearBasketText", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 169);
			this.AddElementFieldMapping("DeliverypointgroupEntity", "ApiVersion", "ApiVersion", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 170);
			this.AddElementFieldMapping("DeliverypointgroupEntity", "MessagingVersion", "MessagingVersion", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 171);
			this.AddElementFieldMapping("DeliverypointgroupEntity", "AddressId", "AddressId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 172);
			this.AddElementFieldMapping("DeliverypointgroupEntity", "AffiliateCampaignId", "AffiliateCampaignId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 173);
		}

		/// <summary>Inits DeliverypointgroupAdvertisementEntity's mappings</summary>
		private void InitDeliverypointgroupAdvertisementEntityMappings()
		{
			this.AddElementMapping("DeliverypointgroupAdvertisementEntity", @"Obymobi", @"dbo", "DeliverypointgroupAdvertisement", 8, 0);
			this.AddElementFieldMapping("DeliverypointgroupAdvertisementEntity", "DeliverypointgroupAdvertisementId", "DeliverypointgroupAdvertisementId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("DeliverypointgroupAdvertisementEntity", "DeliverypointgroupId", "DeliverypointgroupId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("DeliverypointgroupAdvertisementEntity", "AdvertisementId", "AdvertisementId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("DeliverypointgroupAdvertisementEntity", "CreatedBy", "CreatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("DeliverypointgroupAdvertisementEntity", "UpdatedBy", "UpdatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("DeliverypointgroupAdvertisementEntity", "ParentCompanyId", "ParentCompanyId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 5);
			this.AddElementFieldMapping("DeliverypointgroupAdvertisementEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 6);
			this.AddElementFieldMapping("DeliverypointgroupAdvertisementEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 7);
		}

		/// <summary>Inits DeliverypointgroupAnnouncementEntity's mappings</summary>
		private void InitDeliverypointgroupAnnouncementEntityMappings()
		{
			this.AddElementMapping("DeliverypointgroupAnnouncementEntity", @"Obymobi", @"dbo", "DeliverypointgroupAnnouncement", 8, 0);
			this.AddElementFieldMapping("DeliverypointgroupAnnouncementEntity", "DeliverypointAnnouncementId", "DeliverypointAnnouncementId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("DeliverypointgroupAnnouncementEntity", "DeliverypointgroupId", "DeliverypointgroupId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("DeliverypointgroupAnnouncementEntity", "AnnouncementId", "AnnouncementId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("DeliverypointgroupAnnouncementEntity", "CreatedBy", "CreatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("DeliverypointgroupAnnouncementEntity", "UpdatedBy", "UpdatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("DeliverypointgroupAnnouncementEntity", "ParentCompanyId", "ParentCompanyId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 5);
			this.AddElementFieldMapping("DeliverypointgroupAnnouncementEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 6);
			this.AddElementFieldMapping("DeliverypointgroupAnnouncementEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 7);
		}

		/// <summary>Inits DeliverypointgroupEntertainmentEntity's mappings</summary>
		private void InitDeliverypointgroupEntertainmentEntityMappings()
		{
			this.AddElementMapping("DeliverypointgroupEntertainmentEntity", @"Obymobi", @"dbo", "DeliverypointgroupEntertainment", 9, 0);
			this.AddElementFieldMapping("DeliverypointgroupEntertainmentEntity", "DeliverypointgroupEntertainmentId", "DeliverypointgroupEntertainmentId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("DeliverypointgroupEntertainmentEntity", "DeliverypointgroupId", "DeliverypointgroupId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("DeliverypointgroupEntertainmentEntity", "EntertainmentId", "EntertainmentId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("DeliverypointgroupEntertainmentEntity", "CreatedBy", "CreatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("DeliverypointgroupEntertainmentEntity", "UpdatedBy", "UpdatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("DeliverypointgroupEntertainmentEntity", "SortOrder", "SortOrder", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 5);
			this.AddElementFieldMapping("DeliverypointgroupEntertainmentEntity", "ParentCompanyId", "ParentCompanyId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 6);
			this.AddElementFieldMapping("DeliverypointgroupEntertainmentEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 7);
			this.AddElementFieldMapping("DeliverypointgroupEntertainmentEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 8);
		}

		/// <summary>Inits DeliverypointgroupLanguageEntity's mappings</summary>
		private void InitDeliverypointgroupLanguageEntityMappings()
		{
			this.AddElementMapping("DeliverypointgroupLanguageEntity", @"Obymobi", @"dbo", "DeliverypointgroupLanguage", 65, 0);
			this.AddElementFieldMapping("DeliverypointgroupLanguageEntity", "DeliverypointgroupLanguageId", "DeliverypointgroupLanguageId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("DeliverypointgroupLanguageEntity", "DeliverypointgroupId", "DeliverypointgroupId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("DeliverypointgroupLanguageEntity", "LanguageId", "LanguageId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("DeliverypointgroupLanguageEntity", "OutOfChargeTitle", "OutOfChargeTitle", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 3);
			this.AddElementFieldMapping("DeliverypointgroupLanguageEntity", "OutOfChargeMessage", "OutOfChargeMessage", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 4);
			this.AddElementFieldMapping("DeliverypointgroupLanguageEntity", "OrderProcessedTitle", "OrderProcessedTitle", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 5);
			this.AddElementFieldMapping("DeliverypointgroupLanguageEntity", "OrderProcessedMessage", "OrderProcessedMessage", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 6);
			this.AddElementFieldMapping("DeliverypointgroupLanguageEntity", "OrderFailedTitle", "OrderFailedTitle", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 7);
			this.AddElementFieldMapping("DeliverypointgroupLanguageEntity", "OrderFailedMessage", "OrderFailedMessage", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 8);
			this.AddElementFieldMapping("DeliverypointgroupLanguageEntity", "OrderCompletedTitle", "OrderCompletedTitle", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 9);
			this.AddElementFieldMapping("DeliverypointgroupLanguageEntity", "OrderCompletedMessage", "OrderCompletedMessage", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 10);
			this.AddElementFieldMapping("DeliverypointgroupLanguageEntity", "OrderSavingTitle", "OrderSavingTitle", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 11);
			this.AddElementFieldMapping("DeliverypointgroupLanguageEntity", "OrderSavingMessage", "OrderSavingMessage", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 12);
			this.AddElementFieldMapping("DeliverypointgroupLanguageEntity", "ServiceSavingTitle", "ServiceSavingTitle", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 13);
			this.AddElementFieldMapping("DeliverypointgroupLanguageEntity", "ServiceSavingMessage", "ServiceSavingMessage", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 14);
			this.AddElementFieldMapping("DeliverypointgroupLanguageEntity", "ServiceProcessedTitle", "ServiceProcessedTitle", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 15);
			this.AddElementFieldMapping("DeliverypointgroupLanguageEntity", "ServiceProcessedMessage", "ServiceProcessedMessage", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 16);
			this.AddElementFieldMapping("DeliverypointgroupLanguageEntity", "RatingSavingTitle", "RatingSavingTitle", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 17);
			this.AddElementFieldMapping("DeliverypointgroupLanguageEntity", "RatingSavingMessage", "RatingSavingMessage", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 18);
			this.AddElementFieldMapping("DeliverypointgroupLanguageEntity", "RatingProcessedTitle", "RatingProcessedTitle", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 19);
			this.AddElementFieldMapping("DeliverypointgroupLanguageEntity", "RatingProcessedMessage", "RatingProcessedMessage", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 20);
			this.AddElementFieldMapping("DeliverypointgroupLanguageEntity", "HotelUrl1", "HotelUrl1", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 21);
			this.AddElementFieldMapping("DeliverypointgroupLanguageEntity", "HotelUrl1Caption", "HotelUrl1Caption", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 22);
			this.AddElementFieldMapping("DeliverypointgroupLanguageEntity", "HotelUrl1Zoom", "HotelUrl1Zoom", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 23);
			this.AddElementFieldMapping("DeliverypointgroupLanguageEntity", "HotelUrl2", "HotelUrl2", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 24);
			this.AddElementFieldMapping("DeliverypointgroupLanguageEntity", "HotelUrl2Caption", "HotelUrl2Caption", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 25);
			this.AddElementFieldMapping("DeliverypointgroupLanguageEntity", "HotelUrl2Zoom", "HotelUrl2Zoom", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 26);
			this.AddElementFieldMapping("DeliverypointgroupLanguageEntity", "HotelUrl3", "HotelUrl3", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 27);
			this.AddElementFieldMapping("DeliverypointgroupLanguageEntity", "HotelUrl3Caption", "HotelUrl3Caption", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 28);
			this.AddElementFieldMapping("DeliverypointgroupLanguageEntity", "HotelUrl3Zoom", "HotelUrl3Zoom", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 29);
			this.AddElementFieldMapping("DeliverypointgroupLanguageEntity", "CreatedBy", "CreatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 30);
			this.AddElementFieldMapping("DeliverypointgroupLanguageEntity", "DeliverypointCaption", "DeliverypointCaption", true, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 31);
			this.AddElementFieldMapping("DeliverypointgroupLanguageEntity", "PmsCheckinFailedText", "PmsCheckinFailedText", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 32);
			this.AddElementFieldMapping("DeliverypointgroupLanguageEntity", "PmsCheckinFailedTitle", "PmsCheckinFailedTitle", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 33);
			this.AddElementFieldMapping("DeliverypointgroupLanguageEntity", "PmsCheckoutApproveText", "PmsCheckoutApproveText", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 34);
			this.AddElementFieldMapping("DeliverypointgroupLanguageEntity", "PmsDeviceLockedText", "PmsDeviceLockedText", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 35);
			this.AddElementFieldMapping("DeliverypointgroupLanguageEntity", "PmsDeviceLockedTitle", "PmsDeviceLockedTitle", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 36);
			this.AddElementFieldMapping("DeliverypointgroupLanguageEntity", "PmsRestartText", "PmsRestartText", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 37);
			this.AddElementFieldMapping("DeliverypointgroupLanguageEntity", "PmsRestartTitle", "PmsRestartTitle", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 38);
			this.AddElementFieldMapping("DeliverypointgroupLanguageEntity", "PmsWelcomeText", "PmsWelcomeText", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 39);
			this.AddElementFieldMapping("DeliverypointgroupLanguageEntity", "PmsWelcomeTitle", "PmsWelcomeTitle", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 40);
			this.AddElementFieldMapping("DeliverypointgroupLanguageEntity", "PmsCheckoutCompleteText", "PmsCheckoutCompleteText", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 41);
			this.AddElementFieldMapping("DeliverypointgroupLanguageEntity", "PmsCheckoutCompleteTitle", "PmsCheckoutCompleteTitle", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 42);
			this.AddElementFieldMapping("DeliverypointgroupLanguageEntity", "Name", "Name", true, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 43);
			this.AddElementFieldMapping("DeliverypointgroupLanguageEntity", "RoomserviceChargeText", "RoomserviceChargeText", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 44);
			this.AddElementFieldMapping("DeliverypointgroupLanguageEntity", "SuggestionsCaption", "SuggestionsCaption", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 45);
			this.AddElementFieldMapping("DeliverypointgroupLanguageEntity", "PrintingConfirmationTitle", "PrintingConfirmationTitle", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 46);
			this.AddElementFieldMapping("DeliverypointgroupLanguageEntity", "PrintingConfirmationText", "PrintingConfirmationText", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 47);
			this.AddElementFieldMapping("DeliverypointgroupLanguageEntity", "PagesCaption", "PagesCaption", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 48);
			this.AddElementFieldMapping("DeliverypointgroupLanguageEntity", "PrintingSucceededTitle", "PrintingSucceededTitle", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 49);
			this.AddElementFieldMapping("DeliverypointgroupLanguageEntity", "PrintingSucceededText", "PrintingSucceededText", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 50);
			this.AddElementFieldMapping("DeliverypointgroupLanguageEntity", "ParentCompanyId", "ParentCompanyId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 51);
			this.AddElementFieldMapping("DeliverypointgroupLanguageEntity", "ChargerRemovedDialogTitle", "ChargerRemovedDialogTitle", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 52);
			this.AddElementFieldMapping("DeliverypointgroupLanguageEntity", "ChargerRemovedDialogText", "ChargerRemovedDialogText", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 53);
			this.AddElementFieldMapping("DeliverypointgroupLanguageEntity", "ChargerRemovedReminderDialogTitle", "ChargerRemovedReminderDialogTitle", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 54);
			this.AddElementFieldMapping("DeliverypointgroupLanguageEntity", "ChargerRemovedReminderDialogText", "ChargerRemovedReminderDialogText", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 55);
			this.AddElementFieldMapping("DeliverypointgroupLanguageEntity", "TurnOffPrivacyTitle", "TurnOffPrivacyTitle", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 56);
			this.AddElementFieldMapping("DeliverypointgroupLanguageEntity", "TurnOffPrivacyText", "TurnOffPrivacyText", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 57);
			this.AddElementFieldMapping("DeliverypointgroupLanguageEntity", "ItemCurrentlyUnavailableText", "ItemCurrentlyUnavailableText", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 58);
			this.AddElementFieldMapping("DeliverypointgroupLanguageEntity", "ServiceFailedTitle", "ServiceFailedTitle", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 59);
			this.AddElementFieldMapping("DeliverypointgroupLanguageEntity", "ServiceFailedMessage", "ServiceFailedMessage", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 60);
			this.AddElementFieldMapping("DeliverypointgroupLanguageEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 61);
			this.AddElementFieldMapping("DeliverypointgroupLanguageEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 62);
			this.AddElementFieldMapping("DeliverypointgroupLanguageEntity", "AlarmSetWhileNotChargingTitle", "AlarmSetWhileNotChargingTitle", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 63);
			this.AddElementFieldMapping("DeliverypointgroupLanguageEntity", "AlarmSetWhileNotChargingText", "AlarmSetWhileNotChargingText", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 64);
		}

		/// <summary>Inits DeliverypointgroupOccupancyEntity's mappings</summary>
		private void InitDeliverypointgroupOccupancyEntityMappings()
		{
			this.AddElementMapping("DeliverypointgroupOccupancyEntity", @"Obymobi", @"dbo", "DeliverypointgroupOccupancy", 7, 0);
			this.AddElementFieldMapping("DeliverypointgroupOccupancyEntity", "DeliverypointgroupOccupancyId", "DeliverypointgroupOccupancyId", false, "BigInt", 0, 19, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int64), 0);
			this.AddElementFieldMapping("DeliverypointgroupOccupancyEntity", "DeliverypointgroupId", "DeliverypointgroupId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("DeliverypointgroupOccupancyEntity", "Date", "Date", false, "DateTime", 0, 0, 0, false, "", null, typeof(System.DateTime), 2);
			this.AddElementFieldMapping("DeliverypointgroupOccupancyEntity", "Amount", "Amount", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("DeliverypointgroupOccupancyEntity", "ParentCompanyId", "ParentCompanyId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("DeliverypointgroupOccupancyEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 5);
			this.AddElementFieldMapping("DeliverypointgroupOccupancyEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 6);
		}

		/// <summary>Inits DeliverypointgroupProductEntity's mappings</summary>
		private void InitDeliverypointgroupProductEntityMappings()
		{
			this.AddElementMapping("DeliverypointgroupProductEntity", @"Obymobi", @"dbo", "DeliverypointgroupProduct", 10, 0);
			this.AddElementFieldMapping("DeliverypointgroupProductEntity", "DeliverypointgroupProductId", "DeliverypointgroupProductId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("DeliverypointgroupProductEntity", "DeliverypointgroupId", "DeliverypointgroupId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("DeliverypointgroupProductEntity", "ProductId", "ProductId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("DeliverypointgroupProductEntity", "SortOrder", "SortOrder", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("DeliverypointgroupProductEntity", "Type", "Type", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("DeliverypointgroupProductEntity", "CreatedBy", "CreatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 5);
			this.AddElementFieldMapping("DeliverypointgroupProductEntity", "UpdatedBy", "UpdatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 6);
			this.AddElementFieldMapping("DeliverypointgroupProductEntity", "ParentCompanyId", "ParentCompanyId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 7);
			this.AddElementFieldMapping("DeliverypointgroupProductEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 8);
			this.AddElementFieldMapping("DeliverypointgroupProductEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 9);
		}

		/// <summary>Inits DeviceEntity's mappings</summary>
		private void InitDeviceEntityMappings()
		{
			this.AddElementMapping("DeviceEntity", @"Obymobi", @"dbo", "Device", 49, 0);
			this.AddElementFieldMapping("DeviceEntity", "DeviceId", "DeviceId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("DeviceEntity", "Identifier", "Identifier", false, "VarChar", 255, 0, 0, false, "", null, typeof(System.String), 1);
			this.AddElementFieldMapping("DeviceEntity", "Type", "Type", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("DeviceEntity", "Notes", "Notes", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 3);
			this.AddElementFieldMapping("DeviceEntity", "CreatedBy", "CreatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("DeviceEntity", "UpdatedBy", "UpdatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 5);
			this.AddElementFieldMapping("DeviceEntity", "UpdateStatus", "UpdateStatus", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 6);
			this.AddElementFieldMapping("DeviceEntity", "CustomerId", "CustomerId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 7);
			this.AddElementFieldMapping("DeviceEntity", "DevicePlatform", "DevicePlatform", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 8);
			this.AddElementFieldMapping("DeviceEntity", "IsDevelopment", "IsDevelopment", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 9);
			this.AddElementFieldMapping("DeviceEntity", "DeviceModel", "DeviceModel", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 10);
			this.AddElementFieldMapping("DeviceEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 11);
			this.AddElementFieldMapping("DeviceEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 12);
			this.AddElementFieldMapping("DeviceEntity", "UpdateStatusChangedUTC", "UpdateStatusChangedUTC", false, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 13);
			this.AddElementFieldMapping("DeviceEntity", "LastRequestUTC", "LastRequestUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 14);
			this.AddElementFieldMapping("DeviceEntity", "LastRequestNotifiedBySmsUTC", "LastRequestNotifiedBySmsUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 15);
			this.AddElementFieldMapping("DeviceEntity", "LastSupportToolsRequestUTC", "LastSupportToolsRequestUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 16);
			this.AddElementFieldMapping("DeviceEntity", "IsCharging", "IsCharging", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 17);
			this.AddElementFieldMapping("DeviceEntity", "BatteryLevel", "BatteryLevel", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 18);
			this.AddElementFieldMapping("DeviceEntity", "PublicIpAddress", "PublicIpAddress", true, "VarChar", 255, 0, 0, false, "", null, typeof(System.String), 19);
			this.AddElementFieldMapping("DeviceEntity", "PrivateIpAddresses", "PrivateIpAddresses", true, "VarChar", 255, 0, 0, false, "", null, typeof(System.String), 20);
			this.AddElementFieldMapping("DeviceEntity", "ApplicationVersion", "ApplicationVersion", true, "VarChar", 50, 0, 0, false, "", null, typeof(System.String), 21);
			this.AddElementFieldMapping("DeviceEntity", "AgentRunning", "AgentRunning", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 22);
			this.AddElementFieldMapping("DeviceEntity", "AgentVersion", "AgentVersion", true, "VarChar", 50, 0, 0, false, "", null, typeof(System.String), 23);
			this.AddElementFieldMapping("DeviceEntity", "SupportToolsRunning", "SupportToolsRunning", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 24);
			this.AddElementFieldMapping("DeviceEntity", "SupportToolsVersion", "SupportToolsVersion", true, "VarChar", 50, 0, 0, false, "", null, typeof(System.String), 25);
			this.AddElementFieldMapping("DeviceEntity", "OsVersion", "OsVersion", true, "VarChar", 50, 0, 0, false, "", null, typeof(System.String), 26);
			this.AddElementFieldMapping("DeviceEntity", "WifiStrength", "WifiStrength", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 27);
			this.AddElementFieldMapping("DeviceEntity", "DeviceInfoUpdatedUTC", "DeviceInfoUpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 28);
			this.AddElementFieldMapping("DeviceEntity", "UpdateEmenuDownloaded", "UpdateEmenuDownloaded", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 29);
			this.AddElementFieldMapping("DeviceEntity", "UpdateAgentDownloaded", "UpdateAgentDownloaded", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 30);
			this.AddElementFieldMapping("DeviceEntity", "UpdateSupportToolsDownloaded", "UpdateSupportToolsDownloaded", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 31);
			this.AddElementFieldMapping("DeviceEntity", "UpdateOSDownloaded", "UpdateOSDownloaded", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 32);
			this.AddElementFieldMapping("DeviceEntity", "CloudEnvironment", "CloudEnvironment", true, "VarChar", 50, 0, 0, false, "", null, typeof(System.String), 33);
			this.AddElementFieldMapping("DeviceEntity", "CommunicationMethod", "CommunicationMethod", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 34);
			this.AddElementFieldMapping("DeviceEntity", "UpdateConsoleDownloaded", "UpdateConsoleDownloaded", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 35);
			this.AddElementFieldMapping("DeviceEntity", "LastUserInteraction", "LastUserInteraction", true, "BigInt", 0, 19, 0, false, "", null, typeof(System.Int64), 36);
			this.AddElementFieldMapping("DeviceEntity", "Token", "Token", true, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 37);
			this.AddElementFieldMapping("DeviceEntity", "SandboxMode", "SandboxMode", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 38);
			this.AddElementFieldMapping("DeviceEntity", "MessagingServiceRunning", "MessagingServiceRunning", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 39);
			this.AddElementFieldMapping("DeviceEntity", "MessagingServiceVersion", "MessagingServiceVersion", true, "VarChar", 50, 0, 0, false, "", null, typeof(System.String), 40);
			this.AddElementFieldMapping("DeviceEntity", "LastMessagingServiceRequest", "LastMessagingServiceRequest", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 41);
			this.AddElementFieldMapping("DeviceEntity", "LastMessagingServiceConnectionType", "LastMessagingServiceConnectionType", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 42);
			this.AddElementFieldMapping("DeviceEntity", "LastMessagingServiceConnectionStatus", "LastMessagingServiceConnectionStatus", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 43);
			this.AddElementFieldMapping("DeviceEntity", "LastMessagingServiceSuccessfulConnection", "LastMessagingServiceSuccessfulConnection", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 44);
			this.AddElementFieldMapping("DeviceEntity", "MessagingServiceConnectingCount24h", "MessagingServiceConnectingCount24h", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 45);
			this.AddElementFieldMapping("DeviceEntity", "MessagingServiceConnectedCount24h", "MessagingServiceConnectedCount24h", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 46);
			this.AddElementFieldMapping("DeviceEntity", "MessagingServiceReconnectingCount24h", "MessagingServiceReconnectingCount24h", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 47);
			this.AddElementFieldMapping("DeviceEntity", "MessagingServiceConnectionLostCount24h", "MessagingServiceConnectionLostCount24h", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 48);
		}

		/// <summary>Inits DevicemediaEntity's mappings</summary>
		private void InitDevicemediaEntityMappings()
		{
			this.AddElementMapping("DevicemediaEntity", @"Obymobi", @"dbo", "Devicemedia", 8, 0);
			this.AddElementFieldMapping("DevicemediaEntity", "DevicemediaId", "DevicemediaId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("DevicemediaEntity", "DeviceId", "DeviceId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("DevicemediaEntity", "Filename", "Filename", true, "NVarChar", 250, 0, 0, false, "", null, typeof(System.String), 2);
			this.AddElementFieldMapping("DevicemediaEntity", "CreatedBy", "CreatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("DevicemediaEntity", "UpdatedBy", "UpdatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("DevicemediaEntity", "WebserviceUrl", "WebserviceUrl", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 5);
			this.AddElementFieldMapping("DevicemediaEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 6);
			this.AddElementFieldMapping("DevicemediaEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 7);
		}

		/// <summary>Inits DeviceTokenHistoryEntity's mappings</summary>
		private void InitDeviceTokenHistoryEntityMappings()
		{
			this.AddElementMapping("DeviceTokenHistoryEntity", @"Obymobi", @"dbo", "DeviceTokenHistory", 10, 0);
			this.AddElementFieldMapping("DeviceTokenHistoryEntity", "DeviceTokenHistoryId", "DeviceTokenHistoryId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("DeviceTokenHistoryEntity", "DeviceId", "DeviceId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("DeviceTokenHistoryEntity", "Identifier", "Identifier", false, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 2);
			this.AddElementFieldMapping("DeviceTokenHistoryEntity", "UserId", "UserId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("DeviceTokenHistoryEntity", "Username", "Username", true, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 4);
			this.AddElementFieldMapping("DeviceTokenHistoryEntity", "Token", "Token", true, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 5);
			this.AddElementFieldMapping("DeviceTokenHistoryEntity", "Action", "Action", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 6);
			this.AddElementFieldMapping("DeviceTokenHistoryEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 7);
			this.AddElementFieldMapping("DeviceTokenHistoryEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 8);
			this.AddElementFieldMapping("DeviceTokenHistoryEntity", "Notes", "Notes", false, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 9);
		}

		/// <summary>Inits DeviceTokenTaskEntity's mappings</summary>
		private void InitDeviceTokenTaskEntityMappings()
		{
			this.AddElementMapping("DeviceTokenTaskEntity", @"Obymobi", @"dbo", "DeviceTokenTask", 10, 0);
			this.AddElementFieldMapping("DeviceTokenTaskEntity", "DeviceTokenTaskId", "DeviceTokenTaskId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("DeviceTokenTaskEntity", "DeviceId", "DeviceId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("DeviceTokenTaskEntity", "UserId", "UserId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("DeviceTokenTaskEntity", "Action", "Action", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("DeviceTokenTaskEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 4);
			this.AddElementFieldMapping("DeviceTokenTaskEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 5);
			this.AddElementFieldMapping("DeviceTokenTaskEntity", "State", "State", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 6);
			this.AddElementFieldMapping("DeviceTokenTaskEntity", "Token", "Token", true, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 7);
			this.AddElementFieldMapping("DeviceTokenTaskEntity", "StateUpdatedUTC", "StateUpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 8);
			this.AddElementFieldMapping("DeviceTokenTaskEntity", "Notes", "Notes", false, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 9);
		}

		/// <summary>Inits EntertainmentEntity's mappings</summary>
		private void InitEntertainmentEntityMappings()
		{
			this.AddElementMapping("EntertainmentEntity", @"Obymobi", @"dbo", "Entertainment", 34, 0);
			this.AddElementFieldMapping("EntertainmentEntity", "EntertainmentId", "EntertainmentId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("EntertainmentEntity", "Name", "Name", true, "NVarChar", 200, 0, 0, false, "", null, typeof(System.String), 1);
			this.AddElementFieldMapping("EntertainmentEntity", "TextColor", "TextColor", true, "NVarChar", 15, 0, 0, false, "", null, typeof(System.String), 2);
			this.AddElementFieldMapping("EntertainmentEntity", "BackgroundColor", "BackgroundColor", true, "NVarChar", 15, 0, 0, false, "", null, typeof(System.String), 3);
			this.AddElementFieldMapping("EntertainmentEntity", "Visible", "Visible", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 4);
			this.AddElementFieldMapping("EntertainmentEntity", "PackageName", "PackageName", true, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 5);
			this.AddElementFieldMapping("EntertainmentEntity", "ClassName", "ClassName", true, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 6);
			this.AddElementFieldMapping("EntertainmentEntity", "DefaultEntertainmenturlId", "DefaultEntertainmenturlId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 7);
			this.AddElementFieldMapping("EntertainmentEntity", "EntertainmentType", "EntertainmentType", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 8);
			this.AddElementFieldMapping("EntertainmentEntity", "SurveyName", "SurveyName", true, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 9);
			this.AddElementFieldMapping("EntertainmentEntity", "FormName", "FormName", true, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 10);
			this.AddElementFieldMapping("EntertainmentEntity", "SocialmediaType", "SocialmediaType", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 11);
			this.AddElementFieldMapping("EntertainmentEntity", "BackButton", "BackButton", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 12);
			this.AddElementFieldMapping("EntertainmentEntity", "EntertainmentcategoryId", "EntertainmentcategoryId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 13);
			this.AddElementFieldMapping("EntertainmentEntity", "CompanyId", "CompanyId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 14);
			this.AddElementFieldMapping("EntertainmentEntity", "Description", "Description", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 15);
			this.AddElementFieldMapping("EntertainmentEntity", "UpdatedBy", "UpdatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 16);
			this.AddElementFieldMapping("EntertainmentEntity", "CreatedBy", "CreatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 17);
			this.AddElementFieldMapping("EntertainmentEntity", "AnnouncementAction", "AnnouncementAction", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 18);
			this.AddElementFieldMapping("EntertainmentEntity", "PreventCaching", "PreventCaching", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 19);
			this.AddElementFieldMapping("EntertainmentEntity", "PostfixWithTimestamp", "PostfixWithTimestamp", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 20);
			this.AddElementFieldMapping("EntertainmentEntity", "HomeButton", "HomeButton", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 21);
			this.AddElementFieldMapping("EntertainmentEntity", "MenuContainer", "MenuContainer", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 22);
			this.AddElementFieldMapping("EntertainmentEntity", "NavigateButton", "NavigateButton", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 23);
			this.AddElementFieldMapping("EntertainmentEntity", "NavigationBar", "NavigationBar", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 24);
			this.AddElementFieldMapping("EntertainmentEntity", "TitleAsHeader", "TitleAsHeader", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 25);
			this.AddElementFieldMapping("EntertainmentEntity", "RestrictedAccess", "RestrictedAccess", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 26);
			this.AddElementFieldMapping("EntertainmentEntity", "EntertainmentFileId", "EntertainmentFileId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 27);
			this.AddElementFieldMapping("EntertainmentEntity", "EntertainmentFileVersion", "EntertainmentFileVersion", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 28);
			this.AddElementFieldMapping("EntertainmentEntity", "AppDataClearInterval", "AppDataClearInterval", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 29);
			this.AddElementFieldMapping("EntertainmentEntity", "SiteId", "SiteId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 30);
			this.AddElementFieldMapping("EntertainmentEntity", "AppCloseInterval", "AppCloseInterval", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 31);
			this.AddElementFieldMapping("EntertainmentEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 32);
			this.AddElementFieldMapping("EntertainmentEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 33);
		}

		/// <summary>Inits EntertainmentcategoryEntity's mappings</summary>
		private void InitEntertainmentcategoryEntityMappings()
		{
			this.AddElementMapping("EntertainmentcategoryEntity", @"Obymobi", @"dbo", "Entertainmentcategory", 6, 0);
			this.AddElementFieldMapping("EntertainmentcategoryEntity", "EntertainmentcategoryId", "EntertainmentcategoryId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("EntertainmentcategoryEntity", "Name", "Name", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 1);
			this.AddElementFieldMapping("EntertainmentcategoryEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 2);
			this.AddElementFieldMapping("EntertainmentcategoryEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 3);
			this.AddElementFieldMapping("EntertainmentcategoryEntity", "CreatedBy", "CreatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("EntertainmentcategoryEntity", "UpdatedBy", "UpdatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 5);
		}

		/// <summary>Inits EntertainmentcategoryLanguageEntity's mappings</summary>
		private void InitEntertainmentcategoryLanguageEntityMappings()
		{
			this.AddElementMapping("EntertainmentcategoryLanguageEntity", @"Obymobi", @"dbo", "EntertainmentcategoryLanguage", 8, 0);
			this.AddElementFieldMapping("EntertainmentcategoryLanguageEntity", "EntertainmentcategoryLanguageId", "EntertainmentcategoryLanguageId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("EntertainmentcategoryLanguageEntity", "EntertainmentcategoryId", "EntertainmentcategoryId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("EntertainmentcategoryLanguageEntity", "LanguageId", "LanguageId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("EntertainmentcategoryLanguageEntity", "Name", "Name", true, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 3);
			this.AddElementFieldMapping("EntertainmentcategoryLanguageEntity", "CreatedBy", "CreatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("EntertainmentcategoryLanguageEntity", "UpdatedBy", "UpdatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 5);
			this.AddElementFieldMapping("EntertainmentcategoryLanguageEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 6);
			this.AddElementFieldMapping("EntertainmentcategoryLanguageEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 7);
		}

		/// <summary>Inits EntertainmentConfigurationEntity's mappings</summary>
		private void InitEntertainmentConfigurationEntityMappings()
		{
			this.AddElementMapping("EntertainmentConfigurationEntity", @"Obymobi", @"dbo", "EntertainmentConfiguration", 7, 0);
			this.AddElementFieldMapping("EntertainmentConfigurationEntity", "EntertainmentConfigurationId", "EntertainmentConfigurationId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("EntertainmentConfigurationEntity", "CompanyId", "CompanyId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("EntertainmentConfigurationEntity", "Name", "Name", false, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 2);
			this.AddElementFieldMapping("EntertainmentConfigurationEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 3);
			this.AddElementFieldMapping("EntertainmentConfigurationEntity", "CreatedBy", "CreatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("EntertainmentConfigurationEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 5);
			this.AddElementFieldMapping("EntertainmentConfigurationEntity", "UpdatedBy", "UpdatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 6);
		}

		/// <summary>Inits EntertainmentConfigurationEntertainmentEntity's mappings</summary>
		private void InitEntertainmentConfigurationEntertainmentEntityMappings()
		{
			this.AddElementMapping("EntertainmentConfigurationEntertainmentEntity", @"Obymobi", @"dbo", "EntertainmentConfigurationEntertainment", 9, 0);
			this.AddElementFieldMapping("EntertainmentConfigurationEntertainmentEntity", "EntertainmentConfigurationEntertainmentId", "EntertainmentConfigurationEntertainmentId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("EntertainmentConfigurationEntertainmentEntity", "ParentCompanyId", "ParentCompanyId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("EntertainmentConfigurationEntertainmentEntity", "EntertainmentConfigurationId", "EntertainmentConfigurationId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("EntertainmentConfigurationEntertainmentEntity", "EntertainmentId", "EntertainmentId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("EntertainmentConfigurationEntertainmentEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 4);
			this.AddElementFieldMapping("EntertainmentConfigurationEntertainmentEntity", "CreatedBy", "CreatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 5);
			this.AddElementFieldMapping("EntertainmentConfigurationEntertainmentEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 6);
			this.AddElementFieldMapping("EntertainmentConfigurationEntertainmentEntity", "UpdatedBy", "UpdatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 7);
			this.AddElementFieldMapping("EntertainmentConfigurationEntertainmentEntity", "SortOrder", "SortOrder", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 8);
		}

		/// <summary>Inits EntertainmentDependencyEntity's mappings</summary>
		private void InitEntertainmentDependencyEntityMappings()
		{
			this.AddElementMapping("EntertainmentDependencyEntity", @"Obymobi", @"dbo", "EntertainmentDependency", 8, 0);
			this.AddElementFieldMapping("EntertainmentDependencyEntity", "EntertainmentDependencyId", "EntertainmentDependencyId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("EntertainmentDependencyEntity", "ParentCompanyId", "ParentCompanyId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("EntertainmentDependencyEntity", "EntertainmentId", "EntertainmentId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("EntertainmentDependencyEntity", "DependentEntertainmentId", "DependentEntertainmentId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("EntertainmentDependencyEntity", "CreatedBy", "CreatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("EntertainmentDependencyEntity", "UpdatedBy", "UpdatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 5);
			this.AddElementFieldMapping("EntertainmentDependencyEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 6);
			this.AddElementFieldMapping("EntertainmentDependencyEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 7);
		}

		/// <summary>Inits EntertainmentFileEntity's mappings</summary>
		private void InitEntertainmentFileEntityMappings()
		{
			this.AddElementMapping("EntertainmentFileEntity", @"Obymobi", @"dbo", "EntertainmentFile", 7, 0);
			this.AddElementFieldMapping("EntertainmentFileEntity", "EntertainmentFileId", "EntertainmentFileId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("EntertainmentFileEntity", "Blob", "Blob", false, "VarBinary", 2147483647, 0, 0, false, "", null, typeof(System.Byte[]), 1);
			this.AddElementFieldMapping("EntertainmentFileEntity", "Filename", "Filename", false, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 2);
			this.AddElementFieldMapping("EntertainmentFileEntity", "UpdatedBy", "UpdatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("EntertainmentFileEntity", "CreatedBy", "CreatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("EntertainmentFileEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 5);
			this.AddElementFieldMapping("EntertainmentFileEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 6);
		}

		/// <summary>Inits EntertainmenturlEntity's mappings</summary>
		private void InitEntertainmenturlEntityMappings()
		{
			this.AddElementMapping("EntertainmenturlEntity", @"Obymobi", @"dbo", "Entertainmenturl", 10, 0);
			this.AddElementFieldMapping("EntertainmenturlEntity", "EntertainmenturlId", "EntertainmenturlId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("EntertainmenturlEntity", "EntertainmentId", "EntertainmentId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("EntertainmenturlEntity", "Url", "Url", true, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 2);
			this.AddElementFieldMapping("EntertainmenturlEntity", "AccessFullDomain", "AccessFullDomain", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 3);
			this.AddElementFieldMapping("EntertainmenturlEntity", "InitialZoom", "InitialZoom", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("EntertainmenturlEntity", "UpdatedBy", "UpdatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 5);
			this.AddElementFieldMapping("EntertainmenturlEntity", "CreatedBy", "CreatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 6);
			this.AddElementFieldMapping("EntertainmenturlEntity", "ParentCompanyId", "ParentCompanyId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 7);
			this.AddElementFieldMapping("EntertainmenturlEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 8);
			this.AddElementFieldMapping("EntertainmenturlEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 9);
		}

		/// <summary>Inits EntityFieldInformationEntity's mappings</summary>
		private void InitEntityFieldInformationEntityMappings()
		{
			this.AddElementMapping("EntityFieldInformationEntity", @"Obymobi", @"dbo", "EntityFieldInformation", 30, 0);
			this.AddElementFieldMapping("EntityFieldInformationEntity", "EntityFieldInformationId", "EntityFieldInformationId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("EntityFieldInformationEntity", "EntityName", "EntityName", true, "NVarChar", 150, 0, 0, false, "", null, typeof(System.String), 1);
			this.AddElementFieldMapping("EntityFieldInformationEntity", "FieldName", "FieldName", true, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 2);
			this.AddElementFieldMapping("EntityFieldInformationEntity", "FriendlyName", "FriendlyName", true, "NVarChar", 100, 0, 0, false, "", null, typeof(System.String), 3);
			this.AddElementFieldMapping("EntityFieldInformationEntity", "ShowOnDefaultGridView", "ShowOnDefaultGridView", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 4);
			this.AddElementFieldMapping("EntityFieldInformationEntity", "DefaultDisplayPosition", "DefaultDisplayPosition", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 5);
			this.AddElementFieldMapping("EntityFieldInformationEntity", "AllowShowOnGridView", "AllowShowOnGridView", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 6);
			this.AddElementFieldMapping("EntityFieldInformationEntity", "HelpText", "HelpText", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 7);
			this.AddElementFieldMapping("EntityFieldInformationEntity", "DisplayFormatString", "DisplayFormatString", true, "NVarChar", 100, 0, 0, false, "", null, typeof(System.String), 8);
			this.AddElementFieldMapping("EntityFieldInformationEntity", "DefaultSortPosition", "DefaultSortPosition", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 9);
			this.AddElementFieldMapping("EntityFieldInformationEntity", "DefaultSortOperator", "DefaultSortOperator", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 10);
			this.AddElementFieldMapping("EntityFieldInformationEntity", "Type", "Type", true, "NVarChar", 100, 0, 0, false, "", null, typeof(System.String), 11);
			this.AddElementFieldMapping("EntityFieldInformationEntity", "IsRequired", "IsRequired", true, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 12);
			this.AddElementFieldMapping("EntityFieldInformationEntity", "EntityFieldType", "EntityFieldType", true, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 13);
			this.AddElementFieldMapping("EntityFieldInformationEntity", "ExcludeForUpdateWmsCacheDateOnChange", "ExcludeForUpdateWmsCacheDateOnChange", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 14);
			this.AddElementFieldMapping("EntityFieldInformationEntity", "ExcludeForUpdateCompanyDataVersionOnChange", "ExcludeForUpdateCompanyDataVersionOnChange", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 15);
			this.AddElementFieldMapping("EntityFieldInformationEntity", "ExcludeForUpdateMenuDataVersionOnChange", "ExcludeForUpdateMenuDataVersionOnChange", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 16);
			this.AddElementFieldMapping("EntityFieldInformationEntity", "ExcludeForUpdatePosIntegrationInformationVersionOnChange", "ExcludeForUpdatePosIntegrationInformationVersionOnChange", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 17);
			this.AddElementFieldMapping("EntityFieldInformationEntity", "ForLocalUseWhileSyncingIsUpdated", "ForLocalUseWhileSyncingIsUpdated", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 18);
			this.AddElementFieldMapping("EntityFieldInformationEntity", "Archived", "Archived", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 19);
			this.AddElementFieldMapping("EntityFieldInformationEntity", "CreatedBy", "CreatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 20);
			this.AddElementFieldMapping("EntityFieldInformationEntity", "UpdatedBy", "UpdatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 21);
			this.AddElementFieldMapping("EntityFieldInformationEntity", "Deleted", "Deleted", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 22);
			this.AddElementFieldMapping("EntityFieldInformationEntity", "ExcludeForUpdateDeliverypointDataVersionOnChange", "ExcludeForUpdateDeliverypointDataVersionOnChange", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 23);
			this.AddElementFieldMapping("EntityFieldInformationEntity", "ExcludeForUpdateSurveyDataVersionOnChange", "ExcludeForUpdateSurveyDataVersionOnChange", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 24);
			this.AddElementFieldMapping("EntityFieldInformationEntity", "ExcludeForUpdateAnnouncementDataVersionOnChange", "ExcludeForUpdateAnnouncementDataVersionOnChange", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 25);
			this.AddElementFieldMapping("EntityFieldInformationEntity", "ExcludeForUpdateEntertainmentDataVersionOnChange", "ExcludeForUpdateEntertainmentDataVersionOnChange", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 26);
			this.AddElementFieldMapping("EntityFieldInformationEntity", "ExcludeForUpdateAdvertisementDataVersionOnChange", "ExcludeForUpdateAdvertisementDataVersionOnChange", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 27);
			this.AddElementFieldMapping("EntityFieldInformationEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 28);
			this.AddElementFieldMapping("EntityFieldInformationEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 29);
		}

		/// <summary>Inits EntityFieldInformationCustomEntity's mappings</summary>
		private void InitEntityFieldInformationCustomEntityMappings()
		{
			this.AddElementMapping("EntityFieldInformationCustomEntity", @"Obymobi", @"dbo", "EntityFieldInformationCustom", 16, 0);
			this.AddElementFieldMapping("EntityFieldInformationCustomEntity", "EntityName", "EntityName", false, "VarChar", 150, 0, 0, false, "", null, typeof(System.String), 0);
			this.AddElementFieldMapping("EntityFieldInformationCustomEntity", "FieldName", "FieldName", false, "VarChar", 255, 0, 0, false, "", null, typeof(System.String), 1);
			this.AddElementFieldMapping("EntityFieldInformationCustomEntity", "FriendlyName", "FriendlyName", true, "NVarChar", 100, 0, 0, false, "", null, typeof(System.String), 2);
			this.AddElementFieldMapping("EntityFieldInformationCustomEntity", "ShowOnDefaultGridView", "ShowOnDefaultGridView", true, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 3);
			this.AddElementFieldMapping("EntityFieldInformationCustomEntity", "DefaultDisplayPosition", "DefaultDisplayPosition", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("EntityFieldInformationCustomEntity", "DefaultSortPosition", "DefaultSortPosition", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 5);
			this.AddElementFieldMapping("EntityFieldInformationCustomEntity", "DefaultSortOperator", "DefaultSortOperator", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 6);
			this.AddElementFieldMapping("EntityFieldInformationCustomEntity", "AllowShowOnGridView", "AllowShowOnGridView", true, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 7);
			this.AddElementFieldMapping("EntityFieldInformationCustomEntity", "IsRequired", "IsRequired", true, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 8);
			this.AddElementFieldMapping("EntityFieldInformationCustomEntity", "ForLocalUseWhileSyncingIsUpdated", "ForLocalUseWhileSyncingIsUpdated", true, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 9);
			this.AddElementFieldMapping("EntityFieldInformationCustomEntity", "DisplayFormatString", "DisplayFormatString", true, "NVarChar", 100, 0, 0, false, "", null, typeof(System.String), 10);
			this.AddElementFieldMapping("EntityFieldInformationCustomEntity", "Archived", "Archived", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 11);
			this.AddElementFieldMapping("EntityFieldInformationCustomEntity", "CreatedBy", "CreatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 12);
			this.AddElementFieldMapping("EntityFieldInformationCustomEntity", "UpdatedBy", "UpdatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 13);
			this.AddElementFieldMapping("EntityFieldInformationCustomEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 14);
			this.AddElementFieldMapping("EntityFieldInformationCustomEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 15);
		}

		/// <summary>Inits EntityInformationEntity's mappings</summary>
		private void InitEntityInformationEntityMappings()
		{
			this.AddElementMapping("EntityInformationEntity", @"Obymobi", @"dbo", "EntityInformation", 26, 0);
			this.AddElementFieldMapping("EntityInformationEntity", "EntityInformationId", "EntityInformationId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("EntityInformationEntity", "EntityName", "EntityName", false, "VarChar", 250, 0, 0, false, "", null, typeof(System.String), 1);
			this.AddElementFieldMapping("EntityInformationEntity", "ShowFieldName", "ShowFieldName", true, "NVarChar", 250, 0, 0, false, "", null, typeof(System.String), 2);
			this.AddElementFieldMapping("EntityInformationEntity", "ShowFieldNameBreadCrumb", "ShowFieldNameBreadCrumb", true, "NVarChar", 250, 0, 0, false, "", null, typeof(System.String), 3);
			this.AddElementFieldMapping("EntityInformationEntity", "FriendlyName", "FriendlyName", true, "VarChar", 150, 0, 0, false, "", null, typeof(System.String), 4);
			this.AddElementFieldMapping("EntityInformationEntity", "FriendlyNamePlural", "FriendlyNamePlural", true, "VarChar", 150, 0, 0, false, "", null, typeof(System.String), 5);
			this.AddElementFieldMapping("EntityInformationEntity", "HelpText", "HelpText", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 6);
			this.AddElementFieldMapping("EntityInformationEntity", "TitleAsBreadCrumbHierarchy", "TitleAsBreadCrumbHierarchy", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 7);
			this.AddElementFieldMapping("EntityInformationEntity", "DefaultEntityEditPage", "DefaultEntityEditPage", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 8);
			this.AddElementFieldMapping("EntityInformationEntity", "DefaultEntityCollectionPage", "DefaultEntityCollectionPage", true, "VarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 9);
			this.AddElementFieldMapping("EntityInformationEntity", "UpdateWmsCacheDateOnChange", "UpdateWmsCacheDateOnChange", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 10);
			this.AddElementFieldMapping("EntityInformationEntity", "UpdateCompanyDataVersionOnChange", "UpdateCompanyDataVersionOnChange", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 11);
			this.AddElementFieldMapping("EntityInformationEntity", "UpdateMenuDataVersionOnChange", "UpdateMenuDataVersionOnChange", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 12);
			this.AddElementFieldMapping("EntityInformationEntity", "UpdatePosIntegrationInformationOnChange", "UpdatePosIntegrationInformationOnChange", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 13);
			this.AddElementFieldMapping("EntityInformationEntity", "ForLocalUseWhileSyncingIsUpdated", "ForLocalUseWhileSyncingIsUpdated", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 14);
			this.AddElementFieldMapping("EntityInformationEntity", "Archived", "Archived", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 15);
			this.AddElementFieldMapping("EntityInformationEntity", "CreatedBy", "CreatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 16);
			this.AddElementFieldMapping("EntityInformationEntity", "UpdatedBy", "UpdatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 17);
			this.AddElementFieldMapping("EntityInformationEntity", "Deleted", "Deleted", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 18);
			this.AddElementFieldMapping("EntityInformationEntity", "UpdateDeliverypointDataVersionOnChange", "UpdateDeliverypointDataVersionOnChange", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 19);
			this.AddElementFieldMapping("EntityInformationEntity", "UpdateSurveyDataVersionOnChange", "UpdateSurveyDataVersionOnChange", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 20);
			this.AddElementFieldMapping("EntityInformationEntity", "UpdateAnnouncementDataVersionOnChange", "UpdateAnnouncementDataVersionOnChange", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 21);
			this.AddElementFieldMapping("EntityInformationEntity", "UpdateEntertainmentDataVersionOnChange", "UpdateEntertainmentDataVersionOnChange", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 22);
			this.AddElementFieldMapping("EntityInformationEntity", "UpdateAdvertisementDataVersionOnChange", "UpdateAdvertisementDataVersionOnChange", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 23);
			this.AddElementFieldMapping("EntityInformationEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 24);
			this.AddElementFieldMapping("EntityInformationEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 25);
		}

		/// <summary>Inits EntityInformationCustomEntity's mappings</summary>
		private void InitEntityInformationCustomEntityMappings()
		{
			this.AddElementMapping("EntityInformationCustomEntity", @"Obymobi", @"dbo", "EntityInformationCustom", 14, 0);
			this.AddElementFieldMapping("EntityInformationCustomEntity", "EntityName", "EntityName", false, "VarChar", 150, 0, 0, false, "", null, typeof(System.String), 0);
			this.AddElementFieldMapping("EntityInformationCustomEntity", "ShowFieldName", "ShowFieldName", true, "NVarChar", 150, 0, 0, false, "", null, typeof(System.String), 1);
			this.AddElementFieldMapping("EntityInformationCustomEntity", "FriendlyName", "FriendlyName", true, "VarChar", 150, 0, 0, false, "", null, typeof(System.String), 2);
			this.AddElementFieldMapping("EntityInformationCustomEntity", "FriendlyNamePlural", "FriendlyNamePlural", true, "VarChar", 150, 0, 0, false, "", null, typeof(System.String), 3);
			this.AddElementFieldMapping("EntityInformationCustomEntity", "HelpText", "HelpText", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 4);
			this.AddElementFieldMapping("EntityInformationCustomEntity", "TitleAsBreadCrumbHierarchy", "TitleAsBreadCrumbHierarchy", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 5);
			this.AddElementFieldMapping("EntityInformationCustomEntity", "DefaultEntityEditPage", "DefaultEntityEditPage", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 6);
			this.AddElementFieldMapping("EntityInformationCustomEntity", "DefaultEntityCollectionPage", "DefaultEntityCollectionPage", true, "VarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 7);
			this.AddElementFieldMapping("EntityInformationCustomEntity", "ForLocalUseWhileSyncingIsUpdated", "ForLocalUseWhileSyncingIsUpdated", true, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 8);
			this.AddElementFieldMapping("EntityInformationCustomEntity", "Archived", "Archived", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 9);
			this.AddElementFieldMapping("EntityInformationCustomEntity", "CreatedBy", "CreatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 10);
			this.AddElementFieldMapping("EntityInformationCustomEntity", "UpdatedBy", "UpdatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 11);
			this.AddElementFieldMapping("EntityInformationCustomEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 12);
			this.AddElementFieldMapping("EntityInformationCustomEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 13);
		}

		/// <summary>Inits ExternalDeliverypointEntity's mappings</summary>
		private void InitExternalDeliverypointEntityMappings()
		{
			this.AddElementMapping("ExternalDeliverypointEntity", @"Obymobi", @"dbo", "ExternalDeliverypoint", 28, 0);
			this.AddElementFieldMapping("ExternalDeliverypointEntity", "ExternalDeliverypointId", "ExternalDeliverypointId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("ExternalDeliverypointEntity", "ExternalSystemId", "ExternalSystemId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("ExternalDeliverypointEntity", "Id", "Id", false, "VarChar", 50, 0, 0, false, "", null, typeof(System.String), 2);
			this.AddElementFieldMapping("ExternalDeliverypointEntity", "Name", "Name", false, "NVarChar", 100, 0, 0, false, "", null, typeof(System.String), 3);
			this.AddElementFieldMapping("ExternalDeliverypointEntity", "Visible", "Visible", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 4);
			this.AddElementFieldMapping("ExternalDeliverypointEntity", "StringValue1", "StringValue1", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 5);
			this.AddElementFieldMapping("ExternalDeliverypointEntity", "StringValue2", "StringValue2", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 6);
			this.AddElementFieldMapping("ExternalDeliverypointEntity", "StringValue3", "StringValue3", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 7);
			this.AddElementFieldMapping("ExternalDeliverypointEntity", "StringValue4", "StringValue4", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 8);
			this.AddElementFieldMapping("ExternalDeliverypointEntity", "StringValue5", "StringValue5", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 9);
			this.AddElementFieldMapping("ExternalDeliverypointEntity", "IntValue1", "IntValue1", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 10);
			this.AddElementFieldMapping("ExternalDeliverypointEntity", "IntValue2", "IntValue2", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 11);
			this.AddElementFieldMapping("ExternalDeliverypointEntity", "IntValue3", "IntValue3", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 12);
			this.AddElementFieldMapping("ExternalDeliverypointEntity", "IntValue4", "IntValue4", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 13);
			this.AddElementFieldMapping("ExternalDeliverypointEntity", "IntValue5", "IntValue5", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 14);
			this.AddElementFieldMapping("ExternalDeliverypointEntity", "BoolValue1", "BoolValue1", true, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 15);
			this.AddElementFieldMapping("ExternalDeliverypointEntity", "BoolValue2", "BoolValue2", true, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 16);
			this.AddElementFieldMapping("ExternalDeliverypointEntity", "BoolValue3", "BoolValue3", true, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 17);
			this.AddElementFieldMapping("ExternalDeliverypointEntity", "BoolValue4", "BoolValue4", true, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 18);
			this.AddElementFieldMapping("ExternalDeliverypointEntity", "BoolValue5", "BoolValue5", true, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 19);
			this.AddElementFieldMapping("ExternalDeliverypointEntity", "CreatedInBatchId", "CreatedInBatchId", true, "BigInt", 0, 19, 0, false, "", null, typeof(System.Int64), 20);
			this.AddElementFieldMapping("ExternalDeliverypointEntity", "UpdatedInBatchId", "UpdatedInBatchId", true, "BigInt", 0, 19, 0, false, "", null, typeof(System.Int64), 21);
			this.AddElementFieldMapping("ExternalDeliverypointEntity", "SynchronisationBatchId", "SynchronisationBatchId", true, "BigInt", 0, 19, 0, false, "", null, typeof(System.Int64), 22);
			this.AddElementFieldMapping("ExternalDeliverypointEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 23);
			this.AddElementFieldMapping("ExternalDeliverypointEntity", "CreatedBy", "CreatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 24);
			this.AddElementFieldMapping("ExternalDeliverypointEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 25);
			this.AddElementFieldMapping("ExternalDeliverypointEntity", "UpdatedBy", "UpdatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 26);
			this.AddElementFieldMapping("ExternalDeliverypointEntity", "ParentCompanyId", "ParentCompanyId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 27);
		}

		/// <summary>Inits ExternalMenuEntity's mappings</summary>
		private void InitExternalMenuEntityMappings()
		{
			this.AddElementMapping("ExternalMenuEntity", @"Obymobi", @"dbo", "ExternalMenu", 7, 0);
			this.AddElementFieldMapping("ExternalMenuEntity", "ExternalMenuId", "ExternalMenuId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("ExternalMenuEntity", "ExternalSystemId", "ExternalSystemId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("ExternalMenuEntity", "Id", "Id", false, "VarChar", 50, 0, 0, false, "", null, typeof(System.String), 2);
			this.AddElementFieldMapping("ExternalMenuEntity", "Name", "Name", true, "NVarChar", 256, 0, 0, false, "", null, typeof(System.String), 3);
			this.AddElementFieldMapping("ExternalMenuEntity", "CreatedUTC", "CreatedUTC", false, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 4);
			this.AddElementFieldMapping("ExternalMenuEntity", "UpdateUTC", "UpdateUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 5);
			this.AddElementFieldMapping("ExternalMenuEntity", "ParentCompanyId", "ParentCompanyId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 6);
		}

		/// <summary>Inits ExternalProductEntity's mappings</summary>
		private void InitExternalProductEntityMappings()
		{
			this.AddElementMapping("ExternalProductEntity", @"Obymobi", @"dbo", "ExternalProduct", 36, 0);
			this.AddElementFieldMapping("ExternalProductEntity", "ExternalProductId", "ExternalProductId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("ExternalProductEntity", "ExternalSystemId", "ExternalSystemId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("ExternalProductEntity", "Id", "Id", false, "VarChar", 50, 0, 0, false, "", null, typeof(System.String), 2);
			this.AddElementFieldMapping("ExternalProductEntity", "Name", "Name", false, "NVarChar", 100, 0, 0, false, "", null, typeof(System.String), 3);
			this.AddElementFieldMapping("ExternalProductEntity", "Price", "Price", false, "Money", 0, 19, 4, false, "", null, typeof(System.Decimal), 4);
			this.AddElementFieldMapping("ExternalProductEntity", "Visible", "Visible", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 5);
			this.AddElementFieldMapping("ExternalProductEntity", "StringValue1", "StringValue1", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 6);
			this.AddElementFieldMapping("ExternalProductEntity", "StringValue2", "StringValue2", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 7);
			this.AddElementFieldMapping("ExternalProductEntity", "StringValue3", "StringValue3", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 8);
			this.AddElementFieldMapping("ExternalProductEntity", "StringValue4", "StringValue4", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 9);
			this.AddElementFieldMapping("ExternalProductEntity", "StringValue5", "StringValue5", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 10);
			this.AddElementFieldMapping("ExternalProductEntity", "IntValue1", "IntValue1", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 11);
			this.AddElementFieldMapping("ExternalProductEntity", "IntValue2", "IntValue2", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 12);
			this.AddElementFieldMapping("ExternalProductEntity", "IntValue3", "IntValue3", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 13);
			this.AddElementFieldMapping("ExternalProductEntity", "IntValue4", "IntValue4", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 14);
			this.AddElementFieldMapping("ExternalProductEntity", "IntValue5", "IntValue5", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 15);
			this.AddElementFieldMapping("ExternalProductEntity", "BoolValue1", "BoolValue1", true, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 16);
			this.AddElementFieldMapping("ExternalProductEntity", "BoolValue2", "BoolValue2", true, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 17);
			this.AddElementFieldMapping("ExternalProductEntity", "BoolValue3", "BoolValue3", true, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 18);
			this.AddElementFieldMapping("ExternalProductEntity", "BoolValue4", "BoolValue4", true, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 19);
			this.AddElementFieldMapping("ExternalProductEntity", "BoolValue5", "BoolValue5", true, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 20);
			this.AddElementFieldMapping("ExternalProductEntity", "CreatedInBatchId", "CreatedInBatchId", true, "BigInt", 0, 19, 0, false, "", null, typeof(System.Int64), 21);
			this.AddElementFieldMapping("ExternalProductEntity", "UpdatedInBatchId", "UpdatedInBatchId", true, "BigInt", 0, 19, 0, false, "", null, typeof(System.Int64), 22);
			this.AddElementFieldMapping("ExternalProductEntity", "SynchronisationBatchId", "SynchronisationBatchId", true, "BigInt", 0, 19, 0, false, "", null, typeof(System.Int64), 23);
			this.AddElementFieldMapping("ExternalProductEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 24);
			this.AddElementFieldMapping("ExternalProductEntity", "CreatedBy", "CreatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 25);
			this.AddElementFieldMapping("ExternalProductEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 26);
			this.AddElementFieldMapping("ExternalProductEntity", "UpdatedBy", "UpdatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 27);
			this.AddElementFieldMapping("ExternalProductEntity", "ParentCompanyId", "ParentCompanyId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 28);
			this.AddElementFieldMapping("ExternalProductEntity", "Type", "Type", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 29);
			this.AddElementFieldMapping("ExternalProductEntity", "MinOptions", "MinOptions", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 30);
			this.AddElementFieldMapping("ExternalProductEntity", "MaxOptions", "MaxOptions", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 31);
			this.AddElementFieldMapping("ExternalProductEntity", "AllowDuplicates", "AllowDuplicates", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 32);
			this.AddElementFieldMapping("ExternalProductEntity", "IsEnabled", "IsEnabled", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 33);
			this.AddElementFieldMapping("ExternalProductEntity", "IsSnoozed", "IsSnoozed", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 34);
			this.AddElementFieldMapping("ExternalProductEntity", "ExternalMenuId", "ExternalMenuId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 35);
		}

		/// <summary>Inits ExternalSubProductEntity's mappings</summary>
		private void InitExternalSubProductEntityMappings()
		{
			this.AddElementMapping("ExternalSubProductEntity", @"Obymobi", @"dbo", "ExternalSubProduct", 5, 0);
			this.AddElementFieldMapping("ExternalSubProductEntity", "ExternalProductExternalProductId", "ExternalProductExternalProductId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("ExternalSubProductEntity", "ExternalProductId", "ExternalProductId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("ExternalSubProductEntity", "ExternalSubProductId", "ExternalSubProductId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("ExternalSubProductEntity", "CreatedUTC", "CreatedUTC", false, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 3);
			this.AddElementFieldMapping("ExternalSubProductEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 4);
		}

		/// <summary>Inits ExternalSystemEntity's mappings</summary>
		private void InitExternalSystemEntityMappings()
		{
			this.AddElementMapping("ExternalSystemEntity", @"Obymobi", @"dbo", "ExternalSystem", 21, 0);
			this.AddElementFieldMapping("ExternalSystemEntity", "ExternalSystemId", "ExternalSystemId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("ExternalSystemEntity", "CompanyId", "CompanyId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("ExternalSystemEntity", "Name", "Name", false, "VarChar", 255, 0, 0, false, "", null, typeof(System.String), 2);
			this.AddElementFieldMapping("ExternalSystemEntity", "Type", "Type", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("ExternalSystemEntity", "StringValue1", "StringValue1", true, "VarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 4);
			this.AddElementFieldMapping("ExternalSystemEntity", "StringValue2", "StringValue2", true, "VarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 5);
			this.AddElementFieldMapping("ExternalSystemEntity", "StringValue3", "StringValue3", true, "VarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 6);
			this.AddElementFieldMapping("ExternalSystemEntity", "StringValue4", "StringValue4", true, "VarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 7);
			this.AddElementFieldMapping("ExternalSystemEntity", "StringValue5", "StringValue5", true, "VarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 8);
			this.AddElementFieldMapping("ExternalSystemEntity", "ExpiryDateUtc", "ExpiryDateUtc", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 9);
			this.AddElementFieldMapping("ExternalSystemEntity", "CreatedUTC", "CreatedUTC", false, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 10);
			this.AddElementFieldMapping("ExternalSystemEntity", "CreatedBy", "CreatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 11);
			this.AddElementFieldMapping("ExternalSystemEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 12);
			this.AddElementFieldMapping("ExternalSystemEntity", "UpdatedBy", "UpdatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 13);
			this.AddElementFieldMapping("ExternalSystemEntity", "Environment", "Environment", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 14);
			this.AddElementFieldMapping("ExternalSystemEntity", "BoolValue1", "BoolValue1", true, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 15);
			this.AddElementFieldMapping("ExternalSystemEntity", "BoolValue2", "BoolValue2", true, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 16);
			this.AddElementFieldMapping("ExternalSystemEntity", "BoolValue3", "BoolValue3", true, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 17);
			this.AddElementFieldMapping("ExternalSystemEntity", "BoolValue4", "BoolValue4", true, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 18);
			this.AddElementFieldMapping("ExternalSystemEntity", "BoolValue5", "BoolValue5", true, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 19);
			this.AddElementFieldMapping("ExternalSystemEntity", "IsBusy", "IsBusy", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 20);
		}

		/// <summary>Inits ExternalSystemLogEntity's mappings</summary>
		private void InitExternalSystemLogEntityMappings()
		{
			this.AddElementMapping("ExternalSystemLogEntity", @"Obymobi", @"dbo", "ExternalSystemLog", 10, 0);
			this.AddElementFieldMapping("ExternalSystemLogEntity", "ExternalSystemLogId", "ExternalSystemLogId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("ExternalSystemLogEntity", "ExternalSystemId", "ExternalSystemId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("ExternalSystemLogEntity", "Message", "Message", true, "NVarChar", 1024, 0, 0, false, "", null, typeof(System.String), 2);
			this.AddElementFieldMapping("ExternalSystemLogEntity", "LogType", "LogType", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("ExternalSystemLogEntity", "Success", "Success", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 4);
			this.AddElementFieldMapping("ExternalSystemLogEntity", "Raw", "Raw", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 5);
			this.AddElementFieldMapping("ExternalSystemLogEntity", "CreatedUTC", "CreatedUTC", false, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 6);
			this.AddElementFieldMapping("ExternalSystemLogEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 7);
			this.AddElementFieldMapping("ExternalSystemLogEntity", "OrderId", "OrderId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 8);
			this.AddElementFieldMapping("ExternalSystemLogEntity", "Log", "Log", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 9);
		}

		/// <summary>Inits FeatureToggleAvailabilityEntity's mappings</summary>
		private void InitFeatureToggleAvailabilityEntityMappings()
		{
			this.AddElementMapping("FeatureToggleAvailabilityEntity", @"Obymobi", @"dbo", "FeatureToggleAvailability", 6, 0);
			this.AddElementFieldMapping("FeatureToggleAvailabilityEntity", "FeatureToggleAvailabilityId", "FeatureToggleAvailabilityId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("FeatureToggleAvailabilityEntity", "FeatureToggle", "FeatureToggle", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("FeatureToggleAvailabilityEntity", "ReleaseGroup", "ReleaseGroup", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("FeatureToggleAvailabilityEntity", "Role", "Role", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("FeatureToggleAvailabilityEntity", "CreatedUTC", "CreatedUTC", false, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 4);
			this.AddElementFieldMapping("FeatureToggleAvailabilityEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 5);
		}

		/// <summary>Inits FolioEntity's mappings</summary>
		private void InitFolioEntityMappings()
		{
			this.AddElementMapping("FolioEntity", @"Obymobi", @"dbo", "Folio", 8, 0);
			this.AddElementFieldMapping("FolioEntity", "FolioId", "FolioId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("FolioEntity", "ParentCompanyId", "ParentCompanyId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("FolioEntity", "GuestInformationId", "GuestInformationId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("FolioEntity", "Balance", "Balance", false, "Money", 0, 19, 4, false, "", null, typeof(System.Decimal), 3);
			this.AddElementFieldMapping("FolioEntity", "CreatedBy", "CreatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("FolioEntity", "UpdatedBy", "UpdatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 5);
			this.AddElementFieldMapping("FolioEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 6);
			this.AddElementFieldMapping("FolioEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 7);
		}

		/// <summary>Inits FolioItemEntity's mappings</summary>
		private void InitFolioItemEntityMappings()
		{
			this.AddElementMapping("FolioItemEntity", @"Obymobi", @"dbo", "FolioItem", 12, 0);
			this.AddElementFieldMapping("FolioItemEntity", "FolioItemId", "FolioItemId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("FolioItemEntity", "ParentCompanyId", "ParentCompanyId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("FolioItemEntity", "FolioId", "FolioId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("FolioItemEntity", "Code", "Code", false, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 3);
			this.AddElementFieldMapping("FolioItemEntity", "Description", "Description", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 4);
			this.AddElementFieldMapping("FolioItemEntity", "PriceIn", "PriceIn", false, "Money", 0, 19, 4, false, "", null, typeof(System.Decimal), 5);
			this.AddElementFieldMapping("FolioItemEntity", "CurrencyCode", "CurrencyCode", true, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 6);
			this.AddElementFieldMapping("FolioItemEntity", "ReferenceId", "ReferenceId", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 7);
			this.AddElementFieldMapping("FolioItemEntity", "CreatedBy", "CreatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 8);
			this.AddElementFieldMapping("FolioItemEntity", "UpdatedBy", "UpdatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 9);
			this.AddElementFieldMapping("FolioItemEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 10);
			this.AddElementFieldMapping("FolioItemEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 11);
		}

		/// <summary>Inits GameEntity's mappings</summary>
		private void InitGameEntityMappings()
		{
			this.AddElementMapping("GameEntity", @"Obymobi", @"dbo", "Game", 6, 0);
			this.AddElementFieldMapping("GameEntity", "GameId", "GameId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("GameEntity", "Name", "Name", false, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 1);
			this.AddElementFieldMapping("GameEntity", "Type", "Type", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("GameEntity", "Url", "Url", true, "VarChar", 1024, 0, 0, false, "", null, typeof(System.String), 3);
			this.AddElementFieldMapping("GameEntity", "CreatedUTC", "CreatedUTC", false, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 4);
			this.AddElementFieldMapping("GameEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 5);
		}

		/// <summary>Inits GameSessionEntity's mappings</summary>
		private void InitGameSessionEntityMappings()
		{
			this.AddElementMapping("GameSessionEntity", @"Obymobi", @"dbo", "GameSession", 14, 0);
			this.AddElementFieldMapping("GameSessionEntity", "GameSessionId", "GameSessionId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("GameSessionEntity", "GameId", "GameId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("GameSessionEntity", "ClientId", "ClientId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("GameSessionEntity", "StartUTC", "StartUTC", false, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 3);
			this.AddElementFieldMapping("GameSessionEntity", "EndUTC", "EndUTC", false, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 4);
			this.AddElementFieldMapping("GameSessionEntity", "Length", "Length", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 5);
			this.AddElementFieldMapping("GameSessionEntity", "CreatedUTC", "CreatedUTC", false, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 6);
			this.AddElementFieldMapping("GameSessionEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 7);
			this.AddElementFieldMapping("GameSessionEntity", "ParentCompanyId", "ParentCompanyId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 8);
			this.AddElementFieldMapping("GameSessionEntity", "GameName", "GameName", false, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 9);
			this.AddElementFieldMapping("GameSessionEntity", "ClientIdentifier", "ClientIdentifier", false, "VarChar", 255, 0, 0, false, "", null, typeof(System.String), 10);
			this.AddElementFieldMapping("GameSessionEntity", "DeliverypointNumber", "DeliverypointNumber", false, "VarChar", 50, 0, 0, false, "", null, typeof(System.String), 11);
			this.AddElementFieldMapping("GameSessionEntity", "DeliverypointName", "DeliverypointName", false, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 12);
			this.AddElementFieldMapping("GameSessionEntity", "DeliverypointId", "DeliverypointId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 13);
		}

		/// <summary>Inits GameSessionReportEntity's mappings</summary>
		private void InitGameSessionReportEntityMappings()
		{
			this.AddElementMapping("GameSessionReportEntity", @"Obymobi", @"dbo", "GameSessionReport", 6, 0);
			this.AddElementFieldMapping("GameSessionReportEntity", "GameSessionReportId", "GameSessionReportId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("GameSessionReportEntity", "CompanyId", "CompanyId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("GameSessionReportEntity", "Recipients", "Recipients", true, "VarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 2);
			this.AddElementFieldMapping("GameSessionReportEntity", "Sent", "Sent", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 3);
			this.AddElementFieldMapping("GameSessionReportEntity", "CreatedUTC", "CreatedUTC", false, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 4);
			this.AddElementFieldMapping("GameSessionReportEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 5);
		}

		/// <summary>Inits GameSessionReportConfigurationEntity's mappings</summary>
		private void InitGameSessionReportConfigurationEntityMappings()
		{
			this.AddElementMapping("GameSessionReportConfigurationEntity", @"Obymobi", @"dbo", "GameSessionReportConfiguration", 11, 0);
			this.AddElementFieldMapping("GameSessionReportConfigurationEntity", "GameSessionReportConfigurationId", "GameSessionReportConfigurationId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("GameSessionReportConfigurationEntity", "CompanyId", "CompanyId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("GameSessionReportConfigurationEntity", "RecurrenceType", "RecurrenceType", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("GameSessionReportConfigurationEntity", "SendTime", "SendTime", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 3);
			this.AddElementFieldMapping("GameSessionReportConfigurationEntity", "Recipients", "Recipients", true, "VarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 4);
			this.AddElementFieldMapping("GameSessionReportConfigurationEntity", "CreatedUTC", "CreatedUTC", false, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 5);
			this.AddElementFieldMapping("GameSessionReportConfigurationEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 6);
			this.AddElementFieldMapping("GameSessionReportConfigurationEntity", "Name", "Name", false, "VarChar", 255, 0, 0, false, "", null, typeof(System.String), 7);
			this.AddElementFieldMapping("GameSessionReportConfigurationEntity", "Active", "Active", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 8);
			this.AddElementFieldMapping("GameSessionReportConfigurationEntity", "LastReportSentUTC", "LastReportSentUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 9);
			this.AddElementFieldMapping("GameSessionReportConfigurationEntity", "DailyResetTime", "DailyResetTime", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 10);
		}

		/// <summary>Inits GameSessionReportItemEntity's mappings</summary>
		private void InitGameSessionReportItemEntityMappings()
		{
			this.AddElementMapping("GameSessionReportItemEntity", @"Obymobi", @"dbo", "GameSessionReportItem", 11, 0);
			this.AddElementFieldMapping("GameSessionReportItemEntity", "GameSessionReportItemId", "GameSessionReportItemId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("GameSessionReportItemEntity", "GameSessionReportId", "GameSessionReportId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("GameSessionReportItemEntity", "GameSessionId", "Length", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("GameSessionReportItemEntity", "CreatedUTC", "CreatedUTC", false, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 3);
			this.AddElementFieldMapping("GameSessionReportItemEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 4);
			this.AddElementFieldMapping("GameSessionReportItemEntity", "ParentCompanyId", "ParentCompanyId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 5);
			this.AddElementFieldMapping("GameSessionReportItemEntity", "DeliverypointName", "DeliverypointName", false, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 6);
			this.AddElementFieldMapping("GameSessionReportItemEntity", "GameName", "GameName", false, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 7);
			this.AddElementFieldMapping("GameSessionReportItemEntity", "StartUTC", "StartUTC", false, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 8);
			this.AddElementFieldMapping("GameSessionReportItemEntity", "EndUTC", "EndUTC", false, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 9);
			this.AddElementFieldMapping("GameSessionReportItemEntity", "Length", "Length", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 10);
		}

		/// <summary>Inits GenericalterationEntity's mappings</summary>
		private void InitGenericalterationEntityMappings()
		{
			this.AddElementMapping("GenericalterationEntity", @"Obymobi", @"dbo", "Genericalteration", 20, 0);
			this.AddElementFieldMapping("GenericalterationEntity", "GenericalterationId", "GenericalterationId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("GenericalterationEntity", "Name", "Name", false, "NVarChar", 100, 0, 0, false, "", null, typeof(System.String), 1);
			this.AddElementFieldMapping("GenericalterationEntity", "ExternalId", "ExternalId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("GenericalterationEntity", "GenericalterationType", "GenericalterationType", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("GenericalterationEntity", "Type", "Type", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("GenericalterationEntity", "MinOptions", "MinOptions", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 5);
			this.AddElementFieldMapping("GenericalterationEntity", "MaxOptions", "MaxOptions", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 6);
			this.AddElementFieldMapping("GenericalterationEntity", "AvailableOnOtoucho", "AvailableOnOtoucho", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 7);
			this.AddElementFieldMapping("GenericalterationEntity", "AvailableOnObymobi", "AvailableOnObymobi", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 8);
			this.AddElementFieldMapping("GenericalterationEntity", "StartTime", "StartTime", true, "DateTime", 0, 0, 0, false, "", null, typeof(System.DateTime), 9);
			this.AddElementFieldMapping("GenericalterationEntity", "EndTime", "EndTime", true, "DateTime", 0, 0, 0, false, "", null, typeof(System.DateTime), 10);
			this.AddElementFieldMapping("GenericalterationEntity", "MinLeadMinutes", "MinLeadMinutes", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 11);
			this.AddElementFieldMapping("GenericalterationEntity", "MaxLeadHours", "MaxLeadHours", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 12);
			this.AddElementFieldMapping("GenericalterationEntity", "IntervalMinutes", "IntervalMinutes", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 13);
			this.AddElementFieldMapping("GenericalterationEntity", "ShowDatePicker", "ShowDatePicker", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 14);
			this.AddElementFieldMapping("GenericalterationEntity", "CreatedBy", "CreatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 15);
			this.AddElementFieldMapping("GenericalterationEntity", "UpdatedBy", "UpdatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 16);
			this.AddElementFieldMapping("GenericalterationEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 17);
			this.AddElementFieldMapping("GenericalterationEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 18);
			this.AddElementFieldMapping("GenericalterationEntity", "BrandId", "BrandId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 19);
		}

		/// <summary>Inits GenericalterationitemEntity's mappings</summary>
		private void InitGenericalterationitemEntityMappings()
		{
			this.AddElementMapping("GenericalterationitemEntity", @"Obymobi", @"dbo", "Genericalterationitem", 9, 0);
			this.AddElementFieldMapping("GenericalterationitemEntity", "GenericalterationitemId", "GenericalterationitemId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("GenericalterationitemEntity", "GenericalterationId", "GenericalterationId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("GenericalterationitemEntity", "GenericalterationoptionId", "GenericalterationoptionId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("GenericalterationitemEntity", "SelectedOnDefault", "SelectedOnDefault", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 3);
			this.AddElementFieldMapping("GenericalterationitemEntity", "SortOrder", "SortOrder", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("GenericalterationitemEntity", "CreatedBy", "CreatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 5);
			this.AddElementFieldMapping("GenericalterationitemEntity", "UpdatedBy", "UpdatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 6);
			this.AddElementFieldMapping("GenericalterationitemEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 7);
			this.AddElementFieldMapping("GenericalterationitemEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 8);
		}

		/// <summary>Inits GenericalterationoptionEntity's mappings</summary>
		private void InitGenericalterationoptionEntityMappings()
		{
			this.AddElementMapping("GenericalterationoptionEntity", @"Obymobi", @"dbo", "Genericalterationoption", 10, 0);
			this.AddElementFieldMapping("GenericalterationoptionEntity", "GenericalterationoptionId", "GenericalterationoptionId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("GenericalterationoptionEntity", "Name", "Name", false, "NVarChar", 100, 0, 0, false, "", null, typeof(System.String), 1);
			this.AddElementFieldMapping("GenericalterationoptionEntity", "GenericalterationoptionType", "GenericalterationoptionType", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("GenericalterationoptionEntity", "PriceIn", "PriceIn", false, "Money", 0, 19, 4, false, "", null, typeof(System.Decimal), 3);
			this.AddElementFieldMapping("GenericalterationoptionEntity", "PriceAddition", "PriceAddition", true, "Money", 0, 19, 4, false, "", null, typeof(System.Decimal), 4);
			this.AddElementFieldMapping("GenericalterationoptionEntity", "Description", "Description", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 5);
			this.AddElementFieldMapping("GenericalterationoptionEntity", "CreatedBy", "CreatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 6);
			this.AddElementFieldMapping("GenericalterationoptionEntity", "UpdatedBy", "UpdatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 7);
			this.AddElementFieldMapping("GenericalterationoptionEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 8);
			this.AddElementFieldMapping("GenericalterationoptionEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 9);
		}

		/// <summary>Inits GenericcategoryEntity's mappings</summary>
		private void InitGenericcategoryEntityMappings()
		{
			this.AddElementMapping("GenericcategoryEntity", @"Obymobi", @"dbo", "Genericcategory", 13, 0);
			this.AddElementFieldMapping("GenericcategoryEntity", "GenericcategoryId", "GenericcategoryId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("GenericcategoryEntity", "ParentGenericcategoryId", "ParentGenericcategoryId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("GenericcategoryEntity", "Name", "Name", true, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 2);
			this.AddElementFieldMapping("GenericcategoryEntity", "CreatedBy", "CreatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("GenericcategoryEntity", "UpdatedBy", "UpdatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("GenericcategoryEntity", "ExternalId", "ExternalId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 5);
			this.AddElementFieldMapping("GenericcategoryEntity", "GenericCategoryType", "GenericcategoryType", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 6);
			this.AddElementFieldMapping("GenericcategoryEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 7);
			this.AddElementFieldMapping("GenericcategoryEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 8);
			this.AddElementFieldMapping("GenericcategoryEntity", "BrandId", "BrandId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 9);
			this.AddElementFieldMapping("GenericcategoryEntity", "SortOrder", "SortOrder", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 10);
			this.AddElementFieldMapping("GenericcategoryEntity", "VisibilityType", "VisibilityType", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 11);
			this.AddElementFieldMapping("GenericcategoryEntity", "Visible", "Visible", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 12);
		}

		/// <summary>Inits GenericcategoryLanguageEntity's mappings</summary>
		private void InitGenericcategoryLanguageEntityMappings()
		{
			this.AddElementMapping("GenericcategoryLanguageEntity", @"Obymobi", @"dbo", "GenericcategoryLanguage", 8, 0);
			this.AddElementFieldMapping("GenericcategoryLanguageEntity", "GenericcategoryLanguageId", "GenericcategoryLanguageId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("GenericcategoryLanguageEntity", "GenericcategoryId", "GenericcategoryId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("GenericcategoryLanguageEntity", "LanguageId", "LanguageId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("GenericcategoryLanguageEntity", "Name", "Name", true, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 3);
			this.AddElementFieldMapping("GenericcategoryLanguageEntity", "CreatedBy", "CreatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("GenericcategoryLanguageEntity", "UpdatedBy", "UpdatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 5);
			this.AddElementFieldMapping("GenericcategoryLanguageEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 6);
			this.AddElementFieldMapping("GenericcategoryLanguageEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 7);
		}

		/// <summary>Inits GenericproductEntity's mappings</summary>
		private void InitGenericproductEntityMappings()
		{
			this.AddElementMapping("GenericproductEntity", @"Obymobi", @"dbo", "Genericproduct", 25, 0);
			this.AddElementFieldMapping("GenericproductEntity", "GenericproductId", "GenericproductId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("GenericproductEntity", "SupplierId", "SupplierId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("GenericproductEntity", "GenericcategoryId", "GenericcategoryId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("GenericproductEntity", "Name", "Name", false, "NVarChar", 100, 0, 0, false, "", null, typeof(System.String), 3);
			this.AddElementFieldMapping("GenericproductEntity", "Code", "Code", true, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 4);
			this.AddElementFieldMapping("GenericproductEntity", "Description", "Description", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 5);
			this.AddElementFieldMapping("GenericproductEntity", "TextColor", "TextColor", true, "NVarChar", 15, 0, 0, false, "", null, typeof(System.String), 6);
			this.AddElementFieldMapping("GenericproductEntity", "BackgroundColor", "BackgroundColor", true, "NVarChar", 15, 0, 0, false, "", null, typeof(System.String), 7);
			this.AddElementFieldMapping("GenericproductEntity", "VattariffId", "VattariffId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 8);
			this.AddElementFieldMapping("GenericproductEntity", "CreatedBy", "CreatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 9);
			this.AddElementFieldMapping("GenericproductEntity", "UpdatedBy", "UpdatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 10);
			this.AddElementFieldMapping("GenericproductEntity", "ExternalId", "ExternalId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 11);
			this.AddElementFieldMapping("GenericproductEntity", "GenericproductType", "GenericproductType", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 12);
			this.AddElementFieldMapping("GenericproductEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 13);
			this.AddElementFieldMapping("GenericproductEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 14);
			this.AddElementFieldMapping("GenericproductEntity", "BrandId", "BrandId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 15);
			this.AddElementFieldMapping("GenericproductEntity", "SortOrder", "SortOrder", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 16);
			this.AddElementFieldMapping("GenericproductEntity", "VisibilityType", "VisibilityType", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 17);
			this.AddElementFieldMapping("GenericproductEntity", "Visible", "Visible", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 18);
			this.AddElementFieldMapping("GenericproductEntity", "SubType", "SubType", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 19);
			this.AddElementFieldMapping("GenericproductEntity", "PriceIn", "PriceIn", true, "Money", 0, 19, 4, false, "", null, typeof(System.Decimal), 20);
			this.AddElementFieldMapping("GenericproductEntity", "HidePrice", "HidePrice", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 21);
			this.AddElementFieldMapping("GenericproductEntity", "WebTypeSmartphoneUrl", "WebTypeSmartphoneUrl", true, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 22);
			this.AddElementFieldMapping("GenericproductEntity", "WebTypeTabletUrl", "WebTypeTabletUrl", true, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 23);
			this.AddElementFieldMapping("GenericproductEntity", "ViewLayoutType", "ViewLayoutType", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 24);
		}

		/// <summary>Inits GenericproductGenericalterationEntity's mappings</summary>
		private void InitGenericproductGenericalterationEntityMappings()
		{
			this.AddElementMapping("GenericproductGenericalterationEntity", @"Obymobi", @"dbo", "GenericproductGenericalteration", 8, 0);
			this.AddElementFieldMapping("GenericproductGenericalterationEntity", "GenericproductGenericalterationId", "GenericproductGenericalterationId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("GenericproductGenericalterationEntity", "GenericproductId", "GenericproductId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("GenericproductGenericalterationEntity", "GenericalterationId", "GenericalterationId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("GenericproductGenericalterationEntity", "CreatedBy", "CreatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("GenericproductGenericalterationEntity", "UpdatedBy", "UpdatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("GenericproductGenericalterationEntity", "SortOrder", "SortOrder", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 5);
			this.AddElementFieldMapping("GenericproductGenericalterationEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 6);
			this.AddElementFieldMapping("GenericproductGenericalterationEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 7);
		}

		/// <summary>Inits GenericproductLanguageEntity's mappings</summary>
		private void InitGenericproductLanguageEntityMappings()
		{
			this.AddElementMapping("GenericproductLanguageEntity", @"Obymobi", @"dbo", "GenericproductLanguage", 9, 0);
			this.AddElementFieldMapping("GenericproductLanguageEntity", "GenericproductLanguageId", "GenericproductLanguageId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("GenericproductLanguageEntity", "GenericproductId", "GenericproductId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("GenericproductLanguageEntity", "LanguageId", "LanguageId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("GenericproductLanguageEntity", "Name", "Name", true, "NVarChar", 100, 0, 0, false, "", null, typeof(System.String), 3);
			this.AddElementFieldMapping("GenericproductLanguageEntity", "Description", "Description", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 4);
			this.AddElementFieldMapping("GenericproductLanguageEntity", "CreatedBy", "CreatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 5);
			this.AddElementFieldMapping("GenericproductLanguageEntity", "UpdatedBy", "UpdatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 6);
			this.AddElementFieldMapping("GenericproductLanguageEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 7);
			this.AddElementFieldMapping("GenericproductLanguageEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 8);
		}

		/// <summary>Inits GuestInformationEntity's mappings</summary>
		private void InitGuestInformationEntityMappings()
		{
			this.AddElementMapping("GuestInformationEntity", @"Obymobi", @"dbo", "GuestInformation", 19, 0);
			this.AddElementFieldMapping("GuestInformationEntity", "GuestInformationId", "GuestInformationId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("GuestInformationEntity", "CompanyId", "CompanyId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("GuestInformationEntity", "ExternalGuestId", "ExternalGuestId", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 2);
			this.AddElementFieldMapping("GuestInformationEntity", "DeliverypointNumber", "DeliverypointNumber", false, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 3);
			this.AddElementFieldMapping("GuestInformationEntity", "ReservationNumber", "ReservationNumber", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 4);
			this.AddElementFieldMapping("GuestInformationEntity", "Vip", "Vip", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 5);
			this.AddElementFieldMapping("GuestInformationEntity", "Occupied", "Occupied", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 6);
			this.AddElementFieldMapping("GuestInformationEntity", "GroupName", "GroupName", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 7);
			this.AddElementFieldMapping("GuestInformationEntity", "LanguageCode", "LanguageCode", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 8);
			this.AddElementFieldMapping("GuestInformationEntity", "DoNotDisturb", "DoNotDisturb", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 9);
			this.AddElementFieldMapping("GuestInformationEntity", "MessageWaiting", "MessageWaiting", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 10);
			this.AddElementFieldMapping("GuestInformationEntity", "ViewBill", "ViewBill", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 11);
			this.AddElementFieldMapping("GuestInformationEntity", "ExpressCheckOut", "ExpressCheckOut", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 12);
			this.AddElementFieldMapping("GuestInformationEntity", "CheckInUTC", "CheckIn", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 13);
			this.AddElementFieldMapping("GuestInformationEntity", "CheckOutUTC", "CheckOut", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 14);
			this.AddElementFieldMapping("GuestInformationEntity", "CreatedBy", "CreatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 15);
			this.AddElementFieldMapping("GuestInformationEntity", "UpdatedBy", "UpdatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 16);
			this.AddElementFieldMapping("GuestInformationEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 17);
			this.AddElementFieldMapping("GuestInformationEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 18);
		}

		/// <summary>Inits IcrtouchprintermappingEntity's mappings</summary>
		private void InitIcrtouchprintermappingEntityMappings()
		{
			this.AddElementMapping("IcrtouchprintermappingEntity", @"Obymobi", @"dbo", "Icrtouchprintermapping", 8, 0);
			this.AddElementFieldMapping("IcrtouchprintermappingEntity", "IcrtouchprintermappingId", "IcrtouchprintermappingId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("IcrtouchprintermappingEntity", "TerminalId", "TerminalId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("IcrtouchprintermappingEntity", "Name", "Name", true, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 2);
			this.AddElementFieldMapping("IcrtouchprintermappingEntity", "CreatedBy", "CreatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("IcrtouchprintermappingEntity", "UpdatedBy", "UpdatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("IcrtouchprintermappingEntity", "ParentCompanyId", "ParentCompanyId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 5);
			this.AddElementFieldMapping("IcrtouchprintermappingEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 6);
			this.AddElementFieldMapping("IcrtouchprintermappingEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 7);
		}

		/// <summary>Inits IcrtouchprintermappingDeliverypointEntity's mappings</summary>
		private void InitIcrtouchprintermappingDeliverypointEntityMappings()
		{
			this.AddElementMapping("IcrtouchprintermappingDeliverypointEntity", @"Obymobi", @"dbo", "IcrtouchprintermappingDeliverypoint", 18, 0);
			this.AddElementFieldMapping("IcrtouchprintermappingDeliverypointEntity", "IcrtouchprintermappingDeliverypointId", "IcrtouchprintermappingDeliverypointId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("IcrtouchprintermappingDeliverypointEntity", "IcrtouchprintermappingId", "IcrtouchprintermappingId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("IcrtouchprintermappingDeliverypointEntity", "DeliverypointId", "DeliverypointId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("IcrtouchprintermappingDeliverypointEntity", "Printer1", "Printer1", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("IcrtouchprintermappingDeliverypointEntity", "Printer2", "Printer2", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("IcrtouchprintermappingDeliverypointEntity", "Printer3", "Printer3", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 5);
			this.AddElementFieldMapping("IcrtouchprintermappingDeliverypointEntity", "Printer4", "Printer4", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 6);
			this.AddElementFieldMapping("IcrtouchprintermappingDeliverypointEntity", "Printer5", "Printer5", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 7);
			this.AddElementFieldMapping("IcrtouchprintermappingDeliverypointEntity", "Printer6", "Printer6", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 8);
			this.AddElementFieldMapping("IcrtouchprintermappingDeliverypointEntity", "Printer7", "Printer7", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 9);
			this.AddElementFieldMapping("IcrtouchprintermappingDeliverypointEntity", "Printer8", "Printer8", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 10);
			this.AddElementFieldMapping("IcrtouchprintermappingDeliverypointEntity", "Printer9", "Printer9", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 11);
			this.AddElementFieldMapping("IcrtouchprintermappingDeliverypointEntity", "Printer10", "Printer10", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 12);
			this.AddElementFieldMapping("IcrtouchprintermappingDeliverypointEntity", "CreatedBy", "CreatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 13);
			this.AddElementFieldMapping("IcrtouchprintermappingDeliverypointEntity", "UpdatedBy", "UpdatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 14);
			this.AddElementFieldMapping("IcrtouchprintermappingDeliverypointEntity", "ParentCompanyId", "ParentCompanyId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 15);
			this.AddElementFieldMapping("IcrtouchprintermappingDeliverypointEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 16);
			this.AddElementFieldMapping("IcrtouchprintermappingDeliverypointEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 17);
		}

		/// <summary>Inits IncomingSmsEntity's mappings</summary>
		private void InitIncomingSmsEntityMappings()
		{
			this.AddElementMapping("IncomingSmsEntity", @"Obymobi", @"dbo", "IncomingSms", 9, 0);
			this.AddElementFieldMapping("IncomingSmsEntity", "IncomingSmsId", "IncomingSmsId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("IncomingSmsEntity", "Number", "Number", true, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 1);
			this.AddElementFieldMapping("IncomingSmsEntity", "Operator", "Operator", true, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 2);
			this.AddElementFieldMapping("IncomingSmsEntity", "Message", "Message", true, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 3);
			this.AddElementFieldMapping("IncomingSmsEntity", "SmsType", "SmsType", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("IncomingSmsEntity", "CreatedBy", "CreatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 5);
			this.AddElementFieldMapping("IncomingSmsEntity", "UpdatedBy", "UpdatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 6);
			this.AddElementFieldMapping("IncomingSmsEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 7);
			this.AddElementFieldMapping("IncomingSmsEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 8);
		}

		/// <summary>Inits InfraredCommandEntity's mappings</summary>
		private void InitInfraredCommandEntityMappings()
		{
			this.AddElementMapping("InfraredCommandEntity", @"Obymobi", @"dbo", "InfraredCommand", 9, 0);
			this.AddElementFieldMapping("InfraredCommandEntity", "InfraredCommandId", "InfraredCommandId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("InfraredCommandEntity", "InfraredConfigurationId", "InfraredConfigurationId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("InfraredCommandEntity", "Type", "Type", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("InfraredCommandEntity", "Hex", "Hex", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 3);
			this.AddElementFieldMapping("InfraredCommandEntity", "UpdatedBy", "UpdatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("InfraredCommandEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 5);
			this.AddElementFieldMapping("InfraredCommandEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 6);
			this.AddElementFieldMapping("InfraredCommandEntity", "CreatedBy", "CreatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 7);
			this.AddElementFieldMapping("InfraredCommandEntity", "Name", "Name", true, "VarChar", 255, 0, 0, false, "", null, typeof(System.String), 8);
		}

		/// <summary>Inits InfraredConfigurationEntity's mappings</summary>
		private void InitInfraredConfigurationEntityMappings()
		{
			this.AddElementMapping("InfraredConfigurationEntity", @"Obymobi", @"dbo", "InfraredConfiguration", 7, 0);
			this.AddElementFieldMapping("InfraredConfigurationEntity", "InfraredConfigurationId", "InfraredConfigurationId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("InfraredConfigurationEntity", "Name", "Name", false, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 1);
			this.AddElementFieldMapping("InfraredConfigurationEntity", "UpdatedBy", "UpdatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("InfraredConfigurationEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 3);
			this.AddElementFieldMapping("InfraredConfigurationEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 4);
			this.AddElementFieldMapping("InfraredConfigurationEntity", "CreatedBy", "CreatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 5);
			this.AddElementFieldMapping("InfraredConfigurationEntity", "MillisecondsBetweenCommands", "MillisecondsBetweenCommands", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 6);
		}

		/// <summary>Inits LanguageEntity's mappings</summary>
		private void InitLanguageEntityMappings()
		{
			this.AddElementMapping("LanguageEntity", @"Obymobi", @"dbo", "Language", 9, 0);
			this.AddElementFieldMapping("LanguageEntity", "LanguageId", "LanguageId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("LanguageEntity", "Code", "Code", true, "NVarChar", 2, 0, 0, false, "", null, typeof(System.String), 1);
			this.AddElementFieldMapping("LanguageEntity", "Name", "Name", true, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 2);
			this.AddElementFieldMapping("LanguageEntity", "LocalizedName", "LocalizedName", true, "NVarChar", 100, 0, 0, false, "", null, typeof(System.String), 3);
			this.AddElementFieldMapping("LanguageEntity", "CreatedBy", "CreatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("LanguageEntity", "UpdatedBy", "UpdatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 5);
			this.AddElementFieldMapping("LanguageEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 6);
			this.AddElementFieldMapping("LanguageEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 7);
			this.AddElementFieldMapping("LanguageEntity", "CodeAlpha3", "CodeAlpha3", true, "NVarChar", 3, 0, 0, false, "", null, typeof(System.String), 8);
		}

		/// <summary>Inits LicensedModuleEntity's mappings</summary>
		private void InitLicensedModuleEntityMappings()
		{
			this.AddElementMapping("LicensedModuleEntity", @"Obymobi", @"dbo", "LicensedModule", 9, 0);
			this.AddElementFieldMapping("LicensedModuleEntity", "LicensedModuleId", "LicensedModuleId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("LicensedModuleEntity", "ModuleNameSystem", "ModuleNameSystem", true, "VarChar", 50, 0, 0, false, "", null, typeof(System.String), 1);
			this.AddElementFieldMapping("LicensedModuleEntity", "IsLicensed", "IsLicensed", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 2);
			this.AddElementFieldMapping("LicensedModuleEntity", "Deleted", "Deleted", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 3);
			this.AddElementFieldMapping("LicensedModuleEntity", "Archived", "Archived", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 4);
			this.AddElementFieldMapping("LicensedModuleEntity", "CreatedBy", "CreatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 5);
			this.AddElementFieldMapping("LicensedModuleEntity", "UpdatedBy", "UpdatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 6);
			this.AddElementFieldMapping("LicensedModuleEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 7);
			this.AddElementFieldMapping("LicensedModuleEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 8);
		}

		/// <summary>Inits LicensedUIElementEntity's mappings</summary>
		private void InitLicensedUIElementEntityMappings()
		{
			this.AddElementMapping("LicensedUIElementEntity", @"Obymobi", @"dbo", "LicensedUIElement", 7, 0);
			this.AddElementFieldMapping("LicensedUIElementEntity", "LicensedUIElementId", "LicensedUIElementId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("LicensedUIElementEntity", "UiElementTypeNameFull", "UiElementTypeNameFull", false, "VarChar", 255, 0, 0, false, "", null, typeof(System.String), 1);
			this.AddElementFieldMapping("LicensedUIElementEntity", "IsLicensed", "IsLicensed", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 2);
			this.AddElementFieldMapping("LicensedUIElementEntity", "CreatedBy", "CreatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("LicensedUIElementEntity", "UpdatedBy", "UpdatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("LicensedUIElementEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 5);
			this.AddElementFieldMapping("LicensedUIElementEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 6);
		}

		/// <summary>Inits LicensedUIElementSubPanelEntity's mappings</summary>
		private void InitLicensedUIElementSubPanelEntityMappings()
		{
			this.AddElementMapping("LicensedUIElementSubPanelEntity", @"Obymobi", @"dbo", "LicensedUIElementSubPanel", 7, 0);
			this.AddElementFieldMapping("LicensedUIElementSubPanelEntity", "LicensedUIElementSubPanelId", "LicensedUIElementSubPanelId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("LicensedUIElementSubPanelEntity", "UiElementSubPanelTypeNameFull", "UiElementSubPanelTypeNameFull", false, "VarChar", 255, 0, 0, false, "", null, typeof(System.String), 1);
			this.AddElementFieldMapping("LicensedUIElementSubPanelEntity", "IsLicensed", "IsLicensed", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 2);
			this.AddElementFieldMapping("LicensedUIElementSubPanelEntity", "CreatedBy", "CreatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("LicensedUIElementSubPanelEntity", "UpdatedBy", "UpdatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("LicensedUIElementSubPanelEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 5);
			this.AddElementFieldMapping("LicensedUIElementSubPanelEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 6);
		}

		/// <summary>Inits MapEntity's mappings</summary>
		private void InitMapEntityMappings()
		{
			this.AddElementMapping("MapEntity", @"Obymobi", @"dbo", "Map", 13, 0);
			this.AddElementFieldMapping("MapEntity", "MapId", "MapId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("MapEntity", "Name", "Name", false, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 1);
			this.AddElementFieldMapping("MapEntity", "MyLocationEnabled", "MyLocationEnabled", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 2);
			this.AddElementFieldMapping("MapEntity", "ZoomControlsEnabled", "ZoomControlsEnabled", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 3);
			this.AddElementFieldMapping("MapEntity", "MapType", "MapType", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("MapEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 5);
			this.AddElementFieldMapping("MapEntity", "CreatedBy", "CreatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 6);
			this.AddElementFieldMapping("MapEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 7);
			this.AddElementFieldMapping("MapEntity", "UpdatedBy", "UpdatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 8);
			this.AddElementFieldMapping("MapEntity", "CompanyId", "CompanyId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 9);
			this.AddElementFieldMapping("MapEntity", "IndoorEnabled", "IndoorEnabled", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 10);
			this.AddElementFieldMapping("MapEntity", "ZoomLevel", "ZoomLevel", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 11);
			this.AddElementFieldMapping("MapEntity", "MapProvider", "MapProvider", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 12);
		}

		/// <summary>Inits MapPointOfInterestEntity's mappings</summary>
		private void InitMapPointOfInterestEntityMappings()
		{
			this.AddElementMapping("MapPointOfInterestEntity", @"Obymobi", @"dbo", "MapPointOfInterest", 7, 0);
			this.AddElementFieldMapping("MapPointOfInterestEntity", "MapPointOfInterestId", "MapPointOfInterestId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("MapPointOfInterestEntity", "MapId", "MapId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("MapPointOfInterestEntity", "PointOfInterestId", "PointOfInterestId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("MapPointOfInterestEntity", "CreatedBy", "CreatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("MapPointOfInterestEntity", "UpdatedBy", "UpdatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("MapPointOfInterestEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 5);
			this.AddElementFieldMapping("MapPointOfInterestEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 6);
		}

		/// <summary>Inits MediaEntity's mappings</summary>
		private void InitMediaEntityMappings()
		{
			this.AddElementMapping("MediaEntity", @"Obymobi", @"dbo", "Media", 64, 0);
			this.AddElementFieldMapping("MediaEntity", "MediaId", "MediaId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("MediaEntity", "MediaType", "MediaType", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("MediaEntity", "SortOrder", "SortOrder", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("MediaEntity", "DefaultItem", "DefaultItem", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 3);
			this.AddElementFieldMapping("MediaEntity", "Name", "Name", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 4);
			this.AddElementFieldMapping("MediaEntity", "FilePathRelativeToMediaPath", "FilePathRelativeToMediaPath", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 5);
			this.AddElementFieldMapping("MediaEntity", "Description", "Description", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 6);
			this.AddElementFieldMapping("MediaEntity", "MimeType", "MimeType", true, "NVarChar", 150, 0, 0, false, "", null, typeof(System.String), 7);
			this.AddElementFieldMapping("MediaEntity", "SizeKb", "SizeKb", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 8);
			this.AddElementFieldMapping("MediaEntity", "CompanyId", "CompanyId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 9);
			this.AddElementFieldMapping("MediaEntity", "ProductId", "ProductId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 10);
			this.AddElementFieldMapping("MediaEntity", "CategoryId", "CategoryId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 11);
			this.AddElementFieldMapping("MediaEntity", "AdvertisementId", "AdvertisementId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 12);
			this.AddElementFieldMapping("MediaEntity", "EntertainmentId", "EntertainmentId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 13);
			this.AddElementFieldMapping("MediaEntity", "AlterationoptionId", "AlterationoptionId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 14);
			this.AddElementFieldMapping("MediaEntity", "GenericproductId", "GenericproductId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 15);
			this.AddElementFieldMapping("MediaEntity", "GenericcategoryId", "GenericcategoryId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 16);
			this.AddElementFieldMapping("MediaEntity", "DeliverypointgroupId", "DeliverypointgroupId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 17);
			this.AddElementFieldMapping("MediaEntity", "SurveyId", "SurveyId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 18);
			this.AddElementFieldMapping("MediaEntity", "FromSupplier", "FromSupplier", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 19);
			this.AddElementFieldMapping("MediaEntity", "CreatedBy", "CreatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 20);
			this.AddElementFieldMapping("MediaEntity", "UpdatedBy", "UpdatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 21);
			this.AddElementFieldMapping("MediaEntity", "AlterationId", "AlterationId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 22);
			this.AddElementFieldMapping("MediaEntity", "SurveyPageId", "SurveyPageId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 23);
			this.AddElementFieldMapping("MediaEntity", "RoutestephandlerId", "RoutestephandlerId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 24);
			this.AddElementFieldMapping("MediaEntity", "PageElementId", "PageElementId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 25);
			this.AddElementFieldMapping("MediaEntity", "PageId", "PageId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 26);
			this.AddElementFieldMapping("MediaEntity", "PointOfInterestId", "PointOfInterestId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 27);
			this.AddElementFieldMapping("MediaEntity", "SiteId", "SiteId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 28);
			this.AddElementFieldMapping("MediaEntity", "ActionUrl", "ActionUrl", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 29);
			this.AddElementFieldMapping("MediaEntity", "ActionEntertainmentId", "ActionEntertainmentId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 30);
			this.AddElementFieldMapping("MediaEntity", "ActionProductId", "ActionProductId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 31);
			this.AddElementFieldMapping("MediaEntity", "ActionCategoryId", "ActionCategoryId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 32);
			this.AddElementFieldMapping("MediaEntity", "ActionEntertainmentcategoryId", "ActionEntertainmentcategoryId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 33);
			this.AddElementFieldMapping("MediaEntity", "JpgQuality", "JpgQuality", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 34);
			this.AddElementFieldMapping("MediaEntity", "SizeMode", "SizeMode", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 35);
			this.AddElementFieldMapping("MediaEntity", "ZoomLevel", "ZoomLevel", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 36);
			this.AddElementFieldMapping("MediaEntity", "MediaFileMd5", "MediaFileMd5", true, "NVarChar", 32, 0, 0, false, "", null, typeof(System.String), 37);
			this.AddElementFieldMapping("MediaEntity", "Extension", "Extension", true, "NVarChar", 25, 0, 0, false, "", null, typeof(System.String), 38);
			this.AddElementFieldMapping("MediaEntity", "AgnosticMediaId", "AgnosticMediaId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 39);
			this.AddElementFieldMapping("MediaEntity", "AttachmentId", "AttachmentId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 40);
			this.AddElementFieldMapping("MediaEntity", "ActionPageId", "ActionPageId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 41);
			this.AddElementFieldMapping("MediaEntity", "ActionSiteId", "ActionSiteId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 42);
			this.AddElementFieldMapping("MediaEntity", "PageTemplateElementId", "PageTemplateElementId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 43);
			this.AddElementFieldMapping("MediaEntity", "PageTemplateId", "PageTemplateId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 44);
			this.AddElementFieldMapping("MediaEntity", "UIWidgetId", "UIWidgetId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 45);
			this.AddElementFieldMapping("MediaEntity", "UIThemeId", "UIThemeId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 46);
			this.AddElementFieldMapping("MediaEntity", "RoomControlSectionId", "RoomControlSectionId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 47);
			this.AddElementFieldMapping("MediaEntity", "RoomControlSectionItemId", "RoomControlSectionItemId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 48);
			this.AddElementFieldMapping("MediaEntity", "StationId", "StationId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 49);
			this.AddElementFieldMapping("MediaEntity", "UIFooterItemId", "UIFooterItemId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 50);
			this.AddElementFieldMapping("MediaEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 51);
			this.AddElementFieldMapping("MediaEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 52);
			this.AddElementFieldMapping("MediaEntity", "RelatedCompanyId", "RelatedCompanyId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 53);
			this.AddElementFieldMapping("MediaEntity", "ClientConfigurationId", "ClientConfigurationId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 54);
			this.AddElementFieldMapping("MediaEntity", "LastDistributedVersionTicksAmazon", "LastDistributedVersionTicksAmazon", true, "BigInt", 0, 19, 0, false, "", null, typeof(System.Int64), 55);
			this.AddElementFieldMapping("MediaEntity", "LastDistributedVersionTicks", "LastDistributedVersionTicks", true, "BigInt", 0, 19, 0, false, "", null, typeof(System.Int64), 56);
			this.AddElementFieldMapping("MediaEntity", "SizeWidth", "SizeWidth", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 57);
			this.AddElementFieldMapping("MediaEntity", "SizeHeight", "SizeHeight", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 58);
			this.AddElementFieldMapping("MediaEntity", "ProductgroupId", "ProductgroupId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 59);
			this.AddElementFieldMapping("MediaEntity", "LandingPageId", "LandingPageId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 60);
			this.AddElementFieldMapping("MediaEntity", "CarouselItemId", "CarouselItemId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 61);
			this.AddElementFieldMapping("MediaEntity", "WidgetId", "WidgetId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 62);
			this.AddElementFieldMapping("MediaEntity", "ApplicationConfigurationId", "ApplicationConfigurationId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 63);
		}

		/// <summary>Inits MediaCultureEntity's mappings</summary>
		private void InitMediaCultureEntityMappings()
		{
			this.AddElementMapping("MediaCultureEntity", @"Obymobi", @"dbo", "MediaCulture", 8, 0);
			this.AddElementFieldMapping("MediaCultureEntity", "MediaCultureId", "MediaCultureId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("MediaCultureEntity", "ParentCompanyId", "ParentCompanyId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("MediaCultureEntity", "MediaId", "MediaId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("MediaCultureEntity", "CultureCode", "CultureCode", false, "NVarChar", 10, 0, 0, false, "", null, typeof(System.String), 3);
			this.AddElementFieldMapping("MediaCultureEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 4);
			this.AddElementFieldMapping("MediaCultureEntity", "CreatedBy", "CreatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 5);
			this.AddElementFieldMapping("MediaCultureEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 6);
			this.AddElementFieldMapping("MediaCultureEntity", "UpdatedBy", "UpdatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 7);
		}

		/// <summary>Inits MediaLanguageEntity's mappings</summary>
		private void InitMediaLanguageEntityMappings()
		{
			this.AddElementMapping("MediaLanguageEntity", @"Obymobi", @"dbo", "MediaLanguage", 7, 0);
			this.AddElementFieldMapping("MediaLanguageEntity", "MediaLanguageId", "MediaLanguageId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("MediaLanguageEntity", "MediaId", "MediaId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("MediaLanguageEntity", "LanguageId", "LanguageId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("MediaLanguageEntity", "CreatedBy", "CreatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("MediaLanguageEntity", "UpdatedBy", "UpdatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("MediaLanguageEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 5);
			this.AddElementFieldMapping("MediaLanguageEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 6);
		}

		/// <summary>Inits MediaProcessingTaskEntity's mappings</summary>
		private void InitMediaProcessingTaskEntityMappings()
		{
			this.AddElementMapping("MediaProcessingTaskEntity", @"Obymobi", @"dbo", "MediaProcessingTask", 16, 0);
			this.AddElementFieldMapping("MediaProcessingTaskEntity", "MediaProcessingTaskId", "MediaProcessingTaskId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("MediaProcessingTaskEntity", "MediaRatioTypeMediaId", "MediaRatioTypeMediaId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("MediaProcessingTaskEntity", "Action", "Action", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("MediaProcessingTaskEntity", "Errors", "Errors", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 3);
			this.AddElementFieldMapping("MediaProcessingTaskEntity", "CreatedBy", "CreatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("MediaProcessingTaskEntity", "UpdatedBy", "UpdatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 5);
			this.AddElementFieldMapping("MediaProcessingTaskEntity", "RelatedToCompanyId", "RelatedToCompanyId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 6);
			this.AddElementFieldMapping("MediaProcessingTaskEntity", "PathFormat", "PathFormat", true, "NVarChar", 260, 0, 0, false, "", null, typeof(System.String), 7);
			this.AddElementFieldMapping("MediaProcessingTaskEntity", "Attempts", "Attempts", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 8);
			this.AddElementFieldMapping("MediaProcessingTaskEntity", "MediaRatioTypeMediaIdNonRelationCopy", "MediaRatioTypeMediaIdNonRelationCopy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 9);
			this.AddElementFieldMapping("MediaProcessingTaskEntity", "Priority", "Priority", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 10);
			this.AddElementFieldMapping("MediaProcessingTaskEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 11);
			this.AddElementFieldMapping("MediaProcessingTaskEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 12);
			this.AddElementFieldMapping("MediaProcessingTaskEntity", "CompletedOnAmazonUTC", "CompletedOnAmazonUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 13);
			this.AddElementFieldMapping("MediaProcessingTaskEntity", "LastAttemptUTC", "LastAttemptUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 14);
			this.AddElementFieldMapping("MediaProcessingTaskEntity", "NextAttemptUTC", "NextAttemptUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 15);
		}

		/// <summary>Inits MediaRatioTypeMediaEntity's mappings</summary>
		private void InitMediaRatioTypeMediaEntityMappings()
		{
			this.AddElementMapping("MediaRatioTypeMediaEntity", @"Obymobi", @"dbo", "MediaRatioTypeMedia", 14, 0);
			this.AddElementFieldMapping("MediaRatioTypeMediaEntity", "MediaRatioTypeMediaId", "MediaRatioTypeMediaId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("MediaRatioTypeMediaEntity", "MediaId", "MediaId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("MediaRatioTypeMediaEntity", "Top", "Top", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("MediaRatioTypeMediaEntity", "Left", "Left", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("MediaRatioTypeMediaEntity", "Width", "Width", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("MediaRatioTypeMediaEntity", "Height", "Height", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 5);
			this.AddElementFieldMapping("MediaRatioTypeMediaEntity", "CreatedBy", "CreatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 6);
			this.AddElementFieldMapping("MediaRatioTypeMediaEntity", "UpdatedBy", "UpdatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 7);
			this.AddElementFieldMapping("MediaRatioTypeMediaEntity", "MediaType", "MediaType", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 8);
			this.AddElementFieldMapping("MediaRatioTypeMediaEntity", "LastDistributedVersionTicks", "LastDistributedVersionTicks", true, "BigInt", 0, 19, 0, false, "", null, typeof(System.Int64), 9);
			this.AddElementFieldMapping("MediaRatioTypeMediaEntity", "LastDistributedVersionTicksAmazon", "LastDistributedVersionTicksAmazon", true, "BigInt", 0, 19, 0, false, "", null, typeof(System.Int64), 10);
			this.AddElementFieldMapping("MediaRatioTypeMediaEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 11);
			this.AddElementFieldMapping("MediaRatioTypeMediaEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 12);
			this.AddElementFieldMapping("MediaRatioTypeMediaEntity", "ManuallyVerified", "ManuallyVerified", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 13);
		}

		/// <summary>Inits MediaRatioTypeMediaFileEntity's mappings</summary>
		private void InitMediaRatioTypeMediaFileEntityMappings()
		{
			this.AddElementMapping("MediaRatioTypeMediaFileEntity", @"Obymobi", @"dbo", "MediaRatioTypeMediaFile", 7, 0);
			this.AddElementFieldMapping("MediaRatioTypeMediaFileEntity", "MediaRatioTypeMediaFileId", "MediaRatioTypeMediaFileId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("MediaRatioTypeMediaFileEntity", "MediaRatioTypeMediaId", "MediaRatioTypeMediaId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("MediaRatioTypeMediaFileEntity", "CreatedBy", "CreatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("MediaRatioTypeMediaFileEntity", "UpdatedBy", "UpdatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("MediaRatioTypeMediaFileEntity", "FileMd5", "FileMd5", true, "NVarChar", 128, 0, 0, false, "", null, typeof(System.String), 4);
			this.AddElementFieldMapping("MediaRatioTypeMediaFileEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 5);
			this.AddElementFieldMapping("MediaRatioTypeMediaFileEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 6);
		}

		/// <summary>Inits MediaRelationshipEntity's mappings</summary>
		private void InitMediaRelationshipEntityMappings()
		{
			this.AddElementMapping("MediaRelationshipEntity", @"Obymobi", @"dbo", "MediaRelationship", 8, 0);
			this.AddElementFieldMapping("MediaRelationshipEntity", "MediaRelationshipId", "MediaRelationshipId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("MediaRelationshipEntity", "MediaId", "MediaId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("MediaRelationshipEntity", "ProductId", "ProductId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("MediaRelationshipEntity", "CreatedBy", "CreatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("MediaRelationshipEntity", "UpdatedBy", "UpdatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("MediaRelationshipEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 5);
			this.AddElementFieldMapping("MediaRelationshipEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 6);
			this.AddElementFieldMapping("MediaRelationshipEntity", "ParentCompanyId", "ParentCompanyId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 7);
		}

		/// <summary>Inits MenuEntity's mappings</summary>
		private void InitMenuEntityMappings()
		{
			this.AddElementMapping("MenuEntity", @"Obymobi", @"dbo", "Menu", 7, 0);
			this.AddElementFieldMapping("MenuEntity", "MenuId", "MenuId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("MenuEntity", "Name", "Name", true, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 1);
			this.AddElementFieldMapping("MenuEntity", "CreatedBy", "CreatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("MenuEntity", "UpdatedBy", "UpdatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("MenuEntity", "CompanyId", "CompanyId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("MenuEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 5);
			this.AddElementFieldMapping("MenuEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 6);
		}

		/// <summary>Inits MessageEntity's mappings</summary>
		private void InitMessageEntityMappings()
		{
			this.AddElementMapping("MessageEntity", @"Obymobi", @"dbo", "Message", 39, 0);
			this.AddElementFieldMapping("MessageEntity", "MessageId", "MessageId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("MessageEntity", "CustomerId", "CustomerId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("MessageEntity", "ClientId", "ClientId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("MessageEntity", "CompanyId", "CompanyId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("MessageEntity", "Title", "Title", true, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 4);
			this.AddElementFieldMapping("MessageEntity", "Message", "Message", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 5);
			this.AddElementFieldMapping("MessageEntity", "OrderId", "OrderId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 6);
			this.AddElementFieldMapping("MessageEntity", "CreatedBy", "CreatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 7);
			this.AddElementFieldMapping("MessageEntity", "UpdatedBy", "UpdatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 8);
			this.AddElementFieldMapping("MessageEntity", "Duration", "Duration", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 9);
			this.AddElementFieldMapping("MessageEntity", "Urgent", "Urgent", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 10);
			this.AddElementFieldMapping("MessageEntity", "MessageButtonType", "MessageButtonType", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 11);
			this.AddElementFieldMapping("MessageEntity", "EntertainmentId", "EntertainmentId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 12);
			this.AddElementFieldMapping("MessageEntity", "MediaId", "MediaId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 13);
			this.AddElementFieldMapping("MessageEntity", "CategoryId", "CategoryId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 14);
			this.AddElementFieldMapping("MessageEntity", "DeliverypointId", "DeliverypointId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 15);
			this.AddElementFieldMapping("MessageEntity", "ProductId", "ProductId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 16);
			this.AddElementFieldMapping("MessageEntity", "Url", "Url", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 17);
			this.AddElementFieldMapping("MessageEntity", "PageId", "PageId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 18);
			this.AddElementFieldMapping("MessageEntity", "SiteId", "SiteId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 19);
			this.AddElementFieldMapping("MessageEntity", "ProductCategoryId", "ProductCategoryId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 20);
			this.AddElementFieldMapping("MessageEntity", "Queued", "Queued", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 21);
			this.AddElementFieldMapping("MessageEntity", "NotifyOnYes", "NotifyOnYes", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 22);
			this.AddElementFieldMapping("MessageEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 23);
			this.AddElementFieldMapping("MessageEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 24);
			this.AddElementFieldMapping("MessageEntity", "MessageLayoutType", "MessageLayoutType", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 25);
			this.AddElementFieldMapping("MessageEntity", "RecipientType", "RecipientType", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 26);
			this.AddElementFieldMapping("MessageEntity", "RecipientsCount", "RecipientsCount", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 27);
			this.AddElementFieldMapping("MessageEntity", "RecipientsAsString", "RecipientsAsString", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 28);
			this.AddElementFieldMapping("MessageEntity", "RecipientsOkResultCount", "RecipientsOkResultCount", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 29);
			this.AddElementFieldMapping("MessageEntity", "RecipientsOkResultAsString", "RecipientsOkResultAsString", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 30);
			this.AddElementFieldMapping("MessageEntity", "RecipientsYesResultCount", "RecipientsYesResultCount", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 31);
			this.AddElementFieldMapping("MessageEntity", "RecipientsYesResultAsString", "RecipientsYesResultAsString", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 32);
			this.AddElementFieldMapping("MessageEntity", "RecipientsNoResultCount", "RecipientsNoResultCount", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 33);
			this.AddElementFieldMapping("MessageEntity", "RecipientsNoResultAsString", "RecipientsNoResultAsString", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 34);
			this.AddElementFieldMapping("MessageEntity", "RecipientsTimeOutResultCount", "RecipientsTimeOutResultCount", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 35);
			this.AddElementFieldMapping("MessageEntity", "RecipientsTimeOutResultAsString", "RecipientsTimeOutResultAsString", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 36);
			this.AddElementFieldMapping("MessageEntity", "RecipientsClearManuallyResultCount", "RecipientsClearManuallyResultCount", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 37);
			this.AddElementFieldMapping("MessageEntity", "RecipientsClearManuallyResultAsString", "RecipientsClearManuallyResultAsString", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 38);
		}

		/// <summary>Inits MessagegroupEntity's mappings</summary>
		private void InitMessagegroupEntityMappings()
		{
			this.AddElementMapping("MessagegroupEntity", @"Obymobi", @"dbo", "Messagegroup", 9, 0);
			this.AddElementFieldMapping("MessagegroupEntity", "MessagegroupId", "MessagegroupId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("MessagegroupEntity", "CompanyId", "CompanyId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("MessagegroupEntity", "Name", "Name", false, "NVarChar", 200, 0, 0, false, "", null, typeof(System.String), 2);
			this.AddElementFieldMapping("MessagegroupEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 3);
			this.AddElementFieldMapping("MessagegroupEntity", "CreatedBy", "CreatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("MessagegroupEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 5);
			this.AddElementFieldMapping("MessagegroupEntity", "UpdatedBy", "UpdatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 6);
			this.AddElementFieldMapping("MessagegroupEntity", "Type", "Type", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 7);
			this.AddElementFieldMapping("MessagegroupEntity", "GroupName", "GroupName", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 8);
		}

		/// <summary>Inits MessagegroupDeliverypointEntity's mappings</summary>
		private void InitMessagegroupDeliverypointEntityMappings()
		{
			this.AddElementMapping("MessagegroupDeliverypointEntity", @"Obymobi", @"dbo", "MessagegroupDeliverypoint", 8, 0);
			this.AddElementFieldMapping("MessagegroupDeliverypointEntity", "MessagegroupDeliverypointId", "MessagegroupDeliverypointId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("MessagegroupDeliverypointEntity", "MessagegroupId", "MessagegroupId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("MessagegroupDeliverypointEntity", "DeliverypointId", "DeliverypointId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("MessagegroupDeliverypointEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 3);
			this.AddElementFieldMapping("MessagegroupDeliverypointEntity", "CreatedBy", "CreatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("MessagegroupDeliverypointEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 5);
			this.AddElementFieldMapping("MessagegroupDeliverypointEntity", "UpdatedBy", "UpdatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 6);
			this.AddElementFieldMapping("MessagegroupDeliverypointEntity", "ParentCompanyId", "ParentCompanyId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 7);
		}

		/// <summary>Inits MessageRecipientEntity's mappings</summary>
		private void InitMessageRecipientEntityMappings()
		{
			this.AddElementMapping("MessageRecipientEntity", @"Obymobi", @"dbo", "MessageRecipient", 15, 0);
			this.AddElementFieldMapping("MessageRecipientEntity", "MessageRecipientId", "MessageRecipientId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("MessageRecipientEntity", "ParentCompanyId", "ParentCompanyId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("MessageRecipientEntity", "MessageId", "MessageId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("MessageRecipientEntity", "CustomerId", "CustomerId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("MessageRecipientEntity", "ClientId", "ClientId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("MessageRecipientEntity", "DeliverypointId", "DeliverypointId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 5);
			this.AddElementFieldMapping("MessageRecipientEntity", "CreatedBy", "CreatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 6);
			this.AddElementFieldMapping("MessageRecipientEntity", "UpdatedBy", "UpdatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 7);
			this.AddElementFieldMapping("MessageRecipientEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 8);
			this.AddElementFieldMapping("MessageRecipientEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 9);
			this.AddElementFieldMapping("MessageRecipientEntity", "Queued", "Queued", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 10);
			this.AddElementFieldMapping("MessageRecipientEntity", "SentUTC", "SentUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 11);
			this.AddElementFieldMapping("MessageRecipientEntity", "CategoryId", "CategoryId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 12);
			this.AddElementFieldMapping("MessageRecipientEntity", "EntertainmentId", "EntertainmentId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 13);
			this.AddElementFieldMapping("MessageRecipientEntity", "ProductId", "ProductId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 14);
		}

		/// <summary>Inits MessageTemplateEntity's mappings</summary>
		private void InitMessageTemplateEntityMappings()
		{
			this.AddElementMapping("MessageTemplateEntity", @"Obymobi", @"dbo", "MessageTemplate", 20, 0);
			this.AddElementFieldMapping("MessageTemplateEntity", "MessageTemplateId", "MessageTemplateId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("MessageTemplateEntity", "Title", "Title", true, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 1);
			this.AddElementFieldMapping("MessageTemplateEntity", "Message", "Message", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 2);
			this.AddElementFieldMapping("MessageTemplateEntity", "CompanyId", "CompanyId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("MessageTemplateEntity", "CreatedBy", "CreatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("MessageTemplateEntity", "UpdatedBy", "UpdatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 5);
			this.AddElementFieldMapping("MessageTemplateEntity", "Name", "Name", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 6);
			this.AddElementFieldMapping("MessageTemplateEntity", "Duration", "Duration", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 7);
			this.AddElementFieldMapping("MessageTemplateEntity", "EntertainmentId", "EntertainmentId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 8);
			this.AddElementFieldMapping("MessageTemplateEntity", "MediaId", "MediaId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 9);
			this.AddElementFieldMapping("MessageTemplateEntity", "CategoryId", "CategoryId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 10);
			this.AddElementFieldMapping("MessageTemplateEntity", "ProductId", "ProductId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 11);
			this.AddElementFieldMapping("MessageTemplateEntity", "NotifyOnYes", "NotifyOnYes", true, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 12);
			this.AddElementFieldMapping("MessageTemplateEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 13);
			this.AddElementFieldMapping("MessageTemplateEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 14);
			this.AddElementFieldMapping("MessageTemplateEntity", "Url", "Url", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 15);
			this.AddElementFieldMapping("MessageTemplateEntity", "PageId", "PageId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 16);
			this.AddElementFieldMapping("MessageTemplateEntity", "SiteId", "SiteId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 17);
			this.AddElementFieldMapping("MessageTemplateEntity", "ProductCategoryId", "ProductCategoryId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 18);
			this.AddElementFieldMapping("MessageTemplateEntity", "MessageLayoutType", "MessageLayoutType", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 19);
		}

		/// <summary>Inits MessageTemplateCategoryEntity's mappings</summary>
		private void InitMessageTemplateCategoryEntityMappings()
		{
			this.AddElementMapping("MessageTemplateCategoryEntity", @"Obymobi", @"dbo", "MessageTemplateCategory", 7, 0);
			this.AddElementFieldMapping("MessageTemplateCategoryEntity", "MessageTemplateCategoryId", "MessageTemplateCategoryId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("MessageTemplateCategoryEntity", "CompanyId", "CompanyId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("MessageTemplateCategoryEntity", "Name", "Name", false, "NVarChar", 100, 0, 0, false, "", null, typeof(System.String), 2);
			this.AddElementFieldMapping("MessageTemplateCategoryEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 3);
			this.AddElementFieldMapping("MessageTemplateCategoryEntity", "CreatedBy", "CreatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("MessageTemplateCategoryEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 5);
			this.AddElementFieldMapping("MessageTemplateCategoryEntity", "UpdatedBy", "UpdatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 6);
		}

		/// <summary>Inits MessageTemplateCategoryMessageTemplateEntity's mappings</summary>
		private void InitMessageTemplateCategoryMessageTemplateEntityMappings()
		{
			this.AddElementMapping("MessageTemplateCategoryMessageTemplateEntity", @"Obymobi", @"dbo", "MessageTemplateCategoryMessageTemplate", 7, 0);
			this.AddElementFieldMapping("MessageTemplateCategoryMessageTemplateEntity", "MessageTemplateCategoryMessageTemplateId", "MessageTemplateCategoryMessageTemplateId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("MessageTemplateCategoryMessageTemplateEntity", "MessageTemplateCategoryId", "MessageTemplateCategoryId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("MessageTemplateCategoryMessageTemplateEntity", "MessageTemplateId", "MessageTemplateId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("MessageTemplateCategoryMessageTemplateEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 3);
			this.AddElementFieldMapping("MessageTemplateCategoryMessageTemplateEntity", "CreatedBy", "CreatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("MessageTemplateCategoryMessageTemplateEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 5);
			this.AddElementFieldMapping("MessageTemplateCategoryMessageTemplateEntity", "UpdatedBy", "UpdatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 6);
		}

		/// <summary>Inits ModuleEntity's mappings</summary>
		private void InitModuleEntityMappings()
		{
			this.AddElementMapping("ModuleEntity", @"Obymobi", @"dbo", "Module", 14, 0);
			this.AddElementFieldMapping("ModuleEntity", "ModuleId", "ModuleId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("ModuleEntity", "NameSystem", "NameSystem", true, "VarChar", 50, 0, 0, false, "", null, typeof(System.String), 1);
			this.AddElementFieldMapping("ModuleEntity", "NameFull", "NameFull", true, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 2);
			this.AddElementFieldMapping("ModuleEntity", "NameShort", "NameShort", true, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 3);
			this.AddElementFieldMapping("ModuleEntity", "IconPath", "IconPath", true, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 4);
			this.AddElementFieldMapping("ModuleEntity", "DefaultUiElementTypeNameFull", "DefaultUiElementTypeNameFull", true, "VarChar", 255, 0, 0, false, "", null, typeof(System.String), 5);
			this.AddElementFieldMapping("ModuleEntity", "SortOrder", "SortOrder", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 6);
			this.AddElementFieldMapping("ModuleEntity", "LicensingFree", "LicensingFree", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 7);
			this.AddElementFieldMapping("ModuleEntity", "UserRightsFree", "UserRightsFree", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 8);
			this.AddElementFieldMapping("ModuleEntity", "ForLocalUseWhileSyncingIsUpdated", "ForLocalUseWhileSyncingIsUpdated", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 9);
			this.AddElementFieldMapping("ModuleEntity", "CreatedBy", "CreatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 10);
			this.AddElementFieldMapping("ModuleEntity", "UpdatedBy", "UpdatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 11);
			this.AddElementFieldMapping("ModuleEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 12);
			this.AddElementFieldMapping("ModuleEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 13);
		}

		/// <summary>Inits NetmessageEntity's mappings</summary>
		private void InitNetmessageEntityMappings()
		{
			this.AddElementMapping("NetmessageEntity", @"Obymobi", @"dbo", "Netmessage", 35, 0);
			this.AddElementFieldMapping("NetmessageEntity", "NetmessageId", "NetmessageId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("NetmessageEntity", "SenderCustomerId", "SenderCustomerId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("NetmessageEntity", "SenderClientId", "SenderClientId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("NetmessageEntity", "SenderCompanyId", "SenderCompanyId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("NetmessageEntity", "SenderDeliverypointId", "SenderDeliverypointId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("NetmessageEntity", "ReceiverCustomerId", "ReceiverCustomerId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 5);
			this.AddElementFieldMapping("NetmessageEntity", "ReceiverClientId", "ReceiverClientId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 6);
			this.AddElementFieldMapping("NetmessageEntity", "ReceiverCompanyId", "ReceiverCompanyId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 7);
			this.AddElementFieldMapping("NetmessageEntity", "ReceiverDeliverypointId", "ReceiverDeliverypointId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 8);
			this.AddElementFieldMapping("NetmessageEntity", "MessageType", "MessageType", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 9);
			this.AddElementFieldMapping("NetmessageEntity", "FieldValue1", "FieldValue1", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 10);
			this.AddElementFieldMapping("NetmessageEntity", "FieldValue2", "FieldValue2", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 11);
			this.AddElementFieldMapping("NetmessageEntity", "FieldValue3", "FieldValue3", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 12);
			this.AddElementFieldMapping("NetmessageEntity", "FieldValue4", "FieldValue4", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 13);
			this.AddElementFieldMapping("NetmessageEntity", "FieldValue5", "FieldValue5", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 14);
			this.AddElementFieldMapping("NetmessageEntity", "FieldValue6", "FieldValue6", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 15);
			this.AddElementFieldMapping("NetmessageEntity", "FieldValue7", "FieldValue7", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 16);
			this.AddElementFieldMapping("NetmessageEntity", "FieldValue8", "FieldValue8", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 17);
			this.AddElementFieldMapping("NetmessageEntity", "FieldValue9", "FieldValue9", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 18);
			this.AddElementFieldMapping("NetmessageEntity", "FieldValue10", "FieldValue10", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 19);
			this.AddElementFieldMapping("NetmessageEntity", "CreatedBy", "CreatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 20);
			this.AddElementFieldMapping("NetmessageEntity", "UpdatedBy", "UpdatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 21);
			this.AddElementFieldMapping("NetmessageEntity", "SenderTerminalId", "SenderTerminalId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 22);
			this.AddElementFieldMapping("NetmessageEntity", "ReceiverTerminalId", "ReceiverTerminalId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 23);
			this.AddElementFieldMapping("NetmessageEntity", "Guid", "Guid", true, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 24);
			this.AddElementFieldMapping("NetmessageEntity", "ReceiverDeliverypointgroupId", "ReceiverDeliverypointgroupId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 25);
			this.AddElementFieldMapping("NetmessageEntity", "Status", "Status", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 26);
			this.AddElementFieldMapping("NetmessageEntity", "MessageTypeText", "MessageTypeText", true, "NVarChar", 100, 0, 0, false, "", null, typeof(System.String), 27);
			this.AddElementFieldMapping("NetmessageEntity", "StatusText", "StatusText", true, "NVarChar", 100, 0, 0, false, "", null, typeof(System.String), 28);
			this.AddElementFieldMapping("NetmessageEntity", "ErrorMessage", "ErrorMessage", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 29);
			this.AddElementFieldMapping("NetmessageEntity", "MessageLog", "MessageLog", true, "Text", 2147483647, 0, 0, false, "", null, typeof(System.String), 30);
			this.AddElementFieldMapping("NetmessageEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 31);
			this.AddElementFieldMapping("NetmessageEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 32);
			this.AddElementFieldMapping("NetmessageEntity", "SenderIdentifier", "SenderIdentifier", true, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 33);
			this.AddElementFieldMapping("NetmessageEntity", "ReceiverIdentifier", "ReceiverIdentifier", true, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 34);
		}

		/// <summary>Inits OptInEntity's mappings</summary>
		private void InitOptInEntityMappings()
		{
			this.AddElementMapping("OptInEntity", @"Obymobi", @"dbo", "OptIn", 10, 0);
			this.AddElementFieldMapping("OptInEntity", "OptInId", "OptInId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("OptInEntity", "ParentCompanyId", "ParentCompanyId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("OptInEntity", "OutletId", "OutletId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("OptInEntity", "Email", "Email", true, "NVarChar", 250, 0, 0, false, "", null, typeof(System.String), 3);
			this.AddElementFieldMapping("OptInEntity", "PhoneNumber", "PhoneNumber", true, "NVarChar", 250, 0, 0, false, "", null, typeof(System.String), 4);
			this.AddElementFieldMapping("OptInEntity", "CreatedUTC", "CreatedUTC", false, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 5);
			this.AddElementFieldMapping("OptInEntity", "CreatedBy", "CreatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 6);
			this.AddElementFieldMapping("OptInEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 7);
			this.AddElementFieldMapping("OptInEntity", "UpdatedBy", "UpdatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 8);
			this.AddElementFieldMapping("OptInEntity", "CustomerName", "CustomerName", true, "NVarChar", 250, 0, 0, false, "", null, typeof(System.String), 9);
		}

		/// <summary>Inits OrderEntity's mappings</summary>
		private void InitOrderEntityMappings()
		{
			this.AddElementMapping("OrderEntity", @"Obymobi", @"dbo", "Order", 68, 0);
			this.AddElementFieldMapping("OrderEntity", "OrderId", "OrderId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("OrderEntity", "CustomerId", "CustomerId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("OrderEntity", "CustomerFirstname", "CustomerFirstname", true, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 2);
			this.AddElementFieldMapping("OrderEntity", "CustomerLastname", "CustomerLastname", true, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 3);
			this.AddElementFieldMapping("OrderEntity", "CustomerLastnamePrefix", "CustomerLastnamePrefix", true, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 4);
			this.AddElementFieldMapping("OrderEntity", "CustomerPhonenumber", "CustomerPhonenumber", true, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 5);
			this.AddElementFieldMapping("OrderEntity", "ClientId", "ClientId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 6);
			this.AddElementFieldMapping("OrderEntity", "ClientMacAddress", "ClientMacAddress", true, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 7);
			this.AddElementFieldMapping("OrderEntity", "CompanyId", "CompanyId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 8);
			this.AddElementFieldMapping("OrderEntity", "CompanyName", "CompanyName", true, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 9);
			this.AddElementFieldMapping("OrderEntity", "CurrencyId", "CurrencyId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 10);
			this.AddElementFieldMapping("OrderEntity", "DeliverypointId", "DeliverypointId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 11);
			this.AddElementFieldMapping("OrderEntity", "DeliverypointName", "DeliverypointName", true, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 12);
			this.AddElementFieldMapping("OrderEntity", "DeliverypointNumber", "DeliverypointNumber", true, "NVarChar", 20, 0, 0, false, "", null, typeof(System.String), 13);
			this.AddElementFieldMapping("OrderEntity", "ConfirmationCode", "ConfirmationCode", true, "NVarChar", 5, 0, 0, false, "", null, typeof(System.String), 14);
			this.AddElementFieldMapping("OrderEntity", "AgeVerificationType", "AgeVerificationType", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 15);
			this.AddElementFieldMapping("OrderEntity", "Notes", "Notes", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 16);
			this.AddElementFieldMapping("OrderEntity", "BenchmarkInformation", "BenchmarkInformation", true, "VarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 17);
			this.AddElementFieldMapping("OrderEntity", "Type", "Type", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 18);
			this.AddElementFieldMapping("OrderEntity", "TypeText", "TypeText", true, "NVarChar", 100, 0, 0, false, "", null, typeof(System.String), 19);
			this.AddElementFieldMapping("OrderEntity", "Status", "Status", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 20);
			this.AddElementFieldMapping("OrderEntity", "StatusText", "StatusText", false, "VarChar", 100, 0, 0, false, "", null, typeof(System.String), 21);
			this.AddElementFieldMapping("OrderEntity", "ErrorSentByEmail", "ErrorSentByEmail", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 22);
			this.AddElementFieldMapping("OrderEntity", "ErrorSentBySMS", "ErrorSentBySMS", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 23);
			this.AddElementFieldMapping("OrderEntity", "Guid", "Guid", true, "VarChar", 128, 0, 0, false, "", null, typeof(System.String), 24);
			this.AddElementFieldMapping("OrderEntity", "CreatedBy", "CreatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 25);
			this.AddElementFieldMapping("OrderEntity", "UpdatedBy", "UpdatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 26);
			this.AddElementFieldMapping("OrderEntity", "RoutingLog", "RoutingLog", true, "VarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 27);
			this.AddElementFieldMapping("OrderEntity", "ErrorCode", "ErrorCode", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 28);
			this.AddElementFieldMapping("OrderEntity", "ErrorText", "ErrorText", true, "VarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 29);
			this.AddElementFieldMapping("OrderEntity", "Printed", "Printed", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 30);
			this.AddElementFieldMapping("OrderEntity", "MasterOrderId", "MasterOrderId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 31);
			this.AddElementFieldMapping("OrderEntity", "MobileOrder", "MobileOrder", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 32);
			this.AddElementFieldMapping("OrderEntity", "Email", "Email", true, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 33);
			this.AddElementFieldMapping("OrderEntity", "HotSOSServiceOrderId", "HotSOSServiceOrderId", true, "VarChar", 50, 0, 0, false, "", null, typeof(System.String), 34);
			this.AddElementFieldMapping("OrderEntity", "DeliverypointgroupName", "DeliverypointgroupName", true, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 35);
			this.AddElementFieldMapping("OrderEntity", "IsCustomerVip", "IsCustomerVip", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 36);
			this.AddElementFieldMapping("OrderEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 37);
			this.AddElementFieldMapping("OrderEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 38);
			this.AddElementFieldMapping("OrderEntity", "BenchmarkProcessedUTC", "BenchmarkProcessedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 39);
			this.AddElementFieldMapping("OrderEntity", "ProcessedUTC", "ProcessedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 40);
			this.AddElementFieldMapping("OrderEntity", "CurrencyCode", "CurrencyCode", true, "NVarChar", 10, 0, 0, false, "", null, typeof(System.String), 41);
			this.AddElementFieldMapping("OrderEntity", "AnalyticsPayLoad", "AnalyticsPayLoad", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 42);
			this.AddElementFieldMapping("OrderEntity", "AnalyticsTrackingIds", "AnalyticsTrackingIds", true, "NVarChar", 100, 0, 0, false, "", null, typeof(System.String), 43);
			this.AddElementFieldMapping("OrderEntity", "TimeZoneOlsonId", "TimeZoneOlsonId", true, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 44);
			this.AddElementFieldMapping("OrderEntity", "DeliverypointEnableAnalytics", "DeliverypointEnableAnalytics", true, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 45);
			this.AddElementFieldMapping("OrderEntity", "PlaceTimeUTC", "PlaceTimeUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 46);
			this.AddElementFieldMapping("OrderEntity", "ExternalOrderId", "ExternalOrderId", true, "VarChar", 50, 0, 0, false, "", null, typeof(System.String), 47);
			this.AddElementFieldMapping("OrderEntity", "ServiceMethodId", "ServiceMethodId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 48);
			this.AddElementFieldMapping("OrderEntity", "CheckoutMethodId", "CheckoutMethodId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 49);
			this.AddElementFieldMapping("OrderEntity", "CheckoutMethodName", "CheckoutMethodName", true, "NVarChar", 256, 0, 0, false, "", null, typeof(System.String), 50);
			this.AddElementFieldMapping("OrderEntity", "CheckoutMethodType", "CheckoutMethodType", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 51);
			this.AddElementFieldMapping("OrderEntity", "ServiceMethodName", "ServiceMethodName", true, "NVarChar", 256, 0, 0, false, "", null, typeof(System.String), 52);
			this.AddElementFieldMapping("OrderEntity", "ServiceMethodType", "ServiceMethodType", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 53);
			this.AddElementFieldMapping("OrderEntity", "CultureCode", "CultureCode", true, "NVarChar", 10, 0, 0, false, "", null, typeof(System.String), 54);
			this.AddElementFieldMapping("OrderEntity", "Total", "Total", false, "Money", 0, 19, 4, false, "", null, typeof(System.Decimal), 55);
			this.AddElementFieldMapping("OrderEntity", "SubTotal", "SubTotal", false, "Money", 0, 19, 4, false, "", null, typeof(System.Decimal), 56);
			this.AddElementFieldMapping("OrderEntity", "DeliveryInformationId", "DeliveryInformationId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 57);
			this.AddElementFieldMapping("OrderEntity", "ChargeToDeliverypointName", "ChargeToDeliverypointName", true, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 58);
			this.AddElementFieldMapping("OrderEntity", "ChargeToDeliverypointNumber", "ChargeToDeliverypointNumber", true, "NVarChar", 20, 0, 0, false, "", null, typeof(System.String), 59);
			this.AddElementFieldMapping("OrderEntity", "ChargeToDeliverypointgroupName", "ChargeToDeliverypointgroupName", true, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 60);
			this.AddElementFieldMapping("OrderEntity", "PricesIncludeTaxes", "PricesIncludeTaxes", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 61);
			this.AddElementFieldMapping("OrderEntity", "OutletId", "OutletId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 62);
			this.AddElementFieldMapping("OrderEntity", "ChargeToDeliverypointId", "ChargeToDeliverypointId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 63);
			this.AddElementFieldMapping("OrderEntity", "OptInStatus", "OptInStatus", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 64);
			this.AddElementFieldMapping("OrderEntity", "TaxBreakdown", "TaxBreakdown", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 65);
			this.AddElementFieldMapping("OrderEntity", "CountryCode", "CountryCode", true, "NVarChar", 10, 0, 0, false, "", null, typeof(System.String), 66);
			this.AddElementFieldMapping("OrderEntity", "CoversCount", "CoversCount", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 67);
		}

		/// <summary>Inits OrderHourEntity's mappings</summary>
		private void InitOrderHourEntityMappings()
		{
			this.AddElementMapping("OrderHourEntity", @"Obymobi", @"dbo", "OrderHour", 12, 0);
			this.AddElementFieldMapping("OrderHourEntity", "OrderHourId", "OrderHourId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("OrderHourEntity", "ProductId", "ProductId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("OrderHourEntity", "CategoryId", "CategoryId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("OrderHourEntity", "SortOrder", "SortOrder", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("OrderHourEntity", "DayOfWeek", "DayOfWeek", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("OrderHourEntity", "TimeStart", "TimeStart", false, "Char", 4, 0, 0, false, "", null, typeof(System.String), 5);
			this.AddElementFieldMapping("OrderHourEntity", "TimeEnd", "TimeEnd", false, "Char", 4, 0, 0, false, "", null, typeof(System.String), 6);
			this.AddElementFieldMapping("OrderHourEntity", "CreatedBy", "CreatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 7);
			this.AddElementFieldMapping("OrderHourEntity", "UpdatedBy", "UpdatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 8);
			this.AddElementFieldMapping("OrderHourEntity", "ParentCompanyId", "ParentCompanyId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 9);
			this.AddElementFieldMapping("OrderHourEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 10);
			this.AddElementFieldMapping("OrderHourEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 11);
		}

		/// <summary>Inits OrderitemEntity's mappings</summary>
		private void InitOrderitemEntityMappings()
		{
			this.AddElementMapping("OrderitemEntity", @"Obymobi", @"dbo", "Orderitem", 34, 0);
			this.AddElementFieldMapping("OrderitemEntity", "OrderitemId", "OrderitemId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("OrderitemEntity", "OrderId", "OrderId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("OrderitemEntity", "ProductId", "ProductId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("OrderitemEntity", "XProductDescription", "ProductDescription", true, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 3);
			this.AddElementFieldMapping("OrderitemEntity", "ProductName", "ProductName", true, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 4);
			this.AddElementFieldMapping("OrderitemEntity", "ProductPriceIn", "ProductPriceIn", false, "Money", 0, 19, 4, false, "", null, typeof(System.Decimal), 5);
			this.AddElementFieldMapping("OrderitemEntity", "Quantity", "Quantity", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 6);
			this.AddElementFieldMapping("OrderitemEntity", "XPriceIn", "PriceIn", true, "Money", 0, 19, 4, false, "", null, typeof(System.Decimal), 7);
			this.AddElementFieldMapping("OrderitemEntity", "VatPercentage", "VatPercentage", false, "Float", 0, 38, 0, false, "", null, typeof(System.Double), 8);
			this.AddElementFieldMapping("OrderitemEntity", "Notes", "Notes", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 9);
			this.AddElementFieldMapping("OrderitemEntity", "CreatedBy", "CreatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 10);
			this.AddElementFieldMapping("OrderitemEntity", "UpdatedBy", "UpdatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 11);
			this.AddElementFieldMapping("OrderitemEntity", "Guid", "Guid", true, "VarChar", 128, 0, 0, false, "", null, typeof(System.String), 12);
			this.AddElementFieldMapping("OrderitemEntity", "CategoryId", "CategoryId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 13);
			this.AddElementFieldMapping("OrderitemEntity", "Color", "Color", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 14);
			this.AddElementFieldMapping("OrderitemEntity", "CategoryName", "CategoryName", true, "NVarChar", 500, 0, 0, false, "", null, typeof(System.String), 15);
			this.AddElementFieldMapping("OrderitemEntity", "OrderSource", "OrderSource", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 16);
			this.AddElementFieldMapping("OrderitemEntity", "ParentCompanyId", "ParentCompanyId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 17);
			this.AddElementFieldMapping("OrderitemEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 18);
			this.AddElementFieldMapping("OrderitemEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 19);
			this.AddElementFieldMapping("OrderitemEntity", "CategoryPath", "CategoryPath", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 20);
			this.AddElementFieldMapping("OrderitemEntity", "PriceLevelItemId", "PriceLevelItemId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 21);
			this.AddElementFieldMapping("OrderitemEntity", "PriceLevelLog", "PriceLevelLog", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 22);
			this.AddElementFieldMapping("OrderitemEntity", "Type", "Type", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 23);
			this.AddElementFieldMapping("OrderitemEntity", "PriceInTax", "PriceInTax", false, "Money", 0, 19, 4, false, "", null, typeof(System.Decimal), 24);
			this.AddElementFieldMapping("OrderitemEntity", "PriceExTax", "PriceExTax", false, "Money", 0, 19, 4, false, "", null, typeof(System.Decimal), 25);
			this.AddElementFieldMapping("OrderitemEntity", "PriceTotalInTax", "PriceTotalInTax", false, "Money", 0, 19, 4, false, "", null, typeof(System.Decimal), 26);
			this.AddElementFieldMapping("OrderitemEntity", "PriceTotalExTax", "PriceTotalExTax", false, "Money", 0, 19, 4, false, "", null, typeof(System.Decimal), 27);
			this.AddElementFieldMapping("OrderitemEntity", "TaxPercentage", "TaxPercentage", false, "Float", 0, 38, 0, false, "", null, typeof(System.Double), 28);
			this.AddElementFieldMapping("OrderitemEntity", "Tax", "Tax", false, "Money", 0, 19, 4, false, "", null, typeof(System.Decimal), 29);
			this.AddElementFieldMapping("OrderitemEntity", "TaxTariffId", "TaxTariffId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 30);
			this.AddElementFieldMapping("OrderitemEntity", "TaxTotal", "TaxTotal", false, "Money", 0, 19, 4, false, "", null, typeof(System.Decimal), 31);
			this.AddElementFieldMapping("OrderitemEntity", "TaxName", "TaxName", true, "NVarChar", 200, 0, 0, false, "", null, typeof(System.String), 32);
			this.AddElementFieldMapping("OrderitemEntity", "ExternalIdentifier", "ExternalIdentifier", true, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 33);
		}

		/// <summary>Inits OrderitemAlterationitemEntity's mappings</summary>
		private void InitOrderitemAlterationitemEntityMappings()
		{
			this.AddElementMapping("OrderitemAlterationitemEntity", @"Obymobi", @"dbo", "OrderitemAlterationitem", 33, 0);
			this.AddElementFieldMapping("OrderitemAlterationitemEntity", "OrderitemAlterationitemId", "OrderitemAlterationitemId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("OrderitemAlterationitemEntity", "OrderitemId", "OrderitemId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("OrderitemAlterationitemEntity", "AlterationitemId", "AlterationitemId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("OrderitemAlterationitemEntity", "AlterationName", "AlterationName", true, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 3);
			this.AddElementFieldMapping("OrderitemAlterationitemEntity", "AlterationoptionName", "AlterationoptionName", true, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 4);
			this.AddElementFieldMapping("OrderitemAlterationitemEntity", "AlterationoptionPriceIn", "AlterationoptionPriceIn", false, "Money", 0, 19, 4, false, "", null, typeof(System.Decimal), 5);
			this.AddElementFieldMapping("OrderitemAlterationitemEntity", "FormerAlterationitemId", "FormerAlterationitemId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 6);
			this.AddElementFieldMapping("OrderitemAlterationitemEntity", "Guid", "Guid", true, "VarChar", 128, 0, 0, false, "", null, typeof(System.String), 7);
			this.AddElementFieldMapping("OrderitemAlterationitemEntity", "CreatedBy", "CreatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 8);
			this.AddElementFieldMapping("OrderitemAlterationitemEntity", "UpdatedBy", "UpdatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 9);
			this.AddElementFieldMapping("OrderitemAlterationitemEntity", "Time", "Time", true, "DateTime", 0, 0, 0, false, "", null, typeof(System.DateTime), 10);
			this.AddElementFieldMapping("OrderitemAlterationitemEntity", "AlterationType", "AlterationType", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 11);
			this.AddElementFieldMapping("OrderitemAlterationitemEntity", "Value", "Value", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 12);
			this.AddElementFieldMapping("OrderitemAlterationitemEntity", "ParentCompanyId", "ParentCompanyId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 13);
			this.AddElementFieldMapping("OrderitemAlterationitemEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 14);
			this.AddElementFieldMapping("OrderitemAlterationitemEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 15);
			this.AddElementFieldMapping("OrderitemAlterationitemEntity", "AlterationoptionType", "AlterationoptionType", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 16);
			this.AddElementFieldMapping("OrderitemAlterationitemEntity", "PriceLevelItemId", "PriceLevelItemId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 17);
			this.AddElementFieldMapping("OrderitemAlterationitemEntity", "PriceLevelLog", "PriceLevelLog", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 18);
			this.AddElementFieldMapping("OrderitemAlterationitemEntity", "AlterationId", "AlterationId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 19);
			this.AddElementFieldMapping("OrderitemAlterationitemEntity", "TimeUTC", "TimeUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 20);
			this.AddElementFieldMapping("OrderitemAlterationitemEntity", "OrderLevelEnabled", "OrderLevelEnabled", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 21);
			this.AddElementFieldMapping("OrderitemAlterationitemEntity", "PriceInTax", "PriceInTax", false, "Money", 0, 19, 4, false, "", null, typeof(System.Decimal), 22);
			this.AddElementFieldMapping("OrderitemAlterationitemEntity", "PriceExTax", "PriceExTax", false, "Money", 0, 19, 4, false, "", null, typeof(System.Decimal), 23);
			this.AddElementFieldMapping("OrderitemAlterationitemEntity", "PriceTotalInTax", "PriceTotalInTax", false, "Money", 0, 19, 4, false, "", null, typeof(System.Decimal), 24);
			this.AddElementFieldMapping("OrderitemAlterationitemEntity", "PriceTotalExTax", "PriceTotalExTax", false, "Money", 0, 19, 4, false, "", null, typeof(System.Decimal), 25);
			this.AddElementFieldMapping("OrderitemAlterationitemEntity", "TaxPercentage", "TaxPercentage", false, "Float", 0, 38, 0, false, "", null, typeof(System.Double), 26);
			this.AddElementFieldMapping("OrderitemAlterationitemEntity", "Tax", "Tax", false, "Money", 0, 19, 4, false, "", null, typeof(System.Decimal), 27);
			this.AddElementFieldMapping("OrderitemAlterationitemEntity", "TaxTariffId", "TaxTariffId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 28);
			this.AddElementFieldMapping("OrderitemAlterationitemEntity", "TaxTotal", "TaxTotal", false, "Money", 0, 19, 4, false, "", null, typeof(System.Decimal), 29);
			this.AddElementFieldMapping("OrderitemAlterationitemEntity", "TaxName", "TaxName", true, "NVarChar", 200, 0, 0, false, "", null, typeof(System.String), 30);
			this.AddElementFieldMapping("OrderitemAlterationitemEntity", "AlterationoptionProductId", "AlterationoptionProductId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 31);
			this.AddElementFieldMapping("OrderitemAlterationitemEntity", "ExternalIdentifier", "ExternalIdentifier", true, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 32);
		}

		/// <summary>Inits OrderitemAlterationitemTagEntity's mappings</summary>
		private void InitOrderitemAlterationitemTagEntityMappings()
		{
			this.AddElementMapping("OrderitemAlterationitemTagEntity", @"Obymobi", @"dbo", "OrderitemAlterationitemTag", 7, 0);
			this.AddElementFieldMapping("OrderitemAlterationitemTagEntity", "OrderitemAlterationitemTagId", "OrderitemAlterationitemTagId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("OrderitemAlterationitemTagEntity", "OrderitemAlterationitemId", "OrderitemAlterationitemId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("OrderitemAlterationitemTagEntity", "AlterationoptionId", "AlterationoptionId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("OrderitemAlterationitemTagEntity", "TagId", "TagId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("OrderitemAlterationitemTagEntity", "CreatedUTC", "CreatedUTC", false, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 4);
			this.AddElementFieldMapping("OrderitemAlterationitemTagEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 5);
			this.AddElementFieldMapping("OrderitemAlterationitemTagEntity", "ParentCompanyId", "ParentCompanyId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 6);
		}

		/// <summary>Inits OrderitemTagEntity's mappings</summary>
		private void InitOrderitemTagEntityMappings()
		{
			this.AddElementMapping("OrderitemTagEntity", @"Obymobi", @"dbo", "OrderitemTag", 8, 0);
			this.AddElementFieldMapping("OrderitemTagEntity", "OrderitemTagId", "OrderitemTagId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("OrderitemTagEntity", "OrderitemId", "OrderitemId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("OrderitemTagEntity", "ProductCategoryTagId", "ProductCategoryTagId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("OrderitemTagEntity", "ProductId", "ProductId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("OrderitemTagEntity", "TagId", "TagId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("OrderitemTagEntity", "CreatedUTC", "CreatedUTC", false, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 5);
			this.AddElementFieldMapping("OrderitemTagEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 6);
			this.AddElementFieldMapping("OrderitemTagEntity", "ParentCompanyId", "ParentCompanyId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 7);
		}

		/// <summary>Inits OrderNotificationLogEntity's mappings</summary>
		private void InitOrderNotificationLogEntityMappings()
		{
			this.AddElementMapping("OrderNotificationLogEntity", @"Obymobi", @"dbo", "OrderNotificationLog", 12, 0);
			this.AddElementFieldMapping("OrderNotificationLogEntity", "OrderNotificationLogId", "OrderNotificationLogId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("OrderNotificationLogEntity", "OrderId", "OrderId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("OrderNotificationLogEntity", "ParentCompanyId", "ParentCompanyId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("OrderNotificationLogEntity", "Type", "Type", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("OrderNotificationLogEntity", "Method", "Method", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("OrderNotificationLogEntity", "Status", "Status", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 5);
			this.AddElementFieldMapping("OrderNotificationLogEntity", "Receiver", "Receiver", false, "NVarChar", 250, 0, 0, false, "", null, typeof(System.String), 6);
			this.AddElementFieldMapping("OrderNotificationLogEntity", "Subject", "Subject", true, "NVarChar", 250, 0, 0, false, "", null, typeof(System.String), 7);
			this.AddElementFieldMapping("OrderNotificationLogEntity", "Message", "Message", false, "Text", 2147483647, 0, 0, false, "", null, typeof(System.String), 8);
			this.AddElementFieldMapping("OrderNotificationLogEntity", "Log", "Log", true, "Text", 2147483647, 0, 0, false, "", null, typeof(System.String), 9);
			this.AddElementFieldMapping("OrderNotificationLogEntity", "CreatedUTC", "CreatedUTC", false, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 10);
			this.AddElementFieldMapping("OrderNotificationLogEntity", "SentUTC", "SentUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 11);
		}

		/// <summary>Inits OrderRoutestephandlerEntity's mappings</summary>
		private void InitOrderRoutestephandlerEntityMappings()
		{
			this.AddElementMapping("OrderRoutestephandlerEntity", @"Obymobi", @"dbo", "OrderRoutestephandler", 48, 0);
			this.AddElementFieldMapping("OrderRoutestephandlerEntity", "OrderRoutestephandlerId", "OrderRoutestephandlerId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("OrderRoutestephandlerEntity", "OrderId", "OrderId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("OrderRoutestephandlerEntity", "TerminalId", "TerminalId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("OrderRoutestephandlerEntity", "Number", "Number", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("OrderRoutestephandlerEntity", "HandlerType", "HandlerType", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("OrderRoutestephandlerEntity", "PrintReportType", "PrintReportType", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 5);
			this.AddElementFieldMapping("OrderRoutestephandlerEntity", "Status", "Status", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 6);
			this.AddElementFieldMapping("OrderRoutestephandlerEntity", "FieldValue1", "FieldValue1", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 7);
			this.AddElementFieldMapping("OrderRoutestephandlerEntity", "FieldValue2", "FieldValue2", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 8);
			this.AddElementFieldMapping("OrderRoutestephandlerEntity", "FieldValue3", "FieldValue3", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 9);
			this.AddElementFieldMapping("OrderRoutestephandlerEntity", "FieldValue4", "FieldValue4", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 10);
			this.AddElementFieldMapping("OrderRoutestephandlerEntity", "FieldValue5", "FieldValue5", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 11);
			this.AddElementFieldMapping("OrderRoutestephandlerEntity", "FieldValue6", "FieldValue6", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 12);
			this.AddElementFieldMapping("OrderRoutestephandlerEntity", "FieldValue7", "FieldValue7", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 13);
			this.AddElementFieldMapping("OrderRoutestephandlerEntity", "FieldValue8", "FieldValue8", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 14);
			this.AddElementFieldMapping("OrderRoutestephandlerEntity", "FieldValue9", "FieldValue9", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 15);
			this.AddElementFieldMapping("OrderRoutestephandlerEntity", "FieldValue10", "FieldValue10", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 16);
			this.AddElementFieldMapping("OrderRoutestephandlerEntity", "CreatedBy", "CreatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 17);
			this.AddElementFieldMapping("OrderRoutestephandlerEntity", "UpdatedBy", "UpdatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 18);
			this.AddElementFieldMapping("OrderRoutestephandlerEntity", "ErrorCode", "ErrorCode", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 19);
			this.AddElementFieldMapping("OrderRoutestephandlerEntity", "ErrorText", "ErrorText", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 20);
			this.AddElementFieldMapping("OrderRoutestephandlerEntity", "Guid", "Guid", true, "NVarChar", 128, 0, 0, false, "", null, typeof(System.String), 21);
			this.AddElementFieldMapping("OrderRoutestephandlerEntity", "Timeout", "Timeout", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 22);
			this.AddElementFieldMapping("OrderRoutestephandlerEntity", "LogAlways", "LogAlways", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 23);
			this.AddElementFieldMapping("OrderRoutestephandlerEntity", "ForwardedFromTerminalId", "ForwardedFromTerminalId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 24);
			this.AddElementFieldMapping("OrderRoutestephandlerEntity", "EscalationStep", "EscalationStep", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 25);
			this.AddElementFieldMapping("OrderRoutestephandlerEntity", "ContinueOnFailure", "ContinueOnFailure", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 26);
			this.AddElementFieldMapping("OrderRoutestephandlerEntity", "CompleteRouteOnComplete", "CompleteRouteOnComplete", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 27);
			this.AddElementFieldMapping("OrderRoutestephandlerEntity", "EscalationRouteId", "EscalationRouteId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 28);
			this.AddElementFieldMapping("OrderRoutestephandlerEntity", "SupportpoolId", "SupportpoolId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 29);
			this.AddElementFieldMapping("OrderRoutestephandlerEntity", "RetrievalSupportNotificationSent", "RetrievalSupportNotificationSent", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 30);
			this.AddElementFieldMapping("OrderRoutestephandlerEntity", "BeingHandledSupportNotificationSent", "BeingHandledSupportNotificationSent", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 31);
			this.AddElementFieldMapping("OrderRoutestephandlerEntity", "RetrievalSupportNotificationTimeoutMinutes", "RetrievalSupportNotificationTimeoutMinutes", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 32);
			this.AddElementFieldMapping("OrderRoutestephandlerEntity", "BeingHandledSupportNotificationTimeoutMinutes", "BeingHandledSupportNotificationTimeoutMinutes", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 33);
			this.AddElementFieldMapping("OrderRoutestephandlerEntity", "HandlerTypeText", "HandlerTypeText", true, "NVarChar", 100, 0, 0, false, "", null, typeof(System.String), 34);
			this.AddElementFieldMapping("OrderRoutestephandlerEntity", "StatusText", "StatusText", true, "NVarChar", 100, 0, 0, false, "", null, typeof(System.String), 35);
			this.AddElementFieldMapping("OrderRoutestephandlerEntity", "OriginatedFromRoutestepHandlerId", "OriginatedFromRoutestepHandlerId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 36);
			this.AddElementFieldMapping("OrderRoutestephandlerEntity", "ParentCompanyId", "ParentCompanyId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 37);
			this.AddElementFieldMapping("OrderRoutestephandlerEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 38);
			this.AddElementFieldMapping("OrderRoutestephandlerEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 39);
			this.AddElementFieldMapping("OrderRoutestephandlerEntity", "TimeoutExpiresUTC", "TimeoutExpiresUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 40);
			this.AddElementFieldMapping("OrderRoutestephandlerEntity", "RetrievalSupportNotificationTimeoutUTC", "RetrievalSupportNotificationTimeoutUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 41);
			this.AddElementFieldMapping("OrderRoutestephandlerEntity", "BeingHandledSupportNotificationTimeoutUTC", "BeingHandledSupportNotificationTimeoutUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 42);
			this.AddElementFieldMapping("OrderRoutestephandlerEntity", "WaitingToBeRetrievedUTC", "WaitingToBeRetrievedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 43);
			this.AddElementFieldMapping("OrderRoutestephandlerEntity", "RetrievedByHandlerUTC", "RetrievedByHandlerUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 44);
			this.AddElementFieldMapping("OrderRoutestephandlerEntity", "BeingHandledUTC", "BeingHandledUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 45);
			this.AddElementFieldMapping("OrderRoutestephandlerEntity", "CompletedUTC", "CompletedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 46);
			this.AddElementFieldMapping("OrderRoutestephandlerEntity", "ExternalSystemId", "ExternalSystemId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 47);
		}

		/// <summary>Inits OrderRoutestephandlerHistoryEntity's mappings</summary>
		private void InitOrderRoutestephandlerHistoryEntityMappings()
		{
			this.AddElementMapping("OrderRoutestephandlerHistoryEntity", @"Obymobi", @"dbo", "OrderRoutestephandlerHistory", 48, 0);
			this.AddElementFieldMapping("OrderRoutestephandlerHistoryEntity", "OrderRoutestephandlerHistoryId", "OrderRoutestephandlerHistoryId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("OrderRoutestephandlerHistoryEntity", "Guid", "Guid", true, "NVarChar", 128, 0, 0, false, "", null, typeof(System.String), 1);
			this.AddElementFieldMapping("OrderRoutestephandlerHistoryEntity", "OrderId", "OrderId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("OrderRoutestephandlerHistoryEntity", "TerminalId", "TerminalId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("OrderRoutestephandlerHistoryEntity", "Number", "Number", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("OrderRoutestephandlerHistoryEntity", "HandlerType", "HandlerType", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 5);
			this.AddElementFieldMapping("OrderRoutestephandlerHistoryEntity", "PrintReportType", "PrintReportType", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 6);
			this.AddElementFieldMapping("OrderRoutestephandlerHistoryEntity", "Status", "Status", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 7);
			this.AddElementFieldMapping("OrderRoutestephandlerHistoryEntity", "Timeout", "Timeout", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 8);
			this.AddElementFieldMapping("OrderRoutestephandlerHistoryEntity", "ErrorCode", "ErrorCode", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 9);
			this.AddElementFieldMapping("OrderRoutestephandlerHistoryEntity", "ErrorText", "ErrorText", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 10);
			this.AddElementFieldMapping("OrderRoutestephandlerHistoryEntity", "FieldValue1", "FieldValue1", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 11);
			this.AddElementFieldMapping("OrderRoutestephandlerHistoryEntity", "FieldValue2", "FieldValue2", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 12);
			this.AddElementFieldMapping("OrderRoutestephandlerHistoryEntity", "FieldValue3", "FieldValue3", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 13);
			this.AddElementFieldMapping("OrderRoutestephandlerHistoryEntity", "FieldValue4", "FieldValue4", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 14);
			this.AddElementFieldMapping("OrderRoutestephandlerHistoryEntity", "FieldValue5", "FieldValue5", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 15);
			this.AddElementFieldMapping("OrderRoutestephandlerHistoryEntity", "FieldValue6", "FieldValue6", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 16);
			this.AddElementFieldMapping("OrderRoutestephandlerHistoryEntity", "FieldValue7", "FieldValue7", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 17);
			this.AddElementFieldMapping("OrderRoutestephandlerHistoryEntity", "FieldValue8", "FieldValue8", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 18);
			this.AddElementFieldMapping("OrderRoutestephandlerHistoryEntity", "FieldValue9", "FieldValue9", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 19);
			this.AddElementFieldMapping("OrderRoutestephandlerHistoryEntity", "FieldValue10", "FieldValue10", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 20);
			this.AddElementFieldMapping("OrderRoutestephandlerHistoryEntity", "LogAlways", "LogAlways", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 21);
			this.AddElementFieldMapping("OrderRoutestephandlerHistoryEntity", "CreatedBy", "CreatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 22);
			this.AddElementFieldMapping("OrderRoutestephandlerHistoryEntity", "UpdatedBy", "UpdatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 23);
			this.AddElementFieldMapping("OrderRoutestephandlerHistoryEntity", "ForwardedFromTerminalId", "ForwardedFromTerminalId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 24);
			this.AddElementFieldMapping("OrderRoutestephandlerHistoryEntity", "ContinueOnFailure", "ContinueOnFailure", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 25);
			this.AddElementFieldMapping("OrderRoutestephandlerHistoryEntity", "CompleteRouteOnComplete", "CompleteRouteOnComplete", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 26);
			this.AddElementFieldMapping("OrderRoutestephandlerHistoryEntity", "EscalationRouteId", "EscalationRouteId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 27);
			this.AddElementFieldMapping("OrderRoutestephandlerHistoryEntity", "EscalationStep", "EscalationStep", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 28);
			this.AddElementFieldMapping("OrderRoutestephandlerHistoryEntity", "SupportpoolId", "SupportpoolId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 29);
			this.AddElementFieldMapping("OrderRoutestephandlerHistoryEntity", "RetrievalSupportNotificationTimeoutMinutes", "RetrievalSupportNotificationTimeoutMinutes", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 30);
			this.AddElementFieldMapping("OrderRoutestephandlerHistoryEntity", "RetrievalSupportNotificationSent", "RetrievalSupportNotificationSent", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 31);
			this.AddElementFieldMapping("OrderRoutestephandlerHistoryEntity", "BeingHandledSupportNotificationTimeoutMinutes", "BeingHandledSupportNotificationTimeoutMinutes", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 32);
			this.AddElementFieldMapping("OrderRoutestephandlerHistoryEntity", "BeingHandledSupportNotificationSent", "BeingHandledSupportNotificationSent", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 33);
			this.AddElementFieldMapping("OrderRoutestephandlerHistoryEntity", "HandlerTypeText", "HandlerTypeText", true, "NVarChar", 100, 0, 0, false, "", null, typeof(System.String), 34);
			this.AddElementFieldMapping("OrderRoutestephandlerHistoryEntity", "StatusText", "StatusText", true, "NVarChar", 100, 0, 0, false, "", null, typeof(System.String), 35);
			this.AddElementFieldMapping("OrderRoutestephandlerHistoryEntity", "OriginatedFromRoutestepHandlerId", "OriginatedFromRoutestepHandlerId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 36);
			this.AddElementFieldMapping("OrderRoutestephandlerHistoryEntity", "ParentCompanyId", "ParentCompanyId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 37);
			this.AddElementFieldMapping("OrderRoutestephandlerHistoryEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 38);
			this.AddElementFieldMapping("OrderRoutestephandlerHistoryEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 39);
			this.AddElementFieldMapping("OrderRoutestephandlerHistoryEntity", "TimeoutExpiresUTC", "TimeoutExpiresUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 40);
			this.AddElementFieldMapping("OrderRoutestephandlerHistoryEntity", "RetrievalSupportNotificationTimeoutUTC", "RetrievalSupportNotificationTimeoutUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 41);
			this.AddElementFieldMapping("OrderRoutestephandlerHistoryEntity", "BeingHandledSupportNotificationTimeoutUTC", "BeingHandledSupportNotificationTimeoutUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 42);
			this.AddElementFieldMapping("OrderRoutestephandlerHistoryEntity", "WaitingToBeRetrievedUTC", "WaitingToBeRetrievedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 43);
			this.AddElementFieldMapping("OrderRoutestephandlerHistoryEntity", "RetrievedByHandlerUTC", "RetrievedByHandlerUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 44);
			this.AddElementFieldMapping("OrderRoutestephandlerHistoryEntity", "BeingHandledUTC", "BeingHandledUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 45);
			this.AddElementFieldMapping("OrderRoutestephandlerHistoryEntity", "CompletedUTC", "CompletedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 46);
			this.AddElementFieldMapping("OrderRoutestephandlerHistoryEntity", "ExternalSystemId", "ExternalSystemId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 47);
		}

		/// <summary>Inits OutletEntity's mappings</summary>
		private void InitOutletEntityMappings()
		{
			this.AddElementMapping("OutletEntity", @"Obymobi", @"dbo", "Outlet", 30, 0);
			this.AddElementFieldMapping("OutletEntity", "OutletId", "OutletId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("OutletEntity", "CompanyId", "CompanyId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("OutletEntity", "Name", "Name", false, "NVarChar", 256, 0, 0, false, "", null, typeof(System.String), 2);
			this.AddElementFieldMapping("OutletEntity", "TippingActive", "TippingActive", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 3);
			this.AddElementFieldMapping("OutletEntity", "TippingDefaultPercentage", "TippingDefaultPercentage", true, "Float", 0, 38, 0, false, "", null, typeof(System.Double), 4);
			this.AddElementFieldMapping("OutletEntity", "TippingMinimumPercentage", "TippingMinimumPercentage", true, "Float", 0, 38, 0, false, "", null, typeof(System.Double), 5);
			this.AddElementFieldMapping("OutletEntity", "TippingStepSize", "TippingStepSize", true, "Float", 0, 38, 0, false, "", null, typeof(System.Double), 6);
			this.AddElementFieldMapping("OutletEntity", "TippingProductId", "TippingProductId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 7);
			this.AddElementFieldMapping("OutletEntity", "CustomerDetailsSectionActive", "CustomerDetailsSectionActive", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 8);
			this.AddElementFieldMapping("OutletEntity", "CustomerDetailsPhoneRequired", "CustomerDetailsPhoneRequired", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 9);
			this.AddElementFieldMapping("OutletEntity", "CustomerDetailsEmailRequired", "CustomerDetailsEmailRequired", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 10);
			this.AddElementFieldMapping("OutletEntity", "ShowPricesIncludingTax", "ShowPricesIncludingTax", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 11);
			this.AddElementFieldMapping("OutletEntity", "ShowTaxBreakDownOnBasket", "ShowTaxBreakDownOnBasket", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 12);
			this.AddElementFieldMapping("OutletEntity", "ShowTaxBreakDownOnCheckout", "ShowTaxBreakDownOnCheckout", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 13);
			this.AddElementFieldMapping("OutletEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 14);
			this.AddElementFieldMapping("OutletEntity", "CreatedBy", "CreatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 15);
			this.AddElementFieldMapping("OutletEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 16);
			this.AddElementFieldMapping("OutletEntity", "UpdatedBy", "UpdatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 17);
			this.AddElementFieldMapping("OutletEntity", "ReceiptEmail", "ReceiptEmail", true, "NVarChar", 2000, 0, 0, false, "", null, typeof(System.String), 18);
			this.AddElementFieldMapping("OutletEntity", "OutletOperationalStateId", "OutletOperationalStateId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 19);
			this.AddElementFieldMapping("OutletEntity", "Latitude", "Latitude", false, "Float", 0, 38, 0, false, "", null, typeof(System.Double), 20);
			this.AddElementFieldMapping("OutletEntity", "Longitude", "Longitude", false, "Float", 0, 38, 0, false, "", null, typeof(System.Double), 21);
			this.AddElementFieldMapping("OutletEntity", "DeliveryChargeProductId", "DeliveryChargeProductId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 22);
			this.AddElementFieldMapping("OutletEntity", "ServiceChargeProductId", "ServiceChargeProductId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 23);
			this.AddElementFieldMapping("OutletEntity", "CustomerDetailsNameRequired", "CustomerDetailsNameRequired", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 24);
			this.AddElementFieldMapping("OutletEntity", "OutletSellerInformationId", "OutletSellerInformationId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 25);
			this.AddElementFieldMapping("OutletEntity", "ShowTaxBreakdown", "ShowTaxBreakdown", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 26);
			this.AddElementFieldMapping("OutletEntity", "OptInType", "OptInType", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 27);
			this.AddElementFieldMapping("OutletEntity", "TaxDisclaimer", "TaxDisclaimer", true, "VarChar", 2000, 0, 0, false, "", null, typeof(System.String), 28);
			this.AddElementFieldMapping("OutletEntity", "TaxNumberTitle", "TaxNumberTitle", true, "VarChar", 100, 0, 0, false, "", null, typeof(System.String), 29);
		}

		/// <summary>Inits OutletOperationalStateEntity's mappings</summary>
		private void InitOutletOperationalStateEntityMappings()
		{
			this.AddElementMapping("OutletOperationalStateEntity", @"Obymobi", @"dbo", "OutletOperationalState", 7, 0);
			this.AddElementFieldMapping("OutletOperationalStateEntity", "OutletOperationalStateId", "OutletOperationalStateId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("OutletOperationalStateEntity", "WaitTime", "WaitTime", true, "Time", 0, 0, 0, false, "", null, typeof(System.TimeSpan), 1);
			this.AddElementFieldMapping("OutletOperationalStateEntity", "OrderIntakeDisabled", "OrderIntakeDisabled", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 2);
			this.AddElementFieldMapping("OutletOperationalStateEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 3);
			this.AddElementFieldMapping("OutletOperationalStateEntity", "CreatedBy", "CreatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("OutletOperationalStateEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 5);
			this.AddElementFieldMapping("OutletOperationalStateEntity", "UpdatedBy", "UpdatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 6);
		}

		/// <summary>Inits OutletSellerInformationEntity's mappings</summary>
		private void InitOutletSellerInformationEntityMappings()
		{
			this.AddElementMapping("OutletSellerInformationEntity", @"Obymobi", @"dbo", "OutletSellerInformation", 15, 0);
			this.AddElementFieldMapping("OutletSellerInformationEntity", "OutletSellerInformationId", "OutletSellerInformationId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("OutletSellerInformationEntity", "SellerName", "SellerName", false, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 1);
			this.AddElementFieldMapping("OutletSellerInformationEntity", "Address", "Address", false, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 2);
			this.AddElementFieldMapping("OutletSellerInformationEntity", "Phonenumber", "Phonenumber", false, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 3);
			this.AddElementFieldMapping("OutletSellerInformationEntity", "Email", "Email", false, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 4);
			this.AddElementFieldMapping("OutletSellerInformationEntity", "VatNumber", "VatNumber", true, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 5);
			this.AddElementFieldMapping("OutletSellerInformationEntity", "ReceiptReceiversEmail", "ReceiptReceiversEmail", true, "NVarChar", 2000, 0, 0, false, "", null, typeof(System.String), 6);
			this.AddElementFieldMapping("OutletSellerInformationEntity", "ReceiptReceiversSms", "ReceiptReceiversSms", true, "NVarChar", 2000, 0, 0, false, "", null, typeof(System.String), 7);
			this.AddElementFieldMapping("OutletSellerInformationEntity", "SmsOriginator", "SmsOriginator", true, "NVarChar", 20, 0, 0, false, "", null, typeof(System.String), 8);
			this.AddElementFieldMapping("OutletSellerInformationEntity", "CreatedUTC", "CreatedUTC", false, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 9);
			this.AddElementFieldMapping("OutletSellerInformationEntity", "CreatedBy", "CreatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 10);
			this.AddElementFieldMapping("OutletSellerInformationEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 11);
			this.AddElementFieldMapping("OutletSellerInformationEntity", "UpdatedBy", "UpdatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 12);
			this.AddElementFieldMapping("OutletSellerInformationEntity", "ReceiveOrderReceiptNotifications", "ReceiveOrderReceiptNotifications", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 13);
			this.AddElementFieldMapping("OutletSellerInformationEntity", "ReceiveOrderConfirmationNotifications", "ReceiveOrderConfirmationNotifications", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 14);
		}

		/// <summary>Inits PageEntity's mappings</summary>
		private void InitPageEntityMappings()
		{
			this.AddElementMapping("PageEntity", @"Obymobi", @"dbo", "Page", 13, 0);
			this.AddElementFieldMapping("PageEntity", "PageId", "PageId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("PageEntity", "Name", "Name", true, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 1);
			this.AddElementFieldMapping("PageEntity", "SiteId", "SiteId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("PageEntity", "ParentPageId", "ParentPageId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("PageEntity", "PageType", "PageType", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("PageEntity", "CreatedBy", "CreatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 5);
			this.AddElementFieldMapping("PageEntity", "UpdatedBy", "UpdatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 6);
			this.AddElementFieldMapping("PageEntity", "PageTemplateId", "PageTemplateId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 7);
			this.AddElementFieldMapping("PageEntity", "SortOrder", "SortOrder", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 8);
			this.AddElementFieldMapping("PageEntity", "Visible", "Visible", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 9);
			this.AddElementFieldMapping("PageEntity", "ParentCompanyId", "ParentCompanyId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 10);
			this.AddElementFieldMapping("PageEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 11);
			this.AddElementFieldMapping("PageEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 12);
		}

		/// <summary>Inits PageElementEntity's mappings</summary>
		private void InitPageElementEntityMappings()
		{
			this.AddElementMapping("PageElementEntity", @"Obymobi", @"dbo", "PageElement", 26, 0);
			this.AddElementFieldMapping("PageElementEntity", "PageElementId", "PageElementId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("PageElementEntity", "PageId", "PageId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("PageElementEntity", "LanguageId", "LanguageId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("PageElementEntity", "PageElementType", "PageElementType", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("PageElementEntity", "StringValue1", "StringValue1", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 4);
			this.AddElementFieldMapping("PageElementEntity", "StringValue2", "StringValue2", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 5);
			this.AddElementFieldMapping("PageElementEntity", "StringValue3", "StringValue3", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 6);
			this.AddElementFieldMapping("PageElementEntity", "StringValue4", "StringValue4", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 7);
			this.AddElementFieldMapping("PageElementEntity", "StringValue5", "StringValue5", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 8);
			this.AddElementFieldMapping("PageElementEntity", "IntValue1", "IntValue1", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 9);
			this.AddElementFieldMapping("PageElementEntity", "IntValue2", "IntValue2", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 10);
			this.AddElementFieldMapping("PageElementEntity", "IntValue3", "IntValue3", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 11);
			this.AddElementFieldMapping("PageElementEntity", "IntValue4", "IntValue4", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 12);
			this.AddElementFieldMapping("PageElementEntity", "IntValue5", "IntValue5", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 13);
			this.AddElementFieldMapping("PageElementEntity", "BoolValue1", "BoolValue1", true, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 14);
			this.AddElementFieldMapping("PageElementEntity", "BoolValue2", "BoolValue2", true, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 15);
			this.AddElementFieldMapping("PageElementEntity", "BoolValue3", "BoolValue3", true, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 16);
			this.AddElementFieldMapping("PageElementEntity", "BoolValue4", "BoolValue4", true, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 17);
			this.AddElementFieldMapping("PageElementEntity", "BoolValue5", "BoolValue5", true, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 18);
			this.AddElementFieldMapping("PageElementEntity", "CreatedBy", "CreatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 19);
			this.AddElementFieldMapping("PageElementEntity", "UpdatedBy", "UpdatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 20);
			this.AddElementFieldMapping("PageElementEntity", "SystemName", "SystemName", true, "NVarChar", 150, 0, 0, false, "", null, typeof(System.String), 21);
			this.AddElementFieldMapping("PageElementEntity", "ParentCompanyId", "ParentCompanyId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 22);
			this.AddElementFieldMapping("PageElementEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 23);
			this.AddElementFieldMapping("PageElementEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 24);
			this.AddElementFieldMapping("PageElementEntity", "CultureCode", "CultureCode", true, "NVarChar", 10, 0, 0, false, "", null, typeof(System.String), 25);
		}

		/// <summary>Inits PageLanguageEntity's mappings</summary>
		private void InitPageLanguageEntityMappings()
		{
			this.AddElementMapping("PageLanguageEntity", @"Obymobi", @"dbo", "PageLanguage", 9, 0);
			this.AddElementFieldMapping("PageLanguageEntity", "PageLanguageId", "PageLanguageId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("PageLanguageEntity", "PageId", "PageId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("PageLanguageEntity", "LanguageId", "LanguageId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("PageLanguageEntity", "Name", "Name", true, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 3);
			this.AddElementFieldMapping("PageLanguageEntity", "CreatedBy", "CreatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("PageLanguageEntity", "UpdatedBy", "UpdatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 5);
			this.AddElementFieldMapping("PageLanguageEntity", "ParentCompanyId", "ParentCompanyId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 6);
			this.AddElementFieldMapping("PageLanguageEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 7);
			this.AddElementFieldMapping("PageLanguageEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 8);
		}

		/// <summary>Inits PageTemplateEntity's mappings</summary>
		private void InitPageTemplateEntityMappings()
		{
			this.AddElementMapping("PageTemplateEntity", @"Obymobi", @"dbo", "PageTemplate", 10, 0);
			this.AddElementFieldMapping("PageTemplateEntity", "PageTemplateId", "PageTemplateId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("PageTemplateEntity", "Name", "Name", false, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 1);
			this.AddElementFieldMapping("PageTemplateEntity", "PageType", "PageType", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("PageTemplateEntity", "CreatedBy", "CreatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("PageTemplateEntity", "UpdatedBy", "UpdatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("PageTemplateEntity", "ParentPageTemplateId", "ParentPageTemplateId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 5);
			this.AddElementFieldMapping("PageTemplateEntity", "SiteTemplateId", "SiteTemplateId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 6);
			this.AddElementFieldMapping("PageTemplateEntity", "SortOrder", "SortOrder", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 7);
			this.AddElementFieldMapping("PageTemplateEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 8);
			this.AddElementFieldMapping("PageTemplateEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 9);
		}

		/// <summary>Inits PageTemplateElementEntity's mappings</summary>
		private void InitPageTemplateElementEntityMappings()
		{
			this.AddElementMapping("PageTemplateElementEntity", @"Obymobi", @"dbo", "PageTemplateElement", 25, 0);
			this.AddElementFieldMapping("PageTemplateElementEntity", "PageTemplateElementId", "PageTemplateElementId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("PageTemplateElementEntity", "PageTemplateId", "PageTemplateId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("PageTemplateElementEntity", "LanguageId", "LanguageId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("PageTemplateElementEntity", "PageElementType", "PageElementType", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("PageTemplateElementEntity", "SystemName", "SystemName", true, "NVarChar", 150, 0, 0, false, "", null, typeof(System.String), 4);
			this.AddElementFieldMapping("PageTemplateElementEntity", "StringValue1", "StringValue1", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 5);
			this.AddElementFieldMapping("PageTemplateElementEntity", "StringValue2", "StringValue2", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 6);
			this.AddElementFieldMapping("PageTemplateElementEntity", "StringValue3", "StringValue3", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 7);
			this.AddElementFieldMapping("PageTemplateElementEntity", "StringValue4", "StringValue4", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 8);
			this.AddElementFieldMapping("PageTemplateElementEntity", "StringValue5", "StringValue5", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 9);
			this.AddElementFieldMapping("PageTemplateElementEntity", "IntValue1", "IntValue1", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 10);
			this.AddElementFieldMapping("PageTemplateElementEntity", "IntValue2", "IntValue2", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 11);
			this.AddElementFieldMapping("PageTemplateElementEntity", "IntValue3", "IntValue3", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 12);
			this.AddElementFieldMapping("PageTemplateElementEntity", "IntValue4", "IntValue4", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 13);
			this.AddElementFieldMapping("PageTemplateElementEntity", "IntValue5", "IntValue5", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 14);
			this.AddElementFieldMapping("PageTemplateElementEntity", "BoolValue1", "BoolValue1", true, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 15);
			this.AddElementFieldMapping("PageTemplateElementEntity", "BoolValue2", "BoolValue2", true, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 16);
			this.AddElementFieldMapping("PageTemplateElementEntity", "BoolValue3", "BoolValue3", true, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 17);
			this.AddElementFieldMapping("PageTemplateElementEntity", "BoolValue4", "BoolValue4", true, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 18);
			this.AddElementFieldMapping("PageTemplateElementEntity", "BoolValue5", "BoolValue5", true, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 19);
			this.AddElementFieldMapping("PageTemplateElementEntity", "CreatedBy", "CreatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 20);
			this.AddElementFieldMapping("PageTemplateElementEntity", "UpdatedBy", "UpdatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 21);
			this.AddElementFieldMapping("PageTemplateElementEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 22);
			this.AddElementFieldMapping("PageTemplateElementEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 23);
			this.AddElementFieldMapping("PageTemplateElementEntity", "CultureCode", "CultureCode", true, "NVarChar", 10, 0, 0, false, "", null, typeof(System.String), 24);
		}

		/// <summary>Inits PageTemplateLanguageEntity's mappings</summary>
		private void InitPageTemplateLanguageEntityMappings()
		{
			this.AddElementMapping("PageTemplateLanguageEntity", @"Obymobi", @"dbo", "PageTemplateLanguage", 8, 0);
			this.AddElementFieldMapping("PageTemplateLanguageEntity", "PageTemplateLanguageId", "PageTemplateLanguageId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("PageTemplateLanguageEntity", "PageTemplateId", "PageTemplateId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("PageTemplateLanguageEntity", "LanguageId", "LanguageId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("PageTemplateLanguageEntity", "Name", "Name", true, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 3);
			this.AddElementFieldMapping("PageTemplateLanguageEntity", "CreatedBy", "CreatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("PageTemplateLanguageEntity", "UpdatedBy", "UpdatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 5);
			this.AddElementFieldMapping("PageTemplateLanguageEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 6);
			this.AddElementFieldMapping("PageTemplateLanguageEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 7);
		}

		/// <summary>Inits PaymentIntegrationConfigurationEntity's mappings</summary>
		private void InitPaymentIntegrationConfigurationEntityMappings()
		{
			this.AddElementMapping("PaymentIntegrationConfigurationEntity", @"Obymobi", @"dbo", "PaymentIntegrationConfiguration", 16, 0);
			this.AddElementFieldMapping("PaymentIntegrationConfigurationEntity", "PaymentIntegrationConfigurationId", "PaymentIntegrationConfigurationId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("PaymentIntegrationConfigurationEntity", "PaymentProviderId", "PaymentProviderId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("PaymentIntegrationConfigurationEntity", "DisplayName", "DisplayName", false, "NVarChar", 250, 0, 0, false, "", null, typeof(System.String), 2);
			this.AddElementFieldMapping("PaymentIntegrationConfigurationEntity", "MerchantName", "MerchantName", false, "NVarChar", 250, 0, 0, false, "", null, typeof(System.String), 3);
			this.AddElementFieldMapping("PaymentIntegrationConfigurationEntity", "AdyenMerchantCode", "AdyenMerchantCode", true, "NVarChar", 250, 0, 0, false, "", null, typeof(System.String), 4);
			this.AddElementFieldMapping("PaymentIntegrationConfigurationEntity", "GooglePayMerchantIdentifier", "GooglePayMerchantIdentifier", true, "NVarChar", 250, 0, 0, false, "", null, typeof(System.String), 5);
			this.AddElementFieldMapping("PaymentIntegrationConfigurationEntity", "ApplePayMerchantIdentifier", "ApplePayMerchantIdentifier", true, "NVarChar", 250, 0, 0, false, "", null, typeof(System.String), 6);
			this.AddElementFieldMapping("PaymentIntegrationConfigurationEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 7);
			this.AddElementFieldMapping("PaymentIntegrationConfigurationEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 8);
			this.AddElementFieldMapping("PaymentIntegrationConfigurationEntity", "CompanyId", "CompanyId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 9);
			this.AddElementFieldMapping("PaymentIntegrationConfigurationEntity", "PaymentProcessorInfo", "PaymentProcessorInfo", true, "Text", 2147483647, 0, 0, false, "", null, typeof(System.String), 10);
			this.AddElementFieldMapping("PaymentIntegrationConfigurationEntity", "PaymentProcessorInfoFooter", "PaymentProcessorInfoFooter", true, "Text", 2147483647, 0, 0, false, "", null, typeof(System.String), 11);
			this.AddElementFieldMapping("PaymentIntegrationConfigurationEntity", "AdyenAccountCode", "AdyenAccountCode", true, "NVarChar", 250, 0, 0, false, "", null, typeof(System.String), 12);
			this.AddElementFieldMapping("PaymentIntegrationConfigurationEntity", "AdyenAccountName", "AdyenAccountName", true, "NVarChar", 250, 0, 0, false, "", null, typeof(System.String), 13);
			this.AddElementFieldMapping("PaymentIntegrationConfigurationEntity", "AdyenAccountHolderCode", "AdyenAccountHolderCode", true, "NVarChar", 250, 0, 0, false, "", null, typeof(System.String), 14);
			this.AddElementFieldMapping("PaymentIntegrationConfigurationEntity", "AdyenAccountHolderName", "AdyenAccountHolderName", true, "NVarChar", 250, 0, 0, false, "", null, typeof(System.String), 15);
		}

		/// <summary>Inits PaymentProviderEntity's mappings</summary>
		private void InitPaymentProviderEntityMappings()
		{
			this.AddElementMapping("PaymentProviderEntity", @"Obymobi", @"dbo", "PaymentProvider", 22, 0);
			this.AddElementFieldMapping("PaymentProviderEntity", "PaymentProviderId", "PaymentProviderId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("PaymentProviderEntity", "CompanyId", "CompanyId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("PaymentProviderEntity", "Type", "Type", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("PaymentProviderEntity", "Name", "Name", false, "NVarChar", 100, 0, 0, false, "", null, typeof(System.String), 3);
			this.AddElementFieldMapping("PaymentProviderEntity", "Environment", "Environment", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("PaymentProviderEntity", "ApiKey", "ApiKey", true, "NVarChar", 512, 0, 0, false, "", null, typeof(System.String), 5);
			this.AddElementFieldMapping("PaymentProviderEntity", "CreatedUTC", "CreatedUTC", false, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 6);
			this.AddElementFieldMapping("PaymentProviderEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 7);
			this.AddElementFieldMapping("PaymentProviderEntity", "LiveEndpointUrlPrefix", "LiveEndpointUrlPrefix", true, "NVarChar", 250, 0, 0, false, "", null, typeof(System.String), 8);
			this.AddElementFieldMapping("PaymentProviderEntity", "OriginDomain", "OriginDomain", true, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 9);
			this.AddElementFieldMapping("PaymentProviderEntity", "PaymentProcessorInfo", "PaymentProcessorInfo", true, "Text", 2147483647, 0, 0, false, "", null, typeof(System.String), 10);
			this.AddElementFieldMapping("PaymentProviderEntity", "PaymentProcessorInfoFooter", "PaymentProcessorInfoFooter", true, "Text", 2147483647, 0, 0, false, "", null, typeof(System.String), 11);
			this.AddElementFieldMapping("PaymentProviderEntity", "AdyenUsername", "AdyenUsername", true, "NVarChar", 100, 0, 0, false, "", null, typeof(System.String), 12);
			this.AddElementFieldMapping("PaymentProviderEntity", "AdyenPassword", "AdyenPassword", true, "NVarChar", 250, 0, 0, false, "", null, typeof(System.String), 13);
			this.AddElementFieldMapping("PaymentProviderEntity", "OmisePublicKey", "OmisePublicKey", true, "NVarChar", 100, 0, 0, false, "", null, typeof(System.String), 14);
			this.AddElementFieldMapping("PaymentProviderEntity", "OmiseSecretKey", "OmiseSecretKey", true, "NVarChar", 250, 0, 0, false, "", null, typeof(System.String), 15);
			this.AddElementFieldMapping("PaymentProviderEntity", "SkinCode", "SkinCode", true, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 16);
			this.AddElementFieldMapping("PaymentProviderEntity", "HmacKey", "HmacKey", true, "NVarChar", 250, 0, 0, false, "", null, typeof(System.String), 17);
			this.AddElementFieldMapping("PaymentProviderEntity", "ClientKey", "ClientKey", true, "NVarChar", 250, 0, 0, false, "", null, typeof(System.String), 18);
			this.AddElementFieldMapping("PaymentProviderEntity", "AdyenMerchantCode", "AdyenMerchantCode", true, "NVarChar", 250, 0, 0, false, "", null, typeof(System.String), 19);
			this.AddElementFieldMapping("PaymentProviderEntity", "GooglePayMerchantIdentifier", "GooglePayMerchantIdentifier", true, "NVarChar", 250, 0, 0, false, "", null, typeof(System.String), 20);
			this.AddElementFieldMapping("PaymentProviderEntity", "ApplePayMerchantIdentifier", "ApplePayMerchantIdentifier", true, "NVarChar", 250, 0, 0, false, "", null, typeof(System.String), 21);
		}

		/// <summary>Inits PaymentProviderCompanyEntity's mappings</summary>
		private void InitPaymentProviderCompanyEntityMappings()
		{
			this.AddElementMapping("PaymentProviderCompanyEntity", @"Obymobi", @"dbo", "PaymentProviderCompany", 7, 0);
			this.AddElementFieldMapping("PaymentProviderCompanyEntity", "PaymentProviderCompanyId", "PaymentProviderCompanyId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("PaymentProviderCompanyEntity", "PaymentProviderId", "PaymentProviderId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("PaymentProviderCompanyEntity", "CompanyId", "CompanyId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("PaymentProviderCompanyEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 3);
			this.AddElementFieldMapping("PaymentProviderCompanyEntity", "CreatedBy", "CreatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("PaymentProviderCompanyEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 5);
			this.AddElementFieldMapping("PaymentProviderCompanyEntity", "UpdatedBy", "UpdatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 6);
		}

		/// <summary>Inits PaymentTransactionEntity's mappings</summary>
		private void InitPaymentTransactionEntityMappings()
		{
			this.AddElementMapping("PaymentTransactionEntity", @"Obymobi", @"dbo", "PaymentTransaction", 17, 0);
			this.AddElementFieldMapping("PaymentTransactionEntity", "PaymentTransactionId", "PaymentTransactionId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("PaymentTransactionEntity", "OrderId", "OrderId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("PaymentTransactionEntity", "ReferenceId", "ReferenceId", true, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 2);
			this.AddElementFieldMapping("PaymentTransactionEntity", "MerchantReference", "MerchantReference", true, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 3);
			this.AddElementFieldMapping("PaymentTransactionEntity", "PaymentMethod", "PaymentMethod", false, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 4);
			this.AddElementFieldMapping("PaymentTransactionEntity", "Status", "Status", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 5);
			this.AddElementFieldMapping("PaymentTransactionEntity", "PaymentData", "PaymentData", true, "Text", 2147483647, 0, 0, false, "", null, typeof(System.String), 6);
			this.AddElementFieldMapping("PaymentTransactionEntity", "CreatedUTC", "CreatedUTC", false, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 7);
			this.AddElementFieldMapping("PaymentTransactionEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 8);
			this.AddElementFieldMapping("PaymentTransactionEntity", "MerchantAccount", "MerchantAccount", true, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 9);
			this.AddElementFieldMapping("PaymentTransactionEntity", "CardSummary", "CardSummary", true, "NVarChar", 25, 0, 0, false, "", null, typeof(System.String), 10);
			this.AddElementFieldMapping("PaymentTransactionEntity", "AuthCode", "AuthCode", true, "NVarChar", 25, 0, 0, false, "", null, typeof(System.String), 11);
			this.AddElementFieldMapping("PaymentTransactionEntity", "PaymentEnvironment", "PaymentEnvironment", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 12);
			this.AddElementFieldMapping("PaymentTransactionEntity", "PaymentProviderType", "PaymentProviderType", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 13);
			this.AddElementFieldMapping("PaymentTransactionEntity", "PaymentProviderName", "PaymentProviderName", true, "NVarChar", 100, 0, 0, false, "", null, typeof(System.String), 14);
			this.AddElementFieldMapping("PaymentTransactionEntity", "PaymentProviderId", "PaymentProviderId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 15);
			this.AddElementFieldMapping("PaymentTransactionEntity", "PaymentIntegrationConfigurationId", "PaymentIntegrationConfigurationId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 16);
		}

		/// <summary>Inits PaymentTransactionLogEntity's mappings</summary>
		private void InitPaymentTransactionLogEntityMappings()
		{
			this.AddElementMapping("PaymentTransactionLogEntity", @"Obymobi", @"dbo", "PaymentTransactionLog", 11, 0);
			this.AddElementFieldMapping("PaymentTransactionLogEntity", "PaymentTransactionLogId", "PaymentTransactionLogId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("PaymentTransactionLogEntity", "PaymentTransactionId", "PaymentTransactionId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("PaymentTransactionLogEntity", "EventCode", "EventCode", true, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 2);
			this.AddElementFieldMapping("PaymentTransactionLogEntity", "EventText", "EventText", true, "NVarChar", 250, 0, 0, false, "", null, typeof(System.String), 3);
			this.AddElementFieldMapping("PaymentTransactionLogEntity", "EventDate", "EventDate", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 4);
			this.AddElementFieldMapping("PaymentTransactionLogEntity", "Success", "Success", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 5);
			this.AddElementFieldMapping("PaymentTransactionLogEntity", "RawResponse", "RawResponse", false, "Text", 2147483647, 0, 0, false, "", null, typeof(System.String), 6);
			this.AddElementFieldMapping("PaymentTransactionLogEntity", "CreatedUTC", "CreatedUTC", false, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 7);
			this.AddElementFieldMapping("PaymentTransactionLogEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 8);
			this.AddElementFieldMapping("PaymentTransactionLogEntity", "LogType", "LogType", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 9);
			this.AddElementFieldMapping("PaymentTransactionLogEntity", "Amount", "Amount", true, "Money", 0, 19, 4, false, "", null, typeof(System.Decimal), 10);
		}

		/// <summary>Inits PaymentTransactionSplitEntity's mappings</summary>
		private void InitPaymentTransactionSplitEntityMappings()
		{
			this.AddElementMapping("PaymentTransactionSplitEntity", @"Obymobi", @"dbo", "PaymentTransactionSplit", 8, 0);
			this.AddElementFieldMapping("PaymentTransactionSplitEntity", "PaymentTransactionSplit", "PaymentTransactionSplit", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("PaymentTransactionSplitEntity", "PaymentTransactionId", "PaymentTransactionId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("PaymentTransactionSplitEntity", "ReferenceId", "ReferenceId", false, "NVarChar", 100, 0, 0, false, "", null, typeof(System.String), 2);
			this.AddElementFieldMapping("PaymentTransactionSplitEntity", "SplitType", "SplitType", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("PaymentTransactionSplitEntity", "Amount", "Amount", false, "Money", 0, 19, 4, false, "", null, typeof(System.Decimal), 4);
			this.AddElementFieldMapping("PaymentTransactionSplitEntity", "AccountCode", "AccountCode", true, "NVarChar", 250, 0, 0, false, "", null, typeof(System.String), 5);
			this.AddElementFieldMapping("PaymentTransactionSplitEntity", "CreatedUTC", "CreatedUTC", false, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 6);
			this.AddElementFieldMapping("PaymentTransactionSplitEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 7);
		}

		/// <summary>Inits PmsActionRuleEntity's mappings</summary>
		private void InitPmsActionRuleEntityMappings()
		{
			this.AddElementMapping("PmsActionRuleEntity", @"Obymobi", @"dbo", "PmsActionRule", 12, 0);
			this.AddElementFieldMapping("PmsActionRuleEntity", "PmsActionRuleId", "PmsActionRuleId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("PmsActionRuleEntity", "ParentCompanyId", "ParentCompanyId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("PmsActionRuleEntity", "PmsReportConfigurationId", "PmsReportConfigurationId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("PmsActionRuleEntity", "Active", "Active", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 3);
			this.AddElementFieldMapping("PmsActionRuleEntity", "Type", "Type", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("PmsActionRuleEntity", "ScheduledMessageId", "ScheduledMessageId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 5);
			this.AddElementFieldMapping("PmsActionRuleEntity", "ClientConfigurationId", "ClientConfigurationId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 6);
			this.AddElementFieldMapping("PmsActionRuleEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 7);
			this.AddElementFieldMapping("PmsActionRuleEntity", "UpdatedBy", "UpdatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 8);
			this.AddElementFieldMapping("PmsActionRuleEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 9);
			this.AddElementFieldMapping("PmsActionRuleEntity", "CreatedBy", "CreatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 10);
			this.AddElementFieldMapping("PmsActionRuleEntity", "MessagegroupId", "MessagegroupId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 11);
		}

		/// <summary>Inits PmsReportColumnEntity's mappings</summary>
		private void InitPmsReportColumnEntityMappings()
		{
			this.AddElementMapping("PmsReportColumnEntity", @"Obymobi", @"dbo", "PmsReportColumn", 11, 0);
			this.AddElementFieldMapping("PmsReportColumnEntity", "PmsReportColumnId", "PmsReportColumnId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("PmsReportColumnEntity", "ParentCompanyId", "ParentCompanyId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("PmsReportColumnEntity", "PmsReportConfigurationId", "PmsReportConfigurationId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("PmsReportColumnEntity", "FriendlyName", "FriendlyName", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 3);
			this.AddElementFieldMapping("PmsReportColumnEntity", "DataType", "DataType", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("PmsReportColumnEntity", "Format", "Format", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 5);
			this.AddElementFieldMapping("PmsReportColumnEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 6);
			this.AddElementFieldMapping("PmsReportColumnEntity", "UpdatedBy", "UpdatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 7);
			this.AddElementFieldMapping("PmsReportColumnEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 8);
			this.AddElementFieldMapping("PmsReportColumnEntity", "CreatedBy", "CreatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 9);
			this.AddElementFieldMapping("PmsReportColumnEntity", "ColumnIndex", "ColumnIndex", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 10);
		}

		/// <summary>Inits PmsReportConfigurationEntity's mappings</summary>
		private void InitPmsReportConfigurationEntityMappings()
		{
			this.AddElementMapping("PmsReportConfigurationEntity", @"Obymobi", @"dbo", "PmsReportConfiguration", 11, 0);
			this.AddElementFieldMapping("PmsReportConfigurationEntity", "PmsReportConfigurationId", "PmsReportConfigurationId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("PmsReportConfigurationEntity", "CompanyId", "CompanyId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("PmsReportConfigurationEntity", "Name", "Name", false, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 2);
			this.AddElementFieldMapping("PmsReportConfigurationEntity", "Email", "Email", false, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 3);
			this.AddElementFieldMapping("PmsReportConfigurationEntity", "ColumnSeparationType", "ColumnSeparationType", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("PmsReportConfigurationEntity", "RoomNumberColumnId", "RoomNumberColumnId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 5);
			this.AddElementFieldMapping("PmsReportConfigurationEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 6);
			this.AddElementFieldMapping("PmsReportConfigurationEntity", "UpdatedBy", "UpdatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 7);
			this.AddElementFieldMapping("PmsReportConfigurationEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 8);
			this.AddElementFieldMapping("PmsReportConfigurationEntity", "CreatedBy", "CreatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 9);
			this.AddElementFieldMapping("PmsReportConfigurationEntity", "PmsReportType", "PmsReportType", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 10);
		}

		/// <summary>Inits PmsRuleEntity's mappings</summary>
		private void InitPmsRuleEntityMappings()
		{
			this.AddElementMapping("PmsRuleEntity", @"Obymobi", @"dbo", "PmsRule", 10, 0);
			this.AddElementFieldMapping("PmsRuleEntity", "PmsRuleId", "PmsRuleId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("PmsRuleEntity", "ParentCompanyId", "ParentCompanyId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("PmsRuleEntity", "PmsActionRuleId", "PmsActionRuleId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("PmsRuleEntity", "PmsReportColumnId", "PmsReportColumnId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("PmsRuleEntity", "RuleType", "RuleType", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("PmsRuleEntity", "Value", "Value", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 5);
			this.AddElementFieldMapping("PmsRuleEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 6);
			this.AddElementFieldMapping("PmsRuleEntity", "UpdatedBy", "UpdatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 7);
			this.AddElementFieldMapping("PmsRuleEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 8);
			this.AddElementFieldMapping("PmsRuleEntity", "CreatedBy", "CreatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 9);
		}

		/// <summary>Inits PointOfInterestEntity's mappings</summary>
		private void InitPointOfInterestEntityMappings()
		{
			this.AddElementMapping("PointOfInterestEntity", @"Obymobi", @"dbo", "PointOfInterest", 63, 0);
			this.AddElementFieldMapping("PointOfInterestEntity", "PointOfInterestId", "PointOfInterestId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("PointOfInterestEntity", "Name", "Name", false, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 1);
			this.AddElementFieldMapping("PointOfInterestEntity", "Addressline1", "Addressline1", true, "NVarChar", 200, 0, 0, false, "", null, typeof(System.String), 2);
			this.AddElementFieldMapping("PointOfInterestEntity", "Addressline2", "Addressline2", true, "NVarChar", 200, 0, 0, false, "", null, typeof(System.String), 3);
			this.AddElementFieldMapping("PointOfInterestEntity", "Addressline3", "Addressline3", true, "NVarChar", 200, 0, 0, false, "", null, typeof(System.String), 4);
			this.AddElementFieldMapping("PointOfInterestEntity", "Zipcode", "Zipcode", true, "NVarChar", 200, 0, 0, false, "", null, typeof(System.String), 5);
			this.AddElementFieldMapping("PointOfInterestEntity", "City", "City", true, "NVarChar", 200, 0, 0, false, "", null, typeof(System.String), 6);
			this.AddElementFieldMapping("PointOfInterestEntity", "CountryId", "CountryId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 7);
			this.AddElementFieldMapping("PointOfInterestEntity", "Latitude", "Latitude", true, "Float", 0, 38, 0, false, "", null, typeof(System.Double), 8);
			this.AddElementFieldMapping("PointOfInterestEntity", "Longitude", "Longitude", true, "Float", 0, 38, 0, false, "", null, typeof(System.Double), 9);
			this.AddElementFieldMapping("PointOfInterestEntity", "Telephone", "Telephone", true, "NVarChar", 200, 0, 0, false, "", null, typeof(System.String), 10);
			this.AddElementFieldMapping("PointOfInterestEntity", "Fax", "Fax", true, "NVarChar", 200, 0, 0, false, "", null, typeof(System.String), 11);
			this.AddElementFieldMapping("PointOfInterestEntity", "Website", "Website", true, "NVarChar", 200, 0, 0, false, "", null, typeof(System.String), 12);
			this.AddElementFieldMapping("PointOfInterestEntity", "Email", "Email", true, "NVarChar", 200, 0, 0, false, "", null, typeof(System.String), 13);
			this.AddElementFieldMapping("PointOfInterestEntity", "Visible", "Visible", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 14);
			this.AddElementFieldMapping("PointOfInterestEntity", "CreatedBy", "CreatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 15);
			this.AddElementFieldMapping("PointOfInterestEntity", "UpdatedBy", "UpdatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 16);
			this.AddElementFieldMapping("PointOfInterestEntity", "ActionButtonId", "ActionButtonId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 17);
			this.AddElementFieldMapping("PointOfInterestEntity", "CostIndicationType", "CostIndicationType", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 18);
			this.AddElementFieldMapping("PointOfInterestEntity", "CostIndicationValue", "CostIndicationValue", true, "Money", 0, 19, 4, false, "", null, typeof(System.Decimal), 19);
			this.AddElementFieldMapping("PointOfInterestEntity", "Description", "Description", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 20);
			this.AddElementFieldMapping("PointOfInterestEntity", "DescriptionSingleLine", "DescriptionSingleLine", true, "NVarChar", 250, 0, 0, false, "", null, typeof(System.String), 21);
			this.AddElementFieldMapping("PointOfInterestEntity", "ActionButtonUrlMobile", "ActionButtonUrlMobile", true, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 22);
			this.AddElementFieldMapping("PointOfInterestEntity", "ActionButtonUrlTablet", "ActionButtonUrlTablet", true, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 23);
			this.AddElementFieldMapping("PointOfInterestEntity", "CurrencyId", "CurrencyId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 24);
			this.AddElementFieldMapping("PointOfInterestEntity", "ExternalId", "ExternalId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 25);
			this.AddElementFieldMapping("PointOfInterestEntity", "NameOverridden", "NameOverridden", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 26);
			this.AddElementFieldMapping("PointOfInterestEntity", "DescriptionOverridden", "DescriptionOverridden", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 27);
			this.AddElementFieldMapping("PointOfInterestEntity", "DescriptionSingleLineOverridden", "DescriptionSingleLineOverridden", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 28);
			this.AddElementFieldMapping("PointOfInterestEntity", "Addressline1Overridden", "Addressline1Overridden", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 29);
			this.AddElementFieldMapping("PointOfInterestEntity", "Addressline2Overridden", "Addressline2Overridden", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 30);
			this.AddElementFieldMapping("PointOfInterestEntity", "Addressline3Overridden", "Addressline3Overridden", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 31);
			this.AddElementFieldMapping("PointOfInterestEntity", "ZipcodeOverridden", "ZipcodeOverridden", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 32);
			this.AddElementFieldMapping("PointOfInterestEntity", "CityOverridden", "CityOverridden", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 33);
			this.AddElementFieldMapping("PointOfInterestEntity", "CountryIdOverridden", "CountryIdOverridden", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 34);
			this.AddElementFieldMapping("PointOfInterestEntity", "CurrencyIdOverridden", "CurrencyIdOverridden", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 35);
			this.AddElementFieldMapping("PointOfInterestEntity", "LatitudeOverridden", "LatitudeOverridden", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 36);
			this.AddElementFieldMapping("PointOfInterestEntity", "LongitudeOverridden", "LongitudeOverridden", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 37);
			this.AddElementFieldMapping("PointOfInterestEntity", "TelephoneOverridden", "TelephoneOverridden", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 38);
			this.AddElementFieldMapping("PointOfInterestEntity", "FaxOverridden", "FaxOverridden", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 39);
			this.AddElementFieldMapping("PointOfInterestEntity", "WebsiteOverridden", "WebsiteOverridden", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 40);
			this.AddElementFieldMapping("PointOfInterestEntity", "EmailOverridden", "EmailOverridden", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 41);
			this.AddElementFieldMapping("PointOfInterestEntity", "CostIndicationTypeOverridden", "CostIndicationTypeOverridden", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 42);
			this.AddElementFieldMapping("PointOfInterestEntity", "CostIndicationValueOverridden", "CostIndicationValueOverridden", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 43);
			this.AddElementFieldMapping("PointOfInterestEntity", "ActionButtonIdOverridden", "ActionButtonIdOverridden", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 44);
			this.AddElementFieldMapping("PointOfInterestEntity", "ActionButtonUrlTabletOverridden", "ActionButtonUrlTabletOverridden", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 45);
			this.AddElementFieldMapping("PointOfInterestEntity", "ActionButtonUrlMobileOverridden", "ActionButtonUrlMobileOverridden", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 46);
			this.AddElementFieldMapping("PointOfInterestEntity", "BusinesshoursOverridden", "BusinesshoursOverridden", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 47);
			this.AddElementFieldMapping("PointOfInterestEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 48);
			this.AddElementFieldMapping("PointOfInterestEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 49);
			this.AddElementFieldMapping("PointOfInterestEntity", "LastModifiedUTC", "LastModifiedUTC", false, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 50);
			this.AddElementFieldMapping("PointOfInterestEntity", "TimeZoneIdOverridden", "TimeZoneIdOverridden", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 51);
			this.AddElementFieldMapping("PointOfInterestEntity", "TimeZoneId", "TimeZoneId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 52);
			this.AddElementFieldMapping("PointOfInterestEntity", "CountryCode", "CountryCode", true, "VarChar", 3, 0, 0, false, "", null, typeof(System.String), 53);
			this.AddElementFieldMapping("PointOfInterestEntity", "CountryCodeOverridden", "CountryCodeOverridden", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 54);
			this.AddElementFieldMapping("PointOfInterestEntity", "CurrencyCode", "CurrencyCode", true, "NVarChar", 10, 0, 0, false, "", null, typeof(System.String), 55);
			this.AddElementFieldMapping("PointOfInterestEntity", "CurrencyCodeOverridden", "CurrencyCodeOverridden", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 56);
			this.AddElementFieldMapping("PointOfInterestEntity", "CultureCode", "CultureCode", true, "VarChar", 10, 0, 0, false, "", null, typeof(System.String), 57);
			this.AddElementFieldMapping("PointOfInterestEntity", "CultureCodeOverridden", "CultureCodeOverridden", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 58);
			this.AddElementFieldMapping("PointOfInterestEntity", "TimeZoneOlsonId", "TimeZoneOlsonId", true, "VarChar", 255, 0, 0, false, "", null, typeof(System.String), 59);
			this.AddElementFieldMapping("PointOfInterestEntity", "TimeZoneOlsonIdOverridden", "TimeZoneOlsonIdOverridden", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 60);
			this.AddElementFieldMapping("PointOfInterestEntity", "Floor", "Floor", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 61);
			this.AddElementFieldMapping("PointOfInterestEntity", "DisplayDistance", "DisplayDistance", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 62);
		}

		/// <summary>Inits PointOfInterestAmenityEntity's mappings</summary>
		private void InitPointOfInterestAmenityEntityMappings()
		{
			this.AddElementMapping("PointOfInterestAmenityEntity", @"Obymobi", @"dbo", "PointOfInterestAmenity", 7, 0);
			this.AddElementFieldMapping("PointOfInterestAmenityEntity", "PointOfInterestAmenityId", "PointOfInterestAmenityId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("PointOfInterestAmenityEntity", "PointOfInterestId", "PointOfInterestId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("PointOfInterestAmenityEntity", "AmenityId", "AmenityId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("PointOfInterestAmenityEntity", "CreatedBy", "CreatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("PointOfInterestAmenityEntity", "UpdatedBy", "UpdatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("PointOfInterestAmenityEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 5);
			this.AddElementFieldMapping("PointOfInterestAmenityEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 6);
		}

		/// <summary>Inits PointOfInterestLanguageEntity's mappings</summary>
		private void InitPointOfInterestLanguageEntityMappings()
		{
			this.AddElementMapping("PointOfInterestLanguageEntity", @"Obymobi", @"dbo", "PointOfInterestLanguage", 10, 0);
			this.AddElementFieldMapping("PointOfInterestLanguageEntity", "PointOfInterestLanguageId", "PointOfInterestLanguageId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("PointOfInterestLanguageEntity", "PointOfInterestId", "PointOfInterestId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("PointOfInterestLanguageEntity", "LanguageId", "LanguageId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("PointOfInterestLanguageEntity", "Description", "Description", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 3);
			this.AddElementFieldMapping("PointOfInterestLanguageEntity", "DescriptionSingleLine", "DescriptionSingleLine", true, "NVarChar", 250, 0, 0, false, "", null, typeof(System.String), 4);
			this.AddElementFieldMapping("PointOfInterestLanguageEntity", "CreatedBy", "CreatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 5);
			this.AddElementFieldMapping("PointOfInterestLanguageEntity", "UpdatedBy", "UpdatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 6);
			this.AddElementFieldMapping("PointOfInterestLanguageEntity", "VenuePageDescription", "VenuePageDescription", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 7);
			this.AddElementFieldMapping("PointOfInterestLanguageEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 8);
			this.AddElementFieldMapping("PointOfInterestLanguageEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 9);
		}

		/// <summary>Inits PointOfInterestVenueCategoryEntity's mappings</summary>
		private void InitPointOfInterestVenueCategoryEntityMappings()
		{
			this.AddElementMapping("PointOfInterestVenueCategoryEntity", @"Obymobi", @"dbo", "PointOfInterestVenueCategory", 7, 0);
			this.AddElementFieldMapping("PointOfInterestVenueCategoryEntity", "PointOfInterestVenueCategoryId", "PointOfInterestVenueCategoryId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("PointOfInterestVenueCategoryEntity", "PointOfInterestId", "PointOfInterestId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("PointOfInterestVenueCategoryEntity", "VenueCategoryId", "VenueCategoryId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("PointOfInterestVenueCategoryEntity", "CreatedBy", "CreatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("PointOfInterestVenueCategoryEntity", "UpdatedBy", "UpdatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("PointOfInterestVenueCategoryEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 5);
			this.AddElementFieldMapping("PointOfInterestVenueCategoryEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 6);
		}

		/// <summary>Inits PosalterationEntity's mappings</summary>
		private void InitPosalterationEntityMappings()
		{
			this.AddElementMapping("PosalterationEntity", @"Obymobi", @"dbo", "Posalteration", 24, 0);
			this.AddElementFieldMapping("PosalterationEntity", "PosalterationId", "PosalterationId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("PosalterationEntity", "CompanyId", "CompanyId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("PosalterationEntity", "ExternalId", "ExternalId", false, "VarChar", 255, 0, 0, false, "", null, typeof(System.String), 2);
			this.AddElementFieldMapping("PosalterationEntity", "Name", "Name", true, "NVarChar", 100, 0, 0, false, "", null, typeof(System.String), 3);
			this.AddElementFieldMapping("PosalterationEntity", "MinOptions", "MinOptions", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("PosalterationEntity", "MaxOptions", "MaxOptions", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 5);
			this.AddElementFieldMapping("PosalterationEntity", "FieldValue1", "FieldValue1", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 6);
			this.AddElementFieldMapping("PosalterationEntity", "FieldValue2", "FieldValue2", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 7);
			this.AddElementFieldMapping("PosalterationEntity", "FieldValue3", "FieldValue3", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 8);
			this.AddElementFieldMapping("PosalterationEntity", "FieldValue4", "FieldValue4", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 9);
			this.AddElementFieldMapping("PosalterationEntity", "FieldValue5", "FieldValue5", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 10);
			this.AddElementFieldMapping("PosalterationEntity", "FieldValue6", "FieldValue6", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 11);
			this.AddElementFieldMapping("PosalterationEntity", "FieldValue7", "FieldValue7", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 12);
			this.AddElementFieldMapping("PosalterationEntity", "FieldValue8", "FieldValue8", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 13);
			this.AddElementFieldMapping("PosalterationEntity", "FieldValue9", "FieldValue9", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 14);
			this.AddElementFieldMapping("PosalterationEntity", "FieldValue10", "FieldValue10", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 15);
			this.AddElementFieldMapping("PosalterationEntity", "CreatedInBatchId", "CreatedInBatchId", true, "BigInt", 0, 19, 0, false, "", null, typeof(System.Int64), 16);
			this.AddElementFieldMapping("PosalterationEntity", "UpdatedInBatchId", "UpdatedInBatchId", true, "BigInt", 0, 19, 0, false, "", null, typeof(System.Int64), 17);
			this.AddElementFieldMapping("PosalterationEntity", "SynchronisationBatchId", "SynchronisationBatchId", true, "BigInt", 0, 19, 0, false, "", null, typeof(System.Int64), 18);
			this.AddElementFieldMapping("PosalterationEntity", "CreatedBy", "CreatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 19);
			this.AddElementFieldMapping("PosalterationEntity", "UpdatedBy", "UpdatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 20);
			this.AddElementFieldMapping("PosalterationEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 21);
			this.AddElementFieldMapping("PosalterationEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 22);
			this.AddElementFieldMapping("PosalterationEntity", "RevenueCenter", "RevenueCenter", true, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 23);
		}

		/// <summary>Inits PosalterationitemEntity's mappings</summary>
		private void InitPosalterationitemEntityMappings()
		{
			this.AddElementMapping("PosalterationitemEntity", @"Obymobi", @"dbo", "Posalterationitem", 26, 0);
			this.AddElementFieldMapping("PosalterationitemEntity", "PosalterationitemId", "PosalterationitemId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("PosalterationitemEntity", "CompanyId", "CompanyId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("PosalterationitemEntity", "ExternalPosalterationId", "ExternalPosalterationId", true, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 2);
			this.AddElementFieldMapping("PosalterationitemEntity", "ExternalPosalterationoptionId", "ExternalPosalterationoptionId", true, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 3);
			this.AddElementFieldMapping("PosalterationitemEntity", "ExternalId", "ExternalId", true, "VarChar", 255, 0, 0, false, "", null, typeof(System.String), 4);
			this.AddElementFieldMapping("PosalterationitemEntity", "SelectedOnDefault", "SelectedOnDefault", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 5);
			this.AddElementFieldMapping("PosalterationitemEntity", "SortOrder", "SortOrder", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 6);
			this.AddElementFieldMapping("PosalterationitemEntity", "FieldValue1", "FieldValue1", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 7);
			this.AddElementFieldMapping("PosalterationitemEntity", "FieldValue2", "FieldValue2", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 8);
			this.AddElementFieldMapping("PosalterationitemEntity", "FieldValue3", "FieldValue3", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 9);
			this.AddElementFieldMapping("PosalterationitemEntity", "FieldValue4", "FieldValue4", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 10);
			this.AddElementFieldMapping("PosalterationitemEntity", "FieldValue5", "FieldValue5", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 11);
			this.AddElementFieldMapping("PosalterationitemEntity", "FieldValue6", "FieldValue6", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 12);
			this.AddElementFieldMapping("PosalterationitemEntity", "FieldValue7", "FieldValue7", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 13);
			this.AddElementFieldMapping("PosalterationitemEntity", "FieldValue8", "FieldValue8", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 14);
			this.AddElementFieldMapping("PosalterationitemEntity", "FieldValue9", "FieldValue9", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 15);
			this.AddElementFieldMapping("PosalterationitemEntity", "FieldValue10", "FieldValue10", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 16);
			this.AddElementFieldMapping("PosalterationitemEntity", "DeletedFromCms", "DeletedFromCms", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 17);
			this.AddElementFieldMapping("PosalterationitemEntity", "CreatedInBatchId", "CreatedInBatchId", true, "BigInt", 0, 19, 0, false, "", null, typeof(System.Int64), 18);
			this.AddElementFieldMapping("PosalterationitemEntity", "UpdatedInBatchId", "UpdatedInBatchId", true, "BigInt", 0, 19, 0, false, "", null, typeof(System.Int64), 19);
			this.AddElementFieldMapping("PosalterationitemEntity", "SynchronisationBatchId", "SynchronisationBatchId", true, "BigInt", 0, 19, 0, false, "", null, typeof(System.Int64), 20);
			this.AddElementFieldMapping("PosalterationitemEntity", "CreatedBy", "CreatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 21);
			this.AddElementFieldMapping("PosalterationitemEntity", "UpdatedBy", "UpdatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 22);
			this.AddElementFieldMapping("PosalterationitemEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 23);
			this.AddElementFieldMapping("PosalterationitemEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 24);
			this.AddElementFieldMapping("PosalterationitemEntity", "RevenueCenter", "RevenueCenter", true, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 25);
		}

		/// <summary>Inits PosalterationoptionEntity's mappings</summary>
		private void InitPosalterationoptionEntityMappings()
		{
			this.AddElementMapping("PosalterationoptionEntity", @"Obymobi", @"dbo", "Posalterationoption", 26, 0);
			this.AddElementFieldMapping("PosalterationoptionEntity", "PosalterationoptionId", "PosalterationoptionId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("PosalterationoptionEntity", "CompanyId", "CompanyId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("PosalterationoptionEntity", "ExternalId", "ExternalId", false, "VarChar", 255, 0, 0, false, "", null, typeof(System.String), 2);
			this.AddElementFieldMapping("PosalterationoptionEntity", "Name", "Name", true, "NVarChar", 100, 0, 0, false, "", null, typeof(System.String), 3);
			this.AddElementFieldMapping("PosalterationoptionEntity", "PriceAddition", "PriceAddition", true, "Money", 0, 19, 4, false, "", null, typeof(System.Decimal), 4);
			this.AddElementFieldMapping("PosalterationoptionEntity", "PriceIn", "PriceIn", false, "Money", 0, 19, 4, false, "", null, typeof(System.Decimal), 5);
			this.AddElementFieldMapping("PosalterationoptionEntity", "Description", "Description", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 6);
			this.AddElementFieldMapping("PosalterationoptionEntity", "PosproductExternalId", "PosproductExternalId", true, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 7);
			this.AddElementFieldMapping("PosalterationoptionEntity", "FieldValue1", "FieldValue1", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 8);
			this.AddElementFieldMapping("PosalterationoptionEntity", "FieldValue2", "FieldValue2", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 9);
			this.AddElementFieldMapping("PosalterationoptionEntity", "FieldValue3", "FieldValue3", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 10);
			this.AddElementFieldMapping("PosalterationoptionEntity", "FieldValue4", "FieldValue4", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 11);
			this.AddElementFieldMapping("PosalterationoptionEntity", "FieldValue5", "FieldValue5", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 12);
			this.AddElementFieldMapping("PosalterationoptionEntity", "FieldValue6", "FieldValue6", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 13);
			this.AddElementFieldMapping("PosalterationoptionEntity", "FieldValue7", "FieldValue7", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 14);
			this.AddElementFieldMapping("PosalterationoptionEntity", "FieldValue8", "FieldValue8", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 15);
			this.AddElementFieldMapping("PosalterationoptionEntity", "FieldValue9", "FieldValue9", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 16);
			this.AddElementFieldMapping("PosalterationoptionEntity", "FieldValue10", "FieldValue10", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 17);
			this.AddElementFieldMapping("PosalterationoptionEntity", "CreatedBy", "CreatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 18);
			this.AddElementFieldMapping("PosalterationoptionEntity", "UpdatedBy", "UpdatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 19);
			this.AddElementFieldMapping("PosalterationoptionEntity", "CreatedInBatchId", "CreatedInBatchId", true, "BigInt", 0, 19, 0, false, "", null, typeof(System.Int64), 20);
			this.AddElementFieldMapping("PosalterationoptionEntity", "UpdatedInBatchId", "UpdatedInBatchId", true, "BigInt", 0, 19, 0, false, "", null, typeof(System.Int64), 21);
			this.AddElementFieldMapping("PosalterationoptionEntity", "SynchronisationBatchId", "SynchronisationBatchId", true, "BigInt", 0, 19, 0, false, "", null, typeof(System.Int64), 22);
			this.AddElementFieldMapping("PosalterationoptionEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 23);
			this.AddElementFieldMapping("PosalterationoptionEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 24);
			this.AddElementFieldMapping("PosalterationoptionEntity", "RevenueCenter", "RevenueCenter", true, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 25);
		}

		/// <summary>Inits PoscategoryEntity's mappings</summary>
		private void InitPoscategoryEntityMappings()
		{
			this.AddElementMapping("PoscategoryEntity", @"Obymobi", @"dbo", "Poscategory", 22, 0);
			this.AddElementFieldMapping("PoscategoryEntity", "PoscategoryId", "PoscategoryId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("PoscategoryEntity", "CompanyId", "CompanyId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("PoscategoryEntity", "ExternalId", "ExternalId", false, "VarChar", 50, 0, 0, false, "", null, typeof(System.String), 2);
			this.AddElementFieldMapping("PoscategoryEntity", "Name", "Name", true, "NVarChar", 100, 0, 0, false, "", null, typeof(System.String), 3);
			this.AddElementFieldMapping("PoscategoryEntity", "FieldValue1", "FieldValue1", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 4);
			this.AddElementFieldMapping("PoscategoryEntity", "FieldValue2", "FieldValue2", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 5);
			this.AddElementFieldMapping("PoscategoryEntity", "FieldValue3", "FieldValue3", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 6);
			this.AddElementFieldMapping("PoscategoryEntity", "FieldValue4", "FieldValue4", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 7);
			this.AddElementFieldMapping("PoscategoryEntity", "FieldValue5", "FieldValue5", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 8);
			this.AddElementFieldMapping("PoscategoryEntity", "FieldValue6", "FieldValue6", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 9);
			this.AddElementFieldMapping("PoscategoryEntity", "FieldValue7", "FieldValue7", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 10);
			this.AddElementFieldMapping("PoscategoryEntity", "FieldValue8", "FieldValue8", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 11);
			this.AddElementFieldMapping("PoscategoryEntity", "FieldValue9", "FieldValue9", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 12);
			this.AddElementFieldMapping("PoscategoryEntity", "FieldValue10", "FieldValue10", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 13);
			this.AddElementFieldMapping("PoscategoryEntity", "CreatedInBatchId", "CreatedInBatchId", true, "BigInt", 0, 19, 0, false, "", null, typeof(System.Int64), 14);
			this.AddElementFieldMapping("PoscategoryEntity", "UpdatedInBatchId", "UpdatedInBatchId", true, "BigInt", 0, 19, 0, false, "", null, typeof(System.Int64), 15);
			this.AddElementFieldMapping("PoscategoryEntity", "SynchronisationBatchId", "SynchronisationBatchId", true, "BigInt", 0, 19, 0, false, "", null, typeof(System.Int64), 16);
			this.AddElementFieldMapping("PoscategoryEntity", "CreatedBy", "CreatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 17);
			this.AddElementFieldMapping("PoscategoryEntity", "UpdatedBy", "UpdatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 18);
			this.AddElementFieldMapping("PoscategoryEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 19);
			this.AddElementFieldMapping("PoscategoryEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 20);
			this.AddElementFieldMapping("PoscategoryEntity", "RevenueCenter", "RevenueCenter", true, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 21);
		}

		/// <summary>Inits PosdeliverypointEntity's mappings</summary>
		private void InitPosdeliverypointEntityMappings()
		{
			this.AddElementMapping("PosdeliverypointEntity", @"Obymobi", @"dbo", "Posdeliverypoint", 23, 0);
			this.AddElementFieldMapping("PosdeliverypointEntity", "PosdeliverypointId", "PosdeliverypointId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("PosdeliverypointEntity", "CompanyId", "CompanyId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("PosdeliverypointEntity", "ExternalId", "ExternalId", false, "VarChar", 50, 0, 0, false, "", null, typeof(System.String), 2);
			this.AddElementFieldMapping("PosdeliverypointEntity", "ExternalPosdeliverypointgroupId", "ExternalPosdeliverypointgroupId", true, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 3);
			this.AddElementFieldMapping("PosdeliverypointEntity", "Name", "Name", true, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 4);
			this.AddElementFieldMapping("PosdeliverypointEntity", "FieldValue1", "FieldValue1", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 5);
			this.AddElementFieldMapping("PosdeliverypointEntity", "FieldValue2", "FieldValue2", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 6);
			this.AddElementFieldMapping("PosdeliverypointEntity", "FieldValue3", "FieldValue3", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 7);
			this.AddElementFieldMapping("PosdeliverypointEntity", "FieldValue4", "FieldValue4", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 8);
			this.AddElementFieldMapping("PosdeliverypointEntity", "FieldValue5", "FieldValue5", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 9);
			this.AddElementFieldMapping("PosdeliverypointEntity", "FieldValue6", "FieldValue6", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 10);
			this.AddElementFieldMapping("PosdeliverypointEntity", "FieldValue7", "FieldValue7", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 11);
			this.AddElementFieldMapping("PosdeliverypointEntity", "FieldValue8", "FieldValue8", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 12);
			this.AddElementFieldMapping("PosdeliverypointEntity", "FieldValue9", "FieldValue9", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 13);
			this.AddElementFieldMapping("PosdeliverypointEntity", "FieldValue10", "FieldValue10", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 14);
			this.AddElementFieldMapping("PosdeliverypointEntity", "CreatedInBatchId", "CreatedInBatchId", true, "BigInt", 0, 19, 0, false, "", null, typeof(System.Int64), 15);
			this.AddElementFieldMapping("PosdeliverypointEntity", "UpdatedInBatchId", "UpdatedInBatchId", true, "BigInt", 0, 19, 0, false, "", null, typeof(System.Int64), 16);
			this.AddElementFieldMapping("PosdeliverypointEntity", "SynchronisationBatchId", "SynchronisationBatchId", true, "BigInt", 0, 19, 0, false, "", null, typeof(System.Int64), 17);
			this.AddElementFieldMapping("PosdeliverypointEntity", "CreatedBy", "CreatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 18);
			this.AddElementFieldMapping("PosdeliverypointEntity", "UpdatedBy", "UpdatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 19);
			this.AddElementFieldMapping("PosdeliverypointEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 20);
			this.AddElementFieldMapping("PosdeliverypointEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 21);
			this.AddElementFieldMapping("PosdeliverypointEntity", "RevenueCenter", "RevenueCenter", true, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 22);
		}

		/// <summary>Inits PosdeliverypointgroupEntity's mappings</summary>
		private void InitPosdeliverypointgroupEntityMappings()
		{
			this.AddElementMapping("PosdeliverypointgroupEntity", @"Obymobi", @"dbo", "Posdeliverypointgroup", 22, 0);
			this.AddElementFieldMapping("PosdeliverypointgroupEntity", "PosdeliverypointgroupId", "PosdeliverypointgroupId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("PosdeliverypointgroupEntity", "CompanyId", "CompanyId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("PosdeliverypointgroupEntity", "ExternalId", "ExternalId", false, "VarChar", 50, 0, 0, false, "", null, typeof(System.String), 2);
			this.AddElementFieldMapping("PosdeliverypointgroupEntity", "Name", "Name", true, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 3);
			this.AddElementFieldMapping("PosdeliverypointgroupEntity", "FieldValue1", "FieldValue1", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 4);
			this.AddElementFieldMapping("PosdeliverypointgroupEntity", "FieldValue2", "FieldValue2", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 5);
			this.AddElementFieldMapping("PosdeliverypointgroupEntity", "FieldValue3", "FieldValue3", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 6);
			this.AddElementFieldMapping("PosdeliverypointgroupEntity", "FieldValue4", "FieldValue4", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 7);
			this.AddElementFieldMapping("PosdeliverypointgroupEntity", "FieldValue5", "FieldValue5", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 8);
			this.AddElementFieldMapping("PosdeliverypointgroupEntity", "FieldValue6", "FieldValue6", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 9);
			this.AddElementFieldMapping("PosdeliverypointgroupEntity", "FieldValue7", "FieldValue7", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 10);
			this.AddElementFieldMapping("PosdeliverypointgroupEntity", "FieldValue8", "FieldValue8", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 11);
			this.AddElementFieldMapping("PosdeliverypointgroupEntity", "FieldValue9", "FieldValue9", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 12);
			this.AddElementFieldMapping("PosdeliverypointgroupEntity", "FieldValue10", "FieldValue10", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 13);
			this.AddElementFieldMapping("PosdeliverypointgroupEntity", "CreatedInBatchId", "CreatedInBatchId", true, "BigInt", 0, 19, 0, false, "", null, typeof(System.Int64), 14);
			this.AddElementFieldMapping("PosdeliverypointgroupEntity", "UpdatedInBatchId", "UpdatedInBatchId", true, "BigInt", 0, 19, 0, false, "", null, typeof(System.Int64), 15);
			this.AddElementFieldMapping("PosdeliverypointgroupEntity", "SynchronisationBatchId", "SynchronisationBatchId", true, "BigInt", 0, 19, 0, false, "", null, typeof(System.Int64), 16);
			this.AddElementFieldMapping("PosdeliverypointgroupEntity", "CreatedBy", "CreatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 17);
			this.AddElementFieldMapping("PosdeliverypointgroupEntity", "UpdatedBy", "UpdatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 18);
			this.AddElementFieldMapping("PosdeliverypointgroupEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 19);
			this.AddElementFieldMapping("PosdeliverypointgroupEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 20);
			this.AddElementFieldMapping("PosdeliverypointgroupEntity", "RevenueCenter", "RevenueCenter", true, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 21);
		}

		/// <summary>Inits PospaymentmethodEntity's mappings</summary>
		private void InitPospaymentmethodEntityMappings()
		{
			this.AddElementMapping("PospaymentmethodEntity", @"Obymobi", @"dbo", "Pospaymentmethod", 22, 0);
			this.AddElementFieldMapping("PospaymentmethodEntity", "PospaymentmethodId", "PospaymentmethodId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("PospaymentmethodEntity", "CompanyId", "CompanyId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("PospaymentmethodEntity", "ExternalId", "ExternalId", false, "VarChar", 50, 0, 0, false, "", null, typeof(System.String), 2);
			this.AddElementFieldMapping("PospaymentmethodEntity", "Name", "Name", true, "NVarChar", 100, 0, 0, false, "", null, typeof(System.String), 3);
			this.AddElementFieldMapping("PospaymentmethodEntity", "FieldValue1", "FieldValue1", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 4);
			this.AddElementFieldMapping("PospaymentmethodEntity", "FieldValue2", "FieldValue2", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 5);
			this.AddElementFieldMapping("PospaymentmethodEntity", "FieldValue3", "FieldValue3", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 6);
			this.AddElementFieldMapping("PospaymentmethodEntity", "FieldValue4", "FieldValue4", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 7);
			this.AddElementFieldMapping("PospaymentmethodEntity", "FieldValue5", "FieldValue5", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 8);
			this.AddElementFieldMapping("PospaymentmethodEntity", "FieldValue6", "FieldValue6", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 9);
			this.AddElementFieldMapping("PospaymentmethodEntity", "FieldValue7", "FieldValue7", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 10);
			this.AddElementFieldMapping("PospaymentmethodEntity", "FieldValue8", "FieldValue8", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 11);
			this.AddElementFieldMapping("PospaymentmethodEntity", "FieldValue9", "FieldValue9", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 12);
			this.AddElementFieldMapping("PospaymentmethodEntity", "FieldValue10", "FieldValue10", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 13);
			this.AddElementFieldMapping("PospaymentmethodEntity", "CreatedInBatchId", "CreatedInBatchId", true, "BigInt", 0, 19, 0, false, "", null, typeof(System.Int64), 14);
			this.AddElementFieldMapping("PospaymentmethodEntity", "UpdatedInBatchId", "UpdatedInBatchId", true, "BigInt", 0, 19, 0, false, "", null, typeof(System.Int64), 15);
			this.AddElementFieldMapping("PospaymentmethodEntity", "SynchronisationBatchId", "SynchronisationBatchId", true, "BigInt", 0, 19, 0, false, "", null, typeof(System.Int64), 16);
			this.AddElementFieldMapping("PospaymentmethodEntity", "CreatedBy", "CreatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 17);
			this.AddElementFieldMapping("PospaymentmethodEntity", "UpdatedBy", "UpdatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 18);
			this.AddElementFieldMapping("PospaymentmethodEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 19);
			this.AddElementFieldMapping("PospaymentmethodEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 20);
			this.AddElementFieldMapping("PospaymentmethodEntity", "RevenueCenter", "RevenueCenter", true, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 21);
		}

		/// <summary>Inits PosproductEntity's mappings</summary>
		private void InitPosproductEntityMappings()
		{
			this.AddElementMapping("PosproductEntity", @"Obymobi", @"dbo", "Posproduct", 26, 0);
			this.AddElementFieldMapping("PosproductEntity", "PosproductId", "PosproductId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("PosproductEntity", "CompanyId", "CompanyId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("PosproductEntity", "ExternalId", "ExternalId", false, "VarChar", 50, 0, 0, false, "", null, typeof(System.String), 2);
			this.AddElementFieldMapping("PosproductEntity", "ExternalPoscategoryId", "ExternalPoscategoryId", true, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 3);
			this.AddElementFieldMapping("PosproductEntity", "Name", "Name", true, "NVarChar", 100, 0, 0, false, "", null, typeof(System.String), 4);
			this.AddElementFieldMapping("PosproductEntity", "PriceIn", "PriceIn", false, "Money", 0, 19, 4, false, "", null, typeof(System.Decimal), 5);
			this.AddElementFieldMapping("PosproductEntity", "VatTariff", "VatTariff", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 6);
			this.AddElementFieldMapping("PosproductEntity", "Visible", "Visible", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 7);
			this.AddElementFieldMapping("PosproductEntity", "FieldValue1", "FieldValue1", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 8);
			this.AddElementFieldMapping("PosproductEntity", "FieldValue2", "FieldValue2", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 9);
			this.AddElementFieldMapping("PosproductEntity", "FieldValue3", "FieldValue3", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 10);
			this.AddElementFieldMapping("PosproductEntity", "FieldValue4", "FieldValue4", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 11);
			this.AddElementFieldMapping("PosproductEntity", "FieldValue5", "FieldValue5", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 12);
			this.AddElementFieldMapping("PosproductEntity", "FieldValue6", "FieldValue6", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 13);
			this.AddElementFieldMapping("PosproductEntity", "FieldValue7", "FieldValue7", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 14);
			this.AddElementFieldMapping("PosproductEntity", "FieldValue8", "FieldValue8", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 15);
			this.AddElementFieldMapping("PosproductEntity", "FieldValue9", "FieldValue9", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 16);
			this.AddElementFieldMapping("PosproductEntity", "FieldValue10", "FieldValue10", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 17);
			this.AddElementFieldMapping("PosproductEntity", "CreatedInBatchId", "CreatedInBatchId", true, "BigInt", 0, 19, 0, false, "", null, typeof(System.Int64), 18);
			this.AddElementFieldMapping("PosproductEntity", "UpdatedInBatchId", "UpdatedInBatchId", true, "BigInt", 0, 19, 0, false, "", null, typeof(System.Int64), 19);
			this.AddElementFieldMapping("PosproductEntity", "SynchronisationBatchId", "SynchronisationBatchId", true, "BigInt", 0, 19, 0, false, "", null, typeof(System.Int64), 20);
			this.AddElementFieldMapping("PosproductEntity", "CreatedBy", "CreatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 21);
			this.AddElementFieldMapping("PosproductEntity", "UpdatedBy", "UpdatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 22);
			this.AddElementFieldMapping("PosproductEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 23);
			this.AddElementFieldMapping("PosproductEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 24);
			this.AddElementFieldMapping("PosproductEntity", "RevenueCenter", "RevenueCenter", true, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 25);
		}

		/// <summary>Inits PosproductPosalterationEntity's mappings</summary>
		private void InitPosproductPosalterationEntityMappings()
		{
			this.AddElementMapping("PosproductPosalterationEntity", @"Obymobi", @"dbo", "PosproductPosalteration", 13, 0);
			this.AddElementFieldMapping("PosproductPosalterationEntity", "PosproductPosalterationId", "PosproductPosalterationId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("PosproductPosalterationEntity", "CompanyId", "CompanyId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("PosproductPosalterationEntity", "PosproductExternalId", "PosproductExternalId", true, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 2);
			this.AddElementFieldMapping("PosproductPosalterationEntity", "PosalterationExternalId", "PosalterationExternalId", true, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 3);
			this.AddElementFieldMapping("PosproductPosalterationEntity", "ExternalId", "ExternalId", true, "VarChar", 255, 0, 0, false, "", null, typeof(System.String), 4);
			this.AddElementFieldMapping("PosproductPosalterationEntity", "DeletedFromCms", "DeletedFromCms", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 5);
			this.AddElementFieldMapping("PosproductPosalterationEntity", "CreatedInBatchId", "CreatedInBatchId", true, "BigInt", 0, 19, 0, false, "", null, typeof(System.Int64), 6);
			this.AddElementFieldMapping("PosproductPosalterationEntity", "UpdatedInBatchId", "UpdatedInBatchId", true, "BigInt", 0, 19, 0, false, "", null, typeof(System.Int64), 7);
			this.AddElementFieldMapping("PosproductPosalterationEntity", "SynchronisationBatchId", "SynchronisationBatchId", true, "BigInt", 0, 19, 0, false, "", null, typeof(System.Int64), 8);
			this.AddElementFieldMapping("PosproductPosalterationEntity", "CreatedBy", "CreatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 9);
			this.AddElementFieldMapping("PosproductPosalterationEntity", "UpdatedBy", "UpdatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 10);
			this.AddElementFieldMapping("PosproductPosalterationEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 11);
			this.AddElementFieldMapping("PosproductPosalterationEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 12);
		}

		/// <summary>Inits PriceLevelEntity's mappings</summary>
		private void InitPriceLevelEntityMappings()
		{
			this.AddElementMapping("PriceLevelEntity", @"Obymobi", @"dbo", "PriceLevel", 7, 0);
			this.AddElementFieldMapping("PriceLevelEntity", "PriceLevelId", "PriceLevelId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("PriceLevelEntity", "CompanyId", "CompanyId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("PriceLevelEntity", "Name", "Name", false, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 2);
			this.AddElementFieldMapping("PriceLevelEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 3);
			this.AddElementFieldMapping("PriceLevelEntity", "CreatedBy", "CreatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("PriceLevelEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 5);
			this.AddElementFieldMapping("PriceLevelEntity", "UpdatedBy", "UpdatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 6);
		}

		/// <summary>Inits PriceLevelItemEntity's mappings</summary>
		private void InitPriceLevelItemEntityMappings()
		{
			this.AddElementMapping("PriceLevelItemEntity", @"Obymobi", @"dbo", "PriceLevelItem", 11, 0);
			this.AddElementFieldMapping("PriceLevelItemEntity", "PriceLevelItemId", "PriceLevelItemId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("PriceLevelItemEntity", "ParentCompanyId", "ParentCompanyId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("PriceLevelItemEntity", "PriceLevelId", "PriceLevelId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("PriceLevelItemEntity", "ProductId", "ProductId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("PriceLevelItemEntity", "AlterationId", "AlterationId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("PriceLevelItemEntity", "AlterationoptionId", "AlterationoptionId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 5);
			this.AddElementFieldMapping("PriceLevelItemEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 6);
			this.AddElementFieldMapping("PriceLevelItemEntity", "CreatedBy", "CreatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 7);
			this.AddElementFieldMapping("PriceLevelItemEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 8);
			this.AddElementFieldMapping("PriceLevelItemEntity", "UpdatedBy", "UpdatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 9);
			this.AddElementFieldMapping("PriceLevelItemEntity", "Price", "Price", false, "Money", 0, 19, 4, false, "", null, typeof(System.Decimal), 10);
		}

		/// <summary>Inits PriceScheduleEntity's mappings</summary>
		private void InitPriceScheduleEntityMappings()
		{
			this.AddElementMapping("PriceScheduleEntity", @"Obymobi", @"dbo", "PriceSchedule", 7, 0);
			this.AddElementFieldMapping("PriceScheduleEntity", "PriceScheduleId", "PriceScheduleId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("PriceScheduleEntity", "CompanyId", "CompanyId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("PriceScheduleEntity", "Name", "Name", false, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 2);
			this.AddElementFieldMapping("PriceScheduleEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 3);
			this.AddElementFieldMapping("PriceScheduleEntity", "CreatedBy", "CreatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("PriceScheduleEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 5);
			this.AddElementFieldMapping("PriceScheduleEntity", "UpdatedBy", "UpdatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 6);
		}

		/// <summary>Inits PriceScheduleItemEntity's mappings</summary>
		private void InitPriceScheduleItemEntityMappings()
		{
			this.AddElementMapping("PriceScheduleItemEntity", @"Obymobi", @"dbo", "PriceScheduleItem", 8, 0);
			this.AddElementFieldMapping("PriceScheduleItemEntity", "PriceScheduleItemId", "PriceScheduleItemId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("PriceScheduleItemEntity", "ParentCompanyId", "ParentCompanyId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("PriceScheduleItemEntity", "PriceScheduleId", "PriceScheduleId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("PriceScheduleItemEntity", "PriceLevelId", "PriceLevelId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("PriceScheduleItemEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 4);
			this.AddElementFieldMapping("PriceScheduleItemEntity", "CreatedBy", "CreatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 5);
			this.AddElementFieldMapping("PriceScheduleItemEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 6);
			this.AddElementFieldMapping("PriceScheduleItemEntity", "UpdatedBy", "UpdatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 7);
		}

		/// <summary>Inits PriceScheduleItemOccurrenceEntity's mappings</summary>
		private void InitPriceScheduleItemOccurrenceEntityMappings()
		{
			this.AddElementMapping("PriceScheduleItemOccurrenceEntity", @"Obymobi", @"dbo", "PriceScheduleItemOccurrence", 26, 0);
			this.AddElementFieldMapping("PriceScheduleItemOccurrenceEntity", "PriceScheduleItemOccurrenceId", "PriceScheduleItemOccurrenceId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("PriceScheduleItemOccurrenceEntity", "ParentCompanyId", "ParentCompanyId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("PriceScheduleItemOccurrenceEntity", "PriceScheduleItemId", "PriceScheduleItemId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("PriceScheduleItemOccurrenceEntity", "StartTime", "StartTime", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 3);
			this.AddElementFieldMapping("PriceScheduleItemOccurrenceEntity", "EndTime", "EndTime", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 4);
			this.AddElementFieldMapping("PriceScheduleItemOccurrenceEntity", "Recurring", "Recurring", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 5);
			this.AddElementFieldMapping("PriceScheduleItemOccurrenceEntity", "RecurrenceType", "RecurrenceType", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 6);
			this.AddElementFieldMapping("PriceScheduleItemOccurrenceEntity", "RecurrenceRange", "RecurrenceRange", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 7);
			this.AddElementFieldMapping("PriceScheduleItemOccurrenceEntity", "RecurrenceStart", "RecurrenceStart", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 8);
			this.AddElementFieldMapping("PriceScheduleItemOccurrenceEntity", "RecurrenceEnd", "RecurrenceEnd", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 9);
			this.AddElementFieldMapping("PriceScheduleItemOccurrenceEntity", "RecurrenceOccurrenceCount", "RecurrenceOccurrenceCount", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 10);
			this.AddElementFieldMapping("PriceScheduleItemOccurrenceEntity", "RecurrencePeriodicity", "RecurrencePeriodicity", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 11);
			this.AddElementFieldMapping("PriceScheduleItemOccurrenceEntity", "RecurrenceDayNumber", "RecurrenceDayNumber", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 12);
			this.AddElementFieldMapping("PriceScheduleItemOccurrenceEntity", "RecurrenceWeekDays", "RecurrenceWeekDays", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 13);
			this.AddElementFieldMapping("PriceScheduleItemOccurrenceEntity", "RecurrenceWeekOfMonth", "RecurrenceWeekOfMonth", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 14);
			this.AddElementFieldMapping("PriceScheduleItemOccurrenceEntity", "RecurrenceMonth", "RecurrenceMonth", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 15);
			this.AddElementFieldMapping("PriceScheduleItemOccurrenceEntity", "BackgroundColor", "BackgroundColor", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 16);
			this.AddElementFieldMapping("PriceScheduleItemOccurrenceEntity", "TextColor", "TextColor", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 17);
			this.AddElementFieldMapping("PriceScheduleItemOccurrenceEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 18);
			this.AddElementFieldMapping("PriceScheduleItemOccurrenceEntity", "CreatedBy", "CreatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 19);
			this.AddElementFieldMapping("PriceScheduleItemOccurrenceEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 20);
			this.AddElementFieldMapping("PriceScheduleItemOccurrenceEntity", "UpdatedBy", "UpdatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 21);
			this.AddElementFieldMapping("PriceScheduleItemOccurrenceEntity", "StartTimeUTC", "StartTimeUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 22);
			this.AddElementFieldMapping("PriceScheduleItemOccurrenceEntity", "EndTimeUTC", "EndTimeUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 23);
			this.AddElementFieldMapping("PriceScheduleItemOccurrenceEntity", "RecurrenceStartUTC", "RecurrenceStartUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 24);
			this.AddElementFieldMapping("PriceScheduleItemOccurrenceEntity", "RecurrenceEndUTC", "RecurrenceEndUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 25);
		}

		/// <summary>Inits ProductEntity's mappings</summary>
		private void InitProductEntityMappings()
		{
			this.AddElementMapping("ProductEntity", @"Obymobi", @"dbo", "Product", 71, 0);
			this.AddElementFieldMapping("ProductEntity", "ProductId", "ProductId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("ProductEntity", "CompanyId", "CompanyId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("ProductEntity", "GenericproductId", "GenericproductId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("ProductEntity", "Name", "Name", false, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 3);
			this.AddElementFieldMapping("ProductEntity", "Description", "Description", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 4);
			this.AddElementFieldMapping("ProductEntity", "Type", "Type", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 5);
			this.AddElementFieldMapping("ProductEntity", "PriceIn", "PriceIn", true, "Money", 0, 19, 4, false, "", null, typeof(System.Decimal), 6);
			this.AddElementFieldMapping("ProductEntity", "VattariffId", "VattariffId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 7);
			this.AddElementFieldMapping("ProductEntity", "SortOrder", "SortOrder", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 8);
			this.AddElementFieldMapping("ProductEntity", "VattariffPercentage", "VattariffPercentage", false, "Float", 0, 38, 0, false, "", null, typeof(System.Double), 9);
			this.AddElementFieldMapping("ProductEntity", "PosproductId", "PosproductId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 10);
			this.AddElementFieldMapping("ProductEntity", "TextColor", "TextColor", true, "NVarChar", 15, 0, 0, false, "", null, typeof(System.String), 11);
			this.AddElementFieldMapping("ProductEntity", "BackgroundColor", "BackgroundColor", true, "NVarChar", 15, 0, 0, false, "", null, typeof(System.String), 12);
			this.AddElementFieldMapping("ProductEntity", "DisplayOnHomepage", "DisplayOnHomepage", true, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 13);
			this.AddElementFieldMapping("ProductEntity", "Visible", "AvailableOnOtoucho", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 14);
			this.AddElementFieldMapping("ProductEntity", "AvailableOnObymobi", "AvailableOnObymobi", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 15);
			this.AddElementFieldMapping("ProductEntity", "SoldOut", "SoldOut", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 16);
			this.AddElementFieldMapping("ProductEntity", "Rateable", "Rateable", true, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 17);
			this.AddElementFieldMapping("ProductEntity", "AllowFreeText", "AllowFreeText", true, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 18);
			this.AddElementFieldMapping("ProductEntity", "CreatedBy", "CreatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 19);
			this.AddElementFieldMapping("ProductEntity", "RouteId", "RouteId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 20);
			this.AddElementFieldMapping("ProductEntity", "UpdatedBy", "UpdatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 21);
			this.AddElementFieldMapping("ProductEntity", "AnnouncementAction", "AnnouncementAction", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 22);
			this.AddElementFieldMapping("ProductEntity", "Color", "Color", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 23);
			this.AddElementFieldMapping("ProductEntity", "SubType", "SubType", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 24);
			this.AddElementFieldMapping("ProductEntity", "ButtonText", "ButtonText", true, "NVarChar", 100, 0, 0, false, "", null, typeof(System.String), 25);
			this.AddElementFieldMapping("ProductEntity", "ManualDescriptionEnabled", "ManualDescriptionEnabled", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 26);
			this.AddElementFieldMapping("ProductEntity", "WebTypeSmartphoneUrl", "WebTypeSmartphoneUrl", true, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 27);
			this.AddElementFieldMapping("ProductEntity", "Geofencing", "Geofencing", true, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 28);
			this.AddElementFieldMapping("ProductEntity", "WebTypeTabletUrl", "WebTypeTabletUrl", true, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 29);
			this.AddElementFieldMapping("ProductEntity", "HotSOSIssueId", "HotSOSIssueId", true, "VarChar", 50, 0, 0, false, "", null, typeof(System.String), 30);
			this.AddElementFieldMapping("ProductEntity", "ScheduleId", "ScheduleId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 31);
			this.AddElementFieldMapping("ProductEntity", "ViewLayoutType", "ViewLayoutType", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 32);
			this.AddElementFieldMapping("ProductEntity", "DeliveryLocationType", "DeliveryLocationType", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 33);
			this.AddElementFieldMapping("ProductEntity", "SupportNotificationType", "SupportNotificationType", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 34);
			this.AddElementFieldMapping("ProductEntity", "HidePrice", "HidePrice", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 35);
			this.AddElementFieldMapping("ProductEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 36);
			this.AddElementFieldMapping("ProductEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 37);
			this.AddElementFieldMapping("ProductEntity", "GenericProductChangedUTC", "GenericProductChangedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 38);
			this.AddElementFieldMapping("ProductEntity", "VisibilityType", "VisibilityType", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 39);
			this.AddElementFieldMapping("ProductEntity", "CustomizeButtonText", "CustomizeButtonText", true, "NVarChar", 100, 0, 0, false, "", null, typeof(System.String), 40);
			this.AddElementFieldMapping("ProductEntity", "InheritAlterations", "InheritAlterations", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 41);
			this.AddElementFieldMapping("ProductEntity", "ProductgroupId", "ProductgroupId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 42);
			this.AddElementFieldMapping("ProductEntity", "IsLinkedToGeneric", "IsLinkedToGeneric", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 43);
			this.AddElementFieldMapping("ProductEntity", "OverrideSubType", "OverrideSubType", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 44);
			this.AddElementFieldMapping("ProductEntity", "ExternalProductId", "ExternalProductId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 45);
			this.AddElementFieldMapping("ProductEntity", "BrandId", "BrandId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 46);
			this.AddElementFieldMapping("ProductEntity", "BrandProductId", "BrandProductId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 47);
			this.AddElementFieldMapping("ProductEntity", "InheritAttachmentsFromBrand", "InheritAttachmentsFromBrand", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 48);
			this.AddElementFieldMapping("ProductEntity", "InheritAlterationsFromBrand", "InheritAlterationsFromBrand", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 49);
			this.AddElementFieldMapping("ProductEntity", "InheritName", "InheritName", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 50);
			this.AddElementFieldMapping("ProductEntity", "InheritPrice", "InheritPrice", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 51);
			this.AddElementFieldMapping("ProductEntity", "InheritButtonText", "InheritButtonText", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 52);
			this.AddElementFieldMapping("ProductEntity", "InheritCustomizeButtonText", "InheritCustomizeButtonText", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 53);
			this.AddElementFieldMapping("ProductEntity", "InheritWebTypeTabletUrl", "InheritWebTypeTabletUrl", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 54);
			this.AddElementFieldMapping("ProductEntity", "InheritWebTypeSmartphoneUrl", "InheritWebTypeSmartphoneUrl", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 55);
			this.AddElementFieldMapping("ProductEntity", "InheritDescription", "InheritDescription", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 56);
			this.AddElementFieldMapping("ProductEntity", "InheritColor", "InheritColor", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 57);
			this.AddElementFieldMapping("ProductEntity", "ShortDescription", "ShortDescription", true, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 58);
			this.AddElementFieldMapping("ProductEntity", "InheritMedia", "InheritMedia", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 59);
			this.AddElementFieldMapping("ProductEntity", "TaxTariffId", "TaxTariffId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 60);
			this.AddElementFieldMapping("ProductEntity", "IsAlcoholic", "IsAlcoholic", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 61);
			this.AddElementFieldMapping("ProductEntity", "IsAvailable", "IsAvailable", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 62);
			this.AddElementFieldMapping("ProductEntity", "SystemName", "SystemName", true, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 63);
			this.AddElementFieldMapping("ProductEntity", "InheritSystemName", "InheritSystemName", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 64);
			this.AddElementFieldMapping("ProductEntity", "InheritExternalIdentifier", "InheritExternalIdentifier", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 65);
			this.AddElementFieldMapping("ProductEntity", "ExternalIdentifier", "ExternalIdentifier", true, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 66);
			this.AddElementFieldMapping("ProductEntity", "PointOfInterestId", "PointOfInterestId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 67);
			this.AddElementFieldMapping("ProductEntity", "CoversType", "CoversType", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 68);
			this.AddElementFieldMapping("ProductEntity", "OpenNewWindow", "OpenNewWindow", true, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 69);
			this.AddElementFieldMapping("ProductEntity", "WebLinkType", "WebLinkType", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 70);
		}

		/// <summary>Inits ProductAlterationEntity's mappings</summary>
		private void InitProductAlterationEntityMappings()
		{
			this.AddElementMapping("ProductAlterationEntity", @"Obymobi", @"dbo", "ProductAlteration", 12, 0);
			this.AddElementFieldMapping("ProductAlterationEntity", "ProductAlterationId", "ProductAlterationId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("ProductAlterationEntity", "ProductId", "ProductId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("ProductAlterationEntity", "AlterationId", "AlterationId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("ProductAlterationEntity", "SortOrder", "SortOrder", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("ProductAlterationEntity", "PosproductPosalterationId", "PosproductPosalterationId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("ProductAlterationEntity", "CreatedBy", "CreatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 5);
			this.AddElementFieldMapping("ProductAlterationEntity", "UpdatedBy", "UpdatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 6);
			this.AddElementFieldMapping("ProductAlterationEntity", "GenericproductGenericalterationId", "GenericproductGenericalterationId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 7);
			this.AddElementFieldMapping("ProductAlterationEntity", "ParentCompanyId", "ParentCompanyId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 8);
			this.AddElementFieldMapping("ProductAlterationEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 9);
			this.AddElementFieldMapping("ProductAlterationEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 10);
			this.AddElementFieldMapping("ProductAlterationEntity", "Version", "Version", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 11);
		}

		/// <summary>Inits ProductAttachmentEntity's mappings</summary>
		private void InitProductAttachmentEntityMappings()
		{
			this.AddElementMapping("ProductAttachmentEntity", @"Obymobi", @"dbo", "ProductAttachment", 8, 0);
			this.AddElementFieldMapping("ProductAttachmentEntity", "ProductAttachmentId", "ProductAttachmentId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("ProductAttachmentEntity", "ProductId", "ProductId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("ProductAttachmentEntity", "AttachmentId", "AttachmentId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("ProductAttachmentEntity", "CreatedBy", "CreatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("ProductAttachmentEntity", "UpdatedBy", "UpdatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("ProductAttachmentEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 5);
			this.AddElementFieldMapping("ProductAttachmentEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 6);
			this.AddElementFieldMapping("ProductAttachmentEntity", "ParentCompanyId", "ParentCompanyId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 7);
		}

		/// <summary>Inits ProductCategoryEntity's mappings</summary>
		private void InitProductCategoryEntityMappings()
		{
			this.AddElementMapping("ProductCategoryEntity", @"Obymobi", @"dbo", "ProductCategory", 10, 0);
			this.AddElementFieldMapping("ProductCategoryEntity", "ProductCategoryId", "ProductCategoryId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("ProductCategoryEntity", "ProductId", "ProductId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("ProductCategoryEntity", "CategoryId", "CategoryId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("ProductCategoryEntity", "SortOrder", "SortOrder", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("ProductCategoryEntity", "CreatedBy", "CreatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("ProductCategoryEntity", "UpdatedBy", "UpdatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 5);
			this.AddElementFieldMapping("ProductCategoryEntity", "ParentCompanyId", "ParentCompanyId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 6);
			this.AddElementFieldMapping("ProductCategoryEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 7);
			this.AddElementFieldMapping("ProductCategoryEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 8);
			this.AddElementFieldMapping("ProductCategoryEntity", "InheritSuggestions", "InheritSuggestions", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 9);
		}

		/// <summary>Inits ProductCategorySuggestionEntity's mappings</summary>
		private void InitProductCategorySuggestionEntityMappings()
		{
			this.AddElementMapping("ProductCategorySuggestionEntity", @"Obymobi", @"dbo", "ProductCategorySuggestion", 10, 0);
			this.AddElementFieldMapping("ProductCategorySuggestionEntity", "ProductCategorySuggestionId", "ProductCategorySuggestionId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("ProductCategorySuggestionEntity", "ParentCompanyId", "ParentCompanyId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("ProductCategorySuggestionEntity", "ProductCategoryId", "ProductCategoryId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("ProductCategorySuggestionEntity", "SuggestedProductCategoryId", "SuggestedProductCategoryId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("ProductCategorySuggestionEntity", "SortOrder", "SortOrder", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("ProductCategorySuggestionEntity", "Checkout", "Checkout", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 5);
			this.AddElementFieldMapping("ProductCategorySuggestionEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 6);
			this.AddElementFieldMapping("ProductCategorySuggestionEntity", "CreatedBy", "CreatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 7);
			this.AddElementFieldMapping("ProductCategorySuggestionEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 8);
			this.AddElementFieldMapping("ProductCategorySuggestionEntity", "UpdatedBy", "UpdatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 9);
		}

		/// <summary>Inits ProductCategoryTagEntity's mappings</summary>
		private void InitProductCategoryTagEntityMappings()
		{
			this.AddElementMapping("ProductCategoryTagEntity", @"Obymobi", @"dbo", "ProductCategoryTag", 7, 0);
			this.AddElementFieldMapping("ProductCategoryTagEntity", "ProductCategoryTagId", "ProductCategoryTagId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("ProductCategoryTagEntity", "ProductCategoryId", "ProductCategoryId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("ProductCategoryTagEntity", "CategoryId", "CategoryId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("ProductCategoryTagEntity", "TagId", "TagId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("ProductCategoryTagEntity", "CreatedUTC", "CreatedUTC", false, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 4);
			this.AddElementFieldMapping("ProductCategoryTagEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 5);
			this.AddElementFieldMapping("ProductCategoryTagEntity", "ParentCompanyId", "ParentCompanyId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 6);
		}

		/// <summary>Inits ProductgroupEntity's mappings</summary>
		private void InitProductgroupEntityMappings()
		{
			this.AddElementMapping("ProductgroupEntity", @"Obymobi", @"dbo", "Productgroup", 7, 0);
			this.AddElementFieldMapping("ProductgroupEntity", "ProductgroupId", "ProductgroupId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("ProductgroupEntity", "CompanyId", "CompanyId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("ProductgroupEntity", "Name", "Name", false, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 2);
			this.AddElementFieldMapping("ProductgroupEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 3);
			this.AddElementFieldMapping("ProductgroupEntity", "CreatedBy", "CreatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("ProductgroupEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 5);
			this.AddElementFieldMapping("ProductgroupEntity", "UpdatedBy", "UpdatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 6);
		}

		/// <summary>Inits ProductgroupItemEntity's mappings</summary>
		private void InitProductgroupItemEntityMappings()
		{
			this.AddElementMapping("ProductgroupItemEntity", @"Obymobi", @"dbo", "ProductgroupItem", 10, 0);
			this.AddElementFieldMapping("ProductgroupItemEntity", "ProductgroupItemId", "ProductgroupItemId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("ProductgroupItemEntity", "ParentCompanyId", "ParentCompanyId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("ProductgroupItemEntity", "ProductgroupId", "ProductgroupId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("ProductgroupItemEntity", "ProductId", "ProductId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("ProductgroupItemEntity", "NestedProductgroupId", "NestedProductgroupId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("ProductgroupItemEntity", "SortOrder", "SortOrder", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 5);
			this.AddElementFieldMapping("ProductgroupItemEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 6);
			this.AddElementFieldMapping("ProductgroupItemEntity", "CreatedBy", "CreatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 7);
			this.AddElementFieldMapping("ProductgroupItemEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 8);
			this.AddElementFieldMapping("ProductgroupItemEntity", "UpdatedBy", "UpdatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 9);
		}

		/// <summary>Inits ProductLanguageEntity's mappings</summary>
		private void InitProductLanguageEntityMappings()
		{
			this.AddElementMapping("ProductLanguageEntity", @"Obymobi", @"dbo", "ProductLanguage", 13, 0);
			this.AddElementFieldMapping("ProductLanguageEntity", "ProductLanguageId", "ProductLanguageId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("ProductLanguageEntity", "ProductId", "ProductId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("ProductLanguageEntity", "LanguageId", "LanguageId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("ProductLanguageEntity", "Name", "Name", true, "NVarChar", 200, 0, 0, false, "", null, typeof(System.String), 3);
			this.AddElementFieldMapping("ProductLanguageEntity", "Description", "Description", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 4);
			this.AddElementFieldMapping("ProductLanguageEntity", "CreatedBy", "CreatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 5);
			this.AddElementFieldMapping("ProductLanguageEntity", "UpdatedBy", "UpdatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 6);
			this.AddElementFieldMapping("ProductLanguageEntity", "ButtonText", "ButtonText", true, "NVarChar", 100, 0, 0, false, "", null, typeof(System.String), 7);
			this.AddElementFieldMapping("ProductLanguageEntity", "OrderProcessedTitle", "OrderProcessedTitle", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 8);
			this.AddElementFieldMapping("ProductLanguageEntity", "OrderProcessedMessage", "OrderProcessedMessage", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 9);
			this.AddElementFieldMapping("ProductLanguageEntity", "ParentCompanyId", "ParentCompanyId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 10);
			this.AddElementFieldMapping("ProductLanguageEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 11);
			this.AddElementFieldMapping("ProductLanguageEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 12);
		}

		/// <summary>Inits ProductSuggestionEntity's mappings</summary>
		private void InitProductSuggestionEntityMappings()
		{
			this.AddElementMapping("ProductSuggestionEntity", @"Obymobi", @"dbo", "ProductSuggestion", 11, 0);
			this.AddElementFieldMapping("ProductSuggestionEntity", "ProductSuggestionId", "ProductSuggestionId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("ProductSuggestionEntity", "ProductId", "ProductId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("ProductSuggestionEntity", "SuggestedProductId", "SuggestedProductId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("ProductSuggestionEntity", "Direct", "Direct", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 3);
			this.AddElementFieldMapping("ProductSuggestionEntity", "SortOrder", "SortOrder", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("ProductSuggestionEntity", "CreatedBy", "CreatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 5);
			this.AddElementFieldMapping("ProductSuggestionEntity", "UpdatedBy", "UpdatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 6);
			this.AddElementFieldMapping("ProductSuggestionEntity", "Checkout", "Checkout", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 7);
			this.AddElementFieldMapping("ProductSuggestionEntity", "ParentCompanyId", "ParentCompanyId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 8);
			this.AddElementFieldMapping("ProductSuggestionEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 9);
			this.AddElementFieldMapping("ProductSuggestionEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 10);
		}

		/// <summary>Inits ProductTagEntity's mappings</summary>
		private void InitProductTagEntityMappings()
		{
			this.AddElementMapping("ProductTagEntity", @"Obymobi", @"dbo", "ProductTag", 6, 0);
			this.AddElementFieldMapping("ProductTagEntity", "ProductTagId", "ProductTagId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("ProductTagEntity", "ProductId", "ProductId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("ProductTagEntity", "TagId", "TagId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("ProductTagEntity", "CreatedUTC", "CreatedUTC", false, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 3);
			this.AddElementFieldMapping("ProductTagEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 4);
			this.AddElementFieldMapping("ProductTagEntity", "ParentCompanyId", "ParentCompanyId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 5);
		}

		/// <summary>Inits PublishingEntity's mappings</summary>
		private void InitPublishingEntityMappings()
		{
			this.AddElementMapping("PublishingEntity", @"Obymobi", @"dbo", "Publishing", 17, 0);
			this.AddElementFieldMapping("PublishingEntity", "PublishingId", "PublishingId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("PublishingEntity", "PublishedUTC", "PublishedUTC", false, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 1);
			this.AddElementFieldMapping("PublishingEntity", "PublishedTicks", "PublishedTicks", false, "BigInt", 0, 19, 0, false, "", null, typeof(System.Int64), 2);
			this.AddElementFieldMapping("PublishingEntity", "UserId", "UserId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("PublishingEntity", "Username", "Username", false, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 4);
			this.AddElementFieldMapping("PublishingEntity", "Log", "Log", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 5);
			this.AddElementFieldMapping("PublishingEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 6);
			this.AddElementFieldMapping("PublishingEntity", "CreatedBy", "CreatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 7);
			this.AddElementFieldMapping("PublishingEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 8);
			this.AddElementFieldMapping("PublishingEntity", "UpdatedBy", "UpdatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 9);
			this.AddElementFieldMapping("PublishingEntity", "PublishRequest", "PublishRequest", false, "VarChar", 255, 0, 0, false, "", null, typeof(System.String), 10);
			this.AddElementFieldMapping("PublishingEntity", "Parameter1Name", "Parameter1Name", true, "VarChar", 255, 0, 0, false, "", null, typeof(System.String), 11);
			this.AddElementFieldMapping("PublishingEntity", "Parameter1Value", "Parameter1Value", true, "VarChar", 255, 0, 0, false, "", null, typeof(System.String), 12);
			this.AddElementFieldMapping("PublishingEntity", "Parameter2Name", "Parameter2Name", true, "VarChar", 255, 0, 0, false, "", null, typeof(System.String), 13);
			this.AddElementFieldMapping("PublishingEntity", "Parameter2Value", "Parameter2Value", true, "VarChar", 255, 0, 0, false, "", null, typeof(System.String), 14);
			this.AddElementFieldMapping("PublishingEntity", "Status", "Status", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 15);
			this.AddElementFieldMapping("PublishingEntity", "StatusText", "StatusText", false, "VarChar", 255, 0, 0, false, "", null, typeof(System.String), 16);
		}

		/// <summary>Inits PublishingItemEntity's mappings</summary>
		private void InitPublishingItemEntityMappings()
		{
			this.AddElementMapping("PublishingItemEntity", @"Obymobi", @"dbo", "PublishingItem", 16, 0);
			this.AddElementFieldMapping("PublishingItemEntity", "PublishingItemId", "PublishingItemId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("PublishingItemEntity", "PublishingId", "PublishingId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("PublishingItemEntity", "ApiOperation", "ApiOperation", false, "VarChar", 255, 0, 0, false, "", null, typeof(System.String), 2);
			this.AddElementFieldMapping("PublishingItemEntity", "Status", "Status", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("PublishingItemEntity", "StatusText", "StatusText", false, "VarChar", 255, 0, 0, false, "", null, typeof(System.String), 4);
			this.AddElementFieldMapping("PublishingItemEntity", "Log", "Log", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 5);
			this.AddElementFieldMapping("PublishingItemEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 6);
			this.AddElementFieldMapping("PublishingItemEntity", "CreatedBy", "CreatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 7);
			this.AddElementFieldMapping("PublishingItemEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 8);
			this.AddElementFieldMapping("PublishingItemEntity", "UpdatedBy", "UpdatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 9);
			this.AddElementFieldMapping("PublishingItemEntity", "Parameter1Name", "Parameter1Name", true, "VarChar", 255, 0, 0, false, "", null, typeof(System.String), 10);
			this.AddElementFieldMapping("PublishingItemEntity", "Parameter1Value", "Parameter1Value", true, "VarChar", 255, 0, 0, false, "", null, typeof(System.String), 11);
			this.AddElementFieldMapping("PublishingItemEntity", "Parameter2Name", "Parameter2Name", true, "VarChar", 255, 0, 0, false, "", null, typeof(System.String), 12);
			this.AddElementFieldMapping("PublishingItemEntity", "Parameter2Value", "Parameter2Value", true, "VarChar", 255, 0, 0, false, "", null, typeof(System.String), 13);
			this.AddElementFieldMapping("PublishingItemEntity", "PublishedUTC", "PublishedUTC", false, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 14);
			this.AddElementFieldMapping("PublishingItemEntity", "PublishedTicks", "PublishedTicks", false, "BigInt", 0, 19, 0, false, "", null, typeof(System.Int64), 15);
		}

		/// <summary>Inits RatingEntity's mappings</summary>
		private void InitRatingEntityMappings()
		{
			this.AddElementMapping("RatingEntity", @"Obymobi", @"dbo", "Rating", 10, 0);
			this.AddElementFieldMapping("RatingEntity", "RatingId", "RatingId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("RatingEntity", "ProductId", "ProductId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("RatingEntity", "Value", "Value", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("RatingEntity", "Visible", "Visible", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 3);
			this.AddElementFieldMapping("RatingEntity", "Comments", "Comments", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 4);
			this.AddElementFieldMapping("RatingEntity", "CreatedBy", "CreatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 5);
			this.AddElementFieldMapping("RatingEntity", "UpdatedBy", "UpdatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 6);
			this.AddElementFieldMapping("RatingEntity", "ParentCompanyId", "ParentCompanyId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 7);
			this.AddElementFieldMapping("RatingEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 8);
			this.AddElementFieldMapping("RatingEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 9);
		}

		/// <summary>Inits ReceiptEntity's mappings</summary>
		private void InitReceiptEntityMappings()
		{
			this.AddElementMapping("ReceiptEntity", @"Obymobi", @"dbo", "Receipt", 21, 0);
			this.AddElementFieldMapping("ReceiptEntity", "ReceiptId", "ReceiptId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("ReceiptEntity", "ReceiptTemplateId", "ReceiptTemplateId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("ReceiptEntity", "CompanyId", "CompanyId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("ReceiptEntity", "OrderId", "OrderId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("ReceiptEntity", "Number", "Number", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("ReceiptEntity", "SellerName", "SellerName", false, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 5);
			this.AddElementFieldMapping("ReceiptEntity", "SellerAddress", "SellerAddress", false, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 6);
			this.AddElementFieldMapping("ReceiptEntity", "SellerContactInfo", "SellerContactInfo", false, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 7);
			this.AddElementFieldMapping("ReceiptEntity", "ServiceMethodName", "ServiceMethodName", false, "NVarChar", 100, 0, 0, false, "", null, typeof(System.String), 8);
			this.AddElementFieldMapping("ReceiptEntity", "CheckoutMethodName", "CheckoutMethodName", false, "NVarChar", 100, 0, 0, false, "", null, typeof(System.String), 9);
			this.AddElementFieldMapping("ReceiptEntity", "CreatedBy", "CreatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 10);
			this.AddElementFieldMapping("ReceiptEntity", "UpdatedBy", "UpdatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 11);
			this.AddElementFieldMapping("ReceiptEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 12);
			this.AddElementFieldMapping("ReceiptEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 13);
			this.AddElementFieldMapping("ReceiptEntity", "TaxBreakdown", "TaxBreakdown", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 14);
			this.AddElementFieldMapping("ReceiptEntity", "Email", "Email", true, "NVarChar", 2000, 0, 0, false, "", null, typeof(System.String), 15);
			this.AddElementFieldMapping("ReceiptEntity", "Phonenumber", "Phonenumber", true, "NVarChar", 2000, 0, 0, false, "", null, typeof(System.String), 16);
			this.AddElementFieldMapping("ReceiptEntity", "VatNumber", "VatNumber", true, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 17);
			this.AddElementFieldMapping("ReceiptEntity", "PaymentProcessorInfo", "PaymentProcessorInfo", true, "Text", 2147483647, 0, 0, false, "", null, typeof(System.String), 18);
			this.AddElementFieldMapping("ReceiptEntity", "SellerContactEmail", "SellerContactEmail", true, "NVarChar", 250, 0, 0, false, "", null, typeof(System.String), 19);
			this.AddElementFieldMapping("ReceiptEntity", "OutletSellerInformationId", "OutletSellerInformationId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 20);
		}

		/// <summary>Inits ReceiptTemplateEntity's mappings</summary>
		private void InitReceiptTemplateEntityMappings()
		{
			this.AddElementMapping("ReceiptTemplateEntity", @"Obymobi", @"dbo", "ReceiptTemplate", 15, 0);
			this.AddElementFieldMapping("ReceiptTemplateEntity", "ReceiptTemplateId", "ReceiptTemplateId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("ReceiptTemplateEntity", "CompanyId", "CompanyId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("ReceiptTemplateEntity", "Name", "Name", false, "NVarChar", 256, 0, 0, false, "", null, typeof(System.String), 2);
			this.AddElementFieldMapping("ReceiptTemplateEntity", "SellerName", "SellerName", false, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 3);
			this.AddElementFieldMapping("ReceiptTemplateEntity", "SellerAddress", "SellerAddress", false, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 4);
			this.AddElementFieldMapping("ReceiptTemplateEntity", "SellerContactInfo", "SellerContactInfo", false, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 5);
			this.AddElementFieldMapping("ReceiptTemplateEntity", "TaxBreakdown", "TaxBreakdown", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 6);
			this.AddElementFieldMapping("ReceiptTemplateEntity", "CreatedBy", "CreatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 7);
			this.AddElementFieldMapping("ReceiptTemplateEntity", "UpdatedBy", "UpdatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 8);
			this.AddElementFieldMapping("ReceiptTemplateEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 9);
			this.AddElementFieldMapping("ReceiptTemplateEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 10);
			this.AddElementFieldMapping("ReceiptTemplateEntity", "Email", "Email", true, "NVarChar", 2000, 0, 0, false, "", null, typeof(System.String), 11);
			this.AddElementFieldMapping("ReceiptTemplateEntity", "Phonenumber", "Phonenumber", true, "NVarChar", 2000, 0, 0, false, "", null, typeof(System.String), 12);
			this.AddElementFieldMapping("ReceiptTemplateEntity", "VatNumber", "VatNumber", true, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 13);
			this.AddElementFieldMapping("ReceiptTemplateEntity", "SellerContactEmail", "SellerContactEmail", true, "NVarChar", 250, 0, 0, false, "", null, typeof(System.String), 14);
		}

		/// <summary>Inits ReferentialConstraintEntity's mappings</summary>
		private void InitReferentialConstraintEntityMappings()
		{
			this.AddElementMapping("ReferentialConstraintEntity", @"Obymobi", @"dbo", "ReferentialConstraint", 13, 0);
			this.AddElementFieldMapping("ReferentialConstraintEntity", "ReferentialConstraintId", "ReferentialConstraintId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("ReferentialConstraintEntity", "EntityName", "EntityName", true, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 1);
			this.AddElementFieldMapping("ReferentialConstraintEntity", "ForeignKeyName", "ForeignKeyName", true, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 2);
			this.AddElementFieldMapping("ReferentialConstraintEntity", "DeleteRule", "DeleteRule", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("ReferentialConstraintEntity", "UpdateRule", "UpdateRule", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("ReferentialConstraintEntity", "ArchiveRule", "ArchiveRule", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 5);
			this.AddElementFieldMapping("ReferentialConstraintEntity", "ForLocalUseWhileSyncingIsUpdated", "ForLocalUseWhileSyncingIsUpdated", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 6);
			this.AddElementFieldMapping("ReferentialConstraintEntity", "Archived", "Archived", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 7);
			this.AddElementFieldMapping("ReferentialConstraintEntity", "CreatedBy", "CreatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 8);
			this.AddElementFieldMapping("ReferentialConstraintEntity", "UpdatedBy", "UpdatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 9);
			this.AddElementFieldMapping("ReferentialConstraintEntity", "Deleted", "Deleted", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 10);
			this.AddElementFieldMapping("ReferentialConstraintEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 11);
			this.AddElementFieldMapping("ReferentialConstraintEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 12);
		}

		/// <summary>Inits ReferentialConstraintCustomEntity's mappings</summary>
		private void InitReferentialConstraintCustomEntityMappings()
		{
			this.AddElementMapping("ReferentialConstraintCustomEntity", @"Obymobi", @"dbo", "ReferentialConstraintCustom", 9, 0);
			this.AddElementFieldMapping("ReferentialConstraintCustomEntity", "ReferentialConstraintCustomId", "ReferentialConstraintCustomId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("ReferentialConstraintCustomEntity", "EntityName", "EntityName", true, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 1);
			this.AddElementFieldMapping("ReferentialConstraintCustomEntity", "ForeignKeyName", "ForeignKeyName", true, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 2);
			this.AddElementFieldMapping("ReferentialConstraintCustomEntity", "DeleteRule", "DeleteRule", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("ReferentialConstraintCustomEntity", "UpdateRule", "UpdateRule", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("ReferentialConstraintCustomEntity", "CreatedBy", "CreatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 5);
			this.AddElementFieldMapping("ReferentialConstraintCustomEntity", "UpdatedBy", "UpdatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 6);
			this.AddElementFieldMapping("ReferentialConstraintCustomEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 7);
			this.AddElementFieldMapping("ReferentialConstraintCustomEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 8);
		}

		/// <summary>Inits ReleaseEntity's mappings</summary>
		private void InitReleaseEntityMappings()
		{
			this.AddElementMapping("ReleaseEntity", @"Obymobi", @"dbo", "Release", 14, 0);
			this.AddElementFieldMapping("ReleaseEntity", "ReleaseId", "ReleaseId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("ReleaseEntity", "ApplicationId", "ApplicationId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("ReleaseEntity", "Version", "Version", true, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 2);
			this.AddElementFieldMapping("ReleaseEntity", "Comment", "Comment", true, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 3);
			this.AddElementFieldMapping("ReleaseEntity", "Filename", "Filename", true, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 4);
			this.AddElementFieldMapping("ReleaseEntity", "CreatedBy", "CreatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 5);
			this.AddElementFieldMapping("ReleaseEntity", "UpdatedBy", "UpdatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 6);
			this.AddElementFieldMapping("ReleaseEntity", "File", "File", true, "VarBinary", 2147483647, 0, 0, false, "", null, typeof(System.Byte[]), 7);
			this.AddElementFieldMapping("ReleaseEntity", "Crc32", "Crc32", true, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 8);
			this.AddElementFieldMapping("ReleaseEntity", "Intermediate", "Intermediate", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 9);
			this.AddElementFieldMapping("ReleaseEntity", "IntermediateReleaseId", "IntermediateReleaseId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 10);
			this.AddElementFieldMapping("ReleaseEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 11);
			this.AddElementFieldMapping("ReleaseEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 12);
			this.AddElementFieldMapping("ReleaseEntity", "ReleaseGroup", "ReleaseGroup", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 13);
		}

		/// <summary>Inits ReportProcessingTaskEntity's mappings</summary>
		private void InitReportProcessingTaskEntityMappings()
		{
			this.AddElementMapping("ReportProcessingTaskEntity", @"Obymobi", @"dbo", "ReportProcessingTask", 21, 0);
			this.AddElementFieldMapping("ReportProcessingTaskEntity", "ReportProcessingTaskId", "ReportProcessingTaskId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("ReportProcessingTaskEntity", "AnalyticsReportType", "AnalyticsReportType", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("ReportProcessingTaskEntity", "Processed", "Processed", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 2);
			this.AddElementFieldMapping("ReportProcessingTaskEntity", "CompanyId", "CompanyId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("ReportProcessingTaskEntity", "XFrom", "From", true, "DateTime", 0, 0, 0, false, "", null, typeof(System.DateTime), 4);
			this.AddElementFieldMapping("ReportProcessingTaskEntity", "XTill", "Till", true, "DateTime", 0, 0, 0, false, "", null, typeof(System.DateTime), 5);
			this.AddElementFieldMapping("ReportProcessingTaskEntity", "Filter", "Filter", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 6);
			this.AddElementFieldMapping("ReportProcessingTaskEntity", "Failed", "Failed", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 7);
			this.AddElementFieldMapping("ReportProcessingTaskEntity", "ExceptionText", "ExceptionText", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 8);
			this.AddElementFieldMapping("ReportProcessingTaskEntity", "CreatedBy", "CreatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 9);
			this.AddElementFieldMapping("ReportProcessingTaskEntity", "UpdatedBy", "UpdatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 10);
			this.AddElementFieldMapping("ReportProcessingTaskEntity", "ExternalTask", "ExternalTask", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 11);
			this.AddElementFieldMapping("ReportProcessingTaskEntity", "ExternalHandled", "ExternalHandled", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 12);
			this.AddElementFieldMapping("ReportProcessingTaskEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 13);
			this.AddElementFieldMapping("ReportProcessingTaskEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 14);
			this.AddElementFieldMapping("ReportProcessingTaskEntity", "FromUTC", "FromUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 15);
			this.AddElementFieldMapping("ReportProcessingTaskEntity", "TillUTC", "TillUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 16);
			this.AddElementFieldMapping("ReportProcessingTaskEntity", "TimeZoneId", "TimeZoneId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 17);
			this.AddElementFieldMapping("ReportProcessingTaskEntity", "TimeZoneOlsonId", "TimeZoneOlsonId", true, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 18);
			this.AddElementFieldMapping("ReportProcessingTaskEntity", "Email", "Email", true, "NVarChar", 2048, 0, 0, false, "", null, typeof(System.String), 19);
			this.AddElementFieldMapping("ReportProcessingTaskEntity", "ReportName", "ReportName", true, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 20);
		}

		/// <summary>Inits ReportProcessingTaskFileEntity's mappings</summary>
		private void InitReportProcessingTaskFileEntityMappings()
		{
			this.AddElementMapping("ReportProcessingTaskFileEntity", @"Obymobi", @"dbo", "ReportProcessingTaskFile", 8, 0);
			this.AddElementFieldMapping("ReportProcessingTaskFileEntity", "ReportProcessingTaskFileId", "ReportProcessingTaskFileId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("ReportProcessingTaskFileEntity", "ReportProcessingTaskId", "ReportProcessingTaskId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("ReportProcessingTaskFileEntity", "File", "File", false, "VarBinary", 2147483647, 0, 0, false, "", null, typeof(System.Byte[]), 2);
			this.AddElementFieldMapping("ReportProcessingTaskFileEntity", "CreatedBy", "CreatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("ReportProcessingTaskFileEntity", "UpdatedBy", "UpdatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("ReportProcessingTaskFileEntity", "ParentCompanyId", "ParentCompanyId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 5);
			this.AddElementFieldMapping("ReportProcessingTaskFileEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 6);
			this.AddElementFieldMapping("ReportProcessingTaskFileEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 7);
		}

		/// <summary>Inits ReportProcessingTaskTemplateEntity's mappings</summary>
		private void InitReportProcessingTaskTemplateEntityMappings()
		{
			this.AddElementMapping("ReportProcessingTaskTemplateEntity", @"Obymobi", @"dbo", "ReportProcessingTaskTemplate", 13, 0);
			this.AddElementFieldMapping("ReportProcessingTaskTemplateEntity", "ReportProcessingTaskTemplateId", "ReportProcessingTaskTemplateId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("ReportProcessingTaskTemplateEntity", "ReportProcessingTaskId", "ReportProcessingTaskId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("ReportProcessingTaskTemplateEntity", "TemplateFile", "TemplateFile", true, "VarBinary", 2147483647, 0, 0, false, "", null, typeof(System.Byte[]), 2);
			this.AddElementFieldMapping("ReportProcessingTaskTemplateEntity", "CreatedBy", "CreatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("ReportProcessingTaskTemplateEntity", "UpdatedBy", "UpdatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("ReportProcessingTaskTemplateEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 5);
			this.AddElementFieldMapping("ReportProcessingTaskTemplateEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 6);
			this.AddElementFieldMapping("ReportProcessingTaskTemplateEntity", "CompanyId", "CompanyId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 7);
			this.AddElementFieldMapping("ReportProcessingTaskTemplateEntity", "Filter", "Filter", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 8);
			this.AddElementFieldMapping("ReportProcessingTaskTemplateEntity", "ReportingPeriod", "ReportingPeriod", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 9);
			this.AddElementFieldMapping("ReportProcessingTaskTemplateEntity", "FriendlyName", "FriendlyName", true, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 10);
			this.AddElementFieldMapping("ReportProcessingTaskTemplateEntity", "Email", "Email", true, "NVarChar", 2024, 0, 0, false, "", null, typeof(System.String), 11);
			this.AddElementFieldMapping("ReportProcessingTaskTemplateEntity", "ReportType", "ReportType", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 12);
		}

		/// <summary>Inits RequestlogEntity's mappings</summary>
		private void InitRequestlogEntityMappings()
		{
			this.AddElementMapping("RequestlogEntity", @"Obymobi", @"dbo", "Requestlog", 36, 0);
			this.AddElementFieldMapping("RequestlogEntity", "RequestlogId", "RequestlogId", false, "BigInt", 0, 19, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int64), 0);
			this.AddElementFieldMapping("RequestlogEntity", "UserAgent", "UserAgent", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 1);
			this.AddElementFieldMapping("RequestlogEntity", "SourceApplication", "SourceApplication", true, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 2);
			this.AddElementFieldMapping("RequestlogEntity", "Identifier", "Identifier", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 3);
			this.AddElementFieldMapping("RequestlogEntity", "CustomerId", "CustomerId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("RequestlogEntity", "TerminalId", "TerminalId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 5);
			this.AddElementFieldMapping("RequestlogEntity", "Phonenumber", "Phonenumber", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 6);
			this.AddElementFieldMapping("RequestlogEntity", "MethodName", "MethodName", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 7);
			this.AddElementFieldMapping("RequestlogEntity", "ResultEnumTypeName", "ResultEnumTypeName", true, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 8);
			this.AddElementFieldMapping("RequestlogEntity", "ResultEnumValueName", "ResultEnumValueName", true, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 9);
			this.AddElementFieldMapping("RequestlogEntity", "ResultCode", "ResultCode", true, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 10);
			this.AddElementFieldMapping("RequestlogEntity", "ResultMessage", "ResultMessage", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 11);
			this.AddElementFieldMapping("RequestlogEntity", "Parameter1", "Parameter1", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 12);
			this.AddElementFieldMapping("RequestlogEntity", "Parameter2", "Parameter2", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 13);
			this.AddElementFieldMapping("RequestlogEntity", "Parameter3", "Parameter3", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 14);
			this.AddElementFieldMapping("RequestlogEntity", "Parameter4", "Parameter4", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 15);
			this.AddElementFieldMapping("RequestlogEntity", "Parameter5", "Parameter5", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 16);
			this.AddElementFieldMapping("RequestlogEntity", "Parameter6", "Parameter6", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 17);
			this.AddElementFieldMapping("RequestlogEntity", "ResultBody", "ResultBody", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 18);
			this.AddElementFieldMapping("RequestlogEntity", "ErrorMessage", "ErrorMessage", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 19);
			this.AddElementFieldMapping("RequestlogEntity", "ErrorStackTrace", "ErrorStackTrace", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 20);
			this.AddElementFieldMapping("RequestlogEntity", "Xml", "Xml", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 21);
			this.AddElementFieldMapping("RequestlogEntity", "RawRequest", "RawRequest", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 22);
			this.AddElementFieldMapping("RequestlogEntity", "TraceFromMobile", "TraceFromMobile", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 23);
			this.AddElementFieldMapping("RequestlogEntity", "MobileVendor", "MobileVendor", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 24);
			this.AddElementFieldMapping("RequestlogEntity", "MobileIdentifier", "MobileIdentifier", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 25);
			this.AddElementFieldMapping("RequestlogEntity", "MobilePlatform", "MobilePlatform", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 26);
			this.AddElementFieldMapping("RequestlogEntity", "MobileOs", "MobileOs", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 27);
			this.AddElementFieldMapping("RequestlogEntity", "MobileName", "MobileName", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 28);
			this.AddElementFieldMapping("RequestlogEntity", "DeviceInfo", "DeviceInfo", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 29);
			this.AddElementFieldMapping("RequestlogEntity", "Log", "Log", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 30);
			this.AddElementFieldMapping("RequestlogEntity", "CreatedBy", "CreatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 31);
			this.AddElementFieldMapping("RequestlogEntity", "UpdatedBy", "UpdatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 32);
			this.AddElementFieldMapping("RequestlogEntity", "ServerName", "ServerName", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 33);
			this.AddElementFieldMapping("RequestlogEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 34);
			this.AddElementFieldMapping("RequestlogEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 35);
		}

		/// <summary>Inits RoleEntity's mappings</summary>
		private void InitRoleEntityMappings()
		{
			this.AddElementMapping("RoleEntity", @"Obymobi", @"dbo", "Role", 12, 0);
			this.AddElementFieldMapping("RoleEntity", "RoleId", "RoleId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("RoleEntity", "ParentRoleId", "ParentRoleId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("RoleEntity", "Name", "Name", true, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 2);
			this.AddElementFieldMapping("RoleEntity", "IsAdministrator", "IsAdministrator", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 3);
			this.AddElementFieldMapping("RoleEntity", "SystemName", "SystemName", true, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 4);
			this.AddElementFieldMapping("RoleEntity", "SystemRole", "SystemRole", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 5);
			this.AddElementFieldMapping("RoleEntity", "CreatedBy", "CreatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 6);
			this.AddElementFieldMapping("RoleEntity", "UpdatedBy", "UpdatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 7);
			this.AddElementFieldMapping("RoleEntity", "Deleted", "Deleted", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 8);
			this.AddElementFieldMapping("RoleEntity", "Archived", "Archived", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 9);
			this.AddElementFieldMapping("RoleEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 10);
			this.AddElementFieldMapping("RoleEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 11);
		}

		/// <summary>Inits RoleModuleRightsEntity's mappings</summary>
		private void InitRoleModuleRightsEntityMappings()
		{
			this.AddElementMapping("RoleModuleRightsEntity", @"Obymobi", @"dbo", "RoleModuleRights", 8, 0);
			this.AddElementFieldMapping("RoleModuleRightsEntity", "RoleModuleRightsId", "RoleModuleRightsId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("RoleModuleRightsEntity", "RoleId", "RoleId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("RoleModuleRightsEntity", "ModuleId", "ModuleId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("RoleModuleRightsEntity", "IsAllowed", "IsAllowed", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 3);
			this.AddElementFieldMapping("RoleModuleRightsEntity", "CreatedBy", "CreatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("RoleModuleRightsEntity", "UpdatedBy", "UpdatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 5);
			this.AddElementFieldMapping("RoleModuleRightsEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 6);
			this.AddElementFieldMapping("RoleModuleRightsEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 7);
		}

		/// <summary>Inits RoleUIElementRightsEntity's mappings</summary>
		private void InitRoleUIElementRightsEntityMappings()
		{
			this.AddElementMapping("RoleUIElementRightsEntity", @"Obymobi", @"dbo", "RoleUIElementRights", 8, 0);
			this.AddElementFieldMapping("RoleUIElementRightsEntity", "RoleUIElementRightsId", "RoleUIElementRightsId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("RoleUIElementRightsEntity", "RoleId", "RoleId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("RoleUIElementRightsEntity", "UiElementTypeNameFull", "UiElementTypeNameFull", false, "VarChar", 255, 0, 0, false, "", null, typeof(System.String), 2);
			this.AddElementFieldMapping("RoleUIElementRightsEntity", "IsAllowed", "IsAllowed", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 3);
			this.AddElementFieldMapping("RoleUIElementRightsEntity", "IsReadOnly", "IsReadOnly", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 4);
			this.AddElementFieldMapping("RoleUIElementRightsEntity", "CreatedBy", "CreatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 5);
			this.AddElementFieldMapping("RoleUIElementRightsEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 6);
			this.AddElementFieldMapping("RoleUIElementRightsEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 7);
		}

		/// <summary>Inits RoleUIElementSubPanelRightsEntity's mappings</summary>
		private void InitRoleUIElementSubPanelRightsEntityMappings()
		{
			this.AddElementMapping("RoleUIElementSubPanelRightsEntity", @"Obymobi", @"dbo", "RoleUIElementSubPanelRights", 8, 0);
			this.AddElementFieldMapping("RoleUIElementSubPanelRightsEntity", "RoleUIElementSubPanelRightsId", "RoleUIElementSubPanelRightsId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("RoleUIElementSubPanelRightsEntity", "RoleId", "RoleId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("RoleUIElementSubPanelRightsEntity", "UiElementTypeNameFull", "UiElementTypeNameFull", false, "VarChar", 255, 0, 0, false, "", null, typeof(System.String), 2);
			this.AddElementFieldMapping("RoleUIElementSubPanelRightsEntity", "IsAllowed", "IsAllowed", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 3);
			this.AddElementFieldMapping("RoleUIElementSubPanelRightsEntity", "CreatedBy", "CreatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("RoleUIElementSubPanelRightsEntity", "UpdatedBy", "UpdatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 5);
			this.AddElementFieldMapping("RoleUIElementSubPanelRightsEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 6);
			this.AddElementFieldMapping("RoleUIElementSubPanelRightsEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 7);
		}

		/// <summary>Inits RoomControlAreaEntity's mappings</summary>
		private void InitRoomControlAreaEntityMappings()
		{
			this.AddElementMapping("RoomControlAreaEntity", @"Obymobi", @"dbo", "RoomControlArea", 13, 0);
			this.AddElementFieldMapping("RoomControlAreaEntity", "RoomControlAreaId", "RoomControlAreaId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("RoomControlAreaEntity", "ParentCompanyId", "ParentCompanyId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("RoomControlAreaEntity", "RoomControlConfigurationId", "RoomControlConfigurationId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("RoomControlAreaEntity", "Name", "Name", true, "NVarChar", 200, 0, 0, false, "", null, typeof(System.String), 3);
			this.AddElementFieldMapping("RoomControlAreaEntity", "SortOrder", "SortOrder", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("RoomControlAreaEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 5);
			this.AddElementFieldMapping("RoomControlAreaEntity", "CreatedBy", "CreatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 6);
			this.AddElementFieldMapping("RoomControlAreaEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 7);
			this.AddElementFieldMapping("RoomControlAreaEntity", "UpdatedBy", "UpdatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 8);
			this.AddElementFieldMapping("RoomControlAreaEntity", "Visible", "Visible", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 9);
			this.AddElementFieldMapping("RoomControlAreaEntity", "NameSystem", "NameSystem", true, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 10);
			this.AddElementFieldMapping("RoomControlAreaEntity", "Type", "Type", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 11);
			this.AddElementFieldMapping("RoomControlAreaEntity", "IsNameSystemBaseId", "IsNameSystemBaseId", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 12);
		}

		/// <summary>Inits RoomControlAreaLanguageEntity's mappings</summary>
		private void InitRoomControlAreaLanguageEntityMappings()
		{
			this.AddElementMapping("RoomControlAreaLanguageEntity", @"Obymobi", @"dbo", "RoomControlAreaLanguage", 9, 0);
			this.AddElementFieldMapping("RoomControlAreaLanguageEntity", "RoomControlAreaLanguageId", "RoomControlAreaLanguageId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("RoomControlAreaLanguageEntity", "ParentCompanyId", "ParentCompanyId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("RoomControlAreaLanguageEntity", "RoomControlAreaId", "RoomControlAreaId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("RoomControlAreaLanguageEntity", "LanguageId", "LanguageId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("RoomControlAreaLanguageEntity", "Name", "Name", true, "NVarChar", 200, 0, 0, false, "", null, typeof(System.String), 4);
			this.AddElementFieldMapping("RoomControlAreaLanguageEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 5);
			this.AddElementFieldMapping("RoomControlAreaLanguageEntity", "CreatedBy", "CreatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 6);
			this.AddElementFieldMapping("RoomControlAreaLanguageEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 7);
			this.AddElementFieldMapping("RoomControlAreaLanguageEntity", "UpdatedBy", "UpdatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 8);
		}

		/// <summary>Inits RoomControlComponentEntity's mappings</summary>
		private void InitRoomControlComponentEntityMappings()
		{
			this.AddElementMapping("RoomControlComponentEntity", @"Obymobi", @"dbo", "RoomControlComponent", 19, 0);
			this.AddElementFieldMapping("RoomControlComponentEntity", "RoomControlComponentId", "RoomControlComponentId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("RoomControlComponentEntity", "ParentCompanyId", "ParentCompanyId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("RoomControlComponentEntity", "RoomControlSectionId", "RoomControlSectionId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("RoomControlComponentEntity", "Name", "Name", true, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 3);
			this.AddElementFieldMapping("RoomControlComponentEntity", "NameSystem", "NameSystem", true, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 4);
			this.AddElementFieldMapping("RoomControlComponentEntity", "Type", "Type", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 5);
			this.AddElementFieldMapping("RoomControlComponentEntity", "SortOrder", "SortOrder", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 6);
			this.AddElementFieldMapping("RoomControlComponentEntity", "Visible", "Visible", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 7);
			this.AddElementFieldMapping("RoomControlComponentEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 8);
			this.AddElementFieldMapping("RoomControlComponentEntity", "CreatedBy", "CreatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 9);
			this.AddElementFieldMapping("RoomControlComponentEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 10);
			this.AddElementFieldMapping("RoomControlComponentEntity", "UpdatedBy", "UpdatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 11);
			this.AddElementFieldMapping("RoomControlComponentEntity", "FieldValue1", "FieldValue1", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 12);
			this.AddElementFieldMapping("RoomControlComponentEntity", "FieldValue2", "FieldValue2", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 13);
			this.AddElementFieldMapping("RoomControlComponentEntity", "FieldValue3", "FieldValue3", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 14);
			this.AddElementFieldMapping("RoomControlComponentEntity", "FieldValue4", "FieldValue4", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 15);
			this.AddElementFieldMapping("RoomControlComponentEntity", "FieldValue5", "FieldValue5", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 16);
			this.AddElementFieldMapping("RoomControlComponentEntity", "InfraredConfigurationId", "InfraredConfigurationId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 17);
			this.AddElementFieldMapping("RoomControlComponentEntity", "ControllerId", "ControllerId", true, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 18);
		}

		/// <summary>Inits RoomControlComponentLanguageEntity's mappings</summary>
		private void InitRoomControlComponentLanguageEntityMappings()
		{
			this.AddElementMapping("RoomControlComponentLanguageEntity", @"Obymobi", @"dbo", "RoomControlComponentLanguage", 9, 0);
			this.AddElementFieldMapping("RoomControlComponentLanguageEntity", "RoomControlComponentLanguageId", "RoomControlComponentLanguageId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("RoomControlComponentLanguageEntity", "ParentCompanyId", "ParentCompanyId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("RoomControlComponentLanguageEntity", "RoomControlComponentId", "RoomControlComponentId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("RoomControlComponentLanguageEntity", "LanguageId", "LanguageId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("RoomControlComponentLanguageEntity", "Name", "Name", true, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 4);
			this.AddElementFieldMapping("RoomControlComponentLanguageEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 5);
			this.AddElementFieldMapping("RoomControlComponentLanguageEntity", "CreatedBy", "CreatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 6);
			this.AddElementFieldMapping("RoomControlComponentLanguageEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 7);
			this.AddElementFieldMapping("RoomControlComponentLanguageEntity", "UpdatedBy", "UpdatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 8);
		}

		/// <summary>Inits RoomControlConfigurationEntity's mappings</summary>
		private void InitRoomControlConfigurationEntityMappings()
		{
			this.AddElementMapping("RoomControlConfigurationEntity", @"Obymobi", @"dbo", "RoomControlConfiguration", 7, 0);
			this.AddElementFieldMapping("RoomControlConfigurationEntity", "RoomControlConfigurationId", "RoomControlConfigurationId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("RoomControlConfigurationEntity", "CompanyId", "CompanyId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("RoomControlConfigurationEntity", "Name", "Name", false, "NVarChar", 200, 0, 0, false, "", null, typeof(System.String), 2);
			this.AddElementFieldMapping("RoomControlConfigurationEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 3);
			this.AddElementFieldMapping("RoomControlConfigurationEntity", "CreatedBy", "CreatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("RoomControlConfigurationEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 5);
			this.AddElementFieldMapping("RoomControlConfigurationEntity", "UpdatedBy", "UpdatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 6);
		}

		/// <summary>Inits RoomControlSectionEntity's mappings</summary>
		private void InitRoomControlSectionEntityMappings()
		{
			this.AddElementMapping("RoomControlSectionEntity", @"Obymobi", @"dbo", "RoomControlSection", 16, 0);
			this.AddElementFieldMapping("RoomControlSectionEntity", "RoomControlSectionId", "RoomControlSectionId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("RoomControlSectionEntity", "ParentCompanyId", "ParentCompanyId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("RoomControlSectionEntity", "RoomControlAreaId", "RoomControlAreaId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("RoomControlSectionEntity", "Name", "Name", true, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 3);
			this.AddElementFieldMapping("RoomControlSectionEntity", "Type", "Type", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("RoomControlSectionEntity", "SortOrder", "SortOrder", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 5);
			this.AddElementFieldMapping("RoomControlSectionEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 6);
			this.AddElementFieldMapping("RoomControlSectionEntity", "CreatedBy", "CreatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 7);
			this.AddElementFieldMapping("RoomControlSectionEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 8);
			this.AddElementFieldMapping("RoomControlSectionEntity", "UpdatedBy", "UpdatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 9);
			this.AddElementFieldMapping("RoomControlSectionEntity", "FieldValue1", "FieldValue1", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 10);
			this.AddElementFieldMapping("RoomControlSectionEntity", "FieldValue2", "FieldValue2", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 11);
			this.AddElementFieldMapping("RoomControlSectionEntity", "FieldValue3", "FieldValue3", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 12);
			this.AddElementFieldMapping("RoomControlSectionEntity", "FieldValue4", "FieldValue4", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 13);
			this.AddElementFieldMapping("RoomControlSectionEntity", "FieldValue5", "FieldValue5", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 14);
			this.AddElementFieldMapping("RoomControlSectionEntity", "Visible", "Visible", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 15);
		}

		/// <summary>Inits RoomControlSectionItemEntity's mappings</summary>
		private void InitRoomControlSectionItemEntityMappings()
		{
			this.AddElementMapping("RoomControlSectionItemEntity", @"Obymobi", @"dbo", "RoomControlSectionItem", 18, 0);
			this.AddElementFieldMapping("RoomControlSectionItemEntity", "RoomControlSectionItemId", "RoomControlSectionItemId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("RoomControlSectionItemEntity", "ParentCompanyId", "ParentCompanyId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("RoomControlSectionItemEntity", "RoomControlSectionId", "RoomControlSectionId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("RoomControlSectionItemEntity", "Name", "Name", true, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 3);
			this.AddElementFieldMapping("RoomControlSectionItemEntity", "Type", "Type", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("RoomControlSectionItemEntity", "SortOrder", "SortOrder", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 5);
			this.AddElementFieldMapping("RoomControlSectionItemEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 6);
			this.AddElementFieldMapping("RoomControlSectionItemEntity", "CreatedBy", "CreatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 7);
			this.AddElementFieldMapping("RoomControlSectionItemEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 8);
			this.AddElementFieldMapping("RoomControlSectionItemEntity", "UpdatedBy", "UpdatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 9);
			this.AddElementFieldMapping("RoomControlSectionItemEntity", "StationListId", "StationListId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 10);
			this.AddElementFieldMapping("RoomControlSectionItemEntity", "FieldValue1", "FieldValue1", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 11);
			this.AddElementFieldMapping("RoomControlSectionItemEntity", "FieldValue2", "FieldValue2", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 12);
			this.AddElementFieldMapping("RoomControlSectionItemEntity", "FieldValue3", "FieldValue3", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 13);
			this.AddElementFieldMapping("RoomControlSectionItemEntity", "FieldValue4", "FieldValue4", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 14);
			this.AddElementFieldMapping("RoomControlSectionItemEntity", "FieldValue5", "FieldValue5", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 15);
			this.AddElementFieldMapping("RoomControlSectionItemEntity", "Visible", "Visible", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 16);
			this.AddElementFieldMapping("RoomControlSectionItemEntity", "InfraredConfigurationId", "InfraredConfigurationId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 17);
		}

		/// <summary>Inits RoomControlSectionItemLanguageEntity's mappings</summary>
		private void InitRoomControlSectionItemLanguageEntityMappings()
		{
			this.AddElementMapping("RoomControlSectionItemLanguageEntity", @"Obymobi", @"dbo", "RoomControlSectionItemLanguage", 9, 0);
			this.AddElementFieldMapping("RoomControlSectionItemLanguageEntity", "RoomControlSectionItemLanguageId", "RoomControlSectionItemLanguageId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("RoomControlSectionItemLanguageEntity", "ParentCompanyId", "ParentCompanyId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("RoomControlSectionItemLanguageEntity", "RoomControlSectionItemId", "RoomControlSectionItemId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("RoomControlSectionItemLanguageEntity", "LanguageId", "LanguageId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("RoomControlSectionItemLanguageEntity", "Name", "Name", true, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 4);
			this.AddElementFieldMapping("RoomControlSectionItemLanguageEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 5);
			this.AddElementFieldMapping("RoomControlSectionItemLanguageEntity", "CreatedBy", "CreatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 6);
			this.AddElementFieldMapping("RoomControlSectionItemLanguageEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 7);
			this.AddElementFieldMapping("RoomControlSectionItemLanguageEntity", "UpdatedBy", "UpdatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 8);
		}

		/// <summary>Inits RoomControlSectionLanguageEntity's mappings</summary>
		private void InitRoomControlSectionLanguageEntityMappings()
		{
			this.AddElementMapping("RoomControlSectionLanguageEntity", @"Obymobi", @"dbo", "RoomControlSectionLanguage", 9, 0);
			this.AddElementFieldMapping("RoomControlSectionLanguageEntity", "RoomControlSectionLanguageId", "RoomControlSectionLanguageId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("RoomControlSectionLanguageEntity", "ParentCompanyId", "ParentCompanyId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("RoomControlSectionLanguageEntity", "RoomControlSectionId", "RoomControlSectionId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("RoomControlSectionLanguageEntity", "LanguageId", "LanguageId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("RoomControlSectionLanguageEntity", "Name", "Name", true, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 4);
			this.AddElementFieldMapping("RoomControlSectionLanguageEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 5);
			this.AddElementFieldMapping("RoomControlSectionLanguageEntity", "CreatedBy", "CreatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 6);
			this.AddElementFieldMapping("RoomControlSectionLanguageEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 7);
			this.AddElementFieldMapping("RoomControlSectionLanguageEntity", "UpdatedBy", "UpdatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 8);
		}

		/// <summary>Inits RoomControlWidgetEntity's mappings</summary>
		private void InitRoomControlWidgetEntityMappings()
		{
			this.AddElementMapping("RoomControlWidgetEntity", @"Obymobi", @"dbo", "RoomControlWidget", 33, 0);
			this.AddElementFieldMapping("RoomControlWidgetEntity", "RoomControlWidgetId", "RoomControlWidgetId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("RoomControlWidgetEntity", "ParentCompanyId", "ParentCompanyId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("RoomControlWidgetEntity", "RoomControlSectionId", "RoomControlSectionId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("RoomControlWidgetEntity", "RoomControlComponentId1", "RoomControlComponentId1", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("RoomControlWidgetEntity", "Caption", "Caption", true, "VarChar", 50, 0, 0, false, "", null, typeof(System.String), 4);
			this.AddElementFieldMapping("RoomControlWidgetEntity", "Type", "Type", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 5);
			this.AddElementFieldMapping("RoomControlWidgetEntity", "SortOrder", "SortOrder", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 6);
			this.AddElementFieldMapping("RoomControlWidgetEntity", "Visible", "Visible", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 7);
			this.AddElementFieldMapping("RoomControlWidgetEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 8);
			this.AddElementFieldMapping("RoomControlWidgetEntity", "CreatedBy", "CreatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 9);
			this.AddElementFieldMapping("RoomControlWidgetEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 10);
			this.AddElementFieldMapping("RoomControlWidgetEntity", "UpdatedBy", "UpdatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 11);
			this.AddElementFieldMapping("RoomControlWidgetEntity", "FieldValue1", "FieldValue1", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 12);
			this.AddElementFieldMapping("RoomControlWidgetEntity", "FieldValue2", "FieldValue2", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 13);
			this.AddElementFieldMapping("RoomControlWidgetEntity", "FieldValue3", "FieldValue3", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 14);
			this.AddElementFieldMapping("RoomControlWidgetEntity", "FieldValue4", "FieldValue4", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 15);
			this.AddElementFieldMapping("RoomControlWidgetEntity", "FieldValue5", "FieldValue5", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 16);
			this.AddElementFieldMapping("RoomControlWidgetEntity", "FieldValue6", "FieldValue6", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 17);
			this.AddElementFieldMapping("RoomControlWidgetEntity", "FieldValue7", "FieldValue7", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 18);
			this.AddElementFieldMapping("RoomControlWidgetEntity", "FieldValue8", "FieldValue8", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 19);
			this.AddElementFieldMapping("RoomControlWidgetEntity", "FieldValue9", "FieldValue9", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 20);
			this.AddElementFieldMapping("RoomControlWidgetEntity", "FieldValue10", "FieldValue10", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 21);
			this.AddElementFieldMapping("RoomControlWidgetEntity", "RoomControlSectionItemId", "RoomControlSectionItemId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 22);
			this.AddElementFieldMapping("RoomControlWidgetEntity", "RoomControlComponentId10", "RoomControlComponentId10", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 23);
			this.AddElementFieldMapping("RoomControlWidgetEntity", "RoomControlComponentId2", "RoomControlComponentId2", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 24);
			this.AddElementFieldMapping("RoomControlWidgetEntity", "RoomControlComponentId3", "RoomControlComponentId3", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 25);
			this.AddElementFieldMapping("RoomControlWidgetEntity", "RoomControlComponentId4", "RoomControlComponentId4", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 26);
			this.AddElementFieldMapping("RoomControlWidgetEntity", "RoomControlComponentId5", "RoomControlComponentId5", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 27);
			this.AddElementFieldMapping("RoomControlWidgetEntity", "RoomControlComponentId6", "RoomControlComponentId6", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 28);
			this.AddElementFieldMapping("RoomControlWidgetEntity", "RoomControlComponentId7", "RoomControlComponentId7", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 29);
			this.AddElementFieldMapping("RoomControlWidgetEntity", "RoomControlComponentId8", "RoomControlComponentId8", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 30);
			this.AddElementFieldMapping("RoomControlWidgetEntity", "RoomControlComponentId9", "RoomControlComponentId9", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 31);
			this.AddElementFieldMapping("RoomControlWidgetEntity", "InfraredConfigurationId", "InfraredConfigurationId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 32);
		}

		/// <summary>Inits RoomControlWidgetLanguageEntity's mappings</summary>
		private void InitRoomControlWidgetLanguageEntityMappings()
		{
			this.AddElementMapping("RoomControlWidgetLanguageEntity", @"Obymobi", @"dbo", "RoomControlWidgetLanguage", 9, 0);
			this.AddElementFieldMapping("RoomControlWidgetLanguageEntity", "RoomControlWidgetLanguageId", "RoomControlWidgetLanguageId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("RoomControlWidgetLanguageEntity", "ParentCompanyId", "ParentCompanyId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("RoomControlWidgetLanguageEntity", "RoomControlWidgetId", "RoomControlWidgetId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("RoomControlWidgetLanguageEntity", "LanguageId", "LanguageId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("RoomControlWidgetLanguageEntity", "Caption", "Caption", true, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 4);
			this.AddElementFieldMapping("RoomControlWidgetLanguageEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 5);
			this.AddElementFieldMapping("RoomControlWidgetLanguageEntity", "CreatedBy", "CreatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 6);
			this.AddElementFieldMapping("RoomControlWidgetLanguageEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 7);
			this.AddElementFieldMapping("RoomControlWidgetLanguageEntity", "UpdatedBy", "UpdatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 8);
		}

		/// <summary>Inits RouteEntity's mappings</summary>
		private void InitRouteEntityMappings()
		{
			this.AddElementMapping("RouteEntity", @"Obymobi", @"dbo", "Route", 12, 0);
			this.AddElementFieldMapping("RouteEntity", "RouteId", "RouteId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("RouteEntity", "CompanyId", "CompanyId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("RouteEntity", "Name", "Name", true, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 2);
			this.AddElementFieldMapping("RouteEntity", "CreatedBy", "CreatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("RouteEntity", "UpdatedBy", "UpdatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("RouteEntity", "LogAlways", "LogAlways", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 5);
			this.AddElementFieldMapping("RouteEntity", "StepTimeoutMinutes", "StepTimeoutMinutes", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 6);
			this.AddElementFieldMapping("RouteEntity", "EscalationRouteId", "EscalationRouteId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 7);
			this.AddElementFieldMapping("RouteEntity", "BeingHandledSupportNotificationTimeoutMinutes", "BeingHandledSupportNotificationTimeoutMinutes", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 8);
			this.AddElementFieldMapping("RouteEntity", "RetrievalSupportNotificationTimeoutMinutes", "RetrievalSupportNotificationTimeoutMinutes", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 9);
			this.AddElementFieldMapping("RouteEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 10);
			this.AddElementFieldMapping("RouteEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 11);
		}

		/// <summary>Inits RoutestepEntity's mappings</summary>
		private void InitRoutestepEntityMappings()
		{
			this.AddElementMapping("RoutestepEntity", @"Obymobi", @"dbo", "Routestep", 11, 0);
			this.AddElementFieldMapping("RoutestepEntity", "RoutestepId", "RoutestepId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("RoutestepEntity", "RouteId", "RouteId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("RoutestepEntity", "Number", "Number", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("RoutestepEntity", "CreatedBy", "CreatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("RoutestepEntity", "UpdatedBy", "UpdatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("RoutestepEntity", "TimeoutMinutes", "TimeoutMinutes", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 5);
			this.AddElementFieldMapping("RoutestepEntity", "ContinueOnFailure", "ContinueOnFailure", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 6);
			this.AddElementFieldMapping("RoutestepEntity", "CompleteRouteOnComplete", "CompleteRouteOnComplete", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 7);
			this.AddElementFieldMapping("RoutestepEntity", "ParentCompanyId", "ParentCompanyId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 8);
			this.AddElementFieldMapping("RoutestepEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 9);
			this.AddElementFieldMapping("RoutestepEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 10);
		}

		/// <summary>Inits RoutestephandlerEntity's mappings</summary>
		private void InitRoutestephandlerEntityMappings()
		{
			this.AddElementMapping("RoutestephandlerEntity", @"Obymobi", @"dbo", "Routestephandler", 22, 0);
			this.AddElementFieldMapping("RoutestephandlerEntity", "RoutestephandlerId", "RoutestephandlerId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("RoutestephandlerEntity", "RoutestepId", "RoutestepId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("RoutestephandlerEntity", "TerminalId", "TerminalId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("RoutestephandlerEntity", "HandlerType", "HandlerType", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("RoutestephandlerEntity", "PrintReportType", "PrintReportType", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("RoutestephandlerEntity", "FieldValue1", "FieldValue1", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 5);
			this.AddElementFieldMapping("RoutestephandlerEntity", "FieldValue2", "FieldValue2", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 6);
			this.AddElementFieldMapping("RoutestephandlerEntity", "FieldValue3", "FieldValue3", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 7);
			this.AddElementFieldMapping("RoutestephandlerEntity", "FieldValue4", "FieldValue4", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 8);
			this.AddElementFieldMapping("RoutestephandlerEntity", "FieldValue5", "FieldValue5", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 9);
			this.AddElementFieldMapping("RoutestephandlerEntity", "FieldValue6", "FieldValue6", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 10);
			this.AddElementFieldMapping("RoutestephandlerEntity", "FieldValue7", "FieldValue7", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 11);
			this.AddElementFieldMapping("RoutestephandlerEntity", "FieldValue8", "FieldValue8", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 12);
			this.AddElementFieldMapping("RoutestephandlerEntity", "FieldValue9", "FieldValue9", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 13);
			this.AddElementFieldMapping("RoutestephandlerEntity", "FieldValue10", "FieldValue10", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 14);
			this.AddElementFieldMapping("RoutestephandlerEntity", "CreatedBy", "CreatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 15);
			this.AddElementFieldMapping("RoutestephandlerEntity", "UpdatedBy", "UpdatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 16);
			this.AddElementFieldMapping("RoutestephandlerEntity", "SupportpoolId", "SupportpoolId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 17);
			this.AddElementFieldMapping("RoutestephandlerEntity", "ParentCompanyId", "ParentCompanyId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 18);
			this.AddElementFieldMapping("RoutestephandlerEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 19);
			this.AddElementFieldMapping("RoutestephandlerEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 20);
			this.AddElementFieldMapping("RoutestephandlerEntity", "ExternalSystemId", "ExternalSystemId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 21);
		}

		/// <summary>Inits ScheduleEntity's mappings</summary>
		private void InitScheduleEntityMappings()
		{
			this.AddElementMapping("ScheduleEntity", @"Obymobi", @"dbo", "Schedule", 5, 0);
			this.AddElementFieldMapping("ScheduleEntity", "ScheduleId", "ScheduleId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("ScheduleEntity", "Name", "Name", false, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 1);
			this.AddElementFieldMapping("ScheduleEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 2);
			this.AddElementFieldMapping("ScheduleEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 3);
			this.AddElementFieldMapping("ScheduleEntity", "CompanyId", "CompanyId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
		}

		/// <summary>Inits ScheduledCommandEntity's mappings</summary>
		private void InitScheduledCommandEntityMappings()
		{
			this.AddElementMapping("ScheduledCommandEntity", @"Obymobi", @"dbo", "ScheduledCommand", 16, 0);
			this.AddElementFieldMapping("ScheduledCommandEntity", "ScheduledCommandId", "ScheduledCommandId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("ScheduledCommandEntity", "ScheduledCommandTaskId", "ScheduledCommandTaskId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("ScheduledCommandEntity", "ClientCommand", "ClientCommand", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("ScheduledCommandEntity", "ClientId", "ClientId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("ScheduledCommandEntity", "TerminalCommand", "TerminalCommand", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("ScheduledCommandEntity", "TerminalId", "TerminalId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 5);
			this.AddElementFieldMapping("ScheduledCommandEntity", "Status", "Status", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 6);
			this.AddElementFieldMapping("ScheduledCommandEntity", "CreatedBy", "CreatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 7);
			this.AddElementFieldMapping("ScheduledCommandEntity", "UpdatedBy", "UpdatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 8);
			this.AddElementFieldMapping("ScheduledCommandEntity", "Sort", "Sort", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 9);
			this.AddElementFieldMapping("ScheduledCommandEntity", "ParentCompanyId", "ParentCompanyId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 10);
			this.AddElementFieldMapping("ScheduledCommandEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 11);
			this.AddElementFieldMapping("ScheduledCommandEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 12);
			this.AddElementFieldMapping("ScheduledCommandEntity", "StartedUTC", "StartedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 13);
			this.AddElementFieldMapping("ScheduledCommandEntity", "ExpiresUTC", "ExpiresUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 14);
			this.AddElementFieldMapping("ScheduledCommandEntity", "CompletedUTC", "CompletedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 15);
		}

		/// <summary>Inits ScheduledCommandTaskEntity's mappings</summary>
		private void InitScheduledCommandTaskEntityMappings()
		{
			this.AddElementMapping("ScheduledCommandTaskEntity", @"Obymobi", @"dbo", "ScheduledCommandTask", 15, 0);
			this.AddElementFieldMapping("ScheduledCommandTaskEntity", "ScheduledCommandTaskId", "ScheduledCommandTaskId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("ScheduledCommandTaskEntity", "CompanyId", "CompanyId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("ScheduledCommandTaskEntity", "Active", "Active", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 2);
			this.AddElementFieldMapping("ScheduledCommandTaskEntity", "Status", "Status", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("ScheduledCommandTaskEntity", "Notification", "Notification", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 4);
			this.AddElementFieldMapping("ScheduledCommandTaskEntity", "NotificationEmailAddresses", "NotificationEmailAddresses", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 5);
			this.AddElementFieldMapping("ScheduledCommandTaskEntity", "CreatedBy", "CreatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 6);
			this.AddElementFieldMapping("ScheduledCommandTaskEntity", "UpdatedBy", "UpdatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 7);
			this.AddElementFieldMapping("ScheduledCommandTaskEntity", "CommandCount", "CommandCount", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 8);
			this.AddElementFieldMapping("ScheduledCommandTaskEntity", "FinishedCount", "FinishedCount", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 9);
			this.AddElementFieldMapping("ScheduledCommandTaskEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 10);
			this.AddElementFieldMapping("ScheduledCommandTaskEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 11);
			this.AddElementFieldMapping("ScheduledCommandTaskEntity", "CompletedUTC", "CompletedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 12);
			this.AddElementFieldMapping("ScheduledCommandTaskEntity", "StartUTC", "StartUTC", false, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 13);
			this.AddElementFieldMapping("ScheduledCommandTaskEntity", "ExpiresUTC", "ExpiresUTC", false, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 14);
		}

		/// <summary>Inits ScheduledMessageEntity's mappings</summary>
		private void InitScheduledMessageEntityMappings()
		{
			this.AddElementMapping("ScheduledMessageEntity", @"Obymobi", @"dbo", "ScheduledMessage", 26, 0);
			this.AddElementFieldMapping("ScheduledMessageEntity", "ScheduledMessageId", "ScheduledMessageId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("ScheduledMessageEntity", "CompanyId", "CompanyId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("ScheduledMessageEntity", "Title", "Title", true, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 2);
			this.AddElementFieldMapping("ScheduledMessageEntity", "Message", "Message", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 3);
			this.AddElementFieldMapping("ScheduledMessageEntity", "MediaId", "MediaId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("ScheduledMessageEntity", "EntertainmentId", "EntertainmentId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 5);
			this.AddElementFieldMapping("ScheduledMessageEntity", "CategoryId", "CategoryId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 6);
			this.AddElementFieldMapping("ScheduledMessageEntity", "ProductCategoryId", "ProductCategoryId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 7);
			this.AddElementFieldMapping("ScheduledMessageEntity", "SiteId", "SiteId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 8);
			this.AddElementFieldMapping("ScheduledMessageEntity", "PageId", "PageId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 9);
			this.AddElementFieldMapping("ScheduledMessageEntity", "Url", "Url", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 10);
			this.AddElementFieldMapping("ScheduledMessageEntity", "Urgent", "Urgent", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 11);
			this.AddElementFieldMapping("ScheduledMessageEntity", "MessageButtonType", "MessageButtonType", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 12);
			this.AddElementFieldMapping("ScheduledMessageEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 13);
			this.AddElementFieldMapping("ScheduledMessageEntity", "CreatedBy", "CreatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 14);
			this.AddElementFieldMapping("ScheduledMessageEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 15);
			this.AddElementFieldMapping("ScheduledMessageEntity", "UpdatedBy", "UpdatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 16);
			this.AddElementFieldMapping("ScheduledMessageEntity", "NotifyOnYes", "NotifyOnYes", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 17);
			this.AddElementFieldMapping("ScheduledMessageEntity", "FriendlyName", "FriendlyName", true, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 18);
			this.AddElementFieldMapping("ScheduledMessageEntity", "ManualTitleEnabled", "ManualTitleEnabled", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 19);
			this.AddElementFieldMapping("ScheduledMessageEntity", "ManualFriendlyNameEnabled", "ManualFriendlyNameEnabled", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 20);
			this.AddElementFieldMapping("ScheduledMessageEntity", "ManualMessageEnabled", "ManualMessageEnabled", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 21);
			this.AddElementFieldMapping("ScheduledMessageEntity", "ManualMediaEnabled", "ManualMediaEnabled", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 22);
			this.AddElementFieldMapping("ScheduledMessageEntity", "MessageTemplateId", "MessageTemplateId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 23);
			this.AddElementFieldMapping("ScheduledMessageEntity", "MessageLayoutType", "MessageLayoutType", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 24);
			this.AddElementFieldMapping("ScheduledMessageEntity", "ManualMessageLayoutTypeEnabled", "ManualMessageLayoutTypeEnabled", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 25);
		}

		/// <summary>Inits ScheduledMessageHistoryEntity's mappings</summary>
		private void InitScheduledMessageHistoryEntityMappings()
		{
			this.AddElementMapping("ScheduledMessageHistoryEntity", @"Obymobi", @"dbo", "ScheduledMessageHistory", 15, 0);
			this.AddElementFieldMapping("ScheduledMessageHistoryEntity", "ScheduledMessageHistoryId", "ScheduledMessageHistoryId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("ScheduledMessageHistoryEntity", "ParentCompanyId", "ParentCompanyId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("ScheduledMessageHistoryEntity", "ScheduledMessageId", "ScheduledMessageId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("ScheduledMessageHistoryEntity", "MessagegroupId", "MessagegroupId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("ScheduledMessageHistoryEntity", "Sent", "Sent", false, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 4);
			this.AddElementFieldMapping("ScheduledMessageHistoryEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 5);
			this.AddElementFieldMapping("ScheduledMessageHistoryEntity", "CreatedBy", "CreatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 6);
			this.AddElementFieldMapping("ScheduledMessageHistoryEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 7);
			this.AddElementFieldMapping("ScheduledMessageHistoryEntity", "UpdatedBy", "UpdatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 8);
			this.AddElementFieldMapping("ScheduledMessageHistoryEntity", "Title", "Title", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 9);
			this.AddElementFieldMapping("ScheduledMessageHistoryEntity", "Message", "Message", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 10);
			this.AddElementFieldMapping("ScheduledMessageHistoryEntity", "RecipientsAsString", "RecipientsAsString", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 11);
			this.AddElementFieldMapping("ScheduledMessageHistoryEntity", "UIScheduleName", "UIScheduleName", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 12);
			this.AddElementFieldMapping("ScheduledMessageHistoryEntity", "MessageId", "MessageId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 13);
			this.AddElementFieldMapping("ScheduledMessageHistoryEntity", "UIScheduleItemOccurrenceId", "UIScheduleItemOccurrenceId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 14);
		}

		/// <summary>Inits ScheduledMessageLanguageEntity's mappings</summary>
		private void InitScheduledMessageLanguageEntityMappings()
		{
			this.AddElementMapping("ScheduledMessageLanguageEntity", @"Obymobi", @"dbo", "ScheduledMessageLanguage", 10, 0);
			this.AddElementFieldMapping("ScheduledMessageLanguageEntity", "ScheduledMessageLanguageId", "ScheduledMessageLanguageId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("ScheduledMessageLanguageEntity", "ParentCompanyId", "ParentCompanyId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("ScheduledMessageLanguageEntity", "ScheduledMessageId", "ScheduledMessageId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("ScheduledMessageLanguageEntity", "LanguageId", "LanguageId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("ScheduledMessageLanguageEntity", "Title", "Title", true, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 4);
			this.AddElementFieldMapping("ScheduledMessageLanguageEntity", "Message", "Message", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 5);
			this.AddElementFieldMapping("ScheduledMessageLanguageEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 6);
			this.AddElementFieldMapping("ScheduledMessageLanguageEntity", "CreatedBy", "CreatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 7);
			this.AddElementFieldMapping("ScheduledMessageLanguageEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 8);
			this.AddElementFieldMapping("ScheduledMessageLanguageEntity", "UpdatedBy", "UpdatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 9);
		}

		/// <summary>Inits ScheduleitemEntity's mappings</summary>
		private void InitScheduleitemEntityMappings()
		{
			this.AddElementMapping("ScheduleitemEntity", @"Obymobi", @"dbo", "Scheduleitem", 11, 0);
			this.AddElementFieldMapping("ScheduleitemEntity", "ScheduleitemId", "ScheduleitemId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("ScheduleitemEntity", "ScheduleId", "ScheduleId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("ScheduleitemEntity", "TimeStart", "TimeStart", false, "Char", 4, 0, 0, false, "", null, typeof(System.String), 2);
			this.AddElementFieldMapping("ScheduleitemEntity", "TimeEnd", "TimeEnd", false, "Char", 4, 0, 0, false, "", null, typeof(System.String), 3);
			this.AddElementFieldMapping("ScheduleitemEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 4);
			this.AddElementFieldMapping("ScheduleitemEntity", "CreatedBy", "CreatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 5);
			this.AddElementFieldMapping("ScheduleitemEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 6);
			this.AddElementFieldMapping("ScheduleitemEntity", "UpdatedBy", "UpdatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 7);
			this.AddElementFieldMapping("ScheduleitemEntity", "SortOrder", "SortOrder", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 8);
			this.AddElementFieldMapping("ScheduleitemEntity", "DayOfWeek", "DayOfWeek", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 9);
			this.AddElementFieldMapping("ScheduleitemEntity", "ParentCompanyId", "ParentCompanyId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 10);
		}

		/// <summary>Inits ServiceMethodEntity's mappings</summary>
		private void InitServiceMethodEntityMappings()
		{
			this.AddElementMapping("ServiceMethodEntity", @"Obymobi", @"dbo", "ServiceMethod", 28, 0);
			this.AddElementFieldMapping("ServiceMethodEntity", "ServiceMethodId", "ServiceMethodId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("ServiceMethodEntity", "CompanyId", "CompanyId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("ServiceMethodEntity", "OutletId", "OutletId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("ServiceMethodEntity", "Active", "Active", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 3);
			this.AddElementFieldMapping("ServiceMethodEntity", "Type", "Type", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("ServiceMethodEntity", "Name", "Name", true, "NVarChar", 256, 0, 0, false, "", null, typeof(System.String), 5);
			this.AddElementFieldMapping("ServiceMethodEntity", "Label", "Label", true, "NVarChar", 256, 0, 0, false, "", null, typeof(System.String), 6);
			this.AddElementFieldMapping("ServiceMethodEntity", "Description", "Description", true, "NVarChar", 256, 0, 0, false, "", null, typeof(System.String), 7);
			this.AddElementFieldMapping("ServiceMethodEntity", "AskEntryPermission", "AskEntryPermission", true, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 8);
			this.AddElementFieldMapping("ServiceMethodEntity", "ServiceChargePercentage", "ServiceChargePercentage", true, "Float", 0, 38, 0, false, "", null, typeof(System.Double), 9);
			this.AddElementFieldMapping("ServiceMethodEntity", "ServiceChargeAmount", "ServiceChargeAmount", true, "Money", 0, 19, 4, false, "", null, typeof(System.Decimal), 10);
			this.AddElementFieldMapping("ServiceMethodEntity", "ServiceChargeProductId", "ServiceChargeProductId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 11);
			this.AddElementFieldMapping("ServiceMethodEntity", "ConfirmationActive", "ConfirmationActive", true, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 12);
			this.AddElementFieldMapping("ServiceMethodEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 13);
			this.AddElementFieldMapping("ServiceMethodEntity", "CreatedBy", "CreatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 14);
			this.AddElementFieldMapping("ServiceMethodEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 15);
			this.AddElementFieldMapping("ServiceMethodEntity", "UpdatedBy", "UpdatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 16);
			this.AddElementFieldMapping("ServiceMethodEntity", "MinimumOrderAmount", "MinimumOrderAmount", false, "Money", 0, 19, 4, false, "", null, typeof(System.Decimal), 17);
			this.AddElementFieldMapping("ServiceMethodEntity", "FreeDeliveryAmount", "FreeDeliveryAmount", false, "Money", 0, 19, 4, false, "", null, typeof(System.Decimal), 18);
			this.AddElementFieldMapping("ServiceMethodEntity", "ServiceChargeType", "ServiceChargeType", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 19);
			this.AddElementFieldMapping("ServiceMethodEntity", "MinimumAmountForFreeDelivery", "MinimumAmountForFreeDelivery", false, "Money", 0, 19, 4, false, "", null, typeof(System.Decimal), 20);
			this.AddElementFieldMapping("ServiceMethodEntity", "MinimumAmountForFreeServiceCharge", "MinimumAmountForFreeServiceCharge", false, "Money", 0, 19, 4, false, "", null, typeof(System.Decimal), 21);
			this.AddElementFieldMapping("ServiceMethodEntity", "OrderCompleteTextMessage", "OrderCompleteTextMessage", true, "NVarChar", 320, 0, 0, false, "", null, typeof(System.String), 22);
			this.AddElementFieldMapping("ServiceMethodEntity", "OrderCompleteMailSubject", "OrderCompleteMailSubject", true, "NVarChar", 250, 0, 0, false, "", null, typeof(System.String), 23);
			this.AddElementFieldMapping("ServiceMethodEntity", "OrderCompleteMailMessage", "OrderCompleteMailMessage", true, "NVarChar", 2048, 0, 0, false, "", null, typeof(System.String), 24);
			this.AddElementFieldMapping("ServiceMethodEntity", "OrderCompleteNotificationMethod", "OrderCompleteNotificationMethod", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 25);
			this.AddElementFieldMapping("ServiceMethodEntity", "MaxCovers", "MaxCovers", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 26);
			this.AddElementFieldMapping("ServiceMethodEntity", "CoversType", "CoversType", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 27);
		}

		/// <summary>Inits ServiceMethodDeliverypointgroupEntity's mappings</summary>
		private void InitServiceMethodDeliverypointgroupEntityMappings()
		{
			this.AddElementMapping("ServiceMethodDeliverypointgroupEntity", @"Obymobi", @"dbo", "ServiceMethodDeliverypointgroup", 7, 0);
			this.AddElementFieldMapping("ServiceMethodDeliverypointgroupEntity", "ServiceMethodId", "ServiceMethodId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("ServiceMethodDeliverypointgroupEntity", "DeliverypointgroupId", "DeliverypointgroupId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("ServiceMethodDeliverypointgroupEntity", "ParentCompanyId", "ParentCompanyId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("ServiceMethodDeliverypointgroupEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 3);
			this.AddElementFieldMapping("ServiceMethodDeliverypointgroupEntity", "CreatedBy", "CreatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("ServiceMethodDeliverypointgroupEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 5);
			this.AddElementFieldMapping("ServiceMethodDeliverypointgroupEntity", "UpdatedBy", "UpdatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 6);
		}

		/// <summary>Inits SetupCodeEntity's mappings</summary>
		private void InitSetupCodeEntityMappings()
		{
			this.AddElementMapping("SetupCodeEntity", @"Obymobi", @"dbo", "SetupCode", 9, 0);
			this.AddElementFieldMapping("SetupCodeEntity", "SetupCodeId", "SetupCodeId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("SetupCodeEntity", "CompanyId", "CompanyId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("SetupCodeEntity", "DeliverypointgroupId", "DeliverypointgroupId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("SetupCodeEntity", "Code", "Code", false, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 3);
			this.AddElementFieldMapping("SetupCodeEntity", "CreatedBy", "CreatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("SetupCodeEntity", "UpdatedBy", "UpdatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 5);
			this.AddElementFieldMapping("SetupCodeEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 6);
			this.AddElementFieldMapping("SetupCodeEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 7);
			this.AddElementFieldMapping("SetupCodeEntity", "ExpireDateUTC", "ExpireDateUTC", false, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 8);
		}

		/// <summary>Inits SiteEntity's mappings</summary>
		private void InitSiteEntityMappings()
		{
			this.AddElementMapping("SiteEntity", @"Obymobi", @"dbo", "Site", 12, 0);
			this.AddElementFieldMapping("SiteEntity", "SiteId", "SiteId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("SiteEntity", "Name", "Name", true, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 1);
			this.AddElementFieldMapping("SiteEntity", "SiteType", "SiteType", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("SiteEntity", "SiteTemplateId", "SiteTemplateId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("SiteEntity", "CompanyId", "CompanyId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("SiteEntity", "CreatedBy", "CreatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 5);
			this.AddElementFieldMapping("SiteEntity", "UpdatedBy", "UpdatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 6);
			this.AddElementFieldMapping("SiteEntity", "Description", "Description", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 7);
			this.AddElementFieldMapping("SiteEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 8);
			this.AddElementFieldMapping("SiteEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 9);
			this.AddElementFieldMapping("SiteEntity", "LastModifiedUTC", "LastModifiedUTC", false, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 10);
			this.AddElementFieldMapping("SiteEntity", "Version", "Version", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 11);
		}

		/// <summary>Inits SiteCultureEntity's mappings</summary>
		private void InitSiteCultureEntityMappings()
		{
			this.AddElementMapping("SiteCultureEntity", @"Obymobi", @"dbo", "SiteCulture", 8, 0);
			this.AddElementFieldMapping("SiteCultureEntity", "SiteCultureId", "SiteCultureId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("SiteCultureEntity", "SiteId", "SiteId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("SiteCultureEntity", "CultureCode", "CultureCode", false, "NVarChar", 10, 0, 0, false, "", null, typeof(System.String), 2);
			this.AddElementFieldMapping("SiteCultureEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 3);
			this.AddElementFieldMapping("SiteCultureEntity", "CreatedBy", "CreatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("SiteCultureEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 5);
			this.AddElementFieldMapping("SiteCultureEntity", "UpdatedBy", "UpdatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 6);
			this.AddElementFieldMapping("SiteCultureEntity", "ParentCompanyId", "ParentCompanyId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 7);
		}

		/// <summary>Inits SiteLanguageEntity's mappings</summary>
		private void InitSiteLanguageEntityMappings()
		{
			this.AddElementMapping("SiteLanguageEntity", @"Obymobi", @"dbo", "SiteLanguage", 9, 0);
			this.AddElementFieldMapping("SiteLanguageEntity", "SiteLanguageId", "SiteLanguageId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("SiteLanguageEntity", "SiteId", "SiteId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("SiteLanguageEntity", "LanguageId", "LanguageId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("SiteLanguageEntity", "CreatedBy", "CreatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("SiteLanguageEntity", "UpdatedBy", "UpdatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("SiteLanguageEntity", "Description", "Description", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 5);
			this.AddElementFieldMapping("SiteLanguageEntity", "ParentCompanyId", "ParentCompanyId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 6);
			this.AddElementFieldMapping("SiteLanguageEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 7);
			this.AddElementFieldMapping("SiteLanguageEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 8);
		}

		/// <summary>Inits SiteTemplateEntity's mappings</summary>
		private void InitSiteTemplateEntityMappings()
		{
			this.AddElementMapping("SiteTemplateEntity", @"Obymobi", @"dbo", "SiteTemplate", 7, 0);
			this.AddElementFieldMapping("SiteTemplateEntity", "SiteTemplateId", "SiteTemplateId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("SiteTemplateEntity", "Name", "Name", false, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 1);
			this.AddElementFieldMapping("SiteTemplateEntity", "SiteType", "SiteType", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("SiteTemplateEntity", "CreatedBy", "CreatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("SiteTemplateEntity", "UpdatedBy", "UpdatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("SiteTemplateEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 5);
			this.AddElementFieldMapping("SiteTemplateEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 6);
		}

		/// <summary>Inits SiteTemplateCultureEntity's mappings</summary>
		private void InitSiteTemplateCultureEntityMappings()
		{
			this.AddElementMapping("SiteTemplateCultureEntity", @"Obymobi", @"dbo", "SiteTemplateCulture", 7, 0);
			this.AddElementFieldMapping("SiteTemplateCultureEntity", "SiteTemplateCultureId", "SiteTemplateCultureId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("SiteTemplateCultureEntity", "SiteTemplateId", "SiteTemplateId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("SiteTemplateCultureEntity", "CultureCode", "CultureCode", false, "NVarChar", 10, 0, 0, false, "", null, typeof(System.String), 2);
			this.AddElementFieldMapping("SiteTemplateCultureEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 3);
			this.AddElementFieldMapping("SiteTemplateCultureEntity", "CreatedBy", "CreatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("SiteTemplateCultureEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 5);
			this.AddElementFieldMapping("SiteTemplateCultureEntity", "UpdatedBy", "UpdatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 6);
		}

		/// <summary>Inits SiteTemplateLanguageEntity's mappings</summary>
		private void InitSiteTemplateLanguageEntityMappings()
		{
			this.AddElementMapping("SiteTemplateLanguageEntity", @"Obymobi", @"dbo", "SiteTemplateLanguage", 8, 0);
			this.AddElementFieldMapping("SiteTemplateLanguageEntity", "SiteTemplateLanguageId", "SiteTemplateLanguageId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("SiteTemplateLanguageEntity", "SiteTemplateId", "SiteTemplateId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("SiteTemplateLanguageEntity", "LanguageId", "LanguageId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("SiteTemplateLanguageEntity", "Name", "Name", true, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 3);
			this.AddElementFieldMapping("SiteTemplateLanguageEntity", "CreatedBy", "CreatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("SiteTemplateLanguageEntity", "UpdatedBy", "UpdatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 5);
			this.AddElementFieldMapping("SiteTemplateLanguageEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 6);
			this.AddElementFieldMapping("SiteTemplateLanguageEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 7);
		}

		/// <summary>Inits SmsInformationEntity's mappings</summary>
		private void InitSmsInformationEntityMappings()
		{
			this.AddElementMapping("SmsInformationEntity", @"Obymobi", @"dbo", "SmsInformation", 11, 0);
			this.AddElementFieldMapping("SmsInformationEntity", "SmsInformationId", "SmsInformationId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("SmsInformationEntity", "Originator", "Originator", false, "NVarChar", 9, 0, 0, false, "", null, typeof(System.String), 1);
			this.AddElementFieldMapping("SmsInformationEntity", "BodyText", "BodyText", false, "NVarChar", 160, 0, 0, false, "", null, typeof(System.String), 2);
			this.AddElementFieldMapping("SmsInformationEntity", "GooglePlayPackage", "GooglePlayPackage", false, "NVarChar", 100, 0, 0, false, "", null, typeof(System.String), 3);
			this.AddElementFieldMapping("SmsInformationEntity", "AppStoreAppId", "AppStoreAppId", false, "NVarChar", 100, 0, 0, false, "", null, typeof(System.String), 4);
			this.AddElementFieldMapping("SmsInformationEntity", "IsDefault", "IsDefault", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 5);
			this.AddElementFieldMapping("SmsInformationEntity", "Name", "Name", false, "NVarChar", 250, 0, 0, false, "", null, typeof(System.String), 6);
			this.AddElementFieldMapping("SmsInformationEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 7);
			this.AddElementFieldMapping("SmsInformationEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 8);
			this.AddElementFieldMapping("SmsInformationEntity", "CreatedBy", "CreatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 9);
			this.AddElementFieldMapping("SmsInformationEntity", "UpdatedBy", "UpdatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 10);
		}

		/// <summary>Inits SmsKeywordEntity's mappings</summary>
		private void InitSmsKeywordEntityMappings()
		{
			this.AddElementMapping("SmsKeywordEntity", @"Obymobi", @"dbo", "SmsKeyword", 8, 0);
			this.AddElementFieldMapping("SmsKeywordEntity", "SmsKeywordId", "SmsKeywordId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("SmsKeywordEntity", "SmsInformationId", "SmsInformationId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("SmsKeywordEntity", "Keyword", "Keyword", false, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 2);
			this.AddElementFieldMapping("SmsKeywordEntity", "CompanyId", "CompanyId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("SmsKeywordEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 4);
			this.AddElementFieldMapping("SmsKeywordEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 5);
			this.AddElementFieldMapping("SmsKeywordEntity", "CreatedBy", "CreatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 6);
			this.AddElementFieldMapping("SmsKeywordEntity", "UpdatedBy", "UpdatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 7);
		}

		/// <summary>Inits StationEntity's mappings</summary>
		private void InitStationEntityMappings()
		{
			this.AddElementMapping("StationEntity", @"Obymobi", @"dbo", "Station", 14, 0);
			this.AddElementFieldMapping("StationEntity", "StationId", "StationId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("StationEntity", "ParentCompanyId", "ParentCompanyId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("StationEntity", "StationListId", "StationListId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("StationEntity", "Name", "Name", false, "NVarChar", 200, 0, 0, false, "", null, typeof(System.String), 3);
			this.AddElementFieldMapping("StationEntity", "SortOrder", "SortOrder", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("StationEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 5);
			this.AddElementFieldMapping("StationEntity", "CreatedBy", "CreatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 6);
			this.AddElementFieldMapping("StationEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 7);
			this.AddElementFieldMapping("StationEntity", "UpdatedBy", "UpdatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 8);
			this.AddElementFieldMapping("StationEntity", "Scene", "Scene", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 9);
			this.AddElementFieldMapping("StationEntity", "SuccessMessage", "SuccessMessage", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 10);
			this.AddElementFieldMapping("StationEntity", "Description", "Description", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 11);
			this.AddElementFieldMapping("StationEntity", "Url", "Url", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 12);
			this.AddElementFieldMapping("StationEntity", "Channel", "Channel", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 13);
		}

		/// <summary>Inits StationLanguageEntity's mappings</summary>
		private void InitStationLanguageEntityMappings()
		{
			this.AddElementMapping("StationLanguageEntity", @"Obymobi", @"dbo", "StationLanguage", 11, 0);
			this.AddElementFieldMapping("StationLanguageEntity", "StationLanguageId", "StationLanguageId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("StationLanguageEntity", "ParentCompanyId", "ParentCompanyId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("StationLanguageEntity", "StationId", "StationId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("StationLanguageEntity", "LanguageId", "LanguageId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("StationLanguageEntity", "Name", "Name", true, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 4);
			this.AddElementFieldMapping("StationLanguageEntity", "Description", "Description", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 5);
			this.AddElementFieldMapping("StationLanguageEntity", "SuccessMessage", "SuccessMessage", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 6);
			this.AddElementFieldMapping("StationLanguageEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 7);
			this.AddElementFieldMapping("StationLanguageEntity", "CreatedBy", "CreatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 8);
			this.AddElementFieldMapping("StationLanguageEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 9);
			this.AddElementFieldMapping("StationLanguageEntity", "UpdatedBy", "UpdatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 10);
		}

		/// <summary>Inits StationListEntity's mappings</summary>
		private void InitStationListEntityMappings()
		{
			this.AddElementMapping("StationListEntity", @"Obymobi", @"dbo", "StationList", 7, 0);
			this.AddElementFieldMapping("StationListEntity", "StationListId", "StationListId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("StationListEntity", "CompanyId", "CompanyId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("StationListEntity", "Name", "Name", false, "NVarChar", 200, 0, 0, false, "", null, typeof(System.String), 2);
			this.AddElementFieldMapping("StationListEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 3);
			this.AddElementFieldMapping("StationListEntity", "CreatedBy", "CreatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("StationListEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 5);
			this.AddElementFieldMapping("StationListEntity", "UpdatedBy", "UpdatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 6);
		}

		/// <summary>Inits SupplierEntity's mappings</summary>
		private void InitSupplierEntityMappings()
		{
			this.AddElementMapping("SupplierEntity", @"Obymobi", @"dbo", "Supplier", 15, 0);
			this.AddElementFieldMapping("SupplierEntity", "SupplierId", "SupplierId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("SupplierEntity", "Name", "Name", true, "NVarChar", 200, 0, 0, false, "", null, typeof(System.String), 1);
			this.AddElementFieldMapping("SupplierEntity", "Addressline1", "Addressline1", true, "NVarChar", 250, 0, 0, false, "", null, typeof(System.String), 2);
			this.AddElementFieldMapping("SupplierEntity", "Addressline2", "Addressline2", true, "NVarChar", 250, 0, 0, false, "", null, typeof(System.String), 3);
			this.AddElementFieldMapping("SupplierEntity", "Addressline3", "Addressline3", true, "NVarChar", 250, 0, 0, false, "", null, typeof(System.String), 4);
			this.AddElementFieldMapping("SupplierEntity", "Addressline4", "Addressline4", true, "NVarChar", 250, 0, 0, false, "", null, typeof(System.String), 5);
			this.AddElementFieldMapping("SupplierEntity", "Telephone1", "Telephone1", true, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 6);
			this.AddElementFieldMapping("SupplierEntity", "Telephone2", "Telephone2", true, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 7);
			this.AddElementFieldMapping("SupplierEntity", "Fax", "Fax", true, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 8);
			this.AddElementFieldMapping("SupplierEntity", "Email", "Email", true, "NVarChar", 100, 0, 0, false, "", null, typeof(System.String), 9);
			this.AddElementFieldMapping("SupplierEntity", "Website", "Website", true, "NVarChar", 100, 0, 0, false, "", null, typeof(System.String), 10);
			this.AddElementFieldMapping("SupplierEntity", "CreatedBy", "CreatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 11);
			this.AddElementFieldMapping("SupplierEntity", "UpdatedBy", "UpdatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 12);
			this.AddElementFieldMapping("SupplierEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 13);
			this.AddElementFieldMapping("SupplierEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 14);
		}

		/// <summary>Inits SupportagentEntity's mappings</summary>
		private void InitSupportagentEntityMappings()
		{
			this.AddElementMapping("SupportagentEntity", @"Obymobi", @"dbo", "Supportagent", 12, 0);
			this.AddElementFieldMapping("SupportagentEntity", "SupportagentId", "SupportagentId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("SupportagentEntity", "Firstname", "Firstname", true, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 1);
			this.AddElementFieldMapping("SupportagentEntity", "Lastname", "Lastname", true, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 2);
			this.AddElementFieldMapping("SupportagentEntity", "LastnamePrefix", "LastnamePrefix", true, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 3);
			this.AddElementFieldMapping("SupportagentEntity", "Email", "Email", true, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 4);
			this.AddElementFieldMapping("SupportagentEntity", "Active", "Active", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 5);
			this.AddElementFieldMapping("SupportagentEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 6);
			this.AddElementFieldMapping("SupportagentEntity", "CreatedBy", "CreatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 7);
			this.AddElementFieldMapping("SupportagentEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 8);
			this.AddElementFieldMapping("SupportagentEntity", "UpdatedBy", "UpdatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 9);
			this.AddElementFieldMapping("SupportagentEntity", "Phonenumber", "Phonenumber", true, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 10);
			this.AddElementFieldMapping("SupportagentEntity", "SlackUsername", "SlackUsername", true, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 11);
		}

		/// <summary>Inits SupportpoolEntity's mappings</summary>
		private void InitSupportpoolEntityMappings()
		{
			this.AddElementMapping("SupportpoolEntity", @"Obymobi", @"dbo", "Supportpool", 10, 0);
			this.AddElementFieldMapping("SupportpoolEntity", "SupportpoolId", "SupportpoolId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("SupportpoolEntity", "Name", "Name", true, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 1);
			this.AddElementFieldMapping("SupportpoolEntity", "Phonenumber", "Phonenumber", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 2);
			this.AddElementFieldMapping("SupportpoolEntity", "CreatedBy", "CreatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("SupportpoolEntity", "UpdatedBy", "UpdatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("SupportpoolEntity", "Email", "Email", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 5);
			this.AddElementFieldMapping("SupportpoolEntity", "SystemSupportPool", "SystemSupportPool", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 6);
			this.AddElementFieldMapping("SupportpoolEntity", "ContentSupportPool", "ContentSupportPool", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 7);
			this.AddElementFieldMapping("SupportpoolEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 8);
			this.AddElementFieldMapping("SupportpoolEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 9);
		}

		/// <summary>Inits SupportpoolNotificationRecipientEntity's mappings</summary>
		private void InitSupportpoolNotificationRecipientEntityMappings()
		{
			this.AddElementMapping("SupportpoolNotificationRecipientEntity", @"Obymobi", @"dbo", "SupportpoolNotificationRecipient", 15, 0);
			this.AddElementFieldMapping("SupportpoolNotificationRecipientEntity", "SupportpoolNotificationRecipientId", "SupportpoolNotificationRecipientId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("SupportpoolNotificationRecipientEntity", "SupportpoolId", "SupportpoolId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("SupportpoolNotificationRecipientEntity", "Name", "Name", true, "VarChar", 255, 0, 0, false, "", null, typeof(System.String), 2);
			this.AddElementFieldMapping("SupportpoolNotificationRecipientEntity", "Phonenumber", "Phonenumber", true, "VarChar", 255, 0, 0, false, "", null, typeof(System.String), 3);
			this.AddElementFieldMapping("SupportpoolNotificationRecipientEntity", "Email", "Email", true, "VarChar", 255, 0, 0, false, "", null, typeof(System.String), 4);
			this.AddElementFieldMapping("SupportpoolNotificationRecipientEntity", "NotifyOfflineTerminals", "NotifyOfflineTerminals", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 5);
			this.AddElementFieldMapping("SupportpoolNotificationRecipientEntity", "NotifyOfflineClients", "NotifyOfflineClients", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 6);
			this.AddElementFieldMapping("SupportpoolNotificationRecipientEntity", "NotifyUnprocessableOrders", "NotifyUnprocessableOrders", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 7);
			this.AddElementFieldMapping("SupportpoolNotificationRecipientEntity", "NotifyExpiredSteps", "NotifyExpiredSteps", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 8);
			this.AddElementFieldMapping("SupportpoolNotificationRecipientEntity", "NotifyTooMuchOfflineClientsJump", "NotifyTooMuchOfflineClientsJump", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 9);
			this.AddElementFieldMapping("SupportpoolNotificationRecipientEntity", "NotifyBouncedEmails", "NotifyBouncedEmails", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 10);
			this.AddElementFieldMapping("SupportpoolNotificationRecipientEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 11);
			this.AddElementFieldMapping("SupportpoolNotificationRecipientEntity", "CreatedBy", "CreatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 12);
			this.AddElementFieldMapping("SupportpoolNotificationRecipientEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 13);
			this.AddElementFieldMapping("SupportpoolNotificationRecipientEntity", "UpdatedBy", "UpdatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 14);
		}

		/// <summary>Inits SupportpoolSupportagentEntity's mappings</summary>
		private void InitSupportpoolSupportagentEntityMappings()
		{
			this.AddElementMapping("SupportpoolSupportagentEntity", @"Obymobi", @"dbo", "SupportpoolSupportagent", 14, 0);
			this.AddElementFieldMapping("SupportpoolSupportagentEntity", "SupportpoolSupportagentId", "SupportpoolSupportagentId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("SupportpoolSupportagentEntity", "SupportpoolId", "SupportpoolId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("SupportpoolSupportagentEntity", "SupportagentId", "SupportagentId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("SupportpoolSupportagentEntity", "OnSupport", "OnSupport", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 3);
			this.AddElementFieldMapping("SupportpoolSupportagentEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 4);
			this.AddElementFieldMapping("SupportpoolSupportagentEntity", "CreatedBy", "CreatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 5);
			this.AddElementFieldMapping("SupportpoolSupportagentEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 6);
			this.AddElementFieldMapping("SupportpoolSupportagentEntity", "UpdatedBy", "UpdatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 7);
			this.AddElementFieldMapping("SupportpoolSupportagentEntity", "NotifyOfflineTerminals", "NotifyOfflineTerminals", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 8);
			this.AddElementFieldMapping("SupportpoolSupportagentEntity", "NotifyOfflineClients", "NotifyOfflineClients", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 9);
			this.AddElementFieldMapping("SupportpoolSupportagentEntity", "NotifyUnprocessableOrders", "NotifyUnprocessableOrders", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 10);
			this.AddElementFieldMapping("SupportpoolSupportagentEntity", "NotifyExpiredSteps", "NotifyExpiredSteps", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 11);
			this.AddElementFieldMapping("SupportpoolSupportagentEntity", "NotifyTooMuchOfflineClientsJump", "NotifyTooMuchOfflineClientsJump", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 12);
			this.AddElementFieldMapping("SupportpoolSupportagentEntity", "NotifyBouncedEmails", "NotifyBouncedEmails", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 13);
		}

		/// <summary>Inits SurveyEntity's mappings</summary>
		private void InitSurveyEntityMappings()
		{
			this.AddElementMapping("SurveyEntity", @"Obymobi", @"dbo", "Survey", 15, 0);
			this.AddElementFieldMapping("SurveyEntity", "SurveyId", "SurveyId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("SurveyEntity", "CompanyId", "CompanyId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("SurveyEntity", "Name", "Name", true, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 2);
			this.AddElementFieldMapping("SurveyEntity", "ProcessedTitle", "ProcessedTitle", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 3);
			this.AddElementFieldMapping("SurveyEntity", "ProcessedMessage", "ProcessedMessage", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 4);
			this.AddElementFieldMapping("SurveyEntity", "SavingTitle", "SavingTitle", true, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 5);
			this.AddElementFieldMapping("SurveyEntity", "SavingMessage", "SavingMessage", true, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 6);
			this.AddElementFieldMapping("SurveyEntity", "CreatedBy", "CreatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 7);
			this.AddElementFieldMapping("SurveyEntity", "UpdatedBy", "UpdatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 8);
			this.AddElementFieldMapping("SurveyEntity", "AnswerRequiredMessage", "AnswerRequiredMessage", true, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 9);
			this.AddElementFieldMapping("SurveyEntity", "AnswerRequiredTitle", "AnswerRequiredTitle", true, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 10);
			this.AddElementFieldMapping("SurveyEntity", "EmailResults", "EmailResults", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 11);
			this.AddElementFieldMapping("SurveyEntity", "Email", "Email", true, "NVarChar", 100, 0, 0, false, "", null, typeof(System.String), 12);
			this.AddElementFieldMapping("SurveyEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 13);
			this.AddElementFieldMapping("SurveyEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 14);
		}

		/// <summary>Inits SurveyAnswerEntity's mappings</summary>
		private void InitSurveyAnswerEntityMappings()
		{
			this.AddElementMapping("SurveyAnswerEntity", @"Obymobi", @"dbo", "SurveyAnswer", 10, 0);
			this.AddElementFieldMapping("SurveyAnswerEntity", "SurveyAnswerId", "SurveyAnswerId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("SurveyAnswerEntity", "SurveyQuestionId", "SurveyQuestionId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("SurveyAnswerEntity", "Answer", "Answer", true, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 2);
			this.AddElementFieldMapping("SurveyAnswerEntity", "CreatedBy", "CreatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("SurveyAnswerEntity", "UpdatedBy", "UpdatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("SurveyAnswerEntity", "SortOrder", "SortOrder", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 5);
			this.AddElementFieldMapping("SurveyAnswerEntity", "TargetSurveyQuestionId", "TargetSurveyQuestionId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 6);
			this.AddElementFieldMapping("SurveyAnswerEntity", "ParentCompanyId", "ParentCompanyId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 7);
			this.AddElementFieldMapping("SurveyAnswerEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 8);
			this.AddElementFieldMapping("SurveyAnswerEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 9);
		}

		/// <summary>Inits SurveyAnswerLanguageEntity's mappings</summary>
		private void InitSurveyAnswerLanguageEntityMappings()
		{
			this.AddElementMapping("SurveyAnswerLanguageEntity", @"Obymobi", @"dbo", "SurveyAnswerLanguage", 9, 0);
			this.AddElementFieldMapping("SurveyAnswerLanguageEntity", "SurveyAnswerLanguageId", "SurveyAnswerLanguageId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("SurveyAnswerLanguageEntity", "SurveyAnswerId", "SurveyAnswerId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("SurveyAnswerLanguageEntity", "LanguageId", "LanguageId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("SurveyAnswerLanguageEntity", "Answer", "Answer", true, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 3);
			this.AddElementFieldMapping("SurveyAnswerLanguageEntity", "CreatedBy", "CreatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("SurveyAnswerLanguageEntity", "UpdatedBy", "UpdatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 5);
			this.AddElementFieldMapping("SurveyAnswerLanguageEntity", "ParentCompanyId", "ParentCompanyId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 6);
			this.AddElementFieldMapping("SurveyAnswerLanguageEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 7);
			this.AddElementFieldMapping("SurveyAnswerLanguageEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 8);
		}

		/// <summary>Inits SurveyLanguageEntity's mappings</summary>
		private void InitSurveyLanguageEntityMappings()
		{
			this.AddElementMapping("SurveyLanguageEntity", @"Obymobi", @"dbo", "SurveyLanguage", 12, 0);
			this.AddElementFieldMapping("SurveyLanguageEntity", "SurveyLanguageId", "SurveyLanguageId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("SurveyLanguageEntity", "SurveyId", "SurveyId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("SurveyLanguageEntity", "LanguageId", "LanguageId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("SurveyLanguageEntity", "ProcessedTitle", "ProcessedTitle", true, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 3);
			this.AddElementFieldMapping("SurveyLanguageEntity", "ProcessedMessage", "ProcessedMessage", true, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 4);
			this.AddElementFieldMapping("SurveyLanguageEntity", "SavingTitle", "SavingTitle", true, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 5);
			this.AddElementFieldMapping("SurveyLanguageEntity", "SavingMessage", "SavingMessage", true, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 6);
			this.AddElementFieldMapping("SurveyLanguageEntity", "CreatedBy", "CreatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 7);
			this.AddElementFieldMapping("SurveyLanguageEntity", "UpdatedBy", "UpdatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 8);
			this.AddElementFieldMapping("SurveyLanguageEntity", "ParentCompanyId", "ParentCompanyId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 9);
			this.AddElementFieldMapping("SurveyLanguageEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 10);
			this.AddElementFieldMapping("SurveyLanguageEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 11);
		}

		/// <summary>Inits SurveyPageEntity's mappings</summary>
		private void InitSurveyPageEntityMappings()
		{
			this.AddElementMapping("SurveyPageEntity", @"Obymobi", @"dbo", "SurveyPage", 9, 0);
			this.AddElementFieldMapping("SurveyPageEntity", "SurveyPageId", "SurveyPageId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("SurveyPageEntity", "SurveyId", "SurveyId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("SurveyPageEntity", "Name", "Name", true, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 2);
			this.AddElementFieldMapping("SurveyPageEntity", "SortOrder", "SortOrder", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("SurveyPageEntity", "CreatedBy", "CreatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("SurveyPageEntity", "UpdatedBy", "UpdatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 5);
			this.AddElementFieldMapping("SurveyPageEntity", "ParentCompanyId", "ParentCompanyId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 6);
			this.AddElementFieldMapping("SurveyPageEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 7);
			this.AddElementFieldMapping("SurveyPageEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 8);
		}

		/// <summary>Inits SurveyQuestionEntity's mappings</summary>
		private void InitSurveyQuestionEntityMappings()
		{
			this.AddElementMapping("SurveyQuestionEntity", @"Obymobi", @"dbo", "SurveyQuestion", 23, 0);
			this.AddElementFieldMapping("SurveyQuestionEntity", "SurveyQuestionId", "SurveyQuestionId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("SurveyQuestionEntity", "Question", "Question", true, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 1);
			this.AddElementFieldMapping("SurveyQuestionEntity", "Type", "Type", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("SurveyQuestionEntity", "CreatedBy", "CreatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("SurveyQuestionEntity", "UpdatedBy", "UpdatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("SurveyQuestionEntity", "Required", "Required", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 5);
			this.AddElementFieldMapping("SurveyQuestionEntity", "SortOrder", "SortOrder", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 6);
			this.AddElementFieldMapping("SurveyQuestionEntity", "FieldValue1", "FieldValue1", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 7);
			this.AddElementFieldMapping("SurveyQuestionEntity", "FieldValue10", "FieldValue10", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 8);
			this.AddElementFieldMapping("SurveyQuestionEntity", "FieldValue2", "FieldValue2", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 9);
			this.AddElementFieldMapping("SurveyQuestionEntity", "FieldValue3", "FieldValue3", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 10);
			this.AddElementFieldMapping("SurveyQuestionEntity", "FieldValue4", "FieldValue4", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 11);
			this.AddElementFieldMapping("SurveyQuestionEntity", "FieldValue5", "FieldValue5", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 12);
			this.AddElementFieldMapping("SurveyQuestionEntity", "FieldValue6", "FieldValue6", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 13);
			this.AddElementFieldMapping("SurveyQuestionEntity", "FieldValue7", "FieldValue7", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 14);
			this.AddElementFieldMapping("SurveyQuestionEntity", "FieldValue8", "FieldValue8", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 15);
			this.AddElementFieldMapping("SurveyQuestionEntity", "FieldValue9", "FieldValue9", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 16);
			this.AddElementFieldMapping("SurveyQuestionEntity", "ParentQuestionId", "ParentSurveyQuestionId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 17);
			this.AddElementFieldMapping("SurveyQuestionEntity", "SurveyPageId", "SurveyPageId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 18);
			this.AddElementFieldMapping("SurveyQuestionEntity", "NotesEnabled", "NotesEnabled", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 19);
			this.AddElementFieldMapping("SurveyQuestionEntity", "ParentCompanyId", "ParentCompanyId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 20);
			this.AddElementFieldMapping("SurveyQuestionEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 21);
			this.AddElementFieldMapping("SurveyQuestionEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 22);
		}

		/// <summary>Inits SurveyQuestionLanguageEntity's mappings</summary>
		private void InitSurveyQuestionLanguageEntityMappings()
		{
			this.AddElementMapping("SurveyQuestionLanguageEntity", @"Obymobi", @"dbo", "SurveyQuestionLanguage", 9, 0);
			this.AddElementFieldMapping("SurveyQuestionLanguageEntity", "SurveyQuestionLanguageId", "SurveyQuestionLanguageId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("SurveyQuestionLanguageEntity", "SurveyQuestionId", "SurveyQuestionId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("SurveyQuestionLanguageEntity", "LanguageId", "LanguageId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("SurveyQuestionLanguageEntity", "Question", "Question", true, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 3);
			this.AddElementFieldMapping("SurveyQuestionLanguageEntity", "CreatedBy", "CreatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("SurveyQuestionLanguageEntity", "UpdatedBy", "UpdatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 5);
			this.AddElementFieldMapping("SurveyQuestionLanguageEntity", "ParentCompanyId", "ParentCompanyId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 6);
			this.AddElementFieldMapping("SurveyQuestionLanguageEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 7);
			this.AddElementFieldMapping("SurveyQuestionLanguageEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 8);
		}

		/// <summary>Inits SurveyResultEntity's mappings</summary>
		private void InitSurveyResultEntityMappings()
		{
			this.AddElementMapping("SurveyResultEntity", @"Obymobi", @"dbo", "SurveyResult", 12, 0);
			this.AddElementFieldMapping("SurveyResultEntity", "SurveyResultId", "SurveyResultId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("SurveyResultEntity", "SurveyAnswerId", "SurveyAnswerId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("SurveyResultEntity", "SurveyQuestionId", "SurveyQuestionId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("SurveyResultEntity", "CustomerId", "CustomerId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("SurveyResultEntity", "ClientId", "ClientId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("SurveyResultEntity", "Comment", "Comment", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 5);
			this.AddElementFieldMapping("SurveyResultEntity", "CreatedBy", "CreatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 6);
			this.AddElementFieldMapping("SurveyResultEntity", "UpdatedBy", "UpdatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 7);
			this.AddElementFieldMapping("SurveyResultEntity", "ParentCompanyId", "ParentCompanyId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 8);
			this.AddElementFieldMapping("SurveyResultEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 9);
			this.AddElementFieldMapping("SurveyResultEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 10);
			this.AddElementFieldMapping("SurveyResultEntity", "SubmittedUTC", "SubmittedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 11);
		}

		/// <summary>Inits TagEntity's mappings</summary>
		private void InitTagEntityMappings()
		{
			this.AddElementMapping("TagEntity", @"Obymobi", @"dbo", "Tag", 5, 0);
			this.AddElementFieldMapping("TagEntity", "TagId", "TagId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("TagEntity", "CompanyId", "CompanyId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("TagEntity", "Name", "Name", false, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 2);
			this.AddElementFieldMapping("TagEntity", "CreatedUTC", "CreatedUTC", false, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 3);
			this.AddElementFieldMapping("TagEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 4);
		}

		/// <summary>Inits TaxTariffEntity's mappings</summary>
		private void InitTaxTariffEntityMappings()
		{
			this.AddElementMapping("TaxTariffEntity", @"Obymobi", @"dbo", "TaxTariff", 8, 0);
			this.AddElementFieldMapping("TaxTariffEntity", "TaxTariffId", "TaxTariffId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("TaxTariffEntity", "CompanyId", "CompanyId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("TaxTariffEntity", "Name", "Name", false, "NVarChar", 100, 0, 0, false, "", null, typeof(System.String), 2);
			this.AddElementFieldMapping("TaxTariffEntity", "Percentage", "Percentage", false, "Float", 0, 38, 0, false, "", null, typeof(System.Double), 3);
			this.AddElementFieldMapping("TaxTariffEntity", "CreatedBy", "CreatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("TaxTariffEntity", "UpdatedBy", "UpdatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 5);
			this.AddElementFieldMapping("TaxTariffEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 6);
			this.AddElementFieldMapping("TaxTariffEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 7);
		}

		/// <summary>Inits TerminalEntity's mappings</summary>
		private void InitTerminalEntityMappings()
		{
			this.AddElementMapping("TerminalEntity", @"Obymobi", @"dbo", "Terminal", 86, 0);
			this.AddElementFieldMapping("TerminalEntity", "TerminalId", "TerminalId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("TerminalEntity", "CompanyId", "CompanyId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("TerminalEntity", "Name", "Name", true, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 2);
			this.AddElementFieldMapping("TerminalEntity", "LastStatus", "LastStatus", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("TerminalEntity", "LastStatusText", "LastStatusText", true, "NVarChar", 250, 0, 0, false, "", null, typeof(System.String), 4);
			this.AddElementFieldMapping("TerminalEntity", "LastStatusMessage", "LastStatusMessage", true, "NVarChar", 250, 0, 0, false, "", null, typeof(System.String), 5);
			this.AddElementFieldMapping("TerminalEntity", "ReceiptTypes", "ReceiptTypes", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 6);
			this.AddElementFieldMapping("TerminalEntity", "RequestInterval", "RequestInterval", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 7);
			this.AddElementFieldMapping("TerminalEntity", "DiagnoseInterval", "DiagnoseInterval", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 8);
			this.AddElementFieldMapping("TerminalEntity", "PrinterName", "PrinterName", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 9);
			this.AddElementFieldMapping("TerminalEntity", "RecordAllRequestsToRequestLog", "RecordAllRequestsToRequestLog", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 10);
			this.AddElementFieldMapping("TerminalEntity", "TeamviewerId", "TeamviewerId", true, "NVarChar", 250, 0, 0, false, "", null, typeof(System.String), 11);
			this.AddElementFieldMapping("TerminalEntity", "MaxDaysLogHistory", "MaxDaysLogHistory", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 12);
			this.AddElementFieldMapping("TerminalEntity", "POSConnectorType", "POSConnectorType", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 13);
			this.AddElementFieldMapping("TerminalEntity", "PMSConnectorType", "PMSConnectorType", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 14);
			this.AddElementFieldMapping("TerminalEntity", "SynchronizeWithPOSOnStart", "SynchronizeWithPOSOnStart", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 15);
			this.AddElementFieldMapping("TerminalEntity", "MaxStatusReports", "MaxStatusReports", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 16);
			this.AddElementFieldMapping("TerminalEntity", "Active", "Active", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 17);
			this.AddElementFieldMapping("TerminalEntity", "LanguageCode", "LanguageCode", false, "Char", 2, 0, 0, false, "", null, typeof(System.String), 18);
			this.AddElementFieldMapping("TerminalEntity", "UnlockDeliverypointProductId", "UnlockDeliverypointProductId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 19);
			this.AddElementFieldMapping("TerminalEntity", "BatteryLowProductId", "BatteryLowProductId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 20);
			this.AddElementFieldMapping("TerminalEntity", "ClientDisconnectedProductId", "ClientDisconnectedProductId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 21);
			this.AddElementFieldMapping("TerminalEntity", "OrderFailedProductId", "OrderFailedProductId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 22);
			this.AddElementFieldMapping("TerminalEntity", "SystemMessagesDeliverypointId", "SystemMessagesDeliverypointId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 23);
			this.AddElementFieldMapping("TerminalEntity", "AltSystemMessagesDeliverypointId", "AltSystemMessagesDeliverypointId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 24);
			this.AddElementFieldMapping("TerminalEntity", "IcrtouchprintermappingId", "IcrtouchprintermappingId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 25);
			this.AddElementFieldMapping("TerminalEntity", "OperationMode", "OperationMode", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 26);
			this.AddElementFieldMapping("TerminalEntity", "OperationState", "OperationState", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 27);
			this.AddElementFieldMapping("TerminalEntity", "HandlingMethod", "HandlingMethod", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 28);
			this.AddElementFieldMapping("TerminalEntity", "NotificationForNewOrder", "NotificationForNewOrder", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 29);
			this.AddElementFieldMapping("TerminalEntity", "NotificationForOverdueOrder", "NotificationForOverdueOrder", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 30);
			this.AddElementFieldMapping("TerminalEntity", "MaxProcessTime", "MaxProcessTime", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 31);
			this.AddElementFieldMapping("TerminalEntity", "PosValue1", "PosValue1", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 32);
			this.AddElementFieldMapping("TerminalEntity", "PosValue2", "PosValue2", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 33);
			this.AddElementFieldMapping("TerminalEntity", "PosValue3", "PosValue3", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 34);
			this.AddElementFieldMapping("TerminalEntity", "PosValue4", "PosValue4", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 35);
			this.AddElementFieldMapping("TerminalEntity", "PosValue5", "PosValue5", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 36);
			this.AddElementFieldMapping("TerminalEntity", "PosValue6", "PosValue6", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 37);
			this.AddElementFieldMapping("TerminalEntity", "PosValue7", "PosValue7", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 38);
			this.AddElementFieldMapping("TerminalEntity", "PosValue8", "PosValue8", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 39);
			this.AddElementFieldMapping("TerminalEntity", "PosValue9", "PosValue9", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 40);
			this.AddElementFieldMapping("TerminalEntity", "PosValue10", "PosValue10", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 41);
			this.AddElementFieldMapping("TerminalEntity", "PosValue11", "PosValue11", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 42);
			this.AddElementFieldMapping("TerminalEntity", "PosValue12", "PosValue12", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 43);
			this.AddElementFieldMapping("TerminalEntity", "PosValue13", "PosValue13", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 44);
			this.AddElementFieldMapping("TerminalEntity", "PosValue14", "PosValue14", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 45);
			this.AddElementFieldMapping("TerminalEntity", "PosValue15", "PosValue15", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 46);
			this.AddElementFieldMapping("TerminalEntity", "PosValue16", "PosValue16", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 47);
			this.AddElementFieldMapping("TerminalEntity", "PosValue17", "PosValue17", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 48);
			this.AddElementFieldMapping("TerminalEntity", "PosValue18", "PosValue18", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 49);
			this.AddElementFieldMapping("TerminalEntity", "PosValue19", "PosValue19", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 50);
			this.AddElementFieldMapping("TerminalEntity", "PosValue20", "PosValue20", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 51);
			this.AddElementFieldMapping("TerminalEntity", "CreatedBy", "CreatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 52);
			this.AddElementFieldMapping("TerminalEntity", "UpdatedBy", "UpdatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 53);
			this.AddElementFieldMapping("TerminalEntity", "AnnouncementDuration", "AnnouncementDuration", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 54);
			this.AddElementFieldMapping("TerminalEntity", "ForwardToTerminalId", "ForwardToTerminalId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 55);
			this.AddElementFieldMapping("TerminalEntity", "DeviceId", "DeviceId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 56);
			this.AddElementFieldMapping("TerminalEntity", "OnsiteServerDeliverypointgroupId", "OnsiteServerDeliverypointgroupId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 57);
			this.AddElementFieldMapping("TerminalEntity", "OutOfChargeNotificationsSent", "OutOfChargeNotificationsSent", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 58);
			this.AddElementFieldMapping("TerminalEntity", "Browser1", "Browser1", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 59);
			this.AddElementFieldMapping("TerminalEntity", "Browser2", "Browser2", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 60);
			this.AddElementFieldMapping("TerminalEntity", "CmsPage", "CmsPage", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 61);
			this.AddElementFieldMapping("TerminalEntity", "AutomaticSignOnUserId", "AutomaticSignOnUserId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 62);
			this.AddElementFieldMapping("TerminalEntity", "SurveyResultNotifications", "SurveyResultNotifications", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 63);
			this.AddElementFieldMapping("TerminalEntity", "PrintingEnabled", "PrintingEnabled", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 64);
			this.AddElementFieldMapping("TerminalEntity", "DeliverypointgroupId", "DeliverypointgroupId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 65);
			this.AddElementFieldMapping("TerminalEntity", "LastStateOnline", "LastStateOnline", true, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 66);
			this.AddElementFieldMapping("TerminalEntity", "LastStateOperationMode", "LastStateOperationMode", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 67);
			this.AddElementFieldMapping("TerminalEntity", "UIModeId", "UIModeId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 68);
			this.AddElementFieldMapping("TerminalEntity", "UseMonitoring", "UseMonitoring", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 69);
			this.AddElementFieldMapping("TerminalEntity", "MasterTab", "MasterTab", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 70);
			this.AddElementFieldMapping("TerminalEntity", "ResetTimeout", "ResetTimeout", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 71);
			this.AddElementFieldMapping("TerminalEntity", "PrinterConnected", "PrinterConnected", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 72);
			this.AddElementFieldMapping("TerminalEntity", "UseHardKeyboard", "UseHardKeyboard", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 73);
			this.AddElementFieldMapping("TerminalEntity", "MaxVolume", "MaxVolume", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 74);
			this.AddElementFieldMapping("TerminalEntity", "OfflineNotificationEnabled", "OfflineNotificationEnabled", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 75);
			this.AddElementFieldMapping("TerminalEntity", "LoadedSuccessfully", "LoadedSuccessfully", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 76);
			this.AddElementFieldMapping("TerminalEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 77);
			this.AddElementFieldMapping("TerminalEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 78);
			this.AddElementFieldMapping("TerminalEntity", "OutOfChargeNotificationLastSentUTC", "OutOfChargeNotificationLastSentUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 79);
			this.AddElementFieldMapping("TerminalEntity", "Type", "Type", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 80);
			this.AddElementFieldMapping("TerminalEntity", "StatusUpdatedUTC", "StatusUpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 81);
			this.AddElementFieldMapping("TerminalEntity", "SynchronizeWithPMSOnStart", "SynchronizeWithPMSOnStart", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 82);
			this.AddElementFieldMapping("TerminalEntity", "TerminalConfigurationId", "TerminalConfigurationId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 83);
			this.AddElementFieldMapping("TerminalEntity", "LastSyncUTC", "LastSyncUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 84);
			this.AddElementFieldMapping("TerminalEntity", "PmsTerminalOfflineNotificationEnabled", "PmsTerminalOfflineNotificationEnabled", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 85);
		}

		/// <summary>Inits TerminalConfigurationEntity's mappings</summary>
		private void InitTerminalConfigurationEntityMappings()
		{
			this.AddElementMapping("TerminalConfigurationEntity", @"Obymobi", @"dbo", "TerminalConfiguration", 15, 0);
			this.AddElementFieldMapping("TerminalConfigurationEntity", "TerminalConfigurationId", "TerminalConfigurationId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("TerminalConfigurationEntity", "CompanyId", "CompanyId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("TerminalConfigurationEntity", "Name", "Name", false, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 2);
			this.AddElementFieldMapping("TerminalConfigurationEntity", "UIModeId", "UIModeId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("TerminalConfigurationEntity", "Pincode", "Pincode", true, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 4);
			this.AddElementFieldMapping("TerminalConfigurationEntity", "PincodeSU", "PincodeSU", true, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 5);
			this.AddElementFieldMapping("TerminalConfigurationEntity", "PincodeGM", "PincodeGM", true, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 6);
			this.AddElementFieldMapping("TerminalConfigurationEntity", "PrintingEnabled", "PrintingEnabled", true, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 7);
			this.AddElementFieldMapping("TerminalConfigurationEntity", "UseHardKeyboard", "UseHardKeyboard", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 8);
			this.AddElementFieldMapping("TerminalConfigurationEntity", "MaxVolume", "MaxVolume", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 9);
			this.AddElementFieldMapping("TerminalConfigurationEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 10);
			this.AddElementFieldMapping("TerminalConfigurationEntity", "CreatedBy", "CreatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 11);
			this.AddElementFieldMapping("TerminalConfigurationEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 12);
			this.AddElementFieldMapping("TerminalConfigurationEntity", "UpdatedBy", "UpdatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 13);
			this.AddElementFieldMapping("TerminalConfigurationEntity", "TerminalId", "TerminalId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 14);
		}

		/// <summary>Inits TerminalLogEntity's mappings</summary>
		private void InitTerminalLogEntityMappings()
		{
			this.AddElementMapping("TerminalLogEntity", @"Obymobi", @"dbo", "TerminalLog", 16, 0);
			this.AddElementFieldMapping("TerminalLogEntity", "TerminalLogId", "TerminalLogId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("TerminalLogEntity", "TerminalId", "TerminalId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("TerminalLogEntity", "OrderId", "OrderId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("TerminalLogEntity", "OrderGuid", "OrderGuid", true, "NVarChar", 128, 0, 0, false, "", null, typeof(System.String), 3);
			this.AddElementFieldMapping("TerminalLogEntity", "Type", "Type", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("TerminalLogEntity", "Message", "Message", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 5);
			this.AddElementFieldMapping("TerminalLogEntity", "Status", "Status", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 6);
			this.AddElementFieldMapping("TerminalLogEntity", "Log", "Log", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 7);
			this.AddElementFieldMapping("TerminalLogEntity", "FromOperationMode", "FromOperationMode", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 8);
			this.AddElementFieldMapping("TerminalLogEntity", "ToOperationMode", "ToOperationMode", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 9);
			this.AddElementFieldMapping("TerminalLogEntity", "CreatedBy", "CreatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 10);
			this.AddElementFieldMapping("TerminalLogEntity", "UpdatedBy", "UpdatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 11);
			this.AddElementFieldMapping("TerminalLogEntity", "TerminalLogFileId", "TerminalLogFileId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 12);
			this.AddElementFieldMapping("TerminalLogEntity", "ParentCompanyId", "ParentCompanyId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 13);
			this.AddElementFieldMapping("TerminalLogEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 14);
			this.AddElementFieldMapping("TerminalLogEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 15);
		}

		/// <summary>Inits TerminalLogFileEntity's mappings</summary>
		private void InitTerminalLogFileEntityMappings()
		{
			this.AddElementMapping("TerminalLogFileEntity", @"Obymobi", @"dbo", "TerminalLogFile", 8, 0);
			this.AddElementFieldMapping("TerminalLogFileEntity", "TerminalLogFileId", "TerminalLogFileId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("TerminalLogFileEntity", "Message", "Message", false, "Text", 2147483647, 0, 0, false, "", null, typeof(System.String), 1);
			this.AddElementFieldMapping("TerminalLogFileEntity", "CreatedBy", "CreatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("TerminalLogFileEntity", "UpdatedBy", "UpdatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("TerminalLogFileEntity", "Application", "Application", true, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 4);
			this.AddElementFieldMapping("TerminalLogFileEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 5);
			this.AddElementFieldMapping("TerminalLogFileEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 6);
			this.AddElementFieldMapping("TerminalLogFileEntity", "LogDate", "LogDate", false, "DateTime", 0, 0, 0, false, "", null, typeof(System.DateTime), 7);
		}

		/// <summary>Inits TerminalMessageTemplateCategoryEntity's mappings</summary>
		private void InitTerminalMessageTemplateCategoryEntityMappings()
		{
			this.AddElementMapping("TerminalMessageTemplateCategoryEntity", @"Obymobi", @"dbo", "TerminalMessageTemplateCategory", 7, 0);
			this.AddElementFieldMapping("TerminalMessageTemplateCategoryEntity", "TerminalMessageTemplateCategoryId", "TerminalMessageTemplateCategoryId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("TerminalMessageTemplateCategoryEntity", "MessageTemplateCategoryId", "MessageTemplateCategoryId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("TerminalMessageTemplateCategoryEntity", "TerminalId", "TerminalId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("TerminalMessageTemplateCategoryEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 3);
			this.AddElementFieldMapping("TerminalMessageTemplateCategoryEntity", "CreatedBy", "CreatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("TerminalMessageTemplateCategoryEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 5);
			this.AddElementFieldMapping("TerminalMessageTemplateCategoryEntity", "UpdatedBy", "UpdatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 6);
		}

		/// <summary>Inits TerminalStateEntity's mappings</summary>
		private void InitTerminalStateEntityMappings()
		{
			this.AddElementMapping("TerminalStateEntity", @"Obymobi", @"dbo", "TerminalState", 13, 0);
			this.AddElementFieldMapping("TerminalStateEntity", "TerminalStateId", "TerminalStateId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("TerminalStateEntity", "TerminalId", "TerminalId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("TerminalStateEntity", "Online", "Online", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 2);
			this.AddElementFieldMapping("TerminalStateEntity", "OperationMode", "OperationMode", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("TerminalStateEntity", "LastBatteryLevel", "LastBatteryLevel", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("TerminalStateEntity", "LastIsCharging", "LastIsCharging", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 5);
			this.AddElementFieldMapping("TerminalStateEntity", "Message", "Message", true, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 6);
			this.AddElementFieldMapping("TerminalStateEntity", "CreatedBy", "CreatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 7);
			this.AddElementFieldMapping("TerminalStateEntity", "UpdatedBy", "UpdatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 8);
			this.AddElementFieldMapping("TerminalStateEntity", "TimeSpanTicks", "TimeSpanTicks", false, "BigInt", 0, 19, 0, false, "", null, typeof(System.Int64), 9);
			this.AddElementFieldMapping("TerminalStateEntity", "ParentCompanyId", "ParentCompanyId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 10);
			this.AddElementFieldMapping("TerminalStateEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 11);
			this.AddElementFieldMapping("TerminalStateEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 12);
		}

		/// <summary>Inits TimestampEntity's mappings</summary>
		private void InitTimestampEntityMappings()
		{
			this.AddElementMapping("TimestampEntity", @"Obymobi", @"dbo", "Timestamp", 179, 0);
			this.AddElementFieldMapping("TimestampEntity", "TimestampId", "TimestampId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("TimestampEntity", "AdvertisementConfigurationId", "AdvertisementConfigurationId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("TimestampEntity", "ModifiedAdvertisementConfigurationUTC", "ModifiedAdvertisementConfigurationUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 2);
			this.AddElementFieldMapping("TimestampEntity", "PublishedAdvertisementConfigurationUTC", "PublishedAdvertisementConfigurationUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 3);
			this.AddElementFieldMapping("TimestampEntity", "ModifiedAdvertisementConfigurationCustomTextUTC", "ModifiedAdvertisementConfigurationCustomTextUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 4);
			this.AddElementFieldMapping("TimestampEntity", "PublishedAdvertisementConfigurationCustomTextUTC", "PublishedAdvertisementConfigurationCustomTextUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 5);
			this.AddElementFieldMapping("TimestampEntity", "ModifiedAdvertisementConfigurationMediaUTC", "ModifiedAdvertisementConfigurationMediaUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 6);
			this.AddElementFieldMapping("TimestampEntity", "PublishedAdvertisementConfigurationMediaUTC", "PublishedAdvertisementConfigurationMediaUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 7);
			this.AddElementFieldMapping("TimestampEntity", "ClientId", "ClientId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 8);
			this.AddElementFieldMapping("TimestampEntity", "ModifiedClientUTC", "ModifiedClientUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 9);
			this.AddElementFieldMapping("TimestampEntity", "PublishedClientUTC", "PublishedClientUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 10);
			this.AddElementFieldMapping("TimestampEntity", "ClientConfigurationId", "ClientConfigurationId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 11);
			this.AddElementFieldMapping("TimestampEntity", "ModifiedClientConfigurationUTC", "ModifiedClientConfigurationUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 12);
			this.AddElementFieldMapping("TimestampEntity", "PublishedClientConfigurationUTC", "PublishedClientConfigurationUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 13);
			this.AddElementFieldMapping("TimestampEntity", "ModifiedClientConfigurationCustomTextUTC", "ModifiedClientConfigurationCustomTextUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 14);
			this.AddElementFieldMapping("TimestampEntity", "PublishedClientConfigurationCustomTextUTC", "PublishedClientConfigurationCustomTextUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 15);
			this.AddElementFieldMapping("TimestampEntity", "ModifiedClientConfigurationMediaUTC", "ModifiedClientConfigurationMediaUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 16);
			this.AddElementFieldMapping("TimestampEntity", "PublishedClientConfigurationMediaUTC", "PublishedClientConfigurationMediaUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 17);
			this.AddElementFieldMapping("TimestampEntity", "CompanyId", "CompanyId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 18);
			this.AddElementFieldMapping("TimestampEntity", "ModifiedAmenitiesUTC", "ModifiedAmenitiesUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 19);
			this.AddElementFieldMapping("TimestampEntity", "PublishedAmenitiesUTC", "PublishedAmenitiesUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 20);
			this.AddElementFieldMapping("TimestampEntity", "ModifiedAmenitiesCustomTextUTC", "ModifiedAmenitiesCustomTextUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 21);
			this.AddElementFieldMapping("TimestampEntity", "PublishedAmenitiesCustomTextUTC", "PublishedAmenitiesCustomTextUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 22);
			this.AddElementFieldMapping("TimestampEntity", "ModifiedAnnouncementActionsUTC", "ModifiedAnnouncementActionsUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 23);
			this.AddElementFieldMapping("TimestampEntity", "PublishedAnnouncementActionsUTC", "PublishedAnnouncementActionsUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 24);
			this.AddElementFieldMapping("TimestampEntity", "ModifiedAvailabilitiesUTC", "ModifiedAvailabilitiesUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 25);
			this.AddElementFieldMapping("TimestampEntity", "PublishedAvailabilitiesUTC", "PublishedAvailabilitiesUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 26);
			this.AddElementFieldMapping("TimestampEntity", "ModifiedAvailabilitiesCustomTextUTC", "ModifiedAvailabilitiesCustomTextUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 27);
			this.AddElementFieldMapping("TimestampEntity", "PublishedAvailabilitiesCustomTextUTC", "PublishedAvailabilitiesCustomTextUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 28);
			this.AddElementFieldMapping("TimestampEntity", "ModifiedCloudStorageAccountsUTC", "ModifiedCloudStorageAccountsUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 29);
			this.AddElementFieldMapping("TimestampEntity", "PublishedCloudStorageAccountsUTC", "PublishedCloudStorageAccountsUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 30);
			this.AddElementFieldMapping("TimestampEntity", "ModifiedCompanyUTC", "ModifiedCompanyUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 31);
			this.AddElementFieldMapping("TimestampEntity", "PublishedCompanyUTC", "PublishedCompanyUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 32);
			this.AddElementFieldMapping("TimestampEntity", "ModifiedCompanyCustomTextUTC", "ModifiedCompanyCustomTextUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 33);
			this.AddElementFieldMapping("TimestampEntity", "PublishedCompanyCustomTextUTC", "PublishedCompanyCustomTextUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 34);
			this.AddElementFieldMapping("TimestampEntity", "ModifiedCompanyMediaUTC", "ModifiedCompanyMediaUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 35);
			this.AddElementFieldMapping("TimestampEntity", "PublishedCompanyMediaUTC", "PublishedCompanyMediaUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 36);
			this.AddElementFieldMapping("TimestampEntity", "ModifiedMessagegroupsUTC", "ModifiedMessagegroupsUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 37);
			this.AddElementFieldMapping("TimestampEntity", "PublishedMessagegroupsUTC", "PublishedMessagegroupsUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 38);
			this.AddElementFieldMapping("TimestampEntity", "ModifiedMessageTemplatesUTC", "ModifiedMessageTemplatesUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 39);
			this.AddElementFieldMapping("TimestampEntity", "PublishedMessageTemplatesUTC", "PublishedMessageTemplatesUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 40);
			this.AddElementFieldMapping("TimestampEntity", "ModifiedPmsTerminalStatusUTC", "ModifiedPmsTerminalStatusUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 41);
			this.AddElementFieldMapping("TimestampEntity", "PublishedPmsTerminalStatusUTC", "PublishedPmsTerminalStatusUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 42);
			this.AddElementFieldMapping("TimestampEntity", "ModifiedUIModesUTC", "ModifiedUIModesUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 43);
			this.AddElementFieldMapping("TimestampEntity", "PublishedUIModesUTC", "PublishedUIModesUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 44);
			this.AddElementFieldMapping("TimestampEntity", "ModifiedUIModesCustomTextUTC", "ModifiedUIModesCustomTextUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 45);
			this.AddElementFieldMapping("TimestampEntity", "PublishedUIModesCustomTextUTC", "PublishedUIModesCustomTextUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 46);
			this.AddElementFieldMapping("TimestampEntity", "ModifiedUIModesMediaUTC", "ModifiedUIModesMediaUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 47);
			this.AddElementFieldMapping("TimestampEntity", "PublishedUIModesMediaUTC", "PublishedUIModesMediaUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 48);
			this.AddElementFieldMapping("TimestampEntity", "ModifiedCompanyVenueCategoriesUTC", "ModifiedCompanyVenueCategoriesUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 49);
			this.AddElementFieldMapping("TimestampEntity", "PublishedCompanyVenueCategoriesUTC", "PublishedCompanyVenueCategoriesUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 50);
			this.AddElementFieldMapping("TimestampEntity", "ModifiedCompanyVenueCategoriesCustomTextUTC", "ModifiedCompanyVenueCategoriesCustomTextUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 51);
			this.AddElementFieldMapping("TimestampEntity", "PublishedCompanyVenueCategoriesCustomTextUTC", "PublishedCompanyVenueCategoriesCustomTextUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 52);
			this.AddElementFieldMapping("TimestampEntity", "DeliverypointId", "DeliverypointId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 53);
			this.AddElementFieldMapping("TimestampEntity", "ModifiedMessagesUTC", "ModifiedMessagesUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 54);
			this.AddElementFieldMapping("TimestampEntity", "PublishedMessagesUTC", "PublishedMessagesUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 55);
			this.AddElementFieldMapping("TimestampEntity", "ModifiedDeliverypointUTC", "ModifiedDeliverypointUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 56);
			this.AddElementFieldMapping("TimestampEntity", "PublishedDeliverypointUTC", "PublishedDeliverypointUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 57);
			this.AddElementFieldMapping("TimestampEntity", "DeliverypointgroupId", "DeliverypointgroupId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 58);
			this.AddElementFieldMapping("TimestampEntity", "ModifiedDeliverypointsUTC", "ModifiedDeliverypointsUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 59);
			this.AddElementFieldMapping("TimestampEntity", "PublishedDeliverypointsUTC", "PublishedDeliverypointsUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 60);
			this.AddElementFieldMapping("TimestampEntity", "ModifiedDeliveryTimeUTC", "ModifiedDeliveryTimeUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 61);
			this.AddElementFieldMapping("TimestampEntity", "PublishedDeliveryTimeUTC", "PublishedDeliveryTimeUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 62);
			this.AddElementFieldMapping("TimestampEntity", "DeviceIdentifier", "DeviceIdentifier", true, "VarChar", 255, 0, 0, false, "", null, typeof(System.String), 63);
			this.AddElementFieldMapping("TimestampEntity", "ModifiedNetmessagesUTC", "ModifiedNetmessagesUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 64);
			this.AddElementFieldMapping("TimestampEntity", "PublishedNetmessagesUTC", "PublishedNetmessagesUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 65);
			this.AddElementFieldMapping("TimestampEntity", "EntertainmentConfigurationId", "EntertainmentConfigurationId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 66);
			this.AddElementFieldMapping("TimestampEntity", "ModifiedEntertainmentMediaUTC", "ModifiedEntertainmentMediaUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 67);
			this.AddElementFieldMapping("TimestampEntity", "PublishedEntertainmentMediaUTC", "PublishedEntertainmentMediaUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 68);
			this.AddElementFieldMapping("TimestampEntity", "ModifiedEntertainmentConfigurationUTC", "ModifiedEntertainmentConfigurationUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 69);
			this.AddElementFieldMapping("TimestampEntity", "PublishedEntertainmentConfigurationUTC", "PublishedEntertainmentConfigurationUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 70);
			this.AddElementFieldMapping("TimestampEntity", "ModifiedEntertainmentConfigurationMediaUTC", "ModifiedEntertainmentConfigurationMediaUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 71);
			this.AddElementFieldMapping("TimestampEntity", "PublishedEntertainmentConfigurationMediaUTC", "PublishedEntertainmentConfigurationMediaUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 72);
			this.AddElementFieldMapping("TimestampEntity", "ModifiedEntertainmentCategoriesUTC", "ModifiedEntertainmentCategoriesUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 73);
			this.AddElementFieldMapping("TimestampEntity", "PublishedEntertainmentCategoriesUTC", "PublishedEntertainmentCategoriesUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 74);
			this.AddElementFieldMapping("TimestampEntity", "ModifiedEntertainmentCategoriesCustomTextUTC", "ModifiedEntertainmentCategoriesCustomTextUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 75);
			this.AddElementFieldMapping("TimestampEntity", "PublishedEntertainmentCategoriesCustomTextUTC", "PublishedEntertainmentCategoriesCustomTextUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 76);
			this.AddElementFieldMapping("TimestampEntity", "ModifiedEntertainmentCategoriesMediaUTC", "ModifiedEntertainmentCategoriesMediaUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 77);
			this.AddElementFieldMapping("TimestampEntity", "PublishedEntertainmentCategoriesMediaUTC", "PublishedEntertainmentCategoriesMediaUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 78);
			this.AddElementFieldMapping("TimestampEntity", "InfraredConfigurationId", "InfraredConfigurationId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 79);
			this.AddElementFieldMapping("TimestampEntity", "ModifiedInfraredConfigurationUTC", "ModifiedInfraredConfigurationUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 80);
			this.AddElementFieldMapping("TimestampEntity", "PublishedInfraredConfigurationUTC", "PublishedInfraredConfigurationUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 81);
			this.AddElementFieldMapping("TimestampEntity", "MapId", "MapId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 82);
			this.AddElementFieldMapping("TimestampEntity", "ModifiedMapUTC", "ModifiedMapUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 83);
			this.AddElementFieldMapping("TimestampEntity", "PublishedMapUTC", "PublishedMapUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 84);
			this.AddElementFieldMapping("TimestampEntity", "MenuId", "MenuId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 85);
			this.AddElementFieldMapping("TimestampEntity", "ModifiedAlterationsUTC", "ModifiedAlterationsUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 86);
			this.AddElementFieldMapping("TimestampEntity", "PublishedAlterationsUTC", "PublishedAlterationsUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 87);
			this.AddElementFieldMapping("TimestampEntity", "ModifiedAlterationsCustomTextUTC", "ModifiedAlterationsCustomTextUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 88);
			this.AddElementFieldMapping("TimestampEntity", "PublishedAlterationsCustomTextUTC", "PublishedAlterationsCustomTextUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 89);
			this.AddElementFieldMapping("TimestampEntity", "ModifiedAlterationsMediaUTC", "ModifiedAlterationsMediaUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 90);
			this.AddElementFieldMapping("TimestampEntity", "PublishedAlterationsMediaUTC", "PublishedAlterationsMediaUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 91);
			this.AddElementFieldMapping("TimestampEntity", "ModifiedMenuUTC", "ModifiedMenuUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 92);
			this.AddElementFieldMapping("TimestampEntity", "PublishedMenuUTC", "PublishedMenuUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 93);
			this.AddElementFieldMapping("TimestampEntity", "ModifiedMenuCustomTextUTC", "ModifiedMenuCustomTextUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 94);
			this.AddElementFieldMapping("TimestampEntity", "PublishedMenuCustomTextUTC", "PublishedMenuCustomTextUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 95);
			this.AddElementFieldMapping("TimestampEntity", "ModifiedMenuMediaUTC", "ModifiedMenuMediaUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 96);
			this.AddElementFieldMapping("TimestampEntity", "PublishedMenuMediaUTC", "PublishedMenuMediaUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 97);
			this.AddElementFieldMapping("TimestampEntity", "ModifiedProductgroupsUTC", "ModifiedProductgroupsUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 98);
			this.AddElementFieldMapping("TimestampEntity", "PublishedProductgroupsUTC", "PublishedProductgroupsUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 99);
			this.AddElementFieldMapping("TimestampEntity", "ModifiedProductgroupsCustomTextUTC", "ModifiedProductgroupsCustomTextUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 100);
			this.AddElementFieldMapping("TimestampEntity", "PublishedProductgroupsCustomTextUTC", "PublishedProductgroupsCustomTextUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 101);
			this.AddElementFieldMapping("TimestampEntity", "ModifiedProductgroupsMediaUTC", "ModifiedProductgroupsMediaUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 102);
			this.AddElementFieldMapping("TimestampEntity", "PublishedProductgroupsMediaUTC", "PublishedProductgroupsMediaUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 103);
			this.AddElementFieldMapping("TimestampEntity", "ModifiedProductsUTC", "ModifiedProductsUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 104);
			this.AddElementFieldMapping("TimestampEntity", "PublishedProductsUTC", "PublishedProductsUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 105);
			this.AddElementFieldMapping("TimestampEntity", "ModifiedProductsCustomTextUTC", "ModifiedProductsCustomTextUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 106);
			this.AddElementFieldMapping("TimestampEntity", "PublishedProductsCustomTextUTC", "PublishedProductsCustomTextUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 107);
			this.AddElementFieldMapping("TimestampEntity", "ModifiedProductsMediaUTC", "ModifiedProductsMediaUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 108);
			this.AddElementFieldMapping("TimestampEntity", "PublishedProductsMediaUTC", "PublishedProductsMediaUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 109);
			this.AddElementFieldMapping("TimestampEntity", "ModifiedSchedulesUTC", "ModifiedSchedulesUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 110);
			this.AddElementFieldMapping("TimestampEntity", "PublishedSchedulesUTC", "PublishedSchedulesUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 111);
			this.AddElementFieldMapping("TimestampEntity", "PointOfInterestId", "PointOfInterestId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 112);
			this.AddElementFieldMapping("TimestampEntity", "ModifiedPointOfInterestUTC", "ModifiedPointOfInterestUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 113);
			this.AddElementFieldMapping("TimestampEntity", "PublishedPointOfInterestUTC", "PublishedPointOfInterestUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 114);
			this.AddElementFieldMapping("TimestampEntity", "ModifiedPointOfInterestCustomTextUTC", "ModifiedPointOfInterestCustomTextUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 115);
			this.AddElementFieldMapping("TimestampEntity", "PublishedPointOfInterestCustomTextUTC", "PublishedPointOfInterestCustomTextUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 116);
			this.AddElementFieldMapping("TimestampEntity", "ModifiedPointOfInterestMediaUTC", "ModifiedPointOfInterestMediaUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 117);
			this.AddElementFieldMapping("TimestampEntity", "PublishedPointOfInterestMediaUTC", "PublishedPointOfInterestMediaUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 118);
			this.AddElementFieldMapping("TimestampEntity", "ModifiedPointOfInterestVenueCategoriesUTC", "ModifiedPointOfInterestVenueCategoriesUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 119);
			this.AddElementFieldMapping("TimestampEntity", "PublishedPointOfInterestVenueCategoriesUTC", "PublishedPointOfInterestVenueCategoriesUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 120);
			this.AddElementFieldMapping("TimestampEntity", "ModifiedPointOfInterestVenueCategoriesCustomTextUTC", "ModifiedPointOfInterestVenueCategoriesCustomTextUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 121);
			this.AddElementFieldMapping("TimestampEntity", "PublishedPointOfInterestVenueCategoriesCustomTextUTC", "PublishedPointOfInterestVenueCategoriesCustomTextUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 122);
			this.AddElementFieldMapping("TimestampEntity", "PriceScheduleId", "PriceScheduleId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 123);
			this.AddElementFieldMapping("TimestampEntity", "ModifiedPriceScheduleUTC", "ModifiedPriceScheduleUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 124);
			this.AddElementFieldMapping("TimestampEntity", "PublishedPriceScheduleUTC", "PublishedPriceScheduleUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 125);
			this.AddElementFieldMapping("TimestampEntity", "RoomControlConfigurationId", "RoomControlConfigurationId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 126);
			this.AddElementFieldMapping("TimestampEntity", "ModifiedRoomControlConfigurationUTC", "ModifiedRoomControlConfigurationUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 127);
			this.AddElementFieldMapping("TimestampEntity", "PublishedRoomControlConfigurationUTC", "PublishedRoomControlConfigurationUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 128);
			this.AddElementFieldMapping("TimestampEntity", "ModifiedRoomControlConfigurationCustomTextUTC", "ModifiedRoomControlConfigurationCustomTextUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 129);
			this.AddElementFieldMapping("TimestampEntity", "PublishedRoomControlConfigurationCustomTextUTC", "PublishedRoomControlConfigurationCustomTextUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 130);
			this.AddElementFieldMapping("TimestampEntity", "ModifiedRoomControlConfigurationMediaUTC", "ModifiedRoomControlConfigurationMediaUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 131);
			this.AddElementFieldMapping("TimestampEntity", "PublishedRoomControlConfigurationMediaUTC", "PublishedRoomControlConfigurationMediaUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 132);
			this.AddElementFieldMapping("TimestampEntity", "SiteId", "SiteId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 133);
			this.AddElementFieldMapping("TimestampEntity", "ModifiedSiteUTC", "ModifiedSiteUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 134);
			this.AddElementFieldMapping("TimestampEntity", "PublishedSiteUTC", "PublishedSiteUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 135);
			this.AddElementFieldMapping("TimestampEntity", "ModifiedSiteCustomTextUTC", "ModifiedSiteCustomTextUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 136);
			this.AddElementFieldMapping("TimestampEntity", "PublishedSiteCustomTextUTC", "PublishedSiteCustomTextUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 137);
			this.AddElementFieldMapping("TimestampEntity", "ModifiedSiteMediaUTC", "ModifiedSiteMediaUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 138);
			this.AddElementFieldMapping("TimestampEntity", "PublishedSiteMediaUTC", "PublishedSiteMediaUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 139);
			this.AddElementFieldMapping("TimestampEntity", "TerminalId", "TerminalId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 140);
			this.AddElementFieldMapping("TimestampEntity", "ModifiedTerminalUTC", "ModifiedTerminalUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 141);
			this.AddElementFieldMapping("TimestampEntity", "PublishedTerminalUTC", "PublishedTerminalUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 142);
			this.AddElementFieldMapping("TimestampEntity", "ModifiedOrdersHistoryUTC", "ModifiedOrdersHistoryUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 143);
			this.AddElementFieldMapping("TimestampEntity", "PublishedOrdersHistoryUTC", "PublishedOrdersHistoryUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 144);
			this.AddElementFieldMapping("TimestampEntity", "ModifiedOrdersMasterUTC", "ModifiedOrdersMasterUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 145);
			this.AddElementFieldMapping("TimestampEntity", "PublishedOrdersMasterUTC", "PublishedOrdersMasterUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 146);
			this.AddElementFieldMapping("TimestampEntity", "ModifiedOrdersUTC", "ModifiedOrdersUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 147);
			this.AddElementFieldMapping("TimestampEntity", "PublishedOrdersUTC", "PublishedOrdersUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 148);
			this.AddElementFieldMapping("TimestampEntity", "TerminalConfigurationId", "TerminalConfigurationId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 149);
			this.AddElementFieldMapping("TimestampEntity", "ModifiedTerminalConfigurationUTC", "ModifiedTerminalConfigurationUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 150);
			this.AddElementFieldMapping("TimestampEntity", "PublishedTerminalConfigurationUTC", "PublishedTerminalConfigurationUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 151);
			this.AddElementFieldMapping("TimestampEntity", "UIModeId", "UIModeId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 152);
			this.AddElementFieldMapping("TimestampEntity", "ModifiedUIModeUTC", "ModifiedUIModeUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 153);
			this.AddElementFieldMapping("TimestampEntity", "PublishedUIModeUTC", "PublishedUIModeUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 154);
			this.AddElementFieldMapping("TimestampEntity", "ModifiedUIModeCustomTextUTC", "ModifiedUIModeCustomTextUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 155);
			this.AddElementFieldMapping("TimestampEntity", "PublishedUIModeCustomTextUTC", "PublishedUIModeCustomTextUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 156);
			this.AddElementFieldMapping("TimestampEntity", "ModifiedUIModeMediaUTC", "ModifiedUIModeMediaUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 157);
			this.AddElementFieldMapping("TimestampEntity", "PublishedUIModeMediaUTC", "PublishedUIModeMediaUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 158);
			this.AddElementFieldMapping("TimestampEntity", "UIScheduleId", "UIScheduleId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 159);
			this.AddElementFieldMapping("TimestampEntity", "ModifiedUIScheduleUTC", "ModifiedUIScheduleUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 160);
			this.AddElementFieldMapping("TimestampEntity", "PublishedUIScheduleUTC", "PublishedUIScheduleUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 161);
			this.AddElementFieldMapping("TimestampEntity", "UIThemeId", "UIThemeId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 162);
			this.AddElementFieldMapping("TimestampEntity", "ModifiedUIThemeUTC", "ModifiedUIThemeUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 163);
			this.AddElementFieldMapping("TimestampEntity", "PublishedUIThemeUTC", "PublishedUIThemeUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 164);
			this.AddElementFieldMapping("TimestampEntity", "ModifiedUIThemeMediaUTC", "ModifiedUIThemeMediaUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 165);
			this.AddElementFieldMapping("TimestampEntity", "PublishedUIThemeMediaUTC", "PublishedUIThemeMediaUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 166);
			this.AddElementFieldMapping("TimestampEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 167);
			this.AddElementFieldMapping("TimestampEntity", "CreatedBy", "CreatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 168);
			this.AddElementFieldMapping("TimestampEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 169);
			this.AddElementFieldMapping("TimestampEntity", "UpdatedBy", "UpdatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 170);
			this.AddElementFieldMapping("TimestampEntity", "ModifiedCompanyReleasesUTC", "ModifiedCompanyReleasesUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 171);
			this.AddElementFieldMapping("TimestampEntity", "PublishedCompanyReleasesUTC", "PublishedCompanyReleasesUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 172);
			this.AddElementFieldMapping("TimestampEntity", "ModifiedMapCustomTextUTC", "ModifiedMapCustomTextUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 173);
			this.AddElementFieldMapping("TimestampEntity", "PublishedMapCustomTextUTC", "PublishedMapCustomTextUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 174);
			this.AddElementFieldMapping("TimestampEntity", "ModifiedMapMediaUTC", "ModifiedMapMediaUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 175);
			this.AddElementFieldMapping("TimestampEntity", "PublishedMapMediaUTC", "PublishedMapMediaUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 176);
			this.AddElementFieldMapping("TimestampEntity", "ModifiedInfraredConfigurationCustomTextUTC", "ModifiedInfraredConfigurationCustomTextUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 177);
			this.AddElementFieldMapping("TimestampEntity", "PublishedInfraredConfigurationCustomTextUTC", "PublishedInfraredConfigurationCustomTextUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 178);
		}

		/// <summary>Inits TimeZoneEntity's mappings</summary>
		private void InitTimeZoneEntityMappings()
		{
			this.AddElementMapping("TimeZoneEntity", @"Obymobi", @"dbo", "TimeZone", 8, 0);
			this.AddElementFieldMapping("TimeZoneEntity", "TimeZoneId", "TimeZoneId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("TimeZoneEntity", "Name", "Name", false, "VarChar", 255, 0, 0, false, "", null, typeof(System.String), 1);
			this.AddElementFieldMapping("TimeZoneEntity", "NameAndroid", "NameAndroid", false, "VarChar", 255, 0, 0, false, "", null, typeof(System.String), 2);
			this.AddElementFieldMapping("TimeZoneEntity", "CreatedBy", "CreatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("TimeZoneEntity", "UpdatedBy", "UpdatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("TimeZoneEntity", "NameDotNet", "NameDotNet", false, "VarChar", 255, 0, 0, false, "", null, typeof(System.String), 5);
			this.AddElementFieldMapping("TimeZoneEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 6);
			this.AddElementFieldMapping("TimeZoneEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 7);
		}

		/// <summary>Inits TranslationEntity's mappings</summary>
		private void InitTranslationEntityMappings()
		{
			this.AddElementMapping("TranslationEntity", @"Obymobi", @"dbo", "Translation", 9, 0);
			this.AddElementFieldMapping("TranslationEntity", "TranslationId", "TranslationId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("TranslationEntity", "TranslationKey", "TranslationKey", false, "VarChar", 500, 0, 0, false, "", null, typeof(System.String), 1);
			this.AddElementFieldMapping("TranslationEntity", "LanguageCode", "LanguageCode", false, "Char", 2, 0, 0, false, "", null, typeof(System.String), 2);
			this.AddElementFieldMapping("TranslationEntity", "TranslationValue", "TranslationValue", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 3);
			this.AddElementFieldMapping("TranslationEntity", "LocallyCreated", "LocallyCreated", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 4);
			this.AddElementFieldMapping("TranslationEntity", "CreatedBy", "CreatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 5);
			this.AddElementFieldMapping("TranslationEntity", "UpdatedBy", "UpdatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 6);
			this.AddElementFieldMapping("TranslationEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 7);
			this.AddElementFieldMapping("TranslationEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 8);
		}

		/// <summary>Inits UIElementEntity's mappings</summary>
		private void InitUIElementEntityMappings()
		{
			this.AddElementMapping("UIElementEntity", @"Obymobi", @"dbo", "UIElement", 19, 0);
			this.AddElementFieldMapping("UIElementEntity", "UIElementId", "UIElementId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("UIElementEntity", "ModuleNameSystem", "ModuleNameSystem", true, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 1);
			this.AddElementFieldMapping("UIElementEntity", "NameSystem", "NameSystem", true, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 2);
			this.AddElementFieldMapping("UIElementEntity", "NameFull", "NameFull", true, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 3);
			this.AddElementFieldMapping("UIElementEntity", "NameShort", "NameShort", true, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 4);
			this.AddElementFieldMapping("UIElementEntity", "Url", "Url", true, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 5);
			this.AddElementFieldMapping("UIElementEntity", "DisplayInMenu", "DisplayInMenu", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 6);
			this.AddElementFieldMapping("UIElementEntity", "TypeNameFull", "TypeNameFull", true, "VarChar", 255, 0, 0, false, "", null, typeof(System.String), 7);
			this.AddElementFieldMapping("UIElementEntity", "SortOrder", "SortOrder", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 8);
			this.AddElementFieldMapping("UIElementEntity", "FreeAccess", "FreeAccess", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 9);
			this.AddElementFieldMapping("UIElementEntity", "LicensingFree", "LicensingFree", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 10);
			this.AddElementFieldMapping("UIElementEntity", "UserRightsFree", "UserRightsFree", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 11);
			this.AddElementFieldMapping("UIElementEntity", "IsConfigurationItem", "IsConfigurationItem", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 12);
			this.AddElementFieldMapping("UIElementEntity", "EntityName", "EntityName", true, "NVarChar", 150, 0, 0, false, "", null, typeof(System.String), 13);
			this.AddElementFieldMapping("UIElementEntity", "ForLocalUseWhileSyncingIsUpdated", "ForLocalUseWhileSyncingIsUpdated", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 14);
			this.AddElementFieldMapping("UIElementEntity", "CreatedBy", "CreatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 15);
			this.AddElementFieldMapping("UIElementEntity", "UpdatedBy", "UpdatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 16);
			this.AddElementFieldMapping("UIElementEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 17);
			this.AddElementFieldMapping("UIElementEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 18);
		}

		/// <summary>Inits UIElementCustomEntity's mappings</summary>
		private void InitUIElementCustomEntityMappings()
		{
			this.AddElementMapping("UIElementCustomEntity", @"Obymobi", @"dbo", "UIElementCustom", 11, 0);
			this.AddElementFieldMapping("UIElementCustomEntity", "TypeNameFull", "TypeNameFull", false, "VarChar", 255, 0, 0, false, "", null, typeof(System.String), 0);
			this.AddElementFieldMapping("UIElementCustomEntity", "ModuleNameSystem", "ModuleNameSystem", true, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 1);
			this.AddElementFieldMapping("UIElementCustomEntity", "NameFull", "NameFull", true, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 2);
			this.AddElementFieldMapping("UIElementCustomEntity", "NameShort", "NameShort", true, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 3);
			this.AddElementFieldMapping("UIElementCustomEntity", "SortOrder", "SortOrder", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("UIElementCustomEntity", "IsConfigurationItem", "IsConfigurationItem", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 5);
			this.AddElementFieldMapping("UIElementCustomEntity", "ForLocalUseWhileSyncingIsUpdated", "ForLocalUseWhileSyncingIsUpdated", true, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 6);
			this.AddElementFieldMapping("UIElementCustomEntity", "CreatedBy", "CreatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 7);
			this.AddElementFieldMapping("UIElementCustomEntity", "UpdatedBy", "UpdatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 8);
			this.AddElementFieldMapping("UIElementCustomEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 9);
			this.AddElementFieldMapping("UIElementCustomEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 10);
		}

		/// <summary>Inits UIElementSubPanelEntity's mappings</summary>
		private void InitUIElementSubPanelEntityMappings()
		{
			this.AddElementMapping("UIElementSubPanelEntity", @"Obymobi", @"dbo", "UIElementSubPanel", 16, 0);
			this.AddElementFieldMapping("UIElementSubPanelEntity", "UIElementSubPanelId", "UIElementSubPanelId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("UIElementSubPanelEntity", "NameSystem", "NameSystem", true, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 1);
			this.AddElementFieldMapping("UIElementSubPanelEntity", "NameFull", "NameFull", true, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 2);
			this.AddElementFieldMapping("UIElementSubPanelEntity", "NameShort", "NameShort", true, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 3);
			this.AddElementFieldMapping("UIElementSubPanelEntity", "Url", "Url", true, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 4);
			this.AddElementFieldMapping("UIElementSubPanelEntity", "TypeNameFull", "TypeNameFull", true, "VarChar", 255, 0, 0, false, "", null, typeof(System.String), 5);
			this.AddElementFieldMapping("UIElementSubPanelEntity", "SortOrder", "SortOrder", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 6);
			this.AddElementFieldMapping("UIElementSubPanelEntity", "FreeAccess", "FreeAccess", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 7);
			this.AddElementFieldMapping("UIElementSubPanelEntity", "LicensingFree", "LicensingFree", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 8);
			this.AddElementFieldMapping("UIElementSubPanelEntity", "EntityName", "EntityName", true, "NVarChar", 150, 0, 0, false, "", null, typeof(System.String), 9);
			this.AddElementFieldMapping("UIElementSubPanelEntity", "OnTabPage", "OnTabPage", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 10);
			this.AddElementFieldMapping("UIElementSubPanelEntity", "ForLocalUseWhileSyncingIsUpdated", "ForLocalUseWhileSyncingIsUpdated", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 11);
			this.AddElementFieldMapping("UIElementSubPanelEntity", "CreatedBy", "CreatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 12);
			this.AddElementFieldMapping("UIElementSubPanelEntity", "UpdatedBy", "UpdatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 13);
			this.AddElementFieldMapping("UIElementSubPanelEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 14);
			this.AddElementFieldMapping("UIElementSubPanelEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 15);
		}

		/// <summary>Inits UIElementSubPanelCustomEntity's mappings</summary>
		private void InitUIElementSubPanelCustomEntityMappings()
		{
			this.AddElementMapping("UIElementSubPanelCustomEntity", @"Obymobi", @"dbo", "UIElementSubPanelCustom", 10, 0);
			this.AddElementFieldMapping("UIElementSubPanelCustomEntity", "TypeNameFull", "TypeNameFull", false, "VarChar", 255, 0, 0, false, "", null, typeof(System.String), 0);
			this.AddElementFieldMapping("UIElementSubPanelCustomEntity", "NameFull", "NameFull", true, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 1);
			this.AddElementFieldMapping("UIElementSubPanelCustomEntity", "NameShort", "NameShort", true, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 2);
			this.AddElementFieldMapping("UIElementSubPanelCustomEntity", "SortOrder", "SortOrder", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("UIElementSubPanelCustomEntity", "OnTabPage", "OnTabPage", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 4);
			this.AddElementFieldMapping("UIElementSubPanelCustomEntity", "ForLocalUseWhileSyncingIsUpdated", "ForLocalUseWhileSyncingIsUpdated", true, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 5);
			this.AddElementFieldMapping("UIElementSubPanelCustomEntity", "CreatedBy", "CreatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 6);
			this.AddElementFieldMapping("UIElementSubPanelCustomEntity", "UpdatedBy", "UpdatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 7);
			this.AddElementFieldMapping("UIElementSubPanelCustomEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 8);
			this.AddElementFieldMapping("UIElementSubPanelCustomEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 9);
		}

		/// <summary>Inits UIElementSubPanelUIElementEntity's mappings</summary>
		private void InitUIElementSubPanelUIElementEntityMappings()
		{
			this.AddElementMapping("UIElementSubPanelUIElementEntity", @"Obymobi", @"dbo", "UIElementSubPanelUIElement", 9, 0);
			this.AddElementFieldMapping("UIElementSubPanelUIElementEntity", "UIElementSubPanelUIElementId", "UIElementSubPanelUIElementId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("UIElementSubPanelUIElementEntity", "UIElementSubPanelTypeNameFull", "UIElementSubPanelTypeNameFull", false, "VarChar", 255, 0, 0, false, "", null, typeof(System.String), 1);
			this.AddElementFieldMapping("UIElementSubPanelUIElementEntity", "ParentUIElementTypeNameFull", "ParentUIElementTypeNameFull", true, "VarChar", 255, 0, 0, false, "", null, typeof(System.String), 2);
			this.AddElementFieldMapping("UIElementSubPanelUIElementEntity", "ParentUIElementSubPanelTypeNameFull", "ParentUIElementSubPanelTypeNameFull", true, "VarChar", 255, 0, 0, false, "", null, typeof(System.String), 3);
			this.AddElementFieldMapping("UIElementSubPanelUIElementEntity", "ForLocalUseWhileSyncingIsUpdated", "ForLocalUseWhileSyncingIsUpdated", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 4);
			this.AddElementFieldMapping("UIElementSubPanelUIElementEntity", "CreatedBy", "CreatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 5);
			this.AddElementFieldMapping("UIElementSubPanelUIElementEntity", "UpdatedBy", "UpdatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 6);
			this.AddElementFieldMapping("UIElementSubPanelUIElementEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 7);
			this.AddElementFieldMapping("UIElementSubPanelUIElementEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 8);
		}

		/// <summary>Inits UIFooterItemEntity's mappings</summary>
		private void InitUIFooterItemEntityMappings()
		{
			this.AddElementMapping("UIFooterItemEntity", @"Obymobi", @"dbo", "UIFooterItem", 13, 0);
			this.AddElementFieldMapping("UIFooterItemEntity", "UIFooterItemId", "UIFooterItemId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("UIFooterItemEntity", "UIModeId", "UIModeId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("UIFooterItemEntity", "Name", "Name", false, "VarChar", 50, 0, 0, false, "", null, typeof(System.String), 2);
			this.AddElementFieldMapping("UIFooterItemEntity", "Type", "Type", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("UIFooterItemEntity", "Position", "Position", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("UIFooterItemEntity", "SortOrder", "SortOrder", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 5);
			this.AddElementFieldMapping("UIFooterItemEntity", "ActionIntent", "ActionIntent", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 6);
			this.AddElementFieldMapping("UIFooterItemEntity", "Visible", "Visible", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 7);
			this.AddElementFieldMapping("UIFooterItemEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 8);
			this.AddElementFieldMapping("UIFooterItemEntity", "CreatedBy", "CreatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 9);
			this.AddElementFieldMapping("UIFooterItemEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 10);
			this.AddElementFieldMapping("UIFooterItemEntity", "UpdatedBy", "UpdatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 11);
			this.AddElementFieldMapping("UIFooterItemEntity", "ParentCompanyId", "ParentCompanyId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 12);
		}

		/// <summary>Inits UIFooterItemLanguageEntity's mappings</summary>
		private void InitUIFooterItemLanguageEntityMappings()
		{
			this.AddElementMapping("UIFooterItemLanguageEntity", @"Obymobi", @"dbo", "UIFooterItemLanguage", 9, 0);
			this.AddElementFieldMapping("UIFooterItemLanguageEntity", "UIFooterItemLanguageId", "UIFooterItemLanguageId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("UIFooterItemLanguageEntity", "UIFooterItemId", "UIFooterItemId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("UIFooterItemLanguageEntity", "LanguageId", "LanguageId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("UIFooterItemLanguageEntity", "Name", "Name", true, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 3);
			this.AddElementFieldMapping("UIFooterItemLanguageEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 4);
			this.AddElementFieldMapping("UIFooterItemLanguageEntity", "CreatedBy", "CreatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 5);
			this.AddElementFieldMapping("UIFooterItemLanguageEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 6);
			this.AddElementFieldMapping("UIFooterItemLanguageEntity", "UpdatedBy", "UpdatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 7);
			this.AddElementFieldMapping("UIFooterItemLanguageEntity", "ParentCompanyId", "ParentCompanyId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 8);
		}

		/// <summary>Inits UIModeEntity's mappings</summary>
		private void InitUIModeEntityMappings()
		{
			this.AddElementMapping("UIModeEntity", @"Obymobi", @"dbo", "UIMode", 18, 0);
			this.AddElementFieldMapping("UIModeEntity", "UIModeId", "UIModeId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("UIModeEntity", "CompanyId", "CompanyId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("UIModeEntity", "Name", "Name", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 2);
			this.AddElementFieldMapping("UIModeEntity", "ShowServiceOptions", "ShowServiceOptions", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 3);
			this.AddElementFieldMapping("UIModeEntity", "ShowRequestBill", "ShowRequestBill", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 4);
			this.AddElementFieldMapping("UIModeEntity", "ShowDeliverypoint", "ShowDeliverypoint", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 5);
			this.AddElementFieldMapping("UIModeEntity", "ShowClock", "ShowClock", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 6);
			this.AddElementFieldMapping("UIModeEntity", "ShowBattery", "ShowBattery", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 7);
			this.AddElementFieldMapping("UIModeEntity", "ShowBrightness", "ShowBrightness", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 8);
			this.AddElementFieldMapping("UIModeEntity", "ShowFullscreenEyecatcher", "ShowFullscreenEyecatcher", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 9);
			this.AddElementFieldMapping("UIModeEntity", "CreatedBy", "CreatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 10);
			this.AddElementFieldMapping("UIModeEntity", "UpdatedBy", "UpdatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 11);
			this.AddElementFieldMapping("UIModeEntity", "Type", "Type", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 12);
			this.AddElementFieldMapping("UIModeEntity", "DefaultUITabId", "DefaultUITabId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 13);
			this.AddElementFieldMapping("UIModeEntity", "PointOfInterestId", "PointOfInterestId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 14);
			this.AddElementFieldMapping("UIModeEntity", "ShowFooterLogo", "ShowFooterLogo", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 15);
			this.AddElementFieldMapping("UIModeEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 16);
			this.AddElementFieldMapping("UIModeEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 17);
		}

		/// <summary>Inits UIScheduleEntity's mappings</summary>
		private void InitUIScheduleEntityMappings()
		{
			this.AddElementMapping("UIScheduleEntity", @"Obymobi", @"dbo", "UISchedule", 9, 0);
			this.AddElementFieldMapping("UIScheduleEntity", "UIScheduleId", "UIScheduleId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("UIScheduleEntity", "CompanyId", "CompanyId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("UIScheduleEntity", "DeliverypointgroupId", "DeliverypointgroupId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("UIScheduleEntity", "Name", "Name", false, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 3);
			this.AddElementFieldMapping("UIScheduleEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 4);
			this.AddElementFieldMapping("UIScheduleEntity", "CreatedBy", "CreatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 5);
			this.AddElementFieldMapping("UIScheduleEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 6);
			this.AddElementFieldMapping("UIScheduleEntity", "UpdatedBy", "UpdatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 7);
			this.AddElementFieldMapping("UIScheduleEntity", "Type", "Type", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 8);
		}

		/// <summary>Inits UIScheduleItemEntity's mappings</summary>
		private void InitUIScheduleItemEntityMappings()
		{
			this.AddElementMapping("UIScheduleItemEntity", @"Obymobi", @"dbo", "UIScheduleItem", 11, 0);
			this.AddElementFieldMapping("UIScheduleItemEntity", "UIScheduleItemId", "UIScheduleItemId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("UIScheduleItemEntity", "ParentCompanyId", "ParentCompanyId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("UIScheduleItemEntity", "UIScheduleId", "UIScheduleId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("UIScheduleItemEntity", "UIWidgetId", "UIWidgetId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("UIScheduleItemEntity", "MediaId", "MediaId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("UIScheduleItemEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 5);
			this.AddElementFieldMapping("UIScheduleItemEntity", "CreatedBy", "CreatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 6);
			this.AddElementFieldMapping("UIScheduleItemEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 7);
			this.AddElementFieldMapping("UIScheduleItemEntity", "UpdatedBy", "UpdatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 8);
			this.AddElementFieldMapping("UIScheduleItemEntity", "ScheduledMessageId", "ScheduledMessageId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 9);
			this.AddElementFieldMapping("UIScheduleItemEntity", "ReportProcessingTaskTemplateId", "ReportProcessingTaskTemplateId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 10);
		}

		/// <summary>Inits UIScheduleItemOccurrenceEntity's mappings</summary>
		private void InitUIScheduleItemOccurrenceEntityMappings()
		{
			this.AddElementMapping("UIScheduleItemOccurrenceEntity", @"Obymobi", @"dbo", "UIScheduleItemOccurrence", 33, 0);
			this.AddElementFieldMapping("UIScheduleItemOccurrenceEntity", "UIScheduleItemOccurrenceId", "UIScheduleItemOccurrenceId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("UIScheduleItemOccurrenceEntity", "ParentCompanyId", "ParentCompanyId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("UIScheduleItemOccurrenceEntity", "UIScheduleItemId", "UIScheduleItemId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("UIScheduleItemOccurrenceEntity", "StartTime", "StartTime", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 3);
			this.AddElementFieldMapping("UIScheduleItemOccurrenceEntity", "EndTime", "EndTime", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 4);
			this.AddElementFieldMapping("UIScheduleItemOccurrenceEntity", "Recurring", "Recurring", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 5);
			this.AddElementFieldMapping("UIScheduleItemOccurrenceEntity", "RecurrenceType", "RecurrenceType", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 6);
			this.AddElementFieldMapping("UIScheduleItemOccurrenceEntity", "RecurrenceRange", "RecurrenceRange", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 7);
			this.AddElementFieldMapping("UIScheduleItemOccurrenceEntity", "RecurrenceStart", "RecurrenceStart", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 8);
			this.AddElementFieldMapping("UIScheduleItemOccurrenceEntity", "RecurrenceEnd", "RecurrenceEnd", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 9);
			this.AddElementFieldMapping("UIScheduleItemOccurrenceEntity", "RecurrenceOccurrenceCount", "RecurrenceOccurrenceCount", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 10);
			this.AddElementFieldMapping("UIScheduleItemOccurrenceEntity", "RecurrencePeriodicity", "RecurrencePeriodicity", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 11);
			this.AddElementFieldMapping("UIScheduleItemOccurrenceEntity", "RecurrenceDayNumber", "RecurrenceDayNumber", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 12);
			this.AddElementFieldMapping("UIScheduleItemOccurrenceEntity", "RecurrenceWeekDays", "RecurrenceWeekDays", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 13);
			this.AddElementFieldMapping("UIScheduleItemOccurrenceEntity", "RecurrenceWeekOfMonth", "RecurrenceWeekOfMonth", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 14);
			this.AddElementFieldMapping("UIScheduleItemOccurrenceEntity", "RecurrenceMonth", "RecurrenceMonth", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 15);
			this.AddElementFieldMapping("UIScheduleItemOccurrenceEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 16);
			this.AddElementFieldMapping("UIScheduleItemOccurrenceEntity", "CreatedBy", "CreatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 17);
			this.AddElementFieldMapping("UIScheduleItemOccurrenceEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 18);
			this.AddElementFieldMapping("UIScheduleItemOccurrenceEntity", "UpdatedBy", "UpdatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 19);
			this.AddElementFieldMapping("UIScheduleItemOccurrenceEntity", "BackgroundColor", "BackgroundColor", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 20);
			this.AddElementFieldMapping("UIScheduleItemOccurrenceEntity", "TextColor", "TextColor", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 21);
			this.AddElementFieldMapping("UIScheduleItemOccurrenceEntity", "MessagegroupId", "MessagegroupId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 22);
			this.AddElementFieldMapping("UIScheduleItemOccurrenceEntity", "StartTimeUTC", "StartTimeUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 23);
			this.AddElementFieldMapping("UIScheduleItemOccurrenceEntity", "EndTimeUTC", "EndTimeUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 24);
			this.AddElementFieldMapping("UIScheduleItemOccurrenceEntity", "RecurrenceStartUTC", "RecurrenceStartUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 25);
			this.AddElementFieldMapping("UIScheduleItemOccurrenceEntity", "RecurrenceEndUTC", "RecurrenceEndUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 26);
			this.AddElementFieldMapping("UIScheduleItemOccurrenceEntity", "Type", "Type", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 27);
			this.AddElementFieldMapping("UIScheduleItemOccurrenceEntity", "RecurrenceIndex", "RecurrenceIndex", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 28);
			this.AddElementFieldMapping("UIScheduleItemOccurrenceEntity", "ParentUIScheduleItemOccurrenceId", "ParentUIScheduleItemOccurrenceId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 29);
			this.AddElementFieldMapping("UIScheduleItemOccurrenceEntity", "LastTriggeredUTC", "LastTriggeredUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 30);
			this.AddElementFieldMapping("UIScheduleItemOccurrenceEntity", "ReportProcessingTaskTemplateId", "ReportProcessingTaskTemplateId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 31);
			this.AddElementFieldMapping("UIScheduleItemOccurrenceEntity", "UIScheduleId", "UIScheduleId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 32);
		}

		/// <summary>Inits UITabEntity's mappings</summary>
		private void InitUITabEntityMappings()
		{
			this.AddElementMapping("UITabEntity", @"Obymobi", @"dbo", "UITab", 21, 0);
			this.AddElementFieldMapping("UITabEntity", "UITabId", "UITabId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("UITabEntity", "UIModeId", "UIModeId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("UITabEntity", "Caption", "Caption", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 2);
			this.AddElementFieldMapping("UITabEntity", "Type", "Type", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("UITabEntity", "CategoryId", "CategoryId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("UITabEntity", "EntertainmentId", "EntertainmentId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 5);
			this.AddElementFieldMapping("UITabEntity", "URL", "URL", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 6);
			this.AddElementFieldMapping("UITabEntity", "Width", "Width", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 7);
			this.AddElementFieldMapping("UITabEntity", "CreatedBy", "CreatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 8);
			this.AddElementFieldMapping("UITabEntity", "UpdatedBy", "UpdatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 9);
			this.AddElementFieldMapping("UITabEntity", "SortOrder", "SortOrder", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 10);
			this.AddElementFieldMapping("UITabEntity", "Zoom", "Zoom", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 11);
			this.AddElementFieldMapping("UITabEntity", "Visible", "Visible", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 12);
			this.AddElementFieldMapping("UITabEntity", "SiteId", "SiteId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 13);
			this.AddElementFieldMapping("UITabEntity", "RestrictedAccess", "RestrictedAccess", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 14);
			this.AddElementFieldMapping("UITabEntity", "ParentCompanyId", "ParentCompanyId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 15);
			this.AddElementFieldMapping("UITabEntity", "AllCategoryVisible", "AllCategoryVisible", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 16);
			this.AddElementFieldMapping("UITabEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 17);
			this.AddElementFieldMapping("UITabEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 18);
			this.AddElementFieldMapping("UITabEntity", "ShowPmsMessagegroups", "ShowPmsMessagegroups", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 19);
			this.AddElementFieldMapping("UITabEntity", "MapId", "MapId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 20);
		}

		/// <summary>Inits UITabLanguageEntity's mappings</summary>
		private void InitUITabLanguageEntityMappings()
		{
			this.AddElementMapping("UITabLanguageEntity", @"Obymobi", @"dbo", "UITabLanguage", 11, 0);
			this.AddElementFieldMapping("UITabLanguageEntity", "UITabLanguageId", "UITabLanguageId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("UITabLanguageEntity", "LanguageId", "LanguageId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("UITabLanguageEntity", "UITabId", "UITabId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("UITabLanguageEntity", "Caption", "Caption", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 3);
			this.AddElementFieldMapping("UITabLanguageEntity", "URL", "URL", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 4);
			this.AddElementFieldMapping("UITabLanguageEntity", "CreatedBy", "CreatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 5);
			this.AddElementFieldMapping("UITabLanguageEntity", "UpdatedBy", "UpdatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 6);
			this.AddElementFieldMapping("UITabLanguageEntity", "Zoom", "Zoom", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 7);
			this.AddElementFieldMapping("UITabLanguageEntity", "ParentCompanyId", "ParentCompanyId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 8);
			this.AddElementFieldMapping("UITabLanguageEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 9);
			this.AddElementFieldMapping("UITabLanguageEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 10);
		}

		/// <summary>Inits UIThemeEntity's mappings</summary>
		private void InitUIThemeEntityMappings()
		{
			this.AddElementMapping("UIThemeEntity", @"Obymobi", @"dbo", "UITheme", 130, 0);
			this.AddElementFieldMapping("UIThemeEntity", "UIThemeId", "UIThemeId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("UIThemeEntity", "ListCategoryBackgroundColor", "ListCategoryBackgroundColor", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("UIThemeEntity", "ListCategoryDividerColor", "ListCategoryDividerColor", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("UIThemeEntity", "ListCategoryTextColor", "ListCategoryTextColor", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("UIThemeEntity", "ListCategorySelectedBackgroundColor", "ListCategorySelectedBackgroundColor", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("UIThemeEntity", "ListCategorySelectedDividerColor", "ListCategorySelectedDividerColor", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 5);
			this.AddElementFieldMapping("UIThemeEntity", "ListCategorySelectedTextColor", "ListCategorySelectedTextColor", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 6);
			this.AddElementFieldMapping("UIThemeEntity", "ListItemBackgroundColor", "ListItemBackgroundColor", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 7);
			this.AddElementFieldMapping("UIThemeEntity", "ListItemDividerColor", "ListItemDividerColor", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 8);
			this.AddElementFieldMapping("UIThemeEntity", "ListItemTextColor", "ListItemTextColor", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 9);
			this.AddElementFieldMapping("UIThemeEntity", "ListItemSelectedBackgroundColor", "ListItemSelectedBackgroundColor", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 10);
			this.AddElementFieldMapping("UIThemeEntity", "ListItemSelectedDividerColor", "ListItemSelectedDividerColor", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 11);
			this.AddElementFieldMapping("UIThemeEntity", "ListItemSelectedTextColor", "ListItemSelectedTextColor", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 12);
			this.AddElementFieldMapping("UIThemeEntity", "WidgetBackgroundColor", "WidgetBackgroundColor", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 13);
			this.AddElementFieldMapping("UIThemeEntity", "WidgetBorderColor", "WidgetBorderColor", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 14);
			this.AddElementFieldMapping("UIThemeEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 15);
			this.AddElementFieldMapping("UIThemeEntity", "CreatedBy", "CreatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 16);
			this.AddElementFieldMapping("UIThemeEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 17);
			this.AddElementFieldMapping("UIThemeEntity", "UpdatedBy", "UpdatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 18);
			this.AddElementFieldMapping("UIThemeEntity", "Name", "Name", false, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 19);
			this.AddElementFieldMapping("UIThemeEntity", "WidgetTextColor", "WidgetTextColor", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 20);
			this.AddElementFieldMapping("UIThemeEntity", "HeaderColor", "HeaderColor", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 21);
			this.AddElementFieldMapping("UIThemeEntity", "TabBackgroundBottomColor", "TabBackgroundBottomColor", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 22);
			this.AddElementFieldMapping("UIThemeEntity", "TabBackgroundTopColor", "TabBackgroundTopColor", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 23);
			this.AddElementFieldMapping("UIThemeEntity", "TabDividerColor", "TabDividerColor", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 24);
			this.AddElementFieldMapping("UIThemeEntity", "TabBorderColor", "TabBorderColor", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 25);
			this.AddElementFieldMapping("UIThemeEntity", "TabActiveTextColor", "TabActiveTextColor", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 26);
			this.AddElementFieldMapping("UIThemeEntity", "TabInactiveTextColor", "TabInactiveTextColor", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 27);
			this.AddElementFieldMapping("UIThemeEntity", "PageBackgroundColor", "PageBackgroundColor", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 28);
			this.AddElementFieldMapping("UIThemeEntity", "PageBorderColor", "PageBorderColor", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 29);
			this.AddElementFieldMapping("UIThemeEntity", "PageTitleTextColor", "PageTitleTextColor", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 30);
			this.AddElementFieldMapping("UIThemeEntity", "PageDescriptionTextColor", "PageDescriptionTextColor", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 31);
			this.AddElementFieldMapping("UIThemeEntity", "PageDividerTopColor", "PageDividerTopColor", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 32);
			this.AddElementFieldMapping("UIThemeEntity", "PageDividerBottomColor", "PageDividerBottomColor", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 33);
			this.AddElementFieldMapping("UIThemeEntity", "FooterBackgroundBottomColor", "FooterBackgroundBottomColor", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 34);
			this.AddElementFieldMapping("UIThemeEntity", "FooterBackgroundTopColor", "FooterBackgroundTopColor", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 35);
			this.AddElementFieldMapping("UIThemeEntity", "FooterTextColor", "FooterTextColor", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 36);
			this.AddElementFieldMapping("UIThemeEntity", "PageErrorTextColor", "PageErrorTextColor", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 37);
			this.AddElementFieldMapping("UIThemeEntity", "FooterDividerColor", "FooterDividerColor", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 38);
			this.AddElementFieldMapping("UIThemeEntity", "PagePriceTextColor", "PagePriceTextColor", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 39);
			this.AddElementFieldMapping("UIThemeEntity", "PageFooterColor", "PageFooterColor", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 40);
			this.AddElementFieldMapping("UIThemeEntity", "ButtonBackgroundTopColor", "ButtonBackgroundTopColor", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 41);
			this.AddElementFieldMapping("UIThemeEntity", "ButtonBackgroundBottomColor", "ButtonBackgroundBottomColor", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 42);
			this.AddElementFieldMapping("UIThemeEntity", "ButtonBorderColor", "ButtonBorderColor", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 43);
			this.AddElementFieldMapping("UIThemeEntity", "ButtonTextColor", "ButtonTextColor", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 44);
			this.AddElementFieldMapping("UIThemeEntity", "ButtonPositiveBackgroundTopColor", "ButtonPositiveBackgroundTopColor", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 45);
			this.AddElementFieldMapping("UIThemeEntity", "ButtonPositiveBackgroundBottomColor", "ButtonPositiveBackgroundBottomColor", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 46);
			this.AddElementFieldMapping("UIThemeEntity", "ButtonPositiveBorderColor", "ButtonPositiveBorderColor", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 47);
			this.AddElementFieldMapping("UIThemeEntity", "ButtonPositiveTextColor", "ButtonPositiveTextColor", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 48);
			this.AddElementFieldMapping("UIThemeEntity", "ButtonDisabledBackgroundTopColor", "ButtonDisabledBackgroundTopColor", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 49);
			this.AddElementFieldMapping("UIThemeEntity", "ButtonDisabledBackgroundBottomColor", "ButtonDisabledBackgroundBottomColor", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 50);
			this.AddElementFieldMapping("UIThemeEntity", "ButtonDisabledBorderColor", "ButtonDisabledBorderColor", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 51);
			this.AddElementFieldMapping("UIThemeEntity", "ButtonDisabledTextColor", "ButtonDisabledTextColor", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 52);
			this.AddElementFieldMapping("UIThemeEntity", "DialogBackgroundColor", "DialogBackgroundColor", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 53);
			this.AddElementFieldMapping("UIThemeEntity", "DialogBorderColor", "DialogBorderColor", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 54);
			this.AddElementFieldMapping("UIThemeEntity", "DialogPanelBackgroundColor", "DialogPanelBackgroundColor", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 55);
			this.AddElementFieldMapping("UIThemeEntity", "DialogPanelBorderColor", "DialogPanelBorderColor", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 56);
			this.AddElementFieldMapping("UIThemeEntity", "DialogPrimaryTextColor", "DialogPrimaryTextColor", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 57);
			this.AddElementFieldMapping("UIThemeEntity", "DialogSecondaryTextColor", "DialogSecondaryTextColor", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 58);
			this.AddElementFieldMapping("UIThemeEntity", "TextboxBackgroundColor", "TextboxBackgroundColor", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 59);
			this.AddElementFieldMapping("UIThemeEntity", "TextboxBorderColor", "TextboxBorderColor", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 60);
			this.AddElementFieldMapping("UIThemeEntity", "TextboxTextColor", "TextboxTextColor", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 61);
			this.AddElementFieldMapping("UIThemeEntity", "TextboxCursorColor", "TextboxCursorColor", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 62);
			this.AddElementFieldMapping("UIThemeEntity", "PageInstructionsTextColor", "PageInstructionsTextColor", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 63);
			this.AddElementFieldMapping("UIThemeEntity", "DialogTitleTextColor", "DialogTitleTextColor", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 64);
			this.AddElementFieldMapping("UIThemeEntity", "SpinnerBackgroundColor", "SpinnerBackgroundColor", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 65);
			this.AddElementFieldMapping("UIThemeEntity", "SpinnerBorderColor", "SpinnerBorderColor", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 66);
			this.AddElementFieldMapping("UIThemeEntity", "SpinnerTextColor", "SpinnerTextColor", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 67);
			this.AddElementFieldMapping("UIThemeEntity", "CompanyId", "CompanyId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 68);
			this.AddElementFieldMapping("UIThemeEntity", "RoomControlButtonOuterBorderColor", "RoomControlButtonOuterBorderColor", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 69);
			this.AddElementFieldMapping("UIThemeEntity", "RoomControlButtonInnerBorderTopColor", "RoomControlButtonInnerBorderTopColor", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 70);
			this.AddElementFieldMapping("UIThemeEntity", "RoomControlButtonInnerBorderBottomColor", "RoomControlButtonInnerBorderBottomColor", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 71);
			this.AddElementFieldMapping("UIThemeEntity", "RoomControlButtonBackgroundTopColor", "RoomControlButtonBackgroundTopColor", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 72);
			this.AddElementFieldMapping("UIThemeEntity", "RoomControlButtonBackgroundBottomColor", "RoomControlButtonBackgroundBottomColor", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 73);
			this.AddElementFieldMapping("UIThemeEntity", "RoomControlDividerTopColor", "RoomControlDividerTopColor", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 74);
			this.AddElementFieldMapping("UIThemeEntity", "RoomControlDividerBottomColor", "RoomControlDividerBottomColor", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 75);
			this.AddElementFieldMapping("UIThemeEntity", "RoomControlButtonTextColor", "RoomControlButtonTextColor", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 76);
			this.AddElementFieldMapping("UIThemeEntity", "RoomControlTitleColor", "RoomControlTitleColor", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 77);
			this.AddElementFieldMapping("UIThemeEntity", "RoomControlTextColor", "RoomControlTextColor", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 78);
			this.AddElementFieldMapping("UIThemeEntity", "RoomControlPageBackgroundColor", "RoomControlPageBackgroundColor", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 79);
			this.AddElementFieldMapping("UIThemeEntity", "RoomControlSpinnerBorderColor", "RoomControlSpinnerBorderColor", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 80);
			this.AddElementFieldMapping("UIThemeEntity", "RoomControlSpinnerBackgroundColor", "RoomControlSpinnerBackgroundColor", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 81);
			this.AddElementFieldMapping("UIThemeEntity", "RoomControlSpinnerTextColor", "RoomControlSpinnerTextColor", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 82);
			this.AddElementFieldMapping("UIThemeEntity", "RoomControlButtonIndicatorColor", "RoomControlButtonIndicatorColor", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 83);
			this.AddElementFieldMapping("UIThemeEntity", "RoomControlButtonIndicatorBackgroundColor", "RoomControlButtonIndicatorBackgroundColor", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 84);
			this.AddElementFieldMapping("UIThemeEntity", "RoomControlHeaderButtonBackgroundColor", "RoomControlHeaderButtonBackgroundColor", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 85);
			this.AddElementFieldMapping("UIThemeEntity", "RoomControlHeaderButtonSelectorColor", "RoomControlHeaderButtonSelectorColor", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 86);
			this.AddElementFieldMapping("UIThemeEntity", "RoomControlHeaderButtonTextColor", "RoomControlHeaderButtonTextColor", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 87);
			this.AddElementFieldMapping("UIThemeEntity", "RoomControlStationNumberColor", "RoomControlStationNumberColor", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 88);
			this.AddElementFieldMapping("UIThemeEntity", "RoomControlSliderMinColor", "RoomControlSliderMinColor", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 89);
			this.AddElementFieldMapping("UIThemeEntity", "RoomControlSliderMaxColor", "RoomControlSliderMaxColor", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 90);
			this.AddElementFieldMapping("UIThemeEntity", "RoomControlToggleOnTextColor", "RoomControlToggleOnTextColor", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 91);
			this.AddElementFieldMapping("UIThemeEntity", "RoomControlToggleOffTextColor", "RoomControlToggleOffTextColor", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 92);
			this.AddElementFieldMapping("UIThemeEntity", "RoomControlToggleBackgroundColor", "RoomControlToggleBackgroundColor", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 93);
			this.AddElementFieldMapping("UIThemeEntity", "RoomControlToggleBorderColor", "RoomControlToggleBorderColor", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 94);
			this.AddElementFieldMapping("UIThemeEntity", "RoomControlThermostatComponentColor", "RoomControlThermostatComponentColor", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 95);
			this.AddElementFieldMapping("UIThemeEntity", "FooterButtonBackgroundTopColor", "FooterButtonBackgroundTopColor", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 96);
			this.AddElementFieldMapping("UIThemeEntity", "FooterButtonBackgroundBottomColor", "FooterButtonBackgroundBottomColor", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 97);
			this.AddElementFieldMapping("UIThemeEntity", "FooterButtonBorderColor", "FooterButtonBorderColor", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 98);
			this.AddElementFieldMapping("UIThemeEntity", "FooterButtonTextColor", "FooterButtonTextColor", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 99);
			this.AddElementFieldMapping("UIThemeEntity", "FooterButtonDisabledBackgroundTopColor", "FooterButtonDisabledBackgroundTopColor", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 100);
			this.AddElementFieldMapping("UIThemeEntity", "FooterButtonDisabledBackgroundBottomColor", "FooterButtonDisabledBackgroundBottomColor", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 101);
			this.AddElementFieldMapping("UIThemeEntity", "FooterButtonDisabledBorderColor", "FooterButtonDisabledBorderColor", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 102);
			this.AddElementFieldMapping("UIThemeEntity", "FooterButtonDisabledTextColor", "FooterButtonDisabledTextColor", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 103);
			this.AddElementFieldMapping("UIThemeEntity", "ListCategoryL2BackgroundColor", "ListCategoryL2BackgroundColor", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 104);
			this.AddElementFieldMapping("UIThemeEntity", "ListCategoryL3BackgroundColor", "ListCategoryL3BackgroundColor", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 105);
			this.AddElementFieldMapping("UIThemeEntity", "ListCategoryL4BackgroundColor", "ListCategoryL4BackgroundColor", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 106);
			this.AddElementFieldMapping("UIThemeEntity", "ListCategoryL5BackgroundColor", "ListCategoryL5BackgroundColor", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 107);
			this.AddElementFieldMapping("UIThemeEntity", "ListItemPriceTextColor", "ListItemPriceTextColor", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 108);
			this.AddElementFieldMapping("UIThemeEntity", "ListItemSelectedPriceTextColor", "ListItemSelectedPriceTextColor", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 109);
			this.AddElementFieldMapping("UIThemeEntity", "AlterationDialogBackgroundColor", "AlterationDialogBackgroundColor", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 110);
			this.AddElementFieldMapping("UIThemeEntity", "AlterationDialogBorderColor", "AlterationDialogBorderColor", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 111);
			this.AddElementFieldMapping("UIThemeEntity", "AlterationDialogPanelBackgroundColor", "AlterationDialogPanelBackgroundColor", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 112);
			this.AddElementFieldMapping("UIThemeEntity", "AlterationDialogListItemBackgroundColor", "AlterationDialogListItemBackgroundColor", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 113);
			this.AddElementFieldMapping("UIThemeEntity", "AlterationDialogListItemSelectedBackgroundColor", "AlterationDialogListItemSelectedBackgroundColor", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 114);
			this.AddElementFieldMapping("UIThemeEntity", "AlterationDialogListItemSelectedBorderColor", "AlterationDialogListItemSelectedBorderColor", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 115);
			this.AddElementFieldMapping("UIThemeEntity", "AlterationDialogPrimaryTextColor", "AlterationDialogPrimaryTextColor", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 116);
			this.AddElementFieldMapping("UIThemeEntity", "AlterationDialogSecondaryTextColor", "AlterationDialogSecondaryTextColor", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 117);
			this.AddElementFieldMapping("UIThemeEntity", "AlterationDialogInputBackgroundColor", "AlterationDialogInputBackgroundColor", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 118);
			this.AddElementFieldMapping("UIThemeEntity", "AlterationDialogInputBorderColor", "AlterationDialogInputBorderColor", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 119);
			this.AddElementFieldMapping("UIThemeEntity", "AlterationDialogInputTextColor", "AlterationDialogInputTextColor", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 120);
			this.AddElementFieldMapping("UIThemeEntity", "AlterationDialogInputHintTextColor", "AlterationDialogInputHintTextColor", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 121);
			this.AddElementFieldMapping("UIThemeEntity", "AlterationDialogPanelBorderColor", "AlterationDialogPanelBorderColor", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 122);
			this.AddElementFieldMapping("UIThemeEntity", "AlterationDialogRadioButtonColor", "AlterationDialogRadioButtonColor", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 123);
			this.AddElementFieldMapping("UIThemeEntity", "AlterationDialogCheckBoxColor", "AlterationDialogCheckBoxColor", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 124);
			this.AddElementFieldMapping("UIThemeEntity", "AlterationDialogCloseButtonColor", "AlterationDialogCloseButtonColor", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 125);
			this.AddElementFieldMapping("UIThemeEntity", "AlterationDialogListViewArrowColor", "AlterationDialogListViewArrowColor", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 126);
			this.AddElementFieldMapping("UIThemeEntity", "AlterationDialogListViewDividerColor", "AlterationDialogListViewDividerColor", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 127);
			this.AddElementFieldMapping("UIThemeEntity", "AreaTabActiveText", "AreaTabActiveText", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 128);
			this.AddElementFieldMapping("UIThemeEntity", "AreaTabInactiveText", "AreaTabInactiveText", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 129);
		}

		/// <summary>Inits UIThemeColorEntity's mappings</summary>
		private void InitUIThemeColorEntityMappings()
		{
			this.AddElementMapping("UIThemeColorEntity", @"Obymobi", @"dbo", "UIThemeColor", 9, 0);
			this.AddElementFieldMapping("UIThemeColorEntity", "UIThemeColorId", "UIThemeColorId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("UIThemeColorEntity", "ParentCompanyId", "ParentCompanyId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("UIThemeColorEntity", "UIThemeId", "UIThemeId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("UIThemeColorEntity", "Type", "Type", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("UIThemeColorEntity", "Color", "Color", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("UIThemeColorEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 5);
			this.AddElementFieldMapping("UIThemeColorEntity", "CreatedBy", "CreatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 6);
			this.AddElementFieldMapping("UIThemeColorEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 7);
			this.AddElementFieldMapping("UIThemeColorEntity", "UpdatedBy", "UpdatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 8);
		}

		/// <summary>Inits UIThemeTextSizeEntity's mappings</summary>
		private void InitUIThemeTextSizeEntityMappings()
		{
			this.AddElementMapping("UIThemeTextSizeEntity", @"Obymobi", @"dbo", "UIThemeTextSize", 9, 0);
			this.AddElementFieldMapping("UIThemeTextSizeEntity", "UIThemeTextSizeId", "UIThemeTextSizeId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("UIThemeTextSizeEntity", "ParentCompanyId", "ParentCompanyId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("UIThemeTextSizeEntity", "UIThemeId", "UIThemeId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("UIThemeTextSizeEntity", "Type", "Type", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("UIThemeTextSizeEntity", "TextSize", "TextSize", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("UIThemeTextSizeEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 5);
			this.AddElementFieldMapping("UIThemeTextSizeEntity", "CreatedBy", "CreatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 6);
			this.AddElementFieldMapping("UIThemeTextSizeEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 7);
			this.AddElementFieldMapping("UIThemeTextSizeEntity", "UpdatedBy", "UpdatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 8);
		}

		/// <summary>Inits UIWidgetEntity's mappings</summary>
		private void InitUIWidgetEntityMappings()
		{
			this.AddElementMapping("UIWidgetEntity", @"Obymobi", @"dbo", "UIWidget", 30, 0);
			this.AddElementFieldMapping("UIWidgetEntity", "UIWidgetId", "UIWidgetId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("UIWidgetEntity", "ParentCompanyId", "ParentCompanyId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("UIWidgetEntity", "UITabId", "UITabId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("UIWidgetEntity", "Type", "Type", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("UIWidgetEntity", "Caption", "Caption", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 4);
			this.AddElementFieldMapping("UIWidgetEntity", "SortOrder", "SortOrder", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 5);
			this.AddElementFieldMapping("UIWidgetEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 6);
			this.AddElementFieldMapping("UIWidgetEntity", "CreatedBy", "CreatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 7);
			this.AddElementFieldMapping("UIWidgetEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 8);
			this.AddElementFieldMapping("UIWidgetEntity", "UpdatedBy", "UpdatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 9);
			this.AddElementFieldMapping("UIWidgetEntity", "AdvertisementId", "AdvertisementId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 10);
			this.AddElementFieldMapping("UIWidgetEntity", "ProductCategoryId", "ProductCategoryId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 11);
			this.AddElementFieldMapping("UIWidgetEntity", "UITabType", "UITabType", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 12);
			this.AddElementFieldMapping("UIWidgetEntity", "Url", "Url", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 13);
			this.AddElementFieldMapping("UIWidgetEntity", "CategoryId", "CategoryId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 14);
			this.AddElementFieldMapping("UIWidgetEntity", "EntertainmentId", "EntertainmentId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 15);
			this.AddElementFieldMapping("UIWidgetEntity", "SiteId", "SiteId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 16);
			this.AddElementFieldMapping("UIWidgetEntity", "EntertainmentcategoryId", "EntertainmentcategoryId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 17);
			this.AddElementFieldMapping("UIWidgetEntity", "PageId", "PageId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 18);
			this.AddElementFieldMapping("UIWidgetEntity", "FieldValue1", "FieldValue1", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 19);
			this.AddElementFieldMapping("UIWidgetEntity", "FieldValue2", "FieldValue2", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 20);
			this.AddElementFieldMapping("UIWidgetEntity", "FieldValue3", "FieldValue3", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 21);
			this.AddElementFieldMapping("UIWidgetEntity", "FieldValue4", "FieldValue4", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 22);
			this.AddElementFieldMapping("UIWidgetEntity", "FieldValue5", "FieldValue5", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 23);
			this.AddElementFieldMapping("UIWidgetEntity", "IsVisible", "IsVisible", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 24);
			this.AddElementFieldMapping("UIWidgetEntity", "RoomControlAreaId", "RoomControlAreaId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 25);
			this.AddElementFieldMapping("UIWidgetEntity", "RoomControlSectionId", "RoomControlSectionId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 26);
			this.AddElementFieldMapping("UIWidgetEntity", "RoomControlType", "RoomControlType", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 27);
			this.AddElementFieldMapping("UIWidgetEntity", "Name", "Name", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 28);
			this.AddElementFieldMapping("UIWidgetEntity", "MessageLayoutType", "MessageLayoutType", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 29);
		}

		/// <summary>Inits UIWidgetAvailabilityEntity's mappings</summary>
		private void InitUIWidgetAvailabilityEntityMappings()
		{
			this.AddElementMapping("UIWidgetAvailabilityEntity", @"Obymobi", @"dbo", "UIWidgetAvailability", 9, 0);
			this.AddElementFieldMapping("UIWidgetAvailabilityEntity", "UIWidgetAvailabilityId", "UIWidgetAvailabilityId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("UIWidgetAvailabilityEntity", "ParentCompanyId", "ParentCompanyId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("UIWidgetAvailabilityEntity", "UIWidgetId", "UIWidgetId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("UIWidgetAvailabilityEntity", "AvailabilityId", "AvailabilityId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("UIWidgetAvailabilityEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 4);
			this.AddElementFieldMapping("UIWidgetAvailabilityEntity", "CreatedBy", "CreatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 5);
			this.AddElementFieldMapping("UIWidgetAvailabilityEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 6);
			this.AddElementFieldMapping("UIWidgetAvailabilityEntity", "UpdatedBy", "UpdatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 7);
			this.AddElementFieldMapping("UIWidgetAvailabilityEntity", "SortOrder", "SortOrder", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 8);
		}

		/// <summary>Inits UIWidgetLanguageEntity's mappings</summary>
		private void InitUIWidgetLanguageEntityMappings()
		{
			this.AddElementMapping("UIWidgetLanguageEntity", @"Obymobi", @"dbo", "UIWidgetLanguage", 14, 0);
			this.AddElementFieldMapping("UIWidgetLanguageEntity", "UIWidgetLanguageId", "UIWidgetLanguageId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("UIWidgetLanguageEntity", "ParentCompanyId", "ParentCompanyId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("UIWidgetLanguageEntity", "UIWidgetId", "UIWidgetId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("UIWidgetLanguageEntity", "Caption", "Caption", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 3);
			this.AddElementFieldMapping("UIWidgetLanguageEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 4);
			this.AddElementFieldMapping("UIWidgetLanguageEntity", "CreatedBy", "CreatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 5);
			this.AddElementFieldMapping("UIWidgetLanguageEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 6);
			this.AddElementFieldMapping("UIWidgetLanguageEntity", "UpdatedBy", "UpdatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 7);
			this.AddElementFieldMapping("UIWidgetLanguageEntity", "LanguageId", "LanguageId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 8);
			this.AddElementFieldMapping("UIWidgetLanguageEntity", "FieldValue1", "FieldValue1", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 9);
			this.AddElementFieldMapping("UIWidgetLanguageEntity", "FieldValue2", "FieldValue2", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 10);
			this.AddElementFieldMapping("UIWidgetLanguageEntity", "FieldValue3", "FieldValue3", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 11);
			this.AddElementFieldMapping("UIWidgetLanguageEntity", "FieldValue4", "FieldValue4", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 12);
			this.AddElementFieldMapping("UIWidgetLanguageEntity", "FieldValue5", "FieldValue5", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 13);
		}

		/// <summary>Inits UIWidgetTimerEntity's mappings</summary>
		private void InitUIWidgetTimerEntityMappings()
		{
			this.AddElementMapping("UIWidgetTimerEntity", @"Obymobi", @"dbo", "UIWidgetTimer", 11, 0);
			this.AddElementFieldMapping("UIWidgetTimerEntity", "UIWidgetTimerId", "UIWidgetTimerId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("UIWidgetTimerEntity", "ParentCompanyId", "ParentCompanyId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("UIWidgetTimerEntity", "UIWidgetId", "UIWidgetId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("UIWidgetTimerEntity", "CountToDate", "CountToDate", true, "DateTime", 0, 0, 0, false, "", null, typeof(System.DateTime), 3);
			this.AddElementFieldMapping("UIWidgetTimerEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 4);
			this.AddElementFieldMapping("UIWidgetTimerEntity", "CreatedBy", "CreatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 5);
			this.AddElementFieldMapping("UIWidgetTimerEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 6);
			this.AddElementFieldMapping("UIWidgetTimerEntity", "UpdatedBy", "UpdatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 7);
			this.AddElementFieldMapping("UIWidgetTimerEntity", "CountToTime", "CountToTime", false, "DateTime", 0, 0, 0, false, "", null, typeof(System.DateTime), 8);
			this.AddElementFieldMapping("UIWidgetTimerEntity", "CountToDateUTC", "CountToDateUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 9);
			this.AddElementFieldMapping("UIWidgetTimerEntity", "CountToTimeUTC", "CountToTimeUTC", false, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 10);
		}

		/// <summary>Inits UserEntity's mappings</summary>
		private void InitUserEntityMappings()
		{
			this.AddElementMapping("UserEntity", @"Obymobi", @"dbo", "User", 37, 0);
			this.AddElementFieldMapping("UserEntity", "UserId", "UserId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("UserEntity", "Username", "Username", false, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 1);
			this.AddElementFieldMapping("UserEntity", "Firstname", "Firstname", true, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 2);
			this.AddElementFieldMapping("UserEntity", "Lastname", "Lastname", true, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 3);
			this.AddElementFieldMapping("UserEntity", "LastnamePrefix", "LastnamePrefix", true, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 4);
			this.AddElementFieldMapping("UserEntity", "Email", "Email", true, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 5);
			this.AddElementFieldMapping("UserEntity", "Password", "Password", true, "NVarChar", 128, 0, 0, false, "", null, typeof(System.String), 6);
			this.AddElementFieldMapping("UserEntity", "IsApproved", "IsApproved", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 7);
			this.AddElementFieldMapping("UserEntity", "IsOnline", "IsOnline", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 8);
			this.AddElementFieldMapping("UserEntity", "IsLockedOut", "IsLockedOut", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 9);
			this.AddElementFieldMapping("UserEntity", "FailedPasswordAttemptCount", "FailedPasswordAttemptCount", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 10);
			this.AddElementFieldMapping("UserEntity", "PasswordResetLinkIdentifier", "PasswordResetLinkIdentifier", true, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 11);
			this.AddElementFieldMapping("UserEntity", "LogingLevel", "LogingLevel", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 12);
			this.AddElementFieldMapping("UserEntity", "RandomValue", "RandomValue", true, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 13);
			this.AddElementFieldMapping("UserEntity", "CreatedBy", "CreatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 14);
			this.AddElementFieldMapping("UserEntity", "UpdatedBy", "UpdatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 15);
			this.AddElementFieldMapping("UserEntity", "Deleted", "Deleted", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 16);
			this.AddElementFieldMapping("UserEntity", "Archived", "Archived", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 17);
			this.AddElementFieldMapping("UserEntity", "PageSize", "PageSize", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 18);
			this.AddElementFieldMapping("UserEntity", "AccountId", "AccountId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 19);
			this.AddElementFieldMapping("UserEntity", "Role", "Role", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 20);
			this.AddElementFieldMapping("UserEntity", "TimeZoneId", "TimeZoneId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 21);
			this.AddElementFieldMapping("UserEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 22);
			this.AddElementFieldMapping("UserEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 23);
			this.AddElementFieldMapping("UserEntity", "LastActivityDateUTC", "LastActivityDateUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 24);
			this.AddElementFieldMapping("UserEntity", "LastLoginDateUTC", "LastLoginDateUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 25);
			this.AddElementFieldMapping("UserEntity", "LastPasswordChangedDateUTC", "LastPasswordChangedDateUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 26);
			this.AddElementFieldMapping("UserEntity", "LastLockedOutDateUTC", "LastLockedOutDateUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 27);
			this.AddElementFieldMapping("UserEntity", "FailedPasswordAttemptWindowStartUTC", "FailedPasswordAttemptWindowStartUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 28);
			this.AddElementFieldMapping("UserEntity", "FailedPasswordAttemptLastDateUTC", "FailedPasswordAttemptLastDateUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 29);
			this.AddElementFieldMapping("UserEntity", "PasswordResetLinkGeneratedUTC", "PasswordResetLinkGeneratedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 30);
			this.AddElementFieldMapping("UserEntity", "TimeZoneOlsonId", "TimeZoneOlsonId", true, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 31);
			this.AddElementFieldMapping("UserEntity", "CultureCode", "CultureCode", true, "NVarChar", 10, 0, 0, false, "", null, typeof(System.String), 32);
			this.AddElementFieldMapping("UserEntity", "AllowPublishing", "AllowPublishing", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 33);
			this.AddElementFieldMapping("UserEntity", "IsTester", "IsTester", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 34);
			this.AddElementFieldMapping("UserEntity", "IsDeveloper", "IsDeveloper", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 35);
			this.AddElementFieldMapping("UserEntity", "HasCboAccount", "hasCboAccount", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 36);
		}

		/// <summary>Inits UserBrandEntity's mappings</summary>
		private void InitUserBrandEntityMappings()
		{
			this.AddElementMapping("UserBrandEntity", @"Obymobi", @"dbo", "UserBrand", 8, 0);
			this.AddElementFieldMapping("UserBrandEntity", "UserBrandId", "UserBrandId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("UserBrandEntity", "UserId", "UserId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("UserBrandEntity", "BrandId", "BrandId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("UserBrandEntity", "Role", "Role", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("UserBrandEntity", "CreatedUTC", "CreatedUTC", false, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 4);
			this.AddElementFieldMapping("UserBrandEntity", "CreatedBy", "CreatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 5);
			this.AddElementFieldMapping("UserBrandEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 6);
			this.AddElementFieldMapping("UserBrandEntity", "UpdatedBy", "UpdatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 7);
		}

		/// <summary>Inits UserLogonEntity's mappings</summary>
		private void InitUserLogonEntityMappings()
		{
			this.AddElementMapping("UserLogonEntity", @"Obymobi", @"dbo", "UserLogon", 11, 0);
			this.AddElementFieldMapping("UserLogonEntity", "UserLogonId", "UserLogonId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("UserLogonEntity", "UserId", "UserId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("UserLogonEntity", "Successful", "Successful", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 2);
			this.AddElementFieldMapping("UserLogonEntity", "IpAddress", "IpAddress", true, "NVarChar", 100, 0, 0, false, "", null, typeof(System.String), 3);
			this.AddElementFieldMapping("UserLogonEntity", "AuthenticateResult", "AuthenticateResult", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("UserLogonEntity", "CreatedBy", "CreatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 5);
			this.AddElementFieldMapping("UserLogonEntity", "UpdatedBy", "UpdatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 6);
			this.AddElementFieldMapping("UserLogonEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 7);
			this.AddElementFieldMapping("UserLogonEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 8);
			this.AddElementFieldMapping("UserLogonEntity", "DateTimeUTC", "DateTimeUTC", false, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 9);
			this.AddElementFieldMapping("UserLogonEntity", "Username", "Username", false, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 10);
		}

		/// <summary>Inits UserRoleEntity's mappings</summary>
		private void InitUserRoleEntityMappings()
		{
			this.AddElementMapping("UserRoleEntity", @"Obymobi", @"dbo", "UserRole", 7, 0);
			this.AddElementFieldMapping("UserRoleEntity", "UserRoleId", "UserRoleId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("UserRoleEntity", "UserId", "UserId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("UserRoleEntity", "RoleId", "RoleId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("UserRoleEntity", "CreatedBy", "CreatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("UserRoleEntity", "UpdatedBy", "UpdatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("UserRoleEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 5);
			this.AddElementFieldMapping("UserRoleEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 6);
		}

		/// <summary>Inits VattariffEntity's mappings</summary>
		private void InitVattariffEntityMappings()
		{
			this.AddElementMapping("VattariffEntity", @"Obymobi", @"dbo", "Vattariff", 8, 0);
			this.AddElementFieldMapping("VattariffEntity", "VattariffId", "VattariffId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("VattariffEntity", "CountryId", "CountryId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("VattariffEntity", "Name", "Name", true, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 2);
			this.AddElementFieldMapping("VattariffEntity", "Percentage", "Percentage", false, "Float", 0, 38, 0, false, "", null, typeof(System.Double), 3);
			this.AddElementFieldMapping("VattariffEntity", "CreatedBy", "CreatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("VattariffEntity", "UpdatedBy", "UpdatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 5);
			this.AddElementFieldMapping("VattariffEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 6);
			this.AddElementFieldMapping("VattariffEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 7);
		}

		/// <summary>Inits VendorEntity's mappings</summary>
		private void InitVendorEntityMappings()
		{
			this.AddElementMapping("VendorEntity", @"Obymobi", @"dbo", "Vendor", 7, 0);
			this.AddElementFieldMapping("VendorEntity", "VendorId", "VendorId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("VendorEntity", "Name", "Name", true, "NVarChar", 250, 0, 0, false, "", null, typeof(System.String), 1);
			this.AddElementFieldMapping("VendorEntity", "UserAgentName", "UserAgentName", true, "NVarChar", 250, 0, 0, false, "", null, typeof(System.String), 2);
			this.AddElementFieldMapping("VendorEntity", "CreatedBy", "CreatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("VendorEntity", "UpdatedBy", "UpdatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("VendorEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 5);
			this.AddElementFieldMapping("VendorEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 6);
		}

		/// <summary>Inits VenueCategoryEntity's mappings</summary>
		private void InitVenueCategoryEntityMappings()
		{
			this.AddElementMapping("VenueCategoryEntity", @"Obymobi", @"dbo", "VenueCategory", 9, 0);
			this.AddElementFieldMapping("VenueCategoryEntity", "VenueCategoryId", "VenueCategoryId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("VenueCategoryEntity", "Name", "Name", false, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 1);
			this.AddElementFieldMapping("VenueCategoryEntity", "CreatedBy", "CreatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("VenueCategoryEntity", "UpdatedBy", "UpdatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("VenueCategoryEntity", "MarkerIcon", "MarkerIcon", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("VenueCategoryEntity", "ParentVenueCategoryId", "ParentVenueCategoryId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 5);
			this.AddElementFieldMapping("VenueCategoryEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 6);
			this.AddElementFieldMapping("VenueCategoryEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 7);
			this.AddElementFieldMapping("VenueCategoryEntity", "LastModifiedUTC", "LastModifiedUTC", false, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 8);
		}

		/// <summary>Inits VenueCategoryLanguageEntity's mappings</summary>
		private void InitVenueCategoryLanguageEntityMappings()
		{
			this.AddElementMapping("VenueCategoryLanguageEntity", @"Obymobi", @"dbo", "VenueCategoryLanguage", 9, 0);
			this.AddElementFieldMapping("VenueCategoryLanguageEntity", "VenueCategoryLanguageId", "VenueCategoryLanguageId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("VenueCategoryLanguageEntity", "VenueCategoryId", "VenueCategoryId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("VenueCategoryLanguageEntity", "LanguageId", "LanguageId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("VenueCategoryLanguageEntity", "Name", "Name", true, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 3);
			this.AddElementFieldMapping("VenueCategoryLanguageEntity", "CreatedBy", "CreatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("VenueCategoryLanguageEntity", "UpdatedBy", "UpdatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 5);
			this.AddElementFieldMapping("VenueCategoryLanguageEntity", "NamePlural", "NamePlural", true, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 6);
			this.AddElementFieldMapping("VenueCategoryLanguageEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 7);
			this.AddElementFieldMapping("VenueCategoryLanguageEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 8);
		}

		/// <summary>Inits ViewEntity's mappings</summary>
		private void InitViewEntityMappings()
		{
			this.AddElementMapping("ViewEntity", @"Obymobi", @"dbo", "View", 9, 0);
			this.AddElementFieldMapping("ViewEntity", "ViewId", "ViewId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("ViewEntity", "Name", "Name", true, "NVarChar", 150, 0, 0, false, "", null, typeof(System.String), 1);
			this.AddElementFieldMapping("ViewEntity", "EntityName", "EntityName", false, "VarChar", 250, 0, 0, false, "", null, typeof(System.String), 2);
			this.AddElementFieldMapping("ViewEntity", "UiElementTypeNameFull", "UiElementTypeNameFull", true, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 3);
			this.AddElementFieldMapping("ViewEntity", "ForLocalUseWhileSyncingIsUpdated", "ForLocalUseWhileSyncingIsUpdated", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 4);
			this.AddElementFieldMapping("ViewEntity", "CreatedBy", "CreatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 5);
			this.AddElementFieldMapping("ViewEntity", "UpdatedBy", "UpdatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 6);
			this.AddElementFieldMapping("ViewEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 7);
			this.AddElementFieldMapping("ViewEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 8);
		}

		/// <summary>Inits ViewCustomEntity's mappings</summary>
		private void InitViewCustomEntityMappings()
		{
			this.AddElementMapping("ViewCustomEntity", @"Obymobi", @"dbo", "ViewCustom", 9, 0);
			this.AddElementFieldMapping("ViewCustomEntity", "ViewCustomId", "ViewCustomId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("ViewCustomEntity", "Name", "Name", true, "NVarChar", 150, 0, 0, false, "", null, typeof(System.String), 1);
			this.AddElementFieldMapping("ViewCustomEntity", "EntityName", "EntityName", true, "NVarChar", 150, 0, 0, false, "", null, typeof(System.String), 2);
			this.AddElementFieldMapping("ViewCustomEntity", "UiElementTypeNameFull", "UiElementTypeNameFull", true, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 3);
			this.AddElementFieldMapping("ViewCustomEntity", "UserId", "UserId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("ViewCustomEntity", "CreatedBy", "CreatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 5);
			this.AddElementFieldMapping("ViewCustomEntity", "UpdatedBy", "UpdatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 6);
			this.AddElementFieldMapping("ViewCustomEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 7);
			this.AddElementFieldMapping("ViewCustomEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 8);
		}

		/// <summary>Inits ViewItemEntity's mappings</summary>
		private void InitViewItemEntityMappings()
		{
			this.AddElementMapping("ViewItemEntity", @"Obymobi", @"dbo", "ViewItem", 18, 0);
			this.AddElementFieldMapping("ViewItemEntity", "ViewItemId", "ViewItemId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("ViewItemEntity", "ViewId", "ViewId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("ViewItemEntity", "EntityName", "EntityName", true, "NVarChar", 150, 0, 0, false, "", null, typeof(System.String), 2);
			this.AddElementFieldMapping("ViewItemEntity", "FieldName", "FieldName", true, "NVarChar", 150, 0, 0, false, "", null, typeof(System.String), 3);
			this.AddElementFieldMapping("ViewItemEntity", "ShowOnGridView", "ShowOnGridView", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 4);
			this.AddElementFieldMapping("ViewItemEntity", "DisplayPosition", "DisplayPosition", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 5);
			this.AddElementFieldMapping("ViewItemEntity", "SortPosition", "SortPosition", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 6);
			this.AddElementFieldMapping("ViewItemEntity", "SortOperator", "SortOperator", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 7);
			this.AddElementFieldMapping("ViewItemEntity", "DisplayFormatString", "DisplayFormatString", true, "NVarChar", 100, 0, 0, false, "", null, typeof(System.String), 8);
			this.AddElementFieldMapping("ViewItemEntity", "Width", "Width", true, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 9);
			this.AddElementFieldMapping("ViewItemEntity", "Type", "Type", true, "NVarChar", 100, 0, 0, false, "", null, typeof(System.String), 10);
			this.AddElementFieldMapping("ViewItemEntity", "UiElementTypeNameFull", "UiElementTypeNameFull", true, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 11);
			this.AddElementFieldMapping("ViewItemEntity", "ForLocalUseWhileSyncingIsUpdated", "ForLocalUseWhileSyncingIsUpdated", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 12);
			this.AddElementFieldMapping("ViewItemEntity", "CreatedBy", "CreatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 13);
			this.AddElementFieldMapping("ViewItemEntity", "Updated", "Updated", true, "DateTime", 0, 0, 0, false, "", null, typeof(System.DateTime), 14);
			this.AddElementFieldMapping("ViewItemEntity", "UpdatedBy", "UpdatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 15);
			this.AddElementFieldMapping("ViewItemEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 16);
			this.AddElementFieldMapping("ViewItemEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 17);
		}

		/// <summary>Inits ViewItemCustomEntity's mappings</summary>
		private void InitViewItemCustomEntityMappings()
		{
			this.AddElementMapping("ViewItemCustomEntity", @"Obymobi", @"dbo", "ViewItemCustom", 18, 0);
			this.AddElementFieldMapping("ViewItemCustomEntity", "ViewItemCustomId", "ViewItemCustomId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("ViewItemCustomEntity", "ViewCustomId", "ViewCustomId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("ViewItemCustomEntity", "EntityName", "EntityName", true, "NVarChar", 150, 0, 0, false, "", null, typeof(System.String), 2);
			this.AddElementFieldMapping("ViewItemCustomEntity", "FieldName", "FieldName", true, "NVarChar", 150, 0, 0, false, "", null, typeof(System.String), 3);
			this.AddElementFieldMapping("ViewItemCustomEntity", "FriendlyName", "FriendlyName", true, "NVarChar", 150, 0, 0, false, "", null, typeof(System.String), 4);
			this.AddElementFieldMapping("ViewItemCustomEntity", "ShowOnGridView", "ShowOnGridView", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 5);
			this.AddElementFieldMapping("ViewItemCustomEntity", "DisplayPosition", "DisplayPosition", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 6);
			this.AddElementFieldMapping("ViewItemCustomEntity", "SortPosition", "SortPosition", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 7);
			this.AddElementFieldMapping("ViewItemCustomEntity", "SortOperator", "SortOperator", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 8);
			this.AddElementFieldMapping("ViewItemCustomEntity", "DisplayFormatString", "DisplayFormatString", true, "NVarChar", 100, 0, 0, false, "", null, typeof(System.String), 9);
			this.AddElementFieldMapping("ViewItemCustomEntity", "Width", "Width", true, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 10);
			this.AddElementFieldMapping("ViewItemCustomEntity", "Type", "Type", true, "NVarChar", 100, 0, 0, false, "", null, typeof(System.String), 11);
			this.AddElementFieldMapping("ViewItemCustomEntity", "UiElementTypeNameFull", "UiElementTypeNameFull", true, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 12);
			this.AddElementFieldMapping("ViewItemCustomEntity", "Archived", "Archived", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 13);
			this.AddElementFieldMapping("ViewItemCustomEntity", "CreatedBy", "CreatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 14);
			this.AddElementFieldMapping("ViewItemCustomEntity", "UpdatedBy", "UpdatedBy", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 15);
			this.AddElementFieldMapping("ViewItemCustomEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 16);
			this.AddElementFieldMapping("ViewItemCustomEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 17);
		}

		/// <summary>Inits WeatherEntity's mappings</summary>
		private void InitWeatherEntityMappings()
		{
			this.AddElementMapping("WeatherEntity", @"Obymobi", @"dbo", "Weather", 8, 0);
			this.AddElementFieldMapping("WeatherEntity", "WeatherId", "WeatherId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("WeatherEntity", "City", "City", false, "NVarChar", 150, 0, 0, false, "", null, typeof(System.String), 1);
			this.AddElementFieldMapping("WeatherEntity", "Filename", "Filename", true, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 2);
			this.AddElementFieldMapping("WeatherEntity", "LastWeatherData", "LastWeatherData", false, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 3);
			this.AddElementFieldMapping("WeatherEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 4);
			this.AddElementFieldMapping("WeatherEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 5);
			this.AddElementFieldMapping("WeatherEntity", "CreatedBy", "CreatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 6);
			this.AddElementFieldMapping("WeatherEntity", "UpdatedBy", "UpdatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 7);
		}

		/// <summary>Inits WifiConfigurationEntity's mappings</summary>
		private void InitWifiConfigurationEntityMappings()
		{
			this.AddElementMapping("WifiConfigurationEntity", @"Obymobi", @"dbo", "WifiConfiguration", 14, 0);
			this.AddElementFieldMapping("WifiConfigurationEntity", "WifiConfigurationId", "WifiConfigurationId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("WifiConfigurationEntity", "Ssid", "Ssid", false, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 1);
			this.AddElementFieldMapping("WifiConfigurationEntity", "HiddenSsid", "HiddenSsid", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 2);
			this.AddElementFieldMapping("WifiConfigurationEntity", "Security", "Security", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("WifiConfigurationEntity", "SecurityKey", "SecurityKey", true, "NVarChar", 100, 0, 0, false, "", null, typeof(System.String), 4);
			this.AddElementFieldMapping("WifiConfigurationEntity", "EapMethod", "EapMethod", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 5);
			this.AddElementFieldMapping("WifiConfigurationEntity", "EapPhase2", "EapPhase2", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 6);
			this.AddElementFieldMapping("WifiConfigurationEntity", "EapIdent", "EapIdent", true, "NVarChar", 100, 0, 0, false, "", null, typeof(System.String), 7);
			this.AddElementFieldMapping("WifiConfigurationEntity", "EapAnonIdent", "EapAnonIdent", true, "NVarChar", 100, 0, 0, false, "", null, typeof(System.String), 8);
			this.AddElementFieldMapping("WifiConfigurationEntity", "CreatedUTC", "CreatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 9);
			this.AddElementFieldMapping("WifiConfigurationEntity", "CreatedBy", "CreatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 10);
			this.AddElementFieldMapping("WifiConfigurationEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 11);
			this.AddElementFieldMapping("WifiConfigurationEntity", "UpdatedBy", "UpdatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 12);
			this.AddElementFieldMapping("WifiConfigurationEntity", "CompanyId", "CompanyId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 13);
		}

		/// <summary>Inits ActionEntity's mappings</summary>
		private void InitActionEntityMappings()
		{
			this.AddElementMapping("ActionEntity", @"Obymobi", @"appless", "Action", 13, 0);
			this.AddElementFieldMapping("ActionEntity", "ActionId", "ActionId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("ActionEntity", "ActionType", "ActionType", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("ActionEntity", "LandingPageId", "LandingPageId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("ActionEntity", "CategoryId", "CategoryId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("ActionEntity", "ProductCategoryId", "ProductCategoryId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("ActionEntity", "Resource", "Resource", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 5);
			this.AddElementFieldMapping("ActionEntity", "ParentCompanyId", "ParentCompanyId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 6);
			this.AddElementFieldMapping("ActionEntity", "CreatedUTC", "CreatedUTC", false, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 7);
			this.AddElementFieldMapping("ActionEntity", "CreatedBy", "CreatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 8);
			this.AddElementFieldMapping("ActionEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 9);
			this.AddElementFieldMapping("ActionEntity", "UpdatedBy", "UpdatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 10);
			this.AddElementFieldMapping("ActionEntity", "ApplicationConfigurationId", "ApplicationConfigurationId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 11);
			this.AddElementFieldMapping("ActionEntity", "ProductId", "ProductId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 12);
		}

		/// <summary>Inits ApplicationConfigurationEntity's mappings</summary>
		private void InitApplicationConfigurationEntityMappings()
		{
			this.AddElementMapping("ApplicationConfigurationEntity", @"Obymobi", @"appless", "ApplicationConfiguration", 18, 0);
			this.AddElementFieldMapping("ApplicationConfigurationEntity", "ApplicationConfigurationId", "ApplicationConfigurationId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("ApplicationConfigurationEntity", "ExternalId", "ExternalId", false, "VarChar", 6, 0, 0, false, "", null, typeof(System.String), 1);
			this.AddElementFieldMapping("ApplicationConfigurationEntity", "CompanyId", "CompanyId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("ApplicationConfigurationEntity", "Name", "Name", false, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 3);
			this.AddElementFieldMapping("ApplicationConfigurationEntity", "ShortName", "ShortName", true, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 4);
			this.AddElementFieldMapping("ApplicationConfigurationEntity", "CreatedBy", "CreatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 5);
			this.AddElementFieldMapping("ApplicationConfigurationEntity", "Updated", "Updated", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 6);
			this.AddElementFieldMapping("ApplicationConfigurationEntity", "UpdatedBy", "UpdatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 7);
			this.AddElementFieldMapping("ApplicationConfigurationEntity", "CreatedUTC", "CreatedUTC", false, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 8);
			this.AddElementFieldMapping("ApplicationConfigurationEntity", "LandingPageId", "LandingPageId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 9);
			this.AddElementFieldMapping("ApplicationConfigurationEntity", "Description", "Description", true, "NVarChar", 1024, 0, 0, false, "", null, typeof(System.String), 10);
			this.AddElementFieldMapping("ApplicationConfigurationEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 11);
			this.AddElementFieldMapping("ApplicationConfigurationEntity", "CultureCode", "CultureCode", true, "NVarChar", 10, 0, 0, false, "", null, typeof(System.String), 12);
			this.AddElementFieldMapping("ApplicationConfigurationEntity", "EnableCookiewall", "EnableCookieWall", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 13);
			this.AddElementFieldMapping("ApplicationConfigurationEntity", "AnalyticsEnvironment", "AnalyticsEnvironment", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 14);
			this.AddElementFieldMapping("ApplicationConfigurationEntity", "GoogleAnalyticsId", "GoogleAnalyticsId", true, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 15);
			this.AddElementFieldMapping("ApplicationConfigurationEntity", "GoogleTagManagerId", "GoogleTagManagerId", true, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 16);
			this.AddElementFieldMapping("ApplicationConfigurationEntity", "ProductSwipingEnabled", "ProductSwipingEnabled", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 17);
		}

		/// <summary>Inits CarouselItemEntity's mappings</summary>
		private void InitCarouselItemEntityMappings()
		{
			this.AddElementMapping("CarouselItemEntity", @"Obymobi", @"appless", "CarouselItem", 12, 0);
			this.AddElementFieldMapping("CarouselItemEntity", "CarouselItemId", "CarouselItemId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("CarouselItemEntity", "WidgetCarouselId", "WidgetCarouselId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("CarouselItemEntity", "WidgetActionButtonId", "WidgetActionButtonId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("CarouselItemEntity", "Message", "Message", true, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 3);
			this.AddElementFieldMapping("CarouselItemEntity", "SortOrder", "SortOrder", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("CarouselItemEntity", "ParentCompanyId", "ParentCompanyId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 5);
			this.AddElementFieldMapping("CarouselItemEntity", "CreatedUTC", "CreatedUTC", false, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 6);
			this.AddElementFieldMapping("CarouselItemEntity", "CreatedBy", "CreatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 7);
			this.AddElementFieldMapping("CarouselItemEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 8);
			this.AddElementFieldMapping("CarouselItemEntity", "UpdatedBy", "UpdatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 9);
			this.AddElementFieldMapping("CarouselItemEntity", "Name", "Name", false, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 10);
			this.AddElementFieldMapping("CarouselItemEntity", "OverlayType", "OverlayType", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 11);
		}

		/// <summary>Inits FeatureFlagEntity's mappings</summary>
		private void InitFeatureFlagEntityMappings()
		{
			this.AddElementMapping("FeatureFlagEntity", @"Obymobi", @"appless", "FeatureFlag", 8, 0);
			this.AddElementFieldMapping("FeatureFlagEntity", "FeatureFlagId", "FeatureFlagId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("FeatureFlagEntity", "ApplicationConfigurationId", "ApplicationConfigurationId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("FeatureFlagEntity", "ApplessFeatureFlags", "ApplessFeatureFlags", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("FeatureFlagEntity", "CreatedUTC", "CreatedUTC", false, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 3);
			this.AddElementFieldMapping("FeatureFlagEntity", "CreatedBy", "CreatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("FeatureFlagEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 5);
			this.AddElementFieldMapping("FeatureFlagEntity", "UpdatedBy", "UpdatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 6);
			this.AddElementFieldMapping("FeatureFlagEntity", "CompanyId", "CompanyId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 7);
		}

		/// <summary>Inits LandingPageEntity's mappings</summary>
		private void InitLandingPageEntityMappings()
		{
			this.AddElementMapping("LandingPageEntity", @"Obymobi", @"appless", "LandingPage", 14, 0);
			this.AddElementFieldMapping("LandingPageEntity", "LandingPageId", "LandingPageId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("LandingPageEntity", "ExternalId", "ExternalId", false, "VarChar", 6, 0, 0, false, "", null, typeof(System.String), 1);
			this.AddElementFieldMapping("LandingPageEntity", "ParentCompanyId", "ParentCompanyId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("LandingPageEntity", "ApplicationConfigurationId", "ApplicationConfigurationId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("LandingPageEntity", "ThemeId", "ThemeId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("LandingPageEntity", "Name", "Name", false, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 5);
			this.AddElementFieldMapping("LandingPageEntity", "WelcomeText", "WelcomeText", true, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 6);
			this.AddElementFieldMapping("LandingPageEntity", "CreatedUTC", "CreatedUTC", false, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 7);
			this.AddElementFieldMapping("LandingPageEntity", "CreatedBy", "CreatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 8);
			this.AddElementFieldMapping("LandingPageEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 9);
			this.AddElementFieldMapping("LandingPageEntity", "UpdatedBy", "UpdatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 10);
			this.AddElementFieldMapping("LandingPageEntity", "NavigationMenuId", "NavigationMenuId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 11);
			this.AddElementFieldMapping("LandingPageEntity", "OutletId", "OutletId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 12);
			this.AddElementFieldMapping("LandingPageEntity", "HomeNavigationEnabled", "HomeNavigationEnabled", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 13);
		}

		/// <summary>Inits LandingPageWidgetEntity's mappings</summary>
		private void InitLandingPageWidgetEntityMappings()
		{
			this.AddElementMapping("LandingPageWidgetEntity", @"Obymobi", @"appless", "LandingPageWidget", 8, 0);
			this.AddElementFieldMapping("LandingPageWidgetEntity", "LandingPageId", "LandingPageId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("LandingPageWidgetEntity", "WidgetId", "WidgetId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("LandingPageWidgetEntity", "SortOrder", "SortOrder", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("LandingPageWidgetEntity", "ParentCompanyId", "ParentCompanyId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("LandingPageWidgetEntity", "CreatedUTC", "CreatedUTC", false, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 4);
			this.AddElementFieldMapping("LandingPageWidgetEntity", "CreatedBy", "CreatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 5);
			this.AddElementFieldMapping("LandingPageWidgetEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 6);
			this.AddElementFieldMapping("LandingPageWidgetEntity", "UpdatedBy", "UpdatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 7);
		}

		/// <summary>Inits NavigationMenuEntity's mappings</summary>
		private void InitNavigationMenuEntityMappings()
		{
			this.AddElementMapping("NavigationMenuEntity", @"Obymobi", @"appless", "NavigationMenu", 8, 0);
			this.AddElementFieldMapping("NavigationMenuEntity", "NavigationMenuId", "NavigationMenuId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("NavigationMenuEntity", "CompanyId", "ParentCompanyId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("NavigationMenuEntity", "CreatedUTC", "CreatedUTC", false, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 2);
			this.AddElementFieldMapping("NavigationMenuEntity", "CreatedBy", "CreatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("NavigationMenuEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 4);
			this.AddElementFieldMapping("NavigationMenuEntity", "UpdatedBy", "UpdatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 5);
			this.AddElementFieldMapping("NavigationMenuEntity", "Name", "Name", false, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 6);
			this.AddElementFieldMapping("NavigationMenuEntity", "ApplicationConfigurationId", "ApplicationConfigurationId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 7);
		}

		/// <summary>Inits NavigationMenuItemEntity's mappings</summary>
		private void InitNavigationMenuItemEntityMappings()
		{
			this.AddElementMapping("NavigationMenuItemEntity", @"Obymobi", @"appless", "NavigationMenuItem", 10, 0);
			this.AddElementFieldMapping("NavigationMenuItemEntity", "NavigationMenuItemId", "NavigationMenuItemId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("NavigationMenuItemEntity", "NavigationMenuId", "NavigationMenuId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("NavigationMenuItemEntity", "ActionId", "ActionId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("NavigationMenuItemEntity", "Text", "Text", false, "NVarChar", 100, 0, 0, false, "", null, typeof(System.String), 3);
			this.AddElementFieldMapping("NavigationMenuItemEntity", "SortOrder", "SortOrder", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("NavigationMenuItemEntity", "ParentCompanyId", "ParentCompanyId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 5);
			this.AddElementFieldMapping("NavigationMenuItemEntity", "CreatedUTC", "CreatedUTC", false, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 6);
			this.AddElementFieldMapping("NavigationMenuItemEntity", "CreatedBy", "CreatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 7);
			this.AddElementFieldMapping("NavigationMenuItemEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 8);
			this.AddElementFieldMapping("NavigationMenuItemEntity", "UpdatedBy", "UpdatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 9);
		}

		/// <summary>Inits NavigationMenuWidgetEntity's mappings</summary>
		private void InitNavigationMenuWidgetEntityMappings()
		{
			this.AddElementMapping("NavigationMenuWidgetEntity", @"Obymobi", @"appless", "NavigationMenuWidget", 8, 0);
			this.AddElementFieldMapping("NavigationMenuWidgetEntity", "NavigationMenuId", "NavigationMenuId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("NavigationMenuWidgetEntity", "WidgetId", "WidgetId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("NavigationMenuWidgetEntity", "SortOrder", "SortOrder", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("NavigationMenuWidgetEntity", "ParentCompanyId", "ParentCompanyId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("NavigationMenuWidgetEntity", "CreatedUTC", "CreatedUTC", false, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 4);
			this.AddElementFieldMapping("NavigationMenuWidgetEntity", "CreatedBy", "CreatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 5);
			this.AddElementFieldMapping("NavigationMenuWidgetEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 6);
			this.AddElementFieldMapping("NavigationMenuWidgetEntity", "UpdatedBy", "UpdatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 7);
		}

		/// <summary>Inits ThemeEntity's mappings</summary>
		private void InitThemeEntityMappings()
		{
			this.AddElementMapping("ThemeEntity", @"Obymobi", @"appless", "Theme", 15, 0);
			this.AddElementFieldMapping("ThemeEntity", "ThemeId", "ThemeId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("ThemeEntity", "ParentCompanyId", "ParentCompanyId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("ThemeEntity", "PrimaryColor", "PrimaryColor", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("ThemeEntity", "SignalColor", "SignalColor", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("ThemeEntity", "FontFamily", "FontFamily", false, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 4);
			this.AddElementFieldMapping("ThemeEntity", "CreatedUTC", "CreatedUTC", false, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 5);
			this.AddElementFieldMapping("ThemeEntity", "CreatedBy", "CreatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 6);
			this.AddElementFieldMapping("ThemeEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 7);
			this.AddElementFieldMapping("ThemeEntity", "UpdatedBy", "UpdatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 8);
			this.AddElementFieldMapping("ThemeEntity", "TextColor", "TextColor", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 9);
			this.AddElementFieldMapping("ThemeEntity", "DividerColor", "DividerColor", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 10);
			this.AddElementFieldMapping("ThemeEntity", "SubnavColor", "SubnavColor", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 11);
			this.AddElementFieldMapping("ThemeEntity", "BackgroundColor", "BackgroundColor", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 12);
			this.AddElementFieldMapping("ThemeEntity", "Name", "Name", false, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 13);
			this.AddElementFieldMapping("ThemeEntity", "ApplicationConfigurationId", "ApplicationConfigurationId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 14);
		}

		/// <summary>Inits WidgetEntity's mappings</summary>
		private void InitWidgetEntityMappings()
		{
			this.AddElementMapping("WidgetEntity", @"Obymobi", @"appless", "Widget", 10, 0);
			this.AddElementFieldMapping("WidgetEntity", "WidgetId", "WidgetId", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("WidgetEntity", "ApplicationConfigurationId", "ApplicationConfigurationId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("WidgetEntity", "Name", "Name", false, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 2);
			this.AddElementFieldMapping("WidgetEntity", "ActionId", "ActionId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("WidgetEntity", "CreatedUTC", "CreatedUTC", false, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 4);
			this.AddElementFieldMapping("WidgetEntity", "CreatedBy", "CreatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 5);
			this.AddElementFieldMapping("WidgetEntity", "UpdatedUTC", "UpdatedUTC", true, "DateTime2", 0, 0, 0, false, "", null, typeof(System.DateTime), 6);
			this.AddElementFieldMapping("WidgetEntity", "UpdatedBy", "UpdatedBy", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 7);
			this.AddElementFieldMapping("WidgetEntity", "ParentCompanyId", "ParentCompanyId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 8);
			this.AddElementFieldMapping("WidgetEntity", "Visible", "Visible", false, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 9);
		}

		/// <summary>Inits WidgetActionBannerEntity's mappings</summary>
		private void InitWidgetActionBannerEntityMappings()
		{
			this.AddElementMapping("WidgetActionBannerEntity", @"Obymobi", @"appless", "WidgetActionBanner", 3, 0);
			this.AddElementFieldMapping("WidgetActionBannerEntity", "WidgetId", "WidgetId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("WidgetActionBannerEntity", "Text", "Text", true, "NVarChar", 100, 0, 0, false, "", null, typeof(System.String), 1);
			this.AddElementFieldMapping("WidgetActionBannerEntity", "OverlayType", "OverlayType", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
		}

		/// <summary>Inits WidgetActionButtonEntity's mappings</summary>
		private void InitWidgetActionButtonEntityMappings()
		{
			this.AddElementMapping("WidgetActionButtonEntity", @"Obymobi", @"appless", "WidgetActionButton", 3, 0);
			this.AddElementFieldMapping("WidgetActionButtonEntity", "WidgetId", "WidgetId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("WidgetActionButtonEntity", "IconType", "IconType", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("WidgetActionButtonEntity", "Text", "Text", false, "NVarChar", 100, 0, 0, false, "", null, typeof(System.String), 2);
		}

		/// <summary>Inits WidgetCarouselEntity's mappings</summary>
		private void InitWidgetCarouselEntityMappings()
		{
			this.AddElementMapping("WidgetCarouselEntity", @"Obymobi", @"appless", "WidgetCarousel", 1, 0);
			this.AddElementFieldMapping("WidgetCarouselEntity", "WidgetId", "WidgetId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 0);
		}

		/// <summary>Inits WidgetGroupEntity's mappings</summary>
		private void InitWidgetGroupEntityMappings()
		{
			this.AddElementMapping("WidgetGroupEntity", @"Obymobi", @"appless", "WidgetGroup", 1, 0);
			this.AddElementFieldMapping("WidgetGroupEntity", "WidgetId", "WidgetId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 0);
		}

		/// <summary>Inits WidgetGroupWidgetEntity's mappings</summary>
		private void InitWidgetGroupWidgetEntityMappings()
		{
			this.AddElementMapping("WidgetGroupWidgetEntity", @"Obymobi", @"appless", "WidgetGroupWidget", 4, 0);
			this.AddElementFieldMapping("WidgetGroupWidgetEntity", "WidgetGroupId", "WidgetGroupId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("WidgetGroupWidgetEntity", "WidgetId", "WidgetId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("WidgetGroupWidgetEntity", "SortOrder", "SortOrder", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("WidgetGroupWidgetEntity", "ParentCompanyId", "ParentCompanyId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
		}

		/// <summary>Inits WidgetHeroEntity's mappings</summary>
		private void InitWidgetHeroEntityMappings()
		{
			this.AddElementMapping("WidgetHeroEntity", @"Obymobi", @"appless", "WidgetHero", 1, 0);
			this.AddElementFieldMapping("WidgetHeroEntity", "WidgetId", "WidgetId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 0);
		}

		/// <summary>Inits WidgetLanguageSwitcherEntity's mappings</summary>
		private void InitWidgetLanguageSwitcherEntityMappings()
		{
			this.AddElementMapping("WidgetLanguageSwitcherEntity", @"Obymobi", @"appless", "WidgetLanguageSwitcher", 2, 0);
			this.AddElementFieldMapping("WidgetLanguageSwitcherEntity", "WidgetId", "WidgetId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("WidgetLanguageSwitcherEntity", "Text", "Text", true, "NVarChar", 100, 0, 0, false, "", null, typeof(System.String), 1);
		}

		/// <summary>Inits WidgetMarkdownEntity's mappings</summary>
		private void InitWidgetMarkdownEntityMappings()
		{
			this.AddElementMapping("WidgetMarkdownEntity", @"Obymobi", @"appless", "WidgetMarkdown", 3, 0);
			this.AddElementFieldMapping("WidgetMarkdownEntity", "WidgetId", "WidgetId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("WidgetMarkdownEntity", "Title", "Title", false, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 1);
			this.AddElementFieldMapping("WidgetMarkdownEntity", "Text", "Text", false, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 2);
		}

		/// <summary>Inits WidgetOpeningTimeEntity's mappings</summary>
		private void InitWidgetOpeningTimeEntityMappings()
		{
			this.AddElementMapping("WidgetOpeningTimeEntity", @"Obymobi", @"appless", "WidgetOpeningTime", 2, 0);
			this.AddElementFieldMapping("WidgetOpeningTimeEntity", "WidgetId", "WidgetId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("WidgetOpeningTimeEntity", "Title", "Title", true, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 1);
		}

		/// <summary>Inits WidgetPageTitleEntity's mappings</summary>
		private void InitWidgetPageTitleEntityMappings()
		{
			this.AddElementMapping("WidgetPageTitleEntity", @"Obymobi", @"appless", "WidgetPageTitle", 2, 0);
			this.AddElementFieldMapping("WidgetPageTitleEntity", "WidgetId", "WidgetId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("WidgetPageTitleEntity", "Title", "Title", false, "NVarChar", 255, 0, 0, false, "", null, typeof(System.String), 1);
		}

		/// <summary>Inits WidgetWaitTimeEntity's mappings</summary>
		private void InitWidgetWaitTimeEntityMappings()
		{
			this.AddElementMapping("WidgetWaitTimeEntity", @"Obymobi", @"appless", "WidgetWaitTime", 2, 0);
			this.AddElementFieldMapping("WidgetWaitTimeEntity", "WidgetId", "WidgetId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("WidgetWaitTimeEntity", "Text", "Text", true, "NVarChar", 100, 0, 0, false, "", null, typeof(System.String), 1);
		}

	}
}
