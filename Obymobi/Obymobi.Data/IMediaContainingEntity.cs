﻿using Obymobi.Data.CollectionClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.Data
{
    public interface IMediaContainingEntity : SD.LLBLGen.Pro.ORMSupportClasses.IEntity
    {
        MediaCollection MediaCollection { get; }
    }
}
