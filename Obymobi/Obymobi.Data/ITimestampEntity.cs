﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.Data
{
    public interface ITimestampEntity
    {
        TimestampUpdaterBase TimestampUpdater { get; set; }

        bool RelevantFieldsChanged { get; set; }

        bool WasNew { get; set; }
    }
}
