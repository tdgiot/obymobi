﻿using Obymobi.Data.EntityClasses;

namespace Obymobi.Data
{
    public interface INullableBrandRelatedEntity
    {
        int? BrandId { get; }

        BrandEntity BrandEntity { get; }
    }
}