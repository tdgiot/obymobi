﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;

namespace Obymobi.Data
{
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: AccessCode.</summary>
	public enum AccessCodeFieldIndex
	{
		///<summary>AccessCodeId. </summary>
		AccessCodeId,
		///<summary>Code. </summary>
		Code,
		///<summary>Type. </summary>
		Type,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>AnalyticsEnabled. </summary>
		AnalyticsEnabled,
		///<summary>LastModifiedUTC. </summary>
		LastModifiedUTC,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: AccessCodeCompany.</summary>
	public enum AccessCodeCompanyFieldIndex
	{
		///<summary>AccessCodeCompanyId. </summary>
		AccessCodeCompanyId,
		///<summary>AccessCodeId. </summary>
		AccessCodeId,
		///<summary>CompanyId. </summary>
		CompanyId,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>IsHero. </summary>
		IsHero,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: AccessCodePointOfInterest.</summary>
	public enum AccessCodePointOfInterestFieldIndex
	{
		///<summary>AccessCodePointOfInterestId. </summary>
		AccessCodePointOfInterestId,
		///<summary>AccessCodeId. </summary>
		AccessCodeId,
		///<summary>PointOfInterestId. </summary>
		PointOfInterestId,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>IsHero. </summary>
		IsHero,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Account.</summary>
	public enum AccountFieldIndex
	{
		///<summary>AccountId. </summary>
		AccountId,
		///<summary>Name. </summary>
		Name,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: AccountCompany.</summary>
	public enum AccountCompanyFieldIndex
	{
		///<summary>AccountCompanyId. </summary>
		AccountCompanyId,
		///<summary>AccountId. </summary>
		AccountId,
		///<summary>CompanyId. </summary>
		CompanyId,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: ActionButton.</summary>
	public enum ActionButtonFieldIndex
	{
		///<summary>ActionButtonId. </summary>
		ActionButtonId,
		///<summary>Name. </summary>
		Name,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>LastModifiedUTC. </summary>
		LastModifiedUTC,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: ActionButtonLanguage.</summary>
	public enum ActionButtonLanguageFieldIndex
	{
		///<summary>ActionButtonLanguageId. </summary>
		ActionButtonLanguageId,
		///<summary>ActionButtonId. </summary>
		ActionButtonId,
		///<summary>LanguageId. </summary>
		LanguageId,
		///<summary>Name. </summary>
		Name,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Address.</summary>
	public enum AddressFieldIndex
	{
		///<summary>AddressId. </summary>
		AddressId,
		///<summary>Addressline1. </summary>
		Addressline1,
		///<summary>Addressline2. </summary>
		Addressline2,
		///<summary>Addressline3. </summary>
		Addressline3,
		///<summary>Zipcode. </summary>
		Zipcode,
		///<summary>City. </summary>
		City,
		///<summary>Latitude. </summary>
		Latitude,
		///<summary>Longitude. </summary>
		Longitude,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Advertisement.</summary>
	public enum AdvertisementFieldIndex
	{
		///<summary>AdvertisementId. </summary>
		AdvertisementId,
		///<summary>SupplierId. </summary>
		SupplierId,
		///<summary>CompanyId. </summary>
		CompanyId,
		///<summary>Name. </summary>
		Name,
		///<summary>GenericproductId. </summary>
		GenericproductId,
		///<summary>ProductId. </summary>
		ProductId,
		///<summary>EntertainmentId. </summary>
		EntertainmentId,
		///<summary>Visible. </summary>
		Visible,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>Description. </summary>
		Description,
		///<summary>ActionUrl. </summary>
		ActionUrl,
		///<summary>ActionCategoryId. </summary>
		ActionCategoryId,
		///<summary>ActionEntertainmentId. </summary>
		ActionEntertainmentId,
		///<summary>ActionEntertainmentCategoryId. </summary>
		ActionEntertainmentCategoryId,
		///<summary>DeliverypointgroupId. </summary>
		DeliverypointgroupId,
		///<summary>ActionPageId. </summary>
		ActionPageId,
		///<summary>ActionSiteId. </summary>
		ActionSiteId,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>ProductCategoryId. </summary>
		ProductCategoryId,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: AdvertisementConfiguration.</summary>
	public enum AdvertisementConfigurationFieldIndex
	{
		///<summary>AdvertisementConfigurationId. </summary>
		AdvertisementConfigurationId,
		///<summary>CompanyId. </summary>
		CompanyId,
		///<summary>Name. </summary>
		Name,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: AdvertisementConfigurationAdvertisement.</summary>
	public enum AdvertisementConfigurationAdvertisementFieldIndex
	{
		///<summary>AdvertisementConfigurationAdvertisementId. </summary>
		AdvertisementConfigurationAdvertisementId,
		///<summary>ParentCompanyId. </summary>
		ParentCompanyId,
		///<summary>AdvertisementConfigurationId. </summary>
		AdvertisementConfigurationId,
		///<summary>AdvertisementId. </summary>
		AdvertisementId,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>SortOrder. </summary>
		SortOrder,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: AdvertisementLanguage.</summary>
	public enum AdvertisementLanguageFieldIndex
	{
		///<summary>AdvertisementLanguageId. </summary>
		AdvertisementLanguageId,
		///<summary>ParentCompanyId. </summary>
		ParentCompanyId,
		///<summary>AdvertisementId. </summary>
		AdvertisementId,
		///<summary>LanguageId. </summary>
		LanguageId,
		///<summary>Name. </summary>
		Name,
		///<summary>Description. </summary>
		Description,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: AdvertisementTag.</summary>
	public enum AdvertisementTagFieldIndex
	{
		///<summary>AdvertisementTagId. </summary>
		AdvertisementTagId,
		///<summary>Name. </summary>
		Name,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>FriendlyName. </summary>
		FriendlyName,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: AdvertisementTagAdvertisement.</summary>
	public enum AdvertisementTagAdvertisementFieldIndex
	{
		///<summary>AdvertisementTagAdvertisementId. </summary>
		AdvertisementTagAdvertisementId,
		///<summary>AdvertisementId. </summary>
		AdvertisementId,
		///<summary>AdvertisementTagId. </summary>
		AdvertisementTagId,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>ParentCompanyId. </summary>
		ParentCompanyId,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: AdvertisementTagCategory.</summary>
	public enum AdvertisementTagCategoryFieldIndex
	{
		///<summary>AdvertisementTagCategoryId. </summary>
		AdvertisementTagCategoryId,
		///<summary>CategoryId. </summary>
		CategoryId,
		///<summary>AdvertisementTagId. </summary>
		AdvertisementTagId,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>ParentCompanyId. </summary>
		ParentCompanyId,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: AdvertisementTagEntertainment.</summary>
	public enum AdvertisementTagEntertainmentFieldIndex
	{
		///<summary>AdvertisementTagEntertainmentId. </summary>
		AdvertisementTagEntertainmentId,
		///<summary>EntertainmentId. </summary>
		EntertainmentId,
		///<summary>AdvertisementTagId. </summary>
		AdvertisementTagId,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>ParentCompanyId. </summary>
		ParentCompanyId,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: AdvertisementTagGenericproduct.</summary>
	public enum AdvertisementTagGenericproductFieldIndex
	{
		///<summary>AdvertisementTagGenericproductId. </summary>
		AdvertisementTagGenericproductId,
		///<summary>GenericproductId. </summary>
		GenericproductId,
		///<summary>AdvertisementTagId. </summary>
		AdvertisementTagId,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: AdvertisementTagLanguage.</summary>
	public enum AdvertisementTagLanguageFieldIndex
	{
		///<summary>AdvertisementTagLanguageId. </summary>
		AdvertisementTagLanguageId,
		///<summary>AdvertisementTagId. </summary>
		AdvertisementTagId,
		///<summary>LanguageId. </summary>
		LanguageId,
		///<summary>FriendlyName. </summary>
		FriendlyName,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: AdvertisementTagProduct.</summary>
	public enum AdvertisementTagProductFieldIndex
	{
		///<summary>AdvertisementTagProductId. </summary>
		AdvertisementTagProductId,
		///<summary>ProductId. </summary>
		ProductId,
		///<summary>AdvertisementTagId. </summary>
		AdvertisementTagId,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>ParentCompanyId. </summary>
		ParentCompanyId,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: AdyenPaymentMethod.</summary>
	public enum AdyenPaymentMethodFieldIndex
	{
		///<summary>AdyenPaymentMethodId. </summary>
		AdyenPaymentMethodId,
		///<summary>PaymentIntegrationConfigurationId. </summary>
		PaymentIntegrationConfigurationId,
		///<summary>ParentCompanyId. </summary>
		ParentCompanyId,
		///<summary>Type. </summary>
		Type,
		///<summary>Active. </summary>
		Active,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: AdyenPaymentMethodBrand.</summary>
	public enum AdyenPaymentMethodBrandFieldIndex
	{
		///<summary>AdyenPaymentMethodBrandId. </summary>
		AdyenPaymentMethodBrandId,
		///<summary>AdyenPaymentMethodId. </summary>
		AdyenPaymentMethodId,
		///<summary>ParentCompanyId. </summary>
		ParentCompanyId,
		///<summary>Brand. </summary>
		Brand,
		///<summary>Active. </summary>
		Active,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: AffiliateCampaign.</summary>
	public enum AffiliateCampaignFieldIndex
	{
		///<summary>AffiliateCampaignId. </summary>
		AffiliateCampaignId,
		///<summary>Name. </summary>
		Name,
		///<summary>Code. </summary>
		Code,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: AffiliateCampaignAffiliatePartner.</summary>
	public enum AffiliateCampaignAffiliatePartnerFieldIndex
	{
		///<summary>AffiliateCampaignAffiliatePartnerId. </summary>
		AffiliateCampaignAffiliatePartnerId,
		///<summary>AffiliateCampaignId. </summary>
		AffiliateCampaignId,
		///<summary>AffiliatePartnerId. </summary>
		AffiliatePartnerId,
		///<summary>PartnerCampaignCode. </summary>
		PartnerCampaignCode,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: AffiliatePartner.</summary>
	public enum AffiliatePartnerFieldIndex
	{
		///<summary>AffiliatePartnerId. </summary>
		AffiliatePartnerId,
		///<summary>Name. </summary>
		Name,
		///<summary>DomainUrl. </summary>
		DomainUrl,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Alteration.</summary>
	public enum AlterationFieldIndex
	{
		///<summary>AlterationId. </summary>
		AlterationId,
		///<summary>Name. </summary>
		Name,
		///<summary>MinOptions. </summary>
		MinOptions,
		///<summary>MaxOptions. </summary>
		MaxOptions,
		///<summary>PosalterationId. </summary>
		PosalterationId,
		///<summary>CompanyId. </summary>
		CompanyId,
		///<summary>AvailableOnOtoucho. </summary>
		AvailableOnOtoucho,
		///<summary>AvailableOnObymobi. </summary>
		AvailableOnObymobi,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>MinLeadMinutes. </summary>
		MinLeadMinutes,
		///<summary>MaxLeadHours. </summary>
		MaxLeadHours,
		///<summary>IntervalMinutes. </summary>
		IntervalMinutes,
		///<summary>ShowDatePicker. </summary>
		ShowDatePicker,
		///<summary>Type. </summary>
		Type,
		///<summary>GenericalterationId. </summary>
		GenericalterationId,
		///<summary>Value. </summary>
		Value,
		///<summary>OrderLevelEnabled. </summary>
		OrderLevelEnabled,
		///<summary>Description. </summary>
		Description,
		///<summary>LayoutType. </summary>
		LayoutType,
		///<summary>ParentAlterationId. </summary>
		ParentAlterationId,
		///<summary>SortOrder. </summary>
		SortOrder,
		///<summary>Visible. </summary>
		Visible,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>FriendlyName. </summary>
		FriendlyName,
		///<summary>PriceAddition. </summary>
		PriceAddition,
		///<summary>Version. </summary>
		Version,
		///<summary>OriginalAlterationId. </summary>
		OriginalAlterationId,
		///<summary>AllowDuplicates. </summary>
		AllowDuplicates,
		///<summary>StartTime. </summary>
		StartTime,
		///<summary>EndTime. </summary>
		EndTime,
		///<summary>StartTimeUTC. </summary>
		StartTimeUTC,
		///<summary>EndTimeUTC. </summary>
		EndTimeUTC,
		///<summary>BrandId. </summary>
		BrandId,
		///<summary>ExternalProductId. </summary>
		ExternalProductId,
		///<summary>IsAvailable. </summary>
		IsAvailable,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Alterationitem.</summary>
	public enum AlterationitemFieldIndex
	{
		///<summary>AlterationitemId. </summary>
		AlterationitemId,
		///<summary>AlterationId. </summary>
		AlterationId,
		///<summary>AlterationoptionId. </summary>
		AlterationoptionId,
		///<summary>SelectedOnDefault. </summary>
		SelectedOnDefault,
		///<summary>SortOrder. </summary>
		SortOrder,
		///<summary>PosalterationitemId. </summary>
		PosalterationitemId,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>GenericalterationitemId. </summary>
		GenericalterationitemId,
		///<summary>ParentCompanyId. </summary>
		ParentCompanyId,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>Version. </summary>
		Version,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: AlterationitemAlteration.</summary>
	public enum AlterationitemAlterationFieldIndex
	{
		///<summary>AlterationitemAlterationId. </summary>
		AlterationitemAlterationId,
		///<summary>ParentCompanyId. </summary>
		ParentCompanyId,
		///<summary>AlterationitemId. </summary>
		AlterationitemId,
		///<summary>AlterationId. </summary>
		AlterationId,
		///<summary>SortOrder. </summary>
		SortOrder,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: AlterationLanguage.</summary>
	public enum AlterationLanguageFieldIndex
	{
		///<summary>AlterationLanguageId. </summary>
		AlterationLanguageId,
		///<summary>AlterationId. </summary>
		AlterationId,
		///<summary>LanguageId. </summary>
		LanguageId,
		///<summary>Name. </summary>
		Name,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>ParentCompanyId. </summary>
		ParentCompanyId,
		///<summary>Description. </summary>
		Description,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Alterationoption.</summary>
	public enum AlterationoptionFieldIndex
	{
		///<summary>AlterationoptionId. </summary>
		AlterationoptionId,
		///<summary>PosalterationoptionId. </summary>
		PosalterationoptionId,
		///<summary>Name. </summary>
		Name,
		///<summary>PriceIn. </summary>
		PriceIn,
		///<summary>Description. </summary>
		Description,
		///<summary>PosproductId. </summary>
		PosproductId,
		///<summary>IsProductRelated. </summary>
		IsProductRelated,
		///<summary>CompanyId. </summary>
		CompanyId,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>GenericalterationoptionId. </summary>
		GenericalterationoptionId,
		///<summary>FriendlyName. </summary>
		FriendlyName,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>Type. </summary>
		Type,
		///<summary>XPriceAddition. </summary>
		XPriceAddition,
		///<summary>StartTime. </summary>
		StartTime,
		///<summary>EndTime. </summary>
		EndTime,
		///<summary>MinLeadMinutes. </summary>
		MinLeadMinutes,
		///<summary>MaxLeadHours. </summary>
		MaxLeadHours,
		///<summary>IntervalMinutes. </summary>
		IntervalMinutes,
		///<summary>ShowDatePicker. </summary>
		ShowDatePicker,
		///<summary>Version. </summary>
		Version,
		///<summary>StartTimeUTC. </summary>
		StartTimeUTC,
		///<summary>EndTimeUTC. </summary>
		EndTimeUTC,
		///<summary>BrandId. </summary>
		BrandId,
		///<summary>TaxTariffId. </summary>
		TaxTariffId,
		///<summary>IsAlcoholic. </summary>
		IsAlcoholic,
		///<summary>InheritName. </summary>
		InheritName,
		///<summary>InheritPrice. </summary>
		InheritPrice,
		///<summary>InheritTaxTariff. </summary>
		InheritTaxTariff,
		///<summary>InheritIsAlcoholic. </summary>
		InheritIsAlcoholic,
		///<summary>ProductId. </summary>
		ProductId,
		///<summary>InheritTags. </summary>
		InheritTags,
		///<summary>InheritCustomText. </summary>
		InheritCustomText,
		///<summary>ExternalProductId. </summary>
		ExternalProductId,
		///<summary>IsAvailable. </summary>
		IsAvailable,
		///<summary>ExternalIdentifier. </summary>
		ExternalIdentifier,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: AlterationoptionLanguage.</summary>
	public enum AlterationoptionLanguageFieldIndex
	{
		///<summary>AlterationoptionLanguageId. </summary>
		AlterationoptionLanguageId,
		///<summary>AlterationoptionId. </summary>
		AlterationoptionId,
		///<summary>LanguageId. </summary>
		LanguageId,
		///<summary>Name. </summary>
		Name,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>ParentCompanyId. </summary>
		ParentCompanyId,
		///<summary>Description. </summary>
		Description,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: AlterationoptionTag.</summary>
	public enum AlterationoptionTagFieldIndex
	{
		///<summary>AlterationoptionTagId. </summary>
		AlterationoptionTagId,
		///<summary>AlterationoptionId. </summary>
		AlterationoptionId,
		///<summary>TagId. </summary>
		TagId,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>ParentCompanyId. </summary>
		ParentCompanyId,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: AlterationProduct.</summary>
	public enum AlterationProductFieldIndex
	{
		///<summary>AlterationProductId. </summary>
		AlterationProductId,
		///<summary>ParentCompanyId. </summary>
		ParentCompanyId,
		///<summary>AlterationId. </summary>
		AlterationId,
		///<summary>ProductId. </summary>
		ProductId,
		///<summary>SortOrder. </summary>
		SortOrder,
		///<summary>Visible. </summary>
		Visible,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Amenity.</summary>
	public enum AmenityFieldIndex
	{
		///<summary>AmenityId. </summary>
		AmenityId,
		///<summary>Name. </summary>
		Name,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>LastModifiedUTC. </summary>
		LastModifiedUTC,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: AmenityLanguage.</summary>
	public enum AmenityLanguageFieldIndex
	{
		///<summary>AmenityLanguageId. </summary>
		AmenityLanguageId,
		///<summary>AmenityId. </summary>
		AmenityId,
		///<summary>LanguageId. </summary>
		LanguageId,
		///<summary>Name. </summary>
		Name,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: AnalyticsProcessingTask.</summary>
	public enum AnalyticsProcessingTaskFieldIndex
	{
		///<summary>AnalyticsProcessingTaskId. </summary>
		AnalyticsProcessingTaskId,
		///<summary>OrderId. </summary>
		OrderId,
		///<summary>EventAction. </summary>
		EventAction,
		///<summary>EventLabel. </summary>
		EventLabel,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Announcement.</summary>
	public enum AnnouncementFieldIndex
	{
		///<summary>AnnouncementId. </summary>
		AnnouncementId,
		///<summary>CompanyId. </summary>
		CompanyId,
		///<summary>Title. </summary>
		Title,
		///<summary>Text. </summary>
		Text,
		///<summary>DateToShow. </summary>
		DateToShow,
		///<summary>TimeToShow. </summary>
		TimeToShow,
		///<summary>Recurring. </summary>
		Recurring,
		///<summary>RecurringPeriod. </summary>
		RecurringPeriod,
		///<summary>RecurringBeginDate. </summary>
		RecurringBeginDate,
		///<summary>RecurringBeginTime. </summary>
		RecurringBeginTime,
		///<summary>RecurringEndDate. </summary>
		RecurringEndDate,
		///<summary>RecurringEndTime. </summary>
		RecurringEndTime,
		///<summary>RecurringAmount. </summary>
		RecurringAmount,
		///<summary>RecurringMinutes. </summary>
		RecurringMinutes,
		///<summary>DialogType. </summary>
		DialogType,
		///<summary>OnYes. </summary>
		OnYes,
		///<summary>OnNo. </summary>
		OnNo,
		///<summary>OnYesCategory. </summary>
		OnYesCategory,
		///<summary>OnNoCategory. </summary>
		OnNoCategory,
		///<summary>OnYesEntertainmentCategory. </summary>
		OnYesEntertainmentCategory,
		///<summary>OnNoEntertainmentCategory. </summary>
		OnNoEntertainmentCategory,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>Duration. </summary>
		Duration,
		///<summary>OnYesEntertainment. </summary>
		OnYesEntertainment,
		///<summary>MediaId. </summary>
		MediaId,
		///<summary>DeliverypointgroupId. </summary>
		DeliverypointgroupId,
		///<summary>OnYesProduct. </summary>
		OnYesProduct,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: AnnouncementLanguage.</summary>
	public enum AnnouncementLanguageFieldIndex
	{
		///<summary>AnnouncementLanguageId. </summary>
		AnnouncementLanguageId,
		///<summary>AnnouncementId. </summary>
		AnnouncementId,
		///<summary>LanguageId. </summary>
		LanguageId,
		///<summary>Title. </summary>
		Title,
		///<summary>Text. </summary>
		Text,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>ParentCompanyId. </summary>
		ParentCompanyId,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: ApiAuthentication.</summary>
	public enum ApiAuthenticationFieldIndex
	{
		///<summary>ApiAuthenticationId. </summary>
		ApiAuthenticationId,
		///<summary>CompanyId. </summary>
		CompanyId,
		///<summary>Type. </summary>
		Type,
		///<summary>Name. </summary>
		Name,
		///<summary>FieldValue1. </summary>
		FieldValue1,
		///<summary>FieldValue2. </summary>
		FieldValue2,
		///<summary>FieldValue3. </summary>
		FieldValue3,
		///<summary>FieldValue4. </summary>
		FieldValue4,
		///<summary>FieldValue5. </summary>
		FieldValue5,
		///<summary>FieldValue6. </summary>
		FieldValue6,
		///<summary>FieldValue7. </summary>
		FieldValue7,
		///<summary>FieldValue8. </summary>
		FieldValue8,
		///<summary>FieldValue9. </summary>
		FieldValue9,
		///<summary>FieldValue10. </summary>
		FieldValue10,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>ExpiryDateUTC. </summary>
		ExpiryDateUTC,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Application.</summary>
	public enum ApplicationFieldIndex
	{
		///<summary>ApplicationId. </summary>
		ApplicationId,
		///<summary>Name. </summary>
		Name,
		///<summary>Code. </summary>
		Code,
		///<summary>Description. </summary>
		Description,
		///<summary>ReleaseId. </summary>
		ReleaseId,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>DeviceModel. </summary>
		DeviceModel,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Attachment.</summary>
	public enum AttachmentFieldIndex
	{
		///<summary>AttachmentId. </summary>
		AttachmentId,
		///<summary>PageId. </summary>
		PageId,
		///<summary>ProductId. </summary>
		ProductId,
		///<summary>Name. </summary>
		Name,
		///<summary>Type. </summary>
		Type,
		///<summary>Url. </summary>
		Url,
		///<summary>AllowedDomains. </summary>
		AllowedDomains,
		///<summary>AllowAllDomains. </summary>
		AllowAllDomains,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>ParentCompanyId. </summary>
		ParentCompanyId,
		///<summary>PageTemplateId. </summary>
		PageTemplateId,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>GenericproductId. </summary>
		GenericproductId,
		///<summary>OpenNewWindow. </summary>
		OpenNewWindow,
		///<summary>SortOrder. </summary>
		SortOrder,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: AttachmentLanguage.</summary>
	public enum AttachmentLanguageFieldIndex
	{
		///<summary>AttachmentLanguageId. </summary>
		AttachmentLanguageId,
		///<summary>AttachmentId. </summary>
		AttachmentId,
		///<summary>LanguageId. </summary>
		LanguageId,
		///<summary>Name. </summary>
		Name,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>ParentCompanyId. </summary>
		ParentCompanyId,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Auditlog.</summary>
	public enum AuditlogFieldIndex
	{
		///<summary>AuditLogId. </summary>
		AuditLogId,
		///<summary>ActionType. </summary>
		ActionType,
		///<summary>EntityType. </summary>
		EntityType,
		///<summary>EntityId. </summary>
		EntityId,
		///<summary>ShowName. </summary>
		ShowName,
		///<summary>FieldName0. </summary>
		FieldName0,
		///<summary>FieldValue0. </summary>
		FieldValue0,
		///<summary>FieldName1. </summary>
		FieldName1,
		///<summary>FieldValue1. </summary>
		FieldValue1,
		///<summary>FieldName2. </summary>
		FieldName2,
		///<summary>FieldValue2. </summary>
		FieldValue2,
		///<summary>FieldName3. </summary>
		FieldName3,
		///<summary>FieldValue3. </summary>
		FieldValue3,
		///<summary>FieldName4. </summary>
		FieldName4,
		///<summary>FieldValue4. </summary>
		FieldValue4,
		///<summary>FieldName5. </summary>
		FieldName5,
		///<summary>FieldValue5. </summary>
		FieldValue5,
		///<summary>FieldName6. </summary>
		FieldName6,
		///<summary>FieldValue6. </summary>
		FieldValue6,
		///<summary>FieldName7. </summary>
		FieldName7,
		///<summary>FieldValue7. </summary>
		FieldValue7,
		///<summary>FieldName8. </summary>
		FieldName8,
		///<summary>FieldValue8. </summary>
		FieldValue8,
		///<summary>FieldName9. </summary>
		FieldName9,
		///<summary>FieldValue9. </summary>
		FieldValue9,
		///<summary>FieldName10. </summary>
		FieldName10,
		///<summary>FieldValue10. </summary>
		FieldValue10,
		///<summary>FieldName11. </summary>
		FieldName11,
		///<summary>FieldValue11. </summary>
		FieldValue11,
		///<summary>FieldName12. </summary>
		FieldName12,
		///<summary>FieldValue12. </summary>
		FieldValue12,
		///<summary>FieldName13. </summary>
		FieldName13,
		///<summary>FieldValue13. </summary>
		FieldValue13,
		///<summary>FieldName14. </summary>
		FieldName14,
		///<summary>FieldValue14. </summary>
		FieldValue14,
		///<summary>FieldName15. </summary>
		FieldName15,
		///<summary>FieldValue15. </summary>
		FieldValue15,
		///<summary>FieldName16. </summary>
		FieldName16,
		///<summary>FieldValue16. </summary>
		FieldValue16,
		///<summary>FieldName17. </summary>
		FieldName17,
		///<summary>FieldValue17. </summary>
		FieldValue17,
		///<summary>FieldName18. </summary>
		FieldName18,
		///<summary>FieldValue18. </summary>
		FieldValue18,
		///<summary>FieldName19. </summary>
		FieldName19,
		///<summary>FieldValue19. </summary>
		FieldValue19,
		///<summary>FieldName20. </summary>
		FieldName20,
		///<summary>FieldValue20. </summary>
		FieldValue20,
		///<summary>FieldsAndValues. </summary>
		FieldsAndValues,
		///<summary>CallStack. </summary>
		CallStack,
		///<summary>Page. </summary>
		Page,
		///<summary>BreadcrumbSteps. </summary>
		BreadcrumbSteps,
		///<summary>ActionBy. </summary>
		ActionBy,
		///<summary>ActionPerformed. </summary>
		ActionPerformed,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>ActionPerformedUTC. </summary>
		ActionPerformedUTC,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Availability.</summary>
	public enum AvailabilityFieldIndex
	{
		///<summary>AvailabilityId. </summary>
		AvailabilityId,
		///<summary>CompanyId. </summary>
		CompanyId,
		///<summary>Name. </summary>
		Name,
		///<summary>StatusText. </summary>
		StatusText,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>Status. </summary>
		Status,
		///<summary>Url. </summary>
		Url,
		///<summary>ActionCategoryId. </summary>
		ActionCategoryId,
		///<summary>ActionEntertainmentId. </summary>
		ActionEntertainmentId,
		///<summary>ActionPageId. </summary>
		ActionPageId,
		///<summary>ActionSiteId. </summary>
		ActionSiteId,
		///<summary>ActionProductCategoryId. </summary>
		ActionProductCategoryId,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: AzureNotificationHub.</summary>
	public enum AzureNotificationHubFieldIndex
	{
		///<summary>NotificationHubId. </summary>
		NotificationHubId,
		///<summary>ConnectionString. </summary>
		ConnectionString,
		///<summary>HubPath. </summary>
		HubPath,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Brand.</summary>
	public enum BrandFieldIndex
	{
		///<summary>BrandId. </summary>
		BrandId,
		///<summary>Name. </summary>
		Name,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>CultureCode. </summary>
		CultureCode,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: BrandCulture.</summary>
	public enum BrandCultureFieldIndex
	{
		///<summary>BrandCultureId. </summary>
		BrandCultureId,
		///<summary>BrandId. </summary>
		BrandId,
		///<summary>CultureCode. </summary>
		CultureCode,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Businesshours.</summary>
	public enum BusinesshoursFieldIndex
	{
		///<summary>BusinesshoursId. </summary>
		BusinesshoursId,
		///<summary>CompanyId. </summary>
		CompanyId,
		///<summary>DayOfWeekAndTime. </summary>
		DayOfWeekAndTime,
		///<summary>Opening. </summary>
		Opening,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>PointOfInterestId. </summary>
		PointOfInterestId,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>OutletId. </summary>
		OutletId,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Businesshoursexception.</summary>
	public enum BusinesshoursexceptionFieldIndex
	{
		///<summary>BusinesshoursexceptionId. </summary>
		BusinesshoursexceptionId,
		///<summary>CompanyId. </summary>
		CompanyId,
		///<summary>Opened. </summary>
		Opened,
		///<summary>FromDateTime. </summary>
		FromDateTime,
		///<summary>TillDateTime. </summary>
		TillDateTime,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Category.</summary>
	public enum CategoryFieldIndex
	{
		///<summary>CategoryId. </summary>
		CategoryId,
		///<summary>CompanyId. </summary>
		CompanyId,
		///<summary>ParentCategoryId. </summary>
		ParentCategoryId,
		///<summary>GenericcategoryId. </summary>
		GenericcategoryId,
		///<summary>Name. </summary>
		Name,
		///<summary>SortOrder. </summary>
		SortOrder,
		///<summary>PoscategoryId. </summary>
		PoscategoryId,
		///<summary>ProductId. </summary>
		ProductId,
		///<summary>AvailableOnOtoucho. </summary>
		AvailableOnOtoucho,
		///<summary>AvailableOnObymobi. </summary>
		AvailableOnObymobi,
		///<summary>Rateable. </summary>
		Rateable,
		///<summary>Visible. </summary>
		Visible,
		///<summary>Type. </summary>
		Type,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>RouteId. </summary>
		RouteId,
		///<summary>MenuId. </summary>
		MenuId,
		///<summary>HidePrices. </summary>
		HidePrices,
		///<summary>AnnouncementAction. </summary>
		AnnouncementAction,
		///<summary>Color. </summary>
		Color,
		///<summary>Description. </summary>
		Description,
		///<summary>Geofencing. </summary>
		Geofencing,
		///<summary>AllowFreeText. </summary>
		AllowFreeText,
		///<summary>ScheduleId. </summary>
		ScheduleId,
		///<summary>ViewLayoutType. </summary>
		ViewLayoutType,
		///<summary>DeliveryLocationType. </summary>
		DeliveryLocationType,
		///<summary>SupportNotificationType. </summary>
		SupportNotificationType,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>VisibilityType. </summary>
		VisibilityType,
		///<summary>ButtonText. </summary>
		ButtonText,
		///<summary>CustomizeButtonText. </summary>
		CustomizeButtonText,
		///<summary>ViewType. </summary>
		ViewType,
		///<summary>MenuItemsMustBeLinkedToExternalProduct. </summary>
		MenuItemsMustBeLinkedToExternalProduct,
		///<summary>ShowName. </summary>
		ShowName,
		///<summary>CoversType. </summary>
		CoversType,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: CategoryAlteration.</summary>
	public enum CategoryAlterationFieldIndex
	{
		///<summary>CategoryAlterationId. </summary>
		CategoryAlterationId,
		///<summary>CategoryId. </summary>
		CategoryId,
		///<summary>AlterationId. </summary>
		AlterationId,
		///<summary>SortOrder. </summary>
		SortOrder,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>ParentCompanyId. </summary>
		ParentCompanyId,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>Version. </summary>
		Version,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: CategoryLanguage.</summary>
	public enum CategoryLanguageFieldIndex
	{
		///<summary>CategoryLanguageId. </summary>
		CategoryLanguageId,
		///<summary>CategoryId. </summary>
		CategoryId,
		///<summary>LanguageId. </summary>
		LanguageId,
		///<summary>Name. </summary>
		Name,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>Description. </summary>
		Description,
		///<summary>OrderProcessedTitle. </summary>
		OrderProcessedTitle,
		///<summary>OrderProcessedMessage. </summary>
		OrderProcessedMessage,
		///<summary>ParentCompanyId. </summary>
		ParentCompanyId,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: CategorySuggestion.</summary>
	public enum CategorySuggestionFieldIndex
	{
		///<summary>CategorySuggestionId. </summary>
		CategorySuggestionId,
		///<summary>CategoryId. </summary>
		CategoryId,
		///<summary>ProductId. </summary>
		ProductId,
		///<summary>SortOrder. </summary>
		SortOrder,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>Checkout. </summary>
		Checkout,
		///<summary>ParentCompanyId. </summary>
		ParentCompanyId,
		///<summary>ProductCategoryId. </summary>
		ProductCategoryId,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: CategoryTag.</summary>
	public enum CategoryTagFieldIndex
	{
		///<summary>CategoryTagId. </summary>
		CategoryTagId,
		///<summary>CategoryId. </summary>
		CategoryId,
		///<summary>TagId. </summary>
		TagId,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>ParentCompanyId. </summary>
		ParentCompanyId,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: CheckoutMethod.</summary>
	public enum CheckoutMethodFieldIndex
	{
		///<summary>CheckoutMethodId. </summary>
		CheckoutMethodId,
		///<summary>CompanyId. </summary>
		CompanyId,
		///<summary>OutletId. </summary>
		OutletId,
		///<summary>Active. </summary>
		Active,
		///<summary>Name. </summary>
		Name,
		///<summary>Label. </summary>
		Label,
		///<summary>Description. </summary>
		Description,
		///<summary>ConfirmationActive. </summary>
		ConfirmationActive,
		///<summary>ConfirmationDescription. </summary>
		ConfirmationDescription,
		///<summary>CheckoutType. </summary>
		CheckoutType,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>ReceiptTemplateId. </summary>
		ReceiptTemplateId,
		///<summary>PaymentIntegrationConfigurationId. </summary>
		PaymentIntegrationConfigurationId,
		///<summary>OrderConfirmationNotificationMethod. </summary>
		OrderConfirmationNotificationMethod,
		///<summary>OrderPaymentPendingNotificationMethod. </summary>
		OrderPaymentPendingNotificationMethod,
		///<summary>OrderPaymentFailedNotificationMethod. </summary>
		OrderPaymentFailedNotificationMethod,
		///<summary>OrderReceiptNotificationMethod. </summary>
		OrderReceiptNotificationMethod,
		///<summary>TermsAndConditionsRequired. </summary>
		TermsAndConditionsRequired,
		///<summary>TermsAndConditionsLabel. </summary>
		TermsAndConditionsLabel,
		///<summary>CurrencyCode. </summary>
		CurrencyCode,
		///<summary>CountryCode. </summary>
		CountryCode,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: CheckoutMethodDeliverypointgroup.</summary>
	public enum CheckoutMethodDeliverypointgroupFieldIndex
	{
		///<summary>DeliverypointgroupId. </summary>
		DeliverypointgroupId,
		///<summary>CheckoutMethodId. </summary>
		CheckoutMethodId,
		///<summary>ParentCompanyId. </summary>
		ParentCompanyId,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Client.</summary>
	public enum ClientFieldIndex
	{
		///<summary>ClientId. </summary>
		ClientId,
		///<summary>CompanyId. </summary>
		CompanyId,
		///<summary>ImageVersion. </summary>
		ImageVersion,
		///<summary>Notes. </summary>
		Notes,
		///<summary>DeliverypointGroupId. </summary>
		DeliverypointGroupId,
		///<summary>OperationMode. </summary>
		OperationMode,
		///<summary>LastStatus. </summary>
		LastStatus,
		///<summary>LastStatusText. </summary>
		LastStatusText,
		///<summary>LogToFile. </summary>
		LogToFile,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>DeviceId. </summary>
		DeviceId,
		///<summary>HeartbeatPoll. </summary>
		HeartbeatPoll,
		///<summary>OutOfChargeNotificationsSent. </summary>
		OutOfChargeNotificationsSent,
		///<summary>DeliverypointId. </summary>
		DeliverypointId,
		///<summary>LastDeliverypointId. </summary>
		LastDeliverypointId,
		///<summary>LastOperationMode. </summary>
		LastOperationMode,
		///<summary>LastStateOnline. </summary>
		LastStateOnline,
		///<summary>LastStateOperationMode. </summary>
		LastStateOperationMode,
		///<summary>BluetoothKeyboardConnected. </summary>
		BluetoothKeyboardConnected,
		///<summary>RoomControlAreaId. </summary>
		RoomControlAreaId,
		///<summary>WakeUpTimeUtc. </summary>
		WakeUpTimeUtc,
		///<summary>OutOfChargeNotificationLastSentUTC. </summary>
		OutOfChargeNotificationLastSentUTC,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>RoomControlConnected. </summary>
		RoomControlConnected,
		///<summary>LoadedSuccessfully. </summary>
		LoadedSuccessfully,
		///<summary>WakeUpTimeUtcUTC. </summary>
		WakeUpTimeUtcUTC,
		///<summary>DoNotDisturbActive. </summary>
		DoNotDisturbActive,
		///<summary>ServiceRoomActive. </summary>
		ServiceRoomActive,
		///<summary>LastUserInteraction. </summary>
		LastUserInteraction,
		///<summary>StatusUpdatedUTC. </summary>
		StatusUpdatedUTC,
		///<summary>LastWifiSsid. </summary>
		LastWifiSsid,
		///<summary>IsTestClient. </summary>
		IsTestClient,
		///<summary>LastApiVersion. </summary>
		LastApiVersion,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: ClientConfiguration.</summary>
	public enum ClientConfigurationFieldIndex
	{
		///<summary>ClientConfigurationId. </summary>
		ClientConfigurationId,
		///<summary>CompanyId. </summary>
		CompanyId,
		///<summary>Name. </summary>
		Name,
		///<summary>DeliverypointgroupId. </summary>
		DeliverypointgroupId,
		///<summary>MenuId. </summary>
		MenuId,
		///<summary>UIModeId. </summary>
		UIModeId,
		///<summary>UIThemeId. </summary>
		UIThemeId,
		///<summary>UIScheduleId. </summary>
		UIScheduleId,
		///<summary>RoomControlConfigurationId. </summary>
		RoomControlConfigurationId,
		///<summary>WifiConfigurationId. </summary>
		WifiConfigurationId,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>Pincode. </summary>
		Pincode,
		///<summary>PincodeSU. </summary>
		PincodeSU,
		///<summary>PincodeGM. </summary>
		PincodeGM,
		///<summary>HidePrices. </summary>
		HidePrices,
		///<summary>ShowCurrencySymbol. </summary>
		ShowCurrencySymbol,
		///<summary>ClearSessionOnTimeout. </summary>
		ClearSessionOnTimeout,
		///<summary>ResetTimeout. </summary>
		ResetTimeout,
		///<summary>ScreenTimeoutInterval. </summary>
		ScreenTimeoutInterval,
		///<summary>DimLevelDull. </summary>
		DimLevelDull,
		///<summary>DimLevelMedium. </summary>
		DimLevelMedium,
		///<summary>DimLevelBright. </summary>
		DimLevelBright,
		///<summary>DockedDimLevelDull. </summary>
		DockedDimLevelDull,
		///<summary>DockedDimLevelMedium. </summary>
		DockedDimLevelMedium,
		///<summary>DockedDimLevelBright. </summary>
		DockedDimLevelBright,
		///<summary>PowerSaveTimeout. </summary>
		PowerSaveTimeout,
		///<summary>PowerSaveLevel. </summary>
		PowerSaveLevel,
		///<summary>OutOfChargeLevel. </summary>
		OutOfChargeLevel,
		///<summary>DailyOrderReset. </summary>
		DailyOrderReset,
		///<summary>SleepTime. </summary>
		SleepTime,
		///<summary>WakeUpTime. </summary>
		WakeUpTime,
		///<summary>HomepageSlideshowInterval. </summary>
		HomepageSlideshowInterval,
		///<summary>IsChargerRemovedDialogEnabled. </summary>
		IsChargerRemovedDialogEnabled,
		///<summary>IsChargerRemovedReminderDialogEnabled. </summary>
		IsChargerRemovedReminderDialogEnabled,
		///<summary>PowerButtonHardBehaviour. </summary>
		PowerButtonHardBehaviour,
		///<summary>PowerButtonSoftBehaviour. </summary>
		PowerButtonSoftBehaviour,
		///<summary>ScreenOffMode. </summary>
		ScreenOffMode,
		///<summary>ScreensaverMode. </summary>
		ScreensaverMode,
		///<summary>DeviceRebootMethod. </summary>
		DeviceRebootMethod,
		///<summary>RoomserviceCharge. </summary>
		RoomserviceCharge,
		///<summary>AllowFreeText. </summary>
		AllowFreeText,
		///<summary>BrowserAgeVerificationEnabled. </summary>
		BrowserAgeVerificationEnabled,
		///<summary>GooglePrinterId. </summary>
		GooglePrinterId,
		///<summary>PriceScheduleId. </summary>
		PriceScheduleId,
		///<summary>AdvertisementConfigurationId. </summary>
		AdvertisementConfigurationId,
		///<summary>EntertainmentConfigurationId. </summary>
		EntertainmentConfigurationId,
		///<summary>BrowserAgeVerificationLayout. </summary>
		BrowserAgeVerificationLayout,
		///<summary>RestartApplicationDialogEnabled. </summary>
		RestartApplicationDialogEnabled,
		///<summary>RebootDeviceDialogEnabled. </summary>
		RebootDeviceDialogEnabled,
		///<summary>RestartTimeoutSeconds. </summary>
		RestartTimeoutSeconds,
		///<summary>PmsIntegration. </summary>
		PmsIntegration,
		///<summary>PmsAllowShowBill. </summary>
		PmsAllowShowBill,
		///<summary>PmsAllowExpressCheckout. </summary>
		PmsAllowExpressCheckout,
		///<summary>PmsAllowShowGuestName. </summary>
		PmsAllowShowGuestName,
		///<summary>PmsLockClientWhenNotCheckedIn. </summary>
		PmsLockClientWhenNotCheckedIn,
		///<summary>PmsWelcomeTimeoutMinutes. </summary>
		PmsWelcomeTimeoutMinutes,
		///<summary>PmsCheckoutCompleteTimeoutMinutes. </summary>
		PmsCheckoutCompleteTimeoutMinutes,
		///<summary>IsOrderitemAddedDialogEnabled. </summary>
		IsOrderitemAddedDialogEnabled,
		///<summary>OrderCompletedNotificationEnabled. </summary>
		OrderCompletedNotificationEnabled,
		///<summary>IsClearBasketDialogEnabled. </summary>
		IsClearBasketDialogEnabled,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: ClientConfigurationRoute.</summary>
	public enum ClientConfigurationRouteFieldIndex
	{
		///<summary>ClientConfigurationRouteId. </summary>
		ClientConfigurationRouteId,
		///<summary>ParentCompanyId. </summary>
		ParentCompanyId,
		///<summary>ClientConfigurationId. </summary>
		ClientConfigurationId,
		///<summary>RouteId. </summary>
		RouteId,
		///<summary>Type. </summary>
		Type,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: ClientEntertainment.</summary>
	public enum ClientEntertainmentFieldIndex
	{
		///<summary>ClientEntertainmentId. </summary>
		ClientEntertainmentId,
		///<summary>ClientId. </summary>
		ClientId,
		///<summary>EntertainmentId. </summary>
		EntertainmentId,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>ParentCompanyId. </summary>
		ParentCompanyId,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: ClientLog.</summary>
	public enum ClientLogFieldIndex
	{
		///<summary>ClientLogId. </summary>
		ClientLogId,
		///<summary>ClientId. </summary>
		ClientId,
		///<summary>Type. </summary>
		Type,
		///<summary>Message. </summary>
		Message,
		///<summary>FromStatus. </summary>
		FromStatus,
		///<summary>ToStatus. </summary>
		ToStatus,
		///<summary>FromOperationMode. </summary>
		FromOperationMode,
		///<summary>ToOperationMode. </summary>
		ToOperationMode,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>FromOperationModeText. </summary>
		FromOperationModeText,
		///<summary>FromStatusText. </summary>
		FromStatusText,
		///<summary>ToOperationModeText. </summary>
		ToOperationModeText,
		///<summary>ToStatusText. </summary>
		ToStatusText,
		///<summary>TypeText. </summary>
		TypeText,
		///<summary>ClientLogFileId. </summary>
		ClientLogFileId,
		///<summary>ParentCompanyId. </summary>
		ParentCompanyId,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: ClientLogFile.</summary>
	public enum ClientLogFileFieldIndex
	{
		///<summary>ClientLogFileId. </summary>
		ClientLogFileId,
		///<summary>Message. </summary>
		Message,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>LogDate. </summary>
		LogDate,
		///<summary>Application. </summary>
		Application,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: ClientState.</summary>
	public enum ClientStateFieldIndex
	{
		///<summary>ClientStateId. </summary>
		ClientStateId,
		///<summary>ClientId. </summary>
		ClientId,
		///<summary>Online. </summary>
		Online,
		///<summary>OperationMode. </summary>
		OperationMode,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>LastBatteryLevel. </summary>
		LastBatteryLevel,
		///<summary>LastIsCharging. </summary>
		LastIsCharging,
		///<summary>Message. </summary>
		Message,
		///<summary>TimeSpanTicks. </summary>
		TimeSpanTicks,
		///<summary>ParentCompanyId. </summary>
		ParentCompanyId,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: CloudApplicationVersion.</summary>
	public enum CloudApplicationVersionFieldIndex
	{
		///<summary>CloudApplicationVersionId. </summary>
		CloudApplicationVersionId,
		///<summary>Version. </summary>
		Version,
		///<summary>CloudApplication. </summary>
		CloudApplication,
		///<summary>MachineName. </summary>
		MachineName,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: CloudProcessingTask.</summary>
	public enum CloudProcessingTaskFieldIndex
	{
		///<summary>CloudProcessingTaskId. </summary>
		CloudProcessingTaskId,
		///<summary>Type. </summary>
		Type,
		///<summary>PathFormat. </summary>
		PathFormat,
		///<summary>Action. </summary>
		Action,
		///<summary>Errors. </summary>
		Errors,
		///<summary>Attempts. </summary>
		Attempts,
		///<summary>EntertainmentFileId. </summary>
		EntertainmentFileId,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>Container. </summary>
		Container,
		///<summary>EntertainmentId. </summary>
		EntertainmentId,
		///<summary>WeatherId. </summary>
		WeatherId,
		///<summary>ReleaseId. </summary>
		ReleaseId,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>LastAttemptUTC. </summary>
		LastAttemptUTC,
		///<summary>NextAttemptUTC. </summary>
		NextAttemptUTC,
		///<summary>CompletedOnAmazonUTC. </summary>
		CompletedOnAmazonUTC,
		///<summary>Priority. </summary>
		Priority,
		///<summary>MediaRatioTypeMediaIdNonRelationCopy. </summary>
		MediaRatioTypeMediaIdNonRelationCopy,
		///<summary>RelatedToCompanyId. </summary>
		RelatedToCompanyId,
		///<summary>MediaRatioTypeMediaId. </summary>
		MediaRatioTypeMediaId,
		///<summary>QueuedUTC. </summary>
		QueuedUTC,
		///<summary>FileContent. </summary>
		FileContent,
		///<summary>FileContentTimestamp. </summary>
		FileContentTimestamp,
		///<summary>FileContentTimestampKey. </summary>
		FileContentTimestampKey,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: CloudStorageAccount.</summary>
	public enum CloudStorageAccountFieldIndex
	{
		///<summary>CloudStorageAccountId. </summary>
		CloudStorageAccountId,
		///<summary>CompanyId. </summary>
		CompanyId,
		///<summary>Type. </summary>
		Type,
		///<summary>AccountName. </summary>
		AccountName,
		///<summary>AccountPassword. </summary>
		AccountPassword,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>UserName. </summary>
		UserName,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Company.</summary>
	public enum CompanyFieldIndex
	{
		///<summary>CompanyId. </summary>
		CompanyId,
		///<summary>CompanyOwnerId. </summary>
		CompanyOwnerId,
		///<summary>CurrencyId. </summary>
		CurrencyId,
		///<summary>LanguageId. </summary>
		LanguageId,
		///<summary>Name. </summary>
		Name,
		///<summary>NameWithoutDiacritics. </summary>
		NameWithoutDiacritics,
		///<summary>Code. </summary>
		Code,
		///<summary>Addressline1. </summary>
		Addressline1,
		///<summary>Addressline2. </summary>
		Addressline2,
		///<summary>Addressline3. </summary>
		Addressline3,
		///<summary>Zipcode. </summary>
		Zipcode,
		///<summary>City. </summary>
		City,
		///<summary>CountryId. </summary>
		CountryId,
		///<summary>Latitude. </summary>
		Latitude,
		///<summary>Longitude. </summary>
		Longitude,
		///<summary>Telephone. </summary>
		Telephone,
		///<summary>Fax. </summary>
		Fax,
		///<summary>Website. </summary>
		Website,
		///<summary>Email. </summary>
		Email,
		///<summary>StandardReceiptTypes. </summary>
		StandardReceiptTypes,
		///<summary>GoogleAnalyticsId. </summary>
		GoogleAnalyticsId,
		///<summary>FacebookPlaceId. </summary>
		FacebookPlaceId,
		///<summary>FoursquareVenueId. </summary>
		FoursquareVenueId,
		///<summary>ConfirmationCodeDeliveryType. </summary>
		ConfirmationCodeDeliveryType,
		///<summary>StandardCheckInterval. </summary>
		StandardCheckInterval,
		///<summary>SMSOrdering. </summary>
		SMSOrdering,
		///<summary>Description. </summary>
		Description,
		///<summary>AutomaticClientOperationModes. </summary>
		AutomaticClientOperationModes,
		///<summary>AvailableOnOtoucho. </summary>
		AvailableOnOtoucho,
		///<summary>AvailableOnObymobi. </summary>
		AvailableOnObymobi,
		///<summary>UseMonitoring. </summary>
		UseMonitoring,
		///<summary>SystemType. </summary>
		SystemType,
		///<summary>AllowRequestService. </summary>
		AllowRequestService,
		///<summary>AllowRateProduct. </summary>
		AllowRateProduct,
		///<summary>AllowRateService. </summary>
		AllowRateService,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>RouteId. </summary>
		RouteId,
		///<summary>SupportpoolId. </summary>
		SupportpoolId,
		///<summary>AllowFreeText. </summary>
		AllowFreeText,
		///<summary>UseBillButton. </summary>
		UseBillButton,
		///<summary>Salt. </summary>
		Salt,
		///<summary>MaxDownloadConnections. </summary>
		MaxDownloadConnections,
		///<summary>ClientApplicationVersion. </summary>
		ClientApplicationVersion,
		///<summary>OsVersion. </summary>
		OsVersion,
		///<summary>TerminalApplicationVersion. </summary>
		TerminalApplicationVersion,
		///<summary>DeviceRebootMethod. </summary>
		DeviceRebootMethod,
		///<summary>CorrespondenceEmail. </summary>
		CorrespondenceEmail,
		///<summary>ShowCurrencySymbol. </summary>
		ShowCurrencySymbol,
		///<summary>MobileOrderingDisabled. </summary>
		MobileOrderingDisabled,
		///<summary>GeoFencingEnabled. </summary>
		GeoFencingEnabled,
		///<summary>GeoFencingRadius. </summary>
		GeoFencingRadius,
		///<summary>Pincode. </summary>
		Pincode,
		///<summary>PincodeSU. </summary>
		PincodeSU,
		///<summary>PincodeGM. </summary>
		PincodeGM,
		///<summary>ActionButtonId. </summary>
		ActionButtonId,
		///<summary>CostIndicationType. </summary>
		CostIndicationType,
		///<summary>CostIndicationValue. </summary>
		CostIndicationValue,
		///<summary>DescriptionSingleLine. </summary>
		DescriptionSingleLine,
		///<summary>ActionButtonUrlMobile. </summary>
		ActionButtonUrlMobile,
		///<summary>ActionButtonUrlTablet. </summary>
		ActionButtonUrlTablet,
		///<summary>CometHandlerType. </summary>
		CometHandlerType,
		///<summary>AgentVersion. </summary>
		AgentVersion,
		///<summary>SupportToolsVersion. </summary>
		SupportToolsVersion,
		///<summary>PercentageDownForNotification. </summary>
		PercentageDownForNotification,
		///<summary>PercentageDownIntervalHours. </summary>
		PercentageDownIntervalHours,
		///<summary>LatitudeOverridden. </summary>
		LatitudeOverridden,
		///<summary>LongitudeOverriden. </summary>
		LongitudeOverriden,
		///<summary>SystemPassword. </summary>
		SystemPassword,
		///<summary>PercentageDownJumpForNotification. </summary>
		PercentageDownJumpForNotification,
		///<summary>TemperatureUnit. </summary>
		TemperatureUnit,
		///<summary>ClockMode. </summary>
		ClockMode,
		///<summary>WeatherClockWidgetMode. </summary>
		WeatherClockWidgetMode,
		///<summary>TimeZoneId. </summary>
		TimeZoneId,
		///<summary>SaltPms. </summary>
		SaltPms,
		///<summary>AlterationDialogMode. </summary>
		AlterationDialogMode,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>PercentageDownNotificationLastSentUTC. </summary>
		PercentageDownNotificationLastSentUTC,
		///<summary>PercentageDownJumpNotificationLastSentUTC. </summary>
		PercentageDownJumpNotificationLastSentUTC,
		///<summary>LastDailyReportSendUTC. </summary>
		LastDailyReportSendUTC,
		///<summary>CompanyDataLastModifiedUTC. </summary>
		CompanyDataLastModifiedUTC,
		///<summary>CompanyMediaLastModifiedUTC. </summary>
		CompanyMediaLastModifiedUTC,
		///<summary>MenuDataLastModifiedUTC. </summary>
		MenuDataLastModifiedUTC,
		///<summary>MenuMediaLastModifiedUTC. </summary>
		MenuMediaLastModifiedUTC,
		///<summary>PosIntegrationInfoLastModifiedUTC. </summary>
		PosIntegrationInfoLastModifiedUTC,
		///<summary>DeliverypointDataLastModifiedUTC. </summary>
		DeliverypointDataLastModifiedUTC,
		///<summary>SurveyDataLastModifiedUTC. </summary>
		SurveyDataLastModifiedUTC,
		///<summary>SurveyMediaLastModifiedUTC. </summary>
		SurveyMediaLastModifiedUTC,
		///<summary>AnnouncementDataLastModifiedUTC. </summary>
		AnnouncementDataLastModifiedUTC,
		///<summary>AnnouncementMediaLastModifiedUTC. </summary>
		AnnouncementMediaLastModifiedUTC,
		///<summary>EntertainmentDataLastModifiedUTC. </summary>
		EntertainmentDataLastModifiedUTC,
		///<summary>EntertainmentMediaLastModifiedUTC. </summary>
		EntertainmentMediaLastModifiedUTC,
		///<summary>AdvertisementDataLastModifiedUTC. </summary>
		AdvertisementDataLastModifiedUTC,
		///<summary>AdvertisementMediaLastModifiedUTC. </summary>
		AdvertisementMediaLastModifiedUTC,
		///<summary>PriceFormatType. </summary>
		PriceFormatType,
		///<summary>CompanygroupId. </summary>
		CompanygroupId,
		///<summary>CountryCode. </summary>
		CountryCode,
		///<summary>TimeZoneOlsonId. </summary>
		TimeZoneOlsonId,
		///<summary>CultureCode. </summary>
		CultureCode,
		///<summary>CurrencyCode. </summary>
		CurrencyCode,
		///<summary>IncludeDeliveryTimeInOrderNotes. </summary>
		IncludeDeliveryTimeInOrderNotes,
		///<summary>GoogleAnalyticsProfileId. </summary>
		GoogleAnalyticsProfileId,
		///<summary>IncludeMenuItemsNotExternallyLinked. </summary>
		IncludeMenuItemsNotExternallyLinked,
		///<summary>DailyReportSendTime. </summary>
		DailyReportSendTime,
		///<summary>OrganizationId. </summary>
		OrganizationId,
		///<summary>ApiVersion. </summary>
		ApiVersion,
		///<summary>MessagingVersion. </summary>
		MessagingVersion,
		///<summary>LinkDefaultRoomControlArea. </summary>
		LinkDefaultRoomControlArea,
		///<summary>PricesIncludeTaxes. </summary>
		PricesIncludeTaxes,
		///<summary>UnitSystem. </summary>
		UnitSystem,
		///<summary>ReleaseGroup. </summary>
		ReleaseGroup,
		///<summary>Notes. </summary>
		Notes,
		///<summary>HotSwappableContentEnabled. </summary>
		HotSwappableContentEnabled,
		///<summary>Archived. </summary>
		Archived,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: CompanyAmenity.</summary>
	public enum CompanyAmenityFieldIndex
	{
		///<summary>CompanyAmenityId. </summary>
		CompanyAmenityId,
		///<summary>CompanyId. </summary>
		CompanyId,
		///<summary>AmenityId. </summary>
		AmenityId,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: CompanyBrand.</summary>
	public enum CompanyBrandFieldIndex
	{
		///<summary>CompanyBrandId. </summary>
		CompanyBrandId,
		///<summary>CompanyId. </summary>
		CompanyId,
		///<summary>BrandId. </summary>
		BrandId,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>Priority. </summary>
		Priority,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: CompanyCulture.</summary>
	public enum CompanyCultureFieldIndex
	{
		///<summary>CompanyCultureId. </summary>
		CompanyCultureId,
		///<summary>CompanyId. </summary>
		CompanyId,
		///<summary>CultureCode. </summary>
		CultureCode,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: CompanyCurrency.</summary>
	public enum CompanyCurrencyFieldIndex
	{
		///<summary>CompanyCurrencyId. </summary>
		CompanyCurrencyId,
		///<summary>CompanyId. </summary>
		CompanyId,
		///<summary>CurrencyCode. </summary>
		CurrencyCode,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>ExchangeRate. </summary>
		ExchangeRate,
		///<summary>OverwriteSymbol. </summary>
		OverwriteSymbol,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: CompanyEntertainment.</summary>
	public enum CompanyEntertainmentFieldIndex
	{
		///<summary>CompanyEntertainmentId. </summary>
		CompanyEntertainmentId,
		///<summary>CompanyId. </summary>
		CompanyId,
		///<summary>EntertainmentId. </summary>
		EntertainmentId,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Companygroup.</summary>
	public enum CompanygroupFieldIndex
	{
		///<summary>CompanygroupId. </summary>
		CompanygroupId,
		///<summary>Name. </summary>
		Name,
		///<summary>GoogleAnalyticsId. </summary>
		GoogleAnalyticsId,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: CompanyLanguage.</summary>
	public enum CompanyLanguageFieldIndex
	{
		///<summary>CompanyLanguageId. </summary>
		CompanyLanguageId,
		///<summary>CompanyId. </summary>
		CompanyId,
		///<summary>LanguageId. </summary>
		LanguageId,
		///<summary>Description. </summary>
		Description,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>DescriptionSingleLine. </summary>
		DescriptionSingleLine,
		///<summary>VenuePageDescription. </summary>
		VenuePageDescription,
		///<summary>BrowserAgeVerificationTitle. </summary>
		BrowserAgeVerificationTitle,
		///<summary>BrowserAgeVerificationText. </summary>
		BrowserAgeVerificationText,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: CompanyManagementTask.</summary>
	public enum CompanyManagementTaskFieldIndex
	{
		///<summary>CompanyManagementTaskId. </summary>
		CompanyManagementTaskId,
		///<summary>CompanyId. </summary>
		CompanyId,
		///<summary>CreatedCompanyId. </summary>
		CreatedCompanyId,
		///<summary>PointOfInterestId. </summary>
		PointOfInterestId,
		///<summary>CreatedPointOfInterestId. </summary>
		CreatedPointOfInterestId,
		///<summary>Type. </summary>
		Type,
		///<summary>Status. </summary>
		Status,
		///<summary>Log. </summary>
		Log,
		///<summary>Progress. </summary>
		Progress,
		///<summary>StringValue1. </summary>
		StringValue1,
		///<summary>StringValue2. </summary>
		StringValue2,
		///<summary>StringValue3. </summary>
		StringValue3,
		///<summary>StringValue4. </summary>
		StringValue4,
		///<summary>StringValue5. </summary>
		StringValue5,
		///<summary>StringValue6. </summary>
		StringValue6,
		///<summary>Created. </summary>
		Created,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>Updated. </summary>
		Updated,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: CompanyOwner.</summary>
	public enum CompanyOwnerFieldIndex
	{
		///<summary>CompanyOwnerId. </summary>
		CompanyOwnerId,
		///<summary>Username. </summary>
		Username,
		///<summary>Password. </summary>
		Password,
		///<summary>UserId. </summary>
		UserId,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>IsPasswordEncrypted. </summary>
		IsPasswordEncrypted,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: CompanyRelease.</summary>
	public enum CompanyReleaseFieldIndex
	{
		///<summary>CompanyReleaseId. </summary>
		CompanyReleaseId,
		///<summary>CompanyId. </summary>
		CompanyId,
		///<summary>ReleaseId. </summary>
		ReleaseId,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>AutoUpdate. </summary>
		AutoUpdate,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: CompanyVenueCategory.</summary>
	public enum CompanyVenueCategoryFieldIndex
	{
		///<summary>CompanyVenueCategoryId. </summary>
		CompanyVenueCategoryId,
		///<summary>CompanyId. </summary>
		CompanyId,
		///<summary>VenueCategoryId. </summary>
		VenueCategoryId,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Configuration.</summary>
	public enum ConfigurationFieldIndex
	{
		///<summary>ConfigurationId. </summary>
		ConfigurationId,
		///<summary>Name. </summary>
		Name,
		///<summary>FriendlyName. </summary>
		FriendlyName,
		///<summary>Type. </summary>
		Type,
		///<summary>Value. </summary>
		Value,
		///<summary>Section. </summary>
		Section,
		///<summary>ApplicationName. </summary>
		ApplicationName,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Country.</summary>
	public enum CountryFieldIndex
	{
		///<summary>CountryId. </summary>
		CountryId,
		///<summary>Name. </summary>
		Name,
		///<summary>Code. </summary>
		Code,
		///<summary>CultureName. </summary>
		CultureName,
		///<summary>CurrencyId. </summary>
		CurrencyId,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>CodeAlpha3. </summary>
		CodeAlpha3,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Currency.</summary>
	public enum CurrencyFieldIndex
	{
		///<summary>CurrencyId. </summary>
		CurrencyId,
		///<summary>Name. </summary>
		Name,
		///<summary>Symbol. </summary>
		Symbol,
		///<summary>CodeIso4217. </summary>
		CodeIso4217,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Customer.</summary>
	public enum CustomerFieldIndex
	{
		///<summary>CustomerId. </summary>
		CustomerId,
		///<summary>Phonenumber. </summary>
		Phonenumber,
		///<summary>Password. </summary>
		Password,
		///<summary>Verified. </summary>
		Verified,
		///<summary>Blacklisted. </summary>
		Blacklisted,
		///<summary>BlacklistedNotes. </summary>
		BlacklistedNotes,
		///<summary>Firstname. </summary>
		Firstname,
		///<summary>Lastname. </summary>
		Lastname,
		///<summary>LastnamePrefix. </summary>
		LastnamePrefix,
		///<summary>RandomValue. </summary>
		RandomValue,
		///<summary>Birthdate. </summary>
		Birthdate,
		///<summary>Email. </summary>
		Email,
		///<summary>Gender. </summary>
		Gender,
		///<summary>Addressline1. </summary>
		Addressline1,
		///<summary>Addressline2. </summary>
		Addressline2,
		///<summary>Zipcode. </summary>
		Zipcode,
		///<summary>City. </summary>
		City,
		///<summary>Notes. </summary>
		Notes,
		///<summary>SmsRequestsSent. </summary>
		SmsRequestsSent,
		///<summary>SmsCreateRequests. </summary>
		SmsCreateRequests,
		///<summary>SmsResetPincodeRequests. </summary>
		SmsResetPincodeRequests,
		///<summary>SmsUnlockAccountRequests. </summary>
		SmsUnlockAccountRequests,
		///<summary>SmsNonProcessableRequests. </summary>
		SmsNonProcessableRequests,
		///<summary>HasChangedInititialPincode. </summary>
		HasChangedInititialPincode,
		///<summary>InitialLinkIdentifier. </summary>
		InitialLinkIdentifier,
		///<summary>PasswordResetLinkIdentifier. </summary>
		PasswordResetLinkIdentifier,
		///<summary>FailedPasswordAttemptCount. </summary>
		FailedPasswordAttemptCount,
		///<summary>RememberOnMobileWebsite. </summary>
		RememberOnMobileWebsite,
		///<summary>ClientId. </summary>
		ClientId,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>CommunicationSalt. </summary>
		CommunicationSalt,
		///<summary>AnonymousAccount. </summary>
		AnonymousAccount,
		///<summary>PasswordSalt. </summary>
		PasswordSalt,
		///<summary>PasswordResetLinkFailedAtttempts. </summary>
		PasswordResetLinkFailedAtttempts,
		///<summary>CountryCode. </summary>
		CountryCode,
		///<summary>CanSingleSignOn. </summary>
		CanSingleSignOn,
		///<summary>GUID. </summary>
		GUID,
		///<summary>NewEmailAddress. </summary>
		NewEmailAddress,
		///<summary>NewEmailAddressConfirmationGuid. </summary>
		NewEmailAddressConfirmationGuid,
		///<summary>NewEmailAddressVerified. </summary>
		NewEmailAddressVerified,
		///<summary>CampaignContent. </summary>
		CampaignContent,
		///<summary>CampaignMedium. </summary>
		CampaignMedium,
		///<summary>CampaignName. </summary>
		CampaignName,
		///<summary>CampaignSource. </summary>
		CampaignSource,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>PasswordResetLinkGeneratedUTC. </summary>
		PasswordResetLinkGeneratedUTC,
		///<summary>LastFailedPasswordAttemptUTC. </summary>
		LastFailedPasswordAttemptUTC,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: CustomText.</summary>
	public enum CustomTextFieldIndex
	{
		///<summary>CustomTextId. </summary>
		CustomTextId,
		///<summary>CompanyId. </summary>
		CompanyId,
		///<summary>LanguageId. </summary>
		LanguageId,
		///<summary>Text. </summary>
		Text,
		///<summary>DeliverypointgroupId. </summary>
		DeliverypointgroupId,
		///<summary>CategoryId. </summary>
		CategoryId,
		///<summary>ProductId. </summary>
		ProductId,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>Type. </summary>
		Type,
		///<summary>AdvertisementId. </summary>
		AdvertisementId,
		///<summary>AlterationId. </summary>
		AlterationId,
		///<summary>AlterationoptionId. </summary>
		AlterationoptionId,
		///<summary>AnnouncementId. </summary>
		AnnouncementId,
		///<summary>ActionButtonId. </summary>
		ActionButtonId,
		///<summary>PointOfInterestId. </summary>
		PointOfInterestId,
		///<summary>EntertainmentcategoryId. </summary>
		EntertainmentcategoryId,
		///<summary>GenericproductId. </summary>
		GenericproductId,
		///<summary>PageId. </summary>
		PageId,
		///<summary>SiteId. </summary>
		SiteId,
		///<summary>AmenityId. </summary>
		AmenityId,
		///<summary>AttachmentId. </summary>
		AttachmentId,
		///<summary>PageTemplateId. </summary>
		PageTemplateId,
		///<summary>GenericcategoryId. </summary>
		GenericcategoryId,
		///<summary>RoomControlAreaId. </summary>
		RoomControlAreaId,
		///<summary>VenueCategoryId. </summary>
		VenueCategoryId,
		///<summary>RoomControlComponentId. </summary>
		RoomControlComponentId,
		///<summary>RoomControlSectionId. </summary>
		RoomControlSectionId,
		///<summary>RoomControlSectionItemId. </summary>
		RoomControlSectionItemId,
		///<summary>RoomControlWidgetId. </summary>
		RoomControlWidgetId,
		///<summary>ScheduledMessageId. </summary>
		ScheduledMessageId,
		///<summary>SiteTemplateId. </summary>
		SiteTemplateId,
		///<summary>StationId. </summary>
		StationId,
		///<summary>UIFooterItemId. </summary>
		UIFooterItemId,
		///<summary>UITabId. </summary>
		UITabId,
		///<summary>UIWidgetId. </summary>
		UIWidgetId,
		///<summary>AvailabilityId. </summary>
		AvailabilityId,
		///<summary>CultureCode. </summary>
		CultureCode,
		///<summary>ParentCompanyId. </summary>
		ParentCompanyId,
		///<summary>ClientConfigurationId. </summary>
		ClientConfigurationId,
		///<summary>ProductgroupId. </summary>
		ProductgroupId,
		///<summary>RoutestephandlerId. </summary>
		RoutestephandlerId,
		///<summary>InfraredCommandId. </summary>
		InfraredCommandId,
		///<summary>ApplicationConfigurationId. </summary>
		ApplicationConfigurationId,
		///<summary>NavigationMenuItemId. </summary>
		NavigationMenuItemId,
		///<summary>CarouselItemId. </summary>
		CarouselItemId,
		///<summary>WidgetId. </summary>
		WidgetId,
		///<summary>LandingPageId. </summary>
		LandingPageId,
		///<summary>OutletId. </summary>
		OutletId,
		///<summary>ServiceMethodId. </summary>
		ServiceMethodId,
		///<summary>CheckoutMethodId. </summary>
		CheckoutMethodId,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: DeliveryDistance.</summary>
	public enum DeliveryDistanceFieldIndex
	{
		///<summary>DeliveryDistanceId. </summary>
		DeliveryDistanceId,
		///<summary>ServiceMethodId. </summary>
		ServiceMethodId,
		///<summary>MaximumInMetres. </summary>
		MaximumInMetres,
		///<summary>Price. </summary>
		Price,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: DeliveryInformation.</summary>
	public enum DeliveryInformationFieldIndex
	{
		///<summary>DeliveryInformationId. </summary>
		DeliveryInformationId,
		///<summary>BuildingName. </summary>
		BuildingName,
		///<summary>Address. </summary>
		Address,
		///<summary>Zipcode. </summary>
		Zipcode,
		///<summary>City. </summary>
		City,
		///<summary>Instructions. </summary>
		Instructions,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>Latitude. </summary>
		Latitude,
		///<summary>Longitude. </summary>
		Longitude,
		///<summary>DistanceToOutletInMetres. </summary>
		DistanceToOutletInMetres,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Deliverypoint.</summary>
	public enum DeliverypointFieldIndex
	{
		///<summary>DeliverypointId. </summary>
		DeliverypointId,
		///<summary>DeliverypointgroupId. </summary>
		DeliverypointgroupId,
		///<summary>Name. </summary>
		Name,
		///<summary>Number. </summary>
		Number,
		///<summary>PosdeliverypointId. </summary>
		PosdeliverypointId,
		///<summary>CompanyId. </summary>
		CompanyId,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>RoomControllerIp. </summary>
		RoomControllerIp,
		///<summary>GooglePrinterId. </summary>
		GooglePrinterId,
		///<summary>GooglePrinterName. </summary>
		GooglePrinterName,
		///<summary>HotSOSRoomId. </summary>
		HotSOSRoomId,
		///<summary>DeviceId. </summary>
		DeviceId,
		///<summary>EnableAnalytics. </summary>
		EnableAnalytics,
		///<summary>RoomControlConfigurationId. </summary>
		RoomControlConfigurationId,
		///<summary>RoomControllerSlaveId. </summary>
		RoomControllerSlaveId,
		///<summary>RoomControllerType. </summary>
		RoomControllerType,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>ClientConfigurationId. </summary>
		ClientConfigurationId,
		///<summary>RoomControlAreaId. </summary>
		RoomControlAreaId,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: DeliverypointExternalDeliverypoint.</summary>
	public enum DeliverypointExternalDeliverypointFieldIndex
	{
		///<summary>DeliverypointExternalDeliverypointId. </summary>
		DeliverypointExternalDeliverypointId,
		///<summary>DeliverypointId. </summary>
		DeliverypointId,
		///<summary>ExternalDeliverypointId. </summary>
		ExternalDeliverypointId,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>ParentCompanyId. </summary>
		ParentCompanyId,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Deliverypointgroup.</summary>
	public enum DeliverypointgroupFieldIndex
	{
		///<summary>DeliverypointgroupId. </summary>
		DeliverypointgroupId,
		///<summary>Name. </summary>
		Name,
		///<summary>ConfirmationCodeDeliveryType. </summary>
		ConfirmationCodeDeliveryType,
		///<summary>Active. </summary>
		Active,
		///<summary>UseAgeCheckInOtoucho. </summary>
		UseAgeCheckInOtoucho,
		///<summary>UseManualDeliverypointsEntryInOtoucho. </summary>
		UseManualDeliverypointsEntryInOtoucho,
		///<summary>UseStatelessInOtoucho. </summary>
		UseStatelessInOtoucho,
		///<summary>SkipOrderOverviewAfterOrderSubmitInOtoucho. </summary>
		SkipOrderOverviewAfterOrderSubmitInOtoucho,
		///<summary>WifiSsid. </summary>
		WifiSsid,
		///<summary>WifiPassword. </summary>
		WifiPassword,
		///<summary>Locale. </summary>
		Locale,
		///<summary>Pincode. </summary>
		Pincode,
		///<summary>UseManualDeliverypoint. </summary>
		UseManualDeliverypoint,
		///<summary>UseManualDeliverypointEncryption. </summary>
		UseManualDeliverypointEncryption,
		///<summary>AddDeliverypointToOrder. </summary>
		AddDeliverypointToOrder,
		///<summary>EncryptionSalt. </summary>
		EncryptionSalt,
		///<summary>HideDeliverypointNumber. </summary>
		HideDeliverypointNumber,
		///<summary>HidePrices. </summary>
		HidePrices,
		///<summary>ClearSessionOnTimeout. </summary>
		ClearSessionOnTimeout,
		///<summary>ResetTimeout. </summary>
		ResetTimeout,
		///<summary>ReorderNotificationEnabled. </summary>
		ReorderNotificationEnabled,
		///<summary>ReorderNotificationInterval. </summary>
		ReorderNotificationInterval,
		///<summary>ReorderNotificationAnnouncementId. </summary>
		ReorderNotificationAnnouncementId,
		///<summary>ScreenTimeoutInterval. </summary>
		ScreenTimeoutInterval,
		///<summary>DeviceActivationRequired. </summary>
		DeviceActivationRequired,
		///<summary>DefaultProductFullView. </summary>
		DefaultProductFullView,
		///<summary>MarketingSurveyUrl. </summary>
		MarketingSurveyUrl,
		///<summary>HotelUrl1. </summary>
		HotelUrl1,
		///<summary>HotelUrl1Caption. </summary>
		HotelUrl1Caption,
		///<summary>HotelUrl1Zoom. </summary>
		HotelUrl1Zoom,
		///<summary>HotelUrl2. </summary>
		HotelUrl2,
		///<summary>HotelUrl2Caption. </summary>
		HotelUrl2Caption,
		///<summary>HotelUrl2Zoom. </summary>
		HotelUrl2Zoom,
		///<summary>HotelUrl3. </summary>
		HotelUrl3,
		///<summary>HotelUrl3Caption. </summary>
		HotelUrl3Caption,
		///<summary>HotelUrl3Zoom. </summary>
		HotelUrl3Zoom,
		///<summary>DimLevelDull. </summary>
		DimLevelDull,
		///<summary>DimLevelMedium. </summary>
		DimLevelMedium,
		///<summary>DimLevelBright. </summary>
		DimLevelBright,
		///<summary>PowerSaveTimeout. </summary>
		PowerSaveTimeout,
		///<summary>PowerSaveLevel. </summary>
		PowerSaveLevel,
		///<summary>OutOfChargeLevel. </summary>
		OutOfChargeLevel,
		///<summary>OutOfChargeTitle. </summary>
		OutOfChargeTitle,
		///<summary>OutOfChargeMessage. </summary>
		OutOfChargeMessage,
		///<summary>OrderProcessedTitle. </summary>
		OrderProcessedTitle,
		///<summary>OrderProcessedMessage. </summary>
		OrderProcessedMessage,
		///<summary>OrderFailedTitle. </summary>
		OrderFailedTitle,
		///<summary>OrderFailedMessage. </summary>
		OrderFailedMessage,
		///<summary>OrderCompletedTitle. </summary>
		OrderCompletedTitle,
		///<summary>OrderCompletedMessage. </summary>
		OrderCompletedMessage,
		///<summary>OrderCompletedEnabled. </summary>
		OrderCompletedEnabled,
		///<summary>OrderSavingTitle. </summary>
		OrderSavingTitle,
		///<summary>OrderSavingMessage. </summary>
		OrderSavingMessage,
		///<summary>ServiceSavingTitle. </summary>
		ServiceSavingTitle,
		///<summary>ServiceSavingMessage. </summary>
		ServiceSavingMessage,
		///<summary>ServiceProcessedTitle. </summary>
		ServiceProcessedTitle,
		///<summary>ServiceProcessedMessage. </summary>
		ServiceProcessedMessage,
		///<summary>RatingSavingTitle. </summary>
		RatingSavingTitle,
		///<summary>RatingSavingMessage. </summary>
		RatingSavingMessage,
		///<summary>RatingProcessedTitle. </summary>
		RatingProcessedTitle,
		///<summary>RatingProcessedMessage. </summary>
		RatingProcessedMessage,
		///<summary>DailyOrderReset. </summary>
		DailyOrderReset,
		///<summary>SleepTime. </summary>
		SleepTime,
		///<summary>WakeUpTime. </summary>
		WakeUpTime,
		///<summary>OrderProcessingNotificationEnabled. </summary>
		OrderProcessingNotificationEnabled,
		///<summary>OrderProcessingNotificationTitle. </summary>
		OrderProcessingNotificationTitle,
		///<summary>OrderProcessingNotification. </summary>
		OrderProcessingNotification,
		///<summary>OrderProcessedNotificationEnabled. </summary>
		OrderProcessedNotificationEnabled,
		///<summary>OrderProcessedNotificationTitle. </summary>
		OrderProcessedNotificationTitle,
		///<summary>OrderProcessedNotification. </summary>
		OrderProcessedNotification,
		///<summary>FreeformMessageTitle. </summary>
		FreeformMessageTitle,
		///<summary>ServiceProcessingNotificationTitle. </summary>
		ServiceProcessingNotificationTitle,
		///<summary>ServiceProcessingNotification. </summary>
		ServiceProcessingNotification,
		///<summary>ServiceProcessedNotificationTitle. </summary>
		ServiceProcessedNotificationTitle,
		///<summary>ServiceProcessedNotification. </summary>
		ServiceProcessedNotification,
		///<summary>PreorderingEnabled. </summary>
		PreorderingEnabled,
		///<summary>PreorderingActiveMinutes. </summary>
		PreorderingActiveMinutes,
		///<summary>AnalyticsOrderingVisible. </summary>
		AnalyticsOrderingVisible,
		///<summary>AnalyticsBestsellersVisible. </summary>
		AnalyticsBestsellersVisible,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>PincodeSU. </summary>
		PincodeSU,
		///<summary>PincodeGM. </summary>
		PincodeGM,
		///<summary>DockedDimLevelDull. </summary>
		DockedDimLevelDull,
		///<summary>DockedDimLevelMedium. </summary>
		DockedDimLevelMedium,
		///<summary>DockedDimLevelBright. </summary>
		DockedDimLevelBright,
		///<summary>DeliverypointCaption. </summary>
		DeliverypointCaption,
		///<summary>OrderingEnabled. </summary>
		OrderingEnabled,
		///<summary>HomepageSlideshowInterval. </summary>
		HomepageSlideshowInterval,
		///<summary>OrderHistoryDialogEnabled. </summary>
		OrderHistoryDialogEnabled,
		///<summary>OrderingNotAvailableMessage. </summary>
		OrderingNotAvailableMessage,
		///<summary>GooglePrinterId. </summary>
		GooglePrinterId,
		///<summary>PmsIntegration. </summary>
		PmsIntegration,
		///<summary>PmsAllowShowBill. </summary>
		PmsAllowShowBill,
		///<summary>PmsAllowExpressCheckout. </summary>
		PmsAllowExpressCheckout,
		///<summary>PmsLockClientWhenNotCheckedIn. </summary>
		PmsLockClientWhenNotCheckedIn,
		///<summary>WifiAnonymousIdentify. </summary>
		WifiAnonymousIdentify,
		///<summary>WifiEapMode. </summary>
		WifiEapMode,
		///<summary>WifiIdentity. </summary>
		WifiIdentity,
		///<summary>WifiPhase2Authentication. </summary>
		WifiPhase2Authentication,
		///<summary>WifiSecurityMethod. </summary>
		WifiSecurityMethod,
		///<summary>CompanyId. </summary>
		CompanyId,
		///<summary>MenuId. </summary>
		MenuId,
		///<summary>PosdeliverypointgroupId. </summary>
		PosdeliverypointgroupId,
		///<summary>RouteId. </summary>
		RouteId,
		///<summary>SystemMessageRouteId. </summary>
		SystemMessageRouteId,
		///<summary>XTerminalId. </summary>
		XTerminalId,
		///<summary>UIModeId. </summary>
		UIModeId,
		///<summary>OutOfChargeNotificationAmount. </summary>
		OutOfChargeNotificationAmount,
		///<summary>IsChargerRemovedDialogEnabled. </summary>
		IsChargerRemovedDialogEnabled,
		///<summary>ChargerRemovedDialogTitle. </summary>
		ChargerRemovedDialogTitle,
		///<summary>ChargerRemovedDialogText. </summary>
		ChargerRemovedDialogText,
		///<summary>IsChargerRemovedReminderDialogEnabled. </summary>
		IsChargerRemovedReminderDialogEnabled,
		///<summary>ChargerRemovedReminderDialogTitle. </summary>
		ChargerRemovedReminderDialogTitle,
		///<summary>ChargerRemovedReminderDialogText. </summary>
		ChargerRemovedReminderDialogText,
		///<summary>PmsDeviceLockedTitle. </summary>
		PmsDeviceLockedTitle,
		///<summary>PmsDeviceLockedText. </summary>
		PmsDeviceLockedText,
		///<summary>PmsCheckinFailedTitle. </summary>
		PmsCheckinFailedTitle,
		///<summary>PmsCheckinFailedText. </summary>
		PmsCheckinFailedText,
		///<summary>PmsRestartTitle. </summary>
		PmsRestartTitle,
		///<summary>PmsRestartText. </summary>
		PmsRestartText,
		///<summary>PmsWelcomeTitle. </summary>
		PmsWelcomeTitle,
		///<summary>PmsWelcomeText. </summary>
		PmsWelcomeText,
		///<summary>PmsCheckoutApproveText. </summary>
		PmsCheckoutApproveText,
		///<summary>PmsWelcomeTimeoutMinutes. </summary>
		PmsWelcomeTimeoutMinutes,
		///<summary>PmsCheckoutCompleteTitle. </summary>
		PmsCheckoutCompleteTitle,
		///<summary>PmsCheckoutCompleteText. </summary>
		PmsCheckoutCompleteText,
		///<summary>PmsCheckoutCompleteTimeoutMinutes. </summary>
		PmsCheckoutCompleteTimeoutMinutes,
		///<summary>AvailableOnObymobi. </summary>
		AvailableOnObymobi,
		///<summary>MobileUIModeId. </summary>
		MobileUIModeId,
		///<summary>TabletUIModeId. </summary>
		TabletUIModeId,
		///<summary>PowerButtonHardBehaviour. </summary>
		PowerButtonHardBehaviour,
		///<summary>PowerButtonSoftBehaviour. </summary>
		PowerButtonSoftBehaviour,
		///<summary>ScreenOffMode. </summary>
		ScreenOffMode,
		///<summary>UseHardKeyboard. </summary>
		UseHardKeyboard,
		///<summary>GooglePrinterName. </summary>
		GooglePrinterName,
		///<summary>RoomserviceCharge. </summary>
		RoomserviceCharge,
		///<summary>HotSOSBatteryLowProductId. </summary>
		HotSOSBatteryLowProductId,
		///<summary>EmailDocumentRouteId. </summary>
		EmailDocumentRouteId,
		///<summary>EnableBatteryLowConsoleNotifications. </summary>
		EnableBatteryLowConsoleNotifications,
		///<summary>UIThemeId. </summary>
		UIThemeId,
		///<summary>ScreensaverMode. </summary>
		ScreensaverMode,
		///<summary>PmsAllowShowGuestName. </summary>
		PmsAllowShowGuestName,
		///<summary>TurnOffPrivacyTitle. </summary>
		TurnOffPrivacyTitle,
		///<summary>TurnOffPrivacyText. </summary>
		TurnOffPrivacyText,
		///<summary>EstimatedDeliveryTime. </summary>
		EstimatedDeliveryTime,
		///<summary>OrderNotesRouteId. </summary>
		OrderNotesRouteId,
		///<summary>UIScheduleId. </summary>
		UIScheduleId,
		///<summary>ItemCurrentlyUnavailableText. </summary>
		ItemCurrentlyUnavailableText,
		///<summary>ServiceFailedTitle. </summary>
		ServiceFailedTitle,
		///<summary>ServiceFailedMessage. </summary>
		ServiceFailedMessage,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>AlarmSetWhileNotChargingTitle. </summary>
		AlarmSetWhileNotChargingTitle,
		///<summary>AlarmSetWhileNotChargingText. </summary>
		AlarmSetWhileNotChargingText,
		///<summary>PriceScheduleId. </summary>
		PriceScheduleId,
		///<summary>ClientConfigurationId. </summary>
		ClientConfigurationId,
		///<summary>IsOrderitemAddedDialogEnabled. </summary>
		IsOrderitemAddedDialogEnabled,
		///<summary>BrowserAgeVerificationEnabled. </summary>
		BrowserAgeVerificationEnabled,
		///<summary>BrowserAgeVerificationLayout. </summary>
		BrowserAgeVerificationLayout,
		///<summary>RestartApplicationDialogEnabled. </summary>
		RestartApplicationDialogEnabled,
		///<summary>RebootDeviceDialogEnabled. </summary>
		RebootDeviceDialogEnabled,
		///<summary>RestartTimeoutSeconds. </summary>
		RestartTimeoutSeconds,
		///<summary>CraveAnalytics. </summary>
		CraveAnalytics,
		///<summary>GeoFencingEnabled. </summary>
		GeoFencingEnabled,
		///<summary>GeoFencingRadius. </summary>
		GeoFencingRadius,
		///<summary>HideCompanyDetails. </summary>
		HideCompanyDetails,
		///<summary>IsClearBasketDialogEnabled. </summary>
		IsClearBasketDialogEnabled,
		///<summary>ClearBasketTitle. </summary>
		ClearBasketTitle,
		///<summary>ClearBasketText. </summary>
		ClearBasketText,
		///<summary>ApiVersion. </summary>
		ApiVersion,
		///<summary>MessagingVersion. </summary>
		MessagingVersion,
		///<summary>AddressId. </summary>
		AddressId,
		///<summary>AffiliateCampaignId. </summary>
		AffiliateCampaignId,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: DeliverypointgroupAdvertisement.</summary>
	public enum DeliverypointgroupAdvertisementFieldIndex
	{
		///<summary>DeliverypointgroupAdvertisementId. </summary>
		DeliverypointgroupAdvertisementId,
		///<summary>DeliverypointgroupId. </summary>
		DeliverypointgroupId,
		///<summary>AdvertisementId. </summary>
		AdvertisementId,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>ParentCompanyId. </summary>
		ParentCompanyId,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: DeliverypointgroupAnnouncement.</summary>
	public enum DeliverypointgroupAnnouncementFieldIndex
	{
		///<summary>DeliverypointAnnouncementId. </summary>
		DeliverypointAnnouncementId,
		///<summary>DeliverypointgroupId. </summary>
		DeliverypointgroupId,
		///<summary>AnnouncementId. </summary>
		AnnouncementId,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>ParentCompanyId. </summary>
		ParentCompanyId,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: DeliverypointgroupEntertainment.</summary>
	public enum DeliverypointgroupEntertainmentFieldIndex
	{
		///<summary>DeliverypointgroupEntertainmentId. </summary>
		DeliverypointgroupEntertainmentId,
		///<summary>DeliverypointgroupId. </summary>
		DeliverypointgroupId,
		///<summary>EntertainmentId. </summary>
		EntertainmentId,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>SortOrder. </summary>
		SortOrder,
		///<summary>ParentCompanyId. </summary>
		ParentCompanyId,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: DeliverypointgroupLanguage.</summary>
	public enum DeliverypointgroupLanguageFieldIndex
	{
		///<summary>DeliverypointgroupLanguageId. </summary>
		DeliverypointgroupLanguageId,
		///<summary>DeliverypointgroupId. </summary>
		DeliverypointgroupId,
		///<summary>LanguageId. </summary>
		LanguageId,
		///<summary>OutOfChargeTitle. </summary>
		OutOfChargeTitle,
		///<summary>OutOfChargeMessage. </summary>
		OutOfChargeMessage,
		///<summary>OrderProcessedTitle. </summary>
		OrderProcessedTitle,
		///<summary>OrderProcessedMessage. </summary>
		OrderProcessedMessage,
		///<summary>OrderFailedTitle. </summary>
		OrderFailedTitle,
		///<summary>OrderFailedMessage. </summary>
		OrderFailedMessage,
		///<summary>OrderCompletedTitle. </summary>
		OrderCompletedTitle,
		///<summary>OrderCompletedMessage. </summary>
		OrderCompletedMessage,
		///<summary>OrderSavingTitle. </summary>
		OrderSavingTitle,
		///<summary>OrderSavingMessage. </summary>
		OrderSavingMessage,
		///<summary>ServiceSavingTitle. </summary>
		ServiceSavingTitle,
		///<summary>ServiceSavingMessage. </summary>
		ServiceSavingMessage,
		///<summary>ServiceProcessedTitle. </summary>
		ServiceProcessedTitle,
		///<summary>ServiceProcessedMessage. </summary>
		ServiceProcessedMessage,
		///<summary>RatingSavingTitle. </summary>
		RatingSavingTitle,
		///<summary>RatingSavingMessage. </summary>
		RatingSavingMessage,
		///<summary>RatingProcessedTitle. </summary>
		RatingProcessedTitle,
		///<summary>RatingProcessedMessage. </summary>
		RatingProcessedMessage,
		///<summary>HotelUrl1. </summary>
		HotelUrl1,
		///<summary>HotelUrl1Caption. </summary>
		HotelUrl1Caption,
		///<summary>HotelUrl1Zoom. </summary>
		HotelUrl1Zoom,
		///<summary>HotelUrl2. </summary>
		HotelUrl2,
		///<summary>HotelUrl2Caption. </summary>
		HotelUrl2Caption,
		///<summary>HotelUrl2Zoom. </summary>
		HotelUrl2Zoom,
		///<summary>HotelUrl3. </summary>
		HotelUrl3,
		///<summary>HotelUrl3Caption. </summary>
		HotelUrl3Caption,
		///<summary>HotelUrl3Zoom. </summary>
		HotelUrl3Zoom,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>DeliverypointCaption. </summary>
		DeliverypointCaption,
		///<summary>PmsCheckinFailedText. </summary>
		PmsCheckinFailedText,
		///<summary>PmsCheckinFailedTitle. </summary>
		PmsCheckinFailedTitle,
		///<summary>PmsCheckoutApproveText. </summary>
		PmsCheckoutApproveText,
		///<summary>PmsDeviceLockedText. </summary>
		PmsDeviceLockedText,
		///<summary>PmsDeviceLockedTitle. </summary>
		PmsDeviceLockedTitle,
		///<summary>PmsRestartText. </summary>
		PmsRestartText,
		///<summary>PmsRestartTitle. </summary>
		PmsRestartTitle,
		///<summary>PmsWelcomeText. </summary>
		PmsWelcomeText,
		///<summary>PmsWelcomeTitle. </summary>
		PmsWelcomeTitle,
		///<summary>PmsCheckoutCompleteText. </summary>
		PmsCheckoutCompleteText,
		///<summary>PmsCheckoutCompleteTitle. </summary>
		PmsCheckoutCompleteTitle,
		///<summary>Name. </summary>
		Name,
		///<summary>RoomserviceChargeText. </summary>
		RoomserviceChargeText,
		///<summary>SuggestionsCaption. </summary>
		SuggestionsCaption,
		///<summary>PrintingConfirmationTitle. </summary>
		PrintingConfirmationTitle,
		///<summary>PrintingConfirmationText. </summary>
		PrintingConfirmationText,
		///<summary>PagesCaption. </summary>
		PagesCaption,
		///<summary>PrintingSucceededTitle. </summary>
		PrintingSucceededTitle,
		///<summary>PrintingSucceededText. </summary>
		PrintingSucceededText,
		///<summary>ParentCompanyId. </summary>
		ParentCompanyId,
		///<summary>ChargerRemovedDialogTitle. </summary>
		ChargerRemovedDialogTitle,
		///<summary>ChargerRemovedDialogText. </summary>
		ChargerRemovedDialogText,
		///<summary>ChargerRemovedReminderDialogTitle. </summary>
		ChargerRemovedReminderDialogTitle,
		///<summary>ChargerRemovedReminderDialogText. </summary>
		ChargerRemovedReminderDialogText,
		///<summary>TurnOffPrivacyTitle. </summary>
		TurnOffPrivacyTitle,
		///<summary>TurnOffPrivacyText. </summary>
		TurnOffPrivacyText,
		///<summary>ItemCurrentlyUnavailableText. </summary>
		ItemCurrentlyUnavailableText,
		///<summary>ServiceFailedTitle. </summary>
		ServiceFailedTitle,
		///<summary>ServiceFailedMessage. </summary>
		ServiceFailedMessage,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>AlarmSetWhileNotChargingTitle. </summary>
		AlarmSetWhileNotChargingTitle,
		///<summary>AlarmSetWhileNotChargingText. </summary>
		AlarmSetWhileNotChargingText,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: DeliverypointgroupOccupancy.</summary>
	public enum DeliverypointgroupOccupancyFieldIndex
	{
		///<summary>DeliverypointgroupOccupancyId. </summary>
		DeliverypointgroupOccupancyId,
		///<summary>DeliverypointgroupId. </summary>
		DeliverypointgroupId,
		///<summary>Date. </summary>
		Date,
		///<summary>Amount. </summary>
		Amount,
		///<summary>ParentCompanyId. </summary>
		ParentCompanyId,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: DeliverypointgroupProduct.</summary>
	public enum DeliverypointgroupProductFieldIndex
	{
		///<summary>DeliverypointgroupProductId. </summary>
		DeliverypointgroupProductId,
		///<summary>DeliverypointgroupId. </summary>
		DeliverypointgroupId,
		///<summary>ProductId. </summary>
		ProductId,
		///<summary>SortOrder. </summary>
		SortOrder,
		///<summary>Type. </summary>
		Type,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>ParentCompanyId. </summary>
		ParentCompanyId,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Device.</summary>
	public enum DeviceFieldIndex
	{
		///<summary>DeviceId. </summary>
		DeviceId,
		///<summary>Identifier. </summary>
		Identifier,
		///<summary>Type. </summary>
		Type,
		///<summary>Notes. </summary>
		Notes,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>UpdateStatus. </summary>
		UpdateStatus,
		///<summary>CustomerId. </summary>
		CustomerId,
		///<summary>DevicePlatform. </summary>
		DevicePlatform,
		///<summary>IsDevelopment. </summary>
		IsDevelopment,
		///<summary>DeviceModel. </summary>
		DeviceModel,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>UpdateStatusChangedUTC. </summary>
		UpdateStatusChangedUTC,
		///<summary>LastRequestUTC. </summary>
		LastRequestUTC,
		///<summary>LastRequestNotifiedBySmsUTC. </summary>
		LastRequestNotifiedBySmsUTC,
		///<summary>LastSupportToolsRequestUTC. </summary>
		LastSupportToolsRequestUTC,
		///<summary>IsCharging. </summary>
		IsCharging,
		///<summary>BatteryLevel. </summary>
		BatteryLevel,
		///<summary>PublicIpAddress. </summary>
		PublicIpAddress,
		///<summary>PrivateIpAddresses. </summary>
		PrivateIpAddresses,
		///<summary>ApplicationVersion. </summary>
		ApplicationVersion,
		///<summary>AgentRunning. </summary>
		AgentRunning,
		///<summary>AgentVersion. </summary>
		AgentVersion,
		///<summary>SupportToolsRunning. </summary>
		SupportToolsRunning,
		///<summary>SupportToolsVersion. </summary>
		SupportToolsVersion,
		///<summary>OsVersion. </summary>
		OsVersion,
		///<summary>WifiStrength. </summary>
		WifiStrength,
		///<summary>DeviceInfoUpdatedUTC. </summary>
		DeviceInfoUpdatedUTC,
		///<summary>UpdateEmenuDownloaded. </summary>
		UpdateEmenuDownloaded,
		///<summary>UpdateAgentDownloaded. </summary>
		UpdateAgentDownloaded,
		///<summary>UpdateSupportToolsDownloaded. </summary>
		UpdateSupportToolsDownloaded,
		///<summary>UpdateOSDownloaded. </summary>
		UpdateOSDownloaded,
		///<summary>CloudEnvironment. </summary>
		CloudEnvironment,
		///<summary>CommunicationMethod. </summary>
		CommunicationMethod,
		///<summary>UpdateConsoleDownloaded. </summary>
		UpdateConsoleDownloaded,
		///<summary>LastUserInteraction. </summary>
		LastUserInteraction,
		///<summary>Token. </summary>
		Token,
		///<summary>SandboxMode. </summary>
		SandboxMode,
		///<summary>MessagingServiceRunning. </summary>
		MessagingServiceRunning,
		///<summary>MessagingServiceVersion. </summary>
		MessagingServiceVersion,
		///<summary>LastMessagingServiceRequest. </summary>
		LastMessagingServiceRequest,
		///<summary>LastMessagingServiceConnectionType. </summary>
		LastMessagingServiceConnectionType,
		///<summary>LastMessagingServiceConnectionStatus. </summary>
		LastMessagingServiceConnectionStatus,
		///<summary>LastMessagingServiceSuccessfulConnection. </summary>
		LastMessagingServiceSuccessfulConnection,
		///<summary>MessagingServiceConnectingCount24h. </summary>
		MessagingServiceConnectingCount24h,
		///<summary>MessagingServiceConnectedCount24h. </summary>
		MessagingServiceConnectedCount24h,
		///<summary>MessagingServiceReconnectingCount24h. </summary>
		MessagingServiceReconnectingCount24h,
		///<summary>MessagingServiceConnectionLostCount24h. </summary>
		MessagingServiceConnectionLostCount24h,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Devicemedia.</summary>
	public enum DevicemediaFieldIndex
	{
		///<summary>DevicemediaId. </summary>
		DevicemediaId,
		///<summary>DeviceId. </summary>
		DeviceId,
		///<summary>Filename. </summary>
		Filename,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>WebserviceUrl. </summary>
		WebserviceUrl,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: DeviceTokenHistory.</summary>
	public enum DeviceTokenHistoryFieldIndex
	{
		///<summary>DeviceTokenHistoryId. </summary>
		DeviceTokenHistoryId,
		///<summary>DeviceId. </summary>
		DeviceId,
		///<summary>Identifier. </summary>
		Identifier,
		///<summary>UserId. </summary>
		UserId,
		///<summary>Username. </summary>
		Username,
		///<summary>Token. </summary>
		Token,
		///<summary>Action. </summary>
		Action,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>Notes. </summary>
		Notes,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: DeviceTokenTask.</summary>
	public enum DeviceTokenTaskFieldIndex
	{
		///<summary>DeviceTokenTaskId. </summary>
		DeviceTokenTaskId,
		///<summary>DeviceId. </summary>
		DeviceId,
		///<summary>UserId. </summary>
		UserId,
		///<summary>Action. </summary>
		Action,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>State. </summary>
		State,
		///<summary>Token. </summary>
		Token,
		///<summary>StateUpdatedUTC. </summary>
		StateUpdatedUTC,
		///<summary>Notes. </summary>
		Notes,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Entertainment.</summary>
	public enum EntertainmentFieldIndex
	{
		///<summary>EntertainmentId. </summary>
		EntertainmentId,
		///<summary>Name. </summary>
		Name,
		///<summary>TextColor. </summary>
		TextColor,
		///<summary>BackgroundColor. </summary>
		BackgroundColor,
		///<summary>Visible. </summary>
		Visible,
		///<summary>PackageName. </summary>
		PackageName,
		///<summary>ClassName. </summary>
		ClassName,
		///<summary>DefaultEntertainmenturlId. </summary>
		DefaultEntertainmenturlId,
		///<summary>EntertainmentType. </summary>
		EntertainmentType,
		///<summary>SurveyName. </summary>
		SurveyName,
		///<summary>FormName. </summary>
		FormName,
		///<summary>SocialmediaType. </summary>
		SocialmediaType,
		///<summary>BackButton. </summary>
		BackButton,
		///<summary>EntertainmentcategoryId. </summary>
		EntertainmentcategoryId,
		///<summary>CompanyId. </summary>
		CompanyId,
		///<summary>Description. </summary>
		Description,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>AnnouncementAction. </summary>
		AnnouncementAction,
		///<summary>PreventCaching. </summary>
		PreventCaching,
		///<summary>PostfixWithTimestamp. </summary>
		PostfixWithTimestamp,
		///<summary>HomeButton. </summary>
		HomeButton,
		///<summary>MenuContainer. </summary>
		MenuContainer,
		///<summary>NavigateButton. </summary>
		NavigateButton,
		///<summary>NavigationBar. </summary>
		NavigationBar,
		///<summary>TitleAsHeader. </summary>
		TitleAsHeader,
		///<summary>RestrictedAccess. </summary>
		RestrictedAccess,
		///<summary>EntertainmentFileId. </summary>
		EntertainmentFileId,
		///<summary>EntertainmentFileVersion. </summary>
		EntertainmentFileVersion,
		///<summary>AppDataClearInterval. </summary>
		AppDataClearInterval,
		///<summary>SiteId. </summary>
		SiteId,
		///<summary>AppCloseInterval. </summary>
		AppCloseInterval,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Entertainmentcategory.</summary>
	public enum EntertainmentcategoryFieldIndex
	{
		///<summary>EntertainmentcategoryId. </summary>
		EntertainmentcategoryId,
		///<summary>Name. </summary>
		Name,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: EntertainmentcategoryLanguage.</summary>
	public enum EntertainmentcategoryLanguageFieldIndex
	{
		///<summary>EntertainmentcategoryLanguageId. </summary>
		EntertainmentcategoryLanguageId,
		///<summary>EntertainmentcategoryId. </summary>
		EntertainmentcategoryId,
		///<summary>LanguageId. </summary>
		LanguageId,
		///<summary>Name. </summary>
		Name,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: EntertainmentConfiguration.</summary>
	public enum EntertainmentConfigurationFieldIndex
	{
		///<summary>EntertainmentConfigurationId. </summary>
		EntertainmentConfigurationId,
		///<summary>CompanyId. </summary>
		CompanyId,
		///<summary>Name. </summary>
		Name,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: EntertainmentConfigurationEntertainment.</summary>
	public enum EntertainmentConfigurationEntertainmentFieldIndex
	{
		///<summary>EntertainmentConfigurationEntertainmentId. </summary>
		EntertainmentConfigurationEntertainmentId,
		///<summary>ParentCompanyId. </summary>
		ParentCompanyId,
		///<summary>EntertainmentConfigurationId. </summary>
		EntertainmentConfigurationId,
		///<summary>EntertainmentId. </summary>
		EntertainmentId,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>SortOrder. </summary>
		SortOrder,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: EntertainmentDependency.</summary>
	public enum EntertainmentDependencyFieldIndex
	{
		///<summary>EntertainmentDependencyId. </summary>
		EntertainmentDependencyId,
		///<summary>ParentCompanyId. </summary>
		ParentCompanyId,
		///<summary>EntertainmentId. </summary>
		EntertainmentId,
		///<summary>DependentEntertainmentId. </summary>
		DependentEntertainmentId,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: EntertainmentFile.</summary>
	public enum EntertainmentFileFieldIndex
	{
		///<summary>EntertainmentFileId. </summary>
		EntertainmentFileId,
		///<summary>Blob. </summary>
		Blob,
		///<summary>Filename. </summary>
		Filename,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Entertainmenturl.</summary>
	public enum EntertainmenturlFieldIndex
	{
		///<summary>EntertainmenturlId. </summary>
		EntertainmenturlId,
		///<summary>EntertainmentId. </summary>
		EntertainmentId,
		///<summary>Url. </summary>
		Url,
		///<summary>AccessFullDomain. </summary>
		AccessFullDomain,
		///<summary>InitialZoom. </summary>
		InitialZoom,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>ParentCompanyId. </summary>
		ParentCompanyId,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: EntityFieldInformation.</summary>
	public enum EntityFieldInformationFieldIndex
	{
		///<summary>EntityFieldInformationId. </summary>
		EntityFieldInformationId,
		///<summary>EntityName. </summary>
		EntityName,
		///<summary>FieldName. </summary>
		FieldName,
		///<summary>FriendlyName. </summary>
		FriendlyName,
		///<summary>ShowOnDefaultGridView. </summary>
		ShowOnDefaultGridView,
		///<summary>DefaultDisplayPosition. </summary>
		DefaultDisplayPosition,
		///<summary>AllowShowOnGridView. </summary>
		AllowShowOnGridView,
		///<summary>HelpText. </summary>
		HelpText,
		///<summary>DisplayFormatString. </summary>
		DisplayFormatString,
		///<summary>DefaultSortPosition. </summary>
		DefaultSortPosition,
		///<summary>DefaultSortOperator. </summary>
		DefaultSortOperator,
		///<summary>Type. </summary>
		Type,
		///<summary>IsRequired. </summary>
		IsRequired,
		///<summary>EntityFieldType. </summary>
		EntityFieldType,
		///<summary>ExcludeForUpdateWmsCacheDateOnChange. </summary>
		ExcludeForUpdateWmsCacheDateOnChange,
		///<summary>ExcludeForUpdateCompanyDataVersionOnChange. </summary>
		ExcludeForUpdateCompanyDataVersionOnChange,
		///<summary>ExcludeForUpdateMenuDataVersionOnChange. </summary>
		ExcludeForUpdateMenuDataVersionOnChange,
		///<summary>ExcludeForUpdatePosIntegrationInformationVersionOnChange. </summary>
		ExcludeForUpdatePosIntegrationInformationVersionOnChange,
		///<summary>ForLocalUseWhileSyncingIsUpdated. </summary>
		ForLocalUseWhileSyncingIsUpdated,
		///<summary>Archived. </summary>
		Archived,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>Deleted. </summary>
		Deleted,
		///<summary>ExcludeForUpdateDeliverypointDataVersionOnChange. </summary>
		ExcludeForUpdateDeliverypointDataVersionOnChange,
		///<summary>ExcludeForUpdateSurveyDataVersionOnChange. </summary>
		ExcludeForUpdateSurveyDataVersionOnChange,
		///<summary>ExcludeForUpdateAnnouncementDataVersionOnChange. </summary>
		ExcludeForUpdateAnnouncementDataVersionOnChange,
		///<summary>ExcludeForUpdateEntertainmentDataVersionOnChange. </summary>
		ExcludeForUpdateEntertainmentDataVersionOnChange,
		///<summary>ExcludeForUpdateAdvertisementDataVersionOnChange. </summary>
		ExcludeForUpdateAdvertisementDataVersionOnChange,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: EntityFieldInformationCustom.</summary>
	public enum EntityFieldInformationCustomFieldIndex
	{
		///<summary>EntityName. </summary>
		EntityName,
		///<summary>FieldName. </summary>
		FieldName,
		///<summary>FriendlyName. </summary>
		FriendlyName,
		///<summary>ShowOnDefaultGridView. </summary>
		ShowOnDefaultGridView,
		///<summary>DefaultDisplayPosition. </summary>
		DefaultDisplayPosition,
		///<summary>DefaultSortPosition. </summary>
		DefaultSortPosition,
		///<summary>DefaultSortOperator. </summary>
		DefaultSortOperator,
		///<summary>AllowShowOnGridView. </summary>
		AllowShowOnGridView,
		///<summary>IsRequired. </summary>
		IsRequired,
		///<summary>ForLocalUseWhileSyncingIsUpdated. </summary>
		ForLocalUseWhileSyncingIsUpdated,
		///<summary>DisplayFormatString. </summary>
		DisplayFormatString,
		///<summary>Archived. </summary>
		Archived,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: EntityInformation.</summary>
	public enum EntityInformationFieldIndex
	{
		///<summary>EntityInformationId. </summary>
		EntityInformationId,
		///<summary>EntityName. </summary>
		EntityName,
		///<summary>ShowFieldName. </summary>
		ShowFieldName,
		///<summary>ShowFieldNameBreadCrumb. </summary>
		ShowFieldNameBreadCrumb,
		///<summary>FriendlyName. </summary>
		FriendlyName,
		///<summary>FriendlyNamePlural. </summary>
		FriendlyNamePlural,
		///<summary>HelpText. </summary>
		HelpText,
		///<summary>TitleAsBreadCrumbHierarchy. </summary>
		TitleAsBreadCrumbHierarchy,
		///<summary>DefaultEntityEditPage. </summary>
		DefaultEntityEditPage,
		///<summary>DefaultEntityCollectionPage. </summary>
		DefaultEntityCollectionPage,
		///<summary>UpdateWmsCacheDateOnChange. </summary>
		UpdateWmsCacheDateOnChange,
		///<summary>UpdateCompanyDataVersionOnChange. </summary>
		UpdateCompanyDataVersionOnChange,
		///<summary>UpdateMenuDataVersionOnChange. </summary>
		UpdateMenuDataVersionOnChange,
		///<summary>UpdatePosIntegrationInformationOnChange. </summary>
		UpdatePosIntegrationInformationOnChange,
		///<summary>ForLocalUseWhileSyncingIsUpdated. </summary>
		ForLocalUseWhileSyncingIsUpdated,
		///<summary>Archived. </summary>
		Archived,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>Deleted. </summary>
		Deleted,
		///<summary>UpdateDeliverypointDataVersionOnChange. </summary>
		UpdateDeliverypointDataVersionOnChange,
		///<summary>UpdateSurveyDataVersionOnChange. </summary>
		UpdateSurveyDataVersionOnChange,
		///<summary>UpdateAnnouncementDataVersionOnChange. </summary>
		UpdateAnnouncementDataVersionOnChange,
		///<summary>UpdateEntertainmentDataVersionOnChange. </summary>
		UpdateEntertainmentDataVersionOnChange,
		///<summary>UpdateAdvertisementDataVersionOnChange. </summary>
		UpdateAdvertisementDataVersionOnChange,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: EntityInformationCustom.</summary>
	public enum EntityInformationCustomFieldIndex
	{
		///<summary>EntityName. </summary>
		EntityName,
		///<summary>ShowFieldName. </summary>
		ShowFieldName,
		///<summary>FriendlyName. </summary>
		FriendlyName,
		///<summary>FriendlyNamePlural. </summary>
		FriendlyNamePlural,
		///<summary>HelpText. </summary>
		HelpText,
		///<summary>TitleAsBreadCrumbHierarchy. </summary>
		TitleAsBreadCrumbHierarchy,
		///<summary>DefaultEntityEditPage. </summary>
		DefaultEntityEditPage,
		///<summary>DefaultEntityCollectionPage. </summary>
		DefaultEntityCollectionPage,
		///<summary>ForLocalUseWhileSyncingIsUpdated. </summary>
		ForLocalUseWhileSyncingIsUpdated,
		///<summary>Archived. </summary>
		Archived,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: ExternalDeliverypoint.</summary>
	public enum ExternalDeliverypointFieldIndex
	{
		///<summary>ExternalDeliverypointId. </summary>
		ExternalDeliverypointId,
		///<summary>ExternalSystemId. </summary>
		ExternalSystemId,
		///<summary>Id. </summary>
		Id,
		///<summary>Name. </summary>
		Name,
		///<summary>Visible. </summary>
		Visible,
		///<summary>StringValue1. </summary>
		StringValue1,
		///<summary>StringValue2. </summary>
		StringValue2,
		///<summary>StringValue3. </summary>
		StringValue3,
		///<summary>StringValue4. </summary>
		StringValue4,
		///<summary>StringValue5. </summary>
		StringValue5,
		///<summary>IntValue1. </summary>
		IntValue1,
		///<summary>IntValue2. </summary>
		IntValue2,
		///<summary>IntValue3. </summary>
		IntValue3,
		///<summary>IntValue4. </summary>
		IntValue4,
		///<summary>IntValue5. </summary>
		IntValue5,
		///<summary>BoolValue1. </summary>
		BoolValue1,
		///<summary>BoolValue2. </summary>
		BoolValue2,
		///<summary>BoolValue3. </summary>
		BoolValue3,
		///<summary>BoolValue4. </summary>
		BoolValue4,
		///<summary>BoolValue5. </summary>
		BoolValue5,
		///<summary>CreatedInBatchId. </summary>
		CreatedInBatchId,
		///<summary>UpdatedInBatchId. </summary>
		UpdatedInBatchId,
		///<summary>SynchronisationBatchId. </summary>
		SynchronisationBatchId,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>ParentCompanyId. </summary>
		ParentCompanyId,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: ExternalMenu.</summary>
	public enum ExternalMenuFieldIndex
	{
		///<summary>ExternalMenuId. </summary>
		ExternalMenuId,
		///<summary>ExternalSystemId. </summary>
		ExternalSystemId,
		///<summary>Id. </summary>
		Id,
		///<summary>Name. </summary>
		Name,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdateUTC. </summary>
		UpdateUTC,
		///<summary>ParentCompanyId. </summary>
		ParentCompanyId,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: ExternalProduct.</summary>
	public enum ExternalProductFieldIndex
	{
		///<summary>ExternalProductId. </summary>
		ExternalProductId,
		///<summary>ExternalSystemId. </summary>
		ExternalSystemId,
		///<summary>Id. </summary>
		Id,
		///<summary>Name. </summary>
		Name,
		///<summary>Price. </summary>
		Price,
		///<summary>Visible. </summary>
		Visible,
		///<summary>StringValue1. </summary>
		StringValue1,
		///<summary>StringValue2. </summary>
		StringValue2,
		///<summary>StringValue3. </summary>
		StringValue3,
		///<summary>StringValue4. </summary>
		StringValue4,
		///<summary>StringValue5. </summary>
		StringValue5,
		///<summary>IntValue1. </summary>
		IntValue1,
		///<summary>IntValue2. </summary>
		IntValue2,
		///<summary>IntValue3. </summary>
		IntValue3,
		///<summary>IntValue4. </summary>
		IntValue4,
		///<summary>IntValue5. </summary>
		IntValue5,
		///<summary>BoolValue1. </summary>
		BoolValue1,
		///<summary>BoolValue2. </summary>
		BoolValue2,
		///<summary>BoolValue3. </summary>
		BoolValue3,
		///<summary>BoolValue4. </summary>
		BoolValue4,
		///<summary>BoolValue5. </summary>
		BoolValue5,
		///<summary>CreatedInBatchId. </summary>
		CreatedInBatchId,
		///<summary>UpdatedInBatchId. </summary>
		UpdatedInBatchId,
		///<summary>SynchronisationBatchId. </summary>
		SynchronisationBatchId,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>ParentCompanyId. </summary>
		ParentCompanyId,
		///<summary>Type. </summary>
		Type,
		///<summary>MinOptions. </summary>
		MinOptions,
		///<summary>MaxOptions. </summary>
		MaxOptions,
		///<summary>AllowDuplicates. </summary>
		AllowDuplicates,
		///<summary>IsEnabled. </summary>
		IsEnabled,
		///<summary>IsSnoozed. </summary>
		IsSnoozed,
		///<summary>ExternalMenuId. </summary>
		ExternalMenuId,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: ExternalSubProduct.</summary>
	public enum ExternalSubProductFieldIndex
	{
		///<summary>ExternalProductExternalProductId. </summary>
		ExternalProductExternalProductId,
		///<summary>ExternalProductId. </summary>
		ExternalProductId,
		///<summary>ExternalSubProductId. </summary>
		ExternalSubProductId,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: ExternalSystem.</summary>
	public enum ExternalSystemFieldIndex
	{
		///<summary>ExternalSystemId. </summary>
		ExternalSystemId,
		///<summary>CompanyId. </summary>
		CompanyId,
		///<summary>Name. </summary>
		Name,
		///<summary>Type. </summary>
		Type,
		///<summary>StringValue1. </summary>
		StringValue1,
		///<summary>StringValue2. </summary>
		StringValue2,
		///<summary>StringValue3. </summary>
		StringValue3,
		///<summary>StringValue4. </summary>
		StringValue4,
		///<summary>StringValue5. </summary>
		StringValue5,
		///<summary>ExpiryDateUtc. </summary>
		ExpiryDateUtc,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>Environment. </summary>
		Environment,
		///<summary>BoolValue1. </summary>
		BoolValue1,
		///<summary>BoolValue2. </summary>
		BoolValue2,
		///<summary>BoolValue3. </summary>
		BoolValue3,
		///<summary>BoolValue4. </summary>
		BoolValue4,
		///<summary>BoolValue5. </summary>
		BoolValue5,
		///<summary>IsBusy. </summary>
		IsBusy,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: ExternalSystemLog.</summary>
	public enum ExternalSystemLogFieldIndex
	{
		///<summary>ExternalSystemLogId. </summary>
		ExternalSystemLogId,
		///<summary>ExternalSystemId. </summary>
		ExternalSystemId,
		///<summary>Message. </summary>
		Message,
		///<summary>LogType. </summary>
		LogType,
		///<summary>Success. </summary>
		Success,
		///<summary>Raw. </summary>
		Raw,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>OrderId. </summary>
		OrderId,
		///<summary>Log. </summary>
		Log,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: FeatureToggleAvailability.</summary>
	public enum FeatureToggleAvailabilityFieldIndex
	{
		///<summary>FeatureToggleAvailabilityId. </summary>
		FeatureToggleAvailabilityId,
		///<summary>FeatureToggle. </summary>
		FeatureToggle,
		///<summary>ReleaseGroup. </summary>
		ReleaseGroup,
		///<summary>Role. </summary>
		Role,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Folio.</summary>
	public enum FolioFieldIndex
	{
		///<summary>FolioId. </summary>
		FolioId,
		///<summary>ParentCompanyId. </summary>
		ParentCompanyId,
		///<summary>GuestInformationId. </summary>
		GuestInformationId,
		///<summary>Balance. </summary>
		Balance,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: FolioItem.</summary>
	public enum FolioItemFieldIndex
	{
		///<summary>FolioItemId. </summary>
		FolioItemId,
		///<summary>ParentCompanyId. </summary>
		ParentCompanyId,
		///<summary>FolioId. </summary>
		FolioId,
		///<summary>Code. </summary>
		Code,
		///<summary>Description. </summary>
		Description,
		///<summary>PriceIn. </summary>
		PriceIn,
		///<summary>CurrencyCode. </summary>
		CurrencyCode,
		///<summary>ReferenceId. </summary>
		ReferenceId,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Game.</summary>
	public enum GameFieldIndex
	{
		///<summary>GameId. </summary>
		GameId,
		///<summary>Name. </summary>
		Name,
		///<summary>Type. </summary>
		Type,
		///<summary>Url. </summary>
		Url,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: GameSession.</summary>
	public enum GameSessionFieldIndex
	{
		///<summary>GameSessionId. </summary>
		GameSessionId,
		///<summary>GameId. </summary>
		GameId,
		///<summary>ClientId. </summary>
		ClientId,
		///<summary>StartUTC. </summary>
		StartUTC,
		///<summary>EndUTC. </summary>
		EndUTC,
		///<summary>Length. </summary>
		Length,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>ParentCompanyId. </summary>
		ParentCompanyId,
		///<summary>GameName. </summary>
		GameName,
		///<summary>ClientIdentifier. </summary>
		ClientIdentifier,
		///<summary>DeliverypointNumber. </summary>
		DeliverypointNumber,
		///<summary>DeliverypointName. </summary>
		DeliverypointName,
		///<summary>DeliverypointId. </summary>
		DeliverypointId,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: GameSessionReport.</summary>
	public enum GameSessionReportFieldIndex
	{
		///<summary>GameSessionReportId. </summary>
		GameSessionReportId,
		///<summary>CompanyId. </summary>
		CompanyId,
		///<summary>Recipients. </summary>
		Recipients,
		///<summary>Sent. </summary>
		Sent,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: GameSessionReportConfiguration.</summary>
	public enum GameSessionReportConfigurationFieldIndex
	{
		///<summary>GameSessionReportConfigurationId. </summary>
		GameSessionReportConfigurationId,
		///<summary>CompanyId. </summary>
		CompanyId,
		///<summary>RecurrenceType. </summary>
		RecurrenceType,
		///<summary>SendTime. </summary>
		SendTime,
		///<summary>Recipients. </summary>
		Recipients,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>Name. </summary>
		Name,
		///<summary>Active. </summary>
		Active,
		///<summary>LastReportSentUTC. </summary>
		LastReportSentUTC,
		///<summary>DailyResetTime. </summary>
		DailyResetTime,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: GameSessionReportItem.</summary>
	public enum GameSessionReportItemFieldIndex
	{
		///<summary>GameSessionReportItemId. </summary>
		GameSessionReportItemId,
		///<summary>GameSessionReportId. </summary>
		GameSessionReportId,
		///<summary>GameSessionId. </summary>
		GameSessionId,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>ParentCompanyId. </summary>
		ParentCompanyId,
		///<summary>DeliverypointName. </summary>
		DeliverypointName,
		///<summary>GameName. </summary>
		GameName,
		///<summary>StartUTC. </summary>
		StartUTC,
		///<summary>EndUTC. </summary>
		EndUTC,
		///<summary>Length. </summary>
		Length,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Genericalteration.</summary>
	public enum GenericalterationFieldIndex
	{
		///<summary>GenericalterationId. </summary>
		GenericalterationId,
		///<summary>Name. </summary>
		Name,
		///<summary>ExternalId. </summary>
		ExternalId,
		///<summary>GenericalterationType. </summary>
		GenericalterationType,
		///<summary>Type. </summary>
		Type,
		///<summary>MinOptions. </summary>
		MinOptions,
		///<summary>MaxOptions. </summary>
		MaxOptions,
		///<summary>AvailableOnOtoucho. </summary>
		AvailableOnOtoucho,
		///<summary>AvailableOnObymobi. </summary>
		AvailableOnObymobi,
		///<summary>StartTime. </summary>
		StartTime,
		///<summary>EndTime. </summary>
		EndTime,
		///<summary>MinLeadMinutes. </summary>
		MinLeadMinutes,
		///<summary>MaxLeadHours. </summary>
		MaxLeadHours,
		///<summary>IntervalMinutes. </summary>
		IntervalMinutes,
		///<summary>ShowDatePicker. </summary>
		ShowDatePicker,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>BrandId. </summary>
		BrandId,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Genericalterationitem.</summary>
	public enum GenericalterationitemFieldIndex
	{
		///<summary>GenericalterationitemId. </summary>
		GenericalterationitemId,
		///<summary>GenericalterationId. </summary>
		GenericalterationId,
		///<summary>GenericalterationoptionId. </summary>
		GenericalterationoptionId,
		///<summary>SelectedOnDefault. </summary>
		SelectedOnDefault,
		///<summary>SortOrder. </summary>
		SortOrder,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Genericalterationoption.</summary>
	public enum GenericalterationoptionFieldIndex
	{
		///<summary>GenericalterationoptionId. </summary>
		GenericalterationoptionId,
		///<summary>Name. </summary>
		Name,
		///<summary>GenericalterationoptionType. </summary>
		GenericalterationoptionType,
		///<summary>PriceIn. </summary>
		PriceIn,
		///<summary>PriceAddition. </summary>
		PriceAddition,
		///<summary>Description. </summary>
		Description,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Genericcategory.</summary>
	public enum GenericcategoryFieldIndex
	{
		///<summary>GenericcategoryId. </summary>
		GenericcategoryId,
		///<summary>ParentGenericcategoryId. </summary>
		ParentGenericcategoryId,
		///<summary>Name. </summary>
		Name,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>ExternalId. </summary>
		ExternalId,
		///<summary>GenericCategoryType. </summary>
		GenericCategoryType,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>BrandId. </summary>
		BrandId,
		///<summary>SortOrder. </summary>
		SortOrder,
		///<summary>VisibilityType. </summary>
		VisibilityType,
		///<summary>Visible. </summary>
		Visible,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: GenericcategoryLanguage.</summary>
	public enum GenericcategoryLanguageFieldIndex
	{
		///<summary>GenericcategoryLanguageId. </summary>
		GenericcategoryLanguageId,
		///<summary>GenericcategoryId. </summary>
		GenericcategoryId,
		///<summary>LanguageId. </summary>
		LanguageId,
		///<summary>Name. </summary>
		Name,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Genericproduct.</summary>
	public enum GenericproductFieldIndex
	{
		///<summary>GenericproductId. </summary>
		GenericproductId,
		///<summary>SupplierId. </summary>
		SupplierId,
		///<summary>GenericcategoryId. </summary>
		GenericcategoryId,
		///<summary>Name. </summary>
		Name,
		///<summary>Code. </summary>
		Code,
		///<summary>Description. </summary>
		Description,
		///<summary>TextColor. </summary>
		TextColor,
		///<summary>BackgroundColor. </summary>
		BackgroundColor,
		///<summary>VattariffId. </summary>
		VattariffId,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>ExternalId. </summary>
		ExternalId,
		///<summary>GenericproductType. </summary>
		GenericproductType,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>BrandId. </summary>
		BrandId,
		///<summary>SortOrder. </summary>
		SortOrder,
		///<summary>VisibilityType. </summary>
		VisibilityType,
		///<summary>Visible. </summary>
		Visible,
		///<summary>SubType. </summary>
		SubType,
		///<summary>PriceIn. </summary>
		PriceIn,
		///<summary>HidePrice. </summary>
		HidePrice,
		///<summary>WebTypeSmartphoneUrl. </summary>
		WebTypeSmartphoneUrl,
		///<summary>WebTypeTabletUrl. </summary>
		WebTypeTabletUrl,
		///<summary>ViewLayoutType. </summary>
		ViewLayoutType,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: GenericproductGenericalteration.</summary>
	public enum GenericproductGenericalterationFieldIndex
	{
		///<summary>GenericproductGenericalterationId. </summary>
		GenericproductGenericalterationId,
		///<summary>GenericproductId. </summary>
		GenericproductId,
		///<summary>GenericalterationId. </summary>
		GenericalterationId,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>SortOrder. </summary>
		SortOrder,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: GenericproductLanguage.</summary>
	public enum GenericproductLanguageFieldIndex
	{
		///<summary>GenericproductLanguageId. </summary>
		GenericproductLanguageId,
		///<summary>GenericproductId. </summary>
		GenericproductId,
		///<summary>LanguageId. </summary>
		LanguageId,
		///<summary>Name. </summary>
		Name,
		///<summary>Description. </summary>
		Description,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: GuestInformation.</summary>
	public enum GuestInformationFieldIndex
	{
		///<summary>GuestInformationId. </summary>
		GuestInformationId,
		///<summary>CompanyId. </summary>
		CompanyId,
		///<summary>ExternalGuestId. </summary>
		ExternalGuestId,
		///<summary>DeliverypointNumber. </summary>
		DeliverypointNumber,
		///<summary>ReservationNumber. </summary>
		ReservationNumber,
		///<summary>Vip. </summary>
		Vip,
		///<summary>Occupied. </summary>
		Occupied,
		///<summary>GroupName. </summary>
		GroupName,
		///<summary>LanguageCode. </summary>
		LanguageCode,
		///<summary>DoNotDisturb. </summary>
		DoNotDisturb,
		///<summary>MessageWaiting. </summary>
		MessageWaiting,
		///<summary>ViewBill. </summary>
		ViewBill,
		///<summary>ExpressCheckOut. </summary>
		ExpressCheckOut,
		///<summary>CheckInUTC. </summary>
		CheckInUTC,
		///<summary>CheckOutUTC. </summary>
		CheckOutUTC,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Icrtouchprintermapping.</summary>
	public enum IcrtouchprintermappingFieldIndex
	{
		///<summary>IcrtouchprintermappingId. </summary>
		IcrtouchprintermappingId,
		///<summary>TerminalId. </summary>
		TerminalId,
		///<summary>Name. </summary>
		Name,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>ParentCompanyId. </summary>
		ParentCompanyId,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: IcrtouchprintermappingDeliverypoint.</summary>
	public enum IcrtouchprintermappingDeliverypointFieldIndex
	{
		///<summary>IcrtouchprintermappingDeliverypointId. </summary>
		IcrtouchprintermappingDeliverypointId,
		///<summary>IcrtouchprintermappingId. </summary>
		IcrtouchprintermappingId,
		///<summary>DeliverypointId. </summary>
		DeliverypointId,
		///<summary>Printer1. </summary>
		Printer1,
		///<summary>Printer2. </summary>
		Printer2,
		///<summary>Printer3. </summary>
		Printer3,
		///<summary>Printer4. </summary>
		Printer4,
		///<summary>Printer5. </summary>
		Printer5,
		///<summary>Printer6. </summary>
		Printer6,
		///<summary>Printer7. </summary>
		Printer7,
		///<summary>Printer8. </summary>
		Printer8,
		///<summary>Printer9. </summary>
		Printer9,
		///<summary>Printer10. </summary>
		Printer10,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>ParentCompanyId. </summary>
		ParentCompanyId,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: IncomingSms.</summary>
	public enum IncomingSmsFieldIndex
	{
		///<summary>IncomingSmsId. </summary>
		IncomingSmsId,
		///<summary>Number. </summary>
		Number,
		///<summary>Operator. </summary>
		Operator,
		///<summary>Message. </summary>
		Message,
		///<summary>SmsType. </summary>
		SmsType,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: InfraredCommand.</summary>
	public enum InfraredCommandFieldIndex
	{
		///<summary>InfraredCommandId. </summary>
		InfraredCommandId,
		///<summary>InfraredConfigurationId. </summary>
		InfraredConfigurationId,
		///<summary>Type. </summary>
		Type,
		///<summary>Hex. </summary>
		Hex,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>Name. </summary>
		Name,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: InfraredConfiguration.</summary>
	public enum InfraredConfigurationFieldIndex
	{
		///<summary>InfraredConfigurationId. </summary>
		InfraredConfigurationId,
		///<summary>Name. </summary>
		Name,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>MillisecondsBetweenCommands. </summary>
		MillisecondsBetweenCommands,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Language.</summary>
	public enum LanguageFieldIndex
	{
		///<summary>LanguageId. </summary>
		LanguageId,
		///<summary>Code. </summary>
		Code,
		///<summary>Name. </summary>
		Name,
		///<summary>LocalizedName. </summary>
		LocalizedName,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>CodeAlpha3. </summary>
		CodeAlpha3,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: LicensedModule.</summary>
	public enum LicensedModuleFieldIndex
	{
		///<summary>LicensedModuleId. </summary>
		LicensedModuleId,
		///<summary>ModuleNameSystem. </summary>
		ModuleNameSystem,
		///<summary>IsLicensed. </summary>
		IsLicensed,
		///<summary>Deleted. </summary>
		Deleted,
		///<summary>Archived. </summary>
		Archived,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: LicensedUIElement.</summary>
	public enum LicensedUIElementFieldIndex
	{
		///<summary>LicensedUIElementId. </summary>
		LicensedUIElementId,
		///<summary>UiElementTypeNameFull. </summary>
		UiElementTypeNameFull,
		///<summary>IsLicensed. </summary>
		IsLicensed,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: LicensedUIElementSubPanel.</summary>
	public enum LicensedUIElementSubPanelFieldIndex
	{
		///<summary>LicensedUIElementSubPanelId. </summary>
		LicensedUIElementSubPanelId,
		///<summary>UiElementSubPanelTypeNameFull. </summary>
		UiElementSubPanelTypeNameFull,
		///<summary>IsLicensed. </summary>
		IsLicensed,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Map.</summary>
	public enum MapFieldIndex
	{
		///<summary>MapId. </summary>
		MapId,
		///<summary>Name. </summary>
		Name,
		///<summary>MyLocationEnabled. </summary>
		MyLocationEnabled,
		///<summary>ZoomControlsEnabled. </summary>
		ZoomControlsEnabled,
		///<summary>MapType. </summary>
		MapType,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>CompanyId. </summary>
		CompanyId,
		///<summary>IndoorEnabled. </summary>
		IndoorEnabled,
		///<summary>ZoomLevel. </summary>
		ZoomLevel,
		///<summary>MapProvider. </summary>
		MapProvider,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: MapPointOfInterest.</summary>
	public enum MapPointOfInterestFieldIndex
	{
		///<summary>MapPointOfInterestId. </summary>
		MapPointOfInterestId,
		///<summary>MapId. </summary>
		MapId,
		///<summary>PointOfInterestId. </summary>
		PointOfInterestId,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Media.</summary>
	public enum MediaFieldIndex
	{
		///<summary>MediaId. </summary>
		MediaId,
		///<summary>MediaType. </summary>
		MediaType,
		///<summary>SortOrder. </summary>
		SortOrder,
		///<summary>DefaultItem. </summary>
		DefaultItem,
		///<summary>Name. </summary>
		Name,
		///<summary>FilePathRelativeToMediaPath. </summary>
		FilePathRelativeToMediaPath,
		///<summary>Description. </summary>
		Description,
		///<summary>MimeType. </summary>
		MimeType,
		///<summary>SizeKb. </summary>
		SizeKb,
		///<summary>CompanyId. </summary>
		CompanyId,
		///<summary>ProductId. </summary>
		ProductId,
		///<summary>CategoryId. </summary>
		CategoryId,
		///<summary>AdvertisementId. </summary>
		AdvertisementId,
		///<summary>EntertainmentId. </summary>
		EntertainmentId,
		///<summary>AlterationoptionId. </summary>
		AlterationoptionId,
		///<summary>GenericproductId. </summary>
		GenericproductId,
		///<summary>GenericcategoryId. </summary>
		GenericcategoryId,
		///<summary>DeliverypointgroupId. </summary>
		DeliverypointgroupId,
		///<summary>SurveyId. </summary>
		SurveyId,
		///<summary>FromSupplier. </summary>
		FromSupplier,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>AlterationId. </summary>
		AlterationId,
		///<summary>SurveyPageId. </summary>
		SurveyPageId,
		///<summary>RoutestephandlerId. </summary>
		RoutestephandlerId,
		///<summary>PageElementId. </summary>
		PageElementId,
		///<summary>PageId. </summary>
		PageId,
		///<summary>PointOfInterestId. </summary>
		PointOfInterestId,
		///<summary>SiteId. </summary>
		SiteId,
		///<summary>ActionUrl. </summary>
		ActionUrl,
		///<summary>ActionEntertainmentId. </summary>
		ActionEntertainmentId,
		///<summary>ActionProductId. </summary>
		ActionProductId,
		///<summary>ActionCategoryId. </summary>
		ActionCategoryId,
		///<summary>ActionEntertainmentcategoryId. </summary>
		ActionEntertainmentcategoryId,
		///<summary>JpgQuality. </summary>
		JpgQuality,
		///<summary>SizeMode. </summary>
		SizeMode,
		///<summary>ZoomLevel. </summary>
		ZoomLevel,
		///<summary>MediaFileMd5. </summary>
		MediaFileMd5,
		///<summary>Extension. </summary>
		Extension,
		///<summary>AgnosticMediaId. </summary>
		AgnosticMediaId,
		///<summary>AttachmentId. </summary>
		AttachmentId,
		///<summary>ActionPageId. </summary>
		ActionPageId,
		///<summary>ActionSiteId. </summary>
		ActionSiteId,
		///<summary>PageTemplateElementId. </summary>
		PageTemplateElementId,
		///<summary>PageTemplateId. </summary>
		PageTemplateId,
		///<summary>UIWidgetId. </summary>
		UIWidgetId,
		///<summary>UIThemeId. </summary>
		UIThemeId,
		///<summary>RoomControlSectionId. </summary>
		RoomControlSectionId,
		///<summary>RoomControlSectionItemId. </summary>
		RoomControlSectionItemId,
		///<summary>StationId. </summary>
		StationId,
		///<summary>UIFooterItemId. </summary>
		UIFooterItemId,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>RelatedCompanyId. </summary>
		RelatedCompanyId,
		///<summary>ClientConfigurationId. </summary>
		ClientConfigurationId,
		///<summary>LastDistributedVersionTicksAmazon. </summary>
		LastDistributedVersionTicksAmazon,
		///<summary>LastDistributedVersionTicks. </summary>
		LastDistributedVersionTicks,
		///<summary>SizeWidth. </summary>
		SizeWidth,
		///<summary>SizeHeight. </summary>
		SizeHeight,
		///<summary>ProductgroupId. </summary>
		ProductgroupId,
		///<summary>LandingPageId. </summary>
		LandingPageId,
		///<summary>CarouselItemId. </summary>
		CarouselItemId,
		///<summary>WidgetId. </summary>
		WidgetId,
		///<summary>ApplicationConfigurationId. </summary>
		ApplicationConfigurationId,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: MediaCulture.</summary>
	public enum MediaCultureFieldIndex
	{
		///<summary>MediaCultureId. </summary>
		MediaCultureId,
		///<summary>ParentCompanyId. </summary>
		ParentCompanyId,
		///<summary>MediaId. </summary>
		MediaId,
		///<summary>CultureCode. </summary>
		CultureCode,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: MediaLanguage.</summary>
	public enum MediaLanguageFieldIndex
	{
		///<summary>MediaLanguageId. </summary>
		MediaLanguageId,
		///<summary>MediaId. </summary>
		MediaId,
		///<summary>LanguageId. </summary>
		LanguageId,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: MediaProcessingTask.</summary>
	public enum MediaProcessingTaskFieldIndex
	{
		///<summary>MediaProcessingTaskId. </summary>
		MediaProcessingTaskId,
		///<summary>MediaRatioTypeMediaId. </summary>
		MediaRatioTypeMediaId,
		///<summary>Action. </summary>
		Action,
		///<summary>Errors. </summary>
		Errors,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>RelatedToCompanyId. </summary>
		RelatedToCompanyId,
		///<summary>PathFormat. </summary>
		PathFormat,
		///<summary>Attempts. </summary>
		Attempts,
		///<summary>MediaRatioTypeMediaIdNonRelationCopy. </summary>
		MediaRatioTypeMediaIdNonRelationCopy,
		///<summary>Priority. </summary>
		Priority,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>CompletedOnAmazonUTC. </summary>
		CompletedOnAmazonUTC,
		///<summary>LastAttemptUTC. </summary>
		LastAttemptUTC,
		///<summary>NextAttemptUTC. </summary>
		NextAttemptUTC,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: MediaRatioTypeMedia.</summary>
	public enum MediaRatioTypeMediaFieldIndex
	{
		///<summary>MediaRatioTypeMediaId. </summary>
		MediaRatioTypeMediaId,
		///<summary>MediaId. </summary>
		MediaId,
		///<summary>Top. </summary>
		Top,
		///<summary>Left. </summary>
		Left,
		///<summary>Width. </summary>
		Width,
		///<summary>Height. </summary>
		Height,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>MediaType. </summary>
		MediaType,
		///<summary>LastDistributedVersionTicks. </summary>
		LastDistributedVersionTicks,
		///<summary>LastDistributedVersionTicksAmazon. </summary>
		LastDistributedVersionTicksAmazon,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>ManuallyVerified. </summary>
		ManuallyVerified,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: MediaRatioTypeMediaFile.</summary>
	public enum MediaRatioTypeMediaFileFieldIndex
	{
		///<summary>MediaRatioTypeMediaFileId. </summary>
		MediaRatioTypeMediaFileId,
		///<summary>MediaRatioTypeMediaId. </summary>
		MediaRatioTypeMediaId,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>FileMd5. </summary>
		FileMd5,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: MediaRelationship.</summary>
	public enum MediaRelationshipFieldIndex
	{
		///<summary>MediaRelationshipId. </summary>
		MediaRelationshipId,
		///<summary>MediaId. </summary>
		MediaId,
		///<summary>ProductId. </summary>
		ProductId,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>ParentCompanyId. </summary>
		ParentCompanyId,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Menu.</summary>
	public enum MenuFieldIndex
	{
		///<summary>MenuId. </summary>
		MenuId,
		///<summary>Name. </summary>
		Name,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>CompanyId. </summary>
		CompanyId,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Message.</summary>
	public enum MessageFieldIndex
	{
		///<summary>MessageId. </summary>
		MessageId,
		///<summary>CustomerId. </summary>
		CustomerId,
		///<summary>ClientId. </summary>
		ClientId,
		///<summary>CompanyId. </summary>
		CompanyId,
		///<summary>Title. </summary>
		Title,
		///<summary>Message. </summary>
		Message,
		///<summary>OrderId. </summary>
		OrderId,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>Duration. </summary>
		Duration,
		///<summary>Urgent. </summary>
		Urgent,
		///<summary>MessageButtonType. </summary>
		MessageButtonType,
		///<summary>EntertainmentId. </summary>
		EntertainmentId,
		///<summary>MediaId. </summary>
		MediaId,
		///<summary>CategoryId. </summary>
		CategoryId,
		///<summary>DeliverypointId. </summary>
		DeliverypointId,
		///<summary>ProductId. </summary>
		ProductId,
		///<summary>Url. </summary>
		Url,
		///<summary>PageId. </summary>
		PageId,
		///<summary>SiteId. </summary>
		SiteId,
		///<summary>ProductCategoryId. </summary>
		ProductCategoryId,
		///<summary>Queued. </summary>
		Queued,
		///<summary>NotifyOnYes. </summary>
		NotifyOnYes,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>MessageLayoutType. </summary>
		MessageLayoutType,
		///<summary>RecipientType. </summary>
		RecipientType,
		///<summary>RecipientsCount. </summary>
		RecipientsCount,
		///<summary>RecipientsAsString. </summary>
		RecipientsAsString,
		///<summary>RecipientsOkResultCount. </summary>
		RecipientsOkResultCount,
		///<summary>RecipientsOkResultAsString. </summary>
		RecipientsOkResultAsString,
		///<summary>RecipientsYesResultCount. </summary>
		RecipientsYesResultCount,
		///<summary>RecipientsYesResultAsString. </summary>
		RecipientsYesResultAsString,
		///<summary>RecipientsNoResultCount. </summary>
		RecipientsNoResultCount,
		///<summary>RecipientsNoResultAsString. </summary>
		RecipientsNoResultAsString,
		///<summary>RecipientsTimeOutResultCount. </summary>
		RecipientsTimeOutResultCount,
		///<summary>RecipientsTimeOutResultAsString. </summary>
		RecipientsTimeOutResultAsString,
		///<summary>RecipientsClearManuallyResultCount. </summary>
		RecipientsClearManuallyResultCount,
		///<summary>RecipientsClearManuallyResultAsString. </summary>
		RecipientsClearManuallyResultAsString,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Messagegroup.</summary>
	public enum MessagegroupFieldIndex
	{
		///<summary>MessagegroupId. </summary>
		MessagegroupId,
		///<summary>CompanyId. </summary>
		CompanyId,
		///<summary>Name. </summary>
		Name,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>Type. </summary>
		Type,
		///<summary>GroupName. </summary>
		GroupName,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: MessagegroupDeliverypoint.</summary>
	public enum MessagegroupDeliverypointFieldIndex
	{
		///<summary>MessagegroupDeliverypointId. </summary>
		MessagegroupDeliverypointId,
		///<summary>MessagegroupId. </summary>
		MessagegroupId,
		///<summary>DeliverypointId. </summary>
		DeliverypointId,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>ParentCompanyId. </summary>
		ParentCompanyId,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: MessageRecipient.</summary>
	public enum MessageRecipientFieldIndex
	{
		///<summary>MessageRecipientId. </summary>
		MessageRecipientId,
		///<summary>ParentCompanyId. </summary>
		ParentCompanyId,
		///<summary>MessageId. </summary>
		MessageId,
		///<summary>CustomerId. </summary>
		CustomerId,
		///<summary>ClientId. </summary>
		ClientId,
		///<summary>DeliverypointId. </summary>
		DeliverypointId,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>Queued. </summary>
		Queued,
		///<summary>SentUTC. </summary>
		SentUTC,
		///<summary>CategoryId. </summary>
		CategoryId,
		///<summary>EntertainmentId. </summary>
		EntertainmentId,
		///<summary>ProductId. </summary>
		ProductId,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: MessageTemplate.</summary>
	public enum MessageTemplateFieldIndex
	{
		///<summary>MessageTemplateId. </summary>
		MessageTemplateId,
		///<summary>Title. </summary>
		Title,
		///<summary>Message. </summary>
		Message,
		///<summary>CompanyId. </summary>
		CompanyId,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>Name. </summary>
		Name,
		///<summary>Duration. </summary>
		Duration,
		///<summary>EntertainmentId. </summary>
		EntertainmentId,
		///<summary>MediaId. </summary>
		MediaId,
		///<summary>CategoryId. </summary>
		CategoryId,
		///<summary>ProductId. </summary>
		ProductId,
		///<summary>NotifyOnYes. </summary>
		NotifyOnYes,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>Url. </summary>
		Url,
		///<summary>PageId. </summary>
		PageId,
		///<summary>SiteId. </summary>
		SiteId,
		///<summary>ProductCategoryId. </summary>
		ProductCategoryId,
		///<summary>MessageLayoutType. </summary>
		MessageLayoutType,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: MessageTemplateCategory.</summary>
	public enum MessageTemplateCategoryFieldIndex
	{
		///<summary>MessageTemplateCategoryId. </summary>
		MessageTemplateCategoryId,
		///<summary>CompanyId. </summary>
		CompanyId,
		///<summary>Name. </summary>
		Name,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: MessageTemplateCategoryMessageTemplate.</summary>
	public enum MessageTemplateCategoryMessageTemplateFieldIndex
	{
		///<summary>MessageTemplateCategoryMessageTemplateId. </summary>
		MessageTemplateCategoryMessageTemplateId,
		///<summary>MessageTemplateCategoryId. </summary>
		MessageTemplateCategoryId,
		///<summary>MessageTemplateId. </summary>
		MessageTemplateId,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Module.</summary>
	public enum ModuleFieldIndex
	{
		///<summary>ModuleId. </summary>
		ModuleId,
		///<summary>NameSystem. </summary>
		NameSystem,
		///<summary>NameFull. </summary>
		NameFull,
		///<summary>NameShort. </summary>
		NameShort,
		///<summary>IconPath. </summary>
		IconPath,
		///<summary>DefaultUiElementTypeNameFull. </summary>
		DefaultUiElementTypeNameFull,
		///<summary>SortOrder. </summary>
		SortOrder,
		///<summary>LicensingFree. </summary>
		LicensingFree,
		///<summary>UserRightsFree. </summary>
		UserRightsFree,
		///<summary>ForLocalUseWhileSyncingIsUpdated. </summary>
		ForLocalUseWhileSyncingIsUpdated,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Netmessage.</summary>
	public enum NetmessageFieldIndex
	{
		///<summary>NetmessageId. </summary>
		NetmessageId,
		///<summary>SenderCustomerId. </summary>
		SenderCustomerId,
		///<summary>SenderClientId. </summary>
		SenderClientId,
		///<summary>SenderCompanyId. </summary>
		SenderCompanyId,
		///<summary>SenderDeliverypointId. </summary>
		SenderDeliverypointId,
		///<summary>ReceiverCustomerId. </summary>
		ReceiverCustomerId,
		///<summary>ReceiverClientId. </summary>
		ReceiverClientId,
		///<summary>ReceiverCompanyId. </summary>
		ReceiverCompanyId,
		///<summary>ReceiverDeliverypointId. </summary>
		ReceiverDeliverypointId,
		///<summary>MessageType. </summary>
		MessageType,
		///<summary>FieldValue1. </summary>
		FieldValue1,
		///<summary>FieldValue2. </summary>
		FieldValue2,
		///<summary>FieldValue3. </summary>
		FieldValue3,
		///<summary>FieldValue4. </summary>
		FieldValue4,
		///<summary>FieldValue5. </summary>
		FieldValue5,
		///<summary>FieldValue6. </summary>
		FieldValue6,
		///<summary>FieldValue7. </summary>
		FieldValue7,
		///<summary>FieldValue8. </summary>
		FieldValue8,
		///<summary>FieldValue9. </summary>
		FieldValue9,
		///<summary>FieldValue10. </summary>
		FieldValue10,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>SenderTerminalId. </summary>
		SenderTerminalId,
		///<summary>ReceiverTerminalId. </summary>
		ReceiverTerminalId,
		///<summary>Guid. </summary>
		Guid,
		///<summary>ReceiverDeliverypointgroupId. </summary>
		ReceiverDeliverypointgroupId,
		///<summary>Status. </summary>
		Status,
		///<summary>MessageTypeText. </summary>
		MessageTypeText,
		///<summary>StatusText. </summary>
		StatusText,
		///<summary>ErrorMessage. </summary>
		ErrorMessage,
		///<summary>MessageLog. </summary>
		MessageLog,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>SenderIdentifier. </summary>
		SenderIdentifier,
		///<summary>ReceiverIdentifier. </summary>
		ReceiverIdentifier,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: OptIn.</summary>
	public enum OptInFieldIndex
	{
		///<summary>OptInId. </summary>
		OptInId,
		///<summary>ParentCompanyId. </summary>
		ParentCompanyId,
		///<summary>OutletId. </summary>
		OutletId,
		///<summary>Email. </summary>
		Email,
		///<summary>PhoneNumber. </summary>
		PhoneNumber,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>CustomerName. </summary>
		CustomerName,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Order.</summary>
	public enum OrderFieldIndex
	{
		///<summary>OrderId. </summary>
		OrderId,
		///<summary>CustomerId. </summary>
		CustomerId,
		///<summary>CustomerFirstname. </summary>
		CustomerFirstname,
		///<summary>CustomerLastname. </summary>
		CustomerLastname,
		///<summary>CustomerLastnamePrefix. </summary>
		CustomerLastnamePrefix,
		///<summary>CustomerPhonenumber. </summary>
		CustomerPhonenumber,
		///<summary>ClientId. </summary>
		ClientId,
		///<summary>ClientMacAddress. </summary>
		ClientMacAddress,
		///<summary>CompanyId. </summary>
		CompanyId,
		///<summary>CompanyName. </summary>
		CompanyName,
		///<summary>CurrencyId. </summary>
		CurrencyId,
		///<summary>DeliverypointId. </summary>
		DeliverypointId,
		///<summary>DeliverypointName. </summary>
		DeliverypointName,
		///<summary>DeliverypointNumber. </summary>
		DeliverypointNumber,
		///<summary>ConfirmationCode. </summary>
		ConfirmationCode,
		///<summary>AgeVerificationType. </summary>
		AgeVerificationType,
		///<summary>Notes. </summary>
		Notes,
		///<summary>BenchmarkInformation. </summary>
		BenchmarkInformation,
		///<summary>Type. </summary>
		Type,
		///<summary>TypeText. </summary>
		TypeText,
		///<summary>Status. </summary>
		Status,
		///<summary>StatusText. </summary>
		StatusText,
		///<summary>ErrorSentByEmail. </summary>
		ErrorSentByEmail,
		///<summary>ErrorSentBySMS. </summary>
		ErrorSentBySMS,
		///<summary>Guid. </summary>
		Guid,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>RoutingLog. </summary>
		RoutingLog,
		///<summary>ErrorCode. </summary>
		ErrorCode,
		///<summary>ErrorText. </summary>
		ErrorText,
		///<summary>Printed. </summary>
		Printed,
		///<summary>MasterOrderId. </summary>
		MasterOrderId,
		///<summary>MobileOrder. </summary>
		MobileOrder,
		///<summary>Email. </summary>
		Email,
		///<summary>HotSOSServiceOrderId. </summary>
		HotSOSServiceOrderId,
		///<summary>DeliverypointgroupName. </summary>
		DeliverypointgroupName,
		///<summary>IsCustomerVip. </summary>
		IsCustomerVip,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>BenchmarkProcessedUTC. </summary>
		BenchmarkProcessedUTC,
		///<summary>ProcessedUTC. </summary>
		ProcessedUTC,
		///<summary>CurrencyCode. </summary>
		CurrencyCode,
		///<summary>AnalyticsPayLoad. </summary>
		AnalyticsPayLoad,
		///<summary>AnalyticsTrackingIds. </summary>
		AnalyticsTrackingIds,
		///<summary>TimeZoneOlsonId. </summary>
		TimeZoneOlsonId,
		///<summary>DeliverypointEnableAnalytics. </summary>
		DeliverypointEnableAnalytics,
		///<summary>PlaceTimeUTC. </summary>
		PlaceTimeUTC,
		///<summary>ExternalOrderId. </summary>
		ExternalOrderId,
		///<summary>ServiceMethodId. </summary>
		ServiceMethodId,
		///<summary>CheckoutMethodId. </summary>
		CheckoutMethodId,
		///<summary>CheckoutMethodName. </summary>
		CheckoutMethodName,
		///<summary>CheckoutMethodType. </summary>
		CheckoutMethodType,
		///<summary>ServiceMethodName. </summary>
		ServiceMethodName,
		///<summary>ServiceMethodType. </summary>
		ServiceMethodType,
		///<summary>CultureCode. </summary>
		CultureCode,
		///<summary>Total. </summary>
		Total,
		///<summary>SubTotal. </summary>
		SubTotal,
		///<summary>DeliveryInformationId. </summary>
		DeliveryInformationId,
		///<summary>ChargeToDeliverypointName. </summary>
		ChargeToDeliverypointName,
		///<summary>ChargeToDeliverypointNumber. </summary>
		ChargeToDeliverypointNumber,
		///<summary>ChargeToDeliverypointgroupName. </summary>
		ChargeToDeliverypointgroupName,
		///<summary>PricesIncludeTaxes. </summary>
		PricesIncludeTaxes,
		///<summary>OutletId. </summary>
		OutletId,
		///<summary>ChargeToDeliverypointId. </summary>
		ChargeToDeliverypointId,
		///<summary>OptInStatus. </summary>
		OptInStatus,
		///<summary>TaxBreakdown. </summary>
		TaxBreakdown,
		///<summary>CountryCode. </summary>
		CountryCode,
		///<summary>CoversCount. </summary>
		CoversCount,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: OrderHour.</summary>
	public enum OrderHourFieldIndex
	{
		///<summary>OrderHourId. </summary>
		OrderHourId,
		///<summary>ProductId. </summary>
		ProductId,
		///<summary>CategoryId. </summary>
		CategoryId,
		///<summary>SortOrder. </summary>
		SortOrder,
		///<summary>DayOfWeek. </summary>
		DayOfWeek,
		///<summary>TimeStart. </summary>
		TimeStart,
		///<summary>TimeEnd. </summary>
		TimeEnd,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>ParentCompanyId. </summary>
		ParentCompanyId,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Orderitem.</summary>
	public enum OrderitemFieldIndex
	{
		///<summary>OrderitemId. </summary>
		OrderitemId,
		///<summary>OrderId. </summary>
		OrderId,
		///<summary>ProductId. </summary>
		ProductId,
		///<summary>XProductDescription. </summary>
		XProductDescription,
		///<summary>ProductName. </summary>
		ProductName,
		///<summary>ProductPriceIn. </summary>
		ProductPriceIn,
		///<summary>Quantity. </summary>
		Quantity,
		///<summary>XPriceIn. </summary>
		XPriceIn,
		///<summary>VatPercentage. </summary>
		VatPercentage,
		///<summary>Notes. </summary>
		Notes,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>Guid. </summary>
		Guid,
		///<summary>CategoryId. </summary>
		CategoryId,
		///<summary>Color. </summary>
		Color,
		///<summary>CategoryName. </summary>
		CategoryName,
		///<summary>OrderSource. </summary>
		OrderSource,
		///<summary>ParentCompanyId. </summary>
		ParentCompanyId,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>CategoryPath. </summary>
		CategoryPath,
		///<summary>PriceLevelItemId. </summary>
		PriceLevelItemId,
		///<summary>PriceLevelLog. </summary>
		PriceLevelLog,
		///<summary>Type. </summary>
		Type,
		///<summary>PriceInTax. </summary>
		PriceInTax,
		///<summary>PriceExTax. </summary>
		PriceExTax,
		///<summary>PriceTotalInTax. </summary>
		PriceTotalInTax,
		///<summary>PriceTotalExTax. </summary>
		PriceTotalExTax,
		///<summary>TaxPercentage. </summary>
		TaxPercentage,
		///<summary>Tax. </summary>
		Tax,
		///<summary>TaxTariffId. </summary>
		TaxTariffId,
		///<summary>TaxTotal. </summary>
		TaxTotal,
		///<summary>TaxName. </summary>
		TaxName,
		///<summary>ExternalIdentifier. </summary>
		ExternalIdentifier,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: OrderitemAlterationitem.</summary>
	public enum OrderitemAlterationitemFieldIndex
	{
		///<summary>OrderitemAlterationitemId. </summary>
		OrderitemAlterationitemId,
		///<summary>OrderitemId. </summary>
		OrderitemId,
		///<summary>AlterationitemId. </summary>
		AlterationitemId,
		///<summary>AlterationName. </summary>
		AlterationName,
		///<summary>AlterationoptionName. </summary>
		AlterationoptionName,
		///<summary>AlterationoptionPriceIn. </summary>
		AlterationoptionPriceIn,
		///<summary>FormerAlterationitemId. </summary>
		FormerAlterationitemId,
		///<summary>Guid. </summary>
		Guid,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>Time. </summary>
		Time,
		///<summary>AlterationType. </summary>
		AlterationType,
		///<summary>Value. </summary>
		Value,
		///<summary>ParentCompanyId. </summary>
		ParentCompanyId,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>AlterationoptionType. </summary>
		AlterationoptionType,
		///<summary>PriceLevelItemId. </summary>
		PriceLevelItemId,
		///<summary>PriceLevelLog. </summary>
		PriceLevelLog,
		///<summary>AlterationId. </summary>
		AlterationId,
		///<summary>TimeUTC. </summary>
		TimeUTC,
		///<summary>OrderLevelEnabled. </summary>
		OrderLevelEnabled,
		///<summary>PriceInTax. </summary>
		PriceInTax,
		///<summary>PriceExTax. </summary>
		PriceExTax,
		///<summary>PriceTotalInTax. </summary>
		PriceTotalInTax,
		///<summary>PriceTotalExTax. </summary>
		PriceTotalExTax,
		///<summary>TaxPercentage. </summary>
		TaxPercentage,
		///<summary>Tax. </summary>
		Tax,
		///<summary>TaxTariffId. </summary>
		TaxTariffId,
		///<summary>TaxTotal. </summary>
		TaxTotal,
		///<summary>TaxName. </summary>
		TaxName,
		///<summary>AlterationoptionProductId. </summary>
		AlterationoptionProductId,
		///<summary>ExternalIdentifier. </summary>
		ExternalIdentifier,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: OrderitemAlterationitemTag.</summary>
	public enum OrderitemAlterationitemTagFieldIndex
	{
		///<summary>OrderitemAlterationitemTagId. </summary>
		OrderitemAlterationitemTagId,
		///<summary>OrderitemAlterationitemId. </summary>
		OrderitemAlterationitemId,
		///<summary>AlterationoptionId. </summary>
		AlterationoptionId,
		///<summary>TagId. </summary>
		TagId,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>ParentCompanyId. </summary>
		ParentCompanyId,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: OrderitemTag.</summary>
	public enum OrderitemTagFieldIndex
	{
		///<summary>OrderitemTagId. </summary>
		OrderitemTagId,
		///<summary>OrderitemId. </summary>
		OrderitemId,
		///<summary>ProductCategoryTagId. </summary>
		ProductCategoryTagId,
		///<summary>ProductId. </summary>
		ProductId,
		///<summary>TagId. </summary>
		TagId,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>ParentCompanyId. </summary>
		ParentCompanyId,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: OrderNotificationLog.</summary>
	public enum OrderNotificationLogFieldIndex
	{
		///<summary>OrderNotificationLogId. </summary>
		OrderNotificationLogId,
		///<summary>OrderId. </summary>
		OrderId,
		///<summary>ParentCompanyId. </summary>
		ParentCompanyId,
		///<summary>Type. </summary>
		Type,
		///<summary>Method. </summary>
		Method,
		///<summary>Status. </summary>
		Status,
		///<summary>Receiver. </summary>
		Receiver,
		///<summary>Subject. </summary>
		Subject,
		///<summary>Message. </summary>
		Message,
		///<summary>Log. </summary>
		Log,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>SentUTC. </summary>
		SentUTC,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: OrderRoutestephandler.</summary>
	public enum OrderRoutestephandlerFieldIndex
	{
		///<summary>OrderRoutestephandlerId. </summary>
		OrderRoutestephandlerId,
		///<summary>OrderId. </summary>
		OrderId,
		///<summary>TerminalId. </summary>
		TerminalId,
		///<summary>Number. </summary>
		Number,
		///<summary>HandlerType. </summary>
		HandlerType,
		///<summary>PrintReportType. </summary>
		PrintReportType,
		///<summary>Status. </summary>
		Status,
		///<summary>FieldValue1. </summary>
		FieldValue1,
		///<summary>FieldValue2. </summary>
		FieldValue2,
		///<summary>FieldValue3. </summary>
		FieldValue3,
		///<summary>FieldValue4. </summary>
		FieldValue4,
		///<summary>FieldValue5. </summary>
		FieldValue5,
		///<summary>FieldValue6. </summary>
		FieldValue6,
		///<summary>FieldValue7. </summary>
		FieldValue7,
		///<summary>FieldValue8. </summary>
		FieldValue8,
		///<summary>FieldValue9. </summary>
		FieldValue9,
		///<summary>FieldValue10. </summary>
		FieldValue10,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>ErrorCode. </summary>
		ErrorCode,
		///<summary>ErrorText. </summary>
		ErrorText,
		///<summary>Guid. </summary>
		Guid,
		///<summary>Timeout. </summary>
		Timeout,
		///<summary>LogAlways. </summary>
		LogAlways,
		///<summary>ForwardedFromTerminalId. </summary>
		ForwardedFromTerminalId,
		///<summary>EscalationStep. </summary>
		EscalationStep,
		///<summary>ContinueOnFailure. </summary>
		ContinueOnFailure,
		///<summary>CompleteRouteOnComplete. </summary>
		CompleteRouteOnComplete,
		///<summary>EscalationRouteId. </summary>
		EscalationRouteId,
		///<summary>SupportpoolId. </summary>
		SupportpoolId,
		///<summary>RetrievalSupportNotificationSent. </summary>
		RetrievalSupportNotificationSent,
		///<summary>BeingHandledSupportNotificationSent. </summary>
		BeingHandledSupportNotificationSent,
		///<summary>RetrievalSupportNotificationTimeoutMinutes. </summary>
		RetrievalSupportNotificationTimeoutMinutes,
		///<summary>BeingHandledSupportNotificationTimeoutMinutes. </summary>
		BeingHandledSupportNotificationTimeoutMinutes,
		///<summary>HandlerTypeText. </summary>
		HandlerTypeText,
		///<summary>StatusText. </summary>
		StatusText,
		///<summary>OriginatedFromRoutestepHandlerId. </summary>
		OriginatedFromRoutestepHandlerId,
		///<summary>ParentCompanyId. </summary>
		ParentCompanyId,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>TimeoutExpiresUTC. </summary>
		TimeoutExpiresUTC,
		///<summary>RetrievalSupportNotificationTimeoutUTC. </summary>
		RetrievalSupportNotificationTimeoutUTC,
		///<summary>BeingHandledSupportNotificationTimeoutUTC. </summary>
		BeingHandledSupportNotificationTimeoutUTC,
		///<summary>WaitingToBeRetrievedUTC. </summary>
		WaitingToBeRetrievedUTC,
		///<summary>RetrievedByHandlerUTC. </summary>
		RetrievedByHandlerUTC,
		///<summary>BeingHandledUTC. </summary>
		BeingHandledUTC,
		///<summary>CompletedUTC. </summary>
		CompletedUTC,
		///<summary>ExternalSystemId. </summary>
		ExternalSystemId,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: OrderRoutestephandlerHistory.</summary>
	public enum OrderRoutestephandlerHistoryFieldIndex
	{
		///<summary>OrderRoutestephandlerHistoryId. </summary>
		OrderRoutestephandlerHistoryId,
		///<summary>Guid. </summary>
		Guid,
		///<summary>OrderId. </summary>
		OrderId,
		///<summary>TerminalId. </summary>
		TerminalId,
		///<summary>Number. </summary>
		Number,
		///<summary>HandlerType. </summary>
		HandlerType,
		///<summary>PrintReportType. </summary>
		PrintReportType,
		///<summary>Status. </summary>
		Status,
		///<summary>Timeout. </summary>
		Timeout,
		///<summary>ErrorCode. </summary>
		ErrorCode,
		///<summary>ErrorText. </summary>
		ErrorText,
		///<summary>FieldValue1. </summary>
		FieldValue1,
		///<summary>FieldValue2. </summary>
		FieldValue2,
		///<summary>FieldValue3. </summary>
		FieldValue3,
		///<summary>FieldValue4. </summary>
		FieldValue4,
		///<summary>FieldValue5. </summary>
		FieldValue5,
		///<summary>FieldValue6. </summary>
		FieldValue6,
		///<summary>FieldValue7. </summary>
		FieldValue7,
		///<summary>FieldValue8. </summary>
		FieldValue8,
		///<summary>FieldValue9. </summary>
		FieldValue9,
		///<summary>FieldValue10. </summary>
		FieldValue10,
		///<summary>LogAlways. </summary>
		LogAlways,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>ForwardedFromTerminalId. </summary>
		ForwardedFromTerminalId,
		///<summary>ContinueOnFailure. </summary>
		ContinueOnFailure,
		///<summary>CompleteRouteOnComplete. </summary>
		CompleteRouteOnComplete,
		///<summary>EscalationRouteId. </summary>
		EscalationRouteId,
		///<summary>EscalationStep. </summary>
		EscalationStep,
		///<summary>SupportpoolId. </summary>
		SupportpoolId,
		///<summary>RetrievalSupportNotificationTimeoutMinutes. </summary>
		RetrievalSupportNotificationTimeoutMinutes,
		///<summary>RetrievalSupportNotificationSent. </summary>
		RetrievalSupportNotificationSent,
		///<summary>BeingHandledSupportNotificationTimeoutMinutes. </summary>
		BeingHandledSupportNotificationTimeoutMinutes,
		///<summary>BeingHandledSupportNotificationSent. </summary>
		BeingHandledSupportNotificationSent,
		///<summary>HandlerTypeText. </summary>
		HandlerTypeText,
		///<summary>StatusText. </summary>
		StatusText,
		///<summary>OriginatedFromRoutestepHandlerId. </summary>
		OriginatedFromRoutestepHandlerId,
		///<summary>ParentCompanyId. </summary>
		ParentCompanyId,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>TimeoutExpiresUTC. </summary>
		TimeoutExpiresUTC,
		///<summary>RetrievalSupportNotificationTimeoutUTC. </summary>
		RetrievalSupportNotificationTimeoutUTC,
		///<summary>BeingHandledSupportNotificationTimeoutUTC. </summary>
		BeingHandledSupportNotificationTimeoutUTC,
		///<summary>WaitingToBeRetrievedUTC. </summary>
		WaitingToBeRetrievedUTC,
		///<summary>RetrievedByHandlerUTC. </summary>
		RetrievedByHandlerUTC,
		///<summary>BeingHandledUTC. </summary>
		BeingHandledUTC,
		///<summary>CompletedUTC. </summary>
		CompletedUTC,
		///<summary>ExternalSystemId. </summary>
		ExternalSystemId,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Outlet.</summary>
	public enum OutletFieldIndex
	{
		///<summary>OutletId. </summary>
		OutletId,
		///<summary>CompanyId. </summary>
		CompanyId,
		///<summary>Name. </summary>
		Name,
		///<summary>TippingActive. </summary>
		TippingActive,
		///<summary>TippingDefaultPercentage. </summary>
		TippingDefaultPercentage,
		///<summary>TippingMinimumPercentage. </summary>
		TippingMinimumPercentage,
		///<summary>TippingStepSize. </summary>
		TippingStepSize,
		///<summary>TippingProductId. </summary>
		TippingProductId,
		///<summary>CustomerDetailsSectionActive. </summary>
		CustomerDetailsSectionActive,
		///<summary>CustomerDetailsPhoneRequired. </summary>
		CustomerDetailsPhoneRequired,
		///<summary>CustomerDetailsEmailRequired. </summary>
		CustomerDetailsEmailRequired,
		///<summary>ShowPricesIncludingTax. </summary>
		ShowPricesIncludingTax,
		///<summary>ShowTaxBreakDownOnBasket. </summary>
		ShowTaxBreakDownOnBasket,
		///<summary>ShowTaxBreakDownOnCheckout. </summary>
		ShowTaxBreakDownOnCheckout,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>ReceiptEmail. </summary>
		ReceiptEmail,
		///<summary>OutletOperationalStateId. </summary>
		OutletOperationalStateId,
		///<summary>Latitude. </summary>
		Latitude,
		///<summary>Longitude. </summary>
		Longitude,
		///<summary>DeliveryChargeProductId. </summary>
		DeliveryChargeProductId,
		///<summary>ServiceChargeProductId. </summary>
		ServiceChargeProductId,
		///<summary>CustomerDetailsNameRequired. </summary>
		CustomerDetailsNameRequired,
		///<summary>OutletSellerInformationId. </summary>
		OutletSellerInformationId,
		///<summary>ShowTaxBreakdown. </summary>
		ShowTaxBreakdown,
		///<summary>OptInType. </summary>
		OptInType,
		///<summary>TaxDisclaimer. </summary>
		TaxDisclaimer,
		///<summary>TaxNumberTitle. </summary>
		TaxNumberTitle,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: OutletOperationalState.</summary>
	public enum OutletOperationalStateFieldIndex
	{
		///<summary>OutletOperationalStateId. </summary>
		OutletOperationalStateId,
		///<summary>WaitTime. </summary>
		WaitTime,
		///<summary>OrderIntakeDisabled. </summary>
		OrderIntakeDisabled,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: OutletSellerInformation.</summary>
	public enum OutletSellerInformationFieldIndex
	{
		///<summary>OutletSellerInformationId. </summary>
		OutletSellerInformationId,
		///<summary>SellerName. </summary>
		SellerName,
		///<summary>Address. </summary>
		Address,
		///<summary>Phonenumber. </summary>
		Phonenumber,
		///<summary>Email. </summary>
		Email,
		///<summary>VatNumber. </summary>
		VatNumber,
		///<summary>ReceiptReceiversEmail. </summary>
		ReceiptReceiversEmail,
		///<summary>ReceiptReceiversSms. </summary>
		ReceiptReceiversSms,
		///<summary>SmsOriginator. </summary>
		SmsOriginator,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>ReceiveOrderReceiptNotifications. </summary>
		ReceiveOrderReceiptNotifications,
		///<summary>ReceiveOrderConfirmationNotifications. </summary>
		ReceiveOrderConfirmationNotifications,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Page.</summary>
	public enum PageFieldIndex
	{
		///<summary>PageId. </summary>
		PageId,
		///<summary>Name. </summary>
		Name,
		///<summary>SiteId. </summary>
		SiteId,
		///<summary>ParentPageId. </summary>
		ParentPageId,
		///<summary>PageType. </summary>
		PageType,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>PageTemplateId. </summary>
		PageTemplateId,
		///<summary>SortOrder. </summary>
		SortOrder,
		///<summary>Visible. </summary>
		Visible,
		///<summary>ParentCompanyId. </summary>
		ParentCompanyId,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: PageElement.</summary>
	public enum PageElementFieldIndex
	{
		///<summary>PageElementId. </summary>
		PageElementId,
		///<summary>PageId. </summary>
		PageId,
		///<summary>LanguageId. </summary>
		LanguageId,
		///<summary>PageElementType. </summary>
		PageElementType,
		///<summary>StringValue1. </summary>
		StringValue1,
		///<summary>StringValue2. </summary>
		StringValue2,
		///<summary>StringValue3. </summary>
		StringValue3,
		///<summary>StringValue4. </summary>
		StringValue4,
		///<summary>StringValue5. </summary>
		StringValue5,
		///<summary>IntValue1. </summary>
		IntValue1,
		///<summary>IntValue2. </summary>
		IntValue2,
		///<summary>IntValue3. </summary>
		IntValue3,
		///<summary>IntValue4. </summary>
		IntValue4,
		///<summary>IntValue5. </summary>
		IntValue5,
		///<summary>BoolValue1. </summary>
		BoolValue1,
		///<summary>BoolValue2. </summary>
		BoolValue2,
		///<summary>BoolValue3. </summary>
		BoolValue3,
		///<summary>BoolValue4. </summary>
		BoolValue4,
		///<summary>BoolValue5. </summary>
		BoolValue5,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>SystemName. </summary>
		SystemName,
		///<summary>ParentCompanyId. </summary>
		ParentCompanyId,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>CultureCode. </summary>
		CultureCode,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: PageLanguage.</summary>
	public enum PageLanguageFieldIndex
	{
		///<summary>PageLanguageId. </summary>
		PageLanguageId,
		///<summary>PageId. </summary>
		PageId,
		///<summary>LanguageId. </summary>
		LanguageId,
		///<summary>Name. </summary>
		Name,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>ParentCompanyId. </summary>
		ParentCompanyId,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: PageTemplate.</summary>
	public enum PageTemplateFieldIndex
	{
		///<summary>PageTemplateId. </summary>
		PageTemplateId,
		///<summary>Name. </summary>
		Name,
		///<summary>PageType. </summary>
		PageType,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>ParentPageTemplateId. </summary>
		ParentPageTemplateId,
		///<summary>SiteTemplateId. </summary>
		SiteTemplateId,
		///<summary>SortOrder. </summary>
		SortOrder,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: PageTemplateElement.</summary>
	public enum PageTemplateElementFieldIndex
	{
		///<summary>PageTemplateElementId. </summary>
		PageTemplateElementId,
		///<summary>PageTemplateId. </summary>
		PageTemplateId,
		///<summary>LanguageId. </summary>
		LanguageId,
		///<summary>PageElementType. </summary>
		PageElementType,
		///<summary>SystemName. </summary>
		SystemName,
		///<summary>StringValue1. </summary>
		StringValue1,
		///<summary>StringValue2. </summary>
		StringValue2,
		///<summary>StringValue3. </summary>
		StringValue3,
		///<summary>StringValue4. </summary>
		StringValue4,
		///<summary>StringValue5. </summary>
		StringValue5,
		///<summary>IntValue1. </summary>
		IntValue1,
		///<summary>IntValue2. </summary>
		IntValue2,
		///<summary>IntValue3. </summary>
		IntValue3,
		///<summary>IntValue4. </summary>
		IntValue4,
		///<summary>IntValue5. </summary>
		IntValue5,
		///<summary>BoolValue1. </summary>
		BoolValue1,
		///<summary>BoolValue2. </summary>
		BoolValue2,
		///<summary>BoolValue3. </summary>
		BoolValue3,
		///<summary>BoolValue4. </summary>
		BoolValue4,
		///<summary>BoolValue5. </summary>
		BoolValue5,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>CultureCode. </summary>
		CultureCode,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: PageTemplateLanguage.</summary>
	public enum PageTemplateLanguageFieldIndex
	{
		///<summary>PageTemplateLanguageId. </summary>
		PageTemplateLanguageId,
		///<summary>PageTemplateId. </summary>
		PageTemplateId,
		///<summary>LanguageId. </summary>
		LanguageId,
		///<summary>Name. </summary>
		Name,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: PaymentIntegrationConfiguration.</summary>
	public enum PaymentIntegrationConfigurationFieldIndex
	{
		///<summary>PaymentIntegrationConfigurationId. </summary>
		PaymentIntegrationConfigurationId,
		///<summary>PaymentProviderId. </summary>
		PaymentProviderId,
		///<summary>DisplayName. </summary>
		DisplayName,
		///<summary>MerchantName. </summary>
		MerchantName,
		///<summary>AdyenMerchantCode. </summary>
		AdyenMerchantCode,
		///<summary>GooglePayMerchantIdentifier. </summary>
		GooglePayMerchantIdentifier,
		///<summary>ApplePayMerchantIdentifier. </summary>
		ApplePayMerchantIdentifier,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>CompanyId. </summary>
		CompanyId,
		///<summary>PaymentProcessorInfo. </summary>
		PaymentProcessorInfo,
		///<summary>PaymentProcessorInfoFooter. </summary>
		PaymentProcessorInfoFooter,
		///<summary>AdyenAccountCode. </summary>
		AdyenAccountCode,
		///<summary>AdyenAccountName. </summary>
		AdyenAccountName,
		///<summary>AdyenAccountHolderCode. </summary>
		AdyenAccountHolderCode,
		///<summary>AdyenAccountHolderName. </summary>
		AdyenAccountHolderName,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: PaymentProvider.</summary>
	public enum PaymentProviderFieldIndex
	{
		///<summary>PaymentProviderId. </summary>
		PaymentProviderId,
		///<summary>CompanyId. </summary>
		CompanyId,
		///<summary>Type. </summary>
		Type,
		///<summary>Name. </summary>
		Name,
		///<summary>Environment. </summary>
		Environment,
		///<summary>ApiKey. </summary>
		ApiKey,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>LiveEndpointUrlPrefix. </summary>
		LiveEndpointUrlPrefix,
		///<summary>OriginDomain. </summary>
		OriginDomain,
		///<summary>PaymentProcessorInfo. </summary>
		PaymentProcessorInfo,
		///<summary>PaymentProcessorInfoFooter. </summary>
		PaymentProcessorInfoFooter,
		///<summary>AdyenUsername. </summary>
		AdyenUsername,
		///<summary>AdyenPassword. </summary>
		AdyenPassword,
		///<summary>OmisePublicKey. </summary>
		OmisePublicKey,
		///<summary>OmiseSecretKey. </summary>
		OmiseSecretKey,
		///<summary>SkinCode. </summary>
		SkinCode,
		///<summary>HmacKey. </summary>
		HmacKey,
		///<summary>ClientKey. </summary>
		ClientKey,
		///<summary>AdyenMerchantCode. </summary>
		AdyenMerchantCode,
		///<summary>GooglePayMerchantIdentifier. </summary>
		GooglePayMerchantIdentifier,
		///<summary>ApplePayMerchantIdentifier. </summary>
		ApplePayMerchantIdentifier,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: PaymentProviderCompany.</summary>
	public enum PaymentProviderCompanyFieldIndex
	{
		///<summary>PaymentProviderCompanyId. </summary>
		PaymentProviderCompanyId,
		///<summary>PaymentProviderId. </summary>
		PaymentProviderId,
		///<summary>CompanyId. </summary>
		CompanyId,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: PaymentTransaction.</summary>
	public enum PaymentTransactionFieldIndex
	{
		///<summary>PaymentTransactionId. </summary>
		PaymentTransactionId,
		///<summary>OrderId. </summary>
		OrderId,
		///<summary>ReferenceId. </summary>
		ReferenceId,
		///<summary>MerchantReference. </summary>
		MerchantReference,
		///<summary>PaymentMethod. </summary>
		PaymentMethod,
		///<summary>Status. </summary>
		Status,
		///<summary>PaymentData. </summary>
		PaymentData,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>MerchantAccount. </summary>
		MerchantAccount,
		///<summary>CardSummary. </summary>
		CardSummary,
		///<summary>AuthCode. </summary>
		AuthCode,
		///<summary>PaymentEnvironment. </summary>
		PaymentEnvironment,
		///<summary>PaymentProviderType. </summary>
		PaymentProviderType,
		///<summary>PaymentProviderName. </summary>
		PaymentProviderName,
		///<summary>PaymentProviderId. </summary>
		PaymentProviderId,
		///<summary>PaymentIntegrationConfigurationId. </summary>
		PaymentIntegrationConfigurationId,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: PaymentTransactionLog.</summary>
	public enum PaymentTransactionLogFieldIndex
	{
		///<summary>PaymentTransactionLogId. </summary>
		PaymentTransactionLogId,
		///<summary>PaymentTransactionId. </summary>
		PaymentTransactionId,
		///<summary>EventCode. </summary>
		EventCode,
		///<summary>EventText. </summary>
		EventText,
		///<summary>EventDate. </summary>
		EventDate,
		///<summary>Success. </summary>
		Success,
		///<summary>RawResponse. </summary>
		RawResponse,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>LogType. </summary>
		LogType,
		///<summary>Amount. </summary>
		Amount,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: PaymentTransactionSplit.</summary>
	public enum PaymentTransactionSplitFieldIndex
	{
		///<summary>PaymentTransactionSplit. </summary>
		PaymentTransactionSplit,
		///<summary>PaymentTransactionId. </summary>
		PaymentTransactionId,
		///<summary>ReferenceId. </summary>
		ReferenceId,
		///<summary>SplitType. </summary>
		SplitType,
		///<summary>Amount. </summary>
		Amount,
		///<summary>AccountCode. </summary>
		AccountCode,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: PmsActionRule.</summary>
	public enum PmsActionRuleFieldIndex
	{
		///<summary>PmsActionRuleId. </summary>
		PmsActionRuleId,
		///<summary>ParentCompanyId. </summary>
		ParentCompanyId,
		///<summary>PmsReportConfigurationId. </summary>
		PmsReportConfigurationId,
		///<summary>Active. </summary>
		Active,
		///<summary>Type. </summary>
		Type,
		///<summary>ScheduledMessageId. </summary>
		ScheduledMessageId,
		///<summary>ClientConfigurationId. </summary>
		ClientConfigurationId,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>MessagegroupId. </summary>
		MessagegroupId,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: PmsReportColumn.</summary>
	public enum PmsReportColumnFieldIndex
	{
		///<summary>PmsReportColumnId. </summary>
		PmsReportColumnId,
		///<summary>ParentCompanyId. </summary>
		ParentCompanyId,
		///<summary>PmsReportConfigurationId. </summary>
		PmsReportConfigurationId,
		///<summary>FriendlyName. </summary>
		FriendlyName,
		///<summary>DataType. </summary>
		DataType,
		///<summary>Format. </summary>
		Format,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>ColumnIndex. </summary>
		ColumnIndex,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: PmsReportConfiguration.</summary>
	public enum PmsReportConfigurationFieldIndex
	{
		///<summary>PmsReportConfigurationId. </summary>
		PmsReportConfigurationId,
		///<summary>CompanyId. </summary>
		CompanyId,
		///<summary>Name. </summary>
		Name,
		///<summary>Email. </summary>
		Email,
		///<summary>ColumnSeparationType. </summary>
		ColumnSeparationType,
		///<summary>RoomNumberColumnId. </summary>
		RoomNumberColumnId,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>PmsReportType. </summary>
		PmsReportType,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: PmsRule.</summary>
	public enum PmsRuleFieldIndex
	{
		///<summary>PmsRuleId. </summary>
		PmsRuleId,
		///<summary>ParentCompanyId. </summary>
		ParentCompanyId,
		///<summary>PmsActionRuleId. </summary>
		PmsActionRuleId,
		///<summary>PmsReportColumnId. </summary>
		PmsReportColumnId,
		///<summary>RuleType. </summary>
		RuleType,
		///<summary>Value. </summary>
		Value,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: PointOfInterest.</summary>
	public enum PointOfInterestFieldIndex
	{
		///<summary>PointOfInterestId. </summary>
		PointOfInterestId,
		///<summary>Name. </summary>
		Name,
		///<summary>Addressline1. </summary>
		Addressline1,
		///<summary>Addressline2. </summary>
		Addressline2,
		///<summary>Addressline3. </summary>
		Addressline3,
		///<summary>Zipcode. </summary>
		Zipcode,
		///<summary>City. </summary>
		City,
		///<summary>CountryId. </summary>
		CountryId,
		///<summary>Latitude. </summary>
		Latitude,
		///<summary>Longitude. </summary>
		Longitude,
		///<summary>Telephone. </summary>
		Telephone,
		///<summary>Fax. </summary>
		Fax,
		///<summary>Website. </summary>
		Website,
		///<summary>Email. </summary>
		Email,
		///<summary>Visible. </summary>
		Visible,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>ActionButtonId. </summary>
		ActionButtonId,
		///<summary>CostIndicationType. </summary>
		CostIndicationType,
		///<summary>CostIndicationValue. </summary>
		CostIndicationValue,
		///<summary>Description. </summary>
		Description,
		///<summary>DescriptionSingleLine. </summary>
		DescriptionSingleLine,
		///<summary>ActionButtonUrlMobile. </summary>
		ActionButtonUrlMobile,
		///<summary>ActionButtonUrlTablet. </summary>
		ActionButtonUrlTablet,
		///<summary>CurrencyId. </summary>
		CurrencyId,
		///<summary>ExternalId. </summary>
		ExternalId,
		///<summary>NameOverridden. </summary>
		NameOverridden,
		///<summary>DescriptionOverridden. </summary>
		DescriptionOverridden,
		///<summary>DescriptionSingleLineOverridden. </summary>
		DescriptionSingleLineOverridden,
		///<summary>Addressline1Overridden. </summary>
		Addressline1Overridden,
		///<summary>Addressline2Overridden. </summary>
		Addressline2Overridden,
		///<summary>Addressline3Overridden. </summary>
		Addressline3Overridden,
		///<summary>ZipcodeOverridden. </summary>
		ZipcodeOverridden,
		///<summary>CityOverridden. </summary>
		CityOverridden,
		///<summary>CountryIdOverridden. </summary>
		CountryIdOverridden,
		///<summary>CurrencyIdOverridden. </summary>
		CurrencyIdOverridden,
		///<summary>LatitudeOverridden. </summary>
		LatitudeOverridden,
		///<summary>LongitudeOverridden. </summary>
		LongitudeOverridden,
		///<summary>TelephoneOverridden. </summary>
		TelephoneOverridden,
		///<summary>FaxOverridden. </summary>
		FaxOverridden,
		///<summary>WebsiteOverridden. </summary>
		WebsiteOverridden,
		///<summary>EmailOverridden. </summary>
		EmailOverridden,
		///<summary>CostIndicationTypeOverridden. </summary>
		CostIndicationTypeOverridden,
		///<summary>CostIndicationValueOverridden. </summary>
		CostIndicationValueOverridden,
		///<summary>ActionButtonIdOverridden. </summary>
		ActionButtonIdOverridden,
		///<summary>ActionButtonUrlTabletOverridden. </summary>
		ActionButtonUrlTabletOverridden,
		///<summary>ActionButtonUrlMobileOverridden. </summary>
		ActionButtonUrlMobileOverridden,
		///<summary>BusinesshoursOverridden. </summary>
		BusinesshoursOverridden,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>LastModifiedUTC. </summary>
		LastModifiedUTC,
		///<summary>TimeZoneIdOverridden. </summary>
		TimeZoneIdOverridden,
		///<summary>TimeZoneId. </summary>
		TimeZoneId,
		///<summary>CountryCode. </summary>
		CountryCode,
		///<summary>CountryCodeOverridden. </summary>
		CountryCodeOverridden,
		///<summary>CurrencyCode. </summary>
		CurrencyCode,
		///<summary>CurrencyCodeOverridden. </summary>
		CurrencyCodeOverridden,
		///<summary>CultureCode. </summary>
		CultureCode,
		///<summary>CultureCodeOverridden. </summary>
		CultureCodeOverridden,
		///<summary>TimeZoneOlsonId. </summary>
		TimeZoneOlsonId,
		///<summary>TimeZoneOlsonIdOverridden. </summary>
		TimeZoneOlsonIdOverridden,
		///<summary>Floor. </summary>
		Floor,
		///<summary>DisplayDistance. </summary>
		DisplayDistance,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: PointOfInterestAmenity.</summary>
	public enum PointOfInterestAmenityFieldIndex
	{
		///<summary>PointOfInterestAmenityId. </summary>
		PointOfInterestAmenityId,
		///<summary>PointOfInterestId. </summary>
		PointOfInterestId,
		///<summary>AmenityId. </summary>
		AmenityId,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: PointOfInterestLanguage.</summary>
	public enum PointOfInterestLanguageFieldIndex
	{
		///<summary>PointOfInterestLanguageId. </summary>
		PointOfInterestLanguageId,
		///<summary>PointOfInterestId. </summary>
		PointOfInterestId,
		///<summary>LanguageId. </summary>
		LanguageId,
		///<summary>Description. </summary>
		Description,
		///<summary>DescriptionSingleLine. </summary>
		DescriptionSingleLine,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>VenuePageDescription. </summary>
		VenuePageDescription,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: PointOfInterestVenueCategory.</summary>
	public enum PointOfInterestVenueCategoryFieldIndex
	{
		///<summary>PointOfInterestVenueCategoryId. </summary>
		PointOfInterestVenueCategoryId,
		///<summary>PointOfInterestId. </summary>
		PointOfInterestId,
		///<summary>VenueCategoryId. </summary>
		VenueCategoryId,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Posalteration.</summary>
	public enum PosalterationFieldIndex
	{
		///<summary>PosalterationId. </summary>
		PosalterationId,
		///<summary>CompanyId. </summary>
		CompanyId,
		///<summary>ExternalId. </summary>
		ExternalId,
		///<summary>Name. </summary>
		Name,
		///<summary>MinOptions. </summary>
		MinOptions,
		///<summary>MaxOptions. </summary>
		MaxOptions,
		///<summary>FieldValue1. </summary>
		FieldValue1,
		///<summary>FieldValue2. </summary>
		FieldValue2,
		///<summary>FieldValue3. </summary>
		FieldValue3,
		///<summary>FieldValue4. </summary>
		FieldValue4,
		///<summary>FieldValue5. </summary>
		FieldValue5,
		///<summary>FieldValue6. </summary>
		FieldValue6,
		///<summary>FieldValue7. </summary>
		FieldValue7,
		///<summary>FieldValue8. </summary>
		FieldValue8,
		///<summary>FieldValue9. </summary>
		FieldValue9,
		///<summary>FieldValue10. </summary>
		FieldValue10,
		///<summary>CreatedInBatchId. </summary>
		CreatedInBatchId,
		///<summary>UpdatedInBatchId. </summary>
		UpdatedInBatchId,
		///<summary>SynchronisationBatchId. </summary>
		SynchronisationBatchId,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>RevenueCenter. </summary>
		RevenueCenter,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Posalterationitem.</summary>
	public enum PosalterationitemFieldIndex
	{
		///<summary>PosalterationitemId. </summary>
		PosalterationitemId,
		///<summary>CompanyId. </summary>
		CompanyId,
		///<summary>ExternalPosalterationId. </summary>
		ExternalPosalterationId,
		///<summary>ExternalPosalterationoptionId. </summary>
		ExternalPosalterationoptionId,
		///<summary>ExternalId. </summary>
		ExternalId,
		///<summary>SelectedOnDefault. </summary>
		SelectedOnDefault,
		///<summary>SortOrder. </summary>
		SortOrder,
		///<summary>FieldValue1. </summary>
		FieldValue1,
		///<summary>FieldValue2. </summary>
		FieldValue2,
		///<summary>FieldValue3. </summary>
		FieldValue3,
		///<summary>FieldValue4. </summary>
		FieldValue4,
		///<summary>FieldValue5. </summary>
		FieldValue5,
		///<summary>FieldValue6. </summary>
		FieldValue6,
		///<summary>FieldValue7. </summary>
		FieldValue7,
		///<summary>FieldValue8. </summary>
		FieldValue8,
		///<summary>FieldValue9. </summary>
		FieldValue9,
		///<summary>FieldValue10. </summary>
		FieldValue10,
		///<summary>DeletedFromCms. </summary>
		DeletedFromCms,
		///<summary>CreatedInBatchId. </summary>
		CreatedInBatchId,
		///<summary>UpdatedInBatchId. </summary>
		UpdatedInBatchId,
		///<summary>SynchronisationBatchId. </summary>
		SynchronisationBatchId,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>RevenueCenter. </summary>
		RevenueCenter,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Posalterationoption.</summary>
	public enum PosalterationoptionFieldIndex
	{
		///<summary>PosalterationoptionId. </summary>
		PosalterationoptionId,
		///<summary>CompanyId. </summary>
		CompanyId,
		///<summary>ExternalId. </summary>
		ExternalId,
		///<summary>Name. </summary>
		Name,
		///<summary>PriceAddition. </summary>
		PriceAddition,
		///<summary>PriceIn. </summary>
		PriceIn,
		///<summary>Description. </summary>
		Description,
		///<summary>PosproductExternalId. </summary>
		PosproductExternalId,
		///<summary>FieldValue1. </summary>
		FieldValue1,
		///<summary>FieldValue2. </summary>
		FieldValue2,
		///<summary>FieldValue3. </summary>
		FieldValue3,
		///<summary>FieldValue4. </summary>
		FieldValue4,
		///<summary>FieldValue5. </summary>
		FieldValue5,
		///<summary>FieldValue6. </summary>
		FieldValue6,
		///<summary>FieldValue7. </summary>
		FieldValue7,
		///<summary>FieldValue8. </summary>
		FieldValue8,
		///<summary>FieldValue9. </summary>
		FieldValue9,
		///<summary>FieldValue10. </summary>
		FieldValue10,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>CreatedInBatchId. </summary>
		CreatedInBatchId,
		///<summary>UpdatedInBatchId. </summary>
		UpdatedInBatchId,
		///<summary>SynchronisationBatchId. </summary>
		SynchronisationBatchId,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>RevenueCenter. </summary>
		RevenueCenter,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Poscategory.</summary>
	public enum PoscategoryFieldIndex
	{
		///<summary>PoscategoryId. </summary>
		PoscategoryId,
		///<summary>CompanyId. </summary>
		CompanyId,
		///<summary>ExternalId. </summary>
		ExternalId,
		///<summary>Name. </summary>
		Name,
		///<summary>FieldValue1. </summary>
		FieldValue1,
		///<summary>FieldValue2. </summary>
		FieldValue2,
		///<summary>FieldValue3. </summary>
		FieldValue3,
		///<summary>FieldValue4. </summary>
		FieldValue4,
		///<summary>FieldValue5. </summary>
		FieldValue5,
		///<summary>FieldValue6. </summary>
		FieldValue6,
		///<summary>FieldValue7. </summary>
		FieldValue7,
		///<summary>FieldValue8. </summary>
		FieldValue8,
		///<summary>FieldValue9. </summary>
		FieldValue9,
		///<summary>FieldValue10. </summary>
		FieldValue10,
		///<summary>CreatedInBatchId. </summary>
		CreatedInBatchId,
		///<summary>UpdatedInBatchId. </summary>
		UpdatedInBatchId,
		///<summary>SynchronisationBatchId. </summary>
		SynchronisationBatchId,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>RevenueCenter. </summary>
		RevenueCenter,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Posdeliverypoint.</summary>
	public enum PosdeliverypointFieldIndex
	{
		///<summary>PosdeliverypointId. </summary>
		PosdeliverypointId,
		///<summary>CompanyId. </summary>
		CompanyId,
		///<summary>ExternalId. </summary>
		ExternalId,
		///<summary>ExternalPosdeliverypointgroupId. </summary>
		ExternalPosdeliverypointgroupId,
		///<summary>Name. </summary>
		Name,
		///<summary>FieldValue1. </summary>
		FieldValue1,
		///<summary>FieldValue2. </summary>
		FieldValue2,
		///<summary>FieldValue3. </summary>
		FieldValue3,
		///<summary>FieldValue4. </summary>
		FieldValue4,
		///<summary>FieldValue5. </summary>
		FieldValue5,
		///<summary>FieldValue6. </summary>
		FieldValue6,
		///<summary>FieldValue7. </summary>
		FieldValue7,
		///<summary>FieldValue8. </summary>
		FieldValue8,
		///<summary>FieldValue9. </summary>
		FieldValue9,
		///<summary>FieldValue10. </summary>
		FieldValue10,
		///<summary>CreatedInBatchId. </summary>
		CreatedInBatchId,
		///<summary>UpdatedInBatchId. </summary>
		UpdatedInBatchId,
		///<summary>SynchronisationBatchId. </summary>
		SynchronisationBatchId,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>RevenueCenter. </summary>
		RevenueCenter,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Posdeliverypointgroup.</summary>
	public enum PosdeliverypointgroupFieldIndex
	{
		///<summary>PosdeliverypointgroupId. </summary>
		PosdeliverypointgroupId,
		///<summary>CompanyId. </summary>
		CompanyId,
		///<summary>ExternalId. </summary>
		ExternalId,
		///<summary>Name. </summary>
		Name,
		///<summary>FieldValue1. </summary>
		FieldValue1,
		///<summary>FieldValue2. </summary>
		FieldValue2,
		///<summary>FieldValue3. </summary>
		FieldValue3,
		///<summary>FieldValue4. </summary>
		FieldValue4,
		///<summary>FieldValue5. </summary>
		FieldValue5,
		///<summary>FieldValue6. </summary>
		FieldValue6,
		///<summary>FieldValue7. </summary>
		FieldValue7,
		///<summary>FieldValue8. </summary>
		FieldValue8,
		///<summary>FieldValue9. </summary>
		FieldValue9,
		///<summary>FieldValue10. </summary>
		FieldValue10,
		///<summary>CreatedInBatchId. </summary>
		CreatedInBatchId,
		///<summary>UpdatedInBatchId. </summary>
		UpdatedInBatchId,
		///<summary>SynchronisationBatchId. </summary>
		SynchronisationBatchId,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>RevenueCenter. </summary>
		RevenueCenter,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Pospaymentmethod.</summary>
	public enum PospaymentmethodFieldIndex
	{
		///<summary>PospaymentmethodId. </summary>
		PospaymentmethodId,
		///<summary>CompanyId. </summary>
		CompanyId,
		///<summary>ExternalId. </summary>
		ExternalId,
		///<summary>Name. </summary>
		Name,
		///<summary>FieldValue1. </summary>
		FieldValue1,
		///<summary>FieldValue2. </summary>
		FieldValue2,
		///<summary>FieldValue3. </summary>
		FieldValue3,
		///<summary>FieldValue4. </summary>
		FieldValue4,
		///<summary>FieldValue5. </summary>
		FieldValue5,
		///<summary>FieldValue6. </summary>
		FieldValue6,
		///<summary>FieldValue7. </summary>
		FieldValue7,
		///<summary>FieldValue8. </summary>
		FieldValue8,
		///<summary>FieldValue9. </summary>
		FieldValue9,
		///<summary>FieldValue10. </summary>
		FieldValue10,
		///<summary>CreatedInBatchId. </summary>
		CreatedInBatchId,
		///<summary>UpdatedInBatchId. </summary>
		UpdatedInBatchId,
		///<summary>SynchronisationBatchId. </summary>
		SynchronisationBatchId,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>RevenueCenter. </summary>
		RevenueCenter,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Posproduct.</summary>
	public enum PosproductFieldIndex
	{
		///<summary>PosproductId. </summary>
		PosproductId,
		///<summary>CompanyId. </summary>
		CompanyId,
		///<summary>ExternalId. </summary>
		ExternalId,
		///<summary>ExternalPoscategoryId. </summary>
		ExternalPoscategoryId,
		///<summary>Name. </summary>
		Name,
		///<summary>PriceIn. </summary>
		PriceIn,
		///<summary>VatTariff. </summary>
		VatTariff,
		///<summary>Visible. </summary>
		Visible,
		///<summary>FieldValue1. </summary>
		FieldValue1,
		///<summary>FieldValue2. </summary>
		FieldValue2,
		///<summary>FieldValue3. </summary>
		FieldValue3,
		///<summary>FieldValue4. </summary>
		FieldValue4,
		///<summary>FieldValue5. </summary>
		FieldValue5,
		///<summary>FieldValue6. </summary>
		FieldValue6,
		///<summary>FieldValue7. </summary>
		FieldValue7,
		///<summary>FieldValue8. </summary>
		FieldValue8,
		///<summary>FieldValue9. </summary>
		FieldValue9,
		///<summary>FieldValue10. </summary>
		FieldValue10,
		///<summary>CreatedInBatchId. </summary>
		CreatedInBatchId,
		///<summary>UpdatedInBatchId. </summary>
		UpdatedInBatchId,
		///<summary>SynchronisationBatchId. </summary>
		SynchronisationBatchId,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>RevenueCenter. </summary>
		RevenueCenter,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: PosproductPosalteration.</summary>
	public enum PosproductPosalterationFieldIndex
	{
		///<summary>PosproductPosalterationId. </summary>
		PosproductPosalterationId,
		///<summary>CompanyId. </summary>
		CompanyId,
		///<summary>PosproductExternalId. </summary>
		PosproductExternalId,
		///<summary>PosalterationExternalId. </summary>
		PosalterationExternalId,
		///<summary>ExternalId. </summary>
		ExternalId,
		///<summary>DeletedFromCms. </summary>
		DeletedFromCms,
		///<summary>CreatedInBatchId. </summary>
		CreatedInBatchId,
		///<summary>UpdatedInBatchId. </summary>
		UpdatedInBatchId,
		///<summary>SynchronisationBatchId. </summary>
		SynchronisationBatchId,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: PriceLevel.</summary>
	public enum PriceLevelFieldIndex
	{
		///<summary>PriceLevelId. </summary>
		PriceLevelId,
		///<summary>CompanyId. </summary>
		CompanyId,
		///<summary>Name. </summary>
		Name,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: PriceLevelItem.</summary>
	public enum PriceLevelItemFieldIndex
	{
		///<summary>PriceLevelItemId. </summary>
		PriceLevelItemId,
		///<summary>ParentCompanyId. </summary>
		ParentCompanyId,
		///<summary>PriceLevelId. </summary>
		PriceLevelId,
		///<summary>ProductId. </summary>
		ProductId,
		///<summary>AlterationId. </summary>
		AlterationId,
		///<summary>AlterationoptionId. </summary>
		AlterationoptionId,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>Price. </summary>
		Price,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: PriceSchedule.</summary>
	public enum PriceScheduleFieldIndex
	{
		///<summary>PriceScheduleId. </summary>
		PriceScheduleId,
		///<summary>CompanyId. </summary>
		CompanyId,
		///<summary>Name. </summary>
		Name,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: PriceScheduleItem.</summary>
	public enum PriceScheduleItemFieldIndex
	{
		///<summary>PriceScheduleItemId. </summary>
		PriceScheduleItemId,
		///<summary>ParentCompanyId. </summary>
		ParentCompanyId,
		///<summary>PriceScheduleId. </summary>
		PriceScheduleId,
		///<summary>PriceLevelId. </summary>
		PriceLevelId,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: PriceScheduleItemOccurrence.</summary>
	public enum PriceScheduleItemOccurrenceFieldIndex
	{
		///<summary>PriceScheduleItemOccurrenceId. </summary>
		PriceScheduleItemOccurrenceId,
		///<summary>ParentCompanyId. </summary>
		ParentCompanyId,
		///<summary>PriceScheduleItemId. </summary>
		PriceScheduleItemId,
		///<summary>StartTime. </summary>
		StartTime,
		///<summary>EndTime. </summary>
		EndTime,
		///<summary>Recurring. </summary>
		Recurring,
		///<summary>RecurrenceType. </summary>
		RecurrenceType,
		///<summary>RecurrenceRange. </summary>
		RecurrenceRange,
		///<summary>RecurrenceStart. </summary>
		RecurrenceStart,
		///<summary>RecurrenceEnd. </summary>
		RecurrenceEnd,
		///<summary>RecurrenceOccurrenceCount. </summary>
		RecurrenceOccurrenceCount,
		///<summary>RecurrencePeriodicity. </summary>
		RecurrencePeriodicity,
		///<summary>RecurrenceDayNumber. </summary>
		RecurrenceDayNumber,
		///<summary>RecurrenceWeekDays. </summary>
		RecurrenceWeekDays,
		///<summary>RecurrenceWeekOfMonth. </summary>
		RecurrenceWeekOfMonth,
		///<summary>RecurrenceMonth. </summary>
		RecurrenceMonth,
		///<summary>BackgroundColor. </summary>
		BackgroundColor,
		///<summary>TextColor. </summary>
		TextColor,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>StartTimeUTC. </summary>
		StartTimeUTC,
		///<summary>EndTimeUTC. </summary>
		EndTimeUTC,
		///<summary>RecurrenceStartUTC. </summary>
		RecurrenceStartUTC,
		///<summary>RecurrenceEndUTC. </summary>
		RecurrenceEndUTC,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Product.</summary>
	public enum ProductFieldIndex
	{
		///<summary>ProductId. </summary>
		ProductId,
		///<summary>CompanyId. </summary>
		CompanyId,
		///<summary>GenericproductId. </summary>
		GenericproductId,
		///<summary>Name. </summary>
		Name,
		///<summary>Description. </summary>
		Description,
		///<summary>Type. </summary>
		Type,
		///<summary>PriceIn. </summary>
		PriceIn,
		///<summary>VattariffId. </summary>
		VattariffId,
		///<summary>SortOrder. </summary>
		SortOrder,
		///<summary>VattariffPercentage. </summary>
		VattariffPercentage,
		///<summary>PosproductId. </summary>
		PosproductId,
		///<summary>TextColor. </summary>
		TextColor,
		///<summary>BackgroundColor. </summary>
		BackgroundColor,
		///<summary>DisplayOnHomepage. </summary>
		DisplayOnHomepage,
		///<summary>Visible. </summary>
		Visible,
		///<summary>AvailableOnObymobi. </summary>
		AvailableOnObymobi,
		///<summary>SoldOut. </summary>
		SoldOut,
		///<summary>Rateable. </summary>
		Rateable,
		///<summary>AllowFreeText. </summary>
		AllowFreeText,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>RouteId. </summary>
		RouteId,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>AnnouncementAction. </summary>
		AnnouncementAction,
		///<summary>Color. </summary>
		Color,
		///<summary>SubType. </summary>
		SubType,
		///<summary>ButtonText. </summary>
		ButtonText,
		///<summary>ManualDescriptionEnabled. </summary>
		ManualDescriptionEnabled,
		///<summary>WebTypeSmartphoneUrl. </summary>
		WebTypeSmartphoneUrl,
		///<summary>Geofencing. </summary>
		Geofencing,
		///<summary>WebTypeTabletUrl. </summary>
		WebTypeTabletUrl,
		///<summary>HotSOSIssueId. </summary>
		HotSOSIssueId,
		///<summary>ScheduleId. </summary>
		ScheduleId,
		///<summary>ViewLayoutType. </summary>
		ViewLayoutType,
		///<summary>DeliveryLocationType. </summary>
		DeliveryLocationType,
		///<summary>SupportNotificationType. </summary>
		SupportNotificationType,
		///<summary>HidePrice. </summary>
		HidePrice,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>GenericProductChangedUTC. </summary>
		GenericProductChangedUTC,
		///<summary>VisibilityType. </summary>
		VisibilityType,
		///<summary>CustomizeButtonText. </summary>
		CustomizeButtonText,
		///<summary>InheritAlterations. </summary>
		InheritAlterations,
		///<summary>ProductgroupId. </summary>
		ProductgroupId,
		///<summary>IsLinkedToGeneric. </summary>
		IsLinkedToGeneric,
		///<summary>OverrideSubType. </summary>
		OverrideSubType,
		///<summary>ExternalProductId. </summary>
		ExternalProductId,
		///<summary>BrandId. </summary>
		BrandId,
		///<summary>BrandProductId. </summary>
		BrandProductId,
		///<summary>InheritAttachmentsFromBrand. </summary>
		InheritAttachmentsFromBrand,
		///<summary>InheritAlterationsFromBrand. </summary>
		InheritAlterationsFromBrand,
		///<summary>InheritName. </summary>
		InheritName,
		///<summary>InheritPrice. </summary>
		InheritPrice,
		///<summary>InheritButtonText. </summary>
		InheritButtonText,
		///<summary>InheritCustomizeButtonText. </summary>
		InheritCustomizeButtonText,
		///<summary>InheritWebTypeTabletUrl. </summary>
		InheritWebTypeTabletUrl,
		///<summary>InheritWebTypeSmartphoneUrl. </summary>
		InheritWebTypeSmartphoneUrl,
		///<summary>InheritDescription. </summary>
		InheritDescription,
		///<summary>InheritColor. </summary>
		InheritColor,
		///<summary>ShortDescription. </summary>
		ShortDescription,
		///<summary>InheritMedia. </summary>
		InheritMedia,
		///<summary>TaxTariffId. </summary>
		TaxTariffId,
		///<summary>IsAlcoholic. </summary>
		IsAlcoholic,
		///<summary>IsAvailable. </summary>
		IsAvailable,
		///<summary>SystemName. </summary>
		SystemName,
		///<summary>InheritSystemName. </summary>
		InheritSystemName,
		///<summary>InheritExternalIdentifier. </summary>
		InheritExternalIdentifier,
		///<summary>ExternalIdentifier. </summary>
		ExternalIdentifier,
		///<summary>PointOfInterestId. </summary>
		PointOfInterestId,
		///<summary>CoversType. </summary>
		CoversType,
		///<summary>OpenNewWindow. </summary>
		OpenNewWindow,
		///<summary>WebLinkType. </summary>
		WebLinkType,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: ProductAlteration.</summary>
	public enum ProductAlterationFieldIndex
	{
		///<summary>ProductAlterationId. </summary>
		ProductAlterationId,
		///<summary>ProductId. </summary>
		ProductId,
		///<summary>AlterationId. </summary>
		AlterationId,
		///<summary>SortOrder. </summary>
		SortOrder,
		///<summary>PosproductPosalterationId. </summary>
		PosproductPosalterationId,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>GenericproductGenericalterationId. </summary>
		GenericproductGenericalterationId,
		///<summary>ParentCompanyId. </summary>
		ParentCompanyId,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>Version. </summary>
		Version,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: ProductAttachment.</summary>
	public enum ProductAttachmentFieldIndex
	{
		///<summary>ProductAttachmentId. </summary>
		ProductAttachmentId,
		///<summary>ProductId. </summary>
		ProductId,
		///<summary>AttachmentId. </summary>
		AttachmentId,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>ParentCompanyId. </summary>
		ParentCompanyId,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: ProductCategory.</summary>
	public enum ProductCategoryFieldIndex
	{
		///<summary>ProductCategoryId. </summary>
		ProductCategoryId,
		///<summary>ProductId. </summary>
		ProductId,
		///<summary>CategoryId. </summary>
		CategoryId,
		///<summary>SortOrder. </summary>
		SortOrder,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>ParentCompanyId. </summary>
		ParentCompanyId,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>InheritSuggestions. </summary>
		InheritSuggestions,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: ProductCategorySuggestion.</summary>
	public enum ProductCategorySuggestionFieldIndex
	{
		///<summary>ProductCategorySuggestionId. </summary>
		ProductCategorySuggestionId,
		///<summary>ParentCompanyId. </summary>
		ParentCompanyId,
		///<summary>ProductCategoryId. </summary>
		ProductCategoryId,
		///<summary>SuggestedProductCategoryId. </summary>
		SuggestedProductCategoryId,
		///<summary>SortOrder. </summary>
		SortOrder,
		///<summary>Checkout. </summary>
		Checkout,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: ProductCategoryTag.</summary>
	public enum ProductCategoryTagFieldIndex
	{
		///<summary>ProductCategoryTagId. </summary>
		ProductCategoryTagId,
		///<summary>ProductCategoryId. </summary>
		ProductCategoryId,
		///<summary>CategoryId. </summary>
		CategoryId,
		///<summary>TagId. </summary>
		TagId,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>ParentCompanyId. </summary>
		ParentCompanyId,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Productgroup.</summary>
	public enum ProductgroupFieldIndex
	{
		///<summary>ProductgroupId. </summary>
		ProductgroupId,
		///<summary>CompanyId. </summary>
		CompanyId,
		///<summary>Name. </summary>
		Name,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: ProductgroupItem.</summary>
	public enum ProductgroupItemFieldIndex
	{
		///<summary>ProductgroupItemId. </summary>
		ProductgroupItemId,
		///<summary>ParentCompanyId. </summary>
		ParentCompanyId,
		///<summary>ProductgroupId. </summary>
		ProductgroupId,
		///<summary>ProductId. </summary>
		ProductId,
		///<summary>NestedProductgroupId. </summary>
		NestedProductgroupId,
		///<summary>SortOrder. </summary>
		SortOrder,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: ProductLanguage.</summary>
	public enum ProductLanguageFieldIndex
	{
		///<summary>ProductLanguageId. </summary>
		ProductLanguageId,
		///<summary>ProductId. </summary>
		ProductId,
		///<summary>LanguageId. </summary>
		LanguageId,
		///<summary>Name. </summary>
		Name,
		///<summary>Description. </summary>
		Description,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>ButtonText. </summary>
		ButtonText,
		///<summary>OrderProcessedTitle. </summary>
		OrderProcessedTitle,
		///<summary>OrderProcessedMessage. </summary>
		OrderProcessedMessage,
		///<summary>ParentCompanyId. </summary>
		ParentCompanyId,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: ProductSuggestion.</summary>
	public enum ProductSuggestionFieldIndex
	{
		///<summary>ProductSuggestionId. </summary>
		ProductSuggestionId,
		///<summary>ProductId. </summary>
		ProductId,
		///<summary>SuggestedProductId. </summary>
		SuggestedProductId,
		///<summary>Direct. </summary>
		Direct,
		///<summary>SortOrder. </summary>
		SortOrder,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>Checkout. </summary>
		Checkout,
		///<summary>ParentCompanyId. </summary>
		ParentCompanyId,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: ProductTag.</summary>
	public enum ProductTagFieldIndex
	{
		///<summary>ProductTagId. </summary>
		ProductTagId,
		///<summary>ProductId. </summary>
		ProductId,
		///<summary>TagId. </summary>
		TagId,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>ParentCompanyId. </summary>
		ParentCompanyId,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Publishing.</summary>
	public enum PublishingFieldIndex
	{
		///<summary>PublishingId. </summary>
		PublishingId,
		///<summary>PublishedUTC. </summary>
		PublishedUTC,
		///<summary>PublishedTicks. </summary>
		PublishedTicks,
		///<summary>UserId. </summary>
		UserId,
		///<summary>Username. </summary>
		Username,
		///<summary>Log. </summary>
		Log,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>PublishRequest. </summary>
		PublishRequest,
		///<summary>Parameter1Name. </summary>
		Parameter1Name,
		///<summary>Parameter1Value. </summary>
		Parameter1Value,
		///<summary>Parameter2Name. </summary>
		Parameter2Name,
		///<summary>Parameter2Value. </summary>
		Parameter2Value,
		///<summary>Status. </summary>
		Status,
		///<summary>StatusText. </summary>
		StatusText,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: PublishingItem.</summary>
	public enum PublishingItemFieldIndex
	{
		///<summary>PublishingItemId. </summary>
		PublishingItemId,
		///<summary>PublishingId. </summary>
		PublishingId,
		///<summary>ApiOperation. </summary>
		ApiOperation,
		///<summary>Status. </summary>
		Status,
		///<summary>StatusText. </summary>
		StatusText,
		///<summary>Log. </summary>
		Log,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>Parameter1Name. </summary>
		Parameter1Name,
		///<summary>Parameter1Value. </summary>
		Parameter1Value,
		///<summary>Parameter2Name. </summary>
		Parameter2Name,
		///<summary>Parameter2Value. </summary>
		Parameter2Value,
		///<summary>PublishedUTC. </summary>
		PublishedUTC,
		///<summary>PublishedTicks. </summary>
		PublishedTicks,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Rating.</summary>
	public enum RatingFieldIndex
	{
		///<summary>RatingId. </summary>
		RatingId,
		///<summary>ProductId. </summary>
		ProductId,
		///<summary>Value. </summary>
		Value,
		///<summary>Visible. </summary>
		Visible,
		///<summary>Comments. </summary>
		Comments,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>ParentCompanyId. </summary>
		ParentCompanyId,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Receipt.</summary>
	public enum ReceiptFieldIndex
	{
		///<summary>ReceiptId. </summary>
		ReceiptId,
		///<summary>ReceiptTemplateId. </summary>
		ReceiptTemplateId,
		///<summary>CompanyId. </summary>
		CompanyId,
		///<summary>OrderId. </summary>
		OrderId,
		///<summary>Number. </summary>
		Number,
		///<summary>SellerName. </summary>
		SellerName,
		///<summary>SellerAddress. </summary>
		SellerAddress,
		///<summary>SellerContactInfo. </summary>
		SellerContactInfo,
		///<summary>ServiceMethodName. </summary>
		ServiceMethodName,
		///<summary>CheckoutMethodName. </summary>
		CheckoutMethodName,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>TaxBreakdown. </summary>
		TaxBreakdown,
		///<summary>Email. </summary>
		Email,
		///<summary>Phonenumber. </summary>
		Phonenumber,
		///<summary>VatNumber. </summary>
		VatNumber,
		///<summary>PaymentProcessorInfo. </summary>
		PaymentProcessorInfo,
		///<summary>SellerContactEmail. </summary>
		SellerContactEmail,
		///<summary>OutletSellerInformationId. </summary>
		OutletSellerInformationId,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: ReceiptTemplate.</summary>
	public enum ReceiptTemplateFieldIndex
	{
		///<summary>ReceiptTemplateId. </summary>
		ReceiptTemplateId,
		///<summary>CompanyId. </summary>
		CompanyId,
		///<summary>Name. </summary>
		Name,
		///<summary>SellerName. </summary>
		SellerName,
		///<summary>SellerAddress. </summary>
		SellerAddress,
		///<summary>SellerContactInfo. </summary>
		SellerContactInfo,
		///<summary>TaxBreakdown. </summary>
		TaxBreakdown,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>Email. </summary>
		Email,
		///<summary>Phonenumber. </summary>
		Phonenumber,
		///<summary>VatNumber. </summary>
		VatNumber,
		///<summary>SellerContactEmail. </summary>
		SellerContactEmail,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: ReferentialConstraint.</summary>
	public enum ReferentialConstraintFieldIndex
	{
		///<summary>ReferentialConstraintId. </summary>
		ReferentialConstraintId,
		///<summary>EntityName. </summary>
		EntityName,
		///<summary>ForeignKeyName. </summary>
		ForeignKeyName,
		///<summary>DeleteRule. </summary>
		DeleteRule,
		///<summary>UpdateRule. </summary>
		UpdateRule,
		///<summary>ArchiveRule. </summary>
		ArchiveRule,
		///<summary>ForLocalUseWhileSyncingIsUpdated. </summary>
		ForLocalUseWhileSyncingIsUpdated,
		///<summary>Archived. </summary>
		Archived,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>Deleted. </summary>
		Deleted,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: ReferentialConstraintCustom.</summary>
	public enum ReferentialConstraintCustomFieldIndex
	{
		///<summary>ReferentialConstraintCustomId. </summary>
		ReferentialConstraintCustomId,
		///<summary>EntityName. </summary>
		EntityName,
		///<summary>ForeignKeyName. </summary>
		ForeignKeyName,
		///<summary>DeleteRule. </summary>
		DeleteRule,
		///<summary>UpdateRule. </summary>
		UpdateRule,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Release.</summary>
	public enum ReleaseFieldIndex
	{
		///<summary>ReleaseId. </summary>
		ReleaseId,
		///<summary>ApplicationId. </summary>
		ApplicationId,
		///<summary>Version. </summary>
		Version,
		///<summary>Comment. </summary>
		Comment,
		///<summary>Filename. </summary>
		Filename,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>File. </summary>
		File,
		///<summary>Crc32. </summary>
		Crc32,
		///<summary>Intermediate. </summary>
		Intermediate,
		///<summary>IntermediateReleaseId. </summary>
		IntermediateReleaseId,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>ReleaseGroup. </summary>
		ReleaseGroup,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: ReportProcessingTask.</summary>
	public enum ReportProcessingTaskFieldIndex
	{
		///<summary>ReportProcessingTaskId. </summary>
		ReportProcessingTaskId,
		///<summary>AnalyticsReportType. </summary>
		AnalyticsReportType,
		///<summary>Processed. </summary>
		Processed,
		///<summary>CompanyId. </summary>
		CompanyId,
		///<summary>XFrom. </summary>
		XFrom,
		///<summary>XTill. </summary>
		XTill,
		///<summary>Filter. </summary>
		Filter,
		///<summary>Failed. </summary>
		Failed,
		///<summary>ExceptionText. </summary>
		ExceptionText,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>ExternalTask. </summary>
		ExternalTask,
		///<summary>ExternalHandled. </summary>
		ExternalHandled,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>FromUTC. </summary>
		FromUTC,
		///<summary>TillUTC. </summary>
		TillUTC,
		///<summary>TimeZoneId. </summary>
		TimeZoneId,
		///<summary>TimeZoneOlsonId. </summary>
		TimeZoneOlsonId,
		///<summary>Email. </summary>
		Email,
		///<summary>ReportName. </summary>
		ReportName,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: ReportProcessingTaskFile.</summary>
	public enum ReportProcessingTaskFileFieldIndex
	{
		///<summary>ReportProcessingTaskFileId. </summary>
		ReportProcessingTaskFileId,
		///<summary>ReportProcessingTaskId. </summary>
		ReportProcessingTaskId,
		///<summary>File. </summary>
		File,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>ParentCompanyId. </summary>
		ParentCompanyId,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: ReportProcessingTaskTemplate.</summary>
	public enum ReportProcessingTaskTemplateFieldIndex
	{
		///<summary>ReportProcessingTaskTemplateId. </summary>
		ReportProcessingTaskTemplateId,
		///<summary>ReportProcessingTaskId. </summary>
		ReportProcessingTaskId,
		///<summary>TemplateFile. </summary>
		TemplateFile,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>CompanyId. </summary>
		CompanyId,
		///<summary>Filter. </summary>
		Filter,
		///<summary>ReportingPeriod. </summary>
		ReportingPeriod,
		///<summary>FriendlyName. </summary>
		FriendlyName,
		///<summary>Email. </summary>
		Email,
		///<summary>ReportType. </summary>
		ReportType,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Requestlog.</summary>
	public enum RequestlogFieldIndex
	{
		///<summary>RequestlogId. </summary>
		RequestlogId,
		///<summary>UserAgent. </summary>
		UserAgent,
		///<summary>SourceApplication. </summary>
		SourceApplication,
		///<summary>Identifier. </summary>
		Identifier,
		///<summary>CustomerId. </summary>
		CustomerId,
		///<summary>TerminalId. </summary>
		TerminalId,
		///<summary>Phonenumber. </summary>
		Phonenumber,
		///<summary>MethodName. </summary>
		MethodName,
		///<summary>ResultEnumTypeName. </summary>
		ResultEnumTypeName,
		///<summary>ResultEnumValueName. </summary>
		ResultEnumValueName,
		///<summary>ResultCode. </summary>
		ResultCode,
		///<summary>ResultMessage. </summary>
		ResultMessage,
		///<summary>Parameter1. </summary>
		Parameter1,
		///<summary>Parameter2. </summary>
		Parameter2,
		///<summary>Parameter3. </summary>
		Parameter3,
		///<summary>Parameter4. </summary>
		Parameter4,
		///<summary>Parameter5. </summary>
		Parameter5,
		///<summary>Parameter6. </summary>
		Parameter6,
		///<summary>ResultBody. </summary>
		ResultBody,
		///<summary>ErrorMessage. </summary>
		ErrorMessage,
		///<summary>ErrorStackTrace. </summary>
		ErrorStackTrace,
		///<summary>Xml. </summary>
		Xml,
		///<summary>RawRequest. </summary>
		RawRequest,
		///<summary>TraceFromMobile. </summary>
		TraceFromMobile,
		///<summary>MobileVendor. </summary>
		MobileVendor,
		///<summary>MobileIdentifier. </summary>
		MobileIdentifier,
		///<summary>MobilePlatform. </summary>
		MobilePlatform,
		///<summary>MobileOs. </summary>
		MobileOs,
		///<summary>MobileName. </summary>
		MobileName,
		///<summary>DeviceInfo. </summary>
		DeviceInfo,
		///<summary>Log. </summary>
		Log,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>ServerName. </summary>
		ServerName,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Role.</summary>
	public enum RoleFieldIndex
	{
		///<summary>RoleId. </summary>
		RoleId,
		///<summary>ParentRoleId. </summary>
		ParentRoleId,
		///<summary>Name. </summary>
		Name,
		///<summary>IsAdministrator. </summary>
		IsAdministrator,
		///<summary>SystemName. </summary>
		SystemName,
		///<summary>SystemRole. </summary>
		SystemRole,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>Deleted. </summary>
		Deleted,
		///<summary>Archived. </summary>
		Archived,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: RoleModuleRights.</summary>
	public enum RoleModuleRightsFieldIndex
	{
		///<summary>RoleModuleRightsId. </summary>
		RoleModuleRightsId,
		///<summary>RoleId. </summary>
		RoleId,
		///<summary>ModuleId. </summary>
		ModuleId,
		///<summary>IsAllowed. </summary>
		IsAllowed,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: RoleUIElementRights.</summary>
	public enum RoleUIElementRightsFieldIndex
	{
		///<summary>RoleUIElementRightsId. </summary>
		RoleUIElementRightsId,
		///<summary>RoleId. </summary>
		RoleId,
		///<summary>UiElementTypeNameFull. </summary>
		UiElementTypeNameFull,
		///<summary>IsAllowed. </summary>
		IsAllowed,
		///<summary>IsReadOnly. </summary>
		IsReadOnly,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: RoleUIElementSubPanelRights.</summary>
	public enum RoleUIElementSubPanelRightsFieldIndex
	{
		///<summary>RoleUIElementSubPanelRightsId. </summary>
		RoleUIElementSubPanelRightsId,
		///<summary>RoleId. </summary>
		RoleId,
		///<summary>UiElementTypeNameFull. </summary>
		UiElementTypeNameFull,
		///<summary>IsAllowed. </summary>
		IsAllowed,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: RoomControlArea.</summary>
	public enum RoomControlAreaFieldIndex
	{
		///<summary>RoomControlAreaId. </summary>
		RoomControlAreaId,
		///<summary>ParentCompanyId. </summary>
		ParentCompanyId,
		///<summary>RoomControlConfigurationId. </summary>
		RoomControlConfigurationId,
		///<summary>Name. </summary>
		Name,
		///<summary>SortOrder. </summary>
		SortOrder,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>Visible. </summary>
		Visible,
		///<summary>NameSystem. </summary>
		NameSystem,
		///<summary>Type. </summary>
		Type,
		///<summary>IsNameSystemBaseId. </summary>
		IsNameSystemBaseId,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: RoomControlAreaLanguage.</summary>
	public enum RoomControlAreaLanguageFieldIndex
	{
		///<summary>RoomControlAreaLanguageId. </summary>
		RoomControlAreaLanguageId,
		///<summary>ParentCompanyId. </summary>
		ParentCompanyId,
		///<summary>RoomControlAreaId. </summary>
		RoomControlAreaId,
		///<summary>LanguageId. </summary>
		LanguageId,
		///<summary>Name. </summary>
		Name,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: RoomControlComponent.</summary>
	public enum RoomControlComponentFieldIndex
	{
		///<summary>RoomControlComponentId. </summary>
		RoomControlComponentId,
		///<summary>ParentCompanyId. </summary>
		ParentCompanyId,
		///<summary>RoomControlSectionId. </summary>
		RoomControlSectionId,
		///<summary>Name. </summary>
		Name,
		///<summary>NameSystem. </summary>
		NameSystem,
		///<summary>Type. </summary>
		Type,
		///<summary>SortOrder. </summary>
		SortOrder,
		///<summary>Visible. </summary>
		Visible,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>FieldValue1. </summary>
		FieldValue1,
		///<summary>FieldValue2. </summary>
		FieldValue2,
		///<summary>FieldValue3. </summary>
		FieldValue3,
		///<summary>FieldValue4. </summary>
		FieldValue4,
		///<summary>FieldValue5. </summary>
		FieldValue5,
		///<summary>InfraredConfigurationId. </summary>
		InfraredConfigurationId,
		///<summary>ControllerId. </summary>
		ControllerId,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: RoomControlComponentLanguage.</summary>
	public enum RoomControlComponentLanguageFieldIndex
	{
		///<summary>RoomControlComponentLanguageId. </summary>
		RoomControlComponentLanguageId,
		///<summary>ParentCompanyId. </summary>
		ParentCompanyId,
		///<summary>RoomControlComponentId. </summary>
		RoomControlComponentId,
		///<summary>LanguageId. </summary>
		LanguageId,
		///<summary>Name. </summary>
		Name,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: RoomControlConfiguration.</summary>
	public enum RoomControlConfigurationFieldIndex
	{
		///<summary>RoomControlConfigurationId. </summary>
		RoomControlConfigurationId,
		///<summary>CompanyId. </summary>
		CompanyId,
		///<summary>Name. </summary>
		Name,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: RoomControlSection.</summary>
	public enum RoomControlSectionFieldIndex
	{
		///<summary>RoomControlSectionId. </summary>
		RoomControlSectionId,
		///<summary>ParentCompanyId. </summary>
		ParentCompanyId,
		///<summary>RoomControlAreaId. </summary>
		RoomControlAreaId,
		///<summary>Name. </summary>
		Name,
		///<summary>Type. </summary>
		Type,
		///<summary>SortOrder. </summary>
		SortOrder,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>FieldValue1. </summary>
		FieldValue1,
		///<summary>FieldValue2. </summary>
		FieldValue2,
		///<summary>FieldValue3. </summary>
		FieldValue3,
		///<summary>FieldValue4. </summary>
		FieldValue4,
		///<summary>FieldValue5. </summary>
		FieldValue5,
		///<summary>Visible. </summary>
		Visible,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: RoomControlSectionItem.</summary>
	public enum RoomControlSectionItemFieldIndex
	{
		///<summary>RoomControlSectionItemId. </summary>
		RoomControlSectionItemId,
		///<summary>ParentCompanyId. </summary>
		ParentCompanyId,
		///<summary>RoomControlSectionId. </summary>
		RoomControlSectionId,
		///<summary>Name. </summary>
		Name,
		///<summary>Type. </summary>
		Type,
		///<summary>SortOrder. </summary>
		SortOrder,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>StationListId. </summary>
		StationListId,
		///<summary>FieldValue1. </summary>
		FieldValue1,
		///<summary>FieldValue2. </summary>
		FieldValue2,
		///<summary>FieldValue3. </summary>
		FieldValue3,
		///<summary>FieldValue4. </summary>
		FieldValue4,
		///<summary>FieldValue5. </summary>
		FieldValue5,
		///<summary>Visible. </summary>
		Visible,
		///<summary>InfraredConfigurationId. </summary>
		InfraredConfigurationId,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: RoomControlSectionItemLanguage.</summary>
	public enum RoomControlSectionItemLanguageFieldIndex
	{
		///<summary>RoomControlSectionItemLanguageId. </summary>
		RoomControlSectionItemLanguageId,
		///<summary>ParentCompanyId. </summary>
		ParentCompanyId,
		///<summary>RoomControlSectionItemId. </summary>
		RoomControlSectionItemId,
		///<summary>LanguageId. </summary>
		LanguageId,
		///<summary>Name. </summary>
		Name,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: RoomControlSectionLanguage.</summary>
	public enum RoomControlSectionLanguageFieldIndex
	{
		///<summary>RoomControlSectionLanguageId. </summary>
		RoomControlSectionLanguageId,
		///<summary>ParentCompanyId. </summary>
		ParentCompanyId,
		///<summary>RoomControlSectionId. </summary>
		RoomControlSectionId,
		///<summary>LanguageId. </summary>
		LanguageId,
		///<summary>Name. </summary>
		Name,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: RoomControlWidget.</summary>
	public enum RoomControlWidgetFieldIndex
	{
		///<summary>RoomControlWidgetId. </summary>
		RoomControlWidgetId,
		///<summary>ParentCompanyId. </summary>
		ParentCompanyId,
		///<summary>RoomControlSectionId. </summary>
		RoomControlSectionId,
		///<summary>RoomControlComponentId1. </summary>
		RoomControlComponentId1,
		///<summary>Caption. </summary>
		Caption,
		///<summary>Type. </summary>
		Type,
		///<summary>SortOrder. </summary>
		SortOrder,
		///<summary>Visible. </summary>
		Visible,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>FieldValue1. </summary>
		FieldValue1,
		///<summary>FieldValue2. </summary>
		FieldValue2,
		///<summary>FieldValue3. </summary>
		FieldValue3,
		///<summary>FieldValue4. </summary>
		FieldValue4,
		///<summary>FieldValue5. </summary>
		FieldValue5,
		///<summary>FieldValue6. </summary>
		FieldValue6,
		///<summary>FieldValue7. </summary>
		FieldValue7,
		///<summary>FieldValue8. </summary>
		FieldValue8,
		///<summary>FieldValue9. </summary>
		FieldValue9,
		///<summary>FieldValue10. </summary>
		FieldValue10,
		///<summary>RoomControlSectionItemId. </summary>
		RoomControlSectionItemId,
		///<summary>RoomControlComponentId10. </summary>
		RoomControlComponentId10,
		///<summary>RoomControlComponentId2. </summary>
		RoomControlComponentId2,
		///<summary>RoomControlComponentId3. </summary>
		RoomControlComponentId3,
		///<summary>RoomControlComponentId4. </summary>
		RoomControlComponentId4,
		///<summary>RoomControlComponentId5. </summary>
		RoomControlComponentId5,
		///<summary>RoomControlComponentId6. </summary>
		RoomControlComponentId6,
		///<summary>RoomControlComponentId7. </summary>
		RoomControlComponentId7,
		///<summary>RoomControlComponentId8. </summary>
		RoomControlComponentId8,
		///<summary>RoomControlComponentId9. </summary>
		RoomControlComponentId9,
		///<summary>InfraredConfigurationId. </summary>
		InfraredConfigurationId,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: RoomControlWidgetLanguage.</summary>
	public enum RoomControlWidgetLanguageFieldIndex
	{
		///<summary>RoomControlWidgetLanguageId. </summary>
		RoomControlWidgetLanguageId,
		///<summary>ParentCompanyId. </summary>
		ParentCompanyId,
		///<summary>RoomControlWidgetId. </summary>
		RoomControlWidgetId,
		///<summary>LanguageId. </summary>
		LanguageId,
		///<summary>Caption. </summary>
		Caption,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Route.</summary>
	public enum RouteFieldIndex
	{
		///<summary>RouteId. </summary>
		RouteId,
		///<summary>CompanyId. </summary>
		CompanyId,
		///<summary>Name. </summary>
		Name,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>LogAlways. </summary>
		LogAlways,
		///<summary>StepTimeoutMinutes. </summary>
		StepTimeoutMinutes,
		///<summary>EscalationRouteId. </summary>
		EscalationRouteId,
		///<summary>BeingHandledSupportNotificationTimeoutMinutes. </summary>
		BeingHandledSupportNotificationTimeoutMinutes,
		///<summary>RetrievalSupportNotificationTimeoutMinutes. </summary>
		RetrievalSupportNotificationTimeoutMinutes,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Routestep.</summary>
	public enum RoutestepFieldIndex
	{
		///<summary>RoutestepId. </summary>
		RoutestepId,
		///<summary>RouteId. </summary>
		RouteId,
		///<summary>Number. </summary>
		Number,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>TimeoutMinutes. </summary>
		TimeoutMinutes,
		///<summary>ContinueOnFailure. </summary>
		ContinueOnFailure,
		///<summary>CompleteRouteOnComplete. </summary>
		CompleteRouteOnComplete,
		///<summary>ParentCompanyId. </summary>
		ParentCompanyId,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Routestephandler.</summary>
	public enum RoutestephandlerFieldIndex
	{
		///<summary>RoutestephandlerId. </summary>
		RoutestephandlerId,
		///<summary>RoutestepId. </summary>
		RoutestepId,
		///<summary>TerminalId. </summary>
		TerminalId,
		///<summary>HandlerType. </summary>
		HandlerType,
		///<summary>PrintReportType. </summary>
		PrintReportType,
		///<summary>FieldValue1. </summary>
		FieldValue1,
		///<summary>FieldValue2. </summary>
		FieldValue2,
		///<summary>FieldValue3. </summary>
		FieldValue3,
		///<summary>FieldValue4. </summary>
		FieldValue4,
		///<summary>FieldValue5. </summary>
		FieldValue5,
		///<summary>FieldValue6. </summary>
		FieldValue6,
		///<summary>FieldValue7. </summary>
		FieldValue7,
		///<summary>FieldValue8. </summary>
		FieldValue8,
		///<summary>FieldValue9. </summary>
		FieldValue9,
		///<summary>FieldValue10. </summary>
		FieldValue10,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>SupportpoolId. </summary>
		SupportpoolId,
		///<summary>ParentCompanyId. </summary>
		ParentCompanyId,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>ExternalSystemId. </summary>
		ExternalSystemId,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Schedule.</summary>
	public enum ScheduleFieldIndex
	{
		///<summary>ScheduleId. </summary>
		ScheduleId,
		///<summary>Name. </summary>
		Name,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>CompanyId. </summary>
		CompanyId,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: ScheduledCommand.</summary>
	public enum ScheduledCommandFieldIndex
	{
		///<summary>ScheduledCommandId. </summary>
		ScheduledCommandId,
		///<summary>ScheduledCommandTaskId. </summary>
		ScheduledCommandTaskId,
		///<summary>ClientCommand. </summary>
		ClientCommand,
		///<summary>ClientId. </summary>
		ClientId,
		///<summary>TerminalCommand. </summary>
		TerminalCommand,
		///<summary>TerminalId. </summary>
		TerminalId,
		///<summary>Status. </summary>
		Status,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>Sort. </summary>
		Sort,
		///<summary>ParentCompanyId. </summary>
		ParentCompanyId,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>StartedUTC. </summary>
		StartedUTC,
		///<summary>ExpiresUTC. </summary>
		ExpiresUTC,
		///<summary>CompletedUTC. </summary>
		CompletedUTC,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: ScheduledCommandTask.</summary>
	public enum ScheduledCommandTaskFieldIndex
	{
		///<summary>ScheduledCommandTaskId. </summary>
		ScheduledCommandTaskId,
		///<summary>CompanyId. </summary>
		CompanyId,
		///<summary>Active. </summary>
		Active,
		///<summary>Status. </summary>
		Status,
		///<summary>Notification. </summary>
		Notification,
		///<summary>NotificationEmailAddresses. </summary>
		NotificationEmailAddresses,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>CommandCount. </summary>
		CommandCount,
		///<summary>FinishedCount. </summary>
		FinishedCount,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>CompletedUTC. </summary>
		CompletedUTC,
		///<summary>StartUTC. </summary>
		StartUTC,
		///<summary>ExpiresUTC. </summary>
		ExpiresUTC,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: ScheduledMessage.</summary>
	public enum ScheduledMessageFieldIndex
	{
		///<summary>ScheduledMessageId. </summary>
		ScheduledMessageId,
		///<summary>CompanyId. </summary>
		CompanyId,
		///<summary>Title. </summary>
		Title,
		///<summary>Message. </summary>
		Message,
		///<summary>MediaId. </summary>
		MediaId,
		///<summary>EntertainmentId. </summary>
		EntertainmentId,
		///<summary>CategoryId. </summary>
		CategoryId,
		///<summary>ProductCategoryId. </summary>
		ProductCategoryId,
		///<summary>SiteId. </summary>
		SiteId,
		///<summary>PageId. </summary>
		PageId,
		///<summary>Url. </summary>
		Url,
		///<summary>Urgent. </summary>
		Urgent,
		///<summary>MessageButtonType. </summary>
		MessageButtonType,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>NotifyOnYes. </summary>
		NotifyOnYes,
		///<summary>FriendlyName. </summary>
		FriendlyName,
		///<summary>ManualTitleEnabled. </summary>
		ManualTitleEnabled,
		///<summary>ManualFriendlyNameEnabled. </summary>
		ManualFriendlyNameEnabled,
		///<summary>ManualMessageEnabled. </summary>
		ManualMessageEnabled,
		///<summary>ManualMediaEnabled. </summary>
		ManualMediaEnabled,
		///<summary>MessageTemplateId. </summary>
		MessageTemplateId,
		///<summary>MessageLayoutType. </summary>
		MessageLayoutType,
		///<summary>ManualMessageLayoutTypeEnabled. </summary>
		ManualMessageLayoutTypeEnabled,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: ScheduledMessageHistory.</summary>
	public enum ScheduledMessageHistoryFieldIndex
	{
		///<summary>ScheduledMessageHistoryId. </summary>
		ScheduledMessageHistoryId,
		///<summary>ParentCompanyId. </summary>
		ParentCompanyId,
		///<summary>ScheduledMessageId. </summary>
		ScheduledMessageId,
		///<summary>MessagegroupId. </summary>
		MessagegroupId,
		///<summary>Sent. </summary>
		Sent,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>Title. </summary>
		Title,
		///<summary>Message. </summary>
		Message,
		///<summary>RecipientsAsString. </summary>
		RecipientsAsString,
		///<summary>UIScheduleName. </summary>
		UIScheduleName,
		///<summary>MessageId. </summary>
		MessageId,
		///<summary>UIScheduleItemOccurrenceId. </summary>
		UIScheduleItemOccurrenceId,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: ScheduledMessageLanguage.</summary>
	public enum ScheduledMessageLanguageFieldIndex
	{
		///<summary>ScheduledMessageLanguageId. </summary>
		ScheduledMessageLanguageId,
		///<summary>ParentCompanyId. </summary>
		ParentCompanyId,
		///<summary>ScheduledMessageId. </summary>
		ScheduledMessageId,
		///<summary>LanguageId. </summary>
		LanguageId,
		///<summary>Title. </summary>
		Title,
		///<summary>Message. </summary>
		Message,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Scheduleitem.</summary>
	public enum ScheduleitemFieldIndex
	{
		///<summary>ScheduleitemId. </summary>
		ScheduleitemId,
		///<summary>ScheduleId. </summary>
		ScheduleId,
		///<summary>TimeStart. </summary>
		TimeStart,
		///<summary>TimeEnd. </summary>
		TimeEnd,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>SortOrder. </summary>
		SortOrder,
		///<summary>DayOfWeek. </summary>
		DayOfWeek,
		///<summary>ParentCompanyId. </summary>
		ParentCompanyId,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: ServiceMethod.</summary>
	public enum ServiceMethodFieldIndex
	{
		///<summary>ServiceMethodId. </summary>
		ServiceMethodId,
		///<summary>CompanyId. </summary>
		CompanyId,
		///<summary>OutletId. </summary>
		OutletId,
		///<summary>Active. </summary>
		Active,
		///<summary>Type. </summary>
		Type,
		///<summary>Name. </summary>
		Name,
		///<summary>Label. </summary>
		Label,
		///<summary>Description. </summary>
		Description,
		///<summary>AskEntryPermission. </summary>
		AskEntryPermission,
		///<summary>ServiceChargePercentage. </summary>
		ServiceChargePercentage,
		///<summary>ServiceChargeAmount. </summary>
		ServiceChargeAmount,
		///<summary>ServiceChargeProductId. </summary>
		ServiceChargeProductId,
		///<summary>ConfirmationActive. </summary>
		ConfirmationActive,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>MinimumOrderAmount. </summary>
		MinimumOrderAmount,
		///<summary>FreeDeliveryAmount. </summary>
		FreeDeliveryAmount,
		///<summary>ServiceChargeType. </summary>
		ServiceChargeType,
		///<summary>MinimumAmountForFreeDelivery. </summary>
		MinimumAmountForFreeDelivery,
		///<summary>MinimumAmountForFreeServiceCharge. </summary>
		MinimumAmountForFreeServiceCharge,
		///<summary>OrderCompleteTextMessage. </summary>
		OrderCompleteTextMessage,
		///<summary>OrderCompleteMailSubject. </summary>
		OrderCompleteMailSubject,
		///<summary>OrderCompleteMailMessage. </summary>
		OrderCompleteMailMessage,
		///<summary>OrderCompleteNotificationMethod. </summary>
		OrderCompleteNotificationMethod,
		///<summary>MaxCovers. </summary>
		MaxCovers,
		///<summary>CoversType. </summary>
		CoversType,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: ServiceMethodDeliverypointgroup.</summary>
	public enum ServiceMethodDeliverypointgroupFieldIndex
	{
		///<summary>ServiceMethodId. </summary>
		ServiceMethodId,
		///<summary>DeliverypointgroupId. </summary>
		DeliverypointgroupId,
		///<summary>ParentCompanyId. </summary>
		ParentCompanyId,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: SetupCode.</summary>
	public enum SetupCodeFieldIndex
	{
		///<summary>SetupCodeId. </summary>
		SetupCodeId,
		///<summary>CompanyId. </summary>
		CompanyId,
		///<summary>DeliverypointgroupId. </summary>
		DeliverypointgroupId,
		///<summary>Code. </summary>
		Code,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>ExpireDateUTC. </summary>
		ExpireDateUTC,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Site.</summary>
	public enum SiteFieldIndex
	{
		///<summary>SiteId. </summary>
		SiteId,
		///<summary>Name. </summary>
		Name,
		///<summary>SiteType. </summary>
		SiteType,
		///<summary>SiteTemplateId. </summary>
		SiteTemplateId,
		///<summary>CompanyId. </summary>
		CompanyId,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>Description. </summary>
		Description,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>LastModifiedUTC. </summary>
		LastModifiedUTC,
		///<summary>Version. </summary>
		Version,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: SiteCulture.</summary>
	public enum SiteCultureFieldIndex
	{
		///<summary>SiteCultureId. </summary>
		SiteCultureId,
		///<summary>SiteId. </summary>
		SiteId,
		///<summary>CultureCode. </summary>
		CultureCode,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>ParentCompanyId. </summary>
		ParentCompanyId,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: SiteLanguage.</summary>
	public enum SiteLanguageFieldIndex
	{
		///<summary>SiteLanguageId. </summary>
		SiteLanguageId,
		///<summary>SiteId. </summary>
		SiteId,
		///<summary>LanguageId. </summary>
		LanguageId,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>Description. </summary>
		Description,
		///<summary>ParentCompanyId. </summary>
		ParentCompanyId,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: SiteTemplate.</summary>
	public enum SiteTemplateFieldIndex
	{
		///<summary>SiteTemplateId. </summary>
		SiteTemplateId,
		///<summary>Name. </summary>
		Name,
		///<summary>SiteType. </summary>
		SiteType,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: SiteTemplateCulture.</summary>
	public enum SiteTemplateCultureFieldIndex
	{
		///<summary>SiteTemplateCultureId. </summary>
		SiteTemplateCultureId,
		///<summary>SiteTemplateId. </summary>
		SiteTemplateId,
		///<summary>CultureCode. </summary>
		CultureCode,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: SiteTemplateLanguage.</summary>
	public enum SiteTemplateLanguageFieldIndex
	{
		///<summary>SiteTemplateLanguageId. </summary>
		SiteTemplateLanguageId,
		///<summary>SiteTemplateId. </summary>
		SiteTemplateId,
		///<summary>LanguageId. </summary>
		LanguageId,
		///<summary>Name. </summary>
		Name,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: SmsInformation.</summary>
	public enum SmsInformationFieldIndex
	{
		///<summary>SmsInformationId. </summary>
		SmsInformationId,
		///<summary>Originator. </summary>
		Originator,
		///<summary>BodyText. </summary>
		BodyText,
		///<summary>GooglePlayPackage. </summary>
		GooglePlayPackage,
		///<summary>AppStoreAppId. </summary>
		AppStoreAppId,
		///<summary>IsDefault. </summary>
		IsDefault,
		///<summary>Name. </summary>
		Name,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: SmsKeyword.</summary>
	public enum SmsKeywordFieldIndex
	{
		///<summary>SmsKeywordId. </summary>
		SmsKeywordId,
		///<summary>SmsInformationId. </summary>
		SmsInformationId,
		///<summary>Keyword. </summary>
		Keyword,
		///<summary>CompanyId. </summary>
		CompanyId,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Station.</summary>
	public enum StationFieldIndex
	{
		///<summary>StationId. </summary>
		StationId,
		///<summary>ParentCompanyId. </summary>
		ParentCompanyId,
		///<summary>StationListId. </summary>
		StationListId,
		///<summary>Name. </summary>
		Name,
		///<summary>SortOrder. </summary>
		SortOrder,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>Scene. </summary>
		Scene,
		///<summary>SuccessMessage. </summary>
		SuccessMessage,
		///<summary>Description. </summary>
		Description,
		///<summary>Url. </summary>
		Url,
		///<summary>Channel. </summary>
		Channel,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: StationLanguage.</summary>
	public enum StationLanguageFieldIndex
	{
		///<summary>StationLanguageId. </summary>
		StationLanguageId,
		///<summary>ParentCompanyId. </summary>
		ParentCompanyId,
		///<summary>StationId. </summary>
		StationId,
		///<summary>LanguageId. </summary>
		LanguageId,
		///<summary>Name. </summary>
		Name,
		///<summary>Description. </summary>
		Description,
		///<summary>SuccessMessage. </summary>
		SuccessMessage,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: StationList.</summary>
	public enum StationListFieldIndex
	{
		///<summary>StationListId. </summary>
		StationListId,
		///<summary>CompanyId. </summary>
		CompanyId,
		///<summary>Name. </summary>
		Name,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Supplier.</summary>
	public enum SupplierFieldIndex
	{
		///<summary>SupplierId. </summary>
		SupplierId,
		///<summary>Name. </summary>
		Name,
		///<summary>Addressline1. </summary>
		Addressline1,
		///<summary>Addressline2. </summary>
		Addressline2,
		///<summary>Addressline3. </summary>
		Addressline3,
		///<summary>Addressline4. </summary>
		Addressline4,
		///<summary>Telephone1. </summary>
		Telephone1,
		///<summary>Telephone2. </summary>
		Telephone2,
		///<summary>Fax. </summary>
		Fax,
		///<summary>Email. </summary>
		Email,
		///<summary>Website. </summary>
		Website,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Supportagent.</summary>
	public enum SupportagentFieldIndex
	{
		///<summary>SupportagentId. </summary>
		SupportagentId,
		///<summary>Firstname. </summary>
		Firstname,
		///<summary>Lastname. </summary>
		Lastname,
		///<summary>LastnamePrefix. </summary>
		LastnamePrefix,
		///<summary>Email. </summary>
		Email,
		///<summary>Active. </summary>
		Active,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>Phonenumber. </summary>
		Phonenumber,
		///<summary>SlackUsername. </summary>
		SlackUsername,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Supportpool.</summary>
	public enum SupportpoolFieldIndex
	{
		///<summary>SupportpoolId. </summary>
		SupportpoolId,
		///<summary>Name. </summary>
		Name,
		///<summary>Phonenumber. </summary>
		Phonenumber,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>Email. </summary>
		Email,
		///<summary>SystemSupportPool. </summary>
		SystemSupportPool,
		///<summary>ContentSupportPool. </summary>
		ContentSupportPool,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: SupportpoolNotificationRecipient.</summary>
	public enum SupportpoolNotificationRecipientFieldIndex
	{
		///<summary>SupportpoolNotificationRecipientId. </summary>
		SupportpoolNotificationRecipientId,
		///<summary>SupportpoolId. </summary>
		SupportpoolId,
		///<summary>Name. </summary>
		Name,
		///<summary>Phonenumber. </summary>
		Phonenumber,
		///<summary>Email. </summary>
		Email,
		///<summary>NotifyOfflineTerminals. </summary>
		NotifyOfflineTerminals,
		///<summary>NotifyOfflineClients. </summary>
		NotifyOfflineClients,
		///<summary>NotifyUnprocessableOrders. </summary>
		NotifyUnprocessableOrders,
		///<summary>NotifyExpiredSteps. </summary>
		NotifyExpiredSteps,
		///<summary>NotifyTooMuchOfflineClientsJump. </summary>
		NotifyTooMuchOfflineClientsJump,
		///<summary>NotifyBouncedEmails. </summary>
		NotifyBouncedEmails,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: SupportpoolSupportagent.</summary>
	public enum SupportpoolSupportagentFieldIndex
	{
		///<summary>SupportpoolSupportagentId. </summary>
		SupportpoolSupportagentId,
		///<summary>SupportpoolId. </summary>
		SupportpoolId,
		///<summary>SupportagentId. </summary>
		SupportagentId,
		///<summary>OnSupport. </summary>
		OnSupport,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>NotifyOfflineTerminals. </summary>
		NotifyOfflineTerminals,
		///<summary>NotifyOfflineClients. </summary>
		NotifyOfflineClients,
		///<summary>NotifyUnprocessableOrders. </summary>
		NotifyUnprocessableOrders,
		///<summary>NotifyExpiredSteps. </summary>
		NotifyExpiredSteps,
		///<summary>NotifyTooMuchOfflineClientsJump. </summary>
		NotifyTooMuchOfflineClientsJump,
		///<summary>NotifyBouncedEmails. </summary>
		NotifyBouncedEmails,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Survey.</summary>
	public enum SurveyFieldIndex
	{
		///<summary>SurveyId. </summary>
		SurveyId,
		///<summary>CompanyId. </summary>
		CompanyId,
		///<summary>Name. </summary>
		Name,
		///<summary>ProcessedTitle. </summary>
		ProcessedTitle,
		///<summary>ProcessedMessage. </summary>
		ProcessedMessage,
		///<summary>SavingTitle. </summary>
		SavingTitle,
		///<summary>SavingMessage. </summary>
		SavingMessage,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>AnswerRequiredMessage. </summary>
		AnswerRequiredMessage,
		///<summary>AnswerRequiredTitle. </summary>
		AnswerRequiredTitle,
		///<summary>EmailResults. </summary>
		EmailResults,
		///<summary>Email. </summary>
		Email,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: SurveyAnswer.</summary>
	public enum SurveyAnswerFieldIndex
	{
		///<summary>SurveyAnswerId. </summary>
		SurveyAnswerId,
		///<summary>SurveyQuestionId. </summary>
		SurveyQuestionId,
		///<summary>Answer. </summary>
		Answer,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>SortOrder. </summary>
		SortOrder,
		///<summary>TargetSurveyQuestionId. </summary>
		TargetSurveyQuestionId,
		///<summary>ParentCompanyId. </summary>
		ParentCompanyId,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: SurveyAnswerLanguage.</summary>
	public enum SurveyAnswerLanguageFieldIndex
	{
		///<summary>SurveyAnswerLanguageId. </summary>
		SurveyAnswerLanguageId,
		///<summary>SurveyAnswerId. </summary>
		SurveyAnswerId,
		///<summary>LanguageId. </summary>
		LanguageId,
		///<summary>Answer. </summary>
		Answer,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>ParentCompanyId. </summary>
		ParentCompanyId,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: SurveyLanguage.</summary>
	public enum SurveyLanguageFieldIndex
	{
		///<summary>SurveyLanguageId. </summary>
		SurveyLanguageId,
		///<summary>SurveyId. </summary>
		SurveyId,
		///<summary>LanguageId. </summary>
		LanguageId,
		///<summary>ProcessedTitle. </summary>
		ProcessedTitle,
		///<summary>ProcessedMessage. </summary>
		ProcessedMessage,
		///<summary>SavingTitle. </summary>
		SavingTitle,
		///<summary>SavingMessage. </summary>
		SavingMessage,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>ParentCompanyId. </summary>
		ParentCompanyId,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: SurveyPage.</summary>
	public enum SurveyPageFieldIndex
	{
		///<summary>SurveyPageId. </summary>
		SurveyPageId,
		///<summary>SurveyId. </summary>
		SurveyId,
		///<summary>Name. </summary>
		Name,
		///<summary>SortOrder. </summary>
		SortOrder,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>ParentCompanyId. </summary>
		ParentCompanyId,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: SurveyQuestion.</summary>
	public enum SurveyQuestionFieldIndex
	{
		///<summary>SurveyQuestionId. </summary>
		SurveyQuestionId,
		///<summary>Question. </summary>
		Question,
		///<summary>Type. </summary>
		Type,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>Required. </summary>
		Required,
		///<summary>SortOrder. </summary>
		SortOrder,
		///<summary>FieldValue1. </summary>
		FieldValue1,
		///<summary>FieldValue10. </summary>
		FieldValue10,
		///<summary>FieldValue2. </summary>
		FieldValue2,
		///<summary>FieldValue3. </summary>
		FieldValue3,
		///<summary>FieldValue4. </summary>
		FieldValue4,
		///<summary>FieldValue5. </summary>
		FieldValue5,
		///<summary>FieldValue6. </summary>
		FieldValue6,
		///<summary>FieldValue7. </summary>
		FieldValue7,
		///<summary>FieldValue8. </summary>
		FieldValue8,
		///<summary>FieldValue9. </summary>
		FieldValue9,
		///<summary>ParentQuestionId. </summary>
		ParentQuestionId,
		///<summary>SurveyPageId. </summary>
		SurveyPageId,
		///<summary>NotesEnabled. </summary>
		NotesEnabled,
		///<summary>ParentCompanyId. </summary>
		ParentCompanyId,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: SurveyQuestionLanguage.</summary>
	public enum SurveyQuestionLanguageFieldIndex
	{
		///<summary>SurveyQuestionLanguageId. </summary>
		SurveyQuestionLanguageId,
		///<summary>SurveyQuestionId. </summary>
		SurveyQuestionId,
		///<summary>LanguageId. </summary>
		LanguageId,
		///<summary>Question. </summary>
		Question,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>ParentCompanyId. </summary>
		ParentCompanyId,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: SurveyResult.</summary>
	public enum SurveyResultFieldIndex
	{
		///<summary>SurveyResultId. </summary>
		SurveyResultId,
		///<summary>SurveyAnswerId. </summary>
		SurveyAnswerId,
		///<summary>SurveyQuestionId. </summary>
		SurveyQuestionId,
		///<summary>CustomerId. </summary>
		CustomerId,
		///<summary>ClientId. </summary>
		ClientId,
		///<summary>Comment. </summary>
		Comment,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>ParentCompanyId. </summary>
		ParentCompanyId,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>SubmittedUTC. </summary>
		SubmittedUTC,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Tag.</summary>
	public enum TagFieldIndex
	{
		///<summary>TagId. </summary>
		TagId,
		///<summary>CompanyId. </summary>
		CompanyId,
		///<summary>Name. </summary>
		Name,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: TaxTariff.</summary>
	public enum TaxTariffFieldIndex
	{
		///<summary>TaxTariffId. </summary>
		TaxTariffId,
		///<summary>CompanyId. </summary>
		CompanyId,
		///<summary>Name. </summary>
		Name,
		///<summary>Percentage. </summary>
		Percentage,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Terminal.</summary>
	public enum TerminalFieldIndex
	{
		///<summary>TerminalId. </summary>
		TerminalId,
		///<summary>CompanyId. </summary>
		CompanyId,
		///<summary>Name. </summary>
		Name,
		///<summary>LastStatus. </summary>
		LastStatus,
		///<summary>LastStatusText. </summary>
		LastStatusText,
		///<summary>LastStatusMessage. </summary>
		LastStatusMessage,
		///<summary>ReceiptTypes. </summary>
		ReceiptTypes,
		///<summary>RequestInterval. </summary>
		RequestInterval,
		///<summary>DiagnoseInterval. </summary>
		DiagnoseInterval,
		///<summary>PrinterName. </summary>
		PrinterName,
		///<summary>RecordAllRequestsToRequestLog. </summary>
		RecordAllRequestsToRequestLog,
		///<summary>TeamviewerId. </summary>
		TeamviewerId,
		///<summary>MaxDaysLogHistory. </summary>
		MaxDaysLogHistory,
		///<summary>POSConnectorType. </summary>
		POSConnectorType,
		///<summary>PMSConnectorType. </summary>
		PMSConnectorType,
		///<summary>SynchronizeWithPOSOnStart. </summary>
		SynchronizeWithPOSOnStart,
		///<summary>MaxStatusReports. </summary>
		MaxStatusReports,
		///<summary>Active. </summary>
		Active,
		///<summary>LanguageCode. </summary>
		LanguageCode,
		///<summary>UnlockDeliverypointProductId. </summary>
		UnlockDeliverypointProductId,
		///<summary>BatteryLowProductId. </summary>
		BatteryLowProductId,
		///<summary>ClientDisconnectedProductId. </summary>
		ClientDisconnectedProductId,
		///<summary>OrderFailedProductId. </summary>
		OrderFailedProductId,
		///<summary>SystemMessagesDeliverypointId. </summary>
		SystemMessagesDeliverypointId,
		///<summary>AltSystemMessagesDeliverypointId. </summary>
		AltSystemMessagesDeliverypointId,
		///<summary>IcrtouchprintermappingId. </summary>
		IcrtouchprintermappingId,
		///<summary>OperationMode. </summary>
		OperationMode,
		///<summary>OperationState. </summary>
		OperationState,
		///<summary>HandlingMethod. </summary>
		HandlingMethod,
		///<summary>NotificationForNewOrder. </summary>
		NotificationForNewOrder,
		///<summary>NotificationForOverdueOrder. </summary>
		NotificationForOverdueOrder,
		///<summary>MaxProcessTime. </summary>
		MaxProcessTime,
		///<summary>PosValue1. </summary>
		PosValue1,
		///<summary>PosValue2. </summary>
		PosValue2,
		///<summary>PosValue3. </summary>
		PosValue3,
		///<summary>PosValue4. </summary>
		PosValue4,
		///<summary>PosValue5. </summary>
		PosValue5,
		///<summary>PosValue6. </summary>
		PosValue6,
		///<summary>PosValue7. </summary>
		PosValue7,
		///<summary>PosValue8. </summary>
		PosValue8,
		///<summary>PosValue9. </summary>
		PosValue9,
		///<summary>PosValue10. </summary>
		PosValue10,
		///<summary>PosValue11. </summary>
		PosValue11,
		///<summary>PosValue12. </summary>
		PosValue12,
		///<summary>PosValue13. </summary>
		PosValue13,
		///<summary>PosValue14. </summary>
		PosValue14,
		///<summary>PosValue15. </summary>
		PosValue15,
		///<summary>PosValue16. </summary>
		PosValue16,
		///<summary>PosValue17. </summary>
		PosValue17,
		///<summary>PosValue18. </summary>
		PosValue18,
		///<summary>PosValue19. </summary>
		PosValue19,
		///<summary>PosValue20. </summary>
		PosValue20,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>AnnouncementDuration. </summary>
		AnnouncementDuration,
		///<summary>ForwardToTerminalId. </summary>
		ForwardToTerminalId,
		///<summary>DeviceId. </summary>
		DeviceId,
		///<summary>OnsiteServerDeliverypointgroupId. </summary>
		OnsiteServerDeliverypointgroupId,
		///<summary>OutOfChargeNotificationsSent. </summary>
		OutOfChargeNotificationsSent,
		///<summary>Browser1. </summary>
		Browser1,
		///<summary>Browser2. </summary>
		Browser2,
		///<summary>CmsPage. </summary>
		CmsPage,
		///<summary>AutomaticSignOnUserId. </summary>
		AutomaticSignOnUserId,
		///<summary>SurveyResultNotifications. </summary>
		SurveyResultNotifications,
		///<summary>PrintingEnabled. </summary>
		PrintingEnabled,
		///<summary>DeliverypointgroupId. </summary>
		DeliverypointgroupId,
		///<summary>LastStateOnline. </summary>
		LastStateOnline,
		///<summary>LastStateOperationMode. </summary>
		LastStateOperationMode,
		///<summary>UIModeId. </summary>
		UIModeId,
		///<summary>UseMonitoring. </summary>
		UseMonitoring,
		///<summary>MasterTab. </summary>
		MasterTab,
		///<summary>ResetTimeout. </summary>
		ResetTimeout,
		///<summary>PrinterConnected. </summary>
		PrinterConnected,
		///<summary>UseHardKeyboard. </summary>
		UseHardKeyboard,
		///<summary>MaxVolume. </summary>
		MaxVolume,
		///<summary>OfflineNotificationEnabled. </summary>
		OfflineNotificationEnabled,
		///<summary>LoadedSuccessfully. </summary>
		LoadedSuccessfully,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>OutOfChargeNotificationLastSentUTC. </summary>
		OutOfChargeNotificationLastSentUTC,
		///<summary>Type. </summary>
		Type,
		///<summary>StatusUpdatedUTC. </summary>
		StatusUpdatedUTC,
		///<summary>SynchronizeWithPMSOnStart. </summary>
		SynchronizeWithPMSOnStart,
		///<summary>TerminalConfigurationId. </summary>
		TerminalConfigurationId,
		///<summary>LastSyncUTC. </summary>
		LastSyncUTC,
		///<summary>PmsTerminalOfflineNotificationEnabled. </summary>
		PmsTerminalOfflineNotificationEnabled,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: TerminalConfiguration.</summary>
	public enum TerminalConfigurationFieldIndex
	{
		///<summary>TerminalConfigurationId. </summary>
		TerminalConfigurationId,
		///<summary>CompanyId. </summary>
		CompanyId,
		///<summary>Name. </summary>
		Name,
		///<summary>UIModeId. </summary>
		UIModeId,
		///<summary>Pincode. </summary>
		Pincode,
		///<summary>PincodeSU. </summary>
		PincodeSU,
		///<summary>PincodeGM. </summary>
		PincodeGM,
		///<summary>PrintingEnabled. </summary>
		PrintingEnabled,
		///<summary>UseHardKeyboard. </summary>
		UseHardKeyboard,
		///<summary>MaxVolume. </summary>
		MaxVolume,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>TerminalId. </summary>
		TerminalId,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: TerminalLog.</summary>
	public enum TerminalLogFieldIndex
	{
		///<summary>TerminalLogId. </summary>
		TerminalLogId,
		///<summary>TerminalId. </summary>
		TerminalId,
		///<summary>OrderId. </summary>
		OrderId,
		///<summary>OrderGuid. </summary>
		OrderGuid,
		///<summary>Type. </summary>
		Type,
		///<summary>Message. </summary>
		Message,
		///<summary>Status. </summary>
		Status,
		///<summary>Log. </summary>
		Log,
		///<summary>FromOperationMode. </summary>
		FromOperationMode,
		///<summary>ToOperationMode. </summary>
		ToOperationMode,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>TerminalLogFileId. </summary>
		TerminalLogFileId,
		///<summary>ParentCompanyId. </summary>
		ParentCompanyId,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: TerminalLogFile.</summary>
	public enum TerminalLogFileFieldIndex
	{
		///<summary>TerminalLogFileId. </summary>
		TerminalLogFileId,
		///<summary>Message. </summary>
		Message,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>Application. </summary>
		Application,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>LogDate. </summary>
		LogDate,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: TerminalMessageTemplateCategory.</summary>
	public enum TerminalMessageTemplateCategoryFieldIndex
	{
		///<summary>TerminalMessageTemplateCategoryId. </summary>
		TerminalMessageTemplateCategoryId,
		///<summary>MessageTemplateCategoryId. </summary>
		MessageTemplateCategoryId,
		///<summary>TerminalId. </summary>
		TerminalId,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: TerminalState.</summary>
	public enum TerminalStateFieldIndex
	{
		///<summary>TerminalStateId. </summary>
		TerminalStateId,
		///<summary>TerminalId. </summary>
		TerminalId,
		///<summary>Online. </summary>
		Online,
		///<summary>OperationMode. </summary>
		OperationMode,
		///<summary>LastBatteryLevel. </summary>
		LastBatteryLevel,
		///<summary>LastIsCharging. </summary>
		LastIsCharging,
		///<summary>Message. </summary>
		Message,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>TimeSpanTicks. </summary>
		TimeSpanTicks,
		///<summary>ParentCompanyId. </summary>
		ParentCompanyId,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Timestamp.</summary>
	public enum TimestampFieldIndex
	{
		///<summary>TimestampId. </summary>
		TimestampId,
		///<summary>AdvertisementConfigurationId. </summary>
		AdvertisementConfigurationId,
		///<summary>ModifiedAdvertisementConfigurationUTC. </summary>
		ModifiedAdvertisementConfigurationUTC,
		///<summary>PublishedAdvertisementConfigurationUTC. </summary>
		PublishedAdvertisementConfigurationUTC,
		///<summary>ModifiedAdvertisementConfigurationCustomTextUTC. </summary>
		ModifiedAdvertisementConfigurationCustomTextUTC,
		///<summary>PublishedAdvertisementConfigurationCustomTextUTC. </summary>
		PublishedAdvertisementConfigurationCustomTextUTC,
		///<summary>ModifiedAdvertisementConfigurationMediaUTC. </summary>
		ModifiedAdvertisementConfigurationMediaUTC,
		///<summary>PublishedAdvertisementConfigurationMediaUTC. </summary>
		PublishedAdvertisementConfigurationMediaUTC,
		///<summary>ClientId. </summary>
		ClientId,
		///<summary>ModifiedClientUTC. </summary>
		ModifiedClientUTC,
		///<summary>PublishedClientUTC. </summary>
		PublishedClientUTC,
		///<summary>ClientConfigurationId. </summary>
		ClientConfigurationId,
		///<summary>ModifiedClientConfigurationUTC. </summary>
		ModifiedClientConfigurationUTC,
		///<summary>PublishedClientConfigurationUTC. </summary>
		PublishedClientConfigurationUTC,
		///<summary>ModifiedClientConfigurationCustomTextUTC. </summary>
		ModifiedClientConfigurationCustomTextUTC,
		///<summary>PublishedClientConfigurationCustomTextUTC. </summary>
		PublishedClientConfigurationCustomTextUTC,
		///<summary>ModifiedClientConfigurationMediaUTC. </summary>
		ModifiedClientConfigurationMediaUTC,
		///<summary>PublishedClientConfigurationMediaUTC. </summary>
		PublishedClientConfigurationMediaUTC,
		///<summary>CompanyId. </summary>
		CompanyId,
		///<summary>ModifiedAmenitiesUTC. </summary>
		ModifiedAmenitiesUTC,
		///<summary>PublishedAmenitiesUTC. </summary>
		PublishedAmenitiesUTC,
		///<summary>ModifiedAmenitiesCustomTextUTC. </summary>
		ModifiedAmenitiesCustomTextUTC,
		///<summary>PublishedAmenitiesCustomTextUTC. </summary>
		PublishedAmenitiesCustomTextUTC,
		///<summary>ModifiedAnnouncementActionsUTC. </summary>
		ModifiedAnnouncementActionsUTC,
		///<summary>PublishedAnnouncementActionsUTC. </summary>
		PublishedAnnouncementActionsUTC,
		///<summary>ModifiedAvailabilitiesUTC. </summary>
		ModifiedAvailabilitiesUTC,
		///<summary>PublishedAvailabilitiesUTC. </summary>
		PublishedAvailabilitiesUTC,
		///<summary>ModifiedAvailabilitiesCustomTextUTC. </summary>
		ModifiedAvailabilitiesCustomTextUTC,
		///<summary>PublishedAvailabilitiesCustomTextUTC. </summary>
		PublishedAvailabilitiesCustomTextUTC,
		///<summary>ModifiedCloudStorageAccountsUTC. </summary>
		ModifiedCloudStorageAccountsUTC,
		///<summary>PublishedCloudStorageAccountsUTC. </summary>
		PublishedCloudStorageAccountsUTC,
		///<summary>ModifiedCompanyUTC. </summary>
		ModifiedCompanyUTC,
		///<summary>PublishedCompanyUTC. </summary>
		PublishedCompanyUTC,
		///<summary>ModifiedCompanyCustomTextUTC. </summary>
		ModifiedCompanyCustomTextUTC,
		///<summary>PublishedCompanyCustomTextUTC. </summary>
		PublishedCompanyCustomTextUTC,
		///<summary>ModifiedCompanyMediaUTC. </summary>
		ModifiedCompanyMediaUTC,
		///<summary>PublishedCompanyMediaUTC. </summary>
		PublishedCompanyMediaUTC,
		///<summary>ModifiedMessagegroupsUTC. </summary>
		ModifiedMessagegroupsUTC,
		///<summary>PublishedMessagegroupsUTC. </summary>
		PublishedMessagegroupsUTC,
		///<summary>ModifiedMessageTemplatesUTC. </summary>
		ModifiedMessageTemplatesUTC,
		///<summary>PublishedMessageTemplatesUTC. </summary>
		PublishedMessageTemplatesUTC,
		///<summary>ModifiedPmsTerminalStatusUTC. </summary>
		ModifiedPmsTerminalStatusUTC,
		///<summary>PublishedPmsTerminalStatusUTC. </summary>
		PublishedPmsTerminalStatusUTC,
		///<summary>ModifiedUIModesUTC. </summary>
		ModifiedUIModesUTC,
		///<summary>PublishedUIModesUTC. </summary>
		PublishedUIModesUTC,
		///<summary>ModifiedUIModesCustomTextUTC. </summary>
		ModifiedUIModesCustomTextUTC,
		///<summary>PublishedUIModesCustomTextUTC. </summary>
		PublishedUIModesCustomTextUTC,
		///<summary>ModifiedUIModesMediaUTC. </summary>
		ModifiedUIModesMediaUTC,
		///<summary>PublishedUIModesMediaUTC. </summary>
		PublishedUIModesMediaUTC,
		///<summary>ModifiedCompanyVenueCategoriesUTC. </summary>
		ModifiedCompanyVenueCategoriesUTC,
		///<summary>PublishedCompanyVenueCategoriesUTC. </summary>
		PublishedCompanyVenueCategoriesUTC,
		///<summary>ModifiedCompanyVenueCategoriesCustomTextUTC. </summary>
		ModifiedCompanyVenueCategoriesCustomTextUTC,
		///<summary>PublishedCompanyVenueCategoriesCustomTextUTC. </summary>
		PublishedCompanyVenueCategoriesCustomTextUTC,
		///<summary>DeliverypointId. </summary>
		DeliverypointId,
		///<summary>ModifiedMessagesUTC. </summary>
		ModifiedMessagesUTC,
		///<summary>PublishedMessagesUTC. </summary>
		PublishedMessagesUTC,
		///<summary>ModifiedDeliverypointUTC. </summary>
		ModifiedDeliverypointUTC,
		///<summary>PublishedDeliverypointUTC. </summary>
		PublishedDeliverypointUTC,
		///<summary>DeliverypointgroupId. </summary>
		DeliverypointgroupId,
		///<summary>ModifiedDeliverypointsUTC. </summary>
		ModifiedDeliverypointsUTC,
		///<summary>PublishedDeliverypointsUTC. </summary>
		PublishedDeliverypointsUTC,
		///<summary>ModifiedDeliveryTimeUTC. </summary>
		ModifiedDeliveryTimeUTC,
		///<summary>PublishedDeliveryTimeUTC. </summary>
		PublishedDeliveryTimeUTC,
		///<summary>DeviceIdentifier. </summary>
		DeviceIdentifier,
		///<summary>ModifiedNetmessagesUTC. </summary>
		ModifiedNetmessagesUTC,
		///<summary>PublishedNetmessagesUTC. </summary>
		PublishedNetmessagesUTC,
		///<summary>EntertainmentConfigurationId. </summary>
		EntertainmentConfigurationId,
		///<summary>ModifiedEntertainmentMediaUTC. </summary>
		ModifiedEntertainmentMediaUTC,
		///<summary>PublishedEntertainmentMediaUTC. </summary>
		PublishedEntertainmentMediaUTC,
		///<summary>ModifiedEntertainmentConfigurationUTC. </summary>
		ModifiedEntertainmentConfigurationUTC,
		///<summary>PublishedEntertainmentConfigurationUTC. </summary>
		PublishedEntertainmentConfigurationUTC,
		///<summary>ModifiedEntertainmentConfigurationMediaUTC. </summary>
		ModifiedEntertainmentConfigurationMediaUTC,
		///<summary>PublishedEntertainmentConfigurationMediaUTC. </summary>
		PublishedEntertainmentConfigurationMediaUTC,
		///<summary>ModifiedEntertainmentCategoriesUTC. </summary>
		ModifiedEntertainmentCategoriesUTC,
		///<summary>PublishedEntertainmentCategoriesUTC. </summary>
		PublishedEntertainmentCategoriesUTC,
		///<summary>ModifiedEntertainmentCategoriesCustomTextUTC. </summary>
		ModifiedEntertainmentCategoriesCustomTextUTC,
		///<summary>PublishedEntertainmentCategoriesCustomTextUTC. </summary>
		PublishedEntertainmentCategoriesCustomTextUTC,
		///<summary>ModifiedEntertainmentCategoriesMediaUTC. </summary>
		ModifiedEntertainmentCategoriesMediaUTC,
		///<summary>PublishedEntertainmentCategoriesMediaUTC. </summary>
		PublishedEntertainmentCategoriesMediaUTC,
		///<summary>InfraredConfigurationId. </summary>
		InfraredConfigurationId,
		///<summary>ModifiedInfraredConfigurationUTC. </summary>
		ModifiedInfraredConfigurationUTC,
		///<summary>PublishedInfraredConfigurationUTC. </summary>
		PublishedInfraredConfigurationUTC,
		///<summary>MapId. </summary>
		MapId,
		///<summary>ModifiedMapUTC. </summary>
		ModifiedMapUTC,
		///<summary>PublishedMapUTC. </summary>
		PublishedMapUTC,
		///<summary>MenuId. </summary>
		MenuId,
		///<summary>ModifiedAlterationsUTC. </summary>
		ModifiedAlterationsUTC,
		///<summary>PublishedAlterationsUTC. </summary>
		PublishedAlterationsUTC,
		///<summary>ModifiedAlterationsCustomTextUTC. </summary>
		ModifiedAlterationsCustomTextUTC,
		///<summary>PublishedAlterationsCustomTextUTC. </summary>
		PublishedAlterationsCustomTextUTC,
		///<summary>ModifiedAlterationsMediaUTC. </summary>
		ModifiedAlterationsMediaUTC,
		///<summary>PublishedAlterationsMediaUTC. </summary>
		PublishedAlterationsMediaUTC,
		///<summary>ModifiedMenuUTC. </summary>
		ModifiedMenuUTC,
		///<summary>PublishedMenuUTC. </summary>
		PublishedMenuUTC,
		///<summary>ModifiedMenuCustomTextUTC. </summary>
		ModifiedMenuCustomTextUTC,
		///<summary>PublishedMenuCustomTextUTC. </summary>
		PublishedMenuCustomTextUTC,
		///<summary>ModifiedMenuMediaUTC. </summary>
		ModifiedMenuMediaUTC,
		///<summary>PublishedMenuMediaUTC. </summary>
		PublishedMenuMediaUTC,
		///<summary>ModifiedProductgroupsUTC. </summary>
		ModifiedProductgroupsUTC,
		///<summary>PublishedProductgroupsUTC. </summary>
		PublishedProductgroupsUTC,
		///<summary>ModifiedProductgroupsCustomTextUTC. </summary>
		ModifiedProductgroupsCustomTextUTC,
		///<summary>PublishedProductgroupsCustomTextUTC. </summary>
		PublishedProductgroupsCustomTextUTC,
		///<summary>ModifiedProductgroupsMediaUTC. </summary>
		ModifiedProductgroupsMediaUTC,
		///<summary>PublishedProductgroupsMediaUTC. </summary>
		PublishedProductgroupsMediaUTC,
		///<summary>ModifiedProductsUTC. </summary>
		ModifiedProductsUTC,
		///<summary>PublishedProductsUTC. </summary>
		PublishedProductsUTC,
		///<summary>ModifiedProductsCustomTextUTC. </summary>
		ModifiedProductsCustomTextUTC,
		///<summary>PublishedProductsCustomTextUTC. </summary>
		PublishedProductsCustomTextUTC,
		///<summary>ModifiedProductsMediaUTC. </summary>
		ModifiedProductsMediaUTC,
		///<summary>PublishedProductsMediaUTC. </summary>
		PublishedProductsMediaUTC,
		///<summary>ModifiedSchedulesUTC. </summary>
		ModifiedSchedulesUTC,
		///<summary>PublishedSchedulesUTC. </summary>
		PublishedSchedulesUTC,
		///<summary>PointOfInterestId. </summary>
		PointOfInterestId,
		///<summary>ModifiedPointOfInterestUTC. </summary>
		ModifiedPointOfInterestUTC,
		///<summary>PublishedPointOfInterestUTC. </summary>
		PublishedPointOfInterestUTC,
		///<summary>ModifiedPointOfInterestCustomTextUTC. </summary>
		ModifiedPointOfInterestCustomTextUTC,
		///<summary>PublishedPointOfInterestCustomTextUTC. </summary>
		PublishedPointOfInterestCustomTextUTC,
		///<summary>ModifiedPointOfInterestMediaUTC. </summary>
		ModifiedPointOfInterestMediaUTC,
		///<summary>PublishedPointOfInterestMediaUTC. </summary>
		PublishedPointOfInterestMediaUTC,
		///<summary>ModifiedPointOfInterestVenueCategoriesUTC. </summary>
		ModifiedPointOfInterestVenueCategoriesUTC,
		///<summary>PublishedPointOfInterestVenueCategoriesUTC. </summary>
		PublishedPointOfInterestVenueCategoriesUTC,
		///<summary>ModifiedPointOfInterestVenueCategoriesCustomTextUTC. </summary>
		ModifiedPointOfInterestVenueCategoriesCustomTextUTC,
		///<summary>PublishedPointOfInterestVenueCategoriesCustomTextUTC. </summary>
		PublishedPointOfInterestVenueCategoriesCustomTextUTC,
		///<summary>PriceScheduleId. </summary>
		PriceScheduleId,
		///<summary>ModifiedPriceScheduleUTC. </summary>
		ModifiedPriceScheduleUTC,
		///<summary>PublishedPriceScheduleUTC. </summary>
		PublishedPriceScheduleUTC,
		///<summary>RoomControlConfigurationId. </summary>
		RoomControlConfigurationId,
		///<summary>ModifiedRoomControlConfigurationUTC. </summary>
		ModifiedRoomControlConfigurationUTC,
		///<summary>PublishedRoomControlConfigurationUTC. </summary>
		PublishedRoomControlConfigurationUTC,
		///<summary>ModifiedRoomControlConfigurationCustomTextUTC. </summary>
		ModifiedRoomControlConfigurationCustomTextUTC,
		///<summary>PublishedRoomControlConfigurationCustomTextUTC. </summary>
		PublishedRoomControlConfigurationCustomTextUTC,
		///<summary>ModifiedRoomControlConfigurationMediaUTC. </summary>
		ModifiedRoomControlConfigurationMediaUTC,
		///<summary>PublishedRoomControlConfigurationMediaUTC. </summary>
		PublishedRoomControlConfigurationMediaUTC,
		///<summary>SiteId. </summary>
		SiteId,
		///<summary>ModifiedSiteUTC. </summary>
		ModifiedSiteUTC,
		///<summary>PublishedSiteUTC. </summary>
		PublishedSiteUTC,
		///<summary>ModifiedSiteCustomTextUTC. </summary>
		ModifiedSiteCustomTextUTC,
		///<summary>PublishedSiteCustomTextUTC. </summary>
		PublishedSiteCustomTextUTC,
		///<summary>ModifiedSiteMediaUTC. </summary>
		ModifiedSiteMediaUTC,
		///<summary>PublishedSiteMediaUTC. </summary>
		PublishedSiteMediaUTC,
		///<summary>TerminalId. </summary>
		TerminalId,
		///<summary>ModifiedTerminalUTC. </summary>
		ModifiedTerminalUTC,
		///<summary>PublishedTerminalUTC. </summary>
		PublishedTerminalUTC,
		///<summary>ModifiedOrdersHistoryUTC. </summary>
		ModifiedOrdersHistoryUTC,
		///<summary>PublishedOrdersHistoryUTC. </summary>
		PublishedOrdersHistoryUTC,
		///<summary>ModifiedOrdersMasterUTC. </summary>
		ModifiedOrdersMasterUTC,
		///<summary>PublishedOrdersMasterUTC. </summary>
		PublishedOrdersMasterUTC,
		///<summary>ModifiedOrdersUTC. </summary>
		ModifiedOrdersUTC,
		///<summary>PublishedOrdersUTC. </summary>
		PublishedOrdersUTC,
		///<summary>TerminalConfigurationId. </summary>
		TerminalConfigurationId,
		///<summary>ModifiedTerminalConfigurationUTC. </summary>
		ModifiedTerminalConfigurationUTC,
		///<summary>PublishedTerminalConfigurationUTC. </summary>
		PublishedTerminalConfigurationUTC,
		///<summary>UIModeId. </summary>
		UIModeId,
		///<summary>ModifiedUIModeUTC. </summary>
		ModifiedUIModeUTC,
		///<summary>PublishedUIModeUTC. </summary>
		PublishedUIModeUTC,
		///<summary>ModifiedUIModeCustomTextUTC. </summary>
		ModifiedUIModeCustomTextUTC,
		///<summary>PublishedUIModeCustomTextUTC. </summary>
		PublishedUIModeCustomTextUTC,
		///<summary>ModifiedUIModeMediaUTC. </summary>
		ModifiedUIModeMediaUTC,
		///<summary>PublishedUIModeMediaUTC. </summary>
		PublishedUIModeMediaUTC,
		///<summary>UIScheduleId. </summary>
		UIScheduleId,
		///<summary>ModifiedUIScheduleUTC. </summary>
		ModifiedUIScheduleUTC,
		///<summary>PublishedUIScheduleUTC. </summary>
		PublishedUIScheduleUTC,
		///<summary>UIThemeId. </summary>
		UIThemeId,
		///<summary>ModifiedUIThemeUTC. </summary>
		ModifiedUIThemeUTC,
		///<summary>PublishedUIThemeUTC. </summary>
		PublishedUIThemeUTC,
		///<summary>ModifiedUIThemeMediaUTC. </summary>
		ModifiedUIThemeMediaUTC,
		///<summary>PublishedUIThemeMediaUTC. </summary>
		PublishedUIThemeMediaUTC,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>ModifiedCompanyReleasesUTC. </summary>
		ModifiedCompanyReleasesUTC,
		///<summary>PublishedCompanyReleasesUTC. </summary>
		PublishedCompanyReleasesUTC,
		///<summary>ModifiedMapCustomTextUTC. </summary>
		ModifiedMapCustomTextUTC,
		///<summary>PublishedMapCustomTextUTC. </summary>
		PublishedMapCustomTextUTC,
		///<summary>ModifiedMapMediaUTC. </summary>
		ModifiedMapMediaUTC,
		///<summary>PublishedMapMediaUTC. </summary>
		PublishedMapMediaUTC,
		///<summary>ModifiedInfraredConfigurationCustomTextUTC. </summary>
		ModifiedInfraredConfigurationCustomTextUTC,
		///<summary>PublishedInfraredConfigurationCustomTextUTC. </summary>
		PublishedInfraredConfigurationCustomTextUTC,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: TimeZone.</summary>
	public enum TimeZoneFieldIndex
	{
		///<summary>TimeZoneId. </summary>
		TimeZoneId,
		///<summary>Name. </summary>
		Name,
		///<summary>NameAndroid. </summary>
		NameAndroid,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>NameDotNet. </summary>
		NameDotNet,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Translation.</summary>
	public enum TranslationFieldIndex
	{
		///<summary>TranslationId. </summary>
		TranslationId,
		///<summary>TranslationKey. </summary>
		TranslationKey,
		///<summary>LanguageCode. </summary>
		LanguageCode,
		///<summary>TranslationValue. </summary>
		TranslationValue,
		///<summary>LocallyCreated. </summary>
		LocallyCreated,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: UIElement.</summary>
	public enum UIElementFieldIndex
	{
		///<summary>UIElementId. </summary>
		UIElementId,
		///<summary>ModuleNameSystem. </summary>
		ModuleNameSystem,
		///<summary>NameSystem. </summary>
		NameSystem,
		///<summary>NameFull. </summary>
		NameFull,
		///<summary>NameShort. </summary>
		NameShort,
		///<summary>Url. </summary>
		Url,
		///<summary>DisplayInMenu. </summary>
		DisplayInMenu,
		///<summary>TypeNameFull. </summary>
		TypeNameFull,
		///<summary>SortOrder. </summary>
		SortOrder,
		///<summary>FreeAccess. </summary>
		FreeAccess,
		///<summary>LicensingFree. </summary>
		LicensingFree,
		///<summary>UserRightsFree. </summary>
		UserRightsFree,
		///<summary>IsConfigurationItem. </summary>
		IsConfigurationItem,
		///<summary>EntityName. </summary>
		EntityName,
		///<summary>ForLocalUseWhileSyncingIsUpdated. </summary>
		ForLocalUseWhileSyncingIsUpdated,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: UIElementCustom.</summary>
	public enum UIElementCustomFieldIndex
	{
		///<summary>TypeNameFull. </summary>
		TypeNameFull,
		///<summary>ModuleNameSystem. </summary>
		ModuleNameSystem,
		///<summary>NameFull. </summary>
		NameFull,
		///<summary>NameShort. </summary>
		NameShort,
		///<summary>SortOrder. </summary>
		SortOrder,
		///<summary>IsConfigurationItem. </summary>
		IsConfigurationItem,
		///<summary>ForLocalUseWhileSyncingIsUpdated. </summary>
		ForLocalUseWhileSyncingIsUpdated,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: UIElementSubPanel.</summary>
	public enum UIElementSubPanelFieldIndex
	{
		///<summary>UIElementSubPanelId. </summary>
		UIElementSubPanelId,
		///<summary>NameSystem. </summary>
		NameSystem,
		///<summary>NameFull. </summary>
		NameFull,
		///<summary>NameShort. </summary>
		NameShort,
		///<summary>Url. </summary>
		Url,
		///<summary>TypeNameFull. </summary>
		TypeNameFull,
		///<summary>SortOrder. </summary>
		SortOrder,
		///<summary>FreeAccess. </summary>
		FreeAccess,
		///<summary>LicensingFree. </summary>
		LicensingFree,
		///<summary>EntityName. </summary>
		EntityName,
		///<summary>OnTabPage. </summary>
		OnTabPage,
		///<summary>ForLocalUseWhileSyncingIsUpdated. </summary>
		ForLocalUseWhileSyncingIsUpdated,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: UIElementSubPanelCustom.</summary>
	public enum UIElementSubPanelCustomFieldIndex
	{
		///<summary>TypeNameFull. </summary>
		TypeNameFull,
		///<summary>NameFull. </summary>
		NameFull,
		///<summary>NameShort. </summary>
		NameShort,
		///<summary>SortOrder. </summary>
		SortOrder,
		///<summary>OnTabPage. </summary>
		OnTabPage,
		///<summary>ForLocalUseWhileSyncingIsUpdated. </summary>
		ForLocalUseWhileSyncingIsUpdated,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: UIElementSubPanelUIElement.</summary>
	public enum UIElementSubPanelUIElementFieldIndex
	{
		///<summary>UIElementSubPanelUIElementId. </summary>
		UIElementSubPanelUIElementId,
		///<summary>UIElementSubPanelTypeNameFull. </summary>
		UIElementSubPanelTypeNameFull,
		///<summary>ParentUIElementTypeNameFull. </summary>
		ParentUIElementTypeNameFull,
		///<summary>ParentUIElementSubPanelTypeNameFull. </summary>
		ParentUIElementSubPanelTypeNameFull,
		///<summary>ForLocalUseWhileSyncingIsUpdated. </summary>
		ForLocalUseWhileSyncingIsUpdated,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: UIFooterItem.</summary>
	public enum UIFooterItemFieldIndex
	{
		///<summary>UIFooterItemId. </summary>
		UIFooterItemId,
		///<summary>UIModeId. </summary>
		UIModeId,
		///<summary>Name. </summary>
		Name,
		///<summary>Type. </summary>
		Type,
		///<summary>Position. </summary>
		Position,
		///<summary>SortOrder. </summary>
		SortOrder,
		///<summary>ActionIntent. </summary>
		ActionIntent,
		///<summary>Visible. </summary>
		Visible,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>ParentCompanyId. </summary>
		ParentCompanyId,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: UIFooterItemLanguage.</summary>
	public enum UIFooterItemLanguageFieldIndex
	{
		///<summary>UIFooterItemLanguageId. </summary>
		UIFooterItemLanguageId,
		///<summary>UIFooterItemId. </summary>
		UIFooterItemId,
		///<summary>LanguageId. </summary>
		LanguageId,
		///<summary>Name. </summary>
		Name,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>ParentCompanyId. </summary>
		ParentCompanyId,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: UIMode.</summary>
	public enum UIModeFieldIndex
	{
		///<summary>UIModeId. </summary>
		UIModeId,
		///<summary>CompanyId. </summary>
		CompanyId,
		///<summary>Name. </summary>
		Name,
		///<summary>ShowServiceOptions. </summary>
		ShowServiceOptions,
		///<summary>ShowRequestBill. </summary>
		ShowRequestBill,
		///<summary>ShowDeliverypoint. </summary>
		ShowDeliverypoint,
		///<summary>ShowClock. </summary>
		ShowClock,
		///<summary>ShowBattery. </summary>
		ShowBattery,
		///<summary>ShowBrightness. </summary>
		ShowBrightness,
		///<summary>ShowFullscreenEyecatcher. </summary>
		ShowFullscreenEyecatcher,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>Type. </summary>
		Type,
		///<summary>DefaultUITabId. </summary>
		DefaultUITabId,
		///<summary>PointOfInterestId. </summary>
		PointOfInterestId,
		///<summary>ShowFooterLogo. </summary>
		ShowFooterLogo,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: UISchedule.</summary>
	public enum UIScheduleFieldIndex
	{
		///<summary>UIScheduleId. </summary>
		UIScheduleId,
		///<summary>CompanyId. </summary>
		CompanyId,
		///<summary>DeliverypointgroupId. </summary>
		DeliverypointgroupId,
		///<summary>Name. </summary>
		Name,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>Type. </summary>
		Type,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: UIScheduleItem.</summary>
	public enum UIScheduleItemFieldIndex
	{
		///<summary>UIScheduleItemId. </summary>
		UIScheduleItemId,
		///<summary>ParentCompanyId. </summary>
		ParentCompanyId,
		///<summary>UIScheduleId. </summary>
		UIScheduleId,
		///<summary>UIWidgetId. </summary>
		UIWidgetId,
		///<summary>MediaId. </summary>
		MediaId,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>ScheduledMessageId. </summary>
		ScheduledMessageId,
		///<summary>ReportProcessingTaskTemplateId. </summary>
		ReportProcessingTaskTemplateId,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: UIScheduleItemOccurrence.</summary>
	public enum UIScheduleItemOccurrenceFieldIndex
	{
		///<summary>UIScheduleItemOccurrenceId. </summary>
		UIScheduleItemOccurrenceId,
		///<summary>ParentCompanyId. </summary>
		ParentCompanyId,
		///<summary>UIScheduleItemId. </summary>
		UIScheduleItemId,
		///<summary>StartTime. </summary>
		StartTime,
		///<summary>EndTime. </summary>
		EndTime,
		///<summary>Recurring. </summary>
		Recurring,
		///<summary>RecurrenceType. </summary>
		RecurrenceType,
		///<summary>RecurrenceRange. </summary>
		RecurrenceRange,
		///<summary>RecurrenceStart. </summary>
		RecurrenceStart,
		///<summary>RecurrenceEnd. </summary>
		RecurrenceEnd,
		///<summary>RecurrenceOccurrenceCount. </summary>
		RecurrenceOccurrenceCount,
		///<summary>RecurrencePeriodicity. </summary>
		RecurrencePeriodicity,
		///<summary>RecurrenceDayNumber. </summary>
		RecurrenceDayNumber,
		///<summary>RecurrenceWeekDays. </summary>
		RecurrenceWeekDays,
		///<summary>RecurrenceWeekOfMonth. </summary>
		RecurrenceWeekOfMonth,
		///<summary>RecurrenceMonth. </summary>
		RecurrenceMonth,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>BackgroundColor. </summary>
		BackgroundColor,
		///<summary>TextColor. </summary>
		TextColor,
		///<summary>MessagegroupId. </summary>
		MessagegroupId,
		///<summary>StartTimeUTC. </summary>
		StartTimeUTC,
		///<summary>EndTimeUTC. </summary>
		EndTimeUTC,
		///<summary>RecurrenceStartUTC. </summary>
		RecurrenceStartUTC,
		///<summary>RecurrenceEndUTC. </summary>
		RecurrenceEndUTC,
		///<summary>Type. </summary>
		Type,
		///<summary>RecurrenceIndex. </summary>
		RecurrenceIndex,
		///<summary>ParentUIScheduleItemOccurrenceId. </summary>
		ParentUIScheduleItemOccurrenceId,
		///<summary>LastTriggeredUTC. </summary>
		LastTriggeredUTC,
		///<summary>ReportProcessingTaskTemplateId. </summary>
		ReportProcessingTaskTemplateId,
		///<summary>UIScheduleId. </summary>
		UIScheduleId,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: UITab.</summary>
	public enum UITabFieldIndex
	{
		///<summary>UITabId. </summary>
		UITabId,
		///<summary>UIModeId. </summary>
		UIModeId,
		///<summary>Caption. </summary>
		Caption,
		///<summary>Type. </summary>
		Type,
		///<summary>CategoryId. </summary>
		CategoryId,
		///<summary>EntertainmentId. </summary>
		EntertainmentId,
		///<summary>URL. </summary>
		URL,
		///<summary>Width. </summary>
		Width,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>SortOrder. </summary>
		SortOrder,
		///<summary>Zoom. </summary>
		Zoom,
		///<summary>Visible. </summary>
		Visible,
		///<summary>SiteId. </summary>
		SiteId,
		///<summary>RestrictedAccess. </summary>
		RestrictedAccess,
		///<summary>ParentCompanyId. </summary>
		ParentCompanyId,
		///<summary>AllCategoryVisible. </summary>
		AllCategoryVisible,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>ShowPmsMessagegroups. </summary>
		ShowPmsMessagegroups,
		///<summary>MapId. </summary>
		MapId,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: UITabLanguage.</summary>
	public enum UITabLanguageFieldIndex
	{
		///<summary>UITabLanguageId. </summary>
		UITabLanguageId,
		///<summary>LanguageId. </summary>
		LanguageId,
		///<summary>UITabId. </summary>
		UITabId,
		///<summary>Caption. </summary>
		Caption,
		///<summary>URL. </summary>
		URL,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>Zoom. </summary>
		Zoom,
		///<summary>ParentCompanyId. </summary>
		ParentCompanyId,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: UITheme.</summary>
	public enum UIThemeFieldIndex
	{
		///<summary>UIThemeId. </summary>
		UIThemeId,
		///<summary>ListCategoryBackgroundColor. </summary>
		ListCategoryBackgroundColor,
		///<summary>ListCategoryDividerColor. </summary>
		ListCategoryDividerColor,
		///<summary>ListCategoryTextColor. </summary>
		ListCategoryTextColor,
		///<summary>ListCategorySelectedBackgroundColor. </summary>
		ListCategorySelectedBackgroundColor,
		///<summary>ListCategorySelectedDividerColor. </summary>
		ListCategorySelectedDividerColor,
		///<summary>ListCategorySelectedTextColor. </summary>
		ListCategorySelectedTextColor,
		///<summary>ListItemBackgroundColor. </summary>
		ListItemBackgroundColor,
		///<summary>ListItemDividerColor. </summary>
		ListItemDividerColor,
		///<summary>ListItemTextColor. </summary>
		ListItemTextColor,
		///<summary>ListItemSelectedBackgroundColor. </summary>
		ListItemSelectedBackgroundColor,
		///<summary>ListItemSelectedDividerColor. </summary>
		ListItemSelectedDividerColor,
		///<summary>ListItemSelectedTextColor. </summary>
		ListItemSelectedTextColor,
		///<summary>WidgetBackgroundColor. </summary>
		WidgetBackgroundColor,
		///<summary>WidgetBorderColor. </summary>
		WidgetBorderColor,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>Name. </summary>
		Name,
		///<summary>WidgetTextColor. </summary>
		WidgetTextColor,
		///<summary>HeaderColor. </summary>
		HeaderColor,
		///<summary>TabBackgroundBottomColor. </summary>
		TabBackgroundBottomColor,
		///<summary>TabBackgroundTopColor. </summary>
		TabBackgroundTopColor,
		///<summary>TabDividerColor. </summary>
		TabDividerColor,
		///<summary>TabBorderColor. </summary>
		TabBorderColor,
		///<summary>TabActiveTextColor. </summary>
		TabActiveTextColor,
		///<summary>TabInactiveTextColor. </summary>
		TabInactiveTextColor,
		///<summary>PageBackgroundColor. </summary>
		PageBackgroundColor,
		///<summary>PageBorderColor. </summary>
		PageBorderColor,
		///<summary>PageTitleTextColor. </summary>
		PageTitleTextColor,
		///<summary>PageDescriptionTextColor. </summary>
		PageDescriptionTextColor,
		///<summary>PageDividerTopColor. </summary>
		PageDividerTopColor,
		///<summary>PageDividerBottomColor. </summary>
		PageDividerBottomColor,
		///<summary>FooterBackgroundBottomColor. </summary>
		FooterBackgroundBottomColor,
		///<summary>FooterBackgroundTopColor. </summary>
		FooterBackgroundTopColor,
		///<summary>FooterTextColor. </summary>
		FooterTextColor,
		///<summary>PageErrorTextColor. </summary>
		PageErrorTextColor,
		///<summary>FooterDividerColor. </summary>
		FooterDividerColor,
		///<summary>PagePriceTextColor. </summary>
		PagePriceTextColor,
		///<summary>PageFooterColor. </summary>
		PageFooterColor,
		///<summary>ButtonBackgroundTopColor. </summary>
		ButtonBackgroundTopColor,
		///<summary>ButtonBackgroundBottomColor. </summary>
		ButtonBackgroundBottomColor,
		///<summary>ButtonBorderColor. </summary>
		ButtonBorderColor,
		///<summary>ButtonTextColor. </summary>
		ButtonTextColor,
		///<summary>ButtonPositiveBackgroundTopColor. </summary>
		ButtonPositiveBackgroundTopColor,
		///<summary>ButtonPositiveBackgroundBottomColor. </summary>
		ButtonPositiveBackgroundBottomColor,
		///<summary>ButtonPositiveBorderColor. </summary>
		ButtonPositiveBorderColor,
		///<summary>ButtonPositiveTextColor. </summary>
		ButtonPositiveTextColor,
		///<summary>ButtonDisabledBackgroundTopColor. </summary>
		ButtonDisabledBackgroundTopColor,
		///<summary>ButtonDisabledBackgroundBottomColor. </summary>
		ButtonDisabledBackgroundBottomColor,
		///<summary>ButtonDisabledBorderColor. </summary>
		ButtonDisabledBorderColor,
		///<summary>ButtonDisabledTextColor. </summary>
		ButtonDisabledTextColor,
		///<summary>DialogBackgroundColor. </summary>
		DialogBackgroundColor,
		///<summary>DialogBorderColor. </summary>
		DialogBorderColor,
		///<summary>DialogPanelBackgroundColor. </summary>
		DialogPanelBackgroundColor,
		///<summary>DialogPanelBorderColor. </summary>
		DialogPanelBorderColor,
		///<summary>DialogPrimaryTextColor. </summary>
		DialogPrimaryTextColor,
		///<summary>DialogSecondaryTextColor. </summary>
		DialogSecondaryTextColor,
		///<summary>TextboxBackgroundColor. </summary>
		TextboxBackgroundColor,
		///<summary>TextboxBorderColor. </summary>
		TextboxBorderColor,
		///<summary>TextboxTextColor. </summary>
		TextboxTextColor,
		///<summary>TextboxCursorColor. </summary>
		TextboxCursorColor,
		///<summary>PageInstructionsTextColor. </summary>
		PageInstructionsTextColor,
		///<summary>DialogTitleTextColor. </summary>
		DialogTitleTextColor,
		///<summary>SpinnerBackgroundColor. </summary>
		SpinnerBackgroundColor,
		///<summary>SpinnerBorderColor. </summary>
		SpinnerBorderColor,
		///<summary>SpinnerTextColor. </summary>
		SpinnerTextColor,
		///<summary>CompanyId. </summary>
		CompanyId,
		///<summary>RoomControlButtonOuterBorderColor. </summary>
		RoomControlButtonOuterBorderColor,
		///<summary>RoomControlButtonInnerBorderTopColor. </summary>
		RoomControlButtonInnerBorderTopColor,
		///<summary>RoomControlButtonInnerBorderBottomColor. </summary>
		RoomControlButtonInnerBorderBottomColor,
		///<summary>RoomControlButtonBackgroundTopColor. </summary>
		RoomControlButtonBackgroundTopColor,
		///<summary>RoomControlButtonBackgroundBottomColor. </summary>
		RoomControlButtonBackgroundBottomColor,
		///<summary>RoomControlDividerTopColor. </summary>
		RoomControlDividerTopColor,
		///<summary>RoomControlDividerBottomColor. </summary>
		RoomControlDividerBottomColor,
		///<summary>RoomControlButtonTextColor. </summary>
		RoomControlButtonTextColor,
		///<summary>RoomControlTitleColor. </summary>
		RoomControlTitleColor,
		///<summary>RoomControlTextColor. </summary>
		RoomControlTextColor,
		///<summary>RoomControlPageBackgroundColor. </summary>
		RoomControlPageBackgroundColor,
		///<summary>RoomControlSpinnerBorderColor. </summary>
		RoomControlSpinnerBorderColor,
		///<summary>RoomControlSpinnerBackgroundColor. </summary>
		RoomControlSpinnerBackgroundColor,
		///<summary>RoomControlSpinnerTextColor. </summary>
		RoomControlSpinnerTextColor,
		///<summary>RoomControlButtonIndicatorColor. </summary>
		RoomControlButtonIndicatorColor,
		///<summary>RoomControlButtonIndicatorBackgroundColor. </summary>
		RoomControlButtonIndicatorBackgroundColor,
		///<summary>RoomControlHeaderButtonBackgroundColor. </summary>
		RoomControlHeaderButtonBackgroundColor,
		///<summary>RoomControlHeaderButtonSelectorColor. </summary>
		RoomControlHeaderButtonSelectorColor,
		///<summary>RoomControlHeaderButtonTextColor. </summary>
		RoomControlHeaderButtonTextColor,
		///<summary>RoomControlStationNumberColor. </summary>
		RoomControlStationNumberColor,
		///<summary>RoomControlSliderMinColor. </summary>
		RoomControlSliderMinColor,
		///<summary>RoomControlSliderMaxColor. </summary>
		RoomControlSliderMaxColor,
		///<summary>RoomControlToggleOnTextColor. </summary>
		RoomControlToggleOnTextColor,
		///<summary>RoomControlToggleOffTextColor. </summary>
		RoomControlToggleOffTextColor,
		///<summary>RoomControlToggleBackgroundColor. </summary>
		RoomControlToggleBackgroundColor,
		///<summary>RoomControlToggleBorderColor. </summary>
		RoomControlToggleBorderColor,
		///<summary>RoomControlThermostatComponentColor. </summary>
		RoomControlThermostatComponentColor,
		///<summary>FooterButtonBackgroundTopColor. </summary>
		FooterButtonBackgroundTopColor,
		///<summary>FooterButtonBackgroundBottomColor. </summary>
		FooterButtonBackgroundBottomColor,
		///<summary>FooterButtonBorderColor. </summary>
		FooterButtonBorderColor,
		///<summary>FooterButtonTextColor. </summary>
		FooterButtonTextColor,
		///<summary>FooterButtonDisabledBackgroundTopColor. </summary>
		FooterButtonDisabledBackgroundTopColor,
		///<summary>FooterButtonDisabledBackgroundBottomColor. </summary>
		FooterButtonDisabledBackgroundBottomColor,
		///<summary>FooterButtonDisabledBorderColor. </summary>
		FooterButtonDisabledBorderColor,
		///<summary>FooterButtonDisabledTextColor. </summary>
		FooterButtonDisabledTextColor,
		///<summary>ListCategoryL2BackgroundColor. </summary>
		ListCategoryL2BackgroundColor,
		///<summary>ListCategoryL3BackgroundColor. </summary>
		ListCategoryL3BackgroundColor,
		///<summary>ListCategoryL4BackgroundColor. </summary>
		ListCategoryL4BackgroundColor,
		///<summary>ListCategoryL5BackgroundColor. </summary>
		ListCategoryL5BackgroundColor,
		///<summary>ListItemPriceTextColor. </summary>
		ListItemPriceTextColor,
		///<summary>ListItemSelectedPriceTextColor. </summary>
		ListItemSelectedPriceTextColor,
		///<summary>AlterationDialogBackgroundColor. </summary>
		AlterationDialogBackgroundColor,
		///<summary>AlterationDialogBorderColor. </summary>
		AlterationDialogBorderColor,
		///<summary>AlterationDialogPanelBackgroundColor. </summary>
		AlterationDialogPanelBackgroundColor,
		///<summary>AlterationDialogListItemBackgroundColor. </summary>
		AlterationDialogListItemBackgroundColor,
		///<summary>AlterationDialogListItemSelectedBackgroundColor. </summary>
		AlterationDialogListItemSelectedBackgroundColor,
		///<summary>AlterationDialogListItemSelectedBorderColor. </summary>
		AlterationDialogListItemSelectedBorderColor,
		///<summary>AlterationDialogPrimaryTextColor. </summary>
		AlterationDialogPrimaryTextColor,
		///<summary>AlterationDialogSecondaryTextColor. </summary>
		AlterationDialogSecondaryTextColor,
		///<summary>AlterationDialogInputBackgroundColor. </summary>
		AlterationDialogInputBackgroundColor,
		///<summary>AlterationDialogInputBorderColor. </summary>
		AlterationDialogInputBorderColor,
		///<summary>AlterationDialogInputTextColor. </summary>
		AlterationDialogInputTextColor,
		///<summary>AlterationDialogInputHintTextColor. </summary>
		AlterationDialogInputHintTextColor,
		///<summary>AlterationDialogPanelBorderColor. </summary>
		AlterationDialogPanelBorderColor,
		///<summary>AlterationDialogRadioButtonColor. </summary>
		AlterationDialogRadioButtonColor,
		///<summary>AlterationDialogCheckBoxColor. </summary>
		AlterationDialogCheckBoxColor,
		///<summary>AlterationDialogCloseButtonColor. </summary>
		AlterationDialogCloseButtonColor,
		///<summary>AlterationDialogListViewArrowColor. </summary>
		AlterationDialogListViewArrowColor,
		///<summary>AlterationDialogListViewDividerColor. </summary>
		AlterationDialogListViewDividerColor,
		///<summary>AreaTabActiveText. </summary>
		AreaTabActiveText,
		///<summary>AreaTabInactiveText. </summary>
		AreaTabInactiveText,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: UIThemeColor.</summary>
	public enum UIThemeColorFieldIndex
	{
		///<summary>UIThemeColorId. </summary>
		UIThemeColorId,
		///<summary>ParentCompanyId. </summary>
		ParentCompanyId,
		///<summary>UIThemeId. </summary>
		UIThemeId,
		///<summary>Type. </summary>
		Type,
		///<summary>Color. </summary>
		Color,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: UIThemeTextSize.</summary>
	public enum UIThemeTextSizeFieldIndex
	{
		///<summary>UIThemeTextSizeId. </summary>
		UIThemeTextSizeId,
		///<summary>ParentCompanyId. </summary>
		ParentCompanyId,
		///<summary>UIThemeId. </summary>
		UIThemeId,
		///<summary>Type. </summary>
		Type,
		///<summary>TextSize. </summary>
		TextSize,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: UIWidget.</summary>
	public enum UIWidgetFieldIndex
	{
		///<summary>UIWidgetId. </summary>
		UIWidgetId,
		///<summary>ParentCompanyId. </summary>
		ParentCompanyId,
		///<summary>UITabId. </summary>
		UITabId,
		///<summary>Type. </summary>
		Type,
		///<summary>Caption. </summary>
		Caption,
		///<summary>SortOrder. </summary>
		SortOrder,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>AdvertisementId. </summary>
		AdvertisementId,
		///<summary>ProductCategoryId. </summary>
		ProductCategoryId,
		///<summary>UITabType. </summary>
		UITabType,
		///<summary>Url. </summary>
		Url,
		///<summary>CategoryId. </summary>
		CategoryId,
		///<summary>EntertainmentId. </summary>
		EntertainmentId,
		///<summary>SiteId. </summary>
		SiteId,
		///<summary>EntertainmentcategoryId. </summary>
		EntertainmentcategoryId,
		///<summary>PageId. </summary>
		PageId,
		///<summary>FieldValue1. </summary>
		FieldValue1,
		///<summary>FieldValue2. </summary>
		FieldValue2,
		///<summary>FieldValue3. </summary>
		FieldValue3,
		///<summary>FieldValue4. </summary>
		FieldValue4,
		///<summary>FieldValue5. </summary>
		FieldValue5,
		///<summary>IsVisible. </summary>
		IsVisible,
		///<summary>RoomControlAreaId. </summary>
		RoomControlAreaId,
		///<summary>RoomControlSectionId. </summary>
		RoomControlSectionId,
		///<summary>RoomControlType. </summary>
		RoomControlType,
		///<summary>Name. </summary>
		Name,
		///<summary>MessageLayoutType. </summary>
		MessageLayoutType,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: UIWidgetAvailability.</summary>
	public enum UIWidgetAvailabilityFieldIndex
	{
		///<summary>UIWidgetAvailabilityId. </summary>
		UIWidgetAvailabilityId,
		///<summary>ParentCompanyId. </summary>
		ParentCompanyId,
		///<summary>UIWidgetId. </summary>
		UIWidgetId,
		///<summary>AvailabilityId. </summary>
		AvailabilityId,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>SortOrder. </summary>
		SortOrder,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: UIWidgetLanguage.</summary>
	public enum UIWidgetLanguageFieldIndex
	{
		///<summary>UIWidgetLanguageId. </summary>
		UIWidgetLanguageId,
		///<summary>ParentCompanyId. </summary>
		ParentCompanyId,
		///<summary>UIWidgetId. </summary>
		UIWidgetId,
		///<summary>Caption. </summary>
		Caption,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>LanguageId. </summary>
		LanguageId,
		///<summary>FieldValue1. </summary>
		FieldValue1,
		///<summary>FieldValue2. </summary>
		FieldValue2,
		///<summary>FieldValue3. </summary>
		FieldValue3,
		///<summary>FieldValue4. </summary>
		FieldValue4,
		///<summary>FieldValue5. </summary>
		FieldValue5,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: UIWidgetTimer.</summary>
	public enum UIWidgetTimerFieldIndex
	{
		///<summary>UIWidgetTimerId. </summary>
		UIWidgetTimerId,
		///<summary>ParentCompanyId. </summary>
		ParentCompanyId,
		///<summary>UIWidgetId. </summary>
		UIWidgetId,
		///<summary>CountToDate. </summary>
		CountToDate,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>CountToTime. </summary>
		CountToTime,
		///<summary>CountToDateUTC. </summary>
		CountToDateUTC,
		///<summary>CountToTimeUTC. </summary>
		CountToTimeUTC,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: User.</summary>
	public enum UserFieldIndex
	{
		///<summary>UserId. </summary>
		UserId,
		///<summary>Username. </summary>
		Username,
		///<summary>Firstname. </summary>
		Firstname,
		///<summary>Lastname. </summary>
		Lastname,
		///<summary>LastnamePrefix. </summary>
		LastnamePrefix,
		///<summary>Email. </summary>
		Email,
		///<summary>Password. </summary>
		Password,
		///<summary>IsApproved. </summary>
		IsApproved,
		///<summary>IsOnline. </summary>
		IsOnline,
		///<summary>IsLockedOut. </summary>
		IsLockedOut,
		///<summary>FailedPasswordAttemptCount. </summary>
		FailedPasswordAttemptCount,
		///<summary>PasswordResetLinkIdentifier. </summary>
		PasswordResetLinkIdentifier,
		///<summary>LogingLevel. </summary>
		LogingLevel,
		///<summary>RandomValue. </summary>
		RandomValue,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>Deleted. </summary>
		Deleted,
		///<summary>Archived. </summary>
		Archived,
		///<summary>PageSize. </summary>
		PageSize,
		///<summary>AccountId. </summary>
		AccountId,
		///<summary>Role. </summary>
		Role,
		///<summary>TimeZoneId. </summary>
		TimeZoneId,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>LastActivityDateUTC. </summary>
		LastActivityDateUTC,
		///<summary>LastLoginDateUTC. </summary>
		LastLoginDateUTC,
		///<summary>LastPasswordChangedDateUTC. </summary>
		LastPasswordChangedDateUTC,
		///<summary>LastLockedOutDateUTC. </summary>
		LastLockedOutDateUTC,
		///<summary>FailedPasswordAttemptWindowStartUTC. </summary>
		FailedPasswordAttemptWindowStartUTC,
		///<summary>FailedPasswordAttemptLastDateUTC. </summary>
		FailedPasswordAttemptLastDateUTC,
		///<summary>PasswordResetLinkGeneratedUTC. </summary>
		PasswordResetLinkGeneratedUTC,
		///<summary>TimeZoneOlsonId. </summary>
		TimeZoneOlsonId,
		///<summary>CultureCode. </summary>
		CultureCode,
		///<summary>AllowPublishing. </summary>
		AllowPublishing,
		///<summary>IsTester. </summary>
		IsTester,
		///<summary>IsDeveloper. </summary>
		IsDeveloper,
		///<summary>HasCboAccount. </summary>
		HasCboAccount,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: UserBrand.</summary>
	public enum UserBrandFieldIndex
	{
		///<summary>UserBrandId. </summary>
		UserBrandId,
		///<summary>UserId. </summary>
		UserId,
		///<summary>BrandId. </summary>
		BrandId,
		///<summary>Role. </summary>
		Role,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: UserLogon.</summary>
	public enum UserLogonFieldIndex
	{
		///<summary>UserLogonId. </summary>
		UserLogonId,
		///<summary>UserId. </summary>
		UserId,
		///<summary>Successful. </summary>
		Successful,
		///<summary>IpAddress. </summary>
		IpAddress,
		///<summary>AuthenticateResult. </summary>
		AuthenticateResult,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>DateTimeUTC. </summary>
		DateTimeUTC,
		///<summary>Username. </summary>
		Username,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: UserRole.</summary>
	public enum UserRoleFieldIndex
	{
		///<summary>UserRoleId. </summary>
		UserRoleId,
		///<summary>UserId. </summary>
		UserId,
		///<summary>RoleId. </summary>
		RoleId,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Vattariff.</summary>
	public enum VattariffFieldIndex
	{
		///<summary>VattariffId. </summary>
		VattariffId,
		///<summary>CountryId. </summary>
		CountryId,
		///<summary>Name. </summary>
		Name,
		///<summary>Percentage. </summary>
		Percentage,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Vendor.</summary>
	public enum VendorFieldIndex
	{
		///<summary>VendorId. </summary>
		VendorId,
		///<summary>Name. </summary>
		Name,
		///<summary>UserAgentName. </summary>
		UserAgentName,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: VenueCategory.</summary>
	public enum VenueCategoryFieldIndex
	{
		///<summary>VenueCategoryId. </summary>
		VenueCategoryId,
		///<summary>Name. </summary>
		Name,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>MarkerIcon. </summary>
		MarkerIcon,
		///<summary>ParentVenueCategoryId. </summary>
		ParentVenueCategoryId,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>LastModifiedUTC. </summary>
		LastModifiedUTC,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: VenueCategoryLanguage.</summary>
	public enum VenueCategoryLanguageFieldIndex
	{
		///<summary>VenueCategoryLanguageId. </summary>
		VenueCategoryLanguageId,
		///<summary>VenueCategoryId. </summary>
		VenueCategoryId,
		///<summary>LanguageId. </summary>
		LanguageId,
		///<summary>Name. </summary>
		Name,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>NamePlural. </summary>
		NamePlural,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: View.</summary>
	public enum ViewFieldIndex
	{
		///<summary>ViewId. </summary>
		ViewId,
		///<summary>Name. </summary>
		Name,
		///<summary>EntityName. </summary>
		EntityName,
		///<summary>UiElementTypeNameFull. </summary>
		UiElementTypeNameFull,
		///<summary>ForLocalUseWhileSyncingIsUpdated. </summary>
		ForLocalUseWhileSyncingIsUpdated,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: ViewCustom.</summary>
	public enum ViewCustomFieldIndex
	{
		///<summary>ViewCustomId. </summary>
		ViewCustomId,
		///<summary>Name. </summary>
		Name,
		///<summary>EntityName. </summary>
		EntityName,
		///<summary>UiElementTypeNameFull. </summary>
		UiElementTypeNameFull,
		///<summary>UserId. </summary>
		UserId,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: ViewItem.</summary>
	public enum ViewItemFieldIndex
	{
		///<summary>ViewItemId. </summary>
		ViewItemId,
		///<summary>ViewId. </summary>
		ViewId,
		///<summary>EntityName. </summary>
		EntityName,
		///<summary>FieldName. </summary>
		FieldName,
		///<summary>ShowOnGridView. </summary>
		ShowOnGridView,
		///<summary>DisplayPosition. </summary>
		DisplayPosition,
		///<summary>SortPosition. </summary>
		SortPosition,
		///<summary>SortOperator. </summary>
		SortOperator,
		///<summary>DisplayFormatString. </summary>
		DisplayFormatString,
		///<summary>Width. </summary>
		Width,
		///<summary>Type. </summary>
		Type,
		///<summary>UiElementTypeNameFull. </summary>
		UiElementTypeNameFull,
		///<summary>ForLocalUseWhileSyncingIsUpdated. </summary>
		ForLocalUseWhileSyncingIsUpdated,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>Updated. </summary>
		Updated,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: ViewItemCustom.</summary>
	public enum ViewItemCustomFieldIndex
	{
		///<summary>ViewItemCustomId. </summary>
		ViewItemCustomId,
		///<summary>ViewCustomId. </summary>
		ViewCustomId,
		///<summary>EntityName. </summary>
		EntityName,
		///<summary>FieldName. </summary>
		FieldName,
		///<summary>FriendlyName. </summary>
		FriendlyName,
		///<summary>ShowOnGridView. </summary>
		ShowOnGridView,
		///<summary>DisplayPosition. </summary>
		DisplayPosition,
		///<summary>SortPosition. </summary>
		SortPosition,
		///<summary>SortOperator. </summary>
		SortOperator,
		///<summary>DisplayFormatString. </summary>
		DisplayFormatString,
		///<summary>Width. </summary>
		Width,
		///<summary>Type. </summary>
		Type,
		///<summary>UiElementTypeNameFull. </summary>
		UiElementTypeNameFull,
		///<summary>Archived. </summary>
		Archived,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Weather.</summary>
	public enum WeatherFieldIndex
	{
		///<summary>WeatherId. </summary>
		WeatherId,
		///<summary>City. </summary>
		City,
		///<summary>Filename. </summary>
		Filename,
		///<summary>LastWeatherData. </summary>
		LastWeatherData,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: WifiConfiguration.</summary>
	public enum WifiConfigurationFieldIndex
	{
		///<summary>WifiConfigurationId. </summary>
		WifiConfigurationId,
		///<summary>Ssid. </summary>
		Ssid,
		///<summary>HiddenSsid. </summary>
		HiddenSsid,
		///<summary>Security. </summary>
		Security,
		///<summary>SecurityKey. </summary>
		SecurityKey,
		///<summary>EapMethod. </summary>
		EapMethod,
		///<summary>EapPhase2. </summary>
		EapPhase2,
		///<summary>EapIdent. </summary>
		EapIdent,
		///<summary>EapAnonIdent. </summary>
		EapAnonIdent,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>CompanyId. </summary>
		CompanyId,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Action.</summary>
	public enum ActionFieldIndex
	{
		///<summary>ActionId. </summary>
		ActionId,
		///<summary>ActionType. </summary>
		ActionType,
		///<summary>LandingPageId. </summary>
		LandingPageId,
		///<summary>CategoryId. </summary>
		CategoryId,
		///<summary>ProductCategoryId. </summary>
		ProductCategoryId,
		///<summary>Resource. </summary>
		Resource,
		///<summary>ParentCompanyId. </summary>
		ParentCompanyId,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>ApplicationConfigurationId. </summary>
		ApplicationConfigurationId,
		///<summary>ProductId. </summary>
		ProductId,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: ApplicationConfiguration.</summary>
	public enum ApplicationConfigurationFieldIndex
	{
		///<summary>ApplicationConfigurationId. </summary>
		ApplicationConfigurationId,
		///<summary>ExternalId. </summary>
		ExternalId,
		///<summary>CompanyId. </summary>
		CompanyId,
		///<summary>Name. </summary>
		Name,
		///<summary>ShortName. </summary>
		ShortName,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>Updated. </summary>
		Updated,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>LandingPageId. </summary>
		LandingPageId,
		///<summary>Description. </summary>
		Description,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>CultureCode. </summary>
		CultureCode,
		///<summary>EnableCookiewall. </summary>
		EnableCookiewall,
		///<summary>AnalyticsEnvironment. </summary>
		AnalyticsEnvironment,
		///<summary>GoogleAnalyticsId. </summary>
		GoogleAnalyticsId,
		///<summary>GoogleTagManagerId. </summary>
		GoogleTagManagerId,
		///<summary>ProductSwipingEnabled. </summary>
		ProductSwipingEnabled,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: CarouselItem.</summary>
	public enum CarouselItemFieldIndex
	{
		///<summary>CarouselItemId. </summary>
		CarouselItemId,
		///<summary>WidgetCarouselId. </summary>
		WidgetCarouselId,
		///<summary>WidgetActionButtonId. </summary>
		WidgetActionButtonId,
		///<summary>Message. </summary>
		Message,
		///<summary>SortOrder. </summary>
		SortOrder,
		///<summary>ParentCompanyId. </summary>
		ParentCompanyId,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>Name. </summary>
		Name,
		///<summary>OverlayType. </summary>
		OverlayType,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: FeatureFlag.</summary>
	public enum FeatureFlagFieldIndex
	{
		///<summary>FeatureFlagId. </summary>
		FeatureFlagId,
		///<summary>ApplicationConfigurationId. </summary>
		ApplicationConfigurationId,
		///<summary>ApplessFeatureFlags. </summary>
		ApplessFeatureFlags,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>CompanyId. </summary>
		CompanyId,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: LandingPage.</summary>
	public enum LandingPageFieldIndex
	{
		///<summary>LandingPageId. </summary>
		LandingPageId,
		///<summary>ExternalId. </summary>
		ExternalId,
		///<summary>ParentCompanyId. </summary>
		ParentCompanyId,
		///<summary>ApplicationConfigurationId. </summary>
		ApplicationConfigurationId,
		///<summary>ThemeId. </summary>
		ThemeId,
		///<summary>Name. </summary>
		Name,
		///<summary>WelcomeText. </summary>
		WelcomeText,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>NavigationMenuId. </summary>
		NavigationMenuId,
		///<summary>OutletId. </summary>
		OutletId,
		///<summary>HomeNavigationEnabled. </summary>
		HomeNavigationEnabled,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: LandingPageWidget.</summary>
	public enum LandingPageWidgetFieldIndex
	{
		///<summary>LandingPageId. </summary>
		LandingPageId,
		///<summary>WidgetId. </summary>
		WidgetId,
		///<summary>SortOrder. </summary>
		SortOrder,
		///<summary>ParentCompanyId. </summary>
		ParentCompanyId,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: NavigationMenu.</summary>
	public enum NavigationMenuFieldIndex
	{
		///<summary>NavigationMenuId. </summary>
		NavigationMenuId,
		///<summary>CompanyId. </summary>
		CompanyId,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>Name. </summary>
		Name,
		///<summary>ApplicationConfigurationId. </summary>
		ApplicationConfigurationId,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: NavigationMenuItem.</summary>
	public enum NavigationMenuItemFieldIndex
	{
		///<summary>NavigationMenuItemId. </summary>
		NavigationMenuItemId,
		///<summary>NavigationMenuId. </summary>
		NavigationMenuId,
		///<summary>ActionId. </summary>
		ActionId,
		///<summary>Text. </summary>
		Text,
		///<summary>SortOrder. </summary>
		SortOrder,
		///<summary>ParentCompanyId. </summary>
		ParentCompanyId,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: NavigationMenuWidget.</summary>
	public enum NavigationMenuWidgetFieldIndex
	{
		///<summary>NavigationMenuId. </summary>
		NavigationMenuId,
		///<summary>WidgetId. </summary>
		WidgetId,
		///<summary>SortOrder. </summary>
		SortOrder,
		///<summary>ParentCompanyId. </summary>
		ParentCompanyId,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Theme.</summary>
	public enum ThemeFieldIndex
	{
		///<summary>ThemeId. </summary>
		ThemeId,
		///<summary>ParentCompanyId. </summary>
		ParentCompanyId,
		///<summary>PrimaryColor. </summary>
		PrimaryColor,
		///<summary>SignalColor. </summary>
		SignalColor,
		///<summary>FontFamily. </summary>
		FontFamily,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>TextColor. </summary>
		TextColor,
		///<summary>DividerColor. </summary>
		DividerColor,
		///<summary>SubnavColor. </summary>
		SubnavColor,
		///<summary>BackgroundColor. </summary>
		BackgroundColor,
		///<summary>Name. </summary>
		Name,
		///<summary>ApplicationConfigurationId. </summary>
		ApplicationConfigurationId,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Widget.</summary>
	public enum WidgetFieldIndex
	{
		///<summary>WidgetId. </summary>
		WidgetId,
		///<summary>ApplicationConfigurationId. </summary>
		ApplicationConfigurationId,
		///<summary>Name. </summary>
		Name,
		///<summary>ActionId. </summary>
		ActionId,
		///<summary>CreatedUTC. </summary>
		CreatedUTC,
		///<summary>CreatedBy. </summary>
		CreatedBy,
		///<summary>UpdatedUTC. </summary>
		UpdatedUTC,
		///<summary>UpdatedBy. </summary>
		UpdatedBy,
		///<summary>ParentCompanyId. </summary>
		ParentCompanyId,
		///<summary>Visible. </summary>
		Visible,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: WidgetActionBanner.</summary>
	public enum WidgetActionBannerFieldIndex
	{
		///<summary>WidgetId. Inherited from Widget</summary>
		WidgetId_Widget,
		///<summary>ApplicationConfigurationId. Inherited from Widget</summary>
		ApplicationConfigurationId,
		///<summary>Name. Inherited from Widget</summary>
		Name,
		///<summary>ActionId. Inherited from Widget</summary>
		ActionId,
		///<summary>CreatedUTC. Inherited from Widget</summary>
		CreatedUTC,
		///<summary>CreatedBy. Inherited from Widget</summary>
		CreatedBy,
		///<summary>UpdatedUTC. Inherited from Widget</summary>
		UpdatedUTC,
		///<summary>UpdatedBy. Inherited from Widget</summary>
		UpdatedBy,
		///<summary>ParentCompanyId. Inherited from Widget</summary>
		ParentCompanyId,
		///<summary>Visible. Inherited from Widget</summary>
		Visible,
		///<summary>WidgetId. </summary>
		WidgetId,
		///<summary>Text. </summary>
		Text,
		///<summary>OverlayType. </summary>
		OverlayType,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: WidgetActionButton.</summary>
	public enum WidgetActionButtonFieldIndex
	{
		///<summary>WidgetId. Inherited from Widget</summary>
		WidgetId_Widget,
		///<summary>ApplicationConfigurationId. Inherited from Widget</summary>
		ApplicationConfigurationId,
		///<summary>Name. Inherited from Widget</summary>
		Name,
		///<summary>ActionId. Inherited from Widget</summary>
		ActionId,
		///<summary>CreatedUTC. Inherited from Widget</summary>
		CreatedUTC,
		///<summary>CreatedBy. Inherited from Widget</summary>
		CreatedBy,
		///<summary>UpdatedUTC. Inherited from Widget</summary>
		UpdatedUTC,
		///<summary>UpdatedBy. Inherited from Widget</summary>
		UpdatedBy,
		///<summary>ParentCompanyId. Inherited from Widget</summary>
		ParentCompanyId,
		///<summary>Visible. Inherited from Widget</summary>
		Visible,
		///<summary>WidgetId. </summary>
		WidgetId,
		///<summary>IconType. </summary>
		IconType,
		///<summary>Text. </summary>
		Text,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: WidgetCarousel.</summary>
	public enum WidgetCarouselFieldIndex
	{
		///<summary>WidgetId. Inherited from Widget</summary>
		WidgetId_Widget,
		///<summary>ApplicationConfigurationId. Inherited from Widget</summary>
		ApplicationConfigurationId,
		///<summary>Name. Inherited from Widget</summary>
		Name,
		///<summary>ActionId. Inherited from Widget</summary>
		ActionId,
		///<summary>CreatedUTC. Inherited from Widget</summary>
		CreatedUTC,
		///<summary>CreatedBy. Inherited from Widget</summary>
		CreatedBy,
		///<summary>UpdatedUTC. Inherited from Widget</summary>
		UpdatedUTC,
		///<summary>UpdatedBy. Inherited from Widget</summary>
		UpdatedBy,
		///<summary>ParentCompanyId. Inherited from Widget</summary>
		ParentCompanyId,
		///<summary>Visible. Inherited from Widget</summary>
		Visible,
		///<summary>WidgetId. </summary>
		WidgetId,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: WidgetGroup.</summary>
	public enum WidgetGroupFieldIndex
	{
		///<summary>WidgetId. Inherited from Widget</summary>
		WidgetId_Widget,
		///<summary>ApplicationConfigurationId. Inherited from Widget</summary>
		ApplicationConfigurationId,
		///<summary>Name. Inherited from Widget</summary>
		Name,
		///<summary>ActionId. Inherited from Widget</summary>
		ActionId,
		///<summary>CreatedUTC. Inherited from Widget</summary>
		CreatedUTC,
		///<summary>CreatedBy. Inherited from Widget</summary>
		CreatedBy,
		///<summary>UpdatedUTC. Inherited from Widget</summary>
		UpdatedUTC,
		///<summary>UpdatedBy. Inherited from Widget</summary>
		UpdatedBy,
		///<summary>ParentCompanyId. Inherited from Widget</summary>
		ParentCompanyId,
		///<summary>Visible. Inherited from Widget</summary>
		Visible,
		///<summary>WidgetId. </summary>
		WidgetId,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: WidgetGroupWidget.</summary>
	public enum WidgetGroupWidgetFieldIndex
	{
		///<summary>WidgetGroupId. </summary>
		WidgetGroupId,
		///<summary>WidgetId. </summary>
		WidgetId,
		///<summary>SortOrder. </summary>
		SortOrder,
		///<summary>ParentCompanyId. </summary>
		ParentCompanyId,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: WidgetHero.</summary>
	public enum WidgetHeroFieldIndex
	{
		///<summary>WidgetId. Inherited from Widget</summary>
		WidgetId_Widget,
		///<summary>ApplicationConfigurationId. Inherited from Widget</summary>
		ApplicationConfigurationId,
		///<summary>Name. Inherited from Widget</summary>
		Name,
		///<summary>ActionId. Inherited from Widget</summary>
		ActionId,
		///<summary>CreatedUTC. Inherited from Widget</summary>
		CreatedUTC,
		///<summary>CreatedBy. Inherited from Widget</summary>
		CreatedBy,
		///<summary>UpdatedUTC. Inherited from Widget</summary>
		UpdatedUTC,
		///<summary>UpdatedBy. Inherited from Widget</summary>
		UpdatedBy,
		///<summary>ParentCompanyId. Inherited from Widget</summary>
		ParentCompanyId,
		///<summary>Visible. Inherited from Widget</summary>
		Visible,
		///<summary>WidgetId. </summary>
		WidgetId,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: WidgetLanguageSwitcher.</summary>
	public enum WidgetLanguageSwitcherFieldIndex
	{
		///<summary>WidgetId. Inherited from Widget</summary>
		WidgetId_Widget,
		///<summary>ApplicationConfigurationId. Inherited from Widget</summary>
		ApplicationConfigurationId,
		///<summary>Name. Inherited from Widget</summary>
		Name,
		///<summary>ActionId. Inherited from Widget</summary>
		ActionId,
		///<summary>CreatedUTC. Inherited from Widget</summary>
		CreatedUTC,
		///<summary>CreatedBy. Inherited from Widget</summary>
		CreatedBy,
		///<summary>UpdatedUTC. Inherited from Widget</summary>
		UpdatedUTC,
		///<summary>UpdatedBy. Inherited from Widget</summary>
		UpdatedBy,
		///<summary>ParentCompanyId. Inherited from Widget</summary>
		ParentCompanyId,
		///<summary>Visible. Inherited from Widget</summary>
		Visible,
		///<summary>WidgetId. </summary>
		WidgetId,
		///<summary>Text. </summary>
		Text,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: WidgetMarkdown.</summary>
	public enum WidgetMarkdownFieldIndex
	{
		///<summary>WidgetId. Inherited from Widget</summary>
		WidgetId_Widget,
		///<summary>ApplicationConfigurationId. Inherited from Widget</summary>
		ApplicationConfigurationId,
		///<summary>Name. Inherited from Widget</summary>
		Name,
		///<summary>ActionId. Inherited from Widget</summary>
		ActionId,
		///<summary>CreatedUTC. Inherited from Widget</summary>
		CreatedUTC,
		///<summary>CreatedBy. Inherited from Widget</summary>
		CreatedBy,
		///<summary>UpdatedUTC. Inherited from Widget</summary>
		UpdatedUTC,
		///<summary>UpdatedBy. Inherited from Widget</summary>
		UpdatedBy,
		///<summary>ParentCompanyId. Inherited from Widget</summary>
		ParentCompanyId,
		///<summary>Visible. Inherited from Widget</summary>
		Visible,
		///<summary>WidgetId. </summary>
		WidgetId,
		///<summary>Title. </summary>
		Title,
		///<summary>Text. </summary>
		Text,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: WidgetOpeningTime.</summary>
	public enum WidgetOpeningTimeFieldIndex
	{
		///<summary>WidgetId. Inherited from Widget</summary>
		WidgetId_Widget,
		///<summary>ApplicationConfigurationId. Inherited from Widget</summary>
		ApplicationConfigurationId,
		///<summary>Name. Inherited from Widget</summary>
		Name,
		///<summary>ActionId. Inherited from Widget</summary>
		ActionId,
		///<summary>CreatedUTC. Inherited from Widget</summary>
		CreatedUTC,
		///<summary>CreatedBy. Inherited from Widget</summary>
		CreatedBy,
		///<summary>UpdatedUTC. Inherited from Widget</summary>
		UpdatedUTC,
		///<summary>UpdatedBy. Inherited from Widget</summary>
		UpdatedBy,
		///<summary>ParentCompanyId. Inherited from Widget</summary>
		ParentCompanyId,
		///<summary>Visible. Inherited from Widget</summary>
		Visible,
		///<summary>WidgetId. </summary>
		WidgetId,
		///<summary>Title. </summary>
		Title,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: WidgetPageTitle.</summary>
	public enum WidgetPageTitleFieldIndex
	{
		///<summary>WidgetId. Inherited from Widget</summary>
		WidgetId_Widget,
		///<summary>ApplicationConfigurationId. Inherited from Widget</summary>
		ApplicationConfigurationId,
		///<summary>Name. Inherited from Widget</summary>
		Name,
		///<summary>ActionId. Inherited from Widget</summary>
		ActionId,
		///<summary>CreatedUTC. Inherited from Widget</summary>
		CreatedUTC,
		///<summary>CreatedBy. Inherited from Widget</summary>
		CreatedBy,
		///<summary>UpdatedUTC. Inherited from Widget</summary>
		UpdatedUTC,
		///<summary>UpdatedBy. Inherited from Widget</summary>
		UpdatedBy,
		///<summary>ParentCompanyId. Inherited from Widget</summary>
		ParentCompanyId,
		///<summary>Visible. Inherited from Widget</summary>
		Visible,
		///<summary>WidgetId. </summary>
		WidgetId,
		///<summary>Title. </summary>
		Title,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: WidgetWaitTime.</summary>
	public enum WidgetWaitTimeFieldIndex
	{
		///<summary>WidgetId. Inherited from Widget</summary>
		WidgetId_Widget,
		///<summary>ApplicationConfigurationId. Inherited from Widget</summary>
		ApplicationConfigurationId,
		///<summary>Name. Inherited from Widget</summary>
		Name,
		///<summary>ActionId. Inherited from Widget</summary>
		ActionId,
		///<summary>CreatedUTC. Inherited from Widget</summary>
		CreatedUTC,
		///<summary>CreatedBy. Inherited from Widget</summary>
		CreatedBy,
		///<summary>UpdatedUTC. Inherited from Widget</summary>
		UpdatedUTC,
		///<summary>UpdatedBy. Inherited from Widget</summary>
		UpdatedBy,
		///<summary>ParentCompanyId. Inherited from Widget</summary>
		ParentCompanyId,
		///<summary>Visible. Inherited from Widget</summary>
		Visible,
		///<summary>WidgetId. </summary>
		WidgetId,
		///<summary>Text. </summary>
		Text,
		/// <summary></summary>
		AmountOfFields
	}



	/// <summary>Enum definition for all the entity types defined in this namespace. Used by the entityfields factory.</summary>
	public enum EntityType
	{
		///<summary>AccessCode</summary>
		AccessCodeEntity,
		///<summary>AccessCodeCompany</summary>
		AccessCodeCompanyEntity,
		///<summary>AccessCodePointOfInterest</summary>
		AccessCodePointOfInterestEntity,
		///<summary>Account</summary>
		AccountEntity,
		///<summary>AccountCompany</summary>
		AccountCompanyEntity,
		///<summary>ActionButton</summary>
		ActionButtonEntity,
		///<summary>ActionButtonLanguage</summary>
		ActionButtonLanguageEntity,
		///<summary>Address</summary>
		AddressEntity,
		///<summary>Advertisement</summary>
		AdvertisementEntity,
		///<summary>AdvertisementConfiguration</summary>
		AdvertisementConfigurationEntity,
		///<summary>AdvertisementConfigurationAdvertisement</summary>
		AdvertisementConfigurationAdvertisementEntity,
		///<summary>AdvertisementLanguage</summary>
		AdvertisementLanguageEntity,
		///<summary>AdvertisementTag</summary>
		AdvertisementTagEntity,
		///<summary>AdvertisementTagAdvertisement</summary>
		AdvertisementTagAdvertisementEntity,
		///<summary>AdvertisementTagCategory</summary>
		AdvertisementTagCategoryEntity,
		///<summary>AdvertisementTagEntertainment</summary>
		AdvertisementTagEntertainmentEntity,
		///<summary>AdvertisementTagGenericproduct</summary>
		AdvertisementTagGenericproductEntity,
		///<summary>AdvertisementTagLanguage</summary>
		AdvertisementTagLanguageEntity,
		///<summary>AdvertisementTagProduct</summary>
		AdvertisementTagProductEntity,
		///<summary>AdyenPaymentMethod</summary>
		AdyenPaymentMethodEntity,
		///<summary>AdyenPaymentMethodBrand</summary>
		AdyenPaymentMethodBrandEntity,
		///<summary>AffiliateCampaign</summary>
		AffiliateCampaignEntity,
		///<summary>AffiliateCampaignAffiliatePartner</summary>
		AffiliateCampaignAffiliatePartnerEntity,
		///<summary>AffiliatePartner</summary>
		AffiliatePartnerEntity,
		///<summary>Alteration</summary>
		AlterationEntity,
		///<summary>Alterationitem</summary>
		AlterationitemEntity,
		///<summary>AlterationitemAlteration</summary>
		AlterationitemAlterationEntity,
		///<summary>AlterationLanguage</summary>
		AlterationLanguageEntity,
		///<summary>Alterationoption</summary>
		AlterationoptionEntity,
		///<summary>AlterationoptionLanguage</summary>
		AlterationoptionLanguageEntity,
		///<summary>AlterationoptionTag</summary>
		AlterationoptionTagEntity,
		///<summary>AlterationProduct</summary>
		AlterationProductEntity,
		///<summary>Amenity</summary>
		AmenityEntity,
		///<summary>AmenityLanguage</summary>
		AmenityLanguageEntity,
		///<summary>AnalyticsProcessingTask</summary>
		AnalyticsProcessingTaskEntity,
		///<summary>Announcement</summary>
		AnnouncementEntity,
		///<summary>AnnouncementLanguage</summary>
		AnnouncementLanguageEntity,
		///<summary>ApiAuthentication</summary>
		ApiAuthenticationEntity,
		///<summary>Application</summary>
		ApplicationEntity,
		///<summary>Attachment</summary>
		AttachmentEntity,
		///<summary>AttachmentLanguage</summary>
		AttachmentLanguageEntity,
		///<summary>Auditlog</summary>
		AuditlogEntity,
		///<summary>Availability</summary>
		AvailabilityEntity,
		///<summary>AzureNotificationHub</summary>
		AzureNotificationHubEntity,
		///<summary>Brand</summary>
		BrandEntity,
		///<summary>BrandCulture</summary>
		BrandCultureEntity,
		///<summary>Businesshours</summary>
		BusinesshoursEntity,
		///<summary>Businesshoursexception</summary>
		BusinesshoursexceptionEntity,
		///<summary>Category</summary>
		CategoryEntity,
		///<summary>CategoryAlteration</summary>
		CategoryAlterationEntity,
		///<summary>CategoryLanguage</summary>
		CategoryLanguageEntity,
		///<summary>CategorySuggestion</summary>
		CategorySuggestionEntity,
		///<summary>CategoryTag</summary>
		CategoryTagEntity,
		///<summary>CheckoutMethod</summary>
		CheckoutMethodEntity,
		///<summary>CheckoutMethodDeliverypointgroup</summary>
		CheckoutMethodDeliverypointgroupEntity,
		///<summary>Client</summary>
		ClientEntity,
		///<summary>ClientConfiguration</summary>
		ClientConfigurationEntity,
		///<summary>ClientConfigurationRoute</summary>
		ClientConfigurationRouteEntity,
		///<summary>ClientEntertainment</summary>
		ClientEntertainmentEntity,
		///<summary>ClientLog</summary>
		ClientLogEntity,
		///<summary>ClientLogFile</summary>
		ClientLogFileEntity,
		///<summary>ClientState</summary>
		ClientStateEntity,
		///<summary>CloudApplicationVersion</summary>
		CloudApplicationVersionEntity,
		///<summary>CloudProcessingTask</summary>
		CloudProcessingTaskEntity,
		///<summary>CloudStorageAccount</summary>
		CloudStorageAccountEntity,
		///<summary>Company</summary>
		CompanyEntity,
		///<summary>CompanyAmenity</summary>
		CompanyAmenityEntity,
		///<summary>CompanyBrand</summary>
		CompanyBrandEntity,
		///<summary>CompanyCulture</summary>
		CompanyCultureEntity,
		///<summary>CompanyCurrency</summary>
		CompanyCurrencyEntity,
		///<summary>CompanyEntertainment</summary>
		CompanyEntertainmentEntity,
		///<summary>Companygroup</summary>
		CompanygroupEntity,
		///<summary>CompanyLanguage</summary>
		CompanyLanguageEntity,
		///<summary>CompanyManagementTask</summary>
		CompanyManagementTaskEntity,
		///<summary>CompanyOwner</summary>
		CompanyOwnerEntity,
		///<summary>CompanyRelease</summary>
		CompanyReleaseEntity,
		///<summary>CompanyVenueCategory</summary>
		CompanyVenueCategoryEntity,
		///<summary>Configuration</summary>
		ConfigurationEntity,
		///<summary>Country</summary>
		CountryEntity,
		///<summary>Currency</summary>
		CurrencyEntity,
		///<summary>Customer</summary>
		CustomerEntity,
		///<summary>CustomText</summary>
		CustomTextEntity,
		///<summary>DeliveryDistance</summary>
		DeliveryDistanceEntity,
		///<summary>DeliveryInformation</summary>
		DeliveryInformationEntity,
		///<summary>Deliverypoint</summary>
		DeliverypointEntity,
		///<summary>DeliverypointExternalDeliverypoint</summary>
		DeliverypointExternalDeliverypointEntity,
		///<summary>Deliverypointgroup</summary>
		DeliverypointgroupEntity,
		///<summary>DeliverypointgroupAdvertisement</summary>
		DeliverypointgroupAdvertisementEntity,
		///<summary>DeliverypointgroupAnnouncement</summary>
		DeliverypointgroupAnnouncementEntity,
		///<summary>DeliverypointgroupEntertainment</summary>
		DeliverypointgroupEntertainmentEntity,
		///<summary>DeliverypointgroupLanguage</summary>
		DeliverypointgroupLanguageEntity,
		///<summary>DeliverypointgroupOccupancy</summary>
		DeliverypointgroupOccupancyEntity,
		///<summary>DeliverypointgroupProduct</summary>
		DeliverypointgroupProductEntity,
		///<summary>Device</summary>
		DeviceEntity,
		///<summary>Devicemedia</summary>
		DevicemediaEntity,
		///<summary>DeviceTokenHistory</summary>
		DeviceTokenHistoryEntity,
		///<summary>DeviceTokenTask</summary>
		DeviceTokenTaskEntity,
		///<summary>Entertainment</summary>
		EntertainmentEntity,
		///<summary>Entertainmentcategory</summary>
		EntertainmentcategoryEntity,
		///<summary>EntertainmentcategoryLanguage</summary>
		EntertainmentcategoryLanguageEntity,
		///<summary>EntertainmentConfiguration</summary>
		EntertainmentConfigurationEntity,
		///<summary>EntertainmentConfigurationEntertainment</summary>
		EntertainmentConfigurationEntertainmentEntity,
		///<summary>EntertainmentDependency</summary>
		EntertainmentDependencyEntity,
		///<summary>EntertainmentFile</summary>
		EntertainmentFileEntity,
		///<summary>Entertainmenturl</summary>
		EntertainmenturlEntity,
		///<summary>EntityFieldInformation</summary>
		EntityFieldInformationEntity,
		///<summary>EntityFieldInformationCustom</summary>
		EntityFieldInformationCustomEntity,
		///<summary>EntityInformation</summary>
		EntityInformationEntity,
		///<summary>EntityInformationCustom</summary>
		EntityInformationCustomEntity,
		///<summary>ExternalDeliverypoint</summary>
		ExternalDeliverypointEntity,
		///<summary>ExternalMenu</summary>
		ExternalMenuEntity,
		///<summary>ExternalProduct</summary>
		ExternalProductEntity,
		///<summary>ExternalSubProduct</summary>
		ExternalSubProductEntity,
		///<summary>ExternalSystem</summary>
		ExternalSystemEntity,
		///<summary>ExternalSystemLog</summary>
		ExternalSystemLogEntity,
		///<summary>FeatureToggleAvailability</summary>
		FeatureToggleAvailabilityEntity,
		///<summary>Folio</summary>
		FolioEntity,
		///<summary>FolioItem</summary>
		FolioItemEntity,
		///<summary>Game</summary>
		GameEntity,
		///<summary>GameSession</summary>
		GameSessionEntity,
		///<summary>GameSessionReport</summary>
		GameSessionReportEntity,
		///<summary>GameSessionReportConfiguration</summary>
		GameSessionReportConfigurationEntity,
		///<summary>GameSessionReportItem</summary>
		GameSessionReportItemEntity,
		///<summary>Genericalteration</summary>
		GenericalterationEntity,
		///<summary>Genericalterationitem</summary>
		GenericalterationitemEntity,
		///<summary>Genericalterationoption</summary>
		GenericalterationoptionEntity,
		///<summary>Genericcategory</summary>
		GenericcategoryEntity,
		///<summary>GenericcategoryLanguage</summary>
		GenericcategoryLanguageEntity,
		///<summary>Genericproduct</summary>
		GenericproductEntity,
		///<summary>GenericproductGenericalteration</summary>
		GenericproductGenericalterationEntity,
		///<summary>GenericproductLanguage</summary>
		GenericproductLanguageEntity,
		///<summary>GuestInformation</summary>
		GuestInformationEntity,
		///<summary>Icrtouchprintermapping</summary>
		IcrtouchprintermappingEntity,
		///<summary>IcrtouchprintermappingDeliverypoint</summary>
		IcrtouchprintermappingDeliverypointEntity,
		///<summary>IncomingSms</summary>
		IncomingSmsEntity,
		///<summary>InfraredCommand</summary>
		InfraredCommandEntity,
		///<summary>InfraredConfiguration</summary>
		InfraredConfigurationEntity,
		///<summary>Language</summary>
		LanguageEntity,
		///<summary>LicensedModule</summary>
		LicensedModuleEntity,
		///<summary>LicensedUIElement</summary>
		LicensedUIElementEntity,
		///<summary>LicensedUIElementSubPanel</summary>
		LicensedUIElementSubPanelEntity,
		///<summary>Map</summary>
		MapEntity,
		///<summary>MapPointOfInterest</summary>
		MapPointOfInterestEntity,
		///<summary>Media</summary>
		MediaEntity,
		///<summary>MediaCulture</summary>
		MediaCultureEntity,
		///<summary>MediaLanguage</summary>
		MediaLanguageEntity,
		///<summary>MediaProcessingTask</summary>
		MediaProcessingTaskEntity,
		///<summary>MediaRatioTypeMedia</summary>
		MediaRatioTypeMediaEntity,
		///<summary>MediaRatioTypeMediaFile</summary>
		MediaRatioTypeMediaFileEntity,
		///<summary>MediaRelationship</summary>
		MediaRelationshipEntity,
		///<summary>Menu</summary>
		MenuEntity,
		///<summary>Message</summary>
		MessageEntity,
		///<summary>Messagegroup</summary>
		MessagegroupEntity,
		///<summary>MessagegroupDeliverypoint</summary>
		MessagegroupDeliverypointEntity,
		///<summary>MessageRecipient</summary>
		MessageRecipientEntity,
		///<summary>MessageTemplate</summary>
		MessageTemplateEntity,
		///<summary>MessageTemplateCategory</summary>
		MessageTemplateCategoryEntity,
		///<summary>MessageTemplateCategoryMessageTemplate</summary>
		MessageTemplateCategoryMessageTemplateEntity,
		///<summary>Module</summary>
		ModuleEntity,
		///<summary>Netmessage</summary>
		NetmessageEntity,
		///<summary>OptIn</summary>
		OptInEntity,
		///<summary>Order</summary>
		OrderEntity,
		///<summary>OrderHour</summary>
		OrderHourEntity,
		///<summary>Orderitem</summary>
		OrderitemEntity,
		///<summary>OrderitemAlterationitem</summary>
		OrderitemAlterationitemEntity,
		///<summary>OrderitemAlterationitemTag</summary>
		OrderitemAlterationitemTagEntity,
		///<summary>OrderitemTag</summary>
		OrderitemTagEntity,
		///<summary>OrderNotificationLog</summary>
		OrderNotificationLogEntity,
		///<summary>OrderRoutestephandler</summary>
		OrderRoutestephandlerEntity,
		///<summary>OrderRoutestephandlerHistory</summary>
		OrderRoutestephandlerHistoryEntity,
		///<summary>Outlet</summary>
		OutletEntity,
		///<summary>OutletOperationalState</summary>
		OutletOperationalStateEntity,
		///<summary>OutletSellerInformation</summary>
		OutletSellerInformationEntity,
		///<summary>Page</summary>
		PageEntity,
		///<summary>PageElement</summary>
		PageElementEntity,
		///<summary>PageLanguage</summary>
		PageLanguageEntity,
		///<summary>PageTemplate</summary>
		PageTemplateEntity,
		///<summary>PageTemplateElement</summary>
		PageTemplateElementEntity,
		///<summary>PageTemplateLanguage</summary>
		PageTemplateLanguageEntity,
		///<summary>PaymentIntegrationConfiguration</summary>
		PaymentIntegrationConfigurationEntity,
		///<summary>PaymentProvider</summary>
		PaymentProviderEntity,
		///<summary>PaymentProviderCompany</summary>
		PaymentProviderCompanyEntity,
		///<summary>PaymentTransaction</summary>
		PaymentTransactionEntity,
		///<summary>PaymentTransactionLog</summary>
		PaymentTransactionLogEntity,
		///<summary>PaymentTransactionSplit</summary>
		PaymentTransactionSplitEntity,
		///<summary>PmsActionRule</summary>
		PmsActionRuleEntity,
		///<summary>PmsReportColumn</summary>
		PmsReportColumnEntity,
		///<summary>PmsReportConfiguration</summary>
		PmsReportConfigurationEntity,
		///<summary>PmsRule</summary>
		PmsRuleEntity,
		///<summary>PointOfInterest</summary>
		PointOfInterestEntity,
		///<summary>PointOfInterestAmenity</summary>
		PointOfInterestAmenityEntity,
		///<summary>PointOfInterestLanguage</summary>
		PointOfInterestLanguageEntity,
		///<summary>PointOfInterestVenueCategory</summary>
		PointOfInterestVenueCategoryEntity,
		///<summary>Posalteration</summary>
		PosalterationEntity,
		///<summary>Posalterationitem</summary>
		PosalterationitemEntity,
		///<summary>Posalterationoption</summary>
		PosalterationoptionEntity,
		///<summary>Poscategory</summary>
		PoscategoryEntity,
		///<summary>Posdeliverypoint</summary>
		PosdeliverypointEntity,
		///<summary>Posdeliverypointgroup</summary>
		PosdeliverypointgroupEntity,
		///<summary>Pospaymentmethod</summary>
		PospaymentmethodEntity,
		///<summary>Posproduct</summary>
		PosproductEntity,
		///<summary>PosproductPosalteration</summary>
		PosproductPosalterationEntity,
		///<summary>PriceLevel</summary>
		PriceLevelEntity,
		///<summary>PriceLevelItem</summary>
		PriceLevelItemEntity,
		///<summary>PriceSchedule</summary>
		PriceScheduleEntity,
		///<summary>PriceScheduleItem</summary>
		PriceScheduleItemEntity,
		///<summary>PriceScheduleItemOccurrence</summary>
		PriceScheduleItemOccurrenceEntity,
		///<summary>Product</summary>
		ProductEntity,
		///<summary>ProductAlteration</summary>
		ProductAlterationEntity,
		///<summary>ProductAttachment</summary>
		ProductAttachmentEntity,
		///<summary>ProductCategory</summary>
		ProductCategoryEntity,
		///<summary>ProductCategorySuggestion</summary>
		ProductCategorySuggestionEntity,
		///<summary>ProductCategoryTag</summary>
		ProductCategoryTagEntity,
		///<summary>Productgroup</summary>
		ProductgroupEntity,
		///<summary>ProductgroupItem</summary>
		ProductgroupItemEntity,
		///<summary>ProductLanguage</summary>
		ProductLanguageEntity,
		///<summary>ProductSuggestion</summary>
		ProductSuggestionEntity,
		///<summary>ProductTag</summary>
		ProductTagEntity,
		///<summary>Publishing</summary>
		PublishingEntity,
		///<summary>PublishingItem</summary>
		PublishingItemEntity,
		///<summary>Rating</summary>
		RatingEntity,
		///<summary>Receipt</summary>
		ReceiptEntity,
		///<summary>ReceiptTemplate</summary>
		ReceiptTemplateEntity,
		///<summary>ReferentialConstraint</summary>
		ReferentialConstraintEntity,
		///<summary>ReferentialConstraintCustom</summary>
		ReferentialConstraintCustomEntity,
		///<summary>Release</summary>
		ReleaseEntity,
		///<summary>ReportProcessingTask</summary>
		ReportProcessingTaskEntity,
		///<summary>ReportProcessingTaskFile</summary>
		ReportProcessingTaskFileEntity,
		///<summary>ReportProcessingTaskTemplate</summary>
		ReportProcessingTaskTemplateEntity,
		///<summary>Requestlog</summary>
		RequestlogEntity,
		///<summary>Role</summary>
		RoleEntity,
		///<summary>RoleModuleRights</summary>
		RoleModuleRightsEntity,
		///<summary>RoleUIElementRights</summary>
		RoleUIElementRightsEntity,
		///<summary>RoleUIElementSubPanelRights</summary>
		RoleUIElementSubPanelRightsEntity,
		///<summary>RoomControlArea</summary>
		RoomControlAreaEntity,
		///<summary>RoomControlAreaLanguage</summary>
		RoomControlAreaLanguageEntity,
		///<summary>RoomControlComponent</summary>
		RoomControlComponentEntity,
		///<summary>RoomControlComponentLanguage</summary>
		RoomControlComponentLanguageEntity,
		///<summary>RoomControlConfiguration</summary>
		RoomControlConfigurationEntity,
		///<summary>RoomControlSection</summary>
		RoomControlSectionEntity,
		///<summary>RoomControlSectionItem</summary>
		RoomControlSectionItemEntity,
		///<summary>RoomControlSectionItemLanguage</summary>
		RoomControlSectionItemLanguageEntity,
		///<summary>RoomControlSectionLanguage</summary>
		RoomControlSectionLanguageEntity,
		///<summary>RoomControlWidget</summary>
		RoomControlWidgetEntity,
		///<summary>RoomControlWidgetLanguage</summary>
		RoomControlWidgetLanguageEntity,
		///<summary>Route</summary>
		RouteEntity,
		///<summary>Routestep</summary>
		RoutestepEntity,
		///<summary>Routestephandler</summary>
		RoutestephandlerEntity,
		///<summary>Schedule</summary>
		ScheduleEntity,
		///<summary>ScheduledCommand</summary>
		ScheduledCommandEntity,
		///<summary>ScheduledCommandTask</summary>
		ScheduledCommandTaskEntity,
		///<summary>ScheduledMessage</summary>
		ScheduledMessageEntity,
		///<summary>ScheduledMessageHistory</summary>
		ScheduledMessageHistoryEntity,
		///<summary>ScheduledMessageLanguage</summary>
		ScheduledMessageLanguageEntity,
		///<summary>Scheduleitem</summary>
		ScheduleitemEntity,
		///<summary>ServiceMethod</summary>
		ServiceMethodEntity,
		///<summary>ServiceMethodDeliverypointgroup</summary>
		ServiceMethodDeliverypointgroupEntity,
		///<summary>SetupCode</summary>
		SetupCodeEntity,
		///<summary>Site</summary>
		SiteEntity,
		///<summary>SiteCulture</summary>
		SiteCultureEntity,
		///<summary>SiteLanguage</summary>
		SiteLanguageEntity,
		///<summary>SiteTemplate</summary>
		SiteTemplateEntity,
		///<summary>SiteTemplateCulture</summary>
		SiteTemplateCultureEntity,
		///<summary>SiteTemplateLanguage</summary>
		SiteTemplateLanguageEntity,
		///<summary>SmsInformation</summary>
		SmsInformationEntity,
		///<summary>SmsKeyword</summary>
		SmsKeywordEntity,
		///<summary>Station</summary>
		StationEntity,
		///<summary>StationLanguage</summary>
		StationLanguageEntity,
		///<summary>StationList</summary>
		StationListEntity,
		///<summary>Supplier</summary>
		SupplierEntity,
		///<summary>Supportagent</summary>
		SupportagentEntity,
		///<summary>Supportpool</summary>
		SupportpoolEntity,
		///<summary>SupportpoolNotificationRecipient</summary>
		SupportpoolNotificationRecipientEntity,
		///<summary>SupportpoolSupportagent</summary>
		SupportpoolSupportagentEntity,
		///<summary>Survey</summary>
		SurveyEntity,
		///<summary>SurveyAnswer</summary>
		SurveyAnswerEntity,
		///<summary>SurveyAnswerLanguage</summary>
		SurveyAnswerLanguageEntity,
		///<summary>SurveyLanguage</summary>
		SurveyLanguageEntity,
		///<summary>SurveyPage</summary>
		SurveyPageEntity,
		///<summary>SurveyQuestion</summary>
		SurveyQuestionEntity,
		///<summary>SurveyQuestionLanguage</summary>
		SurveyQuestionLanguageEntity,
		///<summary>SurveyResult</summary>
		SurveyResultEntity,
		///<summary>Tag</summary>
		TagEntity,
		///<summary>TaxTariff</summary>
		TaxTariffEntity,
		///<summary>Terminal</summary>
		TerminalEntity,
		///<summary>TerminalConfiguration</summary>
		TerminalConfigurationEntity,
		///<summary>TerminalLog</summary>
		TerminalLogEntity,
		///<summary>TerminalLogFile</summary>
		TerminalLogFileEntity,
		///<summary>TerminalMessageTemplateCategory</summary>
		TerminalMessageTemplateCategoryEntity,
		///<summary>TerminalState</summary>
		TerminalStateEntity,
		///<summary>Timestamp</summary>
		TimestampEntity,
		///<summary>TimeZone</summary>
		TimeZoneEntity,
		///<summary>Translation</summary>
		TranslationEntity,
		///<summary>UIElement</summary>
		UIElementEntity,
		///<summary>UIElementCustom</summary>
		UIElementCustomEntity,
		///<summary>UIElementSubPanel</summary>
		UIElementSubPanelEntity,
		///<summary>UIElementSubPanelCustom</summary>
		UIElementSubPanelCustomEntity,
		///<summary>UIElementSubPanelUIElement</summary>
		UIElementSubPanelUIElementEntity,
		///<summary>UIFooterItem</summary>
		UIFooterItemEntity,
		///<summary>UIFooterItemLanguage</summary>
		UIFooterItemLanguageEntity,
		///<summary>UIMode</summary>
		UIModeEntity,
		///<summary>UISchedule</summary>
		UIScheduleEntity,
		///<summary>UIScheduleItem</summary>
		UIScheduleItemEntity,
		///<summary>UIScheduleItemOccurrence</summary>
		UIScheduleItemOccurrenceEntity,
		///<summary>UITab</summary>
		UITabEntity,
		///<summary>UITabLanguage</summary>
		UITabLanguageEntity,
		///<summary>UITheme</summary>
		UIThemeEntity,
		///<summary>UIThemeColor</summary>
		UIThemeColorEntity,
		///<summary>UIThemeTextSize</summary>
		UIThemeTextSizeEntity,
		///<summary>UIWidget</summary>
		UIWidgetEntity,
		///<summary>UIWidgetAvailability</summary>
		UIWidgetAvailabilityEntity,
		///<summary>UIWidgetLanguage</summary>
		UIWidgetLanguageEntity,
		///<summary>UIWidgetTimer</summary>
		UIWidgetTimerEntity,
		///<summary>User</summary>
		UserEntity,
		///<summary>UserBrand</summary>
		UserBrandEntity,
		///<summary>UserLogon</summary>
		UserLogonEntity,
		///<summary>UserRole</summary>
		UserRoleEntity,
		///<summary>Vattariff</summary>
		VattariffEntity,
		///<summary>Vendor</summary>
		VendorEntity,
		///<summary>VenueCategory</summary>
		VenueCategoryEntity,
		///<summary>VenueCategoryLanguage</summary>
		VenueCategoryLanguageEntity,
		///<summary>View</summary>
		ViewEntity,
		///<summary>ViewCustom</summary>
		ViewCustomEntity,
		///<summary>ViewItem</summary>
		ViewItemEntity,
		///<summary>ViewItemCustom</summary>
		ViewItemCustomEntity,
		///<summary>Weather</summary>
		WeatherEntity,
		///<summary>WifiConfiguration</summary>
		WifiConfigurationEntity,
		///<summary>Action</summary>
		ActionEntity,
		///<summary>ApplicationConfiguration</summary>
		ApplicationConfigurationEntity,
		///<summary>CarouselItem</summary>
		CarouselItemEntity,
		///<summary>FeatureFlag</summary>
		FeatureFlagEntity,
		///<summary>LandingPage</summary>
		LandingPageEntity,
		///<summary>LandingPageWidget</summary>
		LandingPageWidgetEntity,
		///<summary>NavigationMenu</summary>
		NavigationMenuEntity,
		///<summary>NavigationMenuItem</summary>
		NavigationMenuItemEntity,
		///<summary>NavigationMenuWidget</summary>
		NavigationMenuWidgetEntity,
		///<summary>Theme</summary>
		ThemeEntity,
		///<summary>Widget</summary>
		WidgetEntity,
		///<summary>WidgetActionBanner</summary>
		WidgetActionBannerEntity,
		///<summary>WidgetActionButton</summary>
		WidgetActionButtonEntity,
		///<summary>WidgetCarousel</summary>
		WidgetCarouselEntity,
		///<summary>WidgetGroup</summary>
		WidgetGroupEntity,
		///<summary>WidgetGroupWidget</summary>
		WidgetGroupWidgetEntity,
		///<summary>WidgetHero</summary>
		WidgetHeroEntity,
		///<summary>WidgetLanguageSwitcher</summary>
		WidgetLanguageSwitcherEntity,
		///<summary>WidgetMarkdown</summary>
		WidgetMarkdownEntity,
		///<summary>WidgetOpeningTime</summary>
		WidgetOpeningTimeEntity,
		///<summary>WidgetPageTitle</summary>
		WidgetPageTitleEntity,
		///<summary>WidgetWaitTime</summary>
		WidgetWaitTimeEntity
	}


	#region Custom ConstantsEnums Code
	
	// __LLBLGENPRO_USER_CODE_REGION_START CustomUserConstants
	// __LLBLGENPRO_USER_CODE_REGION_END
	#endregion

	#region Included code

	#endregion
}

