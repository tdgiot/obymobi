﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
//////////////////////////////////////////////////////////////
using System;
using System.Linq;
using System.Collections.Generic;
using SD.LLBLGen.Pro.LinqSupportClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

using Obymobi.Data;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.RelationClasses;

namespace Obymobi.Data.Linq
{
	/// <summary>Meta-data class for the construction of Linq queries which are to be executed using LLBLGen Pro code.</summary>
	public partial class LinqMetaData : ILinqMetaData
	{
		#region Class Member Declarations
		private ITransaction _transactionToUse;
		private FunctionMappingStore _customFunctionMappings;
		private Context _contextToUse;
		#endregion
		
		/// <summary>CTor. Using this ctor will leave the transaction object to use empty. This is ok if you're not executing queries created with this
		/// meta data inside a transaction. If you're executing the queries created with this meta-data inside a transaction, either set the Transaction property
		/// on the IQueryable.Provider instance of the created LLBLGenProQuery object prior to execution or use the ctor which accepts a transaction object.</summary>
		public LinqMetaData() : this(null, null)
		{
		}
		
		/// <summary>CTor. If you're executing the queries created with this meta-data inside a transaction, pass a live ITransaction object to this ctor.</summary>
		/// <param name="transactionToUse">the transaction to use in queries created with this meta-data</param>
		/// <remarks> Be aware that the ITransaction object set via this property is kept alive by the LLBLGenProQuery objects created with this meta data
		/// till they go out of scope.</remarks>
		public LinqMetaData(ITransaction transactionToUse) : this(transactionToUse, null)
		{
		}
		
		/// <summary>CTor. If you're executing the queries created with this meta-data inside a transaction, pass a live ITransaction object to this ctor.</summary>
		/// <param name="transactionToUse">the transaction to use in queries created with this meta-data</param>
		/// <param name="customFunctionMappings">The custom function mappings to use. These take higher precedence than the ones in the DQE to use.</param>
		/// <remarks> Be aware that the ITransaction object set via this property is kept alive by the LLBLGenProQuery objects created with this meta data
		/// till they go out of scope.</remarks>
		public LinqMetaData(ITransaction transactionToUse, FunctionMappingStore customFunctionMappings)
		{
			_transactionToUse = transactionToUse;
			_customFunctionMappings = customFunctionMappings;
		}
		
		/// <summary>returns the datasource to use in a Linq query for the entity type specified</summary>
		/// <param name="typeOfEntity">the type of the entity to get the datasource for</param>
		/// <returns>the requested datasource</returns>
		public IDataSource GetQueryableForEntity(int typeOfEntity)
		{
			IDataSource toReturn = null;
			switch((Obymobi.Data.EntityType)typeOfEntity)
			{
				case Obymobi.Data.EntityType.AccessCodeEntity:
					toReturn = this.AccessCode;
					break;
				case Obymobi.Data.EntityType.AccessCodeCompanyEntity:
					toReturn = this.AccessCodeCompany;
					break;
				case Obymobi.Data.EntityType.AccessCodePointOfInterestEntity:
					toReturn = this.AccessCodePointOfInterest;
					break;
				case Obymobi.Data.EntityType.AccountEntity:
					toReturn = this.Account;
					break;
				case Obymobi.Data.EntityType.AccountCompanyEntity:
					toReturn = this.AccountCompany;
					break;
				case Obymobi.Data.EntityType.ActionButtonEntity:
					toReturn = this.ActionButton;
					break;
				case Obymobi.Data.EntityType.ActionButtonLanguageEntity:
					toReturn = this.ActionButtonLanguage;
					break;
				case Obymobi.Data.EntityType.AddressEntity:
					toReturn = this.Address;
					break;
				case Obymobi.Data.EntityType.AdvertisementEntity:
					toReturn = this.Advertisement;
					break;
				case Obymobi.Data.EntityType.AdvertisementConfigurationEntity:
					toReturn = this.AdvertisementConfiguration;
					break;
				case Obymobi.Data.EntityType.AdvertisementConfigurationAdvertisementEntity:
					toReturn = this.AdvertisementConfigurationAdvertisement;
					break;
				case Obymobi.Data.EntityType.AdvertisementLanguageEntity:
					toReturn = this.AdvertisementLanguage;
					break;
				case Obymobi.Data.EntityType.AdvertisementTagEntity:
					toReturn = this.AdvertisementTag;
					break;
				case Obymobi.Data.EntityType.AdvertisementTagAdvertisementEntity:
					toReturn = this.AdvertisementTagAdvertisement;
					break;
				case Obymobi.Data.EntityType.AdvertisementTagCategoryEntity:
					toReturn = this.AdvertisementTagCategory;
					break;
				case Obymobi.Data.EntityType.AdvertisementTagEntertainmentEntity:
					toReturn = this.AdvertisementTagEntertainment;
					break;
				case Obymobi.Data.EntityType.AdvertisementTagGenericproductEntity:
					toReturn = this.AdvertisementTagGenericproduct;
					break;
				case Obymobi.Data.EntityType.AdvertisementTagLanguageEntity:
					toReturn = this.AdvertisementTagLanguage;
					break;
				case Obymobi.Data.EntityType.AdvertisementTagProductEntity:
					toReturn = this.AdvertisementTagProduct;
					break;
				case Obymobi.Data.EntityType.AdyenPaymentMethodEntity:
					toReturn = this.AdyenPaymentMethod;
					break;
				case Obymobi.Data.EntityType.AdyenPaymentMethodBrandEntity:
					toReturn = this.AdyenPaymentMethodBrand;
					break;
				case Obymobi.Data.EntityType.AffiliateCampaignEntity:
					toReturn = this.AffiliateCampaign;
					break;
				case Obymobi.Data.EntityType.AffiliateCampaignAffiliatePartnerEntity:
					toReturn = this.AffiliateCampaignAffiliatePartner;
					break;
				case Obymobi.Data.EntityType.AffiliatePartnerEntity:
					toReturn = this.AffiliatePartner;
					break;
				case Obymobi.Data.EntityType.AlterationEntity:
					toReturn = this.Alteration;
					break;
				case Obymobi.Data.EntityType.AlterationitemEntity:
					toReturn = this.Alterationitem;
					break;
				case Obymobi.Data.EntityType.AlterationitemAlterationEntity:
					toReturn = this.AlterationitemAlteration;
					break;
				case Obymobi.Data.EntityType.AlterationLanguageEntity:
					toReturn = this.AlterationLanguage;
					break;
				case Obymobi.Data.EntityType.AlterationoptionEntity:
					toReturn = this.Alterationoption;
					break;
				case Obymobi.Data.EntityType.AlterationoptionLanguageEntity:
					toReturn = this.AlterationoptionLanguage;
					break;
				case Obymobi.Data.EntityType.AlterationoptionTagEntity:
					toReturn = this.AlterationoptionTag;
					break;
				case Obymobi.Data.EntityType.AlterationProductEntity:
					toReturn = this.AlterationProduct;
					break;
				case Obymobi.Data.EntityType.AmenityEntity:
					toReturn = this.Amenity;
					break;
				case Obymobi.Data.EntityType.AmenityLanguageEntity:
					toReturn = this.AmenityLanguage;
					break;
				case Obymobi.Data.EntityType.AnalyticsProcessingTaskEntity:
					toReturn = this.AnalyticsProcessingTask;
					break;
				case Obymobi.Data.EntityType.AnnouncementEntity:
					toReturn = this.Announcement;
					break;
				case Obymobi.Data.EntityType.AnnouncementLanguageEntity:
					toReturn = this.AnnouncementLanguage;
					break;
				case Obymobi.Data.EntityType.ApiAuthenticationEntity:
					toReturn = this.ApiAuthentication;
					break;
				case Obymobi.Data.EntityType.ApplicationEntity:
					toReturn = this.Application;
					break;
				case Obymobi.Data.EntityType.AttachmentEntity:
					toReturn = this.Attachment;
					break;
				case Obymobi.Data.EntityType.AttachmentLanguageEntity:
					toReturn = this.AttachmentLanguage;
					break;
				case Obymobi.Data.EntityType.AuditlogEntity:
					toReturn = this.Auditlog;
					break;
				case Obymobi.Data.EntityType.AvailabilityEntity:
					toReturn = this.Availability;
					break;
				case Obymobi.Data.EntityType.AzureNotificationHubEntity:
					toReturn = this.AzureNotificationHub;
					break;
				case Obymobi.Data.EntityType.BrandEntity:
					toReturn = this.Brand;
					break;
				case Obymobi.Data.EntityType.BrandCultureEntity:
					toReturn = this.BrandCulture;
					break;
				case Obymobi.Data.EntityType.BusinesshoursEntity:
					toReturn = this.Businesshours;
					break;
				case Obymobi.Data.EntityType.BusinesshoursexceptionEntity:
					toReturn = this.Businesshoursexception;
					break;
				case Obymobi.Data.EntityType.CategoryEntity:
					toReturn = this.Category;
					break;
				case Obymobi.Data.EntityType.CategoryAlterationEntity:
					toReturn = this.CategoryAlteration;
					break;
				case Obymobi.Data.EntityType.CategoryLanguageEntity:
					toReturn = this.CategoryLanguage;
					break;
				case Obymobi.Data.EntityType.CategorySuggestionEntity:
					toReturn = this.CategorySuggestion;
					break;
				case Obymobi.Data.EntityType.CategoryTagEntity:
					toReturn = this.CategoryTag;
					break;
				case Obymobi.Data.EntityType.CheckoutMethodEntity:
					toReturn = this.CheckoutMethod;
					break;
				case Obymobi.Data.EntityType.CheckoutMethodDeliverypointgroupEntity:
					toReturn = this.CheckoutMethodDeliverypointgroup;
					break;
				case Obymobi.Data.EntityType.ClientEntity:
					toReturn = this.Client;
					break;
				case Obymobi.Data.EntityType.ClientConfigurationEntity:
					toReturn = this.ClientConfiguration;
					break;
				case Obymobi.Data.EntityType.ClientConfigurationRouteEntity:
					toReturn = this.ClientConfigurationRoute;
					break;
				case Obymobi.Data.EntityType.ClientEntertainmentEntity:
					toReturn = this.ClientEntertainment;
					break;
				case Obymobi.Data.EntityType.ClientLogEntity:
					toReturn = this.ClientLog;
					break;
				case Obymobi.Data.EntityType.ClientLogFileEntity:
					toReturn = this.ClientLogFile;
					break;
				case Obymobi.Data.EntityType.ClientStateEntity:
					toReturn = this.ClientState;
					break;
				case Obymobi.Data.EntityType.CloudApplicationVersionEntity:
					toReturn = this.CloudApplicationVersion;
					break;
				case Obymobi.Data.EntityType.CloudProcessingTaskEntity:
					toReturn = this.CloudProcessingTask;
					break;
				case Obymobi.Data.EntityType.CloudStorageAccountEntity:
					toReturn = this.CloudStorageAccount;
					break;
				case Obymobi.Data.EntityType.CompanyEntity:
					toReturn = this.Company;
					break;
				case Obymobi.Data.EntityType.CompanyAmenityEntity:
					toReturn = this.CompanyAmenity;
					break;
				case Obymobi.Data.EntityType.CompanyBrandEntity:
					toReturn = this.CompanyBrand;
					break;
				case Obymobi.Data.EntityType.CompanyCultureEntity:
					toReturn = this.CompanyCulture;
					break;
				case Obymobi.Data.EntityType.CompanyCurrencyEntity:
					toReturn = this.CompanyCurrency;
					break;
				case Obymobi.Data.EntityType.CompanyEntertainmentEntity:
					toReturn = this.CompanyEntertainment;
					break;
				case Obymobi.Data.EntityType.CompanygroupEntity:
					toReturn = this.Companygroup;
					break;
				case Obymobi.Data.EntityType.CompanyLanguageEntity:
					toReturn = this.CompanyLanguage;
					break;
				case Obymobi.Data.EntityType.CompanyManagementTaskEntity:
					toReturn = this.CompanyManagementTask;
					break;
				case Obymobi.Data.EntityType.CompanyOwnerEntity:
					toReturn = this.CompanyOwner;
					break;
				case Obymobi.Data.EntityType.CompanyReleaseEntity:
					toReturn = this.CompanyRelease;
					break;
				case Obymobi.Data.EntityType.CompanyVenueCategoryEntity:
					toReturn = this.CompanyVenueCategory;
					break;
				case Obymobi.Data.EntityType.ConfigurationEntity:
					toReturn = this.Configuration;
					break;
				case Obymobi.Data.EntityType.CountryEntity:
					toReturn = this.Country;
					break;
				case Obymobi.Data.EntityType.CurrencyEntity:
					toReturn = this.Currency;
					break;
				case Obymobi.Data.EntityType.CustomerEntity:
					toReturn = this.Customer;
					break;
				case Obymobi.Data.EntityType.CustomTextEntity:
					toReturn = this.CustomText;
					break;
				case Obymobi.Data.EntityType.DeliveryDistanceEntity:
					toReturn = this.DeliveryDistance;
					break;
				case Obymobi.Data.EntityType.DeliveryInformationEntity:
					toReturn = this.DeliveryInformation;
					break;
				case Obymobi.Data.EntityType.DeliverypointEntity:
					toReturn = this.Deliverypoint;
					break;
				case Obymobi.Data.EntityType.DeliverypointExternalDeliverypointEntity:
					toReturn = this.DeliverypointExternalDeliverypoint;
					break;
				case Obymobi.Data.EntityType.DeliverypointgroupEntity:
					toReturn = this.Deliverypointgroup;
					break;
				case Obymobi.Data.EntityType.DeliverypointgroupAdvertisementEntity:
					toReturn = this.DeliverypointgroupAdvertisement;
					break;
				case Obymobi.Data.EntityType.DeliverypointgroupAnnouncementEntity:
					toReturn = this.DeliverypointgroupAnnouncement;
					break;
				case Obymobi.Data.EntityType.DeliverypointgroupEntertainmentEntity:
					toReturn = this.DeliverypointgroupEntertainment;
					break;
				case Obymobi.Data.EntityType.DeliverypointgroupLanguageEntity:
					toReturn = this.DeliverypointgroupLanguage;
					break;
				case Obymobi.Data.EntityType.DeliverypointgroupOccupancyEntity:
					toReturn = this.DeliverypointgroupOccupancy;
					break;
				case Obymobi.Data.EntityType.DeliverypointgroupProductEntity:
					toReturn = this.DeliverypointgroupProduct;
					break;
				case Obymobi.Data.EntityType.DeviceEntity:
					toReturn = this.Device;
					break;
				case Obymobi.Data.EntityType.DevicemediaEntity:
					toReturn = this.Devicemedia;
					break;
				case Obymobi.Data.EntityType.DeviceTokenHistoryEntity:
					toReturn = this.DeviceTokenHistory;
					break;
				case Obymobi.Data.EntityType.DeviceTokenTaskEntity:
					toReturn = this.DeviceTokenTask;
					break;
				case Obymobi.Data.EntityType.EntertainmentEntity:
					toReturn = this.Entertainment;
					break;
				case Obymobi.Data.EntityType.EntertainmentcategoryEntity:
					toReturn = this.Entertainmentcategory;
					break;
				case Obymobi.Data.EntityType.EntertainmentcategoryLanguageEntity:
					toReturn = this.EntertainmentcategoryLanguage;
					break;
				case Obymobi.Data.EntityType.EntertainmentConfigurationEntity:
					toReturn = this.EntertainmentConfiguration;
					break;
				case Obymobi.Data.EntityType.EntertainmentConfigurationEntertainmentEntity:
					toReturn = this.EntertainmentConfigurationEntertainment;
					break;
				case Obymobi.Data.EntityType.EntertainmentDependencyEntity:
					toReturn = this.EntertainmentDependency;
					break;
				case Obymobi.Data.EntityType.EntertainmentFileEntity:
					toReturn = this.EntertainmentFile;
					break;
				case Obymobi.Data.EntityType.EntertainmenturlEntity:
					toReturn = this.Entertainmenturl;
					break;
				case Obymobi.Data.EntityType.EntityFieldInformationEntity:
					toReturn = this.EntityFieldInformation;
					break;
				case Obymobi.Data.EntityType.EntityFieldInformationCustomEntity:
					toReturn = this.EntityFieldInformationCustom;
					break;
				case Obymobi.Data.EntityType.EntityInformationEntity:
					toReturn = this.EntityInformation;
					break;
				case Obymobi.Data.EntityType.EntityInformationCustomEntity:
					toReturn = this.EntityInformationCustom;
					break;
				case Obymobi.Data.EntityType.ExternalDeliverypointEntity:
					toReturn = this.ExternalDeliverypoint;
					break;
				case Obymobi.Data.EntityType.ExternalMenuEntity:
					toReturn = this.ExternalMenu;
					break;
				case Obymobi.Data.EntityType.ExternalProductEntity:
					toReturn = this.ExternalProduct;
					break;
				case Obymobi.Data.EntityType.ExternalSubProductEntity:
					toReturn = this.ExternalSubProduct;
					break;
				case Obymobi.Data.EntityType.ExternalSystemEntity:
					toReturn = this.ExternalSystem;
					break;
				case Obymobi.Data.EntityType.ExternalSystemLogEntity:
					toReturn = this.ExternalSystemLog;
					break;
				case Obymobi.Data.EntityType.FeatureToggleAvailabilityEntity:
					toReturn = this.FeatureToggleAvailability;
					break;
				case Obymobi.Data.EntityType.FolioEntity:
					toReturn = this.Folio;
					break;
				case Obymobi.Data.EntityType.FolioItemEntity:
					toReturn = this.FolioItem;
					break;
				case Obymobi.Data.EntityType.GameEntity:
					toReturn = this.Game;
					break;
				case Obymobi.Data.EntityType.GameSessionEntity:
					toReturn = this.GameSession;
					break;
				case Obymobi.Data.EntityType.GameSessionReportEntity:
					toReturn = this.GameSessionReport;
					break;
				case Obymobi.Data.EntityType.GameSessionReportConfigurationEntity:
					toReturn = this.GameSessionReportConfiguration;
					break;
				case Obymobi.Data.EntityType.GameSessionReportItemEntity:
					toReturn = this.GameSessionReportItem;
					break;
				case Obymobi.Data.EntityType.GenericalterationEntity:
					toReturn = this.Genericalteration;
					break;
				case Obymobi.Data.EntityType.GenericalterationitemEntity:
					toReturn = this.Genericalterationitem;
					break;
				case Obymobi.Data.EntityType.GenericalterationoptionEntity:
					toReturn = this.Genericalterationoption;
					break;
				case Obymobi.Data.EntityType.GenericcategoryEntity:
					toReturn = this.Genericcategory;
					break;
				case Obymobi.Data.EntityType.GenericcategoryLanguageEntity:
					toReturn = this.GenericcategoryLanguage;
					break;
				case Obymobi.Data.EntityType.GenericproductEntity:
					toReturn = this.Genericproduct;
					break;
				case Obymobi.Data.EntityType.GenericproductGenericalterationEntity:
					toReturn = this.GenericproductGenericalteration;
					break;
				case Obymobi.Data.EntityType.GenericproductLanguageEntity:
					toReturn = this.GenericproductLanguage;
					break;
				case Obymobi.Data.EntityType.GuestInformationEntity:
					toReturn = this.GuestInformation;
					break;
				case Obymobi.Data.EntityType.IcrtouchprintermappingEntity:
					toReturn = this.Icrtouchprintermapping;
					break;
				case Obymobi.Data.EntityType.IcrtouchprintermappingDeliverypointEntity:
					toReturn = this.IcrtouchprintermappingDeliverypoint;
					break;
				case Obymobi.Data.EntityType.IncomingSmsEntity:
					toReturn = this.IncomingSms;
					break;
				case Obymobi.Data.EntityType.InfraredCommandEntity:
					toReturn = this.InfraredCommand;
					break;
				case Obymobi.Data.EntityType.InfraredConfigurationEntity:
					toReturn = this.InfraredConfiguration;
					break;
				case Obymobi.Data.EntityType.LanguageEntity:
					toReturn = this.Language;
					break;
				case Obymobi.Data.EntityType.LicensedModuleEntity:
					toReturn = this.LicensedModule;
					break;
				case Obymobi.Data.EntityType.LicensedUIElementEntity:
					toReturn = this.LicensedUIElement;
					break;
				case Obymobi.Data.EntityType.LicensedUIElementSubPanelEntity:
					toReturn = this.LicensedUIElementSubPanel;
					break;
				case Obymobi.Data.EntityType.MapEntity:
					toReturn = this.Map;
					break;
				case Obymobi.Data.EntityType.MapPointOfInterestEntity:
					toReturn = this.MapPointOfInterest;
					break;
				case Obymobi.Data.EntityType.MediaEntity:
					toReturn = this.Media;
					break;
				case Obymobi.Data.EntityType.MediaCultureEntity:
					toReturn = this.MediaCulture;
					break;
				case Obymobi.Data.EntityType.MediaLanguageEntity:
					toReturn = this.MediaLanguage;
					break;
				case Obymobi.Data.EntityType.MediaProcessingTaskEntity:
					toReturn = this.MediaProcessingTask;
					break;
				case Obymobi.Data.EntityType.MediaRatioTypeMediaEntity:
					toReturn = this.MediaRatioTypeMedia;
					break;
				case Obymobi.Data.EntityType.MediaRatioTypeMediaFileEntity:
					toReturn = this.MediaRatioTypeMediaFile;
					break;
				case Obymobi.Data.EntityType.MediaRelationshipEntity:
					toReturn = this.MediaRelationship;
					break;
				case Obymobi.Data.EntityType.MenuEntity:
					toReturn = this.Menu;
					break;
				case Obymobi.Data.EntityType.MessageEntity:
					toReturn = this.Message;
					break;
				case Obymobi.Data.EntityType.MessagegroupEntity:
					toReturn = this.Messagegroup;
					break;
				case Obymobi.Data.EntityType.MessagegroupDeliverypointEntity:
					toReturn = this.MessagegroupDeliverypoint;
					break;
				case Obymobi.Data.EntityType.MessageRecipientEntity:
					toReturn = this.MessageRecipient;
					break;
				case Obymobi.Data.EntityType.MessageTemplateEntity:
					toReturn = this.MessageTemplate;
					break;
				case Obymobi.Data.EntityType.MessageTemplateCategoryEntity:
					toReturn = this.MessageTemplateCategory;
					break;
				case Obymobi.Data.EntityType.MessageTemplateCategoryMessageTemplateEntity:
					toReturn = this.MessageTemplateCategoryMessageTemplate;
					break;
				case Obymobi.Data.EntityType.ModuleEntity:
					toReturn = this.Module;
					break;
				case Obymobi.Data.EntityType.NetmessageEntity:
					toReturn = this.Netmessage;
					break;
				case Obymobi.Data.EntityType.OptInEntity:
					toReturn = this.OptIn;
					break;
				case Obymobi.Data.EntityType.OrderEntity:
					toReturn = this.Order;
					break;
				case Obymobi.Data.EntityType.OrderHourEntity:
					toReturn = this.OrderHour;
					break;
				case Obymobi.Data.EntityType.OrderitemEntity:
					toReturn = this.Orderitem;
					break;
				case Obymobi.Data.EntityType.OrderitemAlterationitemEntity:
					toReturn = this.OrderitemAlterationitem;
					break;
				case Obymobi.Data.EntityType.OrderitemAlterationitemTagEntity:
					toReturn = this.OrderitemAlterationitemTag;
					break;
				case Obymobi.Data.EntityType.OrderitemTagEntity:
					toReturn = this.OrderitemTag;
					break;
				case Obymobi.Data.EntityType.OrderNotificationLogEntity:
					toReturn = this.OrderNotificationLog;
					break;
				case Obymobi.Data.EntityType.OrderRoutestephandlerEntity:
					toReturn = this.OrderRoutestephandler;
					break;
				case Obymobi.Data.EntityType.OrderRoutestephandlerHistoryEntity:
					toReturn = this.OrderRoutestephandlerHistory;
					break;
				case Obymobi.Data.EntityType.OutletEntity:
					toReturn = this.Outlet;
					break;
				case Obymobi.Data.EntityType.OutletOperationalStateEntity:
					toReturn = this.OutletOperationalState;
					break;
				case Obymobi.Data.EntityType.OutletSellerInformationEntity:
					toReturn = this.OutletSellerInformation;
					break;
				case Obymobi.Data.EntityType.PageEntity:
					toReturn = this.Page;
					break;
				case Obymobi.Data.EntityType.PageElementEntity:
					toReturn = this.PageElement;
					break;
				case Obymobi.Data.EntityType.PageLanguageEntity:
					toReturn = this.PageLanguage;
					break;
				case Obymobi.Data.EntityType.PageTemplateEntity:
					toReturn = this.PageTemplate;
					break;
				case Obymobi.Data.EntityType.PageTemplateElementEntity:
					toReturn = this.PageTemplateElement;
					break;
				case Obymobi.Data.EntityType.PageTemplateLanguageEntity:
					toReturn = this.PageTemplateLanguage;
					break;
				case Obymobi.Data.EntityType.PaymentIntegrationConfigurationEntity:
					toReturn = this.PaymentIntegrationConfiguration;
					break;
				case Obymobi.Data.EntityType.PaymentProviderEntity:
					toReturn = this.PaymentProvider;
					break;
				case Obymobi.Data.EntityType.PaymentProviderCompanyEntity:
					toReturn = this.PaymentProviderCompany;
					break;
				case Obymobi.Data.EntityType.PaymentTransactionEntity:
					toReturn = this.PaymentTransaction;
					break;
				case Obymobi.Data.EntityType.PaymentTransactionLogEntity:
					toReturn = this.PaymentTransactionLog;
					break;
				case Obymobi.Data.EntityType.PaymentTransactionSplitEntity:
					toReturn = this.PaymentTransactionSplit;
					break;
				case Obymobi.Data.EntityType.PmsActionRuleEntity:
					toReturn = this.PmsActionRule;
					break;
				case Obymobi.Data.EntityType.PmsReportColumnEntity:
					toReturn = this.PmsReportColumn;
					break;
				case Obymobi.Data.EntityType.PmsReportConfigurationEntity:
					toReturn = this.PmsReportConfiguration;
					break;
				case Obymobi.Data.EntityType.PmsRuleEntity:
					toReturn = this.PmsRule;
					break;
				case Obymobi.Data.EntityType.PointOfInterestEntity:
					toReturn = this.PointOfInterest;
					break;
				case Obymobi.Data.EntityType.PointOfInterestAmenityEntity:
					toReturn = this.PointOfInterestAmenity;
					break;
				case Obymobi.Data.EntityType.PointOfInterestLanguageEntity:
					toReturn = this.PointOfInterestLanguage;
					break;
				case Obymobi.Data.EntityType.PointOfInterestVenueCategoryEntity:
					toReturn = this.PointOfInterestVenueCategory;
					break;
				case Obymobi.Data.EntityType.PosalterationEntity:
					toReturn = this.Posalteration;
					break;
				case Obymobi.Data.EntityType.PosalterationitemEntity:
					toReturn = this.Posalterationitem;
					break;
				case Obymobi.Data.EntityType.PosalterationoptionEntity:
					toReturn = this.Posalterationoption;
					break;
				case Obymobi.Data.EntityType.PoscategoryEntity:
					toReturn = this.Poscategory;
					break;
				case Obymobi.Data.EntityType.PosdeliverypointEntity:
					toReturn = this.Posdeliverypoint;
					break;
				case Obymobi.Data.EntityType.PosdeliverypointgroupEntity:
					toReturn = this.Posdeliverypointgroup;
					break;
				case Obymobi.Data.EntityType.PospaymentmethodEntity:
					toReturn = this.Pospaymentmethod;
					break;
				case Obymobi.Data.EntityType.PosproductEntity:
					toReturn = this.Posproduct;
					break;
				case Obymobi.Data.EntityType.PosproductPosalterationEntity:
					toReturn = this.PosproductPosalteration;
					break;
				case Obymobi.Data.EntityType.PriceLevelEntity:
					toReturn = this.PriceLevel;
					break;
				case Obymobi.Data.EntityType.PriceLevelItemEntity:
					toReturn = this.PriceLevelItem;
					break;
				case Obymobi.Data.EntityType.PriceScheduleEntity:
					toReturn = this.PriceSchedule;
					break;
				case Obymobi.Data.EntityType.PriceScheduleItemEntity:
					toReturn = this.PriceScheduleItem;
					break;
				case Obymobi.Data.EntityType.PriceScheduleItemOccurrenceEntity:
					toReturn = this.PriceScheduleItemOccurrence;
					break;
				case Obymobi.Data.EntityType.ProductEntity:
					toReturn = this.Product;
					break;
				case Obymobi.Data.EntityType.ProductAlterationEntity:
					toReturn = this.ProductAlteration;
					break;
				case Obymobi.Data.EntityType.ProductAttachmentEntity:
					toReturn = this.ProductAttachment;
					break;
				case Obymobi.Data.EntityType.ProductCategoryEntity:
					toReturn = this.ProductCategory;
					break;
				case Obymobi.Data.EntityType.ProductCategorySuggestionEntity:
					toReturn = this.ProductCategorySuggestion;
					break;
				case Obymobi.Data.EntityType.ProductCategoryTagEntity:
					toReturn = this.ProductCategoryTag;
					break;
				case Obymobi.Data.EntityType.ProductgroupEntity:
					toReturn = this.Productgroup;
					break;
				case Obymobi.Data.EntityType.ProductgroupItemEntity:
					toReturn = this.ProductgroupItem;
					break;
				case Obymobi.Data.EntityType.ProductLanguageEntity:
					toReturn = this.ProductLanguage;
					break;
				case Obymobi.Data.EntityType.ProductSuggestionEntity:
					toReturn = this.ProductSuggestion;
					break;
				case Obymobi.Data.EntityType.ProductTagEntity:
					toReturn = this.ProductTag;
					break;
				case Obymobi.Data.EntityType.PublishingEntity:
					toReturn = this.Publishing;
					break;
				case Obymobi.Data.EntityType.PublishingItemEntity:
					toReturn = this.PublishingItem;
					break;
				case Obymobi.Data.EntityType.RatingEntity:
					toReturn = this.Rating;
					break;
				case Obymobi.Data.EntityType.ReceiptEntity:
					toReturn = this.Receipt;
					break;
				case Obymobi.Data.EntityType.ReceiptTemplateEntity:
					toReturn = this.ReceiptTemplate;
					break;
				case Obymobi.Data.EntityType.ReferentialConstraintEntity:
					toReturn = this.ReferentialConstraint;
					break;
				case Obymobi.Data.EntityType.ReferentialConstraintCustomEntity:
					toReturn = this.ReferentialConstraintCustom;
					break;
				case Obymobi.Data.EntityType.ReleaseEntity:
					toReturn = this.Release;
					break;
				case Obymobi.Data.EntityType.ReportProcessingTaskEntity:
					toReturn = this.ReportProcessingTask;
					break;
				case Obymobi.Data.EntityType.ReportProcessingTaskFileEntity:
					toReturn = this.ReportProcessingTaskFile;
					break;
				case Obymobi.Data.EntityType.ReportProcessingTaskTemplateEntity:
					toReturn = this.ReportProcessingTaskTemplate;
					break;
				case Obymobi.Data.EntityType.RequestlogEntity:
					toReturn = this.Requestlog;
					break;
				case Obymobi.Data.EntityType.RoleEntity:
					toReturn = this.Role;
					break;
				case Obymobi.Data.EntityType.RoleModuleRightsEntity:
					toReturn = this.RoleModuleRights;
					break;
				case Obymobi.Data.EntityType.RoleUIElementRightsEntity:
					toReturn = this.RoleUIElementRights;
					break;
				case Obymobi.Data.EntityType.RoleUIElementSubPanelRightsEntity:
					toReturn = this.RoleUIElementSubPanelRights;
					break;
				case Obymobi.Data.EntityType.RoomControlAreaEntity:
					toReturn = this.RoomControlArea;
					break;
				case Obymobi.Data.EntityType.RoomControlAreaLanguageEntity:
					toReturn = this.RoomControlAreaLanguage;
					break;
				case Obymobi.Data.EntityType.RoomControlComponentEntity:
					toReturn = this.RoomControlComponent;
					break;
				case Obymobi.Data.EntityType.RoomControlComponentLanguageEntity:
					toReturn = this.RoomControlComponentLanguage;
					break;
				case Obymobi.Data.EntityType.RoomControlConfigurationEntity:
					toReturn = this.RoomControlConfiguration;
					break;
				case Obymobi.Data.EntityType.RoomControlSectionEntity:
					toReturn = this.RoomControlSection;
					break;
				case Obymobi.Data.EntityType.RoomControlSectionItemEntity:
					toReturn = this.RoomControlSectionItem;
					break;
				case Obymobi.Data.EntityType.RoomControlSectionItemLanguageEntity:
					toReturn = this.RoomControlSectionItemLanguage;
					break;
				case Obymobi.Data.EntityType.RoomControlSectionLanguageEntity:
					toReturn = this.RoomControlSectionLanguage;
					break;
				case Obymobi.Data.EntityType.RoomControlWidgetEntity:
					toReturn = this.RoomControlWidget;
					break;
				case Obymobi.Data.EntityType.RoomControlWidgetLanguageEntity:
					toReturn = this.RoomControlWidgetLanguage;
					break;
				case Obymobi.Data.EntityType.RouteEntity:
					toReturn = this.Route;
					break;
				case Obymobi.Data.EntityType.RoutestepEntity:
					toReturn = this.Routestep;
					break;
				case Obymobi.Data.EntityType.RoutestephandlerEntity:
					toReturn = this.Routestephandler;
					break;
				case Obymobi.Data.EntityType.ScheduleEntity:
					toReturn = this.Schedule;
					break;
				case Obymobi.Data.EntityType.ScheduledCommandEntity:
					toReturn = this.ScheduledCommand;
					break;
				case Obymobi.Data.EntityType.ScheduledCommandTaskEntity:
					toReturn = this.ScheduledCommandTask;
					break;
				case Obymobi.Data.EntityType.ScheduledMessageEntity:
					toReturn = this.ScheduledMessage;
					break;
				case Obymobi.Data.EntityType.ScheduledMessageHistoryEntity:
					toReturn = this.ScheduledMessageHistory;
					break;
				case Obymobi.Data.EntityType.ScheduledMessageLanguageEntity:
					toReturn = this.ScheduledMessageLanguage;
					break;
				case Obymobi.Data.EntityType.ScheduleitemEntity:
					toReturn = this.Scheduleitem;
					break;
				case Obymobi.Data.EntityType.ServiceMethodEntity:
					toReturn = this.ServiceMethod;
					break;
				case Obymobi.Data.EntityType.ServiceMethodDeliverypointgroupEntity:
					toReturn = this.ServiceMethodDeliverypointgroup;
					break;
				case Obymobi.Data.EntityType.SetupCodeEntity:
					toReturn = this.SetupCode;
					break;
				case Obymobi.Data.EntityType.SiteEntity:
					toReturn = this.Site;
					break;
				case Obymobi.Data.EntityType.SiteCultureEntity:
					toReturn = this.SiteCulture;
					break;
				case Obymobi.Data.EntityType.SiteLanguageEntity:
					toReturn = this.SiteLanguage;
					break;
				case Obymobi.Data.EntityType.SiteTemplateEntity:
					toReturn = this.SiteTemplate;
					break;
				case Obymobi.Data.EntityType.SiteTemplateCultureEntity:
					toReturn = this.SiteTemplateCulture;
					break;
				case Obymobi.Data.EntityType.SiteTemplateLanguageEntity:
					toReturn = this.SiteTemplateLanguage;
					break;
				case Obymobi.Data.EntityType.SmsInformationEntity:
					toReturn = this.SmsInformation;
					break;
				case Obymobi.Data.EntityType.SmsKeywordEntity:
					toReturn = this.SmsKeyword;
					break;
				case Obymobi.Data.EntityType.StationEntity:
					toReturn = this.Station;
					break;
				case Obymobi.Data.EntityType.StationLanguageEntity:
					toReturn = this.StationLanguage;
					break;
				case Obymobi.Data.EntityType.StationListEntity:
					toReturn = this.StationList;
					break;
				case Obymobi.Data.EntityType.SupplierEntity:
					toReturn = this.Supplier;
					break;
				case Obymobi.Data.EntityType.SupportagentEntity:
					toReturn = this.Supportagent;
					break;
				case Obymobi.Data.EntityType.SupportpoolEntity:
					toReturn = this.Supportpool;
					break;
				case Obymobi.Data.EntityType.SupportpoolNotificationRecipientEntity:
					toReturn = this.SupportpoolNotificationRecipient;
					break;
				case Obymobi.Data.EntityType.SupportpoolSupportagentEntity:
					toReturn = this.SupportpoolSupportagent;
					break;
				case Obymobi.Data.EntityType.SurveyEntity:
					toReturn = this.Survey;
					break;
				case Obymobi.Data.EntityType.SurveyAnswerEntity:
					toReturn = this.SurveyAnswer;
					break;
				case Obymobi.Data.EntityType.SurveyAnswerLanguageEntity:
					toReturn = this.SurveyAnswerLanguage;
					break;
				case Obymobi.Data.EntityType.SurveyLanguageEntity:
					toReturn = this.SurveyLanguage;
					break;
				case Obymobi.Data.EntityType.SurveyPageEntity:
					toReturn = this.SurveyPage;
					break;
				case Obymobi.Data.EntityType.SurveyQuestionEntity:
					toReturn = this.SurveyQuestion;
					break;
				case Obymobi.Data.EntityType.SurveyQuestionLanguageEntity:
					toReturn = this.SurveyQuestionLanguage;
					break;
				case Obymobi.Data.EntityType.SurveyResultEntity:
					toReturn = this.SurveyResult;
					break;
				case Obymobi.Data.EntityType.TagEntity:
					toReturn = this.Tag;
					break;
				case Obymobi.Data.EntityType.TaxTariffEntity:
					toReturn = this.TaxTariff;
					break;
				case Obymobi.Data.EntityType.TerminalEntity:
					toReturn = this.Terminal;
					break;
				case Obymobi.Data.EntityType.TerminalConfigurationEntity:
					toReturn = this.TerminalConfiguration;
					break;
				case Obymobi.Data.EntityType.TerminalLogEntity:
					toReturn = this.TerminalLog;
					break;
				case Obymobi.Data.EntityType.TerminalLogFileEntity:
					toReturn = this.TerminalLogFile;
					break;
				case Obymobi.Data.EntityType.TerminalMessageTemplateCategoryEntity:
					toReturn = this.TerminalMessageTemplateCategory;
					break;
				case Obymobi.Data.EntityType.TerminalStateEntity:
					toReturn = this.TerminalState;
					break;
				case Obymobi.Data.EntityType.TimestampEntity:
					toReturn = this.Timestamp;
					break;
				case Obymobi.Data.EntityType.TimeZoneEntity:
					toReturn = this.TimeZone;
					break;
				case Obymobi.Data.EntityType.TranslationEntity:
					toReturn = this.Translation;
					break;
				case Obymobi.Data.EntityType.UIElementEntity:
					toReturn = this.UIElement;
					break;
				case Obymobi.Data.EntityType.UIElementCustomEntity:
					toReturn = this.UIElementCustom;
					break;
				case Obymobi.Data.EntityType.UIElementSubPanelEntity:
					toReturn = this.UIElementSubPanel;
					break;
				case Obymobi.Data.EntityType.UIElementSubPanelCustomEntity:
					toReturn = this.UIElementSubPanelCustom;
					break;
				case Obymobi.Data.EntityType.UIElementSubPanelUIElementEntity:
					toReturn = this.UIElementSubPanelUIElement;
					break;
				case Obymobi.Data.EntityType.UIFooterItemEntity:
					toReturn = this.UIFooterItem;
					break;
				case Obymobi.Data.EntityType.UIFooterItemLanguageEntity:
					toReturn = this.UIFooterItemLanguage;
					break;
				case Obymobi.Data.EntityType.UIModeEntity:
					toReturn = this.UIMode;
					break;
				case Obymobi.Data.EntityType.UIScheduleEntity:
					toReturn = this.UISchedule;
					break;
				case Obymobi.Data.EntityType.UIScheduleItemEntity:
					toReturn = this.UIScheduleItem;
					break;
				case Obymobi.Data.EntityType.UIScheduleItemOccurrenceEntity:
					toReturn = this.UIScheduleItemOccurrence;
					break;
				case Obymobi.Data.EntityType.UITabEntity:
					toReturn = this.UITab;
					break;
				case Obymobi.Data.EntityType.UITabLanguageEntity:
					toReturn = this.UITabLanguage;
					break;
				case Obymobi.Data.EntityType.UIThemeEntity:
					toReturn = this.UITheme;
					break;
				case Obymobi.Data.EntityType.UIThemeColorEntity:
					toReturn = this.UIThemeColor;
					break;
				case Obymobi.Data.EntityType.UIThemeTextSizeEntity:
					toReturn = this.UIThemeTextSize;
					break;
				case Obymobi.Data.EntityType.UIWidgetEntity:
					toReturn = this.UIWidget;
					break;
				case Obymobi.Data.EntityType.UIWidgetAvailabilityEntity:
					toReturn = this.UIWidgetAvailability;
					break;
				case Obymobi.Data.EntityType.UIWidgetLanguageEntity:
					toReturn = this.UIWidgetLanguage;
					break;
				case Obymobi.Data.EntityType.UIWidgetTimerEntity:
					toReturn = this.UIWidgetTimer;
					break;
				case Obymobi.Data.EntityType.UserEntity:
					toReturn = this.User;
					break;
				case Obymobi.Data.EntityType.UserBrandEntity:
					toReturn = this.UserBrand;
					break;
				case Obymobi.Data.EntityType.UserLogonEntity:
					toReturn = this.UserLogon;
					break;
				case Obymobi.Data.EntityType.UserRoleEntity:
					toReturn = this.UserRole;
					break;
				case Obymobi.Data.EntityType.VattariffEntity:
					toReturn = this.Vattariff;
					break;
				case Obymobi.Data.EntityType.VendorEntity:
					toReturn = this.Vendor;
					break;
				case Obymobi.Data.EntityType.VenueCategoryEntity:
					toReturn = this.VenueCategory;
					break;
				case Obymobi.Data.EntityType.VenueCategoryLanguageEntity:
					toReturn = this.VenueCategoryLanguage;
					break;
				case Obymobi.Data.EntityType.ViewEntity:
					toReturn = this.View;
					break;
				case Obymobi.Data.EntityType.ViewCustomEntity:
					toReturn = this.ViewCustom;
					break;
				case Obymobi.Data.EntityType.ViewItemEntity:
					toReturn = this.ViewItem;
					break;
				case Obymobi.Data.EntityType.ViewItemCustomEntity:
					toReturn = this.ViewItemCustom;
					break;
				case Obymobi.Data.EntityType.WeatherEntity:
					toReturn = this.Weather;
					break;
				case Obymobi.Data.EntityType.WifiConfigurationEntity:
					toReturn = this.WifiConfiguration;
					break;
				case Obymobi.Data.EntityType.ActionEntity:
					toReturn = this.Action;
					break;
				case Obymobi.Data.EntityType.ApplicationConfigurationEntity:
					toReturn = this.ApplicationConfiguration;
					break;
				case Obymobi.Data.EntityType.CarouselItemEntity:
					toReturn = this.CarouselItem;
					break;
				case Obymobi.Data.EntityType.FeatureFlagEntity:
					toReturn = this.FeatureFlag;
					break;
				case Obymobi.Data.EntityType.LandingPageEntity:
					toReturn = this.LandingPage;
					break;
				case Obymobi.Data.EntityType.LandingPageWidgetEntity:
					toReturn = this.LandingPageWidget;
					break;
				case Obymobi.Data.EntityType.NavigationMenuEntity:
					toReturn = this.NavigationMenu;
					break;
				case Obymobi.Data.EntityType.NavigationMenuItemEntity:
					toReturn = this.NavigationMenuItem;
					break;
				case Obymobi.Data.EntityType.NavigationMenuWidgetEntity:
					toReturn = this.NavigationMenuWidget;
					break;
				case Obymobi.Data.EntityType.ThemeEntity:
					toReturn = this.Theme;
					break;
				case Obymobi.Data.EntityType.WidgetEntity:
					toReturn = this.Widget;
					break;
				case Obymobi.Data.EntityType.WidgetActionBannerEntity:
					toReturn = this.WidgetActionBanner;
					break;
				case Obymobi.Data.EntityType.WidgetActionButtonEntity:
					toReturn = this.WidgetActionButton;
					break;
				case Obymobi.Data.EntityType.WidgetCarouselEntity:
					toReturn = this.WidgetCarousel;
					break;
				case Obymobi.Data.EntityType.WidgetGroupEntity:
					toReturn = this.WidgetGroup;
					break;
				case Obymobi.Data.EntityType.WidgetGroupWidgetEntity:
					toReturn = this.WidgetGroupWidget;
					break;
				case Obymobi.Data.EntityType.WidgetHeroEntity:
					toReturn = this.WidgetHero;
					break;
				case Obymobi.Data.EntityType.WidgetLanguageSwitcherEntity:
					toReturn = this.WidgetLanguageSwitcher;
					break;
				case Obymobi.Data.EntityType.WidgetMarkdownEntity:
					toReturn = this.WidgetMarkdown;
					break;
				case Obymobi.Data.EntityType.WidgetOpeningTimeEntity:
					toReturn = this.WidgetOpeningTime;
					break;
				case Obymobi.Data.EntityType.WidgetPageTitleEntity:
					toReturn = this.WidgetPageTitle;
					break;
				case Obymobi.Data.EntityType.WidgetWaitTimeEntity:
					toReturn = this.WidgetWaitTime;
					break;
				default:
					toReturn = null;
					break;
			}
			return toReturn;
		}

		/// <summary>returns the datasource to use in a Linq query for the entity type specified</summary>
		/// <typeparam name="TEntity">the type of the entity to get the datasource for</typeparam>
		/// <returns>the requested datasource</returns>
		public DataSource<TEntity> GetQueryableForEntity<TEntity>()
			    where TEntity : class
		{
    		return new DataSource<TEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse);
		}

		/// <summary>returns the datasource to use in a Linq query when targeting AccessCodeEntity instances in the database.</summary>
		public DataSource<AccessCodeEntity> AccessCode
		{
			get { return new DataSource<AccessCodeEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting AccessCodeCompanyEntity instances in the database.</summary>
		public DataSource<AccessCodeCompanyEntity> AccessCodeCompany
		{
			get { return new DataSource<AccessCodeCompanyEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting AccessCodePointOfInterestEntity instances in the database.</summary>
		public DataSource<AccessCodePointOfInterestEntity> AccessCodePointOfInterest
		{
			get { return new DataSource<AccessCodePointOfInterestEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting AccountEntity instances in the database.</summary>
		public DataSource<AccountEntity> Account
		{
			get { return new DataSource<AccountEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting AccountCompanyEntity instances in the database.</summary>
		public DataSource<AccountCompanyEntity> AccountCompany
		{
			get { return new DataSource<AccountCompanyEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting ActionButtonEntity instances in the database.</summary>
		public DataSource<ActionButtonEntity> ActionButton
		{
			get { return new DataSource<ActionButtonEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting ActionButtonLanguageEntity instances in the database.</summary>
		public DataSource<ActionButtonLanguageEntity> ActionButtonLanguage
		{
			get { return new DataSource<ActionButtonLanguageEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting AddressEntity instances in the database.</summary>
		public DataSource<AddressEntity> Address
		{
			get { return new DataSource<AddressEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting AdvertisementEntity instances in the database.</summary>
		public DataSource<AdvertisementEntity> Advertisement
		{
			get { return new DataSource<AdvertisementEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting AdvertisementConfigurationEntity instances in the database.</summary>
		public DataSource<AdvertisementConfigurationEntity> AdvertisementConfiguration
		{
			get { return new DataSource<AdvertisementConfigurationEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting AdvertisementConfigurationAdvertisementEntity instances in the database.</summary>
		public DataSource<AdvertisementConfigurationAdvertisementEntity> AdvertisementConfigurationAdvertisement
		{
			get { return new DataSource<AdvertisementConfigurationAdvertisementEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting AdvertisementLanguageEntity instances in the database.</summary>
		public DataSource<AdvertisementLanguageEntity> AdvertisementLanguage
		{
			get { return new DataSource<AdvertisementLanguageEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting AdvertisementTagEntity instances in the database.</summary>
		public DataSource<AdvertisementTagEntity> AdvertisementTag
		{
			get { return new DataSource<AdvertisementTagEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting AdvertisementTagAdvertisementEntity instances in the database.</summary>
		public DataSource<AdvertisementTagAdvertisementEntity> AdvertisementTagAdvertisement
		{
			get { return new DataSource<AdvertisementTagAdvertisementEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting AdvertisementTagCategoryEntity instances in the database.</summary>
		public DataSource<AdvertisementTagCategoryEntity> AdvertisementTagCategory
		{
			get { return new DataSource<AdvertisementTagCategoryEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting AdvertisementTagEntertainmentEntity instances in the database.</summary>
		public DataSource<AdvertisementTagEntertainmentEntity> AdvertisementTagEntertainment
		{
			get { return new DataSource<AdvertisementTagEntertainmentEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting AdvertisementTagGenericproductEntity instances in the database.</summary>
		public DataSource<AdvertisementTagGenericproductEntity> AdvertisementTagGenericproduct
		{
			get { return new DataSource<AdvertisementTagGenericproductEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting AdvertisementTagLanguageEntity instances in the database.</summary>
		public DataSource<AdvertisementTagLanguageEntity> AdvertisementTagLanguage
		{
			get { return new DataSource<AdvertisementTagLanguageEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting AdvertisementTagProductEntity instances in the database.</summary>
		public DataSource<AdvertisementTagProductEntity> AdvertisementTagProduct
		{
			get { return new DataSource<AdvertisementTagProductEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting AdyenPaymentMethodEntity instances in the database.</summary>
		public DataSource<AdyenPaymentMethodEntity> AdyenPaymentMethod
		{
			get { return new DataSource<AdyenPaymentMethodEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting AdyenPaymentMethodBrandEntity instances in the database.</summary>
		public DataSource<AdyenPaymentMethodBrandEntity> AdyenPaymentMethodBrand
		{
			get { return new DataSource<AdyenPaymentMethodBrandEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting AffiliateCampaignEntity instances in the database.</summary>
		public DataSource<AffiliateCampaignEntity> AffiliateCampaign
		{
			get { return new DataSource<AffiliateCampaignEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting AffiliateCampaignAffiliatePartnerEntity instances in the database.</summary>
		public DataSource<AffiliateCampaignAffiliatePartnerEntity> AffiliateCampaignAffiliatePartner
		{
			get { return new DataSource<AffiliateCampaignAffiliatePartnerEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting AffiliatePartnerEntity instances in the database.</summary>
		public DataSource<AffiliatePartnerEntity> AffiliatePartner
		{
			get { return new DataSource<AffiliatePartnerEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting AlterationEntity instances in the database.</summary>
		public DataSource<AlterationEntity> Alteration
		{
			get { return new DataSource<AlterationEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting AlterationitemEntity instances in the database.</summary>
		public DataSource<AlterationitemEntity> Alterationitem
		{
			get { return new DataSource<AlterationitemEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting AlterationitemAlterationEntity instances in the database.</summary>
		public DataSource<AlterationitemAlterationEntity> AlterationitemAlteration
		{
			get { return new DataSource<AlterationitemAlterationEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting AlterationLanguageEntity instances in the database.</summary>
		public DataSource<AlterationLanguageEntity> AlterationLanguage
		{
			get { return new DataSource<AlterationLanguageEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting AlterationoptionEntity instances in the database.</summary>
		public DataSource<AlterationoptionEntity> Alterationoption
		{
			get { return new DataSource<AlterationoptionEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting AlterationoptionLanguageEntity instances in the database.</summary>
		public DataSource<AlterationoptionLanguageEntity> AlterationoptionLanguage
		{
			get { return new DataSource<AlterationoptionLanguageEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting AlterationoptionTagEntity instances in the database.</summary>
		public DataSource<AlterationoptionTagEntity> AlterationoptionTag
		{
			get { return new DataSource<AlterationoptionTagEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting AlterationProductEntity instances in the database.</summary>
		public DataSource<AlterationProductEntity> AlterationProduct
		{
			get { return new DataSource<AlterationProductEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting AmenityEntity instances in the database.</summary>
		public DataSource<AmenityEntity> Amenity
		{
			get { return new DataSource<AmenityEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting AmenityLanguageEntity instances in the database.</summary>
		public DataSource<AmenityLanguageEntity> AmenityLanguage
		{
			get { return new DataSource<AmenityLanguageEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting AnalyticsProcessingTaskEntity instances in the database.</summary>
		public DataSource<AnalyticsProcessingTaskEntity> AnalyticsProcessingTask
		{
			get { return new DataSource<AnalyticsProcessingTaskEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting AnnouncementEntity instances in the database.</summary>
		public DataSource<AnnouncementEntity> Announcement
		{
			get { return new DataSource<AnnouncementEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting AnnouncementLanguageEntity instances in the database.</summary>
		public DataSource<AnnouncementLanguageEntity> AnnouncementLanguage
		{
			get { return new DataSource<AnnouncementLanguageEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting ApiAuthenticationEntity instances in the database.</summary>
		public DataSource<ApiAuthenticationEntity> ApiAuthentication
		{
			get { return new DataSource<ApiAuthenticationEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting ApplicationEntity instances in the database.</summary>
		public DataSource<ApplicationEntity> Application
		{
			get { return new DataSource<ApplicationEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting AttachmentEntity instances in the database.</summary>
		public DataSource<AttachmentEntity> Attachment
		{
			get { return new DataSource<AttachmentEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting AttachmentLanguageEntity instances in the database.</summary>
		public DataSource<AttachmentLanguageEntity> AttachmentLanguage
		{
			get { return new DataSource<AttachmentLanguageEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting AuditlogEntity instances in the database.</summary>
		public DataSource<AuditlogEntity> Auditlog
		{
			get { return new DataSource<AuditlogEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting AvailabilityEntity instances in the database.</summary>
		public DataSource<AvailabilityEntity> Availability
		{
			get { return new DataSource<AvailabilityEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting AzureNotificationHubEntity instances in the database.</summary>
		public DataSource<AzureNotificationHubEntity> AzureNotificationHub
		{
			get { return new DataSource<AzureNotificationHubEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting BrandEntity instances in the database.</summary>
		public DataSource<BrandEntity> Brand
		{
			get { return new DataSource<BrandEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting BrandCultureEntity instances in the database.</summary>
		public DataSource<BrandCultureEntity> BrandCulture
		{
			get { return new DataSource<BrandCultureEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting BusinesshoursEntity instances in the database.</summary>
		public DataSource<BusinesshoursEntity> Businesshours
		{
			get { return new DataSource<BusinesshoursEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting BusinesshoursexceptionEntity instances in the database.</summary>
		public DataSource<BusinesshoursexceptionEntity> Businesshoursexception
		{
			get { return new DataSource<BusinesshoursexceptionEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting CategoryEntity instances in the database.</summary>
		public DataSource<CategoryEntity> Category
		{
			get { return new DataSource<CategoryEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting CategoryAlterationEntity instances in the database.</summary>
		public DataSource<CategoryAlterationEntity> CategoryAlteration
		{
			get { return new DataSource<CategoryAlterationEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting CategoryLanguageEntity instances in the database.</summary>
		public DataSource<CategoryLanguageEntity> CategoryLanguage
		{
			get { return new DataSource<CategoryLanguageEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting CategorySuggestionEntity instances in the database.</summary>
		public DataSource<CategorySuggestionEntity> CategorySuggestion
		{
			get { return new DataSource<CategorySuggestionEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting CategoryTagEntity instances in the database.</summary>
		public DataSource<CategoryTagEntity> CategoryTag
		{
			get { return new DataSource<CategoryTagEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting CheckoutMethodEntity instances in the database.</summary>
		public DataSource<CheckoutMethodEntity> CheckoutMethod
		{
			get { return new DataSource<CheckoutMethodEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting CheckoutMethodDeliverypointgroupEntity instances in the database.</summary>
		public DataSource<CheckoutMethodDeliverypointgroupEntity> CheckoutMethodDeliverypointgroup
		{
			get { return new DataSource<CheckoutMethodDeliverypointgroupEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting ClientEntity instances in the database.</summary>
		public DataSource<ClientEntity> Client
		{
			get { return new DataSource<ClientEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting ClientConfigurationEntity instances in the database.</summary>
		public DataSource<ClientConfigurationEntity> ClientConfiguration
		{
			get { return new DataSource<ClientConfigurationEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting ClientConfigurationRouteEntity instances in the database.</summary>
		public DataSource<ClientConfigurationRouteEntity> ClientConfigurationRoute
		{
			get { return new DataSource<ClientConfigurationRouteEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting ClientEntertainmentEntity instances in the database.</summary>
		public DataSource<ClientEntertainmentEntity> ClientEntertainment
		{
			get { return new DataSource<ClientEntertainmentEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting ClientLogEntity instances in the database.</summary>
		public DataSource<ClientLogEntity> ClientLog
		{
			get { return new DataSource<ClientLogEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting ClientLogFileEntity instances in the database.</summary>
		public DataSource<ClientLogFileEntity> ClientLogFile
		{
			get { return new DataSource<ClientLogFileEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting ClientStateEntity instances in the database.</summary>
		public DataSource<ClientStateEntity> ClientState
		{
			get { return new DataSource<ClientStateEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting CloudApplicationVersionEntity instances in the database.</summary>
		public DataSource<CloudApplicationVersionEntity> CloudApplicationVersion
		{
			get { return new DataSource<CloudApplicationVersionEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting CloudProcessingTaskEntity instances in the database.</summary>
		public DataSource<CloudProcessingTaskEntity> CloudProcessingTask
		{
			get { return new DataSource<CloudProcessingTaskEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting CloudStorageAccountEntity instances in the database.</summary>
		public DataSource<CloudStorageAccountEntity> CloudStorageAccount
		{
			get { return new DataSource<CloudStorageAccountEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting CompanyEntity instances in the database.</summary>
		public DataSource<CompanyEntity> Company
		{
			get { return new DataSource<CompanyEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting CompanyAmenityEntity instances in the database.</summary>
		public DataSource<CompanyAmenityEntity> CompanyAmenity
		{
			get { return new DataSource<CompanyAmenityEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting CompanyBrandEntity instances in the database.</summary>
		public DataSource<CompanyBrandEntity> CompanyBrand
		{
			get { return new DataSource<CompanyBrandEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting CompanyCultureEntity instances in the database.</summary>
		public DataSource<CompanyCultureEntity> CompanyCulture
		{
			get { return new DataSource<CompanyCultureEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting CompanyCurrencyEntity instances in the database.</summary>
		public DataSource<CompanyCurrencyEntity> CompanyCurrency
		{
			get { return new DataSource<CompanyCurrencyEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting CompanyEntertainmentEntity instances in the database.</summary>
		public DataSource<CompanyEntertainmentEntity> CompanyEntertainment
		{
			get { return new DataSource<CompanyEntertainmentEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting CompanygroupEntity instances in the database.</summary>
		public DataSource<CompanygroupEntity> Companygroup
		{
			get { return new DataSource<CompanygroupEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting CompanyLanguageEntity instances in the database.</summary>
		public DataSource<CompanyLanguageEntity> CompanyLanguage
		{
			get { return new DataSource<CompanyLanguageEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting CompanyManagementTaskEntity instances in the database.</summary>
		public DataSource<CompanyManagementTaskEntity> CompanyManagementTask
		{
			get { return new DataSource<CompanyManagementTaskEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting CompanyOwnerEntity instances in the database.</summary>
		public DataSource<CompanyOwnerEntity> CompanyOwner
		{
			get { return new DataSource<CompanyOwnerEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting CompanyReleaseEntity instances in the database.</summary>
		public DataSource<CompanyReleaseEntity> CompanyRelease
		{
			get { return new DataSource<CompanyReleaseEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting CompanyVenueCategoryEntity instances in the database.</summary>
		public DataSource<CompanyVenueCategoryEntity> CompanyVenueCategory
		{
			get { return new DataSource<CompanyVenueCategoryEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting ConfigurationEntity instances in the database.</summary>
		public DataSource<ConfigurationEntity> Configuration
		{
			get { return new DataSource<ConfigurationEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting CountryEntity instances in the database.</summary>
		public DataSource<CountryEntity> Country
		{
			get { return new DataSource<CountryEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting CurrencyEntity instances in the database.</summary>
		public DataSource<CurrencyEntity> Currency
		{
			get { return new DataSource<CurrencyEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting CustomerEntity instances in the database.</summary>
		public DataSource<CustomerEntity> Customer
		{
			get { return new DataSource<CustomerEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting CustomTextEntity instances in the database.</summary>
		public DataSource<CustomTextEntity> CustomText
		{
			get { return new DataSource<CustomTextEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting DeliveryDistanceEntity instances in the database.</summary>
		public DataSource<DeliveryDistanceEntity> DeliveryDistance
		{
			get { return new DataSource<DeliveryDistanceEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting DeliveryInformationEntity instances in the database.</summary>
		public DataSource<DeliveryInformationEntity> DeliveryInformation
		{
			get { return new DataSource<DeliveryInformationEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting DeliverypointEntity instances in the database.</summary>
		public DataSource<DeliverypointEntity> Deliverypoint
		{
			get { return new DataSource<DeliverypointEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting DeliverypointExternalDeliverypointEntity instances in the database.</summary>
		public DataSource<DeliverypointExternalDeliverypointEntity> DeliverypointExternalDeliverypoint
		{
			get { return new DataSource<DeliverypointExternalDeliverypointEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting DeliverypointgroupEntity instances in the database.</summary>
		public DataSource<DeliverypointgroupEntity> Deliverypointgroup
		{
			get { return new DataSource<DeliverypointgroupEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting DeliverypointgroupAdvertisementEntity instances in the database.</summary>
		public DataSource<DeliverypointgroupAdvertisementEntity> DeliverypointgroupAdvertisement
		{
			get { return new DataSource<DeliverypointgroupAdvertisementEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting DeliverypointgroupAnnouncementEntity instances in the database.</summary>
		public DataSource<DeliverypointgroupAnnouncementEntity> DeliverypointgroupAnnouncement
		{
			get { return new DataSource<DeliverypointgroupAnnouncementEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting DeliverypointgroupEntertainmentEntity instances in the database.</summary>
		public DataSource<DeliverypointgroupEntertainmentEntity> DeliverypointgroupEntertainment
		{
			get { return new DataSource<DeliverypointgroupEntertainmentEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting DeliverypointgroupLanguageEntity instances in the database.</summary>
		public DataSource<DeliverypointgroupLanguageEntity> DeliverypointgroupLanguage
		{
			get { return new DataSource<DeliverypointgroupLanguageEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting DeliverypointgroupOccupancyEntity instances in the database.</summary>
		public DataSource<DeliverypointgroupOccupancyEntity> DeliverypointgroupOccupancy
		{
			get { return new DataSource<DeliverypointgroupOccupancyEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting DeliverypointgroupProductEntity instances in the database.</summary>
		public DataSource<DeliverypointgroupProductEntity> DeliverypointgroupProduct
		{
			get { return new DataSource<DeliverypointgroupProductEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting DeviceEntity instances in the database.</summary>
		public DataSource<DeviceEntity> Device
		{
			get { return new DataSource<DeviceEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting DevicemediaEntity instances in the database.</summary>
		public DataSource<DevicemediaEntity> Devicemedia
		{
			get { return new DataSource<DevicemediaEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting DeviceTokenHistoryEntity instances in the database.</summary>
		public DataSource<DeviceTokenHistoryEntity> DeviceTokenHistory
		{
			get { return new DataSource<DeviceTokenHistoryEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting DeviceTokenTaskEntity instances in the database.</summary>
		public DataSource<DeviceTokenTaskEntity> DeviceTokenTask
		{
			get { return new DataSource<DeviceTokenTaskEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting EntertainmentEntity instances in the database.</summary>
		public DataSource<EntertainmentEntity> Entertainment
		{
			get { return new DataSource<EntertainmentEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting EntertainmentcategoryEntity instances in the database.</summary>
		public DataSource<EntertainmentcategoryEntity> Entertainmentcategory
		{
			get { return new DataSource<EntertainmentcategoryEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting EntertainmentcategoryLanguageEntity instances in the database.</summary>
		public DataSource<EntertainmentcategoryLanguageEntity> EntertainmentcategoryLanguage
		{
			get { return new DataSource<EntertainmentcategoryLanguageEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting EntertainmentConfigurationEntity instances in the database.</summary>
		public DataSource<EntertainmentConfigurationEntity> EntertainmentConfiguration
		{
			get { return new DataSource<EntertainmentConfigurationEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting EntertainmentConfigurationEntertainmentEntity instances in the database.</summary>
		public DataSource<EntertainmentConfigurationEntertainmentEntity> EntertainmentConfigurationEntertainment
		{
			get { return new DataSource<EntertainmentConfigurationEntertainmentEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting EntertainmentDependencyEntity instances in the database.</summary>
		public DataSource<EntertainmentDependencyEntity> EntertainmentDependency
		{
			get { return new DataSource<EntertainmentDependencyEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting EntertainmentFileEntity instances in the database.</summary>
		public DataSource<EntertainmentFileEntity> EntertainmentFile
		{
			get { return new DataSource<EntertainmentFileEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting EntertainmenturlEntity instances in the database.</summary>
		public DataSource<EntertainmenturlEntity> Entertainmenturl
		{
			get { return new DataSource<EntertainmenturlEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting EntityFieldInformationEntity instances in the database.</summary>
		public DataSource<EntityFieldInformationEntity> EntityFieldInformation
		{
			get { return new DataSource<EntityFieldInformationEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting EntityFieldInformationCustomEntity instances in the database.</summary>
		public DataSource<EntityFieldInformationCustomEntity> EntityFieldInformationCustom
		{
			get { return new DataSource<EntityFieldInformationCustomEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting EntityInformationEntity instances in the database.</summary>
		public DataSource<EntityInformationEntity> EntityInformation
		{
			get { return new DataSource<EntityInformationEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting EntityInformationCustomEntity instances in the database.</summary>
		public DataSource<EntityInformationCustomEntity> EntityInformationCustom
		{
			get { return new DataSource<EntityInformationCustomEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting ExternalDeliverypointEntity instances in the database.</summary>
		public DataSource<ExternalDeliverypointEntity> ExternalDeliverypoint
		{
			get { return new DataSource<ExternalDeliverypointEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting ExternalMenuEntity instances in the database.</summary>
		public DataSource<ExternalMenuEntity> ExternalMenu
		{
			get { return new DataSource<ExternalMenuEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting ExternalProductEntity instances in the database.</summary>
		public DataSource<ExternalProductEntity> ExternalProduct
		{
			get { return new DataSource<ExternalProductEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting ExternalSubProductEntity instances in the database.</summary>
		public DataSource<ExternalSubProductEntity> ExternalSubProduct
		{
			get { return new DataSource<ExternalSubProductEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting ExternalSystemEntity instances in the database.</summary>
		public DataSource<ExternalSystemEntity> ExternalSystem
		{
			get { return new DataSource<ExternalSystemEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting ExternalSystemLogEntity instances in the database.</summary>
		public DataSource<ExternalSystemLogEntity> ExternalSystemLog
		{
			get { return new DataSource<ExternalSystemLogEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting FeatureToggleAvailabilityEntity instances in the database.</summary>
		public DataSource<FeatureToggleAvailabilityEntity> FeatureToggleAvailability
		{
			get { return new DataSource<FeatureToggleAvailabilityEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting FolioEntity instances in the database.</summary>
		public DataSource<FolioEntity> Folio
		{
			get { return new DataSource<FolioEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting FolioItemEntity instances in the database.</summary>
		public DataSource<FolioItemEntity> FolioItem
		{
			get { return new DataSource<FolioItemEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting GameEntity instances in the database.</summary>
		public DataSource<GameEntity> Game
		{
			get { return new DataSource<GameEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting GameSessionEntity instances in the database.</summary>
		public DataSource<GameSessionEntity> GameSession
		{
			get { return new DataSource<GameSessionEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting GameSessionReportEntity instances in the database.</summary>
		public DataSource<GameSessionReportEntity> GameSessionReport
		{
			get { return new DataSource<GameSessionReportEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting GameSessionReportConfigurationEntity instances in the database.</summary>
		public DataSource<GameSessionReportConfigurationEntity> GameSessionReportConfiguration
		{
			get { return new DataSource<GameSessionReportConfigurationEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting GameSessionReportItemEntity instances in the database.</summary>
		public DataSource<GameSessionReportItemEntity> GameSessionReportItem
		{
			get { return new DataSource<GameSessionReportItemEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting GenericalterationEntity instances in the database.</summary>
		public DataSource<GenericalterationEntity> Genericalteration
		{
			get { return new DataSource<GenericalterationEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting GenericalterationitemEntity instances in the database.</summary>
		public DataSource<GenericalterationitemEntity> Genericalterationitem
		{
			get { return new DataSource<GenericalterationitemEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting GenericalterationoptionEntity instances in the database.</summary>
		public DataSource<GenericalterationoptionEntity> Genericalterationoption
		{
			get { return new DataSource<GenericalterationoptionEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting GenericcategoryEntity instances in the database.</summary>
		public DataSource<GenericcategoryEntity> Genericcategory
		{
			get { return new DataSource<GenericcategoryEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting GenericcategoryLanguageEntity instances in the database.</summary>
		public DataSource<GenericcategoryLanguageEntity> GenericcategoryLanguage
		{
			get { return new DataSource<GenericcategoryLanguageEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting GenericproductEntity instances in the database.</summary>
		public DataSource<GenericproductEntity> Genericproduct
		{
			get { return new DataSource<GenericproductEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting GenericproductGenericalterationEntity instances in the database.</summary>
		public DataSource<GenericproductGenericalterationEntity> GenericproductGenericalteration
		{
			get { return new DataSource<GenericproductGenericalterationEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting GenericproductLanguageEntity instances in the database.</summary>
		public DataSource<GenericproductLanguageEntity> GenericproductLanguage
		{
			get { return new DataSource<GenericproductLanguageEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting GuestInformationEntity instances in the database.</summary>
		public DataSource<GuestInformationEntity> GuestInformation
		{
			get { return new DataSource<GuestInformationEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting IcrtouchprintermappingEntity instances in the database.</summary>
		public DataSource<IcrtouchprintermappingEntity> Icrtouchprintermapping
		{
			get { return new DataSource<IcrtouchprintermappingEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting IcrtouchprintermappingDeliverypointEntity instances in the database.</summary>
		public DataSource<IcrtouchprintermappingDeliverypointEntity> IcrtouchprintermappingDeliverypoint
		{
			get { return new DataSource<IcrtouchprintermappingDeliverypointEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting IncomingSmsEntity instances in the database.</summary>
		public DataSource<IncomingSmsEntity> IncomingSms
		{
			get { return new DataSource<IncomingSmsEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting InfraredCommandEntity instances in the database.</summary>
		public DataSource<InfraredCommandEntity> InfraredCommand
		{
			get { return new DataSource<InfraredCommandEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting InfraredConfigurationEntity instances in the database.</summary>
		public DataSource<InfraredConfigurationEntity> InfraredConfiguration
		{
			get { return new DataSource<InfraredConfigurationEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting LanguageEntity instances in the database.</summary>
		public DataSource<LanguageEntity> Language
		{
			get { return new DataSource<LanguageEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting LicensedModuleEntity instances in the database.</summary>
		public DataSource<LicensedModuleEntity> LicensedModule
		{
			get { return new DataSource<LicensedModuleEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting LicensedUIElementEntity instances in the database.</summary>
		public DataSource<LicensedUIElementEntity> LicensedUIElement
		{
			get { return new DataSource<LicensedUIElementEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting LicensedUIElementSubPanelEntity instances in the database.</summary>
		public DataSource<LicensedUIElementSubPanelEntity> LicensedUIElementSubPanel
		{
			get { return new DataSource<LicensedUIElementSubPanelEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting MapEntity instances in the database.</summary>
		public DataSource<MapEntity> Map
		{
			get { return new DataSource<MapEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting MapPointOfInterestEntity instances in the database.</summary>
		public DataSource<MapPointOfInterestEntity> MapPointOfInterest
		{
			get { return new DataSource<MapPointOfInterestEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting MediaEntity instances in the database.</summary>
		public DataSource<MediaEntity> Media
		{
			get { return new DataSource<MediaEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting MediaCultureEntity instances in the database.</summary>
		public DataSource<MediaCultureEntity> MediaCulture
		{
			get { return new DataSource<MediaCultureEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting MediaLanguageEntity instances in the database.</summary>
		public DataSource<MediaLanguageEntity> MediaLanguage
		{
			get { return new DataSource<MediaLanguageEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting MediaProcessingTaskEntity instances in the database.</summary>
		public DataSource<MediaProcessingTaskEntity> MediaProcessingTask
		{
			get { return new DataSource<MediaProcessingTaskEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting MediaRatioTypeMediaEntity instances in the database.</summary>
		public DataSource<MediaRatioTypeMediaEntity> MediaRatioTypeMedia
		{
			get { return new DataSource<MediaRatioTypeMediaEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting MediaRatioTypeMediaFileEntity instances in the database.</summary>
		public DataSource<MediaRatioTypeMediaFileEntity> MediaRatioTypeMediaFile
		{
			get { return new DataSource<MediaRatioTypeMediaFileEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting MediaRelationshipEntity instances in the database.</summary>
		public DataSource<MediaRelationshipEntity> MediaRelationship
		{
			get { return new DataSource<MediaRelationshipEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting MenuEntity instances in the database.</summary>
		public DataSource<MenuEntity> Menu
		{
			get { return new DataSource<MenuEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting MessageEntity instances in the database.</summary>
		public DataSource<MessageEntity> Message
		{
			get { return new DataSource<MessageEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting MessagegroupEntity instances in the database.</summary>
		public DataSource<MessagegroupEntity> Messagegroup
		{
			get { return new DataSource<MessagegroupEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting MessagegroupDeliverypointEntity instances in the database.</summary>
		public DataSource<MessagegroupDeliverypointEntity> MessagegroupDeliverypoint
		{
			get { return new DataSource<MessagegroupDeliverypointEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting MessageRecipientEntity instances in the database.</summary>
		public DataSource<MessageRecipientEntity> MessageRecipient
		{
			get { return new DataSource<MessageRecipientEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting MessageTemplateEntity instances in the database.</summary>
		public DataSource<MessageTemplateEntity> MessageTemplate
		{
			get { return new DataSource<MessageTemplateEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting MessageTemplateCategoryEntity instances in the database.</summary>
		public DataSource<MessageTemplateCategoryEntity> MessageTemplateCategory
		{
			get { return new DataSource<MessageTemplateCategoryEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting MessageTemplateCategoryMessageTemplateEntity instances in the database.</summary>
		public DataSource<MessageTemplateCategoryMessageTemplateEntity> MessageTemplateCategoryMessageTemplate
		{
			get { return new DataSource<MessageTemplateCategoryMessageTemplateEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting ModuleEntity instances in the database.</summary>
		public DataSource<ModuleEntity> Module
		{
			get { return new DataSource<ModuleEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting NetmessageEntity instances in the database.</summary>
		public DataSource<NetmessageEntity> Netmessage
		{
			get { return new DataSource<NetmessageEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting OptInEntity instances in the database.</summary>
		public DataSource<OptInEntity> OptIn
		{
			get { return new DataSource<OptInEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting OrderEntity instances in the database.</summary>
		public DataSource<OrderEntity> Order
		{
			get { return new DataSource<OrderEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting OrderHourEntity instances in the database.</summary>
		public DataSource<OrderHourEntity> OrderHour
		{
			get { return new DataSource<OrderHourEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting OrderitemEntity instances in the database.</summary>
		public DataSource<OrderitemEntity> Orderitem
		{
			get { return new DataSource<OrderitemEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting OrderitemAlterationitemEntity instances in the database.</summary>
		public DataSource<OrderitemAlterationitemEntity> OrderitemAlterationitem
		{
			get { return new DataSource<OrderitemAlterationitemEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting OrderitemAlterationitemTagEntity instances in the database.</summary>
		public DataSource<OrderitemAlterationitemTagEntity> OrderitemAlterationitemTag
		{
			get { return new DataSource<OrderitemAlterationitemTagEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting OrderitemTagEntity instances in the database.</summary>
		public DataSource<OrderitemTagEntity> OrderitemTag
		{
			get { return new DataSource<OrderitemTagEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting OrderNotificationLogEntity instances in the database.</summary>
		public DataSource<OrderNotificationLogEntity> OrderNotificationLog
		{
			get { return new DataSource<OrderNotificationLogEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting OrderRoutestephandlerEntity instances in the database.</summary>
		public DataSource<OrderRoutestephandlerEntity> OrderRoutestephandler
		{
			get { return new DataSource<OrderRoutestephandlerEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting OrderRoutestephandlerHistoryEntity instances in the database.</summary>
		public DataSource<OrderRoutestephandlerHistoryEntity> OrderRoutestephandlerHistory
		{
			get { return new DataSource<OrderRoutestephandlerHistoryEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting OutletEntity instances in the database.</summary>
		public DataSource<OutletEntity> Outlet
		{
			get { return new DataSource<OutletEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting OutletOperationalStateEntity instances in the database.</summary>
		public DataSource<OutletOperationalStateEntity> OutletOperationalState
		{
			get { return new DataSource<OutletOperationalStateEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting OutletSellerInformationEntity instances in the database.</summary>
		public DataSource<OutletSellerInformationEntity> OutletSellerInformation
		{
			get { return new DataSource<OutletSellerInformationEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting PageEntity instances in the database.</summary>
		public DataSource<PageEntity> Page
		{
			get { return new DataSource<PageEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting PageElementEntity instances in the database.</summary>
		public DataSource<PageElementEntity> PageElement
		{
			get { return new DataSource<PageElementEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting PageLanguageEntity instances in the database.</summary>
		public DataSource<PageLanguageEntity> PageLanguage
		{
			get { return new DataSource<PageLanguageEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting PageTemplateEntity instances in the database.</summary>
		public DataSource<PageTemplateEntity> PageTemplate
		{
			get { return new DataSource<PageTemplateEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting PageTemplateElementEntity instances in the database.</summary>
		public DataSource<PageTemplateElementEntity> PageTemplateElement
		{
			get { return new DataSource<PageTemplateElementEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting PageTemplateLanguageEntity instances in the database.</summary>
		public DataSource<PageTemplateLanguageEntity> PageTemplateLanguage
		{
			get { return new DataSource<PageTemplateLanguageEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting PaymentIntegrationConfigurationEntity instances in the database.</summary>
		public DataSource<PaymentIntegrationConfigurationEntity> PaymentIntegrationConfiguration
		{
			get { return new DataSource<PaymentIntegrationConfigurationEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting PaymentProviderEntity instances in the database.</summary>
		public DataSource<PaymentProviderEntity> PaymentProvider
		{
			get { return new DataSource<PaymentProviderEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting PaymentProviderCompanyEntity instances in the database.</summary>
		public DataSource<PaymentProviderCompanyEntity> PaymentProviderCompany
		{
			get { return new DataSource<PaymentProviderCompanyEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting PaymentTransactionEntity instances in the database.</summary>
		public DataSource<PaymentTransactionEntity> PaymentTransaction
		{
			get { return new DataSource<PaymentTransactionEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting PaymentTransactionLogEntity instances in the database.</summary>
		public DataSource<PaymentTransactionLogEntity> PaymentTransactionLog
		{
			get { return new DataSource<PaymentTransactionLogEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting PaymentTransactionSplitEntity instances in the database.</summary>
		public DataSource<PaymentTransactionSplitEntity> PaymentTransactionSplit
		{
			get { return new DataSource<PaymentTransactionSplitEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting PmsActionRuleEntity instances in the database.</summary>
		public DataSource<PmsActionRuleEntity> PmsActionRule
		{
			get { return new DataSource<PmsActionRuleEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting PmsReportColumnEntity instances in the database.</summary>
		public DataSource<PmsReportColumnEntity> PmsReportColumn
		{
			get { return new DataSource<PmsReportColumnEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting PmsReportConfigurationEntity instances in the database.</summary>
		public DataSource<PmsReportConfigurationEntity> PmsReportConfiguration
		{
			get { return new DataSource<PmsReportConfigurationEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting PmsRuleEntity instances in the database.</summary>
		public DataSource<PmsRuleEntity> PmsRule
		{
			get { return new DataSource<PmsRuleEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting PointOfInterestEntity instances in the database.</summary>
		public DataSource<PointOfInterestEntity> PointOfInterest
		{
			get { return new DataSource<PointOfInterestEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting PointOfInterestAmenityEntity instances in the database.</summary>
		public DataSource<PointOfInterestAmenityEntity> PointOfInterestAmenity
		{
			get { return new DataSource<PointOfInterestAmenityEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting PointOfInterestLanguageEntity instances in the database.</summary>
		public DataSource<PointOfInterestLanguageEntity> PointOfInterestLanguage
		{
			get { return new DataSource<PointOfInterestLanguageEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting PointOfInterestVenueCategoryEntity instances in the database.</summary>
		public DataSource<PointOfInterestVenueCategoryEntity> PointOfInterestVenueCategory
		{
			get { return new DataSource<PointOfInterestVenueCategoryEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting PosalterationEntity instances in the database.</summary>
		public DataSource<PosalterationEntity> Posalteration
		{
			get { return new DataSource<PosalterationEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting PosalterationitemEntity instances in the database.</summary>
		public DataSource<PosalterationitemEntity> Posalterationitem
		{
			get { return new DataSource<PosalterationitemEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting PosalterationoptionEntity instances in the database.</summary>
		public DataSource<PosalterationoptionEntity> Posalterationoption
		{
			get { return new DataSource<PosalterationoptionEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting PoscategoryEntity instances in the database.</summary>
		public DataSource<PoscategoryEntity> Poscategory
		{
			get { return new DataSource<PoscategoryEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting PosdeliverypointEntity instances in the database.</summary>
		public DataSource<PosdeliverypointEntity> Posdeliverypoint
		{
			get { return new DataSource<PosdeliverypointEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting PosdeliverypointgroupEntity instances in the database.</summary>
		public DataSource<PosdeliverypointgroupEntity> Posdeliverypointgroup
		{
			get { return new DataSource<PosdeliverypointgroupEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting PospaymentmethodEntity instances in the database.</summary>
		public DataSource<PospaymentmethodEntity> Pospaymentmethod
		{
			get { return new DataSource<PospaymentmethodEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting PosproductEntity instances in the database.</summary>
		public DataSource<PosproductEntity> Posproduct
		{
			get { return new DataSource<PosproductEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting PosproductPosalterationEntity instances in the database.</summary>
		public DataSource<PosproductPosalterationEntity> PosproductPosalteration
		{
			get { return new DataSource<PosproductPosalterationEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting PriceLevelEntity instances in the database.</summary>
		public DataSource<PriceLevelEntity> PriceLevel
		{
			get { return new DataSource<PriceLevelEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting PriceLevelItemEntity instances in the database.</summary>
		public DataSource<PriceLevelItemEntity> PriceLevelItem
		{
			get { return new DataSource<PriceLevelItemEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting PriceScheduleEntity instances in the database.</summary>
		public DataSource<PriceScheduleEntity> PriceSchedule
		{
			get { return new DataSource<PriceScheduleEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting PriceScheduleItemEntity instances in the database.</summary>
		public DataSource<PriceScheduleItemEntity> PriceScheduleItem
		{
			get { return new DataSource<PriceScheduleItemEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting PriceScheduleItemOccurrenceEntity instances in the database.</summary>
		public DataSource<PriceScheduleItemOccurrenceEntity> PriceScheduleItemOccurrence
		{
			get { return new DataSource<PriceScheduleItemOccurrenceEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting ProductEntity instances in the database.</summary>
		public DataSource<ProductEntity> Product
		{
			get { return new DataSource<ProductEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting ProductAlterationEntity instances in the database.</summary>
		public DataSource<ProductAlterationEntity> ProductAlteration
		{
			get { return new DataSource<ProductAlterationEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting ProductAttachmentEntity instances in the database.</summary>
		public DataSource<ProductAttachmentEntity> ProductAttachment
		{
			get { return new DataSource<ProductAttachmentEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting ProductCategoryEntity instances in the database.</summary>
		public DataSource<ProductCategoryEntity> ProductCategory
		{
			get { return new DataSource<ProductCategoryEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting ProductCategorySuggestionEntity instances in the database.</summary>
		public DataSource<ProductCategorySuggestionEntity> ProductCategorySuggestion
		{
			get { return new DataSource<ProductCategorySuggestionEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting ProductCategoryTagEntity instances in the database.</summary>
		public DataSource<ProductCategoryTagEntity> ProductCategoryTag
		{
			get { return new DataSource<ProductCategoryTagEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting ProductgroupEntity instances in the database.</summary>
		public DataSource<ProductgroupEntity> Productgroup
		{
			get { return new DataSource<ProductgroupEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting ProductgroupItemEntity instances in the database.</summary>
		public DataSource<ProductgroupItemEntity> ProductgroupItem
		{
			get { return new DataSource<ProductgroupItemEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting ProductLanguageEntity instances in the database.</summary>
		public DataSource<ProductLanguageEntity> ProductLanguage
		{
			get { return new DataSource<ProductLanguageEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting ProductSuggestionEntity instances in the database.</summary>
		public DataSource<ProductSuggestionEntity> ProductSuggestion
		{
			get { return new DataSource<ProductSuggestionEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting ProductTagEntity instances in the database.</summary>
		public DataSource<ProductTagEntity> ProductTag
		{
			get { return new DataSource<ProductTagEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting PublishingEntity instances in the database.</summary>
		public DataSource<PublishingEntity> Publishing
		{
			get { return new DataSource<PublishingEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting PublishingItemEntity instances in the database.</summary>
		public DataSource<PublishingItemEntity> PublishingItem
		{
			get { return new DataSource<PublishingItemEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting RatingEntity instances in the database.</summary>
		public DataSource<RatingEntity> Rating
		{
			get { return new DataSource<RatingEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting ReceiptEntity instances in the database.</summary>
		public DataSource<ReceiptEntity> Receipt
		{
			get { return new DataSource<ReceiptEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting ReceiptTemplateEntity instances in the database.</summary>
		public DataSource<ReceiptTemplateEntity> ReceiptTemplate
		{
			get { return new DataSource<ReceiptTemplateEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting ReferentialConstraintEntity instances in the database.</summary>
		public DataSource<ReferentialConstraintEntity> ReferentialConstraint
		{
			get { return new DataSource<ReferentialConstraintEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting ReferentialConstraintCustomEntity instances in the database.</summary>
		public DataSource<ReferentialConstraintCustomEntity> ReferentialConstraintCustom
		{
			get { return new DataSource<ReferentialConstraintCustomEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting ReleaseEntity instances in the database.</summary>
		public DataSource<ReleaseEntity> Release
		{
			get { return new DataSource<ReleaseEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting ReportProcessingTaskEntity instances in the database.</summary>
		public DataSource<ReportProcessingTaskEntity> ReportProcessingTask
		{
			get { return new DataSource<ReportProcessingTaskEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting ReportProcessingTaskFileEntity instances in the database.</summary>
		public DataSource<ReportProcessingTaskFileEntity> ReportProcessingTaskFile
		{
			get { return new DataSource<ReportProcessingTaskFileEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting ReportProcessingTaskTemplateEntity instances in the database.</summary>
		public DataSource<ReportProcessingTaskTemplateEntity> ReportProcessingTaskTemplate
		{
			get { return new DataSource<ReportProcessingTaskTemplateEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting RequestlogEntity instances in the database.</summary>
		public DataSource<RequestlogEntity> Requestlog
		{
			get { return new DataSource<RequestlogEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting RoleEntity instances in the database.</summary>
		public DataSource<RoleEntity> Role
		{
			get { return new DataSource<RoleEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting RoleModuleRightsEntity instances in the database.</summary>
		public DataSource<RoleModuleRightsEntity> RoleModuleRights
		{
			get { return new DataSource<RoleModuleRightsEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting RoleUIElementRightsEntity instances in the database.</summary>
		public DataSource<RoleUIElementRightsEntity> RoleUIElementRights
		{
			get { return new DataSource<RoleUIElementRightsEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting RoleUIElementSubPanelRightsEntity instances in the database.</summary>
		public DataSource<RoleUIElementSubPanelRightsEntity> RoleUIElementSubPanelRights
		{
			get { return new DataSource<RoleUIElementSubPanelRightsEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting RoomControlAreaEntity instances in the database.</summary>
		public DataSource<RoomControlAreaEntity> RoomControlArea
		{
			get { return new DataSource<RoomControlAreaEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting RoomControlAreaLanguageEntity instances in the database.</summary>
		public DataSource<RoomControlAreaLanguageEntity> RoomControlAreaLanguage
		{
			get { return new DataSource<RoomControlAreaLanguageEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting RoomControlComponentEntity instances in the database.</summary>
		public DataSource<RoomControlComponentEntity> RoomControlComponent
		{
			get { return new DataSource<RoomControlComponentEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting RoomControlComponentLanguageEntity instances in the database.</summary>
		public DataSource<RoomControlComponentLanguageEntity> RoomControlComponentLanguage
		{
			get { return new DataSource<RoomControlComponentLanguageEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting RoomControlConfigurationEntity instances in the database.</summary>
		public DataSource<RoomControlConfigurationEntity> RoomControlConfiguration
		{
			get { return new DataSource<RoomControlConfigurationEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting RoomControlSectionEntity instances in the database.</summary>
		public DataSource<RoomControlSectionEntity> RoomControlSection
		{
			get { return new DataSource<RoomControlSectionEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting RoomControlSectionItemEntity instances in the database.</summary>
		public DataSource<RoomControlSectionItemEntity> RoomControlSectionItem
		{
			get { return new DataSource<RoomControlSectionItemEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting RoomControlSectionItemLanguageEntity instances in the database.</summary>
		public DataSource<RoomControlSectionItemLanguageEntity> RoomControlSectionItemLanguage
		{
			get { return new DataSource<RoomControlSectionItemLanguageEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting RoomControlSectionLanguageEntity instances in the database.</summary>
		public DataSource<RoomControlSectionLanguageEntity> RoomControlSectionLanguage
		{
			get { return new DataSource<RoomControlSectionLanguageEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting RoomControlWidgetEntity instances in the database.</summary>
		public DataSource<RoomControlWidgetEntity> RoomControlWidget
		{
			get { return new DataSource<RoomControlWidgetEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting RoomControlWidgetLanguageEntity instances in the database.</summary>
		public DataSource<RoomControlWidgetLanguageEntity> RoomControlWidgetLanguage
		{
			get { return new DataSource<RoomControlWidgetLanguageEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting RouteEntity instances in the database.</summary>
		public DataSource<RouteEntity> Route
		{
			get { return new DataSource<RouteEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting RoutestepEntity instances in the database.</summary>
		public DataSource<RoutestepEntity> Routestep
		{
			get { return new DataSource<RoutestepEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting RoutestephandlerEntity instances in the database.</summary>
		public DataSource<RoutestephandlerEntity> Routestephandler
		{
			get { return new DataSource<RoutestephandlerEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting ScheduleEntity instances in the database.</summary>
		public DataSource<ScheduleEntity> Schedule
		{
			get { return new DataSource<ScheduleEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting ScheduledCommandEntity instances in the database.</summary>
		public DataSource<ScheduledCommandEntity> ScheduledCommand
		{
			get { return new DataSource<ScheduledCommandEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting ScheduledCommandTaskEntity instances in the database.</summary>
		public DataSource<ScheduledCommandTaskEntity> ScheduledCommandTask
		{
			get { return new DataSource<ScheduledCommandTaskEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting ScheduledMessageEntity instances in the database.</summary>
		public DataSource<ScheduledMessageEntity> ScheduledMessage
		{
			get { return new DataSource<ScheduledMessageEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting ScheduledMessageHistoryEntity instances in the database.</summary>
		public DataSource<ScheduledMessageHistoryEntity> ScheduledMessageHistory
		{
			get { return new DataSource<ScheduledMessageHistoryEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting ScheduledMessageLanguageEntity instances in the database.</summary>
		public DataSource<ScheduledMessageLanguageEntity> ScheduledMessageLanguage
		{
			get { return new DataSource<ScheduledMessageLanguageEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting ScheduleitemEntity instances in the database.</summary>
		public DataSource<ScheduleitemEntity> Scheduleitem
		{
			get { return new DataSource<ScheduleitemEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting ServiceMethodEntity instances in the database.</summary>
		public DataSource<ServiceMethodEntity> ServiceMethod
		{
			get { return new DataSource<ServiceMethodEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting ServiceMethodDeliverypointgroupEntity instances in the database.</summary>
		public DataSource<ServiceMethodDeliverypointgroupEntity> ServiceMethodDeliverypointgroup
		{
			get { return new DataSource<ServiceMethodDeliverypointgroupEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting SetupCodeEntity instances in the database.</summary>
		public DataSource<SetupCodeEntity> SetupCode
		{
			get { return new DataSource<SetupCodeEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting SiteEntity instances in the database.</summary>
		public DataSource<SiteEntity> Site
		{
			get { return new DataSource<SiteEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting SiteCultureEntity instances in the database.</summary>
		public DataSource<SiteCultureEntity> SiteCulture
		{
			get { return new DataSource<SiteCultureEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting SiteLanguageEntity instances in the database.</summary>
		public DataSource<SiteLanguageEntity> SiteLanguage
		{
			get { return new DataSource<SiteLanguageEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting SiteTemplateEntity instances in the database.</summary>
		public DataSource<SiteTemplateEntity> SiteTemplate
		{
			get { return new DataSource<SiteTemplateEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting SiteTemplateCultureEntity instances in the database.</summary>
		public DataSource<SiteTemplateCultureEntity> SiteTemplateCulture
		{
			get { return new DataSource<SiteTemplateCultureEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting SiteTemplateLanguageEntity instances in the database.</summary>
		public DataSource<SiteTemplateLanguageEntity> SiteTemplateLanguage
		{
			get { return new DataSource<SiteTemplateLanguageEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting SmsInformationEntity instances in the database.</summary>
		public DataSource<SmsInformationEntity> SmsInformation
		{
			get { return new DataSource<SmsInformationEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting SmsKeywordEntity instances in the database.</summary>
		public DataSource<SmsKeywordEntity> SmsKeyword
		{
			get { return new DataSource<SmsKeywordEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting StationEntity instances in the database.</summary>
		public DataSource<StationEntity> Station
		{
			get { return new DataSource<StationEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting StationLanguageEntity instances in the database.</summary>
		public DataSource<StationLanguageEntity> StationLanguage
		{
			get { return new DataSource<StationLanguageEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting StationListEntity instances in the database.</summary>
		public DataSource<StationListEntity> StationList
		{
			get { return new DataSource<StationListEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting SupplierEntity instances in the database.</summary>
		public DataSource<SupplierEntity> Supplier
		{
			get { return new DataSource<SupplierEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting SupportagentEntity instances in the database.</summary>
		public DataSource<SupportagentEntity> Supportagent
		{
			get { return new DataSource<SupportagentEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting SupportpoolEntity instances in the database.</summary>
		public DataSource<SupportpoolEntity> Supportpool
		{
			get { return new DataSource<SupportpoolEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting SupportpoolNotificationRecipientEntity instances in the database.</summary>
		public DataSource<SupportpoolNotificationRecipientEntity> SupportpoolNotificationRecipient
		{
			get { return new DataSource<SupportpoolNotificationRecipientEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting SupportpoolSupportagentEntity instances in the database.</summary>
		public DataSource<SupportpoolSupportagentEntity> SupportpoolSupportagent
		{
			get { return new DataSource<SupportpoolSupportagentEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting SurveyEntity instances in the database.</summary>
		public DataSource<SurveyEntity> Survey
		{
			get { return new DataSource<SurveyEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting SurveyAnswerEntity instances in the database.</summary>
		public DataSource<SurveyAnswerEntity> SurveyAnswer
		{
			get { return new DataSource<SurveyAnswerEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting SurveyAnswerLanguageEntity instances in the database.</summary>
		public DataSource<SurveyAnswerLanguageEntity> SurveyAnswerLanguage
		{
			get { return new DataSource<SurveyAnswerLanguageEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting SurveyLanguageEntity instances in the database.</summary>
		public DataSource<SurveyLanguageEntity> SurveyLanguage
		{
			get { return new DataSource<SurveyLanguageEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting SurveyPageEntity instances in the database.</summary>
		public DataSource<SurveyPageEntity> SurveyPage
		{
			get { return new DataSource<SurveyPageEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting SurveyQuestionEntity instances in the database.</summary>
		public DataSource<SurveyQuestionEntity> SurveyQuestion
		{
			get { return new DataSource<SurveyQuestionEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting SurveyQuestionLanguageEntity instances in the database.</summary>
		public DataSource<SurveyQuestionLanguageEntity> SurveyQuestionLanguage
		{
			get { return new DataSource<SurveyQuestionLanguageEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting SurveyResultEntity instances in the database.</summary>
		public DataSource<SurveyResultEntity> SurveyResult
		{
			get { return new DataSource<SurveyResultEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting TagEntity instances in the database.</summary>
		public DataSource<TagEntity> Tag
		{
			get { return new DataSource<TagEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting TaxTariffEntity instances in the database.</summary>
		public DataSource<TaxTariffEntity> TaxTariff
		{
			get { return new DataSource<TaxTariffEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting TerminalEntity instances in the database.</summary>
		public DataSource<TerminalEntity> Terminal
		{
			get { return new DataSource<TerminalEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting TerminalConfigurationEntity instances in the database.</summary>
		public DataSource<TerminalConfigurationEntity> TerminalConfiguration
		{
			get { return new DataSource<TerminalConfigurationEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting TerminalLogEntity instances in the database.</summary>
		public DataSource<TerminalLogEntity> TerminalLog
		{
			get { return new DataSource<TerminalLogEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting TerminalLogFileEntity instances in the database.</summary>
		public DataSource<TerminalLogFileEntity> TerminalLogFile
		{
			get { return new DataSource<TerminalLogFileEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting TerminalMessageTemplateCategoryEntity instances in the database.</summary>
		public DataSource<TerminalMessageTemplateCategoryEntity> TerminalMessageTemplateCategory
		{
			get { return new DataSource<TerminalMessageTemplateCategoryEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting TerminalStateEntity instances in the database.</summary>
		public DataSource<TerminalStateEntity> TerminalState
		{
			get { return new DataSource<TerminalStateEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting TimestampEntity instances in the database.</summary>
		public DataSource<TimestampEntity> Timestamp
		{
			get { return new DataSource<TimestampEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting TimeZoneEntity instances in the database.</summary>
		public DataSource<TimeZoneEntity> TimeZone
		{
			get { return new DataSource<TimeZoneEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting TranslationEntity instances in the database.</summary>
		public DataSource<TranslationEntity> Translation
		{
			get { return new DataSource<TranslationEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting UIElementEntity instances in the database.</summary>
		public DataSource<UIElementEntity> UIElement
		{
			get { return new DataSource<UIElementEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting UIElementCustomEntity instances in the database.</summary>
		public DataSource<UIElementCustomEntity> UIElementCustom
		{
			get { return new DataSource<UIElementCustomEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting UIElementSubPanelEntity instances in the database.</summary>
		public DataSource<UIElementSubPanelEntity> UIElementSubPanel
		{
			get { return new DataSource<UIElementSubPanelEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting UIElementSubPanelCustomEntity instances in the database.</summary>
		public DataSource<UIElementSubPanelCustomEntity> UIElementSubPanelCustom
		{
			get { return new DataSource<UIElementSubPanelCustomEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting UIElementSubPanelUIElementEntity instances in the database.</summary>
		public DataSource<UIElementSubPanelUIElementEntity> UIElementSubPanelUIElement
		{
			get { return new DataSource<UIElementSubPanelUIElementEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting UIFooterItemEntity instances in the database.</summary>
		public DataSource<UIFooterItemEntity> UIFooterItem
		{
			get { return new DataSource<UIFooterItemEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting UIFooterItemLanguageEntity instances in the database.</summary>
		public DataSource<UIFooterItemLanguageEntity> UIFooterItemLanguage
		{
			get { return new DataSource<UIFooterItemLanguageEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting UIModeEntity instances in the database.</summary>
		public DataSource<UIModeEntity> UIMode
		{
			get { return new DataSource<UIModeEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting UIScheduleEntity instances in the database.</summary>
		public DataSource<UIScheduleEntity> UISchedule
		{
			get { return new DataSource<UIScheduleEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting UIScheduleItemEntity instances in the database.</summary>
		public DataSource<UIScheduleItemEntity> UIScheduleItem
		{
			get { return new DataSource<UIScheduleItemEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting UIScheduleItemOccurrenceEntity instances in the database.</summary>
		public DataSource<UIScheduleItemOccurrenceEntity> UIScheduleItemOccurrence
		{
			get { return new DataSource<UIScheduleItemOccurrenceEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting UITabEntity instances in the database.</summary>
		public DataSource<UITabEntity> UITab
		{
			get { return new DataSource<UITabEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting UITabLanguageEntity instances in the database.</summary>
		public DataSource<UITabLanguageEntity> UITabLanguage
		{
			get { return new DataSource<UITabLanguageEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting UIThemeEntity instances in the database.</summary>
		public DataSource<UIThemeEntity> UITheme
		{
			get { return new DataSource<UIThemeEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting UIThemeColorEntity instances in the database.</summary>
		public DataSource<UIThemeColorEntity> UIThemeColor
		{
			get { return new DataSource<UIThemeColorEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting UIThemeTextSizeEntity instances in the database.</summary>
		public DataSource<UIThemeTextSizeEntity> UIThemeTextSize
		{
			get { return new DataSource<UIThemeTextSizeEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting UIWidgetEntity instances in the database.</summary>
		public DataSource<UIWidgetEntity> UIWidget
		{
			get { return new DataSource<UIWidgetEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting UIWidgetAvailabilityEntity instances in the database.</summary>
		public DataSource<UIWidgetAvailabilityEntity> UIWidgetAvailability
		{
			get { return new DataSource<UIWidgetAvailabilityEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting UIWidgetLanguageEntity instances in the database.</summary>
		public DataSource<UIWidgetLanguageEntity> UIWidgetLanguage
		{
			get { return new DataSource<UIWidgetLanguageEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting UIWidgetTimerEntity instances in the database.</summary>
		public DataSource<UIWidgetTimerEntity> UIWidgetTimer
		{
			get { return new DataSource<UIWidgetTimerEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting UserEntity instances in the database.</summary>
		public DataSource<UserEntity> User
		{
			get { return new DataSource<UserEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting UserBrandEntity instances in the database.</summary>
		public DataSource<UserBrandEntity> UserBrand
		{
			get { return new DataSource<UserBrandEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting UserLogonEntity instances in the database.</summary>
		public DataSource<UserLogonEntity> UserLogon
		{
			get { return new DataSource<UserLogonEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting UserRoleEntity instances in the database.</summary>
		public DataSource<UserRoleEntity> UserRole
		{
			get { return new DataSource<UserRoleEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting VattariffEntity instances in the database.</summary>
		public DataSource<VattariffEntity> Vattariff
		{
			get { return new DataSource<VattariffEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting VendorEntity instances in the database.</summary>
		public DataSource<VendorEntity> Vendor
		{
			get { return new DataSource<VendorEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting VenueCategoryEntity instances in the database.</summary>
		public DataSource<VenueCategoryEntity> VenueCategory
		{
			get { return new DataSource<VenueCategoryEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting VenueCategoryLanguageEntity instances in the database.</summary>
		public DataSource<VenueCategoryLanguageEntity> VenueCategoryLanguage
		{
			get { return new DataSource<VenueCategoryLanguageEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting ViewEntity instances in the database.</summary>
		public DataSource<ViewEntity> View
		{
			get { return new DataSource<ViewEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting ViewCustomEntity instances in the database.</summary>
		public DataSource<ViewCustomEntity> ViewCustom
		{
			get { return new DataSource<ViewCustomEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting ViewItemEntity instances in the database.</summary>
		public DataSource<ViewItemEntity> ViewItem
		{
			get { return new DataSource<ViewItemEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting ViewItemCustomEntity instances in the database.</summary>
		public DataSource<ViewItemCustomEntity> ViewItemCustom
		{
			get { return new DataSource<ViewItemCustomEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting WeatherEntity instances in the database.</summary>
		public DataSource<WeatherEntity> Weather
		{
			get { return new DataSource<WeatherEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting WifiConfigurationEntity instances in the database.</summary>
		public DataSource<WifiConfigurationEntity> WifiConfiguration
		{
			get { return new DataSource<WifiConfigurationEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting ActionEntity instances in the database.</summary>
		public DataSource<ActionEntity> Action
		{
			get { return new DataSource<ActionEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting ApplicationConfigurationEntity instances in the database.</summary>
		public DataSource<ApplicationConfigurationEntity> ApplicationConfiguration
		{
			get { return new DataSource<ApplicationConfigurationEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting CarouselItemEntity instances in the database.</summary>
		public DataSource<CarouselItemEntity> CarouselItem
		{
			get { return new DataSource<CarouselItemEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting FeatureFlagEntity instances in the database.</summary>
		public DataSource<FeatureFlagEntity> FeatureFlag
		{
			get { return new DataSource<FeatureFlagEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting LandingPageEntity instances in the database.</summary>
		public DataSource<LandingPageEntity> LandingPage
		{
			get { return new DataSource<LandingPageEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting LandingPageWidgetEntity instances in the database.</summary>
		public DataSource<LandingPageWidgetEntity> LandingPageWidget
		{
			get { return new DataSource<LandingPageWidgetEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting NavigationMenuEntity instances in the database.</summary>
		public DataSource<NavigationMenuEntity> NavigationMenu
		{
			get { return new DataSource<NavigationMenuEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting NavigationMenuItemEntity instances in the database.</summary>
		public DataSource<NavigationMenuItemEntity> NavigationMenuItem
		{
			get { return new DataSource<NavigationMenuItemEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting NavigationMenuWidgetEntity instances in the database.</summary>
		public DataSource<NavigationMenuWidgetEntity> NavigationMenuWidget
		{
			get { return new DataSource<NavigationMenuWidgetEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting ThemeEntity instances in the database.</summary>
		public DataSource<ThemeEntity> Theme
		{
			get { return new DataSource<ThemeEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting WidgetEntity instances in the database.</summary>
		public DataSource<WidgetEntity> Widget
		{
			get { return new DataSource<WidgetEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting WidgetActionBannerEntity instances in the database.</summary>
		public DataSource<WidgetActionBannerEntity> WidgetActionBanner
		{
			get { return new DataSource<WidgetActionBannerEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting WidgetActionButtonEntity instances in the database.</summary>
		public DataSource<WidgetActionButtonEntity> WidgetActionButton
		{
			get { return new DataSource<WidgetActionButtonEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting WidgetCarouselEntity instances in the database.</summary>
		public DataSource<WidgetCarouselEntity> WidgetCarousel
		{
			get { return new DataSource<WidgetCarouselEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting WidgetGroupEntity instances in the database.</summary>
		public DataSource<WidgetGroupEntity> WidgetGroup
		{
			get { return new DataSource<WidgetGroupEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting WidgetGroupWidgetEntity instances in the database.</summary>
		public DataSource<WidgetGroupWidgetEntity> WidgetGroupWidget
		{
			get { return new DataSource<WidgetGroupWidgetEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting WidgetHeroEntity instances in the database.</summary>
		public DataSource<WidgetHeroEntity> WidgetHero
		{
			get { return new DataSource<WidgetHeroEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting WidgetLanguageSwitcherEntity instances in the database.</summary>
		public DataSource<WidgetLanguageSwitcherEntity> WidgetLanguageSwitcher
		{
			get { return new DataSource<WidgetLanguageSwitcherEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting WidgetMarkdownEntity instances in the database.</summary>
		public DataSource<WidgetMarkdownEntity> WidgetMarkdown
		{
			get { return new DataSource<WidgetMarkdownEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting WidgetOpeningTimeEntity instances in the database.</summary>
		public DataSource<WidgetOpeningTimeEntity> WidgetOpeningTime
		{
			get { return new DataSource<WidgetOpeningTimeEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting WidgetPageTitleEntity instances in the database.</summary>
		public DataSource<WidgetPageTitleEntity> WidgetPageTitle
		{
			get { return new DataSource<WidgetPageTitleEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
		/// <summary>returns the datasource to use in a Linq query when targeting WidgetWaitTimeEntity instances in the database.</summary>
		public DataSource<WidgetWaitTimeEntity> WidgetWaitTime
		{
			get { return new DataSource<WidgetWaitTimeEntity>(_transactionToUse, new ElementCreator(), _customFunctionMappings, _contextToUse); }
		}
 
		#region Class Property Declarations
		/// <summary> Gets / sets the ITransaction to use for the queries created with this meta data object.</summary>
		/// <remarks> Be aware that the ITransaction object set via this property is kept alive by the LLBLGenProQuery objects created with this meta data
		/// till they go out of scope.</remarks>
		public ITransaction TransactionToUse
		{
			get { return _transactionToUse;}
			set { _transactionToUse = value;}
		}

		/// <summary>Gets or sets the custom function mappings to use. These take higher precedence than the ones in the DQE to use</summary>
		public FunctionMappingStore CustomFunctionMappings
		{
			get { return _customFunctionMappings; }
			set { _customFunctionMappings = value; }
		}
		
		/// <summary>Gets or sets the Context instance to use for entity fetches.</summary>
		public Context ContextToUse
		{
			get { return _contextToUse;}
			set { _contextToUse = value;}
		}
		#endregion
	}
}