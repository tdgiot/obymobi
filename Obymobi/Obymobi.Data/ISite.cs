﻿using SD.LLBLGen.Pro.ORMSupportClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Obymobi.Data.EntityClasses;
using Obymobi.Logic.Cms;

namespace Obymobi.Data
{    
    public interface ISite : IEntity
    {
        int PrimaryId { get; set; }
        string Name { get; set; }
        List<Obymobi.Logic.Cms.IPage> IPageCollection { get; }
        SiteType SiteTypeAsEnum { get; set; }    
    }
}
