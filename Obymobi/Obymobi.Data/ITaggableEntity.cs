﻿using Obymobi.Data.EntityClasses;

namespace Obymobi.Data
{
    public interface ITaggableEntity
    {
        int TagId { get; set; }
        TagEntity TagEntity { get; set; }
    }
}
