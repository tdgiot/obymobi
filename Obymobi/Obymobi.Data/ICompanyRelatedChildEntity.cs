﻿using Obymobi.Data.EntityClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.Data
{
    public interface ICompanyRelatedChildEntity
    {
        int     ParentCompanyId { get; set; }

        CommonEntityBase Parent { get; } 
    }
}
