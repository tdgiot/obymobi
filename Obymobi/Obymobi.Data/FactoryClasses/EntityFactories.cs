﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections.Generic;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.RelationClasses;
using Obymobi.Data.DaoClasses;

using Obymobi.Data.EntityClasses;
using Obymobi.Data.CollectionClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.FactoryClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>general base class for the generated factories</summary>
	[Serializable]
	public partial class EntityFactoryBase : EntityFactoryCore
	{
		private readonly Obymobi.Data.EntityType _typeOfEntity;
		
		/// <summary>CTor</summary>
		/// <param name="entityName">Name of the entity.</param>
		/// <param name="typeOfEntity">The type of entity.</param>
		public EntityFactoryBase(string entityName, Obymobi.Data.EntityType typeOfEntity) : base(entityName)
		{
			_typeOfEntity = typeOfEntity;
		}

		/// <summary>Creates a new entity instance using the GeneralEntityFactory in the generated code, using the passed in entitytype value</summary>
		/// <param name="entityTypeValue">The entity type value of the entity to create an instance for.</param>
		/// <returns>new IEntity instance</returns>
		public override IEntity CreateEntityFromEntityTypeValue(int entityTypeValue)
		{
			return GeneralEntityFactory.Create((Obymobi.Data.EntityType)entityTypeValue);
		}
		
		/// <summary>Creates, using the generated EntityFieldsFactory, the IEntityFields object for the entity to create. </summary>
		/// <returns>Empty IEntityFields object.</returns>
		public override IEntityFields CreateFields()
		{
			return EntityFieldsFactory.CreateEntityFieldsObject(_typeOfEntity);
		}

		/// <summary>Creates the relations collection to the entity to join all targets so this entity can be fetched. </summary>
		/// <param name="objectAlias">The object alias to use for the elements in the relations.</param>
		/// <returns>null if the entity isn't in a hierarchy of type TargetPerEntity, otherwise the relations collection needed to join all targets together to fetch all subtypes of this entity and this entity itself</returns>
		public override IRelationCollection CreateHierarchyRelations(string objectAlias) 
		{
			return InheritanceInfoProviderSingleton.GetInstance().GetHierarchyRelations(ForEntityName, objectAlias);
		}

		/// <summary>This method retrieves, using the InheritanceInfoprovider, the factory for the entity represented by the values passed in.</summary>
		/// <param name="fieldValues">Field values read from the db, to determine which factory to return, based on the field values passed in.</param>
		/// <param name="entityFieldStartIndexesPerEntity">indexes into values where per entity type their own fields start.</param>
		/// <returns>the factory for the entity which is represented by the values passed in.</returns>
		public override IEntityFactory GetEntityFactory(object[] fieldValues, Dictionary<string, int> entityFieldStartIndexesPerEntity)
		{
			return (IEntityFactory)InheritanceInfoProviderSingleton.GetInstance().GetEntityFactory(ForEntityName, fieldValues, entityFieldStartIndexesPerEntity) ?? this;
		}
						
		/// <summary>Creates a new entity collection for the entity of this factory.</summary>
		/// <returns>ready to use new entity collection, typed.</returns>
		public override IEntityCollection CreateEntityCollection()
		{
			return GeneralEntityCollectionFactory.Create(_typeOfEntity);
		}
	}
	
	/// <summary>Factory to create new, empty AccessCodeEntity objects.</summary>
	[Serializable]
	public partial class AccessCodeEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public AccessCodeEntityFactory() : base("AccessCodeEntity", Obymobi.Data.EntityType.AccessCodeEntity) { }

		/// <summary>Creates a new, empty AccessCodeEntity object.</summary>
		/// <returns>A new, empty AccessCodeEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new AccessCodeEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewAccessCode
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty AccessCodeCompanyEntity objects.</summary>
	[Serializable]
	public partial class AccessCodeCompanyEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public AccessCodeCompanyEntityFactory() : base("AccessCodeCompanyEntity", Obymobi.Data.EntityType.AccessCodeCompanyEntity) { }

		/// <summary>Creates a new, empty AccessCodeCompanyEntity object.</summary>
		/// <returns>A new, empty AccessCodeCompanyEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new AccessCodeCompanyEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewAccessCodeCompany
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty AccessCodePointOfInterestEntity objects.</summary>
	[Serializable]
	public partial class AccessCodePointOfInterestEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public AccessCodePointOfInterestEntityFactory() : base("AccessCodePointOfInterestEntity", Obymobi.Data.EntityType.AccessCodePointOfInterestEntity) { }

		/// <summary>Creates a new, empty AccessCodePointOfInterestEntity object.</summary>
		/// <returns>A new, empty AccessCodePointOfInterestEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new AccessCodePointOfInterestEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewAccessCodePointOfInterest
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty AccountEntity objects.</summary>
	[Serializable]
	public partial class AccountEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public AccountEntityFactory() : base("AccountEntity", Obymobi.Data.EntityType.AccountEntity) { }

		/// <summary>Creates a new, empty AccountEntity object.</summary>
		/// <returns>A new, empty AccountEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new AccountEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewAccount
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty AccountCompanyEntity objects.</summary>
	[Serializable]
	public partial class AccountCompanyEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public AccountCompanyEntityFactory() : base("AccountCompanyEntity", Obymobi.Data.EntityType.AccountCompanyEntity) { }

		/// <summary>Creates a new, empty AccountCompanyEntity object.</summary>
		/// <returns>A new, empty AccountCompanyEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new AccountCompanyEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewAccountCompany
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty ActionButtonEntity objects.</summary>
	[Serializable]
	public partial class ActionButtonEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public ActionButtonEntityFactory() : base("ActionButtonEntity", Obymobi.Data.EntityType.ActionButtonEntity) { }

		/// <summary>Creates a new, empty ActionButtonEntity object.</summary>
		/// <returns>A new, empty ActionButtonEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new ActionButtonEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewActionButton
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty ActionButtonLanguageEntity objects.</summary>
	[Serializable]
	public partial class ActionButtonLanguageEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public ActionButtonLanguageEntityFactory() : base("ActionButtonLanguageEntity", Obymobi.Data.EntityType.ActionButtonLanguageEntity) { }

		/// <summary>Creates a new, empty ActionButtonLanguageEntity object.</summary>
		/// <returns>A new, empty ActionButtonLanguageEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new ActionButtonLanguageEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewActionButtonLanguage
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty AddressEntity objects.</summary>
	[Serializable]
	public partial class AddressEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public AddressEntityFactory() : base("AddressEntity", Obymobi.Data.EntityType.AddressEntity) { }

		/// <summary>Creates a new, empty AddressEntity object.</summary>
		/// <returns>A new, empty AddressEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new AddressEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewAddress
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty AdvertisementEntity objects.</summary>
	[Serializable]
	public partial class AdvertisementEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public AdvertisementEntityFactory() : base("AdvertisementEntity", Obymobi.Data.EntityType.AdvertisementEntity) { }

		/// <summary>Creates a new, empty AdvertisementEntity object.</summary>
		/// <returns>A new, empty AdvertisementEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new AdvertisementEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewAdvertisement
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty AdvertisementConfigurationEntity objects.</summary>
	[Serializable]
	public partial class AdvertisementConfigurationEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public AdvertisementConfigurationEntityFactory() : base("AdvertisementConfigurationEntity", Obymobi.Data.EntityType.AdvertisementConfigurationEntity) { }

		/// <summary>Creates a new, empty AdvertisementConfigurationEntity object.</summary>
		/// <returns>A new, empty AdvertisementConfigurationEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new AdvertisementConfigurationEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewAdvertisementConfiguration
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty AdvertisementConfigurationAdvertisementEntity objects.</summary>
	[Serializable]
	public partial class AdvertisementConfigurationAdvertisementEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public AdvertisementConfigurationAdvertisementEntityFactory() : base("AdvertisementConfigurationAdvertisementEntity", Obymobi.Data.EntityType.AdvertisementConfigurationAdvertisementEntity) { }

		/// <summary>Creates a new, empty AdvertisementConfigurationAdvertisementEntity object.</summary>
		/// <returns>A new, empty AdvertisementConfigurationAdvertisementEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new AdvertisementConfigurationAdvertisementEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewAdvertisementConfigurationAdvertisement
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty AdvertisementLanguageEntity objects.</summary>
	[Serializable]
	public partial class AdvertisementLanguageEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public AdvertisementLanguageEntityFactory() : base("AdvertisementLanguageEntity", Obymobi.Data.EntityType.AdvertisementLanguageEntity) { }

		/// <summary>Creates a new, empty AdvertisementLanguageEntity object.</summary>
		/// <returns>A new, empty AdvertisementLanguageEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new AdvertisementLanguageEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewAdvertisementLanguage
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty AdvertisementTagEntity objects.</summary>
	[Serializable]
	public partial class AdvertisementTagEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public AdvertisementTagEntityFactory() : base("AdvertisementTagEntity", Obymobi.Data.EntityType.AdvertisementTagEntity) { }

		/// <summary>Creates a new, empty AdvertisementTagEntity object.</summary>
		/// <returns>A new, empty AdvertisementTagEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new AdvertisementTagEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewAdvertisementTag
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty AdvertisementTagAdvertisementEntity objects.</summary>
	[Serializable]
	public partial class AdvertisementTagAdvertisementEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public AdvertisementTagAdvertisementEntityFactory() : base("AdvertisementTagAdvertisementEntity", Obymobi.Data.EntityType.AdvertisementTagAdvertisementEntity) { }

		/// <summary>Creates a new, empty AdvertisementTagAdvertisementEntity object.</summary>
		/// <returns>A new, empty AdvertisementTagAdvertisementEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new AdvertisementTagAdvertisementEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewAdvertisementTagAdvertisement
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty AdvertisementTagCategoryEntity objects.</summary>
	[Serializable]
	public partial class AdvertisementTagCategoryEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public AdvertisementTagCategoryEntityFactory() : base("AdvertisementTagCategoryEntity", Obymobi.Data.EntityType.AdvertisementTagCategoryEntity) { }

		/// <summary>Creates a new, empty AdvertisementTagCategoryEntity object.</summary>
		/// <returns>A new, empty AdvertisementTagCategoryEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new AdvertisementTagCategoryEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewAdvertisementTagCategory
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty AdvertisementTagEntertainmentEntity objects.</summary>
	[Serializable]
	public partial class AdvertisementTagEntertainmentEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public AdvertisementTagEntertainmentEntityFactory() : base("AdvertisementTagEntertainmentEntity", Obymobi.Data.EntityType.AdvertisementTagEntertainmentEntity) { }

		/// <summary>Creates a new, empty AdvertisementTagEntertainmentEntity object.</summary>
		/// <returns>A new, empty AdvertisementTagEntertainmentEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new AdvertisementTagEntertainmentEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewAdvertisementTagEntertainment
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty AdvertisementTagGenericproductEntity objects.</summary>
	[Serializable]
	public partial class AdvertisementTagGenericproductEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public AdvertisementTagGenericproductEntityFactory() : base("AdvertisementTagGenericproductEntity", Obymobi.Data.EntityType.AdvertisementTagGenericproductEntity) { }

		/// <summary>Creates a new, empty AdvertisementTagGenericproductEntity object.</summary>
		/// <returns>A new, empty AdvertisementTagGenericproductEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new AdvertisementTagGenericproductEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewAdvertisementTagGenericproduct
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty AdvertisementTagLanguageEntity objects.</summary>
	[Serializable]
	public partial class AdvertisementTagLanguageEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public AdvertisementTagLanguageEntityFactory() : base("AdvertisementTagLanguageEntity", Obymobi.Data.EntityType.AdvertisementTagLanguageEntity) { }

		/// <summary>Creates a new, empty AdvertisementTagLanguageEntity object.</summary>
		/// <returns>A new, empty AdvertisementTagLanguageEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new AdvertisementTagLanguageEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewAdvertisementTagLanguage
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty AdvertisementTagProductEntity objects.</summary>
	[Serializable]
	public partial class AdvertisementTagProductEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public AdvertisementTagProductEntityFactory() : base("AdvertisementTagProductEntity", Obymobi.Data.EntityType.AdvertisementTagProductEntity) { }

		/// <summary>Creates a new, empty AdvertisementTagProductEntity object.</summary>
		/// <returns>A new, empty AdvertisementTagProductEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new AdvertisementTagProductEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewAdvertisementTagProduct
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty AdyenPaymentMethodEntity objects.</summary>
	[Serializable]
	public partial class AdyenPaymentMethodEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public AdyenPaymentMethodEntityFactory() : base("AdyenPaymentMethodEntity", Obymobi.Data.EntityType.AdyenPaymentMethodEntity) { }

		/// <summary>Creates a new, empty AdyenPaymentMethodEntity object.</summary>
		/// <returns>A new, empty AdyenPaymentMethodEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new AdyenPaymentMethodEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewAdyenPaymentMethod
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty AdyenPaymentMethodBrandEntity objects.</summary>
	[Serializable]
	public partial class AdyenPaymentMethodBrandEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public AdyenPaymentMethodBrandEntityFactory() : base("AdyenPaymentMethodBrandEntity", Obymobi.Data.EntityType.AdyenPaymentMethodBrandEntity) { }

		/// <summary>Creates a new, empty AdyenPaymentMethodBrandEntity object.</summary>
		/// <returns>A new, empty AdyenPaymentMethodBrandEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new AdyenPaymentMethodBrandEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewAdyenPaymentMethodBrand
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty AffiliateCampaignEntity objects.</summary>
	[Serializable]
	public partial class AffiliateCampaignEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public AffiliateCampaignEntityFactory() : base("AffiliateCampaignEntity", Obymobi.Data.EntityType.AffiliateCampaignEntity) { }

		/// <summary>Creates a new, empty AffiliateCampaignEntity object.</summary>
		/// <returns>A new, empty AffiliateCampaignEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new AffiliateCampaignEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewAffiliateCampaign
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty AffiliateCampaignAffiliatePartnerEntity objects.</summary>
	[Serializable]
	public partial class AffiliateCampaignAffiliatePartnerEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public AffiliateCampaignAffiliatePartnerEntityFactory() : base("AffiliateCampaignAffiliatePartnerEntity", Obymobi.Data.EntityType.AffiliateCampaignAffiliatePartnerEntity) { }

		/// <summary>Creates a new, empty AffiliateCampaignAffiliatePartnerEntity object.</summary>
		/// <returns>A new, empty AffiliateCampaignAffiliatePartnerEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new AffiliateCampaignAffiliatePartnerEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewAffiliateCampaignAffiliatePartner
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty AffiliatePartnerEntity objects.</summary>
	[Serializable]
	public partial class AffiliatePartnerEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public AffiliatePartnerEntityFactory() : base("AffiliatePartnerEntity", Obymobi.Data.EntityType.AffiliatePartnerEntity) { }

		/// <summary>Creates a new, empty AffiliatePartnerEntity object.</summary>
		/// <returns>A new, empty AffiliatePartnerEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new AffiliatePartnerEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewAffiliatePartner
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty AlterationEntity objects.</summary>
	[Serializable]
	public partial class AlterationEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public AlterationEntityFactory() : base("AlterationEntity", Obymobi.Data.EntityType.AlterationEntity) { }

		/// <summary>Creates a new, empty AlterationEntity object.</summary>
		/// <returns>A new, empty AlterationEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new AlterationEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewAlteration
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty AlterationitemEntity objects.</summary>
	[Serializable]
	public partial class AlterationitemEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public AlterationitemEntityFactory() : base("AlterationitemEntity", Obymobi.Data.EntityType.AlterationitemEntity) { }

		/// <summary>Creates a new, empty AlterationitemEntity object.</summary>
		/// <returns>A new, empty AlterationitemEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new AlterationitemEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewAlterationitem
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty AlterationitemAlterationEntity objects.</summary>
	[Serializable]
	public partial class AlterationitemAlterationEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public AlterationitemAlterationEntityFactory() : base("AlterationitemAlterationEntity", Obymobi.Data.EntityType.AlterationitemAlterationEntity) { }

		/// <summary>Creates a new, empty AlterationitemAlterationEntity object.</summary>
		/// <returns>A new, empty AlterationitemAlterationEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new AlterationitemAlterationEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewAlterationitemAlteration
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty AlterationLanguageEntity objects.</summary>
	[Serializable]
	public partial class AlterationLanguageEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public AlterationLanguageEntityFactory() : base("AlterationLanguageEntity", Obymobi.Data.EntityType.AlterationLanguageEntity) { }

		/// <summary>Creates a new, empty AlterationLanguageEntity object.</summary>
		/// <returns>A new, empty AlterationLanguageEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new AlterationLanguageEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewAlterationLanguage
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty AlterationoptionEntity objects.</summary>
	[Serializable]
	public partial class AlterationoptionEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public AlterationoptionEntityFactory() : base("AlterationoptionEntity", Obymobi.Data.EntityType.AlterationoptionEntity) { }

		/// <summary>Creates a new, empty AlterationoptionEntity object.</summary>
		/// <returns>A new, empty AlterationoptionEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new AlterationoptionEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewAlterationoption
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty AlterationoptionLanguageEntity objects.</summary>
	[Serializable]
	public partial class AlterationoptionLanguageEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public AlterationoptionLanguageEntityFactory() : base("AlterationoptionLanguageEntity", Obymobi.Data.EntityType.AlterationoptionLanguageEntity) { }

		/// <summary>Creates a new, empty AlterationoptionLanguageEntity object.</summary>
		/// <returns>A new, empty AlterationoptionLanguageEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new AlterationoptionLanguageEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewAlterationoptionLanguage
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty AlterationoptionTagEntity objects.</summary>
	[Serializable]
	public partial class AlterationoptionTagEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public AlterationoptionTagEntityFactory() : base("AlterationoptionTagEntity", Obymobi.Data.EntityType.AlterationoptionTagEntity) { }

		/// <summary>Creates a new, empty AlterationoptionTagEntity object.</summary>
		/// <returns>A new, empty AlterationoptionTagEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new AlterationoptionTagEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewAlterationoptionTag
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty AlterationProductEntity objects.</summary>
	[Serializable]
	public partial class AlterationProductEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public AlterationProductEntityFactory() : base("AlterationProductEntity", Obymobi.Data.EntityType.AlterationProductEntity) { }

		/// <summary>Creates a new, empty AlterationProductEntity object.</summary>
		/// <returns>A new, empty AlterationProductEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new AlterationProductEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewAlterationProduct
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty AmenityEntity objects.</summary>
	[Serializable]
	public partial class AmenityEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public AmenityEntityFactory() : base("AmenityEntity", Obymobi.Data.EntityType.AmenityEntity) { }

		/// <summary>Creates a new, empty AmenityEntity object.</summary>
		/// <returns>A new, empty AmenityEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new AmenityEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewAmenity
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty AmenityLanguageEntity objects.</summary>
	[Serializable]
	public partial class AmenityLanguageEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public AmenityLanguageEntityFactory() : base("AmenityLanguageEntity", Obymobi.Data.EntityType.AmenityLanguageEntity) { }

		/// <summary>Creates a new, empty AmenityLanguageEntity object.</summary>
		/// <returns>A new, empty AmenityLanguageEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new AmenityLanguageEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewAmenityLanguage
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty AnalyticsProcessingTaskEntity objects.</summary>
	[Serializable]
	public partial class AnalyticsProcessingTaskEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public AnalyticsProcessingTaskEntityFactory() : base("AnalyticsProcessingTaskEntity", Obymobi.Data.EntityType.AnalyticsProcessingTaskEntity) { }

		/// <summary>Creates a new, empty AnalyticsProcessingTaskEntity object.</summary>
		/// <returns>A new, empty AnalyticsProcessingTaskEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new AnalyticsProcessingTaskEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewAnalyticsProcessingTask
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty AnnouncementEntity objects.</summary>
	[Serializable]
	public partial class AnnouncementEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public AnnouncementEntityFactory() : base("AnnouncementEntity", Obymobi.Data.EntityType.AnnouncementEntity) { }

		/// <summary>Creates a new, empty AnnouncementEntity object.</summary>
		/// <returns>A new, empty AnnouncementEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new AnnouncementEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewAnnouncement
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty AnnouncementLanguageEntity objects.</summary>
	[Serializable]
	public partial class AnnouncementLanguageEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public AnnouncementLanguageEntityFactory() : base("AnnouncementLanguageEntity", Obymobi.Data.EntityType.AnnouncementLanguageEntity) { }

		/// <summary>Creates a new, empty AnnouncementLanguageEntity object.</summary>
		/// <returns>A new, empty AnnouncementLanguageEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new AnnouncementLanguageEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewAnnouncementLanguage
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty ApiAuthenticationEntity objects.</summary>
	[Serializable]
	public partial class ApiAuthenticationEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public ApiAuthenticationEntityFactory() : base("ApiAuthenticationEntity", Obymobi.Data.EntityType.ApiAuthenticationEntity) { }

		/// <summary>Creates a new, empty ApiAuthenticationEntity object.</summary>
		/// <returns>A new, empty ApiAuthenticationEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new ApiAuthenticationEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewApiAuthentication
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty ApplicationEntity objects.</summary>
	[Serializable]
	public partial class ApplicationEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public ApplicationEntityFactory() : base("ApplicationEntity", Obymobi.Data.EntityType.ApplicationEntity) { }

		/// <summary>Creates a new, empty ApplicationEntity object.</summary>
		/// <returns>A new, empty ApplicationEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new ApplicationEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewApplication
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty AttachmentEntity objects.</summary>
	[Serializable]
	public partial class AttachmentEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public AttachmentEntityFactory() : base("AttachmentEntity", Obymobi.Data.EntityType.AttachmentEntity) { }

		/// <summary>Creates a new, empty AttachmentEntity object.</summary>
		/// <returns>A new, empty AttachmentEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new AttachmentEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewAttachment
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty AttachmentLanguageEntity objects.</summary>
	[Serializable]
	public partial class AttachmentLanguageEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public AttachmentLanguageEntityFactory() : base("AttachmentLanguageEntity", Obymobi.Data.EntityType.AttachmentLanguageEntity) { }

		/// <summary>Creates a new, empty AttachmentLanguageEntity object.</summary>
		/// <returns>A new, empty AttachmentLanguageEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new AttachmentLanguageEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewAttachmentLanguage
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty AuditlogEntity objects.</summary>
	[Serializable]
	public partial class AuditlogEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public AuditlogEntityFactory() : base("AuditlogEntity", Obymobi.Data.EntityType.AuditlogEntity) { }

		/// <summary>Creates a new, empty AuditlogEntity object.</summary>
		/// <returns>A new, empty AuditlogEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new AuditlogEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewAuditlog
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty AvailabilityEntity objects.</summary>
	[Serializable]
	public partial class AvailabilityEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public AvailabilityEntityFactory() : base("AvailabilityEntity", Obymobi.Data.EntityType.AvailabilityEntity) { }

		/// <summary>Creates a new, empty AvailabilityEntity object.</summary>
		/// <returns>A new, empty AvailabilityEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new AvailabilityEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewAvailability
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty AzureNotificationHubEntity objects.</summary>
	[Serializable]
	public partial class AzureNotificationHubEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public AzureNotificationHubEntityFactory() : base("AzureNotificationHubEntity", Obymobi.Data.EntityType.AzureNotificationHubEntity) { }

		/// <summary>Creates a new, empty AzureNotificationHubEntity object.</summary>
		/// <returns>A new, empty AzureNotificationHubEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new AzureNotificationHubEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewAzureNotificationHub
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty BrandEntity objects.</summary>
	[Serializable]
	public partial class BrandEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public BrandEntityFactory() : base("BrandEntity", Obymobi.Data.EntityType.BrandEntity) { }

		/// <summary>Creates a new, empty BrandEntity object.</summary>
		/// <returns>A new, empty BrandEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new BrandEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewBrand
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty BrandCultureEntity objects.</summary>
	[Serializable]
	public partial class BrandCultureEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public BrandCultureEntityFactory() : base("BrandCultureEntity", Obymobi.Data.EntityType.BrandCultureEntity) { }

		/// <summary>Creates a new, empty BrandCultureEntity object.</summary>
		/// <returns>A new, empty BrandCultureEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new BrandCultureEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewBrandCulture
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty BusinesshoursEntity objects.</summary>
	[Serializable]
	public partial class BusinesshoursEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public BusinesshoursEntityFactory() : base("BusinesshoursEntity", Obymobi.Data.EntityType.BusinesshoursEntity) { }

		/// <summary>Creates a new, empty BusinesshoursEntity object.</summary>
		/// <returns>A new, empty BusinesshoursEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new BusinesshoursEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewBusinesshours
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty BusinesshoursexceptionEntity objects.</summary>
	[Serializable]
	public partial class BusinesshoursexceptionEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public BusinesshoursexceptionEntityFactory() : base("BusinesshoursexceptionEntity", Obymobi.Data.EntityType.BusinesshoursexceptionEntity) { }

		/// <summary>Creates a new, empty BusinesshoursexceptionEntity object.</summary>
		/// <returns>A new, empty BusinesshoursexceptionEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new BusinesshoursexceptionEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewBusinesshoursexception
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty CategoryEntity objects.</summary>
	[Serializable]
	public partial class CategoryEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public CategoryEntityFactory() : base("CategoryEntity", Obymobi.Data.EntityType.CategoryEntity) { }

		/// <summary>Creates a new, empty CategoryEntity object.</summary>
		/// <returns>A new, empty CategoryEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new CategoryEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewCategory
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty CategoryAlterationEntity objects.</summary>
	[Serializable]
	public partial class CategoryAlterationEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public CategoryAlterationEntityFactory() : base("CategoryAlterationEntity", Obymobi.Data.EntityType.CategoryAlterationEntity) { }

		/// <summary>Creates a new, empty CategoryAlterationEntity object.</summary>
		/// <returns>A new, empty CategoryAlterationEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new CategoryAlterationEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewCategoryAlteration
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty CategoryLanguageEntity objects.</summary>
	[Serializable]
	public partial class CategoryLanguageEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public CategoryLanguageEntityFactory() : base("CategoryLanguageEntity", Obymobi.Data.EntityType.CategoryLanguageEntity) { }

		/// <summary>Creates a new, empty CategoryLanguageEntity object.</summary>
		/// <returns>A new, empty CategoryLanguageEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new CategoryLanguageEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewCategoryLanguage
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty CategorySuggestionEntity objects.</summary>
	[Serializable]
	public partial class CategorySuggestionEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public CategorySuggestionEntityFactory() : base("CategorySuggestionEntity", Obymobi.Data.EntityType.CategorySuggestionEntity) { }

		/// <summary>Creates a new, empty CategorySuggestionEntity object.</summary>
		/// <returns>A new, empty CategorySuggestionEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new CategorySuggestionEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewCategorySuggestion
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty CategoryTagEntity objects.</summary>
	[Serializable]
	public partial class CategoryTagEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public CategoryTagEntityFactory() : base("CategoryTagEntity", Obymobi.Data.EntityType.CategoryTagEntity) { }

		/// <summary>Creates a new, empty CategoryTagEntity object.</summary>
		/// <returns>A new, empty CategoryTagEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new CategoryTagEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewCategoryTag
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty CheckoutMethodEntity objects.</summary>
	[Serializable]
	public partial class CheckoutMethodEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public CheckoutMethodEntityFactory() : base("CheckoutMethodEntity", Obymobi.Data.EntityType.CheckoutMethodEntity) { }

		/// <summary>Creates a new, empty CheckoutMethodEntity object.</summary>
		/// <returns>A new, empty CheckoutMethodEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new CheckoutMethodEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewCheckoutMethod
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty CheckoutMethodDeliverypointgroupEntity objects.</summary>
	[Serializable]
	public partial class CheckoutMethodDeliverypointgroupEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public CheckoutMethodDeliverypointgroupEntityFactory() : base("CheckoutMethodDeliverypointgroupEntity", Obymobi.Data.EntityType.CheckoutMethodDeliverypointgroupEntity) { }

		/// <summary>Creates a new, empty CheckoutMethodDeliverypointgroupEntity object.</summary>
		/// <returns>A new, empty CheckoutMethodDeliverypointgroupEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new CheckoutMethodDeliverypointgroupEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewCheckoutMethodDeliverypointgroup
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty ClientEntity objects.</summary>
	[Serializable]
	public partial class ClientEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public ClientEntityFactory() : base("ClientEntity", Obymobi.Data.EntityType.ClientEntity) { }

		/// <summary>Creates a new, empty ClientEntity object.</summary>
		/// <returns>A new, empty ClientEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new ClientEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewClient
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty ClientConfigurationEntity objects.</summary>
	[Serializable]
	public partial class ClientConfigurationEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public ClientConfigurationEntityFactory() : base("ClientConfigurationEntity", Obymobi.Data.EntityType.ClientConfigurationEntity) { }

		/// <summary>Creates a new, empty ClientConfigurationEntity object.</summary>
		/// <returns>A new, empty ClientConfigurationEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new ClientConfigurationEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewClientConfiguration
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty ClientConfigurationRouteEntity objects.</summary>
	[Serializable]
	public partial class ClientConfigurationRouteEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public ClientConfigurationRouteEntityFactory() : base("ClientConfigurationRouteEntity", Obymobi.Data.EntityType.ClientConfigurationRouteEntity) { }

		/// <summary>Creates a new, empty ClientConfigurationRouteEntity object.</summary>
		/// <returns>A new, empty ClientConfigurationRouteEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new ClientConfigurationRouteEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewClientConfigurationRoute
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty ClientEntertainmentEntity objects.</summary>
	[Serializable]
	public partial class ClientEntertainmentEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public ClientEntertainmentEntityFactory() : base("ClientEntertainmentEntity", Obymobi.Data.EntityType.ClientEntertainmentEntity) { }

		/// <summary>Creates a new, empty ClientEntertainmentEntity object.</summary>
		/// <returns>A new, empty ClientEntertainmentEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new ClientEntertainmentEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewClientEntertainment
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty ClientLogEntity objects.</summary>
	[Serializable]
	public partial class ClientLogEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public ClientLogEntityFactory() : base("ClientLogEntity", Obymobi.Data.EntityType.ClientLogEntity) { }

		/// <summary>Creates a new, empty ClientLogEntity object.</summary>
		/// <returns>A new, empty ClientLogEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new ClientLogEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewClientLog
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty ClientLogFileEntity objects.</summary>
	[Serializable]
	public partial class ClientLogFileEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public ClientLogFileEntityFactory() : base("ClientLogFileEntity", Obymobi.Data.EntityType.ClientLogFileEntity) { }

		/// <summary>Creates a new, empty ClientLogFileEntity object.</summary>
		/// <returns>A new, empty ClientLogFileEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new ClientLogFileEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewClientLogFile
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty ClientStateEntity objects.</summary>
	[Serializable]
	public partial class ClientStateEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public ClientStateEntityFactory() : base("ClientStateEntity", Obymobi.Data.EntityType.ClientStateEntity) { }

		/// <summary>Creates a new, empty ClientStateEntity object.</summary>
		/// <returns>A new, empty ClientStateEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new ClientStateEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewClientState
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty CloudApplicationVersionEntity objects.</summary>
	[Serializable]
	public partial class CloudApplicationVersionEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public CloudApplicationVersionEntityFactory() : base("CloudApplicationVersionEntity", Obymobi.Data.EntityType.CloudApplicationVersionEntity) { }

		/// <summary>Creates a new, empty CloudApplicationVersionEntity object.</summary>
		/// <returns>A new, empty CloudApplicationVersionEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new CloudApplicationVersionEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewCloudApplicationVersion
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty CloudProcessingTaskEntity objects.</summary>
	[Serializable]
	public partial class CloudProcessingTaskEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public CloudProcessingTaskEntityFactory() : base("CloudProcessingTaskEntity", Obymobi.Data.EntityType.CloudProcessingTaskEntity) { }

		/// <summary>Creates a new, empty CloudProcessingTaskEntity object.</summary>
		/// <returns>A new, empty CloudProcessingTaskEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new CloudProcessingTaskEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewCloudProcessingTask
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty CloudStorageAccountEntity objects.</summary>
	[Serializable]
	public partial class CloudStorageAccountEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public CloudStorageAccountEntityFactory() : base("CloudStorageAccountEntity", Obymobi.Data.EntityType.CloudStorageAccountEntity) { }

		/// <summary>Creates a new, empty CloudStorageAccountEntity object.</summary>
		/// <returns>A new, empty CloudStorageAccountEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new CloudStorageAccountEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewCloudStorageAccount
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty CompanyEntity objects.</summary>
	[Serializable]
	public partial class CompanyEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public CompanyEntityFactory() : base("CompanyEntity", Obymobi.Data.EntityType.CompanyEntity) { }

		/// <summary>Creates a new, empty CompanyEntity object.</summary>
		/// <returns>A new, empty CompanyEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new CompanyEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewCompany
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty CompanyAmenityEntity objects.</summary>
	[Serializable]
	public partial class CompanyAmenityEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public CompanyAmenityEntityFactory() : base("CompanyAmenityEntity", Obymobi.Data.EntityType.CompanyAmenityEntity) { }

		/// <summary>Creates a new, empty CompanyAmenityEntity object.</summary>
		/// <returns>A new, empty CompanyAmenityEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new CompanyAmenityEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewCompanyAmenity
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty CompanyBrandEntity objects.</summary>
	[Serializable]
	public partial class CompanyBrandEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public CompanyBrandEntityFactory() : base("CompanyBrandEntity", Obymobi.Data.EntityType.CompanyBrandEntity) { }

		/// <summary>Creates a new, empty CompanyBrandEntity object.</summary>
		/// <returns>A new, empty CompanyBrandEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new CompanyBrandEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewCompanyBrand
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty CompanyCultureEntity objects.</summary>
	[Serializable]
	public partial class CompanyCultureEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public CompanyCultureEntityFactory() : base("CompanyCultureEntity", Obymobi.Data.EntityType.CompanyCultureEntity) { }

		/// <summary>Creates a new, empty CompanyCultureEntity object.</summary>
		/// <returns>A new, empty CompanyCultureEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new CompanyCultureEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewCompanyCulture
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty CompanyCurrencyEntity objects.</summary>
	[Serializable]
	public partial class CompanyCurrencyEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public CompanyCurrencyEntityFactory() : base("CompanyCurrencyEntity", Obymobi.Data.EntityType.CompanyCurrencyEntity) { }

		/// <summary>Creates a new, empty CompanyCurrencyEntity object.</summary>
		/// <returns>A new, empty CompanyCurrencyEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new CompanyCurrencyEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewCompanyCurrency
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty CompanyEntertainmentEntity objects.</summary>
	[Serializable]
	public partial class CompanyEntertainmentEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public CompanyEntertainmentEntityFactory() : base("CompanyEntertainmentEntity", Obymobi.Data.EntityType.CompanyEntertainmentEntity) { }

		/// <summary>Creates a new, empty CompanyEntertainmentEntity object.</summary>
		/// <returns>A new, empty CompanyEntertainmentEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new CompanyEntertainmentEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewCompanyEntertainment
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty CompanygroupEntity objects.</summary>
	[Serializable]
	public partial class CompanygroupEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public CompanygroupEntityFactory() : base("CompanygroupEntity", Obymobi.Data.EntityType.CompanygroupEntity) { }

		/// <summary>Creates a new, empty CompanygroupEntity object.</summary>
		/// <returns>A new, empty CompanygroupEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new CompanygroupEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewCompanygroup
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty CompanyLanguageEntity objects.</summary>
	[Serializable]
	public partial class CompanyLanguageEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public CompanyLanguageEntityFactory() : base("CompanyLanguageEntity", Obymobi.Data.EntityType.CompanyLanguageEntity) { }

		/// <summary>Creates a new, empty CompanyLanguageEntity object.</summary>
		/// <returns>A new, empty CompanyLanguageEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new CompanyLanguageEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewCompanyLanguage
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty CompanyManagementTaskEntity objects.</summary>
	[Serializable]
	public partial class CompanyManagementTaskEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public CompanyManagementTaskEntityFactory() : base("CompanyManagementTaskEntity", Obymobi.Data.EntityType.CompanyManagementTaskEntity) { }

		/// <summary>Creates a new, empty CompanyManagementTaskEntity object.</summary>
		/// <returns>A new, empty CompanyManagementTaskEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new CompanyManagementTaskEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewCompanyManagementTask
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty CompanyOwnerEntity objects.</summary>
	[Serializable]
	public partial class CompanyOwnerEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public CompanyOwnerEntityFactory() : base("CompanyOwnerEntity", Obymobi.Data.EntityType.CompanyOwnerEntity) { }

		/// <summary>Creates a new, empty CompanyOwnerEntity object.</summary>
		/// <returns>A new, empty CompanyOwnerEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new CompanyOwnerEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewCompanyOwner
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty CompanyReleaseEntity objects.</summary>
	[Serializable]
	public partial class CompanyReleaseEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public CompanyReleaseEntityFactory() : base("CompanyReleaseEntity", Obymobi.Data.EntityType.CompanyReleaseEntity) { }

		/// <summary>Creates a new, empty CompanyReleaseEntity object.</summary>
		/// <returns>A new, empty CompanyReleaseEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new CompanyReleaseEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewCompanyRelease
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty CompanyVenueCategoryEntity objects.</summary>
	[Serializable]
	public partial class CompanyVenueCategoryEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public CompanyVenueCategoryEntityFactory() : base("CompanyVenueCategoryEntity", Obymobi.Data.EntityType.CompanyVenueCategoryEntity) { }

		/// <summary>Creates a new, empty CompanyVenueCategoryEntity object.</summary>
		/// <returns>A new, empty CompanyVenueCategoryEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new CompanyVenueCategoryEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewCompanyVenueCategory
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty ConfigurationEntity objects.</summary>
	[Serializable]
	public partial class ConfigurationEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public ConfigurationEntityFactory() : base("ConfigurationEntity", Obymobi.Data.EntityType.ConfigurationEntity) { }

		/// <summary>Creates a new, empty ConfigurationEntity object.</summary>
		/// <returns>A new, empty ConfigurationEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new ConfigurationEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewConfiguration
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty CountryEntity objects.</summary>
	[Serializable]
	public partial class CountryEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public CountryEntityFactory() : base("CountryEntity", Obymobi.Data.EntityType.CountryEntity) { }

		/// <summary>Creates a new, empty CountryEntity object.</summary>
		/// <returns>A new, empty CountryEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new CountryEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewCountry
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty CurrencyEntity objects.</summary>
	[Serializable]
	public partial class CurrencyEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public CurrencyEntityFactory() : base("CurrencyEntity", Obymobi.Data.EntityType.CurrencyEntity) { }

		/// <summary>Creates a new, empty CurrencyEntity object.</summary>
		/// <returns>A new, empty CurrencyEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new CurrencyEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewCurrency
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty CustomerEntity objects.</summary>
	[Serializable]
	public partial class CustomerEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public CustomerEntityFactory() : base("CustomerEntity", Obymobi.Data.EntityType.CustomerEntity) { }

		/// <summary>Creates a new, empty CustomerEntity object.</summary>
		/// <returns>A new, empty CustomerEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new CustomerEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewCustomer
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty CustomTextEntity objects.</summary>
	[Serializable]
	public partial class CustomTextEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public CustomTextEntityFactory() : base("CustomTextEntity", Obymobi.Data.EntityType.CustomTextEntity) { }

		/// <summary>Creates a new, empty CustomTextEntity object.</summary>
		/// <returns>A new, empty CustomTextEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new CustomTextEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewCustomText
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty DeliveryDistanceEntity objects.</summary>
	[Serializable]
	public partial class DeliveryDistanceEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public DeliveryDistanceEntityFactory() : base("DeliveryDistanceEntity", Obymobi.Data.EntityType.DeliveryDistanceEntity) { }

		/// <summary>Creates a new, empty DeliveryDistanceEntity object.</summary>
		/// <returns>A new, empty DeliveryDistanceEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new DeliveryDistanceEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewDeliveryDistance
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty DeliveryInformationEntity objects.</summary>
	[Serializable]
	public partial class DeliveryInformationEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public DeliveryInformationEntityFactory() : base("DeliveryInformationEntity", Obymobi.Data.EntityType.DeliveryInformationEntity) { }

		/// <summary>Creates a new, empty DeliveryInformationEntity object.</summary>
		/// <returns>A new, empty DeliveryInformationEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new DeliveryInformationEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewDeliveryInformation
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty DeliverypointEntity objects.</summary>
	[Serializable]
	public partial class DeliverypointEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public DeliverypointEntityFactory() : base("DeliverypointEntity", Obymobi.Data.EntityType.DeliverypointEntity) { }

		/// <summary>Creates a new, empty DeliverypointEntity object.</summary>
		/// <returns>A new, empty DeliverypointEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new DeliverypointEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewDeliverypoint
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty DeliverypointExternalDeliverypointEntity objects.</summary>
	[Serializable]
	public partial class DeliverypointExternalDeliverypointEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public DeliverypointExternalDeliverypointEntityFactory() : base("DeliverypointExternalDeliverypointEntity", Obymobi.Data.EntityType.DeliverypointExternalDeliverypointEntity) { }

		/// <summary>Creates a new, empty DeliverypointExternalDeliverypointEntity object.</summary>
		/// <returns>A new, empty DeliverypointExternalDeliverypointEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new DeliverypointExternalDeliverypointEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewDeliverypointExternalDeliverypoint
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty DeliverypointgroupEntity objects.</summary>
	[Serializable]
	public partial class DeliverypointgroupEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public DeliverypointgroupEntityFactory() : base("DeliverypointgroupEntity", Obymobi.Data.EntityType.DeliverypointgroupEntity) { }

		/// <summary>Creates a new, empty DeliverypointgroupEntity object.</summary>
		/// <returns>A new, empty DeliverypointgroupEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new DeliverypointgroupEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewDeliverypointgroup
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty DeliverypointgroupAdvertisementEntity objects.</summary>
	[Serializable]
	public partial class DeliverypointgroupAdvertisementEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public DeliverypointgroupAdvertisementEntityFactory() : base("DeliverypointgroupAdvertisementEntity", Obymobi.Data.EntityType.DeliverypointgroupAdvertisementEntity) { }

		/// <summary>Creates a new, empty DeliverypointgroupAdvertisementEntity object.</summary>
		/// <returns>A new, empty DeliverypointgroupAdvertisementEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new DeliverypointgroupAdvertisementEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewDeliverypointgroupAdvertisement
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty DeliverypointgroupAnnouncementEntity objects.</summary>
	[Serializable]
	public partial class DeliverypointgroupAnnouncementEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public DeliverypointgroupAnnouncementEntityFactory() : base("DeliverypointgroupAnnouncementEntity", Obymobi.Data.EntityType.DeliverypointgroupAnnouncementEntity) { }

		/// <summary>Creates a new, empty DeliverypointgroupAnnouncementEntity object.</summary>
		/// <returns>A new, empty DeliverypointgroupAnnouncementEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new DeliverypointgroupAnnouncementEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewDeliverypointgroupAnnouncement
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty DeliverypointgroupEntertainmentEntity objects.</summary>
	[Serializable]
	public partial class DeliverypointgroupEntertainmentEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public DeliverypointgroupEntertainmentEntityFactory() : base("DeliverypointgroupEntertainmentEntity", Obymobi.Data.EntityType.DeliverypointgroupEntertainmentEntity) { }

		/// <summary>Creates a new, empty DeliverypointgroupEntertainmentEntity object.</summary>
		/// <returns>A new, empty DeliverypointgroupEntertainmentEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new DeliverypointgroupEntertainmentEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewDeliverypointgroupEntertainment
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty DeliverypointgroupLanguageEntity objects.</summary>
	[Serializable]
	public partial class DeliverypointgroupLanguageEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public DeliverypointgroupLanguageEntityFactory() : base("DeliverypointgroupLanguageEntity", Obymobi.Data.EntityType.DeliverypointgroupLanguageEntity) { }

		/// <summary>Creates a new, empty DeliverypointgroupLanguageEntity object.</summary>
		/// <returns>A new, empty DeliverypointgroupLanguageEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new DeliverypointgroupLanguageEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewDeliverypointgroupLanguage
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty DeliverypointgroupOccupancyEntity objects.</summary>
	[Serializable]
	public partial class DeliverypointgroupOccupancyEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public DeliverypointgroupOccupancyEntityFactory() : base("DeliverypointgroupOccupancyEntity", Obymobi.Data.EntityType.DeliverypointgroupOccupancyEntity) { }

		/// <summary>Creates a new, empty DeliverypointgroupOccupancyEntity object.</summary>
		/// <returns>A new, empty DeliverypointgroupOccupancyEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new DeliverypointgroupOccupancyEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewDeliverypointgroupOccupancy
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty DeliverypointgroupProductEntity objects.</summary>
	[Serializable]
	public partial class DeliverypointgroupProductEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public DeliverypointgroupProductEntityFactory() : base("DeliverypointgroupProductEntity", Obymobi.Data.EntityType.DeliverypointgroupProductEntity) { }

		/// <summary>Creates a new, empty DeliverypointgroupProductEntity object.</summary>
		/// <returns>A new, empty DeliverypointgroupProductEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new DeliverypointgroupProductEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewDeliverypointgroupProduct
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty DeviceEntity objects.</summary>
	[Serializable]
	public partial class DeviceEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public DeviceEntityFactory() : base("DeviceEntity", Obymobi.Data.EntityType.DeviceEntity) { }

		/// <summary>Creates a new, empty DeviceEntity object.</summary>
		/// <returns>A new, empty DeviceEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new DeviceEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewDevice
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty DevicemediaEntity objects.</summary>
	[Serializable]
	public partial class DevicemediaEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public DevicemediaEntityFactory() : base("DevicemediaEntity", Obymobi.Data.EntityType.DevicemediaEntity) { }

		/// <summary>Creates a new, empty DevicemediaEntity object.</summary>
		/// <returns>A new, empty DevicemediaEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new DevicemediaEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewDevicemedia
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty DeviceTokenHistoryEntity objects.</summary>
	[Serializable]
	public partial class DeviceTokenHistoryEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public DeviceTokenHistoryEntityFactory() : base("DeviceTokenHistoryEntity", Obymobi.Data.EntityType.DeviceTokenHistoryEntity) { }

		/// <summary>Creates a new, empty DeviceTokenHistoryEntity object.</summary>
		/// <returns>A new, empty DeviceTokenHistoryEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new DeviceTokenHistoryEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewDeviceTokenHistory
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty DeviceTokenTaskEntity objects.</summary>
	[Serializable]
	public partial class DeviceTokenTaskEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public DeviceTokenTaskEntityFactory() : base("DeviceTokenTaskEntity", Obymobi.Data.EntityType.DeviceTokenTaskEntity) { }

		/// <summary>Creates a new, empty DeviceTokenTaskEntity object.</summary>
		/// <returns>A new, empty DeviceTokenTaskEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new DeviceTokenTaskEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewDeviceTokenTask
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty EntertainmentEntity objects.</summary>
	[Serializable]
	public partial class EntertainmentEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public EntertainmentEntityFactory() : base("EntertainmentEntity", Obymobi.Data.EntityType.EntertainmentEntity) { }

		/// <summary>Creates a new, empty EntertainmentEntity object.</summary>
		/// <returns>A new, empty EntertainmentEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new EntertainmentEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewEntertainment
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty EntertainmentcategoryEntity objects.</summary>
	[Serializable]
	public partial class EntertainmentcategoryEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public EntertainmentcategoryEntityFactory() : base("EntertainmentcategoryEntity", Obymobi.Data.EntityType.EntertainmentcategoryEntity) { }

		/// <summary>Creates a new, empty EntertainmentcategoryEntity object.</summary>
		/// <returns>A new, empty EntertainmentcategoryEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new EntertainmentcategoryEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewEntertainmentcategory
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty EntertainmentcategoryLanguageEntity objects.</summary>
	[Serializable]
	public partial class EntertainmentcategoryLanguageEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public EntertainmentcategoryLanguageEntityFactory() : base("EntertainmentcategoryLanguageEntity", Obymobi.Data.EntityType.EntertainmentcategoryLanguageEntity) { }

		/// <summary>Creates a new, empty EntertainmentcategoryLanguageEntity object.</summary>
		/// <returns>A new, empty EntertainmentcategoryLanguageEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new EntertainmentcategoryLanguageEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewEntertainmentcategoryLanguage
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty EntertainmentConfigurationEntity objects.</summary>
	[Serializable]
	public partial class EntertainmentConfigurationEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public EntertainmentConfigurationEntityFactory() : base("EntertainmentConfigurationEntity", Obymobi.Data.EntityType.EntertainmentConfigurationEntity) { }

		/// <summary>Creates a new, empty EntertainmentConfigurationEntity object.</summary>
		/// <returns>A new, empty EntertainmentConfigurationEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new EntertainmentConfigurationEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewEntertainmentConfiguration
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty EntertainmentConfigurationEntertainmentEntity objects.</summary>
	[Serializable]
	public partial class EntertainmentConfigurationEntertainmentEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public EntertainmentConfigurationEntertainmentEntityFactory() : base("EntertainmentConfigurationEntertainmentEntity", Obymobi.Data.EntityType.EntertainmentConfigurationEntertainmentEntity) { }

		/// <summary>Creates a new, empty EntertainmentConfigurationEntertainmentEntity object.</summary>
		/// <returns>A new, empty EntertainmentConfigurationEntertainmentEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new EntertainmentConfigurationEntertainmentEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewEntertainmentConfigurationEntertainment
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty EntertainmentDependencyEntity objects.</summary>
	[Serializable]
	public partial class EntertainmentDependencyEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public EntertainmentDependencyEntityFactory() : base("EntertainmentDependencyEntity", Obymobi.Data.EntityType.EntertainmentDependencyEntity) { }

		/// <summary>Creates a new, empty EntertainmentDependencyEntity object.</summary>
		/// <returns>A new, empty EntertainmentDependencyEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new EntertainmentDependencyEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewEntertainmentDependency
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty EntertainmentFileEntity objects.</summary>
	[Serializable]
	public partial class EntertainmentFileEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public EntertainmentFileEntityFactory() : base("EntertainmentFileEntity", Obymobi.Data.EntityType.EntertainmentFileEntity) { }

		/// <summary>Creates a new, empty EntertainmentFileEntity object.</summary>
		/// <returns>A new, empty EntertainmentFileEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new EntertainmentFileEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewEntertainmentFile
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty EntertainmenturlEntity objects.</summary>
	[Serializable]
	public partial class EntertainmenturlEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public EntertainmenturlEntityFactory() : base("EntertainmenturlEntity", Obymobi.Data.EntityType.EntertainmenturlEntity) { }

		/// <summary>Creates a new, empty EntertainmenturlEntity object.</summary>
		/// <returns>A new, empty EntertainmenturlEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new EntertainmenturlEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewEntertainmenturl
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty EntityFieldInformationEntity objects.</summary>
	[Serializable]
	public partial class EntityFieldInformationEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public EntityFieldInformationEntityFactory() : base("EntityFieldInformationEntity", Obymobi.Data.EntityType.EntityFieldInformationEntity) { }

		/// <summary>Creates a new, empty EntityFieldInformationEntity object.</summary>
		/// <returns>A new, empty EntityFieldInformationEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new EntityFieldInformationEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewEntityFieldInformation
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty EntityFieldInformationCustomEntity objects.</summary>
	[Serializable]
	public partial class EntityFieldInformationCustomEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public EntityFieldInformationCustomEntityFactory() : base("EntityFieldInformationCustomEntity", Obymobi.Data.EntityType.EntityFieldInformationCustomEntity) { }

		/// <summary>Creates a new, empty EntityFieldInformationCustomEntity object.</summary>
		/// <returns>A new, empty EntityFieldInformationCustomEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new EntityFieldInformationCustomEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewEntityFieldInformationCustom
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty EntityInformationEntity objects.</summary>
	[Serializable]
	public partial class EntityInformationEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public EntityInformationEntityFactory() : base("EntityInformationEntity", Obymobi.Data.EntityType.EntityInformationEntity) { }

		/// <summary>Creates a new, empty EntityInformationEntity object.</summary>
		/// <returns>A new, empty EntityInformationEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new EntityInformationEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewEntityInformation
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty EntityInformationCustomEntity objects.</summary>
	[Serializable]
	public partial class EntityInformationCustomEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public EntityInformationCustomEntityFactory() : base("EntityInformationCustomEntity", Obymobi.Data.EntityType.EntityInformationCustomEntity) { }

		/// <summary>Creates a new, empty EntityInformationCustomEntity object.</summary>
		/// <returns>A new, empty EntityInformationCustomEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new EntityInformationCustomEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewEntityInformationCustom
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty ExternalDeliverypointEntity objects.</summary>
	[Serializable]
	public partial class ExternalDeliverypointEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public ExternalDeliverypointEntityFactory() : base("ExternalDeliverypointEntity", Obymobi.Data.EntityType.ExternalDeliverypointEntity) { }

		/// <summary>Creates a new, empty ExternalDeliverypointEntity object.</summary>
		/// <returns>A new, empty ExternalDeliverypointEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new ExternalDeliverypointEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewExternalDeliverypoint
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty ExternalMenuEntity objects.</summary>
	[Serializable]
	public partial class ExternalMenuEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public ExternalMenuEntityFactory() : base("ExternalMenuEntity", Obymobi.Data.EntityType.ExternalMenuEntity) { }

		/// <summary>Creates a new, empty ExternalMenuEntity object.</summary>
		/// <returns>A new, empty ExternalMenuEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new ExternalMenuEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewExternalMenu
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty ExternalProductEntity objects.</summary>
	[Serializable]
	public partial class ExternalProductEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public ExternalProductEntityFactory() : base("ExternalProductEntity", Obymobi.Data.EntityType.ExternalProductEntity) { }

		/// <summary>Creates a new, empty ExternalProductEntity object.</summary>
		/// <returns>A new, empty ExternalProductEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new ExternalProductEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewExternalProduct
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty ExternalSubProductEntity objects.</summary>
	[Serializable]
	public partial class ExternalSubProductEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public ExternalSubProductEntityFactory() : base("ExternalSubProductEntity", Obymobi.Data.EntityType.ExternalSubProductEntity) { }

		/// <summary>Creates a new, empty ExternalSubProductEntity object.</summary>
		/// <returns>A new, empty ExternalSubProductEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new ExternalSubProductEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewExternalSubProduct
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty ExternalSystemEntity objects.</summary>
	[Serializable]
	public partial class ExternalSystemEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public ExternalSystemEntityFactory() : base("ExternalSystemEntity", Obymobi.Data.EntityType.ExternalSystemEntity) { }

		/// <summary>Creates a new, empty ExternalSystemEntity object.</summary>
		/// <returns>A new, empty ExternalSystemEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new ExternalSystemEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewExternalSystem
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty ExternalSystemLogEntity objects.</summary>
	[Serializable]
	public partial class ExternalSystemLogEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public ExternalSystemLogEntityFactory() : base("ExternalSystemLogEntity", Obymobi.Data.EntityType.ExternalSystemLogEntity) { }

		/// <summary>Creates a new, empty ExternalSystemLogEntity object.</summary>
		/// <returns>A new, empty ExternalSystemLogEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new ExternalSystemLogEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewExternalSystemLog
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty FeatureToggleAvailabilityEntity objects.</summary>
	[Serializable]
	public partial class FeatureToggleAvailabilityEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public FeatureToggleAvailabilityEntityFactory() : base("FeatureToggleAvailabilityEntity", Obymobi.Data.EntityType.FeatureToggleAvailabilityEntity) { }

		/// <summary>Creates a new, empty FeatureToggleAvailabilityEntity object.</summary>
		/// <returns>A new, empty FeatureToggleAvailabilityEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new FeatureToggleAvailabilityEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewFeatureToggleAvailability
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty FolioEntity objects.</summary>
	[Serializable]
	public partial class FolioEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public FolioEntityFactory() : base("FolioEntity", Obymobi.Data.EntityType.FolioEntity) { }

		/// <summary>Creates a new, empty FolioEntity object.</summary>
		/// <returns>A new, empty FolioEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new FolioEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewFolio
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty FolioItemEntity objects.</summary>
	[Serializable]
	public partial class FolioItemEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public FolioItemEntityFactory() : base("FolioItemEntity", Obymobi.Data.EntityType.FolioItemEntity) { }

		/// <summary>Creates a new, empty FolioItemEntity object.</summary>
		/// <returns>A new, empty FolioItemEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new FolioItemEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewFolioItem
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty GameEntity objects.</summary>
	[Serializable]
	public partial class GameEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public GameEntityFactory() : base("GameEntity", Obymobi.Data.EntityType.GameEntity) { }

		/// <summary>Creates a new, empty GameEntity object.</summary>
		/// <returns>A new, empty GameEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new GameEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewGame
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty GameSessionEntity objects.</summary>
	[Serializable]
	public partial class GameSessionEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public GameSessionEntityFactory() : base("GameSessionEntity", Obymobi.Data.EntityType.GameSessionEntity) { }

		/// <summary>Creates a new, empty GameSessionEntity object.</summary>
		/// <returns>A new, empty GameSessionEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new GameSessionEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewGameSession
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty GameSessionReportEntity objects.</summary>
	[Serializable]
	public partial class GameSessionReportEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public GameSessionReportEntityFactory() : base("GameSessionReportEntity", Obymobi.Data.EntityType.GameSessionReportEntity) { }

		/// <summary>Creates a new, empty GameSessionReportEntity object.</summary>
		/// <returns>A new, empty GameSessionReportEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new GameSessionReportEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewGameSessionReport
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty GameSessionReportConfigurationEntity objects.</summary>
	[Serializable]
	public partial class GameSessionReportConfigurationEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public GameSessionReportConfigurationEntityFactory() : base("GameSessionReportConfigurationEntity", Obymobi.Data.EntityType.GameSessionReportConfigurationEntity) { }

		/// <summary>Creates a new, empty GameSessionReportConfigurationEntity object.</summary>
		/// <returns>A new, empty GameSessionReportConfigurationEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new GameSessionReportConfigurationEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewGameSessionReportConfiguration
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty GameSessionReportItemEntity objects.</summary>
	[Serializable]
	public partial class GameSessionReportItemEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public GameSessionReportItemEntityFactory() : base("GameSessionReportItemEntity", Obymobi.Data.EntityType.GameSessionReportItemEntity) { }

		/// <summary>Creates a new, empty GameSessionReportItemEntity object.</summary>
		/// <returns>A new, empty GameSessionReportItemEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new GameSessionReportItemEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewGameSessionReportItem
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty GenericalterationEntity objects.</summary>
	[Serializable]
	public partial class GenericalterationEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public GenericalterationEntityFactory() : base("GenericalterationEntity", Obymobi.Data.EntityType.GenericalterationEntity) { }

		/// <summary>Creates a new, empty GenericalterationEntity object.</summary>
		/// <returns>A new, empty GenericalterationEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new GenericalterationEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewGenericalteration
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty GenericalterationitemEntity objects.</summary>
	[Serializable]
	public partial class GenericalterationitemEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public GenericalterationitemEntityFactory() : base("GenericalterationitemEntity", Obymobi.Data.EntityType.GenericalterationitemEntity) { }

		/// <summary>Creates a new, empty GenericalterationitemEntity object.</summary>
		/// <returns>A new, empty GenericalterationitemEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new GenericalterationitemEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewGenericalterationitem
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty GenericalterationoptionEntity objects.</summary>
	[Serializable]
	public partial class GenericalterationoptionEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public GenericalterationoptionEntityFactory() : base("GenericalterationoptionEntity", Obymobi.Data.EntityType.GenericalterationoptionEntity) { }

		/// <summary>Creates a new, empty GenericalterationoptionEntity object.</summary>
		/// <returns>A new, empty GenericalterationoptionEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new GenericalterationoptionEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewGenericalterationoption
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty GenericcategoryEntity objects.</summary>
	[Serializable]
	public partial class GenericcategoryEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public GenericcategoryEntityFactory() : base("GenericcategoryEntity", Obymobi.Data.EntityType.GenericcategoryEntity) { }

		/// <summary>Creates a new, empty GenericcategoryEntity object.</summary>
		/// <returns>A new, empty GenericcategoryEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new GenericcategoryEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewGenericcategory
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty GenericcategoryLanguageEntity objects.</summary>
	[Serializable]
	public partial class GenericcategoryLanguageEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public GenericcategoryLanguageEntityFactory() : base("GenericcategoryLanguageEntity", Obymobi.Data.EntityType.GenericcategoryLanguageEntity) { }

		/// <summary>Creates a new, empty GenericcategoryLanguageEntity object.</summary>
		/// <returns>A new, empty GenericcategoryLanguageEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new GenericcategoryLanguageEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewGenericcategoryLanguage
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty GenericproductEntity objects.</summary>
	[Serializable]
	public partial class GenericproductEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public GenericproductEntityFactory() : base("GenericproductEntity", Obymobi.Data.EntityType.GenericproductEntity) { }

		/// <summary>Creates a new, empty GenericproductEntity object.</summary>
		/// <returns>A new, empty GenericproductEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new GenericproductEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewGenericproduct
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty GenericproductGenericalterationEntity objects.</summary>
	[Serializable]
	public partial class GenericproductGenericalterationEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public GenericproductGenericalterationEntityFactory() : base("GenericproductGenericalterationEntity", Obymobi.Data.EntityType.GenericproductGenericalterationEntity) { }

		/// <summary>Creates a new, empty GenericproductGenericalterationEntity object.</summary>
		/// <returns>A new, empty GenericproductGenericalterationEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new GenericproductGenericalterationEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewGenericproductGenericalteration
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty GenericproductLanguageEntity objects.</summary>
	[Serializable]
	public partial class GenericproductLanguageEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public GenericproductLanguageEntityFactory() : base("GenericproductLanguageEntity", Obymobi.Data.EntityType.GenericproductLanguageEntity) { }

		/// <summary>Creates a new, empty GenericproductLanguageEntity object.</summary>
		/// <returns>A new, empty GenericproductLanguageEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new GenericproductLanguageEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewGenericproductLanguage
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty GuestInformationEntity objects.</summary>
	[Serializable]
	public partial class GuestInformationEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public GuestInformationEntityFactory() : base("GuestInformationEntity", Obymobi.Data.EntityType.GuestInformationEntity) { }

		/// <summary>Creates a new, empty GuestInformationEntity object.</summary>
		/// <returns>A new, empty GuestInformationEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new GuestInformationEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewGuestInformation
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty IcrtouchprintermappingEntity objects.</summary>
	[Serializable]
	public partial class IcrtouchprintermappingEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public IcrtouchprintermappingEntityFactory() : base("IcrtouchprintermappingEntity", Obymobi.Data.EntityType.IcrtouchprintermappingEntity) { }

		/// <summary>Creates a new, empty IcrtouchprintermappingEntity object.</summary>
		/// <returns>A new, empty IcrtouchprintermappingEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new IcrtouchprintermappingEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewIcrtouchprintermapping
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty IcrtouchprintermappingDeliverypointEntity objects.</summary>
	[Serializable]
	public partial class IcrtouchprintermappingDeliverypointEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public IcrtouchprintermappingDeliverypointEntityFactory() : base("IcrtouchprintermappingDeliverypointEntity", Obymobi.Data.EntityType.IcrtouchprintermappingDeliverypointEntity) { }

		/// <summary>Creates a new, empty IcrtouchprintermappingDeliverypointEntity object.</summary>
		/// <returns>A new, empty IcrtouchprintermappingDeliverypointEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new IcrtouchprintermappingDeliverypointEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewIcrtouchprintermappingDeliverypoint
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty IncomingSmsEntity objects.</summary>
	[Serializable]
	public partial class IncomingSmsEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public IncomingSmsEntityFactory() : base("IncomingSmsEntity", Obymobi.Data.EntityType.IncomingSmsEntity) { }

		/// <summary>Creates a new, empty IncomingSmsEntity object.</summary>
		/// <returns>A new, empty IncomingSmsEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new IncomingSmsEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewIncomingSms
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty InfraredCommandEntity objects.</summary>
	[Serializable]
	public partial class InfraredCommandEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public InfraredCommandEntityFactory() : base("InfraredCommandEntity", Obymobi.Data.EntityType.InfraredCommandEntity) { }

		/// <summary>Creates a new, empty InfraredCommandEntity object.</summary>
		/// <returns>A new, empty InfraredCommandEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new InfraredCommandEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewInfraredCommand
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty InfraredConfigurationEntity objects.</summary>
	[Serializable]
	public partial class InfraredConfigurationEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public InfraredConfigurationEntityFactory() : base("InfraredConfigurationEntity", Obymobi.Data.EntityType.InfraredConfigurationEntity) { }

		/// <summary>Creates a new, empty InfraredConfigurationEntity object.</summary>
		/// <returns>A new, empty InfraredConfigurationEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new InfraredConfigurationEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewInfraredConfiguration
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty LanguageEntity objects.</summary>
	[Serializable]
	public partial class LanguageEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public LanguageEntityFactory() : base("LanguageEntity", Obymobi.Data.EntityType.LanguageEntity) { }

		/// <summary>Creates a new, empty LanguageEntity object.</summary>
		/// <returns>A new, empty LanguageEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new LanguageEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewLanguage
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty LicensedModuleEntity objects.</summary>
	[Serializable]
	public partial class LicensedModuleEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public LicensedModuleEntityFactory() : base("LicensedModuleEntity", Obymobi.Data.EntityType.LicensedModuleEntity) { }

		/// <summary>Creates a new, empty LicensedModuleEntity object.</summary>
		/// <returns>A new, empty LicensedModuleEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new LicensedModuleEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewLicensedModule
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty LicensedUIElementEntity objects.</summary>
	[Serializable]
	public partial class LicensedUIElementEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public LicensedUIElementEntityFactory() : base("LicensedUIElementEntity", Obymobi.Data.EntityType.LicensedUIElementEntity) { }

		/// <summary>Creates a new, empty LicensedUIElementEntity object.</summary>
		/// <returns>A new, empty LicensedUIElementEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new LicensedUIElementEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewLicensedUIElement
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty LicensedUIElementSubPanelEntity objects.</summary>
	[Serializable]
	public partial class LicensedUIElementSubPanelEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public LicensedUIElementSubPanelEntityFactory() : base("LicensedUIElementSubPanelEntity", Obymobi.Data.EntityType.LicensedUIElementSubPanelEntity) { }

		/// <summary>Creates a new, empty LicensedUIElementSubPanelEntity object.</summary>
		/// <returns>A new, empty LicensedUIElementSubPanelEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new LicensedUIElementSubPanelEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewLicensedUIElementSubPanel
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty MapEntity objects.</summary>
	[Serializable]
	public partial class MapEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public MapEntityFactory() : base("MapEntity", Obymobi.Data.EntityType.MapEntity) { }

		/// <summary>Creates a new, empty MapEntity object.</summary>
		/// <returns>A new, empty MapEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new MapEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewMap
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty MapPointOfInterestEntity objects.</summary>
	[Serializable]
	public partial class MapPointOfInterestEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public MapPointOfInterestEntityFactory() : base("MapPointOfInterestEntity", Obymobi.Data.EntityType.MapPointOfInterestEntity) { }

		/// <summary>Creates a new, empty MapPointOfInterestEntity object.</summary>
		/// <returns>A new, empty MapPointOfInterestEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new MapPointOfInterestEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewMapPointOfInterest
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty MediaEntity objects.</summary>
	[Serializable]
	public partial class MediaEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public MediaEntityFactory() : base("MediaEntity", Obymobi.Data.EntityType.MediaEntity) { }

		/// <summary>Creates a new, empty MediaEntity object.</summary>
		/// <returns>A new, empty MediaEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new MediaEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewMedia
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty MediaCultureEntity objects.</summary>
	[Serializable]
	public partial class MediaCultureEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public MediaCultureEntityFactory() : base("MediaCultureEntity", Obymobi.Data.EntityType.MediaCultureEntity) { }

		/// <summary>Creates a new, empty MediaCultureEntity object.</summary>
		/// <returns>A new, empty MediaCultureEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new MediaCultureEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewMediaCulture
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty MediaLanguageEntity objects.</summary>
	[Serializable]
	public partial class MediaLanguageEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public MediaLanguageEntityFactory() : base("MediaLanguageEntity", Obymobi.Data.EntityType.MediaLanguageEntity) { }

		/// <summary>Creates a new, empty MediaLanguageEntity object.</summary>
		/// <returns>A new, empty MediaLanguageEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new MediaLanguageEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewMediaLanguage
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty MediaProcessingTaskEntity objects.</summary>
	[Serializable]
	public partial class MediaProcessingTaskEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public MediaProcessingTaskEntityFactory() : base("MediaProcessingTaskEntity", Obymobi.Data.EntityType.MediaProcessingTaskEntity) { }

		/// <summary>Creates a new, empty MediaProcessingTaskEntity object.</summary>
		/// <returns>A new, empty MediaProcessingTaskEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new MediaProcessingTaskEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewMediaProcessingTask
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty MediaRatioTypeMediaEntity objects.</summary>
	[Serializable]
	public partial class MediaRatioTypeMediaEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public MediaRatioTypeMediaEntityFactory() : base("MediaRatioTypeMediaEntity", Obymobi.Data.EntityType.MediaRatioTypeMediaEntity) { }

		/// <summary>Creates a new, empty MediaRatioTypeMediaEntity object.</summary>
		/// <returns>A new, empty MediaRatioTypeMediaEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new MediaRatioTypeMediaEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewMediaRatioTypeMedia
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty MediaRatioTypeMediaFileEntity objects.</summary>
	[Serializable]
	public partial class MediaRatioTypeMediaFileEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public MediaRatioTypeMediaFileEntityFactory() : base("MediaRatioTypeMediaFileEntity", Obymobi.Data.EntityType.MediaRatioTypeMediaFileEntity) { }

		/// <summary>Creates a new, empty MediaRatioTypeMediaFileEntity object.</summary>
		/// <returns>A new, empty MediaRatioTypeMediaFileEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new MediaRatioTypeMediaFileEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewMediaRatioTypeMediaFile
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty MediaRelationshipEntity objects.</summary>
	[Serializable]
	public partial class MediaRelationshipEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public MediaRelationshipEntityFactory() : base("MediaRelationshipEntity", Obymobi.Data.EntityType.MediaRelationshipEntity) { }

		/// <summary>Creates a new, empty MediaRelationshipEntity object.</summary>
		/// <returns>A new, empty MediaRelationshipEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new MediaRelationshipEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewMediaRelationship
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty MenuEntity objects.</summary>
	[Serializable]
	public partial class MenuEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public MenuEntityFactory() : base("MenuEntity", Obymobi.Data.EntityType.MenuEntity) { }

		/// <summary>Creates a new, empty MenuEntity object.</summary>
		/// <returns>A new, empty MenuEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new MenuEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewMenu
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty MessageEntity objects.</summary>
	[Serializable]
	public partial class MessageEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public MessageEntityFactory() : base("MessageEntity", Obymobi.Data.EntityType.MessageEntity) { }

		/// <summary>Creates a new, empty MessageEntity object.</summary>
		/// <returns>A new, empty MessageEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new MessageEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewMessage
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty MessagegroupEntity objects.</summary>
	[Serializable]
	public partial class MessagegroupEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public MessagegroupEntityFactory() : base("MessagegroupEntity", Obymobi.Data.EntityType.MessagegroupEntity) { }

		/// <summary>Creates a new, empty MessagegroupEntity object.</summary>
		/// <returns>A new, empty MessagegroupEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new MessagegroupEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewMessagegroup
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty MessagegroupDeliverypointEntity objects.</summary>
	[Serializable]
	public partial class MessagegroupDeliverypointEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public MessagegroupDeliverypointEntityFactory() : base("MessagegroupDeliverypointEntity", Obymobi.Data.EntityType.MessagegroupDeliverypointEntity) { }

		/// <summary>Creates a new, empty MessagegroupDeliverypointEntity object.</summary>
		/// <returns>A new, empty MessagegroupDeliverypointEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new MessagegroupDeliverypointEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewMessagegroupDeliverypoint
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty MessageRecipientEntity objects.</summary>
	[Serializable]
	public partial class MessageRecipientEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public MessageRecipientEntityFactory() : base("MessageRecipientEntity", Obymobi.Data.EntityType.MessageRecipientEntity) { }

		/// <summary>Creates a new, empty MessageRecipientEntity object.</summary>
		/// <returns>A new, empty MessageRecipientEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new MessageRecipientEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewMessageRecipient
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty MessageTemplateEntity objects.</summary>
	[Serializable]
	public partial class MessageTemplateEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public MessageTemplateEntityFactory() : base("MessageTemplateEntity", Obymobi.Data.EntityType.MessageTemplateEntity) { }

		/// <summary>Creates a new, empty MessageTemplateEntity object.</summary>
		/// <returns>A new, empty MessageTemplateEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new MessageTemplateEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewMessageTemplate
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty MessageTemplateCategoryEntity objects.</summary>
	[Serializable]
	public partial class MessageTemplateCategoryEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public MessageTemplateCategoryEntityFactory() : base("MessageTemplateCategoryEntity", Obymobi.Data.EntityType.MessageTemplateCategoryEntity) { }

		/// <summary>Creates a new, empty MessageTemplateCategoryEntity object.</summary>
		/// <returns>A new, empty MessageTemplateCategoryEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new MessageTemplateCategoryEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewMessageTemplateCategory
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty MessageTemplateCategoryMessageTemplateEntity objects.</summary>
	[Serializable]
	public partial class MessageTemplateCategoryMessageTemplateEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public MessageTemplateCategoryMessageTemplateEntityFactory() : base("MessageTemplateCategoryMessageTemplateEntity", Obymobi.Data.EntityType.MessageTemplateCategoryMessageTemplateEntity) { }

		/// <summary>Creates a new, empty MessageTemplateCategoryMessageTemplateEntity object.</summary>
		/// <returns>A new, empty MessageTemplateCategoryMessageTemplateEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new MessageTemplateCategoryMessageTemplateEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewMessageTemplateCategoryMessageTemplate
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty ModuleEntity objects.</summary>
	[Serializable]
	public partial class ModuleEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public ModuleEntityFactory() : base("ModuleEntity", Obymobi.Data.EntityType.ModuleEntity) { }

		/// <summary>Creates a new, empty ModuleEntity object.</summary>
		/// <returns>A new, empty ModuleEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new ModuleEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewModule
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty NetmessageEntity objects.</summary>
	[Serializable]
	public partial class NetmessageEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public NetmessageEntityFactory() : base("NetmessageEntity", Obymobi.Data.EntityType.NetmessageEntity) { }

		/// <summary>Creates a new, empty NetmessageEntity object.</summary>
		/// <returns>A new, empty NetmessageEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new NetmessageEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewNetmessage
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty OptInEntity objects.</summary>
	[Serializable]
	public partial class OptInEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public OptInEntityFactory() : base("OptInEntity", Obymobi.Data.EntityType.OptInEntity) { }

		/// <summary>Creates a new, empty OptInEntity object.</summary>
		/// <returns>A new, empty OptInEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new OptInEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewOptIn
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty OrderEntity objects.</summary>
	[Serializable]
	public partial class OrderEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public OrderEntityFactory() : base("OrderEntity", Obymobi.Data.EntityType.OrderEntity) { }

		/// <summary>Creates a new, empty OrderEntity object.</summary>
		/// <returns>A new, empty OrderEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new OrderEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewOrder
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty OrderHourEntity objects.</summary>
	[Serializable]
	public partial class OrderHourEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public OrderHourEntityFactory() : base("OrderHourEntity", Obymobi.Data.EntityType.OrderHourEntity) { }

		/// <summary>Creates a new, empty OrderHourEntity object.</summary>
		/// <returns>A new, empty OrderHourEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new OrderHourEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewOrderHour
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty OrderitemEntity objects.</summary>
	[Serializable]
	public partial class OrderitemEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public OrderitemEntityFactory() : base("OrderitemEntity", Obymobi.Data.EntityType.OrderitemEntity) { }

		/// <summary>Creates a new, empty OrderitemEntity object.</summary>
		/// <returns>A new, empty OrderitemEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new OrderitemEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewOrderitem
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty OrderitemAlterationitemEntity objects.</summary>
	[Serializable]
	public partial class OrderitemAlterationitemEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public OrderitemAlterationitemEntityFactory() : base("OrderitemAlterationitemEntity", Obymobi.Data.EntityType.OrderitemAlterationitemEntity) { }

		/// <summary>Creates a new, empty OrderitemAlterationitemEntity object.</summary>
		/// <returns>A new, empty OrderitemAlterationitemEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new OrderitemAlterationitemEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewOrderitemAlterationitem
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty OrderitemAlterationitemTagEntity objects.</summary>
	[Serializable]
	public partial class OrderitemAlterationitemTagEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public OrderitemAlterationitemTagEntityFactory() : base("OrderitemAlterationitemTagEntity", Obymobi.Data.EntityType.OrderitemAlterationitemTagEntity) { }

		/// <summary>Creates a new, empty OrderitemAlterationitemTagEntity object.</summary>
		/// <returns>A new, empty OrderitemAlterationitemTagEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new OrderitemAlterationitemTagEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewOrderitemAlterationitemTag
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty OrderitemTagEntity objects.</summary>
	[Serializable]
	public partial class OrderitemTagEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public OrderitemTagEntityFactory() : base("OrderitemTagEntity", Obymobi.Data.EntityType.OrderitemTagEntity) { }

		/// <summary>Creates a new, empty OrderitemTagEntity object.</summary>
		/// <returns>A new, empty OrderitemTagEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new OrderitemTagEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewOrderitemTag
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty OrderNotificationLogEntity objects.</summary>
	[Serializable]
	public partial class OrderNotificationLogEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public OrderNotificationLogEntityFactory() : base("OrderNotificationLogEntity", Obymobi.Data.EntityType.OrderNotificationLogEntity) { }

		/// <summary>Creates a new, empty OrderNotificationLogEntity object.</summary>
		/// <returns>A new, empty OrderNotificationLogEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new OrderNotificationLogEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewOrderNotificationLog
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty OrderRoutestephandlerEntity objects.</summary>
	[Serializable]
	public partial class OrderRoutestephandlerEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public OrderRoutestephandlerEntityFactory() : base("OrderRoutestephandlerEntity", Obymobi.Data.EntityType.OrderRoutestephandlerEntity) { }

		/// <summary>Creates a new, empty OrderRoutestephandlerEntity object.</summary>
		/// <returns>A new, empty OrderRoutestephandlerEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new OrderRoutestephandlerEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewOrderRoutestephandler
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty OrderRoutestephandlerHistoryEntity objects.</summary>
	[Serializable]
	public partial class OrderRoutestephandlerHistoryEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public OrderRoutestephandlerHistoryEntityFactory() : base("OrderRoutestephandlerHistoryEntity", Obymobi.Data.EntityType.OrderRoutestephandlerHistoryEntity) { }

		/// <summary>Creates a new, empty OrderRoutestephandlerHistoryEntity object.</summary>
		/// <returns>A new, empty OrderRoutestephandlerHistoryEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new OrderRoutestephandlerHistoryEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewOrderRoutestephandlerHistory
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty OutletEntity objects.</summary>
	[Serializable]
	public partial class OutletEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public OutletEntityFactory() : base("OutletEntity", Obymobi.Data.EntityType.OutletEntity) { }

		/// <summary>Creates a new, empty OutletEntity object.</summary>
		/// <returns>A new, empty OutletEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new OutletEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewOutlet
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty OutletOperationalStateEntity objects.</summary>
	[Serializable]
	public partial class OutletOperationalStateEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public OutletOperationalStateEntityFactory() : base("OutletOperationalStateEntity", Obymobi.Data.EntityType.OutletOperationalStateEntity) { }

		/// <summary>Creates a new, empty OutletOperationalStateEntity object.</summary>
		/// <returns>A new, empty OutletOperationalStateEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new OutletOperationalStateEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewOutletOperationalState
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty OutletSellerInformationEntity objects.</summary>
	[Serializable]
	public partial class OutletSellerInformationEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public OutletSellerInformationEntityFactory() : base("OutletSellerInformationEntity", Obymobi.Data.EntityType.OutletSellerInformationEntity) { }

		/// <summary>Creates a new, empty OutletSellerInformationEntity object.</summary>
		/// <returns>A new, empty OutletSellerInformationEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new OutletSellerInformationEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewOutletSellerInformation
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty PageEntity objects.</summary>
	[Serializable]
	public partial class PageEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public PageEntityFactory() : base("PageEntity", Obymobi.Data.EntityType.PageEntity) { }

		/// <summary>Creates a new, empty PageEntity object.</summary>
		/// <returns>A new, empty PageEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new PageEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewPage
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty PageElementEntity objects.</summary>
	[Serializable]
	public partial class PageElementEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public PageElementEntityFactory() : base("PageElementEntity", Obymobi.Data.EntityType.PageElementEntity) { }

		/// <summary>Creates a new, empty PageElementEntity object.</summary>
		/// <returns>A new, empty PageElementEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new PageElementEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewPageElement
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty PageLanguageEntity objects.</summary>
	[Serializable]
	public partial class PageLanguageEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public PageLanguageEntityFactory() : base("PageLanguageEntity", Obymobi.Data.EntityType.PageLanguageEntity) { }

		/// <summary>Creates a new, empty PageLanguageEntity object.</summary>
		/// <returns>A new, empty PageLanguageEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new PageLanguageEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewPageLanguage
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty PageTemplateEntity objects.</summary>
	[Serializable]
	public partial class PageTemplateEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public PageTemplateEntityFactory() : base("PageTemplateEntity", Obymobi.Data.EntityType.PageTemplateEntity) { }

		/// <summary>Creates a new, empty PageTemplateEntity object.</summary>
		/// <returns>A new, empty PageTemplateEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new PageTemplateEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewPageTemplate
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty PageTemplateElementEntity objects.</summary>
	[Serializable]
	public partial class PageTemplateElementEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public PageTemplateElementEntityFactory() : base("PageTemplateElementEntity", Obymobi.Data.EntityType.PageTemplateElementEntity) { }

		/// <summary>Creates a new, empty PageTemplateElementEntity object.</summary>
		/// <returns>A new, empty PageTemplateElementEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new PageTemplateElementEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewPageTemplateElement
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty PageTemplateLanguageEntity objects.</summary>
	[Serializable]
	public partial class PageTemplateLanguageEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public PageTemplateLanguageEntityFactory() : base("PageTemplateLanguageEntity", Obymobi.Data.EntityType.PageTemplateLanguageEntity) { }

		/// <summary>Creates a new, empty PageTemplateLanguageEntity object.</summary>
		/// <returns>A new, empty PageTemplateLanguageEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new PageTemplateLanguageEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewPageTemplateLanguage
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty PaymentIntegrationConfigurationEntity objects.</summary>
	[Serializable]
	public partial class PaymentIntegrationConfigurationEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public PaymentIntegrationConfigurationEntityFactory() : base("PaymentIntegrationConfigurationEntity", Obymobi.Data.EntityType.PaymentIntegrationConfigurationEntity) { }

		/// <summary>Creates a new, empty PaymentIntegrationConfigurationEntity object.</summary>
		/// <returns>A new, empty PaymentIntegrationConfigurationEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new PaymentIntegrationConfigurationEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewPaymentIntegrationConfiguration
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty PaymentProviderEntity objects.</summary>
	[Serializable]
	public partial class PaymentProviderEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public PaymentProviderEntityFactory() : base("PaymentProviderEntity", Obymobi.Data.EntityType.PaymentProviderEntity) { }

		/// <summary>Creates a new, empty PaymentProviderEntity object.</summary>
		/// <returns>A new, empty PaymentProviderEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new PaymentProviderEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewPaymentProvider
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty PaymentProviderCompanyEntity objects.</summary>
	[Serializable]
	public partial class PaymentProviderCompanyEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public PaymentProviderCompanyEntityFactory() : base("PaymentProviderCompanyEntity", Obymobi.Data.EntityType.PaymentProviderCompanyEntity) { }

		/// <summary>Creates a new, empty PaymentProviderCompanyEntity object.</summary>
		/// <returns>A new, empty PaymentProviderCompanyEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new PaymentProviderCompanyEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewPaymentProviderCompany
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty PaymentTransactionEntity objects.</summary>
	[Serializable]
	public partial class PaymentTransactionEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public PaymentTransactionEntityFactory() : base("PaymentTransactionEntity", Obymobi.Data.EntityType.PaymentTransactionEntity) { }

		/// <summary>Creates a new, empty PaymentTransactionEntity object.</summary>
		/// <returns>A new, empty PaymentTransactionEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new PaymentTransactionEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewPaymentTransaction
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty PaymentTransactionLogEntity objects.</summary>
	[Serializable]
	public partial class PaymentTransactionLogEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public PaymentTransactionLogEntityFactory() : base("PaymentTransactionLogEntity", Obymobi.Data.EntityType.PaymentTransactionLogEntity) { }

		/// <summary>Creates a new, empty PaymentTransactionLogEntity object.</summary>
		/// <returns>A new, empty PaymentTransactionLogEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new PaymentTransactionLogEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewPaymentTransactionLog
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty PaymentTransactionSplitEntity objects.</summary>
	[Serializable]
	public partial class PaymentTransactionSplitEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public PaymentTransactionSplitEntityFactory() : base("PaymentTransactionSplitEntity", Obymobi.Data.EntityType.PaymentTransactionSplitEntity) { }

		/// <summary>Creates a new, empty PaymentTransactionSplitEntity object.</summary>
		/// <returns>A new, empty PaymentTransactionSplitEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new PaymentTransactionSplitEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewPaymentTransactionSplit
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty PmsActionRuleEntity objects.</summary>
	[Serializable]
	public partial class PmsActionRuleEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public PmsActionRuleEntityFactory() : base("PmsActionRuleEntity", Obymobi.Data.EntityType.PmsActionRuleEntity) { }

		/// <summary>Creates a new, empty PmsActionRuleEntity object.</summary>
		/// <returns>A new, empty PmsActionRuleEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new PmsActionRuleEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewPmsActionRule
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty PmsReportColumnEntity objects.</summary>
	[Serializable]
	public partial class PmsReportColumnEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public PmsReportColumnEntityFactory() : base("PmsReportColumnEntity", Obymobi.Data.EntityType.PmsReportColumnEntity) { }

		/// <summary>Creates a new, empty PmsReportColumnEntity object.</summary>
		/// <returns>A new, empty PmsReportColumnEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new PmsReportColumnEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewPmsReportColumn
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty PmsReportConfigurationEntity objects.</summary>
	[Serializable]
	public partial class PmsReportConfigurationEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public PmsReportConfigurationEntityFactory() : base("PmsReportConfigurationEntity", Obymobi.Data.EntityType.PmsReportConfigurationEntity) { }

		/// <summary>Creates a new, empty PmsReportConfigurationEntity object.</summary>
		/// <returns>A new, empty PmsReportConfigurationEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new PmsReportConfigurationEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewPmsReportConfiguration
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty PmsRuleEntity objects.</summary>
	[Serializable]
	public partial class PmsRuleEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public PmsRuleEntityFactory() : base("PmsRuleEntity", Obymobi.Data.EntityType.PmsRuleEntity) { }

		/// <summary>Creates a new, empty PmsRuleEntity object.</summary>
		/// <returns>A new, empty PmsRuleEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new PmsRuleEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewPmsRule
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty PointOfInterestEntity objects.</summary>
	[Serializable]
	public partial class PointOfInterestEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public PointOfInterestEntityFactory() : base("PointOfInterestEntity", Obymobi.Data.EntityType.PointOfInterestEntity) { }

		/// <summary>Creates a new, empty PointOfInterestEntity object.</summary>
		/// <returns>A new, empty PointOfInterestEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new PointOfInterestEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewPointOfInterest
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty PointOfInterestAmenityEntity objects.</summary>
	[Serializable]
	public partial class PointOfInterestAmenityEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public PointOfInterestAmenityEntityFactory() : base("PointOfInterestAmenityEntity", Obymobi.Data.EntityType.PointOfInterestAmenityEntity) { }

		/// <summary>Creates a new, empty PointOfInterestAmenityEntity object.</summary>
		/// <returns>A new, empty PointOfInterestAmenityEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new PointOfInterestAmenityEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewPointOfInterestAmenity
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty PointOfInterestLanguageEntity objects.</summary>
	[Serializable]
	public partial class PointOfInterestLanguageEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public PointOfInterestLanguageEntityFactory() : base("PointOfInterestLanguageEntity", Obymobi.Data.EntityType.PointOfInterestLanguageEntity) { }

		/// <summary>Creates a new, empty PointOfInterestLanguageEntity object.</summary>
		/// <returns>A new, empty PointOfInterestLanguageEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new PointOfInterestLanguageEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewPointOfInterestLanguage
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty PointOfInterestVenueCategoryEntity objects.</summary>
	[Serializable]
	public partial class PointOfInterestVenueCategoryEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public PointOfInterestVenueCategoryEntityFactory() : base("PointOfInterestVenueCategoryEntity", Obymobi.Data.EntityType.PointOfInterestVenueCategoryEntity) { }

		/// <summary>Creates a new, empty PointOfInterestVenueCategoryEntity object.</summary>
		/// <returns>A new, empty PointOfInterestVenueCategoryEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new PointOfInterestVenueCategoryEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewPointOfInterestVenueCategory
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty PosalterationEntity objects.</summary>
	[Serializable]
	public partial class PosalterationEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public PosalterationEntityFactory() : base("PosalterationEntity", Obymobi.Data.EntityType.PosalterationEntity) { }

		/// <summary>Creates a new, empty PosalterationEntity object.</summary>
		/// <returns>A new, empty PosalterationEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new PosalterationEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewPosalteration
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty PosalterationitemEntity objects.</summary>
	[Serializable]
	public partial class PosalterationitemEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public PosalterationitemEntityFactory() : base("PosalterationitemEntity", Obymobi.Data.EntityType.PosalterationitemEntity) { }

		/// <summary>Creates a new, empty PosalterationitemEntity object.</summary>
		/// <returns>A new, empty PosalterationitemEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new PosalterationitemEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewPosalterationitem
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty PosalterationoptionEntity objects.</summary>
	[Serializable]
	public partial class PosalterationoptionEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public PosalterationoptionEntityFactory() : base("PosalterationoptionEntity", Obymobi.Data.EntityType.PosalterationoptionEntity) { }

		/// <summary>Creates a new, empty PosalterationoptionEntity object.</summary>
		/// <returns>A new, empty PosalterationoptionEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new PosalterationoptionEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewPosalterationoption
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty PoscategoryEntity objects.</summary>
	[Serializable]
	public partial class PoscategoryEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public PoscategoryEntityFactory() : base("PoscategoryEntity", Obymobi.Data.EntityType.PoscategoryEntity) { }

		/// <summary>Creates a new, empty PoscategoryEntity object.</summary>
		/// <returns>A new, empty PoscategoryEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new PoscategoryEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewPoscategory
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty PosdeliverypointEntity objects.</summary>
	[Serializable]
	public partial class PosdeliverypointEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public PosdeliverypointEntityFactory() : base("PosdeliverypointEntity", Obymobi.Data.EntityType.PosdeliverypointEntity) { }

		/// <summary>Creates a new, empty PosdeliverypointEntity object.</summary>
		/// <returns>A new, empty PosdeliverypointEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new PosdeliverypointEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewPosdeliverypoint
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty PosdeliverypointgroupEntity objects.</summary>
	[Serializable]
	public partial class PosdeliverypointgroupEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public PosdeliverypointgroupEntityFactory() : base("PosdeliverypointgroupEntity", Obymobi.Data.EntityType.PosdeliverypointgroupEntity) { }

		/// <summary>Creates a new, empty PosdeliverypointgroupEntity object.</summary>
		/// <returns>A new, empty PosdeliverypointgroupEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new PosdeliverypointgroupEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewPosdeliverypointgroup
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty PospaymentmethodEntity objects.</summary>
	[Serializable]
	public partial class PospaymentmethodEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public PospaymentmethodEntityFactory() : base("PospaymentmethodEntity", Obymobi.Data.EntityType.PospaymentmethodEntity) { }

		/// <summary>Creates a new, empty PospaymentmethodEntity object.</summary>
		/// <returns>A new, empty PospaymentmethodEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new PospaymentmethodEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewPospaymentmethod
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty PosproductEntity objects.</summary>
	[Serializable]
	public partial class PosproductEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public PosproductEntityFactory() : base("PosproductEntity", Obymobi.Data.EntityType.PosproductEntity) { }

		/// <summary>Creates a new, empty PosproductEntity object.</summary>
		/// <returns>A new, empty PosproductEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new PosproductEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewPosproduct
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty PosproductPosalterationEntity objects.</summary>
	[Serializable]
	public partial class PosproductPosalterationEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public PosproductPosalterationEntityFactory() : base("PosproductPosalterationEntity", Obymobi.Data.EntityType.PosproductPosalterationEntity) { }

		/// <summary>Creates a new, empty PosproductPosalterationEntity object.</summary>
		/// <returns>A new, empty PosproductPosalterationEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new PosproductPosalterationEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewPosproductPosalteration
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty PriceLevelEntity objects.</summary>
	[Serializable]
	public partial class PriceLevelEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public PriceLevelEntityFactory() : base("PriceLevelEntity", Obymobi.Data.EntityType.PriceLevelEntity) { }

		/// <summary>Creates a new, empty PriceLevelEntity object.</summary>
		/// <returns>A new, empty PriceLevelEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new PriceLevelEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewPriceLevel
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty PriceLevelItemEntity objects.</summary>
	[Serializable]
	public partial class PriceLevelItemEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public PriceLevelItemEntityFactory() : base("PriceLevelItemEntity", Obymobi.Data.EntityType.PriceLevelItemEntity) { }

		/// <summary>Creates a new, empty PriceLevelItemEntity object.</summary>
		/// <returns>A new, empty PriceLevelItemEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new PriceLevelItemEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewPriceLevelItem
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty PriceScheduleEntity objects.</summary>
	[Serializable]
	public partial class PriceScheduleEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public PriceScheduleEntityFactory() : base("PriceScheduleEntity", Obymobi.Data.EntityType.PriceScheduleEntity) { }

		/// <summary>Creates a new, empty PriceScheduleEntity object.</summary>
		/// <returns>A new, empty PriceScheduleEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new PriceScheduleEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewPriceSchedule
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty PriceScheduleItemEntity objects.</summary>
	[Serializable]
	public partial class PriceScheduleItemEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public PriceScheduleItemEntityFactory() : base("PriceScheduleItemEntity", Obymobi.Data.EntityType.PriceScheduleItemEntity) { }

		/// <summary>Creates a new, empty PriceScheduleItemEntity object.</summary>
		/// <returns>A new, empty PriceScheduleItemEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new PriceScheduleItemEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewPriceScheduleItem
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty PriceScheduleItemOccurrenceEntity objects.</summary>
	[Serializable]
	public partial class PriceScheduleItemOccurrenceEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public PriceScheduleItemOccurrenceEntityFactory() : base("PriceScheduleItemOccurrenceEntity", Obymobi.Data.EntityType.PriceScheduleItemOccurrenceEntity) { }

		/// <summary>Creates a new, empty PriceScheduleItemOccurrenceEntity object.</summary>
		/// <returns>A new, empty PriceScheduleItemOccurrenceEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new PriceScheduleItemOccurrenceEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewPriceScheduleItemOccurrence
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty ProductEntity objects.</summary>
	[Serializable]
	public partial class ProductEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public ProductEntityFactory() : base("ProductEntity", Obymobi.Data.EntityType.ProductEntity) { }

		/// <summary>Creates a new, empty ProductEntity object.</summary>
		/// <returns>A new, empty ProductEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new ProductEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewProduct
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty ProductAlterationEntity objects.</summary>
	[Serializable]
	public partial class ProductAlterationEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public ProductAlterationEntityFactory() : base("ProductAlterationEntity", Obymobi.Data.EntityType.ProductAlterationEntity) { }

		/// <summary>Creates a new, empty ProductAlterationEntity object.</summary>
		/// <returns>A new, empty ProductAlterationEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new ProductAlterationEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewProductAlteration
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty ProductAttachmentEntity objects.</summary>
	[Serializable]
	public partial class ProductAttachmentEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public ProductAttachmentEntityFactory() : base("ProductAttachmentEntity", Obymobi.Data.EntityType.ProductAttachmentEntity) { }

		/// <summary>Creates a new, empty ProductAttachmentEntity object.</summary>
		/// <returns>A new, empty ProductAttachmentEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new ProductAttachmentEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewProductAttachment
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty ProductCategoryEntity objects.</summary>
	[Serializable]
	public partial class ProductCategoryEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public ProductCategoryEntityFactory() : base("ProductCategoryEntity", Obymobi.Data.EntityType.ProductCategoryEntity) { }

		/// <summary>Creates a new, empty ProductCategoryEntity object.</summary>
		/// <returns>A new, empty ProductCategoryEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new ProductCategoryEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewProductCategory
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty ProductCategorySuggestionEntity objects.</summary>
	[Serializable]
	public partial class ProductCategorySuggestionEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public ProductCategorySuggestionEntityFactory() : base("ProductCategorySuggestionEntity", Obymobi.Data.EntityType.ProductCategorySuggestionEntity) { }

		/// <summary>Creates a new, empty ProductCategorySuggestionEntity object.</summary>
		/// <returns>A new, empty ProductCategorySuggestionEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new ProductCategorySuggestionEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewProductCategorySuggestion
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty ProductCategoryTagEntity objects.</summary>
	[Serializable]
	public partial class ProductCategoryTagEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public ProductCategoryTagEntityFactory() : base("ProductCategoryTagEntity", Obymobi.Data.EntityType.ProductCategoryTagEntity) { }

		/// <summary>Creates a new, empty ProductCategoryTagEntity object.</summary>
		/// <returns>A new, empty ProductCategoryTagEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new ProductCategoryTagEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewProductCategoryTag
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty ProductgroupEntity objects.</summary>
	[Serializable]
	public partial class ProductgroupEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public ProductgroupEntityFactory() : base("ProductgroupEntity", Obymobi.Data.EntityType.ProductgroupEntity) { }

		/// <summary>Creates a new, empty ProductgroupEntity object.</summary>
		/// <returns>A new, empty ProductgroupEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new ProductgroupEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewProductgroup
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty ProductgroupItemEntity objects.</summary>
	[Serializable]
	public partial class ProductgroupItemEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public ProductgroupItemEntityFactory() : base("ProductgroupItemEntity", Obymobi.Data.EntityType.ProductgroupItemEntity) { }

		/// <summary>Creates a new, empty ProductgroupItemEntity object.</summary>
		/// <returns>A new, empty ProductgroupItemEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new ProductgroupItemEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewProductgroupItem
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty ProductLanguageEntity objects.</summary>
	[Serializable]
	public partial class ProductLanguageEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public ProductLanguageEntityFactory() : base("ProductLanguageEntity", Obymobi.Data.EntityType.ProductLanguageEntity) { }

		/// <summary>Creates a new, empty ProductLanguageEntity object.</summary>
		/// <returns>A new, empty ProductLanguageEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new ProductLanguageEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewProductLanguage
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty ProductSuggestionEntity objects.</summary>
	[Serializable]
	public partial class ProductSuggestionEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public ProductSuggestionEntityFactory() : base("ProductSuggestionEntity", Obymobi.Data.EntityType.ProductSuggestionEntity) { }

		/// <summary>Creates a new, empty ProductSuggestionEntity object.</summary>
		/// <returns>A new, empty ProductSuggestionEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new ProductSuggestionEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewProductSuggestion
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty ProductTagEntity objects.</summary>
	[Serializable]
	public partial class ProductTagEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public ProductTagEntityFactory() : base("ProductTagEntity", Obymobi.Data.EntityType.ProductTagEntity) { }

		/// <summary>Creates a new, empty ProductTagEntity object.</summary>
		/// <returns>A new, empty ProductTagEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new ProductTagEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewProductTag
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty PublishingEntity objects.</summary>
	[Serializable]
	public partial class PublishingEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public PublishingEntityFactory() : base("PublishingEntity", Obymobi.Data.EntityType.PublishingEntity) { }

		/// <summary>Creates a new, empty PublishingEntity object.</summary>
		/// <returns>A new, empty PublishingEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new PublishingEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewPublishing
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty PublishingItemEntity objects.</summary>
	[Serializable]
	public partial class PublishingItemEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public PublishingItemEntityFactory() : base("PublishingItemEntity", Obymobi.Data.EntityType.PublishingItemEntity) { }

		/// <summary>Creates a new, empty PublishingItemEntity object.</summary>
		/// <returns>A new, empty PublishingItemEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new PublishingItemEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewPublishingItem
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty RatingEntity objects.</summary>
	[Serializable]
	public partial class RatingEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public RatingEntityFactory() : base("RatingEntity", Obymobi.Data.EntityType.RatingEntity) { }

		/// <summary>Creates a new, empty RatingEntity object.</summary>
		/// <returns>A new, empty RatingEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new RatingEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewRating
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty ReceiptEntity objects.</summary>
	[Serializable]
	public partial class ReceiptEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public ReceiptEntityFactory() : base("ReceiptEntity", Obymobi.Data.EntityType.ReceiptEntity) { }

		/// <summary>Creates a new, empty ReceiptEntity object.</summary>
		/// <returns>A new, empty ReceiptEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new ReceiptEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewReceipt
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty ReceiptTemplateEntity objects.</summary>
	[Serializable]
	public partial class ReceiptTemplateEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public ReceiptTemplateEntityFactory() : base("ReceiptTemplateEntity", Obymobi.Data.EntityType.ReceiptTemplateEntity) { }

		/// <summary>Creates a new, empty ReceiptTemplateEntity object.</summary>
		/// <returns>A new, empty ReceiptTemplateEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new ReceiptTemplateEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewReceiptTemplate
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty ReferentialConstraintEntity objects.</summary>
	[Serializable]
	public partial class ReferentialConstraintEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public ReferentialConstraintEntityFactory() : base("ReferentialConstraintEntity", Obymobi.Data.EntityType.ReferentialConstraintEntity) { }

		/// <summary>Creates a new, empty ReferentialConstraintEntity object.</summary>
		/// <returns>A new, empty ReferentialConstraintEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new ReferentialConstraintEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewReferentialConstraint
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty ReferentialConstraintCustomEntity objects.</summary>
	[Serializable]
	public partial class ReferentialConstraintCustomEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public ReferentialConstraintCustomEntityFactory() : base("ReferentialConstraintCustomEntity", Obymobi.Data.EntityType.ReferentialConstraintCustomEntity) { }

		/// <summary>Creates a new, empty ReferentialConstraintCustomEntity object.</summary>
		/// <returns>A new, empty ReferentialConstraintCustomEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new ReferentialConstraintCustomEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewReferentialConstraintCustom
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty ReleaseEntity objects.</summary>
	[Serializable]
	public partial class ReleaseEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public ReleaseEntityFactory() : base("ReleaseEntity", Obymobi.Data.EntityType.ReleaseEntity) { }

		/// <summary>Creates a new, empty ReleaseEntity object.</summary>
		/// <returns>A new, empty ReleaseEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new ReleaseEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewRelease
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty ReportProcessingTaskEntity objects.</summary>
	[Serializable]
	public partial class ReportProcessingTaskEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public ReportProcessingTaskEntityFactory() : base("ReportProcessingTaskEntity", Obymobi.Data.EntityType.ReportProcessingTaskEntity) { }

		/// <summary>Creates a new, empty ReportProcessingTaskEntity object.</summary>
		/// <returns>A new, empty ReportProcessingTaskEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new ReportProcessingTaskEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewReportProcessingTask
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty ReportProcessingTaskFileEntity objects.</summary>
	[Serializable]
	public partial class ReportProcessingTaskFileEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public ReportProcessingTaskFileEntityFactory() : base("ReportProcessingTaskFileEntity", Obymobi.Data.EntityType.ReportProcessingTaskFileEntity) { }

		/// <summary>Creates a new, empty ReportProcessingTaskFileEntity object.</summary>
		/// <returns>A new, empty ReportProcessingTaskFileEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new ReportProcessingTaskFileEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewReportProcessingTaskFile
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty ReportProcessingTaskTemplateEntity objects.</summary>
	[Serializable]
	public partial class ReportProcessingTaskTemplateEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public ReportProcessingTaskTemplateEntityFactory() : base("ReportProcessingTaskTemplateEntity", Obymobi.Data.EntityType.ReportProcessingTaskTemplateEntity) { }

		/// <summary>Creates a new, empty ReportProcessingTaskTemplateEntity object.</summary>
		/// <returns>A new, empty ReportProcessingTaskTemplateEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new ReportProcessingTaskTemplateEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewReportProcessingTaskTemplate
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty RequestlogEntity objects.</summary>
	[Serializable]
	public partial class RequestlogEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public RequestlogEntityFactory() : base("RequestlogEntity", Obymobi.Data.EntityType.RequestlogEntity) { }

		/// <summary>Creates a new, empty RequestlogEntity object.</summary>
		/// <returns>A new, empty RequestlogEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new RequestlogEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewRequestlog
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty RoleEntity objects.</summary>
	[Serializable]
	public partial class RoleEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public RoleEntityFactory() : base("RoleEntity", Obymobi.Data.EntityType.RoleEntity) { }

		/// <summary>Creates a new, empty RoleEntity object.</summary>
		/// <returns>A new, empty RoleEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new RoleEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewRole
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty RoleModuleRightsEntity objects.</summary>
	[Serializable]
	public partial class RoleModuleRightsEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public RoleModuleRightsEntityFactory() : base("RoleModuleRightsEntity", Obymobi.Data.EntityType.RoleModuleRightsEntity) { }

		/// <summary>Creates a new, empty RoleModuleRightsEntity object.</summary>
		/// <returns>A new, empty RoleModuleRightsEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new RoleModuleRightsEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewRoleModuleRights
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty RoleUIElementRightsEntity objects.</summary>
	[Serializable]
	public partial class RoleUIElementRightsEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public RoleUIElementRightsEntityFactory() : base("RoleUIElementRightsEntity", Obymobi.Data.EntityType.RoleUIElementRightsEntity) { }

		/// <summary>Creates a new, empty RoleUIElementRightsEntity object.</summary>
		/// <returns>A new, empty RoleUIElementRightsEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new RoleUIElementRightsEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewRoleUIElementRights
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty RoleUIElementSubPanelRightsEntity objects.</summary>
	[Serializable]
	public partial class RoleUIElementSubPanelRightsEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public RoleUIElementSubPanelRightsEntityFactory() : base("RoleUIElementSubPanelRightsEntity", Obymobi.Data.EntityType.RoleUIElementSubPanelRightsEntity) { }

		/// <summary>Creates a new, empty RoleUIElementSubPanelRightsEntity object.</summary>
		/// <returns>A new, empty RoleUIElementSubPanelRightsEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new RoleUIElementSubPanelRightsEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewRoleUIElementSubPanelRights
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty RoomControlAreaEntity objects.</summary>
	[Serializable]
	public partial class RoomControlAreaEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public RoomControlAreaEntityFactory() : base("RoomControlAreaEntity", Obymobi.Data.EntityType.RoomControlAreaEntity) { }

		/// <summary>Creates a new, empty RoomControlAreaEntity object.</summary>
		/// <returns>A new, empty RoomControlAreaEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new RoomControlAreaEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewRoomControlArea
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty RoomControlAreaLanguageEntity objects.</summary>
	[Serializable]
	public partial class RoomControlAreaLanguageEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public RoomControlAreaLanguageEntityFactory() : base("RoomControlAreaLanguageEntity", Obymobi.Data.EntityType.RoomControlAreaLanguageEntity) { }

		/// <summary>Creates a new, empty RoomControlAreaLanguageEntity object.</summary>
		/// <returns>A new, empty RoomControlAreaLanguageEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new RoomControlAreaLanguageEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewRoomControlAreaLanguage
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty RoomControlComponentEntity objects.</summary>
	[Serializable]
	public partial class RoomControlComponentEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public RoomControlComponentEntityFactory() : base("RoomControlComponentEntity", Obymobi.Data.EntityType.RoomControlComponentEntity) { }

		/// <summary>Creates a new, empty RoomControlComponentEntity object.</summary>
		/// <returns>A new, empty RoomControlComponentEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new RoomControlComponentEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewRoomControlComponent
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty RoomControlComponentLanguageEntity objects.</summary>
	[Serializable]
	public partial class RoomControlComponentLanguageEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public RoomControlComponentLanguageEntityFactory() : base("RoomControlComponentLanguageEntity", Obymobi.Data.EntityType.RoomControlComponentLanguageEntity) { }

		/// <summary>Creates a new, empty RoomControlComponentLanguageEntity object.</summary>
		/// <returns>A new, empty RoomControlComponentLanguageEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new RoomControlComponentLanguageEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewRoomControlComponentLanguage
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty RoomControlConfigurationEntity objects.</summary>
	[Serializable]
	public partial class RoomControlConfigurationEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public RoomControlConfigurationEntityFactory() : base("RoomControlConfigurationEntity", Obymobi.Data.EntityType.RoomControlConfigurationEntity) { }

		/// <summary>Creates a new, empty RoomControlConfigurationEntity object.</summary>
		/// <returns>A new, empty RoomControlConfigurationEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new RoomControlConfigurationEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewRoomControlConfiguration
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty RoomControlSectionEntity objects.</summary>
	[Serializable]
	public partial class RoomControlSectionEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public RoomControlSectionEntityFactory() : base("RoomControlSectionEntity", Obymobi.Data.EntityType.RoomControlSectionEntity) { }

		/// <summary>Creates a new, empty RoomControlSectionEntity object.</summary>
		/// <returns>A new, empty RoomControlSectionEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new RoomControlSectionEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewRoomControlSection
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty RoomControlSectionItemEntity objects.</summary>
	[Serializable]
	public partial class RoomControlSectionItemEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public RoomControlSectionItemEntityFactory() : base("RoomControlSectionItemEntity", Obymobi.Data.EntityType.RoomControlSectionItemEntity) { }

		/// <summary>Creates a new, empty RoomControlSectionItemEntity object.</summary>
		/// <returns>A new, empty RoomControlSectionItemEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new RoomControlSectionItemEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewRoomControlSectionItem
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty RoomControlSectionItemLanguageEntity objects.</summary>
	[Serializable]
	public partial class RoomControlSectionItemLanguageEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public RoomControlSectionItemLanguageEntityFactory() : base("RoomControlSectionItemLanguageEntity", Obymobi.Data.EntityType.RoomControlSectionItemLanguageEntity) { }

		/// <summary>Creates a new, empty RoomControlSectionItemLanguageEntity object.</summary>
		/// <returns>A new, empty RoomControlSectionItemLanguageEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new RoomControlSectionItemLanguageEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewRoomControlSectionItemLanguage
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty RoomControlSectionLanguageEntity objects.</summary>
	[Serializable]
	public partial class RoomControlSectionLanguageEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public RoomControlSectionLanguageEntityFactory() : base("RoomControlSectionLanguageEntity", Obymobi.Data.EntityType.RoomControlSectionLanguageEntity) { }

		/// <summary>Creates a new, empty RoomControlSectionLanguageEntity object.</summary>
		/// <returns>A new, empty RoomControlSectionLanguageEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new RoomControlSectionLanguageEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewRoomControlSectionLanguage
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty RoomControlWidgetEntity objects.</summary>
	[Serializable]
	public partial class RoomControlWidgetEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public RoomControlWidgetEntityFactory() : base("RoomControlWidgetEntity", Obymobi.Data.EntityType.RoomControlWidgetEntity) { }

		/// <summary>Creates a new, empty RoomControlWidgetEntity object.</summary>
		/// <returns>A new, empty RoomControlWidgetEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new RoomControlWidgetEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewRoomControlWidget
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty RoomControlWidgetLanguageEntity objects.</summary>
	[Serializable]
	public partial class RoomControlWidgetLanguageEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public RoomControlWidgetLanguageEntityFactory() : base("RoomControlWidgetLanguageEntity", Obymobi.Data.EntityType.RoomControlWidgetLanguageEntity) { }

		/// <summary>Creates a new, empty RoomControlWidgetLanguageEntity object.</summary>
		/// <returns>A new, empty RoomControlWidgetLanguageEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new RoomControlWidgetLanguageEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewRoomControlWidgetLanguage
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty RouteEntity objects.</summary>
	[Serializable]
	public partial class RouteEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public RouteEntityFactory() : base("RouteEntity", Obymobi.Data.EntityType.RouteEntity) { }

		/// <summary>Creates a new, empty RouteEntity object.</summary>
		/// <returns>A new, empty RouteEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new RouteEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewRoute
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty RoutestepEntity objects.</summary>
	[Serializable]
	public partial class RoutestepEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public RoutestepEntityFactory() : base("RoutestepEntity", Obymobi.Data.EntityType.RoutestepEntity) { }

		/// <summary>Creates a new, empty RoutestepEntity object.</summary>
		/// <returns>A new, empty RoutestepEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new RoutestepEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewRoutestep
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty RoutestephandlerEntity objects.</summary>
	[Serializable]
	public partial class RoutestephandlerEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public RoutestephandlerEntityFactory() : base("RoutestephandlerEntity", Obymobi.Data.EntityType.RoutestephandlerEntity) { }

		/// <summary>Creates a new, empty RoutestephandlerEntity object.</summary>
		/// <returns>A new, empty RoutestephandlerEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new RoutestephandlerEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewRoutestephandler
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty ScheduleEntity objects.</summary>
	[Serializable]
	public partial class ScheduleEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public ScheduleEntityFactory() : base("ScheduleEntity", Obymobi.Data.EntityType.ScheduleEntity) { }

		/// <summary>Creates a new, empty ScheduleEntity object.</summary>
		/// <returns>A new, empty ScheduleEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new ScheduleEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewSchedule
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty ScheduledCommandEntity objects.</summary>
	[Serializable]
	public partial class ScheduledCommandEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public ScheduledCommandEntityFactory() : base("ScheduledCommandEntity", Obymobi.Data.EntityType.ScheduledCommandEntity) { }

		/// <summary>Creates a new, empty ScheduledCommandEntity object.</summary>
		/// <returns>A new, empty ScheduledCommandEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new ScheduledCommandEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewScheduledCommand
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty ScheduledCommandTaskEntity objects.</summary>
	[Serializable]
	public partial class ScheduledCommandTaskEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public ScheduledCommandTaskEntityFactory() : base("ScheduledCommandTaskEntity", Obymobi.Data.EntityType.ScheduledCommandTaskEntity) { }

		/// <summary>Creates a new, empty ScheduledCommandTaskEntity object.</summary>
		/// <returns>A new, empty ScheduledCommandTaskEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new ScheduledCommandTaskEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewScheduledCommandTask
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty ScheduledMessageEntity objects.</summary>
	[Serializable]
	public partial class ScheduledMessageEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public ScheduledMessageEntityFactory() : base("ScheduledMessageEntity", Obymobi.Data.EntityType.ScheduledMessageEntity) { }

		/// <summary>Creates a new, empty ScheduledMessageEntity object.</summary>
		/// <returns>A new, empty ScheduledMessageEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new ScheduledMessageEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewScheduledMessage
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty ScheduledMessageHistoryEntity objects.</summary>
	[Serializable]
	public partial class ScheduledMessageHistoryEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public ScheduledMessageHistoryEntityFactory() : base("ScheduledMessageHistoryEntity", Obymobi.Data.EntityType.ScheduledMessageHistoryEntity) { }

		/// <summary>Creates a new, empty ScheduledMessageHistoryEntity object.</summary>
		/// <returns>A new, empty ScheduledMessageHistoryEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new ScheduledMessageHistoryEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewScheduledMessageHistory
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty ScheduledMessageLanguageEntity objects.</summary>
	[Serializable]
	public partial class ScheduledMessageLanguageEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public ScheduledMessageLanguageEntityFactory() : base("ScheduledMessageLanguageEntity", Obymobi.Data.EntityType.ScheduledMessageLanguageEntity) { }

		/// <summary>Creates a new, empty ScheduledMessageLanguageEntity object.</summary>
		/// <returns>A new, empty ScheduledMessageLanguageEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new ScheduledMessageLanguageEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewScheduledMessageLanguage
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty ScheduleitemEntity objects.</summary>
	[Serializable]
	public partial class ScheduleitemEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public ScheduleitemEntityFactory() : base("ScheduleitemEntity", Obymobi.Data.EntityType.ScheduleitemEntity) { }

		/// <summary>Creates a new, empty ScheduleitemEntity object.</summary>
		/// <returns>A new, empty ScheduleitemEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new ScheduleitemEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewScheduleitem
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty ServiceMethodEntity objects.</summary>
	[Serializable]
	public partial class ServiceMethodEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public ServiceMethodEntityFactory() : base("ServiceMethodEntity", Obymobi.Data.EntityType.ServiceMethodEntity) { }

		/// <summary>Creates a new, empty ServiceMethodEntity object.</summary>
		/// <returns>A new, empty ServiceMethodEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new ServiceMethodEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewServiceMethod
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty ServiceMethodDeliverypointgroupEntity objects.</summary>
	[Serializable]
	public partial class ServiceMethodDeliverypointgroupEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public ServiceMethodDeliverypointgroupEntityFactory() : base("ServiceMethodDeliverypointgroupEntity", Obymobi.Data.EntityType.ServiceMethodDeliverypointgroupEntity) { }

		/// <summary>Creates a new, empty ServiceMethodDeliverypointgroupEntity object.</summary>
		/// <returns>A new, empty ServiceMethodDeliverypointgroupEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new ServiceMethodDeliverypointgroupEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewServiceMethodDeliverypointgroup
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty SetupCodeEntity objects.</summary>
	[Serializable]
	public partial class SetupCodeEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public SetupCodeEntityFactory() : base("SetupCodeEntity", Obymobi.Data.EntityType.SetupCodeEntity) { }

		/// <summary>Creates a new, empty SetupCodeEntity object.</summary>
		/// <returns>A new, empty SetupCodeEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new SetupCodeEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewSetupCode
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty SiteEntity objects.</summary>
	[Serializable]
	public partial class SiteEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public SiteEntityFactory() : base("SiteEntity", Obymobi.Data.EntityType.SiteEntity) { }

		/// <summary>Creates a new, empty SiteEntity object.</summary>
		/// <returns>A new, empty SiteEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new SiteEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewSite
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty SiteCultureEntity objects.</summary>
	[Serializable]
	public partial class SiteCultureEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public SiteCultureEntityFactory() : base("SiteCultureEntity", Obymobi.Data.EntityType.SiteCultureEntity) { }

		/// <summary>Creates a new, empty SiteCultureEntity object.</summary>
		/// <returns>A new, empty SiteCultureEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new SiteCultureEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewSiteCulture
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty SiteLanguageEntity objects.</summary>
	[Serializable]
	public partial class SiteLanguageEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public SiteLanguageEntityFactory() : base("SiteLanguageEntity", Obymobi.Data.EntityType.SiteLanguageEntity) { }

		/// <summary>Creates a new, empty SiteLanguageEntity object.</summary>
		/// <returns>A new, empty SiteLanguageEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new SiteLanguageEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewSiteLanguage
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty SiteTemplateEntity objects.</summary>
	[Serializable]
	public partial class SiteTemplateEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public SiteTemplateEntityFactory() : base("SiteTemplateEntity", Obymobi.Data.EntityType.SiteTemplateEntity) { }

		/// <summary>Creates a new, empty SiteTemplateEntity object.</summary>
		/// <returns>A new, empty SiteTemplateEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new SiteTemplateEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewSiteTemplate
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty SiteTemplateCultureEntity objects.</summary>
	[Serializable]
	public partial class SiteTemplateCultureEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public SiteTemplateCultureEntityFactory() : base("SiteTemplateCultureEntity", Obymobi.Data.EntityType.SiteTemplateCultureEntity) { }

		/// <summary>Creates a new, empty SiteTemplateCultureEntity object.</summary>
		/// <returns>A new, empty SiteTemplateCultureEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new SiteTemplateCultureEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewSiteTemplateCulture
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty SiteTemplateLanguageEntity objects.</summary>
	[Serializable]
	public partial class SiteTemplateLanguageEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public SiteTemplateLanguageEntityFactory() : base("SiteTemplateLanguageEntity", Obymobi.Data.EntityType.SiteTemplateLanguageEntity) { }

		/// <summary>Creates a new, empty SiteTemplateLanguageEntity object.</summary>
		/// <returns>A new, empty SiteTemplateLanguageEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new SiteTemplateLanguageEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewSiteTemplateLanguage
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty SmsInformationEntity objects.</summary>
	[Serializable]
	public partial class SmsInformationEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public SmsInformationEntityFactory() : base("SmsInformationEntity", Obymobi.Data.EntityType.SmsInformationEntity) { }

		/// <summary>Creates a new, empty SmsInformationEntity object.</summary>
		/// <returns>A new, empty SmsInformationEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new SmsInformationEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewSmsInformation
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty SmsKeywordEntity objects.</summary>
	[Serializable]
	public partial class SmsKeywordEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public SmsKeywordEntityFactory() : base("SmsKeywordEntity", Obymobi.Data.EntityType.SmsKeywordEntity) { }

		/// <summary>Creates a new, empty SmsKeywordEntity object.</summary>
		/// <returns>A new, empty SmsKeywordEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new SmsKeywordEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewSmsKeyword
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty StationEntity objects.</summary>
	[Serializable]
	public partial class StationEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public StationEntityFactory() : base("StationEntity", Obymobi.Data.EntityType.StationEntity) { }

		/// <summary>Creates a new, empty StationEntity object.</summary>
		/// <returns>A new, empty StationEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new StationEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewStation
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty StationLanguageEntity objects.</summary>
	[Serializable]
	public partial class StationLanguageEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public StationLanguageEntityFactory() : base("StationLanguageEntity", Obymobi.Data.EntityType.StationLanguageEntity) { }

		/// <summary>Creates a new, empty StationLanguageEntity object.</summary>
		/// <returns>A new, empty StationLanguageEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new StationLanguageEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewStationLanguage
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty StationListEntity objects.</summary>
	[Serializable]
	public partial class StationListEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public StationListEntityFactory() : base("StationListEntity", Obymobi.Data.EntityType.StationListEntity) { }

		/// <summary>Creates a new, empty StationListEntity object.</summary>
		/// <returns>A new, empty StationListEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new StationListEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewStationList
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty SupplierEntity objects.</summary>
	[Serializable]
	public partial class SupplierEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public SupplierEntityFactory() : base("SupplierEntity", Obymobi.Data.EntityType.SupplierEntity) { }

		/// <summary>Creates a new, empty SupplierEntity object.</summary>
		/// <returns>A new, empty SupplierEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new SupplierEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewSupplier
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty SupportagentEntity objects.</summary>
	[Serializable]
	public partial class SupportagentEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public SupportagentEntityFactory() : base("SupportagentEntity", Obymobi.Data.EntityType.SupportagentEntity) { }

		/// <summary>Creates a new, empty SupportagentEntity object.</summary>
		/// <returns>A new, empty SupportagentEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new SupportagentEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewSupportagent
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty SupportpoolEntity objects.</summary>
	[Serializable]
	public partial class SupportpoolEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public SupportpoolEntityFactory() : base("SupportpoolEntity", Obymobi.Data.EntityType.SupportpoolEntity) { }

		/// <summary>Creates a new, empty SupportpoolEntity object.</summary>
		/// <returns>A new, empty SupportpoolEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new SupportpoolEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewSupportpool
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty SupportpoolNotificationRecipientEntity objects.</summary>
	[Serializable]
	public partial class SupportpoolNotificationRecipientEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public SupportpoolNotificationRecipientEntityFactory() : base("SupportpoolNotificationRecipientEntity", Obymobi.Data.EntityType.SupportpoolNotificationRecipientEntity) { }

		/// <summary>Creates a new, empty SupportpoolNotificationRecipientEntity object.</summary>
		/// <returns>A new, empty SupportpoolNotificationRecipientEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new SupportpoolNotificationRecipientEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewSupportpoolNotificationRecipient
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty SupportpoolSupportagentEntity objects.</summary>
	[Serializable]
	public partial class SupportpoolSupportagentEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public SupportpoolSupportagentEntityFactory() : base("SupportpoolSupportagentEntity", Obymobi.Data.EntityType.SupportpoolSupportagentEntity) { }

		/// <summary>Creates a new, empty SupportpoolSupportagentEntity object.</summary>
		/// <returns>A new, empty SupportpoolSupportagentEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new SupportpoolSupportagentEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewSupportpoolSupportagent
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty SurveyEntity objects.</summary>
	[Serializable]
	public partial class SurveyEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public SurveyEntityFactory() : base("SurveyEntity", Obymobi.Data.EntityType.SurveyEntity) { }

		/// <summary>Creates a new, empty SurveyEntity object.</summary>
		/// <returns>A new, empty SurveyEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new SurveyEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewSurvey
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty SurveyAnswerEntity objects.</summary>
	[Serializable]
	public partial class SurveyAnswerEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public SurveyAnswerEntityFactory() : base("SurveyAnswerEntity", Obymobi.Data.EntityType.SurveyAnswerEntity) { }

		/// <summary>Creates a new, empty SurveyAnswerEntity object.</summary>
		/// <returns>A new, empty SurveyAnswerEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new SurveyAnswerEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewSurveyAnswer
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty SurveyAnswerLanguageEntity objects.</summary>
	[Serializable]
	public partial class SurveyAnswerLanguageEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public SurveyAnswerLanguageEntityFactory() : base("SurveyAnswerLanguageEntity", Obymobi.Data.EntityType.SurveyAnswerLanguageEntity) { }

		/// <summary>Creates a new, empty SurveyAnswerLanguageEntity object.</summary>
		/// <returns>A new, empty SurveyAnswerLanguageEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new SurveyAnswerLanguageEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewSurveyAnswerLanguage
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty SurveyLanguageEntity objects.</summary>
	[Serializable]
	public partial class SurveyLanguageEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public SurveyLanguageEntityFactory() : base("SurveyLanguageEntity", Obymobi.Data.EntityType.SurveyLanguageEntity) { }

		/// <summary>Creates a new, empty SurveyLanguageEntity object.</summary>
		/// <returns>A new, empty SurveyLanguageEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new SurveyLanguageEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewSurveyLanguage
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty SurveyPageEntity objects.</summary>
	[Serializable]
	public partial class SurveyPageEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public SurveyPageEntityFactory() : base("SurveyPageEntity", Obymobi.Data.EntityType.SurveyPageEntity) { }

		/// <summary>Creates a new, empty SurveyPageEntity object.</summary>
		/// <returns>A new, empty SurveyPageEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new SurveyPageEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewSurveyPage
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty SurveyQuestionEntity objects.</summary>
	[Serializable]
	public partial class SurveyQuestionEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public SurveyQuestionEntityFactory() : base("SurveyQuestionEntity", Obymobi.Data.EntityType.SurveyQuestionEntity) { }

		/// <summary>Creates a new, empty SurveyQuestionEntity object.</summary>
		/// <returns>A new, empty SurveyQuestionEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new SurveyQuestionEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewSurveyQuestion
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty SurveyQuestionLanguageEntity objects.</summary>
	[Serializable]
	public partial class SurveyQuestionLanguageEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public SurveyQuestionLanguageEntityFactory() : base("SurveyQuestionLanguageEntity", Obymobi.Data.EntityType.SurveyQuestionLanguageEntity) { }

		/// <summary>Creates a new, empty SurveyQuestionLanguageEntity object.</summary>
		/// <returns>A new, empty SurveyQuestionLanguageEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new SurveyQuestionLanguageEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewSurveyQuestionLanguage
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty SurveyResultEntity objects.</summary>
	[Serializable]
	public partial class SurveyResultEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public SurveyResultEntityFactory() : base("SurveyResultEntity", Obymobi.Data.EntityType.SurveyResultEntity) { }

		/// <summary>Creates a new, empty SurveyResultEntity object.</summary>
		/// <returns>A new, empty SurveyResultEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new SurveyResultEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewSurveyResult
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty TagEntity objects.</summary>
	[Serializable]
	public partial class TagEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public TagEntityFactory() : base("TagEntity", Obymobi.Data.EntityType.TagEntity) { }

		/// <summary>Creates a new, empty TagEntity object.</summary>
		/// <returns>A new, empty TagEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new TagEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewTag
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty TaxTariffEntity objects.</summary>
	[Serializable]
	public partial class TaxTariffEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public TaxTariffEntityFactory() : base("TaxTariffEntity", Obymobi.Data.EntityType.TaxTariffEntity) { }

		/// <summary>Creates a new, empty TaxTariffEntity object.</summary>
		/// <returns>A new, empty TaxTariffEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new TaxTariffEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewTaxTariff
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty TerminalEntity objects.</summary>
	[Serializable]
	public partial class TerminalEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public TerminalEntityFactory() : base("TerminalEntity", Obymobi.Data.EntityType.TerminalEntity) { }

		/// <summary>Creates a new, empty TerminalEntity object.</summary>
		/// <returns>A new, empty TerminalEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new TerminalEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewTerminal
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty TerminalConfigurationEntity objects.</summary>
	[Serializable]
	public partial class TerminalConfigurationEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public TerminalConfigurationEntityFactory() : base("TerminalConfigurationEntity", Obymobi.Data.EntityType.TerminalConfigurationEntity) { }

		/// <summary>Creates a new, empty TerminalConfigurationEntity object.</summary>
		/// <returns>A new, empty TerminalConfigurationEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new TerminalConfigurationEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewTerminalConfiguration
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty TerminalLogEntity objects.</summary>
	[Serializable]
	public partial class TerminalLogEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public TerminalLogEntityFactory() : base("TerminalLogEntity", Obymobi.Data.EntityType.TerminalLogEntity) { }

		/// <summary>Creates a new, empty TerminalLogEntity object.</summary>
		/// <returns>A new, empty TerminalLogEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new TerminalLogEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewTerminalLog
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty TerminalLogFileEntity objects.</summary>
	[Serializable]
	public partial class TerminalLogFileEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public TerminalLogFileEntityFactory() : base("TerminalLogFileEntity", Obymobi.Data.EntityType.TerminalLogFileEntity) { }

		/// <summary>Creates a new, empty TerminalLogFileEntity object.</summary>
		/// <returns>A new, empty TerminalLogFileEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new TerminalLogFileEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewTerminalLogFile
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty TerminalMessageTemplateCategoryEntity objects.</summary>
	[Serializable]
	public partial class TerminalMessageTemplateCategoryEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public TerminalMessageTemplateCategoryEntityFactory() : base("TerminalMessageTemplateCategoryEntity", Obymobi.Data.EntityType.TerminalMessageTemplateCategoryEntity) { }

		/// <summary>Creates a new, empty TerminalMessageTemplateCategoryEntity object.</summary>
		/// <returns>A new, empty TerminalMessageTemplateCategoryEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new TerminalMessageTemplateCategoryEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewTerminalMessageTemplateCategory
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty TerminalStateEntity objects.</summary>
	[Serializable]
	public partial class TerminalStateEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public TerminalStateEntityFactory() : base("TerminalStateEntity", Obymobi.Data.EntityType.TerminalStateEntity) { }

		/// <summary>Creates a new, empty TerminalStateEntity object.</summary>
		/// <returns>A new, empty TerminalStateEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new TerminalStateEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewTerminalState
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty TimestampEntity objects.</summary>
	[Serializable]
	public partial class TimestampEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public TimestampEntityFactory() : base("TimestampEntity", Obymobi.Data.EntityType.TimestampEntity) { }

		/// <summary>Creates a new, empty TimestampEntity object.</summary>
		/// <returns>A new, empty TimestampEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new TimestampEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewTimestamp
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty TimeZoneEntity objects.</summary>
	[Serializable]
	public partial class TimeZoneEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public TimeZoneEntityFactory() : base("TimeZoneEntity", Obymobi.Data.EntityType.TimeZoneEntity) { }

		/// <summary>Creates a new, empty TimeZoneEntity object.</summary>
		/// <returns>A new, empty TimeZoneEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new TimeZoneEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewTimeZone
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty TranslationEntity objects.</summary>
	[Serializable]
	public partial class TranslationEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public TranslationEntityFactory() : base("TranslationEntity", Obymobi.Data.EntityType.TranslationEntity) { }

		/// <summary>Creates a new, empty TranslationEntity object.</summary>
		/// <returns>A new, empty TranslationEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new TranslationEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewTranslation
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty UIElementEntity objects.</summary>
	[Serializable]
	public partial class UIElementEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public UIElementEntityFactory() : base("UIElementEntity", Obymobi.Data.EntityType.UIElementEntity) { }

		/// <summary>Creates a new, empty UIElementEntity object.</summary>
		/// <returns>A new, empty UIElementEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new UIElementEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewUIElement
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty UIElementCustomEntity objects.</summary>
	[Serializable]
	public partial class UIElementCustomEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public UIElementCustomEntityFactory() : base("UIElementCustomEntity", Obymobi.Data.EntityType.UIElementCustomEntity) { }

		/// <summary>Creates a new, empty UIElementCustomEntity object.</summary>
		/// <returns>A new, empty UIElementCustomEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new UIElementCustomEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewUIElementCustom
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty UIElementSubPanelEntity objects.</summary>
	[Serializable]
	public partial class UIElementSubPanelEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public UIElementSubPanelEntityFactory() : base("UIElementSubPanelEntity", Obymobi.Data.EntityType.UIElementSubPanelEntity) { }

		/// <summary>Creates a new, empty UIElementSubPanelEntity object.</summary>
		/// <returns>A new, empty UIElementSubPanelEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new UIElementSubPanelEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewUIElementSubPanel
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty UIElementSubPanelCustomEntity objects.</summary>
	[Serializable]
	public partial class UIElementSubPanelCustomEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public UIElementSubPanelCustomEntityFactory() : base("UIElementSubPanelCustomEntity", Obymobi.Data.EntityType.UIElementSubPanelCustomEntity) { }

		/// <summary>Creates a new, empty UIElementSubPanelCustomEntity object.</summary>
		/// <returns>A new, empty UIElementSubPanelCustomEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new UIElementSubPanelCustomEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewUIElementSubPanelCustom
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty UIElementSubPanelUIElementEntity objects.</summary>
	[Serializable]
	public partial class UIElementSubPanelUIElementEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public UIElementSubPanelUIElementEntityFactory() : base("UIElementSubPanelUIElementEntity", Obymobi.Data.EntityType.UIElementSubPanelUIElementEntity) { }

		/// <summary>Creates a new, empty UIElementSubPanelUIElementEntity object.</summary>
		/// <returns>A new, empty UIElementSubPanelUIElementEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new UIElementSubPanelUIElementEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewUIElementSubPanelUIElement
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty UIFooterItemEntity objects.</summary>
	[Serializable]
	public partial class UIFooterItemEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public UIFooterItemEntityFactory() : base("UIFooterItemEntity", Obymobi.Data.EntityType.UIFooterItemEntity) { }

		/// <summary>Creates a new, empty UIFooterItemEntity object.</summary>
		/// <returns>A new, empty UIFooterItemEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new UIFooterItemEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewUIFooterItem
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty UIFooterItemLanguageEntity objects.</summary>
	[Serializable]
	public partial class UIFooterItemLanguageEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public UIFooterItemLanguageEntityFactory() : base("UIFooterItemLanguageEntity", Obymobi.Data.EntityType.UIFooterItemLanguageEntity) { }

		/// <summary>Creates a new, empty UIFooterItemLanguageEntity object.</summary>
		/// <returns>A new, empty UIFooterItemLanguageEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new UIFooterItemLanguageEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewUIFooterItemLanguage
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty UIModeEntity objects.</summary>
	[Serializable]
	public partial class UIModeEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public UIModeEntityFactory() : base("UIModeEntity", Obymobi.Data.EntityType.UIModeEntity) { }

		/// <summary>Creates a new, empty UIModeEntity object.</summary>
		/// <returns>A new, empty UIModeEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new UIModeEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewUIMode
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty UIScheduleEntity objects.</summary>
	[Serializable]
	public partial class UIScheduleEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public UIScheduleEntityFactory() : base("UIScheduleEntity", Obymobi.Data.EntityType.UIScheduleEntity) { }

		/// <summary>Creates a new, empty UIScheduleEntity object.</summary>
		/// <returns>A new, empty UIScheduleEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new UIScheduleEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewUISchedule
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty UIScheduleItemEntity objects.</summary>
	[Serializable]
	public partial class UIScheduleItemEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public UIScheduleItemEntityFactory() : base("UIScheduleItemEntity", Obymobi.Data.EntityType.UIScheduleItemEntity) { }

		/// <summary>Creates a new, empty UIScheduleItemEntity object.</summary>
		/// <returns>A new, empty UIScheduleItemEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new UIScheduleItemEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewUIScheduleItem
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty UIScheduleItemOccurrenceEntity objects.</summary>
	[Serializable]
	public partial class UIScheduleItemOccurrenceEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public UIScheduleItemOccurrenceEntityFactory() : base("UIScheduleItemOccurrenceEntity", Obymobi.Data.EntityType.UIScheduleItemOccurrenceEntity) { }

		/// <summary>Creates a new, empty UIScheduleItemOccurrenceEntity object.</summary>
		/// <returns>A new, empty UIScheduleItemOccurrenceEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new UIScheduleItemOccurrenceEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewUIScheduleItemOccurrence
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty UITabEntity objects.</summary>
	[Serializable]
	public partial class UITabEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public UITabEntityFactory() : base("UITabEntity", Obymobi.Data.EntityType.UITabEntity) { }

		/// <summary>Creates a new, empty UITabEntity object.</summary>
		/// <returns>A new, empty UITabEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new UITabEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewUITab
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty UITabLanguageEntity objects.</summary>
	[Serializable]
	public partial class UITabLanguageEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public UITabLanguageEntityFactory() : base("UITabLanguageEntity", Obymobi.Data.EntityType.UITabLanguageEntity) { }

		/// <summary>Creates a new, empty UITabLanguageEntity object.</summary>
		/// <returns>A new, empty UITabLanguageEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new UITabLanguageEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewUITabLanguage
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty UIThemeEntity objects.</summary>
	[Serializable]
	public partial class UIThemeEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public UIThemeEntityFactory() : base("UIThemeEntity", Obymobi.Data.EntityType.UIThemeEntity) { }

		/// <summary>Creates a new, empty UIThemeEntity object.</summary>
		/// <returns>A new, empty UIThemeEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new UIThemeEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewUITheme
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty UIThemeColorEntity objects.</summary>
	[Serializable]
	public partial class UIThemeColorEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public UIThemeColorEntityFactory() : base("UIThemeColorEntity", Obymobi.Data.EntityType.UIThemeColorEntity) { }

		/// <summary>Creates a new, empty UIThemeColorEntity object.</summary>
		/// <returns>A new, empty UIThemeColorEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new UIThemeColorEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewUIThemeColor
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty UIThemeTextSizeEntity objects.</summary>
	[Serializable]
	public partial class UIThemeTextSizeEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public UIThemeTextSizeEntityFactory() : base("UIThemeTextSizeEntity", Obymobi.Data.EntityType.UIThemeTextSizeEntity) { }

		/// <summary>Creates a new, empty UIThemeTextSizeEntity object.</summary>
		/// <returns>A new, empty UIThemeTextSizeEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new UIThemeTextSizeEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewUIThemeTextSize
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty UIWidgetEntity objects.</summary>
	[Serializable]
	public partial class UIWidgetEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public UIWidgetEntityFactory() : base("UIWidgetEntity", Obymobi.Data.EntityType.UIWidgetEntity) { }

		/// <summary>Creates a new, empty UIWidgetEntity object.</summary>
		/// <returns>A new, empty UIWidgetEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new UIWidgetEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewUIWidget
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty UIWidgetAvailabilityEntity objects.</summary>
	[Serializable]
	public partial class UIWidgetAvailabilityEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public UIWidgetAvailabilityEntityFactory() : base("UIWidgetAvailabilityEntity", Obymobi.Data.EntityType.UIWidgetAvailabilityEntity) { }

		/// <summary>Creates a new, empty UIWidgetAvailabilityEntity object.</summary>
		/// <returns>A new, empty UIWidgetAvailabilityEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new UIWidgetAvailabilityEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewUIWidgetAvailability
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty UIWidgetLanguageEntity objects.</summary>
	[Serializable]
	public partial class UIWidgetLanguageEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public UIWidgetLanguageEntityFactory() : base("UIWidgetLanguageEntity", Obymobi.Data.EntityType.UIWidgetLanguageEntity) { }

		/// <summary>Creates a new, empty UIWidgetLanguageEntity object.</summary>
		/// <returns>A new, empty UIWidgetLanguageEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new UIWidgetLanguageEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewUIWidgetLanguage
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty UIWidgetTimerEntity objects.</summary>
	[Serializable]
	public partial class UIWidgetTimerEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public UIWidgetTimerEntityFactory() : base("UIWidgetTimerEntity", Obymobi.Data.EntityType.UIWidgetTimerEntity) { }

		/// <summary>Creates a new, empty UIWidgetTimerEntity object.</summary>
		/// <returns>A new, empty UIWidgetTimerEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new UIWidgetTimerEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewUIWidgetTimer
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty UserEntity objects.</summary>
	[Serializable]
	public partial class UserEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public UserEntityFactory() : base("UserEntity", Obymobi.Data.EntityType.UserEntity) { }

		/// <summary>Creates a new, empty UserEntity object.</summary>
		/// <returns>A new, empty UserEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new UserEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewUser
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty UserBrandEntity objects.</summary>
	[Serializable]
	public partial class UserBrandEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public UserBrandEntityFactory() : base("UserBrandEntity", Obymobi.Data.EntityType.UserBrandEntity) { }

		/// <summary>Creates a new, empty UserBrandEntity object.</summary>
		/// <returns>A new, empty UserBrandEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new UserBrandEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewUserBrand
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty UserLogonEntity objects.</summary>
	[Serializable]
	public partial class UserLogonEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public UserLogonEntityFactory() : base("UserLogonEntity", Obymobi.Data.EntityType.UserLogonEntity) { }

		/// <summary>Creates a new, empty UserLogonEntity object.</summary>
		/// <returns>A new, empty UserLogonEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new UserLogonEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewUserLogon
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty UserRoleEntity objects.</summary>
	[Serializable]
	public partial class UserRoleEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public UserRoleEntityFactory() : base("UserRoleEntity", Obymobi.Data.EntityType.UserRoleEntity) { }

		/// <summary>Creates a new, empty UserRoleEntity object.</summary>
		/// <returns>A new, empty UserRoleEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new UserRoleEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewUserRole
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty VattariffEntity objects.</summary>
	[Serializable]
	public partial class VattariffEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public VattariffEntityFactory() : base("VattariffEntity", Obymobi.Data.EntityType.VattariffEntity) { }

		/// <summary>Creates a new, empty VattariffEntity object.</summary>
		/// <returns>A new, empty VattariffEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new VattariffEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewVattariff
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty VendorEntity objects.</summary>
	[Serializable]
	public partial class VendorEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public VendorEntityFactory() : base("VendorEntity", Obymobi.Data.EntityType.VendorEntity) { }

		/// <summary>Creates a new, empty VendorEntity object.</summary>
		/// <returns>A new, empty VendorEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new VendorEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewVendor
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty VenueCategoryEntity objects.</summary>
	[Serializable]
	public partial class VenueCategoryEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public VenueCategoryEntityFactory() : base("VenueCategoryEntity", Obymobi.Data.EntityType.VenueCategoryEntity) { }

		/// <summary>Creates a new, empty VenueCategoryEntity object.</summary>
		/// <returns>A new, empty VenueCategoryEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new VenueCategoryEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewVenueCategory
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty VenueCategoryLanguageEntity objects.</summary>
	[Serializable]
	public partial class VenueCategoryLanguageEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public VenueCategoryLanguageEntityFactory() : base("VenueCategoryLanguageEntity", Obymobi.Data.EntityType.VenueCategoryLanguageEntity) { }

		/// <summary>Creates a new, empty VenueCategoryLanguageEntity object.</summary>
		/// <returns>A new, empty VenueCategoryLanguageEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new VenueCategoryLanguageEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewVenueCategoryLanguage
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty ViewEntity objects.</summary>
	[Serializable]
	public partial class ViewEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public ViewEntityFactory() : base("ViewEntity", Obymobi.Data.EntityType.ViewEntity) { }

		/// <summary>Creates a new, empty ViewEntity object.</summary>
		/// <returns>A new, empty ViewEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new ViewEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewView
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty ViewCustomEntity objects.</summary>
	[Serializable]
	public partial class ViewCustomEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public ViewCustomEntityFactory() : base("ViewCustomEntity", Obymobi.Data.EntityType.ViewCustomEntity) { }

		/// <summary>Creates a new, empty ViewCustomEntity object.</summary>
		/// <returns>A new, empty ViewCustomEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new ViewCustomEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewViewCustom
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty ViewItemEntity objects.</summary>
	[Serializable]
	public partial class ViewItemEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public ViewItemEntityFactory() : base("ViewItemEntity", Obymobi.Data.EntityType.ViewItemEntity) { }

		/// <summary>Creates a new, empty ViewItemEntity object.</summary>
		/// <returns>A new, empty ViewItemEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new ViewItemEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewViewItem
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty ViewItemCustomEntity objects.</summary>
	[Serializable]
	public partial class ViewItemCustomEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public ViewItemCustomEntityFactory() : base("ViewItemCustomEntity", Obymobi.Data.EntityType.ViewItemCustomEntity) { }

		/// <summary>Creates a new, empty ViewItemCustomEntity object.</summary>
		/// <returns>A new, empty ViewItemCustomEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new ViewItemCustomEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewViewItemCustom
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty WeatherEntity objects.</summary>
	[Serializable]
	public partial class WeatherEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public WeatherEntityFactory() : base("WeatherEntity", Obymobi.Data.EntityType.WeatherEntity) { }

		/// <summary>Creates a new, empty WeatherEntity object.</summary>
		/// <returns>A new, empty WeatherEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new WeatherEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewWeather
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty WifiConfigurationEntity objects.</summary>
	[Serializable]
	public partial class WifiConfigurationEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public WifiConfigurationEntityFactory() : base("WifiConfigurationEntity", Obymobi.Data.EntityType.WifiConfigurationEntity) { }

		/// <summary>Creates a new, empty WifiConfigurationEntity object.</summary>
		/// <returns>A new, empty WifiConfigurationEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new WifiConfigurationEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewWifiConfiguration
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty ActionEntity objects.</summary>
	[Serializable]
	public partial class ActionEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public ActionEntityFactory() : base("ActionEntity", Obymobi.Data.EntityType.ActionEntity) { }

		/// <summary>Creates a new, empty ActionEntity object.</summary>
		/// <returns>A new, empty ActionEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new ActionEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewAction
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty ApplicationConfigurationEntity objects.</summary>
	[Serializable]
	public partial class ApplicationConfigurationEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public ApplicationConfigurationEntityFactory() : base("ApplicationConfigurationEntity", Obymobi.Data.EntityType.ApplicationConfigurationEntity) { }

		/// <summary>Creates a new, empty ApplicationConfigurationEntity object.</summary>
		/// <returns>A new, empty ApplicationConfigurationEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new ApplicationConfigurationEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewApplicationConfiguration
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty CarouselItemEntity objects.</summary>
	[Serializable]
	public partial class CarouselItemEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public CarouselItemEntityFactory() : base("CarouselItemEntity", Obymobi.Data.EntityType.CarouselItemEntity) { }

		/// <summary>Creates a new, empty CarouselItemEntity object.</summary>
		/// <returns>A new, empty CarouselItemEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new CarouselItemEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewCarouselItem
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty FeatureFlagEntity objects.</summary>
	[Serializable]
	public partial class FeatureFlagEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public FeatureFlagEntityFactory() : base("FeatureFlagEntity", Obymobi.Data.EntityType.FeatureFlagEntity) { }

		/// <summary>Creates a new, empty FeatureFlagEntity object.</summary>
		/// <returns>A new, empty FeatureFlagEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new FeatureFlagEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewFeatureFlag
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty LandingPageEntity objects.</summary>
	[Serializable]
	public partial class LandingPageEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public LandingPageEntityFactory() : base("LandingPageEntity", Obymobi.Data.EntityType.LandingPageEntity) { }

		/// <summary>Creates a new, empty LandingPageEntity object.</summary>
		/// <returns>A new, empty LandingPageEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new LandingPageEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewLandingPage
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty LandingPageWidgetEntity objects.</summary>
	[Serializable]
	public partial class LandingPageWidgetEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public LandingPageWidgetEntityFactory() : base("LandingPageWidgetEntity", Obymobi.Data.EntityType.LandingPageWidgetEntity) { }

		/// <summary>Creates a new, empty LandingPageWidgetEntity object.</summary>
		/// <returns>A new, empty LandingPageWidgetEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new LandingPageWidgetEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewLandingPageWidget
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty NavigationMenuEntity objects.</summary>
	[Serializable]
	public partial class NavigationMenuEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public NavigationMenuEntityFactory() : base("NavigationMenuEntity", Obymobi.Data.EntityType.NavigationMenuEntity) { }

		/// <summary>Creates a new, empty NavigationMenuEntity object.</summary>
		/// <returns>A new, empty NavigationMenuEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new NavigationMenuEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewNavigationMenu
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty NavigationMenuItemEntity objects.</summary>
	[Serializable]
	public partial class NavigationMenuItemEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public NavigationMenuItemEntityFactory() : base("NavigationMenuItemEntity", Obymobi.Data.EntityType.NavigationMenuItemEntity) { }

		/// <summary>Creates a new, empty NavigationMenuItemEntity object.</summary>
		/// <returns>A new, empty NavigationMenuItemEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new NavigationMenuItemEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewNavigationMenuItem
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty NavigationMenuWidgetEntity objects.</summary>
	[Serializable]
	public partial class NavigationMenuWidgetEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public NavigationMenuWidgetEntityFactory() : base("NavigationMenuWidgetEntity", Obymobi.Data.EntityType.NavigationMenuWidgetEntity) { }

		/// <summary>Creates a new, empty NavigationMenuWidgetEntity object.</summary>
		/// <returns>A new, empty NavigationMenuWidgetEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new NavigationMenuWidgetEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewNavigationMenuWidget
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty ThemeEntity objects.</summary>
	[Serializable]
	public partial class ThemeEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public ThemeEntityFactory() : base("ThemeEntity", Obymobi.Data.EntityType.ThemeEntity) { }

		/// <summary>Creates a new, empty ThemeEntity object.</summary>
		/// <returns>A new, empty ThemeEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new ThemeEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewTheme
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty WidgetEntity objects.</summary>
	[Serializable]
	public partial class WidgetEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public WidgetEntityFactory() : base("WidgetEntity", Obymobi.Data.EntityType.WidgetEntity) { }

		/// <summary>Creates a new, empty WidgetEntity object.</summary>
		/// <returns>A new, empty WidgetEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new WidgetEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewWidget
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}

		/// <summary>Creates the hierarchy fields for the entity to which this factory belongs.</summary>
		/// <returns>IEntityFields object with the fields of all the entities in teh hierarchy of this entity or the fields of this entity if the entity isn't in a hierarchy.</returns>
		public override IEntityFields CreateHierarchyFields()
		{
			return new EntityFields(InheritanceInfoProviderSingleton.GetInstance().GetHierarchyFields("WidgetEntity"), InheritanceInfoProviderSingleton.GetInstance(), null);
		}

		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty WidgetActionBannerEntity objects.</summary>
	[Serializable]
	public partial class WidgetActionBannerEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public WidgetActionBannerEntityFactory() : base("WidgetActionBannerEntity", Obymobi.Data.EntityType.WidgetActionBannerEntity) { }

		/// <summary>Creates a new, empty WidgetActionBannerEntity object.</summary>
		/// <returns>A new, empty WidgetActionBannerEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new WidgetActionBannerEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewWidgetActionBanner
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}

		/// <summary>Creates the hierarchy fields for the entity to which this factory belongs.</summary>
		/// <returns>IEntityFields object with the fields of all the entities in teh hierarchy of this entity or the fields of this entity if the entity isn't in a hierarchy.</returns>
		public override IEntityFields CreateHierarchyFields()
		{
			return new EntityFields(InheritanceInfoProviderSingleton.GetInstance().GetHierarchyFields("WidgetActionBannerEntity"), InheritanceInfoProviderSingleton.GetInstance(), null);
		}

		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty WidgetActionButtonEntity objects.</summary>
	[Serializable]
	public partial class WidgetActionButtonEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public WidgetActionButtonEntityFactory() : base("WidgetActionButtonEntity", Obymobi.Data.EntityType.WidgetActionButtonEntity) { }

		/// <summary>Creates a new, empty WidgetActionButtonEntity object.</summary>
		/// <returns>A new, empty WidgetActionButtonEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new WidgetActionButtonEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewWidgetActionButton
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}

		/// <summary>Creates the hierarchy fields for the entity to which this factory belongs.</summary>
		/// <returns>IEntityFields object with the fields of all the entities in teh hierarchy of this entity or the fields of this entity if the entity isn't in a hierarchy.</returns>
		public override IEntityFields CreateHierarchyFields()
		{
			return new EntityFields(InheritanceInfoProviderSingleton.GetInstance().GetHierarchyFields("WidgetActionButtonEntity"), InheritanceInfoProviderSingleton.GetInstance(), null);
		}

		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty WidgetCarouselEntity objects.</summary>
	[Serializable]
	public partial class WidgetCarouselEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public WidgetCarouselEntityFactory() : base("WidgetCarouselEntity", Obymobi.Data.EntityType.WidgetCarouselEntity) { }

		/// <summary>Creates a new, empty WidgetCarouselEntity object.</summary>
		/// <returns>A new, empty WidgetCarouselEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new WidgetCarouselEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewWidgetCarousel
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}

		/// <summary>Creates the hierarchy fields for the entity to which this factory belongs.</summary>
		/// <returns>IEntityFields object with the fields of all the entities in teh hierarchy of this entity or the fields of this entity if the entity isn't in a hierarchy.</returns>
		public override IEntityFields CreateHierarchyFields()
		{
			return new EntityFields(InheritanceInfoProviderSingleton.GetInstance().GetHierarchyFields("WidgetCarouselEntity"), InheritanceInfoProviderSingleton.GetInstance(), null);
		}

		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty WidgetGroupEntity objects.</summary>
	[Serializable]
	public partial class WidgetGroupEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public WidgetGroupEntityFactory() : base("WidgetGroupEntity", Obymobi.Data.EntityType.WidgetGroupEntity) { }

		/// <summary>Creates a new, empty WidgetGroupEntity object.</summary>
		/// <returns>A new, empty WidgetGroupEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new WidgetGroupEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewWidgetGroup
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}

		/// <summary>Creates the hierarchy fields for the entity to which this factory belongs.</summary>
		/// <returns>IEntityFields object with the fields of all the entities in teh hierarchy of this entity or the fields of this entity if the entity isn't in a hierarchy.</returns>
		public override IEntityFields CreateHierarchyFields()
		{
			return new EntityFields(InheritanceInfoProviderSingleton.GetInstance().GetHierarchyFields("WidgetGroupEntity"), InheritanceInfoProviderSingleton.GetInstance(), null);
		}

		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty WidgetGroupWidgetEntity objects.</summary>
	[Serializable]
	public partial class WidgetGroupWidgetEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public WidgetGroupWidgetEntityFactory() : base("WidgetGroupWidgetEntity", Obymobi.Data.EntityType.WidgetGroupWidgetEntity) { }

		/// <summary>Creates a new, empty WidgetGroupWidgetEntity object.</summary>
		/// <returns>A new, empty WidgetGroupWidgetEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new WidgetGroupWidgetEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewWidgetGroupWidget
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}


		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty WidgetHeroEntity objects.</summary>
	[Serializable]
	public partial class WidgetHeroEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public WidgetHeroEntityFactory() : base("WidgetHeroEntity", Obymobi.Data.EntityType.WidgetHeroEntity) { }

		/// <summary>Creates a new, empty WidgetHeroEntity object.</summary>
		/// <returns>A new, empty WidgetHeroEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new WidgetHeroEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewWidgetHero
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}

		/// <summary>Creates the hierarchy fields for the entity to which this factory belongs.</summary>
		/// <returns>IEntityFields object with the fields of all the entities in teh hierarchy of this entity or the fields of this entity if the entity isn't in a hierarchy.</returns>
		public override IEntityFields CreateHierarchyFields()
		{
			return new EntityFields(InheritanceInfoProviderSingleton.GetInstance().GetHierarchyFields("WidgetHeroEntity"), InheritanceInfoProviderSingleton.GetInstance(), null);
		}

		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty WidgetLanguageSwitcherEntity objects.</summary>
	[Serializable]
	public partial class WidgetLanguageSwitcherEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public WidgetLanguageSwitcherEntityFactory() : base("WidgetLanguageSwitcherEntity", Obymobi.Data.EntityType.WidgetLanguageSwitcherEntity) { }

		/// <summary>Creates a new, empty WidgetLanguageSwitcherEntity object.</summary>
		/// <returns>A new, empty WidgetLanguageSwitcherEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new WidgetLanguageSwitcherEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewWidgetLanguageSwitcher
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}

		/// <summary>Creates the hierarchy fields for the entity to which this factory belongs.</summary>
		/// <returns>IEntityFields object with the fields of all the entities in teh hierarchy of this entity or the fields of this entity if the entity isn't in a hierarchy.</returns>
		public override IEntityFields CreateHierarchyFields()
		{
			return new EntityFields(InheritanceInfoProviderSingleton.GetInstance().GetHierarchyFields("WidgetLanguageSwitcherEntity"), InheritanceInfoProviderSingleton.GetInstance(), null);
		}

		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty WidgetMarkdownEntity objects.</summary>
	[Serializable]
	public partial class WidgetMarkdownEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public WidgetMarkdownEntityFactory() : base("WidgetMarkdownEntity", Obymobi.Data.EntityType.WidgetMarkdownEntity) { }

		/// <summary>Creates a new, empty WidgetMarkdownEntity object.</summary>
		/// <returns>A new, empty WidgetMarkdownEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new WidgetMarkdownEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewWidgetMarkdown
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}

		/// <summary>Creates the hierarchy fields for the entity to which this factory belongs.</summary>
		/// <returns>IEntityFields object with the fields of all the entities in teh hierarchy of this entity or the fields of this entity if the entity isn't in a hierarchy.</returns>
		public override IEntityFields CreateHierarchyFields()
		{
			return new EntityFields(InheritanceInfoProviderSingleton.GetInstance().GetHierarchyFields("WidgetMarkdownEntity"), InheritanceInfoProviderSingleton.GetInstance(), null);
		}

		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty WidgetOpeningTimeEntity objects.</summary>
	[Serializable]
	public partial class WidgetOpeningTimeEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public WidgetOpeningTimeEntityFactory() : base("WidgetOpeningTimeEntity", Obymobi.Data.EntityType.WidgetOpeningTimeEntity) { }

		/// <summary>Creates a new, empty WidgetOpeningTimeEntity object.</summary>
		/// <returns>A new, empty WidgetOpeningTimeEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new WidgetOpeningTimeEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewWidgetOpeningTime
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}

		/// <summary>Creates the hierarchy fields for the entity to which this factory belongs.</summary>
		/// <returns>IEntityFields object with the fields of all the entities in teh hierarchy of this entity or the fields of this entity if the entity isn't in a hierarchy.</returns>
		public override IEntityFields CreateHierarchyFields()
		{
			return new EntityFields(InheritanceInfoProviderSingleton.GetInstance().GetHierarchyFields("WidgetOpeningTimeEntity"), InheritanceInfoProviderSingleton.GetInstance(), null);
		}

		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty WidgetPageTitleEntity objects.</summary>
	[Serializable]
	public partial class WidgetPageTitleEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public WidgetPageTitleEntityFactory() : base("WidgetPageTitleEntity", Obymobi.Data.EntityType.WidgetPageTitleEntity) { }

		/// <summary>Creates a new, empty WidgetPageTitleEntity object.</summary>
		/// <returns>A new, empty WidgetPageTitleEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new WidgetPageTitleEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewWidgetPageTitle
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}

		/// <summary>Creates the hierarchy fields for the entity to which this factory belongs.</summary>
		/// <returns>IEntityFields object with the fields of all the entities in teh hierarchy of this entity or the fields of this entity if the entity isn't in a hierarchy.</returns>
		public override IEntityFields CreateHierarchyFields()
		{
			return new EntityFields(InheritanceInfoProviderSingleton.GetInstance().GetHierarchyFields("WidgetPageTitleEntity"), InheritanceInfoProviderSingleton.GetInstance(), null);
		}

		#region Included Code

		#endregion
	}
	
	/// <summary>Factory to create new, empty WidgetWaitTimeEntity objects.</summary>
	[Serializable]
	public partial class WidgetWaitTimeEntityFactory : EntityFactoryBase {
		/// <summary>CTor</summary>
		public WidgetWaitTimeEntityFactory() : base("WidgetWaitTimeEntity", Obymobi.Data.EntityType.WidgetWaitTimeEntity) { }

		/// <summary>Creates a new, empty WidgetWaitTimeEntity object.</summary>
		/// <returns>A new, empty WidgetWaitTimeEntity object.</returns>
		public override IEntity Create() {
			IEntity toReturn = new WidgetWaitTimeEntity();
			
			// __LLBLGENPRO_USER_CODE_REGION_START CreateNewWidgetWaitTime
			// __LLBLGENPRO_USER_CODE_REGION_END
			return toReturn;
		}

		/// <summary>Creates the hierarchy fields for the entity to which this factory belongs.</summary>
		/// <returns>IEntityFields object with the fields of all the entities in teh hierarchy of this entity or the fields of this entity if the entity isn't in a hierarchy.</returns>
		public override IEntityFields CreateHierarchyFields()
		{
			return new EntityFields(InheritanceInfoProviderSingleton.GetInstance().GetHierarchyFields("WidgetWaitTimeEntity"), InheritanceInfoProviderSingleton.GetInstance(), null);
		}

		#region Included Code

		#endregion
	}

	/// <summary>Factory to create new entity collection objects</summary>
	[Serializable]
	public partial class GeneralEntityCollectionFactory
	{
		/// <summary>Creates a new entity collection</summary>
		/// <param name="typeToUse">The entity type to create the collection for.</param>
		/// <returns>A new entity collection object.</returns>
		public static IEntityCollection Create(Obymobi.Data.EntityType typeToUse)
		{
			switch(typeToUse)
			{
				case Obymobi.Data.EntityType.AccessCodeEntity:
					return new AccessCodeCollection();
				case Obymobi.Data.EntityType.AccessCodeCompanyEntity:
					return new AccessCodeCompanyCollection();
				case Obymobi.Data.EntityType.AccessCodePointOfInterestEntity:
					return new AccessCodePointOfInterestCollection();
				case Obymobi.Data.EntityType.AccountEntity:
					return new AccountCollection();
				case Obymobi.Data.EntityType.AccountCompanyEntity:
					return new AccountCompanyCollection();
				case Obymobi.Data.EntityType.ActionButtonEntity:
					return new ActionButtonCollection();
				case Obymobi.Data.EntityType.ActionButtonLanguageEntity:
					return new ActionButtonLanguageCollection();
				case Obymobi.Data.EntityType.AddressEntity:
					return new AddressCollection();
				case Obymobi.Data.EntityType.AdvertisementEntity:
					return new AdvertisementCollection();
				case Obymobi.Data.EntityType.AdvertisementConfigurationEntity:
					return new AdvertisementConfigurationCollection();
				case Obymobi.Data.EntityType.AdvertisementConfigurationAdvertisementEntity:
					return new AdvertisementConfigurationAdvertisementCollection();
				case Obymobi.Data.EntityType.AdvertisementLanguageEntity:
					return new AdvertisementLanguageCollection();
				case Obymobi.Data.EntityType.AdvertisementTagEntity:
					return new AdvertisementTagCollection();
				case Obymobi.Data.EntityType.AdvertisementTagAdvertisementEntity:
					return new AdvertisementTagAdvertisementCollection();
				case Obymobi.Data.EntityType.AdvertisementTagCategoryEntity:
					return new AdvertisementTagCategoryCollection();
				case Obymobi.Data.EntityType.AdvertisementTagEntertainmentEntity:
					return new AdvertisementTagEntertainmentCollection();
				case Obymobi.Data.EntityType.AdvertisementTagGenericproductEntity:
					return new AdvertisementTagGenericproductCollection();
				case Obymobi.Data.EntityType.AdvertisementTagLanguageEntity:
					return new AdvertisementTagLanguageCollection();
				case Obymobi.Data.EntityType.AdvertisementTagProductEntity:
					return new AdvertisementTagProductCollection();
				case Obymobi.Data.EntityType.AdyenPaymentMethodEntity:
					return new AdyenPaymentMethodCollection();
				case Obymobi.Data.EntityType.AdyenPaymentMethodBrandEntity:
					return new AdyenPaymentMethodBrandCollection();
				case Obymobi.Data.EntityType.AffiliateCampaignEntity:
					return new AffiliateCampaignCollection();
				case Obymobi.Data.EntityType.AffiliateCampaignAffiliatePartnerEntity:
					return new AffiliateCampaignAffiliatePartnerCollection();
				case Obymobi.Data.EntityType.AffiliatePartnerEntity:
					return new AffiliatePartnerCollection();
				case Obymobi.Data.EntityType.AlterationEntity:
					return new AlterationCollection();
				case Obymobi.Data.EntityType.AlterationitemEntity:
					return new AlterationitemCollection();
				case Obymobi.Data.EntityType.AlterationitemAlterationEntity:
					return new AlterationitemAlterationCollection();
				case Obymobi.Data.EntityType.AlterationLanguageEntity:
					return new AlterationLanguageCollection();
				case Obymobi.Data.EntityType.AlterationoptionEntity:
					return new AlterationoptionCollection();
				case Obymobi.Data.EntityType.AlterationoptionLanguageEntity:
					return new AlterationoptionLanguageCollection();
				case Obymobi.Data.EntityType.AlterationoptionTagEntity:
					return new AlterationoptionTagCollection();
				case Obymobi.Data.EntityType.AlterationProductEntity:
					return new AlterationProductCollection();
				case Obymobi.Data.EntityType.AmenityEntity:
					return new AmenityCollection();
				case Obymobi.Data.EntityType.AmenityLanguageEntity:
					return new AmenityLanguageCollection();
				case Obymobi.Data.EntityType.AnalyticsProcessingTaskEntity:
					return new AnalyticsProcessingTaskCollection();
				case Obymobi.Data.EntityType.AnnouncementEntity:
					return new AnnouncementCollection();
				case Obymobi.Data.EntityType.AnnouncementLanguageEntity:
					return new AnnouncementLanguageCollection();
				case Obymobi.Data.EntityType.ApiAuthenticationEntity:
					return new ApiAuthenticationCollection();
				case Obymobi.Data.EntityType.ApplicationEntity:
					return new ApplicationCollection();
				case Obymobi.Data.EntityType.AttachmentEntity:
					return new AttachmentCollection();
				case Obymobi.Data.EntityType.AttachmentLanguageEntity:
					return new AttachmentLanguageCollection();
				case Obymobi.Data.EntityType.AuditlogEntity:
					return new AuditlogCollection();
				case Obymobi.Data.EntityType.AvailabilityEntity:
					return new AvailabilityCollection();
				case Obymobi.Data.EntityType.AzureNotificationHubEntity:
					return new AzureNotificationHubCollection();
				case Obymobi.Data.EntityType.BrandEntity:
					return new BrandCollection();
				case Obymobi.Data.EntityType.BrandCultureEntity:
					return new BrandCultureCollection();
				case Obymobi.Data.EntityType.BusinesshoursEntity:
					return new BusinesshoursCollection();
				case Obymobi.Data.EntityType.BusinesshoursexceptionEntity:
					return new BusinesshoursexceptionCollection();
				case Obymobi.Data.EntityType.CategoryEntity:
					return new CategoryCollection();
				case Obymobi.Data.EntityType.CategoryAlterationEntity:
					return new CategoryAlterationCollection();
				case Obymobi.Data.EntityType.CategoryLanguageEntity:
					return new CategoryLanguageCollection();
				case Obymobi.Data.EntityType.CategorySuggestionEntity:
					return new CategorySuggestionCollection();
				case Obymobi.Data.EntityType.CategoryTagEntity:
					return new CategoryTagCollection();
				case Obymobi.Data.EntityType.CheckoutMethodEntity:
					return new CheckoutMethodCollection();
				case Obymobi.Data.EntityType.CheckoutMethodDeliverypointgroupEntity:
					return new CheckoutMethodDeliverypointgroupCollection();
				case Obymobi.Data.EntityType.ClientEntity:
					return new ClientCollection();
				case Obymobi.Data.EntityType.ClientConfigurationEntity:
					return new ClientConfigurationCollection();
				case Obymobi.Data.EntityType.ClientConfigurationRouteEntity:
					return new ClientConfigurationRouteCollection();
				case Obymobi.Data.EntityType.ClientEntertainmentEntity:
					return new ClientEntertainmentCollection();
				case Obymobi.Data.EntityType.ClientLogEntity:
					return new ClientLogCollection();
				case Obymobi.Data.EntityType.ClientLogFileEntity:
					return new ClientLogFileCollection();
				case Obymobi.Data.EntityType.ClientStateEntity:
					return new ClientStateCollection();
				case Obymobi.Data.EntityType.CloudApplicationVersionEntity:
					return new CloudApplicationVersionCollection();
				case Obymobi.Data.EntityType.CloudProcessingTaskEntity:
					return new CloudProcessingTaskCollection();
				case Obymobi.Data.EntityType.CloudStorageAccountEntity:
					return new CloudStorageAccountCollection();
				case Obymobi.Data.EntityType.CompanyEntity:
					return new CompanyCollection();
				case Obymobi.Data.EntityType.CompanyAmenityEntity:
					return new CompanyAmenityCollection();
				case Obymobi.Data.EntityType.CompanyBrandEntity:
					return new CompanyBrandCollection();
				case Obymobi.Data.EntityType.CompanyCultureEntity:
					return new CompanyCultureCollection();
				case Obymobi.Data.EntityType.CompanyCurrencyEntity:
					return new CompanyCurrencyCollection();
				case Obymobi.Data.EntityType.CompanyEntertainmentEntity:
					return new CompanyEntertainmentCollection();
				case Obymobi.Data.EntityType.CompanygroupEntity:
					return new CompanygroupCollection();
				case Obymobi.Data.EntityType.CompanyLanguageEntity:
					return new CompanyLanguageCollection();
				case Obymobi.Data.EntityType.CompanyManagementTaskEntity:
					return new CompanyManagementTaskCollection();
				case Obymobi.Data.EntityType.CompanyOwnerEntity:
					return new CompanyOwnerCollection();
				case Obymobi.Data.EntityType.CompanyReleaseEntity:
					return new CompanyReleaseCollection();
				case Obymobi.Data.EntityType.CompanyVenueCategoryEntity:
					return new CompanyVenueCategoryCollection();
				case Obymobi.Data.EntityType.ConfigurationEntity:
					return new ConfigurationCollection();
				case Obymobi.Data.EntityType.CountryEntity:
					return new CountryCollection();
				case Obymobi.Data.EntityType.CurrencyEntity:
					return new CurrencyCollection();
				case Obymobi.Data.EntityType.CustomerEntity:
					return new CustomerCollection();
				case Obymobi.Data.EntityType.CustomTextEntity:
					return new CustomTextCollection();
				case Obymobi.Data.EntityType.DeliveryDistanceEntity:
					return new DeliveryDistanceCollection();
				case Obymobi.Data.EntityType.DeliveryInformationEntity:
					return new DeliveryInformationCollection();
				case Obymobi.Data.EntityType.DeliverypointEntity:
					return new DeliverypointCollection();
				case Obymobi.Data.EntityType.DeliverypointExternalDeliverypointEntity:
					return new DeliverypointExternalDeliverypointCollection();
				case Obymobi.Data.EntityType.DeliverypointgroupEntity:
					return new DeliverypointgroupCollection();
				case Obymobi.Data.EntityType.DeliverypointgroupAdvertisementEntity:
					return new DeliverypointgroupAdvertisementCollection();
				case Obymobi.Data.EntityType.DeliverypointgroupAnnouncementEntity:
					return new DeliverypointgroupAnnouncementCollection();
				case Obymobi.Data.EntityType.DeliverypointgroupEntertainmentEntity:
					return new DeliverypointgroupEntertainmentCollection();
				case Obymobi.Data.EntityType.DeliverypointgroupLanguageEntity:
					return new DeliverypointgroupLanguageCollection();
				case Obymobi.Data.EntityType.DeliverypointgroupOccupancyEntity:
					return new DeliverypointgroupOccupancyCollection();
				case Obymobi.Data.EntityType.DeliverypointgroupProductEntity:
					return new DeliverypointgroupProductCollection();
				case Obymobi.Data.EntityType.DeviceEntity:
					return new DeviceCollection();
				case Obymobi.Data.EntityType.DevicemediaEntity:
					return new DevicemediaCollection();
				case Obymobi.Data.EntityType.DeviceTokenHistoryEntity:
					return new DeviceTokenHistoryCollection();
				case Obymobi.Data.EntityType.DeviceTokenTaskEntity:
					return new DeviceTokenTaskCollection();
				case Obymobi.Data.EntityType.EntertainmentEntity:
					return new EntertainmentCollection();
				case Obymobi.Data.EntityType.EntertainmentcategoryEntity:
					return new EntertainmentcategoryCollection();
				case Obymobi.Data.EntityType.EntertainmentcategoryLanguageEntity:
					return new EntertainmentcategoryLanguageCollection();
				case Obymobi.Data.EntityType.EntertainmentConfigurationEntity:
					return new EntertainmentConfigurationCollection();
				case Obymobi.Data.EntityType.EntertainmentConfigurationEntertainmentEntity:
					return new EntertainmentConfigurationEntertainmentCollection();
				case Obymobi.Data.EntityType.EntertainmentDependencyEntity:
					return new EntertainmentDependencyCollection();
				case Obymobi.Data.EntityType.EntertainmentFileEntity:
					return new EntertainmentFileCollection();
				case Obymobi.Data.EntityType.EntertainmenturlEntity:
					return new EntertainmenturlCollection();
				case Obymobi.Data.EntityType.EntityFieldInformationEntity:
					return new EntityFieldInformationCollection();
				case Obymobi.Data.EntityType.EntityFieldInformationCustomEntity:
					return new EntityFieldInformationCustomCollection();
				case Obymobi.Data.EntityType.EntityInformationEntity:
					return new EntityInformationCollection();
				case Obymobi.Data.EntityType.EntityInformationCustomEntity:
					return new EntityInformationCustomCollection();
				case Obymobi.Data.EntityType.ExternalDeliverypointEntity:
					return new ExternalDeliverypointCollection();
				case Obymobi.Data.EntityType.ExternalMenuEntity:
					return new ExternalMenuCollection();
				case Obymobi.Data.EntityType.ExternalProductEntity:
					return new ExternalProductCollection();
				case Obymobi.Data.EntityType.ExternalSubProductEntity:
					return new ExternalSubProductCollection();
				case Obymobi.Data.EntityType.ExternalSystemEntity:
					return new ExternalSystemCollection();
				case Obymobi.Data.EntityType.ExternalSystemLogEntity:
					return new ExternalSystemLogCollection();
				case Obymobi.Data.EntityType.FeatureToggleAvailabilityEntity:
					return new FeatureToggleAvailabilityCollection();
				case Obymobi.Data.EntityType.FolioEntity:
					return new FolioCollection();
				case Obymobi.Data.EntityType.FolioItemEntity:
					return new FolioItemCollection();
				case Obymobi.Data.EntityType.GameEntity:
					return new GameCollection();
				case Obymobi.Data.EntityType.GameSessionEntity:
					return new GameSessionCollection();
				case Obymobi.Data.EntityType.GameSessionReportEntity:
					return new GameSessionReportCollection();
				case Obymobi.Data.EntityType.GameSessionReportConfigurationEntity:
					return new GameSessionReportConfigurationCollection();
				case Obymobi.Data.EntityType.GameSessionReportItemEntity:
					return new GameSessionReportItemCollection();
				case Obymobi.Data.EntityType.GenericalterationEntity:
					return new GenericalterationCollection();
				case Obymobi.Data.EntityType.GenericalterationitemEntity:
					return new GenericalterationitemCollection();
				case Obymobi.Data.EntityType.GenericalterationoptionEntity:
					return new GenericalterationoptionCollection();
				case Obymobi.Data.EntityType.GenericcategoryEntity:
					return new GenericcategoryCollection();
				case Obymobi.Data.EntityType.GenericcategoryLanguageEntity:
					return new GenericcategoryLanguageCollection();
				case Obymobi.Data.EntityType.GenericproductEntity:
					return new GenericproductCollection();
				case Obymobi.Data.EntityType.GenericproductGenericalterationEntity:
					return new GenericproductGenericalterationCollection();
				case Obymobi.Data.EntityType.GenericproductLanguageEntity:
					return new GenericproductLanguageCollection();
				case Obymobi.Data.EntityType.GuestInformationEntity:
					return new GuestInformationCollection();
				case Obymobi.Data.EntityType.IcrtouchprintermappingEntity:
					return new IcrtouchprintermappingCollection();
				case Obymobi.Data.EntityType.IcrtouchprintermappingDeliverypointEntity:
					return new IcrtouchprintermappingDeliverypointCollection();
				case Obymobi.Data.EntityType.IncomingSmsEntity:
					return new IncomingSmsCollection();
				case Obymobi.Data.EntityType.InfraredCommandEntity:
					return new InfraredCommandCollection();
				case Obymobi.Data.EntityType.InfraredConfigurationEntity:
					return new InfraredConfigurationCollection();
				case Obymobi.Data.EntityType.LanguageEntity:
					return new LanguageCollection();
				case Obymobi.Data.EntityType.LicensedModuleEntity:
					return new LicensedModuleCollection();
				case Obymobi.Data.EntityType.LicensedUIElementEntity:
					return new LicensedUIElementCollection();
				case Obymobi.Data.EntityType.LicensedUIElementSubPanelEntity:
					return new LicensedUIElementSubPanelCollection();
				case Obymobi.Data.EntityType.MapEntity:
					return new MapCollection();
				case Obymobi.Data.EntityType.MapPointOfInterestEntity:
					return new MapPointOfInterestCollection();
				case Obymobi.Data.EntityType.MediaEntity:
					return new MediaCollection();
				case Obymobi.Data.EntityType.MediaCultureEntity:
					return new MediaCultureCollection();
				case Obymobi.Data.EntityType.MediaLanguageEntity:
					return new MediaLanguageCollection();
				case Obymobi.Data.EntityType.MediaProcessingTaskEntity:
					return new MediaProcessingTaskCollection();
				case Obymobi.Data.EntityType.MediaRatioTypeMediaEntity:
					return new MediaRatioTypeMediaCollection();
				case Obymobi.Data.EntityType.MediaRatioTypeMediaFileEntity:
					return new MediaRatioTypeMediaFileCollection();
				case Obymobi.Data.EntityType.MediaRelationshipEntity:
					return new MediaRelationshipCollection();
				case Obymobi.Data.EntityType.MenuEntity:
					return new MenuCollection();
				case Obymobi.Data.EntityType.MessageEntity:
					return new MessageCollection();
				case Obymobi.Data.EntityType.MessagegroupEntity:
					return new MessagegroupCollection();
				case Obymobi.Data.EntityType.MessagegroupDeliverypointEntity:
					return new MessagegroupDeliverypointCollection();
				case Obymobi.Data.EntityType.MessageRecipientEntity:
					return new MessageRecipientCollection();
				case Obymobi.Data.EntityType.MessageTemplateEntity:
					return new MessageTemplateCollection();
				case Obymobi.Data.EntityType.MessageTemplateCategoryEntity:
					return new MessageTemplateCategoryCollection();
				case Obymobi.Data.EntityType.MessageTemplateCategoryMessageTemplateEntity:
					return new MessageTemplateCategoryMessageTemplateCollection();
				case Obymobi.Data.EntityType.ModuleEntity:
					return new ModuleCollection();
				case Obymobi.Data.EntityType.NetmessageEntity:
					return new NetmessageCollection();
				case Obymobi.Data.EntityType.OptInEntity:
					return new OptInCollection();
				case Obymobi.Data.EntityType.OrderEntity:
					return new OrderCollection();
				case Obymobi.Data.EntityType.OrderHourEntity:
					return new OrderHourCollection();
				case Obymobi.Data.EntityType.OrderitemEntity:
					return new OrderitemCollection();
				case Obymobi.Data.EntityType.OrderitemAlterationitemEntity:
					return new OrderitemAlterationitemCollection();
				case Obymobi.Data.EntityType.OrderitemAlterationitemTagEntity:
					return new OrderitemAlterationitemTagCollection();
				case Obymobi.Data.EntityType.OrderitemTagEntity:
					return new OrderitemTagCollection();
				case Obymobi.Data.EntityType.OrderNotificationLogEntity:
					return new OrderNotificationLogCollection();
				case Obymobi.Data.EntityType.OrderRoutestephandlerEntity:
					return new OrderRoutestephandlerCollection();
				case Obymobi.Data.EntityType.OrderRoutestephandlerHistoryEntity:
					return new OrderRoutestephandlerHistoryCollection();
				case Obymobi.Data.EntityType.OutletEntity:
					return new OutletCollection();
				case Obymobi.Data.EntityType.OutletOperationalStateEntity:
					return new OutletOperationalStateCollection();
				case Obymobi.Data.EntityType.OutletSellerInformationEntity:
					return new OutletSellerInformationCollection();
				case Obymobi.Data.EntityType.PageEntity:
					return new PageCollection();
				case Obymobi.Data.EntityType.PageElementEntity:
					return new PageElementCollection();
				case Obymobi.Data.EntityType.PageLanguageEntity:
					return new PageLanguageCollection();
				case Obymobi.Data.EntityType.PageTemplateEntity:
					return new PageTemplateCollection();
				case Obymobi.Data.EntityType.PageTemplateElementEntity:
					return new PageTemplateElementCollection();
				case Obymobi.Data.EntityType.PageTemplateLanguageEntity:
					return new PageTemplateLanguageCollection();
				case Obymobi.Data.EntityType.PaymentIntegrationConfigurationEntity:
					return new PaymentIntegrationConfigurationCollection();
				case Obymobi.Data.EntityType.PaymentProviderEntity:
					return new PaymentProviderCollection();
				case Obymobi.Data.EntityType.PaymentProviderCompanyEntity:
					return new PaymentProviderCompanyCollection();
				case Obymobi.Data.EntityType.PaymentTransactionEntity:
					return new PaymentTransactionCollection();
				case Obymobi.Data.EntityType.PaymentTransactionLogEntity:
					return new PaymentTransactionLogCollection();
				case Obymobi.Data.EntityType.PaymentTransactionSplitEntity:
					return new PaymentTransactionSplitCollection();
				case Obymobi.Data.EntityType.PmsActionRuleEntity:
					return new PmsActionRuleCollection();
				case Obymobi.Data.EntityType.PmsReportColumnEntity:
					return new PmsReportColumnCollection();
				case Obymobi.Data.EntityType.PmsReportConfigurationEntity:
					return new PmsReportConfigurationCollection();
				case Obymobi.Data.EntityType.PmsRuleEntity:
					return new PmsRuleCollection();
				case Obymobi.Data.EntityType.PointOfInterestEntity:
					return new PointOfInterestCollection();
				case Obymobi.Data.EntityType.PointOfInterestAmenityEntity:
					return new PointOfInterestAmenityCollection();
				case Obymobi.Data.EntityType.PointOfInterestLanguageEntity:
					return new PointOfInterestLanguageCollection();
				case Obymobi.Data.EntityType.PointOfInterestVenueCategoryEntity:
					return new PointOfInterestVenueCategoryCollection();
				case Obymobi.Data.EntityType.PosalterationEntity:
					return new PosalterationCollection();
				case Obymobi.Data.EntityType.PosalterationitemEntity:
					return new PosalterationitemCollection();
				case Obymobi.Data.EntityType.PosalterationoptionEntity:
					return new PosalterationoptionCollection();
				case Obymobi.Data.EntityType.PoscategoryEntity:
					return new PoscategoryCollection();
				case Obymobi.Data.EntityType.PosdeliverypointEntity:
					return new PosdeliverypointCollection();
				case Obymobi.Data.EntityType.PosdeliverypointgroupEntity:
					return new PosdeliverypointgroupCollection();
				case Obymobi.Data.EntityType.PospaymentmethodEntity:
					return new PospaymentmethodCollection();
				case Obymobi.Data.EntityType.PosproductEntity:
					return new PosproductCollection();
				case Obymobi.Data.EntityType.PosproductPosalterationEntity:
					return new PosproductPosalterationCollection();
				case Obymobi.Data.EntityType.PriceLevelEntity:
					return new PriceLevelCollection();
				case Obymobi.Data.EntityType.PriceLevelItemEntity:
					return new PriceLevelItemCollection();
				case Obymobi.Data.EntityType.PriceScheduleEntity:
					return new PriceScheduleCollection();
				case Obymobi.Data.EntityType.PriceScheduleItemEntity:
					return new PriceScheduleItemCollection();
				case Obymobi.Data.EntityType.PriceScheduleItemOccurrenceEntity:
					return new PriceScheduleItemOccurrenceCollection();
				case Obymobi.Data.EntityType.ProductEntity:
					return new ProductCollection();
				case Obymobi.Data.EntityType.ProductAlterationEntity:
					return new ProductAlterationCollection();
				case Obymobi.Data.EntityType.ProductAttachmentEntity:
					return new ProductAttachmentCollection();
				case Obymobi.Data.EntityType.ProductCategoryEntity:
					return new ProductCategoryCollection();
				case Obymobi.Data.EntityType.ProductCategorySuggestionEntity:
					return new ProductCategorySuggestionCollection();
				case Obymobi.Data.EntityType.ProductCategoryTagEntity:
					return new ProductCategoryTagCollection();
				case Obymobi.Data.EntityType.ProductgroupEntity:
					return new ProductgroupCollection();
				case Obymobi.Data.EntityType.ProductgroupItemEntity:
					return new ProductgroupItemCollection();
				case Obymobi.Data.EntityType.ProductLanguageEntity:
					return new ProductLanguageCollection();
				case Obymobi.Data.EntityType.ProductSuggestionEntity:
					return new ProductSuggestionCollection();
				case Obymobi.Data.EntityType.ProductTagEntity:
					return new ProductTagCollection();
				case Obymobi.Data.EntityType.PublishingEntity:
					return new PublishingCollection();
				case Obymobi.Data.EntityType.PublishingItemEntity:
					return new PublishingItemCollection();
				case Obymobi.Data.EntityType.RatingEntity:
					return new RatingCollection();
				case Obymobi.Data.EntityType.ReceiptEntity:
					return new ReceiptCollection();
				case Obymobi.Data.EntityType.ReceiptTemplateEntity:
					return new ReceiptTemplateCollection();
				case Obymobi.Data.EntityType.ReferentialConstraintEntity:
					return new ReferentialConstraintCollection();
				case Obymobi.Data.EntityType.ReferentialConstraintCustomEntity:
					return new ReferentialConstraintCustomCollection();
				case Obymobi.Data.EntityType.ReleaseEntity:
					return new ReleaseCollection();
				case Obymobi.Data.EntityType.ReportProcessingTaskEntity:
					return new ReportProcessingTaskCollection();
				case Obymobi.Data.EntityType.ReportProcessingTaskFileEntity:
					return new ReportProcessingTaskFileCollection();
				case Obymobi.Data.EntityType.ReportProcessingTaskTemplateEntity:
					return new ReportProcessingTaskTemplateCollection();
				case Obymobi.Data.EntityType.RequestlogEntity:
					return new RequestlogCollection();
				case Obymobi.Data.EntityType.RoleEntity:
					return new RoleCollection();
				case Obymobi.Data.EntityType.RoleModuleRightsEntity:
					return new RoleModuleRightsCollection();
				case Obymobi.Data.EntityType.RoleUIElementRightsEntity:
					return new RoleUIElementRightsCollection();
				case Obymobi.Data.EntityType.RoleUIElementSubPanelRightsEntity:
					return new RoleUIElementSubPanelRightsCollection();
				case Obymobi.Data.EntityType.RoomControlAreaEntity:
					return new RoomControlAreaCollection();
				case Obymobi.Data.EntityType.RoomControlAreaLanguageEntity:
					return new RoomControlAreaLanguageCollection();
				case Obymobi.Data.EntityType.RoomControlComponentEntity:
					return new RoomControlComponentCollection();
				case Obymobi.Data.EntityType.RoomControlComponentLanguageEntity:
					return new RoomControlComponentLanguageCollection();
				case Obymobi.Data.EntityType.RoomControlConfigurationEntity:
					return new RoomControlConfigurationCollection();
				case Obymobi.Data.EntityType.RoomControlSectionEntity:
					return new RoomControlSectionCollection();
				case Obymobi.Data.EntityType.RoomControlSectionItemEntity:
					return new RoomControlSectionItemCollection();
				case Obymobi.Data.EntityType.RoomControlSectionItemLanguageEntity:
					return new RoomControlSectionItemLanguageCollection();
				case Obymobi.Data.EntityType.RoomControlSectionLanguageEntity:
					return new RoomControlSectionLanguageCollection();
				case Obymobi.Data.EntityType.RoomControlWidgetEntity:
					return new RoomControlWidgetCollection();
				case Obymobi.Data.EntityType.RoomControlWidgetLanguageEntity:
					return new RoomControlWidgetLanguageCollection();
				case Obymobi.Data.EntityType.RouteEntity:
					return new RouteCollection();
				case Obymobi.Data.EntityType.RoutestepEntity:
					return new RoutestepCollection();
				case Obymobi.Data.EntityType.RoutestephandlerEntity:
					return new RoutestephandlerCollection();
				case Obymobi.Data.EntityType.ScheduleEntity:
					return new ScheduleCollection();
				case Obymobi.Data.EntityType.ScheduledCommandEntity:
					return new ScheduledCommandCollection();
				case Obymobi.Data.EntityType.ScheduledCommandTaskEntity:
					return new ScheduledCommandTaskCollection();
				case Obymobi.Data.EntityType.ScheduledMessageEntity:
					return new ScheduledMessageCollection();
				case Obymobi.Data.EntityType.ScheduledMessageHistoryEntity:
					return new ScheduledMessageHistoryCollection();
				case Obymobi.Data.EntityType.ScheduledMessageLanguageEntity:
					return new ScheduledMessageLanguageCollection();
				case Obymobi.Data.EntityType.ScheduleitemEntity:
					return new ScheduleitemCollection();
				case Obymobi.Data.EntityType.ServiceMethodEntity:
					return new ServiceMethodCollection();
				case Obymobi.Data.EntityType.ServiceMethodDeliverypointgroupEntity:
					return new ServiceMethodDeliverypointgroupCollection();
				case Obymobi.Data.EntityType.SetupCodeEntity:
					return new SetupCodeCollection();
				case Obymobi.Data.EntityType.SiteEntity:
					return new SiteCollection();
				case Obymobi.Data.EntityType.SiteCultureEntity:
					return new SiteCultureCollection();
				case Obymobi.Data.EntityType.SiteLanguageEntity:
					return new SiteLanguageCollection();
				case Obymobi.Data.EntityType.SiteTemplateEntity:
					return new SiteTemplateCollection();
				case Obymobi.Data.EntityType.SiteTemplateCultureEntity:
					return new SiteTemplateCultureCollection();
				case Obymobi.Data.EntityType.SiteTemplateLanguageEntity:
					return new SiteTemplateLanguageCollection();
				case Obymobi.Data.EntityType.SmsInformationEntity:
					return new SmsInformationCollection();
				case Obymobi.Data.EntityType.SmsKeywordEntity:
					return new SmsKeywordCollection();
				case Obymobi.Data.EntityType.StationEntity:
					return new StationCollection();
				case Obymobi.Data.EntityType.StationLanguageEntity:
					return new StationLanguageCollection();
				case Obymobi.Data.EntityType.StationListEntity:
					return new StationListCollection();
				case Obymobi.Data.EntityType.SupplierEntity:
					return new SupplierCollection();
				case Obymobi.Data.EntityType.SupportagentEntity:
					return new SupportagentCollection();
				case Obymobi.Data.EntityType.SupportpoolEntity:
					return new SupportpoolCollection();
				case Obymobi.Data.EntityType.SupportpoolNotificationRecipientEntity:
					return new SupportpoolNotificationRecipientCollection();
				case Obymobi.Data.EntityType.SupportpoolSupportagentEntity:
					return new SupportpoolSupportagentCollection();
				case Obymobi.Data.EntityType.SurveyEntity:
					return new SurveyCollection();
				case Obymobi.Data.EntityType.SurveyAnswerEntity:
					return new SurveyAnswerCollection();
				case Obymobi.Data.EntityType.SurveyAnswerLanguageEntity:
					return new SurveyAnswerLanguageCollection();
				case Obymobi.Data.EntityType.SurveyLanguageEntity:
					return new SurveyLanguageCollection();
				case Obymobi.Data.EntityType.SurveyPageEntity:
					return new SurveyPageCollection();
				case Obymobi.Data.EntityType.SurveyQuestionEntity:
					return new SurveyQuestionCollection();
				case Obymobi.Data.EntityType.SurveyQuestionLanguageEntity:
					return new SurveyQuestionLanguageCollection();
				case Obymobi.Data.EntityType.SurveyResultEntity:
					return new SurveyResultCollection();
				case Obymobi.Data.EntityType.TagEntity:
					return new TagCollection();
				case Obymobi.Data.EntityType.TaxTariffEntity:
					return new TaxTariffCollection();
				case Obymobi.Data.EntityType.TerminalEntity:
					return new TerminalCollection();
				case Obymobi.Data.EntityType.TerminalConfigurationEntity:
					return new TerminalConfigurationCollection();
				case Obymobi.Data.EntityType.TerminalLogEntity:
					return new TerminalLogCollection();
				case Obymobi.Data.EntityType.TerminalLogFileEntity:
					return new TerminalLogFileCollection();
				case Obymobi.Data.EntityType.TerminalMessageTemplateCategoryEntity:
					return new TerminalMessageTemplateCategoryCollection();
				case Obymobi.Data.EntityType.TerminalStateEntity:
					return new TerminalStateCollection();
				case Obymobi.Data.EntityType.TimestampEntity:
					return new TimestampCollection();
				case Obymobi.Data.EntityType.TimeZoneEntity:
					return new TimeZoneCollection();
				case Obymobi.Data.EntityType.TranslationEntity:
					return new TranslationCollection();
				case Obymobi.Data.EntityType.UIElementEntity:
					return new UIElementCollection();
				case Obymobi.Data.EntityType.UIElementCustomEntity:
					return new UIElementCustomCollection();
				case Obymobi.Data.EntityType.UIElementSubPanelEntity:
					return new UIElementSubPanelCollection();
				case Obymobi.Data.EntityType.UIElementSubPanelCustomEntity:
					return new UIElementSubPanelCustomCollection();
				case Obymobi.Data.EntityType.UIElementSubPanelUIElementEntity:
					return new UIElementSubPanelUIElementCollection();
				case Obymobi.Data.EntityType.UIFooterItemEntity:
					return new UIFooterItemCollection();
				case Obymobi.Data.EntityType.UIFooterItemLanguageEntity:
					return new UIFooterItemLanguageCollection();
				case Obymobi.Data.EntityType.UIModeEntity:
					return new UIModeCollection();
				case Obymobi.Data.EntityType.UIScheduleEntity:
					return new UIScheduleCollection();
				case Obymobi.Data.EntityType.UIScheduleItemEntity:
					return new UIScheduleItemCollection();
				case Obymobi.Data.EntityType.UIScheduleItemOccurrenceEntity:
					return new UIScheduleItemOccurrenceCollection();
				case Obymobi.Data.EntityType.UITabEntity:
					return new UITabCollection();
				case Obymobi.Data.EntityType.UITabLanguageEntity:
					return new UITabLanguageCollection();
				case Obymobi.Data.EntityType.UIThemeEntity:
					return new UIThemeCollection();
				case Obymobi.Data.EntityType.UIThemeColorEntity:
					return new UIThemeColorCollection();
				case Obymobi.Data.EntityType.UIThemeTextSizeEntity:
					return new UIThemeTextSizeCollection();
				case Obymobi.Data.EntityType.UIWidgetEntity:
					return new UIWidgetCollection();
				case Obymobi.Data.EntityType.UIWidgetAvailabilityEntity:
					return new UIWidgetAvailabilityCollection();
				case Obymobi.Data.EntityType.UIWidgetLanguageEntity:
					return new UIWidgetLanguageCollection();
				case Obymobi.Data.EntityType.UIWidgetTimerEntity:
					return new UIWidgetTimerCollection();
				case Obymobi.Data.EntityType.UserEntity:
					return new UserCollection();
				case Obymobi.Data.EntityType.UserBrandEntity:
					return new UserBrandCollection();
				case Obymobi.Data.EntityType.UserLogonEntity:
					return new UserLogonCollection();
				case Obymobi.Data.EntityType.UserRoleEntity:
					return new UserRoleCollection();
				case Obymobi.Data.EntityType.VattariffEntity:
					return new VattariffCollection();
				case Obymobi.Data.EntityType.VendorEntity:
					return new VendorCollection();
				case Obymobi.Data.EntityType.VenueCategoryEntity:
					return new VenueCategoryCollection();
				case Obymobi.Data.EntityType.VenueCategoryLanguageEntity:
					return new VenueCategoryLanguageCollection();
				case Obymobi.Data.EntityType.ViewEntity:
					return new ViewCollection();
				case Obymobi.Data.EntityType.ViewCustomEntity:
					return new ViewCustomCollection();
				case Obymobi.Data.EntityType.ViewItemEntity:
					return new ViewItemCollection();
				case Obymobi.Data.EntityType.ViewItemCustomEntity:
					return new ViewItemCustomCollection();
				case Obymobi.Data.EntityType.WeatherEntity:
					return new WeatherCollection();
				case Obymobi.Data.EntityType.WifiConfigurationEntity:
					return new WifiConfigurationCollection();
				case Obymobi.Data.EntityType.ActionEntity:
					return new ActionCollection();
				case Obymobi.Data.EntityType.ApplicationConfigurationEntity:
					return new ApplicationConfigurationCollection();
				case Obymobi.Data.EntityType.CarouselItemEntity:
					return new CarouselItemCollection();
				case Obymobi.Data.EntityType.FeatureFlagEntity:
					return new FeatureFlagCollection();
				case Obymobi.Data.EntityType.LandingPageEntity:
					return new LandingPageCollection();
				case Obymobi.Data.EntityType.LandingPageWidgetEntity:
					return new LandingPageWidgetCollection();
				case Obymobi.Data.EntityType.NavigationMenuEntity:
					return new NavigationMenuCollection();
				case Obymobi.Data.EntityType.NavigationMenuItemEntity:
					return new NavigationMenuItemCollection();
				case Obymobi.Data.EntityType.NavigationMenuWidgetEntity:
					return new NavigationMenuWidgetCollection();
				case Obymobi.Data.EntityType.ThemeEntity:
					return new ThemeCollection();
				case Obymobi.Data.EntityType.WidgetEntity:
					return new WidgetCollection();
				case Obymobi.Data.EntityType.WidgetActionBannerEntity:
					return new WidgetActionBannerCollection();
				case Obymobi.Data.EntityType.WidgetActionButtonEntity:
					return new WidgetActionButtonCollection();
				case Obymobi.Data.EntityType.WidgetCarouselEntity:
					return new WidgetCarouselCollection();
				case Obymobi.Data.EntityType.WidgetGroupEntity:
					return new WidgetGroupCollection();
				case Obymobi.Data.EntityType.WidgetGroupWidgetEntity:
					return new WidgetGroupWidgetCollection();
				case Obymobi.Data.EntityType.WidgetHeroEntity:
					return new WidgetHeroCollection();
				case Obymobi.Data.EntityType.WidgetLanguageSwitcherEntity:
					return new WidgetLanguageSwitcherCollection();
				case Obymobi.Data.EntityType.WidgetMarkdownEntity:
					return new WidgetMarkdownCollection();
				case Obymobi.Data.EntityType.WidgetOpeningTimeEntity:
					return new WidgetOpeningTimeCollection();
				case Obymobi.Data.EntityType.WidgetPageTitleEntity:
					return new WidgetPageTitleCollection();
				case Obymobi.Data.EntityType.WidgetWaitTimeEntity:
					return new WidgetWaitTimeCollection();
				default:
					return null;
			}
		}		
	}
	
	/// <summary>Factory to create new, empty Entity objects based on the entity type specified. Uses entity specific factory objects</summary>
	[Serializable]
	public partial class GeneralEntityFactory
	{
		/// <summary>Creates a new, empty Entity object of the type specified</summary>
		/// <param name="entityTypeToCreate">The entity type to create.</param>
		/// <returns>A new, empty Entity object.</returns>
		public static IEntity Create(Obymobi.Data.EntityType entityTypeToCreate)
		{
			IEntityFactory factoryToUse = null;
			switch(entityTypeToCreate)
			{
				case Obymobi.Data.EntityType.AccessCodeEntity:
					factoryToUse = new AccessCodeEntityFactory();
					break;
				case Obymobi.Data.EntityType.AccessCodeCompanyEntity:
					factoryToUse = new AccessCodeCompanyEntityFactory();
					break;
				case Obymobi.Data.EntityType.AccessCodePointOfInterestEntity:
					factoryToUse = new AccessCodePointOfInterestEntityFactory();
					break;
				case Obymobi.Data.EntityType.AccountEntity:
					factoryToUse = new AccountEntityFactory();
					break;
				case Obymobi.Data.EntityType.AccountCompanyEntity:
					factoryToUse = new AccountCompanyEntityFactory();
					break;
				case Obymobi.Data.EntityType.ActionButtonEntity:
					factoryToUse = new ActionButtonEntityFactory();
					break;
				case Obymobi.Data.EntityType.ActionButtonLanguageEntity:
					factoryToUse = new ActionButtonLanguageEntityFactory();
					break;
				case Obymobi.Data.EntityType.AddressEntity:
					factoryToUse = new AddressEntityFactory();
					break;
				case Obymobi.Data.EntityType.AdvertisementEntity:
					factoryToUse = new AdvertisementEntityFactory();
					break;
				case Obymobi.Data.EntityType.AdvertisementConfigurationEntity:
					factoryToUse = new AdvertisementConfigurationEntityFactory();
					break;
				case Obymobi.Data.EntityType.AdvertisementConfigurationAdvertisementEntity:
					factoryToUse = new AdvertisementConfigurationAdvertisementEntityFactory();
					break;
				case Obymobi.Data.EntityType.AdvertisementLanguageEntity:
					factoryToUse = new AdvertisementLanguageEntityFactory();
					break;
				case Obymobi.Data.EntityType.AdvertisementTagEntity:
					factoryToUse = new AdvertisementTagEntityFactory();
					break;
				case Obymobi.Data.EntityType.AdvertisementTagAdvertisementEntity:
					factoryToUse = new AdvertisementTagAdvertisementEntityFactory();
					break;
				case Obymobi.Data.EntityType.AdvertisementTagCategoryEntity:
					factoryToUse = new AdvertisementTagCategoryEntityFactory();
					break;
				case Obymobi.Data.EntityType.AdvertisementTagEntertainmentEntity:
					factoryToUse = new AdvertisementTagEntertainmentEntityFactory();
					break;
				case Obymobi.Data.EntityType.AdvertisementTagGenericproductEntity:
					factoryToUse = new AdvertisementTagGenericproductEntityFactory();
					break;
				case Obymobi.Data.EntityType.AdvertisementTagLanguageEntity:
					factoryToUse = new AdvertisementTagLanguageEntityFactory();
					break;
				case Obymobi.Data.EntityType.AdvertisementTagProductEntity:
					factoryToUse = new AdvertisementTagProductEntityFactory();
					break;
				case Obymobi.Data.EntityType.AdyenPaymentMethodEntity:
					factoryToUse = new AdyenPaymentMethodEntityFactory();
					break;
				case Obymobi.Data.EntityType.AdyenPaymentMethodBrandEntity:
					factoryToUse = new AdyenPaymentMethodBrandEntityFactory();
					break;
				case Obymobi.Data.EntityType.AffiliateCampaignEntity:
					factoryToUse = new AffiliateCampaignEntityFactory();
					break;
				case Obymobi.Data.EntityType.AffiliateCampaignAffiliatePartnerEntity:
					factoryToUse = new AffiliateCampaignAffiliatePartnerEntityFactory();
					break;
				case Obymobi.Data.EntityType.AffiliatePartnerEntity:
					factoryToUse = new AffiliatePartnerEntityFactory();
					break;
				case Obymobi.Data.EntityType.AlterationEntity:
					factoryToUse = new AlterationEntityFactory();
					break;
				case Obymobi.Data.EntityType.AlterationitemEntity:
					factoryToUse = new AlterationitemEntityFactory();
					break;
				case Obymobi.Data.EntityType.AlterationitemAlterationEntity:
					factoryToUse = new AlterationitemAlterationEntityFactory();
					break;
				case Obymobi.Data.EntityType.AlterationLanguageEntity:
					factoryToUse = new AlterationLanguageEntityFactory();
					break;
				case Obymobi.Data.EntityType.AlterationoptionEntity:
					factoryToUse = new AlterationoptionEntityFactory();
					break;
				case Obymobi.Data.EntityType.AlterationoptionLanguageEntity:
					factoryToUse = new AlterationoptionLanguageEntityFactory();
					break;
				case Obymobi.Data.EntityType.AlterationoptionTagEntity:
					factoryToUse = new AlterationoptionTagEntityFactory();
					break;
				case Obymobi.Data.EntityType.AlterationProductEntity:
					factoryToUse = new AlterationProductEntityFactory();
					break;
				case Obymobi.Data.EntityType.AmenityEntity:
					factoryToUse = new AmenityEntityFactory();
					break;
				case Obymobi.Data.EntityType.AmenityLanguageEntity:
					factoryToUse = new AmenityLanguageEntityFactory();
					break;
				case Obymobi.Data.EntityType.AnalyticsProcessingTaskEntity:
					factoryToUse = new AnalyticsProcessingTaskEntityFactory();
					break;
				case Obymobi.Data.EntityType.AnnouncementEntity:
					factoryToUse = new AnnouncementEntityFactory();
					break;
				case Obymobi.Data.EntityType.AnnouncementLanguageEntity:
					factoryToUse = new AnnouncementLanguageEntityFactory();
					break;
				case Obymobi.Data.EntityType.ApiAuthenticationEntity:
					factoryToUse = new ApiAuthenticationEntityFactory();
					break;
				case Obymobi.Data.EntityType.ApplicationEntity:
					factoryToUse = new ApplicationEntityFactory();
					break;
				case Obymobi.Data.EntityType.AttachmentEntity:
					factoryToUse = new AttachmentEntityFactory();
					break;
				case Obymobi.Data.EntityType.AttachmentLanguageEntity:
					factoryToUse = new AttachmentLanguageEntityFactory();
					break;
				case Obymobi.Data.EntityType.AuditlogEntity:
					factoryToUse = new AuditlogEntityFactory();
					break;
				case Obymobi.Data.EntityType.AvailabilityEntity:
					factoryToUse = new AvailabilityEntityFactory();
					break;
				case Obymobi.Data.EntityType.AzureNotificationHubEntity:
					factoryToUse = new AzureNotificationHubEntityFactory();
					break;
				case Obymobi.Data.EntityType.BrandEntity:
					factoryToUse = new BrandEntityFactory();
					break;
				case Obymobi.Data.EntityType.BrandCultureEntity:
					factoryToUse = new BrandCultureEntityFactory();
					break;
				case Obymobi.Data.EntityType.BusinesshoursEntity:
					factoryToUse = new BusinesshoursEntityFactory();
					break;
				case Obymobi.Data.EntityType.BusinesshoursexceptionEntity:
					factoryToUse = new BusinesshoursexceptionEntityFactory();
					break;
				case Obymobi.Data.EntityType.CategoryEntity:
					factoryToUse = new CategoryEntityFactory();
					break;
				case Obymobi.Data.EntityType.CategoryAlterationEntity:
					factoryToUse = new CategoryAlterationEntityFactory();
					break;
				case Obymobi.Data.EntityType.CategoryLanguageEntity:
					factoryToUse = new CategoryLanguageEntityFactory();
					break;
				case Obymobi.Data.EntityType.CategorySuggestionEntity:
					factoryToUse = new CategorySuggestionEntityFactory();
					break;
				case Obymobi.Data.EntityType.CategoryTagEntity:
					factoryToUse = new CategoryTagEntityFactory();
					break;
				case Obymobi.Data.EntityType.CheckoutMethodEntity:
					factoryToUse = new CheckoutMethodEntityFactory();
					break;
				case Obymobi.Data.EntityType.CheckoutMethodDeliverypointgroupEntity:
					factoryToUse = new CheckoutMethodDeliverypointgroupEntityFactory();
					break;
				case Obymobi.Data.EntityType.ClientEntity:
					factoryToUse = new ClientEntityFactory();
					break;
				case Obymobi.Data.EntityType.ClientConfigurationEntity:
					factoryToUse = new ClientConfigurationEntityFactory();
					break;
				case Obymobi.Data.EntityType.ClientConfigurationRouteEntity:
					factoryToUse = new ClientConfigurationRouteEntityFactory();
					break;
				case Obymobi.Data.EntityType.ClientEntertainmentEntity:
					factoryToUse = new ClientEntertainmentEntityFactory();
					break;
				case Obymobi.Data.EntityType.ClientLogEntity:
					factoryToUse = new ClientLogEntityFactory();
					break;
				case Obymobi.Data.EntityType.ClientLogFileEntity:
					factoryToUse = new ClientLogFileEntityFactory();
					break;
				case Obymobi.Data.EntityType.ClientStateEntity:
					factoryToUse = new ClientStateEntityFactory();
					break;
				case Obymobi.Data.EntityType.CloudApplicationVersionEntity:
					factoryToUse = new CloudApplicationVersionEntityFactory();
					break;
				case Obymobi.Data.EntityType.CloudProcessingTaskEntity:
					factoryToUse = new CloudProcessingTaskEntityFactory();
					break;
				case Obymobi.Data.EntityType.CloudStorageAccountEntity:
					factoryToUse = new CloudStorageAccountEntityFactory();
					break;
				case Obymobi.Data.EntityType.CompanyEntity:
					factoryToUse = new CompanyEntityFactory();
					break;
				case Obymobi.Data.EntityType.CompanyAmenityEntity:
					factoryToUse = new CompanyAmenityEntityFactory();
					break;
				case Obymobi.Data.EntityType.CompanyBrandEntity:
					factoryToUse = new CompanyBrandEntityFactory();
					break;
				case Obymobi.Data.EntityType.CompanyCultureEntity:
					factoryToUse = new CompanyCultureEntityFactory();
					break;
				case Obymobi.Data.EntityType.CompanyCurrencyEntity:
					factoryToUse = new CompanyCurrencyEntityFactory();
					break;
				case Obymobi.Data.EntityType.CompanyEntertainmentEntity:
					factoryToUse = new CompanyEntertainmentEntityFactory();
					break;
				case Obymobi.Data.EntityType.CompanygroupEntity:
					factoryToUse = new CompanygroupEntityFactory();
					break;
				case Obymobi.Data.EntityType.CompanyLanguageEntity:
					factoryToUse = new CompanyLanguageEntityFactory();
					break;
				case Obymobi.Data.EntityType.CompanyManagementTaskEntity:
					factoryToUse = new CompanyManagementTaskEntityFactory();
					break;
				case Obymobi.Data.EntityType.CompanyOwnerEntity:
					factoryToUse = new CompanyOwnerEntityFactory();
					break;
				case Obymobi.Data.EntityType.CompanyReleaseEntity:
					factoryToUse = new CompanyReleaseEntityFactory();
					break;
				case Obymobi.Data.EntityType.CompanyVenueCategoryEntity:
					factoryToUse = new CompanyVenueCategoryEntityFactory();
					break;
				case Obymobi.Data.EntityType.ConfigurationEntity:
					factoryToUse = new ConfigurationEntityFactory();
					break;
				case Obymobi.Data.EntityType.CountryEntity:
					factoryToUse = new CountryEntityFactory();
					break;
				case Obymobi.Data.EntityType.CurrencyEntity:
					factoryToUse = new CurrencyEntityFactory();
					break;
				case Obymobi.Data.EntityType.CustomerEntity:
					factoryToUse = new CustomerEntityFactory();
					break;
				case Obymobi.Data.EntityType.CustomTextEntity:
					factoryToUse = new CustomTextEntityFactory();
					break;
				case Obymobi.Data.EntityType.DeliveryDistanceEntity:
					factoryToUse = new DeliveryDistanceEntityFactory();
					break;
				case Obymobi.Data.EntityType.DeliveryInformationEntity:
					factoryToUse = new DeliveryInformationEntityFactory();
					break;
				case Obymobi.Data.EntityType.DeliverypointEntity:
					factoryToUse = new DeliverypointEntityFactory();
					break;
				case Obymobi.Data.EntityType.DeliverypointExternalDeliverypointEntity:
					factoryToUse = new DeliverypointExternalDeliverypointEntityFactory();
					break;
				case Obymobi.Data.EntityType.DeliverypointgroupEntity:
					factoryToUse = new DeliverypointgroupEntityFactory();
					break;
				case Obymobi.Data.EntityType.DeliverypointgroupAdvertisementEntity:
					factoryToUse = new DeliverypointgroupAdvertisementEntityFactory();
					break;
				case Obymobi.Data.EntityType.DeliverypointgroupAnnouncementEntity:
					factoryToUse = new DeliverypointgroupAnnouncementEntityFactory();
					break;
				case Obymobi.Data.EntityType.DeliverypointgroupEntertainmentEntity:
					factoryToUse = new DeliverypointgroupEntertainmentEntityFactory();
					break;
				case Obymobi.Data.EntityType.DeliverypointgroupLanguageEntity:
					factoryToUse = new DeliverypointgroupLanguageEntityFactory();
					break;
				case Obymobi.Data.EntityType.DeliverypointgroupOccupancyEntity:
					factoryToUse = new DeliverypointgroupOccupancyEntityFactory();
					break;
				case Obymobi.Data.EntityType.DeliverypointgroupProductEntity:
					factoryToUse = new DeliverypointgroupProductEntityFactory();
					break;
				case Obymobi.Data.EntityType.DeviceEntity:
					factoryToUse = new DeviceEntityFactory();
					break;
				case Obymobi.Data.EntityType.DevicemediaEntity:
					factoryToUse = new DevicemediaEntityFactory();
					break;
				case Obymobi.Data.EntityType.DeviceTokenHistoryEntity:
					factoryToUse = new DeviceTokenHistoryEntityFactory();
					break;
				case Obymobi.Data.EntityType.DeviceTokenTaskEntity:
					factoryToUse = new DeviceTokenTaskEntityFactory();
					break;
				case Obymobi.Data.EntityType.EntertainmentEntity:
					factoryToUse = new EntertainmentEntityFactory();
					break;
				case Obymobi.Data.EntityType.EntertainmentcategoryEntity:
					factoryToUse = new EntertainmentcategoryEntityFactory();
					break;
				case Obymobi.Data.EntityType.EntertainmentcategoryLanguageEntity:
					factoryToUse = new EntertainmentcategoryLanguageEntityFactory();
					break;
				case Obymobi.Data.EntityType.EntertainmentConfigurationEntity:
					factoryToUse = new EntertainmentConfigurationEntityFactory();
					break;
				case Obymobi.Data.EntityType.EntertainmentConfigurationEntertainmentEntity:
					factoryToUse = new EntertainmentConfigurationEntertainmentEntityFactory();
					break;
				case Obymobi.Data.EntityType.EntertainmentDependencyEntity:
					factoryToUse = new EntertainmentDependencyEntityFactory();
					break;
				case Obymobi.Data.EntityType.EntertainmentFileEntity:
					factoryToUse = new EntertainmentFileEntityFactory();
					break;
				case Obymobi.Data.EntityType.EntertainmenturlEntity:
					factoryToUse = new EntertainmenturlEntityFactory();
					break;
				case Obymobi.Data.EntityType.EntityFieldInformationEntity:
					factoryToUse = new EntityFieldInformationEntityFactory();
					break;
				case Obymobi.Data.EntityType.EntityFieldInformationCustomEntity:
					factoryToUse = new EntityFieldInformationCustomEntityFactory();
					break;
				case Obymobi.Data.EntityType.EntityInformationEntity:
					factoryToUse = new EntityInformationEntityFactory();
					break;
				case Obymobi.Data.EntityType.EntityInformationCustomEntity:
					factoryToUse = new EntityInformationCustomEntityFactory();
					break;
				case Obymobi.Data.EntityType.ExternalDeliverypointEntity:
					factoryToUse = new ExternalDeliverypointEntityFactory();
					break;
				case Obymobi.Data.EntityType.ExternalMenuEntity:
					factoryToUse = new ExternalMenuEntityFactory();
					break;
				case Obymobi.Data.EntityType.ExternalProductEntity:
					factoryToUse = new ExternalProductEntityFactory();
					break;
				case Obymobi.Data.EntityType.ExternalSubProductEntity:
					factoryToUse = new ExternalSubProductEntityFactory();
					break;
				case Obymobi.Data.EntityType.ExternalSystemEntity:
					factoryToUse = new ExternalSystemEntityFactory();
					break;
				case Obymobi.Data.EntityType.ExternalSystemLogEntity:
					factoryToUse = new ExternalSystemLogEntityFactory();
					break;
				case Obymobi.Data.EntityType.FeatureToggleAvailabilityEntity:
					factoryToUse = new FeatureToggleAvailabilityEntityFactory();
					break;
				case Obymobi.Data.EntityType.FolioEntity:
					factoryToUse = new FolioEntityFactory();
					break;
				case Obymobi.Data.EntityType.FolioItemEntity:
					factoryToUse = new FolioItemEntityFactory();
					break;
				case Obymobi.Data.EntityType.GameEntity:
					factoryToUse = new GameEntityFactory();
					break;
				case Obymobi.Data.EntityType.GameSessionEntity:
					factoryToUse = new GameSessionEntityFactory();
					break;
				case Obymobi.Data.EntityType.GameSessionReportEntity:
					factoryToUse = new GameSessionReportEntityFactory();
					break;
				case Obymobi.Data.EntityType.GameSessionReportConfigurationEntity:
					factoryToUse = new GameSessionReportConfigurationEntityFactory();
					break;
				case Obymobi.Data.EntityType.GameSessionReportItemEntity:
					factoryToUse = new GameSessionReportItemEntityFactory();
					break;
				case Obymobi.Data.EntityType.GenericalterationEntity:
					factoryToUse = new GenericalterationEntityFactory();
					break;
				case Obymobi.Data.EntityType.GenericalterationitemEntity:
					factoryToUse = new GenericalterationitemEntityFactory();
					break;
				case Obymobi.Data.EntityType.GenericalterationoptionEntity:
					factoryToUse = new GenericalterationoptionEntityFactory();
					break;
				case Obymobi.Data.EntityType.GenericcategoryEntity:
					factoryToUse = new GenericcategoryEntityFactory();
					break;
				case Obymobi.Data.EntityType.GenericcategoryLanguageEntity:
					factoryToUse = new GenericcategoryLanguageEntityFactory();
					break;
				case Obymobi.Data.EntityType.GenericproductEntity:
					factoryToUse = new GenericproductEntityFactory();
					break;
				case Obymobi.Data.EntityType.GenericproductGenericalterationEntity:
					factoryToUse = new GenericproductGenericalterationEntityFactory();
					break;
				case Obymobi.Data.EntityType.GenericproductLanguageEntity:
					factoryToUse = new GenericproductLanguageEntityFactory();
					break;
				case Obymobi.Data.EntityType.GuestInformationEntity:
					factoryToUse = new GuestInformationEntityFactory();
					break;
				case Obymobi.Data.EntityType.IcrtouchprintermappingEntity:
					factoryToUse = new IcrtouchprintermappingEntityFactory();
					break;
				case Obymobi.Data.EntityType.IcrtouchprintermappingDeliverypointEntity:
					factoryToUse = new IcrtouchprintermappingDeliverypointEntityFactory();
					break;
				case Obymobi.Data.EntityType.IncomingSmsEntity:
					factoryToUse = new IncomingSmsEntityFactory();
					break;
				case Obymobi.Data.EntityType.InfraredCommandEntity:
					factoryToUse = new InfraredCommandEntityFactory();
					break;
				case Obymobi.Data.EntityType.InfraredConfigurationEntity:
					factoryToUse = new InfraredConfigurationEntityFactory();
					break;
				case Obymobi.Data.EntityType.LanguageEntity:
					factoryToUse = new LanguageEntityFactory();
					break;
				case Obymobi.Data.EntityType.LicensedModuleEntity:
					factoryToUse = new LicensedModuleEntityFactory();
					break;
				case Obymobi.Data.EntityType.LicensedUIElementEntity:
					factoryToUse = new LicensedUIElementEntityFactory();
					break;
				case Obymobi.Data.EntityType.LicensedUIElementSubPanelEntity:
					factoryToUse = new LicensedUIElementSubPanelEntityFactory();
					break;
				case Obymobi.Data.EntityType.MapEntity:
					factoryToUse = new MapEntityFactory();
					break;
				case Obymobi.Data.EntityType.MapPointOfInterestEntity:
					factoryToUse = new MapPointOfInterestEntityFactory();
					break;
				case Obymobi.Data.EntityType.MediaEntity:
					factoryToUse = new MediaEntityFactory();
					break;
				case Obymobi.Data.EntityType.MediaCultureEntity:
					factoryToUse = new MediaCultureEntityFactory();
					break;
				case Obymobi.Data.EntityType.MediaLanguageEntity:
					factoryToUse = new MediaLanguageEntityFactory();
					break;
				case Obymobi.Data.EntityType.MediaProcessingTaskEntity:
					factoryToUse = new MediaProcessingTaskEntityFactory();
					break;
				case Obymobi.Data.EntityType.MediaRatioTypeMediaEntity:
					factoryToUse = new MediaRatioTypeMediaEntityFactory();
					break;
				case Obymobi.Data.EntityType.MediaRatioTypeMediaFileEntity:
					factoryToUse = new MediaRatioTypeMediaFileEntityFactory();
					break;
				case Obymobi.Data.EntityType.MediaRelationshipEntity:
					factoryToUse = new MediaRelationshipEntityFactory();
					break;
				case Obymobi.Data.EntityType.MenuEntity:
					factoryToUse = new MenuEntityFactory();
					break;
				case Obymobi.Data.EntityType.MessageEntity:
					factoryToUse = new MessageEntityFactory();
					break;
				case Obymobi.Data.EntityType.MessagegroupEntity:
					factoryToUse = new MessagegroupEntityFactory();
					break;
				case Obymobi.Data.EntityType.MessagegroupDeliverypointEntity:
					factoryToUse = new MessagegroupDeliverypointEntityFactory();
					break;
				case Obymobi.Data.EntityType.MessageRecipientEntity:
					factoryToUse = new MessageRecipientEntityFactory();
					break;
				case Obymobi.Data.EntityType.MessageTemplateEntity:
					factoryToUse = new MessageTemplateEntityFactory();
					break;
				case Obymobi.Data.EntityType.MessageTemplateCategoryEntity:
					factoryToUse = new MessageTemplateCategoryEntityFactory();
					break;
				case Obymobi.Data.EntityType.MessageTemplateCategoryMessageTemplateEntity:
					factoryToUse = new MessageTemplateCategoryMessageTemplateEntityFactory();
					break;
				case Obymobi.Data.EntityType.ModuleEntity:
					factoryToUse = new ModuleEntityFactory();
					break;
				case Obymobi.Data.EntityType.NetmessageEntity:
					factoryToUse = new NetmessageEntityFactory();
					break;
				case Obymobi.Data.EntityType.OptInEntity:
					factoryToUse = new OptInEntityFactory();
					break;
				case Obymobi.Data.EntityType.OrderEntity:
					factoryToUse = new OrderEntityFactory();
					break;
				case Obymobi.Data.EntityType.OrderHourEntity:
					factoryToUse = new OrderHourEntityFactory();
					break;
				case Obymobi.Data.EntityType.OrderitemEntity:
					factoryToUse = new OrderitemEntityFactory();
					break;
				case Obymobi.Data.EntityType.OrderitemAlterationitemEntity:
					factoryToUse = new OrderitemAlterationitemEntityFactory();
					break;
				case Obymobi.Data.EntityType.OrderitemAlterationitemTagEntity:
					factoryToUse = new OrderitemAlterationitemTagEntityFactory();
					break;
				case Obymobi.Data.EntityType.OrderitemTagEntity:
					factoryToUse = new OrderitemTagEntityFactory();
					break;
				case Obymobi.Data.EntityType.OrderNotificationLogEntity:
					factoryToUse = new OrderNotificationLogEntityFactory();
					break;
				case Obymobi.Data.EntityType.OrderRoutestephandlerEntity:
					factoryToUse = new OrderRoutestephandlerEntityFactory();
					break;
				case Obymobi.Data.EntityType.OrderRoutestephandlerHistoryEntity:
					factoryToUse = new OrderRoutestephandlerHistoryEntityFactory();
					break;
				case Obymobi.Data.EntityType.OutletEntity:
					factoryToUse = new OutletEntityFactory();
					break;
				case Obymobi.Data.EntityType.OutletOperationalStateEntity:
					factoryToUse = new OutletOperationalStateEntityFactory();
					break;
				case Obymobi.Data.EntityType.OutletSellerInformationEntity:
					factoryToUse = new OutletSellerInformationEntityFactory();
					break;
				case Obymobi.Data.EntityType.PageEntity:
					factoryToUse = new PageEntityFactory();
					break;
				case Obymobi.Data.EntityType.PageElementEntity:
					factoryToUse = new PageElementEntityFactory();
					break;
				case Obymobi.Data.EntityType.PageLanguageEntity:
					factoryToUse = new PageLanguageEntityFactory();
					break;
				case Obymobi.Data.EntityType.PageTemplateEntity:
					factoryToUse = new PageTemplateEntityFactory();
					break;
				case Obymobi.Data.EntityType.PageTemplateElementEntity:
					factoryToUse = new PageTemplateElementEntityFactory();
					break;
				case Obymobi.Data.EntityType.PageTemplateLanguageEntity:
					factoryToUse = new PageTemplateLanguageEntityFactory();
					break;
				case Obymobi.Data.EntityType.PaymentIntegrationConfigurationEntity:
					factoryToUse = new PaymentIntegrationConfigurationEntityFactory();
					break;
				case Obymobi.Data.EntityType.PaymentProviderEntity:
					factoryToUse = new PaymentProviderEntityFactory();
					break;
				case Obymobi.Data.EntityType.PaymentProviderCompanyEntity:
					factoryToUse = new PaymentProviderCompanyEntityFactory();
					break;
				case Obymobi.Data.EntityType.PaymentTransactionEntity:
					factoryToUse = new PaymentTransactionEntityFactory();
					break;
				case Obymobi.Data.EntityType.PaymentTransactionLogEntity:
					factoryToUse = new PaymentTransactionLogEntityFactory();
					break;
				case Obymobi.Data.EntityType.PaymentTransactionSplitEntity:
					factoryToUse = new PaymentTransactionSplitEntityFactory();
					break;
				case Obymobi.Data.EntityType.PmsActionRuleEntity:
					factoryToUse = new PmsActionRuleEntityFactory();
					break;
				case Obymobi.Data.EntityType.PmsReportColumnEntity:
					factoryToUse = new PmsReportColumnEntityFactory();
					break;
				case Obymobi.Data.EntityType.PmsReportConfigurationEntity:
					factoryToUse = new PmsReportConfigurationEntityFactory();
					break;
				case Obymobi.Data.EntityType.PmsRuleEntity:
					factoryToUse = new PmsRuleEntityFactory();
					break;
				case Obymobi.Data.EntityType.PointOfInterestEntity:
					factoryToUse = new PointOfInterestEntityFactory();
					break;
				case Obymobi.Data.EntityType.PointOfInterestAmenityEntity:
					factoryToUse = new PointOfInterestAmenityEntityFactory();
					break;
				case Obymobi.Data.EntityType.PointOfInterestLanguageEntity:
					factoryToUse = new PointOfInterestLanguageEntityFactory();
					break;
				case Obymobi.Data.EntityType.PointOfInterestVenueCategoryEntity:
					factoryToUse = new PointOfInterestVenueCategoryEntityFactory();
					break;
				case Obymobi.Data.EntityType.PosalterationEntity:
					factoryToUse = new PosalterationEntityFactory();
					break;
				case Obymobi.Data.EntityType.PosalterationitemEntity:
					factoryToUse = new PosalterationitemEntityFactory();
					break;
				case Obymobi.Data.EntityType.PosalterationoptionEntity:
					factoryToUse = new PosalterationoptionEntityFactory();
					break;
				case Obymobi.Data.EntityType.PoscategoryEntity:
					factoryToUse = new PoscategoryEntityFactory();
					break;
				case Obymobi.Data.EntityType.PosdeliverypointEntity:
					factoryToUse = new PosdeliverypointEntityFactory();
					break;
				case Obymobi.Data.EntityType.PosdeliverypointgroupEntity:
					factoryToUse = new PosdeliverypointgroupEntityFactory();
					break;
				case Obymobi.Data.EntityType.PospaymentmethodEntity:
					factoryToUse = new PospaymentmethodEntityFactory();
					break;
				case Obymobi.Data.EntityType.PosproductEntity:
					factoryToUse = new PosproductEntityFactory();
					break;
				case Obymobi.Data.EntityType.PosproductPosalterationEntity:
					factoryToUse = new PosproductPosalterationEntityFactory();
					break;
				case Obymobi.Data.EntityType.PriceLevelEntity:
					factoryToUse = new PriceLevelEntityFactory();
					break;
				case Obymobi.Data.EntityType.PriceLevelItemEntity:
					factoryToUse = new PriceLevelItemEntityFactory();
					break;
				case Obymobi.Data.EntityType.PriceScheduleEntity:
					factoryToUse = new PriceScheduleEntityFactory();
					break;
				case Obymobi.Data.EntityType.PriceScheduleItemEntity:
					factoryToUse = new PriceScheduleItemEntityFactory();
					break;
				case Obymobi.Data.EntityType.PriceScheduleItemOccurrenceEntity:
					factoryToUse = new PriceScheduleItemOccurrenceEntityFactory();
					break;
				case Obymobi.Data.EntityType.ProductEntity:
					factoryToUse = new ProductEntityFactory();
					break;
				case Obymobi.Data.EntityType.ProductAlterationEntity:
					factoryToUse = new ProductAlterationEntityFactory();
					break;
				case Obymobi.Data.EntityType.ProductAttachmentEntity:
					factoryToUse = new ProductAttachmentEntityFactory();
					break;
				case Obymobi.Data.EntityType.ProductCategoryEntity:
					factoryToUse = new ProductCategoryEntityFactory();
					break;
				case Obymobi.Data.EntityType.ProductCategorySuggestionEntity:
					factoryToUse = new ProductCategorySuggestionEntityFactory();
					break;
				case Obymobi.Data.EntityType.ProductCategoryTagEntity:
					factoryToUse = new ProductCategoryTagEntityFactory();
					break;
				case Obymobi.Data.EntityType.ProductgroupEntity:
					factoryToUse = new ProductgroupEntityFactory();
					break;
				case Obymobi.Data.EntityType.ProductgroupItemEntity:
					factoryToUse = new ProductgroupItemEntityFactory();
					break;
				case Obymobi.Data.EntityType.ProductLanguageEntity:
					factoryToUse = new ProductLanguageEntityFactory();
					break;
				case Obymobi.Data.EntityType.ProductSuggestionEntity:
					factoryToUse = new ProductSuggestionEntityFactory();
					break;
				case Obymobi.Data.EntityType.ProductTagEntity:
					factoryToUse = new ProductTagEntityFactory();
					break;
				case Obymobi.Data.EntityType.PublishingEntity:
					factoryToUse = new PublishingEntityFactory();
					break;
				case Obymobi.Data.EntityType.PublishingItemEntity:
					factoryToUse = new PublishingItemEntityFactory();
					break;
				case Obymobi.Data.EntityType.RatingEntity:
					factoryToUse = new RatingEntityFactory();
					break;
				case Obymobi.Data.EntityType.ReceiptEntity:
					factoryToUse = new ReceiptEntityFactory();
					break;
				case Obymobi.Data.EntityType.ReceiptTemplateEntity:
					factoryToUse = new ReceiptTemplateEntityFactory();
					break;
				case Obymobi.Data.EntityType.ReferentialConstraintEntity:
					factoryToUse = new ReferentialConstraintEntityFactory();
					break;
				case Obymobi.Data.EntityType.ReferentialConstraintCustomEntity:
					factoryToUse = new ReferentialConstraintCustomEntityFactory();
					break;
				case Obymobi.Data.EntityType.ReleaseEntity:
					factoryToUse = new ReleaseEntityFactory();
					break;
				case Obymobi.Data.EntityType.ReportProcessingTaskEntity:
					factoryToUse = new ReportProcessingTaskEntityFactory();
					break;
				case Obymobi.Data.EntityType.ReportProcessingTaskFileEntity:
					factoryToUse = new ReportProcessingTaskFileEntityFactory();
					break;
				case Obymobi.Data.EntityType.ReportProcessingTaskTemplateEntity:
					factoryToUse = new ReportProcessingTaskTemplateEntityFactory();
					break;
				case Obymobi.Data.EntityType.RequestlogEntity:
					factoryToUse = new RequestlogEntityFactory();
					break;
				case Obymobi.Data.EntityType.RoleEntity:
					factoryToUse = new RoleEntityFactory();
					break;
				case Obymobi.Data.EntityType.RoleModuleRightsEntity:
					factoryToUse = new RoleModuleRightsEntityFactory();
					break;
				case Obymobi.Data.EntityType.RoleUIElementRightsEntity:
					factoryToUse = new RoleUIElementRightsEntityFactory();
					break;
				case Obymobi.Data.EntityType.RoleUIElementSubPanelRightsEntity:
					factoryToUse = new RoleUIElementSubPanelRightsEntityFactory();
					break;
				case Obymobi.Data.EntityType.RoomControlAreaEntity:
					factoryToUse = new RoomControlAreaEntityFactory();
					break;
				case Obymobi.Data.EntityType.RoomControlAreaLanguageEntity:
					factoryToUse = new RoomControlAreaLanguageEntityFactory();
					break;
				case Obymobi.Data.EntityType.RoomControlComponentEntity:
					factoryToUse = new RoomControlComponentEntityFactory();
					break;
				case Obymobi.Data.EntityType.RoomControlComponentLanguageEntity:
					factoryToUse = new RoomControlComponentLanguageEntityFactory();
					break;
				case Obymobi.Data.EntityType.RoomControlConfigurationEntity:
					factoryToUse = new RoomControlConfigurationEntityFactory();
					break;
				case Obymobi.Data.EntityType.RoomControlSectionEntity:
					factoryToUse = new RoomControlSectionEntityFactory();
					break;
				case Obymobi.Data.EntityType.RoomControlSectionItemEntity:
					factoryToUse = new RoomControlSectionItemEntityFactory();
					break;
				case Obymobi.Data.EntityType.RoomControlSectionItemLanguageEntity:
					factoryToUse = new RoomControlSectionItemLanguageEntityFactory();
					break;
				case Obymobi.Data.EntityType.RoomControlSectionLanguageEntity:
					factoryToUse = new RoomControlSectionLanguageEntityFactory();
					break;
				case Obymobi.Data.EntityType.RoomControlWidgetEntity:
					factoryToUse = new RoomControlWidgetEntityFactory();
					break;
				case Obymobi.Data.EntityType.RoomControlWidgetLanguageEntity:
					factoryToUse = new RoomControlWidgetLanguageEntityFactory();
					break;
				case Obymobi.Data.EntityType.RouteEntity:
					factoryToUse = new RouteEntityFactory();
					break;
				case Obymobi.Data.EntityType.RoutestepEntity:
					factoryToUse = new RoutestepEntityFactory();
					break;
				case Obymobi.Data.EntityType.RoutestephandlerEntity:
					factoryToUse = new RoutestephandlerEntityFactory();
					break;
				case Obymobi.Data.EntityType.ScheduleEntity:
					factoryToUse = new ScheduleEntityFactory();
					break;
				case Obymobi.Data.EntityType.ScheduledCommandEntity:
					factoryToUse = new ScheduledCommandEntityFactory();
					break;
				case Obymobi.Data.EntityType.ScheduledCommandTaskEntity:
					factoryToUse = new ScheduledCommandTaskEntityFactory();
					break;
				case Obymobi.Data.EntityType.ScheduledMessageEntity:
					factoryToUse = new ScheduledMessageEntityFactory();
					break;
				case Obymobi.Data.EntityType.ScheduledMessageHistoryEntity:
					factoryToUse = new ScheduledMessageHistoryEntityFactory();
					break;
				case Obymobi.Data.EntityType.ScheduledMessageLanguageEntity:
					factoryToUse = new ScheduledMessageLanguageEntityFactory();
					break;
				case Obymobi.Data.EntityType.ScheduleitemEntity:
					factoryToUse = new ScheduleitemEntityFactory();
					break;
				case Obymobi.Data.EntityType.ServiceMethodEntity:
					factoryToUse = new ServiceMethodEntityFactory();
					break;
				case Obymobi.Data.EntityType.ServiceMethodDeliverypointgroupEntity:
					factoryToUse = new ServiceMethodDeliverypointgroupEntityFactory();
					break;
				case Obymobi.Data.EntityType.SetupCodeEntity:
					factoryToUse = new SetupCodeEntityFactory();
					break;
				case Obymobi.Data.EntityType.SiteEntity:
					factoryToUse = new SiteEntityFactory();
					break;
				case Obymobi.Data.EntityType.SiteCultureEntity:
					factoryToUse = new SiteCultureEntityFactory();
					break;
				case Obymobi.Data.EntityType.SiteLanguageEntity:
					factoryToUse = new SiteLanguageEntityFactory();
					break;
				case Obymobi.Data.EntityType.SiteTemplateEntity:
					factoryToUse = new SiteTemplateEntityFactory();
					break;
				case Obymobi.Data.EntityType.SiteTemplateCultureEntity:
					factoryToUse = new SiteTemplateCultureEntityFactory();
					break;
				case Obymobi.Data.EntityType.SiteTemplateLanguageEntity:
					factoryToUse = new SiteTemplateLanguageEntityFactory();
					break;
				case Obymobi.Data.EntityType.SmsInformationEntity:
					factoryToUse = new SmsInformationEntityFactory();
					break;
				case Obymobi.Data.EntityType.SmsKeywordEntity:
					factoryToUse = new SmsKeywordEntityFactory();
					break;
				case Obymobi.Data.EntityType.StationEntity:
					factoryToUse = new StationEntityFactory();
					break;
				case Obymobi.Data.EntityType.StationLanguageEntity:
					factoryToUse = new StationLanguageEntityFactory();
					break;
				case Obymobi.Data.EntityType.StationListEntity:
					factoryToUse = new StationListEntityFactory();
					break;
				case Obymobi.Data.EntityType.SupplierEntity:
					factoryToUse = new SupplierEntityFactory();
					break;
				case Obymobi.Data.EntityType.SupportagentEntity:
					factoryToUse = new SupportagentEntityFactory();
					break;
				case Obymobi.Data.EntityType.SupportpoolEntity:
					factoryToUse = new SupportpoolEntityFactory();
					break;
				case Obymobi.Data.EntityType.SupportpoolNotificationRecipientEntity:
					factoryToUse = new SupportpoolNotificationRecipientEntityFactory();
					break;
				case Obymobi.Data.EntityType.SupportpoolSupportagentEntity:
					factoryToUse = new SupportpoolSupportagentEntityFactory();
					break;
				case Obymobi.Data.EntityType.SurveyEntity:
					factoryToUse = new SurveyEntityFactory();
					break;
				case Obymobi.Data.EntityType.SurveyAnswerEntity:
					factoryToUse = new SurveyAnswerEntityFactory();
					break;
				case Obymobi.Data.EntityType.SurveyAnswerLanguageEntity:
					factoryToUse = new SurveyAnswerLanguageEntityFactory();
					break;
				case Obymobi.Data.EntityType.SurveyLanguageEntity:
					factoryToUse = new SurveyLanguageEntityFactory();
					break;
				case Obymobi.Data.EntityType.SurveyPageEntity:
					factoryToUse = new SurveyPageEntityFactory();
					break;
				case Obymobi.Data.EntityType.SurveyQuestionEntity:
					factoryToUse = new SurveyQuestionEntityFactory();
					break;
				case Obymobi.Data.EntityType.SurveyQuestionLanguageEntity:
					factoryToUse = new SurveyQuestionLanguageEntityFactory();
					break;
				case Obymobi.Data.EntityType.SurveyResultEntity:
					factoryToUse = new SurveyResultEntityFactory();
					break;
				case Obymobi.Data.EntityType.TagEntity:
					factoryToUse = new TagEntityFactory();
					break;
				case Obymobi.Data.EntityType.TaxTariffEntity:
					factoryToUse = new TaxTariffEntityFactory();
					break;
				case Obymobi.Data.EntityType.TerminalEntity:
					factoryToUse = new TerminalEntityFactory();
					break;
				case Obymobi.Data.EntityType.TerminalConfigurationEntity:
					factoryToUse = new TerminalConfigurationEntityFactory();
					break;
				case Obymobi.Data.EntityType.TerminalLogEntity:
					factoryToUse = new TerminalLogEntityFactory();
					break;
				case Obymobi.Data.EntityType.TerminalLogFileEntity:
					factoryToUse = new TerminalLogFileEntityFactory();
					break;
				case Obymobi.Data.EntityType.TerminalMessageTemplateCategoryEntity:
					factoryToUse = new TerminalMessageTemplateCategoryEntityFactory();
					break;
				case Obymobi.Data.EntityType.TerminalStateEntity:
					factoryToUse = new TerminalStateEntityFactory();
					break;
				case Obymobi.Data.EntityType.TimestampEntity:
					factoryToUse = new TimestampEntityFactory();
					break;
				case Obymobi.Data.EntityType.TimeZoneEntity:
					factoryToUse = new TimeZoneEntityFactory();
					break;
				case Obymobi.Data.EntityType.TranslationEntity:
					factoryToUse = new TranslationEntityFactory();
					break;
				case Obymobi.Data.EntityType.UIElementEntity:
					factoryToUse = new UIElementEntityFactory();
					break;
				case Obymobi.Data.EntityType.UIElementCustomEntity:
					factoryToUse = new UIElementCustomEntityFactory();
					break;
				case Obymobi.Data.EntityType.UIElementSubPanelEntity:
					factoryToUse = new UIElementSubPanelEntityFactory();
					break;
				case Obymobi.Data.EntityType.UIElementSubPanelCustomEntity:
					factoryToUse = new UIElementSubPanelCustomEntityFactory();
					break;
				case Obymobi.Data.EntityType.UIElementSubPanelUIElementEntity:
					factoryToUse = new UIElementSubPanelUIElementEntityFactory();
					break;
				case Obymobi.Data.EntityType.UIFooterItemEntity:
					factoryToUse = new UIFooterItemEntityFactory();
					break;
				case Obymobi.Data.EntityType.UIFooterItemLanguageEntity:
					factoryToUse = new UIFooterItemLanguageEntityFactory();
					break;
				case Obymobi.Data.EntityType.UIModeEntity:
					factoryToUse = new UIModeEntityFactory();
					break;
				case Obymobi.Data.EntityType.UIScheduleEntity:
					factoryToUse = new UIScheduleEntityFactory();
					break;
				case Obymobi.Data.EntityType.UIScheduleItemEntity:
					factoryToUse = new UIScheduleItemEntityFactory();
					break;
				case Obymobi.Data.EntityType.UIScheduleItemOccurrenceEntity:
					factoryToUse = new UIScheduleItemOccurrenceEntityFactory();
					break;
				case Obymobi.Data.EntityType.UITabEntity:
					factoryToUse = new UITabEntityFactory();
					break;
				case Obymobi.Data.EntityType.UITabLanguageEntity:
					factoryToUse = new UITabLanguageEntityFactory();
					break;
				case Obymobi.Data.EntityType.UIThemeEntity:
					factoryToUse = new UIThemeEntityFactory();
					break;
				case Obymobi.Data.EntityType.UIThemeColorEntity:
					factoryToUse = new UIThemeColorEntityFactory();
					break;
				case Obymobi.Data.EntityType.UIThemeTextSizeEntity:
					factoryToUse = new UIThemeTextSizeEntityFactory();
					break;
				case Obymobi.Data.EntityType.UIWidgetEntity:
					factoryToUse = new UIWidgetEntityFactory();
					break;
				case Obymobi.Data.EntityType.UIWidgetAvailabilityEntity:
					factoryToUse = new UIWidgetAvailabilityEntityFactory();
					break;
				case Obymobi.Data.EntityType.UIWidgetLanguageEntity:
					factoryToUse = new UIWidgetLanguageEntityFactory();
					break;
				case Obymobi.Data.EntityType.UIWidgetTimerEntity:
					factoryToUse = new UIWidgetTimerEntityFactory();
					break;
				case Obymobi.Data.EntityType.UserEntity:
					factoryToUse = new UserEntityFactory();
					break;
				case Obymobi.Data.EntityType.UserBrandEntity:
					factoryToUse = new UserBrandEntityFactory();
					break;
				case Obymobi.Data.EntityType.UserLogonEntity:
					factoryToUse = new UserLogonEntityFactory();
					break;
				case Obymobi.Data.EntityType.UserRoleEntity:
					factoryToUse = new UserRoleEntityFactory();
					break;
				case Obymobi.Data.EntityType.VattariffEntity:
					factoryToUse = new VattariffEntityFactory();
					break;
				case Obymobi.Data.EntityType.VendorEntity:
					factoryToUse = new VendorEntityFactory();
					break;
				case Obymobi.Data.EntityType.VenueCategoryEntity:
					factoryToUse = new VenueCategoryEntityFactory();
					break;
				case Obymobi.Data.EntityType.VenueCategoryLanguageEntity:
					factoryToUse = new VenueCategoryLanguageEntityFactory();
					break;
				case Obymobi.Data.EntityType.ViewEntity:
					factoryToUse = new ViewEntityFactory();
					break;
				case Obymobi.Data.EntityType.ViewCustomEntity:
					factoryToUse = new ViewCustomEntityFactory();
					break;
				case Obymobi.Data.EntityType.ViewItemEntity:
					factoryToUse = new ViewItemEntityFactory();
					break;
				case Obymobi.Data.EntityType.ViewItemCustomEntity:
					factoryToUse = new ViewItemCustomEntityFactory();
					break;
				case Obymobi.Data.EntityType.WeatherEntity:
					factoryToUse = new WeatherEntityFactory();
					break;
				case Obymobi.Data.EntityType.WifiConfigurationEntity:
					factoryToUse = new WifiConfigurationEntityFactory();
					break;
				case Obymobi.Data.EntityType.ActionEntity:
					factoryToUse = new ActionEntityFactory();
					break;
				case Obymobi.Data.EntityType.ApplicationConfigurationEntity:
					factoryToUse = new ApplicationConfigurationEntityFactory();
					break;
				case Obymobi.Data.EntityType.CarouselItemEntity:
					factoryToUse = new CarouselItemEntityFactory();
					break;
				case Obymobi.Data.EntityType.FeatureFlagEntity:
					factoryToUse = new FeatureFlagEntityFactory();
					break;
				case Obymobi.Data.EntityType.LandingPageEntity:
					factoryToUse = new LandingPageEntityFactory();
					break;
				case Obymobi.Data.EntityType.LandingPageWidgetEntity:
					factoryToUse = new LandingPageWidgetEntityFactory();
					break;
				case Obymobi.Data.EntityType.NavigationMenuEntity:
					factoryToUse = new NavigationMenuEntityFactory();
					break;
				case Obymobi.Data.EntityType.NavigationMenuItemEntity:
					factoryToUse = new NavigationMenuItemEntityFactory();
					break;
				case Obymobi.Data.EntityType.NavigationMenuWidgetEntity:
					factoryToUse = new NavigationMenuWidgetEntityFactory();
					break;
				case Obymobi.Data.EntityType.ThemeEntity:
					factoryToUse = new ThemeEntityFactory();
					break;
				case Obymobi.Data.EntityType.WidgetEntity:
					factoryToUse = new WidgetEntityFactory();
					break;
				case Obymobi.Data.EntityType.WidgetActionBannerEntity:
					factoryToUse = new WidgetActionBannerEntityFactory();
					break;
				case Obymobi.Data.EntityType.WidgetActionButtonEntity:
					factoryToUse = new WidgetActionButtonEntityFactory();
					break;
				case Obymobi.Data.EntityType.WidgetCarouselEntity:
					factoryToUse = new WidgetCarouselEntityFactory();
					break;
				case Obymobi.Data.EntityType.WidgetGroupEntity:
					factoryToUse = new WidgetGroupEntityFactory();
					break;
				case Obymobi.Data.EntityType.WidgetGroupWidgetEntity:
					factoryToUse = new WidgetGroupWidgetEntityFactory();
					break;
				case Obymobi.Data.EntityType.WidgetHeroEntity:
					factoryToUse = new WidgetHeroEntityFactory();
					break;
				case Obymobi.Data.EntityType.WidgetLanguageSwitcherEntity:
					factoryToUse = new WidgetLanguageSwitcherEntityFactory();
					break;
				case Obymobi.Data.EntityType.WidgetMarkdownEntity:
					factoryToUse = new WidgetMarkdownEntityFactory();
					break;
				case Obymobi.Data.EntityType.WidgetOpeningTimeEntity:
					factoryToUse = new WidgetOpeningTimeEntityFactory();
					break;
				case Obymobi.Data.EntityType.WidgetPageTitleEntity:
					factoryToUse = new WidgetPageTitleEntityFactory();
					break;
				case Obymobi.Data.EntityType.WidgetWaitTimeEntity:
					factoryToUse = new WidgetWaitTimeEntityFactory();
					break;
			}
			IEntity toReturn = null;
			if(factoryToUse != null)
			{
				toReturn = factoryToUse.Create();
			}
			return toReturn;
		}		
	}
	
	/// <summary>Class which is used to obtain the entity factory based on the .NET type of the entity. </summary>
	[Serializable]
	public static class EntityFactoryFactory
	{
		private static readonly Dictionary<Type, IEntityFactory> _factoryPerType = new Dictionary<Type, IEntityFactory>();

		/// <summary>Initializes the <see cref="EntityFactoryFactory"/> class.</summary>
		static EntityFactoryFactory()
		{
			Array entityTypeValues = Enum.GetValues(typeof(Obymobi.Data.EntityType));
			foreach(int entityTypeValue in entityTypeValues)
			{
				IEntity dummy = GeneralEntityFactory.Create((Obymobi.Data.EntityType)entityTypeValue);
				_factoryPerType.Add(dummy.GetType(), dummy.GetEntityFactory());
			}
		}

		/// <summary>Gets the factory of the entity with the .NET type specified</summary>
		/// <param name="typeOfEntity">The type of entity.</param>
		/// <returns>factory to use or null if not found</returns>
		public static IEntityFactory GetFactory(Type typeOfEntity)
		{
			IEntityFactory toReturn = null;
			_factoryPerType.TryGetValue(typeOfEntity, out toReturn);
			return toReturn;
		}

		/// <summary>Gets the factory of the entity with the Obymobi.Data.EntityType specified</summary>
		/// <param name="typeOfEntity">The type of entity.</param>
		/// <returns>factory to use or null if not found</returns>
		public static IEntityFactory GetFactory(Obymobi.Data.EntityType typeOfEntity)
		{
			return GetFactory(GeneralEntityFactory.Create(typeOfEntity).GetType());
		}
	}
	
	/// <summary>Element creator for creating project elements from somewhere else, like inside Linq providers.</summary>
	public class ElementCreator : ElementCreatorBase, IElementCreator
	{
		/// <summary>Gets the factory of the Entity type with the Obymobi.Data.EntityType value passed in</summary>
		/// <param name="entityTypeValue">The entity type value.</param>
		/// <returns>the entity factory of the entity type or null if not found</returns>
		public IEntityFactory GetFactory(int entityTypeValue)
		{
			return (IEntityFactory)this.GetFactoryImpl(entityTypeValue);
		}

		/// <summary>Gets the factory of the Entity type with the .NET type passed in</summary>
		/// <param name="typeOfEntity">The type of entity.</param>
		/// <returns>the entity factory of the entity type or null if not found</returns>
		public IEntityFactory GetFactory(Type typeOfEntity)
		{
			return (IEntityFactory)this.GetFactoryImpl(typeOfEntity);
		}

		/// <summary>Creates a new resultset fields object with the number of field slots reserved as specified</summary>
		/// <param name="numberOfFields">The number of fields.</param>
		/// <returns>ready to use resultsetfields object</returns>
		public IEntityFields CreateResultsetFields(int numberOfFields)
		{
			return new ResultsetFields(numberOfFields);
		}
		
		/// <summary>Gets an instance of the TypedListDAO class to execute dynamic lists and projections.</summary>
		/// <returns>ready to use typedlistDAO</returns>
		public IDao GetTypedListDao()
		{
			return new TypedListDAO();
		}
		
		/// <summary>Obtains the inheritance info provider instance from the singleton </summary>
		/// <returns>The singleton instance of the inheritance info provider</returns>
		public override IInheritanceInfoProvider ObtainInheritanceInfoProviderInstance()
		{
			return InheritanceInfoProviderSingleton.GetInstance();
		}


		/// <summary>Creates a new dynamic relation instance</summary>
		/// <param name="leftOperand">The left operand.</param>
		/// <returns>ready to use dynamic relation</returns>
		public override IDynamicRelation CreateDynamicRelation(DerivedTableDefinition leftOperand)
		{
			return new DynamicRelation(leftOperand);
		}

		/// <summary>Creates a new dynamic relation instance</summary>
		/// <param name="leftOperand">The left operand.</param>
		/// <param name="joinType">Type of the join. If None is specified, Inner is assumed.</param>
		/// <param name="rightOperand">The right operand.</param>
		/// <param name="onClause">The on clause for the join.</param>
		/// <returns>ready to use dynamic relation</returns>
		public override IDynamicRelation CreateDynamicRelation(DerivedTableDefinition leftOperand, JoinHint joinType, DerivedTableDefinition rightOperand, IPredicate onClause)
		{
			return new DynamicRelation(leftOperand, joinType, rightOperand, onClause);
		}

		/// <summary>Creates a new dynamic relation instance</summary>
		/// <param name="leftOperand">The left operand.</param>
		/// <param name="joinType">Type of the join. If None is specified, Inner is assumed.</param>
		/// <param name="rightOperand">The right operand.</param>
		/// <param name="aliasLeftOperand">The alias of the left operand. If you don't want to / need to alias the right operand (only alias if you have to), specify string.Empty.</param>
		/// <param name="onClause">The on clause for the join.</param>
		/// <returns>ready to use dynamic relation</returns>
		public override IDynamicRelation CreateDynamicRelation(IEntityFieldCore leftOperand, JoinHint joinType, DerivedTableDefinition rightOperand, string aliasLeftOperand, IPredicate onClause)
		{
			return new DynamicRelation(leftOperand, joinType, rightOperand, aliasLeftOperand, onClause);
		}

		/// <summary>Creates a new dynamic relation instance</summary>
		/// <param name="leftOperand">The left operand.</param>
		/// <param name="joinType">Type of the join. If None is specified, Inner is assumed.</param>
		/// <param name="rightOperandEntityName">Name of the entity, which is used as the right operand.</param>
		/// <param name="aliasRightOperand">The alias of the right operand. If you don't want to / need to alias the right operand (only alias if you have to), specify string.Empty.</param>
		/// <param name="onClause">The on clause for the join.</param>
		/// <returns>ready to use dynamic relation</returns>
		public override IDynamicRelation CreateDynamicRelation(DerivedTableDefinition leftOperand, JoinHint joinType, string rightOperandEntityName, string aliasRightOperand, IPredicate onClause)
		{
			return new DynamicRelation(leftOperand, joinType, (Obymobi.Data.EntityType)Enum.Parse(typeof(Obymobi.Data.EntityType), rightOperandEntityName, false), aliasRightOperand, onClause);
		}

		/// <summary>Creates a new dynamic relation instance</summary>
		/// <param name="leftOperandEntityName">Name of the entity which is used as the left operand.</param>
		/// <param name="joinType">Type of the join. If None is specified, Inner is assumed.</param>
		/// <param name="rightOperandEntityName">Name of the entity, which is used as the right operand.</param>
		/// <param name="aliasLeftOperand">The alias of the left operand. If you don't want to / need to alias the right operand (only alias if you have to), specify string.Empty.</param>
		/// <param name="aliasRightOperand">The alias of the right operand. If you don't want to / need to alias the right operand (only alias if you have to), specify string.Empty.</param>
		/// <param name="onClause">The on clause for the join.</param>
		/// <returns>ready to use dynamic relation</returns>
		public override IDynamicRelation CreateDynamicRelation(string leftOperandEntityName, JoinHint joinType, string rightOperandEntityName, string aliasLeftOperand, string aliasRightOperand, IPredicate onClause)
		{
			return new DynamicRelation((Obymobi.Data.EntityType)Enum.Parse(typeof(Obymobi.Data.EntityType), leftOperandEntityName, false), joinType, (Obymobi.Data.EntityType)Enum.Parse(typeof(Obymobi.Data.EntityType), rightOperandEntityName, false), aliasLeftOperand, aliasRightOperand, onClause);
		}
		
		/// <summary>Creates a new dynamic relation instance</summary>
		/// <param name="leftOperand">The left operand.</param>
		/// <param name="joinType">Type of the join. If None is specified, Inner is assumed.</param>
		/// <param name="rightOperandEntityName">Name of the entity, which is used as the right operand.</param>
		/// <param name="aliasLeftOperand">The alias of the left operand. If you don't want to / need to alias the right operand (only alias if you have to), specify string.Empty.</param>
		/// <param name="aliasRightOperand">The alias of the right operand. If you don't want to / need to alias the right operand (only alias if you have to), specify string.Empty.</param>
		/// <param name="onClause">The on clause for the join.</param>
		/// <returns>ready to use dynamic relation</returns>
		public override IDynamicRelation CreateDynamicRelation(IEntityFieldCore leftOperand, JoinHint joinType, string rightOperandEntityName, string aliasLeftOperand, string aliasRightOperand, IPredicate onClause)
		{
			return new DynamicRelation(leftOperand, joinType, (Obymobi.Data.EntityType)Enum.Parse(typeof(Obymobi.Data.EntityType), rightOperandEntityName, false), aliasLeftOperand, aliasRightOperand, onClause);
		}
		
		/// <summary>Implementation of the routine which gets the factory of the Entity type with the Obymobi.Data.EntityType value passed in</summary>
		/// <param name="entityTypeValue">The entity type value.</param>
		/// <returns>the entity factory of the entity type or null if not found</returns>
		protected override IEntityFactoryCore GetFactoryImpl(int entityTypeValue)
		{
			return EntityFactoryFactory.GetFactory((Obymobi.Data.EntityType)entityTypeValue);
		}
	
		/// <summary>Implementation of the routine which gets the factory of the Entity type with the .NET type passed in</summary>
		/// <param name="typeOfEntity">The type of entity.</param>
		/// <returns>the entity factory of the entity type or null if not found</returns>
		protected override IEntityFactoryCore GetFactoryImpl(Type typeOfEntity)
		{
			return EntityFactoryFactory.GetFactory(typeOfEntity);
		}

	}
}
