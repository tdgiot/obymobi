﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;

using Obymobi.Data.DaoClasses;
using Obymobi.Data.HelperClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.FactoryClasses
{
	/// <summary>
	/// Generic factory for DAO objects. 
	/// </summary>
	public partial class DAOFactory
	{
		/// <summary>
		/// Private CTor, no instantiation possible.
		/// </summary>
		private DAOFactory()
		{
		}

		/// <summary>Creates a new AccessCodeDAO object</summary>
		/// <returns>the new DAO object ready to use for AccessCode Entities</returns>
		public static AccessCodeDAO CreateAccessCodeDAO()
		{
			return new AccessCodeDAO();
		}

		/// <summary>Creates a new AccessCodeCompanyDAO object</summary>
		/// <returns>the new DAO object ready to use for AccessCodeCompany Entities</returns>
		public static AccessCodeCompanyDAO CreateAccessCodeCompanyDAO()
		{
			return new AccessCodeCompanyDAO();
		}

		/// <summary>Creates a new AccessCodePointOfInterestDAO object</summary>
		/// <returns>the new DAO object ready to use for AccessCodePointOfInterest Entities</returns>
		public static AccessCodePointOfInterestDAO CreateAccessCodePointOfInterestDAO()
		{
			return new AccessCodePointOfInterestDAO();
		}

		/// <summary>Creates a new AccountDAO object</summary>
		/// <returns>the new DAO object ready to use for Account Entities</returns>
		public static AccountDAO CreateAccountDAO()
		{
			return new AccountDAO();
		}

		/// <summary>Creates a new AccountCompanyDAO object</summary>
		/// <returns>the new DAO object ready to use for AccountCompany Entities</returns>
		public static AccountCompanyDAO CreateAccountCompanyDAO()
		{
			return new AccountCompanyDAO();
		}

		/// <summary>Creates a new ActionButtonDAO object</summary>
		/// <returns>the new DAO object ready to use for ActionButton Entities</returns>
		public static ActionButtonDAO CreateActionButtonDAO()
		{
			return new ActionButtonDAO();
		}

		/// <summary>Creates a new ActionButtonLanguageDAO object</summary>
		/// <returns>the new DAO object ready to use for ActionButtonLanguage Entities</returns>
		public static ActionButtonLanguageDAO CreateActionButtonLanguageDAO()
		{
			return new ActionButtonLanguageDAO();
		}

		/// <summary>Creates a new AddressDAO object</summary>
		/// <returns>the new DAO object ready to use for Address Entities</returns>
		public static AddressDAO CreateAddressDAO()
		{
			return new AddressDAO();
		}

		/// <summary>Creates a new AdvertisementDAO object</summary>
		/// <returns>the new DAO object ready to use for Advertisement Entities</returns>
		public static AdvertisementDAO CreateAdvertisementDAO()
		{
			return new AdvertisementDAO();
		}

		/// <summary>Creates a new AdvertisementConfigurationDAO object</summary>
		/// <returns>the new DAO object ready to use for AdvertisementConfiguration Entities</returns>
		public static AdvertisementConfigurationDAO CreateAdvertisementConfigurationDAO()
		{
			return new AdvertisementConfigurationDAO();
		}

		/// <summary>Creates a new AdvertisementConfigurationAdvertisementDAO object</summary>
		/// <returns>the new DAO object ready to use for AdvertisementConfigurationAdvertisement Entities</returns>
		public static AdvertisementConfigurationAdvertisementDAO CreateAdvertisementConfigurationAdvertisementDAO()
		{
			return new AdvertisementConfigurationAdvertisementDAO();
		}

		/// <summary>Creates a new AdvertisementLanguageDAO object</summary>
		/// <returns>the new DAO object ready to use for AdvertisementLanguage Entities</returns>
		public static AdvertisementLanguageDAO CreateAdvertisementLanguageDAO()
		{
			return new AdvertisementLanguageDAO();
		}

		/// <summary>Creates a new AdvertisementTagDAO object</summary>
		/// <returns>the new DAO object ready to use for AdvertisementTag Entities</returns>
		public static AdvertisementTagDAO CreateAdvertisementTagDAO()
		{
			return new AdvertisementTagDAO();
		}

		/// <summary>Creates a new AdvertisementTagAdvertisementDAO object</summary>
		/// <returns>the new DAO object ready to use for AdvertisementTagAdvertisement Entities</returns>
		public static AdvertisementTagAdvertisementDAO CreateAdvertisementTagAdvertisementDAO()
		{
			return new AdvertisementTagAdvertisementDAO();
		}

		/// <summary>Creates a new AdvertisementTagCategoryDAO object</summary>
		/// <returns>the new DAO object ready to use for AdvertisementTagCategory Entities</returns>
		public static AdvertisementTagCategoryDAO CreateAdvertisementTagCategoryDAO()
		{
			return new AdvertisementTagCategoryDAO();
		}

		/// <summary>Creates a new AdvertisementTagEntertainmentDAO object</summary>
		/// <returns>the new DAO object ready to use for AdvertisementTagEntertainment Entities</returns>
		public static AdvertisementTagEntertainmentDAO CreateAdvertisementTagEntertainmentDAO()
		{
			return new AdvertisementTagEntertainmentDAO();
		}

		/// <summary>Creates a new AdvertisementTagGenericproductDAO object</summary>
		/// <returns>the new DAO object ready to use for AdvertisementTagGenericproduct Entities</returns>
		public static AdvertisementTagGenericproductDAO CreateAdvertisementTagGenericproductDAO()
		{
			return new AdvertisementTagGenericproductDAO();
		}

		/// <summary>Creates a new AdvertisementTagLanguageDAO object</summary>
		/// <returns>the new DAO object ready to use for AdvertisementTagLanguage Entities</returns>
		public static AdvertisementTagLanguageDAO CreateAdvertisementTagLanguageDAO()
		{
			return new AdvertisementTagLanguageDAO();
		}

		/// <summary>Creates a new AdvertisementTagProductDAO object</summary>
		/// <returns>the new DAO object ready to use for AdvertisementTagProduct Entities</returns>
		public static AdvertisementTagProductDAO CreateAdvertisementTagProductDAO()
		{
			return new AdvertisementTagProductDAO();
		}

		/// <summary>Creates a new AdyenPaymentMethodDAO object</summary>
		/// <returns>the new DAO object ready to use for AdyenPaymentMethod Entities</returns>
		public static AdyenPaymentMethodDAO CreateAdyenPaymentMethodDAO()
		{
			return new AdyenPaymentMethodDAO();
		}

		/// <summary>Creates a new AdyenPaymentMethodBrandDAO object</summary>
		/// <returns>the new DAO object ready to use for AdyenPaymentMethodBrand Entities</returns>
		public static AdyenPaymentMethodBrandDAO CreateAdyenPaymentMethodBrandDAO()
		{
			return new AdyenPaymentMethodBrandDAO();
		}

		/// <summary>Creates a new AffiliateCampaignDAO object</summary>
		/// <returns>the new DAO object ready to use for AffiliateCampaign Entities</returns>
		public static AffiliateCampaignDAO CreateAffiliateCampaignDAO()
		{
			return new AffiliateCampaignDAO();
		}

		/// <summary>Creates a new AffiliateCampaignAffiliatePartnerDAO object</summary>
		/// <returns>the new DAO object ready to use for AffiliateCampaignAffiliatePartner Entities</returns>
		public static AffiliateCampaignAffiliatePartnerDAO CreateAffiliateCampaignAffiliatePartnerDAO()
		{
			return new AffiliateCampaignAffiliatePartnerDAO();
		}

		/// <summary>Creates a new AffiliatePartnerDAO object</summary>
		/// <returns>the new DAO object ready to use for AffiliatePartner Entities</returns>
		public static AffiliatePartnerDAO CreateAffiliatePartnerDAO()
		{
			return new AffiliatePartnerDAO();
		}

		/// <summary>Creates a new AlterationDAO object</summary>
		/// <returns>the new DAO object ready to use for Alteration Entities</returns>
		public static AlterationDAO CreateAlterationDAO()
		{
			return new AlterationDAO();
		}

		/// <summary>Creates a new AlterationitemDAO object</summary>
		/// <returns>the new DAO object ready to use for Alterationitem Entities</returns>
		public static AlterationitemDAO CreateAlterationitemDAO()
		{
			return new AlterationitemDAO();
		}

		/// <summary>Creates a new AlterationitemAlterationDAO object</summary>
		/// <returns>the new DAO object ready to use for AlterationitemAlteration Entities</returns>
		public static AlterationitemAlterationDAO CreateAlterationitemAlterationDAO()
		{
			return new AlterationitemAlterationDAO();
		}

		/// <summary>Creates a new AlterationLanguageDAO object</summary>
		/// <returns>the new DAO object ready to use for AlterationLanguage Entities</returns>
		public static AlterationLanguageDAO CreateAlterationLanguageDAO()
		{
			return new AlterationLanguageDAO();
		}

		/// <summary>Creates a new AlterationoptionDAO object</summary>
		/// <returns>the new DAO object ready to use for Alterationoption Entities</returns>
		public static AlterationoptionDAO CreateAlterationoptionDAO()
		{
			return new AlterationoptionDAO();
		}

		/// <summary>Creates a new AlterationoptionLanguageDAO object</summary>
		/// <returns>the new DAO object ready to use for AlterationoptionLanguage Entities</returns>
		public static AlterationoptionLanguageDAO CreateAlterationoptionLanguageDAO()
		{
			return new AlterationoptionLanguageDAO();
		}

		/// <summary>Creates a new AlterationoptionTagDAO object</summary>
		/// <returns>the new DAO object ready to use for AlterationoptionTag Entities</returns>
		public static AlterationoptionTagDAO CreateAlterationoptionTagDAO()
		{
			return new AlterationoptionTagDAO();
		}

		/// <summary>Creates a new AlterationProductDAO object</summary>
		/// <returns>the new DAO object ready to use for AlterationProduct Entities</returns>
		public static AlterationProductDAO CreateAlterationProductDAO()
		{
			return new AlterationProductDAO();
		}

		/// <summary>Creates a new AmenityDAO object</summary>
		/// <returns>the new DAO object ready to use for Amenity Entities</returns>
		public static AmenityDAO CreateAmenityDAO()
		{
			return new AmenityDAO();
		}

		/// <summary>Creates a new AmenityLanguageDAO object</summary>
		/// <returns>the new DAO object ready to use for AmenityLanguage Entities</returns>
		public static AmenityLanguageDAO CreateAmenityLanguageDAO()
		{
			return new AmenityLanguageDAO();
		}

		/// <summary>Creates a new AnalyticsProcessingTaskDAO object</summary>
		/// <returns>the new DAO object ready to use for AnalyticsProcessingTask Entities</returns>
		public static AnalyticsProcessingTaskDAO CreateAnalyticsProcessingTaskDAO()
		{
			return new AnalyticsProcessingTaskDAO();
		}

		/// <summary>Creates a new AnnouncementDAO object</summary>
		/// <returns>the new DAO object ready to use for Announcement Entities</returns>
		public static AnnouncementDAO CreateAnnouncementDAO()
		{
			return new AnnouncementDAO();
		}

		/// <summary>Creates a new AnnouncementLanguageDAO object</summary>
		/// <returns>the new DAO object ready to use for AnnouncementLanguage Entities</returns>
		public static AnnouncementLanguageDAO CreateAnnouncementLanguageDAO()
		{
			return new AnnouncementLanguageDAO();
		}

		/// <summary>Creates a new ApiAuthenticationDAO object</summary>
		/// <returns>the new DAO object ready to use for ApiAuthentication Entities</returns>
		public static ApiAuthenticationDAO CreateApiAuthenticationDAO()
		{
			return new ApiAuthenticationDAO();
		}

		/// <summary>Creates a new ApplicationDAO object</summary>
		/// <returns>the new DAO object ready to use for Application Entities</returns>
		public static ApplicationDAO CreateApplicationDAO()
		{
			return new ApplicationDAO();
		}

		/// <summary>Creates a new AttachmentDAO object</summary>
		/// <returns>the new DAO object ready to use for Attachment Entities</returns>
		public static AttachmentDAO CreateAttachmentDAO()
		{
			return new AttachmentDAO();
		}

		/// <summary>Creates a new AttachmentLanguageDAO object</summary>
		/// <returns>the new DAO object ready to use for AttachmentLanguage Entities</returns>
		public static AttachmentLanguageDAO CreateAttachmentLanguageDAO()
		{
			return new AttachmentLanguageDAO();
		}

		/// <summary>Creates a new AuditlogDAO object</summary>
		/// <returns>the new DAO object ready to use for Auditlog Entities</returns>
		public static AuditlogDAO CreateAuditlogDAO()
		{
			return new AuditlogDAO();
		}

		/// <summary>Creates a new AvailabilityDAO object</summary>
		/// <returns>the new DAO object ready to use for Availability Entities</returns>
		public static AvailabilityDAO CreateAvailabilityDAO()
		{
			return new AvailabilityDAO();
		}

		/// <summary>Creates a new AzureNotificationHubDAO object</summary>
		/// <returns>the new DAO object ready to use for AzureNotificationHub Entities</returns>
		public static AzureNotificationHubDAO CreateAzureNotificationHubDAO()
		{
			return new AzureNotificationHubDAO();
		}

		/// <summary>Creates a new BrandDAO object</summary>
		/// <returns>the new DAO object ready to use for Brand Entities</returns>
		public static BrandDAO CreateBrandDAO()
		{
			return new BrandDAO();
		}

		/// <summary>Creates a new BrandCultureDAO object</summary>
		/// <returns>the new DAO object ready to use for BrandCulture Entities</returns>
		public static BrandCultureDAO CreateBrandCultureDAO()
		{
			return new BrandCultureDAO();
		}

		/// <summary>Creates a new BusinesshoursDAO object</summary>
		/// <returns>the new DAO object ready to use for Businesshours Entities</returns>
		public static BusinesshoursDAO CreateBusinesshoursDAO()
		{
			return new BusinesshoursDAO();
		}

		/// <summary>Creates a new BusinesshoursexceptionDAO object</summary>
		/// <returns>the new DAO object ready to use for Businesshoursexception Entities</returns>
		public static BusinesshoursexceptionDAO CreateBusinesshoursexceptionDAO()
		{
			return new BusinesshoursexceptionDAO();
		}

		/// <summary>Creates a new CategoryDAO object</summary>
		/// <returns>the new DAO object ready to use for Category Entities</returns>
		public static CategoryDAO CreateCategoryDAO()
		{
			return new CategoryDAO();
		}

		/// <summary>Creates a new CategoryAlterationDAO object</summary>
		/// <returns>the new DAO object ready to use for CategoryAlteration Entities</returns>
		public static CategoryAlterationDAO CreateCategoryAlterationDAO()
		{
			return new CategoryAlterationDAO();
		}

		/// <summary>Creates a new CategoryLanguageDAO object</summary>
		/// <returns>the new DAO object ready to use for CategoryLanguage Entities</returns>
		public static CategoryLanguageDAO CreateCategoryLanguageDAO()
		{
			return new CategoryLanguageDAO();
		}

		/// <summary>Creates a new CategorySuggestionDAO object</summary>
		/// <returns>the new DAO object ready to use for CategorySuggestion Entities</returns>
		public static CategorySuggestionDAO CreateCategorySuggestionDAO()
		{
			return new CategorySuggestionDAO();
		}

		/// <summary>Creates a new CategoryTagDAO object</summary>
		/// <returns>the new DAO object ready to use for CategoryTag Entities</returns>
		public static CategoryTagDAO CreateCategoryTagDAO()
		{
			return new CategoryTagDAO();
		}

		/// <summary>Creates a new CheckoutMethodDAO object</summary>
		/// <returns>the new DAO object ready to use for CheckoutMethod Entities</returns>
		public static CheckoutMethodDAO CreateCheckoutMethodDAO()
		{
			return new CheckoutMethodDAO();
		}

		/// <summary>Creates a new CheckoutMethodDeliverypointgroupDAO object</summary>
		/// <returns>the new DAO object ready to use for CheckoutMethodDeliverypointgroup Entities</returns>
		public static CheckoutMethodDeliverypointgroupDAO CreateCheckoutMethodDeliverypointgroupDAO()
		{
			return new CheckoutMethodDeliverypointgroupDAO();
		}

		/// <summary>Creates a new ClientDAO object</summary>
		/// <returns>the new DAO object ready to use for Client Entities</returns>
		public static ClientDAO CreateClientDAO()
		{
			return new ClientDAO();
		}

		/// <summary>Creates a new ClientConfigurationDAO object</summary>
		/// <returns>the new DAO object ready to use for ClientConfiguration Entities</returns>
		public static ClientConfigurationDAO CreateClientConfigurationDAO()
		{
			return new ClientConfigurationDAO();
		}

		/// <summary>Creates a new ClientConfigurationRouteDAO object</summary>
		/// <returns>the new DAO object ready to use for ClientConfigurationRoute Entities</returns>
		public static ClientConfigurationRouteDAO CreateClientConfigurationRouteDAO()
		{
			return new ClientConfigurationRouteDAO();
		}

		/// <summary>Creates a new ClientEntertainmentDAO object</summary>
		/// <returns>the new DAO object ready to use for ClientEntertainment Entities</returns>
		public static ClientEntertainmentDAO CreateClientEntertainmentDAO()
		{
			return new ClientEntertainmentDAO();
		}

		/// <summary>Creates a new ClientLogDAO object</summary>
		/// <returns>the new DAO object ready to use for ClientLog Entities</returns>
		public static ClientLogDAO CreateClientLogDAO()
		{
			return new ClientLogDAO();
		}

		/// <summary>Creates a new ClientLogFileDAO object</summary>
		/// <returns>the new DAO object ready to use for ClientLogFile Entities</returns>
		public static ClientLogFileDAO CreateClientLogFileDAO()
		{
			return new ClientLogFileDAO();
		}

		/// <summary>Creates a new ClientStateDAO object</summary>
		/// <returns>the new DAO object ready to use for ClientState Entities</returns>
		public static ClientStateDAO CreateClientStateDAO()
		{
			return new ClientStateDAO();
		}

		/// <summary>Creates a new CloudApplicationVersionDAO object</summary>
		/// <returns>the new DAO object ready to use for CloudApplicationVersion Entities</returns>
		public static CloudApplicationVersionDAO CreateCloudApplicationVersionDAO()
		{
			return new CloudApplicationVersionDAO();
		}

		/// <summary>Creates a new CloudProcessingTaskDAO object</summary>
		/// <returns>the new DAO object ready to use for CloudProcessingTask Entities</returns>
		public static CloudProcessingTaskDAO CreateCloudProcessingTaskDAO()
		{
			return new CloudProcessingTaskDAO();
		}

		/// <summary>Creates a new CloudStorageAccountDAO object</summary>
		/// <returns>the new DAO object ready to use for CloudStorageAccount Entities</returns>
		public static CloudStorageAccountDAO CreateCloudStorageAccountDAO()
		{
			return new CloudStorageAccountDAO();
		}

		/// <summary>Creates a new CompanyDAO object</summary>
		/// <returns>the new DAO object ready to use for Company Entities</returns>
		public static CompanyDAO CreateCompanyDAO()
		{
			return new CompanyDAO();
		}

		/// <summary>Creates a new CompanyAmenityDAO object</summary>
		/// <returns>the new DAO object ready to use for CompanyAmenity Entities</returns>
		public static CompanyAmenityDAO CreateCompanyAmenityDAO()
		{
			return new CompanyAmenityDAO();
		}

		/// <summary>Creates a new CompanyBrandDAO object</summary>
		/// <returns>the new DAO object ready to use for CompanyBrand Entities</returns>
		public static CompanyBrandDAO CreateCompanyBrandDAO()
		{
			return new CompanyBrandDAO();
		}

		/// <summary>Creates a new CompanyCultureDAO object</summary>
		/// <returns>the new DAO object ready to use for CompanyCulture Entities</returns>
		public static CompanyCultureDAO CreateCompanyCultureDAO()
		{
			return new CompanyCultureDAO();
		}

		/// <summary>Creates a new CompanyCurrencyDAO object</summary>
		/// <returns>the new DAO object ready to use for CompanyCurrency Entities</returns>
		public static CompanyCurrencyDAO CreateCompanyCurrencyDAO()
		{
			return new CompanyCurrencyDAO();
		}

		/// <summary>Creates a new CompanyEntertainmentDAO object</summary>
		/// <returns>the new DAO object ready to use for CompanyEntertainment Entities</returns>
		public static CompanyEntertainmentDAO CreateCompanyEntertainmentDAO()
		{
			return new CompanyEntertainmentDAO();
		}

		/// <summary>Creates a new CompanygroupDAO object</summary>
		/// <returns>the new DAO object ready to use for Companygroup Entities</returns>
		public static CompanygroupDAO CreateCompanygroupDAO()
		{
			return new CompanygroupDAO();
		}

		/// <summary>Creates a new CompanyLanguageDAO object</summary>
		/// <returns>the new DAO object ready to use for CompanyLanguage Entities</returns>
		public static CompanyLanguageDAO CreateCompanyLanguageDAO()
		{
			return new CompanyLanguageDAO();
		}

		/// <summary>Creates a new CompanyManagementTaskDAO object</summary>
		/// <returns>the new DAO object ready to use for CompanyManagementTask Entities</returns>
		public static CompanyManagementTaskDAO CreateCompanyManagementTaskDAO()
		{
			return new CompanyManagementTaskDAO();
		}

		/// <summary>Creates a new CompanyOwnerDAO object</summary>
		/// <returns>the new DAO object ready to use for CompanyOwner Entities</returns>
		public static CompanyOwnerDAO CreateCompanyOwnerDAO()
		{
			return new CompanyOwnerDAO();
		}

		/// <summary>Creates a new CompanyReleaseDAO object</summary>
		/// <returns>the new DAO object ready to use for CompanyRelease Entities</returns>
		public static CompanyReleaseDAO CreateCompanyReleaseDAO()
		{
			return new CompanyReleaseDAO();
		}

		/// <summary>Creates a new CompanyVenueCategoryDAO object</summary>
		/// <returns>the new DAO object ready to use for CompanyVenueCategory Entities</returns>
		public static CompanyVenueCategoryDAO CreateCompanyVenueCategoryDAO()
		{
			return new CompanyVenueCategoryDAO();
		}

		/// <summary>Creates a new ConfigurationDAO object</summary>
		/// <returns>the new DAO object ready to use for Configuration Entities</returns>
		public static ConfigurationDAO CreateConfigurationDAO()
		{
			return new ConfigurationDAO();
		}

		/// <summary>Creates a new CountryDAO object</summary>
		/// <returns>the new DAO object ready to use for Country Entities</returns>
		public static CountryDAO CreateCountryDAO()
		{
			return new CountryDAO();
		}

		/// <summary>Creates a new CurrencyDAO object</summary>
		/// <returns>the new DAO object ready to use for Currency Entities</returns>
		public static CurrencyDAO CreateCurrencyDAO()
		{
			return new CurrencyDAO();
		}

		/// <summary>Creates a new CustomerDAO object</summary>
		/// <returns>the new DAO object ready to use for Customer Entities</returns>
		public static CustomerDAO CreateCustomerDAO()
		{
			return new CustomerDAO();
		}

		/// <summary>Creates a new CustomTextDAO object</summary>
		/// <returns>the new DAO object ready to use for CustomText Entities</returns>
		public static CustomTextDAO CreateCustomTextDAO()
		{
			return new CustomTextDAO();
		}

		/// <summary>Creates a new DeliveryDistanceDAO object</summary>
		/// <returns>the new DAO object ready to use for DeliveryDistance Entities</returns>
		public static DeliveryDistanceDAO CreateDeliveryDistanceDAO()
		{
			return new DeliveryDistanceDAO();
		}

		/// <summary>Creates a new DeliveryInformationDAO object</summary>
		/// <returns>the new DAO object ready to use for DeliveryInformation Entities</returns>
		public static DeliveryInformationDAO CreateDeliveryInformationDAO()
		{
			return new DeliveryInformationDAO();
		}

		/// <summary>Creates a new DeliverypointDAO object</summary>
		/// <returns>the new DAO object ready to use for Deliverypoint Entities</returns>
		public static DeliverypointDAO CreateDeliverypointDAO()
		{
			return new DeliverypointDAO();
		}

		/// <summary>Creates a new DeliverypointExternalDeliverypointDAO object</summary>
		/// <returns>the new DAO object ready to use for DeliverypointExternalDeliverypoint Entities</returns>
		public static DeliverypointExternalDeliverypointDAO CreateDeliverypointExternalDeliverypointDAO()
		{
			return new DeliverypointExternalDeliverypointDAO();
		}

		/// <summary>Creates a new DeliverypointgroupDAO object</summary>
		/// <returns>the new DAO object ready to use for Deliverypointgroup Entities</returns>
		public static DeliverypointgroupDAO CreateDeliverypointgroupDAO()
		{
			return new DeliverypointgroupDAO();
		}

		/// <summary>Creates a new DeliverypointgroupAdvertisementDAO object</summary>
		/// <returns>the new DAO object ready to use for DeliverypointgroupAdvertisement Entities</returns>
		public static DeliverypointgroupAdvertisementDAO CreateDeliverypointgroupAdvertisementDAO()
		{
			return new DeliverypointgroupAdvertisementDAO();
		}

		/// <summary>Creates a new DeliverypointgroupAnnouncementDAO object</summary>
		/// <returns>the new DAO object ready to use for DeliverypointgroupAnnouncement Entities</returns>
		public static DeliverypointgroupAnnouncementDAO CreateDeliverypointgroupAnnouncementDAO()
		{
			return new DeliverypointgroupAnnouncementDAO();
		}

		/// <summary>Creates a new DeliverypointgroupEntertainmentDAO object</summary>
		/// <returns>the new DAO object ready to use for DeliverypointgroupEntertainment Entities</returns>
		public static DeliverypointgroupEntertainmentDAO CreateDeliverypointgroupEntertainmentDAO()
		{
			return new DeliverypointgroupEntertainmentDAO();
		}

		/// <summary>Creates a new DeliverypointgroupLanguageDAO object</summary>
		/// <returns>the new DAO object ready to use for DeliverypointgroupLanguage Entities</returns>
		public static DeliverypointgroupLanguageDAO CreateDeliverypointgroupLanguageDAO()
		{
			return new DeliverypointgroupLanguageDAO();
		}

		/// <summary>Creates a new DeliverypointgroupOccupancyDAO object</summary>
		/// <returns>the new DAO object ready to use for DeliverypointgroupOccupancy Entities</returns>
		public static DeliverypointgroupOccupancyDAO CreateDeliverypointgroupOccupancyDAO()
		{
			return new DeliverypointgroupOccupancyDAO();
		}

		/// <summary>Creates a new DeliverypointgroupProductDAO object</summary>
		/// <returns>the new DAO object ready to use for DeliverypointgroupProduct Entities</returns>
		public static DeliverypointgroupProductDAO CreateDeliverypointgroupProductDAO()
		{
			return new DeliverypointgroupProductDAO();
		}

		/// <summary>Creates a new DeviceDAO object</summary>
		/// <returns>the new DAO object ready to use for Device Entities</returns>
		public static DeviceDAO CreateDeviceDAO()
		{
			return new DeviceDAO();
		}

		/// <summary>Creates a new DevicemediaDAO object</summary>
		/// <returns>the new DAO object ready to use for Devicemedia Entities</returns>
		public static DevicemediaDAO CreateDevicemediaDAO()
		{
			return new DevicemediaDAO();
		}

		/// <summary>Creates a new DeviceTokenHistoryDAO object</summary>
		/// <returns>the new DAO object ready to use for DeviceTokenHistory Entities</returns>
		public static DeviceTokenHistoryDAO CreateDeviceTokenHistoryDAO()
		{
			return new DeviceTokenHistoryDAO();
		}

		/// <summary>Creates a new DeviceTokenTaskDAO object</summary>
		/// <returns>the new DAO object ready to use for DeviceTokenTask Entities</returns>
		public static DeviceTokenTaskDAO CreateDeviceTokenTaskDAO()
		{
			return new DeviceTokenTaskDAO();
		}

		/// <summary>Creates a new EntertainmentDAO object</summary>
		/// <returns>the new DAO object ready to use for Entertainment Entities</returns>
		public static EntertainmentDAO CreateEntertainmentDAO()
		{
			return new EntertainmentDAO();
		}

		/// <summary>Creates a new EntertainmentcategoryDAO object</summary>
		/// <returns>the new DAO object ready to use for Entertainmentcategory Entities</returns>
		public static EntertainmentcategoryDAO CreateEntertainmentcategoryDAO()
		{
			return new EntertainmentcategoryDAO();
		}

		/// <summary>Creates a new EntertainmentcategoryLanguageDAO object</summary>
		/// <returns>the new DAO object ready to use for EntertainmentcategoryLanguage Entities</returns>
		public static EntertainmentcategoryLanguageDAO CreateEntertainmentcategoryLanguageDAO()
		{
			return new EntertainmentcategoryLanguageDAO();
		}

		/// <summary>Creates a new EntertainmentConfigurationDAO object</summary>
		/// <returns>the new DAO object ready to use for EntertainmentConfiguration Entities</returns>
		public static EntertainmentConfigurationDAO CreateEntertainmentConfigurationDAO()
		{
			return new EntertainmentConfigurationDAO();
		}

		/// <summary>Creates a new EntertainmentConfigurationEntertainmentDAO object</summary>
		/// <returns>the new DAO object ready to use for EntertainmentConfigurationEntertainment Entities</returns>
		public static EntertainmentConfigurationEntertainmentDAO CreateEntertainmentConfigurationEntertainmentDAO()
		{
			return new EntertainmentConfigurationEntertainmentDAO();
		}

		/// <summary>Creates a new EntertainmentDependencyDAO object</summary>
		/// <returns>the new DAO object ready to use for EntertainmentDependency Entities</returns>
		public static EntertainmentDependencyDAO CreateEntertainmentDependencyDAO()
		{
			return new EntertainmentDependencyDAO();
		}

		/// <summary>Creates a new EntertainmentFileDAO object</summary>
		/// <returns>the new DAO object ready to use for EntertainmentFile Entities</returns>
		public static EntertainmentFileDAO CreateEntertainmentFileDAO()
		{
			return new EntertainmentFileDAO();
		}

		/// <summary>Creates a new EntertainmenturlDAO object</summary>
		/// <returns>the new DAO object ready to use for Entertainmenturl Entities</returns>
		public static EntertainmenturlDAO CreateEntertainmenturlDAO()
		{
			return new EntertainmenturlDAO();
		}

		/// <summary>Creates a new EntityFieldInformationDAO object</summary>
		/// <returns>the new DAO object ready to use for EntityFieldInformation Entities</returns>
		public static EntityFieldInformationDAO CreateEntityFieldInformationDAO()
		{
			return new EntityFieldInformationDAO();
		}

		/// <summary>Creates a new EntityFieldInformationCustomDAO object</summary>
		/// <returns>the new DAO object ready to use for EntityFieldInformationCustom Entities</returns>
		public static EntityFieldInformationCustomDAO CreateEntityFieldInformationCustomDAO()
		{
			return new EntityFieldInformationCustomDAO();
		}

		/// <summary>Creates a new EntityInformationDAO object</summary>
		/// <returns>the new DAO object ready to use for EntityInformation Entities</returns>
		public static EntityInformationDAO CreateEntityInformationDAO()
		{
			return new EntityInformationDAO();
		}

		/// <summary>Creates a new EntityInformationCustomDAO object</summary>
		/// <returns>the new DAO object ready to use for EntityInformationCustom Entities</returns>
		public static EntityInformationCustomDAO CreateEntityInformationCustomDAO()
		{
			return new EntityInformationCustomDAO();
		}

		/// <summary>Creates a new ExternalDeliverypointDAO object</summary>
		/// <returns>the new DAO object ready to use for ExternalDeliverypoint Entities</returns>
		public static ExternalDeliverypointDAO CreateExternalDeliverypointDAO()
		{
			return new ExternalDeliverypointDAO();
		}

		/// <summary>Creates a new ExternalMenuDAO object</summary>
		/// <returns>the new DAO object ready to use for ExternalMenu Entities</returns>
		public static ExternalMenuDAO CreateExternalMenuDAO()
		{
			return new ExternalMenuDAO();
		}

		/// <summary>Creates a new ExternalProductDAO object</summary>
		/// <returns>the new DAO object ready to use for ExternalProduct Entities</returns>
		public static ExternalProductDAO CreateExternalProductDAO()
		{
			return new ExternalProductDAO();
		}

		/// <summary>Creates a new ExternalSubProductDAO object</summary>
		/// <returns>the new DAO object ready to use for ExternalSubProduct Entities</returns>
		public static ExternalSubProductDAO CreateExternalSubProductDAO()
		{
			return new ExternalSubProductDAO();
		}

		/// <summary>Creates a new ExternalSystemDAO object</summary>
		/// <returns>the new DAO object ready to use for ExternalSystem Entities</returns>
		public static ExternalSystemDAO CreateExternalSystemDAO()
		{
			return new ExternalSystemDAO();
		}

		/// <summary>Creates a new ExternalSystemLogDAO object</summary>
		/// <returns>the new DAO object ready to use for ExternalSystemLog Entities</returns>
		public static ExternalSystemLogDAO CreateExternalSystemLogDAO()
		{
			return new ExternalSystemLogDAO();
		}

		/// <summary>Creates a new FeatureToggleAvailabilityDAO object</summary>
		/// <returns>the new DAO object ready to use for FeatureToggleAvailability Entities</returns>
		public static FeatureToggleAvailabilityDAO CreateFeatureToggleAvailabilityDAO()
		{
			return new FeatureToggleAvailabilityDAO();
		}

		/// <summary>Creates a new FolioDAO object</summary>
		/// <returns>the new DAO object ready to use for Folio Entities</returns>
		public static FolioDAO CreateFolioDAO()
		{
			return new FolioDAO();
		}

		/// <summary>Creates a new FolioItemDAO object</summary>
		/// <returns>the new DAO object ready to use for FolioItem Entities</returns>
		public static FolioItemDAO CreateFolioItemDAO()
		{
			return new FolioItemDAO();
		}

		/// <summary>Creates a new GameDAO object</summary>
		/// <returns>the new DAO object ready to use for Game Entities</returns>
		public static GameDAO CreateGameDAO()
		{
			return new GameDAO();
		}

		/// <summary>Creates a new GameSessionDAO object</summary>
		/// <returns>the new DAO object ready to use for GameSession Entities</returns>
		public static GameSessionDAO CreateGameSessionDAO()
		{
			return new GameSessionDAO();
		}

		/// <summary>Creates a new GameSessionReportDAO object</summary>
		/// <returns>the new DAO object ready to use for GameSessionReport Entities</returns>
		public static GameSessionReportDAO CreateGameSessionReportDAO()
		{
			return new GameSessionReportDAO();
		}

		/// <summary>Creates a new GameSessionReportConfigurationDAO object</summary>
		/// <returns>the new DAO object ready to use for GameSessionReportConfiguration Entities</returns>
		public static GameSessionReportConfigurationDAO CreateGameSessionReportConfigurationDAO()
		{
			return new GameSessionReportConfigurationDAO();
		}

		/// <summary>Creates a new GameSessionReportItemDAO object</summary>
		/// <returns>the new DAO object ready to use for GameSessionReportItem Entities</returns>
		public static GameSessionReportItemDAO CreateGameSessionReportItemDAO()
		{
			return new GameSessionReportItemDAO();
		}

		/// <summary>Creates a new GenericalterationDAO object</summary>
		/// <returns>the new DAO object ready to use for Genericalteration Entities</returns>
		public static GenericalterationDAO CreateGenericalterationDAO()
		{
			return new GenericalterationDAO();
		}

		/// <summary>Creates a new GenericalterationitemDAO object</summary>
		/// <returns>the new DAO object ready to use for Genericalterationitem Entities</returns>
		public static GenericalterationitemDAO CreateGenericalterationitemDAO()
		{
			return new GenericalterationitemDAO();
		}

		/// <summary>Creates a new GenericalterationoptionDAO object</summary>
		/// <returns>the new DAO object ready to use for Genericalterationoption Entities</returns>
		public static GenericalterationoptionDAO CreateGenericalterationoptionDAO()
		{
			return new GenericalterationoptionDAO();
		}

		/// <summary>Creates a new GenericcategoryDAO object</summary>
		/// <returns>the new DAO object ready to use for Genericcategory Entities</returns>
		public static GenericcategoryDAO CreateGenericcategoryDAO()
		{
			return new GenericcategoryDAO();
		}

		/// <summary>Creates a new GenericcategoryLanguageDAO object</summary>
		/// <returns>the new DAO object ready to use for GenericcategoryLanguage Entities</returns>
		public static GenericcategoryLanguageDAO CreateGenericcategoryLanguageDAO()
		{
			return new GenericcategoryLanguageDAO();
		}

		/// <summary>Creates a new GenericproductDAO object</summary>
		/// <returns>the new DAO object ready to use for Genericproduct Entities</returns>
		public static GenericproductDAO CreateGenericproductDAO()
		{
			return new GenericproductDAO();
		}

		/// <summary>Creates a new GenericproductGenericalterationDAO object</summary>
		/// <returns>the new DAO object ready to use for GenericproductGenericalteration Entities</returns>
		public static GenericproductGenericalterationDAO CreateGenericproductGenericalterationDAO()
		{
			return new GenericproductGenericalterationDAO();
		}

		/// <summary>Creates a new GenericproductLanguageDAO object</summary>
		/// <returns>the new DAO object ready to use for GenericproductLanguage Entities</returns>
		public static GenericproductLanguageDAO CreateGenericproductLanguageDAO()
		{
			return new GenericproductLanguageDAO();
		}

		/// <summary>Creates a new GuestInformationDAO object</summary>
		/// <returns>the new DAO object ready to use for GuestInformation Entities</returns>
		public static GuestInformationDAO CreateGuestInformationDAO()
		{
			return new GuestInformationDAO();
		}

		/// <summary>Creates a new IcrtouchprintermappingDAO object</summary>
		/// <returns>the new DAO object ready to use for Icrtouchprintermapping Entities</returns>
		public static IcrtouchprintermappingDAO CreateIcrtouchprintermappingDAO()
		{
			return new IcrtouchprintermappingDAO();
		}

		/// <summary>Creates a new IcrtouchprintermappingDeliverypointDAO object</summary>
		/// <returns>the new DAO object ready to use for IcrtouchprintermappingDeliverypoint Entities</returns>
		public static IcrtouchprintermappingDeliverypointDAO CreateIcrtouchprintermappingDeliverypointDAO()
		{
			return new IcrtouchprintermappingDeliverypointDAO();
		}

		/// <summary>Creates a new IncomingSmsDAO object</summary>
		/// <returns>the new DAO object ready to use for IncomingSms Entities</returns>
		public static IncomingSmsDAO CreateIncomingSmsDAO()
		{
			return new IncomingSmsDAO();
		}

		/// <summary>Creates a new InfraredCommandDAO object</summary>
		/// <returns>the new DAO object ready to use for InfraredCommand Entities</returns>
		public static InfraredCommandDAO CreateInfraredCommandDAO()
		{
			return new InfraredCommandDAO();
		}

		/// <summary>Creates a new InfraredConfigurationDAO object</summary>
		/// <returns>the new DAO object ready to use for InfraredConfiguration Entities</returns>
		public static InfraredConfigurationDAO CreateInfraredConfigurationDAO()
		{
			return new InfraredConfigurationDAO();
		}

		/// <summary>Creates a new LanguageDAO object</summary>
		/// <returns>the new DAO object ready to use for Language Entities</returns>
		public static LanguageDAO CreateLanguageDAO()
		{
			return new LanguageDAO();
		}

		/// <summary>Creates a new LicensedModuleDAO object</summary>
		/// <returns>the new DAO object ready to use for LicensedModule Entities</returns>
		public static LicensedModuleDAO CreateLicensedModuleDAO()
		{
			return new LicensedModuleDAO();
		}

		/// <summary>Creates a new LicensedUIElementDAO object</summary>
		/// <returns>the new DAO object ready to use for LicensedUIElement Entities</returns>
		public static LicensedUIElementDAO CreateLicensedUIElementDAO()
		{
			return new LicensedUIElementDAO();
		}

		/// <summary>Creates a new LicensedUIElementSubPanelDAO object</summary>
		/// <returns>the new DAO object ready to use for LicensedUIElementSubPanel Entities</returns>
		public static LicensedUIElementSubPanelDAO CreateLicensedUIElementSubPanelDAO()
		{
			return new LicensedUIElementSubPanelDAO();
		}

		/// <summary>Creates a new MapDAO object</summary>
		/// <returns>the new DAO object ready to use for Map Entities</returns>
		public static MapDAO CreateMapDAO()
		{
			return new MapDAO();
		}

		/// <summary>Creates a new MapPointOfInterestDAO object</summary>
		/// <returns>the new DAO object ready to use for MapPointOfInterest Entities</returns>
		public static MapPointOfInterestDAO CreateMapPointOfInterestDAO()
		{
			return new MapPointOfInterestDAO();
		}

		/// <summary>Creates a new MediaDAO object</summary>
		/// <returns>the new DAO object ready to use for Media Entities</returns>
		public static MediaDAO CreateMediaDAO()
		{
			return new MediaDAO();
		}

		/// <summary>Creates a new MediaCultureDAO object</summary>
		/// <returns>the new DAO object ready to use for MediaCulture Entities</returns>
		public static MediaCultureDAO CreateMediaCultureDAO()
		{
			return new MediaCultureDAO();
		}

		/// <summary>Creates a new MediaLanguageDAO object</summary>
		/// <returns>the new DAO object ready to use for MediaLanguage Entities</returns>
		public static MediaLanguageDAO CreateMediaLanguageDAO()
		{
			return new MediaLanguageDAO();
		}

		/// <summary>Creates a new MediaProcessingTaskDAO object</summary>
		/// <returns>the new DAO object ready to use for MediaProcessingTask Entities</returns>
		public static MediaProcessingTaskDAO CreateMediaProcessingTaskDAO()
		{
			return new MediaProcessingTaskDAO();
		}

		/// <summary>Creates a new MediaRatioTypeMediaDAO object</summary>
		/// <returns>the new DAO object ready to use for MediaRatioTypeMedia Entities</returns>
		public static MediaRatioTypeMediaDAO CreateMediaRatioTypeMediaDAO()
		{
			return new MediaRatioTypeMediaDAO();
		}

		/// <summary>Creates a new MediaRatioTypeMediaFileDAO object</summary>
		/// <returns>the new DAO object ready to use for MediaRatioTypeMediaFile Entities</returns>
		public static MediaRatioTypeMediaFileDAO CreateMediaRatioTypeMediaFileDAO()
		{
			return new MediaRatioTypeMediaFileDAO();
		}

		/// <summary>Creates a new MediaRelationshipDAO object</summary>
		/// <returns>the new DAO object ready to use for MediaRelationship Entities</returns>
		public static MediaRelationshipDAO CreateMediaRelationshipDAO()
		{
			return new MediaRelationshipDAO();
		}

		/// <summary>Creates a new MenuDAO object</summary>
		/// <returns>the new DAO object ready to use for Menu Entities</returns>
		public static MenuDAO CreateMenuDAO()
		{
			return new MenuDAO();
		}

		/// <summary>Creates a new MessageDAO object</summary>
		/// <returns>the new DAO object ready to use for Message Entities</returns>
		public static MessageDAO CreateMessageDAO()
		{
			return new MessageDAO();
		}

		/// <summary>Creates a new MessagegroupDAO object</summary>
		/// <returns>the new DAO object ready to use for Messagegroup Entities</returns>
		public static MessagegroupDAO CreateMessagegroupDAO()
		{
			return new MessagegroupDAO();
		}

		/// <summary>Creates a new MessagegroupDeliverypointDAO object</summary>
		/// <returns>the new DAO object ready to use for MessagegroupDeliverypoint Entities</returns>
		public static MessagegroupDeliverypointDAO CreateMessagegroupDeliverypointDAO()
		{
			return new MessagegroupDeliverypointDAO();
		}

		/// <summary>Creates a new MessageRecipientDAO object</summary>
		/// <returns>the new DAO object ready to use for MessageRecipient Entities</returns>
		public static MessageRecipientDAO CreateMessageRecipientDAO()
		{
			return new MessageRecipientDAO();
		}

		/// <summary>Creates a new MessageTemplateDAO object</summary>
		/// <returns>the new DAO object ready to use for MessageTemplate Entities</returns>
		public static MessageTemplateDAO CreateMessageTemplateDAO()
		{
			return new MessageTemplateDAO();
		}

		/// <summary>Creates a new MessageTemplateCategoryDAO object</summary>
		/// <returns>the new DAO object ready to use for MessageTemplateCategory Entities</returns>
		public static MessageTemplateCategoryDAO CreateMessageTemplateCategoryDAO()
		{
			return new MessageTemplateCategoryDAO();
		}

		/// <summary>Creates a new MessageTemplateCategoryMessageTemplateDAO object</summary>
		/// <returns>the new DAO object ready to use for MessageTemplateCategoryMessageTemplate Entities</returns>
		public static MessageTemplateCategoryMessageTemplateDAO CreateMessageTemplateCategoryMessageTemplateDAO()
		{
			return new MessageTemplateCategoryMessageTemplateDAO();
		}

		/// <summary>Creates a new ModuleDAO object</summary>
		/// <returns>the new DAO object ready to use for Module Entities</returns>
		public static ModuleDAO CreateModuleDAO()
		{
			return new ModuleDAO();
		}

		/// <summary>Creates a new NetmessageDAO object</summary>
		/// <returns>the new DAO object ready to use for Netmessage Entities</returns>
		public static NetmessageDAO CreateNetmessageDAO()
		{
			return new NetmessageDAO();
		}

		/// <summary>Creates a new OptInDAO object</summary>
		/// <returns>the new DAO object ready to use for OptIn Entities</returns>
		public static OptInDAO CreateOptInDAO()
		{
			return new OptInDAO();
		}

		/// <summary>Creates a new OrderDAO object</summary>
		/// <returns>the new DAO object ready to use for Order Entities</returns>
		public static OrderDAO CreateOrderDAO()
		{
			return new OrderDAO();
		}

		/// <summary>Creates a new OrderHourDAO object</summary>
		/// <returns>the new DAO object ready to use for OrderHour Entities</returns>
		public static OrderHourDAO CreateOrderHourDAO()
		{
			return new OrderHourDAO();
		}

		/// <summary>Creates a new OrderitemDAO object</summary>
		/// <returns>the new DAO object ready to use for Orderitem Entities</returns>
		public static OrderitemDAO CreateOrderitemDAO()
		{
			return new OrderitemDAO();
		}

		/// <summary>Creates a new OrderitemAlterationitemDAO object</summary>
		/// <returns>the new DAO object ready to use for OrderitemAlterationitem Entities</returns>
		public static OrderitemAlterationitemDAO CreateOrderitemAlterationitemDAO()
		{
			return new OrderitemAlterationitemDAO();
		}

		/// <summary>Creates a new OrderitemAlterationitemTagDAO object</summary>
		/// <returns>the new DAO object ready to use for OrderitemAlterationitemTag Entities</returns>
		public static OrderitemAlterationitemTagDAO CreateOrderitemAlterationitemTagDAO()
		{
			return new OrderitemAlterationitemTagDAO();
		}

		/// <summary>Creates a new OrderitemTagDAO object</summary>
		/// <returns>the new DAO object ready to use for OrderitemTag Entities</returns>
		public static OrderitemTagDAO CreateOrderitemTagDAO()
		{
			return new OrderitemTagDAO();
		}

		/// <summary>Creates a new OrderNotificationLogDAO object</summary>
		/// <returns>the new DAO object ready to use for OrderNotificationLog Entities</returns>
		public static OrderNotificationLogDAO CreateOrderNotificationLogDAO()
		{
			return new OrderNotificationLogDAO();
		}

		/// <summary>Creates a new OrderRoutestephandlerDAO object</summary>
		/// <returns>the new DAO object ready to use for OrderRoutestephandler Entities</returns>
		public static OrderRoutestephandlerDAO CreateOrderRoutestephandlerDAO()
		{
			return new OrderRoutestephandlerDAO();
		}

		/// <summary>Creates a new OrderRoutestephandlerHistoryDAO object</summary>
		/// <returns>the new DAO object ready to use for OrderRoutestephandlerHistory Entities</returns>
		public static OrderRoutestephandlerHistoryDAO CreateOrderRoutestephandlerHistoryDAO()
		{
			return new OrderRoutestephandlerHistoryDAO();
		}

		/// <summary>Creates a new OutletDAO object</summary>
		/// <returns>the new DAO object ready to use for Outlet Entities</returns>
		public static OutletDAO CreateOutletDAO()
		{
			return new OutletDAO();
		}

		/// <summary>Creates a new OutletOperationalStateDAO object</summary>
		/// <returns>the new DAO object ready to use for OutletOperationalState Entities</returns>
		public static OutletOperationalStateDAO CreateOutletOperationalStateDAO()
		{
			return new OutletOperationalStateDAO();
		}

		/// <summary>Creates a new OutletSellerInformationDAO object</summary>
		/// <returns>the new DAO object ready to use for OutletSellerInformation Entities</returns>
		public static OutletSellerInformationDAO CreateOutletSellerInformationDAO()
		{
			return new OutletSellerInformationDAO();
		}

		/// <summary>Creates a new PageDAO object</summary>
		/// <returns>the new DAO object ready to use for Page Entities</returns>
		public static PageDAO CreatePageDAO()
		{
			return new PageDAO();
		}

		/// <summary>Creates a new PageElementDAO object</summary>
		/// <returns>the new DAO object ready to use for PageElement Entities</returns>
		public static PageElementDAO CreatePageElementDAO()
		{
			return new PageElementDAO();
		}

		/// <summary>Creates a new PageLanguageDAO object</summary>
		/// <returns>the new DAO object ready to use for PageLanguage Entities</returns>
		public static PageLanguageDAO CreatePageLanguageDAO()
		{
			return new PageLanguageDAO();
		}

		/// <summary>Creates a new PageTemplateDAO object</summary>
		/// <returns>the new DAO object ready to use for PageTemplate Entities</returns>
		public static PageTemplateDAO CreatePageTemplateDAO()
		{
			return new PageTemplateDAO();
		}

		/// <summary>Creates a new PageTemplateElementDAO object</summary>
		/// <returns>the new DAO object ready to use for PageTemplateElement Entities</returns>
		public static PageTemplateElementDAO CreatePageTemplateElementDAO()
		{
			return new PageTemplateElementDAO();
		}

		/// <summary>Creates a new PageTemplateLanguageDAO object</summary>
		/// <returns>the new DAO object ready to use for PageTemplateLanguage Entities</returns>
		public static PageTemplateLanguageDAO CreatePageTemplateLanguageDAO()
		{
			return new PageTemplateLanguageDAO();
		}

		/// <summary>Creates a new PaymentIntegrationConfigurationDAO object</summary>
		/// <returns>the new DAO object ready to use for PaymentIntegrationConfiguration Entities</returns>
		public static PaymentIntegrationConfigurationDAO CreatePaymentIntegrationConfigurationDAO()
		{
			return new PaymentIntegrationConfigurationDAO();
		}

		/// <summary>Creates a new PaymentProviderDAO object</summary>
		/// <returns>the new DAO object ready to use for PaymentProvider Entities</returns>
		public static PaymentProviderDAO CreatePaymentProviderDAO()
		{
			return new PaymentProviderDAO();
		}

		/// <summary>Creates a new PaymentProviderCompanyDAO object</summary>
		/// <returns>the new DAO object ready to use for PaymentProviderCompany Entities</returns>
		public static PaymentProviderCompanyDAO CreatePaymentProviderCompanyDAO()
		{
			return new PaymentProviderCompanyDAO();
		}

		/// <summary>Creates a new PaymentTransactionDAO object</summary>
		/// <returns>the new DAO object ready to use for PaymentTransaction Entities</returns>
		public static PaymentTransactionDAO CreatePaymentTransactionDAO()
		{
			return new PaymentTransactionDAO();
		}

		/// <summary>Creates a new PaymentTransactionLogDAO object</summary>
		/// <returns>the new DAO object ready to use for PaymentTransactionLog Entities</returns>
		public static PaymentTransactionLogDAO CreatePaymentTransactionLogDAO()
		{
			return new PaymentTransactionLogDAO();
		}

		/// <summary>Creates a new PaymentTransactionSplitDAO object</summary>
		/// <returns>the new DAO object ready to use for PaymentTransactionSplit Entities</returns>
		public static PaymentTransactionSplitDAO CreatePaymentTransactionSplitDAO()
		{
			return new PaymentTransactionSplitDAO();
		}

		/// <summary>Creates a new PmsActionRuleDAO object</summary>
		/// <returns>the new DAO object ready to use for PmsActionRule Entities</returns>
		public static PmsActionRuleDAO CreatePmsActionRuleDAO()
		{
			return new PmsActionRuleDAO();
		}

		/// <summary>Creates a new PmsReportColumnDAO object</summary>
		/// <returns>the new DAO object ready to use for PmsReportColumn Entities</returns>
		public static PmsReportColumnDAO CreatePmsReportColumnDAO()
		{
			return new PmsReportColumnDAO();
		}

		/// <summary>Creates a new PmsReportConfigurationDAO object</summary>
		/// <returns>the new DAO object ready to use for PmsReportConfiguration Entities</returns>
		public static PmsReportConfigurationDAO CreatePmsReportConfigurationDAO()
		{
			return new PmsReportConfigurationDAO();
		}

		/// <summary>Creates a new PmsRuleDAO object</summary>
		/// <returns>the new DAO object ready to use for PmsRule Entities</returns>
		public static PmsRuleDAO CreatePmsRuleDAO()
		{
			return new PmsRuleDAO();
		}

		/// <summary>Creates a new PointOfInterestDAO object</summary>
		/// <returns>the new DAO object ready to use for PointOfInterest Entities</returns>
		public static PointOfInterestDAO CreatePointOfInterestDAO()
		{
			return new PointOfInterestDAO();
		}

		/// <summary>Creates a new PointOfInterestAmenityDAO object</summary>
		/// <returns>the new DAO object ready to use for PointOfInterestAmenity Entities</returns>
		public static PointOfInterestAmenityDAO CreatePointOfInterestAmenityDAO()
		{
			return new PointOfInterestAmenityDAO();
		}

		/// <summary>Creates a new PointOfInterestLanguageDAO object</summary>
		/// <returns>the new DAO object ready to use for PointOfInterestLanguage Entities</returns>
		public static PointOfInterestLanguageDAO CreatePointOfInterestLanguageDAO()
		{
			return new PointOfInterestLanguageDAO();
		}

		/// <summary>Creates a new PointOfInterestVenueCategoryDAO object</summary>
		/// <returns>the new DAO object ready to use for PointOfInterestVenueCategory Entities</returns>
		public static PointOfInterestVenueCategoryDAO CreatePointOfInterestVenueCategoryDAO()
		{
			return new PointOfInterestVenueCategoryDAO();
		}

		/// <summary>Creates a new PosalterationDAO object</summary>
		/// <returns>the new DAO object ready to use for Posalteration Entities</returns>
		public static PosalterationDAO CreatePosalterationDAO()
		{
			return new PosalterationDAO();
		}

		/// <summary>Creates a new PosalterationitemDAO object</summary>
		/// <returns>the new DAO object ready to use for Posalterationitem Entities</returns>
		public static PosalterationitemDAO CreatePosalterationitemDAO()
		{
			return new PosalterationitemDAO();
		}

		/// <summary>Creates a new PosalterationoptionDAO object</summary>
		/// <returns>the new DAO object ready to use for Posalterationoption Entities</returns>
		public static PosalterationoptionDAO CreatePosalterationoptionDAO()
		{
			return new PosalterationoptionDAO();
		}

		/// <summary>Creates a new PoscategoryDAO object</summary>
		/// <returns>the new DAO object ready to use for Poscategory Entities</returns>
		public static PoscategoryDAO CreatePoscategoryDAO()
		{
			return new PoscategoryDAO();
		}

		/// <summary>Creates a new PosdeliverypointDAO object</summary>
		/// <returns>the new DAO object ready to use for Posdeliverypoint Entities</returns>
		public static PosdeliverypointDAO CreatePosdeliverypointDAO()
		{
			return new PosdeliverypointDAO();
		}

		/// <summary>Creates a new PosdeliverypointgroupDAO object</summary>
		/// <returns>the new DAO object ready to use for Posdeliverypointgroup Entities</returns>
		public static PosdeliverypointgroupDAO CreatePosdeliverypointgroupDAO()
		{
			return new PosdeliverypointgroupDAO();
		}

		/// <summary>Creates a new PospaymentmethodDAO object</summary>
		/// <returns>the new DAO object ready to use for Pospaymentmethod Entities</returns>
		public static PospaymentmethodDAO CreatePospaymentmethodDAO()
		{
			return new PospaymentmethodDAO();
		}

		/// <summary>Creates a new PosproductDAO object</summary>
		/// <returns>the new DAO object ready to use for Posproduct Entities</returns>
		public static PosproductDAO CreatePosproductDAO()
		{
			return new PosproductDAO();
		}

		/// <summary>Creates a new PosproductPosalterationDAO object</summary>
		/// <returns>the new DAO object ready to use for PosproductPosalteration Entities</returns>
		public static PosproductPosalterationDAO CreatePosproductPosalterationDAO()
		{
			return new PosproductPosalterationDAO();
		}

		/// <summary>Creates a new PriceLevelDAO object</summary>
		/// <returns>the new DAO object ready to use for PriceLevel Entities</returns>
		public static PriceLevelDAO CreatePriceLevelDAO()
		{
			return new PriceLevelDAO();
		}

		/// <summary>Creates a new PriceLevelItemDAO object</summary>
		/// <returns>the new DAO object ready to use for PriceLevelItem Entities</returns>
		public static PriceLevelItemDAO CreatePriceLevelItemDAO()
		{
			return new PriceLevelItemDAO();
		}

		/// <summary>Creates a new PriceScheduleDAO object</summary>
		/// <returns>the new DAO object ready to use for PriceSchedule Entities</returns>
		public static PriceScheduleDAO CreatePriceScheduleDAO()
		{
			return new PriceScheduleDAO();
		}

		/// <summary>Creates a new PriceScheduleItemDAO object</summary>
		/// <returns>the new DAO object ready to use for PriceScheduleItem Entities</returns>
		public static PriceScheduleItemDAO CreatePriceScheduleItemDAO()
		{
			return new PriceScheduleItemDAO();
		}

		/// <summary>Creates a new PriceScheduleItemOccurrenceDAO object</summary>
		/// <returns>the new DAO object ready to use for PriceScheduleItemOccurrence Entities</returns>
		public static PriceScheduleItemOccurrenceDAO CreatePriceScheduleItemOccurrenceDAO()
		{
			return new PriceScheduleItemOccurrenceDAO();
		}

		/// <summary>Creates a new ProductDAO object</summary>
		/// <returns>the new DAO object ready to use for Product Entities</returns>
		public static ProductDAO CreateProductDAO()
		{
			return new ProductDAO();
		}

		/// <summary>Creates a new ProductAlterationDAO object</summary>
		/// <returns>the new DAO object ready to use for ProductAlteration Entities</returns>
		public static ProductAlterationDAO CreateProductAlterationDAO()
		{
			return new ProductAlterationDAO();
		}

		/// <summary>Creates a new ProductAttachmentDAO object</summary>
		/// <returns>the new DAO object ready to use for ProductAttachment Entities</returns>
		public static ProductAttachmentDAO CreateProductAttachmentDAO()
		{
			return new ProductAttachmentDAO();
		}

		/// <summary>Creates a new ProductCategoryDAO object</summary>
		/// <returns>the new DAO object ready to use for ProductCategory Entities</returns>
		public static ProductCategoryDAO CreateProductCategoryDAO()
		{
			return new ProductCategoryDAO();
		}

		/// <summary>Creates a new ProductCategorySuggestionDAO object</summary>
		/// <returns>the new DAO object ready to use for ProductCategorySuggestion Entities</returns>
		public static ProductCategorySuggestionDAO CreateProductCategorySuggestionDAO()
		{
			return new ProductCategorySuggestionDAO();
		}

		/// <summary>Creates a new ProductCategoryTagDAO object</summary>
		/// <returns>the new DAO object ready to use for ProductCategoryTag Entities</returns>
		public static ProductCategoryTagDAO CreateProductCategoryTagDAO()
		{
			return new ProductCategoryTagDAO();
		}

		/// <summary>Creates a new ProductgroupDAO object</summary>
		/// <returns>the new DAO object ready to use for Productgroup Entities</returns>
		public static ProductgroupDAO CreateProductgroupDAO()
		{
			return new ProductgroupDAO();
		}

		/// <summary>Creates a new ProductgroupItemDAO object</summary>
		/// <returns>the new DAO object ready to use for ProductgroupItem Entities</returns>
		public static ProductgroupItemDAO CreateProductgroupItemDAO()
		{
			return new ProductgroupItemDAO();
		}

		/// <summary>Creates a new ProductLanguageDAO object</summary>
		/// <returns>the new DAO object ready to use for ProductLanguage Entities</returns>
		public static ProductLanguageDAO CreateProductLanguageDAO()
		{
			return new ProductLanguageDAO();
		}

		/// <summary>Creates a new ProductSuggestionDAO object</summary>
		/// <returns>the new DAO object ready to use for ProductSuggestion Entities</returns>
		public static ProductSuggestionDAO CreateProductSuggestionDAO()
		{
			return new ProductSuggestionDAO();
		}

		/// <summary>Creates a new ProductTagDAO object</summary>
		/// <returns>the new DAO object ready to use for ProductTag Entities</returns>
		public static ProductTagDAO CreateProductTagDAO()
		{
			return new ProductTagDAO();
		}

		/// <summary>Creates a new PublishingDAO object</summary>
		/// <returns>the new DAO object ready to use for Publishing Entities</returns>
		public static PublishingDAO CreatePublishingDAO()
		{
			return new PublishingDAO();
		}

		/// <summary>Creates a new PublishingItemDAO object</summary>
		/// <returns>the new DAO object ready to use for PublishingItem Entities</returns>
		public static PublishingItemDAO CreatePublishingItemDAO()
		{
			return new PublishingItemDAO();
		}

		/// <summary>Creates a new RatingDAO object</summary>
		/// <returns>the new DAO object ready to use for Rating Entities</returns>
		public static RatingDAO CreateRatingDAO()
		{
			return new RatingDAO();
		}

		/// <summary>Creates a new ReceiptDAO object</summary>
		/// <returns>the new DAO object ready to use for Receipt Entities</returns>
		public static ReceiptDAO CreateReceiptDAO()
		{
			return new ReceiptDAO();
		}

		/// <summary>Creates a new ReceiptTemplateDAO object</summary>
		/// <returns>the new DAO object ready to use for ReceiptTemplate Entities</returns>
		public static ReceiptTemplateDAO CreateReceiptTemplateDAO()
		{
			return new ReceiptTemplateDAO();
		}

		/// <summary>Creates a new ReferentialConstraintDAO object</summary>
		/// <returns>the new DAO object ready to use for ReferentialConstraint Entities</returns>
		public static ReferentialConstraintDAO CreateReferentialConstraintDAO()
		{
			return new ReferentialConstraintDAO();
		}

		/// <summary>Creates a new ReferentialConstraintCustomDAO object</summary>
		/// <returns>the new DAO object ready to use for ReferentialConstraintCustom Entities</returns>
		public static ReferentialConstraintCustomDAO CreateReferentialConstraintCustomDAO()
		{
			return new ReferentialConstraintCustomDAO();
		}

		/// <summary>Creates a new ReleaseDAO object</summary>
		/// <returns>the new DAO object ready to use for Release Entities</returns>
		public static ReleaseDAO CreateReleaseDAO()
		{
			return new ReleaseDAO();
		}

		/// <summary>Creates a new ReportProcessingTaskDAO object</summary>
		/// <returns>the new DAO object ready to use for ReportProcessingTask Entities</returns>
		public static ReportProcessingTaskDAO CreateReportProcessingTaskDAO()
		{
			return new ReportProcessingTaskDAO();
		}

		/// <summary>Creates a new ReportProcessingTaskFileDAO object</summary>
		/// <returns>the new DAO object ready to use for ReportProcessingTaskFile Entities</returns>
		public static ReportProcessingTaskFileDAO CreateReportProcessingTaskFileDAO()
		{
			return new ReportProcessingTaskFileDAO();
		}

		/// <summary>Creates a new ReportProcessingTaskTemplateDAO object</summary>
		/// <returns>the new DAO object ready to use for ReportProcessingTaskTemplate Entities</returns>
		public static ReportProcessingTaskTemplateDAO CreateReportProcessingTaskTemplateDAO()
		{
			return new ReportProcessingTaskTemplateDAO();
		}

		/// <summary>Creates a new RequestlogDAO object</summary>
		/// <returns>the new DAO object ready to use for Requestlog Entities</returns>
		public static RequestlogDAO CreateRequestlogDAO()
		{
			return new RequestlogDAO();
		}

		/// <summary>Creates a new RoleDAO object</summary>
		/// <returns>the new DAO object ready to use for Role Entities</returns>
		public static RoleDAO CreateRoleDAO()
		{
			return new RoleDAO();
		}

		/// <summary>Creates a new RoleModuleRightsDAO object</summary>
		/// <returns>the new DAO object ready to use for RoleModuleRights Entities</returns>
		public static RoleModuleRightsDAO CreateRoleModuleRightsDAO()
		{
			return new RoleModuleRightsDAO();
		}

		/// <summary>Creates a new RoleUIElementRightsDAO object</summary>
		/// <returns>the new DAO object ready to use for RoleUIElementRights Entities</returns>
		public static RoleUIElementRightsDAO CreateRoleUIElementRightsDAO()
		{
			return new RoleUIElementRightsDAO();
		}

		/// <summary>Creates a new RoleUIElementSubPanelRightsDAO object</summary>
		/// <returns>the new DAO object ready to use for RoleUIElementSubPanelRights Entities</returns>
		public static RoleUIElementSubPanelRightsDAO CreateRoleUIElementSubPanelRightsDAO()
		{
			return new RoleUIElementSubPanelRightsDAO();
		}

		/// <summary>Creates a new RoomControlAreaDAO object</summary>
		/// <returns>the new DAO object ready to use for RoomControlArea Entities</returns>
		public static RoomControlAreaDAO CreateRoomControlAreaDAO()
		{
			return new RoomControlAreaDAO();
		}

		/// <summary>Creates a new RoomControlAreaLanguageDAO object</summary>
		/// <returns>the new DAO object ready to use for RoomControlAreaLanguage Entities</returns>
		public static RoomControlAreaLanguageDAO CreateRoomControlAreaLanguageDAO()
		{
			return new RoomControlAreaLanguageDAO();
		}

		/// <summary>Creates a new RoomControlComponentDAO object</summary>
		/// <returns>the new DAO object ready to use for RoomControlComponent Entities</returns>
		public static RoomControlComponentDAO CreateRoomControlComponentDAO()
		{
			return new RoomControlComponentDAO();
		}

		/// <summary>Creates a new RoomControlComponentLanguageDAO object</summary>
		/// <returns>the new DAO object ready to use for RoomControlComponentLanguage Entities</returns>
		public static RoomControlComponentLanguageDAO CreateRoomControlComponentLanguageDAO()
		{
			return new RoomControlComponentLanguageDAO();
		}

		/// <summary>Creates a new RoomControlConfigurationDAO object</summary>
		/// <returns>the new DAO object ready to use for RoomControlConfiguration Entities</returns>
		public static RoomControlConfigurationDAO CreateRoomControlConfigurationDAO()
		{
			return new RoomControlConfigurationDAO();
		}

		/// <summary>Creates a new RoomControlSectionDAO object</summary>
		/// <returns>the new DAO object ready to use for RoomControlSection Entities</returns>
		public static RoomControlSectionDAO CreateRoomControlSectionDAO()
		{
			return new RoomControlSectionDAO();
		}

		/// <summary>Creates a new RoomControlSectionItemDAO object</summary>
		/// <returns>the new DAO object ready to use for RoomControlSectionItem Entities</returns>
		public static RoomControlSectionItemDAO CreateRoomControlSectionItemDAO()
		{
			return new RoomControlSectionItemDAO();
		}

		/// <summary>Creates a new RoomControlSectionItemLanguageDAO object</summary>
		/// <returns>the new DAO object ready to use for RoomControlSectionItemLanguage Entities</returns>
		public static RoomControlSectionItemLanguageDAO CreateRoomControlSectionItemLanguageDAO()
		{
			return new RoomControlSectionItemLanguageDAO();
		}

		/// <summary>Creates a new RoomControlSectionLanguageDAO object</summary>
		/// <returns>the new DAO object ready to use for RoomControlSectionLanguage Entities</returns>
		public static RoomControlSectionLanguageDAO CreateRoomControlSectionLanguageDAO()
		{
			return new RoomControlSectionLanguageDAO();
		}

		/// <summary>Creates a new RoomControlWidgetDAO object</summary>
		/// <returns>the new DAO object ready to use for RoomControlWidget Entities</returns>
		public static RoomControlWidgetDAO CreateRoomControlWidgetDAO()
		{
			return new RoomControlWidgetDAO();
		}

		/// <summary>Creates a new RoomControlWidgetLanguageDAO object</summary>
		/// <returns>the new DAO object ready to use for RoomControlWidgetLanguage Entities</returns>
		public static RoomControlWidgetLanguageDAO CreateRoomControlWidgetLanguageDAO()
		{
			return new RoomControlWidgetLanguageDAO();
		}

		/// <summary>Creates a new RouteDAO object</summary>
		/// <returns>the new DAO object ready to use for Route Entities</returns>
		public static RouteDAO CreateRouteDAO()
		{
			return new RouteDAO();
		}

		/// <summary>Creates a new RoutestepDAO object</summary>
		/// <returns>the new DAO object ready to use for Routestep Entities</returns>
		public static RoutestepDAO CreateRoutestepDAO()
		{
			return new RoutestepDAO();
		}

		/// <summary>Creates a new RoutestephandlerDAO object</summary>
		/// <returns>the new DAO object ready to use for Routestephandler Entities</returns>
		public static RoutestephandlerDAO CreateRoutestephandlerDAO()
		{
			return new RoutestephandlerDAO();
		}

		/// <summary>Creates a new ScheduleDAO object</summary>
		/// <returns>the new DAO object ready to use for Schedule Entities</returns>
		public static ScheduleDAO CreateScheduleDAO()
		{
			return new ScheduleDAO();
		}

		/// <summary>Creates a new ScheduledCommandDAO object</summary>
		/// <returns>the new DAO object ready to use for ScheduledCommand Entities</returns>
		public static ScheduledCommandDAO CreateScheduledCommandDAO()
		{
			return new ScheduledCommandDAO();
		}

		/// <summary>Creates a new ScheduledCommandTaskDAO object</summary>
		/// <returns>the new DAO object ready to use for ScheduledCommandTask Entities</returns>
		public static ScheduledCommandTaskDAO CreateScheduledCommandTaskDAO()
		{
			return new ScheduledCommandTaskDAO();
		}

		/// <summary>Creates a new ScheduledMessageDAO object</summary>
		/// <returns>the new DAO object ready to use for ScheduledMessage Entities</returns>
		public static ScheduledMessageDAO CreateScheduledMessageDAO()
		{
			return new ScheduledMessageDAO();
		}

		/// <summary>Creates a new ScheduledMessageHistoryDAO object</summary>
		/// <returns>the new DAO object ready to use for ScheduledMessageHistory Entities</returns>
		public static ScheduledMessageHistoryDAO CreateScheduledMessageHistoryDAO()
		{
			return new ScheduledMessageHistoryDAO();
		}

		/// <summary>Creates a new ScheduledMessageLanguageDAO object</summary>
		/// <returns>the new DAO object ready to use for ScheduledMessageLanguage Entities</returns>
		public static ScheduledMessageLanguageDAO CreateScheduledMessageLanguageDAO()
		{
			return new ScheduledMessageLanguageDAO();
		}

		/// <summary>Creates a new ScheduleitemDAO object</summary>
		/// <returns>the new DAO object ready to use for Scheduleitem Entities</returns>
		public static ScheduleitemDAO CreateScheduleitemDAO()
		{
			return new ScheduleitemDAO();
		}

		/// <summary>Creates a new ServiceMethodDAO object</summary>
		/// <returns>the new DAO object ready to use for ServiceMethod Entities</returns>
		public static ServiceMethodDAO CreateServiceMethodDAO()
		{
			return new ServiceMethodDAO();
		}

		/// <summary>Creates a new ServiceMethodDeliverypointgroupDAO object</summary>
		/// <returns>the new DAO object ready to use for ServiceMethodDeliverypointgroup Entities</returns>
		public static ServiceMethodDeliverypointgroupDAO CreateServiceMethodDeliverypointgroupDAO()
		{
			return new ServiceMethodDeliverypointgroupDAO();
		}

		/// <summary>Creates a new SetupCodeDAO object</summary>
		/// <returns>the new DAO object ready to use for SetupCode Entities</returns>
		public static SetupCodeDAO CreateSetupCodeDAO()
		{
			return new SetupCodeDAO();
		}

		/// <summary>Creates a new SiteDAO object</summary>
		/// <returns>the new DAO object ready to use for Site Entities</returns>
		public static SiteDAO CreateSiteDAO()
		{
			return new SiteDAO();
		}

		/// <summary>Creates a new SiteCultureDAO object</summary>
		/// <returns>the new DAO object ready to use for SiteCulture Entities</returns>
		public static SiteCultureDAO CreateSiteCultureDAO()
		{
			return new SiteCultureDAO();
		}

		/// <summary>Creates a new SiteLanguageDAO object</summary>
		/// <returns>the new DAO object ready to use for SiteLanguage Entities</returns>
		public static SiteLanguageDAO CreateSiteLanguageDAO()
		{
			return new SiteLanguageDAO();
		}

		/// <summary>Creates a new SiteTemplateDAO object</summary>
		/// <returns>the new DAO object ready to use for SiteTemplate Entities</returns>
		public static SiteTemplateDAO CreateSiteTemplateDAO()
		{
			return new SiteTemplateDAO();
		}

		/// <summary>Creates a new SiteTemplateCultureDAO object</summary>
		/// <returns>the new DAO object ready to use for SiteTemplateCulture Entities</returns>
		public static SiteTemplateCultureDAO CreateSiteTemplateCultureDAO()
		{
			return new SiteTemplateCultureDAO();
		}

		/// <summary>Creates a new SiteTemplateLanguageDAO object</summary>
		/// <returns>the new DAO object ready to use for SiteTemplateLanguage Entities</returns>
		public static SiteTemplateLanguageDAO CreateSiteTemplateLanguageDAO()
		{
			return new SiteTemplateLanguageDAO();
		}

		/// <summary>Creates a new SmsInformationDAO object</summary>
		/// <returns>the new DAO object ready to use for SmsInformation Entities</returns>
		public static SmsInformationDAO CreateSmsInformationDAO()
		{
			return new SmsInformationDAO();
		}

		/// <summary>Creates a new SmsKeywordDAO object</summary>
		/// <returns>the new DAO object ready to use for SmsKeyword Entities</returns>
		public static SmsKeywordDAO CreateSmsKeywordDAO()
		{
			return new SmsKeywordDAO();
		}

		/// <summary>Creates a new StationDAO object</summary>
		/// <returns>the new DAO object ready to use for Station Entities</returns>
		public static StationDAO CreateStationDAO()
		{
			return new StationDAO();
		}

		/// <summary>Creates a new StationLanguageDAO object</summary>
		/// <returns>the new DAO object ready to use for StationLanguage Entities</returns>
		public static StationLanguageDAO CreateStationLanguageDAO()
		{
			return new StationLanguageDAO();
		}

		/// <summary>Creates a new StationListDAO object</summary>
		/// <returns>the new DAO object ready to use for StationList Entities</returns>
		public static StationListDAO CreateStationListDAO()
		{
			return new StationListDAO();
		}

		/// <summary>Creates a new SupplierDAO object</summary>
		/// <returns>the new DAO object ready to use for Supplier Entities</returns>
		public static SupplierDAO CreateSupplierDAO()
		{
			return new SupplierDAO();
		}

		/// <summary>Creates a new SupportagentDAO object</summary>
		/// <returns>the new DAO object ready to use for Supportagent Entities</returns>
		public static SupportagentDAO CreateSupportagentDAO()
		{
			return new SupportagentDAO();
		}

		/// <summary>Creates a new SupportpoolDAO object</summary>
		/// <returns>the new DAO object ready to use for Supportpool Entities</returns>
		public static SupportpoolDAO CreateSupportpoolDAO()
		{
			return new SupportpoolDAO();
		}

		/// <summary>Creates a new SupportpoolNotificationRecipientDAO object</summary>
		/// <returns>the new DAO object ready to use for SupportpoolNotificationRecipient Entities</returns>
		public static SupportpoolNotificationRecipientDAO CreateSupportpoolNotificationRecipientDAO()
		{
			return new SupportpoolNotificationRecipientDAO();
		}

		/// <summary>Creates a new SupportpoolSupportagentDAO object</summary>
		/// <returns>the new DAO object ready to use for SupportpoolSupportagent Entities</returns>
		public static SupportpoolSupportagentDAO CreateSupportpoolSupportagentDAO()
		{
			return new SupportpoolSupportagentDAO();
		}

		/// <summary>Creates a new SurveyDAO object</summary>
		/// <returns>the new DAO object ready to use for Survey Entities</returns>
		public static SurveyDAO CreateSurveyDAO()
		{
			return new SurveyDAO();
		}

		/// <summary>Creates a new SurveyAnswerDAO object</summary>
		/// <returns>the new DAO object ready to use for SurveyAnswer Entities</returns>
		public static SurveyAnswerDAO CreateSurveyAnswerDAO()
		{
			return new SurveyAnswerDAO();
		}

		/// <summary>Creates a new SurveyAnswerLanguageDAO object</summary>
		/// <returns>the new DAO object ready to use for SurveyAnswerLanguage Entities</returns>
		public static SurveyAnswerLanguageDAO CreateSurveyAnswerLanguageDAO()
		{
			return new SurveyAnswerLanguageDAO();
		}

		/// <summary>Creates a new SurveyLanguageDAO object</summary>
		/// <returns>the new DAO object ready to use for SurveyLanguage Entities</returns>
		public static SurveyLanguageDAO CreateSurveyLanguageDAO()
		{
			return new SurveyLanguageDAO();
		}

		/// <summary>Creates a new SurveyPageDAO object</summary>
		/// <returns>the new DAO object ready to use for SurveyPage Entities</returns>
		public static SurveyPageDAO CreateSurveyPageDAO()
		{
			return new SurveyPageDAO();
		}

		/// <summary>Creates a new SurveyQuestionDAO object</summary>
		/// <returns>the new DAO object ready to use for SurveyQuestion Entities</returns>
		public static SurveyQuestionDAO CreateSurveyQuestionDAO()
		{
			return new SurveyQuestionDAO();
		}

		/// <summary>Creates a new SurveyQuestionLanguageDAO object</summary>
		/// <returns>the new DAO object ready to use for SurveyQuestionLanguage Entities</returns>
		public static SurveyQuestionLanguageDAO CreateSurveyQuestionLanguageDAO()
		{
			return new SurveyQuestionLanguageDAO();
		}

		/// <summary>Creates a new SurveyResultDAO object</summary>
		/// <returns>the new DAO object ready to use for SurveyResult Entities</returns>
		public static SurveyResultDAO CreateSurveyResultDAO()
		{
			return new SurveyResultDAO();
		}

		/// <summary>Creates a new TagDAO object</summary>
		/// <returns>the new DAO object ready to use for Tag Entities</returns>
		public static TagDAO CreateTagDAO()
		{
			return new TagDAO();
		}

		/// <summary>Creates a new TaxTariffDAO object</summary>
		/// <returns>the new DAO object ready to use for TaxTariff Entities</returns>
		public static TaxTariffDAO CreateTaxTariffDAO()
		{
			return new TaxTariffDAO();
		}

		/// <summary>Creates a new TerminalDAO object</summary>
		/// <returns>the new DAO object ready to use for Terminal Entities</returns>
		public static TerminalDAO CreateTerminalDAO()
		{
			return new TerminalDAO();
		}

		/// <summary>Creates a new TerminalConfigurationDAO object</summary>
		/// <returns>the new DAO object ready to use for TerminalConfiguration Entities</returns>
		public static TerminalConfigurationDAO CreateTerminalConfigurationDAO()
		{
			return new TerminalConfigurationDAO();
		}

		/// <summary>Creates a new TerminalLogDAO object</summary>
		/// <returns>the new DAO object ready to use for TerminalLog Entities</returns>
		public static TerminalLogDAO CreateTerminalLogDAO()
		{
			return new TerminalLogDAO();
		}

		/// <summary>Creates a new TerminalLogFileDAO object</summary>
		/// <returns>the new DAO object ready to use for TerminalLogFile Entities</returns>
		public static TerminalLogFileDAO CreateTerminalLogFileDAO()
		{
			return new TerminalLogFileDAO();
		}

		/// <summary>Creates a new TerminalMessageTemplateCategoryDAO object</summary>
		/// <returns>the new DAO object ready to use for TerminalMessageTemplateCategory Entities</returns>
		public static TerminalMessageTemplateCategoryDAO CreateTerminalMessageTemplateCategoryDAO()
		{
			return new TerminalMessageTemplateCategoryDAO();
		}

		/// <summary>Creates a new TerminalStateDAO object</summary>
		/// <returns>the new DAO object ready to use for TerminalState Entities</returns>
		public static TerminalStateDAO CreateTerminalStateDAO()
		{
			return new TerminalStateDAO();
		}

		/// <summary>Creates a new TimestampDAO object</summary>
		/// <returns>the new DAO object ready to use for Timestamp Entities</returns>
		public static TimestampDAO CreateTimestampDAO()
		{
			return new TimestampDAO();
		}

		/// <summary>Creates a new TimeZoneDAO object</summary>
		/// <returns>the new DAO object ready to use for TimeZone Entities</returns>
		public static TimeZoneDAO CreateTimeZoneDAO()
		{
			return new TimeZoneDAO();
		}

		/// <summary>Creates a new TranslationDAO object</summary>
		/// <returns>the new DAO object ready to use for Translation Entities</returns>
		public static TranslationDAO CreateTranslationDAO()
		{
			return new TranslationDAO();
		}

		/// <summary>Creates a new UIElementDAO object</summary>
		/// <returns>the new DAO object ready to use for UIElement Entities</returns>
		public static UIElementDAO CreateUIElementDAO()
		{
			return new UIElementDAO();
		}

		/// <summary>Creates a new UIElementCustomDAO object</summary>
		/// <returns>the new DAO object ready to use for UIElementCustom Entities</returns>
		public static UIElementCustomDAO CreateUIElementCustomDAO()
		{
			return new UIElementCustomDAO();
		}

		/// <summary>Creates a new UIElementSubPanelDAO object</summary>
		/// <returns>the new DAO object ready to use for UIElementSubPanel Entities</returns>
		public static UIElementSubPanelDAO CreateUIElementSubPanelDAO()
		{
			return new UIElementSubPanelDAO();
		}

		/// <summary>Creates a new UIElementSubPanelCustomDAO object</summary>
		/// <returns>the new DAO object ready to use for UIElementSubPanelCustom Entities</returns>
		public static UIElementSubPanelCustomDAO CreateUIElementSubPanelCustomDAO()
		{
			return new UIElementSubPanelCustomDAO();
		}

		/// <summary>Creates a new UIElementSubPanelUIElementDAO object</summary>
		/// <returns>the new DAO object ready to use for UIElementSubPanelUIElement Entities</returns>
		public static UIElementSubPanelUIElementDAO CreateUIElementSubPanelUIElementDAO()
		{
			return new UIElementSubPanelUIElementDAO();
		}

		/// <summary>Creates a new UIFooterItemDAO object</summary>
		/// <returns>the new DAO object ready to use for UIFooterItem Entities</returns>
		public static UIFooterItemDAO CreateUIFooterItemDAO()
		{
			return new UIFooterItemDAO();
		}

		/// <summary>Creates a new UIFooterItemLanguageDAO object</summary>
		/// <returns>the new DAO object ready to use for UIFooterItemLanguage Entities</returns>
		public static UIFooterItemLanguageDAO CreateUIFooterItemLanguageDAO()
		{
			return new UIFooterItemLanguageDAO();
		}

		/// <summary>Creates a new UIModeDAO object</summary>
		/// <returns>the new DAO object ready to use for UIMode Entities</returns>
		public static UIModeDAO CreateUIModeDAO()
		{
			return new UIModeDAO();
		}

		/// <summary>Creates a new UIScheduleDAO object</summary>
		/// <returns>the new DAO object ready to use for UISchedule Entities</returns>
		public static UIScheduleDAO CreateUIScheduleDAO()
		{
			return new UIScheduleDAO();
		}

		/// <summary>Creates a new UIScheduleItemDAO object</summary>
		/// <returns>the new DAO object ready to use for UIScheduleItem Entities</returns>
		public static UIScheduleItemDAO CreateUIScheduleItemDAO()
		{
			return new UIScheduleItemDAO();
		}

		/// <summary>Creates a new UIScheduleItemOccurrenceDAO object</summary>
		/// <returns>the new DAO object ready to use for UIScheduleItemOccurrence Entities</returns>
		public static UIScheduleItemOccurrenceDAO CreateUIScheduleItemOccurrenceDAO()
		{
			return new UIScheduleItemOccurrenceDAO();
		}

		/// <summary>Creates a new UITabDAO object</summary>
		/// <returns>the new DAO object ready to use for UITab Entities</returns>
		public static UITabDAO CreateUITabDAO()
		{
			return new UITabDAO();
		}

		/// <summary>Creates a new UITabLanguageDAO object</summary>
		/// <returns>the new DAO object ready to use for UITabLanguage Entities</returns>
		public static UITabLanguageDAO CreateUITabLanguageDAO()
		{
			return new UITabLanguageDAO();
		}

		/// <summary>Creates a new UIThemeDAO object</summary>
		/// <returns>the new DAO object ready to use for UITheme Entities</returns>
		public static UIThemeDAO CreateUIThemeDAO()
		{
			return new UIThemeDAO();
		}

		/// <summary>Creates a new UIThemeColorDAO object</summary>
		/// <returns>the new DAO object ready to use for UIThemeColor Entities</returns>
		public static UIThemeColorDAO CreateUIThemeColorDAO()
		{
			return new UIThemeColorDAO();
		}

		/// <summary>Creates a new UIThemeTextSizeDAO object</summary>
		/// <returns>the new DAO object ready to use for UIThemeTextSize Entities</returns>
		public static UIThemeTextSizeDAO CreateUIThemeTextSizeDAO()
		{
			return new UIThemeTextSizeDAO();
		}

		/// <summary>Creates a new UIWidgetDAO object</summary>
		/// <returns>the new DAO object ready to use for UIWidget Entities</returns>
		public static UIWidgetDAO CreateUIWidgetDAO()
		{
			return new UIWidgetDAO();
		}

		/// <summary>Creates a new UIWidgetAvailabilityDAO object</summary>
		/// <returns>the new DAO object ready to use for UIWidgetAvailability Entities</returns>
		public static UIWidgetAvailabilityDAO CreateUIWidgetAvailabilityDAO()
		{
			return new UIWidgetAvailabilityDAO();
		}

		/// <summary>Creates a new UIWidgetLanguageDAO object</summary>
		/// <returns>the new DAO object ready to use for UIWidgetLanguage Entities</returns>
		public static UIWidgetLanguageDAO CreateUIWidgetLanguageDAO()
		{
			return new UIWidgetLanguageDAO();
		}

		/// <summary>Creates a new UIWidgetTimerDAO object</summary>
		/// <returns>the new DAO object ready to use for UIWidgetTimer Entities</returns>
		public static UIWidgetTimerDAO CreateUIWidgetTimerDAO()
		{
			return new UIWidgetTimerDAO();
		}

		/// <summary>Creates a new UserDAO object</summary>
		/// <returns>the new DAO object ready to use for User Entities</returns>
		public static UserDAO CreateUserDAO()
		{
			return new UserDAO();
		}

		/// <summary>Creates a new UserBrandDAO object</summary>
		/// <returns>the new DAO object ready to use for UserBrand Entities</returns>
		public static UserBrandDAO CreateUserBrandDAO()
		{
			return new UserBrandDAO();
		}

		/// <summary>Creates a new UserLogonDAO object</summary>
		/// <returns>the new DAO object ready to use for UserLogon Entities</returns>
		public static UserLogonDAO CreateUserLogonDAO()
		{
			return new UserLogonDAO();
		}

		/// <summary>Creates a new UserRoleDAO object</summary>
		/// <returns>the new DAO object ready to use for UserRole Entities</returns>
		public static UserRoleDAO CreateUserRoleDAO()
		{
			return new UserRoleDAO();
		}

		/// <summary>Creates a new VattariffDAO object</summary>
		/// <returns>the new DAO object ready to use for Vattariff Entities</returns>
		public static VattariffDAO CreateVattariffDAO()
		{
			return new VattariffDAO();
		}

		/// <summary>Creates a new VendorDAO object</summary>
		/// <returns>the new DAO object ready to use for Vendor Entities</returns>
		public static VendorDAO CreateVendorDAO()
		{
			return new VendorDAO();
		}

		/// <summary>Creates a new VenueCategoryDAO object</summary>
		/// <returns>the new DAO object ready to use for VenueCategory Entities</returns>
		public static VenueCategoryDAO CreateVenueCategoryDAO()
		{
			return new VenueCategoryDAO();
		}

		/// <summary>Creates a new VenueCategoryLanguageDAO object</summary>
		/// <returns>the new DAO object ready to use for VenueCategoryLanguage Entities</returns>
		public static VenueCategoryLanguageDAO CreateVenueCategoryLanguageDAO()
		{
			return new VenueCategoryLanguageDAO();
		}

		/// <summary>Creates a new ViewDAO object</summary>
		/// <returns>the new DAO object ready to use for View Entities</returns>
		public static ViewDAO CreateViewDAO()
		{
			return new ViewDAO();
		}

		/// <summary>Creates a new ViewCustomDAO object</summary>
		/// <returns>the new DAO object ready to use for ViewCustom Entities</returns>
		public static ViewCustomDAO CreateViewCustomDAO()
		{
			return new ViewCustomDAO();
		}

		/// <summary>Creates a new ViewItemDAO object</summary>
		/// <returns>the new DAO object ready to use for ViewItem Entities</returns>
		public static ViewItemDAO CreateViewItemDAO()
		{
			return new ViewItemDAO();
		}

		/// <summary>Creates a new ViewItemCustomDAO object</summary>
		/// <returns>the new DAO object ready to use for ViewItemCustom Entities</returns>
		public static ViewItemCustomDAO CreateViewItemCustomDAO()
		{
			return new ViewItemCustomDAO();
		}

		/// <summary>Creates a new WeatherDAO object</summary>
		/// <returns>the new DAO object ready to use for Weather Entities</returns>
		public static WeatherDAO CreateWeatherDAO()
		{
			return new WeatherDAO();
		}

		/// <summary>Creates a new WifiConfigurationDAO object</summary>
		/// <returns>the new DAO object ready to use for WifiConfiguration Entities</returns>
		public static WifiConfigurationDAO CreateWifiConfigurationDAO()
		{
			return new WifiConfigurationDAO();
		}

		/// <summary>Creates a new ActionDAO object</summary>
		/// <returns>the new DAO object ready to use for Action Entities</returns>
		public static ActionDAO CreateActionDAO()
		{
			return new ActionDAO();
		}

		/// <summary>Creates a new ApplicationConfigurationDAO object</summary>
		/// <returns>the new DAO object ready to use for ApplicationConfiguration Entities</returns>
		public static ApplicationConfigurationDAO CreateApplicationConfigurationDAO()
		{
			return new ApplicationConfigurationDAO();
		}

		/// <summary>Creates a new CarouselItemDAO object</summary>
		/// <returns>the new DAO object ready to use for CarouselItem Entities</returns>
		public static CarouselItemDAO CreateCarouselItemDAO()
		{
			return new CarouselItemDAO();
		}

		/// <summary>Creates a new FeatureFlagDAO object</summary>
		/// <returns>the new DAO object ready to use for FeatureFlag Entities</returns>
		public static FeatureFlagDAO CreateFeatureFlagDAO()
		{
			return new FeatureFlagDAO();
		}

		/// <summary>Creates a new LandingPageDAO object</summary>
		/// <returns>the new DAO object ready to use for LandingPage Entities</returns>
		public static LandingPageDAO CreateLandingPageDAO()
		{
			return new LandingPageDAO();
		}

		/// <summary>Creates a new LandingPageWidgetDAO object</summary>
		/// <returns>the new DAO object ready to use for LandingPageWidget Entities</returns>
		public static LandingPageWidgetDAO CreateLandingPageWidgetDAO()
		{
			return new LandingPageWidgetDAO();
		}

		/// <summary>Creates a new NavigationMenuDAO object</summary>
		/// <returns>the new DAO object ready to use for NavigationMenu Entities</returns>
		public static NavigationMenuDAO CreateNavigationMenuDAO()
		{
			return new NavigationMenuDAO();
		}

		/// <summary>Creates a new NavigationMenuItemDAO object</summary>
		/// <returns>the new DAO object ready to use for NavigationMenuItem Entities</returns>
		public static NavigationMenuItemDAO CreateNavigationMenuItemDAO()
		{
			return new NavigationMenuItemDAO();
		}

		/// <summary>Creates a new NavigationMenuWidgetDAO object</summary>
		/// <returns>the new DAO object ready to use for NavigationMenuWidget Entities</returns>
		public static NavigationMenuWidgetDAO CreateNavigationMenuWidgetDAO()
		{
			return new NavigationMenuWidgetDAO();
		}

		/// <summary>Creates a new ThemeDAO object</summary>
		/// <returns>the new DAO object ready to use for Theme Entities</returns>
		public static ThemeDAO CreateThemeDAO()
		{
			return new ThemeDAO();
		}

		/// <summary>Creates a new WidgetDAO object</summary>
		/// <returns>the new DAO object ready to use for Widget Entities</returns>
		public static WidgetDAO CreateWidgetDAO()
		{
			return new WidgetDAO();
		}

		/// <summary>Creates a new WidgetActionBannerDAO object</summary>
		/// <returns>the new DAO object ready to use for WidgetActionBanner Entities</returns>
		public static WidgetActionBannerDAO CreateWidgetActionBannerDAO()
		{
			return new WidgetActionBannerDAO();
		}

		/// <summary>Creates a new WidgetActionButtonDAO object</summary>
		/// <returns>the new DAO object ready to use for WidgetActionButton Entities</returns>
		public static WidgetActionButtonDAO CreateWidgetActionButtonDAO()
		{
			return new WidgetActionButtonDAO();
		}

		/// <summary>Creates a new WidgetCarouselDAO object</summary>
		/// <returns>the new DAO object ready to use for WidgetCarousel Entities</returns>
		public static WidgetCarouselDAO CreateWidgetCarouselDAO()
		{
			return new WidgetCarouselDAO();
		}

		/// <summary>Creates a new WidgetGroupDAO object</summary>
		/// <returns>the new DAO object ready to use for WidgetGroup Entities</returns>
		public static WidgetGroupDAO CreateWidgetGroupDAO()
		{
			return new WidgetGroupDAO();
		}

		/// <summary>Creates a new WidgetGroupWidgetDAO object</summary>
		/// <returns>the new DAO object ready to use for WidgetGroupWidget Entities</returns>
		public static WidgetGroupWidgetDAO CreateWidgetGroupWidgetDAO()
		{
			return new WidgetGroupWidgetDAO();
		}

		/// <summary>Creates a new WidgetHeroDAO object</summary>
		/// <returns>the new DAO object ready to use for WidgetHero Entities</returns>
		public static WidgetHeroDAO CreateWidgetHeroDAO()
		{
			return new WidgetHeroDAO();
		}

		/// <summary>Creates a new WidgetLanguageSwitcherDAO object</summary>
		/// <returns>the new DAO object ready to use for WidgetLanguageSwitcher Entities</returns>
		public static WidgetLanguageSwitcherDAO CreateWidgetLanguageSwitcherDAO()
		{
			return new WidgetLanguageSwitcherDAO();
		}

		/// <summary>Creates a new WidgetMarkdownDAO object</summary>
		/// <returns>the new DAO object ready to use for WidgetMarkdown Entities</returns>
		public static WidgetMarkdownDAO CreateWidgetMarkdownDAO()
		{
			return new WidgetMarkdownDAO();
		}

		/// <summary>Creates a new WidgetOpeningTimeDAO object</summary>
		/// <returns>the new DAO object ready to use for WidgetOpeningTime Entities</returns>
		public static WidgetOpeningTimeDAO CreateWidgetOpeningTimeDAO()
		{
			return new WidgetOpeningTimeDAO();
		}

		/// <summary>Creates a new WidgetPageTitleDAO object</summary>
		/// <returns>the new DAO object ready to use for WidgetPageTitle Entities</returns>
		public static WidgetPageTitleDAO CreateWidgetPageTitleDAO()
		{
			return new WidgetPageTitleDAO();
		}

		/// <summary>Creates a new WidgetWaitTimeDAO object</summary>
		/// <returns>the new DAO object ready to use for WidgetWaitTime Entities</returns>
		public static WidgetWaitTimeDAO CreateWidgetWaitTimeDAO()
		{
			return new WidgetWaitTimeDAO();
		}

		/// <summary>Creates a new TypedListDAO object</summary>
		/// <returns>The new DAO object ready to use for Typed Lists</returns>
		public static TypedListDAO CreateTypedListDAO()
		{
			return new TypedListDAO();
		}

		#region Included Code

		#endregion
	}
}
