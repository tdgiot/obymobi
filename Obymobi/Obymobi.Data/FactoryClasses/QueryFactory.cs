﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
////////////////////////////////////////////////////////////// 
using System;
using System.Linq;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using SD.LLBLGen.Pro.QuerySpec;

namespace Obymobi.Data.FactoryClasses
{
	/// <summary>Factory class to produce DynamicQuery instances and EntityQuery instances</summary>
	public partial class QueryFactory
	{
		private int _aliasCounter = 0;

		/// <summary>Creates a new DynamicQuery instance with no alias set.</summary>
		/// <returns>Ready to use DynamicQuery instance</returns>
		public DynamicQuery Create()
		{
			return Create(string.Empty);
		}

		/// <summary>Creates a new DynamicQuery instance with the alias specified as the alias set.</summary>
		/// <param name="alias">The alias.</param>
		/// <returns>Ready to use DynamicQuery instance</returns>
		public DynamicQuery Create(string alias)
		{
			return new DynamicQuery(new ElementCreator(), alias, this.GetNextAliasCounterValue());
		}

		/// <summary>Creates a new DynamicQuery which wraps the specified TableValuedFunction call</summary>
		/// <param name="toWrap">The table valued function call to wrap.</param>
		/// <returns>toWrap wrapped in a DynamicQuery.</returns>
		public DynamicQuery Create(TableValuedFunctionCall toWrap)
		{
			return this.Create().From(new TvfCallWrapper(toWrap)).Select(toWrap.GetFieldsAsArray().Select(f => this.Field(toWrap.Alias, f.Alias)).ToArray());
		}

		/// <summary>Creates a new EntityQuery for the entity of the type specified with no alias set.</summary>
		/// <typeparam name="TEntity">The type of the entity to produce the query for.</typeparam>
		/// <returns>ready to use EntityQuery instance</returns>
		public EntityQuery<TEntity> Create<TEntity>()
			where TEntity : IEntityCore
		{
			return Create<TEntity>(string.Empty);
		}

		/// <summary>Creates a new EntityQuery for the entity of the type specified with the alias specified as the alias set.</summary>
		/// <typeparam name="TEntity">The type of the entity to produce the query for.</typeparam>
		/// <param name="alias">The alias.</param>
		/// <returns>ready to use EntityQuery instance</returns>
		public EntityQuery<TEntity> Create<TEntity>(string alias)
			where TEntity : IEntityCore
		{
			return new EntityQuery<TEntity>(new ElementCreator(), alias, this.GetNextAliasCounterValue());
		}
				
		/// <summary>Creates a new field object with the name specified and of resulttype 'object'. Used for referring to aliased fields in another projection.</summary>
		/// <param name="fieldName">Name of the field.</param>
		/// <returns>Ready to use field object</returns>
		public EntityField Field(string fieldName)
		{
			return Field<object>(string.Empty, fieldName);
		}

		/// <summary>Creates a new field object with the name specified and of resulttype 'object'. Used for referring to aliased fields in another projection.</summary>
		/// <param name="targetAlias">The alias of the table/query to target.</param>
		/// <param name="fieldName">Name of the field.</param>
		/// <returns>Ready to use field object</returns>
		public EntityField Field(string targetAlias, string fieldName)
		{
			return Field<object>(targetAlias, fieldName);
		}

		/// <summary>Creates a new field object with the name specified and of resulttype 'TValue'. Used for referring to aliased fields in another projection.</summary>
		/// <typeparam name="TValue">The type of the value represented by the field.</typeparam>
		/// <param name="fieldName">Name of the field.</param>
		/// <returns>Ready to use field object</returns>
		public EntityField Field<TValue>(string fieldName)
		{
			return Field<TValue>(string.Empty, fieldName);
		}

		/// <summary>Creates a new field object with the name specified and of resulttype 'TValue'. Used for referring to aliased fields in another projection.</summary>
		/// <typeparam name="TValue">The type of the value.</typeparam>
		/// <param name="targetAlias">The alias of the table/query to target.</param>
		/// <param name="fieldName">Name of the field.</param>
		/// <returns>Ready to use field object</returns>
		public EntityField Field<TValue>(string targetAlias, string fieldName)
		{
			return new EntityField(fieldName, targetAlias, typeof(TValue));
		}
						
		/// <summary>Gets the next alias counter value to produce artifical aliases with</summary>
		private int GetNextAliasCounterValue()
		{
			_aliasCounter++;
			return _aliasCounter;
		}
		

		/// <summary>Creates and returns a new EntityQuery for the AccessCode entity</summary>
		public EntityQuery<AccessCodeEntity> AccessCode
		{
			get { return Create<AccessCodeEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the AccessCodeCompany entity</summary>
		public EntityQuery<AccessCodeCompanyEntity> AccessCodeCompany
		{
			get { return Create<AccessCodeCompanyEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the AccessCodePointOfInterest entity</summary>
		public EntityQuery<AccessCodePointOfInterestEntity> AccessCodePointOfInterest
		{
			get { return Create<AccessCodePointOfInterestEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the Account entity</summary>
		public EntityQuery<AccountEntity> Account
		{
			get { return Create<AccountEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the AccountCompany entity</summary>
		public EntityQuery<AccountCompanyEntity> AccountCompany
		{
			get { return Create<AccountCompanyEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the ActionButton entity</summary>
		public EntityQuery<ActionButtonEntity> ActionButton
		{
			get { return Create<ActionButtonEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the ActionButtonLanguage entity</summary>
		public EntityQuery<ActionButtonLanguageEntity> ActionButtonLanguage
		{
			get { return Create<ActionButtonLanguageEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the Address entity</summary>
		public EntityQuery<AddressEntity> Address
		{
			get { return Create<AddressEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the Advertisement entity</summary>
		public EntityQuery<AdvertisementEntity> Advertisement
		{
			get { return Create<AdvertisementEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the AdvertisementConfiguration entity</summary>
		public EntityQuery<AdvertisementConfigurationEntity> AdvertisementConfiguration
		{
			get { return Create<AdvertisementConfigurationEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the AdvertisementConfigurationAdvertisement entity</summary>
		public EntityQuery<AdvertisementConfigurationAdvertisementEntity> AdvertisementConfigurationAdvertisement
		{
			get { return Create<AdvertisementConfigurationAdvertisementEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the AdvertisementLanguage entity</summary>
		public EntityQuery<AdvertisementLanguageEntity> AdvertisementLanguage
		{
			get { return Create<AdvertisementLanguageEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the AdvertisementTag entity</summary>
		public EntityQuery<AdvertisementTagEntity> AdvertisementTag
		{
			get { return Create<AdvertisementTagEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the AdvertisementTagAdvertisement entity</summary>
		public EntityQuery<AdvertisementTagAdvertisementEntity> AdvertisementTagAdvertisement
		{
			get { return Create<AdvertisementTagAdvertisementEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the AdvertisementTagCategory entity</summary>
		public EntityQuery<AdvertisementTagCategoryEntity> AdvertisementTagCategory
		{
			get { return Create<AdvertisementTagCategoryEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the AdvertisementTagEntertainment entity</summary>
		public EntityQuery<AdvertisementTagEntertainmentEntity> AdvertisementTagEntertainment
		{
			get { return Create<AdvertisementTagEntertainmentEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the AdvertisementTagGenericproduct entity</summary>
		public EntityQuery<AdvertisementTagGenericproductEntity> AdvertisementTagGenericproduct
		{
			get { return Create<AdvertisementTagGenericproductEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the AdvertisementTagLanguage entity</summary>
		public EntityQuery<AdvertisementTagLanguageEntity> AdvertisementTagLanguage
		{
			get { return Create<AdvertisementTagLanguageEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the AdvertisementTagProduct entity</summary>
		public EntityQuery<AdvertisementTagProductEntity> AdvertisementTagProduct
		{
			get { return Create<AdvertisementTagProductEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the AdyenPaymentMethod entity</summary>
		public EntityQuery<AdyenPaymentMethodEntity> AdyenPaymentMethod
		{
			get { return Create<AdyenPaymentMethodEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the AdyenPaymentMethodBrand entity</summary>
		public EntityQuery<AdyenPaymentMethodBrandEntity> AdyenPaymentMethodBrand
		{
			get { return Create<AdyenPaymentMethodBrandEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the AffiliateCampaign entity</summary>
		public EntityQuery<AffiliateCampaignEntity> AffiliateCampaign
		{
			get { return Create<AffiliateCampaignEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the AffiliateCampaignAffiliatePartner entity</summary>
		public EntityQuery<AffiliateCampaignAffiliatePartnerEntity> AffiliateCampaignAffiliatePartner
		{
			get { return Create<AffiliateCampaignAffiliatePartnerEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the AffiliatePartner entity</summary>
		public EntityQuery<AffiliatePartnerEntity> AffiliatePartner
		{
			get { return Create<AffiliatePartnerEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the Alteration entity</summary>
		public EntityQuery<AlterationEntity> Alteration
		{
			get { return Create<AlterationEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the Alterationitem entity</summary>
		public EntityQuery<AlterationitemEntity> Alterationitem
		{
			get { return Create<AlterationitemEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the AlterationitemAlteration entity</summary>
		public EntityQuery<AlterationitemAlterationEntity> AlterationitemAlteration
		{
			get { return Create<AlterationitemAlterationEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the AlterationLanguage entity</summary>
		public EntityQuery<AlterationLanguageEntity> AlterationLanguage
		{
			get { return Create<AlterationLanguageEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the Alterationoption entity</summary>
		public EntityQuery<AlterationoptionEntity> Alterationoption
		{
			get { return Create<AlterationoptionEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the AlterationoptionLanguage entity</summary>
		public EntityQuery<AlterationoptionLanguageEntity> AlterationoptionLanguage
		{
			get { return Create<AlterationoptionLanguageEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the AlterationoptionTag entity</summary>
		public EntityQuery<AlterationoptionTagEntity> AlterationoptionTag
		{
			get { return Create<AlterationoptionTagEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the AlterationProduct entity</summary>
		public EntityQuery<AlterationProductEntity> AlterationProduct
		{
			get { return Create<AlterationProductEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the Amenity entity</summary>
		public EntityQuery<AmenityEntity> Amenity
		{
			get { return Create<AmenityEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the AmenityLanguage entity</summary>
		public EntityQuery<AmenityLanguageEntity> AmenityLanguage
		{
			get { return Create<AmenityLanguageEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the AnalyticsProcessingTask entity</summary>
		public EntityQuery<AnalyticsProcessingTaskEntity> AnalyticsProcessingTask
		{
			get { return Create<AnalyticsProcessingTaskEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the Announcement entity</summary>
		public EntityQuery<AnnouncementEntity> Announcement
		{
			get { return Create<AnnouncementEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the AnnouncementLanguage entity</summary>
		public EntityQuery<AnnouncementLanguageEntity> AnnouncementLanguage
		{
			get { return Create<AnnouncementLanguageEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the ApiAuthentication entity</summary>
		public EntityQuery<ApiAuthenticationEntity> ApiAuthentication
		{
			get { return Create<ApiAuthenticationEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the Application entity</summary>
		public EntityQuery<ApplicationEntity> Application
		{
			get { return Create<ApplicationEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the Attachment entity</summary>
		public EntityQuery<AttachmentEntity> Attachment
		{
			get { return Create<AttachmentEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the AttachmentLanguage entity</summary>
		public EntityQuery<AttachmentLanguageEntity> AttachmentLanguage
		{
			get { return Create<AttachmentLanguageEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the Auditlog entity</summary>
		public EntityQuery<AuditlogEntity> Auditlog
		{
			get { return Create<AuditlogEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the Availability entity</summary>
		public EntityQuery<AvailabilityEntity> Availability
		{
			get { return Create<AvailabilityEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the AzureNotificationHub entity</summary>
		public EntityQuery<AzureNotificationHubEntity> AzureNotificationHub
		{
			get { return Create<AzureNotificationHubEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the Brand entity</summary>
		public EntityQuery<BrandEntity> Brand
		{
			get { return Create<BrandEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the BrandCulture entity</summary>
		public EntityQuery<BrandCultureEntity> BrandCulture
		{
			get { return Create<BrandCultureEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the Businesshours entity</summary>
		public EntityQuery<BusinesshoursEntity> Businesshours
		{
			get { return Create<BusinesshoursEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the Businesshoursexception entity</summary>
		public EntityQuery<BusinesshoursexceptionEntity> Businesshoursexception
		{
			get { return Create<BusinesshoursexceptionEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the Category entity</summary>
		public EntityQuery<CategoryEntity> Category
		{
			get { return Create<CategoryEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the CategoryAlteration entity</summary>
		public EntityQuery<CategoryAlterationEntity> CategoryAlteration
		{
			get { return Create<CategoryAlterationEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the CategoryLanguage entity</summary>
		public EntityQuery<CategoryLanguageEntity> CategoryLanguage
		{
			get { return Create<CategoryLanguageEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the CategorySuggestion entity</summary>
		public EntityQuery<CategorySuggestionEntity> CategorySuggestion
		{
			get { return Create<CategorySuggestionEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the CategoryTag entity</summary>
		public EntityQuery<CategoryTagEntity> CategoryTag
		{
			get { return Create<CategoryTagEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the CheckoutMethod entity</summary>
		public EntityQuery<CheckoutMethodEntity> CheckoutMethod
		{
			get { return Create<CheckoutMethodEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the CheckoutMethodDeliverypointgroup entity</summary>
		public EntityQuery<CheckoutMethodDeliverypointgroupEntity> CheckoutMethodDeliverypointgroup
		{
			get { return Create<CheckoutMethodDeliverypointgroupEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the Client entity</summary>
		public EntityQuery<ClientEntity> Client
		{
			get { return Create<ClientEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the ClientConfiguration entity</summary>
		public EntityQuery<ClientConfigurationEntity> ClientConfiguration
		{
			get { return Create<ClientConfigurationEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the ClientConfigurationRoute entity</summary>
		public EntityQuery<ClientConfigurationRouteEntity> ClientConfigurationRoute
		{
			get { return Create<ClientConfigurationRouteEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the ClientEntertainment entity</summary>
		public EntityQuery<ClientEntertainmentEntity> ClientEntertainment
		{
			get { return Create<ClientEntertainmentEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the ClientLog entity</summary>
		public EntityQuery<ClientLogEntity> ClientLog
		{
			get { return Create<ClientLogEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the ClientLogFile entity</summary>
		public EntityQuery<ClientLogFileEntity> ClientLogFile
		{
			get { return Create<ClientLogFileEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the ClientState entity</summary>
		public EntityQuery<ClientStateEntity> ClientState
		{
			get { return Create<ClientStateEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the CloudApplicationVersion entity</summary>
		public EntityQuery<CloudApplicationVersionEntity> CloudApplicationVersion
		{
			get { return Create<CloudApplicationVersionEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the CloudProcessingTask entity</summary>
		public EntityQuery<CloudProcessingTaskEntity> CloudProcessingTask
		{
			get { return Create<CloudProcessingTaskEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the CloudStorageAccount entity</summary>
		public EntityQuery<CloudStorageAccountEntity> CloudStorageAccount
		{
			get { return Create<CloudStorageAccountEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the Company entity</summary>
		public EntityQuery<CompanyEntity> Company
		{
			get { return Create<CompanyEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the CompanyAmenity entity</summary>
		public EntityQuery<CompanyAmenityEntity> CompanyAmenity
		{
			get { return Create<CompanyAmenityEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the CompanyBrand entity</summary>
		public EntityQuery<CompanyBrandEntity> CompanyBrand
		{
			get { return Create<CompanyBrandEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the CompanyCulture entity</summary>
		public EntityQuery<CompanyCultureEntity> CompanyCulture
		{
			get { return Create<CompanyCultureEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the CompanyCurrency entity</summary>
		public EntityQuery<CompanyCurrencyEntity> CompanyCurrency
		{
			get { return Create<CompanyCurrencyEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the CompanyEntertainment entity</summary>
		public EntityQuery<CompanyEntertainmentEntity> CompanyEntertainment
		{
			get { return Create<CompanyEntertainmentEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the Companygroup entity</summary>
		public EntityQuery<CompanygroupEntity> Companygroup
		{
			get { return Create<CompanygroupEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the CompanyLanguage entity</summary>
		public EntityQuery<CompanyLanguageEntity> CompanyLanguage
		{
			get { return Create<CompanyLanguageEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the CompanyManagementTask entity</summary>
		public EntityQuery<CompanyManagementTaskEntity> CompanyManagementTask
		{
			get { return Create<CompanyManagementTaskEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the CompanyOwner entity</summary>
		public EntityQuery<CompanyOwnerEntity> CompanyOwner
		{
			get { return Create<CompanyOwnerEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the CompanyRelease entity</summary>
		public EntityQuery<CompanyReleaseEntity> CompanyRelease
		{
			get { return Create<CompanyReleaseEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the CompanyVenueCategory entity</summary>
		public EntityQuery<CompanyVenueCategoryEntity> CompanyVenueCategory
		{
			get { return Create<CompanyVenueCategoryEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the Configuration entity</summary>
		public EntityQuery<ConfigurationEntity> Configuration
		{
			get { return Create<ConfigurationEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the Country entity</summary>
		public EntityQuery<CountryEntity> Country
		{
			get { return Create<CountryEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the Currency entity</summary>
		public EntityQuery<CurrencyEntity> Currency
		{
			get { return Create<CurrencyEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the Customer entity</summary>
		public EntityQuery<CustomerEntity> Customer
		{
			get { return Create<CustomerEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the CustomText entity</summary>
		public EntityQuery<CustomTextEntity> CustomText
		{
			get { return Create<CustomTextEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the DeliveryDistance entity</summary>
		public EntityQuery<DeliveryDistanceEntity> DeliveryDistance
		{
			get { return Create<DeliveryDistanceEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the DeliveryInformation entity</summary>
		public EntityQuery<DeliveryInformationEntity> DeliveryInformation
		{
			get { return Create<DeliveryInformationEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the Deliverypoint entity</summary>
		public EntityQuery<DeliverypointEntity> Deliverypoint
		{
			get { return Create<DeliverypointEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the DeliverypointExternalDeliverypoint entity</summary>
		public EntityQuery<DeliverypointExternalDeliverypointEntity> DeliverypointExternalDeliverypoint
		{
			get { return Create<DeliverypointExternalDeliverypointEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the Deliverypointgroup entity</summary>
		public EntityQuery<DeliverypointgroupEntity> Deliverypointgroup
		{
			get { return Create<DeliverypointgroupEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the DeliverypointgroupAdvertisement entity</summary>
		public EntityQuery<DeliverypointgroupAdvertisementEntity> DeliverypointgroupAdvertisement
		{
			get { return Create<DeliverypointgroupAdvertisementEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the DeliverypointgroupAnnouncement entity</summary>
		public EntityQuery<DeliverypointgroupAnnouncementEntity> DeliverypointgroupAnnouncement
		{
			get { return Create<DeliverypointgroupAnnouncementEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the DeliverypointgroupEntertainment entity</summary>
		public EntityQuery<DeliverypointgroupEntertainmentEntity> DeliverypointgroupEntertainment
		{
			get { return Create<DeliverypointgroupEntertainmentEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the DeliverypointgroupLanguage entity</summary>
		public EntityQuery<DeliverypointgroupLanguageEntity> DeliverypointgroupLanguage
		{
			get { return Create<DeliverypointgroupLanguageEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the DeliverypointgroupOccupancy entity</summary>
		public EntityQuery<DeliverypointgroupOccupancyEntity> DeliverypointgroupOccupancy
		{
			get { return Create<DeliverypointgroupOccupancyEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the DeliverypointgroupProduct entity</summary>
		public EntityQuery<DeliverypointgroupProductEntity> DeliverypointgroupProduct
		{
			get { return Create<DeliverypointgroupProductEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the Device entity</summary>
		public EntityQuery<DeviceEntity> Device
		{
			get { return Create<DeviceEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the Devicemedia entity</summary>
		public EntityQuery<DevicemediaEntity> Devicemedia
		{
			get { return Create<DevicemediaEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the DeviceTokenHistory entity</summary>
		public EntityQuery<DeviceTokenHistoryEntity> DeviceTokenHistory
		{
			get { return Create<DeviceTokenHistoryEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the DeviceTokenTask entity</summary>
		public EntityQuery<DeviceTokenTaskEntity> DeviceTokenTask
		{
			get { return Create<DeviceTokenTaskEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the Entertainment entity</summary>
		public EntityQuery<EntertainmentEntity> Entertainment
		{
			get { return Create<EntertainmentEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the Entertainmentcategory entity</summary>
		public EntityQuery<EntertainmentcategoryEntity> Entertainmentcategory
		{
			get { return Create<EntertainmentcategoryEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the EntertainmentcategoryLanguage entity</summary>
		public EntityQuery<EntertainmentcategoryLanguageEntity> EntertainmentcategoryLanguage
		{
			get { return Create<EntertainmentcategoryLanguageEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the EntertainmentConfiguration entity</summary>
		public EntityQuery<EntertainmentConfigurationEntity> EntertainmentConfiguration
		{
			get { return Create<EntertainmentConfigurationEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the EntertainmentConfigurationEntertainment entity</summary>
		public EntityQuery<EntertainmentConfigurationEntertainmentEntity> EntertainmentConfigurationEntertainment
		{
			get { return Create<EntertainmentConfigurationEntertainmentEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the EntertainmentDependency entity</summary>
		public EntityQuery<EntertainmentDependencyEntity> EntertainmentDependency
		{
			get { return Create<EntertainmentDependencyEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the EntertainmentFile entity</summary>
		public EntityQuery<EntertainmentFileEntity> EntertainmentFile
		{
			get { return Create<EntertainmentFileEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the Entertainmenturl entity</summary>
		public EntityQuery<EntertainmenturlEntity> Entertainmenturl
		{
			get { return Create<EntertainmenturlEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the EntityFieldInformation entity</summary>
		public EntityQuery<EntityFieldInformationEntity> EntityFieldInformation
		{
			get { return Create<EntityFieldInformationEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the EntityFieldInformationCustom entity</summary>
		public EntityQuery<EntityFieldInformationCustomEntity> EntityFieldInformationCustom
		{
			get { return Create<EntityFieldInformationCustomEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the EntityInformation entity</summary>
		public EntityQuery<EntityInformationEntity> EntityInformation
		{
			get { return Create<EntityInformationEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the EntityInformationCustom entity</summary>
		public EntityQuery<EntityInformationCustomEntity> EntityInformationCustom
		{
			get { return Create<EntityInformationCustomEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the ExternalDeliverypoint entity</summary>
		public EntityQuery<ExternalDeliverypointEntity> ExternalDeliverypoint
		{
			get { return Create<ExternalDeliverypointEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the ExternalMenu entity</summary>
		public EntityQuery<ExternalMenuEntity> ExternalMenu
		{
			get { return Create<ExternalMenuEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the ExternalProduct entity</summary>
		public EntityQuery<ExternalProductEntity> ExternalProduct
		{
			get { return Create<ExternalProductEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the ExternalSubProduct entity</summary>
		public EntityQuery<ExternalSubProductEntity> ExternalSubProduct
		{
			get { return Create<ExternalSubProductEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the ExternalSystem entity</summary>
		public EntityQuery<ExternalSystemEntity> ExternalSystem
		{
			get { return Create<ExternalSystemEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the ExternalSystemLog entity</summary>
		public EntityQuery<ExternalSystemLogEntity> ExternalSystemLog
		{
			get { return Create<ExternalSystemLogEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the FeatureToggleAvailability entity</summary>
		public EntityQuery<FeatureToggleAvailabilityEntity> FeatureToggleAvailability
		{
			get { return Create<FeatureToggleAvailabilityEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the Folio entity</summary>
		public EntityQuery<FolioEntity> Folio
		{
			get { return Create<FolioEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the FolioItem entity</summary>
		public EntityQuery<FolioItemEntity> FolioItem
		{
			get { return Create<FolioItemEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the Game entity</summary>
		public EntityQuery<GameEntity> Game
		{
			get { return Create<GameEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the GameSession entity</summary>
		public EntityQuery<GameSessionEntity> GameSession
		{
			get { return Create<GameSessionEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the GameSessionReport entity</summary>
		public EntityQuery<GameSessionReportEntity> GameSessionReport
		{
			get { return Create<GameSessionReportEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the GameSessionReportConfiguration entity</summary>
		public EntityQuery<GameSessionReportConfigurationEntity> GameSessionReportConfiguration
		{
			get { return Create<GameSessionReportConfigurationEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the GameSessionReportItem entity</summary>
		public EntityQuery<GameSessionReportItemEntity> GameSessionReportItem
		{
			get { return Create<GameSessionReportItemEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the Genericalteration entity</summary>
		public EntityQuery<GenericalterationEntity> Genericalteration
		{
			get { return Create<GenericalterationEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the Genericalterationitem entity</summary>
		public EntityQuery<GenericalterationitemEntity> Genericalterationitem
		{
			get { return Create<GenericalterationitemEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the Genericalterationoption entity</summary>
		public EntityQuery<GenericalterationoptionEntity> Genericalterationoption
		{
			get { return Create<GenericalterationoptionEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the Genericcategory entity</summary>
		public EntityQuery<GenericcategoryEntity> Genericcategory
		{
			get { return Create<GenericcategoryEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the GenericcategoryLanguage entity</summary>
		public EntityQuery<GenericcategoryLanguageEntity> GenericcategoryLanguage
		{
			get { return Create<GenericcategoryLanguageEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the Genericproduct entity</summary>
		public EntityQuery<GenericproductEntity> Genericproduct
		{
			get { return Create<GenericproductEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the GenericproductGenericalteration entity</summary>
		public EntityQuery<GenericproductGenericalterationEntity> GenericproductGenericalteration
		{
			get { return Create<GenericproductGenericalterationEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the GenericproductLanguage entity</summary>
		public EntityQuery<GenericproductLanguageEntity> GenericproductLanguage
		{
			get { return Create<GenericproductLanguageEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the GuestInformation entity</summary>
		public EntityQuery<GuestInformationEntity> GuestInformation
		{
			get { return Create<GuestInformationEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the Icrtouchprintermapping entity</summary>
		public EntityQuery<IcrtouchprintermappingEntity> Icrtouchprintermapping
		{
			get { return Create<IcrtouchprintermappingEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the IcrtouchprintermappingDeliverypoint entity</summary>
		public EntityQuery<IcrtouchprintermappingDeliverypointEntity> IcrtouchprintermappingDeliverypoint
		{
			get { return Create<IcrtouchprintermappingDeliverypointEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the IncomingSms entity</summary>
		public EntityQuery<IncomingSmsEntity> IncomingSms
		{
			get { return Create<IncomingSmsEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the InfraredCommand entity</summary>
		public EntityQuery<InfraredCommandEntity> InfraredCommand
		{
			get { return Create<InfraredCommandEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the InfraredConfiguration entity</summary>
		public EntityQuery<InfraredConfigurationEntity> InfraredConfiguration
		{
			get { return Create<InfraredConfigurationEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the Language entity</summary>
		public EntityQuery<LanguageEntity> Language
		{
			get { return Create<LanguageEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the LicensedModule entity</summary>
		public EntityQuery<LicensedModuleEntity> LicensedModule
		{
			get { return Create<LicensedModuleEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the LicensedUIElement entity</summary>
		public EntityQuery<LicensedUIElementEntity> LicensedUIElement
		{
			get { return Create<LicensedUIElementEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the LicensedUIElementSubPanel entity</summary>
		public EntityQuery<LicensedUIElementSubPanelEntity> LicensedUIElementSubPanel
		{
			get { return Create<LicensedUIElementSubPanelEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the Map entity</summary>
		public EntityQuery<MapEntity> Map
		{
			get { return Create<MapEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the MapPointOfInterest entity</summary>
		public EntityQuery<MapPointOfInterestEntity> MapPointOfInterest
		{
			get { return Create<MapPointOfInterestEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the Media entity</summary>
		public EntityQuery<MediaEntity> Media
		{
			get { return Create<MediaEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the MediaCulture entity</summary>
		public EntityQuery<MediaCultureEntity> MediaCulture
		{
			get { return Create<MediaCultureEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the MediaLanguage entity</summary>
		public EntityQuery<MediaLanguageEntity> MediaLanguage
		{
			get { return Create<MediaLanguageEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the MediaProcessingTask entity</summary>
		public EntityQuery<MediaProcessingTaskEntity> MediaProcessingTask
		{
			get { return Create<MediaProcessingTaskEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the MediaRatioTypeMedia entity</summary>
		public EntityQuery<MediaRatioTypeMediaEntity> MediaRatioTypeMedia
		{
			get { return Create<MediaRatioTypeMediaEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the MediaRatioTypeMediaFile entity</summary>
		public EntityQuery<MediaRatioTypeMediaFileEntity> MediaRatioTypeMediaFile
		{
			get { return Create<MediaRatioTypeMediaFileEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the MediaRelationship entity</summary>
		public EntityQuery<MediaRelationshipEntity> MediaRelationship
		{
			get { return Create<MediaRelationshipEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the Menu entity</summary>
		public EntityQuery<MenuEntity> Menu
		{
			get { return Create<MenuEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the Message entity</summary>
		public EntityQuery<MessageEntity> Message
		{
			get { return Create<MessageEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the Messagegroup entity</summary>
		public EntityQuery<MessagegroupEntity> Messagegroup
		{
			get { return Create<MessagegroupEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the MessagegroupDeliverypoint entity</summary>
		public EntityQuery<MessagegroupDeliverypointEntity> MessagegroupDeliverypoint
		{
			get { return Create<MessagegroupDeliverypointEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the MessageRecipient entity</summary>
		public EntityQuery<MessageRecipientEntity> MessageRecipient
		{
			get { return Create<MessageRecipientEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the MessageTemplate entity</summary>
		public EntityQuery<MessageTemplateEntity> MessageTemplate
		{
			get { return Create<MessageTemplateEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the MessageTemplateCategory entity</summary>
		public EntityQuery<MessageTemplateCategoryEntity> MessageTemplateCategory
		{
			get { return Create<MessageTemplateCategoryEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the MessageTemplateCategoryMessageTemplate entity</summary>
		public EntityQuery<MessageTemplateCategoryMessageTemplateEntity> MessageTemplateCategoryMessageTemplate
		{
			get { return Create<MessageTemplateCategoryMessageTemplateEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the Module entity</summary>
		public EntityQuery<ModuleEntity> Module
		{
			get { return Create<ModuleEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the Netmessage entity</summary>
		public EntityQuery<NetmessageEntity> Netmessage
		{
			get { return Create<NetmessageEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the OptIn entity</summary>
		public EntityQuery<OptInEntity> OptIn
		{
			get { return Create<OptInEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the Order entity</summary>
		public EntityQuery<OrderEntity> Order
		{
			get { return Create<OrderEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the OrderHour entity</summary>
		public EntityQuery<OrderHourEntity> OrderHour
		{
			get { return Create<OrderHourEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the Orderitem entity</summary>
		public EntityQuery<OrderitemEntity> Orderitem
		{
			get { return Create<OrderitemEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the OrderitemAlterationitem entity</summary>
		public EntityQuery<OrderitemAlterationitemEntity> OrderitemAlterationitem
		{
			get { return Create<OrderitemAlterationitemEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the OrderitemAlterationitemTag entity</summary>
		public EntityQuery<OrderitemAlterationitemTagEntity> OrderitemAlterationitemTag
		{
			get { return Create<OrderitemAlterationitemTagEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the OrderitemTag entity</summary>
		public EntityQuery<OrderitemTagEntity> OrderitemTag
		{
			get { return Create<OrderitemTagEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the OrderNotificationLog entity</summary>
		public EntityQuery<OrderNotificationLogEntity> OrderNotificationLog
		{
			get { return Create<OrderNotificationLogEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the OrderRoutestephandler entity</summary>
		public EntityQuery<OrderRoutestephandlerEntity> OrderRoutestephandler
		{
			get { return Create<OrderRoutestephandlerEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the OrderRoutestephandlerHistory entity</summary>
		public EntityQuery<OrderRoutestephandlerHistoryEntity> OrderRoutestephandlerHistory
		{
			get { return Create<OrderRoutestephandlerHistoryEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the Outlet entity</summary>
		public EntityQuery<OutletEntity> Outlet
		{
			get { return Create<OutletEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the OutletOperationalState entity</summary>
		public EntityQuery<OutletOperationalStateEntity> OutletOperationalState
		{
			get { return Create<OutletOperationalStateEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the OutletSellerInformation entity</summary>
		public EntityQuery<OutletSellerInformationEntity> OutletSellerInformation
		{
			get { return Create<OutletSellerInformationEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the Page entity</summary>
		public EntityQuery<PageEntity> Page
		{
			get { return Create<PageEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the PageElement entity</summary>
		public EntityQuery<PageElementEntity> PageElement
		{
			get { return Create<PageElementEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the PageLanguage entity</summary>
		public EntityQuery<PageLanguageEntity> PageLanguage
		{
			get { return Create<PageLanguageEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the PageTemplate entity</summary>
		public EntityQuery<PageTemplateEntity> PageTemplate
		{
			get { return Create<PageTemplateEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the PageTemplateElement entity</summary>
		public EntityQuery<PageTemplateElementEntity> PageTemplateElement
		{
			get { return Create<PageTemplateElementEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the PageTemplateLanguage entity</summary>
		public EntityQuery<PageTemplateLanguageEntity> PageTemplateLanguage
		{
			get { return Create<PageTemplateLanguageEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the PaymentIntegrationConfiguration entity</summary>
		public EntityQuery<PaymentIntegrationConfigurationEntity> PaymentIntegrationConfiguration
		{
			get { return Create<PaymentIntegrationConfigurationEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the PaymentProvider entity</summary>
		public EntityQuery<PaymentProviderEntity> PaymentProvider
		{
			get { return Create<PaymentProviderEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the PaymentProviderCompany entity</summary>
		public EntityQuery<PaymentProviderCompanyEntity> PaymentProviderCompany
		{
			get { return Create<PaymentProviderCompanyEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the PaymentTransaction entity</summary>
		public EntityQuery<PaymentTransactionEntity> PaymentTransaction
		{
			get { return Create<PaymentTransactionEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the PaymentTransactionLog entity</summary>
		public EntityQuery<PaymentTransactionLogEntity> PaymentTransactionLog
		{
			get { return Create<PaymentTransactionLogEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the PaymentTransactionSplit entity</summary>
		public EntityQuery<PaymentTransactionSplitEntity> PaymentTransactionSplit
		{
			get { return Create<PaymentTransactionSplitEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the PmsActionRule entity</summary>
		public EntityQuery<PmsActionRuleEntity> PmsActionRule
		{
			get { return Create<PmsActionRuleEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the PmsReportColumn entity</summary>
		public EntityQuery<PmsReportColumnEntity> PmsReportColumn
		{
			get { return Create<PmsReportColumnEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the PmsReportConfiguration entity</summary>
		public EntityQuery<PmsReportConfigurationEntity> PmsReportConfiguration
		{
			get { return Create<PmsReportConfigurationEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the PmsRule entity</summary>
		public EntityQuery<PmsRuleEntity> PmsRule
		{
			get { return Create<PmsRuleEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the PointOfInterest entity</summary>
		public EntityQuery<PointOfInterestEntity> PointOfInterest
		{
			get { return Create<PointOfInterestEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the PointOfInterestAmenity entity</summary>
		public EntityQuery<PointOfInterestAmenityEntity> PointOfInterestAmenity
		{
			get { return Create<PointOfInterestAmenityEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the PointOfInterestLanguage entity</summary>
		public EntityQuery<PointOfInterestLanguageEntity> PointOfInterestLanguage
		{
			get { return Create<PointOfInterestLanguageEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the PointOfInterestVenueCategory entity</summary>
		public EntityQuery<PointOfInterestVenueCategoryEntity> PointOfInterestVenueCategory
		{
			get { return Create<PointOfInterestVenueCategoryEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the Posalteration entity</summary>
		public EntityQuery<PosalterationEntity> Posalteration
		{
			get { return Create<PosalterationEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the Posalterationitem entity</summary>
		public EntityQuery<PosalterationitemEntity> Posalterationitem
		{
			get { return Create<PosalterationitemEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the Posalterationoption entity</summary>
		public EntityQuery<PosalterationoptionEntity> Posalterationoption
		{
			get { return Create<PosalterationoptionEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the Poscategory entity</summary>
		public EntityQuery<PoscategoryEntity> Poscategory
		{
			get { return Create<PoscategoryEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the Posdeliverypoint entity</summary>
		public EntityQuery<PosdeliverypointEntity> Posdeliverypoint
		{
			get { return Create<PosdeliverypointEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the Posdeliverypointgroup entity</summary>
		public EntityQuery<PosdeliverypointgroupEntity> Posdeliverypointgroup
		{
			get { return Create<PosdeliverypointgroupEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the Pospaymentmethod entity</summary>
		public EntityQuery<PospaymentmethodEntity> Pospaymentmethod
		{
			get { return Create<PospaymentmethodEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the Posproduct entity</summary>
		public EntityQuery<PosproductEntity> Posproduct
		{
			get { return Create<PosproductEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the PosproductPosalteration entity</summary>
		public EntityQuery<PosproductPosalterationEntity> PosproductPosalteration
		{
			get { return Create<PosproductPosalterationEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the PriceLevel entity</summary>
		public EntityQuery<PriceLevelEntity> PriceLevel
		{
			get { return Create<PriceLevelEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the PriceLevelItem entity</summary>
		public EntityQuery<PriceLevelItemEntity> PriceLevelItem
		{
			get { return Create<PriceLevelItemEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the PriceSchedule entity</summary>
		public EntityQuery<PriceScheduleEntity> PriceSchedule
		{
			get { return Create<PriceScheduleEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the PriceScheduleItem entity</summary>
		public EntityQuery<PriceScheduleItemEntity> PriceScheduleItem
		{
			get { return Create<PriceScheduleItemEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the PriceScheduleItemOccurrence entity</summary>
		public EntityQuery<PriceScheduleItemOccurrenceEntity> PriceScheduleItemOccurrence
		{
			get { return Create<PriceScheduleItemOccurrenceEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the Product entity</summary>
		public EntityQuery<ProductEntity> Product
		{
			get { return Create<ProductEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the ProductAlteration entity</summary>
		public EntityQuery<ProductAlterationEntity> ProductAlteration
		{
			get { return Create<ProductAlterationEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the ProductAttachment entity</summary>
		public EntityQuery<ProductAttachmentEntity> ProductAttachment
		{
			get { return Create<ProductAttachmentEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the ProductCategory entity</summary>
		public EntityQuery<ProductCategoryEntity> ProductCategory
		{
			get { return Create<ProductCategoryEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the ProductCategorySuggestion entity</summary>
		public EntityQuery<ProductCategorySuggestionEntity> ProductCategorySuggestion
		{
			get { return Create<ProductCategorySuggestionEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the ProductCategoryTag entity</summary>
		public EntityQuery<ProductCategoryTagEntity> ProductCategoryTag
		{
			get { return Create<ProductCategoryTagEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the Productgroup entity</summary>
		public EntityQuery<ProductgroupEntity> Productgroup
		{
			get { return Create<ProductgroupEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the ProductgroupItem entity</summary>
		public EntityQuery<ProductgroupItemEntity> ProductgroupItem
		{
			get { return Create<ProductgroupItemEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the ProductLanguage entity</summary>
		public EntityQuery<ProductLanguageEntity> ProductLanguage
		{
			get { return Create<ProductLanguageEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the ProductSuggestion entity</summary>
		public EntityQuery<ProductSuggestionEntity> ProductSuggestion
		{
			get { return Create<ProductSuggestionEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the ProductTag entity</summary>
		public EntityQuery<ProductTagEntity> ProductTag
		{
			get { return Create<ProductTagEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the Publishing entity</summary>
		public EntityQuery<PublishingEntity> Publishing
		{
			get { return Create<PublishingEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the PublishingItem entity</summary>
		public EntityQuery<PublishingItemEntity> PublishingItem
		{
			get { return Create<PublishingItemEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the Rating entity</summary>
		public EntityQuery<RatingEntity> Rating
		{
			get { return Create<RatingEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the Receipt entity</summary>
		public EntityQuery<ReceiptEntity> Receipt
		{
			get { return Create<ReceiptEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the ReceiptTemplate entity</summary>
		public EntityQuery<ReceiptTemplateEntity> ReceiptTemplate
		{
			get { return Create<ReceiptTemplateEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the ReferentialConstraint entity</summary>
		public EntityQuery<ReferentialConstraintEntity> ReferentialConstraint
		{
			get { return Create<ReferentialConstraintEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the ReferentialConstraintCustom entity</summary>
		public EntityQuery<ReferentialConstraintCustomEntity> ReferentialConstraintCustom
		{
			get { return Create<ReferentialConstraintCustomEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the Release entity</summary>
		public EntityQuery<ReleaseEntity> Release
		{
			get { return Create<ReleaseEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the ReportProcessingTask entity</summary>
		public EntityQuery<ReportProcessingTaskEntity> ReportProcessingTask
		{
			get { return Create<ReportProcessingTaskEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the ReportProcessingTaskFile entity</summary>
		public EntityQuery<ReportProcessingTaskFileEntity> ReportProcessingTaskFile
		{
			get { return Create<ReportProcessingTaskFileEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the ReportProcessingTaskTemplate entity</summary>
		public EntityQuery<ReportProcessingTaskTemplateEntity> ReportProcessingTaskTemplate
		{
			get { return Create<ReportProcessingTaskTemplateEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the Requestlog entity</summary>
		public EntityQuery<RequestlogEntity> Requestlog
		{
			get { return Create<RequestlogEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the Role entity</summary>
		public EntityQuery<RoleEntity> Role
		{
			get { return Create<RoleEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the RoleModuleRights entity</summary>
		public EntityQuery<RoleModuleRightsEntity> RoleModuleRights
		{
			get { return Create<RoleModuleRightsEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the RoleUIElementRights entity</summary>
		public EntityQuery<RoleUIElementRightsEntity> RoleUIElementRights
		{
			get { return Create<RoleUIElementRightsEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the RoleUIElementSubPanelRights entity</summary>
		public EntityQuery<RoleUIElementSubPanelRightsEntity> RoleUIElementSubPanelRights
		{
			get { return Create<RoleUIElementSubPanelRightsEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the RoomControlArea entity</summary>
		public EntityQuery<RoomControlAreaEntity> RoomControlArea
		{
			get { return Create<RoomControlAreaEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the RoomControlAreaLanguage entity</summary>
		public EntityQuery<RoomControlAreaLanguageEntity> RoomControlAreaLanguage
		{
			get { return Create<RoomControlAreaLanguageEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the RoomControlComponent entity</summary>
		public EntityQuery<RoomControlComponentEntity> RoomControlComponent
		{
			get { return Create<RoomControlComponentEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the RoomControlComponentLanguage entity</summary>
		public EntityQuery<RoomControlComponentLanguageEntity> RoomControlComponentLanguage
		{
			get { return Create<RoomControlComponentLanguageEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the RoomControlConfiguration entity</summary>
		public EntityQuery<RoomControlConfigurationEntity> RoomControlConfiguration
		{
			get { return Create<RoomControlConfigurationEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the RoomControlSection entity</summary>
		public EntityQuery<RoomControlSectionEntity> RoomControlSection
		{
			get { return Create<RoomControlSectionEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the RoomControlSectionItem entity</summary>
		public EntityQuery<RoomControlSectionItemEntity> RoomControlSectionItem
		{
			get { return Create<RoomControlSectionItemEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the RoomControlSectionItemLanguage entity</summary>
		public EntityQuery<RoomControlSectionItemLanguageEntity> RoomControlSectionItemLanguage
		{
			get { return Create<RoomControlSectionItemLanguageEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the RoomControlSectionLanguage entity</summary>
		public EntityQuery<RoomControlSectionLanguageEntity> RoomControlSectionLanguage
		{
			get { return Create<RoomControlSectionLanguageEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the RoomControlWidget entity</summary>
		public EntityQuery<RoomControlWidgetEntity> RoomControlWidget
		{
			get { return Create<RoomControlWidgetEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the RoomControlWidgetLanguage entity</summary>
		public EntityQuery<RoomControlWidgetLanguageEntity> RoomControlWidgetLanguage
		{
			get { return Create<RoomControlWidgetLanguageEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the Route entity</summary>
		public EntityQuery<RouteEntity> Route
		{
			get { return Create<RouteEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the Routestep entity</summary>
		public EntityQuery<RoutestepEntity> Routestep
		{
			get { return Create<RoutestepEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the Routestephandler entity</summary>
		public EntityQuery<RoutestephandlerEntity> Routestephandler
		{
			get { return Create<RoutestephandlerEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the Schedule entity</summary>
		public EntityQuery<ScheduleEntity> Schedule
		{
			get { return Create<ScheduleEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the ScheduledCommand entity</summary>
		public EntityQuery<ScheduledCommandEntity> ScheduledCommand
		{
			get { return Create<ScheduledCommandEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the ScheduledCommandTask entity</summary>
		public EntityQuery<ScheduledCommandTaskEntity> ScheduledCommandTask
		{
			get { return Create<ScheduledCommandTaskEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the ScheduledMessage entity</summary>
		public EntityQuery<ScheduledMessageEntity> ScheduledMessage
		{
			get { return Create<ScheduledMessageEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the ScheduledMessageHistory entity</summary>
		public EntityQuery<ScheduledMessageHistoryEntity> ScheduledMessageHistory
		{
			get { return Create<ScheduledMessageHistoryEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the ScheduledMessageLanguage entity</summary>
		public EntityQuery<ScheduledMessageLanguageEntity> ScheduledMessageLanguage
		{
			get { return Create<ScheduledMessageLanguageEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the Scheduleitem entity</summary>
		public EntityQuery<ScheduleitemEntity> Scheduleitem
		{
			get { return Create<ScheduleitemEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the ServiceMethod entity</summary>
		public EntityQuery<ServiceMethodEntity> ServiceMethod
		{
			get { return Create<ServiceMethodEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the ServiceMethodDeliverypointgroup entity</summary>
		public EntityQuery<ServiceMethodDeliverypointgroupEntity> ServiceMethodDeliverypointgroup
		{
			get { return Create<ServiceMethodDeliverypointgroupEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the SetupCode entity</summary>
		public EntityQuery<SetupCodeEntity> SetupCode
		{
			get { return Create<SetupCodeEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the Site entity</summary>
		public EntityQuery<SiteEntity> Site
		{
			get { return Create<SiteEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the SiteCulture entity</summary>
		public EntityQuery<SiteCultureEntity> SiteCulture
		{
			get { return Create<SiteCultureEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the SiteLanguage entity</summary>
		public EntityQuery<SiteLanguageEntity> SiteLanguage
		{
			get { return Create<SiteLanguageEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the SiteTemplate entity</summary>
		public EntityQuery<SiteTemplateEntity> SiteTemplate
		{
			get { return Create<SiteTemplateEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the SiteTemplateCulture entity</summary>
		public EntityQuery<SiteTemplateCultureEntity> SiteTemplateCulture
		{
			get { return Create<SiteTemplateCultureEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the SiteTemplateLanguage entity</summary>
		public EntityQuery<SiteTemplateLanguageEntity> SiteTemplateLanguage
		{
			get { return Create<SiteTemplateLanguageEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the SmsInformation entity</summary>
		public EntityQuery<SmsInformationEntity> SmsInformation
		{
			get { return Create<SmsInformationEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the SmsKeyword entity</summary>
		public EntityQuery<SmsKeywordEntity> SmsKeyword
		{
			get { return Create<SmsKeywordEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the Station entity</summary>
		public EntityQuery<StationEntity> Station
		{
			get { return Create<StationEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the StationLanguage entity</summary>
		public EntityQuery<StationLanguageEntity> StationLanguage
		{
			get { return Create<StationLanguageEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the StationList entity</summary>
		public EntityQuery<StationListEntity> StationList
		{
			get { return Create<StationListEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the Supplier entity</summary>
		public EntityQuery<SupplierEntity> Supplier
		{
			get { return Create<SupplierEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the Supportagent entity</summary>
		public EntityQuery<SupportagentEntity> Supportagent
		{
			get { return Create<SupportagentEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the Supportpool entity</summary>
		public EntityQuery<SupportpoolEntity> Supportpool
		{
			get { return Create<SupportpoolEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the SupportpoolNotificationRecipient entity</summary>
		public EntityQuery<SupportpoolNotificationRecipientEntity> SupportpoolNotificationRecipient
		{
			get { return Create<SupportpoolNotificationRecipientEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the SupportpoolSupportagent entity</summary>
		public EntityQuery<SupportpoolSupportagentEntity> SupportpoolSupportagent
		{
			get { return Create<SupportpoolSupportagentEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the Survey entity</summary>
		public EntityQuery<SurveyEntity> Survey
		{
			get { return Create<SurveyEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the SurveyAnswer entity</summary>
		public EntityQuery<SurveyAnswerEntity> SurveyAnswer
		{
			get { return Create<SurveyAnswerEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the SurveyAnswerLanguage entity</summary>
		public EntityQuery<SurveyAnswerLanguageEntity> SurveyAnswerLanguage
		{
			get { return Create<SurveyAnswerLanguageEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the SurveyLanguage entity</summary>
		public EntityQuery<SurveyLanguageEntity> SurveyLanguage
		{
			get { return Create<SurveyLanguageEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the SurveyPage entity</summary>
		public EntityQuery<SurveyPageEntity> SurveyPage
		{
			get { return Create<SurveyPageEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the SurveyQuestion entity</summary>
		public EntityQuery<SurveyQuestionEntity> SurveyQuestion
		{
			get { return Create<SurveyQuestionEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the SurveyQuestionLanguage entity</summary>
		public EntityQuery<SurveyQuestionLanguageEntity> SurveyQuestionLanguage
		{
			get { return Create<SurveyQuestionLanguageEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the SurveyResult entity</summary>
		public EntityQuery<SurveyResultEntity> SurveyResult
		{
			get { return Create<SurveyResultEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the Tag entity</summary>
		public EntityQuery<TagEntity> Tag
		{
			get { return Create<TagEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the TaxTariff entity</summary>
		public EntityQuery<TaxTariffEntity> TaxTariff
		{
			get { return Create<TaxTariffEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the Terminal entity</summary>
		public EntityQuery<TerminalEntity> Terminal
		{
			get { return Create<TerminalEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the TerminalConfiguration entity</summary>
		public EntityQuery<TerminalConfigurationEntity> TerminalConfiguration
		{
			get { return Create<TerminalConfigurationEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the TerminalLog entity</summary>
		public EntityQuery<TerminalLogEntity> TerminalLog
		{
			get { return Create<TerminalLogEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the TerminalLogFile entity</summary>
		public EntityQuery<TerminalLogFileEntity> TerminalLogFile
		{
			get { return Create<TerminalLogFileEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the TerminalMessageTemplateCategory entity</summary>
		public EntityQuery<TerminalMessageTemplateCategoryEntity> TerminalMessageTemplateCategory
		{
			get { return Create<TerminalMessageTemplateCategoryEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the TerminalState entity</summary>
		public EntityQuery<TerminalStateEntity> TerminalState
		{
			get { return Create<TerminalStateEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the Timestamp entity</summary>
		public EntityQuery<TimestampEntity> Timestamp
		{
			get { return Create<TimestampEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the TimeZone entity</summary>
		public EntityQuery<TimeZoneEntity> TimeZone
		{
			get { return Create<TimeZoneEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the Translation entity</summary>
		public EntityQuery<TranslationEntity> Translation
		{
			get { return Create<TranslationEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the UIElement entity</summary>
		public EntityQuery<UIElementEntity> UIElement
		{
			get { return Create<UIElementEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the UIElementCustom entity</summary>
		public EntityQuery<UIElementCustomEntity> UIElementCustom
		{
			get { return Create<UIElementCustomEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the UIElementSubPanel entity</summary>
		public EntityQuery<UIElementSubPanelEntity> UIElementSubPanel
		{
			get { return Create<UIElementSubPanelEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the UIElementSubPanelCustom entity</summary>
		public EntityQuery<UIElementSubPanelCustomEntity> UIElementSubPanelCustom
		{
			get { return Create<UIElementSubPanelCustomEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the UIElementSubPanelUIElement entity</summary>
		public EntityQuery<UIElementSubPanelUIElementEntity> UIElementSubPanelUIElement
		{
			get { return Create<UIElementSubPanelUIElementEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the UIFooterItem entity</summary>
		public EntityQuery<UIFooterItemEntity> UIFooterItem
		{
			get { return Create<UIFooterItemEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the UIFooterItemLanguage entity</summary>
		public EntityQuery<UIFooterItemLanguageEntity> UIFooterItemLanguage
		{
			get { return Create<UIFooterItemLanguageEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the UIMode entity</summary>
		public EntityQuery<UIModeEntity> UIMode
		{
			get { return Create<UIModeEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the UISchedule entity</summary>
		public EntityQuery<UIScheduleEntity> UISchedule
		{
			get { return Create<UIScheduleEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the UIScheduleItem entity</summary>
		public EntityQuery<UIScheduleItemEntity> UIScheduleItem
		{
			get { return Create<UIScheduleItemEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the UIScheduleItemOccurrence entity</summary>
		public EntityQuery<UIScheduleItemOccurrenceEntity> UIScheduleItemOccurrence
		{
			get { return Create<UIScheduleItemOccurrenceEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the UITab entity</summary>
		public EntityQuery<UITabEntity> UITab
		{
			get { return Create<UITabEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the UITabLanguage entity</summary>
		public EntityQuery<UITabLanguageEntity> UITabLanguage
		{
			get { return Create<UITabLanguageEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the UITheme entity</summary>
		public EntityQuery<UIThemeEntity> UITheme
		{
			get { return Create<UIThemeEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the UIThemeColor entity</summary>
		public EntityQuery<UIThemeColorEntity> UIThemeColor
		{
			get { return Create<UIThemeColorEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the UIThemeTextSize entity</summary>
		public EntityQuery<UIThemeTextSizeEntity> UIThemeTextSize
		{
			get { return Create<UIThemeTextSizeEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the UIWidget entity</summary>
		public EntityQuery<UIWidgetEntity> UIWidget
		{
			get { return Create<UIWidgetEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the UIWidgetAvailability entity</summary>
		public EntityQuery<UIWidgetAvailabilityEntity> UIWidgetAvailability
		{
			get { return Create<UIWidgetAvailabilityEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the UIWidgetLanguage entity</summary>
		public EntityQuery<UIWidgetLanguageEntity> UIWidgetLanguage
		{
			get { return Create<UIWidgetLanguageEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the UIWidgetTimer entity</summary>
		public EntityQuery<UIWidgetTimerEntity> UIWidgetTimer
		{
			get { return Create<UIWidgetTimerEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the User entity</summary>
		public EntityQuery<UserEntity> User
		{
			get { return Create<UserEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the UserBrand entity</summary>
		public EntityQuery<UserBrandEntity> UserBrand
		{
			get { return Create<UserBrandEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the UserLogon entity</summary>
		public EntityQuery<UserLogonEntity> UserLogon
		{
			get { return Create<UserLogonEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the UserRole entity</summary>
		public EntityQuery<UserRoleEntity> UserRole
		{
			get { return Create<UserRoleEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the Vattariff entity</summary>
		public EntityQuery<VattariffEntity> Vattariff
		{
			get { return Create<VattariffEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the Vendor entity</summary>
		public EntityQuery<VendorEntity> Vendor
		{
			get { return Create<VendorEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the VenueCategory entity</summary>
		public EntityQuery<VenueCategoryEntity> VenueCategory
		{
			get { return Create<VenueCategoryEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the VenueCategoryLanguage entity</summary>
		public EntityQuery<VenueCategoryLanguageEntity> VenueCategoryLanguage
		{
			get { return Create<VenueCategoryLanguageEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the View entity</summary>
		public EntityQuery<ViewEntity> View
		{
			get { return Create<ViewEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the ViewCustom entity</summary>
		public EntityQuery<ViewCustomEntity> ViewCustom
		{
			get { return Create<ViewCustomEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the ViewItem entity</summary>
		public EntityQuery<ViewItemEntity> ViewItem
		{
			get { return Create<ViewItemEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the ViewItemCustom entity</summary>
		public EntityQuery<ViewItemCustomEntity> ViewItemCustom
		{
			get { return Create<ViewItemCustomEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the Weather entity</summary>
		public EntityQuery<WeatherEntity> Weather
		{
			get { return Create<WeatherEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the WifiConfiguration entity</summary>
		public EntityQuery<WifiConfigurationEntity> WifiConfiguration
		{
			get { return Create<WifiConfigurationEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the Action entity</summary>
		public EntityQuery<ActionEntity> Action
		{
			get { return Create<ActionEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the ApplicationConfiguration entity</summary>
		public EntityQuery<ApplicationConfigurationEntity> ApplicationConfiguration
		{
			get { return Create<ApplicationConfigurationEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the CarouselItem entity</summary>
		public EntityQuery<CarouselItemEntity> CarouselItem
		{
			get { return Create<CarouselItemEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the FeatureFlag entity</summary>
		public EntityQuery<FeatureFlagEntity> FeatureFlag
		{
			get { return Create<FeatureFlagEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the LandingPage entity</summary>
		public EntityQuery<LandingPageEntity> LandingPage
		{
			get { return Create<LandingPageEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the LandingPageWidget entity</summary>
		public EntityQuery<LandingPageWidgetEntity> LandingPageWidget
		{
			get { return Create<LandingPageWidgetEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the NavigationMenu entity</summary>
		public EntityQuery<NavigationMenuEntity> NavigationMenu
		{
			get { return Create<NavigationMenuEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the NavigationMenuItem entity</summary>
		public EntityQuery<NavigationMenuItemEntity> NavigationMenuItem
		{
			get { return Create<NavigationMenuItemEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the NavigationMenuWidget entity</summary>
		public EntityQuery<NavigationMenuWidgetEntity> NavigationMenuWidget
		{
			get { return Create<NavigationMenuWidgetEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the Theme entity</summary>
		public EntityQuery<ThemeEntity> Theme
		{
			get { return Create<ThemeEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the Widget entity</summary>
		public EntityQuery<WidgetEntity> Widget
		{
			get { return Create<WidgetEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the WidgetActionBanner entity</summary>
		public EntityQuery<WidgetActionBannerEntity> WidgetActionBanner
		{
			get { return Create<WidgetActionBannerEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the WidgetActionButton entity</summary>
		public EntityQuery<WidgetActionButtonEntity> WidgetActionButton
		{
			get { return Create<WidgetActionButtonEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the WidgetCarousel entity</summary>
		public EntityQuery<WidgetCarouselEntity> WidgetCarousel
		{
			get { return Create<WidgetCarouselEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the WidgetGroup entity</summary>
		public EntityQuery<WidgetGroupEntity> WidgetGroup
		{
			get { return Create<WidgetGroupEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the WidgetGroupWidget entity</summary>
		public EntityQuery<WidgetGroupWidgetEntity> WidgetGroupWidget
		{
			get { return Create<WidgetGroupWidgetEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the WidgetHero entity</summary>
		public EntityQuery<WidgetHeroEntity> WidgetHero
		{
			get { return Create<WidgetHeroEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the WidgetLanguageSwitcher entity</summary>
		public EntityQuery<WidgetLanguageSwitcherEntity> WidgetLanguageSwitcher
		{
			get { return Create<WidgetLanguageSwitcherEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the WidgetMarkdown entity</summary>
		public EntityQuery<WidgetMarkdownEntity> WidgetMarkdown
		{
			get { return Create<WidgetMarkdownEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the WidgetOpeningTime entity</summary>
		public EntityQuery<WidgetOpeningTimeEntity> WidgetOpeningTime
		{
			get { return Create<WidgetOpeningTimeEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the WidgetPageTitle entity</summary>
		public EntityQuery<WidgetPageTitleEntity> WidgetPageTitle
		{
			get { return Create<WidgetPageTitleEntity>(); }
		}

		/// <summary>Creates and returns a new EntityQuery for the WidgetWaitTime entity</summary>
		public EntityQuery<WidgetWaitTimeEntity> WidgetWaitTime
		{
			get { return Create<WidgetWaitTimeEntity>(); }
		}


 
	}
}