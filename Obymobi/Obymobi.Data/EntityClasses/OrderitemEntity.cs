﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    using System.Linq;
    using Obymobi.Enums;
    using System.Xml.Serialization;
    // __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'Orderitem'. This class is used for Business Logic or for framework extension code.</summary>
	[Serializable]
	public partial class OrderitemEntity : OrderitemEntityBase
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
        , ICompanyRelatedChildEntity, ITimestampEntity
    // __LLBLGENPRO_USER_CODE_REGION_END	
	{
		/// <summary>CTor</summary>
		public OrderitemEntity():base()
		{
		}

		/// <summary>CTor</summary>
		/// <param name="orderitemId">PK value for Orderitem which data should be fetched into this Orderitem object</param>
		public OrderitemEntity(System.Int32 orderitemId):
			base(orderitemId)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="orderitemId">PK value for Orderitem which data should be fetched into this Orderitem object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public OrderitemEntity(System.Int32 orderitemId, IPrefetchPath prefetchPathToUse):
			base(orderitemId, prefetchPathToUse)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="orderitemId">PK value for Orderitem which data should be fetched into this Orderitem object</param>
		/// <param name="validator">The custom validator object for this OrderitemEntity</param>
		public OrderitemEntity(System.Int32 orderitemId, IValidator validator):
			base(orderitemId, validator)
		{
		}
		
		/// <summary>CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected OrderitemEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
            // __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
            // __LLBLGENPRO_USER_CODE_REGION_END
		}

		#region Custom Entity code
		
        // __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode

        #region Properties

        [XmlIgnore]
        public CommonEntityBase Parent
        {
            get { return this.OrderEntity; }
        }

        private bool overrideChangeProtection = false;
        /// <summary>
        /// When set to true it's possible to change an Orderitem when it's already saved
        /// 
        /// </summary>
        public bool OverrideChangeProtection
        {
            get
            {
                return this.overrideChangeProtection;
            }
            set
            {
                this.overrideChangeProtection = value;
            }
        }

        private bool validatedByOrderProcessingHelper = false;

        /// <summary>
        /// Gets or sets the VerifiedByOrderProcessingHelper
        /// </summary>
        public bool ValidatedByOrderProcessingHelper
        {
            get
            {
                return this.validatedByOrderProcessingHelper;
            }
            set
            {
                this.validatedByOrderProcessingHelper = value;
            }
        }

        /// <summary>
        /// Gets the status of its parent order
        /// </summary>
        public OrderStatus OrderStatus
        {
            get
            {
                return this.OrderEntity.StatusAsEnum;
            }
        }

        #endregion

        #region Calculated Properties

        /// <summary>
        /// Sum of ProductPriceIn and AlterationsPriceIn
        /// </summary>
        public decimal PricePerPiece
        {
            get
            {
                return this.ProductPriceIn + this.AlterationsPriceIn;
            }
        }

        /// <summary>
        /// Sum of all AlterationoptionPriceIn's 
        /// </summary>
        public decimal AlterationsPriceIn
        {
            get
            {
                return this.OrderitemAlterationitemCollection.Sum(oa => oa.AlterationoptionPriceIn);

            }
        }

        /// <summary>
        /// Total price for the Orderitem (= Quantity * (ProductPriceIn + AlterationPriceIn + DiscountsPriceIn))
        /// </summary>
        public decimal PriceIn
        {
            get
            {
                return this.Quantity * this.PricePerPiece;
            }
        }

        #endregion

        // __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included Code

		#endregion
	}
}
