﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'ProductTag'. This class is used for Business Logic or for framework extension code.</summary>
	[Serializable]
	public partial class ProductTagEntity : ProductTagEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		, INullableCompanyRelatedChildEntity, ITaggableEntity
	    // __LLBLGENPRO_USER_CODE_REGION_END	
	{
		/// <summary>CTor</summary>
		public ProductTagEntity():base()
		{
		}

		/// <summary>CTor</summary>
		/// <param name="productTagId">PK value for ProductTag which data should be fetched into this ProductTag object</param>
		public ProductTagEntity(System.Int32 productTagId):
			base(productTagId)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="productTagId">PK value for ProductTag which data should be fetched into this ProductTag object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public ProductTagEntity(System.Int32 productTagId, IPrefetchPath prefetchPathToUse):
			base(productTagId, prefetchPathToUse)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="productTagId">PK value for ProductTag which data should be fetched into this ProductTag object</param>
		/// <param name="validator">The custom validator object for this ProductTagEntity</param>
		public ProductTagEntity(System.Int32 productTagId, IValidator validator):
			base(productTagId, validator)
		{
		}
		
		/// <summary>CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected ProductTagEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}

		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode

        public CommonEntityBase Parent => this.ProductEntity;

		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included Code

		#endregion
	}
}
