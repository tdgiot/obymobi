﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    using Obymobi.Interfaces;
    using Obymobi.Enums;
    using System.Xml.Serialization;
    using Obymobi.Logic.Model;
    using Dionysos;

    // __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'Terminal'. This class is used for Business Logic or for framework extension code.</summary>
	[Serializable]
	public partial class TerminalEntity : TerminalEntityBase
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
        , INocActivityEntity, ICompanyRelatedEntity, ITimestampEntity
    // __LLBLGENPRO_USER_CODE_REGION_END	
	{
		/// <summary>CTor</summary>
		public TerminalEntity():base()
		{
		}

		/// <summary>CTor</summary>
		/// <param name="terminalId">PK value for Terminal which data should be fetched into this Terminal object</param>
		public TerminalEntity(System.Int32 terminalId):
			base(terminalId)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="terminalId">PK value for Terminal which data should be fetched into this Terminal object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public TerminalEntity(System.Int32 terminalId, IPrefetchPath prefetchPathToUse):
			base(terminalId, prefetchPathToUse)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="terminalId">PK value for Terminal which data should be fetched into this Terminal object</param>
		/// <param name="validator">The custom validator object for this TerminalEntity</param>
		public TerminalEntity(System.Int32 terminalId, IValidator validator):
			base(terminalId, validator)
		{
		}
		
		/// <summary>CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected TerminalEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
            // __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
            // __LLBLGENPRO_USER_CODE_REGION_END
		}

		#region Custom Entity code
		
        // __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode

        #region Properties

        [XmlIgnore]
        public bool WasNewBeforeSave = false;

        /// <summary>
        /// If set to true the Validator won't create a TerminalLog when you changed
        /// device id. Should be used when you write a more specific TerminalLog manually  
        /// when changing the device id.
        /// </summary>
        [XmlIgnore]
        public bool ValidatorOverwriteDontLogDeviceChange = false;

	    public bool ValidatorOverwriteSendMasterTabChangedNetmessage = false;

        // GK-ROUTING - Redo after HandlingMethod has been refactored
        [XmlIgnore]
        public int TerminalType
        {
            get
            {
                return this.HandlingMethod;
            }
            set
            {
                this.HandlingMethod = value;
            }
        }

        [XmlIgnore]
        public POSConnectorType POSConnectorTypeEnum
        {
            get
            {
                return this.POSConnectorType.ToEnum<POSConnectorType>();
            }
            set
            {
                this.POSConnectorType = (int)value;
            }
        }

        [XmlIgnore]
        public PMSConnectorType PMSConnectorTypeEnum
        {
            get
            {
                return this.PMSConnectorType.ToEnum<PMSConnectorType>();
            }
            set
            {
                this.PMSConnectorType = (int)value;
            }
        }

        [XmlIgnore]
        public TerminalType TerminalTypeEnum
        {
            get
            {
                return this.TerminalType.ToEnum<TerminalType>();
            }
            set
            {
                this.TerminalType = (int)value;
            }
        }

        [XmlIgnore]
        public TerminalNotificationMethod NotificationForNewOrderEnum
        {
            get
            {
                return this.NotificationForNewOrder.ToEnum<TerminalNotificationMethod>();
            }
            set
            {
                this.NotificationForNewOrder = (int)value;
            }
        }

        [XmlIgnore]
        public TerminalNotificationMethod NotificationForOverdueOrderEnum
        {
            get
            {
                return this.NotificationForOverdueOrder.ToEnum<TerminalNotificationMethod>();
            }
            set
            {
                this.NotificationForOverdueOrder = (int)value;
            }
        }

        /// <summary>
        /// Gets a flag which indicates whether this terminal is online
        /// </summary>
        [XmlIgnore]
        public bool IsOnline
        {
            get
            {
                return this.LastActivityUTC.HasValue && this.LastActivityUTC.Value > DateTime.UtcNow.AddSeconds(-90);
            }
        }

        /// <summary>
        /// Gets the last activity (UTC) for this terminal
        /// </summary>
        [XmlIgnore]
        public DateTime? LastActivityUTC
        {
            get
            {
                if (this.DeviceId.HasValue)
                    return this.DeviceEntity.LastRequestUTC;
                return null;
            }
        }

        #endregion

        #region Status properties

        /// <summary>
        /// Gets a value indicating whether this instance has system messages deliverypoint.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if this instance has system messages deliverypoint; otherwise, <c>false</c>.
        /// </value>
        [XmlIgnore]
        public bool HasSystemMessagesDeliverypoint
        {
            get
            {
                return this.SystemMessagesDeliverypointId.HasValue;
            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance has alt system messages deliverypoint.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if this instance has alt system messages deliverypoint; otherwise, <c>false</c>.
        /// </value>
        [XmlIgnore]
        public bool HasAltSystemMessagesDeliverypoint
        {
            get
            {
                return this.AltSystemMessagesDeliverypointId.HasValue;
            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance has unlock deliverypoint product.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if this instance has unlock deliverypoint product; otherwise, <c>false</c>.
        /// </value>
        [XmlIgnore]
        public bool HasUnlockDeliverypointProduct
        {
            get
            {
                return this.UnlockDeliverypointProductId.HasValue;
            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance has battery low product.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if this instance has battery low product; otherwise, <c>false</c>.
        /// </value>
        [XmlIgnore]
        public bool HasBatteryLowProduct
        {
            get
            {
                return this.BatteryLowProductId.HasValue;
            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance has order failed product.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if this instance has order failed product; otherwise, <c>false</c>.
        /// </value>
        [XmlIgnore]
        public bool HasOrderFailedProduct
        {
            get
            {
                return this.OrderFailedProductId.HasValue;
            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance has client disconnected product.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if this instance has client disconnected product; otherwise, <c>false</c>.
        /// </value>
        [XmlIgnore]
        public bool HasClientDisconnectedProduct
        {
            get
            {
                return this.ClientDisconnectedProductId.HasValue;
            }
        }

        [DataGridViewColumnVisible]
        [XmlIgnore]
        public string LastRequestServerTime
        {
            get
            {
                return this.DeviceEntity.LastRequestUTC.GetValueOrDefault().UtcToLocalTime().ToString("g");
            }
        }

        #endregion

        // __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included Code

		#endregion
	}
}
