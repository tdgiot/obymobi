﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    using Obymobi.Logic.Cms;
    using System.Xml.Serialization;
    using Obymobi.Enums;
    using Obymobi.Data.HelperClasses;
    using System.Collections.Generic;
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'Site'. This class is used for Business Logic or for framework extension code.</summary>
	[Serializable]
	public partial class SiteEntity : SiteEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
        , ISite, INullableCompanyRelatedEntity, IMediaContainingEntity, ICustomTextContainingEntity, ITimestampEntity
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		/// <summary>CTor</summary>
		public SiteEntity():base()
		{
		}

		/// <summary>CTor</summary>
		/// <param name="siteId">PK value for Site which data should be fetched into this Site object</param>
		public SiteEntity(System.Int32 siteId):
			base(siteId)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="siteId">PK value for Site which data should be fetched into this Site object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public SiteEntity(System.Int32 siteId, IPrefetchPath prefetchPathToUse):
			base(siteId, prefetchPathToUse)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="siteId">PK value for Site which data should be fetched into this Site object</param>
		/// <param name="validator">The custom validator object for this SiteEntity</param>
		public SiteEntity(System.Int32 siteId, IValidator validator):
			base(siteId, validator)
		{
		}
		
		/// <summary>CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected SiteEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}

		#region Custom Entity code
		
        // __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode

        #region Generic handling for Sites/SiteTemplates

        public int PrimaryId
        {
            get { return this.SiteId; }
            set { this.SiteId = value; }
        }
       
        [XmlIgnore]
        public SiteType SiteTypeAsEnum
        {
            get
            {
                return this.SiteType.ToEnum<SiteType>();
            }
            set
            {
                this.SiteType = (int)value;
            }
        }

	    public List<IPage> IPageCollection
	    {
	        get
	        {
	            var toReturn = new List<IPage>();
	            toReturn.AddRange(base.PageCollection);
	            return toReturn;
	        }
	    }

        #endregion

        #region Prefetches

        public bool ValidatorFlagRequeueMedia = false;

        public static IPrefetchPathElement GetPrefetchForSiteMedia(DeviceType deviceType)
        {
            // Prefetch Site > Media
            var prefetchSiteMedia = SiteEntity.PrefetchPathMediaCollection;

            // Prefetch Site > Media > MediaRatioTypeMedia
            var prefetchSiteMediaMediaRatioTypeMedia = prefetchSiteMedia.SubPath.Add(MediaEntity.PrefetchPathMediaRatioTypeMediaCollection);
            prefetchSiteMediaMediaRatioTypeMedia.Filter = new PredicateExpression(MediaRatioTypeMediaFields.MediaType == MediaRatioTypes.GetMediaTypeArrayForDeviceTypes(deviceType));

            return prefetchSiteMedia;
        }        

        public static IPrefetchPathElement GetPrefetchForSitePages(DeviceType deviceType, string cultureCode)
        {
            MediaType[] mediaTypes = MediaRatioTypes.GetMediaTypeArrayForDeviceTypes(deviceType);

            // Prefetch Site > Page
            IPrefetchPathElement prefetchSitePage = SiteEntityBase.PrefetchPathPageCollection;

            // Prefetch Site > Page > Media
            IPrefetchPathElement prefetchSitePageMedia = prefetchSitePage.SubPath.Add(PageEntityBase.PrefetchPathMediaCollection);

            // Prefetch Site > Page > Media > MediaRatioTypeMedia
            IPrefetchPathElement prefetchSitePageMediaMediaRatioTypeMedia = prefetchSitePageMedia.SubPath.Add(MediaEntityBase.PrefetchPathMediaRatioTypeMediaCollection);
            prefetchSitePageMediaMediaRatioTypeMedia.Filter = new PredicateExpression(MediaRatioTypeMediaFields.MediaType == mediaTypes);

            // Prefetch Site > Page > CustomTextCollection
            IPrefetchPathElement prefetchSitePageCustomTextCollection = PageEntityBase.PrefetchPathCustomTextCollection;
            prefetchSitePageCustomTextCollection.Filter = new PredicateExpression(CustomTextFields.CultureCode == cultureCode);
            prefetchSitePage.SubPath.Add(prefetchSitePageCustomTextCollection);

            #region Page Elements

            PredicateExpression pageElementFilter = new PredicateExpression();
            pageElementFilter.Add(PageElementFields.CultureCode == DBNull.Value);
            pageElementFilter.AddWithOr(PageElementFields.CultureCode == cultureCode);

            // Prefetch Site > Page > PageElement
            IPrefetchPathElement prefetchSitePagePageElement = prefetchSitePage.SubPath.Add(PageEntityBase.PrefetchPathPageElementCollection);
            prefetchSitePagePageElement.Filter = pageElementFilter;

            // Prefetch Site > Page > PageElement > Media
            IPrefetchPathElement prefetchSitePagePageElementMedia = prefetchSitePagePageElement.SubPath.Add(PageElementEntityBase.PrefetchPathMediaCollection);

            // Prefetch Site > Page > PageElement > Media > MediaRatioTypeMedia
            IPrefetchPathElement prefetchPagePageElementMediaMediaRatioTypeMedia = prefetchSitePagePageElementMedia.SubPath.Add(MediaEntityBase.PrefetchPathMediaRatioTypeMediaCollection);
            prefetchPagePageElementMediaMediaRatioTypeMedia.Filter = new PredicateExpression(MediaRatioTypeMediaFields.MediaType == mediaTypes);

            #endregion

            #region Attachments

            // Prefetch Site > Page > Attachment
            IPrefetchPathElement prefetchSitePageAttachment = prefetchSitePage.SubPath.Add(PageEntityBase.PrefetchPathAttachmentCollection);

            // Prefetch Site > Page > Attachment > CustomTextCollection
            IPrefetchPathElement prefetchSitePageAttachmentCustomTextCollection = AttachmentEntityBase.PrefetchPathCustomTextCollection;
            prefetchSitePageAttachmentCustomTextCollection.Filter = new PredicateExpression(CustomTextFields.CultureCode == cultureCode);
            prefetchSitePageAttachment.SubPath.Add(prefetchSitePageAttachmentCustomTextCollection);

            // Prefetch Site > Page > Attachment > Media
            IPrefetchPathElement prefetchSitePageAttachmentMedia = prefetchSitePageAttachment.SubPath.Add(AttachmentEntityBase.PrefetchPathMediaCollection);

            // Prefetch Site > Page > Attachment > Media > MediaRatioTypeMedia
            IPrefetchPathElement prefetchPageAttachmentMediaMediaRatioTypeMedia = prefetchSitePageAttachmentMedia.SubPath.Add(MediaEntityBase.PrefetchPathMediaRatioTypeMediaCollection);
            prefetchPageAttachmentMediaMediaRatioTypeMedia.Filter = new PredicateExpression(MediaRatioTypeMediaFields.MediaType == mediaTypes);

            #endregion

            #region PageTemplates

            // Site > Page > PageTemplate
            IPrefetchPathElement prefetchSitePagePageTemplate = prefetchSitePage.SubPath.Add(PageEntityBase.PrefetchPathPageTemplateEntity);

            // Site > Page > PageTemplate > CustomTextCollection
            IPrefetchPathElement prefetchSitePagePageTemplateCustomTextCollection = PageTemplateEntityBase.PrefetchPathCustomTextCollection;
            prefetchSitePagePageTemplateCustomTextCollection.Filter = new PredicateExpression(CustomTextFields.CultureCode == cultureCode);
            prefetchSitePagePageTemplate.SubPath.Add(prefetchSitePagePageTemplateCustomTextCollection);

            // Site > Page > PageTemplate > Media
            IPrefetchPathElement prefetchSitePagePageTemplateMedia = prefetchSitePagePageTemplate.SubPath.Add(PageTemplateEntityBase.PrefetchPathMediaCollection);

            // Site > Page > PageTemplate > Media > MediaRatioTypeMedia 
            IPrefetchPathElement prefetchSitePagePageTemplateMediaMediaRatioTypeMedia = prefetchSitePagePageTemplateMedia.SubPath.Add(MediaEntityBase.PrefetchPathMediaRatioTypeMediaCollection);
            prefetchSitePagePageTemplateMediaMediaRatioTypeMedia.Filter = new PredicateExpression(MediaRatioTypeMediaFields.MediaType == mediaTypes);

            // Site > Page > PageTemplate > PageTemplateElement
            PredicateExpression pageTemplateElementFilter = new PredicateExpression();
            pageTemplateElementFilter.Add(PageTemplateElementFields.CultureCode == DBNull.Value);
            pageTemplateElementFilter.AddWithOr(PageTemplateElementFields.CultureCode == cultureCode);

            IPrefetchPathElement prefetchSitePagePageTemplatePageTemplateElement = prefetchSitePagePageTemplate.SubPath.Add(PageTemplateEntityBase.PrefetchPathPageTemplateElementCollection);
            prefetchSitePagePageTemplatePageTemplateElement.Filter = pageTemplateElementFilter;

            // Site > Page > PageTemplate > PageTemplateElement > Media
            IPrefetchPathElement prefetchSitePagePageTemplatePageTemplateElementMedia = prefetchSitePagePageTemplatePageTemplateElement.SubPath.Add(PageTemplateElementEntityBase.PrefetchPathMediaCollection);

            // Site > Page > PageTemplate > PageTemplateElement > Media > MediaRatioTypeMedia
            IPrefetchPathElement prefetchSitePagePageTemplatePageTemplateElementMediaMediaRatioTypeMedia = prefetchSitePagePageTemplatePageTemplateElementMedia.SubPath.Add(MediaEntityBase.PrefetchPathMediaRatioTypeMediaCollection);
            prefetchSitePagePageTemplatePageTemplateElementMediaMediaRatioTypeMedia.Filter = new PredicateExpression(MediaRatioTypeMediaFields.MediaType == mediaTypes);

            // Site > Page > PageTemplate > Attachment
            IPrefetchPathElement prefetchSitePagePageTemplateAttachment = prefetchSitePagePageTemplate.SubPath.Add(PageTemplateEntityBase.PrefetchPathAttachmentCollection);

            // Site > Page > PageTemplate > Attachment > Media
            IPrefetchPathElement prefetchSitePagePageTemplateAttachmentMedia = prefetchSitePagePageTemplateAttachment.SubPath.Add(AttachmentEntityBase.PrefetchPathMediaCollection);

            // Site > Page > PageTemplate > Attachment > Media > MediaRatioTypeMedia
            IPrefetchPathElement prefetchSitePagePageTemplateAttachmentMediaMediaRatioTypeMedia = prefetchSitePagePageTemplateAttachmentMedia.SubPath.Add(MediaEntityBase.PrefetchPathMediaRatioTypeMediaCollection);
            prefetchSitePagePageTemplateAttachmentMediaMediaRatioTypeMedia.Filter = new PredicateExpression(MediaRatioTypeMediaFields.MediaType == mediaTypes);

            #endregion

            return prefetchSitePage;
        }

        #endregion        

		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included Code

		#endregion
	}
}
