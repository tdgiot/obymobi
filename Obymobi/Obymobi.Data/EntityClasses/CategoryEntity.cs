﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    using System.Linq;
    using Dionysos;
    using Obymobi.Enums;
    using System.Collections.Generic;
    using System.Xml.Serialization;
    using Obymobi.Logic.Cms;
    using Obymobi.Interfaces;
    using Obymobi.Data.Comparers;

    // __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'Category'. This class is used for Business Logic or for framework extension code.</summary>
	[Serializable]
	public partial class CategoryEntity : CategoryEntityBase
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
    , 
    ICompanyRelatedEntity, 
    IMediaContainingEntity, 
    IMenuItem, 
    ICustomTextContainingEntity, 
    ITimestampEntity,
    ITagEntity
    // __LLBLGENPRO_USER_CODE_REGION_END	
	{
		/// <summary>CTor</summary>
		public CategoryEntity():base()
		{
		}

		/// <summary>CTor</summary>
		/// <param name="categoryId">PK value for Category which data should be fetched into this Category object</param>
		public CategoryEntity(System.Int32 categoryId):
			base(categoryId)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="categoryId">PK value for Category which data should be fetched into this Category object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public CategoryEntity(System.Int32 categoryId, IPrefetchPath prefetchPathToUse):
			base(categoryId, prefetchPathToUse)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="categoryId">PK value for Category which data should be fetched into this Category object</param>
		/// <param name="validator">The custom validator object for this CategoryEntity</param>
		public CategoryEntity(System.Int32 categoryId, IValidator validator):
			base(categoryId, validator)
		{
		}
		
		/// <summary>CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected CategoryEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
            // __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
            // __LLBLGENPRO_USER_CODE_REGION_END
		}

		#region Custom Entity code
		
        // __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode

        #region Properties

        [XmlIgnore]
        public string FullCategoryName
        {
            get
            {
                return this.GetFullCategoryName(this);
            }
        }

        [XmlIgnore]
	    public string FullCategoryMenuName
	    {
	        get
	        {
                return this.MenuEntity.Name + "\\" + this.GetFullCategoryName(this);
	            
	        }
	    }

        [XmlIgnore]
        public string MenuCategoryName
        {
            get { return string.Format("{0} - {1}", this.MenuEntity.Name, this.Name); }
        }

        /// <summary>
        /// Determine if a least one Media entity contains a MediaRatioEntity related to MediaType CategoryButton
        /// </summary>
        [XmlIgnore]
        public bool HasCategoryButton
        {
            get
            {
                bool toReturn = false;
                if (this.GenericcategoryId.HasValue && this.GenericcategoryEntity.HasCategoryButton)
                {
                    toReturn = true;
                }
                else
                {
                    foreach (var media in this.MediaCollection)
                    {
                        if (media.MediaRatioTypeMediaCollection.Any(mr => (mr.MediaType.HasValue && mr.MediaType.Value == (int)MediaType.CategoryButton)) ||
                            media.MediaRatioTypeMediaCollection.Any(mr => (mr.MediaType.HasValue && mr.MediaType.Value == (int)MediaType.GenericcategoryButton)))
                        {
                            toReturn = true;
                            break;
                        }
                    }
                }
                return toReturn;
            }
        }

        /// <summary>
        /// Determine if this category has a schedule set
        /// </summary>
        [XmlIgnore]
        public bool HasSchedule
        {
            get
            { 
                return (this.OrderHourCollection.Count > 0);
            }
        }

        /// <summary>
        /// Determine if this category has an alteration set
        /// </summary>
        [XmlIgnore]
        public bool HasAlteration
        {
            get
            { 
                return (this.CategoryAlterationCollection.Count > 0);
            }
        }

        /// <summary>
        /// Determine if a least one category has an alteration
        /// </summary>
        [XmlIgnore]
        public bool HasCategoryWithAlteration
        {
            get
            {
                bool hasCategoryWithAlteration = false;

                foreach (CategoryEntity categoryEntity in this.ChildCategoryCollection)
                {
                    if (categoryEntity.HasSuggestions)
                    {
                        hasCategoryWithAlteration = true;
                        break;
                    }
                }

                return hasCategoryWithAlteration;
            }
        }

        /// <summary>
        /// Determine if a least one product has an alteration
        /// </summary>
        [XmlIgnore]
        public bool HasProductWithAlteration
        {
            get
            {
                bool hasProductWithAlteration = false;

                foreach (ProductEntity product in this.ProductCollectionViaProductCategory)
                { 
                    if (product.HasAlterations)
                    {
                        hasProductWithAlteration = true;
                        break;
                    }
                }

                return hasProductWithAlteration;
            }
        }

        /// <summary>
        /// Determine if a least one product has an suggestion
        /// </summary>
        [XmlIgnore]
        public bool HasProductWithSuggestion
        {
            get
            {
                bool hasProductWithSuggestion = false;

                foreach (ProductEntity product in this.ProductCollectionViaProductCategory)
                {
                    if (product.HasSuggestions)
                    {
                        hasProductWithSuggestion = true;
                        break;
                    }
                }

                return hasProductWithSuggestion;
            }
        }

        /// <summary>
        /// Gets a flag indicating whether this category contains products.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if this instance has products; otherwise, <c>false</c>.
        /// </value>
        [XmlIgnore]
        public bool HasProducts
        {
            get
            {
                return (this.ProductCategoryCollection.Count > 0);
            }
        }

        /// <summary>
        /// Gets a flag indicating whether this category contains suggestions
        /// </summary>
        /// <value>
        /// 	<c>true</c> if this instance has suggestions; otherwise, <c>false</c>.
        /// </value>
        [XmlIgnore]
        public bool HasSuggestions
        {
            get
            {
                return (this.CategorySuggestionCollection != null && this.CategorySuggestionCollection.Count > 0);
            }
        }

        /// <summary>
        /// Gets a flag indicating whether this category contains complete products (products with images and description).
        /// </summary>
        /// <value>
        /// 	<c>true</c> if this instance has complete products; otherwise, <c>false</c>.
        /// </value>
        [XmlIgnore]
        public bool HasCompleteProducts
        {
            get 
            {
                bool completeProducts = true;

                foreach (ProductEntity productEntity in this.ProductCollectionViaProductCategory)
                { 
                    if (!productEntity.IsComplete)
                    {
                        completeProducts = false;
                        break;
                    }
                }

                return completeProducts;
            }
        }

        [XmlIgnore]
        public bool HasCompleteCategories
        {
            get
            {
                bool completeCategories = true;

                foreach (CategoryEntity categoryEntity in this.ChildCategoryCollection)
                {
                    if (!categoryEntity.IsComplete)
                    {
                        completeCategories = false;
                        break;
                    }
                }

                return completeCategories;
            }
        }

        [XmlIgnore]
        public bool HasOneCompleteCategoryOrProduct
        {
            get 
            {
                bool oneCompleted = false;

                if (this.HasProducts)
                {
                    foreach (ProductEntity product in this.ProductCollectionViaProductCategory)
                    {
                        if (product.IsComplete)
                        {
                            oneCompleted = true;
                            break;
                        }
                    }
                }
                else if (this.ChildCategoryCollection.Count > 0)
                {
                    foreach (CategoryEntity category in this.ChildCategoryCollection)
                    {
                        if (category.HasOneCompleteCategoryOrProduct)
                        {
                            oneCompleted = true;
                            break;
                        }                        
                    }
                }
                
                return oneCompleted;
            }
        }

        [XmlIgnore]
        public bool IsComplete
        {
            get
            {
                bool isComplete = false;
                bool hasProducts = true;

                if (this.CompanyEntity.SystemType == SystemType.Crave)
                {
                    // Crave system type

                    if (!this.HasProducts || !this.HasCompleteProducts)
                    {
                        // Category does not have products or categories
                        hasProducts = false;
                    }
                    else if (this.ChildCategoryCollection.Count == 0 || !this.HasCompleteCategories)
                    {
                        // Category doesn't have any complete categories or complete products
                        if (hasProducts)
                            isComplete = true;
                    }
                    else
                    {
                        isComplete = true;
                    }
                }
                else
                {
                    // Otoucho system type

                    if (!this.HasProducts || !this.HasCompleteProducts)
                    {
                        // Category doesn't have any products or child categories
                        hasProducts = false;
                    }
                    else if (this.ChildCategoryCollection.Count == 0 || !this.HasCompleteCategories)
                    {
                        // Category doesn't have any complete categories or complete products
                        if (hasProducts)
                            isComplete = true;
                    }
                    else if (!this.HasCategoryButton || this.ParentCategoryId != null)
                    {
                        // Category doesn't have a category button set or isn't a root category
                    }
                    else
                    {
                        isComplete = true;
                    }
                }
                    
                return isComplete;
            }
        }        

        /// <summary>
        /// Gets a value if this category has alterations or not
        /// </summary>
        [XmlIgnore]
        private bool HasAlterations
        {
            get
            {
                return (this.CategoryAlterationCollection != null && this.CategoryAlterationCollection.Count > 0);
            }
        }

        /// <summary>
        /// Gets a flag indicating whether this category is a root category
        /// </summary>
	    public bool IsRootCategory
	    {
            get { return !this.ParentCategoryId.HasValue; }
	    }

        [XmlIgnore]
        public CategoryType TypeAsEnum
        {
            get
            {
                return EnumUtil.ToEnum<CategoryType>(this.Type);
            }
            set
            {
                this.Type = (int)value;
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Get all ids for this category's children categories (to be used in i.e. PredicatExpression)
        /// </summary>
        /// <returns>A List containing the ids</returns>
        public List<int> GetChildrenCategoryIds()
        {
            List<int> childrenCategoryIds = new List<int>();

            for (int i = 0; i < this.ChildCategoryCollection.Count; i++)
            {
                if (this.ChildCategoryCollection[i].ChildCategoryCollection.Count > 0)
                {
                    childrenCategoryIds.AddRange(this.ChildCategoryCollection[i].GetChildrenCategoryIds());
                }
                childrenCategoryIds.Add(this.ChildCategoryCollection[i].CategoryId);
            }

            return childrenCategoryIds;
        }

        private string _nameBreadCrumb = string.Empty;
        private string GetFullCategoryName(CategoryEntity cat)
        {
            string completeName;
            if (this._nameBreadCrumb.Length <= 0)
            {
                if (cat.ParentCategoryId.HasValue && cat.ParentCategoryId > 0)
                {
                    if (cat.ParentCategoryEntity.ParentCategoryId.HasValue && cat.ParentCategoryEntity.ParentCategoryId > 0)
                    {
                        completeName = this.GetFullCategoryName(cat.ParentCategoryEntity) + "\\" + cat.Name;
                    }
                    else
                    {
                        completeName = cat.ParentCategoryEntity.Name + "\\" + cat.Name;
                    }
                }
                else
                {
                    completeName = cat.Name;
                }
            }
            else
            {
                completeName = this._nameBreadCrumb;
            }

            return completeName;
        }

        /// <summary>
        /// Get all id's for the underlying categories + the id of the parent in 1 arraylist (to be used in i.e. PredicatExpression)
        /// </summary>
        /// <param name="parentCategory">Parent Category to search for</param>
        /// <param name="foundIds">Additional Id's you want to include, default null</param>
        /// <returns></returns>
        public ArrayList GetAllUnderlyingCategoryIds(CategoryEntity parentCategory, ArrayList foundIds)
        {
            if (foundIds == null)
            {
                foundIds = new ArrayList();
            }

            for (int i = 0; i < parentCategory.ChildCategoryCollection.Count; i++)
            {
                if (parentCategory.ChildCategoryCollection[i].ChildCategoryCollection.Count > 0)
                {
                    this.GetAllUnderlyingCategoryIds(parentCategory.ChildCategoryCollection[i], foundIds);
                }
                foundIds.Add(parentCategory.ChildCategoryCollection[i].CategoryId);
            }

            foundIds.Add(parentCategory.CategoryId);

            return foundIds;
        }

        /// <summary>
        /// Get the route ID for this category (set on itself or a parent)
        /// </summary>
        public int GetRouteId()
        {
            int routeId = 0;
            if (this.RouteId.HasValue)
            {
                routeId = this.RouteId.Value;
            }
            else if (this.ParentCategoryEntity != null && !this.ParentCategoryEntity.IsNew)
            {
                routeId = this.ParentCategoryEntity.GetRouteId();
            }
            return routeId;
        }

        /// <summary>
        /// Gets all the parent categories
        /// </summary>
        public CategoryCollection GetParentCategories(CategoryEntity cat, bool isFirst = true)
        {            
            CategoryCollection categories = new CategoryCollection();

            // If this category has a parent, add that category to the list first
            if (cat.ParentCategoryId.HasValue)
            {
                categories.AddRange(this.GetParentCategories(cat.ParentCategoryEntity, false));
            }

            if (!isFirst)
                categories.Add(cat);

            return categories;
        }

        /// <summary>
        /// Gets all the parent categories
        /// </summary>
        public CategoryCollection GetParentCategories()
        {
            return this.GetParentCategories(this.ParentCategoryEntity, false);
        }

        public ICollection<ITag> GetTags()
        {
            return this.CategoryTagCollection.Select(x => x.TagEntity).ToArray();
        }

        public ICollection<ITag> GetInheritedTags()
        {
            HashSet<TagEntity> inheritedTags = new HashSet<TagEntity>(new TagIdEqualityComparer());

            foreach (CategoryEntity parentCategoryEntity in GetParentCategories())
            {
                foreach (CategoryTagEntity categoryTagEntity in parentCategoryEntity.CategoryTagCollection)
                {
                    inheritedTags.Add(categoryTagEntity.TagEntity);
                }
            }

            return inheritedTags.ToArray();
        }

        #region Treelist Logic

        public CommonEntityBase Parent
        {
            get { return this.MenuEntity; }
        }

        [XmlIgnore]
        public string ItemId
        {
            get
            {
                return string.Format("{0}-{1}", this.ItemType, this.CategoryId);
            }
        }

        [XmlIgnore]
        public string ParentItemId
        {
            get
            {
                return this.ParentCategoryId.HasValue ? this.ParentCategoryEntity.ItemId : string.Empty;
            }
        }

        [XmlIgnore]
        public string ItemType
        {
            get
            {
                return MenuItemType.Category;
            }
        }

        [XmlIgnore]
        public string TypeName
        {
            get
            {
                return MenuItemType.Category;
            }
        }

	    public bool ValidatorCreateOrUpdateDefaultCategoryLanguage { get; set; }

	    public bool NameChanged { get; set; }
        
        public bool ParentCategoryChanged { get; set; }
        public int? OldParentCategoryId { get; set; }

        [XmlIgnore]
        public bool IsLinkedToBrand
        {
            get
            {
                return false;
            }
        }
        

        public IEntityCollection TagCollection => this.CategoryTagCollection;

        #endregion

        #endregion
        // __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included Code

		#endregion
	}
}
