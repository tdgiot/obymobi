﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    using Dionysos;
    using Dionysos.Data;
    using Obymobi.Enums;
    using System.Xml.Serialization;
    // __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'TerminalLog'. This class is used for Business Logic or for framework extension code.</summary>
	[Serializable]
	public partial class TerminalLogEntity : TerminalLogEntityBase
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
        ,ICompanyRelatedChildEntity
    // __LLBLGENPRO_USER_CODE_REGION_END	
	{
		/// <summary>CTor</summary>
		public TerminalLogEntity():base()
		{
		}

		/// <summary>CTor</summary>
		/// <param name="terminalLogId">PK value for TerminalLog which data should be fetched into this TerminalLog object</param>
		public TerminalLogEntity(System.Int32 terminalLogId):
			base(terminalLogId)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="terminalLogId">PK value for TerminalLog which data should be fetched into this TerminalLog object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public TerminalLogEntity(System.Int32 terminalLogId, IPrefetchPath prefetchPathToUse):
			base(terminalLogId, prefetchPathToUse)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="terminalLogId">PK value for TerminalLog which data should be fetched into this TerminalLog object</param>
		/// <param name="validator">The custom validator object for this TerminalLogEntity</param>
		public TerminalLogEntity(System.Int32 terminalLogId, IValidator validator):
			base(terminalLogId, validator)
		{
		}
		
		/// <summary>CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected TerminalLogEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
            // __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
            // __LLBLGENPRO_USER_CODE_REGION_END
		}

		#region Custom Entity code
		
        // __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode

        [XmlIgnore]
        public CommonEntityBase Parent
        {
            get { return this.TerminalEntity; }
        }

        [DataGridViewColumnVisible]
        public string TypeName
        {
            get { return this.Type.ToEnum<TerminalLogType>().ToString(); }
        }

        public TerminalLogType TypeAsEnum
        {
            get
            {
                return this.Type.ToEnum<TerminalLogType>();
            }
            set
            {
                this.Type = (int)value;
            }
        }

        // __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included Code

		#endregion
	}
}
