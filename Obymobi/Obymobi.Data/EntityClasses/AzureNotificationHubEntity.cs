﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'AzureNotificationHub'. This class is used for Business Logic or for framework extension code.</summary>
	[Serializable]
	public partial class AzureNotificationHubEntity : AzureNotificationHubEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
        , INonCompanyRelatedEntity
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		/// <summary>CTor</summary>
		public AzureNotificationHubEntity():base()
		{
		}

		/// <summary>CTor</summary>
		/// <param name="notificationHubId">PK value for AzureNotificationHub which data should be fetched into this AzureNotificationHub object</param>
		public AzureNotificationHubEntity(System.Int32 notificationHubId):
			base(notificationHubId)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="notificationHubId">PK value for AzureNotificationHub which data should be fetched into this AzureNotificationHub object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public AzureNotificationHubEntity(System.Int32 notificationHubId, IPrefetchPath prefetchPathToUse):
			base(notificationHubId, prefetchPathToUse)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="notificationHubId">PK value for AzureNotificationHub which data should be fetched into this AzureNotificationHub object</param>
		/// <param name="validator">The custom validator object for this AzureNotificationHubEntity</param>
		public AzureNotificationHubEntity(System.Int32 notificationHubId, IValidator validator):
			base(notificationHubId, validator)
		{
		}
		
		/// <summary>CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected AzureNotificationHubEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}

		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included Code

		#endregion
	}
}
