﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    using System.Xml.Serialization;
    using Obymobi.Enums;
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'RoomControlComponent'. This class is used for Business Logic or for framework extension code.</summary>
	[Serializable]
	public partial class RoomControlComponentEntity : RoomControlComponentEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
        , ICompanyRelatedChildEntityOrNullableCompanyRelatedChildEntity, Obymobi.Logic.RoomControl.IRoomControlItem, ICustomTextContainingEntity, ITimestampEntity
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		/// <summary>CTor</summary>
		public RoomControlComponentEntity():base()
		{
		}

		/// <summary>CTor</summary>
		/// <param name="roomControlComponentId">PK value for RoomControlComponent which data should be fetched into this RoomControlComponent object</param>
		public RoomControlComponentEntity(System.Int32 roomControlComponentId):
			base(roomControlComponentId)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="roomControlComponentId">PK value for RoomControlComponent which data should be fetched into this RoomControlComponent object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public RoomControlComponentEntity(System.Int32 roomControlComponentId, IPrefetchPath prefetchPathToUse):
			base(roomControlComponentId, prefetchPathToUse)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="roomControlComponentId">PK value for RoomControlComponent which data should be fetched into this RoomControlComponent object</param>
		/// <param name="validator">The custom validator object for this RoomControlComponentEntity</param>
		public RoomControlComponentEntity(System.Int32 roomControlComponentId, IValidator validator):
			base(roomControlComponentId, validator)
		{
		}
		
		/// <summary>CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected RoomControlComponentEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}

		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode

        [XmlIgnore]
        public CommonEntityBase Parent
        {
            get { return this.RoomControlSectionEntity; }
        }

        [XmlIgnore]
        public string ItemId
        {
            get
            {
                return string.Format("{0}-{1}", this.ItemType.ToString(), this.RoomControlComponentId);
            }
        }

        [XmlIgnore]
        public string ParentItemId
        {
            get
            {
                return this.RoomControlSectionEntity.ItemId;
            }
        }

        [XmlIgnore]
        public string ItemType
        {
            get
            {
                return RoomControlItemType.Component;
            }
        }

        [DataGridViewColumnVisible]
        [XmlIgnore]
        public string TypeName
        {
            get
            {
                return this.Type.ToString();
            }
        }

        [DataGridViewColumnVisible]
        [XmlIgnore]
        public string NameAndArea
        {
            get
            {
                return this.RoomControlSectionId.HasValue ? string.Format("{0} - {1}", this.RoomControlSectionEntity.RoomControlAreaEntity.Name, this.Name) : this.Name;
            }
        }

	    public bool ValidatorCreateOrUpdateDefaultComponentLanguage { get; set; }

	    public bool NameChanged { get; set; }

	    // __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included Code

		#endregion
	}
}
