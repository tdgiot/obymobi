﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    using System.Xml.Serialization;
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'PriceLevelItem'. This class is used for Business Logic or for framework extension code.</summary>
	[Serializable]
	public partial class PriceLevelItemEntity : PriceLevelItemEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
        , ICompanyRelatedChildEntityOrNullableCompanyRelatedChildEntity, ITimestampEntity
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		/// <summary>CTor</summary>
		public PriceLevelItemEntity():base()
		{
		}

		/// <summary>CTor</summary>
		/// <param name="priceLevelItemId">PK value for PriceLevelItem which data should be fetched into this PriceLevelItem object</param>
		public PriceLevelItemEntity(System.Int32 priceLevelItemId):
			base(priceLevelItemId)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="priceLevelItemId">PK value for PriceLevelItem which data should be fetched into this PriceLevelItem object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public PriceLevelItemEntity(System.Int32 priceLevelItemId, IPrefetchPath prefetchPathToUse):
			base(priceLevelItemId, prefetchPathToUse)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="priceLevelItemId">PK value for PriceLevelItem which data should be fetched into this PriceLevelItem object</param>
		/// <param name="validator">The custom validator object for this PriceLevelItemEntity</param>
		public PriceLevelItemEntity(System.Int32 priceLevelItemId, IValidator validator):
			base(priceLevelItemId, validator)
		{
		}
		
		/// <summary>CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected PriceLevelItemEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}

		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode

        [XmlIgnore]
        public CommonEntityBase Parent
        {
            get { return this.PriceLevelEntity; }
        }

        [XmlIgnore]
        public string Name
        {
            get
            {
                if (this.ProductId.HasValue)
                    return this.ProductEntity.Name;
                else if (this.AlterationId.HasValue)
                    return this.AlterationEntity.Name;
                else if (this.AlterationoptionId.HasValue)
                    return this.AlterationoptionEntity.Name;

                return string.Empty;
            }
            set
            {
                if (this.ProductId.HasValue)
                    this.ProductEntity.Name = value;
                if (this.AlterationId.HasValue)
                    this.AlterationEntity.Name = value;
                if (this.AlterationoptionId.HasValue)
                    this.AlterationoptionEntity.Name = value;
            }
        }



		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included Code

		#endregion
	}
}
