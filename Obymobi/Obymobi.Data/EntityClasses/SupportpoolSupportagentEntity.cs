﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    using System.Collections.Generic;
    using Dionysos;
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'SupportpoolSupportagent'. This class is used for Business Logic or for framework extension code.</summary>
	[Serializable]
	public partial class SupportpoolSupportagentEntity : SupportpoolSupportagentEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
        , ISystemEntity
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		/// <summary>CTor</summary>
		public SupportpoolSupportagentEntity():base()
		{
		}

		/// <summary>CTor</summary>
		/// <param name="supportpoolSupportagentId">PK value for SupportpoolSupportagent which data should be fetched into this SupportpoolSupportagent object</param>
		public SupportpoolSupportagentEntity(System.Int32 supportpoolSupportagentId):
			base(supportpoolSupportagentId)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="supportpoolSupportagentId">PK value for SupportpoolSupportagent which data should be fetched into this SupportpoolSupportagent object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public SupportpoolSupportagentEntity(System.Int32 supportpoolSupportagentId, IPrefetchPath prefetchPathToUse):
			base(supportpoolSupportagentId, prefetchPathToUse)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="supportpoolSupportagentId">PK value for SupportpoolSupportagent which data should be fetched into this SupportpoolSupportagent object</param>
		/// <param name="validator">The custom validator object for this SupportpoolSupportagentEntity</param>
		public SupportpoolSupportagentEntity(System.Int32 supportpoolSupportagentId, IValidator validator):
			base(supportpoolSupportagentId, validator)
		{
		}
		
		/// <summary>CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected SupportpoolSupportagentEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}

		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode

        [DataGridViewColumnVisible]
        public string SupportagentName
        {
            get
            {
                return this.SupportagentEntity.Name;
            }
        }

        [DataGridViewColumnVisible]
        public string NotificationTypes
        {
            get
            {
                List<string> notificationTypes = new List<string>();

                if (this.NotifyOfflineTerminals)
                    notificationTypes.Add("Offline terminals");
                if (this.NotifyOfflineClients)
                    notificationTypes.Add("Offline clients");
                if (this.NotifyUnprocessableOrders)
                    notificationTypes.Add("Unprocessable orders");
                if (this.NotifyExpiredSteps)
                    notificationTypes.Add("Expired steps");
                if (this.NotifyTooMuchOfflineClientsJump)
                    notificationTypes.Add("Offline clients jump");
                if (this.NotifyBouncedEmails)
                    notificationTypes.Add("Bounced emails");

                return StringUtil.CombineWithCommaSpace(notificationTypes.ToArray());
            }
        }

		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included Code

		#endregion
	}
}
