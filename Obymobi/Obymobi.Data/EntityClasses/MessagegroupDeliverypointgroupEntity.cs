﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.1
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	

	/// <summary>Entity class which represents the entity 'MessagegroupDeliverypointgroup'. This class is used for Business Logic or for framework extension code.</summary>
	[Serializable]
	public partial class MessagegroupDeliverypointgroupEntity : MessagegroupDeliverypointgroupEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END
			
	{
		/// <summary>CTor</summary>
		public MessagegroupDeliverypointgroupEntity():base()
		{
		}

		/// <summary>CTor</summary>
		/// <param name="messagegroupDeliverypointgroupId">PK value for MessagegroupDeliverypointgroup which data should be fetched into this MessagegroupDeliverypointgroup object</param>
		public MessagegroupDeliverypointgroupEntity(System.Int32 messagegroupDeliverypointgroupId):
			base(messagegroupDeliverypointgroupId)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="messagegroupDeliverypointgroupId">PK value for MessagegroupDeliverypointgroup which data should be fetched into this MessagegroupDeliverypointgroup object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public MessagegroupDeliverypointgroupEntity(System.Int32 messagegroupDeliverypointgroupId, IPrefetchPath prefetchPathToUse):
			base(messagegroupDeliverypointgroupId, prefetchPathToUse)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="messagegroupDeliverypointgroupId">PK value for MessagegroupDeliverypointgroup which data should be fetched into this MessagegroupDeliverypointgroup object</param>
		/// <param name="validator">The custom validator object for this MessagegroupDeliverypointgroupEntity</param>
		public MessagegroupDeliverypointgroupEntity(System.Int32 messagegroupDeliverypointgroupId, IValidator validator):
			base(messagegroupDeliverypointgroupId, validator)
		{
		}
		
		/// <summary>CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected MessagegroupDeliverypointgroupEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
			
		}

		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		
		#endregion

		#region Included Code

		#endregion
	}
}
