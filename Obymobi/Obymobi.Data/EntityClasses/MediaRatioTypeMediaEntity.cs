﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    using Dionysos;
using Obymobi.Enums;
    using System.Xml.Serialization;
    using System.IO;
    using System.Drawing;
    // __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'MediaRatioTypeMedia'. This class is used for Business Logic or for framework extension code.</summary>
	[Serializable]
	public partial class MediaRatioTypeMediaEntity : MediaRatioTypeMediaEntityBase
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
        , INonCompanyRelatedEntity
        // __LLBLGENPRO_USER_CODE_REGION_END	
	{
		/// <summary>CTor</summary>
		public MediaRatioTypeMediaEntity():base()
		{
		}

		/// <summary>CTor</summary>
		/// <param name="mediaRatioTypeMediaId">PK value for MediaRatioTypeMedia which data should be fetched into this MediaRatioTypeMedia object</param>
		public MediaRatioTypeMediaEntity(System.Int32 mediaRatioTypeMediaId):
			base(mediaRatioTypeMediaId)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="mediaRatioTypeMediaId">PK value for MediaRatioTypeMedia which data should be fetched into this MediaRatioTypeMedia object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public MediaRatioTypeMediaEntity(System.Int32 mediaRatioTypeMediaId, IPrefetchPath prefetchPathToUse):
			base(mediaRatioTypeMediaId, prefetchPathToUse)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="mediaRatioTypeMediaId">PK value for MediaRatioTypeMedia which data should be fetched into this MediaRatioTypeMedia object</param>
		/// <param name="validator">The custom validator object for this MediaRatioTypeMediaEntity</param>
		public MediaRatioTypeMediaEntity(System.Int32 mediaRatioTypeMediaId, IValidator validator):
			base(mediaRatioTypeMediaId, validator)
		{
		}
		
		/// <summary>CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected MediaRatioTypeMediaEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
            // __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
            // __LLBLGENPRO_USER_CODE_REGION_END
		}

		#region Custom Entity code
		
        // __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode

        /// <summary>
        /// Gets or sets the media type as an <see cref="MediaType"/> enumerator value.
        /// </summary>
        [XmlIgnore]
        public MediaType MediaTypeAsEnum
        {
            get
            {
                if (this.MediaType.HasValue)
                    return (Enums.MediaType)this.MediaType.Value;
                else
                    return Enums.MediaType.NoMediaTypeSpecified;
            }
            set
            {
                this.MediaType = (int)value;
            }
        }

        [XmlIgnore]
        private Obymobi.MediaRatioType mediaRatioType = null;

        /// <summary>
        /// Gets the media ratio type instance
        /// </summary>
        [XmlIgnore]
        public Obymobi.MediaRatioType MediaRatioType
        {
            get
            {
                if (this.mediaRatioType == null && this.MediaTypeAsEnum != Enums.MediaType.NoMediaTypeSpecified)
                    this.mediaRatioType = MediaRatioTypes.GetMediaRatioType(this.MediaTypeAsEnum);
                return this.mediaRatioType;
            }
        }

        [XmlIgnore]
        public string FileNameOnCdn
        {
            get
            {
                return this.GetFileName(FileNameType.Cdn);
            }
        }

        [XmlIgnore]
        public string FileNameFormat
        {
            get
            {
                return this.GetFileName(FileNameType.Format);
            }
        }

        [XmlIgnore]
        public string MediaUrl
        {
            get
            {
                return string.Format("~/Generic/Media.aspx?id={0}", this.MediaId);
            }
        }

        [XmlIgnore]
        public string RelatedMediaName
        {
            get
            {
                return base.MediaEntity.Name;
            }
        }
        
        [XmlIgnore]
        public string RelatedEntityName
        {
            get
            {
                return base.MediaEntity.RelatedEntityName;
            }
        }

        [XmlIgnore]
        public string MediaTypeName
        {
            get
            {
                if (this.MediaRatioType != null)
                    return this.MediaRatioType.Name;

                return "Unknown";
            }
        }

        public enum FileNameType
        {
            // Local, - Legacy.
            Cdn,
            Format,
            DeletionWildcard
        }

        /// <summary>
        /// Sets the default crop, and thereby assigning values to Top, Left, Width & Height.
        /// </summary>
        /// <param name="image">The image.</param>
        public void SetDefaultCrop(Size image)
        {
            // Determin ratio's
            decimal widthRatio = Decimal.Divide(image.Width, this.MediaRatioType.Width);
            decimal heightRatio = Decimal.Divide(image.Height, this.MediaRatioType.Height);

            // The smallet ratio (amount of times it's larger than the MediaRatioType) will be the limting one                          
            int cutoutX = 0;
            int cutoutY = 0;
            int cutoutWidth = 0;
            int cutoutHeight = 0;            
            if (image.Width == this.MediaRatioType.Width && image.Height == this.MediaRatioType.Height)
            { 
                // Perfect fit
                cutoutHeight = image.Height;
                cutoutWidth = image.Width;
            }
            else if (widthRatio < heightRatio)
            {
                // Base on Width, need to calculate the centered cut out based on the height
                cutoutX = 0;
                cutoutWidth = image.Width;
                cutoutHeight = Convert.ToInt32(System.Math.Ceiling(cutoutWidth * Decimal.Divide(this.MediaRatioType.Height, this.MediaRatioType.Width)));                                    
                cutoutY = (image.Height - cutoutHeight) / 2;
            }
            else
            {
                cutoutY = 0;
                cutoutHeight = image.Height;
                cutoutWidth = Convert.ToInt32(System.Math.Ceiling(cutoutHeight * Decimal.Divide(this.MediaRatioType.Width, this.MediaRatioType.Height)));                    
                cutoutX = (image.Width - cutoutWidth) / 2;
            }

            this.Left = cutoutX;
            this.Top = cutoutY;
            this.Width = cutoutWidth;
            this.Height = cutoutHeight;
        }

        public Rectangle GetCropRectangle()
        {
            return new Rectangle(this.Left, this.Top, this.Width, this.Height);
        }

        public const string FILENAME_FORMAT_TICKS = "[ticks]"; // MUST BE LOWER CASE!
        public string GetFileName(FileNameType type)        
        {
            string fileName = this.MediaRatioTypeMediaId.ToString() + "-";

            // Only need the first part.
            if (type == FileNameType.DeletionWildcard)
            {
                return fileName; // It's important that we have the dash, otherwise 1 would also delete 100 and 1000 and 1414, now just 1-
            }

            // GK Null check is useless, since you would want an exception I think? Or do you want to filename with no filename?
            Obymobi.MediaRatioType mediaRatioType = this.MediaRatioType;
            if (mediaRatioType != null)
                fileName += string.Format("{0}-{1}w-{2}h-{3}l-{4}t-", mediaRatioType.MediaType.ToString(), mediaRatioType.Width, mediaRatioType.Height, this.Left, this.Top);
                
            if (type == FileNameType.Cdn)
            {
                if (this.LastDistributedVersionTicks.HasValue)
                    fileName += this.LastDistributedVersionTicks.ToString();
                else
                    return string.Empty; // Don't return a file it wasn't distributed yet.                    
            }
            else if (type == FileNameType.Format)
            {
                fileName += FILENAME_FORMAT_TICKS;
            }

            if (this.MediaEntity.FilePathRelativeToMediaPath.IsNullOrWhiteSpace())
                fileName += this.MediaRatioTypeMediaId;
            else
                fileName += Path.GetExtension(this.MediaEntity.FilePathRelativeToMediaPath);            

            return fileName.ToLower();
        }

        public CdnStatus GetAmazonCdnStatus()
        {
            if (!this.LastDistributedVersionTicksAmazon.HasValue)
                return CdnStatus.NotAvailable;
            else if (this.LastDistributedVersionTicksAmazon.Value >= this.MediaRatioTypeMediaFileEntity.ChangedTicks)
                return CdnStatus.UpToDate;
            else
                return CdnStatus.OutOfdate;
        }

        public enum CdnStatus
        {
            NotAvailable,
            OutOfdate,
            UpToDate,
            NotApplicable
        }

        // __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included Code

		#endregion
	}
}
