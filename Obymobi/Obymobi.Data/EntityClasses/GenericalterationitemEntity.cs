﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    using System.Xml.Serialization;
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'Genericalterationitem'. This class is used for Business Logic or for framework extension code.</summary>
	[Serializable]
	public partial class GenericalterationitemEntity : GenericalterationitemEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
        , INonCompanyRelatedEntity
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		/// <summary>CTor</summary>
		public GenericalterationitemEntity():base()
		{
		}

		/// <summary>CTor</summary>
		/// <param name="genericalterationitemId">PK value for Genericalterationitem which data should be fetched into this Genericalterationitem object</param>
		public GenericalterationitemEntity(System.Int32 genericalterationitemId):
			base(genericalterationitemId)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="genericalterationitemId">PK value for Genericalterationitem which data should be fetched into this Genericalterationitem object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public GenericalterationitemEntity(System.Int32 genericalterationitemId, IPrefetchPath prefetchPathToUse):
			base(genericalterationitemId, prefetchPathToUse)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="genericalterationitemId">PK value for Genericalterationitem which data should be fetched into this Genericalterationitem object</param>
		/// <param name="validator">The custom validator object for this GenericalterationitemEntity</param>
		public GenericalterationitemEntity(System.Int32 genericalterationitemId, IValidator validator):
			base(genericalterationitemId, validator)
		{
		}
		
		/// <summary>CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected GenericalterationitemEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}

		#region Custom Entity code
		
        // __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode

        [DataGridViewColumnVisibleAttribute]
        [XmlIgnore]
        public string GenericalterationName
        {
            get
            {
                return this.GenericalterationEntity.Name;
            }
        }

        [DataGridViewColumnVisibleAttribute]
        [XmlIgnore]
        public string GenericalterationoptionName
        {
            get
            {
                return this.GenericalterationoptionEntity.Name;
            }
        }

        [DataGridViewColumnVisibleAttribute]
        [XmlIgnore]
        public string GenericalterationoptionNameAndPriceIn
        {
            get
            {
                return this.GenericalterationoptionEntity.NameAndPriceIn;
            }
        }

        // __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included Code

		#endregion
	}
}
