﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    using System.Xml.Serialization;
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'Posdeliverypointgroup'. This class is used for Business Logic or for framework extension code.</summary>
	[Serializable]
	public partial class PosdeliverypointgroupEntity : PosdeliverypointgroupEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
        , ICompanyRelatedEntity
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		/// <summary>CTor</summary>
		public PosdeliverypointgroupEntity():base()
		{
		}

		/// <summary>CTor</summary>
		/// <param name="posdeliverypointgroupId">PK value for Posdeliverypointgroup which data should be fetched into this Posdeliverypointgroup object</param>
		public PosdeliverypointgroupEntity(System.Int32 posdeliverypointgroupId):
			base(posdeliverypointgroupId)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="posdeliverypointgroupId">PK value for Posdeliverypointgroup which data should be fetched into this Posdeliverypointgroup object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public PosdeliverypointgroupEntity(System.Int32 posdeliverypointgroupId, IPrefetchPath prefetchPathToUse):
			base(posdeliverypointgroupId, prefetchPathToUse)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="posdeliverypointgroupId">PK value for Posdeliverypointgroup which data should be fetched into this Posdeliverypointgroup object</param>
		/// <param name="validator">The custom validator object for this PosdeliverypointgroupEntity</param>
		public PosdeliverypointgroupEntity(System.Int32 posdeliverypointgroupId, IValidator validator):
			base(posdeliverypointgroupId, validator)
		{
		}
		
		/// <summary>CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected PosdeliverypointgroupEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}

		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode

        /// <summary>
        /// Gets the name and external id of the Posdeliverypointgroup
        /// </summary>
        [XmlIgnore]
        public string NameAndExternalId
        {
            get
            {
                return string.Format("{0} ({1})", this.Name, this.ExternalId);
            }
        }

		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included Code

		#endregion
	}
}
