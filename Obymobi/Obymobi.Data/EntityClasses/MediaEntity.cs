﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    using Obymobi.Data.HelperClasses;
    using System.IO;
    using Dionysos.Drawing;
    using Dionysos;
    using Dionysos.Web;
    using System.Drawing;
    using Obymobi.Enums;
    using System.Web;
    using Dionysos.Data.LLBLGen;
    using System.Collections.Generic;
    using System.Xml.Serialization;
    using Dionysos.Interfaces.Data;
    using Interfaces;
    using System.Threading.Tasks;

    // __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'Media'. This class is used for Business Logic or for framework extension code.</summary>
	[Serializable]
	public partial class MediaEntity : MediaEntityBase
                                       // __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
                                       , ICompanyRelatedChildEntityOrNullableCompanyRelatedChildEntity
                                       , IMediaEntity
                                       , ITimestampEntity
        // __LLBLGENPRO_USER_CODE_REGION_END	
	{
		/// <summary>CTor</summary>
		public MediaEntity():base()
		{
		}

		/// <summary>CTor</summary>
		/// <param name="mediaId">PK value for Media which data should be fetched into this Media object</param>
		public MediaEntity(System.Int32 mediaId):
			base(mediaId)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="mediaId">PK value for Media which data should be fetched into this Media object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public MediaEntity(System.Int32 mediaId, IPrefetchPath prefetchPathToUse):
			base(mediaId, prefetchPathToUse)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="mediaId">PK value for Media which data should be fetched into this Media object</param>
		/// <param name="validator">The custom validator object for this MediaEntity</param>
		public MediaEntity(System.Int32 mediaId, IValidator validator):
			base(mediaId, validator)
		{
		}
		
		/// <summary>CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected MediaEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
            // __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
            // __LLBLGENPRO_USER_CODE_REGION_END
		}

		#region Custom Entity code
		
        // __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode		

        #region Properties

        public bool _cachedRelatedCompanyIdRetrieved = false;
        public int? _cachedRelatedCompanyId = null;

        public bool _cachedRelatedBrandIdRetrieved = false;
        public int? _cachedRelatedBrandId = null;

        public CommonEntityBase Parent
        {
            get { return (CommonEntityBase)this.RelatedEntity; }
        }

        public int? ParentCompanyId
        {
            get { return this.RelatedCompanyId; }
            set { this.RelatedCompanyId = value; }
        }

        /// <summary>
        /// Gets the EntityName this MediaEntity is related to (eg. Product, Customer, etc.).
        /// </summary>
        [XmlIgnore]
        public string RelatedEntityName
        {
            get
            {
                // Loop through fields and find which Related Id is filled
                string toReturn = null;
                for (int i = 0; i < (int)MediaFieldIndex.AmountOfFields; i++)
                {
                    if (this.Fields[i].Name.EndsWith("Id") &&
                        this.Fields[i].Name != "MediaId" &&
                        this.Fields[i].Name != "MediaTypeId" &&
                        this.Fields[i].Name != "AgnosticMediaId" &&
                        this.Fields[i].Name != "RelatedCompanyId" &&
                        !this.Fields[i].Name.StartsWith("Action"))
                    {
                        // We have an Id field which is not the MediaId
                        if (this.Fields[i].CurrentValue != null &&
                            this.Fields[i].CurrentValue != DBNull.Value)
                        {
                            // We have a value in the ID field
                            toReturn = this.Fields[i].Name.Replace("Id", String.Empty);
                            break;
                        }
                    }
                }                               

                return toReturn;
            }
        }

        [XmlIgnore]
        public bool IsCultureSpecific
        {
            get { return (this.AgnosticMediaId.HasValue && this.AgnosticMediaId > 0); }
        }

        [XmlIgnore]
        public bool IsCultureAgnostic
        {
            get { return !this.IsCultureSpecific; }
        }

        [XmlIgnore]
        public EntityType RelatedEntityType
        {
            get { return (this.RelatedEntityName + "Entity").ToEnum<EntityType>(); }
        }

        /// <summary>
        /// Gets the temporary filename of a media entity
        /// </summary>
        [XmlIgnore]
        public string TemporaryFileName
        {
            get
            {
                string fileName = string.Empty;

                if (this.Name.Length > 0)
                {
                    if (this.Changed.HasValue)
                        fileName = string.Format("{0}-{1}{2}", this.MediaId, this.Changed.Value.Ticks, this.FilePathRelativeToMediaPath.Substring(this.FilePathRelativeToMediaPath.Length - 4));
                    else
                        fileName = string.Format("{0}-{1}", this.MediaId, this.FilePathRelativeToMediaPath.Substring(this.FilePathRelativeToMediaPath.Length - 4));
                }

                return fileName;
            }
        }

        [XmlIgnore]
        private DateTime? Changed
        {
            get
            {
                if (this.UpdatedUTC != null)
                    return this.UpdatedUTC;
                else
                    return this.CreatedUTC;
            }
        }

        [XmlIgnore]
        public int? RelatedEntityId
        {
            get
            {
                int? toReturn = null;
                if (this.RelatedEntityName != null)
                {
                    toReturn = (int)this.Fields[this.RelatedEntityName + "Id"].CurrentValue;
                }
                return toReturn;
            }

        }

        /// <summary>
        /// Gets the IEntity this MediaEntity is related to.
        /// </summary>
        [XmlIgnore]
        public IEntity RelatedEntity
        {
            get
            {
                IEntity toReturn = null;

                string relatedEntityName = this.RelatedEntityName;
                if (relatedEntityName != null)
                {
                    toReturn = LLBLGenEntityUtil.GetEntityUsingFieldCompareValuePredicateExpression(relatedEntityName,
                                                                                                    this.Fields[relatedEntityName + "Id"].Name,
                                                                                                    ComparisonOperator.Equal,
                                                                                                    (int)this.Fields[relatedEntityName + "Id"].CurrentValue, this.Transaction);                    
                }

                return toReturn;
            }
        }

        /// <summary>
        /// Gets the CompanyId of the entity this MediaEntity is related to.
        /// </summary>
        [XmlIgnore]
        public int RelatedEntityCompanyId
        {
            get
            {
                int toReturn = 0;

                IEntity relatedEntity = this.RelatedEntity;
                if (relatedEntity != null)
                {
                    if (relatedEntity.Fields["CompanyId"] != null && relatedEntity.Fields["CompanyId"].CurrentValue != null && relatedEntity.Fields["CompanyId"].CurrentValue != DBNull.Value)
                    {
                        toReturn = int.Parse(relatedEntity.Fields["CompanyId"].CurrentValue.ToString());
                    }
                    else if (relatedEntity.Fields["ParentCompanyId"] != null && relatedEntity.Fields["ParentCompanyId"].CurrentValue != null && relatedEntity.Fields["ParentCompanyId"].CurrentValue != DBNull.Value)
                    {
                        toReturn = int.Parse(relatedEntity.Fields["ParentCompanyId"].CurrentValue.ToString());
                    }
                }

                return toReturn;
            }
        }

        /// <summary>
        /// Gets the MediaRatioTypeCollection with all generic and RelatedEntity specific ratios.
        /// </summary>
        [XmlIgnore]
        public List<MediaRatioType> MediaRatioTypesForRelatedEntity => RelatedEntity == null ? new List<MediaRatioType>() : MediaRatioTypes.GetMediaRatioTypes(RelatedEntity.LLBLGenProEntityName);

        /// <summary>
        /// Gets a string containing the connected media ratio types for this media instance.
        /// </summary>
        [XmlIgnore]
        public string MediaRatioTypeString
        {
            get
            {
                string mediaRatioTypes = string.Empty;
                foreach (MediaRatioTypeMediaEntity item in this.MediaRatioTypeMediaCollection)
                {
                    if (!mediaRatioTypes.IsNullOrWhiteSpace())
                        mediaRatioTypes += ", ";
                    mediaRatioTypes += item.MediaTypeAsEnum.ToString();
                }
                return mediaRatioTypes;
            }
        }

        [XmlIgnore]
        public string MediaCountriesString
        {
            get
            {
                string cultures = string.Empty;
                foreach (MediaCultureEntity culture in this.MediaCultureCollection)
                {
                    if (!cultures.IsNullOrWhiteSpace())
                        cultures += ", ";
                    cultures += Obymobi.Culture.Mappings[culture.CultureCode].Country.CodeAlpha2;
                }
                return cultures;
            }
        }

        /// <summary>
        /// Gets or sets if this Media is Generic (not linked to a Company, i.e. GenericCategory, GenericProduct, Entertainment without Company, etc.)
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is generic; otherwise, <c>false</c>.
        /// </value>
        [XmlIgnore]
        public bool IsGeneric
        {
            get
            {
                return (this.GenericcategoryId.HasValue ||
                        this.GenericproductId.HasValue ||
                        (this.AdvertisementId.HasValue && !this.AdvertisementEntity.CompanyId.HasValue) ||
                        this.PointOfInterestId.HasValue ||
                        (this.EntertainmentId.HasValue && !this.EntertainmentEntity.CompanyId.HasValue) ||
                        (this.PageId.HasValue && !this.PageEntity.SiteEntity.CompanyId.HasValue) ||
                        (this.PageElementId.HasValue && !this.PageElementEntity.PageEntity.SiteEntity.CompanyId.HasValue) ||
                        (this.SiteId.HasValue && !this.SiteEntity.CompanyId.HasValue) ||
                        (this.PageTemplateId.HasValue) ||
                        (this.PageTemplateElementId.HasValue) ||
                        (this.UIThemeId.HasValue && !this.UIThemeEntity.CompanyId.HasValue));
            }
        }

        [XmlIgnore]
        public string RelatedEntityEditPageRelativeLink
        {
            get
            {
                IEntityInformation entityInfo = EntityInformationUtil.GetEntityInformation(this.RelatedEntity);
                if (!String.IsNullOrEmpty(entityInfo.DefaultEntityEditPage))
                {
                    return String.Format("{0}?id={1}", entityInfo.DefaultEntityEditPage, this.RelatedEntityId);
                }
                else
                    return string.Empty;
            }
        }

        [XmlIgnore]
        public string RelatedEntityNameAndShowFieldValue
        {
            get
            {
                string toReturn = string.Empty;

                // Get related IEntity
                IEntity relatedEntity = DataFactory.EntityFactory.GetEntity(this.RelatedEntity.LLBLGenProEntityName, this.RelatedEntityId) as IEntity;

                // Get EnityInformation of the relatedEntity
                IEntityInformation entityInfo = EntityInformationUtil.GetEntityInformation(relatedEntity);

                // Set the related entity name
                toReturn = entityInfo.FriendlyName + " - ";

                // Set the related entity id
                string relatedShowFieldValue;
                if (entityInfo.ShowFieldName.Length > 0 && Dionysos.Reflection.Member.HasProperty(relatedEntity, entityInfo.ShowFieldName))
                {
                    relatedShowFieldValue = Dionysos.Reflection.Member.InvokePropertyAsString(relatedEntity, entityInfo.ShowFieldName);
                }
                else if (relatedEntity.Fields[entityInfo.ShowFieldName] != null)
                {
                    relatedShowFieldValue = relatedEntity.Fields[entityInfo.ShowFieldName].CurrentValue.ToString();
                }
                else
                {
                    relatedShowFieldValue = relatedEntity.PrimaryKeyFields[0].CurrentValue.ToString();
                }

                if (relatedShowFieldValue.Length > 0)
                {
                    toReturn += relatedShowFieldValue;
                }
                else if (relatedEntity.Fields["Name"] != null && relatedEntity.Fields["Name"].CurrentValue != null)
                {
                    toReturn += relatedEntity.Fields["Name"].CurrentValue.ToString();
                }
                else if (relatedEntity.Fields["Title"] != null && relatedEntity.Fields["Title"].CurrentValue != null)
                {
                    toReturn += relatedEntity.Fields["Title"].CurrentValue.ToString();
                }
                else if (relatedEntity.Fields["Number"] != null && relatedEntity.Fields["Number"].CurrentValue != null)
                {
                    toReturn += relatedEntity.Fields["Number"].CurrentValue.ToString();
                }
                else
                {
                    toReturn += "Id = " + this.RelatedEntityId.ToString();
                }

                return toReturn;
            }
        }

        [XmlIgnore]
        public string ThumbnailImageUrl
        {
            get { return "~/Generic/ThumbnailGenerator.ashx?mediaId={0}&width={1}&height={2}&fitInDimensions=true".FormatSafe(this.MediaId, 25, 35); }
        }

        [XmlIgnore]
        public string PreviewImageUrl
        {
            get { return "~/Generic/ThumbnailGenerator.ashx?mediaId={0}&width={1}&height={2}&fitInDimensions=true".FormatSafe(this.MediaId, 600, 330); }
        }

        private byte[] fileBytes = null;
        private Func<byte[]> fileDownloadDelegate = null;
        private Func<Task<byte[]>> fileDownloadDelegateAsync = null;

        [XmlIgnore]
        public byte[] FileBytes
        {
            get
            {
                if (this.fileBytes == null || this.fileBytes.Length <= 0)
                {
                    if (this.fileDownloadDelegate != null)
                        this.fileBytes = this.FileDownloadDelegate.Invoke();
                    else if (this.fileDownloadDelegateAsync != null)
                        this.fileBytes = this.FileDownloadDelegateAsync.Invoke().Result;
                }
                return this.fileBytes;
            }
            set { this.fileBytes = value; }
        }

        [XmlIgnore]
        public Func<byte[]> FileDownloadDelegate
        {
            get { return this.fileDownloadDelegate; }
            set { this.fileDownloadDelegate = value; }
        }

        [XmlIgnore]
        public Func<Task<byte[]>> FileDownloadDelegateAsync
        {
            get { return this.fileDownloadDelegateAsync; }
            set { this.fileDownloadDelegateAsync = value; }
        }

        [XmlIgnore]
        public WidgetEntity WidgetEntity
        {
            get { return this.WidgetActionBannerEntity; }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Get the physical file path of the MediaEntity using the current HttpContext.
        /// </summary>
        /// <returns>Physical file path of the MediaEntity.</returns>
        public string GetPhysicalPath()
        {
            return System.Web.HttpContext.Current.Server.MapPath(this.GetRelativePath());
        }

        /// <summary>
        /// Get the relative file path for the MediaEntity.
        /// </summary>
        /// <returns>Relative file path of the MediaEntity.</returns>
        public string GetRelativePath()
        {
            string toReturn = String.Empty;

            if (this.FilePathRelativeToMediaPath.StartsWith("~"))
            {
                toReturn = this.FilePathRelativeToMediaPath;
            }
            else
            {
                // Get the Media path & the File Path and combine them
                //toReturn = Dionysos.StringUtil.CombineWithForwardSlash(Dionysos.ConfigurationManager.GetString(ExtraVestigingConfigConstants.MediaPathRelative), this.FilePathRelativeToMediaPath);
                toReturn = Dionysos.StringUtil.CombineWithForwardSlash("~/Media", this.FilePathRelativeToMediaPath);
            }

            toReturn = toReturn.Replace("\\", "/");

            return toReturn;
        }

        /// <summary>
        /// Gets the relative file path for the MediaEntity in the given width and height.
        /// </summary>
        /// <param name="width">The width of the MediaEntity.</param>
        /// <param name="height">The height of the MediaEntity.</param>
        /// <returns>Relative file path of the MediaEntity in the currect size.</returns>
        public string GetRelativeImagePath(int width, int height)
        {
            return this.GetRelativeImagePath(width, height, null, false);
        }

        /// <summary>
        /// Gets the relative image path for the MediaEntity in the specified width and height, considering any related the MediaRatios (by SystemName or Width and Height).
        /// </summary>
        /// <param name="width">The width of image to return.</param>
        /// <param name="height">The height of image to return</param>
        /// <param name="mediaRatioTypeSystemName">The SystemName of the MediaRatioType to use; if NULL, uses the width and height.</param>
        /// <returns>Relative image path of the resized image.</returns>
        public string GetRelativeImagePath(int width, int height, string mediaRatioTypeSystemName, bool overwrite)
        {
            string relativeImagePath = null;

            // Try to get a matching MediaRatioType from the database
            List<MediaRatioType> mediaRatioTypes = MediaRatioTypes.GetMediaRatioTypes(this.RelatedEntityName + "Entity", mediaRatioTypeSystemName, width, height);

            MediaRatioType mediaRatioType = null;

            // Set mediaRatioType
            if (mediaRatioTypes.Count == 1)
            {
                mediaRatioType = mediaRatioTypes[0];
            }

            // If matching MediaRatioType is found, get MediaRatio from database and compare it to the cache
            if (mediaRatioType != null)
            {
                MediaRatioTypeMediaCollection mediaRatios = new MediaRatioTypeMediaCollection();

                PredicateExpression filter = new PredicateExpression();

                // MediaId and MediaRatioTypeId
                filter.Add(MediaRatioTypeMediaFields.MediaId == this.MediaId);
                filter.Add(MediaRatioTypeMediaFields.MediaType == mediaRatioType.MediaType);

                mediaRatios.GetMulti(filter);

                // MediaRatio found
                if (mediaRatios.Count == 1)
                {
                    MediaRatioTypeMediaEntity mediaRatio = mediaRatios[0];

                    // Make sure we can get the physical paths
                    if (HttpContext.Current == null)
                    {
                        throw new NotImplementedException("Media resizing is only implemented for webapplications.");
                    }

                    // Get source and target FilePath
                    string sourceRelativeFilePath = this.GetRelativePath();
                    string sourcePhysicalFilePath = HttpContext.Current.Server.MapPath(sourceRelativeFilePath);

                    string targetRelativeFilePath = String.Format("{0}\\{1}-w{3}-h{4}{2}",
                                                                  Path.GetDirectoryName(sourceRelativeFilePath),
                                                                  Path.GetFileNameWithoutExtension(sourceRelativeFilePath),
                                                                  Path.GetExtension(sourceRelativeFilePath),
                                                                  width,
                                                                  height);
                    string targetPhysicalFilePath = HttpContext.Current.Server.MapPath(targetRelativeFilePath);

                    // Check cache to see if crop area is changed
                    bool recreateImage = true;
                    Rectangle mediaRatioRectangle = new Rectangle(mediaRatio.Left, mediaRatio.Top, mediaRatio.Width, mediaRatio.Height);

                    if (recreateImage || !File.Exists(targetPhysicalFilePath))
                    {
                        // Recreate image or file does not exist yet
                        try
                        {
                            ImageUtil.CropAndSave(sourcePhysicalFilePath, targetPhysicalFilePath, mediaRatioRectangle, new Size(width, height));
                        }
                        catch (Exception ex)
                        {
                            throw new TechnicalException(ex, "Crop and save using MediaRatio could not be performed for MediaId {0}.", this.MediaId);
                        }
                    }

                    // File exists, and if MediaRatio was changed, is now updated
                    relativeImagePath = targetRelativeFilePath;
                }
            }

            // If media is not resized using the MediaRatio, do it manually
            if (String.IsNullOrEmpty(relativeImagePath))
            {
                // Get source and target FilePath
                string sourceRelativeFilePath = this.GetRelativePath();
                string sourcePhysicalFilePath = HttpContext.Current.Server.MapPath(sourceRelativeFilePath);

                string targetRelativeFilePath = String.Format("{0}\\{1}-w{3}-h{4}{2}",
                                                              Path.GetDirectoryName(sourceRelativeFilePath),
                                                              Path.GetFileNameWithoutExtension(sourceRelativeFilePath),
                                                              Path.GetExtension(sourceRelativeFilePath),
                                                              width,
                                                              height);
                string targetPhysicalFilePath = HttpContext.Current.Server.MapPath(targetRelativeFilePath);

                if (!File.Exists(targetPhysicalFilePath) || overwrite)
                {
                    // Crop image from center
                    try
                    {
                        ImageUtil.ResizeAndSave(sourcePhysicalFilePath, targetPhysicalFilePath, ImageCropPosition.Center, new Size(width, height));
                    }
                    catch (Exception ex)
                    {
                        throw new TechnicalException(ex, "Crop and save could not be performed for MediaId {0}.", this.MediaId);
                    }

                    relativeImagePath = targetRelativeFilePath;
                }
                else
                {
                    // Use existing file
                    relativeImagePath = targetRelativeFilePath;
                }
            }

            return relativeImagePath;
        }

        /// <summary>
        /// Gets the relative file path for the MediaEntity in the given width and height with added margins
        /// </summary>
        /// <param name="width">The width of the MediaEntity</param>
        /// <param name="height">The height of the MediaEntity</param>
        /// <param name="margins">The margins to add to the MediaEntity</param>
        /// <param name="hexColor">The color the use as background color of the margins</param>
        /// <returns>
        /// Relative file path of the MediaEntity in the currect size and with added margins
        /// </returns>
        [Obsolete("This method uses depreciated methods of the ImageUtil.")]
        public string GetRelativeImagePath(int width, int height, Margins margins, string hexColor)
        {
            string toReturn = string.Empty;

            // Get clean hexColor
            hexColor = hexColor.Replace("#", string.Empty).ToLower();

            if (System.Web.HttpContext.Current == null)
            {
                new NotImplementedException("GetRelativeImagePath logica van MediaEntity is nog niet geimplementeerd voor niet-Web applicaties. (HttpContext == null)");
            }

            System.Web.HttpContext httpContext = System.Web.HttpContext.Current;
            string fileExtension = Path.GetExtension(this.FilePathRelativeToMediaPath);
            string imageSizeAndMarginsPostFix;
            if (margins != null && !margins.IsEmpty())
            {
                imageSizeAndMarginsPostFix = string.Format("-w{0}-h{1}-margins-{2}-{3}-{4}-{5}-bg-{6}{7}",
                                                           width, height, margins.Top, margins.Right, margins.Bottom, margins.Left, hexColor, fileExtension);
            }
            else
            {
                imageSizeAndMarginsPostFix = string.Format("-w{0}-h{1}{2}", width, height, fileExtension);
            }

            string relativeResizedFilePath = this.GetRelativePath();
            relativeResizedFilePath = StringUtil.ReplaceCaseInsensitive(relativeResizedFilePath, fileExtension, imageSizeAndMarginsPostFix);

            string physicalPathSource = this.GetPhysicalPath();
            string physicalPathResized = httpContext.Server.MapPath(relativeResizedFilePath);

            // first check in cache
            if (CacheHelper.HasValue(relativeResizedFilePath, false))
            {
                // is ok!
                toReturn = relativeResizedFilePath;
            }
            else if (File.Exists(physicalPathResized))
            {
                // is ok!
                //CacheHelper.AddAbsoluteExpire(false, relativeResizedFilePath, true, 600);
                toReturn = relativeResizedFilePath;
            }
            else
            {
                // check if the source file exists
                if (File.Exists(physicalPathSource))
                {
                    try
                    {
                        // We request it on orignal size if width and height are 0
                        if (width == 0 && height == 0)
                        {
                            Bitmap orgImage = new Bitmap(physicalPathSource);
                            width = orgImage.Width;
                            height = orgImage.Height;
                            orgImage.Dispose();
                        }

                        if (margins != null && !margins.IsEmpty())
                        {
                            // Margins								
                            Size targetSize = new Size(width, height);
                            if (width == 0 | height == 0)
                            {
                                Dionysos.Drawing.ImageUtil.ResizeImage(physicalPathSource, physicalPathResized, targetSize, margins, ColorUtil.ToColor(hexColor));
                            }
                            else
                            {
                                Dionysos.Drawing.ImageUtil.CropAndSaveImage(physicalPathSource, physicalPathResized, targetSize, margins, ColorUtil.ToColor(hexColor), false);
                            }
                        }
                        else if (width == 0 | height == 0)
                        {
                            // One size restricted only, resize
                            Dionysos.Drawing.ImageUtil.ResizeImage(physicalPathSource, physicalPathResized, width, height);
                        }
                        else
                        {
                            // Two sided restricted: try getting specified crop zone from MediaRatio; otherwise take the middle


                            Dionysos.Drawing.ImageUtil.CropAndSaveImage(physicalPathSource, physicalPathResized, width, height);
                        }

                        toReturn = relativeResizedFilePath;
                    }
                    catch
                    {
                        throw new Dionysos.TechnicalException("Resize could not be performed for Media {0}. (Incorrect size? File not found?)", this.MediaId);
                    }
                }
            }

            return toReturn;
        }

        /// <summary>
        /// Attaches the file from the supplied path to the MediaEntity and updates the meta information.
        /// </summary>
        /// <param name="path">The full physical path to the file.</param>
        /// <returns>true if the file is succesfuly attached to the MediaEntity; otherwise false.</returns>
        public bool Attach(string path)
        {
            using (Stream fileStream = File.OpenRead(path))
            {
                return this.Attach(fileStream, Path.GetFileName(path));
            }
        }

        /// <summary>
        /// Attaches the fileStream to the MediaEntity and updates the meta information.
        /// </summary>
        /// <param name="fileStream">The stream to attach.</param>
        /// <param name="fileName">The name of the file.</param>
        /// <returns>true if the stream is succesfuly attached to the MediaEntity; otherwise false.</returns>
        public bool Attach(Stream fileStream, string fileName)
        {
            if (this.RelatedEntity == null)
            {
                throw new Dionysos.TechnicalException("Attach can only be used when the MediaEntity is already attached to a RelatedEntity.");
            }

            // Related entity is set
            // MDB TODO
            //string physicalFilePath = HttpContext.Current.Server.MapPath(Dionysos.Global.ConfigurationProvider.GetString(ExtraVestigingConfigConstants.MediaPathRelative));
            string physicalFilePath = HttpContext.Current.Server.MapPath("~/Media");
            string relativeFilePath = String.Empty;

            // Check if root path exisits, if not, create
            if (!Directory.Exists(physicalFilePath))
            {
                Directory.CreateDirectory(physicalFilePath);
            }

            // Add subfolder that is supplied
            string subFolder = this.RelatedEntityName;
            if (!String.IsNullOrEmpty(subFolder))
            {
                physicalFilePath = Path.Combine(physicalFilePath, subFolder);
                relativeFilePath = subFolder;

                // Check if path (now with appended subFolder) exisits, if not, create
                if (!Directory.Exists(physicalFilePath))
                {
                    Directory.CreateDirectory(physicalFilePath);
                }
            }

            // Check if file exists, if it does rename it
            string fileNameWithoutExtension = Path.GetFileNameWithoutExtension(fileName).GenerateSlug();
            string fileExtension = Path.GetExtension(fileName);
            fileName = fileNameWithoutExtension + fileExtension;

            // Here we check if the file exists, if it does we try to append _0 untill _9999, then we give up :)
            int i = 0;
            while (File.Exists(Path.Combine(physicalFilePath, fileName)))
            {
                fileName = String.Format("{0}_{1}{2}", fileNameWithoutExtension, i, fileExtension);
                i++;
            }

            // Create the final physical & relative paths
            physicalFilePath = Path.Combine(physicalFilePath, fileName);
            relativeFilePath = Path.Combine(relativeFilePath, fileName);

            // We found a valid name to save the file.
            using (Stream output = File.OpenWrite(physicalFilePath))
            {
                fileStream.Copy(output);
            }

            FileInfo fi = new FileInfo(physicalFilePath);
            this.SizeKb = Convert.ToInt32(fi.Length) / 1024;
            this.FilePathRelativeToMediaPath = relativeFilePath;
            this.MimeType = MimeTypeHelper.GetMimeType(this.FilePathRelativeToMediaPath);

            return this.Save();
        }

        public const string FILENAME_FORMAT_TICKS = "[ticks]"; // MUST BE LOWER CASE!

        public string GetFileName(FileNameType type)
        {
            string fileName = this.MediaId.ToString() + "-";

            // Only need the first part.
            if (type == FileNameType.DeletionWildcard)
            {
                return fileName; // It's important that we have the dash, otherwise 1 would also delete 100 and 1000 and 1414, now just 1-
            }

            if (type == FileNameType.Cdn)
            {
                if (this.LastDistributedVersionTicks.HasValue)
                    fileName += this.LastDistributedVersionTicks.ToString();
                else
                    return string.Empty; // Don't return a file it wasn't distributed yet.                    
            }
            else if (type == FileNameType.Format)
            {
                fileName += FILENAME_FORMAT_TICKS;
            }

            if (this.FilePathRelativeToMediaPath.IsNullOrWhiteSpace())
                fileName += this.MediaId;
            else
                fileName += Path.GetExtension(this.FilePathRelativeToMediaPath);

            return fileName.ToLower();
        }
        
        public MediaRatioTypeMediaEntity.CdnStatus GetAmazonCdnStatus()
        {
            if (!this.LastDistributedVersionTicksAmazon.HasValue)
                return MediaRatioTypeMediaEntity.CdnStatus.NotAvailable;
            else if (this.LastDistributedVersionTicksAmazon.Value >= this.LastDistributedVersionTicks.Value)
                return MediaRatioTypeMediaEntity.CdnStatus.UpToDate;
            else
                return MediaRatioTypeMediaEntity.CdnStatus.OutOfdate;
        }

        public int? GetRelatedBrandId()
        {
            if (this._cachedRelatedBrandIdRetrieved)
                return this._cachedRelatedBrandId;

            int? toReturn = null;

            try
            {
                if (this.ProductId.HasValue)
                {
                    toReturn = this.ProductEntity.BrandId;
                    this._cachedRelatedBrandIdRetrieved = true;
                    
                }
                else if (this.AttachmentId.HasValue)
                {
                    toReturn = (this.AttachmentEntity.ProductId.HasValue && this.AttachmentEntity.ProductEntity.BrandId.HasValue) ? this.AttachmentEntity.ProductEntity.BrandId : null;
                    this._cachedRelatedBrandIdRetrieved = true;
                }                
            }
            catch
            {
                throw new ObymobiEntityException(MediaSaveResult.MissingOrUnsupportedRelatedEntity, this);
            }

            this._cachedRelatedBrandId = toReturn;
            return toReturn;
	    }

        public int? GetRelatedCompanyId()
        {
            if (this._cachedRelatedCompanyIdRetrieved)
                return this._cachedRelatedCompanyId;

            int? toReturn = null;

            // Set the correct CompanyId > Some might return null, which is ok, since it's generic then.
            try
            {
                if (this.CompanyId.HasValue)
                    toReturn = this.CompanyId;
                else if (this.ProductId.HasValue)
                    toReturn = this.ProductEntity.CompanyId;
                else if (this.CategoryId.HasValue)
                    toReturn = this.CategoryEntity.CompanyId;
                else if (this.AdvertisementId.HasValue)
                    toReturn = this.AdvertisementEntity.CompanyId;
                else if (this.EntertainmentId.HasValue)
                    toReturn = this.EntertainmentEntity.CompanyId;
                else if (this.AlterationoptionId.HasValue)
                    toReturn = this.AlterationoptionEntity.CompanyId;
                else if (this.GenericproductId.HasValue)
                    toReturn = null;
                else if (this.GenericcategoryId.HasValue)
                    toReturn = null;
                else if (this.DeliverypointgroupId.HasValue)
                    toReturn = this.DeliverypointgroupEntity.CompanyId;
                else if (this.SurveyId.HasValue)
                    toReturn = this.SurveyEntity.CompanyId;
                else if (this.SurveyPageId.HasValue)
                    toReturn = this.SurveyPageEntity.SurveyEntity.CompanyId;
                else if (this.ActionProductId.HasValue)
                    toReturn = this.ActionProductEntity.CompanyId;
                else if (this.ActionCategoryId.HasValue)
                    toReturn = this.ActionCategoryEntity.CompanyId;
                else if (this.ActionEntertainmentId.HasValue)
                    toReturn = this.ActionEntertainmentEntity.CompanyId;
                else if (this.ActionEntertainmentcategoryId.HasValue)
                    toReturn = null;
                else if (this.AlterationId.HasValue)
                    toReturn = this.AlterationEntity.CompanyId;
                else if (this.PointOfInterestId.HasValue)
                    toReturn = null;
                else if (this.RoutestephandlerId.HasValue)
                    toReturn = this.RoutestephandlerEntity.RoutestepEntity.RouteEntity.CompanyId;
                else if (this.PageElementId.HasValue)
                    toReturn = this.PageElementEntity.PageEntity.SiteEntity.CompanyId;
                else if (this.PageId.HasValue)
                    toReturn = this.PageEntity.SiteEntity.CompanyId;
                else if (this.SiteId.HasValue)
                    toReturn = this.SiteEntity.CompanyId;
                else if (this.AttachmentId.HasValue)
                    toReturn = this.AttachmentEntity.ProductId.HasValue ? this.AttachmentEntity.ProductEntity.CompanyId : this.AttachmentEntity.PageEntity.SiteEntity.CompanyId;
                else if (this.PageTemplateElementId.HasValue)
                    toReturn = null;
                else if (this.PageTemplateId.HasValue)
                    toReturn = null;
                else if (this.UIWidgetId.HasValue)
                    toReturn = this.UIWidgetEntity.UITabEntity.UIModeEntity.CompanyId;
                else if (this.UIThemeId.HasValue)
                    toReturn = this.UIThemeEntity.CompanyId;
                else if (this.RoomControlSectionId.HasValue)
                    toReturn = this.RoomControlSectionEntity.RoomControlAreaEntity.RoomControlConfigurationEntity.CompanyId;
                else if (this.RoomControlSectionItemId.HasValue)
                    toReturn = this.RoomControlSectionItemEntity.RoomControlSectionEntity.RoomControlAreaEntity.RoomControlConfigurationEntity.CompanyId;
                else if (this.StationId.HasValue)
                    toReturn = this.StationEntity.StationListEntity.CompanyId;
                else if (this.UIFooterItemId.HasValue)
                    toReturn = this.UIFooterItemEntity.UIModeEntity.CompanyId;
                else if (this.ProductgroupId.HasValue)
                    toReturn = this.ProductgroupEntity.CompanyId;
                else if (this.LandingPageId.HasValue)
                    toReturn = this.LandingPageEntity.ParentCompanyId;
                else if (this.CarouselItemId.HasValue)
                    toReturn = this.CarouselItemEntity.ParentCompanyId;
                else if (this.WidgetId.HasValue)
                {
                    if (!this.WidgetActionBannerEntity.IsNew)
                        toReturn = this.WidgetActionBannerEntity.ParentCompanyId;
                    else if (!this.WidgetHeroEntity.IsNew)
                        toReturn = this.WidgetHeroEntity.ParentCompanyId;
                }
                else if (this.ApplicationConfigurationId.HasValue)
                    toReturn = this.ApplicationConfigurationEntity.CompanyId;
                else if (this.LandingPageId.HasValue)
                    toReturn = this.LandingPageEntity.ParentCompanyId;
                else
                {
                    throw new ObymobiEntityException(MediaSaveResult.MissingOrUnsupportedRelatedEntity, this, "MediaEntity.GetRelatedCompanyId");
                }
            }
            catch
            {
                throw new ObymobiEntityException(MediaSaveResult.MissingOrUnsupportedRelatedEntity, this);
            }

            this._cachedRelatedCompanyIdRetrieved = true;
            this._cachedRelatedCompanyId = toReturn;

            return toReturn;
        }

        #endregion

        // __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included Code

		#endregion
	}
}
