﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'IcrtouchprintermappingDeliverypoint'. This class is used for Business Logic or for framework extension code.</summary>
	[Serializable]
	public partial class IcrtouchprintermappingDeliverypointEntity : IcrtouchprintermappingDeliverypointEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
        , ICompanyRelatedChildEntity
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		/// <summary>CTor</summary>
		public IcrtouchprintermappingDeliverypointEntity():base()
		{
		}

		/// <summary>CTor</summary>
		/// <param name="icrtouchprintermappingDeliverypointId">PK value for IcrtouchprintermappingDeliverypoint which data should be fetched into this IcrtouchprintermappingDeliverypoint object</param>
		public IcrtouchprintermappingDeliverypointEntity(System.Int32 icrtouchprintermappingDeliverypointId):
			base(icrtouchprintermappingDeliverypointId)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="icrtouchprintermappingDeliverypointId">PK value for IcrtouchprintermappingDeliverypoint which data should be fetched into this IcrtouchprintermappingDeliverypoint object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public IcrtouchprintermappingDeliverypointEntity(System.Int32 icrtouchprintermappingDeliverypointId, IPrefetchPath prefetchPathToUse):
			base(icrtouchprintermappingDeliverypointId, prefetchPathToUse)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="icrtouchprintermappingDeliverypointId">PK value for IcrtouchprintermappingDeliverypoint which data should be fetched into this IcrtouchprintermappingDeliverypoint object</param>
		/// <param name="validator">The custom validator object for this IcrtouchprintermappingDeliverypointEntity</param>
		public IcrtouchprintermappingDeliverypointEntity(System.Int32 icrtouchprintermappingDeliverypointId, IValidator validator):
			base(icrtouchprintermappingDeliverypointId, validator)
		{
		}
		
		/// <summary>CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected IcrtouchprintermappingDeliverypointEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}

		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode

        public CommonEntityBase Parent
        {
            get { return this.IcrtouchprintermappingEntity; }
        }

		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included Code

		#endregion
	}
}
