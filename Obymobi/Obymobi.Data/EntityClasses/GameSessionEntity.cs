﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    using System.Xml.Serialization;
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'GameSession'. This class is used for Business Logic or for framework extension code.</summary>
	[Serializable]
	public partial class GameSessionEntity : GameSessionEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
        ,ICompanyRelatedChildEntity
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		/// <summary>CTor</summary>
		public GameSessionEntity():base()
		{
		}

		/// <summary>CTor</summary>
		/// <param name="gameSessionId">PK value for GameSession which data should be fetched into this GameSession object</param>
		public GameSessionEntity(System.Int32 gameSessionId):
			base(gameSessionId)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="gameSessionId">PK value for GameSession which data should be fetched into this GameSession object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public GameSessionEntity(System.Int32 gameSessionId, IPrefetchPath prefetchPathToUse):
			base(gameSessionId, prefetchPathToUse)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="gameSessionId">PK value for GameSession which data should be fetched into this GameSession object</param>
		/// <param name="validator">The custom validator object for this GameSessionEntity</param>
		public GameSessionEntity(System.Int32 gameSessionId, IValidator validator):
			base(gameSessionId, validator)
		{
		}
		
		/// <summary>CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected GameSessionEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}

		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode

        [XmlIgnore]
        public CommonEntityBase Parent
        {
            get { return this.ClientEntity; }
        }

        [XmlIgnore]
        [EntityFieldDependency((int)GameSessionFieldIndex.EndUTC)]
        public bool IsActive
        {
            get
            {
                // If the end of the last session is less than 90 seconds ago, we consider the session active
                return (DateTime.UtcNow - this.EndUTC).TotalSeconds < 90; 
            }
        }

		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included Code

		#endregion
	}
}
