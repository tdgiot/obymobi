﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'ViewCustom'. This class is used for Business Logic or for framework extension code.</summary>
	[Serializable]
	public partial class ViewCustomEntity : ViewCustomEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
        , Dionysos.Interfaces.Data.IView, ISystemEntity
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		/// <summary>CTor</summary>
		public ViewCustomEntity():base()
		{
		}

		/// <summary>CTor</summary>
		/// <param name="viewCustomId">PK value for ViewCustom which data should be fetched into this ViewCustom object</param>
		public ViewCustomEntity(System.Int32 viewCustomId):
			base(viewCustomId)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="viewCustomId">PK value for ViewCustom which data should be fetched into this ViewCustom object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public ViewCustomEntity(System.Int32 viewCustomId, IPrefetchPath prefetchPathToUse):
			base(viewCustomId, prefetchPathToUse)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="viewCustomId">PK value for ViewCustom which data should be fetched into this ViewCustom object</param>
		/// <param name="validator">The custom validator object for this ViewCustomEntity</param>
		public ViewCustomEntity(System.Int32 viewCustomId, IValidator validator):
			base(viewCustomId, validator)
		{
		}
		
		/// <summary>CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected ViewCustomEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}

		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode

        #region Methods

        /// <summary>
        /// Adds a ViewItem to this View instance
        /// </summary>
        /// <param name="fieldName">The name of the view of the view item</param>
        /// <param name="friendlyName">The friendly name of the view item</param>
        /// <param name="type">The type string of the view item</param>
        /// <param name="uiElementTypeNameFull">The full type name of the UI element</param>
        /// <returns>True if adding the view item was succesful, False if not</returns>
        public bool AddViewItem(string fieldName, string type, string displayFormatString, string uiElementTypeNameFull)
        {
            ViewItemCustomEntity viewItem = new ViewItemCustomEntity();
            viewItem.ViewCustomId = this.ViewCustomId;
            viewItem.EntityName = this.EntityName;
            viewItem.FieldName = fieldName;
            viewItem.ShowOnGridView = true;
            viewItem.DisplayFormatString = displayFormatString;
            viewItem.DisplayPosition = 10000;
            viewItem.Type = type;
            viewItem.UiElementTypeNameFull = uiElementTypeNameFull;

            return viewItem.Save();
        }

        /// <summary>
        /// Removes a view item from the view
        /// </summary>
        /// <param name="fieldName">The name of the field from the view item to remove</param>
        /// <returns>True if the removal was succesful, false if not</returns>
        public bool RemoveViewItem(string fieldName)
        {
            bool success = false;

            for (int i = 0; i < this.ViewItemCustomCollection.Count; i++)
            {
                ViewItemCustomEntity viewItem = this.ViewItemCustomCollection[i];
                if (!Instance.Empty(viewItem) && viewItem.FieldName == fieldName)
                {
                    success = viewItem.Delete();
                    this.ViewItemCustomCollection.Remove(viewItem);
                    break;
                }
            }

            return success;
        }

        /// <summary>
        /// Check whether a ViewItem for the specified fieldname already exists
        /// </summary>
        /// <param name="fieldName">The name of the field</param>
        /// <returns>True if the ViewItem already exists, False if not</returns>
        public bool ContainsViewItem(string fieldName)
        {
            bool exists = false;

            for (int i = 0; i < this.ViewItemCustomCollection.Count; i++)
            {
                ViewItemCustomEntity viewItem = this.ViewItemCustomCollection[i];
                if (!Instance.Empty(viewItem) && viewItem.FieldName == fieldName)
                {
                    exists = true;
                    break;
                }
            }

            return exists;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets the view items
        /// </summary>
        public IEntityCollection ViewItems
        {
            get
            {
                return this.ViewItemCustomCollection;
            }
        }

        public bool IsCustomView => true;

        #endregion

        // __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included Code

		#endregion
	}
}
