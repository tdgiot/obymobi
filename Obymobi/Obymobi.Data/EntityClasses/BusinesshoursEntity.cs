﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    using Dionysos.Data;
    using System.Xml.Serialization;
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'Businesshours'. This class is used for Business Logic or for framework extension code.</summary>
	[Serializable]
	public partial class BusinesshoursEntity : BusinesshoursEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
        , INullableCompanyRelatedEntity
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		/// <summary>CTor</summary>
		public BusinesshoursEntity():base()
		{
		}

		/// <summary>CTor</summary>
		/// <param name="businesshoursId">PK value for Businesshours which data should be fetched into this Businesshours object</param>
		public BusinesshoursEntity(System.Int32 businesshoursId):
			base(businesshoursId)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="businesshoursId">PK value for Businesshours which data should be fetched into this Businesshours object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public BusinesshoursEntity(System.Int32 businesshoursId, IPrefetchPath prefetchPathToUse):
			base(businesshoursId, prefetchPathToUse)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="businesshoursId">PK value for Businesshours which data should be fetched into this Businesshours object</param>
		/// <param name="validator">The custom validator object for this BusinesshoursEntity</param>
		public BusinesshoursEntity(System.Int32 businesshoursId, IValidator validator):
			base(businesshoursId, validator)
		{
		}
		
		/// <summary>CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected BusinesshoursEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}

		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode

        [DataGridViewColumnVisible]
        [XmlIgnore]
        public string DayName
        {
            get
            {
                string dayName = "Unknown";
                try
                {
                    int day = int.Parse(this.DayOfWeekAndTime.Substring(0, 1));
                    switch(day)
                    {
                        case 0:
                            dayName = Dionysos.Global.TranslationProvider.GetTranslation(Dionysos.Globalization.GenericTranslatables.Sunday);
                            break;

                        case 1:
                            dayName = Dionysos.Global.TranslationProvider.GetTranslation(Dionysos.Globalization.GenericTranslatables.Monday);
                            break;

                        case 2:
                            dayName = Dionysos.Global.TranslationProvider.GetTranslation(Dionysos.Globalization.GenericTranslatables.Tuesday);
                            break;

                        case 3:
                            dayName = Dionysos.Global.TranslationProvider.GetTranslation(Dionysos.Globalization.GenericTranslatables.Wednesday);
                            break;

                        case 4:
                            dayName = Dionysos.Global.TranslationProvider.GetTranslation(Dionysos.Globalization.GenericTranslatables.Thursday);
                            break;

                        case 5:
                            dayName = Dionysos.Global.TranslationProvider.GetTranslation(Dionysos.Globalization.GenericTranslatables.Friday);
                            break;

                        case 6:
                            dayName = Dionysos.Global.TranslationProvider.GetTranslation(Dionysos.Globalization.GenericTranslatables.Saturday);
                            break;
                    }
                }
                catch
                {
                }
                return dayName;
            }
        }

        [DataGridViewColumnVisible]
        [XmlIgnore]
        public string TimeString
        {
            get
            {
                string timeString = "00:00";
                try
                {
                    timeString = this.DayOfWeekAndTime.Substring(1, 2) + ":" + this.DayOfWeekAndTime.Substring(3, 2);
                }
                catch 
                {
                }
                return timeString;
            }
        }

		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included Code

		#endregion
	}
}
