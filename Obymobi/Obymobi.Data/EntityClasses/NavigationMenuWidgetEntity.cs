﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'NavigationMenuWidget'. This class is used for Business Logic or for framework extension code.</summary>
	[Serializable]
	public partial class NavigationMenuWidgetEntity : NavigationMenuWidgetEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		,ICompanyRelatedChildEntity
	// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		/// <summary>CTor</summary>
		public NavigationMenuWidgetEntity():base()
		{
		}

		/// <summary>CTor</summary>
		/// <param name="navigationMenuId">PK value for NavigationMenuWidget which data should be fetched into this NavigationMenuWidget object</param>
		/// <param name="widgetId">PK value for NavigationMenuWidget which data should be fetched into this NavigationMenuWidget object</param>
		public NavigationMenuWidgetEntity(System.Int32 navigationMenuId, System.Int32 widgetId):
			base(navigationMenuId, widgetId)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="navigationMenuId">PK value for NavigationMenuWidget which data should be fetched into this NavigationMenuWidget object</param>
		/// <param name="widgetId">PK value for NavigationMenuWidget which data should be fetched into this NavigationMenuWidget object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public NavigationMenuWidgetEntity(System.Int32 navigationMenuId, System.Int32 widgetId, IPrefetchPath prefetchPathToUse):
			base(navigationMenuId, widgetId, prefetchPathToUse)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="navigationMenuId">PK value for NavigationMenuWidget which data should be fetched into this NavigationMenuWidget object</param>
		/// <param name="widgetId">PK value for NavigationMenuWidget which data should be fetched into this NavigationMenuWidget object</param>
		/// <param name="validator">The custom validator object for this NavigationMenuWidgetEntity</param>
		public NavigationMenuWidgetEntity(System.Int32 navigationMenuId, System.Int32 widgetId, IValidator validator):
			base(navigationMenuId, widgetId, validator)
		{
		}
		
		/// <summary>CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected NavigationMenuWidgetEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}

		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		public CommonEntityBase Parent => this.NavigationMenuEntity;

        public string Name => this.WidgetEntity.Name;

        // __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included Code

		#endregion
	}
}
