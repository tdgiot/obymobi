﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    using System.Xml.Serialization;
    // __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'Posproduct'. This class is used for Business Logic or for framework extension code.</summary>
	[Serializable]
	public partial class PosproductEntity : PosproductEntityBase
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
        , ICompanyRelatedEntity
    // __LLBLGENPRO_USER_CODE_REGION_END	
	{
		/// <summary>CTor</summary>
		public PosproductEntity():base()
		{
		}

		/// <summary>CTor</summary>
		/// <param name="posproductId">PK value for Posproduct which data should be fetched into this Posproduct object</param>
		public PosproductEntity(System.Int32 posproductId):
			base(posproductId)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="posproductId">PK value for Posproduct which data should be fetched into this Posproduct object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public PosproductEntity(System.Int32 posproductId, IPrefetchPath prefetchPathToUse):
			base(posproductId, prefetchPathToUse)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="posproductId">PK value for Posproduct which data should be fetched into this Posproduct object</param>
		/// <param name="validator">The custom validator object for this PosproductEntity</param>
		public PosproductEntity(System.Int32 posproductId, IValidator validator):
			base(posproductId, validator)
		{
		}
		
		/// <summary>CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected PosproductEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
            // __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
            // __LLBLGENPRO_USER_CODE_REGION_END
		}

		#region Custom Entity code
		
        // __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode

        string posCategoryName = null;

        [XmlIgnore]
        public string PosCategoryName
        {
            get
            {
                if (posCategoryName == null)
                    throw new Dionysos.TechnicalException("PosProduct.PosCategoryName can't be called with it being intiliazied manually. (Does not support lazy loading)");
                else
                    return posCategoryName;
            }
            set
            {
                this.posCategoryName = value;
            }
        }


        /// <summary>
        /// Gets the name and external id of the Posproduct
        /// </summary>
        [XmlIgnore]
        public string NameAndExternalId
        {
            get
            {
                return string.Format("{0} ({1})", this.Name, this.ExternalId);
            }
        }

        // __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included Code

		#endregion
	}
}
