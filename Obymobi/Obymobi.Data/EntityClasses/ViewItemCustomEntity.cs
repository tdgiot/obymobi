﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'ViewItemCustom'. This class is used for Business Logic or for framework extension code.</summary>
	[Serializable]
	public partial class ViewItemCustomEntity : ViewItemCustomEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
        , Dionysos.Interfaces.Data.IViewItem, ISystemEntity
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		/// <summary>CTor</summary>
		public ViewItemCustomEntity():base()
		{
		}

		/// <summary>CTor</summary>
		/// <param name="viewItemCustomId">PK value for ViewItemCustom which data should be fetched into this ViewItemCustom object</param>
		public ViewItemCustomEntity(System.Int32 viewItemCustomId):
			base(viewItemCustomId)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="viewItemCustomId">PK value for ViewItemCustom which data should be fetched into this ViewItemCustom object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public ViewItemCustomEntity(System.Int32 viewItemCustomId, IPrefetchPath prefetchPathToUse):
			base(viewItemCustomId, prefetchPathToUse)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="viewItemCustomId">PK value for ViewItemCustom which data should be fetched into this ViewItemCustom object</param>
		/// <param name="validator">The custom validator object for this ViewItemCustomEntity</param>
		public ViewItemCustomEntity(System.Int32 viewItemCustomId, IValidator validator):
			base(viewItemCustomId, validator)
		{
		}
		
		/// <summary>CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected ViewItemCustomEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}

		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included Code

		#endregion
	}
}
