﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    using Dionysos.Interfaces;
    using Dionysos.Web;
    using Obymobi.Data.HelperClasses;
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'UIElement'. This class is used for Business Logic or for framework extension code.</summary>
	[Serializable]
	public partial class UIElementEntity : UIElementEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
        , ILicenseUIElement, ISystemEntity
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		/// <summary>CTor</summary>
		public UIElementEntity():base()
		{
		}

		/// <summary>CTor</summary>
		/// <param name="uIElementId">PK value for UIElement which data should be fetched into this UIElement object</param>
		public UIElementEntity(System.Int32 uIElementId):
			base(uIElementId)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="uIElementId">PK value for UIElement which data should be fetched into this UIElement object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public UIElementEntity(System.Int32 uIElementId, IPrefetchPath prefetchPathToUse):
			base(uIElementId, prefetchPathToUse)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="uIElementId">PK value for UIElement which data should be fetched into this UIElement object</param>
		/// <param name="validator">The custom validator object for this UIElementEntity</param>
		public UIElementEntity(System.Int32 uIElementId, IValidator validator):
			base(uIElementId, validator)
		{
		}
		
		/// <summary>CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected UIElementEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}

		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode

        #region Methods

        /// <summary>
        /// Get a UIElementEntity based on it's full TypeName
        /// </summary>
        /// <param name="fullTypeName">TypeName of the UIElement</param>
        /// <param name="onlyReturnLicensedElements">Bool indicating if any match, or only licensed matches should be returned.</param>
        /// <returns>The found UIElement or null on 0 or > 1 results</returns>
        public static UIElementEntity GetUIElementByTypeName(string fullTypeName, bool onlyReturnLicensedElements)
        {
            UIElementEntity uiElement = null;

            var filter = new PredicateExpression();
            filter.Add(UIElementFields.TypeNameFull == fullTypeName);

            UIElementCollection uielements = new UIElementCollection();
            uielements.GetMulti(filter);

            if (uielements.Count == 1)
                uiElement = uielements[0];

            return uiElement;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets the flag which indicates if the ui element is licensed
        /// </summary>
        public bool IsLicensed
        {
            get
            {
                if (this.LicensingFree)
                {
                    return true;
                }
                else
                {
                    bool toReturn = false;

                    string cacheKey = "UIElementEntity.IsLicensed." + this.UIElementId.ToString();
                    if (!CacheHelper.TryGetValue(cacheKey, false, out toReturn))
                    {
                        var filterUielementLicenses = new PredicateExpression();
                        filterUielementLicenses.Add(LicensedUIElementFields.UiElementTypeNameFull == this.TypeNameFull);

                        LicensedUIElementEntity licensedUiElementEntity = new LicensedUIElementEntity();
                        licensedUiElementEntity = Dionysos.Data.LLBLGen.LLBLGenEntityUtil.GetSingleEntityUsingPredicateExpression(filterUielementLicenses, licensedUiElementEntity) as LicensedUIElementEntity;

                        if (licensedUiElementEntity == null)
                        {
                            toReturn = false;
                        }
                        else
                        {
                            toReturn = licensedUiElementEntity.IsLicensed;
                        }

                        CacheHelper.AddSlidingExpire(false, cacheKey, toReturn, 120);
                    }

                    return toReturn;
                }
            }
        }

        private LicensedUIElementEntity licensedUIElementEntity = null;
        public LicensedUIElementEntity LicensedUIElementEntity
        {
            get
            {
                if (licensedUIElementEntity == null)
                {
                    var filterUielements = new PredicateExpression(LicensedUIElementFields.UiElementTypeNameFull == this.TypeNameFull);
                    this.licensedUIElementEntity = new LicensedUIElementEntity();
                    this.licensedUIElementEntity = Dionysos.Data.LLBLGen.LLBLGenEntityUtil.GetSingleEntityUsingPredicateExpression(filterUielements, this.licensedUIElementEntity) as LicensedUIElementEntity;

                    if (this.licensedUIElementEntity == null)
                    {
                        this.licensedUIElementEntity = new LicensedUIElementEntity();
                        this.licensedUIElementEntity.UiElementTypeNameFull = this.TypeNameFull;
                        this.licensedUIElementEntity.Save();
                    }
                }


                return this.licensedUIElementEntity;
            }
        }

        #endregion

		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included Code

		#endregion
	}
}
