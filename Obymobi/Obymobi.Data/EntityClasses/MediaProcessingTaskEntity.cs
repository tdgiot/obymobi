﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    using Dionysos;
    using System.Xml.Serialization;
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'MediaProcessingTask'. This class is used for Business Logic or for framework extension code.</summary>
	[Serializable]
	public partial class MediaProcessingTaskEntity : MediaProcessingTaskEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
        , INonCompanyRelatedEntity
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		/// <summary>CTor</summary>
		public MediaProcessingTaskEntity():base()
		{
		}

		/// <summary>CTor</summary>
		/// <param name="mediaProcessingTaskId">PK value for MediaProcessingTask which data should be fetched into this MediaProcessingTask object</param>
		public MediaProcessingTaskEntity(System.Int32 mediaProcessingTaskId):
			base(mediaProcessingTaskId)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="mediaProcessingTaskId">PK value for MediaProcessingTask which data should be fetched into this MediaProcessingTask object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public MediaProcessingTaskEntity(System.Int32 mediaProcessingTaskId, IPrefetchPath prefetchPathToUse):
			base(mediaProcessingTaskId, prefetchPathToUse)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="mediaProcessingTaskId">PK value for MediaProcessingTask which data should be fetched into this MediaProcessingTask object</param>
		/// <param name="validator">The custom validator object for this MediaProcessingTaskEntity</param>
		public MediaProcessingTaskEntity(System.Int32 mediaProcessingTaskId, IValidator validator):
			base(mediaProcessingTaskId, validator)
		{
		}
		
		/// <summary>CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected MediaProcessingTaskEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}

		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode

        public enum ProcessingAction 
        { 
            Unknown = 0,
            Upload = 1,
            Delete = 2
        }

        [XmlIgnore]
        public ProcessingAction ActionAsEnum
        {
            get
            {
                if (this.Action.HasValue)
                {
                    return this.Action.Value.ToEnum<ProcessingAction>();
                }
                else
                    return ProcessingAction.Unknown;
            }
            set
            {
                this.Action = (int)value;
            }
        }

        [XmlIgnore]
        public string RelatedMediaRelatedEntityNameAndShowFieldValue
        {
            get
            {
                string toReturn = "N/A";
                if (this.MediaRatioTypeMediaEntity != null &&
                    !this.MediaRatioTypeMediaEntity.IsNew)
                {
                    toReturn = this.MediaRatioTypeMediaEntity.MediaEntity.RelatedEntityNameAndShowFieldValue;
                }
                return toReturn;
            }
        }

        [XmlIgnore]
        public string RelatedMediaRelatedEntityEditPageRelativeLink
        {
            get
            {
                string toReturn = string.Empty;
                if (this.MediaRatioTypeMediaEntity != null &&
                    !this.MediaRatioTypeMediaEntity.IsNew)
                {
                    toReturn = this.MediaRatioTypeMediaEntity.MediaEntity.RelatedEntityEditPageRelativeLink;
                }
                return toReturn;
            }
        }

        [XmlIgnore]
        public int RelatedMediaId
        {
            get
            {
                int toReturn = -1;
                if (this.MediaRatioTypeMediaEntity != null &&
                    !this.MediaRatioTypeMediaEntity.IsNew)
                {
                    toReturn = this.MediaRatioTypeMediaEntity.MediaEntity.MediaId;
                }
                return toReturn;
            }
        }

        [XmlIgnore]
        public string MediaRatioTypeText
        {
            get
            {
                string toReturn = string.Empty;
                if (this.MediaRatioTypeMediaEntity != null &&
                    !this.MediaRatioTypeMediaEntity.IsNew)
                {
                    toReturn = this.MediaRatioTypeMediaEntity.MediaRatioType.Name;
                }
                return toReturn;
            }
        }

        const string DATE_FORMAT = "yyyy/MM/dd hh:mm:ss";
        
        [XmlIgnore]
        public string CompletedOnAmazonText
        {
            get
            {
                if (this.CompletedOnAmazonUTC.HasValue)
                    return this.CompletedOnAmazonUTC.Value.ToString(DATE_FORMAT);
                else
                    return "Pending";
            }
        }

        [XmlIgnore]
        public string NextAttemptText
        {
            get
            {
                if (this.NextAttemptUTC.HasValue)
                    return this.NextAttemptUTC.Value.ToString(DATE_FORMAT);
                else
                    return "None";
            }
        }


		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included Code

		#endregion
	}
}
