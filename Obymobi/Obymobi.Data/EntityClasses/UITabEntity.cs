﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    using System.Xml.Serialization;
    using Obymobi.Enums;
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'UITab'. This class is used for Business Logic or for framework extension code.</summary>
	[Serializable]
	public partial class UITabEntity : UITabEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
        , INullableCompanyRelatedChildEntity, ICustomTextContainingEntity, ITimestampEntity
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		/// <summary>CTor</summary>
		public UITabEntity():base()
		{
		}

		/// <summary>CTor</summary>
		/// <param name="uITabId">PK value for UITab which data should be fetched into this UITab object</param>
		public UITabEntity(System.Int32 uITabId):
			base(uITabId)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="uITabId">PK value for UITab which data should be fetched into this UITab object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public UITabEntity(System.Int32 uITabId, IPrefetchPath prefetchPathToUse):
			base(uITabId, prefetchPathToUse)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="uITabId">PK value for UITab which data should be fetched into this UITab object</param>
		/// <param name="validator">The custom validator object for this UITabEntity</param>
		public UITabEntity(System.Int32 uITabId, IValidator validator):
			base(uITabId, validator)
		{
		}
		
		/// <summary>CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected UITabEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}

		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode

        [XmlIgnore]
        public CommonEntityBase Parent
        {
            get { return this.UIModeEntity; }
        }


        [DataGridViewColumnVisible]
        [XmlIgnore]
        public string TypeName
        {
            get
            {
                if (this.Type > 0)
                {
                    return Enum.GetName(typeof(UITabType), this.Type);
                }
                return "Unknown";
            }
        }

        [XmlIgnore]
        public UITabType TypeAsEnum
        {
            get
            {
                UITabType toReturn = UITabType.Unknown;

                if (this.Type > 0)
                    toReturn = this.Type.ToEnum<UITabType>();

                return toReturn;
            }
            set
            {
                this.Type = (int)value;
            }
        }

        [XmlIgnore]
        public bool IsStandardConsoleTab
        {
            get
            {
                bool toReturn = false;

                if (this.TypeAsEnum == UITabType.Orders ||
                    this.TypeAsEnum == UITabType.Message ||
                    this.TypeAsEnum == UITabType.Management ||
                    this.TypeAsEnum == UITabType.Socialmedia ||
                    this.TypeAsEnum == UITabType.TerminalStatus ||
                    this.TypeAsEnum == UITabType.OrderCount || 
                    this.TypeAsEnum == UITabType.MasterOrders)
                    toReturn = true;

                return toReturn;
            }
        }

        [DataGridViewColumnVisible]
        [XmlIgnore]
        public string CaptionOrType
        {
            get
            {
                string toReturn = this.Caption;

                if (this.IsStandardConsoleTab)
                    toReturn = this.TypeName;

                return toReturn;
            }
        }

		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included Code

		#endregion
	}
}
