﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'VenueCategoryLanguage'. This class is used for Business Logic or for framework extension code.</summary>
	[Serializable]
	public partial class VenueCategoryLanguageEntity : VenueCategoryLanguageEntityBase
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
    , INonCompanyRelatedEntity
    // __LLBLGENPRO_USER_CODE_REGION_END	
	{
		/// <summary>CTor</summary>
		public VenueCategoryLanguageEntity():base()
		{
		}

		/// <summary>CTor</summary>
		/// <param name="venueCategoryLanguageId">PK value for VenueCategoryLanguage which data should be fetched into this VenueCategoryLanguage object</param>
		public VenueCategoryLanguageEntity(System.Int32 venueCategoryLanguageId):
			base(venueCategoryLanguageId)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="venueCategoryLanguageId">PK value for VenueCategoryLanguage which data should be fetched into this VenueCategoryLanguage object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public VenueCategoryLanguageEntity(System.Int32 venueCategoryLanguageId, IPrefetchPath prefetchPathToUse):
			base(venueCategoryLanguageId, prefetchPathToUse)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="venueCategoryLanguageId">PK value for VenueCategoryLanguage which data should be fetched into this VenueCategoryLanguage object</param>
		/// <param name="validator">The custom validator object for this VenueCategoryLanguageEntity</param>
		public VenueCategoryLanguageEntity(System.Int32 venueCategoryLanguageId, IValidator validator):
			base(venueCategoryLanguageId, validator)
		{
		}
		
		/// <summary>CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected VenueCategoryLanguageEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}

		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included Code

		#endregion
	}
}
