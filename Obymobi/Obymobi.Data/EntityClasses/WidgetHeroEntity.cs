﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    using Obymobi.Enums;
    // __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'WidgetHero'. This class is used for Business Logic or for framework extension code.</summary>
	[Serializable]
	public partial class WidgetHeroEntity : WidgetHeroEntityBase
        // __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
        , ICompanyRelatedChildEntity, IMediaContainingEntity
        // __LLBLGENPRO_USER_CODE_REGION_END	
	{
		/// <summary>CTor</summary>
		public WidgetHeroEntity():base()
		{
		}

		/// <summary>CTor</summary>
		/// <param name="widgetId">PK value for WidgetHero which data should be fetched into this WidgetHero object</param>
		public WidgetHeroEntity(System.Int32 widgetId):
			base(widgetId)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="widgetId">PK value for WidgetHero which data should be fetched into this WidgetHero object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public WidgetHeroEntity(System.Int32 widgetId, IPrefetchPath prefetchPathToUse):
			base(widgetId, prefetchPathToUse)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="widgetId">PK value for WidgetHero which data should be fetched into this WidgetHero object</param>
		/// <param name="validator">The custom validator object for this WidgetHeroEntity</param>
		public WidgetHeroEntity(System.Int32 widgetId, IValidator validator):
			base(widgetId, validator)
		{
		}
		
		/// <summary>CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected WidgetHeroEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}

		#region Custom Entity code
		
        // __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode

        public override ApplessWidgetType Type { get { return ApplessWidgetType.Hero; } }

        // __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included Code

		#endregion
	}
}
