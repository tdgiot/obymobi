﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    using Obymobi.Logic.Cms;
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;
    using Dionysos;
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'Page'. This class is used for Business Logic or for framework extension code.</summary>
	[Serializable]
	public partial class PageEntity : PageEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
        , Obymobi.Logic.Cms.IPage, INullableCompanyRelatedChildEntity, IMediaContainingEntity, ICustomTextContainingEntity, ITimestampEntity
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		/// <summary>CTor</summary>
		public PageEntity():base()
		{
		}

		/// <summary>CTor</summary>
		/// <param name="pageId">PK value for Page which data should be fetched into this Page object</param>
		public PageEntity(System.Int32 pageId):
			base(pageId)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="pageId">PK value for Page which data should be fetched into this Page object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public PageEntity(System.Int32 pageId, IPrefetchPath prefetchPathToUse):
			base(pageId, prefetchPathToUse)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="pageId">PK value for Page which data should be fetched into this Page object</param>
		/// <param name="validator">The custom validator object for this PageEntity</param>
		public PageEntity(System.Int32 pageId, IValidator validator):
			base(pageId, validator)
		{
		}
		
		/// <summary>CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected PageEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor                        
			// __LLBLGENPRO_USER_CODE_REGION_END
		}

		#region Custom Entity code
		
        // __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode

        #region Generic handling for Pages/PageTemplates

        public int PrimaryId
        {
            get { return this.PageId; }
            set { this.PageId = value; }
        }

        [XmlIgnore]
        public PageType PageTypeAsEnum
        {
            get
            {
                return this.PageType.ToEnum<PageType>();
            }
            set
            {
                this.PageType = (int)value;
            }
        }

        [CLSCompliant(false)]
        public List<IPageTypeElementData> GetPageTypeDataSource(string cultureCode)
        {
            List<IPageTypeElementData> toReturn = null;

            if (this.PageElementCollection.Count > 0)
            {
                IEnumerable<PageElementEntity> elements;
                
                if (cultureCode.IsNullOrWhiteSpace())
                    elements = this.PageElementCollection.Where(x => x.CultureCode.IsNullOrWhiteSpace());
                else
                    elements = this.PageElementCollection.Where(x => x.CultureCode.Equals(cultureCode, StringComparison.InvariantCultureIgnoreCase));

                toReturn = elements.ToList<IPageTypeElementData>();
            }
            else
            {
                toReturn = new List<IPageTypeElementData>();
            }

            return toReturn;
        }

        public IPage PageTemplate
        {
            get { return base.PageTemplateEntity as IPage; }
        }

	    public List<IPage> IPageCollection
	    {
	        get { return base.PageCollection.ToList<IPage>(); }
	    }

	    public List<IPageTypeElementData> IPageElementCollection
        {
            get
            {
                return base.PageElementCollection.ToList<IPageTypeElementData>();
            }
        }

        #endregion

        [XmlIgnore]
        public CommonEntityBase Parent
        {
            get { return this.SiteEntity; }
        }

	    public bool ValidatorCreateDefaultLanguageAgnosticElements { get; set; }
	    public bool ValidatorCreateOrUpdateDefaultPageLanguage { get; set; }

        public bool NameChanged { get; set; }

	    [DataGridViewColumnVisibleAttribute]
        [XmlIgnore]
        public string SiteNamePageName
        {
            get
            {
                return string.Format("{0} - {1}", this.SiteEntity.Name, this.Name);
            }
        }

	    [XmlIgnore]
	    public override string Name
	    {
	        get 
            {
                var toReturn = base.Name;

                if (toReturn.IsNullOrWhiteSpace() && this.PageTemplateId.HasValue)
                    toReturn = this.PageTemplateEntity.Name;

                return toReturn;
            }
	        set { base.Name = value; }
	    }	    

	    // __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included Code

		#endregion
	}
}
