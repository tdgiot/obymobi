﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    using Dionysos;
    using Obymobi.Data.HelperClasses;
    using System.Xml.Serialization;
    using Obymobi.Enums;
    using System.Linq;
    // __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'Company'. This class is used for Business Logic or for framework extension code.</summary>
	[Serializable]
	public partial class CompanyEntity : CompanyEntityBase
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
        , ICompanyRelatedEntity, IMediaContainingEntity, ICustomTextContainingEntity, ITimestampEntity
    // __LLBLGENPRO_USER_CODE_REGION_END	
	{
		/// <summary>CTor</summary>
		public CompanyEntity():base()
		{
		}

		/// <summary>CTor</summary>
		/// <param name="companyId">PK value for Company which data should be fetched into this Company object</param>
		public CompanyEntity(System.Int32 companyId):
			base(companyId)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="companyId">PK value for Company which data should be fetched into this Company object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public CompanyEntity(System.Int32 companyId, IPrefetchPath prefetchPathToUse):
			base(companyId, prefetchPathToUse)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="companyId">PK value for Company which data should be fetched into this Company object</param>
		/// <param name="validator">The custom validator object for this CompanyEntity</param>
		public CompanyEntity(System.Int32 companyId, IValidator validator):
			base(companyId, validator)
		{
		}
		
		/// <summary>CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected CompanyEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
            // __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
            // __LLBLGENPRO_USER_CODE_REGION_END
		}

		#region Custom Entity code
		
        // __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
        public string PreviousCultureCode { get; set; }

        #region Status properties


        /// <summary>
        /// Flag to indicate that this Company is meant to be deleted intentionally. 
        /// If 'false' deletion will be prevented by the CompanyValidator (Default: false)
        /// </summary>
        [XmlIgnore]
        public bool OverruleDeletionPrevention = false;

        /// <summary>
        /// Gets the flag indicating whether this company has a name specified
        /// </summary>
        [XmlIgnore]
        public bool HasName
        {
            get
            {
                return !this.Name.IsNullOrWhiteSpace();
            }
        }

        /// <summary>
        /// Gets the flag indicating whether this company has a language specified
        /// </summary>
        [XmlIgnore]
        public bool HasLanguage
        {
            get
            {
                return this.LanguageId.HasValue;
            }
        }

        /// <summary>
        /// Gets the flag indicating whether this company has a zipcode specified
        /// </summary>
        [XmlIgnore]
        public bool HasZipcode
        {
            get
            {
                return !this.Zipcode.IsNullOrWhiteSpace();
            }
        }

        /// <summary>
        /// Gets the flag indicating whether this company has payment methods specified
        /// </summary>
        [XmlIgnore]
        public bool HasPaymentmethods
        {
            get
            {
                return (this.Paymentmethods.Count > 0);
            }
        }

        [XmlIgnore]
        public EntityView<ProductEntity> Paymentmethods
        {
            get
            {
                EntityView<ProductEntity> productView = this.ProductCollection.DefaultView;

                PredicateExpression filter = new PredicateExpression();
                filter.Add(ProductFields.CompanyId == this.CompanyId);
                filter.Add(ProductFields.Type == Obymobi.Enums.ProductType.Paymentmethod);

                productView.Filter = filter;

                return productView;
            }
        }

        /// <summary>
        /// Gets the flag indicating whether this company has surveys specified
        /// </summary>
        [XmlIgnore]
        public bool HasSurveys
        {
            get
            {
                return (this.Surveys.Count > 0);
            }
        }

        [XmlIgnore]
        public EntityView<SurveyEntity> Surveys
        {
            get
            {
                EntityView<SurveyEntity> surveyView = this.SurveyCollection.DefaultView;

                PredicateExpression filter = new PredicateExpression();
                filter.Add(SurveyFields.CompanyId == this.CompanyId);

                surveyView.Filter = filter;

                return surveyView;
            }
        }

        [XmlIgnore]
        public bool HasFrontpageProducts
        {
            get
            {
                return (FrontpageProducts.Count >= 4);
            }
        }

        [XmlIgnore]
        public EntityView<ProductEntity> FrontpageProducts
        {
            get
            {
                EntityView<ProductEntity> productView = this.ProductCollection.DefaultView;

                PredicateExpression productFilter = new PredicateExpression();
                productFilter.Add(ProductFields.CompanyId == this.CompanyId);
                productFilter.Add(ProductFields.DisplayOnHomepage == true);

                productView.Filter = productFilter;

                return productView;
            }
        }

        /// <summary>
        /// Gets the flag indicating whether this company has language specified
        /// </summary>
        [XmlIgnore]
        public bool HasLanguages
        {
            get
            {
                return (this.CompanyLanguageCollection.Count > 0);
            }
        }

        [XmlIgnore]
        private bool preventCompanyDataLastModifiedUpdate = false;
        [XmlIgnore]
        public bool PreventCompanyDataLastModifiedUpdate
        {
            get
            {
                return this.preventCompanyDataLastModifiedUpdate;
            }
            set
            {
                this.preventCompanyDataLastModifiedUpdate = value;
            }
        }

        #endregion

        [XmlIgnore]
        public DateTime MediaLastModified
        {
            get
            {
                DateTime latestTimestamp = AdvertisementMediaLastModifiedUTC;

                if (this.EntertainmentMediaLastModifiedUTC > latestTimestamp)
                    latestTimestamp = this.EntertainmentMediaLastModifiedUTC;

                if (this.AnnouncementMediaLastModifiedUTC > latestTimestamp)
                    latestTimestamp = this.AnnouncementMediaLastModifiedUTC;

                if (this.SurveyMediaLastModifiedUTC > latestTimestamp)
                    latestTimestamp = this.SurveyMediaLastModifiedUTC;

                if (this.MenuMediaLastModifiedUTC > latestTimestamp)
                    latestTimestamp = this.MenuMediaLastModifiedUTC;

                if (this.CompanyMediaLastModifiedUTC > latestTimestamp)
                    latestTimestamp = this.CompanyMediaLastModifiedUTC;

                return latestTimestamp;
            }
        }

        [XmlIgnore]
        public string CompanyDataLastModifiedAsTimeStamp
        {
            get
            {
                return DateTimeUtil.DateTimeToSimpleDateTimeStamp(this.CompanyDataLastModifiedUTC);
            }
        }

        [XmlIgnore]
        public string CompanyMediaLastModifiedAsTimeStamp
        {
            get
            {
                return DateTimeUtil.DateTimeToSimpleDateTimeStamp(this.CompanyMediaLastModifiedUTC);
            }
        }

        [XmlIgnore]
        public string MenuDataLastModifiedAsTimeStamp
        {
            get
            {
                return DateTimeUtil.DateTimeToSimpleDateTimeStamp(this.MenuDataLastModifiedUTC);
            }
        }

        [XmlIgnore]
        public string MenuMediaLastModifiedAsTimeStamp
        {
            get
            {
                return DateTimeUtil.DateTimeToSimpleDateTimeStamp(this.MenuMediaLastModifiedUTC);
            }
        }

        [XmlIgnore]
        public string PosIntegrationInformationLastModifiedAsTimeStamp
        {
            get
            {
                return DateTimeUtil.DateTimeToSimpleDateTimeStamp(this.PosIntegrationInfoLastModifiedUTC);
            }
        }

        [XmlIgnore]
        public bool HasDescription
        {
            get
            {
                return this.Description.Length > 0;
            }
        }

        [XmlIgnore]
        public bool HasImage
        {
            get
            {
                return this.MediaCollection.Count > 0;
            }
        }



        [XmlIgnore]
        public bool HasEntertainment
        {
            get
            {
                return (this.CompanyEntertainmentCollection.Count > 0);
            }
        }

        [XmlIgnore]
        public EntertainmentCollection Entertainment
        {
            get
            {
                EntertainmentCollection entertainmentCollection = new EntertainmentCollection();

                foreach (CompanyEntertainmentEntity companyEntertainmentEntity in this.CompanyEntertainmentCollection)
                {
                    EntertainmentEntity entertainmentEntity = companyEntertainmentEntity.EntertainmentEntity;
                    entertainmentCollection.Add(entertainmentEntity);
                }

                return entertainmentCollection;

                //EntityView<EntertainmentEntity> entertainmentView = this.EntertainmentCollection.DefaultView;

                //PredicateExpression filter = new PredicateExpression();
                //filter.Add(EntertainmentFields.CompanyId == this.CompanyId);

                //return entertainmentView;
            }
        }

        [XmlIgnore]
        public EntityView<MediaEntity> Media
        {
            get 
            {
                EntityView<MediaEntity> mediaView = this.MediaCollection.DefaultView;

                PredicateExpression filter = new PredicateExpression();
                filter.Add(MediaFields.CompanyId == this.CompanyId);

                mediaView.Filter = filter;

                return mediaView;
            }
        }

        [XmlIgnore]
        public EntityView<ProductEntity> ServiceRequests
        {
            get
            {
                EntityView<ProductEntity> productView = this.ProductCollection.DefaultView;

                PredicateExpression filter = new PredicateExpression();
                filter.Add(ProductFields.CompanyId == this.CompanyId);
                filter.Add(ProductFields.Type == Obymobi.Enums.ProductType.Service);

                productView.Filter = filter;

                return productView;
            }
        }

        [XmlIgnore]
        public bool HasServiceRequests
        {
            get
            {
                return (this.ServiceRequests.Count > 0);
            }
        }

        [XmlIgnore]
        public bool HasAdvertisements
        {
            get
            {
                return (this.AdvertisementCollection.Count > 0);
            }
        }

        [XmlIgnore]
        public bool IsComplete
        {
            get
            {
                return this.HasDescription && this.HasImage && this.HasFrontpageProducts && this.HasEntertainment && this.HasPaymentmethods && this.HasAdvertisements;
            }
        }

        [XmlIgnore]
        public POSConnectorType POSConnectorType
        {
            get 
            {
                POSConnectorType? posConnectorType = null;
                foreach (TerminalEntity terminal in this.TerminalCollection)
                {
					if (terminal.POSConnectorTypeEnum == POSConnectorType.Unknown)
						continue;

                    if (!posConnectorType.HasValue)
                        posConnectorType = terminal.POSConnectorTypeEnum;
                    else if (posConnectorType.Value != terminal.POSConnectorTypeEnum)
                        throw new NotImplementedException(string.Format("Only one POSConnectorType is supported per company! POSConnectorType '{0}' of terminal '{1}' is not equal to POSConnectorType '{2}'!", terminal.POSConnectorTypeEnum, terminal.TerminalId, posConnectorType));
                }

                if (posConnectorType == null)
                    posConnectorType = POSConnectorType.Unknown;

                return posConnectorType.Value;
            }
        }

        /// <summary>
        /// Gets the client application version as a <see cref="VersionNumber"/> instance
        /// </summary>
        [XmlIgnore]
        public VersionNumber ClientApplicationVersionAsVersionNumber
        {
            get
            {
                return new VersionNumber(this.ClientApplicationVersion);
            }
        }

        /// <summary>
        /// Gets the terminal application version as a <see cref="VersionNumber"/> instance
        /// </summary>
        [XmlIgnore]
        public VersionNumber TerminalApplicationVersionAsVersionNumber
        {
            get
            {
                return new VersionNumber(this.TerminalApplicationVersion);
            }
        }

        /// <summary>
        /// Gets the <see cref="TimeZoneInfo"/> for the configured company TimeZone.
        /// If no TimeZone has been configured UTC is returned.
        /// </summary>
        [XmlIgnore]
	    public TimeZoneInfo TimeZoneInfo
	    {
	        get
	        {
	            if (!this.TimeZoneOlsonId.IsNullOrWhiteSpace())
	            {
                    return TimeZoneInfo.FindSystemTimeZoneById(TimeZone.Mappings[this.TimeZoneOlsonId].WindowsTimeZoneId);
                }

	            return TimeZoneInfo.Utc;
	        }
	    }

        /// <summary>
        /// Gets the culture for this company.
        /// </summary>
        [XmlIgnore]
        public Culture Culture
        {
            get
            {
                return (!this.CultureCode.IsNullOrWhiteSpace() ? Culture.Mappings[this.CultureCode] : null);
            }
        }

        /// <summary>
        /// Gets the culture for this company.
        /// </summary>
        [XmlIgnore]
        public bool IsAppLessCompany => this.ApplicationConfigurationCollection.Any();

        // __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included Code

		#endregion
	}
}
