﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	using System.Linq;
	using Dionysos;
    using Obymobi.Enums;
    using Obymobi.Logic.Cms;
    using System.Xml.Serialization;
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'Genericproduct'. This class is used for Business Logic or for framework extension code.</summary>
	[Serializable]
	public partial class GenericproductEntity : GenericproductEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
        , IMediaContainingEntity, INonCompanyRelatedEntity, ICustomTextContainingEntity, IMenuItem, ITimestampEntity
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		/// <summary>CTor</summary>
		public GenericproductEntity():base()
		{
		}

		/// <summary>CTor</summary>
		/// <param name="genericproductId">PK value for Genericproduct which data should be fetched into this Genericproduct object</param>
		public GenericproductEntity(System.Int32 genericproductId):
			base(genericproductId)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="genericproductId">PK value for Genericproduct which data should be fetched into this Genericproduct object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public GenericproductEntity(System.Int32 genericproductId, IPrefetchPath prefetchPathToUse):
			base(genericproductId, prefetchPathToUse)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="genericproductId">PK value for Genericproduct which data should be fetched into this Genericproduct object</param>
		/// <param name="validator">The custom validator object for this GenericproductEntity</param>
		public GenericproductEntity(System.Int32 genericproductId, IValidator validator):
			base(genericproductId, validator)
		{
		}
		
		/// <summary>CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected GenericproductEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}

		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode

		/// <summary>
		/// Determine if a least one Media entity contains a MediaRatioEntity related to MediaType ProductBranding
		/// </summary>
		public bool HasProductBranding
		{ 
			get
			{
				bool toReturn = false;
				foreach(var media in this.MediaCollection)
				{
                    if (media.MediaRatioTypeMediaCollection.Any(mr => (mr.MediaType.HasValue && mr.MediaType.Value == (int)MediaType.ProductBranding)) ||
                        media.MediaRatioTypeMediaCollection.Any(mr => mr.MediaType.HasValue && mr.MediaType.Value == (int)MediaType.GenericproductBranding))						
					{
						toReturn = true;
						break;
					}
				}
				return toReturn;
			}
		}

        /// <summary>
        /// Determine if a least one Media entity contains a MediaRatioEntity related to MediaType ProductBranding
        /// </summary>
        public bool HasProductBrandingCrave
        {
            get
            {
                bool toReturn = false;
                foreach (var media in this.MediaCollection)
                {
                    if (media.MediaRatioTypeMediaCollection.Any(mr => (mr.MediaType.HasValue && mr.MediaType.Value == (int)MediaType.ProductBranding800x480)) ||
                        media.MediaRatioTypeMediaCollection.Any(mr => (mr.MediaType.HasValue && mr.MediaType.Value == (int)MediaType.GenericProductBranding800x480)))
                    {
                        toReturn = true;
                        break;
                    }
                }
                return toReturn;
            }
        }

        /// <summary>
        /// Determine if a least one Media entity contains a MediaRatioEntity related to MediaType ProductBranding
        /// </summary>
        public bool HasProductBranding1280x800
        {
            get
            {
                bool toReturn = false;
                foreach (var media in this.MediaCollection)
                {
                    if (media.MediaRatioTypeMediaCollection.Any(mr => (mr.MediaType.HasValue && mr.MediaType.Value == (int)MediaType.ProductBranding1280x800)) ||
                        media.MediaRatioTypeMediaCollection.Any(mr => (mr.MediaType.HasValue && mr.MediaType.Value == (int)MediaType.GenericProductBranding1280x800)))
                    {
                        toReturn = true;
                        break;
                    }
                }
                return toReturn;
            }
        }

		/// <summary>
		/// Determine if a least one Media entity contains a MediaRatioEntity related to MediaType ProductButton
		/// </summary>
		public bool HasProductButton
		{ 
			get
			{
				bool toReturn = false;
				foreach(var media in this.MediaCollection)
				{
                    if (media.MediaRatioTypeMediaCollection.Any(mr => (mr.MediaType.HasValue && mr.MediaType.Value == (int)MediaType.ProductButton)) ||
                        media.MediaRatioTypeMediaCollection.Any(mr => (mr.MediaType.HasValue && mr.MediaType.Value == (int)MediaType.GenericproductButton)))						
					{
						toReturn = true;
						break;
					}
				}
				return toReturn;
			}
		}

        /// <summary>
        /// Determine if a least one Media entity contains a MediaRatioEntity related to MediaType ProductButton
        /// </summary>
        public bool HasProductButtonCrave
        {
            get
            {
                bool toReturn = false;
                foreach (var media in this.MediaCollection)
                {
                    if (media.MediaRatioTypeMediaCollection.Any(mr => (mr.MediaType.HasValue && mr.MediaType.Value == (int)MediaType.ProductButton800x480)) ||
                        media.MediaRatioTypeMediaCollection.Any(mr => (mr.MediaType.HasValue && mr.MediaType.Value == (int)MediaType.GenericProductButton800x480)))
                    {
                        toReturn = true;
                        break;
                    }
                }
                return toReturn;
            }
        }

        /// <summary>
        /// Determine if a least one Media entity contains a MediaRatioEntity related to MediaType ProductButton
        /// </summary>
        public bool HasProductButton1280x800
        {
            get
            {
                bool toReturn = false;
                foreach (var media in this.MediaCollection)
                {
                    if (media.MediaRatioTypeMediaCollection.Any(mr => (mr.MediaType.HasValue && mr.MediaType.Value == (int)MediaType.ProductButton1280x800)) ||
                        media.MediaRatioTypeMediaCollection.Any(mr => (mr.MediaType.HasValue && mr.MediaType.Value == (int)MediaType.GenericProductButton1280x800)))
                    {
                        toReturn = true;
                        break;
                    }
                }
                return toReturn;
            }
        }

		/// <summary>
		/// Determine if there is a Description
		/// </summary>
		public bool HasDescription
		{
			get
			{
				return !this.Description.IsNullOrWhiteSpace();
			}
		}

		/// <summary>
		/// Determine if there is a TextColor
		/// </summary>
		public bool HasTextColor
		{
			get
			{
				return !this.TextColor.IsNullOrWhiteSpace();
			}
		}

		/// <summary>
		/// Determine if there is a BackgroundColor
		/// </summary>
		public bool HasBackgroundColor
		{
			get
			{
				return !this.BackgroundColor.IsNullOrWhiteSpace();
			}
		}

		/// <summary>
		/// Determine if there is a SupplierId
		/// </summary>
		public bool HasSupplierId
		{
			get
			{
				// 19 is Unknown in the ProductionDatabase
				return this.SupplierId.HasValue && this.SupplierId.Value != 19;
			}
		}

        /// <summary>
        /// Gets/sets the Generic category name
        /// </summary>
        public string GenericcategoryName { get; set; }

        #region Treelist Logic

        [XmlIgnore]
        public string ItemId
        {
            get
            {
                return string.Format("{0}-{1}-{2}", this.ItemType, this.GenericproductId, this.GenericcategoryId.GetValueOrDefault(0));
            }
        }

        [XmlIgnore]
        public string ParentItemId
        {
            get
            {
                return this.GenericcategoryEntity.ItemId;
            }
        }

        [XmlIgnore]
        public string ItemType
        {
            get
            {
                return MenuItemType.Product;
            }
        }

        [XmlIgnore]
        public string TypeName
        {
            get
            {
                return MenuItemType.Product;
            }
        }

        [XmlIgnore]
        public bool IsLinkedToBrand
        {
            get
            {
                return false;
            }
        }

        #endregion        


		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included Code

		#endregion
	}
}
