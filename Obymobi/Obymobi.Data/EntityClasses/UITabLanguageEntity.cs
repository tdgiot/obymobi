﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    using System.Xml.Serialization;
    // __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'UITabLanguage'. This class is used for Business Logic or for framework extension code.</summary>
	[Serializable]
	public partial class UITabLanguageEntity : UITabLanguageEntityBase
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
    , INullableCompanyRelatedChildEntity
    // __LLBLGENPRO_USER_CODE_REGION_END	
	{
		/// <summary>CTor</summary>
		public UITabLanguageEntity():base()
		{
		}

		/// <summary>CTor</summary>
		/// <param name="uITabLanguageId">PK value for UITabLanguage which data should be fetched into this UITabLanguage object</param>
		public UITabLanguageEntity(System.Int32 uITabLanguageId):
			base(uITabLanguageId)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="uITabLanguageId">PK value for UITabLanguage which data should be fetched into this UITabLanguage object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public UITabLanguageEntity(System.Int32 uITabLanguageId, IPrefetchPath prefetchPathToUse):
			base(uITabLanguageId, prefetchPathToUse)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="uITabLanguageId">PK value for UITabLanguage which data should be fetched into this UITabLanguage object</param>
		/// <param name="validator">The custom validator object for this UITabLanguageEntity</param>
		public UITabLanguageEntity(System.Int32 uITabLanguageId, IValidator validator):
			base(uITabLanguageId, validator)
		{
		}
		
		/// <summary>CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected UITabLanguageEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}

		#region Custom Entity code
		
        // __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode

        [XmlIgnore]
        public CommonEntityBase Parent
        {
            get { return this.UITabEntity; }
        }

        // __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included Code

		#endregion
	}
}
