﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    using System.Linq;
    using Dionysos;
    using Dionysos.Data;
    using Obymobi.Enums;
    // __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'User'. This class is used for Business Logic or for framework extension code.</summary>
	[Serializable]
	public partial class UserEntity : UserEntityBase
        // __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
        , Dionysos.Web.Security.IManagableUser, INonCompanyRelatedEntity
    // __LLBLGENPRO_USER_CODE_REGION_END	
	{
		/// <summary>CTor</summary>
		public UserEntity():base()
		{
		}

		/// <summary>CTor</summary>
		/// <param name="userId">PK value for User which data should be fetched into this User object</param>
		public UserEntity(System.Int32 userId):
			base(userId)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="userId">PK value for User which data should be fetched into this User object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public UserEntity(System.Int32 userId, IPrefetchPath prefetchPathToUse):
			base(userId, prefetchPathToUse)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="userId">PK value for User which data should be fetched into this User object</param>
		/// <param name="validator">The custom validator object for this UserEntity</param>
		public UserEntity(System.Int32 userId, IValidator validator):
			base(userId, validator)
		{
		}
		
		/// <summary>CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected UserEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
            // __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
            // __LLBLGENPRO_USER_CODE_REGION_END
		}

		#region Custom Entity code
		
        // __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode

        #region IManagableUser Members

        /// <summary>
        /// Gets the ShortDisplayName (Username).
        /// </summary>
        public string ShortDisplayName
        {
            get
            {
                return this.Username;
            }
        }

        /// <summary>
        /// Gets the MiddleName (LastnamePrefix).
        /// </summary>
        public string MiddleName
        {
            get
            {
                return this.LastnamePrefix;
            }
        }

        public string[] Roles
        {
            get
            {
                return new string[] { this.RoleName };
            }
        }

        #endregion

        /// <summary>
        /// Gets a flag indicating whether the user has administrator rights
        /// </summary>
        public bool IsAdministrator
        {
            get
            {
                return (this.Role >= Role.Administrator);
            }
        }

        /// <summary>
        /// Gets the name of the role for the current user
        /// </summary>
        [DataGridViewColumnVisible]
        [EntityFieldDependency((int)UserFieldIndex.Role)]
        public string RoleName
        {
            get
            {
                return this.Role.ToString();
            }
        }

        [EntityFieldDependency((int)UserFieldIndex.TimeZoneId)]
	    public TimeZoneInfo TimeZoneInfo
	    {
	        get
	        {
	            if (!this.TimeZoneOlsonId.IsNullOrWhiteSpace())
	            {
                    return TimeZoneInfo.FindSystemTimeZoneById(Obymobi.TimeZone.Mappings[this.TimeZoneOlsonId].WindowsTimeZoneId);
                }

	            return TimeZoneInfo.Utc;
	        }
	    }

	    public string Fullname
	    {
	        get
	        {
	            string fullName = Firstname;
                if (LastnamePrefix.Length > 0)
	            {
	                if (fullName.Length > 0)
	                    fullName += " ";
                    fullName += LastnamePrefix;
	            }

	            if (Lastname.Length > 0)
	            {
                    if (fullName.Length > 0)
                        fullName += " ";
                    fullName += Lastname;
	            }

	            return fullName;
	        }
	    }

        // __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included Code

		#endregion
	}
}
