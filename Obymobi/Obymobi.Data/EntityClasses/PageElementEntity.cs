﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces    
    using System.Xml.Serialization;
    using Dionysos;
    using Obymobi.Logic.Cms;
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'PageElement'. This class is used for Business Logic or for framework extension code.</summary>
	[Serializable]
	public partial class PageElementEntity : PageElementEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
        , Obymobi.Logic.Cms.IPageTypeElementData, INullableCompanyRelatedChildEntity, IMediaContainingEntity, ITimestampEntity
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		/// <summary>CTor</summary>
		public PageElementEntity():base()
		{
		}

		/// <summary>CTor</summary>
		/// <param name="pageElementId">PK value for PageElement which data should be fetched into this PageElement object</param>
		public PageElementEntity(System.Int32 pageElementId):
			base(pageElementId)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="pageElementId">PK value for PageElement which data should be fetched into this PageElement object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public PageElementEntity(System.Int32 pageElementId, IPrefetchPath prefetchPathToUse):
			base(pageElementId, prefetchPathToUse)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="pageElementId">PK value for PageElement which data should be fetched into this PageElement object</param>
		/// <param name="validator">The custom validator object for this PageElementEntity</param>
		public PageElementEntity(System.Int32 pageElementId, IValidator validator):
			base(pageElementId, validator)
		{
		}
		
		/// <summary>CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected PageElementEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor            
			// __LLBLGENPRO_USER_CODE_REGION_END
		}

		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode

        /// <summary>
        /// 1:1 property for PageElementId, needed for IPageTypeElementData interface
        /// </summary>
        [XmlIgnore]
        public int PrimaryId
        {
            get
            {
                return base.PageElementId;
            }
            set
            {
                base.PageElementId = value;
            }
        }

        [XmlIgnore]
        public CommonEntityBase Parent
        {
            get { return this.PageEntity; }
        }

        [DataGridViewColumnVisibleAttribute]
        [XmlIgnore]
        public string SiteNamePageNamePageElementLanguage
        {
            get
            {                 
                string pageElementName = PageTypeHelper.GetPageElementName(this.PageEntity.PageTypeAsEnum, this.SystemName);
                string toReturn = "{0} - {1}: {2}".FormatSafe(this.PageEntity.SiteEntity.Name, this.PageEntity.Name, pageElementName);
                if (!this.CultureCode.IsNullOrWhiteSpace())
                    toReturn += " ({0})".FormatSafe(this.CultureCode);
                return toReturn;
            }
        }

        [XmlIgnore]
        public PageElementType PageElementTypeAsEnum
        {
            get
            {                
                return this.PageElementType.ToEnum<PageElementType>();
            }
            set
            {
                this.PageElementType = (int)value;
            }
        }

	    public bool HasMedia
	    {
	        get
	        {
	            return this.MediaCollection.Count > 0;	            
	        }
            set { }
	    }

	    // __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included Code

		#endregion
	}
}
