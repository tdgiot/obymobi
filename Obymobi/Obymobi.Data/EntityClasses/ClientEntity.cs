﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	using Dionysos;
    using Obymobi.Enums;
    using Obymobi.Interfaces;
    using System.Xml.Serialization;
    // __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'Client'. This class is used for Business Logic or for framework extension code.</summary>
	[Serializable]
	public partial class ClientEntity : ClientEntityBase
        // __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
        , ICompanyRelatedEntity, INocActivityEntity, ITimestampEntity
    // __LLBLGENPRO_USER_CODE_REGION_END	
	{
		/// <summary>CTor</summary>
		public ClientEntity():base()
		{
		}

		/// <summary>CTor</summary>
		/// <param name="clientId">PK value for Client which data should be fetched into this Client object</param>
		public ClientEntity(System.Int32 clientId):
			base(clientId)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="clientId">PK value for Client which data should be fetched into this Client object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public ClientEntity(System.Int32 clientId, IPrefetchPath prefetchPathToUse):
			base(clientId, prefetchPathToUse)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="clientId">PK value for Client which data should be fetched into this Client object</param>
		/// <param name="validator">The custom validator object for this ClientEntity</param>
		public ClientEntity(System.Int32 clientId, IValidator validator):
			base(clientId, validator)
		{
		}
		
		/// <summary>CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected ClientEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
            // __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
            // __LLBLGENPRO_USER_CODE_REGION_END
		}

		#region Custom Entity code
		
        // __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode


        [XmlIgnore]
        public bool UpdateOperationModeViaComet = false;

        [XmlIgnore]
        public bool UpdateDeliverypointIdViaComet = false;

        [XmlIgnore]
        public bool ValidatorOverwriteDontLogDeviceChange = false;

        /// <summary>
        /// Gets or sets the operation mode enum.
        /// </summary>
        /// <value>
        /// The operation mode enum.
        /// </value>
        [XmlIgnore]
        public ClientOperationMode OperationModeEnum
        {
            get
            {
                return this.OperationMode.ToEnum<ClientOperationMode>();
            }
            set
            {
                this.OperationMode = (int)value;
            }
        }

        /// <summary>
        /// Gets a flag which indicates whether this terminal is online
        /// </summary>
        [XmlIgnore]
        public bool IsOnline
        {
            get
            {
                /*if (this.DeviceEntity != null)
                {
                    return this.DeviceEntity.LastRequestUTC.HasValue && this.DeviceEntity.LastRequestUTC.Value > DateTime.UtcNow.AddMinutes(-2);
                }*/
                return this.LastActivityUTC.HasValue && this.LastActivityUTC.Value > DateTime.UtcNow.AddMinutes(-2);
            }
        }

        /// <summary>
        /// Gets the last activity (UTC) for this client
        /// </summary>
        [XmlIgnore]
        public DateTime? LastActivityUTC
        {
            get
            {
                if (this.DeviceId.HasValue)
                    return this.DeviceEntity.LastRequestUTC;

                return null;
            }
        }

        [DataGridViewColumnVisible]
        [XmlIgnore]
        public string MacAddress
        {
            get
            {
                string macAddress = "No device";

                if (this.DeviceEntity != null)
                    macAddress = this.DeviceEntity.Identifier;

                return macAddress;
            }
        }

        [DataGridViewColumnVisible]
        [XmlIgnore]
        public string LastDeliverypointText
        {
            get
            {
                string toReturn = string.Empty;
                if (this.LastDeliverypointId.HasValue)
                    toReturn = this.LastDeliverypointEntity.Name;
                return toReturn;
            }
        }

        [DataGridViewColumnVisible]
        [XmlIgnore]
        public string DeviceModel
        {
            get
            {
                DeviceModel model = Enums.DeviceModel.Unknown;

                if (this.DeviceEntity != null)
                {
                    if (!this.DeviceEntity.DeviceModel.HasValue)
                        this.GetSingleDeviceEntity(true);

                    model = this.DeviceEntity.DeviceModel.GetValueOrDefault(Enums.DeviceModel.Unknown);

                    if (!Enum.IsDefined(typeof(DeviceModel), model))
	                {
	                    model = Enums.DeviceModel.Unknown;
	                }
	            }

                return model.GetStringValue();
            }
        }

        [DataGridViewColumnVisible]
        [XmlIgnore]
        public bool Loaded
        {
            get
            {
                return this.LoadedSuccessfully;
            }
        }

        [DataGridViewColumnVisible]
        [XmlIgnore]
        public string LastRequestServerTime
        {
            get
            {
                if (this.DeviceId.HasValue)
                    return this.DeviceEntity.LastRequestUTC.GetValueOrDefault().UtcToLocalTime().ToString("g");

                return string.Empty;
            }
        }
        
        [DataGridViewColumnVisible]
        [XmlIgnore]
        [EntityFieldDependency((int) ClientFieldIndex.LastApiVersion)]
        public string ApiVersionAsString
        {
            get
            {
                if (this.LastApiVersion == 2)
                    return "API V2";

                return "API V1";
            }
        }

        // __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included Code

		#endregion
	}
}
