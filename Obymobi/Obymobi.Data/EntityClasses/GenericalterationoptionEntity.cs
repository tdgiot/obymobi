﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    using System.Xml.Serialization;
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'Genericalterationoption'. This class is used for Business Logic or for framework extension code.</summary>
	[Serializable]
	public partial class GenericalterationoptionEntity : GenericalterationoptionEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
        , INonCompanyRelatedEntity
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		/// <summary>CTor</summary>
		public GenericalterationoptionEntity():base()
		{
		}

		/// <summary>CTor</summary>
		/// <param name="genericalterationoptionId">PK value for Genericalterationoption which data should be fetched into this Genericalterationoption object</param>
		public GenericalterationoptionEntity(System.Int32 genericalterationoptionId):
			base(genericalterationoptionId)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="genericalterationoptionId">PK value for Genericalterationoption which data should be fetched into this Genericalterationoption object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public GenericalterationoptionEntity(System.Int32 genericalterationoptionId, IPrefetchPath prefetchPathToUse):
			base(genericalterationoptionId, prefetchPathToUse)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="genericalterationoptionId">PK value for Genericalterationoption which data should be fetched into this Genericalterationoption object</param>
		/// <param name="validator">The custom validator object for this GenericalterationoptionEntity</param>
		public GenericalterationoptionEntity(System.Int32 genericalterationoptionId, IValidator validator):
			base(genericalterationoptionId, validator)
		{
		}
		
		/// <summary>CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected GenericalterationoptionEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}

		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode

        [DataGridViewColumnVisibleAttribute]
        [XmlIgnore]
        public string NameAndPriceIn
        {
            get
            {
                return string.Format("{0} ({1})", this.Name, this.PriceIn);
            }
        }

		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included Code

		#endregion
	}
}
