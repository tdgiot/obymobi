﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Dionysos.Data;
using System.Diagnostics;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'OptIn'. This class is used for Business Logic or for framework extension code.</summary>
	[Serializable]
	public partial class OptInEntity : OptInEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		, ICompanyRelatedChildEntity
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		/// <summary>CTor</summary>
		public OptInEntity():base()
		{
		}

		/// <summary>CTor</summary>
		/// <param name="optInId">PK value for OptIn which data should be fetched into this OptIn object</param>
		public OptInEntity(System.Int32 optInId):
			base(optInId)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="optInId">PK value for OptIn which data should be fetched into this OptIn object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public OptInEntity(System.Int32 optInId, IPrefetchPath prefetchPathToUse):
			base(optInId, prefetchPathToUse)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="optInId">PK value for OptIn which data should be fetched into this OptIn object</param>
		/// <param name="validator">The custom validator object for this OptInEntity</param>
		public OptInEntity(System.Int32 optInId, IValidator validator):
			base(optInId, validator)
		{
		}
		
		/// <summary>CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected OptInEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}

		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode

        public CommonEntityBase Parent => OutletEntity;

        // __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included Code

		#endregion
	}
}
