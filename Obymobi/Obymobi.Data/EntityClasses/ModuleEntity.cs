﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    using Dionysos.Interfaces;
    using Obymobi.Data.HelperClasses;
    using System.Collections.Generic;
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'Module'. This class is used for Business Logic or for framework extension code.</summary>
	[Serializable]
	public partial class ModuleEntity : ModuleEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
        , ILicenseModule, ISystemEntity
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		/// <summary>CTor</summary>
		public ModuleEntity():base()
		{
		}

		/// <summary>CTor</summary>
		/// <param name="moduleId">PK value for Module which data should be fetched into this Module object</param>
		public ModuleEntity(System.Int32 moduleId):
			base(moduleId)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="moduleId">PK value for Module which data should be fetched into this Module object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public ModuleEntity(System.Int32 moduleId, IPrefetchPath prefetchPathToUse):
			base(moduleId, prefetchPathToUse)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="moduleId">PK value for Module which data should be fetched into this Module object</param>
		/// <param name="validator">The custom validator object for this ModuleEntity</param>
		public ModuleEntity(System.Int32 moduleId, IValidator validator):
			base(moduleId, validator)
		{
		}
		
		/// <summary>CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected ModuleEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}

		#region Custom Entity code
		
        // __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode

        #region ILicenseModule members

        /// <summary>
        /// Gets the flag which indicates if the module is licensed
        /// </summary>
        public bool IsLicensed
        {
            get
            {
                if (this.LicensingFree)
                {
                    return true;
                }
                else
                {
                    var filterUielementLicenses = new PredicateExpression();
                    filterUielementLicenses.Add(LicensedModuleFields.ModuleNameSystem == this.NameSystem);

                    LicensedModuleEntity licensedModuleEntity = new LicensedModuleEntity();
                    licensedModuleEntity = Dionysos.Data.LLBLGen.LLBLGenEntityUtil.GetSingleEntityUsingPredicateExpression(filterUielementLicenses, licensedModuleEntity) as LicensedModuleEntity;

                    if (licensedModuleEntity == null)
                    {
                        return false;
                    }
                    else
                    {
                        return licensedModuleEntity.IsLicensed;
                    }
                }
            }
        }

        /// <summary>
        /// Gets the UI elements.
        /// </summary>
        /// <value>The UI elements.</value>
        public ICollection<ILicenseUIElement> UIElements
        {
            get
            {
                ICollection<ILicenseUIElement> uielements = new List<ILicenseUIElement>();

                foreach (UIElementEntity uielement in this.UIElementCollection)
                {
                    uielements.Add(uielement);
                }

                return uielements;
            }
        }

        /// <summary>
        /// Gets the UI element collection.
        /// </summary>
        /// <value>The UI element collection.</value>
        public UIElementCollection UIElementCollection
        {
            get
            {
                return Dionysos.Data.UIElementUtil.GetUIElementsForModule(this.NameSystem) as UIElementCollection;
            }
        }

        private LicensedModuleEntity licensedModuleEntity = null;
        public LicensedModuleEntity LicensedModuleEntity
        {
            get
            {
                if (licensedModuleEntity == null)
                {
                    var filterUielements = new PredicateExpression(LicensedModuleFields.ModuleNameSystem == this.NameSystem);
                    this.licensedModuleEntity = new LicensedModuleEntity();
                    this.licensedModuleEntity = Dionysos.Data.LLBLGen.LLBLGenEntityUtil.GetSingleEntityUsingPredicateExpression(filterUielements, this.licensedModuleEntity) as LicensedModuleEntity;

                    if (this.licensedModuleEntity == null)
                    {
                        this.licensedModuleEntity = new LicensedModuleEntity();
                        this.licensedModuleEntity.ModuleNameSystem = this.NameSystem;
                        this.licensedModuleEntity.Save();
                    }
                }


                return this.licensedModuleEntity;
            }
        }

        /// <summary>
        /// Gets the default UI element (used for the NavigationUrl in the Menu in EVAdmin)
        /// </summary>
        public UIElementEntity DefaultUIElement
        {
            get
            {
                var filterUielements = new PredicateExpression(UIElementFields.TypeNameFull == this.DefaultUiElementTypeNameFull);
                UIElementEntity uiElement = new UIElementEntity();
                uiElement = Dionysos.Data.LLBLGen.LLBLGenEntityUtil.GetSingleEntityUsingPredicateExpression(filterUielements, uiElement) as UIElementEntity;

                if (uiElement == null)
                    throw new Dionysos.TechnicalException("Geen Default UI element gevonden voor Module {0}", this.NameFull);
                else
                    return uiElement;
            }
        }

        /// <summary>
        /// Gets or sets the <see cref="Dionysos.Interfaces.ILicenseUIElement"/> with the specified name system.
        /// </summary>
        /// <value>The <see cref="Dionysos.Interfaces.ILicenseUIElement"/> with the specified NameSystem.</value>
        public ILicenseUIElement this[string nameSystem]
        {
            get
            {
                ILicenseUIElement uielementLicense = null;

                foreach (ILicenseUIElement licenseUIElement in this.UIElements)
                {
                    if (licenseUIElement.NameSystem == nameSystem)
                    {
                        uielementLicense = licenseUIElement;
                        break;
                    }
                }

                return uielementLicense;
            }
        }

        #endregion

        // __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included Code

		#endregion
	}
}
