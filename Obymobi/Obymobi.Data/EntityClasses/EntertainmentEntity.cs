﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'Entertainment'. This class is used for Business Logic or for framework extension code.</summary>
	[Serializable]
	public partial class EntertainmentEntity : EntertainmentEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
        , INullableCompanyRelatedEntity, IMediaContainingEntity, ITimestampEntity
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		/// <summary>CTor</summary>
		public EntertainmentEntity():base()
		{
		}

		/// <summary>CTor</summary>
		/// <param name="entertainmentId">PK value for Entertainment which data should be fetched into this Entertainment object</param>
		public EntertainmentEntity(System.Int32 entertainmentId):
			base(entertainmentId)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="entertainmentId">PK value for Entertainment which data should be fetched into this Entertainment object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public EntertainmentEntity(System.Int32 entertainmentId, IPrefetchPath prefetchPathToUse):
			base(entertainmentId, prefetchPathToUse)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="entertainmentId">PK value for Entertainment which data should be fetched into this Entertainment object</param>
		/// <param name="validator">The custom validator object for this EntertainmentEntity</param>
		public EntertainmentEntity(System.Int32 entertainmentId, IValidator validator):
			base(entertainmentId, validator)
		{
		}
		
		/// <summary>CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected EntertainmentEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}

		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode

        public bool ConfigureBrowserSettings = false;

        public delegate void AfterDeleteDelegate(EntertainmentEntity entity);
        public event AfterDeleteDelegate AfterDelete;

        protected override void OnAuditDeleteOfEntity()
        {
            if (AfterDelete != null)
                this.AfterDelete(this);

            base.OnAuditDeleteOfEntity();
        }

		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included Code

		#endregion
	}
}
