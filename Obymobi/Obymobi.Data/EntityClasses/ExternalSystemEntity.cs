﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    using System.Xml.Serialization;
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'ExternalSystem'. This class is used for Business Logic or for framework extension code.</summary>
	[Serializable]
	public partial class ExternalSystemEntity : ExternalSystemEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
        ,ICompanyRelatedEntity
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		/// <summary>CTor</summary>
		public ExternalSystemEntity():base()
		{
		}

		/// <summary>CTor</summary>
		/// <param name="externalSystemId">PK value for ExternalSystem which data should be fetched into this ExternalSystem object</param>
		public ExternalSystemEntity(System.Int32 externalSystemId):
			base(externalSystemId)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="externalSystemId">PK value for ExternalSystem which data should be fetched into this ExternalSystem object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public ExternalSystemEntity(System.Int32 externalSystemId, IPrefetchPath prefetchPathToUse):
			base(externalSystemId, prefetchPathToUse)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="externalSystemId">PK value for ExternalSystem which data should be fetched into this ExternalSystem object</param>
		/// <param name="validator">The custom validator object for this ExternalSystemEntity</param>
		public ExternalSystemEntity(System.Int32 externalSystemId, IValidator validator):
			base(externalSystemId, validator)
		{
		}
		
		/// <summary>CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected ExternalSystemEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}

		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode

        [DataGridViewColumnVisible]
        [XmlIgnore]
        [EntityFieldDependency((int)ExternalSystemFieldIndex.Type)]
        public string TypeText
        {
            get
            {
                return this.Type.ToString();
            }
        }

		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included Code

		#endregion
	}
}
