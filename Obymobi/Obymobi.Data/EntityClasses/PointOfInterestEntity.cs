﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    using System.Xml.Serialization;
    using System.Linq;
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'PointOfInterest'. This class is used for Business Logic or for framework extension code.</summary>
	[Serializable]
	public partial class PointOfInterestEntity : PointOfInterestEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
        , IMediaContainingEntity, INonCompanyRelatedEntity, ICustomTextContainingEntity, ITimestampEntity
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		/// <summary>CTor</summary>
		public PointOfInterestEntity():base()
		{
		}

		/// <summary>CTor</summary>
		/// <param name="pointOfInterestId">PK value for PointOfInterest which data should be fetched into this PointOfInterest object</param>
		public PointOfInterestEntity(System.Int32 pointOfInterestId):
			base(pointOfInterestId)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="pointOfInterestId">PK value for PointOfInterest which data should be fetched into this PointOfInterest object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public PointOfInterestEntity(System.Int32 pointOfInterestId, IPrefetchPath prefetchPathToUse):
			base(pointOfInterestId, prefetchPathToUse)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="pointOfInterestId">PK value for PointOfInterest which data should be fetched into this PointOfInterest object</param>
		/// <param name="validator">The custom validator object for this PointOfInterestEntity</param>
		public PointOfInterestEntity(System.Int32 pointOfInterestId, IValidator validator):
			base(pointOfInterestId, validator)
		{
		}
		
		/// <summary>CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected PointOfInterestEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}

		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode

        /// <summary>
        /// Gets a value indicating whether the POI has been imported from external content
        /// </summary>
        [XmlIgnore]
        public bool IsImported
        {
            get
            {
                return this.ExternalId.HasValue;
            }
        }

        [XmlIgnore]
        public UIModeEntity TabletUIModeEntity
        {
            get
            {
                return this.UIModeCollection.FirstOrDefault(x => x.Type == Enums.UIModeType.GuestOwnedTabletDevices);
            }
        }

        [XmlIgnore]
        public UIModeEntity MobileUIModeEntity
        {
            get
            {
                return this.UIModeCollection.FirstOrDefault(x => x.Type == Enums.UIModeType.GuestOwnedMobileDevices);
            }
        }

		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included Code

		#endregion
	}
}
