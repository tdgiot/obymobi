﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    using System.Xml.Serialization;
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'TerminalMessageTemplateCategory'. This class is used for Business Logic or for framework extension code.</summary>
	[Serializable]
	public partial class TerminalMessageTemplateCategoryEntity : TerminalMessageTemplateCategoryEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
        , INonCompanyRelatedEntity
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		/// <summary>CTor</summary>
		public TerminalMessageTemplateCategoryEntity():base()
		{
		}

		/// <summary>CTor</summary>
		/// <param name="terminalMessageTemplateCategoryId">PK value for TerminalMessageTemplateCategory which data should be fetched into this TerminalMessageTemplateCategory object</param>
		public TerminalMessageTemplateCategoryEntity(System.Int32 terminalMessageTemplateCategoryId):
			base(terminalMessageTemplateCategoryId)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="terminalMessageTemplateCategoryId">PK value for TerminalMessageTemplateCategory which data should be fetched into this TerminalMessageTemplateCategory object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public TerminalMessageTemplateCategoryEntity(System.Int32 terminalMessageTemplateCategoryId, IPrefetchPath prefetchPathToUse):
			base(terminalMessageTemplateCategoryId, prefetchPathToUse)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="terminalMessageTemplateCategoryId">PK value for TerminalMessageTemplateCategory which data should be fetched into this TerminalMessageTemplateCategory object</param>
		/// <param name="validator">The custom validator object for this TerminalMessageTemplateCategoryEntity</param>
		public TerminalMessageTemplateCategoryEntity(System.Int32 terminalMessageTemplateCategoryId, IValidator validator):
			base(terminalMessageTemplateCategoryId, validator)
		{
		}
		
		/// <summary>CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected TerminalMessageTemplateCategoryEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}

		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode

        [XmlIgnore]
        [DataGridViewColumnVisibleAttribute]
	    public string TerminalName
	    {
            get { return this.TerminalEntity.Name;  }
	    }

        [XmlIgnore]
        [DataGridViewColumnVisibleAttribute]
        public string MessageTemplateCategoryName
        {
            get { return this.MessageTemplateCategoryEntity.Name; }
        }

		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included Code

		#endregion
	}
}
