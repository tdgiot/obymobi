﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    using System.Xml.Serialization;
    // __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'OrderitemAlterationitem'. This class is used for Business Logic or for framework extension code.</summary>
	[Serializable]
	public partial class OrderitemAlterationitemEntity : OrderitemAlterationitemEntityBase
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
        , ICompanyRelatedChildEntity, ITimestampEntity
    // __LLBLGENPRO_USER_CODE_REGION_END	
	{
		/// <summary>CTor</summary>
		public OrderitemAlterationitemEntity():base()
		{
		}

		/// <summary>CTor</summary>
		/// <param name="orderitemAlterationitemId">PK value for OrderitemAlterationitem which data should be fetched into this OrderitemAlterationitem object</param>
		public OrderitemAlterationitemEntity(System.Int32 orderitemAlterationitemId):
			base(orderitemAlterationitemId)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="orderitemAlterationitemId">PK value for OrderitemAlterationitem which data should be fetched into this OrderitemAlterationitem object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public OrderitemAlterationitemEntity(System.Int32 orderitemAlterationitemId, IPrefetchPath prefetchPathToUse):
			base(orderitemAlterationitemId, prefetchPathToUse)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="orderitemAlterationitemId">PK value for OrderitemAlterationitem which data should be fetched into this OrderitemAlterationitem object</param>
		/// <param name="validator">The custom validator object for this OrderitemAlterationitemEntity</param>
		public OrderitemAlterationitemEntity(System.Int32 orderitemAlterationitemId, IValidator validator):
			base(orderitemAlterationitemId, validator)
		{
		}
		
		/// <summary>CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected OrderitemAlterationitemEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
            // __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
            // __LLBLGENPRO_USER_CODE_REGION_END
		}

		#region Custom Entity code
		
        // __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode

        [XmlIgnore]
        public CommonEntityBase Parent
        {
            get { return this.OrderitemEntity; }
        }

        private bool overrideChangeProtection = false;
        /// <summary>
        /// When set to true it's possible to change an Order when it's already saved
        /// 
        /// </summary>
        public bool OverrideChangeProtection
        {
            get
            {
                return this.overrideChangeProtection;
            }
            set
            {
                this.overrideChangeProtection = value;
            }
        }

        private bool validatedByOrderProcessingHelper = false;

        /// <summary>
        /// Gets or sets the VerifiedByOrderProcessingHelper
        /// </summary>
        public bool ValidatedByOrderProcessingHelper
        {
            get
            {
                return this.validatedByOrderProcessingHelper;
            }
            set
            {
                this.validatedByOrderProcessingHelper = value;
            }
        }

        // __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included Code

		#endregion
	}
}
