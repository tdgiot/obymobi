﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    using System.Xml.Serialization;
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'Deliverypoint'. This class is used for Business Logic or for framework extension code.</summary>
	[Serializable]
	public partial class DeliverypointEntity : DeliverypointEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
        , ICompanyRelatedEntity, ITimestampEntity
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		/// <summary>CTor</summary>
		public DeliverypointEntity():base()
		{
		}

		/// <summary>CTor</summary>
		/// <param name="deliverypointId">PK value for Deliverypoint which data should be fetched into this Deliverypoint object</param>
		public DeliverypointEntity(System.Int32 deliverypointId):
			base(deliverypointId)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="deliverypointId">PK value for Deliverypoint which data should be fetched into this Deliverypoint object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public DeliverypointEntity(System.Int32 deliverypointId, IPrefetchPath prefetchPathToUse):
			base(deliverypointId, prefetchPathToUse)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="deliverypointId">PK value for Deliverypoint which data should be fetched into this Deliverypoint object</param>
		/// <param name="validator">The custom validator object for this DeliverypointEntity</param>
		public DeliverypointEntity(System.Int32 deliverypointId, IValidator validator):
			base(deliverypointId, validator)
		{
		}
		
		/// <summary>CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected DeliverypointEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}

		#region Custom Entity code
		
        // __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode

        #region Properties

        /// <summary>
        /// Gets the name of the deliverypoint plus the name of the deliveryponint group
        /// </summary>
        [XmlIgnore]
        public string NameFull
        {
            get
            {
                return string.Format("{0} {1}", this.Name, this.DeliverypointgroupEntity.Name);
            }
        }

        [XmlIgnore]
        public string DeliverypointgroupNameDeliverypointNumber
        {
            get
            {
                return string.Format("{0} - {1}", this.DeliverypointgroupEntity.Name, this.Name);
            }
        }
        
        /// <summary>
        /// Gets a flag which indicates if this deliverypoint has clients connected to it
        /// PREFETCH THE CLIENTCOLLECTION IF YOU USE THIS PROPERTY
        /// </summary>
        [XmlIgnore]
        public bool HasClient
        {
            get
            {
                return (this.ClientCollection.Count > 0);
            }
        }

        /// <summary>
        /// Gets a flag which indicates if this deliverypoint has online clients connected to it
        /// PREFETCH THE CLIENTCOLLECTION IF YOU USE THIS PROPERTY
        /// </summary>
        [XmlIgnore]
        public bool HasOnlineClient
        {
            get
            {
                bool isOnline = false;

                foreach (ClientEntity client in this.ClientCollection)
                {
                    if (client.IsOnline)
                    {
                        isOnline = true;
                        break;
                    }
                }
                return isOnline;
            }
        }

        /// <summary>
        /// Gets the datetime of the last request
        /// PREFETCH THE CLIENTCOLLECTION IF YOU USE THIS PROPERTY
        /// </summary>
        [XmlIgnore]
        public DateTime? LastRequest
        {
            get
            {
                DateTime? lastRequest = null;

                foreach (ClientEntity client in this.ClientCollection)
                {
                    if (client.DeviceEntity.LastRequestUTC.HasValue)
                    {
                        if (lastRequest == null || client.DeviceEntity.LastRequestUTC.Value > lastRequest.Value)
                            lastRequest = client.DeviceEntity.LastRequestUTC.Value;
                    }
                }

                return lastRequest;
            }
        }

        [XmlIgnore]
        public int NumberAsInteger
        {
            get
            {
                int number = 999999;
                int.TryParse(this.Number, out number);
                return number;    
            }
        }

        [XmlIgnore]
        public bool ClientConfigurationIdChanged { get; set; }

        #endregion

        // __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included Code

		#endregion
	}
}
