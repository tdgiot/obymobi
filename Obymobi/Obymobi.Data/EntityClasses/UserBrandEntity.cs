﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'UserBrand'. This class is used for Business Logic or for framework extension code.</summary>
	[Serializable]
	public partial class UserBrandEntity : UserBrandEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
        , INonCompanyRelatedEntity
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		/// <summary>CTor</summary>
		public UserBrandEntity():base()
		{
		}

		/// <summary>CTor</summary>
		/// <param name="userBrandId">PK value for UserBrand which data should be fetched into this UserBrand object</param>
		public UserBrandEntity(System.Int32 userBrandId):
			base(userBrandId)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="userBrandId">PK value for UserBrand which data should be fetched into this UserBrand object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public UserBrandEntity(System.Int32 userBrandId, IPrefetchPath prefetchPathToUse):
			base(userBrandId, prefetchPathToUse)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="userBrandId">PK value for UserBrand which data should be fetched into this UserBrand object</param>
		/// <param name="validator">The custom validator object for this UserBrandEntity</param>
		public UserBrandEntity(System.Int32 userBrandId, IValidator validator):
			base(userBrandId, validator)
		{
		}
		
		/// <summary>CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected UserBrandEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}

		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode

        /// <summary>
        /// Gets the name of the role for the current user
        /// </summary>
        [DataGridViewColumnVisible]
        [EntityFieldDependency((int)UserBrandFieldIndex.Role)]
        public string RoleName
        {
            get
            {
                return this.Role.ToString();
            }
        }

		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included Code

		#endregion
	}
}
