﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    using System.Xml.Serialization;
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'EntertainmentDependency'. This class is used for Business Logic or for framework extension code.</summary>
	[Serializable]
	public partial class EntertainmentDependencyEntity : EntertainmentDependencyEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
        , INullableCompanyRelatedChildEntity, ITimestampEntity
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		/// <summary>CTor</summary>
		public EntertainmentDependencyEntity():base()
		{
		}

		/// <summary>CTor</summary>
		/// <param name="entertainmentDependencyId">PK value for EntertainmentDependency which data should be fetched into this EntertainmentDependency object</param>
		public EntertainmentDependencyEntity(System.Int32 entertainmentDependencyId):
			base(entertainmentDependencyId)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="entertainmentDependencyId">PK value for EntertainmentDependency which data should be fetched into this EntertainmentDependency object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public EntertainmentDependencyEntity(System.Int32 entertainmentDependencyId, IPrefetchPath prefetchPathToUse):
			base(entertainmentDependencyId, prefetchPathToUse)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="entertainmentDependencyId">PK value for EntertainmentDependency which data should be fetched into this EntertainmentDependency object</param>
		/// <param name="validator">The custom validator object for this EntertainmentDependencyEntity</param>
		public EntertainmentDependencyEntity(System.Int32 entertainmentDependencyId, IValidator validator):
			base(entertainmentDependencyId, validator)
		{
		}
		
		/// <summary>CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected EntertainmentDependencyEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}

		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode

        [XmlIgnore]
        public CommonEntityBase Parent
        {
            get { return this.EntertainmentEntity; }
        }

		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included Code

		#endregion
	}
}
