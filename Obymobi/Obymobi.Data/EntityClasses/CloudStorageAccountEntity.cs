﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    using System.Xml.Serialization;
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'CloudStorageAccount'. This class is used for Business Logic or for framework extension code.</summary>
	[Serializable]
	public partial class CloudStorageAccountEntity : CloudStorageAccountEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
        , ICompanyRelatedEntity, ITimestampEntity
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		/// <summary>CTor</summary>
		public CloudStorageAccountEntity():base()
		{
		}

		/// <summary>CTor</summary>
		/// <param name="cloudStorageAccountId">PK value for CloudStorageAccount which data should be fetched into this CloudStorageAccount object</param>
		public CloudStorageAccountEntity(System.Int32 cloudStorageAccountId):
			base(cloudStorageAccountId)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="cloudStorageAccountId">PK value for CloudStorageAccount which data should be fetched into this CloudStorageAccount object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public CloudStorageAccountEntity(System.Int32 cloudStorageAccountId, IPrefetchPath prefetchPathToUse):
			base(cloudStorageAccountId, prefetchPathToUse)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="cloudStorageAccountId">PK value for CloudStorageAccount which data should be fetched into this CloudStorageAccount object</param>
		/// <param name="validator">The custom validator object for this CloudStorageAccountEntity</param>
		public CloudStorageAccountEntity(System.Int32 cloudStorageAccountId, IValidator validator):
			base(cloudStorageAccountId, validator)
		{
		}
		
		/// <summary>CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected CloudStorageAccountEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}

		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode

        [DataGridViewColumnVisible]
        [XmlIgnore]
        public string TypeName
        {
            get
            {
                return this.Type.ToString();
            }
        }

		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included Code

		#endregion
	}
}
