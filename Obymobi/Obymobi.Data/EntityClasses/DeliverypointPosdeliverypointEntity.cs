﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 2.6
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates.NET20
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>
	/// Entity class which represents the entity 'DeliverypointPosdeliverypoint'. <br/>
	/// This class is used for Business Logic or for framework extension code. 
	/// </summary>
	[Serializable]
	public partial class DeliverypointPosdeliverypointEntity : DeliverypointPosdeliverypointEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Constructors
		/// <summary>
		/// CTor
		/// </summary>
		public DeliverypointPosdeliverypointEntity():base()
		{
		}

	
		/// <summary>
		/// CTor
		/// </summary>
		/// <param name="deliverypointPosdeliverypointId">PK value for DeliverypointPosdeliverypoint which data should be fetched into this DeliverypointPosdeliverypoint object</param>
		public DeliverypointPosdeliverypointEntity(System.Int32 deliverypointPosdeliverypointId):
			base(deliverypointPosdeliverypointId)
		{
		}


		/// <summary>
		/// CTor
		/// </summary>
		/// <param name="deliverypointPosdeliverypointId">PK value for DeliverypointPosdeliverypoint which data should be fetched into this DeliverypointPosdeliverypoint object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public DeliverypointPosdeliverypointEntity(System.Int32 deliverypointPosdeliverypointId, IPrefetchPath prefetchPathToUse):
			base(deliverypointPosdeliverypointId, prefetchPathToUse)
		{
		}


		/// <summary>
		/// CTor
		/// </summary>
		/// <param name="deliverypointPosdeliverypointId">PK value for DeliverypointPosdeliverypoint which data should be fetched into this DeliverypointPosdeliverypoint object</param>
		/// <param name="validator">The custom validator object for this DeliverypointPosdeliverypointEntity</param>
		public DeliverypointPosdeliverypointEntity(System.Int32 deliverypointPosdeliverypointId, IValidator validator):
			base(deliverypointPosdeliverypointId, validator)
		{
		}
	
		
		/// <summary>
		/// Private CTor for deserialization
		/// </summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected DeliverypointPosdeliverypointEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		#endregion

		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included Code

		#endregion
	}
}
