﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    using Dionysos.Interfaces.Data;
    using System.Collections.Generic;
    using Obymobi.Data.HelperClasses;
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'EntityInformation'. This class is used for Business Logic or for framework extension code.</summary>
	[Serializable]
	public partial class EntityInformationEntity : EntityInformationEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
        , IEntityInformation, ISystemEntity
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		/// <summary>CTor</summary>
		public EntityInformationEntity():base()
		{
		}

		/// <summary>CTor</summary>
		/// <param name="entityInformationId">PK value for EntityInformation which data should be fetched into this EntityInformation object</param>
		public EntityInformationEntity(System.Int32 entityInformationId):
			base(entityInformationId)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="entityInformationId">PK value for EntityInformation which data should be fetched into this EntityInformation object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public EntityInformationEntity(System.Int32 entityInformationId, IPrefetchPath prefetchPathToUse):
			base(entityInformationId, prefetchPathToUse)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="entityInformationId">PK value for EntityInformation which data should be fetched into this EntityInformation object</param>
		/// <param name="validator">The custom validator object for this EntityInformationEntity</param>
		public EntityInformationEntity(System.Int32 entityInformationId, IValidator validator):
			base(entityInformationId, validator)
		{
		}
		
		/// <summary>CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected EntityInformationEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}

		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode

        /// <summary>
        /// Retrieve a EntityInformationEntity for a EntityType
        /// </summary>
        /// <param name="entityType"></param>
        public static EntityInformationEntity GetEntityInformation(IEntity entity)
        {
            return GetEntityInformation(Dionysos.Data.LLBLGen.LLBLGenUtil.GetEntityName(entity));
        }

        /// <summary>
        /// Get information about a entity
        /// </summary>
        /// <param name="entityName">EntityName to find information for</param>
        /// <returns>EntityInformationEntity with relevant info, or null if not found</returns>
        public static EntityInformationEntity GetEntityInformation(string entityName)
        {
            entityName = Dionysos.Data.LLBLGen.LLBLGenUtil.GetEntityName(entityName);
            EntityInformationCollection entityInformationCollection = new EntityInformationCollection();
            PredicateExpression filter = new PredicateExpression(EntityInformationFields.EntityName == entityName);
            entityInformationCollection.GetMulti(filter);

            if (entityInformationCollection.Count != 1)
            {
                return null;
            }
            else
            {
                return entityInformationCollection[0];
            }
        }

        /// <summary>
        /// Check if both FriendlyName and FriendlyNamePlural have a value
        /// </summary>
        public bool ContainsFriendlyNames
        {
            get
            {
                bool toReturn = true;

                if (this.FriendlyName == null || this.FriendlyName.Length == 0)
                    toReturn = false;
                else if (this.FriendlyNamePlural == null || this.FriendlyNamePlural.Length == 0)
                    toReturn = false;

                return toReturn;
            }
        }

        private Dictionary<string, IEntityFieldInformation> allFields = null;
        private Dictionary<string, IEntityFieldInformation> databaseFields = null;
        private Dictionary<string, IEntityFieldInformation> codeFields = null;

        /// <summary>
        /// Gets or sets the codeFields
        /// </summary>
        public Dictionary<string, IEntityFieldInformation> CodeFields
        {
            get
            {
                return this.codeFields;
            }
            set
            {
                this.codeFields = value;
            }
        }



        /// <summary>
        /// Gets or sets the databaseFields
        /// </summary>
        public Dictionary<string, IEntityFieldInformation> DatabaseFields
        {
            get
            {
                return this.databaseFields;
            }
            set
            {
                this.databaseFields = value;
            }
        }


        /// <summary>
        /// Gets or sets the allFields
        /// </summary>
        public Dictionary<string, IEntityFieldInformation> AllFields
        {
            get
            {
                return this.allFields;
            }
            set
            {
                this.allFields = value;
            }
        }


        Dictionary<string, IEntityFieldInformation> primaryKeyFields;
        public Dictionary<string, IEntityFieldInformation> PrimaryKeyFields
        {
            get
            {
                return this.primaryKeyFields;
            }
            set
            {
                this.primaryKeyFields = value;
            }
        }

        Dictionary<string, IEntityFieldInformation> foreignKeyFields;
        public Dictionary<string, IEntityFieldInformation> ForeignKeyFields
        {
            get
            {
                return this.foreignKeyFields;
            }
            set
            {
                this.foreignKeyFields = value;
            }
        }

        Dictionary<string, IEntityRelationInformation> relationInformation;
        public Dictionary<string, IEntityRelationInformation> RelationInformation
        {
            get
            {
                return this.relationInformation;
            }
            set
            {
                this.relationInformation = value;
            }
        }

		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included Code

		#endregion
	}
}
