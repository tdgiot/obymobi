﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    using System.Xml.Serialization;
    // __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'RoomControlSectionLanguage'. This class is used for Business Logic or for framework extension code.</summary>
	[Serializable]
	public partial class RoomControlSectionLanguageEntity : RoomControlSectionLanguageEntityBase
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
    , ICompanyRelatedChildEntityOrNullableCompanyRelatedChildEntity
    // __LLBLGENPRO_USER_CODE_REGION_END	
	{
		/// <summary>CTor</summary>
		public RoomControlSectionLanguageEntity():base()
		{
		}

		/// <summary>CTor</summary>
		/// <param name="roomControlSectionLanguageId">PK value for RoomControlSectionLanguage which data should be fetched into this RoomControlSectionLanguage object</param>
		public RoomControlSectionLanguageEntity(System.Int32 roomControlSectionLanguageId):
			base(roomControlSectionLanguageId)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="roomControlSectionLanguageId">PK value for RoomControlSectionLanguage which data should be fetched into this RoomControlSectionLanguage object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public RoomControlSectionLanguageEntity(System.Int32 roomControlSectionLanguageId, IPrefetchPath prefetchPathToUse):
			base(roomControlSectionLanguageId, prefetchPathToUse)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="roomControlSectionLanguageId">PK value for RoomControlSectionLanguage which data should be fetched into this RoomControlSectionLanguage object</param>
		/// <param name="validator">The custom validator object for this RoomControlSectionLanguageEntity</param>
		public RoomControlSectionLanguageEntity(System.Int32 roomControlSectionLanguageId, IValidator validator):
			base(roomControlSectionLanguageId, validator)
		{
		}
		
		/// <summary>CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected RoomControlSectionLanguageEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}

		#region Custom Entity code
		
        // __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode

        [XmlIgnore]
        public CommonEntityBase Parent
        {
            get { return this.RoomControlSectionEntity; }
        }

        // __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included Code

		#endregion
	}
}
