﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    using System.Xml.Serialization;
    using Obymobi.Logic.Cms;
    using Obymobi.Enums;
    // __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'ProductAlteration'. This class is used for Business Logic or for framework extension code.</summary>
	[Serializable]
	public partial class ProductAlterationEntity : ProductAlterationEntityBase
        // __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
        , INullableCompanyRelatedChildEntity, INullableBrandRelatedChildEntity, ISkipAuthorization
        // __LLBLGENPRO_USER_CODE_REGION_END	
	{
		/// <summary>CTor</summary>
		public ProductAlterationEntity():base()
		{
		}

		/// <summary>CTor</summary>
		/// <param name="productAlterationId">PK value for ProductAlteration which data should be fetched into this ProductAlteration object</param>
		public ProductAlterationEntity(System.Int32 productAlterationId):
			base(productAlterationId)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="productAlterationId">PK value for ProductAlteration which data should be fetched into this ProductAlteration object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public ProductAlterationEntity(System.Int32 productAlterationId, IPrefetchPath prefetchPathToUse):
			base(productAlterationId, prefetchPathToUse)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="productAlterationId">PK value for ProductAlteration which data should be fetched into this ProductAlteration object</param>
		/// <param name="validator">The custom validator object for this ProductAlterationEntity</param>
		public ProductAlterationEntity(System.Int32 productAlterationId, IValidator validator):
			base(productAlterationId, validator)
		{
		}
		
		/// <summary>CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected ProductAlterationEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
            // __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
            // __LLBLGENPRO_USER_CODE_REGION_END
		}

		#region Custom Entity code
		
        // __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode

        [XmlIgnore]
        public CommonEntityBase Parent
        {
            get { return this.ProductEntity; }
        }

        /// <summary>
        /// Indentified for Validators to know if it's being deleted as part of PosproductPosalteration being deleted.
        /// </summary>
        [XmlIgnore]
        public bool BeingDeletedInPosProductPosAlterationDeletionTransaction = false;

        public int? ParentBrandId
        {
            get
            {
                if (this.AlterationEntity.BrandId.HasValue)
                {
                    return this.AlterationEntity.BrandId;
                }

                if (this.ProductEntity.BrandId.HasValue)
                {
                    return this.ProductEntity.BrandId;
                }

                return null;
            }
        }

        public CommonEntityBase BrandParent
        {
            get
            {
                if (this.AlterationEntity.BrandId.HasValue)
                {
                    return this.AlterationEntity;
                }

                if (this.ProductEntity.BrandId.HasValue)
                {
                    return this.ProductEntity;
                }

                return null;
            }
        }

        public BrandEntity BrandParentBrandEntity
        {
            get
            {
                if (this.AlterationEntity.BrandId.HasValue)
                {
                    return this.AlterationEntity.BrandEntity;
                }

                if (this.ProductEntity.BrandId.HasValue)
                {
                    return this.ProductEntity.BrandEntity;
                }

                return null;
            }
        }

        // __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included Code

		#endregion
	}
}
