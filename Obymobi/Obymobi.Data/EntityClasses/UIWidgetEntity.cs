﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    using System.Xml.Serialization;
    using Obymobi.Enums;
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'UIWidget'. This class is used for Business Logic or for framework extension code.</summary>
	[Serializable]
	public partial class UIWidgetEntity : UIWidgetEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
        , INullableCompanyRelatedChildEntity, IMediaContainingEntity, ICustomTextContainingEntity, ITimestampEntity
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		/// <summary>CTor</summary>
		public UIWidgetEntity():base()
		{
		}

		/// <summary>CTor</summary>
		/// <param name="uIWidgetId">PK value for UIWidget which data should be fetched into this UIWidget object</param>
		public UIWidgetEntity(System.Int32 uIWidgetId):
			base(uIWidgetId)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="uIWidgetId">PK value for UIWidget which data should be fetched into this UIWidget object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public UIWidgetEntity(System.Int32 uIWidgetId, IPrefetchPath prefetchPathToUse):
			base(uIWidgetId, prefetchPathToUse)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="uIWidgetId">PK value for UIWidget which data should be fetched into this UIWidget object</param>
		/// <param name="validator">The custom validator object for this UIWidgetEntity</param>
		public UIWidgetEntity(System.Int32 uIWidgetId, IValidator validator):
			base(uIWidgetId, validator)
		{
		}
		
		/// <summary>CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected UIWidgetEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}

		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode

        [XmlIgnore]
        public CommonEntityBase Parent
        {
            get { return this.UITabEntity; }
        }

        public string NameOrTypeName
        {
            get
            {
                if (!string.IsNullOrWhiteSpace(this.Name))
                {
                    return this.Name;
                }
                if (this.Type > 0)
                {
                    return Enum.GetName(typeof(UIWidgetType), (int)this.Type);
                }
                return "Unknown";
            }
        }

        [DataGridViewColumnVisible]
        [XmlIgnore]
        public string TypeName
        {
            get
            {
                if (this.Type > 0)
                {
                    return Enum.GetName(typeof(UIWidgetType), (int)this.Type);
                }
                return "Unknown";
            }
        }

        [DataGridViewColumnVisible]
        [XmlIgnore]
        public string CaptionOrType
        {
            get
            {
                string toReturn = this.Caption;
                if (string.IsNullOrWhiteSpace(toReturn))
                    toReturn = this.TypeName;

                return toReturn;
            }
        }

        [XmlIgnore]
        public string LinkedItemDescription
        {
            get
            {
                if (this.AdvertisementId.HasValue)
                {
                    return string.Format("Advertisement: {0}", this.AdvertisementEntity.Name);
                }
                else if (this.ProductCategoryId.HasValue)
                {
                    return string.Format("Product: {0}", this.ProductCategoryEntity.ProductEntity.Name);
                }
                else if (this.CategoryId.HasValue)
                {
                    return string.Format("Category: {0}", this.CategoryEntity.Name);
                }
                else if (this.EntertainmentId.HasValue)
                {
                    return string.Format("Entertainment: {0}", this.EntertainmentEntity.Name);
                }
                else if (this.EntertainmentcategoryId.HasValue)
                {
                    return string.Format("Entertainmentcategory: {0}", this.EntertainmentcategoryEntity.Name);
                }
                else if (this.SiteId.HasValue)
                {
                    return string.Format("Site: {0}", this.SiteEntity.Name);
                }
                else if (this.PageId.HasValue)
                {
                    return string.Format("Page: {0}", this.PageEntity.Name);
                }
                else if (this.UITabType.HasValue && this.UITabType.Value != Obymobi.Enums.UITabType.Unknown)
                {
                    return string.Format("Tab: {0}", this.UITabType.Value.ToString());
                }
                else if (!string.IsNullOrWhiteSpace(this.Url))
                {
                    return string.Format("URL: {0}", this.Url);
                }
                else if (this.RoomControlType.HasValue)
                {
                    return string.Format("Room Control: {0}", this.RoomControlType.Value.ToString());
                }
                return string.Empty;
            }
        }

		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included Code

		#endregion
	}
}
