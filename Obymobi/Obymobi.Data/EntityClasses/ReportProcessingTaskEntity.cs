﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    using Dionysos;
    using System.Xml.Serialization;

    // __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'ReportProcessingTask'. This class is used for Business Logic or for framework extension code.</summary>
	[Serializable]
	public partial class ReportProcessingTaskEntity : ReportProcessingTaskEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
        ,INullableCompanyRelatedEntity
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		/// <summary>CTor</summary>
		public ReportProcessingTaskEntity():base()
		{
		}

		/// <summary>CTor</summary>
		/// <param name="reportProcessingTaskId">PK value for ReportProcessingTask which data should be fetched into this ReportProcessingTask object</param>
		public ReportProcessingTaskEntity(System.Int32 reportProcessingTaskId):
			base(reportProcessingTaskId)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="reportProcessingTaskId">PK value for ReportProcessingTask which data should be fetched into this ReportProcessingTask object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public ReportProcessingTaskEntity(System.Int32 reportProcessingTaskId, IPrefetchPath prefetchPathToUse):
			base(reportProcessingTaskId, prefetchPathToUse)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="reportProcessingTaskId">PK value for ReportProcessingTask which data should be fetched into this ReportProcessingTask object</param>
		/// <param name="validator">The custom validator object for this ReportProcessingTaskEntity</param>
		public ReportProcessingTaskEntity(System.Int32 reportProcessingTaskId, IValidator validator):
			base(reportProcessingTaskId, validator)
		{
		}
		
		/// <summary>CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected ReportProcessingTaskEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}

		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode  

        [DataGridViewColumnVisible]
        public string Name
        {
            get
            {
                if (string.IsNullOrWhiteSpace(this.ReportName))
                {
                    return $"{this.CompanyEntity.Name} - {this.AnalyticsReportType} - {this.PeriodText}";
                }

                return this.ReportName;
            }
        }

        [DataGridViewColumnVisible]
        public DateTime? CreatedManual
        {
            get
            {
                return this.CreatedUTC;
            }
        }

        [DataGridViewColumnVisible]
        public string AnalyticsReportTypeAndPeriod
        {
            get
            {
                return string.Format("{0} - {1}", this.AnalyticsReportType, this.PeriodText);
            }
        }

        [DataGridViewColumnVisible]
        public string PeriodText
        {
            get
            {
                // GAKR I didn't dare to apply timezone translations here due to performance issues.
                return "{0} - {1}".FormatSafe(this.FromTimeZoned.GetValueOrDefault(DateTime.MinValue).ToShortDateString(), 
                                              this.TillTimeZoned.GetValueOrDefault(DateTime.MinValue).ToShortDateString());
            }
        }
        
        public DateTime? FromTimeZoned
        {
            get
            {
                if (!this.TimeZoneOlsonId.IsNullOrWhiteSpace() && this.FromUTC.HasValue)
                {
                    return this.ConvertFromUtc(this.FromUTC.GetValueOrDefault(DateTime.MinValue));
                }
                else
                {
                    return this.XFrom;
                }                
            }
        }

        public DateTime? TillTimeZoned
        {
            get
            {
                if (!this.TimeZoneOlsonId.IsNullOrWhiteSpace() && this.TillUTC.HasValue)
                {
                    return this.ConvertFromUtc(this.TillUTC.GetValueOrDefault(DateTime.MaxValue));
                }
                else
                {
                    return this.XTill;
                }                    
            }
        }

        public DateTime CreatedLocal => TryConvertFromutc(this.CreatedUTC.GetValueOrDefault());

        [DataGridViewColumnVisible]
        [XmlIgnore]
        public string CreatedLocalTime => CreatedLocal.ToString("g");

        public DateTime FromLocal => TryConvertFromutc(this.FromUTC.GetValueOrDefault());

        [DataGridViewColumnVisible]
        [XmlIgnore]
        public string FromLocalTime => FromLocal.ToString("d");

        public DateTime TillLocal => TryConvertFromutc(this.TillUTC.GetValueOrDefault());

        [DataGridViewColumnVisible]
        [XmlIgnore]
        public string TillLocalTime => TillLocal.ToString("d");

        private DateTime TryConvertFromutc(DateTime dateTimeUtc)
        {
            return this.TimeZoneOlsonId.IsNullOrWhiteSpace() ? dateTimeUtc : ConvertFromUtc(dateTimeUtc);
        }

        private DateTime ConvertFromUtc(DateTime dateTime)
        {
            return dateTime.UtcToLocalTime(this.GetTimeZoneInfo(this.TimeZoneOlsonId));
        }

        private TimeZoneInfo GetTimeZoneInfo(string timeZoneOlsonId)
        {
            return TimeZoneInfo.FindSystemTimeZoneById(Obymobi.TimeZone.Mappings[timeZoneOlsonId].WindowsTimeZoneId);
        }

        [DataGridViewColumnVisible]
        public string FilenameAsSpreadsheet => "{0}_{1}_{2}_{3}-{4}.xlsx".FormatSafe(this.CompanyEntity.Name,
            this.Name,
            this.AnalyticsReportType.ToString(),
            this.FromTimeZoned.GetValueOrDefault(DateTime.MinValue).ToString("yyyyMMdd"),
            this.TillTimeZoned.GetValueOrDefault(DateTime.MinValue).ToString("yyyyMMdd"));

        // __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included Code

		#endregion
	}
}
