﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    using System.Xml.Serialization;
    using Obymobi.Enums;
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'RoomControlWidget'. This class is used for Business Logic or for framework extension code.</summary>
	[Serializable]
	public partial class RoomControlWidgetEntity : RoomControlWidgetEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
        , ICompanyRelatedChildEntityOrNullableCompanyRelatedChildEntity, Obymobi.Logic.RoomControl.IRoomControlItem, ICustomTextContainingEntity, ITimestampEntity
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		/// <summary>CTor</summary>
		public RoomControlWidgetEntity():base()
		{
		}

		/// <summary>CTor</summary>
		/// <param name="roomControlWidgetId">PK value for RoomControlWidget which data should be fetched into this RoomControlWidget object</param>
		public RoomControlWidgetEntity(System.Int32 roomControlWidgetId):
			base(roomControlWidgetId)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="roomControlWidgetId">PK value for RoomControlWidget which data should be fetched into this RoomControlWidget object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public RoomControlWidgetEntity(System.Int32 roomControlWidgetId, IPrefetchPath prefetchPathToUse):
			base(roomControlWidgetId, prefetchPathToUse)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="roomControlWidgetId">PK value for RoomControlWidget which data should be fetched into this RoomControlWidget object</param>
		/// <param name="validator">The custom validator object for this RoomControlWidgetEntity</param>
		public RoomControlWidgetEntity(System.Int32 roomControlWidgetId, IValidator validator):
			base(roomControlWidgetId, validator)
		{
		}
		
		/// <summary>CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected RoomControlWidgetEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}

		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode

        [XmlIgnore]
        public CommonEntityBase Parent
        {
            get { return this.RoomControlSectionEntity; }
        }

        [XmlIgnore]
        public string ItemId
        {
            get
            {
                return string.Format("{0}-{1}", this.ItemType.ToString(), this.RoomControlWidgetId);
            }
        }

        [XmlIgnore]
        public string ParentItemId
        {
            get
            {
                if (this.RoomControlSectionItemId.HasValue)
                    return this.RoomControlSectionItemEntity.ItemId;

                return this.RoomControlSectionEntity.ItemId;
            }
        }

        [XmlIgnore]
        public string ItemType
        {
            get
            {
                return RoomControlItemType.Widget;
            }
        }

        [XmlIgnore]
        public string Name
        {
            get
            {
                return this.Caption;
            }
            set 
            {
                this.Caption = value;
            }
        }

        [XmlIgnore]
        public string NameSystem
        {
            get
            {
                return "";
            }
        }

        [DataGridViewColumnVisible]
        [XmlIgnore]
        public string CaptionOrType
        {
            get
            {
                string toReturn = this.Caption;
                if (string.IsNullOrWhiteSpace(toReturn))
                    toReturn = this.TypeName;

                return toReturn;
            }
        }

        [DataGridViewColumnVisible]
        [XmlIgnore]
        public string TypeName
        {
            get
            {
                return this.Type.ToString();
            }
        }

        [DataGridViewColumnVisible]
        [XmlIgnore]
        public string RoomControlComponentAreaAndName
        {
            get { return this.RoomControlComponentEntity.NameAndArea; }
        }

        public bool ValidatorCreateOrUpdateDefaultWidgetLanguage { get; set; }

	    public bool CaptionChanged { get; set; }

	    // __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included Code

		#endregion
	}
}
