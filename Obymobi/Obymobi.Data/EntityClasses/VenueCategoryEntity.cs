﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    using Obymobi.Enums;
    using System.Xml.Serialization;
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'VenueCategory'. This class is used for Business Logic or for framework extension code.</summary>
	[Serializable]
	public partial class VenueCategoryEntity : VenueCategoryEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
        , INonCompanyRelatedEntity, ICustomTextContainingEntity, ITimestampEntity
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		/// <summary>CTor</summary>
		public VenueCategoryEntity():base()
		{
		}

		/// <summary>CTor</summary>
		/// <param name="venueCategoryId">PK value for VenueCategory which data should be fetched into this VenueCategory object</param>
		public VenueCategoryEntity(System.Int32 venueCategoryId):
			base(venueCategoryId)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="venueCategoryId">PK value for VenueCategory which data should be fetched into this VenueCategory object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public VenueCategoryEntity(System.Int32 venueCategoryId, IPrefetchPath prefetchPathToUse):
			base(venueCategoryId, prefetchPathToUse)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="venueCategoryId">PK value for VenueCategory which data should be fetched into this VenueCategory object</param>
		/// <param name="validator">The custom validator object for this VenueCategoryEntity</param>
		public VenueCategoryEntity(System.Int32 venueCategoryId, IValidator validator):
			base(venueCategoryId, validator)
		{
		}
		
		/// <summary>CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected VenueCategoryEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}

		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode

        [DataGridViewColumnVisible]
        [EntityFieldDependency((int)VenueCategoryFieldIndex.MarkerIcon)]
        public string MarkerIconAsText
        {
            get
            {
                return this.MarkerIcon.ToString();
            }
        }

		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included Code

		#endregion
	}
}
