﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    using System.Xml.Serialization;
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'MessageTemplateCategoryMessageTemplate'. This class is used for Business Logic or for framework extension code.</summary>
	[Serializable]
	public partial class MessageTemplateCategoryMessageTemplateEntity : MessageTemplateCategoryMessageTemplateEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
        , INonCompanyRelatedEntity
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		/// <summary>CTor</summary>
		public MessageTemplateCategoryMessageTemplateEntity():base()
		{
		}

		/// <summary>CTor</summary>
		/// <param name="messageTemplateCategoryMessageTemplateId">PK value for MessageTemplateCategoryMessageTemplate which data should be fetched into this MessageTemplateCategoryMessageTemplate object</param>
		public MessageTemplateCategoryMessageTemplateEntity(System.Int32 messageTemplateCategoryMessageTemplateId):
			base(messageTemplateCategoryMessageTemplateId)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="messageTemplateCategoryMessageTemplateId">PK value for MessageTemplateCategoryMessageTemplate which data should be fetched into this MessageTemplateCategoryMessageTemplate object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public MessageTemplateCategoryMessageTemplateEntity(System.Int32 messageTemplateCategoryMessageTemplateId, IPrefetchPath prefetchPathToUse):
			base(messageTemplateCategoryMessageTemplateId, prefetchPathToUse)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="messageTemplateCategoryMessageTemplateId">PK value for MessageTemplateCategoryMessageTemplate which data should be fetched into this MessageTemplateCategoryMessageTemplate object</param>
		/// <param name="validator">The custom validator object for this MessageTemplateCategoryMessageTemplateEntity</param>
		public MessageTemplateCategoryMessageTemplateEntity(System.Int32 messageTemplateCategoryMessageTemplateId, IValidator validator):
			base(messageTemplateCategoryMessageTemplateId, validator)
		{
		}
		
		/// <summary>CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected MessageTemplateCategoryMessageTemplateEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}

		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode

        [XmlIgnore]
        public CommonEntityBase Parent
        {
            get { return this.MessageTemplateCategoryEntity; }
        }

        [DataGridViewColumnVisibleAttribute]
        [XmlIgnore]
        public string MessageTemplateCategoryName
        {
            get
            {
                return this.MessageTemplateCategoryEntity.Name;
            }
        }

        [DataGridViewColumnVisibleAttribute]
        [XmlIgnore]
        public string MessageTemplateName
        {
            get
            {
                return this.MessageTemplateEntity.Name;
            }
        }

		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included Code

		#endregion
	}
}
