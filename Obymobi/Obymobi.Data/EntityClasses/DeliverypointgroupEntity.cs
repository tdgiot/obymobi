﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    using Dionysos;
    using System.Xml.Serialization;
    using Obymobi.Enums;
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'Deliverypointgroup'. This class is used for Business Logic or for framework extension code.</summary>
	[Serializable]
	public partial class DeliverypointgroupEntity : DeliverypointgroupEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
        , ICompanyRelatedEntity, IMediaContainingEntity, ICustomTextContainingEntity, ITimestampEntity
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		/// <summary>CTor</summary>
		public DeliverypointgroupEntity():base()
		{
		}

		/// <summary>CTor</summary>
		/// <param name="deliverypointgroupId">PK value for Deliverypointgroup which data should be fetched into this Deliverypointgroup object</param>
		public DeliverypointgroupEntity(System.Int32 deliverypointgroupId):
			base(deliverypointgroupId)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="deliverypointgroupId">PK value for Deliverypointgroup which data should be fetched into this Deliverypointgroup object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public DeliverypointgroupEntity(System.Int32 deliverypointgroupId, IPrefetchPath prefetchPathToUse):
			base(deliverypointgroupId, prefetchPathToUse)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="deliverypointgroupId">PK value for Deliverypointgroup which data should be fetched into this Deliverypointgroup object</param>
		/// <param name="validator">The custom validator object for this DeliverypointgroupEntity</param>
		public DeliverypointgroupEntity(System.Int32 deliverypointgroupId, IValidator validator):
			base(deliverypointgroupId, validator)
		{
		}
		
		/// <summary>CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected DeliverypointgroupEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}

		#region Custom Entity code
		
        // __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode

        [XmlIgnore]
        public WifiSecurityMethod WifiSecurityMethodAsEnum 
        {
            get
            {
                return this.WifiSecurityMethod.ToEnum<WifiSecurityMethod>();
            }
            set
            {
                this.WifiSecurityMethod = (int)value;
            }
        }

        [XmlIgnore]
        public WifiEapMode WifiEapModeAsEnum
        {
            get
            {
                return this.WifiEapMode.ToEnum<WifiEapMode>();
            }
            set
            {
                this.WifiEapMode = (int)value;
            }
        }

        [XmlIgnore]
        public WifiPhase2Authentication WifiPhase2AuthenticationAsEnum
        {
            get
            {
                return this.WifiPhase2Authentication.ToEnum<WifiPhase2Authentication>();
            }
            set
            {
                this.WifiPhase2Authentication = (int)value;
            }
        }

        #region Status properties

        /// <summary>
        /// Gets a value indicating whether this instance has screen timeout.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if this instance has screen timeout; otherwise, <c>false</c>.
        /// </value>
        [XmlIgnore]
        public bool HasScreenTimeout
        {
            get
            {
                return (this.ScreenTimeoutInterval > 0);
            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance has reset timeout.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if this instance has reset timeout; otherwise, <c>false</c>.
        /// </value>
        [XmlIgnore]
        public bool HasResetTimeout
        {
            get
            {
                return (this.ResetTimeout > 0);
            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance has pincode.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if this instance has pincode; otherwise, <c>false</c>.
        /// </value>
        [XmlIgnore]
        public bool HasPincode
        {
            get
            {
                return !this.Pincode.IsNullOrWhiteSpace() && !this.Pincode.Equals("0");
            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance has pincode SU.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if this instance has pincode SU; otherwise, <c>false</c>.
        /// </value>
        [XmlIgnore]
        public bool HasPincodeSU
        {
            get
            {
                return !this.PincodeSU.IsNullOrWhiteSpace() && !this.PincodeSU.Equals("0");
            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance has pincode GM.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if this instance has pincode GM; otherwise, <c>false</c>.
        /// </value>
        [XmlIgnore]
        public bool HasPincodeGM
        {
            get
            {
                return !this.PincodeGM.IsNullOrWhiteSpace() && !this.PincodeGM.Equals("0");
            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance has locale.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if this instance has locale; otherwise, <c>false</c>.
        /// </value>
        [XmlIgnore]
        public bool HasLocale
        {
            get
            {
                return !this.Locale.IsNullOrWhiteSpace();
            }
        }

        [XmlIgnore]
        public bool BeingCopied { get; set; }

        [XmlIgnore]
        public bool ClientConfigurationIdChanged { get; set; }

        [XmlIgnore]
        public bool NameChanged { get; set; }

        [XmlIgnore]
        public bool EstimatedDeliveryTimeChanged { get; set; }

	    #endregion

        // __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included Code

		#endregion
	}
}
