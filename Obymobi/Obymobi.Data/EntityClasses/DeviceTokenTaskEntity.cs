﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    using Dionysos;
    // __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'DeviceTokenTask'. This class is used for Business Logic or for framework extension code.</summary>
	[Serializable]
	public partial class DeviceTokenTaskEntity : DeviceTokenTaskEntityBase
        // __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
        , INonCompanyRelatedEntity
        // __LLBLGENPRO_USER_CODE_REGION_END	
	{
		/// <summary>CTor</summary>
		public DeviceTokenTaskEntity():base()
		{
		}

		/// <summary>CTor</summary>
		/// <param name="deviceTokenTaskId">PK value for DeviceTokenTask which data should be fetched into this DeviceTokenTask object</param>
		public DeviceTokenTaskEntity(System.Int32 deviceTokenTaskId):
			base(deviceTokenTaskId)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="deviceTokenTaskId">PK value for DeviceTokenTask which data should be fetched into this DeviceTokenTask object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public DeviceTokenTaskEntity(System.Int32 deviceTokenTaskId, IPrefetchPath prefetchPathToUse):
			base(deviceTokenTaskId, prefetchPathToUse)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="deviceTokenTaskId">PK value for DeviceTokenTask which data should be fetched into this DeviceTokenTask object</param>
		/// <param name="validator">The custom validator object for this DeviceTokenTaskEntity</param>
		public DeviceTokenTaskEntity(System.Int32 deviceTokenTaskId, IValidator validator):
			base(deviceTokenTaskId, validator)
		{
		}
		
		/// <summary>CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected DeviceTokenTaskEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}

		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode

        public string DecryptedToken
        {
            get
            {
                return this.Token.IsNullOrWhiteSpace() ? string.Empty : Dionysos.Security.Cryptography.Cryptographer.DecryptUsingCBC(this.Token);
            }
        }

        // __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included Code

		#endregion
	}
}
