﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    using Obymobi.Enums;
    using System.Xml.Serialization;
    // __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'SurveyQuestion'. This class is used for Business Logic or for framework extension code.</summary>
	[Serializable]
	public partial class SurveyQuestionEntity : SurveyQuestionEntityBase
        // __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
        , INonCompanyRelatedEntity
    // __LLBLGENPRO_USER_CODE_REGION_END	
	{
		/// <summary>CTor</summary>
		public SurveyQuestionEntity():base()
		{
		}

		/// <summary>CTor</summary>
		/// <param name="surveyQuestionId">PK value for SurveyQuestion which data should be fetched into this SurveyQuestion object</param>
		public SurveyQuestionEntity(System.Int32 surveyQuestionId):
			base(surveyQuestionId)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="surveyQuestionId">PK value for SurveyQuestion which data should be fetched into this SurveyQuestion object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public SurveyQuestionEntity(System.Int32 surveyQuestionId, IPrefetchPath prefetchPathToUse):
			base(surveyQuestionId, prefetchPathToUse)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="surveyQuestionId">PK value for SurveyQuestion which data should be fetched into this SurveyQuestion object</param>
		/// <param name="validator">The custom validator object for this SurveyQuestionEntity</param>
		public SurveyQuestionEntity(System.Int32 surveyQuestionId, IValidator validator):
			base(surveyQuestionId, validator)
		{
		}
		
		/// <summary>CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected SurveyQuestionEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
            // __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
            // __LLBLGENPRO_USER_CODE_REGION_END
		}

		#region Custom Entity code
		
        // __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode

        [XmlIgnore]
        public SurveyQuestionType TypeAsEnum
        {
            get
            {
                return (SurveyQuestionType)this.Type;
            }
            set
            {
                this.Type = (int)value;
            }
        }

        /// <summary>
        /// Gets the text representation of the type
        /// </summary>
        [DataGridViewColumnVisible]
        [XmlIgnore]
        public string TypeAsText
        {
            get
            {
                return this.TypeAsEnum.ToString();
            }
        }

        public SurveyQuestionCollection AllChildSurveyQuestions()
        { 
            // Returns all child surveyQuestions including itself

            SurveyQuestionCollection questions = new SurveyQuestionCollection();

            // Add itself to the collection
            questions.Add(this);

            // Add the child questions to the collection
            questions.AddRange(this.SurveyQuestionCollection);

            // Return the questions
            return questions;
        }

        /// <summary>
        /// Gets the text representation of the question
        /// </summary>
        [DataGridViewColumnVisible]
        [XmlIgnore]
        public string FriendlyName
        {
            get
            {
                string name = this.Question;

                switch (this.TypeAsEnum)
                {
                    case SurveyQuestionType.Ranking:
                        name = this.FieldValue3;
                        break;
                    case SurveyQuestionType.Matrix:
                        name = this.FieldValue3;
                        break;
                    case SurveyQuestionType.Rating:
                        name = this.FieldValue2;
                        break;
                }

                return name;
            }
        }

        // __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included Code

		#endregion
	}
}
