﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    using System.Xml.Serialization;
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'Posalteration'. This class is used for Business Logic or for framework extension code.</summary>
	[Serializable]
	public partial class PosalterationEntity : PosalterationEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
        ,ICompanyRelatedEntity
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		/// <summary>CTor</summary>
		public PosalterationEntity():base()
		{
		}

		/// <summary>CTor</summary>
		/// <param name="posalterationId">PK value for Posalteration which data should be fetched into this Posalteration object</param>
		public PosalterationEntity(System.Int32 posalterationId):
			base(posalterationId)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="posalterationId">PK value for Posalteration which data should be fetched into this Posalteration object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public PosalterationEntity(System.Int32 posalterationId, IPrefetchPath prefetchPathToUse):
			base(posalterationId, prefetchPathToUse)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="posalterationId">PK value for Posalteration which data should be fetched into this Posalteration object</param>
		/// <param name="validator">The custom validator object for this PosalterationEntity</param>
		public PosalterationEntity(System.Int32 posalterationId, IValidator validator):
			base(posalterationId, validator)
		{
		}
		
		/// <summary>CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected PosalterationEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}

		#region Custom Entity code
		
        // __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode

        #region Properties

        /// <summary>
        /// Gets the name and external id of the Posalteration
        /// </summary>
        [XmlIgnore]
        public string NameAndExternalId
        {
            get
            {
                return string.Format("{0} ({1})", this.Name, this.ExternalId);
            }
        }

        #endregion

        // __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included Code

		#endregion
	}
}
