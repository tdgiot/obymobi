﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    using System.Xml.Serialization;
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'ExternalSystemLog'. This class is used for Business Logic or for framework extension code.</summary>
	[Serializable]
	public partial class ExternalSystemLogEntity : ExternalSystemLogEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		, INonCompanyRelatedEntity
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		/// <summary>CTor</summary>
		public ExternalSystemLogEntity():base()
		{
		}

		/// <summary>CTor</summary>
		/// <param name="externalSystemLogId">PK value for ExternalSystemLog which data should be fetched into this ExternalSystemLog object</param>
		public ExternalSystemLogEntity(System.Int32 externalSystemLogId):
			base(externalSystemLogId)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="externalSystemLogId">PK value for ExternalSystemLog which data should be fetched into this ExternalSystemLog object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public ExternalSystemLogEntity(System.Int32 externalSystemLogId, IPrefetchPath prefetchPathToUse):
			base(externalSystemLogId, prefetchPathToUse)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="externalSystemLogId">PK value for ExternalSystemLog which data should be fetched into this ExternalSystemLog object</param>
		/// <param name="validator">The custom validator object for this ExternalSystemLogEntity</param>
		public ExternalSystemLogEntity(System.Int32 externalSystemLogId, IValidator validator):
			base(externalSystemLogId, validator)
		{
		}
		
		/// <summary>CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected ExternalSystemLogEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}

		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode

        [DataGridViewColumnVisible]
        [XmlIgnore]
        [EntityFieldDependency((int)ExternalSystemLogFieldIndex.LogType)]
        public string TypeText => LogType.GetStringValue();

		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included Code

		#endregion
	}
}
