﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    using Dionysos.Data;
    using System.Xml.Serialization;
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'Application'. This class is used for Business Logic or for framework extension code.</summary>
	[Serializable]
	public partial class ApplicationEntity : ApplicationEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
        , INonCompanyRelatedEntity
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		/// <summary>CTor</summary>
		public ApplicationEntity():base()
		{
		}

		/// <summary>CTor</summary>
		/// <param name="applicationId">PK value for Application which data should be fetched into this Application object</param>
		public ApplicationEntity(System.Int32 applicationId):
			base(applicationId)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="applicationId">PK value for Application which data should be fetched into this Application object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public ApplicationEntity(System.Int32 applicationId, IPrefetchPath prefetchPathToUse):
			base(applicationId, prefetchPathToUse)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="applicationId">PK value for Application which data should be fetched into this Application object</param>
		/// <param name="validator">The custom validator object for this ApplicationEntity</param>
		public ApplicationEntity(System.Int32 applicationId, IValidator validator):
			base(applicationId, validator)
		{
		}
		
		/// <summary>CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected ApplicationEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}

		#region Custom Entity code
		
        // __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode

        #region Properties

        [DataGridViewColumnVisible]
        [XmlIgnore]
        public string StableVersion
        {
            get
            {
                return (this.ReleaseId.HasValue ? this.ReleaseEntity.Version : string.Empty);
            }
        }

        #endregion

        // __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included Code

		#endregion
	}
}
