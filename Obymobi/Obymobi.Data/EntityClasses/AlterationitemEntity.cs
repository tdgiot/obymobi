﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    using Dionysos.Data;
    using System.Xml.Serialization;
    using Obymobi.Logic.Cms;
    using Obymobi.Enums;
    // __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'Alterationitem'. This class is used for Business Logic or for framework extension code.</summary>
	[Serializable]
	public partial class AlterationitemEntity : AlterationitemEntityBase
        // __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
        , INullableCompanyRelatedChildEntity, INullableBrandRelatedChildEntity, ITimestampEntity
    // __LLBLGENPRO_USER_CODE_REGION_END	
	{
		/// <summary>CTor</summary>
		public AlterationitemEntity():base()
		{
		}

		/// <summary>CTor</summary>
		/// <param name="alterationitemId">PK value for Alterationitem which data should be fetched into this Alterationitem object</param>
		public AlterationitemEntity(System.Int32 alterationitemId):
			base(alterationitemId)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="alterationitemId">PK value for Alterationitem which data should be fetched into this Alterationitem object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public AlterationitemEntity(System.Int32 alterationitemId, IPrefetchPath prefetchPathToUse):
			base(alterationitemId, prefetchPathToUse)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="alterationitemId">PK value for Alterationitem which data should be fetched into this Alterationitem object</param>
		/// <param name="validator">The custom validator object for this AlterationitemEntity</param>
		public AlterationitemEntity(System.Int32 alterationitemId, IValidator validator):
			base(alterationitemId, validator)
		{
		}
		
		/// <summary>CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected AlterationitemEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
            // __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
            // __LLBLGENPRO_USER_CODE_REGION_END
		}

		#region Custom Entity code
		
        // __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode

        [XmlIgnore]
        public CommonEntityBase Parent
        {
            get { return this.AlterationEntity; }
        }

        [DataGridViewColumnVisibleAttribute]
        [XmlIgnore]
        public string AlterationName
        {
            get
            {
                return this.AlterationEntity.Name;
            }
        }

        [DataGridViewColumnVisibleAttribute]
        [XmlIgnore]
        public string AlterationoptionName
        {
            get
            {
                return this.AlterationoptionEntity.Name;
            }
        }

        [DataGridViewColumnVisibleAttribute]
        [XmlIgnore]
        public string AlterationoptionNameAndPriceIn
        {
            get
            {
                return this.AlterationoptionEntity.NameAndPriceIn;
            }
        }

        [XmlIgnore]
        public int? ParentBrandId
        {
            get { return this.AlterationEntity.BrandId; }
        }

        [XmlIgnore]
        public CommonEntityBase BrandParent
        {
            get { return this.AlterationEntity; }
        }

        [XmlIgnore]
        public BrandEntity BrandParentBrandEntity
        {
            get { return this.AlterationEntity.BrandEntity; }
        }

        // __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included Code

		#endregion
	}
}
