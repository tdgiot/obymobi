﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    using Obymobi.Enums;
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'Attachment'. This class is used for Business Logic or for framework extension code.</summary>
	[Serializable]
	public partial class AttachmentEntity : AttachmentEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
        , ICompanyRelatedChildEntityOrNullableCompanyRelatedChildEntity, IMediaContainingEntity, ICustomTextContainingEntity, ITimestampEntity
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		/// <summary>CTor</summary>
		public AttachmentEntity():base()
		{
		}

		/// <summary>CTor</summary>
		/// <param name="attachmentId">PK value for Attachment which data should be fetched into this Attachment object</param>
		public AttachmentEntity(System.Int32 attachmentId):
			base(attachmentId)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="attachmentId">PK value for Attachment which data should be fetched into this Attachment object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public AttachmentEntity(System.Int32 attachmentId, IPrefetchPath prefetchPathToUse):
			base(attachmentId, prefetchPathToUse)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="attachmentId">PK value for Attachment which data should be fetched into this Attachment object</param>
		/// <param name="validator">The custom validator object for this AttachmentEntity</param>
		public AttachmentEntity(System.Int32 attachmentId, IValidator validator):
			base(attachmentId, validator)
		{
		}
		
		/// <summary>CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected AttachmentEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}

		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode

        public CommonEntityBase Parent
        {
            get
            {
                if (this.ProductId.HasValue)
                    return this.ProductEntity;
                else if (this.PageId.HasValue)
                    return this.PageEntity;
                else
                    return null;
            }
        }

        [DataGridViewColumnVisible]
        public string TypeText
        {
            get
            {
                return this.Type.ToString();
            }
        }

        public bool HasDocuments()
        {
            bool toReturn = false;
            foreach (MediaEntity media in this.MediaCollection)
            {
                foreach (MediaRatioTypeMediaEntity mrtm in media.MediaRatioTypeMediaCollection)
                {
                    if (mrtm.MediaType != (int)MediaType.AttachmentThumbnail)
                    {
                        toReturn = true;
                        break;
                    }
                }
            }
            return toReturn;            
        }

		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included Code

		#endregion
	}
}
