﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'Supportagent'. This class is used for Business Logic or for framework extension code.</summary>
	[Serializable]
	public partial class SupportagentEntity : SupportagentEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
        , ISystemEntity
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		/// <summary>CTor</summary>
		public SupportagentEntity():base()
		{
		}

		/// <summary>CTor</summary>
		/// <param name="supportagentId">PK value for Supportagent which data should be fetched into this Supportagent object</param>
		public SupportagentEntity(System.Int32 supportagentId):
			base(supportagentId)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="supportagentId">PK value for Supportagent which data should be fetched into this Supportagent object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public SupportagentEntity(System.Int32 supportagentId, IPrefetchPath prefetchPathToUse):
			base(supportagentId, prefetchPathToUse)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="supportagentId">PK value for Supportagent which data should be fetched into this Supportagent object</param>
		/// <param name="validator">The custom validator object for this SupportagentEntity</param>
		public SupportagentEntity(System.Int32 supportagentId, IValidator validator):
			base(supportagentId, validator)
		{
		}
		
		/// <summary>CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected SupportagentEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}

		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode

        [DataGridViewColumnVisible]
        public string Name
        {
            get
            {
                string name = this.Firstname;

                if (!string.IsNullOrWhiteSpace(name))
                    name += " ";

                if (!string.IsNullOrWhiteSpace(this.LastnamePrefix))
                {
                    name += this.LastnamePrefix;

                    if (!string.IsNullOrWhiteSpace(name))
                        name += " ";
                }

                name += this.Lastname;

                return name;
            }
        }

        [DataGridViewColumnVisible]
        public string Supportpools
        {
            get
            {
                string supportpools = string.Empty;

                foreach (SupportpoolSupportagentEntity supportpoolSupportagentEntity in this.SupportpoolSupportagentCollection)
                {
                    if (!string.IsNullOrWhiteSpace(supportpools))
                        supportpools += ", ";

                    supportpools += supportpoolSupportagentEntity.SupportpoolEntity.Name;
                }

                return supportpools;
            }
        }

		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included Code

		#endregion
	}
}
