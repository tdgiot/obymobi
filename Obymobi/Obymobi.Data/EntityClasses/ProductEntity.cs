﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	using Dionysos;
	using Obymobi.Enums;
	using System.Linq;
    using Obymobi.Data.HelperClasses;
    using System.Xml.Serialization;
    using System.Collections.Generic;
    using Obymobi.Data.Comparers;
    using Obymobi.Interfaces;
    // __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'Product'. This class is used for Business Logic or for framework extension code.</summary>
	[Serializable]
	public partial class ProductEntity : ProductEntityBase
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
    , 
    INullableCompanyRelatedEntity, 
    IMediaContainingEntity, 
    ICustomTextContainingEntity, 
    IPriceContainingEntity, 
    INullableBrandRelatedEntity, 
    INullableBrandRelatedChildEntity,
    ITagEntity
	// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		/// <summary>CTor</summary>
		public ProductEntity():base()
		{
		}

		/// <summary>CTor</summary>
		/// <param name="productId">PK value for Product which data should be fetched into this Product object</param>
		public ProductEntity(System.Int32 productId):
			base(productId)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="productId">PK value for Product which data should be fetched into this Product object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public ProductEntity(System.Int32 productId, IPrefetchPath prefetchPathToUse):
			base(productId, prefetchPathToUse)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="productId">PK value for Product which data should be fetched into this Product object</param>
		/// <param name="validator">The custom validator object for this ProductEntity</param>
		public ProductEntity(System.Int32 productId, IValidator validator):
			base(productId, validator)
		{
		}
		
		/// <summary>CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected ProductEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}

		#region Custom Entity code
		
        // __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode

        private bool? isLinkedToMultipleMenus = null;

        public IEntityCollection TagCollection => this.ProductTagCollection;

        #region Properties

        /// <summary>
		/// Determine if a least one Media entity contains a MediaRatioEntity related to MediaType ProductBranding
		/// </summary>
        [XmlIgnore]
        public bool HasProductBranding
		{
			get
			{
				bool toReturn = false;
				if (this.GenericproductId.HasValue && this.GenericproductEntity.HasProductBranding)
				{
					toReturn = true;
				}
				else
				{
					foreach (var media in this.MediaCollection)
					{
                        if (media.MediaRatioTypeMediaCollection.Any(mr => (mr.MediaType.HasValue && mr.MediaType.Value == (int)MediaType.ProductBranding)) ||
                            media.MediaRatioTypeMediaCollection.Any(mr => (mr.MediaType.HasValue && mr.MediaType.Value == (int)MediaType.GenericproductBranding)))
						{
							toReturn = true;
							break;
						}
					}
				}
				return toReturn;
			}
		}

        /// <summary>
        /// Determine if a least one Media entity contains a MediaRatioEntity related to MediaType ProductBranding
        /// </summary>
        [XmlIgnore]
        public bool HasProductBrandingCrave
        {
            get
            {
                bool toReturn = false;
                if (this.GenericproductId.HasValue && this.GenericproductEntity.HasProductBrandingCrave)
                {
                    toReturn = true;
                }
                else
                {
                    foreach (var media in this.MediaCollection)
                    {
                        if (media.MediaRatioTypeMediaCollection.Any(mr => (mr.MediaType.HasValue && mr.MediaType.Value == (int)MediaType.ProductBranding800x480)) ||
                            media.MediaRatioTypeMediaCollection.Any(mr => (mr.MediaType.HasValue && mr.MediaType.Value == (int)MediaType.GenericProductBranding800x480)))
                        {
                            toReturn = true;
                            break;
                        }
                    }
                }
                return toReturn;
            }
        }

        /// <summary>
        /// Determine if a least one Media entity contains a MediaRatioEntity related to MediaType ProductBranding
        /// </summary>
        [XmlIgnore]
        public bool HasProductBranding1280x800
        {
            get
            {
                bool toReturn = false;
                if (this.GenericproductId.HasValue && this.GenericproductEntity.HasProductBranding1280x800)
                {
                    toReturn = true;
                }
                else
                {
                    foreach (var media in this.MediaCollection)
                    {
                        if (media.MediaRatioTypeMediaCollection.Any(mr => (mr.MediaType.HasValue && mr.MediaType.Value == (int)MediaType.ProductBranding1280x800)) ||
                            media.MediaRatioTypeMediaCollection.Any(mr => (mr.MediaType.HasValue && mr.MediaType.Value == (int)MediaType.GenericProductBranding1280x800)))
                        {
                            toReturn = true;
                            break;
                        }
                    }
                }
                return toReturn;
            }
        }

		/// <summary>
		/// Determine if a least one Media entity contains a MediaRatioEntity related to MediaType ProductButton
		/// </summary>
        [XmlIgnore]
        public bool HasProductButton
		{
			get
			{
				bool toReturn = false;
				if (this.GenericproductId.HasValue && this.GenericproductEntity.HasProductButton)
				{
					toReturn = true;
				}
				else
				{
					foreach (var media in this.MediaCollection)
					{
                        if (media.MediaRatioTypeMediaCollection.Any(mr => (mr.MediaType.HasValue && mr.MediaType.Value == (int)MediaType.ProductButton)) ||
                            media.MediaRatioTypeMediaCollection.Any(mr => (mr.MediaType.HasValue && mr.MediaType.Value == (int)MediaType.GenericproductButton)))
						{
							toReturn = true;
							break;
						}
					}
				}
				return toReturn;
			}
		}

        /// <summary>
        /// Determine if a least one Media entity contains a MediaRatioEntity related to MediaType ProductButton
        /// </summary>
        [XmlIgnore]
        public bool HasProductButtonCrave
        {
            get
            {
                bool toReturn = false;
                if (this.GenericproductId.HasValue && this.GenericproductEntity.HasProductButtonCrave)
                {
                    toReturn = true;
                }
                else
                {
                    foreach (var media in this.MediaCollection)
                    {
                        if (media.MediaRatioTypeMediaCollection.Any(mr => (mr.MediaType.HasValue && mr.MediaType.Value == (int)MediaType.ProductButton800x480)) ||
                            media.MediaRatioTypeMediaCollection.Any(mr => (mr.MediaType.HasValue && mr.MediaType.Value == (int)MediaType.GenericProductButton800x480)))
                        {
                            toReturn = true;
                            break;
                        }
                    }
                }
                return toReturn;
            }
        }

        /// <summary>
        /// Determine if a least one Media entity contains a MediaRatioEntity related to MediaType ProductButton
        /// </summary>
        [XmlIgnore]
        public bool HasProductButton1280x800
        {
            get
            {
                bool toReturn = false;
                if (this.GenericproductId.HasValue && this.GenericproductEntity.HasProductButton1280x800)
                {
                    toReturn = true;
                }
                else
                {
                    foreach (var media in this.MediaCollection)
                    {
                        if (media.MediaRatioTypeMediaCollection.Any(mr => (mr.MediaType.HasValue && mr.MediaType.Value == (int)MediaType.ProductButton1280x800)) ||
                            media.MediaRatioTypeMediaCollection.Any(mr => (mr.MediaType.HasValue && mr.MediaType.Value == (int)MediaType.GenericProductButton1280x800)))
                        {
                            toReturn = true;
                            break;
                        }
                    }
                }
                return toReturn;
            }
        }

        /// <summary>
        /// Determine if this product has a schedule set
        /// </summary>
        [XmlIgnore]
        public bool HasSchedule
        {
            get
            {
                return (this.OrderHourCollection.Count > 0);
            }
        }

		/// <summary>
		/// Determine if a least one Media entity contains a MediaRatioEntity related to MediaType ProductButton
		/// </summary>
        [XmlIgnore]
        public bool HasProductPhoto
		{
			get
			{
				bool toReturn = false;

				foreach (var media in this.MediaCollection)
				{
                    if (media.MediaRatioTypeMediaCollection.Any(mr => (mr.MediaType.HasValue && mr.MediaType.Value == (int)MediaType.ProductPhoto)))
					{
						toReturn = true;
						break;
					}
				}

				return toReturn;
			}
		}

        /// <summary>
        /// Determine if the product contains alterations
        /// </summary>
        [XmlIgnore]
        public bool HasAlterations
        {
            get
            { 
                return (this.ProductAlterationCollection.Count > 0);
            }
        }

        /// <summary>
        /// Determine if the product contains alterations
        /// </summary>
        [XmlIgnore]
        public bool HasSuggestions
        {
            get
            {
                return (this.ProductSuggestionCollection.Count > 0);
            }
        }

		/// <summary>
		/// Determine if there is a Description
		/// </summary>
        [XmlIgnore]
        public bool HasDescription
		{
			get
			{
				bool toReturn = false;

				if (this.GenericproductId.HasValue && this.GenericproductEntity.HasDescription)
				{
					toReturn = true;
				}
				else
					toReturn = !this.Description.IsNullOrWhiteSpace();

				return toReturn;
			}
		}

		/// <summary>
		/// Determine if there is a TextColor
		/// </summary>
        [XmlIgnore]
        public bool HasTextColor
		{
			get
			{
				bool toReturn = false;

				if (this.GenericproductId.HasValue && this.GenericproductEntity.HasTextColor)
				{
					toReturn = true;
				}
				else
					toReturn = !this.TextColor.IsNullOrWhiteSpace();

				return toReturn;
			}
		}

		/// <summary>
		/// Determine if there is a BackgroundColor
		/// </summary>
        [XmlIgnore]
        public bool HasBackgroundColor
		{
			get
			{
				bool toReturn = false;

				if (this.GenericproductId.HasValue && this.GenericproductEntity.HasBackgroundColor)
				{
					toReturn = true;
				}
				else
					toReturn = !this.BackgroundColor.IsNullOrWhiteSpace();

				return toReturn;
			}
		}

        /// <summary>
        /// Gets a flag indicating whether this product is in a visible category
        /// </summary>
        /// <value>
        /// 	<c>true</c> if this instance has category; otherwise, <c>false</c>.
        /// </value>
        [XmlIgnore]
        public bool HasCategory
        {
            get
            {
                bool toReturn = false;

                if (!this.IsNew)
                {
                    PredicateExpression filter = new PredicateExpression();
                    filter.Add(ProductCategoryFields.ProductId == this.ProductId);
                    filter.Add(CategoryFields.ParentCategoryId != DBNull.Value);

                    RelationCollection relations = new RelationCollection();
                    relations.Add(ProductCategoryEntity.Relations.CategoryEntityUsingCategoryId);

                    ProductCategoryCollection productCategories = new ProductCategoryCollection();
                    if (productCategories.GetDbCount(filter, relations) > 0)
                        toReturn = true;
                }

                return toReturn;
            }
        }

        [XmlIgnore]
        public bool IsLinkedToMultipleMenus
        {
            get
            {
                if (IsNew)
                {
                    return false;
                }

                if (!isLinkedToMultipleMenus.HasValue)
                {
                    RelationCollection relations = new RelationCollection();
                    relations.Add(CategoryEntity.Relations.ProductCategoryEntityUsingCategoryId);

                    PredicateExpression filter = new PredicateExpression();
                    filter.Add(ProductCategoryFields.ProductId == this.ProductId);
                    
                    var categories = new CategoryCollection();
                    int numberOfMenus = (int)categories.GetScalar(CategoryFieldIndex.MenuId, null, AggregateFunction.CountDistinct, filter, relations, null);

                    isLinkedToMultipleMenus = numberOfMenus > 1;
                }

                return isLinkedToMultipleMenus.GetValueOrDefault();
            }
        }

        [XmlIgnore]
        public bool IsComplete
        {
            get
            {
                bool isComplete = false;

                if (this.CompanyEntity.SystemType == SystemType.Crave)
                    isComplete = (this.HasDescription && this.HasProductBrandingCrave && this.HasProductButtonCrave);
                else
                    isComplete = (this.HasDescription && this.HasProductBranding && this.HasProductButton && this.HasTextColor && this.HasBackgroundColor);

                return isComplete;
            }
        }

		/// <summary>
		/// Gets the price excluding Vat for this product
		/// </summary>
        [XmlIgnore]
        public decimal PriceEx
		{
			get
			{
				return (this.PriceIn.Value / ((decimal)this.VattariffPercentage + 100)) * 100;
			}
		}                

        [XmlIgnore]
        public ProductSubType SubTypeAsEnum
        {
            get
            {
                return EnumUtil.ToEnum<ProductSubType>(this.SubType);
            }
            set
            {
                this.SubType = (int)value;
            }
        }

	    public bool ValidatorCreateOrUpdateDefaultCustomText { get; set; }

	    public bool NameChanged { get; set; }

        public bool ValidatorPreventBrandProductUpdate { get; set; }

        [XmlIgnore]
	    public String NameWithBrand
	    {
	        get
	        {
	            if (this.BrandId.HasValue)
	            {
	                return string.Format("{0} ({1})", this.Name, this.BrandEntity.Name);
	            }

	            return this.Name;
	        }
	    }

        [XmlIgnore]
        public int? ParentBrandId
        {
            get
            {
                if (this.BrandProductId.HasValue)
                {
                    return this.BrandProductEntity.BrandId;
                }
                return null;
            }
        }

        [XmlIgnore]
        public BrandEntity BrandParentBrandEntity
        {
            get
            {
                if (this.BrandProductId.HasValue)
                {
                    return this.BrandProductEntity.BrandEntity;
                }
                return null;
            }
        }

	    public CommonEntityBase BrandParent
	    {
	        get
	        {
                if (this.BrandProductId.HasValue)
                {
                    return this.BrandProductEntity;
                }
                return null;
	        }
	    }

        [XmlIgnore]
        public bool IsBrandProduct => this.BrandId.HasValue;

        public ICollection<ITag> GetTags()
        {
            return this.ProductTagCollection.Select(x => x.TagEntity).ToArray();
        }

        public ICollection<ITag> GetInheritedTags()
        {
            HashSet<TagEntity> inheritedTags = new HashSet<TagEntity>(new TagIdEqualityComparer());

            foreach (ProductCategoryEntity productCategoryEntity in ProductCategoryCollection)
            {
                foreach (ProductCategoryTagEntity productCategoryTagEntity in productCategoryEntity.ProductCategoryTagCollection)
                {
                    inheritedTags.Add(productCategoryTagEntity.TagEntity);
                }
            }

            return inheritedTags.ToArray();
        }

        #endregion

        // __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included Code

		#endregion
	}
}
