﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    using System.Xml.Serialization;
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'UIScheduleItemOccurrence'. This class is used for Business Logic or for framework extension code.</summary>
	[Serializable]
	public partial class UIScheduleItemOccurrenceEntity : UIScheduleItemOccurrenceEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
        , ICompanyRelatedChildEntityOrNullableCompanyRelatedChildEntity, ITimestampEntity
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		/// <summary>CTor</summary>
		public UIScheduleItemOccurrenceEntity():base()
		{
		}

		/// <summary>CTor</summary>
		/// <param name="uIScheduleItemOccurrenceId">PK value for UIScheduleItemOccurrence which data should be fetched into this UIScheduleItemOccurrence object</param>
		public UIScheduleItemOccurrenceEntity(System.Int32 uIScheduleItemOccurrenceId):
			base(uIScheduleItemOccurrenceId)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="uIScheduleItemOccurrenceId">PK value for UIScheduleItemOccurrence which data should be fetched into this UIScheduleItemOccurrence object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public UIScheduleItemOccurrenceEntity(System.Int32 uIScheduleItemOccurrenceId, IPrefetchPath prefetchPathToUse):
			base(uIScheduleItemOccurrenceId, prefetchPathToUse)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="uIScheduleItemOccurrenceId">PK value for UIScheduleItemOccurrence which data should be fetched into this UIScheduleItemOccurrence object</param>
		/// <param name="validator">The custom validator object for this UIScheduleItemOccurrenceEntity</param>
		public UIScheduleItemOccurrenceEntity(System.Int32 uIScheduleItemOccurrenceId, IValidator validator):
			base(uIScheduleItemOccurrenceId, validator)
		{
		}
		
		/// <summary>CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected UIScheduleItemOccurrenceEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}

		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode

        [XmlIgnore]
        public CommonEntityBase Parent
        {
            get 
            {
                if (this.UIScheduleId.HasValue)
                {
                    return this.UIScheduleEntity;
				}
                return this.UIScheduleItemEntity;
            }
        }

		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included Code

		#endregion
	}
}
