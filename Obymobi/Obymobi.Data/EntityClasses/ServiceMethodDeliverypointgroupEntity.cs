﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'ServiceMethodDeliverypointgroup'. This class is used for Business Logic or for framework extension code.</summary>
	[Serializable]
	public partial class ServiceMethodDeliverypointgroupEntity : ServiceMethodDeliverypointgroupEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		, ICompanyRelatedChildEntity
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		/// <summary>CTor</summary>
		public ServiceMethodDeliverypointgroupEntity():base()
		{
		}

		/// <summary>CTor</summary>
		/// <param name="serviceMethodId">PK value for ServiceMethodDeliverypointgroup which data should be fetched into this ServiceMethodDeliverypointgroup object</param>
		/// <param name="deliverypointgroupId">PK value for ServiceMethodDeliverypointgroup which data should be fetched into this ServiceMethodDeliverypointgroup object</param>
		public ServiceMethodDeliverypointgroupEntity(System.Int32 serviceMethodId, System.Int32 deliverypointgroupId):
			base(serviceMethodId, deliverypointgroupId)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="serviceMethodId">PK value for ServiceMethodDeliverypointgroup which data should be fetched into this ServiceMethodDeliverypointgroup object</param>
		/// <param name="deliverypointgroupId">PK value for ServiceMethodDeliverypointgroup which data should be fetched into this ServiceMethodDeliverypointgroup object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public ServiceMethodDeliverypointgroupEntity(System.Int32 serviceMethodId, System.Int32 deliverypointgroupId, IPrefetchPath prefetchPathToUse):
			base(serviceMethodId, deliverypointgroupId, prefetchPathToUse)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="serviceMethodId">PK value for ServiceMethodDeliverypointgroup which data should be fetched into this ServiceMethodDeliverypointgroup object</param>
		/// <param name="deliverypointgroupId">PK value for ServiceMethodDeliverypointgroup which data should be fetched into this ServiceMethodDeliverypointgroup object</param>
		/// <param name="validator">The custom validator object for this ServiceMethodDeliverypointgroupEntity</param>
		public ServiceMethodDeliverypointgroupEntity(System.Int32 serviceMethodId, System.Int32 deliverypointgroupId, IValidator validator):
			base(serviceMethodId, deliverypointgroupId, validator)
		{
		}
		
		/// <summary>CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected ServiceMethodDeliverypointgroupEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}

		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
        public CommonEntityBase Parent => this.ServiceMethodEntity;
        // __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included Code

		#endregion
	}
}
