﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	using System.Linq;
    using System.Xml.Serialization;
    using Dionysos;
    using Dionysos.Data;
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'Receipt'. This class is used for Business Logic or for framework extension code.</summary>
	[Serializable]
	public partial class ReceiptEntity : ReceiptEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
        , ICompanyRelatedEntity
	    // __LLBLGENPRO_USER_CODE_REGION_END	
	{
		/// <summary>CTor</summary>
		public ReceiptEntity():base()
		{
		}

		/// <summary>CTor</summary>
		/// <param name="receiptId">PK value for Receipt which data should be fetched into this Receipt object</param>
		public ReceiptEntity(System.Int32 receiptId):
			base(receiptId)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="receiptId">PK value for Receipt which data should be fetched into this Receipt object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public ReceiptEntity(System.Int32 receiptId, IPrefetchPath prefetchPathToUse):
			base(receiptId, prefetchPathToUse)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="receiptId">PK value for Receipt which data should be fetched into this Receipt object</param>
		/// <param name="validator">The custom validator object for this ReceiptEntity</param>
		public ReceiptEntity(System.Int32 receiptId, IValidator validator):
			base(receiptId, validator)
		{
		}
		
		/// <summary>CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected ReceiptEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}

		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode

		[DataGridViewColumnVisible]
        [XmlIgnore]
        public string TotalFormatted => this.OrderEntity.TotalFormatted;

		[DataGridViewColumnVisible]
        [XmlIgnore]
        public string CustomerEmail => this.OrderEntity.Email;

        [DataGridViewColumnVisible]
        [XmlIgnore]
        public string CustomerLastname => this.OrderEntity.CustomerLastname;

        [DataGridViewColumnVisible]
        [XmlIgnore]
        public string CustomerPhonenumber => this.OrderEntity.CustomerPhonenumber;

        [DataGridViewColumnVisible]
        [XmlIgnore]
        public string OrderIdAsString => this.OrderEntity.OrderId.ToString();

        [DataGridViewColumnVisible]
		[XmlIgnore]
        public DateTime? CreatedInCompanyTimeZone => this.CreatedUTC.UtcToLocalTime(this.OrderEntity.TimeZoneInfo);

		[DataGridViewColumnVisible]
		[XmlIgnore]
		public string PaymentTransactionReferenceId => this.OrderEntity.PaymentTransactionCollection.FirstOrDefault()?.ReferenceId ?? string.Empty;

        [DataGridViewColumnVisible]
        [XmlIgnore]
        public string PaymentProviderName => this.OrderEntity.PaymentTransactionCollection.FirstOrDefault()?.PaymentProviderName ?? string.Empty;

		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included Code

		#endregion
	}
}
