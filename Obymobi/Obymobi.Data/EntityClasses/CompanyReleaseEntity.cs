﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'CompanyRelease'. This class is used for Business Logic or for framework extension code.</summary>
	[Serializable]
	public partial class CompanyReleaseEntity : CompanyReleaseEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
        , ICompanyRelatedEntity, ITimestampEntity
        // __LLBLGENPRO_USER_CODE_REGION_END	
	{
		/// <summary>CTor</summary>
		public CompanyReleaseEntity():base()
		{
		}

		/// <summary>CTor</summary>
		/// <param name="companyReleaseId">PK value for CompanyRelease which data should be fetched into this CompanyRelease object</param>
		public CompanyReleaseEntity(System.Int32 companyReleaseId):
			base(companyReleaseId)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="companyReleaseId">PK value for CompanyRelease which data should be fetched into this CompanyRelease object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public CompanyReleaseEntity(System.Int32 companyReleaseId, IPrefetchPath prefetchPathToUse):
			base(companyReleaseId, prefetchPathToUse)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="companyReleaseId">PK value for CompanyRelease which data should be fetched into this CompanyRelease object</param>
		/// <param name="validator">The custom validator object for this CompanyReleaseEntity</param>
		public CompanyReleaseEntity(System.Int32 companyReleaseId, IValidator validator):
			base(companyReleaseId, validator)
		{
		}
		
		/// <summary>CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected CompanyReleaseEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}

		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode

		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included Code

		#endregion
	}
}
