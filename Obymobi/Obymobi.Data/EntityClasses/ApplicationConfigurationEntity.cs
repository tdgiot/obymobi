﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    // __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'ApplicationConfiguration'. This class is used for Business Logic or for framework extension code.</summary>
	[Serializable]
	public partial class ApplicationConfigurationEntity : ApplicationConfigurationEntityBase
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
    , ICompanyRelatedEntity, ICustomTextContainingEntity
    // __LLBLGENPRO_USER_CODE_REGION_END	
	{
		/// <summary>CTor</summary>
		public ApplicationConfigurationEntity():base()
		{
		}

		/// <summary>CTor</summary>
		/// <param name="applicationConfigurationId">PK value for ApplicationConfiguration which data should be fetched into this ApplicationConfiguration object</param>
		public ApplicationConfigurationEntity(System.Int32 applicationConfigurationId):
			base(applicationConfigurationId)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="applicationConfigurationId">PK value for ApplicationConfiguration which data should be fetched into this ApplicationConfiguration object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public ApplicationConfigurationEntity(System.Int32 applicationConfigurationId, IPrefetchPath prefetchPathToUse):
			base(applicationConfigurationId, prefetchPathToUse)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="applicationConfigurationId">PK value for ApplicationConfiguration which data should be fetched into this ApplicationConfiguration object</param>
		/// <param name="validator">The custom validator object for this ApplicationConfigurationEntity</param>
		public ApplicationConfigurationEntity(System.Int32 applicationConfigurationId, IValidator validator):
			base(applicationConfigurationId, validator)
		{
		}
		
		/// <summary>CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected ApplicationConfigurationEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
            // __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
            // __LLBLGENPRO_USER_CODE_REGION_END
		}

		#region Custom Entity code
		
        // __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode

        // __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included Code

		#endregion
	}
}
