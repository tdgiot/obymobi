﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'View'. This class is used for Business Logic or for framework extension code.</summary>
	[Serializable]
	public partial class ViewEntity : ViewEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
        , Dionysos.Interfaces.Data.IView, ISystemEntity
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		/// <summary>CTor</summary>
		public ViewEntity():base()
		{
		}

		/// <summary>CTor</summary>
		/// <param name="viewId">PK value for View which data should be fetched into this View object</param>
		public ViewEntity(System.Int32 viewId):
			base(viewId)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="viewId">PK value for View which data should be fetched into this View object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public ViewEntity(System.Int32 viewId, IPrefetchPath prefetchPathToUse):
			base(viewId, prefetchPathToUse)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="viewId">PK value for View which data should be fetched into this View object</param>
		/// <param name="validator">The custom validator object for this ViewEntity</param>
		public ViewEntity(System.Int32 viewId, IValidator validator):
			base(viewId, validator)
		{
		}
		
		/// <summary>CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected ViewEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}

		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode

        #region Methods

        /// <summary>
        /// Adds a ViewItem to this View instance
        /// </summary>
        /// <param name="fieldName">The name of the view of the view item</param>
        /// <param name="friendlyName">The friendly name of the view item</param>
        /// <param name="type">The type string of the view item</param>
        /// <param name="uiElementTypeNameFull">The full type name of the UI element</param>
        /// <returns>True if adding the view item was succesful, False if not</returns>
        public bool AddViewItem(string fieldName, string type, string displayFormatString, string uiElementTypeNameFull)
        {
            ViewItemEntity viewItem = new ViewItemEntity();
            viewItem.ViewId = this.ViewId;
            viewItem.EntityName = this.EntityName;
            viewItem.FieldName = fieldName;
            viewItem.ShowOnGridView = true;
            viewItem.DisplayPosition = 10000;
            viewItem.DisplayFormatString = displayFormatString;
            viewItem.Type = type;
            viewItem.UiElementTypeNameFull = uiElementTypeNameFull;

            return viewItem.Save();
        }

        /// <summary>
        /// Removes a view item from the view
        /// </summary>
        /// <param name="fieldName">The name of the field from the view item to remove</param>
        /// <param name="uiElementTypeNameFull">The full type name of the UI element</param>
        /// <returns>True if the removal was succesful, false if not</returns>
        public bool RemoveViewItem(string fieldName)
        {
            bool success = false;

            for (int i = 0; i < this.ViewItemCollection.Count; i++)
            {
                ViewItemEntity viewItem = this.ViewItemCollection[i];
                if (!Instance.Empty(viewItem) && viewItem.FieldName == fieldName)
                {
                    success = viewItem.Delete();
                    this.ViewItemCollection.Remove(viewItem);
                    break;
                }
            }

            return success;
        }

        /// <summary>
        /// Check whether a ViewItem for the specified fieldname already exists
        /// </summary>
        /// <param name="fieldName">The name of the field</param>
        /// <param name="uiElementTypeNameFull">The full type name of the UI element</param>
        /// <returns>True if the ViewItem already exists, False if not</returns>
        public bool ContainsViewItem(string fieldName)
        {
            bool exists = false;

            for (int i = 0; i < this.ViewItemCollection.Count; i++)
            {
                ViewItemEntity viewItem = this.ViewItemCollection[i];
                if (!Instance.Empty(viewItem) && viewItem.FieldName == fieldName)
                {
                    exists = true;
                    break;
                }
            }

            return exists;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets the view items
        /// </summary>
        public IEntityCollection ViewItems
        {
            get
            {
                return this.ViewItemCollection;
            }
        }

        public bool IsCustomView => false;

        #endregion

        // __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included Code

		#endregion
	}
}
