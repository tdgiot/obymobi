﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    using System.Xml.Serialization;
    // __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'Routestep'. This class is used for Business Logic or for framework extension code.</summary>
	[Serializable]
	public partial class RoutestepEntity : RoutestepEntityBase
        // __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
        , ICompanyRelatedChildEntity, ITimestampEntity
    // __LLBLGENPRO_USER_CODE_REGION_END	
	{
		/// <summary>CTor</summary>
		public RoutestepEntity():base()
		{
		}

		/// <summary>CTor</summary>
		/// <param name="routestepId">PK value for Routestep which data should be fetched into this Routestep object</param>
		public RoutestepEntity(System.Int32 routestepId):
			base(routestepId)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="routestepId">PK value for Routestep which data should be fetched into this Routestep object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public RoutestepEntity(System.Int32 routestepId, IPrefetchPath prefetchPathToUse):
			base(routestepId, prefetchPathToUse)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="routestepId">PK value for Routestep which data should be fetched into this Routestep object</param>
		/// <param name="validator">The custom validator object for this RoutestepEntity</param>
		public RoutestepEntity(System.Int32 routestepId, IValidator validator):
			base(routestepId, validator)
		{
		}
		
		/// <summary>CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected RoutestepEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
            // __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
            // __LLBLGENPRO_USER_CODE_REGION_END
		}

		#region Custom Entity code
		
        // __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode

        [XmlIgnore]
        public CommonEntityBase Parent
        {
            get { return this.RouteEntity; }
        }

        [XmlIgnore]
        public bool ValidatorOverrideSequenceChecking = false;

        [DataGridViewColumnVisibleAttribute]
        [XmlIgnore]
        public string RouteNameAndNumber
        {
            get
            {
                return this.RouteEntity.Name + " - " + this.Number.ToString().PadLeft(2, '0');
            }
        }

        [DataGridViewColumnVisibleAttribute]
        [XmlIgnore]
        public string StepNumberAndRouteName
        {
            get
            {
                return this.Number.ToString().PadLeft(2, '0') + " - " + this.RouteEntity.Name;
            }
        }

        [DataGridViewColumnVisibleAttribute]
        [XmlIgnore]
        public string Routestephandlers
        {
            get{
                string toReturn = "";

                int size = this.RoutestephandlerCollection.Count;
                int currentNumber = 0;
                foreach (RoutestephandlerEntity handler in this.RoutestephandlerCollection)
                {
                    toReturn += handler.RoutestephandlerTypeAsText;

                    currentNumber++;
                    if (size != currentNumber)
                        toReturn += ", ";
                }

                return toReturn;
            }
        }

        // __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included Code

		#endregion
	}
}
