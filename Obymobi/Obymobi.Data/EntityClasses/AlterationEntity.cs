﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    using System.Xml.Serialization;
    using Obymobi.Logic.Cms;
    using Obymobi.Enums;
    using Dionysos;
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'Alteration'. This class is used for Business Logic or for framework extension code.</summary>
	[Serializable]
	public partial class AlterationEntity : AlterationEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
        , INullableCompanyRelatedEntity, IMediaContainingEntity, IAlterationComponent, ICustomTextContainingEntity, IPriceContainingEntity, INullableBrandRelatedEntity, ITimestampEntity
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		/// <summary>CTor</summary>
		public AlterationEntity():base()
		{
		}

		/// <summary>CTor</summary>
		/// <param name="alterationId">PK value for Alteration which data should be fetched into this Alteration object</param>
		public AlterationEntity(System.Int32 alterationId):
			base(alterationId)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="alterationId">PK value for Alteration which data should be fetched into this Alteration object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public AlterationEntity(System.Int32 alterationId, IPrefetchPath prefetchPathToUse):
			base(alterationId, prefetchPathToUse)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="alterationId">PK value for Alteration which data should be fetched into this Alteration object</param>
		/// <param name="validator">The custom validator object for this AlterationEntity</param>
		public AlterationEntity(System.Int32 alterationId, IValidator validator):
			base(alterationId, validator)
		{
		}
		
		/// <summary>CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected AlterationEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}

		#region Custom Entity code
		
        // __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode

        #region Properties

        /// <summary>
        /// Gets a flag which indicates whether the alteration has options
        /// </summary>
        [XmlIgnore]
        public bool HasOptions
        {
            get
            {
                return (this.AlterationitemCollection.Count > 0);
            }
        }

        #region CMS Treelist Logic

        [XmlIgnore]
        public string ItemId
        {
            get
            {
                return string.Format("{0}-{1}", this.ItemType, this.AlterationId);
            }
        }

        [XmlIgnore]
        public string ParentItemId
        {
            get
            {
                return this.ParentAlterationId.HasValue ? this.ParentAlterationEntity.ItemId : string.Empty;
            }
        }

        [XmlIgnore]
        public string ItemType
        {
            get
            {
                return AlterationComponentType.Alteration;
            }
        }	    

	    [DataGridViewColumnVisible]
        [XmlIgnore]
        public string TypeName
        {
            get
            {                
                return this.Type.ToString();
            }
        }

        [DataGridViewColumnVisible]
        [XmlIgnore]
        public string FriendlyNameOrName
        {
            get
            {
                return !this.FriendlyName.IsNullOrWhiteSpace() ? this.FriendlyName : this.Name;
            }
        }

	    [DataGridViewColumnVisible]
	    [XmlIgnore]
	    public string ProductNames
	    {
	        get
            {
                string toReturn = "";
                foreach (ProductAlterationEntity productAlteration in this.ProductAlterationCollection)
                {
                    if (!toReturn.IsNullOrWhiteSpace())
                    {
                        toReturn += ", ";
                    }
                    toReturn += productAlteration.ProductEntity.Name;
                }
                return toReturn;
            }
	    }

	    #endregion

        public bool ValidatorCreateOrUpdateDefaultCustomText { get; set; }

	    public bool NameChanged { get; set; }

	    #endregion

        public AlterationEntity TraverseToRootAlterationEntity
        {
            get
            {
                if (this.ParentAlterationId.HasValue)
                {
                    return this.ParentAlterationEntity.TraverseToRootAlterationEntity;
                }

                return this;
            }
        }

	    // __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included Code

		#endregion
	}
}
