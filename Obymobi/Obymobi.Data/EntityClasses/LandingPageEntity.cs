﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Dionysos.Data;
using System.Diagnostics;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'LandingPage'. This class is used for Business Logic or for framework extension code.</summary>
	[Serializable]
	public partial class LandingPageEntity : LandingPageEntityBase
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
    , ICompanyRelatedChildEntity, ICustomTextContainingEntity
	// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		/// <summary>CTor</summary>
		public LandingPageEntity():base()
		{
		}

		/// <summary>CTor</summary>
		/// <param name="landingPageId">PK value for LandingPage which data should be fetched into this LandingPage object</param>
		public LandingPageEntity(System.Int32 landingPageId):
			base(landingPageId)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="landingPageId">PK value for LandingPage which data should be fetched into this LandingPage object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public LandingPageEntity(System.Int32 landingPageId, IPrefetchPath prefetchPathToUse):
			base(landingPageId, prefetchPathToUse)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="landingPageId">PK value for LandingPage which data should be fetched into this LandingPage object</param>
		/// <param name="validator">The custom validator object for this LandingPageEntity</param>
		public LandingPageEntity(System.Int32 landingPageId, IValidator validator):
			base(landingPageId, validator)
		{
		}
		
		/// <summary>CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected LandingPageEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}

		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		public CommonEntityBase Parent => this.ApplicationConfigurationEntity;
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included Code

		#endregion
	}
}
