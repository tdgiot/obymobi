﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Dionysos.Data;
using System.Diagnostics;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    using System.Xml.Serialization;
    using Dionysos.Data.LLBLGen;
    using Dionysos;
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'CustomText'. This class is used for Business Logic or for framework extension code.</summary>
	[Serializable]
	public partial class CustomTextEntity : CustomTextEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
        , ICompanyRelatedChildEntityOrNullableCompanyRelatedChildEntity
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		/// <summary>CTor</summary>
		public CustomTextEntity():base()
		{
		}

		/// <summary>CTor</summary>
		/// <param name="customTextId">PK value for CustomText which data should be fetched into this CustomText object</param>
		public CustomTextEntity(System.Int32 customTextId):
			base(customTextId)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="customTextId">PK value for CustomText which data should be fetched into this CustomText object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public CustomTextEntity(System.Int32 customTextId, IPrefetchPath prefetchPathToUse):
			base(customTextId, prefetchPathToUse)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="customTextId">PK value for CustomText which data should be fetched into this CustomText object</param>
		/// <param name="validator">The custom validator object for this CustomTextEntity</param>
		public CustomTextEntity(System.Int32 customTextId, IValidator validator):
			base(customTextId, validator)
		{
		}
		
		/// <summary>CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected CustomTextEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}

		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode        

        /// <summary>
        /// Gets the EntityName this CustomTextEntity is related to (eg. Product, Company, etc.).
        /// </summary>
        [XmlIgnore]
        public string RelatedEntityName
        {
            get
            {
                // Loop through fields and find which Related Id is filled
                string toReturn = null;
                for (int i = 0; i < (int)CustomTextFieldIndex.AmountOfFields; i++)
                {
                    if (this.Fields[i].Name.EndsWith("Id") && this.Fields[i].Name != "CustomTextId" && this.Fields[i].Name != "LanguageId" && this.Fields[i].Name != "ParentCompanyId")
                    {
                        // We have an Id field which is not the CustomTextId or LanguageId
                        if (this.Fields[i].CurrentValue != null && this.Fields[i].CurrentValue != DBNull.Value)
                        {
                            // We have a value in the ID field
                            toReturn = this.Fields[i].Name.Replace("Id", String.Empty);
                            break;
                        }
                    }
                }

                return toReturn;
            }
        }

        /// <summary>
        /// Gets the foreign key this CustomTextEntity is related to (eg. ProductId, CompanyId, etc.).
        /// </summary>
        [XmlIgnore]
        public int RelatedEntityId
        {
            get
            {
                // Loop through fields and find which Related Id is filled
                int toReturn = 0;
                for (int i = 0; i < (int)CustomTextFieldIndex.AmountOfFields; i++)
                {
                    if (this.Fields[i].Name.EndsWith("Id") && this.Fields[i].Name != "CustomTextId" && this.Fields[i].Name != "LanguageId" && this.Fields[i].Name != "ParentCompanyId")
                    {
                        // We have an Id field which is not the CustomTextId or LanguageId
                        if (this.Fields[i].CurrentValue != null && this.Fields[i].CurrentValue != DBNull.Value && int.TryParse(this.Fields[i].CurrentValue.ToString(), out toReturn))
                        {
                            // We have a value in the ID field                            
                            break;
                        }
                    }
                }
                return toReturn;
            }
        }

        /// <summary>
        /// Gets the IEntity this CustomTextEntity is related to.
        /// </summary>
        [XmlIgnore]
        public IEntity RelatedEntity
        {
            get
            {
                IEntity toReturn = null;

                string relatedEntityName = this.RelatedEntityName;
                if (!relatedEntityName.IsNullOrWhiteSpace())
                {
                    if (this.Transaction != null)
                    {
                        toReturn = LLBLGenEntityUtil.GetEntityUsingFieldCompareValuePredicateExpression(relatedEntityName,
                            this.Fields[relatedEntityName + "Id"].Name,
                            ComparisonOperator.Equal,
                            (int)this.Fields[relatedEntityName + "Id"].CurrentValue, this.Transaction);
                    }
                    else
                    {
                        toReturn = Dionysos.DataFactory.EntityFactory.GetEntity(relatedEntityName, (int)this.Fields[relatedEntityName + "Id"].CurrentValue) as IEntity;
                    }
                }

                return toReturn;
            }
        }	    

        public CommonEntityBase Parent
        {
            get 
            {
                return (CommonEntityBase)this.RelatedEntity;
            }
        }

	    // __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included Code

		#endregion
	}
}
