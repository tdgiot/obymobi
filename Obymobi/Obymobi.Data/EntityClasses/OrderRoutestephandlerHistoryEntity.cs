﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    using System.Xml.Serialization;
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'OrderRoutestephandlerHistory'. This class is used for Business Logic or for framework extension code.</summary>
	[Serializable]
	public partial class OrderRoutestephandlerHistoryEntity : OrderRoutestephandlerHistoryEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
        ,ICompanyRelatedChildEntity
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		/// <summary>CTor</summary>
		public OrderRoutestephandlerHistoryEntity():base()
		{
		}

		/// <summary>CTor</summary>
		/// <param name="orderRoutestephandlerHistoryId">PK value for OrderRoutestephandlerHistory which data should be fetched into this OrderRoutestephandlerHistory object</param>
		public OrderRoutestephandlerHistoryEntity(System.Int32 orderRoutestephandlerHistoryId):
			base(orderRoutestephandlerHistoryId)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="orderRoutestephandlerHistoryId">PK value for OrderRoutestephandlerHistory which data should be fetched into this OrderRoutestephandlerHistory object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public OrderRoutestephandlerHistoryEntity(System.Int32 orderRoutestephandlerHistoryId, IPrefetchPath prefetchPathToUse):
			base(orderRoutestephandlerHistoryId, prefetchPathToUse)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="orderRoutestephandlerHistoryId">PK value for OrderRoutestephandlerHistory which data should be fetched into this OrderRoutestephandlerHistory object</param>
		/// <param name="validator">The custom validator object for this OrderRoutestephandlerHistoryEntity</param>
		public OrderRoutestephandlerHistoryEntity(System.Int32 orderRoutestephandlerHistoryId, IValidator validator):
			base(orderRoutestephandlerHistoryId, validator)
		{
		}
		
		/// <summary>CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected OrderRoutestephandlerHistoryEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}

		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode

        [XmlIgnore]
        public CommonEntityBase Parent
        {
            get { return this.OrderEntity; }
        }

        public Obymobi.Enums.RoutestephandlerType HandlerTypeAsEnum
        {
            get
            {
                return this.HandlerType.ToEnum<Obymobi.Enums.RoutestephandlerType>();
            }
            set
            {
                this.HandlerType = (int)value;
            }
        }

        public Obymobi.Enums.OrderProcessingError ErrorCodeAsEnum
        {
            get
            {
                return this.ErrorCode.ToEnum<Obymobi.Enums.OrderProcessingError>();
            }
            set
            {
                this.ErrorCode = (int)value;
            }
        }

        public Obymobi.Enums.OrderRoutestephandlerStatus StatusAsEnum
        {
            get
            {
                return this.Status.ToEnum<Obymobi.Enums.OrderRoutestephandlerStatus>();
            }
            set
            {
                this.Status = (int)value;
            }
        }

		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included Code

		#endregion
	}
}
