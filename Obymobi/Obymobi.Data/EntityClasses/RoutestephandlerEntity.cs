﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    using Obymobi.Enums;
    using System.Xml.Serialization;
    // __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'Routestephandler'. This class is used for Business Logic or for framework extension code.</summary>
	[Serializable]
	public partial class RoutestephandlerEntity : RoutestephandlerEntityBase
        // __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
        , ICompanyRelatedChildEntity, IMediaContainingEntity, ICustomTextContainingEntity, ITimestampEntity
    // __LLBLGENPRO_USER_CODE_REGION_END	
	{
		/// <summary>CTor</summary>
		public RoutestephandlerEntity():base()
		{
		}

		/// <summary>CTor</summary>
		/// <param name="routestephandlerId">PK value for Routestephandler which data should be fetched into this Routestephandler object</param>
		public RoutestephandlerEntity(System.Int32 routestephandlerId):
			base(routestephandlerId)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="routestephandlerId">PK value for Routestephandler which data should be fetched into this Routestephandler object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public RoutestephandlerEntity(System.Int32 routestephandlerId, IPrefetchPath prefetchPathToUse):
			base(routestephandlerId, prefetchPathToUse)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="routestephandlerId">PK value for Routestephandler which data should be fetched into this Routestephandler object</param>
		/// <param name="validator">The custom validator object for this RoutestephandlerEntity</param>
		public RoutestephandlerEntity(System.Int32 routestephandlerId, IValidator validator):
			base(routestephandlerId, validator)
		{
		}
		
		/// <summary>CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected RoutestephandlerEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
            // __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
            // __LLBLGENPRO_USER_CODE_REGION_END
		}

		#region Custom Entity code
		
        // __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode

        [XmlIgnore]
        public CommonEntityBase Parent
        {
            get { return this.RoutestepEntity; }
        }

        [XmlIgnore]
        public RoutestephandlerType HandlerTypeAsEnum
        {
            get
            {
                return (RoutestephandlerType)this.HandlerType;
            }
            set
            {
                this.HandlerType = (int)value;
            }
        }


        /// <summary>
        /// Return the routestephandlerType as enum
        /// </summary>
        [XmlIgnore]
        public RoutestephandlerType RoutestephandlerTypeAsEnum
        {
            get
            {
                return (RoutestephandlerType)this.HandlerType;
            }
            set
            {
                this.HandlerType = (int)value;
            }
        }

        /// <summary>
        /// Returns the routestephandlerType as text
        /// </summary>
        [DataGridViewColumnVisibleAttribute]
        [XmlIgnore]
        public string RoutestephandlerTypeAsText
        {
            get
            {
                return this.RoutestephandlerTypeAsEnum.ToString();
            }
        }

        /// <summary>
        /// Returns the Print Report Type as enum
        /// </summary>
        [XmlIgnore]
        public PrintReportType PrintReportTypeAsEnum
        {
            get
            {
                if (this.PrintReportType.HasValue)
                    return (PrintReportType)this.PrintReportType;
                else
                    return Enums.PrintReportType.Standaard;
            }
            set
            {
                this.PrintReportType = (int)value;
            }
        }

        /// <summary>
        /// Returns the Print Report Type as text
        /// </summary>
        [DataGridViewColumnVisibleAttribute]
        [XmlIgnore]
        public string PrintReportTypeAsText
        {
            get
            {
                return this.PrintReportTypeAsEnum.ToString();
            }
        }

        public override bool Save(IPredicate updateRestriction, bool recurse)
        {
            return base.Save(updateRestriction, recurse);
        }

        [XmlIgnore]
	    public string Email
	    {
            get { return this.FieldValue1; }
            set { this.FieldValue1 = value; }
	    }

	    // __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included Code

		#endregion
	}
}
