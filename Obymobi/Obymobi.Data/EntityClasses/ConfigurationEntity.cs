﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'Configuration'. This class is used for Business Logic or for framework extension code.</summary>
	[Serializable]
	public partial class ConfigurationEntity : ConfigurationEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
        ,ISystemEntity
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		/// <summary>CTor</summary>
		public ConfigurationEntity():base()
		{
		}

		/// <summary>CTor</summary>
		/// <param name="configurationId">PK value for Configuration which data should be fetched into this Configuration object</param>
		public ConfigurationEntity(System.Int32 configurationId):
			base(configurationId)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="configurationId">PK value for Configuration which data should be fetched into this Configuration object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public ConfigurationEntity(System.Int32 configurationId, IPrefetchPath prefetchPathToUse):
			base(configurationId, prefetchPathToUse)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="configurationId">PK value for Configuration which data should be fetched into this Configuration object</param>
		/// <param name="validator">The custom validator object for this ConfigurationEntity</param>
		public ConfigurationEntity(System.Int32 configurationId, IValidator validator):
			base(configurationId, validator)
		{
		}
		
		/// <summary>CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected ConfigurationEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}

		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included Code

		#endregion
	}
}
