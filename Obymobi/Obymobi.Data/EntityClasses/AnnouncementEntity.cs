﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    using System.Xml.Serialization;
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'Announcement'. This class is used for Business Logic or for framework extension code.</summary>
	[Serializable]
	public partial class AnnouncementEntity : AnnouncementEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
        , INullableCompanyRelatedEntity, ICustomTextContainingEntity
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		/// <summary>CTor</summary>
		public AnnouncementEntity():base()
		{
		}

		/// <summary>CTor</summary>
		/// <param name="announcementId">PK value for Announcement which data should be fetched into this Announcement object</param>
		public AnnouncementEntity(System.Int32 announcementId):
			base(announcementId)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="announcementId">PK value for Announcement which data should be fetched into this Announcement object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public AnnouncementEntity(System.Int32 announcementId, IPrefetchPath prefetchPathToUse):
			base(announcementId, prefetchPathToUse)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="announcementId">PK value for Announcement which data should be fetched into this Announcement object</param>
		/// <param name="validator">The custom validator object for this AnnouncementEntity</param>
		public AnnouncementEntity(System.Int32 announcementId, IValidator validator):
			base(announcementId, validator)
		{
		}
		
		/// <summary>CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected AnnouncementEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}

		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode

        [DataGridViewColumnVisible]
        [XmlIgnore]
        public string YesActionAsString
        {
            get 
            {
                string toReturn = "";

                if (this.OnYesProduct.HasValue)
                {
                    // Product
                    if (this.ProductEntity != null)
                        toReturn += this.ProductEntity.Name;
                }
                else if (this.OnYesCategory.HasValue)
                { 
                    // Category
                    if (this.CategoryEntity_ != null)
                        toReturn += this.CategoryEntity_.Name;
                }
                else if (this.OnYesEntertainment.HasValue)
                { 
                    // Entertainment 
                    if (this.EntertainmentEntity != null)
                        toReturn += this.EntertainmentEntity.Name;
                }
                else if (this.OnYesEntertainmentCategory.HasValue)
                {
                    // Entertainment category
                    if (this.EntertainmentcategoryEntity != null)
                        toReturn += this.EntertainmentcategoryEntity.Name;
                }   
                
                return toReturn;
            }
        }

		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included Code

		#endregion
	}
}
