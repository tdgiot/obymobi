﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    using Dionysos;
    using Obymobi.Enums;
    using System.Xml.Serialization;
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'WifiConfiguration'. This class is used for Business Logic or for framework extension code.</summary>
	[Serializable]
	public partial class WifiConfigurationEntity : WifiConfigurationEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
        , ICompanyRelatedEntity
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		/// <summary>CTor</summary>
		public WifiConfigurationEntity():base()
		{
		}

		/// <summary>CTor</summary>
		/// <param name="wifiConfigurationId">PK value for WifiConfiguration which data should be fetched into this WifiConfiguration object</param>
		public WifiConfigurationEntity(System.Int32 wifiConfigurationId):
			base(wifiConfigurationId)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="wifiConfigurationId">PK value for WifiConfiguration which data should be fetched into this WifiConfiguration object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public WifiConfigurationEntity(System.Int32 wifiConfigurationId, IPrefetchPath prefetchPathToUse):
			base(wifiConfigurationId, prefetchPathToUse)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="wifiConfigurationId">PK value for WifiConfiguration which data should be fetched into this WifiConfiguration object</param>
		/// <param name="validator">The custom validator object for this WifiConfigurationEntity</param>
		public WifiConfigurationEntity(System.Int32 wifiConfigurationId, IValidator validator):
			base(wifiConfigurationId, validator)
		{
		}
		
		/// <summary>CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected WifiConfigurationEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}

		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode

        [XmlIgnore]
        [DataGridViewColumnVisible]
        public WifiSecurityMethod SecurityAsEnum
        {
            get
            {
                return this.Security.ToEnum<WifiSecurityMethod>();
            }
            set
            {
                this.Security = (int)value;
            }
        }

		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included Code

		#endregion
	}
}
