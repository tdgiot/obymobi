﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'InfraredCommand'. This class is used for Business Logic or for framework extension code.</summary>
	[Serializable]
	public partial class InfraredCommandEntity : InfraredCommandEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
        , INonCompanyRelatedEntity, ITimestampEntity, ICustomTextContainingEntity
        // __LLBLGENPRO_USER_CODE_REGION_END	
	{
		/// <summary>CTor</summary>
		public InfraredCommandEntity():base()
		{
		}

		/// <summary>CTor</summary>
		/// <param name="infraredCommandId">PK value for InfraredCommand which data should be fetched into this InfraredCommand object</param>
		public InfraredCommandEntity(System.Int32 infraredCommandId):
			base(infraredCommandId)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="infraredCommandId">PK value for InfraredCommand which data should be fetched into this InfraredCommand object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public InfraredCommandEntity(System.Int32 infraredCommandId, IPrefetchPath prefetchPathToUse):
			base(infraredCommandId, prefetchPathToUse)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="infraredCommandId">PK value for InfraredCommand which data should be fetched into this InfraredCommand object</param>
		/// <param name="validator">The custom validator object for this InfraredCommandEntity</param>
		public InfraredCommandEntity(System.Int32 infraredCommandId, IValidator validator):
			base(infraredCommandId, validator)
		{
		}
		
		/// <summary>CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected InfraredCommandEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}

		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode

	    public string TypeStringValue
	    {
	        get { return this.Type.GetStringValue(); }
	    }

        public bool CustomCommand
        {
            get { return this.Type == Enums.InfraredCommandType.Custom; }
        }

	    // __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included Code

		#endregion
	}
}
