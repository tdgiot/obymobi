﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    using System.Xml.Serialization;
    using Dionysos;
    // __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'ScheduledMessageHistory'. This class is used for Business Logic or for framework extension code.</summary>
	[Serializable]
	public partial class ScheduledMessageHistoryEntity : ScheduledMessageHistoryEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
        , INullableCompanyRelatedChildEntity
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		/// <summary>CTor</summary>
		public ScheduledMessageHistoryEntity():base()
		{
		}

		/// <summary>CTor</summary>
		/// <param name="scheduledMessageHistoryId">PK value for ScheduledMessageHistory which data should be fetched into this ScheduledMessageHistory object</param>
		public ScheduledMessageHistoryEntity(System.Int32 scheduledMessageHistoryId):
			base(scheduledMessageHistoryId)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="scheduledMessageHistoryId">PK value for ScheduledMessageHistory which data should be fetched into this ScheduledMessageHistory object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public ScheduledMessageHistoryEntity(System.Int32 scheduledMessageHistoryId, IPrefetchPath prefetchPathToUse):
			base(scheduledMessageHistoryId, prefetchPathToUse)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="scheduledMessageHistoryId">PK value for ScheduledMessageHistory which data should be fetched into this ScheduledMessageHistory object</param>
		/// <param name="validator">The custom validator object for this ScheduledMessageHistoryEntity</param>
		public ScheduledMessageHistoryEntity(System.Int32 scheduledMessageHistoryId, IValidator validator):
			base(scheduledMessageHistoryId, validator)
		{
		}
		
		/// <summary>CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected ScheduledMessageHistoryEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}

		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode

        [System.Xml.Serialization.XmlIgnore]
        public CommonEntityBase Parent
        {
            get { return this.ScheduledMessageEntity; }
        }

        [DataGridViewColumnVisible]
        [XmlIgnore]
        public DateTime SentInCompanyTimeZone
        {
            get
            {
                if (!this.ScheduledMessageEntity.IsNew && this.CreatedUTC.HasValue)
                {
                    return this.CreatedUTC.Value.ToTimeZone(TimeZoneInfo.Utc, this.ScheduledMessageEntity.CompanyEntity.TimeZoneInfo);
                }
                return this.Sent;
            }
        }

		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included Code

		#endregion
	}
}
