﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    using Dionysos;
    using Obymobi.Enums;
    using System.Xml.Serialization;
    // __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'Netmessage'. This class is used for Business Logic or for framework extension code.</summary>
	[Serializable]
	public partial class NetmessageEntity : NetmessageEntityBase
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
        , INonCompanyRelatedEntity, ITimestampEntity
    // __LLBLGENPRO_USER_CODE_REGION_END	
	{
		/// <summary>CTor</summary>
		public NetmessageEntity():base()
		{
		}

		/// <summary>CTor</summary>
		/// <param name="netmessageId">PK value for Netmessage which data should be fetched into this Netmessage object</param>
		public NetmessageEntity(System.Int32 netmessageId):
			base(netmessageId)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="netmessageId">PK value for Netmessage which data should be fetched into this Netmessage object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public NetmessageEntity(System.Int32 netmessageId, IPrefetchPath prefetchPathToUse):
			base(netmessageId, prefetchPathToUse)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="netmessageId">PK value for Netmessage which data should be fetched into this Netmessage object</param>
		/// <param name="validator">The custom validator object for this NetmessageEntity</param>
		public NetmessageEntity(System.Int32 netmessageId, IValidator validator):
			base(netmessageId, validator)
		{
		}
		
		/// <summary>CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected NetmessageEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
            // __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
            // __LLBLGENPRO_USER_CODE_REGION_END
		}

		#region Custom Entity code
		
        // __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode

        [XmlIgnore]
        public NetmessageType MessageTypeAsEnum
        {
            get
            {
                try
                {
                    return this.MessageType.ToEnum<NetmessageType>();
                }
                catch
                {
                    return NetmessageType.Unknown;
                }
            }
            set
            {
                this.MessageType = (int)value;
            }
        }

        [XmlIgnore]
        public NetmessageStatus StatusAsEnum
        {
            get
            {
                return this.Status.ToEnum<NetmessageStatus>();
            }
            set
            {
                this.Status = (int)value;
            }
        }        

        public override bool Save(IPredicate updateRestriction, bool recurse)
        {
            return base.Save(updateRestriction, recurse);
        }

        // __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included Code

		#endregion
	}
}
