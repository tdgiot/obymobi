﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    using System.Xml.Serialization;
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'Menu'. This class is used for Business Logic or for framework extension code.</summary>
	[Serializable]
	public partial class MenuEntity : MenuEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
        , ICompanyRelatedEntity, ITimestampEntity
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		/// <summary>CTor</summary>
		public MenuEntity():base()
		{
		}

		/// <summary>CTor</summary>
		/// <param name="menuId">PK value for Menu which data should be fetched into this Menu object</param>
		public MenuEntity(System.Int32 menuId):
			base(menuId)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="menuId">PK value for Menu which data should be fetched into this Menu object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public MenuEntity(System.Int32 menuId, IPrefetchPath prefetchPathToUse):
			base(menuId, prefetchPathToUse)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="menuId">PK value for Menu which data should be fetched into this Menu object</param>
		/// <param name="validator">The custom validator object for this MenuEntity</param>
		public MenuEntity(System.Int32 menuId, IValidator validator):
			base(menuId, validator)
		{
		}
		
		/// <summary>CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected MenuEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}

		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode

        [DataGridViewColumnVisible]
        [XmlIgnore]
        public int DeliverypointgroupsCount
        {
            get { return this.DeliverypointgroupCollection.Count; }
        }

        [DataGridViewColumnVisible]
        [XmlIgnore]
        public string ConnectedDeliverypointgroupsAsString
        { 
            get
            {
                string deliverypointgroupsString = "";

                for (int i = 0; i < this.DeliverypointgroupCollection.Count; i++)
                {
                    int count = i + 1;

                    DeliverypointgroupEntity dpg = this.DeliverypointgroupCollection[i];

                    if (count == this.DeliverypointgroupCollection.Count)
                        deliverypointgroupsString += dpg.Name;
                    else
                        deliverypointgroupsString += dpg.Name + ", ";
                }

                return deliverypointgroupsString;
            }
        }

		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included Code

		#endregion
	}
}
