﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    using System.Xml.Serialization;
    // __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'AttachmentLanguage'. This class is used for Business Logic or for framework extension code.</summary>
	[Serializable]
	public partial class AttachmentLanguageEntity : AttachmentLanguageEntityBase
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
    , ICompanyRelatedChildEntityOrNullableCompanyRelatedChildEntity
    // __LLBLGENPRO_USER_CODE_REGION_END	
	{
		/// <summary>CTor</summary>
		public AttachmentLanguageEntity():base()
		{
		}

		/// <summary>CTor</summary>
		/// <param name="attachmentLanguageId">PK value for AttachmentLanguage which data should be fetched into this AttachmentLanguage object</param>
		public AttachmentLanguageEntity(System.Int32 attachmentLanguageId):
			base(attachmentLanguageId)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="attachmentLanguageId">PK value for AttachmentLanguage which data should be fetched into this AttachmentLanguage object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public AttachmentLanguageEntity(System.Int32 attachmentLanguageId, IPrefetchPath prefetchPathToUse):
			base(attachmentLanguageId, prefetchPathToUse)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="attachmentLanguageId">PK value for AttachmentLanguage which data should be fetched into this AttachmentLanguage object</param>
		/// <param name="validator">The custom validator object for this AttachmentLanguageEntity</param>
		public AttachmentLanguageEntity(System.Int32 attachmentLanguageId, IValidator validator):
			base(attachmentLanguageId, validator)
		{
		}
		
		/// <summary>CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected AttachmentLanguageEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}

		#region Custom Entity code
		
        // __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode

        [XmlIgnore]
        public CommonEntityBase Parent
        {
            get { return this.AttachmentEntity; }
        }

        // __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included Code

		#endregion
	}
}
