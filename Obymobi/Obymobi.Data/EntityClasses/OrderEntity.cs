﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Dionysos.Data;
using System.Diagnostics;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    using Dionysos;
    using Obymobi.Enums;
    using Obymobi.Interfaces;
    using Dionysos.Data;
    using System.Linq;
    using System.Xml.Serialization;
    using Obymobi.Extensions;
    // __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'Order'. This class is used for Business Logic or for framework extension code.</summary>
	[Serializable]
	public partial class OrderEntity : OrderEntityBase
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
        , INocActivityEntity, ICompanyRelatedEntity, ITimestampEntity
    // __LLBLGENPRO_USER_CODE_REGION_END	
	{
		/// <summary>CTor</summary>
		public OrderEntity():base()
		{
		}

		/// <summary>CTor</summary>
		/// <param name="orderId">PK value for Order which data should be fetched into this Order object</param>
		public OrderEntity(System.Int32 orderId):
			base(orderId)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="orderId">PK value for Order which data should be fetched into this Order object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public OrderEntity(System.Int32 orderId, IPrefetchPath prefetchPathToUse):
			base(orderId, prefetchPathToUse)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="orderId">PK value for Order which data should be fetched into this Order object</param>
		/// <param name="validator">The custom validator object for this OrderEntity</param>
		public OrderEntity(System.Int32 orderId, IValidator validator):
			base(orderId, validator)
		{
		}
		
		/// <summary>CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected OrderEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
            // __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
            // __LLBLGENPRO_USER_CODE_REGION_END
		}

		#region Custom Entity code
		
        // __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode

        #region Methods
        #endregion

        #region Properties

        public bool validatorVariableManuallyProcessedOrder = false;

        private bool overrideChangeProtection = false;
        /// <summary>
        /// When set to true it's possible to change an Order when it's already saved
        /// 
        /// </summary>
        public bool OverrideChangeProtection
        {
            get
            {
                return this.overrideChangeProtection;
            }
            set
            {
                this.overrideChangeProtection = value;
            }
        }

        /// <summary>
        /// Gets the price including vat (SUM(Orderitem.PriceIn))
        /// </summary>
        [DataGridViewColumnVisible]
        public decimal PriceIn
        {
            get
            {
                return this.OrderitemCollection.Sum(o => o.PriceIn);
            }
        }

        /// <summary>
        /// Gets or sets the order type as an enumerator
        /// </summary>
        public OrderType TypeAsEnum
        {
            get
            {
                return (OrderType)Enum.Parse(typeof(OrderType), this.Type.ToString());
            }
            set
            {
                this.Type = (int)value;
            }
        }

        /// <summary>
        /// Gets or sets the order type
        /// </summary>
        public override int Type
        {
            get
            {
                return base.Type;
            }
            set
            {
                base.Type = value;
                base.TypeText = this.TypeAsEnum.ToString();
            }
        }

        /// <summary>
        /// Gets or sets the order status
        /// </summary>
        public override int Status
        {
            get
            {
                return base.Status;
            }
            set
            {
                base.Status = value;
                base.StatusText = this.StatusAsEnum.ToString();
            }
        }

        /// <summary>
        /// Gets or sets the order status as an enumerator
        /// </summary>
        public OrderStatus StatusAsEnum
        {
            get
            {
                return this.Status.ToEnum<OrderStatus>();
            }
            set
            {
                this.Status = (int)value;
            }
        }

        private bool validatedByOrderProcessingHelper = false;

        /// <summary>
        /// Gets or sets the VerifiedByOrderProcessingHelper
        /// </summary>
        public bool ValidatedByOrderProcessingHelper
        {
            get
            {
                return this.validatedByOrderProcessingHelper;
            }
            set
            {
                this.validatedByOrderProcessingHelper = value;
            }
        }

        private bool sendPokeInUpdateAfterSave = false;

        /// <summary>
        /// Gets or sets the sendPokeInUpdateAfterSave which determines if the OrderValitor send a poke in
        /// message after save (should not be set by another than the Validator as it will overwrite it anyway)
        /// </summary>
        public bool SendPokeInUpdateAfterSave
        {
            get
            {
                return this.sendPokeInUpdateAfterSave;
            }
            set
            {
                this.sendPokeInUpdateAfterSave = value;
            }
        }


        /// <summary>
        /// Gets or sets the error code as enum.
        /// </summary>
        /// <value>
        /// The error code as enum.
        /// </value>
        public OrderProcessingError ErrorCodeAsEnum
        {
            get
            {
                return this.ErrorCode.ToEnum<OrderProcessingError>();
            }
            set
            {
                this.ErrorCode = (int)value;
            }
        }

        /// <summary>
        /// Gets the last activity (UTC) for this order
        /// </summary>
        public DateTime? LastActivityUTC
        {
            get
            {
                return (this.UpdatedUTC.HasValue ? this.UpdatedUTC : this.CreatedUTC);
            }
        }

	    public byte[] File { get; set; }

        /// <summary>
        /// Gets the time zone info the order was placed in or utc as fallback.
        /// </summary>
        [XmlIgnore]
        public TimeZoneInfo TimeZoneInfo => this.TimeZoneOlsonId.IsNullOrWhiteSpace() ? TimeZoneInfo.Utc : TimeZoneInfo.FindSystemTimeZoneById(TimeZone.Mappings[this.TimeZoneOlsonId].WindowsTimeZoneId);

        /// <summary>
        /// Gets the sub total price (excluding tax) formatted based on company culture.
        /// </summary>
        [XmlIgnore]
        public string SubTotalFormatted => this.SubTotal.Format(this.CultureCode);

        /// <summary>
        /// Gets the total price (including tax) formatted based on company culture.
        /// </summary>
        [XmlIgnore]
        public string TotalFormatted => this.Total.Format(this.CultureCode);

        [XmlIgnore]
        public DateTime? ProcessedLocal => this.ProcessedUTC.UtcToLocalTime(this.OrderEntity.TimeZoneInfo);

        #endregion

        // __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included Code

		#endregion
	}
}
