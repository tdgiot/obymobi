﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    using Obymobi.Data.HelperClasses;
    using System.Xml.Serialization;
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'OrderRoutestephandler'. This class is used for Business Logic or for framework extension code.</summary>
	[Serializable]
	public partial class OrderRoutestephandlerEntity : OrderRoutestephandlerEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
        , ICompanyRelatedChildEntity, ITimestampEntity
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		/// <summary>CTor</summary>
		public OrderRoutestephandlerEntity():base()
		{
		}

		/// <summary>CTor</summary>
		/// <param name="orderRoutestephandlerId">PK value for OrderRoutestephandler which data should be fetched into this OrderRoutestephandler object</param>
		public OrderRoutestephandlerEntity(System.Int32 orderRoutestephandlerId):
			base(orderRoutestephandlerId)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="orderRoutestephandlerId">PK value for OrderRoutestephandler which data should be fetched into this OrderRoutestephandler object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public OrderRoutestephandlerEntity(System.Int32 orderRoutestephandlerId, IPrefetchPath prefetchPathToUse):
			base(orderRoutestephandlerId, prefetchPathToUse)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="orderRoutestephandlerId">PK value for OrderRoutestephandler which data should be fetched into this OrderRoutestephandler object</param>
		/// <param name="validator">The custom validator object for this OrderRoutestephandlerEntity</param>
		public OrderRoutestephandlerEntity(System.Int32 orderRoutestephandlerId, IValidator validator):
			base(orderRoutestephandlerId, validator)
		{
		}
		
		/// <summary>CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected OrderRoutestephandlerEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}

		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode

        [XmlIgnore]
        public CommonEntityBase Parent
        {
            get { return this.OrderEntity; }
        }

        public bool ValidatorFlagRouteHasFinished = false;
        public bool ValidatorFlagBeingPartofUpdateBecauseOfCompletionOfAStep = false;

        public Obymobi.Enums.RoutestephandlerType HandlerTypeAsEnum
        {
            get
            {
                return this.HandlerType.ToEnum<Obymobi.Enums.RoutestephandlerType>();
            }
            set
            {
                this.HandlerType = (int)value;
            }
        }

        public Obymobi.Enums.OrderProcessingError ErrorCodeAsEnum
        {
            get
            {
                return this.ErrorCode.ToEnum<Obymobi.Enums.OrderProcessingError>();
            }
            set
            {
                this.ErrorCode = (int)value;
            }
        }

        public Obymobi.Enums.OrderRoutestephandlerStatus StatusAsEnum
        {
            get
            {
                return this.Status.ToEnum<Obymobi.Enums.OrderRoutestephandlerStatus>();
            }
            set
            {
                this.Status = (int)value;
            }
        }

        public bool HasParallelStep
        {
            get 
            {                 
                EntityView<OrderRoutestephandlerEntity> orView = this.OrderEntity.OrderRoutestephandlerCollection.DefaultView;
                orView.Filter = new PredicateExpression(OrderRoutestephandlerFields.Number == this.Number);
                return orView.Count > 1 ? true : false;
            }
        }

	    public byte[] File { get; set; }

        /// <summary>
        /// Gets or sets whether the receipt should be send through email.
        /// </summary>
        [XmlIgnore]
        public bool EmailEnabled => this.FieldValue2.Equals("1", StringComparison.InvariantCultureIgnoreCase);

        /// <summary>
        /// Gets or sets whether the receipt should be send through sms.
        /// </summary>
        [XmlIgnore]
        public bool SmsEnabled => this.FieldValue3.Equals("1", StringComparison.InvariantCultureIgnoreCase);

        // __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included Code

		#endregion
	}
}
