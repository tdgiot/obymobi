﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'TerminalConfiguration'. This class is used for Business Logic or for framework extension code.</summary>
	[Serializable]
	public partial class TerminalConfigurationEntity : TerminalConfigurationEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
        , ICompanyRelatedEntity
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		/// <summary>CTor</summary>
		public TerminalConfigurationEntity():base()
		{
		}

		/// <summary>CTor</summary>
		/// <param name="terminalConfigurationId">PK value for TerminalConfiguration which data should be fetched into this TerminalConfiguration object</param>
		public TerminalConfigurationEntity(System.Int32 terminalConfigurationId):
			base(terminalConfigurationId)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="terminalConfigurationId">PK value for TerminalConfiguration which data should be fetched into this TerminalConfiguration object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public TerminalConfigurationEntity(System.Int32 terminalConfigurationId, IPrefetchPath prefetchPathToUse):
			base(terminalConfigurationId, prefetchPathToUse)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="terminalConfigurationId">PK value for TerminalConfiguration which data should be fetched into this TerminalConfiguration object</param>
		/// <param name="validator">The custom validator object for this TerminalConfigurationEntity</param>
		public TerminalConfigurationEntity(System.Int32 terminalConfigurationId, IValidator validator):
			base(terminalConfigurationId, validator)
		{
		}
		
		/// <summary>CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected TerminalConfigurationEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}

		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included Code

		#endregion
	}
}
