﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    using System.Xml.Serialization;
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'ExternalProduct'. This class is used for Business Logic or for framework extension code.</summary>
	[Serializable]
	public partial class ExternalProductEntity : ExternalProductEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
        ,ICompanyRelatedChildEntity
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		/// <summary>CTor</summary>
		public ExternalProductEntity():base()
		{
		}

		/// <summary>CTor</summary>
		/// <param name="externalProductId">PK value for ExternalProduct which data should be fetched into this ExternalProduct object</param>
		public ExternalProductEntity(System.Int32 externalProductId):
			base(externalProductId)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="externalProductId">PK value for ExternalProduct which data should be fetched into this ExternalProduct object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public ExternalProductEntity(System.Int32 externalProductId, IPrefetchPath prefetchPathToUse):
			base(externalProductId, prefetchPathToUse)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="externalProductId">PK value for ExternalProduct which data should be fetched into this ExternalProduct object</param>
		/// <param name="validator">The custom validator object for this ExternalProductEntity</param>
		public ExternalProductEntity(System.Int32 externalProductId, IValidator validator):
			base(externalProductId, validator)
		{
		}
		
		/// <summary>CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected ExternalProductEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}

		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode

        [XmlIgnore]
        public CommonEntityBase Parent
        {
            get { return this.ExternalSystemEntity; }
        }

        [DataGridViewColumnVisible]
        [XmlIgnore]
        [EntityFieldDependency((int)ExternalProductFieldIndex.Type)]
        public string TypeText
        {
            get
            {
                return this.Type.ToString();
            }
        }

        public string CombinedNameWithMenuAndSystemName => ExternalMenuId.HasValue
            ? $"{Name} - {Id} - {ExternalMenuEntity.Name} - {ExternalSystemEntity.Name}"
            : $"{Name} - {Id} - {ExternalSystemEntity.Name}";

		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included Code

		#endregion
	}
}
