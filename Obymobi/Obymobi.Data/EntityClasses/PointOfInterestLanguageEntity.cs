﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'PointOfInterestLanguage'. This class is used for Business Logic or for framework extension code.</summary>
	[Serializable]
	public partial class PointOfInterestLanguageEntity : PointOfInterestLanguageEntityBase
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
    , INonCompanyRelatedEntity
    // __LLBLGENPRO_USER_CODE_REGION_END	
	{
		/// <summary>CTor</summary>
		public PointOfInterestLanguageEntity():base()
		{
		}

		/// <summary>CTor</summary>
		/// <param name="pointOfInterestLanguageId">PK value for PointOfInterestLanguage which data should be fetched into this PointOfInterestLanguage object</param>
		public PointOfInterestLanguageEntity(System.Int32 pointOfInterestLanguageId):
			base(pointOfInterestLanguageId)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="pointOfInterestLanguageId">PK value for PointOfInterestLanguage which data should be fetched into this PointOfInterestLanguage object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public PointOfInterestLanguageEntity(System.Int32 pointOfInterestLanguageId, IPrefetchPath prefetchPathToUse):
			base(pointOfInterestLanguageId, prefetchPathToUse)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="pointOfInterestLanguageId">PK value for PointOfInterestLanguage which data should be fetched into this PointOfInterestLanguage object</param>
		/// <param name="validator">The custom validator object for this PointOfInterestLanguageEntity</param>
		public PointOfInterestLanguageEntity(System.Int32 pointOfInterestLanguageId, IValidator validator):
			base(pointOfInterestLanguageId, validator)
		{
		}
		
		/// <summary>CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected PointOfInterestLanguageEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}

		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included Code

		#endregion
	}
}
