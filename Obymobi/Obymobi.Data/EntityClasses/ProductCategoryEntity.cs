﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    using System.Xml.Serialization;
    using Obymobi.Logic.Cms;
    using Obymobi.Enums;
    // __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'ProductCategory'. This class is used for Business Logic or for framework extension code.</summary>
	[Serializable]
	public partial class ProductCategoryEntity : ProductCategoryEntityBase
        // __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
        , INullableCompanyRelatedChildEntity, IMenuItem, ITimestampEntity
    // __LLBLGENPRO_USER_CODE_REGION_END	
	{
		/// <summary>CTor</summary>
		public ProductCategoryEntity():base()
		{
		}

		/// <summary>CTor</summary>
		/// <param name="productCategoryId">PK value for ProductCategory which data should be fetched into this ProductCategory object</param>
		public ProductCategoryEntity(System.Int32 productCategoryId):
			base(productCategoryId)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="productCategoryId">PK value for ProductCategory which data should be fetched into this ProductCategory object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public ProductCategoryEntity(System.Int32 productCategoryId, IPrefetchPath prefetchPathToUse):
			base(productCategoryId, prefetchPathToUse)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="productCategoryId">PK value for ProductCategory which data should be fetched into this ProductCategory object</param>
		/// <param name="validator">The custom validator object for this ProductCategoryEntity</param>
		public ProductCategoryEntity(System.Int32 productCategoryId, IValidator validator):
			base(productCategoryId, validator)
		{
		}
		
		/// <summary>CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected ProductCategoryEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
            // __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
            // __LLBLGENPRO_USER_CODE_REGION_END
		}

		#region Custom Entity code
		
        // __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode

        [XmlIgnore]
        public CommonEntityBase Parent
        {
            get { return this.ProductEntity; }
        }

        [XmlIgnore]
        public string ProductName
        {
            get
            {
                return this.ProductEntity.Name;
            }
        }

	    [XmlIgnore]
	    public string ProductCategoryMenuName
	    {
	        get { return string.Format("{0} - {1} - {2}", this.ProductEntity.Name, this.CategoryEntity.Name, this.CategoryEntity.MenuEntity.Name); }
	    }

        [XmlIgnore]
        public string ProductCategoryName
        {
            get { return string.Format("{0} - {1}", this.ProductEntity.Name, this.CategoryEntity.Name); }
        }

        [XmlIgnore]
        public string MenuCategoryProductName
        {
            get { return string.Format("{0} - {1} - {2}", this.CategoryEntity.MenuEntity.Name, this.CategoryEntity.Name, this.ProductEntity.Name); }
        }

        #region Treelist Logic

        [XmlIgnore]
        public string ItemId
        {
            get
            {
                return string.Format("{0}-{1}-{2}", this.ItemType, this.ProductId, this.CategoryId);
            }
        }

        [XmlIgnore]
        public string ParentItemId
        {
            get
            {
                return this.CategoryEntity.ItemId;
            }
        }

        [XmlIgnore]
        public string ItemType
        {
            get
            {
                return MenuItemType.Product;
            }
        }

        [XmlIgnore]
        public string TypeName
        {
            get
            {
                return MenuItemType.Product;
            }
        }

	    [XmlIgnore]
	    public string Name
	    {
	        get { return this.ProductEntity.Name; }
	        set { this.ProductEntity.Name = value; }
	    }

        [XmlIgnore]
        public bool Visible
        {
            get
            {
                return this.ProductEntity.Visible;
            }
        }

        [XmlIgnore]
        public VisibilityType VisibilityType
        {
            get { return this.ProductEntity.VisibilityType; }
        }

	    [XmlIgnore]
	    public bool IsLinkedToBrand
	    {
	        get
	        {
                return this.ProductEntity.IsLinkedToGeneric || this.ProductEntity.BrandProductId.HasValue;
	        }
	    }
        
        public bool CategoryChanged { get; set; }

	    #endregion        

	    // __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included Code

		#endregion
	}
}
