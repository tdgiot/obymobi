﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    using System.Xml.Serialization;
    // __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'CategorySuggestion'. This class is used for Business Logic or for framework extension code.</summary>
	[Serializable]
	public partial class CategorySuggestionEntity : CategorySuggestionEntityBase
        // __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
        , ICompanyRelatedChildEntity, ITimestampEntity
    // __LLBLGENPRO_USER_CODE_REGION_END	
	{
		/// <summary>CTor</summary>
		public CategorySuggestionEntity():base()
		{
		}

		/// <summary>CTor</summary>
		/// <param name="categorySuggestionId">PK value for CategorySuggestion which data should be fetched into this CategorySuggestion object</param>
		public CategorySuggestionEntity(System.Int32 categorySuggestionId):
			base(categorySuggestionId)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="categorySuggestionId">PK value for CategorySuggestion which data should be fetched into this CategorySuggestion object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public CategorySuggestionEntity(System.Int32 categorySuggestionId, IPrefetchPath prefetchPathToUse):
			base(categorySuggestionId, prefetchPathToUse)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="categorySuggestionId">PK value for CategorySuggestion which data should be fetched into this CategorySuggestion object</param>
		/// <param name="validator">The custom validator object for this CategorySuggestionEntity</param>
		public CategorySuggestionEntity(System.Int32 categorySuggestionId, IValidator validator):
			base(categorySuggestionId, validator)
		{
		}
		
		/// <summary>CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected CategorySuggestionEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
            // __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
            // __LLBLGENPRO_USER_CODE_REGION_END
		}

		#region Custom Entity code
		
        // __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode

        [XmlIgnore]
        public CommonEntityBase Parent
        {
            get { return this.CategoryEntity; }
        }

        // __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included Code

		#endregion
	}
}
