﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    using System.Xml.Serialization;
    using Obymobi.Enums;
    // __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'WidgetGroupWidget'. This class is used for Business Logic or for framework extension code.</summary>
	[Serializable]
	public partial class WidgetGroupWidgetEntity : WidgetGroupWidgetEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		, ICompanyRelatedChildEntity
	// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		/// <summary>CTor</summary>
		public WidgetGroupWidgetEntity():base()
		{
		}

		/// <summary>CTor</summary>
		/// <param name="widgetGroupId">PK value for WidgetGroupWidget which data should be fetched into this WidgetGroupWidget object</param>
		/// <param name="widgetId">PK value for WidgetGroupWidget which data should be fetched into this WidgetGroupWidget object</param>
		public WidgetGroupWidgetEntity(System.Int32 widgetGroupId, System.Int32 widgetId):
			base(widgetGroupId, widgetId)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="widgetGroupId">PK value for WidgetGroupWidget which data should be fetched into this WidgetGroupWidget object</param>
		/// <param name="widgetId">PK value for WidgetGroupWidget which data should be fetched into this WidgetGroupWidget object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public WidgetGroupWidgetEntity(System.Int32 widgetGroupId, System.Int32 widgetId, IPrefetchPath prefetchPathToUse):
			base(widgetGroupId, widgetId, prefetchPathToUse)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="widgetGroupId">PK value for WidgetGroupWidget which data should be fetched into this WidgetGroupWidget object</param>
		/// <param name="widgetId">PK value for WidgetGroupWidget which data should be fetched into this WidgetGroupWidget object</param>
		/// <param name="validator">The custom validator object for this WidgetGroupWidgetEntity</param>
		public WidgetGroupWidgetEntity(System.Int32 widgetGroupId, System.Int32 widgetId, IValidator validator):
			base(widgetGroupId, widgetId, validator)
		{
		}
		
		/// <summary>CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected WidgetGroupWidgetEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}

		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		public CommonEntityBase Parent => this.ParentWidgetEntity;

        [XmlIgnore]
        public string Name
        {
            get { return this.WidgetAsActionButtonEntity.Name; }
            set { this.WidgetAsActionButtonEntity.Name = value; }
        }

        [XmlIgnore]
        public string Text
        {
            get { return this.WidgetAsActionButtonEntity.Text; }
            set { this.WidgetAsActionButtonEntity.Text = value; }
        }

        [XmlIgnore]
        public ActionButtonIconType IconType
        {
            get { return this.WidgetAsActionButtonEntity.IconType; }
            set { this.WidgetAsActionButtonEntity.IconType = value; }
        }

        public WidgetActionButtonEntity WidgetAsActionButtonEntity
        {
            get { return this.ParentWidgetEntity as WidgetActionButtonEntity; }
        }

		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included Code

		#endregion
	}
}
