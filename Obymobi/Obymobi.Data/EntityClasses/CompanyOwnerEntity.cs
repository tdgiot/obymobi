﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    using Dionysos;
    using Obymobi.Enums;
    using System.Xml.Serialization;
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'CompanyOwner'. This class is used for Business Logic or for framework extension code.</summary>
	[Serializable]
	public partial class CompanyOwnerEntity : CompanyOwnerEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
        , INonCompanyRelatedEntity
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		/// <summary>CTor</summary>
		public CompanyOwnerEntity():base()
		{
		}

		/// <summary>CTor</summary>
		/// <param name="companyOwnerId">PK value for CompanyOwner which data should be fetched into this CompanyOwner object</param>
		public CompanyOwnerEntity(System.Int32 companyOwnerId):
			base(companyOwnerId)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="companyOwnerId">PK value for CompanyOwner which data should be fetched into this CompanyOwner object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public CompanyOwnerEntity(System.Int32 companyOwnerId, IPrefetchPath prefetchPathToUse):
			base(companyOwnerId, prefetchPathToUse)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="companyOwnerId">PK value for CompanyOwner which data should be fetched into this CompanyOwner object</param>
		/// <param name="validator">The custom validator object for this CompanyOwnerEntity</param>
		public CompanyOwnerEntity(System.Int32 companyOwnerId, IValidator validator):
			base(companyOwnerId, validator)
		{
		}
		
		/// <summary>CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected CompanyOwnerEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}

		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode

        [XmlIgnore]
        public bool MaintenanceBypassNewValidation = false;

        [DataGridViewColumnVisible]
        [XmlIgnore]
        [EntityFieldDependency((int)CompanyOwnerFieldIndex.Password, (int)CompanyOwnerFieldIndex.IsPasswordEncrypted)]
        public string DecryptedPassword
        {
            get {

                string password = "";

                if (this.IsPasswordEncrypted)
                    password = Dionysos.Security.Cryptography.Cryptographer.DecryptStringUsingRijndael(this.Password);
                else
                    password = this.Password;

                return password;
            }
        }

		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included Code

		#endregion
	}
}
