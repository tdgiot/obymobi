﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    using System.Xml.Serialization;
    // __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'SurveyResult'. This class is used for Business Logic or for framework extension code.</summary>
	[Serializable]
	public partial class SurveyResultEntity : SurveyResultEntityBase
        // __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
        , INonCompanyRelatedEntity
        // __LLBLGENPRO_USER_CODE_REGION_END	
	{
		/// <summary>CTor</summary>
		public SurveyResultEntity():base()
		{
		}

		/// <summary>CTor</summary>
		/// <param name="surveyResultId">PK value for SurveyResult which data should be fetched into this SurveyResult object</param>
		public SurveyResultEntity(System.Int32 surveyResultId):
			base(surveyResultId)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="surveyResultId">PK value for SurveyResult which data should be fetched into this SurveyResult object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public SurveyResultEntity(System.Int32 surveyResultId, IPrefetchPath prefetchPathToUse):
			base(surveyResultId, prefetchPathToUse)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="surveyResultId">PK value for SurveyResult which data should be fetched into this SurveyResult object</param>
		/// <param name="validator">The custom validator object for this SurveyResultEntity</param>
		public SurveyResultEntity(System.Int32 surveyResultId, IValidator validator):
			base(surveyResultId, validator)
		{
		}
		
		/// <summary>CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected SurveyResultEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
            // __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
            // __LLBLGENPRO_USER_CODE_REGION_END
		}

		#region Custom Entity code
		
        // __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode

        public int GetSurveyId()
        {
            int ret = -1;

            if (this.SurveyQuestionEntity != null)
            {
                SurveyPageEntity page = null;

                if (this.SurveyQuestionEntity.SurveyPageEntity != null && this.SurveyQuestionEntity.SurveyPageEntity.SurveyId > 0)
                    page = this.SurveyQuestionEntity.SurveyPageEntity;
                else if (this.SurveyQuestionEntity.ParentSurveyQuestionEntity != null && this.SurveyQuestionEntity.ParentSurveyQuestionEntity.SurveyPageEntity != null)
                    page = this.SurveyQuestionEntity.ParentSurveyQuestionEntity.SurveyPageEntity;

                if (page != null)
                    ret = page.SurveyId;
            }

            return ret;
        }

        [XmlIgnore]
        public string SubmittedAndTableString
        {
            get
            {
                string deliverypoint = "not set";

                if (this.ClientEntity != null)
                {
                    if (this.ClientEntity.LastDeliverypointId.HasValue)
                        deliverypoint = this.ClientEntity.LastDeliverypointEntity.Number;
                }
                
                return String.Format("{0} - {1}", this.SubmittedUTC.ToString(), deliverypoint);
            }
        }

        // __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included Code

		#endregion
	}
}
