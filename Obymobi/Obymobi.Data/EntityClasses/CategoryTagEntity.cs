﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'CategoryTag'. This class is used for Business Logic or for framework extension code.</summary>
	[Serializable]
	public partial class CategoryTagEntity : CategoryTagEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
        , INullableCompanyRelatedChildEntity, ITaggableEntity
	    // __LLBLGENPRO_USER_CODE_REGION_END	
	{
		/// <summary>CTor</summary>
		public CategoryTagEntity():base()
		{
		}

		/// <summary>CTor</summary>
		/// <param name="categoryTagId">PK value for CategoryTag which data should be fetched into this CategoryTag object</param>
		public CategoryTagEntity(System.Int32 categoryTagId):
			base(categoryTagId)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="categoryTagId">PK value for CategoryTag which data should be fetched into this CategoryTag object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public CategoryTagEntity(System.Int32 categoryTagId, IPrefetchPath prefetchPathToUse):
			base(categoryTagId, prefetchPathToUse)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="categoryTagId">PK value for CategoryTag which data should be fetched into this CategoryTag object</param>
		/// <param name="validator">The custom validator object for this CategoryTagEntity</param>
		public CategoryTagEntity(System.Int32 categoryTagId, IValidator validator):
			base(categoryTagId, validator)
		{
		}
		
		/// <summary>CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected CategoryTagEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}

		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode

        public CommonEntityBase Parent => this.CategoryEntity;

		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included Code

		#endregion
	}
}
