﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    using System.Linq;
    using Dionysos;
    using Obymobi.Enums;
    using Obymobi.Logic.Cms;
    using System.Xml.Serialization;
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'Genericcategory'. This class is used for Business Logic or for framework extension code.</summary>
	[Serializable]
	public partial class GenericcategoryEntity : GenericcategoryEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
        , IMediaContainingEntity, INonCompanyRelatedEntity, ICustomTextContainingEntity, IMenuItem, ITimestampEntity
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		/// <summary>CTor</summary>
		public GenericcategoryEntity():base()
		{
		}

		/// <summary>CTor</summary>
		/// <param name="genericcategoryId">PK value for Genericcategory which data should be fetched into this Genericcategory object</param>
		public GenericcategoryEntity(System.Int32 genericcategoryId):
			base(genericcategoryId)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="genericcategoryId">PK value for Genericcategory which data should be fetched into this Genericcategory object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public GenericcategoryEntity(System.Int32 genericcategoryId, IPrefetchPath prefetchPathToUse):
			base(genericcategoryId, prefetchPathToUse)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="genericcategoryId">PK value for Genericcategory which data should be fetched into this Genericcategory object</param>
		/// <param name="validator">The custom validator object for this GenericcategoryEntity</param>
		public GenericcategoryEntity(System.Int32 genericcategoryId, IValidator validator):
			base(genericcategoryId, validator)
		{
		}
		
		/// <summary>CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected GenericcategoryEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}

		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode

        #region Methods

        /// <summary>
        /// Get all id's for the underlying categories + the id of the parent in 1 arraylist (to be used in i.e. PredicatExpression)
        /// </summary>
        /// <param name="parentCategory">Parent Category to search for</param>
        /// <param name="foundIds">Additional Id's you want to include, default null</param>
        /// <returns></returns>
        public ArrayList GetAllUnderlyingGenericccategoryIds(GenericcategoryEntity parentCategory, ArrayList foundIds)
        {
            if (foundIds == null)
            {
                foundIds = new ArrayList();
            }

            for (int i = 0; i < parentCategory.GenericcategoryCollection.Count; i++)
            {
				if (this.GenericcategoryCollection[i].GenericcategoryCollection.Count > 0)
                {
					this.GetAllUnderlyingGenericccategoryIds(parentCategory.GenericcategoryCollection[i], foundIds);
                }
				foundIds.Add(parentCategory.GenericcategoryCollection[i].GenericcategoryId);
            }

            foundIds.Add(parentCategory.GenericcategoryId);

            return foundIds;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Determine if a least one Media entity contains a MediaRatioEntity related to MediaType ProductButton
        /// </summary>
        public bool HasCategoryButton
        {
            get
            {
                bool toReturn = false;
                foreach (var media in this.MediaCollection)
                {
                    if (media.MediaRatioTypeMediaCollection.Any(mr => (mr.MediaType.HasValue && mr.MediaType.Value == (int)MediaType.CategoryButton)) ||
                        media.MediaRatioTypeMediaCollection.Any(mr => (mr.MediaType.HasValue && mr.MediaType.Value == (int)MediaType.GenericcategoryButton)))
                    {
                        toReturn = true;
                        break;
                    }
                }
                return toReturn;
            }
        }

        #endregion

        #region IMenuItem

        [XmlIgnore]
        public string ItemId
        {
            get { return string.Format("{0}-{1}", this.ItemType, this.GenericcategoryId); }
        }

        [XmlIgnore]
        public string ParentItemId
        {
            get { return this.ParentGenericcategoryId.HasValue ? this.GenericcategoryEntity.ItemId : string.Empty; }
        }

        [XmlIgnore]
        public string ItemType
        {
            get { return MenuItemType.Category; }
        }

        [XmlIgnore]
        public string TypeName
        {
            get { return MenuItemType.Category; }
        }

        [XmlIgnore]
        public bool IsLinkedToBrand
        {
            get
            {
                return false;
            }
        }

        #endregion

        // __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included Code

		#endregion
	}
}
