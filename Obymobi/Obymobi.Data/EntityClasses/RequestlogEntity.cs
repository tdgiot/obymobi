﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    // __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'Requestlog'. This class is used for Business Logic or for framework extension code.</summary>
	[Serializable]
	public partial class RequestlogEntity : RequestlogEntityBase
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
        , INonCompanyRelatedEntity
    // __LLBLGENPRO_USER_CODE_REGION_END	
	{
		/// <summary>CTor</summary>
		public RequestlogEntity():base()
		{
		}

		/// <summary>CTor</summary>
		/// <param name="requestlogId">PK value for Requestlog which data should be fetched into this Requestlog object</param>
		public RequestlogEntity(System.Int64 requestlogId):
			base(requestlogId)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="requestlogId">PK value for Requestlog which data should be fetched into this Requestlog object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public RequestlogEntity(System.Int64 requestlogId, IPrefetchPath prefetchPathToUse):
			base(requestlogId, prefetchPathToUse)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="requestlogId">PK value for Requestlog which data should be fetched into this Requestlog object</param>
		/// <param name="validator">The custom validator object for this RequestlogEntity</param>
		public RequestlogEntity(System.Int64 requestlogId, IValidator validator):
			base(requestlogId, validator)
		{
		}
		
		/// <summary>CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected RequestlogEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
            // __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
            // __LLBLGENPRO_USER_CODE_REGION_END
		}

		#region Custom Entity code
		
        // __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode

	    public override bool Save(IPredicate updateRestriction, bool recurse)
	    {
	        CanQueue = true;
            bool RequestlogLogSuccessfulCalls = false;

            try
            {
                RequestlogLogSuccessfulCalls = Dionysos.ConfigurationManager.GetBool(ObymobiConfigConstants.RequestlogLogSuccessfulCalls);
            }
            catch
            { }

            if (!RequestlogLogSuccessfulCalls && this.ResultCode.Equals("100", StringComparison.OrdinalIgnoreCase))
            {
                // don't save successful calls
                return true;
            }
            else
            {
                return base.Save(updateRestriction, recurse);
            }
        }

        // __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included Code

		#endregion
	}
}
