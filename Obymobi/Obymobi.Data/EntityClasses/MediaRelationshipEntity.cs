﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'MediaRelationship'. This class is used for Business Logic or for framework extension code.</summary>
	[Serializable]
	public partial class MediaRelationshipEntity : MediaRelationshipEntityBase
        // __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
        , INullableCompanyRelatedChildEntity
        // __LLBLGENPRO_USER_CODE_REGION_END	
	{
		/// <summary>CTor</summary>
		public MediaRelationshipEntity():base()
		{
		}

		/// <summary>CTor</summary>
		/// <param name="mediaRelationshipId">PK value for MediaRelationship which data should be fetched into this MediaRelationship object</param>
		public MediaRelationshipEntity(System.Int32 mediaRelationshipId):
			base(mediaRelationshipId)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="mediaRelationshipId">PK value for MediaRelationship which data should be fetched into this MediaRelationship object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public MediaRelationshipEntity(System.Int32 mediaRelationshipId, IPrefetchPath prefetchPathToUse):
			base(mediaRelationshipId, prefetchPathToUse)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="mediaRelationshipId">PK value for MediaRelationship which data should be fetched into this MediaRelationship object</param>
		/// <param name="validator">The custom validator object for this MediaRelationshipEntity</param>
		public MediaRelationshipEntity(System.Int32 mediaRelationshipId, IValidator validator):
			base(mediaRelationshipId, validator)
		{
		}
		
		/// <summary>CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected MediaRelationshipEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}

		#region Custom Entity code
		
        // __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode

        public CommonEntityBase Parent => this.ProductEntity;

        // __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included Code

		#endregion
	}
}
