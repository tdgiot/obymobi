﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    using System.Xml.Serialization;
    using Obymobi.Enums;
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'RoomControlSection'. This class is used for Business Logic or for framework extension code.</summary>
	[Serializable]
	public partial class RoomControlSectionEntity : RoomControlSectionEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
        , ICompanyRelatedChildEntityOrNullableCompanyRelatedChildEntity, Obymobi.Logic.RoomControl.IRoomControlItem, ICustomTextContainingEntity, ITimestampEntity
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		/// <summary>CTor</summary>
		public RoomControlSectionEntity():base()
		{
		}

		/// <summary>CTor</summary>
		/// <param name="roomControlSectionId">PK value for RoomControlSection which data should be fetched into this RoomControlSection object</param>
		public RoomControlSectionEntity(System.Int32 roomControlSectionId):
			base(roomControlSectionId)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="roomControlSectionId">PK value for RoomControlSection which data should be fetched into this RoomControlSection object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public RoomControlSectionEntity(System.Int32 roomControlSectionId, IPrefetchPath prefetchPathToUse):
			base(roomControlSectionId, prefetchPathToUse)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="roomControlSectionId">PK value for RoomControlSection which data should be fetched into this RoomControlSection object</param>
		/// <param name="validator">The custom validator object for this RoomControlSectionEntity</param>
		public RoomControlSectionEntity(System.Int32 roomControlSectionId, IValidator validator):
			base(roomControlSectionId, validator)
		{
		}
		
		/// <summary>CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected RoomControlSectionEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}

		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode


        [XmlIgnore]
        public CommonEntityBase Parent
        {
            get { return this.RoomControlAreaEntity; }
        }

        [XmlIgnore]
        public string ItemId
        {
            get
            {
                return string.Format("{0}-{1}", this.ItemType.ToString(), this.RoomControlSectionId);
            }
        }

        [XmlIgnore]
        public string ParentItemId
        {
            get
            {
                return this.RoomControlAreaEntity.ItemId;
            }
        }

        [XmlIgnore]
        public string ItemType
        {
            get
            {
                return RoomControlItemType.Section;
            }
        }

        [XmlIgnore]
        public string NameSystem
        {
            get
            {
                return this.FieldValue1;
            }
            set
            {
                this.FieldValue1 = value;
            }
        }

        [DataGridViewColumnVisible]
        [XmlIgnore]
        public string TypeName
        {
            get
            {
                return this.Type.ToString();
            }
        }

	    public bool ValidatorCreateOrUpdateDefaultSectionLanguage { get; set; }

	    public bool NameChanged { get; set; }

	    // __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included Code

		#endregion
	}
}
