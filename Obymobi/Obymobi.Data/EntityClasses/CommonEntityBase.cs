﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Data;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
#if !CF
using System.Runtime.Serialization;
#endif

namespace Obymobi.Data.EntityClasses
{
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    using System.IO;
    using System.Data.SqlClient;
    using System.Text;
    using System.Threading;
    using System.Threading.Tasks;
    using Dionysos;
    using System.Xml.Serialization;
    // __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Common base class which is the base class for all generated entities which aren't a subtype of another entity.</summary>
	[Serializable]
	public abstract partial class CommonEntityBase : EntityBase
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
        ,ITimestampEntity
    // __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
        // __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers

        /// <summary>
        /// When set to true before save, the Entity will be put in the DatabaseSaveQueue and handeled when the server has time
        /// </summary>
        public bool CanQueue { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public bool IsQueued { get; internal set; }

	    internal IPredicate UpdateRestriction { get; private set; }
        internal bool Recurse { get; private set; }

        public bool IsDeleted { get; internal set; }

        public string LLBLGenProEntityName
        {
            get { return string.Empty; }
        }

        // __LLBLGENPRO_USER_CODE_REGION_END
		#endregion
		
		/// <summary>CTor</summary>
		protected CommonEntityBase()
		{
		}
						
		/// <summary> CTor</summary>
		protected CommonEntityBase(string name):base(name)
		{
		}
		
		/// <summary>Protected CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected CommonEntityBase(SerializationInfo info, StreamingContext context) : base(info, context)
		{
            // __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
            // __LLBLGENPRO_USER_CODE_REGION_END
		}
		
		/// <summary>Gets the inheritance info provider instance of the project this entity instance is located in. </summary>
		/// <returns>ready to use inheritance info provider instance.</returns>
		protected override IInheritanceInfoProvider GetInheritanceInfoProvider()
		{
			return InheritanceInfoProviderSingleton.GetInstance();
		}
		
		/// <summary>Creates a new transaction object</summary>
		/// <param name="levelOfIsolation">The level of isolation.</param>
		/// <param name="name">The name.</param>
		protected override ITransaction CreateTransaction( IsolationLevel levelOfIsolation, string name )
		{
			return new Transaction(levelOfIsolation, name);
		}

		/// <summary>
		/// Creates the ITypeDefaultValue instance used to provide default values for value types which aren't of type nullable(of T)
		/// </summary>
		/// <returns></returns>
		protected override ITypeDefaultValue CreateTypeDefaultValueProvider()
		{
			return new TypeDefaultValue();
		}

		#region Custom Entity code
		
        // __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode        

        /// <remarks>
        /// Override this method to perform an extra check. This method is called by the LLBLGen RuntimeTime framework
        /// in file EntityCore.cs around line 2458 in the SetValue(int, object, bool, bool) method.
        /// 
        /// GetCurrentValue returns null when the value is not set, while GetValue will (most of the time) return 
        /// the default value. 
        /// GetValue is used by all properties in EntityBase classes. So when a string value is fetched and the value
        /// is not set, this will always return an empty string "".
        /// When assiging a value to a string property (can also be an empty string), the internal mechanics of LLBLGen 
        /// will fetch the current value of the property using GetCurrentValue(fieldIndex). As described above 
        /// GetCurrentValue will return null if the value is not set. Because of this the internal which checks if a 
        /// value has been changed will always return true.
        /// 
        /// By overriding this method we do an extra check if the valueToSet is a string. If the string is not empty
        /// but length is zero (0), the current value is checked. When the current value is null and valueToSet length
        /// is zero (0), the SetValue operation is canceled.
        /// 
        /// -----------------------------------------------------------------------------------------------------------
        /// Example:
        /// Table: Order
        /// Column: CustomerFirstName
        /// 
        /// OrderEntityBase.cs
        /// public virtual System.String CustomerFirstname
		/// {
        ///     // The true flag makes it so it returns the default value instead of null
        ///	    get { return (System.String)GetValue((int)OrderFieldIndex.CustomerFirstname, true); }
		///     set	{ SetValue((int)OrderFieldIndex.CustomerFirstname, value, true); }
		/// }
		/// 
		/// Above property will always return an empty string if the value is not set or null. Due to the "true" flag
		/// as second parameter.
		/// The internal SetValue method does the following to retrieve the current value of the property/column:
		/// 
		/// EntityCore.cs
		/// protected bool SetValue(int fieldIndex, object value, bool performDesyncForFKFields, bool checkForRefetch)
		/// {
		///     // ... Pre-check code omitted
		/// 
        ///     object originalValue = _fields.GetCurrentValue(fieldIndex);
        ///     OnSetValue(fieldIndex, valueToSet, out cancel);
		///	    if(cancel)
		///	    {
		///		    return false;
		////	}
		/// 
        ///     // ... More code where check is done if value has been changed
		/// }
		/// 
		/// In above method the value is fetched with GetCurrentValue which will return null when there is not value
		/// assigned to the field. Because of this the "new" value will be "" and the orignalValue will be null.
		/// When the method is called to check if the value has been changed it will do a check whether the old or new
		/// value is null and returns true when only one of the two is null. 
		/// 
		/// This causes the field to be flagged as "changed" while it's actually not.
		/// 
        /// - DK
        /// </remarks>
	    protected override void OnSetValue(int fieldIndex, object valueToSet, out bool cancel)
	    {
	        cancel = false;

	        var v = valueToSet as string;
	        if (v != null && v.Length == 0)
	        {
                object originalValue = Fields.GetCurrentValue(fieldIndex);
                if (originalValue == null)
                {
                    cancel = true;
                }
	        }
	    }

	    private static DateTime lastSaveException = DateTime.MinValue;
        private static readonly object LastSaveExceptionLock = new object();

        public override bool Save(IPredicate updateRestriction, bool recurse)
        {
            if (this.Transaction == null && this.CanQueue && !this.IsQueued)
            {
                this.IsQueued = true;
                this.UpdateRestriction = updateRestriction;
                this.Recurse = recurse;

                DatabaseSaveQueue.Enqueue(this);
                return true;
            }

            try
            {
                return base.Save(updateRestriction, recurse);
            }
            catch (Exception ex)
            {
                bool canRun = true;
                DateTime utcNow = DateTime.UtcNow;
                
                if (this is NetmessageEntity)
                {
                    canRun = false;
                }

                if (canRun)
                {
                    lock (LastSaveExceptionLock)
                    {
                        // Only run this every 10 seconds so we dont put extra pressure on the database
                        if ((utcNow - lastSaveException).TotalSeconds < 10)
                            canRun = false;

                        lastSaveException = utcNow;
                    }
                }

                if (canRun)
                {
                    // Read stacktrace here otherwise it will be the stacktrace of the Task
                    var stacktrace = Environment.StackTrace;

                    Task.Factory.StartNew(() =>
                        {
                            var errorMessage = new StringBuilder();
                            errorMessage.AppendLine("Time (UTC): " + utcNow);
                            errorMessage.AppendLine("Entity: " + this.ToString());
                            errorMessage.AppendLine("IsNew: " + this.IsNew);
                            errorMessage.AppendLine("Transaction: " + ((this.Transaction != null) ? this.Transaction.Name : "None"));
                            errorMessage.AppendLine("Exception: " + ex.Message);
                            errorMessage.AppendLine("Stacktrace:");
                            errorMessage.AppendLine(stacktrace);
                            errorMessage.AppendLine("Stacktrace #2:");
                            errorMessage.AppendLine(ex.ProcessStackTrace(true));

                            try
                            {
                                // Write to disk
                                string path = System.Web.Hosting.HostingEnvironment.MapPath("~/App_Data/Error");
                                Directory.CreateDirectory(path);
                                string fileName = Path.Combine(path, string.Format("LLBLGen-{0}-{1}.txt", TimeStamp.CreateTimeStamp(DateTime.Now), DateTime.Now.Ticks));
                                File.WriteAllText(fileName, errorMessage.ToString());
                            }
                            catch
                            {
                                System.Diagnostics.Debug.WriteLine(errorMessage);
                            }
                        });
                }

                // Rethrow exception
                throw;
            }
        }

	    public new bool Delete()
	    {
	        if (base.Delete())
	        {
                // Mark entity as deleted
                IsDeleted = true;

	            return true;
	        }

	        return false;
	    }

	    //public new ITransaction Transaction
        //{
        //    get
        //    {
        //        return base.Transaction;
        //    }
        //    set
        //    {
        //        base.Transaction = value;
        //    }
        //}

        [XmlIgnore]
        public TimestampUpdaterBase TimestampUpdater { get; set; }

        [XmlIgnore]
        public bool RelevantFieldsChanged { get; set; }

        [XmlIgnore]
        public bool WasNew { get; set; }

        // __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
