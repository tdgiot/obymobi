﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'AdyenPaymentMethod'. This class is used for Business Logic or for framework extension code.</summary>
	[Serializable]
	public partial class AdyenPaymentMethodEntity : AdyenPaymentMethodEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		, ICompanyRelatedChildEntity
	// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		/// <summary>CTor</summary>
		public AdyenPaymentMethodEntity():base()
		{
		}

		/// <summary>CTor</summary>
		/// <param name="adyenPaymentMethodId">PK value for AdyenPaymentMethod which data should be fetched into this AdyenPaymentMethod object</param>
		public AdyenPaymentMethodEntity(System.Int32 adyenPaymentMethodId):
			base(adyenPaymentMethodId)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="adyenPaymentMethodId">PK value for AdyenPaymentMethod which data should be fetched into this AdyenPaymentMethod object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public AdyenPaymentMethodEntity(System.Int32 adyenPaymentMethodId, IPrefetchPath prefetchPathToUse):
			base(adyenPaymentMethodId, prefetchPathToUse)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="adyenPaymentMethodId">PK value for AdyenPaymentMethod which data should be fetched into this AdyenPaymentMethod object</param>
		/// <param name="validator">The custom validator object for this AdyenPaymentMethodEntity</param>
		public AdyenPaymentMethodEntity(System.Int32 adyenPaymentMethodId, IValidator validator):
			base(adyenPaymentMethodId, validator)
		{
		}
		
		/// <summary>CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected AdyenPaymentMethodEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}

		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode

		public CommonEntityBase Parent
		{
			get { return this.PaymentIntegrationConfigurationEntity; }
		}

		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included Code

		#endregion
	}
}
