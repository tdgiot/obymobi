﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    using Obymobi.Enums;
    using System.Xml.Serialization;
    using Dionysos;
    // __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'Device'. This class is used for Business Logic or for framework extension code.</summary>
	[Serializable]
	public partial class DeviceEntity : DeviceEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
        , INonCompanyRelatedEntity
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		/// <summary>CTor</summary>
		public DeviceEntity():base()
		{
		}

		/// <summary>CTor</summary>
		/// <param name="deviceId">PK value for Device which data should be fetched into this Device object</param>
		public DeviceEntity(System.Int32 deviceId):
			base(deviceId)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="deviceId">PK value for Device which data should be fetched into this Device object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public DeviceEntity(System.Int32 deviceId, IPrefetchPath prefetchPathToUse):
			base(deviceId, prefetchPathToUse)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="deviceId">PK value for Device which data should be fetched into this Device object</param>
		/// <param name="validator">The custom validator object for this DeviceEntity</param>
		public DeviceEntity(System.Int32 deviceId, IValidator validator):
			base(deviceId, validator)
		{
		}
		
		/// <summary>CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected DeviceEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}

		#region Custom Entity code
		
        // __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode

        public enum DeviceEntityResult
        { 
            MultipleClientsConnectedToDevice = 200,
            MultipleTerminalsConnectedToDevice = 201,
        }

        #region Properties

        /// <summary>
        /// Gets the text representation of the device type
        /// </summary>
        [DataGridViewColumnVisible]
        [XmlIgnore]
        [EntityFieldDependency((int)DeviceFieldIndex.Type)]
        public string TypeText
        {
            get
            {                
                return this.Type.ToString();
            }        
        }

        /// <summary>
        /// Gets the text representation of the device model
        /// </summary>
        [DataGridViewColumnVisible]
        [XmlIgnore]
        [EntityFieldDependency((int)DeviceFieldIndex.DeviceModel)]
        public string DeviceModelText => DeviceModel != null ? DeviceModel.GetStringValue() : string.Empty;

        /// <summary>
        /// Gets the text representation of the device update status 
        /// </summary>
        [XmlIgnore]
        public string UpdateStatusText
        {
            get
            {
                return this.UpdateStatus.ToString();
            }
        }

        /// <summary>
        /// Retrieves the Client if linked, otherwise null
        /// </summary>
        [XmlIgnore]
        public ClientEntity ClientEntitySingle
        {
            get
            {
                if (this.ClientCollection.Count == 1)
                    return this.ClientCollection[0];
                else if(this.ClientCollection.Count > 0)
                    throw new ObymobiException(DeviceEntityResult.MultipleClientsConnectedToDevice, "DeviceId: '{0}'", this.DeviceId);
                else
                    return null;
            }
        }

        /// <summary>
        /// Retrieves the Terminal if linked, otherwise null
        /// </summary>
        [XmlIgnore]
        public TerminalEntity TerminalEntitySingle
        {
            get
            {
                if (this.TerminalCollection.Count == 1)
                    return this.TerminalCollection[0];
                else if (this.TerminalCollection.Count > 0)
                    throw new ObymobiException(DeviceEntityResult.MultipleTerminalsConnectedToDevice, "DeviceId: '{0}'", this.DeviceId);
                else
                    return null;
            }
        }

        [XmlIgnore]
        public DevicePlatform DevicePlatformAsEnum
        {
            get
            {
                return this.DevicePlatform.ToEnum<DevicePlatform>();
            }
            set
            {
                this.DevicePlatform = (int)value;
            }
        }

        /// <summary>
        /// Retrieves the Company from either the Client or Terminal, and null if none are connected.
        /// </summary>        
        public int? GetCompanyId()
        {
            if (this.ClientEntitySingle != null)
                return this.ClientEntitySingle.CompanyId;
            if (this.TerminalEntitySingle != null)
                return this.TerminalEntitySingle.CompanyId;
            return null;
        }

        [XmlIgnore]
        public bool IsOtoucho
        {
            get
            {
                return this.Identifier.StartsWith("OTOUCHO");
            }
        }

        private MediaType[] mediaTypes = null;
        /// <summary>
        /// Gets an array of the media types applicable for this device
        /// </summary>
        [XmlIgnore]
        public MediaType[] MediaTypes
        {
            get
            {
                if (this.mediaTypes == null)
                    this.mediaTypes = MediaRatioTypes.GetMediaTypeArrayForDeviceTypes(this.Type);
                return this.mediaTypes;
            }
        }

        /// <summary>
        /// Gets the last application version as a <see cref="VersionNumber"/> instance
        /// </summary>
        [XmlIgnore]
        public VersionNumber ApplicationVersionAsVersionNumber
        {
            get
            {
                return new VersionNumber(this.ApplicationVersion);
            }
        }

        /// <summary>
        /// Gets the last OS version as a <see cref="VersionNumber"/> instance
        /// </summary>
        [XmlIgnore]
        public VersionNumber OSVersionAsVersionNumber
        {
            get
            {
                return new VersionNumber(this.OsVersion);
            }
        }

        /// <summary>
        /// Gets the last agent version as a <see cref="VersionNumber"/> instance
        /// </summary>
        [XmlIgnore]
        public VersionNumber AgentVersionAsVersionNumber
        {
            get
            {
                return new VersionNumber(this.AgentVersion);
            }
        }

        /// <summary>
        /// Gets the last SupportTools version as a <see cref="VersionNumber"/> instance
        /// </summary>
        [XmlIgnore]
        public VersionNumber SupportToolsVersionAsVersionNumber
        {
            get
            {
                return new VersionNumber(this.SupportToolsVersion);
            }
        }

        public string DecryptedToken
        {
            get
            {
                return this.Token.IsNullOrWhiteSpace() ? string.Empty : Dionysos.Security.Cryptography.Cryptographer.DecryptUsingCBC(this.Token);
            }
        }

        #endregion

        // __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included Code

		#endregion
	}
}
