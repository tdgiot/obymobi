﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 2.6
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates.NET20
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>
	/// Entity class which represents the entity 'GenericcategoryCategory'. <br/>
	/// This class is used for Business Logic or for framework extension code. 
	/// </summary>
	[Serializable]
	public partial class GenericcategoryCategoryEntity : GenericcategoryCategoryEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Constructors
		/// <summary>
		/// CTor
		/// </summary>
		public GenericcategoryCategoryEntity():base()
		{
		}

	
		/// <summary>
		/// CTor
		/// </summary>
		/// <param name="genericcategoryCategoryId">PK value for GenericcategoryCategory which data should be fetched into this GenericcategoryCategory object</param>
		public GenericcategoryCategoryEntity(System.Int32 genericcategoryCategoryId):
			base(genericcategoryCategoryId)
		{
		}


		/// <summary>
		/// CTor
		/// </summary>
		/// <param name="genericcategoryCategoryId">PK value for GenericcategoryCategory which data should be fetched into this GenericcategoryCategory object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public GenericcategoryCategoryEntity(System.Int32 genericcategoryCategoryId, IPrefetchPath prefetchPathToUse):
			base(genericcategoryCategoryId, prefetchPathToUse)
		{
		}


		/// <summary>
		/// CTor
		/// </summary>
		/// <param name="genericcategoryCategoryId">PK value for GenericcategoryCategory which data should be fetched into this GenericcategoryCategory object</param>
		/// <param name="validator">The custom validator object for this GenericcategoryCategoryEntity</param>
		public GenericcategoryCategoryEntity(System.Int32 genericcategoryCategoryId, IValidator validator):
			base(genericcategoryCategoryId, validator)
		{
		}
	
		
		/// <summary>
		/// Private CTor for deserialization
		/// </summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected GenericcategoryCategoryEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}
		#endregion

		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included Code

		#endregion
	}
}
