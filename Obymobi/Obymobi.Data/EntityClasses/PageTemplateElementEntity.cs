﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    using System.Xml.Serialization;
    using Dionysos;
    using Obymobi.Logic.Cms;
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'PageTemplateElement'. This class is used for Business Logic or for framework extension code.</summary>
	[Serializable]
	public partial class PageTemplateElementEntity : PageTemplateElementEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
        , Obymobi.Logic.Cms.IPageTypeElementData, INonCompanyRelatedEntity, IMediaContainingEntity, ITimestampEntity
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		/// <summary>CTor</summary>
		public PageTemplateElementEntity():base()
		{
		}

		/// <summary>CTor</summary>
		/// <param name="pageTemplateElementId">PK value for PageTemplateElement which data should be fetched into this PageTemplateElement object</param>
		public PageTemplateElementEntity(System.Int32 pageTemplateElementId):
			base(pageTemplateElementId)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="pageTemplateElementId">PK value for PageTemplateElement which data should be fetched into this PageTemplateElement object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public PageTemplateElementEntity(System.Int32 pageTemplateElementId, IPrefetchPath prefetchPathToUse):
			base(pageTemplateElementId, prefetchPathToUse)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="pageTemplateElementId">PK value for PageTemplateElement which data should be fetched into this PageTemplateElement object</param>
		/// <param name="validator">The custom validator object for this PageTemplateElementEntity</param>
		public PageTemplateElementEntity(System.Int32 pageTemplateElementId, IValidator validator):
			base(pageTemplateElementId, validator)
		{
		}
		
		/// <summary>CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected PageTemplateElementEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}

		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
        /// <summary>
        /// 1:1 property for PageTemplateElementId, needed for IPageTypeElementData interface
        /// </summary>
        [XmlIgnore]
        public int PrimaryId
        {
            get
            {
                return base.PageTemplateElementId;
            }
            set
            {
                base.PageTemplateElementId = value;
            }
        }

        [DataGridViewColumnVisibleAttribute]
        [XmlIgnore]
        public string SiteNamePageNamePageElementLanguage
        {
            get
            {
                string pageElementName = PageTypeHelper.GetPageElementName(this.PageTemplateEntity.PageType.ToEnum<PageType>(), this.SystemName);
                string toReturn = "{0} - {1}: {2}".FormatSafe(this.PageTemplateEntity.SiteTemplateEntity.Name, this.PageTemplateEntity.Name, pageElementName);
                if (!this.CultureCode.IsNullOrWhiteSpace())
                    toReturn += " ({0})".FormatSafe(this.CultureCode);
                return toReturn;
            }
        }

	    public bool HasMedia
	    {
	        get
	        {
	            return this.MediaCollection.Count > 0;
	        }
	        set { }
	    }

	    // __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included Code

		#endregion
	}
}
