﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    using Obymobi.Enums;
    // __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'WidgetActionBanner'. This class is used for Business Logic or for framework extension code.</summary>
	[Serializable]
	public partial class WidgetActionBannerEntity : WidgetActionBannerEntityBase
        // __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
        , ICompanyRelatedChildEntity, ICustomTextContainingEntity, IMediaContainingEntity
        // __LLBLGENPRO_USER_CODE_REGION_END	
	{
		/// <summary>CTor</summary>
		public WidgetActionBannerEntity():base()
		{
		}

		/// <summary>CTor</summary>
		/// <param name="widgetId">PK value for WidgetActionBanner which data should be fetched into this WidgetActionBanner object</param>
		public WidgetActionBannerEntity(System.Int32 widgetId):
			base(widgetId)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="widgetId">PK value for WidgetActionBanner which data should be fetched into this WidgetActionBanner object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public WidgetActionBannerEntity(System.Int32 widgetId, IPrefetchPath prefetchPathToUse):
			base(widgetId, prefetchPathToUse)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="widgetId">PK value for WidgetActionBanner which data should be fetched into this WidgetActionBanner object</param>
		/// <param name="validator">The custom validator object for this WidgetActionBannerEntity</param>
		public WidgetActionBannerEntity(System.Int32 widgetId, IValidator validator):
			base(widgetId, validator)
		{
		}
		
		/// <summary>CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected WidgetActionBannerEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}

		#region Custom Entity code
		
        // __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode		

        public override ApplessWidgetType Type { get { return ApplessWidgetType.ActionBanner; } }

        // __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included Code

		#endregion
	}
}
