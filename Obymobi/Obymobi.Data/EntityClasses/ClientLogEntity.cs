﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    using Obymobi.Enums;
    using Dionysos.Data;
    using System.Xml.Serialization;
    // __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'ClientLog'. This class is used for Business Logic or for framework extension code.</summary>
	[Serializable]
	public partial class ClientLogEntity : ClientLogEntityBase
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
    , INullableCompanyRelatedChildEntity
    // __LLBLGENPRO_USER_CODE_REGION_END	
	{
		/// <summary>CTor</summary>
		public ClientLogEntity():base()
		{
		}

		/// <summary>CTor</summary>
		/// <param name="clientLogId">PK value for ClientLog which data should be fetched into this ClientLog object</param>
		public ClientLogEntity(System.Int32 clientLogId):
			base(clientLogId)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="clientLogId">PK value for ClientLog which data should be fetched into this ClientLog object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public ClientLogEntity(System.Int32 clientLogId, IPrefetchPath prefetchPathToUse):
			base(clientLogId, prefetchPathToUse)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="clientLogId">PK value for ClientLog which data should be fetched into this ClientLog object</param>
		/// <param name="validator">The custom validator object for this ClientLogEntity</param>
		public ClientLogEntity(System.Int32 clientLogId, IValidator validator):
			base(clientLogId, validator)
		{
		}
		
		/// <summary>CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected ClientLogEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
            // __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
            // __LLBLGENPRO_USER_CODE_REGION_END
		}

		#region Custom Entity code
		
        // __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode

        [XmlIgnore]
        public CommonEntityBase Parent
        {
            get 
            {
                if (this.ClientId.HasValue)
                    return this.ClientEntity;
                else
                    return null;
            }
        }

        [XmlIgnore]
        public ClientLogType TypeEnum
        {
            get
            {
                return this.Type.ToEnum<ClientLogType>();
            }
            set
            {
                this.Type = (int)value;
            }
        }

        /// <summary>
        /// Gets or sets from operation mode enum.
        /// </summary>
        /// <value>
        /// From operation mode enum.
        /// </value>
        [XmlIgnore]
        public ClientOperationMode FromOperationModeEnum
        {
            get
            {
                if (this.FromOperationMode.HasValue)
                    return this.FromOperationMode.Value.ToEnum<ClientOperationMode>();
                else
                    return ClientOperationMode.Unknown;
            }
            set
            {
                this.FromOperationMode = (int)value;
            }
        }

        [XmlIgnore]
        public ClientOperationMode ToOperationModeEnum
        {
            get
            {
                if (this.ToOperationMode.HasValue)
                    return this.ToOperationMode.Value.ToEnum<ClientOperationMode>();
                else
                    return ClientOperationMode.Unknown;
            }
            set
            {
                this.ToOperationMode = (int)value;
            }
        }

        [DataGridViewColumnVisible]
        [XmlIgnore]
        public string TypeName
        {
            get { return this.Type.ToEnum<ClientLogType>().ToString(); }
        }

        [DataGridViewColumnVisible]
        [XmlIgnore]
        public string FromStatusName
        {
            get
            {
                if(this.FromStatus.HasValue)
                    return this.FromStatus.Value.ToEnum<ClientStatusCode>().ToString();

                return ClientStatusCode.Unknown.ToString();
            }
        }

        [DataGridViewColumnVisible]
        [XmlIgnore]
        public string ToStatusName
        {
            get
            {
                if (this.ToStatus.HasValue)
                    return this.ToStatus.Value.ToEnum<ClientStatusCode>().ToString();

                return ClientStatusCode.Unknown.ToString();
            }
        }

        [DataGridViewColumnVisible]
        [XmlIgnore]
        public string FromOperationModeName
        {
            get
            {
                if (this.FromOperationMode.HasValue)
                    return this.FromOperationMode.Value.ToEnum<ClientOperationMode>().ToString();

                return ClientOperationMode.Unknown.ToString();
            }
        }

        [DataGridViewColumnVisible]
        [XmlIgnore]
        public string ToOperationModeName
        {
            get
            {
                if (this.ToOperationMode.HasValue)
                    return this.ToOperationMode.Value.ToEnum<ClientOperationMode>().ToString();

                return ClientOperationMode.Unknown.ToString();
            }
        }

        // __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included Code

		#endregion
	}
}
