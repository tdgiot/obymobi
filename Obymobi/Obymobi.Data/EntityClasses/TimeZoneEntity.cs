﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'TimeZone'. This class is used for Business Logic or for framework extension code.</summary>
	[Serializable]
	public partial class TimeZoneEntity : TimeZoneEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
        , INonCompanyRelatedEntity
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		/// <summary>CTor</summary>
		public TimeZoneEntity():base()
		{
		}

		/// <summary>CTor</summary>
		/// <param name="timeZoneId">PK value for TimeZone which data should be fetched into this TimeZone object</param>
		public TimeZoneEntity(System.Int32 timeZoneId):
			base(timeZoneId)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="timeZoneId">PK value for TimeZone which data should be fetched into this TimeZone object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public TimeZoneEntity(System.Int32 timeZoneId, IPrefetchPath prefetchPathToUse):
			base(timeZoneId, prefetchPathToUse)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="timeZoneId">PK value for TimeZone which data should be fetched into this TimeZone object</param>
		/// <param name="validator">The custom validator object for this TimeZoneEntity</param>
		public TimeZoneEntity(System.Int32 timeZoneId, IValidator validator):
			base(timeZoneId, validator)
		{
		}
		
		/// <summary>CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected TimeZoneEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}

		#region Custom Entity code
		
        // __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode

        private TimeZoneInfo timeZoneInfo = null;
        public TimeZoneInfo GetTimeZoneInfo()
        {
            //if(this.timeZoneInfo == null)
            //    this.timeZoneInfo = GetTimeZoneInfo(this.NameDotNet);

            return this.timeZoneInfo;
        }

        // __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included Code

		#endregion
	}
}
