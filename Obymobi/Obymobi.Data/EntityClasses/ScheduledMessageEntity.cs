﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    using Dionysos;
    // __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'ScheduledMessage'. This class is used for Business Logic or for framework extension code.</summary>
	[Serializable]
	public partial class ScheduledMessageEntity : ScheduledMessageEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
        , ICompanyRelatedEntity, ICustomTextContainingEntity, ITimestampEntity
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		/// <summary>CTor</summary>
		public ScheduledMessageEntity():base()
		{
		}

		/// <summary>CTor</summary>
		/// <param name="scheduledMessageId">PK value for ScheduledMessage which data should be fetched into this ScheduledMessage object</param>
		public ScheduledMessageEntity(System.Int32 scheduledMessageId):
			base(scheduledMessageId)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="scheduledMessageId">PK value for ScheduledMessage which data should be fetched into this ScheduledMessage object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public ScheduledMessageEntity(System.Int32 scheduledMessageId, IPrefetchPath prefetchPathToUse):
			base(scheduledMessageId, prefetchPathToUse)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="scheduledMessageId">PK value for ScheduledMessage which data should be fetched into this ScheduledMessage object</param>
		/// <param name="validator">The custom validator object for this ScheduledMessageEntity</param>
		public ScheduledMessageEntity(System.Int32 scheduledMessageId, IValidator validator):
			base(scheduledMessageId, validator)
		{
		}
		
		/// <summary>CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected ScheduledMessageEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}

		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode

        /// <summary>
        /// Gets the friendly name or when it is null returns the title.
        /// </summary>
        public string MessageName
        {
            get
            {
                return this.FriendlyName.IsNullOrWhiteSpace() ? this.Title : this.FriendlyName;
            }
        }

		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included Code

		#endregion
	}
}
