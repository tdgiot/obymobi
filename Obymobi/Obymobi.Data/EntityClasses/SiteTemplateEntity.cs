﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    using Obymobi.Logic.Cms;
    using System.Xml.Serialization;
    using System.Collections.Generic;
    using Obymobi.Enums;
    using SD.LLBLGen.Pro.ORMSupportClasses;
    using Obymobi.Data.HelperClasses;
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'SiteTemplate'. This class is used for Business Logic or for framework extension code.</summary>
	[Serializable]
	public partial class SiteTemplateEntity : SiteTemplateEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
        , ISite, INonCompanyRelatedEntity, ICustomTextContainingEntity
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		/// <summary>CTor</summary>
		public SiteTemplateEntity():base()
		{
		}

		/// <summary>CTor</summary>
		/// <param name="siteTemplateId">PK value for SiteTemplate which data should be fetched into this SiteTemplate object</param>
		public SiteTemplateEntity(System.Int32 siteTemplateId):
			base(siteTemplateId)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="siteTemplateId">PK value for SiteTemplate which data should be fetched into this SiteTemplate object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public SiteTemplateEntity(System.Int32 siteTemplateId, IPrefetchPath prefetchPathToUse):
			base(siteTemplateId, prefetchPathToUse)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="siteTemplateId">PK value for SiteTemplate which data should be fetched into this SiteTemplate object</param>
		/// <param name="validator">The custom validator object for this SiteTemplateEntity</param>
		public SiteTemplateEntity(System.Int32 siteTemplateId, IValidator validator):
			base(siteTemplateId, validator)
		{
		}
		
		/// <summary>CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected SiteTemplateEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}

		#region Custom Entity code
		
        // __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode

        #region Generic handling for Sites/SiteTemplates

        public int PrimaryId
        {
            get { return this.SiteTemplateId; }
            set { this.SiteTemplateId = value; }
        }

        [XmlIgnore]
        public SiteType SiteTypeAsEnum
        {
            get
            {
                return this.SiteType.ToEnum<SiteType>();
            }
            set
            {
                this.SiteType = (int)value;
            }
        }

        public List<IPage> IPageCollection
        {
            get
            {
                var toReturn = new List<IPage>();
                toReturn.AddRange(base.PageTemplateCollection);
                return toReturn;
            }
        }

        #endregion

        // __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included Code

		#endregion
	}
}
