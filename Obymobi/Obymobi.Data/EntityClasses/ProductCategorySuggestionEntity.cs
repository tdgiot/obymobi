﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    using System.Xml.Serialization;
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'ProductCategorySuggestion'. This class is used for Business Logic or for framework extension code.</summary>
	[Serializable]
	public partial class ProductCategorySuggestionEntity : ProductCategorySuggestionEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
        , ICompanyRelatedChildEntity, ITimestampEntity
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		/// <summary>CTor</summary>
		public ProductCategorySuggestionEntity():base()
		{
		}

		/// <summary>CTor</summary>
		/// <param name="productCategorySuggestionId">PK value for ProductCategorySuggestion which data should be fetched into this ProductCategorySuggestion object</param>
		public ProductCategorySuggestionEntity(System.Int32 productCategorySuggestionId):
			base(productCategorySuggestionId)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="productCategorySuggestionId">PK value for ProductCategorySuggestion which data should be fetched into this ProductCategorySuggestion object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public ProductCategorySuggestionEntity(System.Int32 productCategorySuggestionId, IPrefetchPath prefetchPathToUse):
			base(productCategorySuggestionId, prefetchPathToUse)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="productCategorySuggestionId">PK value for ProductCategorySuggestion which data should be fetched into this ProductCategorySuggestion object</param>
		/// <param name="validator">The custom validator object for this ProductCategorySuggestionEntity</param>
		public ProductCategorySuggestionEntity(System.Int32 productCategorySuggestionId, IValidator validator):
			base(productCategorySuggestionId, validator)
		{
		}
		
		/// <summary>CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected ProductCategorySuggestionEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}

		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode

        [XmlIgnore]
        public CommonEntityBase Parent
        {
            get { return this.ProductCategoryEntity; }
        }

		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included Code

		#endregion
	}
}
