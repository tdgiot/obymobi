﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    using System.Xml.Serialization;
    using System.Collections.Generic;    
    using System.Linq;
    using Dionysos;
    using Obymobi.Logic.Cms;
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'PageTemplate'. This class is used for Business Logic or for framework extension code.</summary>
	[Serializable]
	public partial class PageTemplateEntity : PageTemplateEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
        , IPage, INonCompanyRelatedEntity, IMediaContainingEntity, ICustomTextContainingEntity, ITimestampEntity
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		/// <summary>CTor</summary>
		public PageTemplateEntity():base()
		{
		}

		/// <summary>CTor</summary>
		/// <param name="pageTemplateId">PK value for PageTemplate which data should be fetched into this PageTemplate object</param>
		public PageTemplateEntity(System.Int32 pageTemplateId):
			base(pageTemplateId)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="pageTemplateId">PK value for PageTemplate which data should be fetched into this PageTemplate object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public PageTemplateEntity(System.Int32 pageTemplateId, IPrefetchPath prefetchPathToUse):
			base(pageTemplateId, prefetchPathToUse)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="pageTemplateId">PK value for PageTemplate which data should be fetched into this PageTemplate object</param>
		/// <param name="validator">The custom validator object for this PageTemplateEntity</param>
		public PageTemplateEntity(System.Int32 pageTemplateId, IValidator validator):
			base(pageTemplateId, validator)
		{
		}
		
		/// <summary>CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected PageTemplateEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}

		#region Custom Entity code
		
        // __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode

        #region Generic handling for Pages/PageTemplates
        
        /// <summary>
        /// 1:1 property for PageId, needed for IPage interface
        /// </summary>
        [XmlIgnore]
        public int PrimaryId
        {
            get
            {
                return base.PageTemplateId;
            }
            set
            {
                base.PageTemplateId = value;
            }
        }

        /// <summary>
        /// 1:1 property for ParentPageTemplateId, needed for IPage interface
        /// </summary>
        [XmlIgnore]
        public int? ParentPageId
        {
            get
            {
                return base.ParentPageTemplateId;
            }
            set
            {
                base.ParentPageTemplateId = value;
            }
        }

        [XmlIgnore]
        public PageType PageTypeAsEnum
        {
            get
            {
                return this.PageType.ToEnum<PageType>();
            }
            set
            {
                this.PageType = (int)value;
            }
        }

        [CLSCompliant(false)]
        public List<IPageTypeElementData> GetPageTypeDataSource(string cultureCode)
        {
            List<IPageTypeElementData> toReturn = null;

            if (this.PageTemplateElementCollection.Count > 0)
            {
                IEnumerable<PageTemplateElementEntity> elements;

                if (cultureCode.IsNullOrWhiteSpace())
                    elements = this.PageTemplateElementCollection.Where(x => x.CultureCode.IsNullOrWhiteSpace());
                else
                    elements = this.PageTemplateElementCollection.Where(x => x.CultureCode.Equals(cultureCode, StringComparison.InvariantCultureIgnoreCase));
                
                toReturn = elements.ToList<IPageTypeElementData>();
            }
            else
            {
                toReturn = new List<IPageTypeElementData>();
            }

            return toReturn;
        }

	    public IPage PageTemplate
	    {
	        get { return null; }
	    }

	    public List<IPage> IPageCollection
        {
            get { return base.PageTemplateCollection.ToList<IPage>(); }
        }

        public List<IPageTypeElementData> IPageElementCollection
        {
            get
            {
                return base.PageTemplateElementCollection.ToList<IPageTypeElementData>();
            }
        }

        #endregion

        [DataGridViewColumnVisibleAttribute]
        [XmlIgnore]
        public string SiteNamePageTemplateName
        {
            get
            {
                return string.Format("{0} - {1}", this.SiteTemplateEntity.Name, this.Name);
            }
        }

	    public bool ValidatorCreateOrUpdatePageTemplateLanguage { get; set; }

	    public bool NameChanged { get; set; }

	    // __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included Code

		#endregion
	}
}
