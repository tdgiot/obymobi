﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    using Obymobi.Enums;
    using System.Xml.Serialization;
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'ClientState'. This class is used for Business Logic or for framework extension code.</summary>
	[Serializable]
	public partial class ClientStateEntity : ClientStateEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
        ,ICompanyRelatedChildEntity
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		/// <summary>CTor</summary>
		public ClientStateEntity():base()
		{
		}

		/// <summary>CTor</summary>
		/// <param name="clientStateId">PK value for ClientState which data should be fetched into this ClientState object</param>
		public ClientStateEntity(System.Int32 clientStateId):
			base(clientStateId)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="clientStateId">PK value for ClientState which data should be fetched into this ClientState object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public ClientStateEntity(System.Int32 clientStateId, IPrefetchPath prefetchPathToUse):
			base(clientStateId, prefetchPathToUse)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="clientStateId">PK value for ClientState which data should be fetched into this ClientState object</param>
		/// <param name="validator">The custom validator object for this ClientStateEntity</param>
		public ClientStateEntity(System.Int32 clientStateId, IValidator validator):
			base(clientStateId, validator)
		{
		}
		
		/// <summary>CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected ClientStateEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}

		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode

        [XmlIgnore]
        public CommonEntityBase Parent
        {
            get { return this.ClientEntity; }
        }

        public ClientOperationMode OperationModeAsEnum
        {
            get
            {
                ClientOperationMode operationMode = ClientOperationMode.Unknown;
                if (this.OperationMode.HasValue)
                    operationMode = (ClientOperationMode)this.OperationMode.Value;
                return operationMode;
            }
            set
            {
                this.OperationMode = (int)value;
            }
        }

        public TimeSpan TimeSpan
        {
            get
            {
                return TimeSpan.FromTicks(this.TimeSpanTicks);
            }
            set
            {
                this.TimeSpanTicks = value.Ticks;
            }
        }

        public override string ToString()
        {
            string str = string.Empty;

            str += string.Format("Online: '{0}', ", this.Online);
            str += string.Format("Operation mode: '{0}', ", this.OperationModeAsEnum.ToString());
            if (this.LastBatteryLevel.HasValue)
                str += string.Format("Last battery level: '{0}', ", this.LastBatteryLevel.Value);
            str += string.Format("Last is charging: '{0}', ", this.LastIsCharging);

            TimeSpan timespan = this.TimeSpan;

            if (this.Online)
                str += string.Format("Downtime: '{0} day{1} {2} hour{3} {4} minute{5} {6} second{7}'", timespan.Days, (timespan.Days == 1 ? string.Empty : "s"), timespan.Hours, (timespan.Hours == 1 ? string.Empty : "s"), timespan.Minutes, (timespan.Minutes == 1 ? string.Empty : "s"), timespan.Seconds, (timespan.Seconds == 1 ? string.Empty : "s"));
            else
                str += string.Format("Uptime: '{0} day{1} {2} hour{3} {4} minute{5} {6} second{7}'", timespan.Days, (timespan.Days == 1 ? string.Empty : "s"), timespan.Hours, (timespan.Hours == 1 ? string.Empty : "s"), timespan.Minutes, (timespan.Minutes == 1 ? string.Empty : "s"), timespan.Seconds, (timespan.Seconds == 1 ? string.Empty : "s"));

            return str;
        }

		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included Code

		#endregion
	}
}
