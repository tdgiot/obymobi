﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'ProductCategoryTag'. This class is used for Business Logic or for framework extension code.</summary>
	[Serializable]
	public partial class ProductCategoryTagEntity : ProductCategoryTagEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		, INullableCompanyRelatedChildEntity, ITaggableEntity
    	// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		/// <summary>CTor</summary>
		public ProductCategoryTagEntity():base()
		{
		}

		/// <summary>CTor</summary>
		/// <param name="productCategoryTagId">PK value for ProductCategoryTag which data should be fetched into this ProductCategoryTag object</param>
		public ProductCategoryTagEntity(System.Int32 productCategoryTagId):
			base(productCategoryTagId)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="productCategoryTagId">PK value for ProductCategoryTag which data should be fetched into this ProductCategoryTag object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public ProductCategoryTagEntity(System.Int32 productCategoryTagId, IPrefetchPath prefetchPathToUse):
			base(productCategoryTagId, prefetchPathToUse)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="productCategoryTagId">PK value for ProductCategoryTag which data should be fetched into this ProductCategoryTag object</param>
		/// <param name="validator">The custom validator object for this ProductCategoryTagEntity</param>
		public ProductCategoryTagEntity(System.Int32 productCategoryTagId, IValidator validator):
			base(productCategoryTagId, validator)
		{
		}
		
		/// <summary>CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected ProductCategoryTagEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}

		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode

        public CommonEntityBase Parent => this.ProductCategoryEntity;

		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included Code

		#endregion
	}
}
