﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    using Obymobi.Enums;
    // __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'LandingPageWidget'. This class is used for Business Logic or for framework extension code.</summary>
	[Serializable]
	public partial class LandingPageWidgetEntity : LandingPageWidgetEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		, ICompanyRelatedChildEntity
	// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		/// <summary>CTor</summary>
		public LandingPageWidgetEntity():base()
		{
		}

		/// <summary>CTor</summary>
		/// <param name="landingPageId">PK value for LandingPageWidget which data should be fetched into this LandingPageWidget object</param>
		/// <param name="widgetId">PK value for LandingPageWidget which data should be fetched into this LandingPageWidget object</param>
		public LandingPageWidgetEntity(System.Int32 landingPageId, System.Int32 widgetId):
			base(landingPageId, widgetId)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="landingPageId">PK value for LandingPageWidget which data should be fetched into this LandingPageWidget object</param>
		/// <param name="widgetId">PK value for LandingPageWidget which data should be fetched into this LandingPageWidget object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public LandingPageWidgetEntity(System.Int32 landingPageId, System.Int32 widgetId, IPrefetchPath prefetchPathToUse):
			base(landingPageId, widgetId, prefetchPathToUse)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="landingPageId">PK value for LandingPageWidget which data should be fetched into this LandingPageWidget object</param>
		/// <param name="widgetId">PK value for LandingPageWidget which data should be fetched into this LandingPageWidget object</param>
		/// <param name="validator">The custom validator object for this LandingPageWidgetEntity</param>
		public LandingPageWidgetEntity(System.Int32 landingPageId, System.Int32 widgetId, IValidator validator):
			base(landingPageId, widgetId, validator)
		{
		}
		
		/// <summary>CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected LandingPageWidgetEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}

		#region Custom Entity code
		
        // __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode

        public CommonEntityBase Parent => this.LandingPageEntity;

        public string Name
        {
            get { return this.WidgetEntity.Name; }
            set { this.WidgetEntity.Name = value; }
        }

        public ApplessWidgetType Type
        {
            get { return this.WidgetEntity.Type; }            
        }

        // __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included Code

		#endregion
	}
}
