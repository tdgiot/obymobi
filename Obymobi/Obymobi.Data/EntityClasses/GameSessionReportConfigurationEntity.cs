﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    using System.Xml.Serialization;
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'GameSessionReportConfiguration'. This class is used for Business Logic or for framework extension code.</summary>
	[Serializable]
	public partial class GameSessionReportConfigurationEntity : GameSessionReportConfigurationEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
        ,ICompanyRelatedEntity
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		/// <summary>CTor</summary>
		public GameSessionReportConfigurationEntity():base()
		{
		}

		/// <summary>CTor</summary>
		/// <param name="gameSessionReportConfigurationId">PK value for GameSessionReportConfiguration which data should be fetched into this GameSessionReportConfiguration object</param>
		public GameSessionReportConfigurationEntity(System.Int32 gameSessionReportConfigurationId):
			base(gameSessionReportConfigurationId)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="gameSessionReportConfigurationId">PK value for GameSessionReportConfiguration which data should be fetched into this GameSessionReportConfiguration object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public GameSessionReportConfigurationEntity(System.Int32 gameSessionReportConfigurationId, IPrefetchPath prefetchPathToUse):
			base(gameSessionReportConfigurationId, prefetchPathToUse)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="gameSessionReportConfigurationId">PK value for GameSessionReportConfiguration which data should be fetched into this GameSessionReportConfiguration object</param>
		/// <param name="validator">The custom validator object for this GameSessionReportConfigurationEntity</param>
		public GameSessionReportConfigurationEntity(System.Int32 gameSessionReportConfigurationId, IValidator validator):
			base(gameSessionReportConfigurationId, validator)
		{
		}
		
		/// <summary>CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected GameSessionReportConfigurationEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}

		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode

        [XmlIgnore]
        [DataGridViewColumnVisible]
        [EntityFieldDependency((int)GameSessionReportConfigurationFieldIndex.RecurrenceType, (int)GameSessionReportConfigurationFieldIndex.Active)]
        public string RecurrenceTypeText
        {
            get
            {
                return this.RecurrenceType.ToString();
            }
        }

		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included Code

		#endregion
	}
}
