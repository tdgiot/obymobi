﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    using System.Xml.Serialization;
    using Dionysos;
    using System.Collections.Generic;
    using System.Linq;
    // __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'ScheduledCommandTask'. This class is used for Business Logic or for framework extension code.</summary>
	[Serializable]
	public partial class ScheduledCommandTaskEntity : ScheduledCommandTaskEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
        ,ICompanyRelatedEntity
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		/// <summary>CTor</summary>
		public ScheduledCommandTaskEntity():base()
		{
		}

		/// <summary>CTor</summary>
		/// <param name="scheduledCommandTaskId">PK value for ScheduledCommandTask which data should be fetched into this ScheduledCommandTask object</param>
		public ScheduledCommandTaskEntity(System.Int32 scheduledCommandTaskId):
			base(scheduledCommandTaskId)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="scheduledCommandTaskId">PK value for ScheduledCommandTask which data should be fetched into this ScheduledCommandTask object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public ScheduledCommandTaskEntity(System.Int32 scheduledCommandTaskId, IPrefetchPath prefetchPathToUse):
			base(scheduledCommandTaskId, prefetchPathToUse)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="scheduledCommandTaskId">PK value for ScheduledCommandTask which data should be fetched into this ScheduledCommandTask object</param>
		/// <param name="validator">The custom validator object for this ScheduledCommandTaskEntity</param>
		public ScheduledCommandTaskEntity(System.Int32 scheduledCommandTaskId, IValidator validator):
			base(scheduledCommandTaskId, validator)
		{
		}
		
		/// <summary>CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected ScheduledCommandTaskEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}

		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode

        [DataGridViewColumnVisible]
        [EntityFieldDependency((int)ScheduledCommandTaskFieldIndex.Status)]
        public string StatusText
        {
            get
            {
                return this.Status.ToString();
            }
        }

        [DataGridViewColumnVisible]
        [EntityFieldDependency((int)ScheduledCommandTaskFieldIndex.FinishedCount, (int)ScheduledCommandTaskFieldIndex.CommandCount)]
        public string ProgressText
        {
            get
            {
                return string.Format("{0} of {1} ({2}%)", this.FinishedCount, this.CommandCount, Dionysos.Math.Percentage(this.FinishedCount, this.CommandCount));
            }
        }

        [DataGridViewColumnVisible]
        [EntityFieldDependency((int)ScheduledCommandTaskFieldIndex.StartUTC)]
        public string StartText
        {
            get
            {
                return string.Format("{0} ({1})", GetLocalizedDateTime(this.StartUTC), DateTimeUtil.GetSpanTextBetweenDateTimeAndUtcNow(this.StartUTC, DateTime.UtcNow));
            }
        }

        [DataGridViewColumnVisible]
        [EntityFieldDependency((int)ScheduledCommandTaskFieldIndex.ExpiresUTC)]
        public string ExpiresText
        {
            get
            {
                return string.Format("{0} ({1})", GetLocalizedDateTime(this.ExpiresUTC), DateTimeUtil.GetSpanTextBetweenDateTimeAndUtcNow(this.ExpiresUTC, DateTime.UtcNow));
            }
        }

        [DataGridViewColumnVisible]
        [EntityFieldDependency((int)ScheduledCommandTaskFieldIndex.CompletedUTC)]
        public string CompletedText
        {
            get
            {
                if (!this.CompletedUTC.HasValue)
                    return string.Empty;

                return string.Format("{0} ({1})", GetLocalizedDateTime(this.CompletedUTC.Value), DateTimeUtil.GetSpanTextBetweenDateTimeAndUtcNow(this.CompletedUTC.Value, DateTime.UtcNow));
            }
        }

        [DataGridViewColumnVisible]
        [XmlIgnore]
        public string DistinctCommands
        {
            get
            {
                IEnumerable<object> commands = this.ScheduledCommandCollection.Select(x => x.CommandText).Distinct();
                return StringUtil.CombineWithCommaSpace(commands.ToArray());
            }
        }

        /// <summary>
        /// Gets current time for company timezone
        /// </summary>
        /// <returns></returns>
	    private DateTime GetLocalizedDateTime(DateTime? dateTime = null)
        {
            if (!this.CompanyEntity.TimeZoneOlsonId.IsNullOrWhiteSpace())
            {
                return dateTime.GetValueOrDefault(DateTime.UtcNow).UtcToLocalTime(this.CompanyEntity.TimeZoneInfo);
            }

            return dateTime.GetValueOrDefault(DateTime.UtcNow);
        }

		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included Code

		#endregion
	}
}
