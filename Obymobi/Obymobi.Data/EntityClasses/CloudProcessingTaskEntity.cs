﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    using System.Xml.Serialization;
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'CloudProcessingTask'. This class is used for Business Logic or for framework extension code.</summary>
	[Serializable]
	public partial class CloudProcessingTaskEntity : CloudProcessingTaskEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
        , INonCompanyRelatedEntity
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		/// <summary>CTor</summary>
		public CloudProcessingTaskEntity():base()
		{
		}

		/// <summary>CTor</summary>
		/// <param name="cloudProcessingTaskId">PK value for CloudProcessingTask which data should be fetched into this CloudProcessingTask object</param>
		public CloudProcessingTaskEntity(System.Int32 cloudProcessingTaskId):
			base(cloudProcessingTaskId)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="cloudProcessingTaskId">PK value for CloudProcessingTask which data should be fetched into this CloudProcessingTask object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public CloudProcessingTaskEntity(System.Int32 cloudProcessingTaskId, IPrefetchPath prefetchPathToUse):
			base(cloudProcessingTaskId, prefetchPathToUse)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="cloudProcessingTaskId">PK value for CloudProcessingTask which data should be fetched into this CloudProcessingTask object</param>
		/// <param name="validator">The custom validator object for this CloudProcessingTaskEntity</param>
		public CloudProcessingTaskEntity(System.Int32 cloudProcessingTaskId, IValidator validator):
			base(cloudProcessingTaskId, validator)
		{
		}
		
		/// <summary>CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected CloudProcessingTaskEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}

		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
        
        public enum ProcessingAction
        {
            Unknown = 0,
            Upload = 1,
            Delete = 2
        }

        [XmlIgnore]
        public ProcessingAction ActionAsEnum
        {
            get
            {
                return this.Action.ToEnum<ProcessingAction>();
            }
            set
            {
                this.Action = (int)value;
            }
        }

        [XmlIgnore]
	    public Obymobi.Enums.CloudProcessingTaskType TypeAsEnum
	    {
            get
            {
                return this.Type.ToEnum<Obymobi.Enums.CloudProcessingTaskType>();
            }
            set
            {
                this.Type = (int)value;
            }
	    }

		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included Code

		#endregion
	}
}
