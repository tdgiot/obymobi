﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    using System.Globalization;
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'MediaRatioTypeMediaFile'. This class is used for Business Logic or for framework extension code.</summary>
	[Serializable]
	public partial class MediaRatioTypeMediaFileEntity : MediaRatioTypeMediaFileEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
        , INonCompanyRelatedEntity
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		/// <summary>CTor</summary>
		public MediaRatioTypeMediaFileEntity():base()
		{
		}

		/// <summary>CTor</summary>
		/// <param name="mediaRatioTypeMediaFileId">PK value for MediaRatioTypeMediaFile which data should be fetched into this MediaRatioTypeMediaFile object</param>
		public MediaRatioTypeMediaFileEntity(System.Int32 mediaRatioTypeMediaFileId):
			base(mediaRatioTypeMediaFileId)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="mediaRatioTypeMediaFileId">PK value for MediaRatioTypeMediaFile which data should be fetched into this MediaRatioTypeMediaFile object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public MediaRatioTypeMediaFileEntity(System.Int32 mediaRatioTypeMediaFileId, IPrefetchPath prefetchPathToUse):
			base(mediaRatioTypeMediaFileId, prefetchPathToUse)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="mediaRatioTypeMediaFileId">PK value for MediaRatioTypeMediaFile which data should be fetched into this MediaRatioTypeMediaFile object</param>
		/// <param name="validator">The custom validator object for this MediaRatioTypeMediaFileEntity</param>
		public MediaRatioTypeMediaFileEntity(System.Int32 mediaRatioTypeMediaFileId, IValidator validator):
			base(mediaRatioTypeMediaFileId, validator)
		{
		}
		
		/// <summary>CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected MediaRatioTypeMediaFileEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}

		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode

        public bool ValidatorFileWasChangedBeforeSave = false;
        public bool BypassValidation = false;
        public bool ForceCdnPublication = false;
        public int? MediaProcessingTaskPriority = null;
        public bool SkipUpload = false;

        public delegate void AfterDeleteDelegate(MediaRatioTypeMediaFileEntity entity);
        public event AfterDeleteDelegate AfterDelete;

        protected override void OnAuditDeleteOfEntity()
        {
            if (AfterDelete != null)
                this.AfterDelete(this);

            base.OnAuditDeleteOfEntity();
        }

        public long ChangedTicks
        {
            get
            {
                if (this.UpdatedUTC != null)
                    return this.UpdatedUTC.Value.Ticks;
                else if (this.CreatedUTC != null)
                    return this.CreatedUTC.Value.Ticks;
                else
                    return long.MinValue;
            }
        }

		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included Code

		#endregion
	}
}
