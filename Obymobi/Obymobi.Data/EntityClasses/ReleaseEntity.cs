﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    using Dionysos.Data;
    using System.Xml.Serialization;
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'Release'. This class is used for Business Logic or for framework extension code.</summary>
	[Serializable]
	public partial class ReleaseEntity : ReleaseEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
        , INonCompanyRelatedEntity, ITimestampEntity
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		/// <summary>CTor</summary>
		public ReleaseEntity():base()
		{
		}

		/// <summary>CTor</summary>
		/// <param name="releaseId">PK value for Release which data should be fetched into this Release object</param>
		public ReleaseEntity(System.Int32 releaseId):
			base(releaseId)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="releaseId">PK value for Release which data should be fetched into this Release object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public ReleaseEntity(System.Int32 releaseId, IPrefetchPath prefetchPathToUse):
			base(releaseId, prefetchPathToUse)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="releaseId">PK value for Release which data should be fetched into this Release object</param>
		/// <param name="validator">The custom validator object for this ReleaseEntity</param>
		public ReleaseEntity(System.Int32 releaseId, IValidator validator):
			base(releaseId, validator)
		{
		}
		
		/// <summary>CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected ReleaseEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}

		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode

        /// <summary>
        /// Gets the name of the application belonging to this release
        /// </summary>
        [DataGridViewColumnVisible]
        public string ApplicationName
        {
            get
            {
                return this.ApplicationEntity.Name;
            }
        }

        /// <summary>
        /// Gets the flag which indicates whether this release is set as a stable release
        /// </summary>
        public bool IsStableRelease
        {
            get
            {
                return (this.ApplicationCollection.Count > 0);
            }
        }

        /// <summary>
        /// Gets the version as a <see cref="VersionNumber"/> instance
        /// </summary>
        [XmlIgnore]
        public VersionNumber VersionAsVersionNumber
        {
            get
            {
                return new VersionNumber(this.Version);
            }
        }

        [XmlIgnore]
        public ReleaseEntity AfterSaveFullReleaseEntity { get; set; }

		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included Code

		#endregion
	}
}
