﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    using System.Xml.Serialization;
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'AdvertisementTagAdvertisement'. This class is used for Business Logic or for framework extension code.</summary>
	[Serializable]
	public partial class AdvertisementTagAdvertisementEntity : AdvertisementTagAdvertisementEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
        , INullableCompanyRelatedChildEntity
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		/// <summary>CTor</summary>
		public AdvertisementTagAdvertisementEntity():base()
		{
		}

		/// <summary>CTor</summary>
		/// <param name="advertisementTagAdvertisementId">PK value for AdvertisementTagAdvertisement which data should be fetched into this AdvertisementTagAdvertisement object</param>
		public AdvertisementTagAdvertisementEntity(System.Int32 advertisementTagAdvertisementId):
			base(advertisementTagAdvertisementId)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="advertisementTagAdvertisementId">PK value for AdvertisementTagAdvertisement which data should be fetched into this AdvertisementTagAdvertisement object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public AdvertisementTagAdvertisementEntity(System.Int32 advertisementTagAdvertisementId, IPrefetchPath prefetchPathToUse):
			base(advertisementTagAdvertisementId, prefetchPathToUse)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="advertisementTagAdvertisementId">PK value for AdvertisementTagAdvertisement which data should be fetched into this AdvertisementTagAdvertisement object</param>
		/// <param name="validator">The custom validator object for this AdvertisementTagAdvertisementEntity</param>
		public AdvertisementTagAdvertisementEntity(System.Int32 advertisementTagAdvertisementId, IValidator validator):
			base(advertisementTagAdvertisementId, validator)
		{
		}
		
		/// <summary>CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected AdvertisementTagAdvertisementEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}

		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode

        [XmlIgnore]
        public CommonEntityBase Parent
        {
            get { return this.AdvertisementEntity; }
        }

		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included Code

		#endregion
	}
}
