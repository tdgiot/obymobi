﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    using System.Xml.Serialization;
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'Rating'. This class is used for Business Logic or for framework extension code.</summary>
	[Serializable]
	public partial class RatingEntity : RatingEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
        ,INullableCompanyRelatedChildEntity
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		/// <summary>CTor</summary>
		public RatingEntity():base()
		{
		}

		/// <summary>CTor</summary>
		/// <param name="ratingId">PK value for Rating which data should be fetched into this Rating object</param>
		public RatingEntity(System.Int32 ratingId):
			base(ratingId)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="ratingId">PK value for Rating which data should be fetched into this Rating object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public RatingEntity(System.Int32 ratingId, IPrefetchPath prefetchPathToUse):
			base(ratingId, prefetchPathToUse)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="ratingId">PK value for Rating which data should be fetched into this Rating object</param>
		/// <param name="validator">The custom validator object for this RatingEntity</param>
		public RatingEntity(System.Int32 ratingId, IValidator validator):
			base(ratingId, validator)
		{
		}
		
		/// <summary>CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected RatingEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}

		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode

        [XmlIgnore]
        public CommonEntityBase Parent
        {
            get 
            {
                if (this.ProductId.HasValue)
                    return this.ProductEntity;
                else
                    return null;
            }
        }

		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included Code

		#endregion
	}
}
