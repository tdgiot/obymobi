﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    using System.Xml.Serialization;
    using Dionysos.Data;
    using Dionysos;
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'PaymentTransaction'. This class is used for Business Logic or for framework extension code.</summary>
	[Serializable]
	public partial class PaymentTransactionEntity : PaymentTransactionEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
        , INonCompanyRelatedEntity
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		/// <summary>CTor</summary>
		public PaymentTransactionEntity():base()
		{
		}

		/// <summary>CTor</summary>
		/// <param name="paymentTransactionId">PK value for PaymentTransaction which data should be fetched into this PaymentTransaction object</param>
		public PaymentTransactionEntity(System.Int32 paymentTransactionId):
			base(paymentTransactionId)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="paymentTransactionId">PK value for PaymentTransaction which data should be fetched into this PaymentTransaction object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public PaymentTransactionEntity(System.Int32 paymentTransactionId, IPrefetchPath prefetchPathToUse):
			base(paymentTransactionId, prefetchPathToUse)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="paymentTransactionId">PK value for PaymentTransaction which data should be fetched into this PaymentTransaction object</param>
		/// <param name="validator">The custom validator object for this PaymentTransactionEntity</param>
		public PaymentTransactionEntity(System.Int32 paymentTransactionId, IValidator validator):
			base(paymentTransactionId, validator)
		{
		}
		
		/// <summary>CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected PaymentTransactionEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}

		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode

        [XmlIgnore]
        public string StatusText => this.Status.ToString();

        [XmlIgnore]
        public DateTime? CreatedLocal => this.CreatedUTC.UtcToLocalTime(this.OrderEntity.TimeZoneInfo);

        // __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included Code

		#endregion
	}
}
