﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    using Obymobi.Logic.Cms;
    using System.Xml.Serialization;
    using Obymobi.Enums;
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'AlterationProduct'. This class is used for Business Logic or for framework extension code.</summary>
	[Serializable]
	public partial class AlterationProductEntity : AlterationProductEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
        , INullableCompanyRelatedChildEntity, IAlterationComponent
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		/// <summary>CTor</summary>
		public AlterationProductEntity():base()
		{
		}

		/// <summary>CTor</summary>
		/// <param name="alterationProductId">PK value for AlterationProduct which data should be fetched into this AlterationProduct object</param>
		public AlterationProductEntity(System.Int32 alterationProductId):
			base(alterationProductId)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="alterationProductId">PK value for AlterationProduct which data should be fetched into this AlterationProduct object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public AlterationProductEntity(System.Int32 alterationProductId, IPrefetchPath prefetchPathToUse):
			base(alterationProductId, prefetchPathToUse)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="alterationProductId">PK value for AlterationProduct which data should be fetched into this AlterationProduct object</param>
		/// <param name="validator">The custom validator object for this AlterationProductEntity</param>
		public AlterationProductEntity(System.Int32 alterationProductId, IValidator validator):
			base(alterationProductId, validator)
		{
		}
		
		/// <summary>CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected AlterationProductEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}

		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode

        public CommonEntityBase Parent
        {
            get { return this.AlterationEntity; }
        }

        #region CMS Treelist Logic

	    [XmlIgnore]
	    public string Name
	    {
            get { return this.ProductEntity.Name; }
	        set { this.ProductEntity.Name = value; }
	    }

        [XmlIgnore]
        public string ItemId
        {
            get
            {
                return string.Format("{0}-{1}-{2}-{3}", this.ItemType, this.ProductId, this.AlterationId, this.SortOrder);
            }
        }

        [XmlIgnore]
        public string ParentItemId
        {
            get
            {
                return this.AlterationEntity.ItemId;
            }
        }

        [XmlIgnore]
        public string ItemType
        {
            get
            {
                return AlterationComponentType.Product;
            }
        }

        [XmlIgnore]
        public string Type
        {
            get { return ((ProductType)this.ProductEntity.Type).ToString(); }            
        }

        [DataGridViewColumnVisible]
        [XmlIgnore]
        public string TypeName
        {
            get
            {
                return ((ProductType)this.ProductEntity.Type).ToString();
            }
        }        

        #endregion

        // __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included Code

		#endregion
	}
}
