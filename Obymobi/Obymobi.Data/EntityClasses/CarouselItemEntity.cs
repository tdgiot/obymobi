﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'CarouselItem'. This class is used for Business Logic or for framework extension code.</summary>
	[Serializable]
	public partial class CarouselItemEntity : CarouselItemEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		, ICompanyRelatedChildEntity, ICustomTextContainingEntity, IMediaContainingEntity
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		/// <summary>CTor</summary>
		public CarouselItemEntity():base()
		{
		}

		/// <summary>CTor</summary>
		/// <param name="carouselItemId">PK value for CarouselItem which data should be fetched into this CarouselItem object</param>
		public CarouselItemEntity(System.Int32 carouselItemId):
			base(carouselItemId)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="carouselItemId">PK value for CarouselItem which data should be fetched into this CarouselItem object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public CarouselItemEntity(System.Int32 carouselItemId, IPrefetchPath prefetchPathToUse):
			base(carouselItemId, prefetchPathToUse)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="carouselItemId">PK value for CarouselItem which data should be fetched into this CarouselItem object</param>
		/// <param name="validator">The custom validator object for this CarouselItemEntity</param>
		public CarouselItemEntity(System.Int32 carouselItemId, IValidator validator):
			base(carouselItemId, validator)
		{
		}
		
		/// <summary>CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected CarouselItemEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}

		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		public CommonEntityBase Parent => this.WidgetCarouselEntity;

		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included Code

		#endregion
	}
}
