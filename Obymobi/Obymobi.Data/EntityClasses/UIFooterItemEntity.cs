﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    using Obymobi.Enums;
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'UIFooterItem'. This class is used for Business Logic or for framework extension code.</summary>
	[Serializable]
	public partial class UIFooterItemEntity : UIFooterItemEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
        , INullableCompanyRelatedChildEntity, ICustomTextContainingEntity, ITimestampEntity
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		/// <summary>CTor</summary>
		public UIFooterItemEntity():base()
		{
		}

		/// <summary>CTor</summary>
		/// <param name="uIFooterItemId">PK value for UIFooterItem which data should be fetched into this UIFooterItem object</param>
		public UIFooterItemEntity(System.Int32 uIFooterItemId):
			base(uIFooterItemId)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="uIFooterItemId">PK value for UIFooterItem which data should be fetched into this UIFooterItem object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public UIFooterItemEntity(System.Int32 uIFooterItemId, IPrefetchPath prefetchPathToUse):
			base(uIFooterItemId, prefetchPathToUse)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="uIFooterItemId">PK value for UIFooterItem which data should be fetched into this UIFooterItem object</param>
		/// <param name="validator">The custom validator object for this UIFooterItemEntity</param>
		public UIFooterItemEntity(System.Int32 uIFooterItemId, IValidator validator):
			base(uIFooterItemId, validator)
		{
		}
		
		/// <summary>CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected UIFooterItemEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}

		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode

        public CommonEntityBase Parent
        {
            get { return this.UIModeEntity; }
        }

        [DataGridViewColumnVisible]
	    public FooterbarItemType TypeAsEnum
	    {
	        get { return this.Type.ToEnum<FooterbarItemType>(); }
	    }

        [DataGridViewColumnVisible]
	    public FooterbarItemPosition PositionAsEnum
	    {
            get { return this.Position.ToEnum<FooterbarItemPosition>(); }
	    }

		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included Code

		#endregion
	}
}
