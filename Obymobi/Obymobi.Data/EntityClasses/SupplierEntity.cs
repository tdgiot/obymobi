﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'Supplier'. This class is used for Business Logic or for framework extension code.</summary>
	[Serializable]
	public partial class SupplierEntity : SupplierEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
        , INonCompanyRelatedEntity
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		/// <summary>CTor</summary>
		public SupplierEntity():base()
		{
		}

		/// <summary>CTor</summary>
		/// <param name="supplierId">PK value for Supplier which data should be fetched into this Supplier object</param>
		public SupplierEntity(System.Int32 supplierId):
			base(supplierId)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="supplierId">PK value for Supplier which data should be fetched into this Supplier object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public SupplierEntity(System.Int32 supplierId, IPrefetchPath prefetchPathToUse):
			base(supplierId, prefetchPathToUse)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="supplierId">PK value for Supplier which data should be fetched into this Supplier object</param>
		/// <param name="validator">The custom validator object for this SupplierEntity</param>
		public SupplierEntity(System.Int32 supplierId, IValidator validator):
			base(supplierId, validator)
		{
		}
		
		/// <summary>CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected SupplierEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}

		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included Code

		#endregion
	}
}
