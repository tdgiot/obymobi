﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    using Dionysos;
    using Obymobi.Enums;
    using System.Xml.Serialization;
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'ScheduledCommand'. This class is used for Business Logic or for framework extension code.</summary>
	[Serializable]
	public partial class ScheduledCommandEntity : ScheduledCommandEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
        ,ICompanyRelatedChildEntity
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		/// <summary>CTor</summary>
		public ScheduledCommandEntity():base()
		{
		}

		/// <summary>CTor</summary>
		/// <param name="scheduledCommandId">PK value for ScheduledCommand which data should be fetched into this ScheduledCommand object</param>
		public ScheduledCommandEntity(System.Int32 scheduledCommandId):
			base(scheduledCommandId)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="scheduledCommandId">PK value for ScheduledCommand which data should be fetched into this ScheduledCommand object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public ScheduledCommandEntity(System.Int32 scheduledCommandId, IPrefetchPath prefetchPathToUse):
			base(scheduledCommandId, prefetchPathToUse)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="scheduledCommandId">PK value for ScheduledCommand which data should be fetched into this ScheduledCommand object</param>
		/// <param name="validator">The custom validator object for this ScheduledCommandEntity</param>
		public ScheduledCommandEntity(System.Int32 scheduledCommandId, IValidator validator):
			base(scheduledCommandId, validator)
		{
		}
		
		/// <summary>CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected ScheduledCommandEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}

		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode

        [XmlIgnore]
        public CommonEntityBase Parent
        {
            get { return this.ScheduledCommandTaskEntity; }
        }

        [DataGridViewColumnVisible]
        public string CommandText
        {
            get
            {
                if (this.ClientCommand.HasValue)
                    return string.Format("ClientCommand.{0}", this.ClientCommand.Value.ToString());
                else if (this.TerminalCommand.HasValue)
                    return string.Format("TerminalCommand.{0}", this.TerminalCommand.Value.ToString());
                else
                    return "No command specified";
            }
        }

        [DataGridViewColumnVisible]
        public string StatusText
        {
            get
            {
                return this.Status.ToString();
            }
        }

        [DataGridViewColumnVisible]
        public string ExpiresText
        {
            get
            {
                if (!this.ExpiresUTC.HasValue)
                    return string.Empty;
                if (this.Status == ScheduledCommandStatus.Succeeded)
                    return "Completed";
                if (this.Status == ScheduledCommandStatus.Expired || this.Status == ScheduledCommandStatus.Failed)
                    return "Completed with error";

                return string.Format("{0} ({1})", this.ExpiresUTC.Value.ToString(), DateTimeUtil.GetSpanTextBetweenDateTimeAndUtcNow(this.ExpiresUTC.Value));
            }
        }

        [DataGridViewColumnVisible]
        public string SubjectText
        {
            get
            {
                if (this.ClientId.HasValue)
                    return string.Format("Client {0}",  this.ClientId.Value);
                if (this.TerminalId.HasValue)
                    return string.Format("Terminal {0}", this.TerminalId.Value);
                
                return "No subject specified";
            }
        }

        [DataGridViewColumnVisible]
        public string StartedText
        {
            get
            {
                if (!this.StartedUTC.HasValue)
                    return string.Empty;

                return string.Format("{0} ({1})", this.StartedUTC.Value.ToString(), DateTimeUtil.GetSpanTextBetweenDateTimeAndUtcNow(this.StartedUTC.Value));
            }
        }

        [DataGridViewColumnVisible]
        public string CompletedText
        {
            get
            {
                if (!this.CompletedUTC.HasValue)
                    return string.Empty;

                return string.Format("{0} ({1})", this.CompletedUTC.Value.ToString(), DateTimeUtil.GetSpanTextBetweenDateTimeAndUtcNow(this.CompletedUTC.Value));
            }
        }

		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included Code

		#endregion
	}
}
