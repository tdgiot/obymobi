﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    using Dionysos.Data;
    using System.Xml.Serialization;
    using System.Collections.Generic;
    using Obymobi.Interfaces;
    using System.Linq;
    // __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'Alterationoption'. This class is used for Business Logic or for framework extension code.</summary>
	[Serializable]
	public partial class AlterationoptionEntity : AlterationoptionEntityBase
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
    , 
    INullableCompanyRelatedEntity, 
    IMediaContainingEntity, 
    ICustomTextContainingEntity, 
    IPriceContainingEntity, 
    INullableBrandRelatedEntity, 
    ITimestampEntity,
	ITagEntity
    // __LLBLGENPRO_USER_CODE_REGION_END	
	{
		/// <summary>CTor</summary>
		public AlterationoptionEntity():base()
		{
		}

		/// <summary>CTor</summary>
		/// <param name="alterationoptionId">PK value for Alterationoption which data should be fetched into this Alterationoption object</param>
		public AlterationoptionEntity(System.Int32 alterationoptionId):
			base(alterationoptionId)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="alterationoptionId">PK value for Alterationoption which data should be fetched into this Alterationoption object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public AlterationoptionEntity(System.Int32 alterationoptionId, IPrefetchPath prefetchPathToUse):
			base(alterationoptionId, prefetchPathToUse)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="alterationoptionId">PK value for Alterationoption which data should be fetched into this Alterationoption object</param>
		/// <param name="validator">The custom validator object for this AlterationoptionEntity</param>
		public AlterationoptionEntity(System.Int32 alterationoptionId, IValidator validator):
			base(alterationoptionId, validator)
		{
		}
		
		/// <summary>CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected AlterationoptionEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
            // __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
            // __LLBLGENPRO_USER_CODE_REGION_END
		}

		#region Custom Entity code
		
        // __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode          

        [DataGridViewColumnVisibleAttribute]
        [XmlIgnore]
        public string NameAndPriceIn
        {
            get
            {
                return string.Format("{0} ({1})", this.FriendlyNameOrName, this.PriceIn.HasValue ? this.PriceIn.Value.ToString("N") : string.Empty);
            }
        }

        [DataGridViewColumnVisible]
        [XmlIgnore]
        public string FriendlyNameOrName
        {
            get
            {
                return !string.IsNullOrWhiteSpace(this.FriendlyName) ? this.FriendlyName : this.Name;
            }
        }

	    public bool ValidatorCreateOrUpdateDefaultCustomText { get; set; }

	    public bool NameChanged { get; set; }

        public ICollection<ITag> GetTags()
        {
            return this.AlterationoptionTagCollection.Select(x => x.TagEntity).ToArray();
        }

        public ICollection<ITag> GetInheritedTags()
        {
            if (!ProductId.HasValue)
            {
                return new List<ITag>(0);
            }

            return ProductEntity.ProductTagCollection.Select(pt => pt.TagEntity).ToArray();
        }

        public IEntityCollection TagCollection => this.AlterationoptionTagCollection;

        // __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included Code

		#endregion
	}
}
