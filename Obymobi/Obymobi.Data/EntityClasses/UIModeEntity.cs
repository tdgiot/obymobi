﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    using System.Xml.Serialization;
    using Obymobi.Enums;
using Obymobi.Data.HelperClasses;
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'UIMode'. This class is used for Business Logic or for framework extension code.</summary>
	[Serializable]
	public partial class UIModeEntity : UIModeEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces        
        , INullableCompanyRelatedEntity, ITimestampEntity
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		/// <summary>CTor</summary>
		public UIModeEntity():base()
		{
		}

		/// <summary>CTor</summary>
		/// <param name="uIModeId">PK value for UIMode which data should be fetched into this UIMode object</param>
		public UIModeEntity(System.Int32 uIModeId):
			base(uIModeId)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="uIModeId">PK value for UIMode which data should be fetched into this UIMode object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public UIModeEntity(System.Int32 uIModeId, IPrefetchPath prefetchPathToUse):
			base(uIModeId, prefetchPathToUse)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="uIModeId">PK value for UIMode which data should be fetched into this UIMode object</param>
		/// <param name="validator">The custom validator object for this UIModeEntity</param>
		public UIModeEntity(System.Int32 uIModeId, IValidator validator):
			base(uIModeId, validator)
		{
		}
		
		/// <summary>CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected UIModeEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}

		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode

        [DataGridViewColumnVisible]
        [EntityFieldDependency((int)UIModeFieldIndex.Type)]
        public string TypeText
        {
            get
            {                  
                return this.Type.ToString();
            }
        }

		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included Code

		#endregion
	}
}
