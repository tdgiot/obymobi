﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'CheckoutMethodDeliverypointgroup'. This class is used for Business Logic or for framework extension code.</summary>
	[Serializable]
	public partial class CheckoutMethodDeliverypointgroupEntity : CheckoutMethodDeliverypointgroupEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		, ICompanyRelatedChildEntity
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		/// <summary>CTor</summary>
		public CheckoutMethodDeliverypointgroupEntity():base()
		{
		}

		/// <summary>CTor</summary>
		/// <param name="deliverypointgroupId">PK value for CheckoutMethodDeliverypointgroup which data should be fetched into this CheckoutMethodDeliverypointgroup object</param>
		/// <param name="checkoutMethodId">PK value for CheckoutMethodDeliverypointgroup which data should be fetched into this CheckoutMethodDeliverypointgroup object</param>
		public CheckoutMethodDeliverypointgroupEntity(System.Int32 deliverypointgroupId, System.Int32 checkoutMethodId):
			base(deliverypointgroupId, checkoutMethodId)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="deliverypointgroupId">PK value for CheckoutMethodDeliverypointgroup which data should be fetched into this CheckoutMethodDeliverypointgroup object</param>
		/// <param name="checkoutMethodId">PK value for CheckoutMethodDeliverypointgroup which data should be fetched into this CheckoutMethodDeliverypointgroup object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public CheckoutMethodDeliverypointgroupEntity(System.Int32 deliverypointgroupId, System.Int32 checkoutMethodId, IPrefetchPath prefetchPathToUse):
			base(deliverypointgroupId, checkoutMethodId, prefetchPathToUse)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="deliverypointgroupId">PK value for CheckoutMethodDeliverypointgroup which data should be fetched into this CheckoutMethodDeliverypointgroup object</param>
		/// <param name="checkoutMethodId">PK value for CheckoutMethodDeliverypointgroup which data should be fetched into this CheckoutMethodDeliverypointgroup object</param>
		/// <param name="validator">The custom validator object for this CheckoutMethodDeliverypointgroupEntity</param>
		public CheckoutMethodDeliverypointgroupEntity(System.Int32 deliverypointgroupId, System.Int32 checkoutMethodId, IValidator validator):
			base(deliverypointgroupId, checkoutMethodId, validator)
		{
		}
		
		/// <summary>CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected CheckoutMethodDeliverypointgroupEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}

		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
        public CommonEntityBase Parent => this.CheckoutMethodEntity;
        // __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included Code

		#endregion
	}
}
