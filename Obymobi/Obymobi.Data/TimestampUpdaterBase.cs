﻿using System.Collections.Generic;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data
{
    public abstract class TimestampUpdaterBase
    {
        public abstract void UpdateTimestamps(IEntity entity, bool wasNew);

        public abstract bool RelevantFieldsChanged(IEntity entity);

        protected abstract IEnumerable<EntityField> Fields { get; }
    }
}
