﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Data;
using System.Collections.Generic;
using System.ComponentModel;
using System.Xml;
#if !CF
using System.Runtime.Serialization;
#endif

using Obymobi.Data.EntityClasses;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.CollectionClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Collection class for storing and retrieving collections of OrderEntity objects. </summary>
	[Serializable]
	public partial class OrderCollection : CommonEntityCollection<OrderEntity>
	{
		/// <summary> CTor</summary>
		public OrderCollection():base(new OrderEntityFactory())
		{
		}

		/// <summary> CTor</summary>
		/// <param name="initialContents">The initial contents of this collection.</param>
		public OrderCollection(IEnumerable<OrderEntity> initialContents):base(new OrderEntityFactory())
		{
			AddRange(initialContents);
		}

		/// <summary> CTor</summary>
		/// <param name="entityFactoryToUse">The EntityFactory to use when creating entity objects during a GetMulti() call.</param>
		public OrderCollection(IEntityFactory entityFactoryToUse):base(entityFactoryToUse)
		{
		}

		/// <summary> Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected OrderCollection(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}


		/// <summary> Retrieves in this OrderCollection object all OrderEntity objects which have data in common with the specified related Entities.
		/// If one is omitted, that entity is not used as a filter. All current elements in the collection are removed from the collection.</summary>
		/// <param name="checkoutMethodEntityInstance">CheckoutMethodEntity instance to use as a filter for the OrderEntity objects to return</param>
		/// <param name="clientEntityInstance">ClientEntity instance to use as a filter for the OrderEntity objects to return</param>
		/// <param name="companyEntityInstance">CompanyEntity instance to use as a filter for the OrderEntity objects to return</param>
		/// <param name="currencyEntityInstance">CurrencyEntity instance to use as a filter for the OrderEntity objects to return</param>
		/// <param name="customerEntityInstance">CustomerEntity instance to use as a filter for the OrderEntity objects to return</param>
		/// <param name="deliveryInformationEntityInstance">DeliveryInformationEntity instance to use as a filter for the OrderEntity objects to return</param>
		/// <param name="deliverypointEntityInstance">DeliverypointEntity instance to use as a filter for the OrderEntity objects to return</param>
		/// <param name="deliverypointEntity_Instance">DeliverypointEntity instance to use as a filter for the OrderEntity objects to return</param>
		/// <param name="orderEntityInstance">OrderEntity instance to use as a filter for the OrderEntity objects to return</param>
		/// <param name="outletEntityInstance">OutletEntity instance to use as a filter for the OrderEntity objects to return</param>
		/// <param name="serviceMethodEntityInstance">ServiceMethodEntity instance to use as a filter for the OrderEntity objects to return</param>
		/// <returns>true if succeeded, false otherwise</returns>
		public bool GetMultiManyToOne(IEntity checkoutMethodEntityInstance, IEntity clientEntityInstance, IEntity companyEntityInstance, IEntity currencyEntityInstance, IEntity customerEntityInstance, IEntity deliveryInformationEntityInstance, IEntity deliverypointEntityInstance, IEntity deliverypointEntity_Instance, IEntity orderEntityInstance, IEntity outletEntityInstance, IEntity serviceMethodEntityInstance)
		{
			return GetMultiManyToOne(checkoutMethodEntityInstance, clientEntityInstance, companyEntityInstance, currencyEntityInstance, customerEntityInstance, deliveryInformationEntityInstance, deliverypointEntityInstance, deliverypointEntity_Instance, orderEntityInstance, outletEntityInstance, serviceMethodEntityInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, null, 0, 0);
		}

		/// <summary> Retrieves in this OrderCollection object all OrderEntity objects which have data in common with the specified related Entities.
		/// If one is omitted, that entity is not used as a filter. All current elements in the collection are removed from the collection.</summary>
		/// <param name="checkoutMethodEntityInstance">CheckoutMethodEntity instance to use as a filter for the OrderEntity objects to return</param>
		/// <param name="clientEntityInstance">ClientEntity instance to use as a filter for the OrderEntity objects to return</param>
		/// <param name="companyEntityInstance">CompanyEntity instance to use as a filter for the OrderEntity objects to return</param>
		/// <param name="currencyEntityInstance">CurrencyEntity instance to use as a filter for the OrderEntity objects to return</param>
		/// <param name="customerEntityInstance">CustomerEntity instance to use as a filter for the OrderEntity objects to return</param>
		/// <param name="deliveryInformationEntityInstance">DeliveryInformationEntity instance to use as a filter for the OrderEntity objects to return</param>
		/// <param name="deliverypointEntityInstance">DeliverypointEntity instance to use as a filter for the OrderEntity objects to return</param>
		/// <param name="deliverypointEntity_Instance">DeliverypointEntity instance to use as a filter for the OrderEntity objects to return</param>
		/// <param name="orderEntityInstance">OrderEntity instance to use as a filter for the OrderEntity objects to return</param>
		/// <param name="outletEntityInstance">OutletEntity instance to use as a filter for the OrderEntity objects to return</param>
		/// <param name="serviceMethodEntityInstance">ServiceMethodEntity instance to use as a filter for the OrderEntity objects to return</param>
		/// <param name="filter">Extra filter to limit the resultset. Predicate expression can be null, in which case it will be ignored.</param>
		/// <returns>true if succeeded, false otherwise</returns>
		public bool GetMultiManyToOne(IEntity checkoutMethodEntityInstance, IEntity clientEntityInstance, IEntity companyEntityInstance, IEntity currencyEntityInstance, IEntity customerEntityInstance, IEntity deliveryInformationEntityInstance, IEntity deliverypointEntityInstance, IEntity deliverypointEntity_Instance, IEntity orderEntityInstance, IEntity outletEntityInstance, IEntity serviceMethodEntityInstance, IPredicateExpression filter)
		{
			return GetMultiManyToOne(checkoutMethodEntityInstance, clientEntityInstance, companyEntityInstance, currencyEntityInstance, customerEntityInstance, deliveryInformationEntityInstance, deliverypointEntityInstance, deliverypointEntity_Instance, orderEntityInstance, outletEntityInstance, serviceMethodEntityInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, filter, 0, 0);
		}

		/// <summary> Retrieves in this OrderCollection object all OrderEntity objects which have data in common with the specified related Entities.
		/// If one is omitted, that entity is not used as a filter. All current elements in the collection are removed from the collection.</summary>
		/// <param name="checkoutMethodEntityInstance">CheckoutMethodEntity instance to use as a filter for the OrderEntity objects to return</param>
		/// <param name="clientEntityInstance">ClientEntity instance to use as a filter for the OrderEntity objects to return</param>
		/// <param name="companyEntityInstance">CompanyEntity instance to use as a filter for the OrderEntity objects to return</param>
		/// <param name="currencyEntityInstance">CurrencyEntity instance to use as a filter for the OrderEntity objects to return</param>
		/// <param name="customerEntityInstance">CustomerEntity instance to use as a filter for the OrderEntity objects to return</param>
		/// <param name="deliveryInformationEntityInstance">DeliveryInformationEntity instance to use as a filter for the OrderEntity objects to return</param>
		/// <param name="deliverypointEntityInstance">DeliverypointEntity instance to use as a filter for the OrderEntity objects to return</param>
		/// <param name="deliverypointEntity_Instance">DeliverypointEntity instance to use as a filter for the OrderEntity objects to return</param>
		/// <param name="orderEntityInstance">OrderEntity instance to use as a filter for the OrderEntity objects to return</param>
		/// <param name="outletEntityInstance">OutletEntity instance to use as a filter for the OrderEntity objects to return</param>
		/// <param name="serviceMethodEntityInstance">ServiceMethodEntity instance to use as a filter for the OrderEntity objects to return</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="filter">Extra filter to limit the resultset. Predicate expression can be null, in which case it will be ignored.</param>
		/// <returns>true if succeeded, false otherwise</returns>
		public bool GetMultiManyToOne(IEntity checkoutMethodEntityInstance, IEntity clientEntityInstance, IEntity companyEntityInstance, IEntity currencyEntityInstance, IEntity customerEntityInstance, IEntity deliveryInformationEntityInstance, IEntity deliverypointEntityInstance, IEntity deliverypointEntity_Instance, IEntity orderEntityInstance, IEntity outletEntityInstance, IEntity serviceMethodEntityInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPredicateExpression filter)
		{
			return GetMultiManyToOne(checkoutMethodEntityInstance, clientEntityInstance, companyEntityInstance, currencyEntityInstance, customerEntityInstance, deliveryInformationEntityInstance, deliverypointEntityInstance, deliverypointEntity_Instance, orderEntityInstance, outletEntityInstance, serviceMethodEntityInstance, maxNumberOfItemsToReturn, sortClauses, filter, 0, 0);
		}

		/// <summary> Retrieves in this OrderCollection object all OrderEntity objects which have data in common with the specified related Entities.
		/// If one is omitted, that entity is not used as a filter. All current elements in the collection are removed from the collection.</summary>
		/// <param name="checkoutMethodEntityInstance">CheckoutMethodEntity instance to use as a filter for the OrderEntity objects to return</param>
		/// <param name="clientEntityInstance">ClientEntity instance to use as a filter for the OrderEntity objects to return</param>
		/// <param name="companyEntityInstance">CompanyEntity instance to use as a filter for the OrderEntity objects to return</param>
		/// <param name="currencyEntityInstance">CurrencyEntity instance to use as a filter for the OrderEntity objects to return</param>
		/// <param name="customerEntityInstance">CustomerEntity instance to use as a filter for the OrderEntity objects to return</param>
		/// <param name="deliveryInformationEntityInstance">DeliveryInformationEntity instance to use as a filter for the OrderEntity objects to return</param>
		/// <param name="deliverypointEntityInstance">DeliverypointEntity instance to use as a filter for the OrderEntity objects to return</param>
		/// <param name="deliverypointEntity_Instance">DeliverypointEntity instance to use as a filter for the OrderEntity objects to return</param>
		/// <param name="orderEntityInstance">OrderEntity instance to use as a filter for the OrderEntity objects to return</param>
		/// <param name="outletEntityInstance">OutletEntity instance to use as a filter for the OrderEntity objects to return</param>
		/// <param name="serviceMethodEntityInstance">ServiceMethodEntity instance to use as a filter for the OrderEntity objects to return</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="filter">Extra filter to limit the resultset. Predicate expression can be null, in which case it will be ignored.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToOne(IEntity checkoutMethodEntityInstance, IEntity clientEntityInstance, IEntity companyEntityInstance, IEntity currencyEntityInstance, IEntity customerEntityInstance, IEntity deliveryInformationEntityInstance, IEntity deliverypointEntityInstance, IEntity deliverypointEntity_Instance, IEntity orderEntityInstance, IEntity outletEntityInstance, IEntity serviceMethodEntityInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPredicateExpression filter, int pageNumber, int pageSize)
		{
			bool validParameters = false;
			validParameters |= (checkoutMethodEntityInstance!=null);
			validParameters |= (clientEntityInstance!=null);
			validParameters |= (companyEntityInstance!=null);
			validParameters |= (currencyEntityInstance!=null);
			validParameters |= (customerEntityInstance!=null);
			validParameters |= (deliveryInformationEntityInstance!=null);
			validParameters |= (deliverypointEntityInstance!=null);
			validParameters |= (deliverypointEntity_Instance!=null);
			validParameters |= (orderEntityInstance!=null);
			validParameters |= (outletEntityInstance!=null);
			validParameters |= (serviceMethodEntityInstance!=null);
			if(!validParameters)
			{
				return GetMulti(filter, maxNumberOfItemsToReturn, sortClauses, null, pageNumber, pageSize);
			}
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateOrderDAO().GetMulti(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, filter, checkoutMethodEntityInstance, clientEntityInstance, companyEntityInstance, currencyEntityInstance, customerEntityInstance, deliveryInformationEntityInstance, deliverypointEntityInstance, deliverypointEntity_Instance, orderEntityInstance, outletEntityInstance, serviceMethodEntityInstance, pageNumber, pageSize);
		}

		/// <summary> Deletes from the persistent storage all Order entities which have data in common with the specified related Entities. If one is omitted, that entity is not used as a filter.</summary>
		/// <remarks>Runs directly on the persistent storage. It will not delete entity objects from the current collection.</remarks>
		/// <param name="checkoutMethodEntityInstance">CheckoutMethodEntity instance to use as a filter for the OrderEntity objects to return</param>
		/// <param name="clientEntityInstance">ClientEntity instance to use as a filter for the OrderEntity objects to return</param>
		/// <param name="companyEntityInstance">CompanyEntity instance to use as a filter for the OrderEntity objects to return</param>
		/// <param name="currencyEntityInstance">CurrencyEntity instance to use as a filter for the OrderEntity objects to return</param>
		/// <param name="customerEntityInstance">CustomerEntity instance to use as a filter for the OrderEntity objects to return</param>
		/// <param name="deliveryInformationEntityInstance">DeliveryInformationEntity instance to use as a filter for the OrderEntity objects to return</param>
		/// <param name="deliverypointEntityInstance">DeliverypointEntity instance to use as a filter for the OrderEntity objects to return</param>
		/// <param name="deliverypointEntity_Instance">DeliverypointEntity instance to use as a filter for the OrderEntity objects to return</param>
		/// <param name="orderEntityInstance">OrderEntity instance to use as a filter for the OrderEntity objects to return</param>
		/// <param name="outletEntityInstance">OutletEntity instance to use as a filter for the OrderEntity objects to return</param>
		/// <param name="serviceMethodEntityInstance">ServiceMethodEntity instance to use as a filter for the OrderEntity objects to return</param>
		/// <returns>Amount of entities affected, if the used persistent storage has rowcounting enabled.</returns>
		public int DeleteMultiManyToOne(IEntity checkoutMethodEntityInstance, IEntity clientEntityInstance, IEntity companyEntityInstance, IEntity currencyEntityInstance, IEntity customerEntityInstance, IEntity deliveryInformationEntityInstance, IEntity deliverypointEntityInstance, IEntity deliverypointEntity_Instance, IEntity orderEntityInstance, IEntity outletEntityInstance, IEntity serviceMethodEntityInstance)
		{
			return DAOFactory.CreateOrderDAO().DeleteMulti(this.Transaction, checkoutMethodEntityInstance, clientEntityInstance, companyEntityInstance, currencyEntityInstance, customerEntityInstance, deliveryInformationEntityInstance, deliverypointEntityInstance, deliverypointEntity_Instance, orderEntityInstance, outletEntityInstance, serviceMethodEntityInstance);
		}

		/// <summary> Updates in the persistent storage all Order entities which have data in common with the specified related Entities. If one is omitted, that entity is not used as a filter.
		/// Which fields are updated in those matching entities depends on which fields are <i>changed</i> in the passed in entity entityWithNewValues. The new values of these fields are read from entityWithNewValues. </summary>
		/// <param name="entityWithNewValues">OrderEntity instance which holds the new values for the matching entities to update. Only changed fields are taken into account</param>
		/// <param name="checkoutMethodEntityInstance">CheckoutMethodEntity instance to use as a filter for the OrderEntity objects to return</param>
		/// <param name="clientEntityInstance">ClientEntity instance to use as a filter for the OrderEntity objects to return</param>
		/// <param name="companyEntityInstance">CompanyEntity instance to use as a filter for the OrderEntity objects to return</param>
		/// <param name="currencyEntityInstance">CurrencyEntity instance to use as a filter for the OrderEntity objects to return</param>
		/// <param name="customerEntityInstance">CustomerEntity instance to use as a filter for the OrderEntity objects to return</param>
		/// <param name="deliveryInformationEntityInstance">DeliveryInformationEntity instance to use as a filter for the OrderEntity objects to return</param>
		/// <param name="deliverypointEntityInstance">DeliverypointEntity instance to use as a filter for the OrderEntity objects to return</param>
		/// <param name="deliverypointEntity_Instance">DeliverypointEntity instance to use as a filter for the OrderEntity objects to return</param>
		/// <param name="orderEntityInstance">OrderEntity instance to use as a filter for the OrderEntity objects to return</param>
		/// <param name="outletEntityInstance">OutletEntity instance to use as a filter for the OrderEntity objects to return</param>
		/// <param name="serviceMethodEntityInstance">ServiceMethodEntity instance to use as a filter for the OrderEntity objects to return</param>
		/// <returns>Amount of entities affected, if the used persistent storage has rowcounting enabled.</returns>
		public int UpdateMultiManyToOne(OrderEntity entityWithNewValues, IEntity checkoutMethodEntityInstance, IEntity clientEntityInstance, IEntity companyEntityInstance, IEntity currencyEntityInstance, IEntity customerEntityInstance, IEntity deliveryInformationEntityInstance, IEntity deliverypointEntityInstance, IEntity deliverypointEntity_Instance, IEntity orderEntityInstance, IEntity outletEntityInstance, IEntity serviceMethodEntityInstance)
		{
			return DAOFactory.CreateOrderDAO().UpdateMulti(entityWithNewValues, this.Transaction, checkoutMethodEntityInstance, clientEntityInstance, companyEntityInstance, currencyEntityInstance, customerEntityInstance, deliveryInformationEntityInstance, deliverypointEntityInstance, deliverypointEntity_Instance, orderEntityInstance, outletEntityInstance, serviceMethodEntityInstance);
		}

		/// <summary> Retrieves in this OrderCollection object all OrderEntity objects which are related via a  Relation of type 'm:n' with the passed in CategoryEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="categoryInstance">CategoryEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCategoryCollectionViaMessage(IEntity categoryInstance)
		{
			return GetMultiManyToManyUsingCategoryCollectionViaMessage(categoryInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this OrderCollection object all OrderEntity objects which are related via a  relation of type 'm:n' with the passed in CategoryEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="categoryInstance">CategoryEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCategoryCollectionViaMessage(IEntity categoryInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingCategoryCollectionViaMessage(categoryInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this OrderCollection object all OrderEntity objects which are related via a Relation of type 'm:n' with the passed in CategoryEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="categoryInstance">CategoryEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCategoryCollectionViaMessage(IEntity categoryInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingCategoryCollectionViaMessage(categoryInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this OrderCollection object all OrderEntity objects which are related via a  relation of type 'm:n' with the passed in CategoryEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="categoryInstance">CategoryEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingCategoryCollectionViaMessage(IEntity categoryInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateOrderDAO().GetMultiUsingCategoryCollectionViaMessage(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, categoryInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this OrderCollection object all OrderEntity objects which are related via a  relation of type 'm:n' with the passed in CategoryEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="categoryInstance">CategoryEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCategoryCollectionViaMessage(IEntity categoryInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateOrderDAO().GetMultiUsingCategoryCollectionViaMessage(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, categoryInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this OrderCollection object all OrderEntity objects which are related via a  Relation of type 'm:n' with the passed in CategoryEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="categoryInstance">CategoryEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCategoryCollectionViaOrderitem(IEntity categoryInstance)
		{
			return GetMultiManyToManyUsingCategoryCollectionViaOrderitem(categoryInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this OrderCollection object all OrderEntity objects which are related via a  relation of type 'm:n' with the passed in CategoryEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="categoryInstance">CategoryEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCategoryCollectionViaOrderitem(IEntity categoryInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingCategoryCollectionViaOrderitem(categoryInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this OrderCollection object all OrderEntity objects which are related via a Relation of type 'm:n' with the passed in CategoryEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="categoryInstance">CategoryEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCategoryCollectionViaOrderitem(IEntity categoryInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingCategoryCollectionViaOrderitem(categoryInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this OrderCollection object all OrderEntity objects which are related via a  relation of type 'm:n' with the passed in CategoryEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="categoryInstance">CategoryEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingCategoryCollectionViaOrderitem(IEntity categoryInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateOrderDAO().GetMultiUsingCategoryCollectionViaOrderitem(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, categoryInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this OrderCollection object all OrderEntity objects which are related via a  relation of type 'm:n' with the passed in CategoryEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="categoryInstance">CategoryEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCategoryCollectionViaOrderitem(IEntity categoryInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateOrderDAO().GetMultiUsingCategoryCollectionViaOrderitem(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, categoryInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this OrderCollection object all OrderEntity objects which are related via a  Relation of type 'm:n' with the passed in ClientEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="clientInstance">ClientEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingClientCollectionViaOrder(IEntity clientInstance)
		{
			return GetMultiManyToManyUsingClientCollectionViaOrder(clientInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this OrderCollection object all OrderEntity objects which are related via a  relation of type 'm:n' with the passed in ClientEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="clientInstance">ClientEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingClientCollectionViaOrder(IEntity clientInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingClientCollectionViaOrder(clientInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this OrderCollection object all OrderEntity objects which are related via a Relation of type 'm:n' with the passed in ClientEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="clientInstance">ClientEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingClientCollectionViaOrder(IEntity clientInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingClientCollectionViaOrder(clientInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this OrderCollection object all OrderEntity objects which are related via a  relation of type 'm:n' with the passed in ClientEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="clientInstance">ClientEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingClientCollectionViaOrder(IEntity clientInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateOrderDAO().GetMultiUsingClientCollectionViaOrder(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, clientInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this OrderCollection object all OrderEntity objects which are related via a  relation of type 'm:n' with the passed in ClientEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="clientInstance">ClientEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingClientCollectionViaOrder(IEntity clientInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateOrderDAO().GetMultiUsingClientCollectionViaOrder(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, clientInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this OrderCollection object all OrderEntity objects which are related via a  Relation of type 'm:n' with the passed in CompanyEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="companyInstance">CompanyEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCompanyCollectionViaOrder(IEntity companyInstance)
		{
			return GetMultiManyToManyUsingCompanyCollectionViaOrder(companyInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this OrderCollection object all OrderEntity objects which are related via a  relation of type 'm:n' with the passed in CompanyEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="companyInstance">CompanyEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCompanyCollectionViaOrder(IEntity companyInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingCompanyCollectionViaOrder(companyInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this OrderCollection object all OrderEntity objects which are related via a Relation of type 'm:n' with the passed in CompanyEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="companyInstance">CompanyEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCompanyCollectionViaOrder(IEntity companyInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingCompanyCollectionViaOrder(companyInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this OrderCollection object all OrderEntity objects which are related via a  relation of type 'm:n' with the passed in CompanyEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="companyInstance">CompanyEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingCompanyCollectionViaOrder(IEntity companyInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateOrderDAO().GetMultiUsingCompanyCollectionViaOrder(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, companyInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this OrderCollection object all OrderEntity objects which are related via a  relation of type 'm:n' with the passed in CompanyEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="companyInstance">CompanyEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCompanyCollectionViaOrder(IEntity companyInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateOrderDAO().GetMultiUsingCompanyCollectionViaOrder(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, companyInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this OrderCollection object all OrderEntity objects which are related via a  Relation of type 'm:n' with the passed in CurrencyEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="currencyInstance">CurrencyEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCurrencyCollectionViaOrder(IEntity currencyInstance)
		{
			return GetMultiManyToManyUsingCurrencyCollectionViaOrder(currencyInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this OrderCollection object all OrderEntity objects which are related via a  relation of type 'm:n' with the passed in CurrencyEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="currencyInstance">CurrencyEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCurrencyCollectionViaOrder(IEntity currencyInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingCurrencyCollectionViaOrder(currencyInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this OrderCollection object all OrderEntity objects which are related via a Relation of type 'm:n' with the passed in CurrencyEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="currencyInstance">CurrencyEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCurrencyCollectionViaOrder(IEntity currencyInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingCurrencyCollectionViaOrder(currencyInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this OrderCollection object all OrderEntity objects which are related via a  relation of type 'm:n' with the passed in CurrencyEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="currencyInstance">CurrencyEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingCurrencyCollectionViaOrder(IEntity currencyInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateOrderDAO().GetMultiUsingCurrencyCollectionViaOrder(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, currencyInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this OrderCollection object all OrderEntity objects which are related via a  relation of type 'm:n' with the passed in CurrencyEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="currencyInstance">CurrencyEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCurrencyCollectionViaOrder(IEntity currencyInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateOrderDAO().GetMultiUsingCurrencyCollectionViaOrder(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, currencyInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this OrderCollection object all OrderEntity objects which are related via a  Relation of type 'm:n' with the passed in CustomerEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="customerInstance">CustomerEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCustomerCollectionViaOrder(IEntity customerInstance)
		{
			return GetMultiManyToManyUsingCustomerCollectionViaOrder(customerInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this OrderCollection object all OrderEntity objects which are related via a  relation of type 'm:n' with the passed in CustomerEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="customerInstance">CustomerEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCustomerCollectionViaOrder(IEntity customerInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingCustomerCollectionViaOrder(customerInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this OrderCollection object all OrderEntity objects which are related via a Relation of type 'm:n' with the passed in CustomerEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="customerInstance">CustomerEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCustomerCollectionViaOrder(IEntity customerInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingCustomerCollectionViaOrder(customerInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this OrderCollection object all OrderEntity objects which are related via a  relation of type 'm:n' with the passed in CustomerEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="customerInstance">CustomerEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingCustomerCollectionViaOrder(IEntity customerInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateOrderDAO().GetMultiUsingCustomerCollectionViaOrder(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, customerInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this OrderCollection object all OrderEntity objects which are related via a  relation of type 'm:n' with the passed in CustomerEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="customerInstance">CustomerEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCustomerCollectionViaOrder(IEntity customerInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateOrderDAO().GetMultiUsingCustomerCollectionViaOrder(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, customerInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this OrderCollection object all OrderEntity objects which are related via a  Relation of type 'm:n' with the passed in DeliverypointEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="deliverypointInstance">DeliverypointEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingDeliverypointCollectionViaMessage(IEntity deliverypointInstance)
		{
			return GetMultiManyToManyUsingDeliverypointCollectionViaMessage(deliverypointInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this OrderCollection object all OrderEntity objects which are related via a  relation of type 'm:n' with the passed in DeliverypointEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="deliverypointInstance">DeliverypointEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingDeliverypointCollectionViaMessage(IEntity deliverypointInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingDeliverypointCollectionViaMessage(deliverypointInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this OrderCollection object all OrderEntity objects which are related via a Relation of type 'm:n' with the passed in DeliverypointEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="deliverypointInstance">DeliverypointEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingDeliverypointCollectionViaMessage(IEntity deliverypointInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingDeliverypointCollectionViaMessage(deliverypointInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this OrderCollection object all OrderEntity objects which are related via a  relation of type 'm:n' with the passed in DeliverypointEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="deliverypointInstance">DeliverypointEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingDeliverypointCollectionViaMessage(IEntity deliverypointInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateOrderDAO().GetMultiUsingDeliverypointCollectionViaMessage(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, deliverypointInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this OrderCollection object all OrderEntity objects which are related via a  relation of type 'm:n' with the passed in DeliverypointEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="deliverypointInstance">DeliverypointEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingDeliverypointCollectionViaMessage(IEntity deliverypointInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateOrderDAO().GetMultiUsingDeliverypointCollectionViaMessage(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, deliverypointInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this OrderCollection object all OrderEntity objects which are related via a  Relation of type 'm:n' with the passed in DeliverypointEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="deliverypointInstance">DeliverypointEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingDeliverypointCollectionViaOrder(IEntity deliverypointInstance)
		{
			return GetMultiManyToManyUsingDeliverypointCollectionViaOrder(deliverypointInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this OrderCollection object all OrderEntity objects which are related via a  relation of type 'm:n' with the passed in DeliverypointEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="deliverypointInstance">DeliverypointEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingDeliverypointCollectionViaOrder(IEntity deliverypointInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingDeliverypointCollectionViaOrder(deliverypointInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this OrderCollection object all OrderEntity objects which are related via a Relation of type 'm:n' with the passed in DeliverypointEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="deliverypointInstance">DeliverypointEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingDeliverypointCollectionViaOrder(IEntity deliverypointInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingDeliverypointCollectionViaOrder(deliverypointInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this OrderCollection object all OrderEntity objects which are related via a  relation of type 'm:n' with the passed in DeliverypointEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="deliverypointInstance">DeliverypointEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingDeliverypointCollectionViaOrder(IEntity deliverypointInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateOrderDAO().GetMultiUsingDeliverypointCollectionViaOrder(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, deliverypointInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this OrderCollection object all OrderEntity objects which are related via a  relation of type 'm:n' with the passed in DeliverypointEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="deliverypointInstance">DeliverypointEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingDeliverypointCollectionViaOrder(IEntity deliverypointInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateOrderDAO().GetMultiUsingDeliverypointCollectionViaOrder(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, deliverypointInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this OrderCollection object all OrderEntity objects which are related via a  Relation of type 'm:n' with the passed in EntertainmentEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="entertainmentInstance">EntertainmentEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingEntertainmentCollectionViaMessage(IEntity entertainmentInstance)
		{
			return GetMultiManyToManyUsingEntertainmentCollectionViaMessage(entertainmentInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this OrderCollection object all OrderEntity objects which are related via a  relation of type 'm:n' with the passed in EntertainmentEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="entertainmentInstance">EntertainmentEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingEntertainmentCollectionViaMessage(IEntity entertainmentInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingEntertainmentCollectionViaMessage(entertainmentInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this OrderCollection object all OrderEntity objects which are related via a Relation of type 'm:n' with the passed in EntertainmentEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="entertainmentInstance">EntertainmentEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingEntertainmentCollectionViaMessage(IEntity entertainmentInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingEntertainmentCollectionViaMessage(entertainmentInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this OrderCollection object all OrderEntity objects which are related via a  relation of type 'm:n' with the passed in EntertainmentEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="entertainmentInstance">EntertainmentEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingEntertainmentCollectionViaMessage(IEntity entertainmentInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateOrderDAO().GetMultiUsingEntertainmentCollectionViaMessage(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, entertainmentInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this OrderCollection object all OrderEntity objects which are related via a  relation of type 'm:n' with the passed in EntertainmentEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="entertainmentInstance">EntertainmentEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingEntertainmentCollectionViaMessage(IEntity entertainmentInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateOrderDAO().GetMultiUsingEntertainmentCollectionViaMessage(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, entertainmentInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this OrderCollection object all OrderEntity objects which are related via a  Relation of type 'm:n' with the passed in MediaEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="mediaInstance">MediaEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingMediaCollectionViaMessage(IEntity mediaInstance)
		{
			return GetMultiManyToManyUsingMediaCollectionViaMessage(mediaInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this OrderCollection object all OrderEntity objects which are related via a  relation of type 'm:n' with the passed in MediaEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="mediaInstance">MediaEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingMediaCollectionViaMessage(IEntity mediaInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingMediaCollectionViaMessage(mediaInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this OrderCollection object all OrderEntity objects which are related via a Relation of type 'm:n' with the passed in MediaEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="mediaInstance">MediaEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingMediaCollectionViaMessage(IEntity mediaInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingMediaCollectionViaMessage(mediaInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this OrderCollection object all OrderEntity objects which are related via a  relation of type 'm:n' with the passed in MediaEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="mediaInstance">MediaEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingMediaCollectionViaMessage(IEntity mediaInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateOrderDAO().GetMultiUsingMediaCollectionViaMessage(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, mediaInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this OrderCollection object all OrderEntity objects which are related via a  relation of type 'm:n' with the passed in MediaEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="mediaInstance">MediaEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingMediaCollectionViaMessage(IEntity mediaInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateOrderDAO().GetMultiUsingMediaCollectionViaMessage(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, mediaInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this OrderCollection object all OrderEntity objects which are related via a  Relation of type 'm:n' with the passed in ProductEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="productInstance">ProductEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingProductCollectionViaOrderitem(IEntity productInstance)
		{
			return GetMultiManyToManyUsingProductCollectionViaOrderitem(productInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this OrderCollection object all OrderEntity objects which are related via a  relation of type 'm:n' with the passed in ProductEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="productInstance">ProductEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingProductCollectionViaOrderitem(IEntity productInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingProductCollectionViaOrderitem(productInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this OrderCollection object all OrderEntity objects which are related via a Relation of type 'm:n' with the passed in ProductEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="productInstance">ProductEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingProductCollectionViaOrderitem(IEntity productInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingProductCollectionViaOrderitem(productInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this OrderCollection object all OrderEntity objects which are related via a  relation of type 'm:n' with the passed in ProductEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="productInstance">ProductEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingProductCollectionViaOrderitem(IEntity productInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateOrderDAO().GetMultiUsingProductCollectionViaOrderitem(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, productInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this OrderCollection object all OrderEntity objects which are related via a  relation of type 'm:n' with the passed in ProductEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="productInstance">ProductEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingProductCollectionViaOrderitem(IEntity productInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateOrderDAO().GetMultiUsingProductCollectionViaOrderitem(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, productInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this OrderCollection object all OrderEntity objects which are related via a  Relation of type 'm:n' with the passed in SupportpoolEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="supportpoolInstance">SupportpoolEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingSupportpoolCollectionViaOrderRoutestephandler(IEntity supportpoolInstance)
		{
			return GetMultiManyToManyUsingSupportpoolCollectionViaOrderRoutestephandler(supportpoolInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this OrderCollection object all OrderEntity objects which are related via a  relation of type 'm:n' with the passed in SupportpoolEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="supportpoolInstance">SupportpoolEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingSupportpoolCollectionViaOrderRoutestephandler(IEntity supportpoolInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingSupportpoolCollectionViaOrderRoutestephandler(supportpoolInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this OrderCollection object all OrderEntity objects which are related via a Relation of type 'm:n' with the passed in SupportpoolEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="supportpoolInstance">SupportpoolEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingSupportpoolCollectionViaOrderRoutestephandler(IEntity supportpoolInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingSupportpoolCollectionViaOrderRoutestephandler(supportpoolInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this OrderCollection object all OrderEntity objects which are related via a  relation of type 'm:n' with the passed in SupportpoolEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="supportpoolInstance">SupportpoolEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingSupportpoolCollectionViaOrderRoutestephandler(IEntity supportpoolInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateOrderDAO().GetMultiUsingSupportpoolCollectionViaOrderRoutestephandler(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, supportpoolInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this OrderCollection object all OrderEntity objects which are related via a  relation of type 'm:n' with the passed in SupportpoolEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="supportpoolInstance">SupportpoolEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingSupportpoolCollectionViaOrderRoutestephandler(IEntity supportpoolInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateOrderDAO().GetMultiUsingSupportpoolCollectionViaOrderRoutestephandler(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, supportpoolInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this OrderCollection object all OrderEntity objects which are related via a  Relation of type 'm:n' with the passed in SupportpoolEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="supportpoolInstance">SupportpoolEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingSupportpoolCollectionViaOrderRoutestephandlerHistory(IEntity supportpoolInstance)
		{
			return GetMultiManyToManyUsingSupportpoolCollectionViaOrderRoutestephandlerHistory(supportpoolInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this OrderCollection object all OrderEntity objects which are related via a  relation of type 'm:n' with the passed in SupportpoolEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="supportpoolInstance">SupportpoolEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingSupportpoolCollectionViaOrderRoutestephandlerHistory(IEntity supportpoolInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingSupportpoolCollectionViaOrderRoutestephandlerHistory(supportpoolInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this OrderCollection object all OrderEntity objects which are related via a Relation of type 'm:n' with the passed in SupportpoolEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="supportpoolInstance">SupportpoolEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingSupportpoolCollectionViaOrderRoutestephandlerHistory(IEntity supportpoolInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingSupportpoolCollectionViaOrderRoutestephandlerHistory(supportpoolInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this OrderCollection object all OrderEntity objects which are related via a  relation of type 'm:n' with the passed in SupportpoolEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="supportpoolInstance">SupportpoolEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingSupportpoolCollectionViaOrderRoutestephandlerHistory(IEntity supportpoolInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateOrderDAO().GetMultiUsingSupportpoolCollectionViaOrderRoutestephandlerHistory(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, supportpoolInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this OrderCollection object all OrderEntity objects which are related via a  relation of type 'm:n' with the passed in SupportpoolEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="supportpoolInstance">SupportpoolEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingSupportpoolCollectionViaOrderRoutestephandlerHistory(IEntity supportpoolInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateOrderDAO().GetMultiUsingSupportpoolCollectionViaOrderRoutestephandlerHistory(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, supportpoolInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this OrderCollection object all OrderEntity objects which are related via a  Relation of type 'm:n' with the passed in TerminalEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="terminalInstance">TerminalEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingTerminalCollectionViaOrderRoutestephandler(IEntity terminalInstance)
		{
			return GetMultiManyToManyUsingTerminalCollectionViaOrderRoutestephandler(terminalInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this OrderCollection object all OrderEntity objects which are related via a  relation of type 'm:n' with the passed in TerminalEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="terminalInstance">TerminalEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingTerminalCollectionViaOrderRoutestephandler(IEntity terminalInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingTerminalCollectionViaOrderRoutestephandler(terminalInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this OrderCollection object all OrderEntity objects which are related via a Relation of type 'm:n' with the passed in TerminalEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="terminalInstance">TerminalEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingTerminalCollectionViaOrderRoutestephandler(IEntity terminalInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingTerminalCollectionViaOrderRoutestephandler(terminalInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this OrderCollection object all OrderEntity objects which are related via a  relation of type 'm:n' with the passed in TerminalEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="terminalInstance">TerminalEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingTerminalCollectionViaOrderRoutestephandler(IEntity terminalInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateOrderDAO().GetMultiUsingTerminalCollectionViaOrderRoutestephandler(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, terminalInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this OrderCollection object all OrderEntity objects which are related via a  relation of type 'm:n' with the passed in TerminalEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="terminalInstance">TerminalEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingTerminalCollectionViaOrderRoutestephandler(IEntity terminalInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateOrderDAO().GetMultiUsingTerminalCollectionViaOrderRoutestephandler(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, terminalInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this OrderCollection object all OrderEntity objects which are related via a  Relation of type 'm:n' with the passed in TerminalEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="terminalInstance">TerminalEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingTerminalCollectionViaOrderRoutestephandler_(IEntity terminalInstance)
		{
			return GetMultiManyToManyUsingTerminalCollectionViaOrderRoutestephandler_(terminalInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this OrderCollection object all OrderEntity objects which are related via a  relation of type 'm:n' with the passed in TerminalEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="terminalInstance">TerminalEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingTerminalCollectionViaOrderRoutestephandler_(IEntity terminalInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingTerminalCollectionViaOrderRoutestephandler_(terminalInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this OrderCollection object all OrderEntity objects which are related via a Relation of type 'm:n' with the passed in TerminalEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="terminalInstance">TerminalEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingTerminalCollectionViaOrderRoutestephandler_(IEntity terminalInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingTerminalCollectionViaOrderRoutestephandler_(terminalInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this OrderCollection object all OrderEntity objects which are related via a  relation of type 'm:n' with the passed in TerminalEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="terminalInstance">TerminalEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingTerminalCollectionViaOrderRoutestephandler_(IEntity terminalInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateOrderDAO().GetMultiUsingTerminalCollectionViaOrderRoutestephandler_(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, terminalInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this OrderCollection object all OrderEntity objects which are related via a  relation of type 'm:n' with the passed in TerminalEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="terminalInstance">TerminalEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingTerminalCollectionViaOrderRoutestephandler_(IEntity terminalInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateOrderDAO().GetMultiUsingTerminalCollectionViaOrderRoutestephandler_(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, terminalInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this OrderCollection object all OrderEntity objects which are related via a  Relation of type 'm:n' with the passed in TerminalEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="terminalInstance">TerminalEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingTerminalCollectionViaOrderRoutestephandlerHistory(IEntity terminalInstance)
		{
			return GetMultiManyToManyUsingTerminalCollectionViaOrderRoutestephandlerHistory(terminalInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this OrderCollection object all OrderEntity objects which are related via a  relation of type 'm:n' with the passed in TerminalEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="terminalInstance">TerminalEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingTerminalCollectionViaOrderRoutestephandlerHistory(IEntity terminalInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingTerminalCollectionViaOrderRoutestephandlerHistory(terminalInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this OrderCollection object all OrderEntity objects which are related via a Relation of type 'm:n' with the passed in TerminalEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="terminalInstance">TerminalEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingTerminalCollectionViaOrderRoutestephandlerHistory(IEntity terminalInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingTerminalCollectionViaOrderRoutestephandlerHistory(terminalInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this OrderCollection object all OrderEntity objects which are related via a  relation of type 'm:n' with the passed in TerminalEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="terminalInstance">TerminalEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingTerminalCollectionViaOrderRoutestephandlerHistory(IEntity terminalInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateOrderDAO().GetMultiUsingTerminalCollectionViaOrderRoutestephandlerHistory(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, terminalInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this OrderCollection object all OrderEntity objects which are related via a  relation of type 'm:n' with the passed in TerminalEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="terminalInstance">TerminalEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingTerminalCollectionViaOrderRoutestephandlerHistory(IEntity terminalInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateOrderDAO().GetMultiUsingTerminalCollectionViaOrderRoutestephandlerHistory(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, terminalInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this OrderCollection object all OrderEntity objects which are related via a  Relation of type 'm:n' with the passed in TerminalEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="terminalInstance">TerminalEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingTerminalCollectionViaOrderRoutestephandlerHistory_(IEntity terminalInstance)
		{
			return GetMultiManyToManyUsingTerminalCollectionViaOrderRoutestephandlerHistory_(terminalInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this OrderCollection object all OrderEntity objects which are related via a  relation of type 'm:n' with the passed in TerminalEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="terminalInstance">TerminalEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingTerminalCollectionViaOrderRoutestephandlerHistory_(IEntity terminalInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingTerminalCollectionViaOrderRoutestephandlerHistory_(terminalInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this OrderCollection object all OrderEntity objects which are related via a Relation of type 'm:n' with the passed in TerminalEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="terminalInstance">TerminalEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingTerminalCollectionViaOrderRoutestephandlerHistory_(IEntity terminalInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingTerminalCollectionViaOrderRoutestephandlerHistory_(terminalInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this OrderCollection object all OrderEntity objects which are related via a  relation of type 'm:n' with the passed in TerminalEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="terminalInstance">TerminalEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingTerminalCollectionViaOrderRoutestephandlerHistory_(IEntity terminalInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateOrderDAO().GetMultiUsingTerminalCollectionViaOrderRoutestephandlerHistory_(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, terminalInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this OrderCollection object all OrderEntity objects which are related via a  relation of type 'm:n' with the passed in TerminalEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="terminalInstance">TerminalEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingTerminalCollectionViaOrderRoutestephandlerHistory_(IEntity terminalInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateOrderDAO().GetMultiUsingTerminalCollectionViaOrderRoutestephandlerHistory_(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, terminalInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this OrderCollection object all OrderEntity objects which are related via a  Relation of type 'm:n' with the passed in TerminalLogFileEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="terminalLogFileInstance">TerminalLogFileEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingTerminalLogFileCollectionViaTerminalLog(IEntity terminalLogFileInstance)
		{
			return GetMultiManyToManyUsingTerminalLogFileCollectionViaTerminalLog(terminalLogFileInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this OrderCollection object all OrderEntity objects which are related via a  relation of type 'm:n' with the passed in TerminalLogFileEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="terminalLogFileInstance">TerminalLogFileEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingTerminalLogFileCollectionViaTerminalLog(IEntity terminalLogFileInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingTerminalLogFileCollectionViaTerminalLog(terminalLogFileInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this OrderCollection object all OrderEntity objects which are related via a Relation of type 'm:n' with the passed in TerminalLogFileEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="terminalLogFileInstance">TerminalLogFileEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingTerminalLogFileCollectionViaTerminalLog(IEntity terminalLogFileInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingTerminalLogFileCollectionViaTerminalLog(terminalLogFileInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this OrderCollection object all OrderEntity objects which are related via a  relation of type 'm:n' with the passed in TerminalLogFileEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="terminalLogFileInstance">TerminalLogFileEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingTerminalLogFileCollectionViaTerminalLog(IEntity terminalLogFileInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateOrderDAO().GetMultiUsingTerminalLogFileCollectionViaTerminalLog(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, terminalLogFileInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this OrderCollection object all OrderEntity objects which are related via a  relation of type 'm:n' with the passed in TerminalLogFileEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="terminalLogFileInstance">TerminalLogFileEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingTerminalLogFileCollectionViaTerminalLog(IEntity terminalLogFileInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateOrderDAO().GetMultiUsingTerminalLogFileCollectionViaTerminalLog(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, terminalLogFileInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves Entity rows in a datatable which match the specified filter. It will always create a new connection to the database.</summary>
		/// <param name="selectFilter">A predicate or predicate expression which should be used as filter for the entities to retrieve.</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>DataTable with the rows requested.</returns>
		public static DataTable GetMultiAsDataTable(IPredicate selectFilter, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiAsDataTable(selectFilter, maxNumberOfItemsToReturn, sortClauses, null, 0, 0);
		}

		/// <summary> Retrieves Entity rows in a datatable which match the specified filter. It will always create a new connection to the database.</summary>
		/// <param name="selectFilter">A predicate or predicate expression which should be used as filter for the entities to retrieve.</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="relations">The set of relations to walk to construct to total query.</param>
		/// <returns>DataTable with the rows requested.</returns>
		public static DataTable GetMultiAsDataTable(IPredicate selectFilter, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IRelationCollection relations)
		{
			return GetMultiAsDataTable(selectFilter, maxNumberOfItemsToReturn, sortClauses, relations, 0, 0);
		}
		
		/// <summary> Retrieves Entity rows in a datatable which match the specified filter. It will always create a new connection to the database.</summary>
		/// <param name="selectFilter">A predicate or predicate expression which should be used as filter for the entities to retrieve.</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="relations">The set of relations to walk to construct to total query.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>DataTable with the rows requested.</returns>
		public static DataTable GetMultiAsDataTable(IPredicate selectFilter, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IRelationCollection relations, int pageNumber, int pageSize)
		{
			OrderDAO dao = DAOFactory.CreateOrderDAO();
			return dao.GetMultiAsDataTable(maxNumberOfItemsToReturn, sortClauses, selectFilter, relations, pageNumber, pageSize);
		}


		
		/// <summary> Gets a scalar value, calculated with the aggregate. the field index specified is the field the aggregate are applied on.</summary>
		/// <param name="fieldIndex">Field index of field to which to apply the aggregate function and expression</param>
		/// <param name="aggregateToApply">Aggregate function to apply. </param>
		/// <returns>the scalar value requested</returns>
		public object GetScalar(OrderFieldIndex fieldIndex, AggregateFunction aggregateToApply)
		{
			return GetScalar(fieldIndex, null, aggregateToApply, null, null, null);
		}

		/// <summary> Gets a scalar value, calculated with the aggregate and expression specified. the field index specified is the field the expression and aggregate are applied on.</summary>
		/// <param name="fieldIndex">Field index of field to which to apply the aggregate function and expression</param>
		/// <param name="expressionToExecute">The expression to execute. Can be null</param>
		/// <param name="aggregateToApply">Aggregate function to apply. </param>
		/// <returns>the scalar value requested</returns>
		public object GetScalar(OrderFieldIndex fieldIndex, IExpression expressionToExecute, AggregateFunction aggregateToApply)
		{
			return GetScalar(fieldIndex, expressionToExecute, aggregateToApply, null, null, null);
		}

		/// <summary> Gets a scalar value, calculated with the aggregate and expression specified. the field index specified is the field the expression and aggregate are applied on.</summary>
		/// <param name="fieldIndex">Field index of field to which to apply the aggregate function and expression</param>
		/// <param name="expressionToExecute">The expression to execute. Can be null</param>
		/// <param name="aggregateToApply">Aggregate function to apply. </param>
		/// <param name="filter">The filter to apply to retrieve the scalar</param>
		/// <returns>the scalar value requested</returns>
		public object GetScalar(OrderFieldIndex fieldIndex, IExpression expressionToExecute, AggregateFunction aggregateToApply, IPredicate filter)
		{
			return GetScalar(fieldIndex, expressionToExecute, aggregateToApply, filter, null, null);
		}

		/// <summary> Gets a scalar value, calculated with the aggregate and expression specified. the field index specified is the field the expression and aggregate are applied on.</summary>
		/// <param name="fieldIndex">Field index of field to which to apply the aggregate function and expression</param>
		/// <param name="expressionToExecute">The expression to execute. Can be null</param>
		/// <param name="aggregateToApply">Aggregate function to apply. </param>
		/// <param name="filter">The filter to apply to retrieve the scalar</param>
		/// <param name="groupByClause">The groupby clause to apply to retrieve the scalar</param>
		/// <returns>the scalar value requested</returns>
		public object GetScalar(OrderFieldIndex fieldIndex, IExpression expressionToExecute, AggregateFunction aggregateToApply, IPredicate filter, IGroupByCollection groupByClause)
		{
			return GetScalar(fieldIndex, expressionToExecute, aggregateToApply, filter, null, groupByClause);
		}

		/// <summary> Gets a scalar value, calculated with the aggregate and expression specified. the field index specified is the field the expression and aggregate are applied on.</summary>
		/// <param name="fieldIndex">Field index of field to which to apply the aggregate function and expression</param>
		/// <param name="expressionToExecute">The expression to execute. Can be null</param>
		/// <param name="aggregateToApply">Aggregate function to apply. </param>
		/// <param name="filter">The filter to apply to retrieve the scalar</param>
		/// <param name="relations">The relations to walk</param>
		/// <param name="groupByClause">The groupby clause to apply to retrieve the scalar</param>
		/// <returns>the scalar value requested</returns>
		public virtual object GetScalar(OrderFieldIndex fieldIndex, IExpression expressionToExecute, AggregateFunction aggregateToApply, IPredicate filter, IRelationCollection relations, IGroupByCollection groupByClause)
		{
			EntityFields fields = new EntityFields(1);
			fields[0] = EntityFieldFactory.Create(fieldIndex);
			if((fields[0].ExpressionToApply == null) || (expressionToExecute != null))
			{
				fields[0].ExpressionToApply = expressionToExecute;
			}
			if((fields[0].AggregateFunctionToApply == AggregateFunction.None) || (aggregateToApply != AggregateFunction.None))
			{
				fields[0].AggregateFunctionToApply = aggregateToApply;
			}
			return DAOFactory.CreateOrderDAO().GetScalar(fields, this.Transaction, filter, relations, groupByClause);
		}
		
		/// <summary>Creats a new DAO instance so code which is in the base class can still use the proper DAO object.</summary>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateOrderDAO();
		}
		
		/// <summary>Creates a new transaction object</summary>
		/// <param name="levelOfIsolation">The level of isolation.</param>
		/// <param name="name">The name.</param>
		protected override ITransaction CreateTransaction( IsolationLevel levelOfIsolation, string name )
		{
			return new Transaction(levelOfIsolation, name);
		}

		#region Custom EntityCollection code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCollectionCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion
		
		#region Included Code

		#endregion
	}
}
