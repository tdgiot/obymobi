﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Data;
using System.Collections.Generic;
using System.ComponentModel;
using System.Xml;
#if !CF
using System.Runtime.Serialization;
#endif

using Obymobi.Data.EntityClasses;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.CollectionClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Collection class for storing and retrieving collections of CategoryEntity objects. </summary>
	[Serializable]
	public partial class CategoryCollection : CommonEntityCollection<CategoryEntity>
	{
		/// <summary> CTor</summary>
		public CategoryCollection():base(new CategoryEntityFactory())
		{
		}

		/// <summary> CTor</summary>
		/// <param name="initialContents">The initial contents of this collection.</param>
		public CategoryCollection(IEnumerable<CategoryEntity> initialContents):base(new CategoryEntityFactory())
		{
			AddRange(initialContents);
		}

		/// <summary> CTor</summary>
		/// <param name="entityFactoryToUse">The EntityFactory to use when creating entity objects during a GetMulti() call.</param>
		public CategoryCollection(IEntityFactory entityFactoryToUse):base(entityFactoryToUse)
		{
		}

		/// <summary> Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected CategoryCollection(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}


		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which have data in common with the specified related Entities.
		/// If one is omitted, that entity is not used as a filter. All current elements in the collection are removed from the collection.</summary>
		/// <param name="parentCategoryEntityInstance">CategoryEntity instance to use as a filter for the CategoryEntity objects to return</param>
		/// <param name="companyEntityInstance">CompanyEntity instance to use as a filter for the CategoryEntity objects to return</param>
		/// <param name="genericcategoryEntityInstance">GenericcategoryEntity instance to use as a filter for the CategoryEntity objects to return</param>
		/// <param name="menuEntityInstance">MenuEntity instance to use as a filter for the CategoryEntity objects to return</param>
		/// <param name="poscategoryEntityInstance">PoscategoryEntity instance to use as a filter for the CategoryEntity objects to return</param>
		/// <param name="productEntityInstance">ProductEntity instance to use as a filter for the CategoryEntity objects to return</param>
		/// <param name="routeEntityInstance">RouteEntity instance to use as a filter for the CategoryEntity objects to return</param>
		/// <param name="scheduleEntityInstance">ScheduleEntity instance to use as a filter for the CategoryEntity objects to return</param>
		/// <returns>true if succeeded, false otherwise</returns>
		public bool GetMultiManyToOne(IEntity parentCategoryEntityInstance, IEntity companyEntityInstance, IEntity genericcategoryEntityInstance, IEntity menuEntityInstance, IEntity poscategoryEntityInstance, IEntity productEntityInstance, IEntity routeEntityInstance, IEntity scheduleEntityInstance)
		{
			return GetMultiManyToOne(parentCategoryEntityInstance, companyEntityInstance, genericcategoryEntityInstance, menuEntityInstance, poscategoryEntityInstance, productEntityInstance, routeEntityInstance, scheduleEntityInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, null, 0, 0);
		}

		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which have data in common with the specified related Entities.
		/// If one is omitted, that entity is not used as a filter. All current elements in the collection are removed from the collection.</summary>
		/// <param name="parentCategoryEntityInstance">CategoryEntity instance to use as a filter for the CategoryEntity objects to return</param>
		/// <param name="companyEntityInstance">CompanyEntity instance to use as a filter for the CategoryEntity objects to return</param>
		/// <param name="genericcategoryEntityInstance">GenericcategoryEntity instance to use as a filter for the CategoryEntity objects to return</param>
		/// <param name="menuEntityInstance">MenuEntity instance to use as a filter for the CategoryEntity objects to return</param>
		/// <param name="poscategoryEntityInstance">PoscategoryEntity instance to use as a filter for the CategoryEntity objects to return</param>
		/// <param name="productEntityInstance">ProductEntity instance to use as a filter for the CategoryEntity objects to return</param>
		/// <param name="routeEntityInstance">RouteEntity instance to use as a filter for the CategoryEntity objects to return</param>
		/// <param name="scheduleEntityInstance">ScheduleEntity instance to use as a filter for the CategoryEntity objects to return</param>
		/// <param name="filter">Extra filter to limit the resultset. Predicate expression can be null, in which case it will be ignored.</param>
		/// <returns>true if succeeded, false otherwise</returns>
		public bool GetMultiManyToOne(IEntity parentCategoryEntityInstance, IEntity companyEntityInstance, IEntity genericcategoryEntityInstance, IEntity menuEntityInstance, IEntity poscategoryEntityInstance, IEntity productEntityInstance, IEntity routeEntityInstance, IEntity scheduleEntityInstance, IPredicateExpression filter)
		{
			return GetMultiManyToOne(parentCategoryEntityInstance, companyEntityInstance, genericcategoryEntityInstance, menuEntityInstance, poscategoryEntityInstance, productEntityInstance, routeEntityInstance, scheduleEntityInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, filter, 0, 0);
		}

		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which have data in common with the specified related Entities.
		/// If one is omitted, that entity is not used as a filter. All current elements in the collection are removed from the collection.</summary>
		/// <param name="parentCategoryEntityInstance">CategoryEntity instance to use as a filter for the CategoryEntity objects to return</param>
		/// <param name="companyEntityInstance">CompanyEntity instance to use as a filter for the CategoryEntity objects to return</param>
		/// <param name="genericcategoryEntityInstance">GenericcategoryEntity instance to use as a filter for the CategoryEntity objects to return</param>
		/// <param name="menuEntityInstance">MenuEntity instance to use as a filter for the CategoryEntity objects to return</param>
		/// <param name="poscategoryEntityInstance">PoscategoryEntity instance to use as a filter for the CategoryEntity objects to return</param>
		/// <param name="productEntityInstance">ProductEntity instance to use as a filter for the CategoryEntity objects to return</param>
		/// <param name="routeEntityInstance">RouteEntity instance to use as a filter for the CategoryEntity objects to return</param>
		/// <param name="scheduleEntityInstance">ScheduleEntity instance to use as a filter for the CategoryEntity objects to return</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="filter">Extra filter to limit the resultset. Predicate expression can be null, in which case it will be ignored.</param>
		/// <returns>true if succeeded, false otherwise</returns>
		public bool GetMultiManyToOne(IEntity parentCategoryEntityInstance, IEntity companyEntityInstance, IEntity genericcategoryEntityInstance, IEntity menuEntityInstance, IEntity poscategoryEntityInstance, IEntity productEntityInstance, IEntity routeEntityInstance, IEntity scheduleEntityInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPredicateExpression filter)
		{
			return GetMultiManyToOne(parentCategoryEntityInstance, companyEntityInstance, genericcategoryEntityInstance, menuEntityInstance, poscategoryEntityInstance, productEntityInstance, routeEntityInstance, scheduleEntityInstance, maxNumberOfItemsToReturn, sortClauses, filter, 0, 0);
		}

		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which have data in common with the specified related Entities.
		/// If one is omitted, that entity is not used as a filter. All current elements in the collection are removed from the collection.</summary>
		/// <param name="parentCategoryEntityInstance">CategoryEntity instance to use as a filter for the CategoryEntity objects to return</param>
		/// <param name="companyEntityInstance">CompanyEntity instance to use as a filter for the CategoryEntity objects to return</param>
		/// <param name="genericcategoryEntityInstance">GenericcategoryEntity instance to use as a filter for the CategoryEntity objects to return</param>
		/// <param name="menuEntityInstance">MenuEntity instance to use as a filter for the CategoryEntity objects to return</param>
		/// <param name="poscategoryEntityInstance">PoscategoryEntity instance to use as a filter for the CategoryEntity objects to return</param>
		/// <param name="productEntityInstance">ProductEntity instance to use as a filter for the CategoryEntity objects to return</param>
		/// <param name="routeEntityInstance">RouteEntity instance to use as a filter for the CategoryEntity objects to return</param>
		/// <param name="scheduleEntityInstance">ScheduleEntity instance to use as a filter for the CategoryEntity objects to return</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="filter">Extra filter to limit the resultset. Predicate expression can be null, in which case it will be ignored.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToOne(IEntity parentCategoryEntityInstance, IEntity companyEntityInstance, IEntity genericcategoryEntityInstance, IEntity menuEntityInstance, IEntity poscategoryEntityInstance, IEntity productEntityInstance, IEntity routeEntityInstance, IEntity scheduleEntityInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPredicateExpression filter, int pageNumber, int pageSize)
		{
			bool validParameters = false;
			validParameters |= (parentCategoryEntityInstance!=null);
			validParameters |= (companyEntityInstance!=null);
			validParameters |= (genericcategoryEntityInstance!=null);
			validParameters |= (menuEntityInstance!=null);
			validParameters |= (poscategoryEntityInstance!=null);
			validParameters |= (productEntityInstance!=null);
			validParameters |= (routeEntityInstance!=null);
			validParameters |= (scheduleEntityInstance!=null);
			if(!validParameters)
			{
				return GetMulti(filter, maxNumberOfItemsToReturn, sortClauses, null, pageNumber, pageSize);
			}
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateCategoryDAO().GetMulti(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, filter, parentCategoryEntityInstance, companyEntityInstance, genericcategoryEntityInstance, menuEntityInstance, poscategoryEntityInstance, productEntityInstance, routeEntityInstance, scheduleEntityInstance, pageNumber, pageSize);
		}

		/// <summary> Deletes from the persistent storage all Category entities which have data in common with the specified related Entities. If one is omitted, that entity is not used as a filter.</summary>
		/// <remarks>Runs directly on the persistent storage. It will not delete entity objects from the current collection.</remarks>
		/// <param name="parentCategoryEntityInstance">CategoryEntity instance to use as a filter for the CategoryEntity objects to return</param>
		/// <param name="companyEntityInstance">CompanyEntity instance to use as a filter for the CategoryEntity objects to return</param>
		/// <param name="genericcategoryEntityInstance">GenericcategoryEntity instance to use as a filter for the CategoryEntity objects to return</param>
		/// <param name="menuEntityInstance">MenuEntity instance to use as a filter for the CategoryEntity objects to return</param>
		/// <param name="poscategoryEntityInstance">PoscategoryEntity instance to use as a filter for the CategoryEntity objects to return</param>
		/// <param name="productEntityInstance">ProductEntity instance to use as a filter for the CategoryEntity objects to return</param>
		/// <param name="routeEntityInstance">RouteEntity instance to use as a filter for the CategoryEntity objects to return</param>
		/// <param name="scheduleEntityInstance">ScheduleEntity instance to use as a filter for the CategoryEntity objects to return</param>
		/// <returns>Amount of entities affected, if the used persistent storage has rowcounting enabled.</returns>
		public int DeleteMultiManyToOne(IEntity parentCategoryEntityInstance, IEntity companyEntityInstance, IEntity genericcategoryEntityInstance, IEntity menuEntityInstance, IEntity poscategoryEntityInstance, IEntity productEntityInstance, IEntity routeEntityInstance, IEntity scheduleEntityInstance)
		{
			return DAOFactory.CreateCategoryDAO().DeleteMulti(this.Transaction, parentCategoryEntityInstance, companyEntityInstance, genericcategoryEntityInstance, menuEntityInstance, poscategoryEntityInstance, productEntityInstance, routeEntityInstance, scheduleEntityInstance);
		}

		/// <summary> Updates in the persistent storage all Category entities which have data in common with the specified related Entities. If one is omitted, that entity is not used as a filter.
		/// Which fields are updated in those matching entities depends on which fields are <i>changed</i> in the passed in entity entityWithNewValues. The new values of these fields are read from entityWithNewValues. </summary>
		/// <param name="entityWithNewValues">CategoryEntity instance which holds the new values for the matching entities to update. Only changed fields are taken into account</param>
		/// <param name="parentCategoryEntityInstance">CategoryEntity instance to use as a filter for the CategoryEntity objects to return</param>
		/// <param name="companyEntityInstance">CompanyEntity instance to use as a filter for the CategoryEntity objects to return</param>
		/// <param name="genericcategoryEntityInstance">GenericcategoryEntity instance to use as a filter for the CategoryEntity objects to return</param>
		/// <param name="menuEntityInstance">MenuEntity instance to use as a filter for the CategoryEntity objects to return</param>
		/// <param name="poscategoryEntityInstance">PoscategoryEntity instance to use as a filter for the CategoryEntity objects to return</param>
		/// <param name="productEntityInstance">ProductEntity instance to use as a filter for the CategoryEntity objects to return</param>
		/// <param name="routeEntityInstance">RouteEntity instance to use as a filter for the CategoryEntity objects to return</param>
		/// <param name="scheduleEntityInstance">ScheduleEntity instance to use as a filter for the CategoryEntity objects to return</param>
		/// <returns>Amount of entities affected, if the used persistent storage has rowcounting enabled.</returns>
		public int UpdateMultiManyToOne(CategoryEntity entityWithNewValues, IEntity parentCategoryEntityInstance, IEntity companyEntityInstance, IEntity genericcategoryEntityInstance, IEntity menuEntityInstance, IEntity poscategoryEntityInstance, IEntity productEntityInstance, IEntity routeEntityInstance, IEntity scheduleEntityInstance)
		{
			return DAOFactory.CreateCategoryDAO().UpdateMulti(entityWithNewValues, this.Transaction, parentCategoryEntityInstance, companyEntityInstance, genericcategoryEntityInstance, menuEntityInstance, poscategoryEntityInstance, productEntityInstance, routeEntityInstance, scheduleEntityInstance);
		}

		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  Relation of type 'm:n' with the passed in AdvertisementEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="advertisementInstance">AdvertisementEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingAdvertisementCollectionViaMedium(IEntity advertisementInstance)
		{
			return GetMultiManyToManyUsingAdvertisementCollectionViaMedium(advertisementInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  relation of type 'm:n' with the passed in AdvertisementEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="advertisementInstance">AdvertisementEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingAdvertisementCollectionViaMedium(IEntity advertisementInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingAdvertisementCollectionViaMedium(advertisementInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a Relation of type 'm:n' with the passed in AdvertisementEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="advertisementInstance">AdvertisementEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingAdvertisementCollectionViaMedium(IEntity advertisementInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingAdvertisementCollectionViaMedium(advertisementInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  relation of type 'm:n' with the passed in AdvertisementEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="advertisementInstance">AdvertisementEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingAdvertisementCollectionViaMedium(IEntity advertisementInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateCategoryDAO().GetMultiUsingAdvertisementCollectionViaMedium(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, advertisementInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  relation of type 'm:n' with the passed in AdvertisementEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="advertisementInstance">AdvertisementEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingAdvertisementCollectionViaMedium(IEntity advertisementInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateCategoryDAO().GetMultiUsingAdvertisementCollectionViaMedium(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, advertisementInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  Relation of type 'm:n' with the passed in AlterationEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="alterationInstance">AlterationEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingAlterationCollectionViaMedium(IEntity alterationInstance)
		{
			return GetMultiManyToManyUsingAlterationCollectionViaMedium(alterationInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  relation of type 'm:n' with the passed in AlterationEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="alterationInstance">AlterationEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingAlterationCollectionViaMedium(IEntity alterationInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingAlterationCollectionViaMedium(alterationInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a Relation of type 'm:n' with the passed in AlterationEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="alterationInstance">AlterationEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingAlterationCollectionViaMedium(IEntity alterationInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingAlterationCollectionViaMedium(alterationInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  relation of type 'm:n' with the passed in AlterationEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="alterationInstance">AlterationEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingAlterationCollectionViaMedium(IEntity alterationInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateCategoryDAO().GetMultiUsingAlterationCollectionViaMedium(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, alterationInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  relation of type 'm:n' with the passed in AlterationEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="alterationInstance">AlterationEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingAlterationCollectionViaMedium(IEntity alterationInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateCategoryDAO().GetMultiUsingAlterationCollectionViaMedium(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, alterationInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  Relation of type 'm:n' with the passed in AlterationEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="alterationInstance">AlterationEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingAlterationCollectionViaCategoryAlteration(IEntity alterationInstance)
		{
			return GetMultiManyToManyUsingAlterationCollectionViaCategoryAlteration(alterationInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  relation of type 'm:n' with the passed in AlterationEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="alterationInstance">AlterationEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingAlterationCollectionViaCategoryAlteration(IEntity alterationInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingAlterationCollectionViaCategoryAlteration(alterationInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a Relation of type 'm:n' with the passed in AlterationEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="alterationInstance">AlterationEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingAlterationCollectionViaCategoryAlteration(IEntity alterationInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingAlterationCollectionViaCategoryAlteration(alterationInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  relation of type 'm:n' with the passed in AlterationEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="alterationInstance">AlterationEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingAlterationCollectionViaCategoryAlteration(IEntity alterationInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateCategoryDAO().GetMultiUsingAlterationCollectionViaCategoryAlteration(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, alterationInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  relation of type 'm:n' with the passed in AlterationEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="alterationInstance">AlterationEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingAlterationCollectionViaCategoryAlteration(IEntity alterationInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateCategoryDAO().GetMultiUsingAlterationCollectionViaCategoryAlteration(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, alterationInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  Relation of type 'm:n' with the passed in AlterationoptionEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="alterationoptionInstance">AlterationoptionEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingAlterationoptionCollectionViaMedium(IEntity alterationoptionInstance)
		{
			return GetMultiManyToManyUsingAlterationoptionCollectionViaMedium(alterationoptionInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  relation of type 'm:n' with the passed in AlterationoptionEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="alterationoptionInstance">AlterationoptionEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingAlterationoptionCollectionViaMedium(IEntity alterationoptionInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingAlterationoptionCollectionViaMedium(alterationoptionInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a Relation of type 'm:n' with the passed in AlterationoptionEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="alterationoptionInstance">AlterationoptionEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingAlterationoptionCollectionViaMedium(IEntity alterationoptionInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingAlterationoptionCollectionViaMedium(alterationoptionInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  relation of type 'm:n' with the passed in AlterationoptionEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="alterationoptionInstance">AlterationoptionEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingAlterationoptionCollectionViaMedium(IEntity alterationoptionInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateCategoryDAO().GetMultiUsingAlterationoptionCollectionViaMedium(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, alterationoptionInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  relation of type 'm:n' with the passed in AlterationoptionEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="alterationoptionInstance">AlterationoptionEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingAlterationoptionCollectionViaMedium(IEntity alterationoptionInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateCategoryDAO().GetMultiUsingAlterationoptionCollectionViaMedium(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, alterationoptionInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  Relation of type 'm:n' with the passed in AlterationoptionEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="alterationoptionInstance">AlterationoptionEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingAlterationoptionCollectionViaMedium_(IEntity alterationoptionInstance)
		{
			return GetMultiManyToManyUsingAlterationoptionCollectionViaMedium_(alterationoptionInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  relation of type 'm:n' with the passed in AlterationoptionEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="alterationoptionInstance">AlterationoptionEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingAlterationoptionCollectionViaMedium_(IEntity alterationoptionInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingAlterationoptionCollectionViaMedium_(alterationoptionInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a Relation of type 'm:n' with the passed in AlterationoptionEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="alterationoptionInstance">AlterationoptionEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingAlterationoptionCollectionViaMedium_(IEntity alterationoptionInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingAlterationoptionCollectionViaMedium_(alterationoptionInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  relation of type 'm:n' with the passed in AlterationoptionEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="alterationoptionInstance">AlterationoptionEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingAlterationoptionCollectionViaMedium_(IEntity alterationoptionInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateCategoryDAO().GetMultiUsingAlterationoptionCollectionViaMedium_(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, alterationoptionInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  relation of type 'm:n' with the passed in AlterationoptionEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="alterationoptionInstance">AlterationoptionEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingAlterationoptionCollectionViaMedium_(IEntity alterationoptionInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateCategoryDAO().GetMultiUsingAlterationoptionCollectionViaMedium_(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, alterationoptionInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  Relation of type 'm:n' with the passed in CategoryEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="categoryInstance">CategoryEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCategoryCollectionViaMedium(IEntity categoryInstance)
		{
			return GetMultiManyToManyUsingCategoryCollectionViaMedium(categoryInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  relation of type 'm:n' with the passed in CategoryEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="categoryInstance">CategoryEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCategoryCollectionViaMedium(IEntity categoryInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingCategoryCollectionViaMedium(categoryInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a Relation of type 'm:n' with the passed in CategoryEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="categoryInstance">CategoryEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCategoryCollectionViaMedium(IEntity categoryInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingCategoryCollectionViaMedium(categoryInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  relation of type 'm:n' with the passed in CategoryEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="categoryInstance">CategoryEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingCategoryCollectionViaMedium(IEntity categoryInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateCategoryDAO().GetMultiUsingCategoryCollectionViaMedium(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, categoryInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  relation of type 'm:n' with the passed in CategoryEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="categoryInstance">CategoryEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCategoryCollectionViaMedium(IEntity categoryInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateCategoryDAO().GetMultiUsingCategoryCollectionViaMedium(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, categoryInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  Relation of type 'm:n' with the passed in CategoryEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="categoryInstance">CategoryEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCategoryCollectionViaMedium_(IEntity categoryInstance)
		{
			return GetMultiManyToManyUsingCategoryCollectionViaMedium_(categoryInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  relation of type 'm:n' with the passed in CategoryEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="categoryInstance">CategoryEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCategoryCollectionViaMedium_(IEntity categoryInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingCategoryCollectionViaMedium_(categoryInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a Relation of type 'm:n' with the passed in CategoryEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="categoryInstance">CategoryEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCategoryCollectionViaMedium_(IEntity categoryInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingCategoryCollectionViaMedium_(categoryInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  relation of type 'm:n' with the passed in CategoryEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="categoryInstance">CategoryEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingCategoryCollectionViaMedium_(IEntity categoryInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateCategoryDAO().GetMultiUsingCategoryCollectionViaMedium_(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, categoryInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  relation of type 'm:n' with the passed in CategoryEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="categoryInstance">CategoryEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCategoryCollectionViaMedium_(IEntity categoryInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateCategoryDAO().GetMultiUsingCategoryCollectionViaMedium_(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, categoryInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  Relation of type 'm:n' with the passed in ClientEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="clientInstance">ClientEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingClientCollectionViaMessage(IEntity clientInstance)
		{
			return GetMultiManyToManyUsingClientCollectionViaMessage(clientInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  relation of type 'm:n' with the passed in ClientEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="clientInstance">ClientEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingClientCollectionViaMessage(IEntity clientInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingClientCollectionViaMessage(clientInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a Relation of type 'm:n' with the passed in ClientEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="clientInstance">ClientEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingClientCollectionViaMessage(IEntity clientInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingClientCollectionViaMessage(clientInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  relation of type 'm:n' with the passed in ClientEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="clientInstance">ClientEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingClientCollectionViaMessage(IEntity clientInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateCategoryDAO().GetMultiUsingClientCollectionViaMessage(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, clientInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  relation of type 'm:n' with the passed in ClientEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="clientInstance">ClientEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingClientCollectionViaMessage(IEntity clientInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateCategoryDAO().GetMultiUsingClientCollectionViaMessage(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, clientInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  Relation of type 'm:n' with the passed in CompanyEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="companyInstance">CompanyEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCompanyCollectionViaMedium(IEntity companyInstance)
		{
			return GetMultiManyToManyUsingCompanyCollectionViaMedium(companyInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  relation of type 'm:n' with the passed in CompanyEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="companyInstance">CompanyEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCompanyCollectionViaMedium(IEntity companyInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingCompanyCollectionViaMedium(companyInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a Relation of type 'm:n' with the passed in CompanyEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="companyInstance">CompanyEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCompanyCollectionViaMedium(IEntity companyInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingCompanyCollectionViaMedium(companyInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  relation of type 'm:n' with the passed in CompanyEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="companyInstance">CompanyEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingCompanyCollectionViaMedium(IEntity companyInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateCategoryDAO().GetMultiUsingCompanyCollectionViaMedium(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, companyInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  relation of type 'm:n' with the passed in CompanyEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="companyInstance">CompanyEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCompanyCollectionViaMedium(IEntity companyInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateCategoryDAO().GetMultiUsingCompanyCollectionViaMedium(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, companyInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  Relation of type 'm:n' with the passed in CompanyEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="companyInstance">CompanyEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCompanyCollectionViaMessage(IEntity companyInstance)
		{
			return GetMultiManyToManyUsingCompanyCollectionViaMessage(companyInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  relation of type 'm:n' with the passed in CompanyEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="companyInstance">CompanyEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCompanyCollectionViaMessage(IEntity companyInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingCompanyCollectionViaMessage(companyInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a Relation of type 'm:n' with the passed in CompanyEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="companyInstance">CompanyEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCompanyCollectionViaMessage(IEntity companyInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingCompanyCollectionViaMessage(companyInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  relation of type 'm:n' with the passed in CompanyEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="companyInstance">CompanyEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingCompanyCollectionViaMessage(IEntity companyInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateCategoryDAO().GetMultiUsingCompanyCollectionViaMessage(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, companyInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  relation of type 'm:n' with the passed in CompanyEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="companyInstance">CompanyEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCompanyCollectionViaMessage(IEntity companyInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateCategoryDAO().GetMultiUsingCompanyCollectionViaMessage(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, companyInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  Relation of type 'm:n' with the passed in CompanyEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="companyInstance">CompanyEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCompanyCollectionViaMessageTemplate(IEntity companyInstance)
		{
			return GetMultiManyToManyUsingCompanyCollectionViaMessageTemplate(companyInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  relation of type 'm:n' with the passed in CompanyEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="companyInstance">CompanyEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCompanyCollectionViaMessageTemplate(IEntity companyInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingCompanyCollectionViaMessageTemplate(companyInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a Relation of type 'm:n' with the passed in CompanyEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="companyInstance">CompanyEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCompanyCollectionViaMessageTemplate(IEntity companyInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingCompanyCollectionViaMessageTemplate(companyInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  relation of type 'm:n' with the passed in CompanyEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="companyInstance">CompanyEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingCompanyCollectionViaMessageTemplate(IEntity companyInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateCategoryDAO().GetMultiUsingCompanyCollectionViaMessageTemplate(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, companyInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  relation of type 'm:n' with the passed in CompanyEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="companyInstance">CompanyEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCompanyCollectionViaMessageTemplate(IEntity companyInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateCategoryDAO().GetMultiUsingCompanyCollectionViaMessageTemplate(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, companyInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  Relation of type 'm:n' with the passed in CompanyEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="companyInstance">CompanyEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCompanyCollectionViaAdvertisement(IEntity companyInstance)
		{
			return GetMultiManyToManyUsingCompanyCollectionViaAdvertisement(companyInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  relation of type 'm:n' with the passed in CompanyEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="companyInstance">CompanyEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCompanyCollectionViaAdvertisement(IEntity companyInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingCompanyCollectionViaAdvertisement(companyInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a Relation of type 'm:n' with the passed in CompanyEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="companyInstance">CompanyEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCompanyCollectionViaAdvertisement(IEntity companyInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingCompanyCollectionViaAdvertisement(companyInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  relation of type 'm:n' with the passed in CompanyEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="companyInstance">CompanyEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingCompanyCollectionViaAdvertisement(IEntity companyInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateCategoryDAO().GetMultiUsingCompanyCollectionViaAdvertisement(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, companyInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  relation of type 'm:n' with the passed in CompanyEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="companyInstance">CompanyEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCompanyCollectionViaAdvertisement(IEntity companyInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateCategoryDAO().GetMultiUsingCompanyCollectionViaAdvertisement(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, companyInstance, prefetchPathToUse, 0, 0);
		}


		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  Relation of type 'm:n' with the passed in CustomerEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="customerInstance">CustomerEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCustomerCollectionViaMessage(IEntity customerInstance)
		{
			return GetMultiManyToManyUsingCustomerCollectionViaMessage(customerInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  relation of type 'm:n' with the passed in CustomerEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="customerInstance">CustomerEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCustomerCollectionViaMessage(IEntity customerInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingCustomerCollectionViaMessage(customerInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a Relation of type 'm:n' with the passed in CustomerEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="customerInstance">CustomerEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCustomerCollectionViaMessage(IEntity customerInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingCustomerCollectionViaMessage(customerInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  relation of type 'm:n' with the passed in CustomerEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="customerInstance">CustomerEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingCustomerCollectionViaMessage(IEntity customerInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateCategoryDAO().GetMultiUsingCustomerCollectionViaMessage(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, customerInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  relation of type 'm:n' with the passed in CustomerEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="customerInstance">CustomerEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCustomerCollectionViaMessage(IEntity customerInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateCategoryDAO().GetMultiUsingCustomerCollectionViaMessage(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, customerInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  Relation of type 'm:n' with the passed in DeliverypointEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="deliverypointInstance">DeliverypointEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingDeliverypointCollectionViaMessage(IEntity deliverypointInstance)
		{
			return GetMultiManyToManyUsingDeliverypointCollectionViaMessage(deliverypointInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  relation of type 'm:n' with the passed in DeliverypointEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="deliverypointInstance">DeliverypointEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingDeliverypointCollectionViaMessage(IEntity deliverypointInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingDeliverypointCollectionViaMessage(deliverypointInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a Relation of type 'm:n' with the passed in DeliverypointEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="deliverypointInstance">DeliverypointEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingDeliverypointCollectionViaMessage(IEntity deliverypointInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingDeliverypointCollectionViaMessage(deliverypointInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  relation of type 'm:n' with the passed in DeliverypointEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="deliverypointInstance">DeliverypointEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingDeliverypointCollectionViaMessage(IEntity deliverypointInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateCategoryDAO().GetMultiUsingDeliverypointCollectionViaMessage(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, deliverypointInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  relation of type 'm:n' with the passed in DeliverypointEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="deliverypointInstance">DeliverypointEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingDeliverypointCollectionViaMessage(IEntity deliverypointInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateCategoryDAO().GetMultiUsingDeliverypointCollectionViaMessage(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, deliverypointInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  Relation of type 'm:n' with the passed in DeliverypointgroupEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="deliverypointgroupInstance">DeliverypointgroupEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingDeliverypointgroupCollectionViaAdvertisement(IEntity deliverypointgroupInstance)
		{
			return GetMultiManyToManyUsingDeliverypointgroupCollectionViaAdvertisement(deliverypointgroupInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  relation of type 'm:n' with the passed in DeliverypointgroupEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="deliverypointgroupInstance">DeliverypointgroupEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingDeliverypointgroupCollectionViaAdvertisement(IEntity deliverypointgroupInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingDeliverypointgroupCollectionViaAdvertisement(deliverypointgroupInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a Relation of type 'm:n' with the passed in DeliverypointgroupEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="deliverypointgroupInstance">DeliverypointgroupEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingDeliverypointgroupCollectionViaAdvertisement(IEntity deliverypointgroupInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingDeliverypointgroupCollectionViaAdvertisement(deliverypointgroupInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  relation of type 'm:n' with the passed in DeliverypointgroupEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="deliverypointgroupInstance">DeliverypointgroupEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingDeliverypointgroupCollectionViaAdvertisement(IEntity deliverypointgroupInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateCategoryDAO().GetMultiUsingDeliverypointgroupCollectionViaAdvertisement(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, deliverypointgroupInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  relation of type 'm:n' with the passed in DeliverypointgroupEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="deliverypointgroupInstance">DeliverypointgroupEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingDeliverypointgroupCollectionViaAdvertisement(IEntity deliverypointgroupInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateCategoryDAO().GetMultiUsingDeliverypointgroupCollectionViaAdvertisement(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, deliverypointgroupInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  Relation of type 'm:n' with the passed in DeliverypointgroupEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="deliverypointgroupInstance">DeliverypointgroupEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingDeliverypointgroupCollectionViaAnnouncement(IEntity deliverypointgroupInstance)
		{
			return GetMultiManyToManyUsingDeliverypointgroupCollectionViaAnnouncement(deliverypointgroupInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  relation of type 'm:n' with the passed in DeliverypointgroupEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="deliverypointgroupInstance">DeliverypointgroupEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingDeliverypointgroupCollectionViaAnnouncement(IEntity deliverypointgroupInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingDeliverypointgroupCollectionViaAnnouncement(deliverypointgroupInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a Relation of type 'm:n' with the passed in DeliverypointgroupEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="deliverypointgroupInstance">DeliverypointgroupEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingDeliverypointgroupCollectionViaAnnouncement(IEntity deliverypointgroupInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingDeliverypointgroupCollectionViaAnnouncement(deliverypointgroupInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  relation of type 'm:n' with the passed in DeliverypointgroupEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="deliverypointgroupInstance">DeliverypointgroupEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingDeliverypointgroupCollectionViaAnnouncement(IEntity deliverypointgroupInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateCategoryDAO().GetMultiUsingDeliverypointgroupCollectionViaAnnouncement(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, deliverypointgroupInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  relation of type 'm:n' with the passed in DeliverypointgroupEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="deliverypointgroupInstance">DeliverypointgroupEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingDeliverypointgroupCollectionViaAnnouncement(IEntity deliverypointgroupInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateCategoryDAO().GetMultiUsingDeliverypointgroupCollectionViaAnnouncement(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, deliverypointgroupInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  Relation of type 'm:n' with the passed in DeliverypointgroupEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="deliverypointgroupInstance">DeliverypointgroupEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingDeliverypointgroupCollectionViaAnnouncement_(IEntity deliverypointgroupInstance)
		{
			return GetMultiManyToManyUsingDeliverypointgroupCollectionViaAnnouncement_(deliverypointgroupInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  relation of type 'm:n' with the passed in DeliverypointgroupEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="deliverypointgroupInstance">DeliverypointgroupEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingDeliverypointgroupCollectionViaAnnouncement_(IEntity deliverypointgroupInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingDeliverypointgroupCollectionViaAnnouncement_(deliverypointgroupInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a Relation of type 'm:n' with the passed in DeliverypointgroupEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="deliverypointgroupInstance">DeliverypointgroupEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingDeliverypointgroupCollectionViaAnnouncement_(IEntity deliverypointgroupInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingDeliverypointgroupCollectionViaAnnouncement_(deliverypointgroupInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  relation of type 'm:n' with the passed in DeliverypointgroupEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="deliverypointgroupInstance">DeliverypointgroupEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingDeliverypointgroupCollectionViaAnnouncement_(IEntity deliverypointgroupInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateCategoryDAO().GetMultiUsingDeliverypointgroupCollectionViaAnnouncement_(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, deliverypointgroupInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  relation of type 'm:n' with the passed in DeliverypointgroupEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="deliverypointgroupInstance">DeliverypointgroupEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingDeliverypointgroupCollectionViaAnnouncement_(IEntity deliverypointgroupInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateCategoryDAO().GetMultiUsingDeliverypointgroupCollectionViaAnnouncement_(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, deliverypointgroupInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  Relation of type 'm:n' with the passed in DeliverypointgroupEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="deliverypointgroupInstance">DeliverypointgroupEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingDeliverypointgroupCollectionViaMedium(IEntity deliverypointgroupInstance)
		{
			return GetMultiManyToManyUsingDeliverypointgroupCollectionViaMedium(deliverypointgroupInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  relation of type 'm:n' with the passed in DeliverypointgroupEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="deliverypointgroupInstance">DeliverypointgroupEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingDeliverypointgroupCollectionViaMedium(IEntity deliverypointgroupInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingDeliverypointgroupCollectionViaMedium(deliverypointgroupInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a Relation of type 'm:n' with the passed in DeliverypointgroupEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="deliverypointgroupInstance">DeliverypointgroupEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingDeliverypointgroupCollectionViaMedium(IEntity deliverypointgroupInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingDeliverypointgroupCollectionViaMedium(deliverypointgroupInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  relation of type 'm:n' with the passed in DeliverypointgroupEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="deliverypointgroupInstance">DeliverypointgroupEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingDeliverypointgroupCollectionViaMedium(IEntity deliverypointgroupInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateCategoryDAO().GetMultiUsingDeliverypointgroupCollectionViaMedium(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, deliverypointgroupInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  relation of type 'm:n' with the passed in DeliverypointgroupEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="deliverypointgroupInstance">DeliverypointgroupEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingDeliverypointgroupCollectionViaMedium(IEntity deliverypointgroupInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateCategoryDAO().GetMultiUsingDeliverypointgroupCollectionViaMedium(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, deliverypointgroupInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  Relation of type 'm:n' with the passed in EntertainmentEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="entertainmentInstance">EntertainmentEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingEntertainmentCollectionViaAnnouncement(IEntity entertainmentInstance)
		{
			return GetMultiManyToManyUsingEntertainmentCollectionViaAnnouncement(entertainmentInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  relation of type 'm:n' with the passed in EntertainmentEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="entertainmentInstance">EntertainmentEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingEntertainmentCollectionViaAnnouncement(IEntity entertainmentInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingEntertainmentCollectionViaAnnouncement(entertainmentInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a Relation of type 'm:n' with the passed in EntertainmentEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="entertainmentInstance">EntertainmentEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingEntertainmentCollectionViaAnnouncement(IEntity entertainmentInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingEntertainmentCollectionViaAnnouncement(entertainmentInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  relation of type 'm:n' with the passed in EntertainmentEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="entertainmentInstance">EntertainmentEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingEntertainmentCollectionViaAnnouncement(IEntity entertainmentInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateCategoryDAO().GetMultiUsingEntertainmentCollectionViaAnnouncement(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, entertainmentInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  relation of type 'm:n' with the passed in EntertainmentEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="entertainmentInstance">EntertainmentEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingEntertainmentCollectionViaAnnouncement(IEntity entertainmentInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateCategoryDAO().GetMultiUsingEntertainmentCollectionViaAnnouncement(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, entertainmentInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  Relation of type 'm:n' with the passed in EntertainmentEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="entertainmentInstance">EntertainmentEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingEntertainmentCollectionViaAnnouncement_(IEntity entertainmentInstance)
		{
			return GetMultiManyToManyUsingEntertainmentCollectionViaAnnouncement_(entertainmentInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  relation of type 'm:n' with the passed in EntertainmentEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="entertainmentInstance">EntertainmentEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingEntertainmentCollectionViaAnnouncement_(IEntity entertainmentInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingEntertainmentCollectionViaAnnouncement_(entertainmentInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a Relation of type 'm:n' with the passed in EntertainmentEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="entertainmentInstance">EntertainmentEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingEntertainmentCollectionViaAnnouncement_(IEntity entertainmentInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingEntertainmentCollectionViaAnnouncement_(entertainmentInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  relation of type 'm:n' with the passed in EntertainmentEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="entertainmentInstance">EntertainmentEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingEntertainmentCollectionViaAnnouncement_(IEntity entertainmentInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateCategoryDAO().GetMultiUsingEntertainmentCollectionViaAnnouncement_(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, entertainmentInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  relation of type 'm:n' with the passed in EntertainmentEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="entertainmentInstance">EntertainmentEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingEntertainmentCollectionViaAnnouncement_(IEntity entertainmentInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateCategoryDAO().GetMultiUsingEntertainmentCollectionViaAnnouncement_(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, entertainmentInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  Relation of type 'm:n' with the passed in EntertainmentEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="entertainmentInstance">EntertainmentEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingEntertainmentCollectionViaMedium(IEntity entertainmentInstance)
		{
			return GetMultiManyToManyUsingEntertainmentCollectionViaMedium(entertainmentInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  relation of type 'm:n' with the passed in EntertainmentEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="entertainmentInstance">EntertainmentEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingEntertainmentCollectionViaMedium(IEntity entertainmentInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingEntertainmentCollectionViaMedium(entertainmentInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a Relation of type 'm:n' with the passed in EntertainmentEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="entertainmentInstance">EntertainmentEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingEntertainmentCollectionViaMedium(IEntity entertainmentInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingEntertainmentCollectionViaMedium(entertainmentInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  relation of type 'm:n' with the passed in EntertainmentEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="entertainmentInstance">EntertainmentEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingEntertainmentCollectionViaMedium(IEntity entertainmentInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateCategoryDAO().GetMultiUsingEntertainmentCollectionViaMedium(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, entertainmentInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  relation of type 'm:n' with the passed in EntertainmentEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="entertainmentInstance">EntertainmentEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingEntertainmentCollectionViaMedium(IEntity entertainmentInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateCategoryDAO().GetMultiUsingEntertainmentCollectionViaMedium(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, entertainmentInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  Relation of type 'm:n' with the passed in EntertainmentEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="entertainmentInstance">EntertainmentEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingEntertainmentCollectionViaMedium_(IEntity entertainmentInstance)
		{
			return GetMultiManyToManyUsingEntertainmentCollectionViaMedium_(entertainmentInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  relation of type 'm:n' with the passed in EntertainmentEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="entertainmentInstance">EntertainmentEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingEntertainmentCollectionViaMedium_(IEntity entertainmentInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingEntertainmentCollectionViaMedium_(entertainmentInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a Relation of type 'm:n' with the passed in EntertainmentEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="entertainmentInstance">EntertainmentEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingEntertainmentCollectionViaMedium_(IEntity entertainmentInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingEntertainmentCollectionViaMedium_(entertainmentInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  relation of type 'm:n' with the passed in EntertainmentEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="entertainmentInstance">EntertainmentEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingEntertainmentCollectionViaMedium_(IEntity entertainmentInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateCategoryDAO().GetMultiUsingEntertainmentCollectionViaMedium_(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, entertainmentInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  relation of type 'm:n' with the passed in EntertainmentEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="entertainmentInstance">EntertainmentEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingEntertainmentCollectionViaMedium_(IEntity entertainmentInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateCategoryDAO().GetMultiUsingEntertainmentCollectionViaMedium_(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, entertainmentInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  Relation of type 'm:n' with the passed in EntertainmentEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="entertainmentInstance">EntertainmentEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingEntertainmentCollectionViaMedium__(IEntity entertainmentInstance)
		{
			return GetMultiManyToManyUsingEntertainmentCollectionViaMedium__(entertainmentInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  relation of type 'm:n' with the passed in EntertainmentEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="entertainmentInstance">EntertainmentEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingEntertainmentCollectionViaMedium__(IEntity entertainmentInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingEntertainmentCollectionViaMedium__(entertainmentInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a Relation of type 'm:n' with the passed in EntertainmentEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="entertainmentInstance">EntertainmentEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingEntertainmentCollectionViaMedium__(IEntity entertainmentInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingEntertainmentCollectionViaMedium__(entertainmentInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  relation of type 'm:n' with the passed in EntertainmentEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="entertainmentInstance">EntertainmentEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingEntertainmentCollectionViaMedium__(IEntity entertainmentInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateCategoryDAO().GetMultiUsingEntertainmentCollectionViaMedium__(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, entertainmentInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  relation of type 'm:n' with the passed in EntertainmentEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="entertainmentInstance">EntertainmentEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingEntertainmentCollectionViaMedium__(IEntity entertainmentInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateCategoryDAO().GetMultiUsingEntertainmentCollectionViaMedium__(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, entertainmentInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  Relation of type 'm:n' with the passed in EntertainmentEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="entertainmentInstance">EntertainmentEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingEntertainmentCollectionViaMessage(IEntity entertainmentInstance)
		{
			return GetMultiManyToManyUsingEntertainmentCollectionViaMessage(entertainmentInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  relation of type 'm:n' with the passed in EntertainmentEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="entertainmentInstance">EntertainmentEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingEntertainmentCollectionViaMessage(IEntity entertainmentInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingEntertainmentCollectionViaMessage(entertainmentInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a Relation of type 'm:n' with the passed in EntertainmentEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="entertainmentInstance">EntertainmentEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingEntertainmentCollectionViaMessage(IEntity entertainmentInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingEntertainmentCollectionViaMessage(entertainmentInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  relation of type 'm:n' with the passed in EntertainmentEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="entertainmentInstance">EntertainmentEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingEntertainmentCollectionViaMessage(IEntity entertainmentInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateCategoryDAO().GetMultiUsingEntertainmentCollectionViaMessage(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, entertainmentInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  relation of type 'm:n' with the passed in EntertainmentEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="entertainmentInstance">EntertainmentEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingEntertainmentCollectionViaMessage(IEntity entertainmentInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateCategoryDAO().GetMultiUsingEntertainmentCollectionViaMessage(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, entertainmentInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  Relation of type 'm:n' with the passed in EntertainmentEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="entertainmentInstance">EntertainmentEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingEntertainmentCollectionViaMessageTemplate(IEntity entertainmentInstance)
		{
			return GetMultiManyToManyUsingEntertainmentCollectionViaMessageTemplate(entertainmentInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  relation of type 'm:n' with the passed in EntertainmentEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="entertainmentInstance">EntertainmentEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingEntertainmentCollectionViaMessageTemplate(IEntity entertainmentInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingEntertainmentCollectionViaMessageTemplate(entertainmentInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a Relation of type 'm:n' with the passed in EntertainmentEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="entertainmentInstance">EntertainmentEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingEntertainmentCollectionViaMessageTemplate(IEntity entertainmentInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingEntertainmentCollectionViaMessageTemplate(entertainmentInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  relation of type 'm:n' with the passed in EntertainmentEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="entertainmentInstance">EntertainmentEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingEntertainmentCollectionViaMessageTemplate(IEntity entertainmentInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateCategoryDAO().GetMultiUsingEntertainmentCollectionViaMessageTemplate(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, entertainmentInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  relation of type 'm:n' with the passed in EntertainmentEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="entertainmentInstance">EntertainmentEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingEntertainmentCollectionViaMessageTemplate(IEntity entertainmentInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateCategoryDAO().GetMultiUsingEntertainmentCollectionViaMessageTemplate(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, entertainmentInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  Relation of type 'm:n' with the passed in EntertainmentEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="entertainmentInstance">EntertainmentEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingEntertainmentCollectionViaUITab(IEntity entertainmentInstance)
		{
			return GetMultiManyToManyUsingEntertainmentCollectionViaUITab(entertainmentInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  relation of type 'm:n' with the passed in EntertainmentEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="entertainmentInstance">EntertainmentEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingEntertainmentCollectionViaUITab(IEntity entertainmentInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingEntertainmentCollectionViaUITab(entertainmentInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a Relation of type 'm:n' with the passed in EntertainmentEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="entertainmentInstance">EntertainmentEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingEntertainmentCollectionViaUITab(IEntity entertainmentInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingEntertainmentCollectionViaUITab(entertainmentInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  relation of type 'm:n' with the passed in EntertainmentEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="entertainmentInstance">EntertainmentEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingEntertainmentCollectionViaUITab(IEntity entertainmentInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateCategoryDAO().GetMultiUsingEntertainmentCollectionViaUITab(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, entertainmentInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  relation of type 'm:n' with the passed in EntertainmentEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="entertainmentInstance">EntertainmentEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingEntertainmentCollectionViaUITab(IEntity entertainmentInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateCategoryDAO().GetMultiUsingEntertainmentCollectionViaUITab(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, entertainmentInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  Relation of type 'm:n' with the passed in EntertainmentEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="entertainmentInstance">EntertainmentEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingEntertainmentCollectionViaAdvertisement(IEntity entertainmentInstance)
		{
			return GetMultiManyToManyUsingEntertainmentCollectionViaAdvertisement(entertainmentInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  relation of type 'm:n' with the passed in EntertainmentEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="entertainmentInstance">EntertainmentEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingEntertainmentCollectionViaAdvertisement(IEntity entertainmentInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingEntertainmentCollectionViaAdvertisement(entertainmentInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a Relation of type 'm:n' with the passed in EntertainmentEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="entertainmentInstance">EntertainmentEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingEntertainmentCollectionViaAdvertisement(IEntity entertainmentInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingEntertainmentCollectionViaAdvertisement(entertainmentInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  relation of type 'm:n' with the passed in EntertainmentEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="entertainmentInstance">EntertainmentEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingEntertainmentCollectionViaAdvertisement(IEntity entertainmentInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateCategoryDAO().GetMultiUsingEntertainmentCollectionViaAdvertisement(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, entertainmentInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  relation of type 'm:n' with the passed in EntertainmentEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="entertainmentInstance">EntertainmentEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingEntertainmentCollectionViaAdvertisement(IEntity entertainmentInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateCategoryDAO().GetMultiUsingEntertainmentCollectionViaAdvertisement(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, entertainmentInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  Relation of type 'm:n' with the passed in EntertainmentEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="entertainmentInstance">EntertainmentEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingEntertainmentCollectionViaAdvertisement_(IEntity entertainmentInstance)
		{
			return GetMultiManyToManyUsingEntertainmentCollectionViaAdvertisement_(entertainmentInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  relation of type 'm:n' with the passed in EntertainmentEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="entertainmentInstance">EntertainmentEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingEntertainmentCollectionViaAdvertisement_(IEntity entertainmentInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingEntertainmentCollectionViaAdvertisement_(entertainmentInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a Relation of type 'm:n' with the passed in EntertainmentEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="entertainmentInstance">EntertainmentEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingEntertainmentCollectionViaAdvertisement_(IEntity entertainmentInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingEntertainmentCollectionViaAdvertisement_(entertainmentInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  relation of type 'm:n' with the passed in EntertainmentEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="entertainmentInstance">EntertainmentEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingEntertainmentCollectionViaAdvertisement_(IEntity entertainmentInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateCategoryDAO().GetMultiUsingEntertainmentCollectionViaAdvertisement_(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, entertainmentInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  relation of type 'm:n' with the passed in EntertainmentEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="entertainmentInstance">EntertainmentEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingEntertainmentCollectionViaAdvertisement_(IEntity entertainmentInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateCategoryDAO().GetMultiUsingEntertainmentCollectionViaAdvertisement_(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, entertainmentInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  Relation of type 'm:n' with the passed in EntertainmentcategoryEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="entertainmentcategoryInstance">EntertainmentcategoryEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingEntertainmentcategoryCollectionViaAdvertisement(IEntity entertainmentcategoryInstance)
		{
			return GetMultiManyToManyUsingEntertainmentcategoryCollectionViaAdvertisement(entertainmentcategoryInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  relation of type 'm:n' with the passed in EntertainmentcategoryEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="entertainmentcategoryInstance">EntertainmentcategoryEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingEntertainmentcategoryCollectionViaAdvertisement(IEntity entertainmentcategoryInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingEntertainmentcategoryCollectionViaAdvertisement(entertainmentcategoryInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a Relation of type 'm:n' with the passed in EntertainmentcategoryEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="entertainmentcategoryInstance">EntertainmentcategoryEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingEntertainmentcategoryCollectionViaAdvertisement(IEntity entertainmentcategoryInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingEntertainmentcategoryCollectionViaAdvertisement(entertainmentcategoryInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  relation of type 'm:n' with the passed in EntertainmentcategoryEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="entertainmentcategoryInstance">EntertainmentcategoryEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingEntertainmentcategoryCollectionViaAdvertisement(IEntity entertainmentcategoryInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateCategoryDAO().GetMultiUsingEntertainmentcategoryCollectionViaAdvertisement(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, entertainmentcategoryInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  relation of type 'm:n' with the passed in EntertainmentcategoryEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="entertainmentcategoryInstance">EntertainmentcategoryEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingEntertainmentcategoryCollectionViaAdvertisement(IEntity entertainmentcategoryInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateCategoryDAO().GetMultiUsingEntertainmentcategoryCollectionViaAdvertisement(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, entertainmentcategoryInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  Relation of type 'm:n' with the passed in EntertainmentcategoryEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="entertainmentcategoryInstance">EntertainmentcategoryEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingEntertainmentcategoryCollectionViaMedium(IEntity entertainmentcategoryInstance)
		{
			return GetMultiManyToManyUsingEntertainmentcategoryCollectionViaMedium(entertainmentcategoryInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  relation of type 'm:n' with the passed in EntertainmentcategoryEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="entertainmentcategoryInstance">EntertainmentcategoryEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingEntertainmentcategoryCollectionViaMedium(IEntity entertainmentcategoryInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingEntertainmentcategoryCollectionViaMedium(entertainmentcategoryInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a Relation of type 'm:n' with the passed in EntertainmentcategoryEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="entertainmentcategoryInstance">EntertainmentcategoryEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingEntertainmentcategoryCollectionViaMedium(IEntity entertainmentcategoryInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingEntertainmentcategoryCollectionViaMedium(entertainmentcategoryInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  relation of type 'm:n' with the passed in EntertainmentcategoryEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="entertainmentcategoryInstance">EntertainmentcategoryEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingEntertainmentcategoryCollectionViaMedium(IEntity entertainmentcategoryInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateCategoryDAO().GetMultiUsingEntertainmentcategoryCollectionViaMedium(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, entertainmentcategoryInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  relation of type 'm:n' with the passed in EntertainmentcategoryEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="entertainmentcategoryInstance">EntertainmentcategoryEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingEntertainmentcategoryCollectionViaMedium(IEntity entertainmentcategoryInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateCategoryDAO().GetMultiUsingEntertainmentcategoryCollectionViaMedium(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, entertainmentcategoryInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  Relation of type 'm:n' with the passed in EntertainmentcategoryEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="entertainmentcategoryInstance">EntertainmentcategoryEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingEntertainmentcategoryCollectionViaMedium_(IEntity entertainmentcategoryInstance)
		{
			return GetMultiManyToManyUsingEntertainmentcategoryCollectionViaMedium_(entertainmentcategoryInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  relation of type 'm:n' with the passed in EntertainmentcategoryEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="entertainmentcategoryInstance">EntertainmentcategoryEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingEntertainmentcategoryCollectionViaMedium_(IEntity entertainmentcategoryInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingEntertainmentcategoryCollectionViaMedium_(entertainmentcategoryInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a Relation of type 'm:n' with the passed in EntertainmentcategoryEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="entertainmentcategoryInstance">EntertainmentcategoryEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingEntertainmentcategoryCollectionViaMedium_(IEntity entertainmentcategoryInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingEntertainmentcategoryCollectionViaMedium_(entertainmentcategoryInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  relation of type 'm:n' with the passed in EntertainmentcategoryEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="entertainmentcategoryInstance">EntertainmentcategoryEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingEntertainmentcategoryCollectionViaMedium_(IEntity entertainmentcategoryInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateCategoryDAO().GetMultiUsingEntertainmentcategoryCollectionViaMedium_(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, entertainmentcategoryInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  relation of type 'm:n' with the passed in EntertainmentcategoryEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="entertainmentcategoryInstance">EntertainmentcategoryEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingEntertainmentcategoryCollectionViaMedium_(IEntity entertainmentcategoryInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateCategoryDAO().GetMultiUsingEntertainmentcategoryCollectionViaMedium_(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, entertainmentcategoryInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  Relation of type 'm:n' with the passed in GenericcategoryEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="genericcategoryInstance">GenericcategoryEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingGenericcategoryCollectionViaMedium(IEntity genericcategoryInstance)
		{
			return GetMultiManyToManyUsingGenericcategoryCollectionViaMedium(genericcategoryInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  relation of type 'm:n' with the passed in GenericcategoryEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="genericcategoryInstance">GenericcategoryEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingGenericcategoryCollectionViaMedium(IEntity genericcategoryInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingGenericcategoryCollectionViaMedium(genericcategoryInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a Relation of type 'm:n' with the passed in GenericcategoryEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="genericcategoryInstance">GenericcategoryEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingGenericcategoryCollectionViaMedium(IEntity genericcategoryInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingGenericcategoryCollectionViaMedium(genericcategoryInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  relation of type 'm:n' with the passed in GenericcategoryEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="genericcategoryInstance">GenericcategoryEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingGenericcategoryCollectionViaMedium(IEntity genericcategoryInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateCategoryDAO().GetMultiUsingGenericcategoryCollectionViaMedium(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, genericcategoryInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  relation of type 'm:n' with the passed in GenericcategoryEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="genericcategoryInstance">GenericcategoryEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingGenericcategoryCollectionViaMedium(IEntity genericcategoryInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateCategoryDAO().GetMultiUsingGenericcategoryCollectionViaMedium(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, genericcategoryInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  Relation of type 'm:n' with the passed in GenericproductEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="genericproductInstance">GenericproductEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingGenericproductCollectionViaMedium(IEntity genericproductInstance)
		{
			return GetMultiManyToManyUsingGenericproductCollectionViaMedium(genericproductInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  relation of type 'm:n' with the passed in GenericproductEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="genericproductInstance">GenericproductEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingGenericproductCollectionViaMedium(IEntity genericproductInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingGenericproductCollectionViaMedium(genericproductInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a Relation of type 'm:n' with the passed in GenericproductEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="genericproductInstance">GenericproductEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingGenericproductCollectionViaMedium(IEntity genericproductInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingGenericproductCollectionViaMedium(genericproductInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  relation of type 'm:n' with the passed in GenericproductEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="genericproductInstance">GenericproductEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingGenericproductCollectionViaMedium(IEntity genericproductInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateCategoryDAO().GetMultiUsingGenericproductCollectionViaMedium(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, genericproductInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  relation of type 'm:n' with the passed in GenericproductEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="genericproductInstance">GenericproductEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingGenericproductCollectionViaMedium(IEntity genericproductInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateCategoryDAO().GetMultiUsingGenericproductCollectionViaMedium(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, genericproductInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  Relation of type 'm:n' with the passed in GenericproductEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="genericproductInstance">GenericproductEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingGenericproductCollectionViaAdvertisement(IEntity genericproductInstance)
		{
			return GetMultiManyToManyUsingGenericproductCollectionViaAdvertisement(genericproductInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  relation of type 'm:n' with the passed in GenericproductEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="genericproductInstance">GenericproductEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingGenericproductCollectionViaAdvertisement(IEntity genericproductInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingGenericproductCollectionViaAdvertisement(genericproductInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a Relation of type 'm:n' with the passed in GenericproductEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="genericproductInstance">GenericproductEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingGenericproductCollectionViaAdvertisement(IEntity genericproductInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingGenericproductCollectionViaAdvertisement(genericproductInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  relation of type 'm:n' with the passed in GenericproductEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="genericproductInstance">GenericproductEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingGenericproductCollectionViaAdvertisement(IEntity genericproductInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateCategoryDAO().GetMultiUsingGenericproductCollectionViaAdvertisement(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, genericproductInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  relation of type 'm:n' with the passed in GenericproductEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="genericproductInstance">GenericproductEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingGenericproductCollectionViaAdvertisement(IEntity genericproductInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateCategoryDAO().GetMultiUsingGenericproductCollectionViaAdvertisement(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, genericproductInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  Relation of type 'm:n' with the passed in MediaEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="mediaInstance">MediaEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingMediaCollectionViaAnnouncement(IEntity mediaInstance)
		{
			return GetMultiManyToManyUsingMediaCollectionViaAnnouncement(mediaInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  relation of type 'm:n' with the passed in MediaEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="mediaInstance">MediaEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingMediaCollectionViaAnnouncement(IEntity mediaInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingMediaCollectionViaAnnouncement(mediaInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a Relation of type 'm:n' with the passed in MediaEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="mediaInstance">MediaEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingMediaCollectionViaAnnouncement(IEntity mediaInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingMediaCollectionViaAnnouncement(mediaInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  relation of type 'm:n' with the passed in MediaEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="mediaInstance">MediaEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingMediaCollectionViaAnnouncement(IEntity mediaInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateCategoryDAO().GetMultiUsingMediaCollectionViaAnnouncement(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, mediaInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  relation of type 'm:n' with the passed in MediaEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="mediaInstance">MediaEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingMediaCollectionViaAnnouncement(IEntity mediaInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateCategoryDAO().GetMultiUsingMediaCollectionViaAnnouncement(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, mediaInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  Relation of type 'm:n' with the passed in MediaEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="mediaInstance">MediaEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingMediaCollectionViaAnnouncement_(IEntity mediaInstance)
		{
			return GetMultiManyToManyUsingMediaCollectionViaAnnouncement_(mediaInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  relation of type 'm:n' with the passed in MediaEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="mediaInstance">MediaEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingMediaCollectionViaAnnouncement_(IEntity mediaInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingMediaCollectionViaAnnouncement_(mediaInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a Relation of type 'm:n' with the passed in MediaEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="mediaInstance">MediaEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingMediaCollectionViaAnnouncement_(IEntity mediaInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingMediaCollectionViaAnnouncement_(mediaInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  relation of type 'm:n' with the passed in MediaEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="mediaInstance">MediaEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingMediaCollectionViaAnnouncement_(IEntity mediaInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateCategoryDAO().GetMultiUsingMediaCollectionViaAnnouncement_(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, mediaInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  relation of type 'm:n' with the passed in MediaEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="mediaInstance">MediaEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingMediaCollectionViaAnnouncement_(IEntity mediaInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateCategoryDAO().GetMultiUsingMediaCollectionViaAnnouncement_(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, mediaInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  Relation of type 'm:n' with the passed in MediaEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="mediaInstance">MediaEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingMediaCollectionViaMessage(IEntity mediaInstance)
		{
			return GetMultiManyToManyUsingMediaCollectionViaMessage(mediaInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  relation of type 'm:n' with the passed in MediaEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="mediaInstance">MediaEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingMediaCollectionViaMessage(IEntity mediaInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingMediaCollectionViaMessage(mediaInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a Relation of type 'm:n' with the passed in MediaEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="mediaInstance">MediaEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingMediaCollectionViaMessage(IEntity mediaInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingMediaCollectionViaMessage(mediaInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  relation of type 'm:n' with the passed in MediaEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="mediaInstance">MediaEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingMediaCollectionViaMessage(IEntity mediaInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateCategoryDAO().GetMultiUsingMediaCollectionViaMessage(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, mediaInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  relation of type 'm:n' with the passed in MediaEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="mediaInstance">MediaEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingMediaCollectionViaMessage(IEntity mediaInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateCategoryDAO().GetMultiUsingMediaCollectionViaMessage(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, mediaInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  Relation of type 'm:n' with the passed in MediaEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="mediaInstance">MediaEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingMediaCollectionViaMessageTemplate(IEntity mediaInstance)
		{
			return GetMultiManyToManyUsingMediaCollectionViaMessageTemplate(mediaInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  relation of type 'm:n' with the passed in MediaEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="mediaInstance">MediaEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingMediaCollectionViaMessageTemplate(IEntity mediaInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingMediaCollectionViaMessageTemplate(mediaInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a Relation of type 'm:n' with the passed in MediaEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="mediaInstance">MediaEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingMediaCollectionViaMessageTemplate(IEntity mediaInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingMediaCollectionViaMessageTemplate(mediaInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  relation of type 'm:n' with the passed in MediaEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="mediaInstance">MediaEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingMediaCollectionViaMessageTemplate(IEntity mediaInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateCategoryDAO().GetMultiUsingMediaCollectionViaMessageTemplate(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, mediaInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  relation of type 'm:n' with the passed in MediaEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="mediaInstance">MediaEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingMediaCollectionViaMessageTemplate(IEntity mediaInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateCategoryDAO().GetMultiUsingMediaCollectionViaMessageTemplate(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, mediaInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  Relation of type 'm:n' with the passed in MenuEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="menuInstance">MenuEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingMenuCollectionViaCategory(IEntity menuInstance)
		{
			return GetMultiManyToManyUsingMenuCollectionViaCategory(menuInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  relation of type 'm:n' with the passed in MenuEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="menuInstance">MenuEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingMenuCollectionViaCategory(IEntity menuInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingMenuCollectionViaCategory(menuInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a Relation of type 'm:n' with the passed in MenuEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="menuInstance">MenuEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingMenuCollectionViaCategory(IEntity menuInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingMenuCollectionViaCategory(menuInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  relation of type 'm:n' with the passed in MenuEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="menuInstance">MenuEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingMenuCollectionViaCategory(IEntity menuInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateCategoryDAO().GetMultiUsingMenuCollectionViaCategory(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, menuInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  relation of type 'm:n' with the passed in MenuEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="menuInstance">MenuEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingMenuCollectionViaCategory(IEntity menuInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateCategoryDAO().GetMultiUsingMenuCollectionViaCategory(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, menuInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  Relation of type 'm:n' with the passed in OrderEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="orderInstance">OrderEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingOrderCollectionViaMessage(IEntity orderInstance)
		{
			return GetMultiManyToManyUsingOrderCollectionViaMessage(orderInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  relation of type 'm:n' with the passed in OrderEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="orderInstance">OrderEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingOrderCollectionViaMessage(IEntity orderInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingOrderCollectionViaMessage(orderInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a Relation of type 'm:n' with the passed in OrderEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="orderInstance">OrderEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingOrderCollectionViaMessage(IEntity orderInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingOrderCollectionViaMessage(orderInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  relation of type 'm:n' with the passed in OrderEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="orderInstance">OrderEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingOrderCollectionViaMessage(IEntity orderInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateCategoryDAO().GetMultiUsingOrderCollectionViaMessage(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, orderInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  relation of type 'm:n' with the passed in OrderEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="orderInstance">OrderEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingOrderCollectionViaMessage(IEntity orderInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateCategoryDAO().GetMultiUsingOrderCollectionViaMessage(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, orderInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  Relation of type 'm:n' with the passed in OrderEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="orderInstance">OrderEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingOrderCollectionViaOrderitem(IEntity orderInstance)
		{
			return GetMultiManyToManyUsingOrderCollectionViaOrderitem(orderInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  relation of type 'm:n' with the passed in OrderEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="orderInstance">OrderEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingOrderCollectionViaOrderitem(IEntity orderInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingOrderCollectionViaOrderitem(orderInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a Relation of type 'm:n' with the passed in OrderEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="orderInstance">OrderEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingOrderCollectionViaOrderitem(IEntity orderInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingOrderCollectionViaOrderitem(orderInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  relation of type 'm:n' with the passed in OrderEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="orderInstance">OrderEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingOrderCollectionViaOrderitem(IEntity orderInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateCategoryDAO().GetMultiUsingOrderCollectionViaOrderitem(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, orderInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  relation of type 'm:n' with the passed in OrderEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="orderInstance">OrderEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingOrderCollectionViaOrderitem(IEntity orderInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateCategoryDAO().GetMultiUsingOrderCollectionViaOrderitem(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, orderInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  Relation of type 'm:n' with the passed in PointOfInterestEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="pointOfInterestInstance">PointOfInterestEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingPointOfInterestCollectionViaMedium(IEntity pointOfInterestInstance)
		{
			return GetMultiManyToManyUsingPointOfInterestCollectionViaMedium(pointOfInterestInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  relation of type 'm:n' with the passed in PointOfInterestEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="pointOfInterestInstance">PointOfInterestEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingPointOfInterestCollectionViaMedium(IEntity pointOfInterestInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingPointOfInterestCollectionViaMedium(pointOfInterestInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a Relation of type 'm:n' with the passed in PointOfInterestEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="pointOfInterestInstance">PointOfInterestEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingPointOfInterestCollectionViaMedium(IEntity pointOfInterestInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingPointOfInterestCollectionViaMedium(pointOfInterestInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  relation of type 'm:n' with the passed in PointOfInterestEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="pointOfInterestInstance">PointOfInterestEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingPointOfInterestCollectionViaMedium(IEntity pointOfInterestInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateCategoryDAO().GetMultiUsingPointOfInterestCollectionViaMedium(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, pointOfInterestInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  relation of type 'm:n' with the passed in PointOfInterestEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="pointOfInterestInstance">PointOfInterestEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingPointOfInterestCollectionViaMedium(IEntity pointOfInterestInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateCategoryDAO().GetMultiUsingPointOfInterestCollectionViaMedium(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, pointOfInterestInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  Relation of type 'm:n' with the passed in PointOfInterestEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="pointOfInterestInstance">PointOfInterestEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingPointOfInterestCollectionViaMedium_(IEntity pointOfInterestInstance)
		{
			return GetMultiManyToManyUsingPointOfInterestCollectionViaMedium_(pointOfInterestInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  relation of type 'm:n' with the passed in PointOfInterestEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="pointOfInterestInstance">PointOfInterestEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingPointOfInterestCollectionViaMedium_(IEntity pointOfInterestInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingPointOfInterestCollectionViaMedium_(pointOfInterestInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a Relation of type 'm:n' with the passed in PointOfInterestEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="pointOfInterestInstance">PointOfInterestEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingPointOfInterestCollectionViaMedium_(IEntity pointOfInterestInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingPointOfInterestCollectionViaMedium_(pointOfInterestInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  relation of type 'm:n' with the passed in PointOfInterestEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="pointOfInterestInstance">PointOfInterestEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingPointOfInterestCollectionViaMedium_(IEntity pointOfInterestInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateCategoryDAO().GetMultiUsingPointOfInterestCollectionViaMedium_(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, pointOfInterestInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  relation of type 'm:n' with the passed in PointOfInterestEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="pointOfInterestInstance">PointOfInterestEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingPointOfInterestCollectionViaMedium_(IEntity pointOfInterestInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateCategoryDAO().GetMultiUsingPointOfInterestCollectionViaMedium_(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, pointOfInterestInstance, prefetchPathToUse, 0, 0);
		}


		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  Relation of type 'm:n' with the passed in ProductEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="productInstance">ProductEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingProductCollectionViaAnnouncement(IEntity productInstance)
		{
			return GetMultiManyToManyUsingProductCollectionViaAnnouncement(productInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  relation of type 'm:n' with the passed in ProductEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="productInstance">ProductEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingProductCollectionViaAnnouncement(IEntity productInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingProductCollectionViaAnnouncement(productInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a Relation of type 'm:n' with the passed in ProductEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="productInstance">ProductEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingProductCollectionViaAnnouncement(IEntity productInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingProductCollectionViaAnnouncement(productInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  relation of type 'm:n' with the passed in ProductEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="productInstance">ProductEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingProductCollectionViaAnnouncement(IEntity productInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateCategoryDAO().GetMultiUsingProductCollectionViaAnnouncement(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, productInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  relation of type 'm:n' with the passed in ProductEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="productInstance">ProductEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingProductCollectionViaAnnouncement(IEntity productInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateCategoryDAO().GetMultiUsingProductCollectionViaAnnouncement(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, productInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  Relation of type 'm:n' with the passed in ProductEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="productInstance">ProductEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingProductCollectionViaAnnouncement_(IEntity productInstance)
		{
			return GetMultiManyToManyUsingProductCollectionViaAnnouncement_(productInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  relation of type 'm:n' with the passed in ProductEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="productInstance">ProductEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingProductCollectionViaAnnouncement_(IEntity productInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingProductCollectionViaAnnouncement_(productInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a Relation of type 'm:n' with the passed in ProductEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="productInstance">ProductEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingProductCollectionViaAnnouncement_(IEntity productInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingProductCollectionViaAnnouncement_(productInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  relation of type 'm:n' with the passed in ProductEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="productInstance">ProductEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingProductCollectionViaAnnouncement_(IEntity productInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateCategoryDAO().GetMultiUsingProductCollectionViaAnnouncement_(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, productInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  relation of type 'm:n' with the passed in ProductEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="productInstance">ProductEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingProductCollectionViaAnnouncement_(IEntity productInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateCategoryDAO().GetMultiUsingProductCollectionViaAnnouncement_(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, productInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  Relation of type 'm:n' with the passed in ProductEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="productInstance">ProductEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingProductCollectionViaCategorySuggestion(IEntity productInstance)
		{
			return GetMultiManyToManyUsingProductCollectionViaCategorySuggestion(productInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  relation of type 'm:n' with the passed in ProductEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="productInstance">ProductEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingProductCollectionViaCategorySuggestion(IEntity productInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingProductCollectionViaCategorySuggestion(productInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a Relation of type 'm:n' with the passed in ProductEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="productInstance">ProductEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingProductCollectionViaCategorySuggestion(IEntity productInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingProductCollectionViaCategorySuggestion(productInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  relation of type 'm:n' with the passed in ProductEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="productInstance">ProductEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingProductCollectionViaCategorySuggestion(IEntity productInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateCategoryDAO().GetMultiUsingProductCollectionViaCategorySuggestion(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, productInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  relation of type 'm:n' with the passed in ProductEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="productInstance">ProductEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingProductCollectionViaCategorySuggestion(IEntity productInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateCategoryDAO().GetMultiUsingProductCollectionViaCategorySuggestion(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, productInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  Relation of type 'm:n' with the passed in ProductEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="productInstance">ProductEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingProductCollectionViaMedium(IEntity productInstance)
		{
			return GetMultiManyToManyUsingProductCollectionViaMedium(productInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  relation of type 'm:n' with the passed in ProductEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="productInstance">ProductEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingProductCollectionViaMedium(IEntity productInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingProductCollectionViaMedium(productInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a Relation of type 'm:n' with the passed in ProductEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="productInstance">ProductEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingProductCollectionViaMedium(IEntity productInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingProductCollectionViaMedium(productInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  relation of type 'm:n' with the passed in ProductEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="productInstance">ProductEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingProductCollectionViaMedium(IEntity productInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateCategoryDAO().GetMultiUsingProductCollectionViaMedium(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, productInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  relation of type 'm:n' with the passed in ProductEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="productInstance">ProductEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingProductCollectionViaMedium(IEntity productInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateCategoryDAO().GetMultiUsingProductCollectionViaMedium(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, productInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  Relation of type 'm:n' with the passed in ProductEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="productInstance">ProductEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingProductCollectionViaMedium_(IEntity productInstance)
		{
			return GetMultiManyToManyUsingProductCollectionViaMedium_(productInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  relation of type 'm:n' with the passed in ProductEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="productInstance">ProductEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingProductCollectionViaMedium_(IEntity productInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingProductCollectionViaMedium_(productInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a Relation of type 'm:n' with the passed in ProductEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="productInstance">ProductEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingProductCollectionViaMedium_(IEntity productInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingProductCollectionViaMedium_(productInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  relation of type 'm:n' with the passed in ProductEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="productInstance">ProductEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingProductCollectionViaMedium_(IEntity productInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateCategoryDAO().GetMultiUsingProductCollectionViaMedium_(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, productInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  relation of type 'm:n' with the passed in ProductEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="productInstance">ProductEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingProductCollectionViaMedium_(IEntity productInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateCategoryDAO().GetMultiUsingProductCollectionViaMedium_(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, productInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  Relation of type 'm:n' with the passed in ProductEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="productInstance">ProductEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingProductCollectionViaMedium__(IEntity productInstance)
		{
			return GetMultiManyToManyUsingProductCollectionViaMedium__(productInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  relation of type 'm:n' with the passed in ProductEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="productInstance">ProductEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingProductCollectionViaMedium__(IEntity productInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingProductCollectionViaMedium__(productInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a Relation of type 'm:n' with the passed in ProductEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="productInstance">ProductEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingProductCollectionViaMedium__(IEntity productInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingProductCollectionViaMedium__(productInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  relation of type 'm:n' with the passed in ProductEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="productInstance">ProductEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingProductCollectionViaMedium__(IEntity productInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateCategoryDAO().GetMultiUsingProductCollectionViaMedium__(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, productInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  relation of type 'm:n' with the passed in ProductEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="productInstance">ProductEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingProductCollectionViaMedium__(IEntity productInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateCategoryDAO().GetMultiUsingProductCollectionViaMedium__(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, productInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  Relation of type 'm:n' with the passed in ProductEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="productInstance">ProductEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingProductCollectionViaMessageTemplate(IEntity productInstance)
		{
			return GetMultiManyToManyUsingProductCollectionViaMessageTemplate(productInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  relation of type 'm:n' with the passed in ProductEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="productInstance">ProductEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingProductCollectionViaMessageTemplate(IEntity productInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingProductCollectionViaMessageTemplate(productInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a Relation of type 'm:n' with the passed in ProductEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="productInstance">ProductEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingProductCollectionViaMessageTemplate(IEntity productInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingProductCollectionViaMessageTemplate(productInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  relation of type 'm:n' with the passed in ProductEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="productInstance">ProductEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingProductCollectionViaMessageTemplate(IEntity productInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateCategoryDAO().GetMultiUsingProductCollectionViaMessageTemplate(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, productInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  relation of type 'm:n' with the passed in ProductEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="productInstance">ProductEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingProductCollectionViaMessageTemplate(IEntity productInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateCategoryDAO().GetMultiUsingProductCollectionViaMessageTemplate(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, productInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  Relation of type 'm:n' with the passed in ProductEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="productInstance">ProductEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingProductCollectionViaOrderitem(IEntity productInstance)
		{
			return GetMultiManyToManyUsingProductCollectionViaOrderitem(productInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  relation of type 'm:n' with the passed in ProductEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="productInstance">ProductEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingProductCollectionViaOrderitem(IEntity productInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingProductCollectionViaOrderitem(productInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a Relation of type 'm:n' with the passed in ProductEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="productInstance">ProductEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingProductCollectionViaOrderitem(IEntity productInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingProductCollectionViaOrderitem(productInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  relation of type 'm:n' with the passed in ProductEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="productInstance">ProductEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingProductCollectionViaOrderitem(IEntity productInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateCategoryDAO().GetMultiUsingProductCollectionViaOrderitem(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, productInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  relation of type 'm:n' with the passed in ProductEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="productInstance">ProductEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingProductCollectionViaOrderitem(IEntity productInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateCategoryDAO().GetMultiUsingProductCollectionViaOrderitem(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, productInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  Relation of type 'm:n' with the passed in ProductEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="productInstance">ProductEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingProductCollectionViaProductCategory(IEntity productInstance)
		{
			return GetMultiManyToManyUsingProductCollectionViaProductCategory(productInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  relation of type 'm:n' with the passed in ProductEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="productInstance">ProductEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingProductCollectionViaProductCategory(IEntity productInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingProductCollectionViaProductCategory(productInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a Relation of type 'm:n' with the passed in ProductEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="productInstance">ProductEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingProductCollectionViaProductCategory(IEntity productInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingProductCollectionViaProductCategory(productInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  relation of type 'm:n' with the passed in ProductEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="productInstance">ProductEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingProductCollectionViaProductCategory(IEntity productInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateCategoryDAO().GetMultiUsingProductCollectionViaProductCategory(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, productInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  relation of type 'm:n' with the passed in ProductEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="productInstance">ProductEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingProductCollectionViaProductCategory(IEntity productInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateCategoryDAO().GetMultiUsingProductCollectionViaProductCategory(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, productInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  Relation of type 'm:n' with the passed in ProductEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="productInstance">ProductEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingProductCollectionViaAdvertisement(IEntity productInstance)
		{
			return GetMultiManyToManyUsingProductCollectionViaAdvertisement(productInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  relation of type 'm:n' with the passed in ProductEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="productInstance">ProductEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingProductCollectionViaAdvertisement(IEntity productInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingProductCollectionViaAdvertisement(productInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a Relation of type 'm:n' with the passed in ProductEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="productInstance">ProductEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingProductCollectionViaAdvertisement(IEntity productInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingProductCollectionViaAdvertisement(productInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  relation of type 'm:n' with the passed in ProductEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="productInstance">ProductEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingProductCollectionViaAdvertisement(IEntity productInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateCategoryDAO().GetMultiUsingProductCollectionViaAdvertisement(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, productInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  relation of type 'm:n' with the passed in ProductEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="productInstance">ProductEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingProductCollectionViaAdvertisement(IEntity productInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateCategoryDAO().GetMultiUsingProductCollectionViaAdvertisement(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, productInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  Relation of type 'm:n' with the passed in RouteEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="routeInstance">RouteEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingRouteCollectionViaCategory(IEntity routeInstance)
		{
			return GetMultiManyToManyUsingRouteCollectionViaCategory(routeInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  relation of type 'm:n' with the passed in RouteEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="routeInstance">RouteEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingRouteCollectionViaCategory(IEntity routeInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingRouteCollectionViaCategory(routeInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a Relation of type 'm:n' with the passed in RouteEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="routeInstance">RouteEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingRouteCollectionViaCategory(IEntity routeInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingRouteCollectionViaCategory(routeInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  relation of type 'm:n' with the passed in RouteEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="routeInstance">RouteEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingRouteCollectionViaCategory(IEntity routeInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateCategoryDAO().GetMultiUsingRouteCollectionViaCategory(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, routeInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  relation of type 'm:n' with the passed in RouteEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="routeInstance">RouteEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingRouteCollectionViaCategory(IEntity routeInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateCategoryDAO().GetMultiUsingRouteCollectionViaCategory(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, routeInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  Relation of type 'm:n' with the passed in SupplierEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="supplierInstance">SupplierEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingSupplierCollectionViaAdvertisement(IEntity supplierInstance)
		{
			return GetMultiManyToManyUsingSupplierCollectionViaAdvertisement(supplierInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  relation of type 'm:n' with the passed in SupplierEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="supplierInstance">SupplierEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingSupplierCollectionViaAdvertisement(IEntity supplierInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingSupplierCollectionViaAdvertisement(supplierInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a Relation of type 'm:n' with the passed in SupplierEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="supplierInstance">SupplierEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingSupplierCollectionViaAdvertisement(IEntity supplierInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingSupplierCollectionViaAdvertisement(supplierInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  relation of type 'm:n' with the passed in SupplierEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="supplierInstance">SupplierEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingSupplierCollectionViaAdvertisement(IEntity supplierInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateCategoryDAO().GetMultiUsingSupplierCollectionViaAdvertisement(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, supplierInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  relation of type 'm:n' with the passed in SupplierEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="supplierInstance">SupplierEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingSupplierCollectionViaAdvertisement(IEntity supplierInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateCategoryDAO().GetMultiUsingSupplierCollectionViaAdvertisement(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, supplierInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  Relation of type 'm:n' with the passed in SurveyEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="surveyInstance">SurveyEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingSurveyCollectionViaMedium(IEntity surveyInstance)
		{
			return GetMultiManyToManyUsingSurveyCollectionViaMedium(surveyInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  relation of type 'm:n' with the passed in SurveyEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="surveyInstance">SurveyEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingSurveyCollectionViaMedium(IEntity surveyInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingSurveyCollectionViaMedium(surveyInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a Relation of type 'm:n' with the passed in SurveyEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="surveyInstance">SurveyEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingSurveyCollectionViaMedium(IEntity surveyInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingSurveyCollectionViaMedium(surveyInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  relation of type 'm:n' with the passed in SurveyEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="surveyInstance">SurveyEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingSurveyCollectionViaMedium(IEntity surveyInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateCategoryDAO().GetMultiUsingSurveyCollectionViaMedium(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, surveyInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  relation of type 'm:n' with the passed in SurveyEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="surveyInstance">SurveyEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingSurveyCollectionViaMedium(IEntity surveyInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateCategoryDAO().GetMultiUsingSurveyCollectionViaMedium(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, surveyInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  Relation of type 'm:n' with the passed in SurveyPageEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="surveyPageInstance">SurveyPageEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingSurveyPageCollectionViaMedium(IEntity surveyPageInstance)
		{
			return GetMultiManyToManyUsingSurveyPageCollectionViaMedium(surveyPageInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  relation of type 'm:n' with the passed in SurveyPageEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="surveyPageInstance">SurveyPageEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingSurveyPageCollectionViaMedium(IEntity surveyPageInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingSurveyPageCollectionViaMedium(surveyPageInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a Relation of type 'm:n' with the passed in SurveyPageEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="surveyPageInstance">SurveyPageEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingSurveyPageCollectionViaMedium(IEntity surveyPageInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingSurveyPageCollectionViaMedium(surveyPageInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  relation of type 'm:n' with the passed in SurveyPageEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="surveyPageInstance">SurveyPageEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingSurveyPageCollectionViaMedium(IEntity surveyPageInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateCategoryDAO().GetMultiUsingSurveyPageCollectionViaMedium(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, surveyPageInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  relation of type 'm:n' with the passed in SurveyPageEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="surveyPageInstance">SurveyPageEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingSurveyPageCollectionViaMedium(IEntity surveyPageInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateCategoryDAO().GetMultiUsingSurveyPageCollectionViaMedium(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, surveyPageInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  Relation of type 'm:n' with the passed in SurveyPageEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="surveyPageInstance">SurveyPageEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingSurveyPageCollectionViaMedium_(IEntity surveyPageInstance)
		{
			return GetMultiManyToManyUsingSurveyPageCollectionViaMedium_(surveyPageInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  relation of type 'm:n' with the passed in SurveyPageEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="surveyPageInstance">SurveyPageEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingSurveyPageCollectionViaMedium_(IEntity surveyPageInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingSurveyPageCollectionViaMedium_(surveyPageInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a Relation of type 'm:n' with the passed in SurveyPageEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="surveyPageInstance">SurveyPageEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingSurveyPageCollectionViaMedium_(IEntity surveyPageInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingSurveyPageCollectionViaMedium_(surveyPageInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  relation of type 'm:n' with the passed in SurveyPageEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="surveyPageInstance">SurveyPageEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingSurveyPageCollectionViaMedium_(IEntity surveyPageInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateCategoryDAO().GetMultiUsingSurveyPageCollectionViaMedium_(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, surveyPageInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  relation of type 'm:n' with the passed in SurveyPageEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="surveyPageInstance">SurveyPageEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingSurveyPageCollectionViaMedium_(IEntity surveyPageInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateCategoryDAO().GetMultiUsingSurveyPageCollectionViaMedium_(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, surveyPageInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  Relation of type 'm:n' with the passed in UIModeEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="uIModeInstance">UIModeEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingUIModeCollectionViaUITab(IEntity uIModeInstance)
		{
			return GetMultiManyToManyUsingUIModeCollectionViaUITab(uIModeInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  relation of type 'm:n' with the passed in UIModeEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="uIModeInstance">UIModeEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingUIModeCollectionViaUITab(IEntity uIModeInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingUIModeCollectionViaUITab(uIModeInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a Relation of type 'm:n' with the passed in UIModeEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="uIModeInstance">UIModeEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingUIModeCollectionViaUITab(IEntity uIModeInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingUIModeCollectionViaUITab(uIModeInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  relation of type 'm:n' with the passed in UIModeEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="uIModeInstance">UIModeEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingUIModeCollectionViaUITab(IEntity uIModeInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateCategoryDAO().GetMultiUsingUIModeCollectionViaUITab(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, uIModeInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this CategoryCollection object all CategoryEntity objects which are related via a  relation of type 'm:n' with the passed in UIModeEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="uIModeInstance">UIModeEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingUIModeCollectionViaUITab(IEntity uIModeInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateCategoryDAO().GetMultiUsingUIModeCollectionViaUITab(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, uIModeInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves Entity rows in a datatable which match the specified filter. It will always create a new connection to the database.</summary>
		/// <param name="selectFilter">A predicate or predicate expression which should be used as filter for the entities to retrieve.</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>DataTable with the rows requested.</returns>
		public static DataTable GetMultiAsDataTable(IPredicate selectFilter, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiAsDataTable(selectFilter, maxNumberOfItemsToReturn, sortClauses, null, 0, 0);
		}

		/// <summary> Retrieves Entity rows in a datatable which match the specified filter. It will always create a new connection to the database.</summary>
		/// <param name="selectFilter">A predicate or predicate expression which should be used as filter for the entities to retrieve.</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="relations">The set of relations to walk to construct to total query.</param>
		/// <returns>DataTable with the rows requested.</returns>
		public static DataTable GetMultiAsDataTable(IPredicate selectFilter, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IRelationCollection relations)
		{
			return GetMultiAsDataTable(selectFilter, maxNumberOfItemsToReturn, sortClauses, relations, 0, 0);
		}
		
		/// <summary> Retrieves Entity rows in a datatable which match the specified filter. It will always create a new connection to the database.</summary>
		/// <param name="selectFilter">A predicate or predicate expression which should be used as filter for the entities to retrieve.</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="relations">The set of relations to walk to construct to total query.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>DataTable with the rows requested.</returns>
		public static DataTable GetMultiAsDataTable(IPredicate selectFilter, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IRelationCollection relations, int pageNumber, int pageSize)
		{
			CategoryDAO dao = DAOFactory.CreateCategoryDAO();
			return dao.GetMultiAsDataTable(maxNumberOfItemsToReturn, sortClauses, selectFilter, relations, pageNumber, pageSize);
		}


		
		/// <summary> Gets a scalar value, calculated with the aggregate. the field index specified is the field the aggregate are applied on.</summary>
		/// <param name="fieldIndex">Field index of field to which to apply the aggregate function and expression</param>
		/// <param name="aggregateToApply">Aggregate function to apply. </param>
		/// <returns>the scalar value requested</returns>
		public object GetScalar(CategoryFieldIndex fieldIndex, AggregateFunction aggregateToApply)
		{
			return GetScalar(fieldIndex, null, aggregateToApply, null, null, null);
		}

		/// <summary> Gets a scalar value, calculated with the aggregate and expression specified. the field index specified is the field the expression and aggregate are applied on.</summary>
		/// <param name="fieldIndex">Field index of field to which to apply the aggregate function and expression</param>
		/// <param name="expressionToExecute">The expression to execute. Can be null</param>
		/// <param name="aggregateToApply">Aggregate function to apply. </param>
		/// <returns>the scalar value requested</returns>
		public object GetScalar(CategoryFieldIndex fieldIndex, IExpression expressionToExecute, AggregateFunction aggregateToApply)
		{
			return GetScalar(fieldIndex, expressionToExecute, aggregateToApply, null, null, null);
		}

		/// <summary> Gets a scalar value, calculated with the aggregate and expression specified. the field index specified is the field the expression and aggregate are applied on.</summary>
		/// <param name="fieldIndex">Field index of field to which to apply the aggregate function and expression</param>
		/// <param name="expressionToExecute">The expression to execute. Can be null</param>
		/// <param name="aggregateToApply">Aggregate function to apply. </param>
		/// <param name="filter">The filter to apply to retrieve the scalar</param>
		/// <returns>the scalar value requested</returns>
		public object GetScalar(CategoryFieldIndex fieldIndex, IExpression expressionToExecute, AggregateFunction aggregateToApply, IPredicate filter)
		{
			return GetScalar(fieldIndex, expressionToExecute, aggregateToApply, filter, null, null);
		}

		/// <summary> Gets a scalar value, calculated with the aggregate and expression specified. the field index specified is the field the expression and aggregate are applied on.</summary>
		/// <param name="fieldIndex">Field index of field to which to apply the aggregate function and expression</param>
		/// <param name="expressionToExecute">The expression to execute. Can be null</param>
		/// <param name="aggregateToApply">Aggregate function to apply. </param>
		/// <param name="filter">The filter to apply to retrieve the scalar</param>
		/// <param name="groupByClause">The groupby clause to apply to retrieve the scalar</param>
		/// <returns>the scalar value requested</returns>
		public object GetScalar(CategoryFieldIndex fieldIndex, IExpression expressionToExecute, AggregateFunction aggregateToApply, IPredicate filter, IGroupByCollection groupByClause)
		{
			return GetScalar(fieldIndex, expressionToExecute, aggregateToApply, filter, null, groupByClause);
		}

		/// <summary> Gets a scalar value, calculated with the aggregate and expression specified. the field index specified is the field the expression and aggregate are applied on.</summary>
		/// <param name="fieldIndex">Field index of field to which to apply the aggregate function and expression</param>
		/// <param name="expressionToExecute">The expression to execute. Can be null</param>
		/// <param name="aggregateToApply">Aggregate function to apply. </param>
		/// <param name="filter">The filter to apply to retrieve the scalar</param>
		/// <param name="relations">The relations to walk</param>
		/// <param name="groupByClause">The groupby clause to apply to retrieve the scalar</param>
		/// <returns>the scalar value requested</returns>
		public virtual object GetScalar(CategoryFieldIndex fieldIndex, IExpression expressionToExecute, AggregateFunction aggregateToApply, IPredicate filter, IRelationCollection relations, IGroupByCollection groupByClause)
		{
			EntityFields fields = new EntityFields(1);
			fields[0] = EntityFieldFactory.Create(fieldIndex);
			if((fields[0].ExpressionToApply == null) || (expressionToExecute != null))
			{
				fields[0].ExpressionToApply = expressionToExecute;
			}
			if((fields[0].AggregateFunctionToApply == AggregateFunction.None) || (aggregateToApply != AggregateFunction.None))
			{
				fields[0].AggregateFunctionToApply = aggregateToApply;
			}
			return DAOFactory.CreateCategoryDAO().GetScalar(fields, this.Transaction, filter, relations, groupByClause);
		}
		
		/// <summary>Creats a new DAO instance so code which is in the base class can still use the proper DAO object.</summary>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateCategoryDAO();
		}
		
		/// <summary>Creates a new transaction object</summary>
		/// <param name="levelOfIsolation">The level of isolation.</param>
		/// <param name="name">The name.</param>
		protected override ITransaction CreateTransaction( IsolationLevel levelOfIsolation, string name )
		{
			return new Transaction(levelOfIsolation, name);
		}

		#region Custom EntityCollection code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCollectionCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion
		
		#region Included Code

		#endregion
	}
}
