﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Data;
using System.Collections.Generic;
using System.ComponentModel;
using System.Xml;
#if !CF
using System.Runtime.Serialization;
#endif

using Obymobi.Data.EntityClasses;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.CollectionClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Collection class for storing and retrieving collections of RoomControlWidgetEntity objects. </summary>
	[Serializable]
	public partial class RoomControlWidgetCollection : CommonEntityCollection<RoomControlWidgetEntity>
	{
		/// <summary> CTor</summary>
		public RoomControlWidgetCollection():base(new RoomControlWidgetEntityFactory())
		{
		}

		/// <summary> CTor</summary>
		/// <param name="initialContents">The initial contents of this collection.</param>
		public RoomControlWidgetCollection(IEnumerable<RoomControlWidgetEntity> initialContents):base(new RoomControlWidgetEntityFactory())
		{
			AddRange(initialContents);
		}

		/// <summary> CTor</summary>
		/// <param name="entityFactoryToUse">The EntityFactory to use when creating entity objects during a GetMulti() call.</param>
		public RoomControlWidgetCollection(IEntityFactory entityFactoryToUse):base(entityFactoryToUse)
		{
		}

		/// <summary> Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected RoomControlWidgetCollection(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}


		/// <summary> Retrieves in this RoomControlWidgetCollection object all RoomControlWidgetEntity objects which have data in common with the specified related Entities.
		/// If one is omitted, that entity is not used as a filter. All current elements in the collection are removed from the collection.</summary>
		/// <param name="infraredConfigurationEntityInstance">InfraredConfigurationEntity instance to use as a filter for the RoomControlWidgetEntity objects to return</param>
		/// <param name="roomControlComponentEntityInstance">RoomControlComponentEntity instance to use as a filter for the RoomControlWidgetEntity objects to return</param>
		/// <param name="roomControlComponentEntity_Instance">RoomControlComponentEntity instance to use as a filter for the RoomControlWidgetEntity objects to return</param>
		/// <param name="roomControlComponentEntity__Instance">RoomControlComponentEntity instance to use as a filter for the RoomControlWidgetEntity objects to return</param>
		/// <param name="roomControlComponentEntity___Instance">RoomControlComponentEntity instance to use as a filter for the RoomControlWidgetEntity objects to return</param>
		/// <param name="roomControlComponentEntity____Instance">RoomControlComponentEntity instance to use as a filter for the RoomControlWidgetEntity objects to return</param>
		/// <param name="roomControlComponentEntity_____Instance">RoomControlComponentEntity instance to use as a filter for the RoomControlWidgetEntity objects to return</param>
		/// <param name="roomControlComponentEntity______Instance">RoomControlComponentEntity instance to use as a filter for the RoomControlWidgetEntity objects to return</param>
		/// <param name="roomControlComponentEntity_______Instance">RoomControlComponentEntity instance to use as a filter for the RoomControlWidgetEntity objects to return</param>
		/// <param name="roomControlComponentEntity________Instance">RoomControlComponentEntity instance to use as a filter for the RoomControlWidgetEntity objects to return</param>
		/// <param name="roomControlComponentEntity_________Instance">RoomControlComponentEntity instance to use as a filter for the RoomControlWidgetEntity objects to return</param>
		/// <param name="roomControlSectionEntityInstance">RoomControlSectionEntity instance to use as a filter for the RoomControlWidgetEntity objects to return</param>
		/// <param name="roomControlSectionItemEntityInstance">RoomControlSectionItemEntity instance to use as a filter for the RoomControlWidgetEntity objects to return</param>
		/// <returns>true if succeeded, false otherwise</returns>
		public bool GetMultiManyToOne(IEntity infraredConfigurationEntityInstance, IEntity roomControlComponentEntityInstance, IEntity roomControlComponentEntity_Instance, IEntity roomControlComponentEntity__Instance, IEntity roomControlComponentEntity___Instance, IEntity roomControlComponentEntity____Instance, IEntity roomControlComponentEntity_____Instance, IEntity roomControlComponentEntity______Instance, IEntity roomControlComponentEntity_______Instance, IEntity roomControlComponentEntity________Instance, IEntity roomControlComponentEntity_________Instance, IEntity roomControlSectionEntityInstance, IEntity roomControlSectionItemEntityInstance)
		{
			return GetMultiManyToOne(infraredConfigurationEntityInstance, roomControlComponentEntityInstance, roomControlComponentEntity_Instance, roomControlComponentEntity__Instance, roomControlComponentEntity___Instance, roomControlComponentEntity____Instance, roomControlComponentEntity_____Instance, roomControlComponentEntity______Instance, roomControlComponentEntity_______Instance, roomControlComponentEntity________Instance, roomControlComponentEntity_________Instance, roomControlSectionEntityInstance, roomControlSectionItemEntityInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, null, 0, 0);
		}

		/// <summary> Retrieves in this RoomControlWidgetCollection object all RoomControlWidgetEntity objects which have data in common with the specified related Entities.
		/// If one is omitted, that entity is not used as a filter. All current elements in the collection are removed from the collection.</summary>
		/// <param name="infraredConfigurationEntityInstance">InfraredConfigurationEntity instance to use as a filter for the RoomControlWidgetEntity objects to return</param>
		/// <param name="roomControlComponentEntityInstance">RoomControlComponentEntity instance to use as a filter for the RoomControlWidgetEntity objects to return</param>
		/// <param name="roomControlComponentEntity_Instance">RoomControlComponentEntity instance to use as a filter for the RoomControlWidgetEntity objects to return</param>
		/// <param name="roomControlComponentEntity__Instance">RoomControlComponentEntity instance to use as a filter for the RoomControlWidgetEntity objects to return</param>
		/// <param name="roomControlComponentEntity___Instance">RoomControlComponentEntity instance to use as a filter for the RoomControlWidgetEntity objects to return</param>
		/// <param name="roomControlComponentEntity____Instance">RoomControlComponentEntity instance to use as a filter for the RoomControlWidgetEntity objects to return</param>
		/// <param name="roomControlComponentEntity_____Instance">RoomControlComponentEntity instance to use as a filter for the RoomControlWidgetEntity objects to return</param>
		/// <param name="roomControlComponentEntity______Instance">RoomControlComponentEntity instance to use as a filter for the RoomControlWidgetEntity objects to return</param>
		/// <param name="roomControlComponentEntity_______Instance">RoomControlComponentEntity instance to use as a filter for the RoomControlWidgetEntity objects to return</param>
		/// <param name="roomControlComponentEntity________Instance">RoomControlComponentEntity instance to use as a filter for the RoomControlWidgetEntity objects to return</param>
		/// <param name="roomControlComponentEntity_________Instance">RoomControlComponentEntity instance to use as a filter for the RoomControlWidgetEntity objects to return</param>
		/// <param name="roomControlSectionEntityInstance">RoomControlSectionEntity instance to use as a filter for the RoomControlWidgetEntity objects to return</param>
		/// <param name="roomControlSectionItemEntityInstance">RoomControlSectionItemEntity instance to use as a filter for the RoomControlWidgetEntity objects to return</param>
		/// <param name="filter">Extra filter to limit the resultset. Predicate expression can be null, in which case it will be ignored.</param>
		/// <returns>true if succeeded, false otherwise</returns>
		public bool GetMultiManyToOne(IEntity infraredConfigurationEntityInstance, IEntity roomControlComponentEntityInstance, IEntity roomControlComponentEntity_Instance, IEntity roomControlComponentEntity__Instance, IEntity roomControlComponentEntity___Instance, IEntity roomControlComponentEntity____Instance, IEntity roomControlComponentEntity_____Instance, IEntity roomControlComponentEntity______Instance, IEntity roomControlComponentEntity_______Instance, IEntity roomControlComponentEntity________Instance, IEntity roomControlComponentEntity_________Instance, IEntity roomControlSectionEntityInstance, IEntity roomControlSectionItemEntityInstance, IPredicateExpression filter)
		{
			return GetMultiManyToOne(infraredConfigurationEntityInstance, roomControlComponentEntityInstance, roomControlComponentEntity_Instance, roomControlComponentEntity__Instance, roomControlComponentEntity___Instance, roomControlComponentEntity____Instance, roomControlComponentEntity_____Instance, roomControlComponentEntity______Instance, roomControlComponentEntity_______Instance, roomControlComponentEntity________Instance, roomControlComponentEntity_________Instance, roomControlSectionEntityInstance, roomControlSectionItemEntityInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, filter, 0, 0);
		}

		/// <summary> Retrieves in this RoomControlWidgetCollection object all RoomControlWidgetEntity objects which have data in common with the specified related Entities.
		/// If one is omitted, that entity is not used as a filter. All current elements in the collection are removed from the collection.</summary>
		/// <param name="infraredConfigurationEntityInstance">InfraredConfigurationEntity instance to use as a filter for the RoomControlWidgetEntity objects to return</param>
		/// <param name="roomControlComponentEntityInstance">RoomControlComponentEntity instance to use as a filter for the RoomControlWidgetEntity objects to return</param>
		/// <param name="roomControlComponentEntity_Instance">RoomControlComponentEntity instance to use as a filter for the RoomControlWidgetEntity objects to return</param>
		/// <param name="roomControlComponentEntity__Instance">RoomControlComponentEntity instance to use as a filter for the RoomControlWidgetEntity objects to return</param>
		/// <param name="roomControlComponentEntity___Instance">RoomControlComponentEntity instance to use as a filter for the RoomControlWidgetEntity objects to return</param>
		/// <param name="roomControlComponentEntity____Instance">RoomControlComponentEntity instance to use as a filter for the RoomControlWidgetEntity objects to return</param>
		/// <param name="roomControlComponentEntity_____Instance">RoomControlComponentEntity instance to use as a filter for the RoomControlWidgetEntity objects to return</param>
		/// <param name="roomControlComponentEntity______Instance">RoomControlComponentEntity instance to use as a filter for the RoomControlWidgetEntity objects to return</param>
		/// <param name="roomControlComponentEntity_______Instance">RoomControlComponentEntity instance to use as a filter for the RoomControlWidgetEntity objects to return</param>
		/// <param name="roomControlComponentEntity________Instance">RoomControlComponentEntity instance to use as a filter for the RoomControlWidgetEntity objects to return</param>
		/// <param name="roomControlComponentEntity_________Instance">RoomControlComponentEntity instance to use as a filter for the RoomControlWidgetEntity objects to return</param>
		/// <param name="roomControlSectionEntityInstance">RoomControlSectionEntity instance to use as a filter for the RoomControlWidgetEntity objects to return</param>
		/// <param name="roomControlSectionItemEntityInstance">RoomControlSectionItemEntity instance to use as a filter for the RoomControlWidgetEntity objects to return</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="filter">Extra filter to limit the resultset. Predicate expression can be null, in which case it will be ignored.</param>
		/// <returns>true if succeeded, false otherwise</returns>
		public bool GetMultiManyToOne(IEntity infraredConfigurationEntityInstance, IEntity roomControlComponentEntityInstance, IEntity roomControlComponentEntity_Instance, IEntity roomControlComponentEntity__Instance, IEntity roomControlComponentEntity___Instance, IEntity roomControlComponentEntity____Instance, IEntity roomControlComponentEntity_____Instance, IEntity roomControlComponentEntity______Instance, IEntity roomControlComponentEntity_______Instance, IEntity roomControlComponentEntity________Instance, IEntity roomControlComponentEntity_________Instance, IEntity roomControlSectionEntityInstance, IEntity roomControlSectionItemEntityInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPredicateExpression filter)
		{
			return GetMultiManyToOne(infraredConfigurationEntityInstance, roomControlComponentEntityInstance, roomControlComponentEntity_Instance, roomControlComponentEntity__Instance, roomControlComponentEntity___Instance, roomControlComponentEntity____Instance, roomControlComponentEntity_____Instance, roomControlComponentEntity______Instance, roomControlComponentEntity_______Instance, roomControlComponentEntity________Instance, roomControlComponentEntity_________Instance, roomControlSectionEntityInstance, roomControlSectionItemEntityInstance, maxNumberOfItemsToReturn, sortClauses, filter, 0, 0);
		}

		/// <summary> Retrieves in this RoomControlWidgetCollection object all RoomControlWidgetEntity objects which have data in common with the specified related Entities.
		/// If one is omitted, that entity is not used as a filter. All current elements in the collection are removed from the collection.</summary>
		/// <param name="infraredConfigurationEntityInstance">InfraredConfigurationEntity instance to use as a filter for the RoomControlWidgetEntity objects to return</param>
		/// <param name="roomControlComponentEntityInstance">RoomControlComponentEntity instance to use as a filter for the RoomControlWidgetEntity objects to return</param>
		/// <param name="roomControlComponentEntity_Instance">RoomControlComponentEntity instance to use as a filter for the RoomControlWidgetEntity objects to return</param>
		/// <param name="roomControlComponentEntity__Instance">RoomControlComponentEntity instance to use as a filter for the RoomControlWidgetEntity objects to return</param>
		/// <param name="roomControlComponentEntity___Instance">RoomControlComponentEntity instance to use as a filter for the RoomControlWidgetEntity objects to return</param>
		/// <param name="roomControlComponentEntity____Instance">RoomControlComponentEntity instance to use as a filter for the RoomControlWidgetEntity objects to return</param>
		/// <param name="roomControlComponentEntity_____Instance">RoomControlComponentEntity instance to use as a filter for the RoomControlWidgetEntity objects to return</param>
		/// <param name="roomControlComponentEntity______Instance">RoomControlComponentEntity instance to use as a filter for the RoomControlWidgetEntity objects to return</param>
		/// <param name="roomControlComponentEntity_______Instance">RoomControlComponentEntity instance to use as a filter for the RoomControlWidgetEntity objects to return</param>
		/// <param name="roomControlComponentEntity________Instance">RoomControlComponentEntity instance to use as a filter for the RoomControlWidgetEntity objects to return</param>
		/// <param name="roomControlComponentEntity_________Instance">RoomControlComponentEntity instance to use as a filter for the RoomControlWidgetEntity objects to return</param>
		/// <param name="roomControlSectionEntityInstance">RoomControlSectionEntity instance to use as a filter for the RoomControlWidgetEntity objects to return</param>
		/// <param name="roomControlSectionItemEntityInstance">RoomControlSectionItemEntity instance to use as a filter for the RoomControlWidgetEntity objects to return</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="filter">Extra filter to limit the resultset. Predicate expression can be null, in which case it will be ignored.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToOne(IEntity infraredConfigurationEntityInstance, IEntity roomControlComponentEntityInstance, IEntity roomControlComponentEntity_Instance, IEntity roomControlComponentEntity__Instance, IEntity roomControlComponentEntity___Instance, IEntity roomControlComponentEntity____Instance, IEntity roomControlComponentEntity_____Instance, IEntity roomControlComponentEntity______Instance, IEntity roomControlComponentEntity_______Instance, IEntity roomControlComponentEntity________Instance, IEntity roomControlComponentEntity_________Instance, IEntity roomControlSectionEntityInstance, IEntity roomControlSectionItemEntityInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPredicateExpression filter, int pageNumber, int pageSize)
		{
			bool validParameters = false;
			validParameters |= (infraredConfigurationEntityInstance!=null);
			validParameters |= (roomControlComponentEntityInstance!=null);
			validParameters |= (roomControlComponentEntity_Instance!=null);
			validParameters |= (roomControlComponentEntity__Instance!=null);
			validParameters |= (roomControlComponentEntity___Instance!=null);
			validParameters |= (roomControlComponentEntity____Instance!=null);
			validParameters |= (roomControlComponentEntity_____Instance!=null);
			validParameters |= (roomControlComponentEntity______Instance!=null);
			validParameters |= (roomControlComponentEntity_______Instance!=null);
			validParameters |= (roomControlComponentEntity________Instance!=null);
			validParameters |= (roomControlComponentEntity_________Instance!=null);
			validParameters |= (roomControlSectionEntityInstance!=null);
			validParameters |= (roomControlSectionItemEntityInstance!=null);
			if(!validParameters)
			{
				return GetMulti(filter, maxNumberOfItemsToReturn, sortClauses, null, pageNumber, pageSize);
			}
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateRoomControlWidgetDAO().GetMulti(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, filter, infraredConfigurationEntityInstance, roomControlComponentEntityInstance, roomControlComponentEntity_Instance, roomControlComponentEntity__Instance, roomControlComponentEntity___Instance, roomControlComponentEntity____Instance, roomControlComponentEntity_____Instance, roomControlComponentEntity______Instance, roomControlComponentEntity_______Instance, roomControlComponentEntity________Instance, roomControlComponentEntity_________Instance, roomControlSectionEntityInstance, roomControlSectionItemEntityInstance, pageNumber, pageSize);
		}

		/// <summary> Deletes from the persistent storage all RoomControlWidget entities which have data in common with the specified related Entities. If one is omitted, that entity is not used as a filter.</summary>
		/// <remarks>Runs directly on the persistent storage. It will not delete entity objects from the current collection.</remarks>
		/// <param name="infraredConfigurationEntityInstance">InfraredConfigurationEntity instance to use as a filter for the RoomControlWidgetEntity objects to return</param>
		/// <param name="roomControlComponentEntityInstance">RoomControlComponentEntity instance to use as a filter for the RoomControlWidgetEntity objects to return</param>
		/// <param name="roomControlComponentEntity_Instance">RoomControlComponentEntity instance to use as a filter for the RoomControlWidgetEntity objects to return</param>
		/// <param name="roomControlComponentEntity__Instance">RoomControlComponentEntity instance to use as a filter for the RoomControlWidgetEntity objects to return</param>
		/// <param name="roomControlComponentEntity___Instance">RoomControlComponentEntity instance to use as a filter for the RoomControlWidgetEntity objects to return</param>
		/// <param name="roomControlComponentEntity____Instance">RoomControlComponentEntity instance to use as a filter for the RoomControlWidgetEntity objects to return</param>
		/// <param name="roomControlComponentEntity_____Instance">RoomControlComponentEntity instance to use as a filter for the RoomControlWidgetEntity objects to return</param>
		/// <param name="roomControlComponentEntity______Instance">RoomControlComponentEntity instance to use as a filter for the RoomControlWidgetEntity objects to return</param>
		/// <param name="roomControlComponentEntity_______Instance">RoomControlComponentEntity instance to use as a filter for the RoomControlWidgetEntity objects to return</param>
		/// <param name="roomControlComponentEntity________Instance">RoomControlComponentEntity instance to use as a filter for the RoomControlWidgetEntity objects to return</param>
		/// <param name="roomControlComponentEntity_________Instance">RoomControlComponentEntity instance to use as a filter for the RoomControlWidgetEntity objects to return</param>
		/// <param name="roomControlSectionEntityInstance">RoomControlSectionEntity instance to use as a filter for the RoomControlWidgetEntity objects to return</param>
		/// <param name="roomControlSectionItemEntityInstance">RoomControlSectionItemEntity instance to use as a filter for the RoomControlWidgetEntity objects to return</param>
		/// <returns>Amount of entities affected, if the used persistent storage has rowcounting enabled.</returns>
		public int DeleteMultiManyToOne(IEntity infraredConfigurationEntityInstance, IEntity roomControlComponentEntityInstance, IEntity roomControlComponentEntity_Instance, IEntity roomControlComponentEntity__Instance, IEntity roomControlComponentEntity___Instance, IEntity roomControlComponentEntity____Instance, IEntity roomControlComponentEntity_____Instance, IEntity roomControlComponentEntity______Instance, IEntity roomControlComponentEntity_______Instance, IEntity roomControlComponentEntity________Instance, IEntity roomControlComponentEntity_________Instance, IEntity roomControlSectionEntityInstance, IEntity roomControlSectionItemEntityInstance)
		{
			return DAOFactory.CreateRoomControlWidgetDAO().DeleteMulti(this.Transaction, infraredConfigurationEntityInstance, roomControlComponentEntityInstance, roomControlComponentEntity_Instance, roomControlComponentEntity__Instance, roomControlComponentEntity___Instance, roomControlComponentEntity____Instance, roomControlComponentEntity_____Instance, roomControlComponentEntity______Instance, roomControlComponentEntity_______Instance, roomControlComponentEntity________Instance, roomControlComponentEntity_________Instance, roomControlSectionEntityInstance, roomControlSectionItemEntityInstance);
		}

		/// <summary> Updates in the persistent storage all RoomControlWidget entities which have data in common with the specified related Entities. If one is omitted, that entity is not used as a filter.
		/// Which fields are updated in those matching entities depends on which fields are <i>changed</i> in the passed in entity entityWithNewValues. The new values of these fields are read from entityWithNewValues. </summary>
		/// <param name="entityWithNewValues">RoomControlWidgetEntity instance which holds the new values for the matching entities to update. Only changed fields are taken into account</param>
		/// <param name="infraredConfigurationEntityInstance">InfraredConfigurationEntity instance to use as a filter for the RoomControlWidgetEntity objects to return</param>
		/// <param name="roomControlComponentEntityInstance">RoomControlComponentEntity instance to use as a filter for the RoomControlWidgetEntity objects to return</param>
		/// <param name="roomControlComponentEntity_Instance">RoomControlComponentEntity instance to use as a filter for the RoomControlWidgetEntity objects to return</param>
		/// <param name="roomControlComponentEntity__Instance">RoomControlComponentEntity instance to use as a filter for the RoomControlWidgetEntity objects to return</param>
		/// <param name="roomControlComponentEntity___Instance">RoomControlComponentEntity instance to use as a filter for the RoomControlWidgetEntity objects to return</param>
		/// <param name="roomControlComponentEntity____Instance">RoomControlComponentEntity instance to use as a filter for the RoomControlWidgetEntity objects to return</param>
		/// <param name="roomControlComponentEntity_____Instance">RoomControlComponentEntity instance to use as a filter for the RoomControlWidgetEntity objects to return</param>
		/// <param name="roomControlComponentEntity______Instance">RoomControlComponentEntity instance to use as a filter for the RoomControlWidgetEntity objects to return</param>
		/// <param name="roomControlComponentEntity_______Instance">RoomControlComponentEntity instance to use as a filter for the RoomControlWidgetEntity objects to return</param>
		/// <param name="roomControlComponentEntity________Instance">RoomControlComponentEntity instance to use as a filter for the RoomControlWidgetEntity objects to return</param>
		/// <param name="roomControlComponentEntity_________Instance">RoomControlComponentEntity instance to use as a filter for the RoomControlWidgetEntity objects to return</param>
		/// <param name="roomControlSectionEntityInstance">RoomControlSectionEntity instance to use as a filter for the RoomControlWidgetEntity objects to return</param>
		/// <param name="roomControlSectionItemEntityInstance">RoomControlSectionItemEntity instance to use as a filter for the RoomControlWidgetEntity objects to return</param>
		/// <returns>Amount of entities affected, if the used persistent storage has rowcounting enabled.</returns>
		public int UpdateMultiManyToOne(RoomControlWidgetEntity entityWithNewValues, IEntity infraredConfigurationEntityInstance, IEntity roomControlComponentEntityInstance, IEntity roomControlComponentEntity_Instance, IEntity roomControlComponentEntity__Instance, IEntity roomControlComponentEntity___Instance, IEntity roomControlComponentEntity____Instance, IEntity roomControlComponentEntity_____Instance, IEntity roomControlComponentEntity______Instance, IEntity roomControlComponentEntity_______Instance, IEntity roomControlComponentEntity________Instance, IEntity roomControlComponentEntity_________Instance, IEntity roomControlSectionEntityInstance, IEntity roomControlSectionItemEntityInstance)
		{
			return DAOFactory.CreateRoomControlWidgetDAO().UpdateMulti(entityWithNewValues, this.Transaction, infraredConfigurationEntityInstance, roomControlComponentEntityInstance, roomControlComponentEntity_Instance, roomControlComponentEntity__Instance, roomControlComponentEntity___Instance, roomControlComponentEntity____Instance, roomControlComponentEntity_____Instance, roomControlComponentEntity______Instance, roomControlComponentEntity_______Instance, roomControlComponentEntity________Instance, roomControlComponentEntity_________Instance, roomControlSectionEntityInstance, roomControlSectionItemEntityInstance);
		}


		/// <summary> Retrieves Entity rows in a datatable which match the specified filter. It will always create a new connection to the database.</summary>
		/// <param name="selectFilter">A predicate or predicate expression which should be used as filter for the entities to retrieve.</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>DataTable with the rows requested.</returns>
		public static DataTable GetMultiAsDataTable(IPredicate selectFilter, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiAsDataTable(selectFilter, maxNumberOfItemsToReturn, sortClauses, null, 0, 0);
		}

		/// <summary> Retrieves Entity rows in a datatable which match the specified filter. It will always create a new connection to the database.</summary>
		/// <param name="selectFilter">A predicate or predicate expression which should be used as filter for the entities to retrieve.</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="relations">The set of relations to walk to construct to total query.</param>
		/// <returns>DataTable with the rows requested.</returns>
		public static DataTable GetMultiAsDataTable(IPredicate selectFilter, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IRelationCollection relations)
		{
			return GetMultiAsDataTable(selectFilter, maxNumberOfItemsToReturn, sortClauses, relations, 0, 0);
		}
		
		/// <summary> Retrieves Entity rows in a datatable which match the specified filter. It will always create a new connection to the database.</summary>
		/// <param name="selectFilter">A predicate or predicate expression which should be used as filter for the entities to retrieve.</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="relations">The set of relations to walk to construct to total query.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>DataTable with the rows requested.</returns>
		public static DataTable GetMultiAsDataTable(IPredicate selectFilter, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IRelationCollection relations, int pageNumber, int pageSize)
		{
			RoomControlWidgetDAO dao = DAOFactory.CreateRoomControlWidgetDAO();
			return dao.GetMultiAsDataTable(maxNumberOfItemsToReturn, sortClauses, selectFilter, relations, pageNumber, pageSize);
		}


		
		/// <summary> Gets a scalar value, calculated with the aggregate. the field index specified is the field the aggregate are applied on.</summary>
		/// <param name="fieldIndex">Field index of field to which to apply the aggregate function and expression</param>
		/// <param name="aggregateToApply">Aggregate function to apply. </param>
		/// <returns>the scalar value requested</returns>
		public object GetScalar(RoomControlWidgetFieldIndex fieldIndex, AggregateFunction aggregateToApply)
		{
			return GetScalar(fieldIndex, null, aggregateToApply, null, null, null);
		}

		/// <summary> Gets a scalar value, calculated with the aggregate and expression specified. the field index specified is the field the expression and aggregate are applied on.</summary>
		/// <param name="fieldIndex">Field index of field to which to apply the aggregate function and expression</param>
		/// <param name="expressionToExecute">The expression to execute. Can be null</param>
		/// <param name="aggregateToApply">Aggregate function to apply. </param>
		/// <returns>the scalar value requested</returns>
		public object GetScalar(RoomControlWidgetFieldIndex fieldIndex, IExpression expressionToExecute, AggregateFunction aggregateToApply)
		{
			return GetScalar(fieldIndex, expressionToExecute, aggregateToApply, null, null, null);
		}

		/// <summary> Gets a scalar value, calculated with the aggregate and expression specified. the field index specified is the field the expression and aggregate are applied on.</summary>
		/// <param name="fieldIndex">Field index of field to which to apply the aggregate function and expression</param>
		/// <param name="expressionToExecute">The expression to execute. Can be null</param>
		/// <param name="aggregateToApply">Aggregate function to apply. </param>
		/// <param name="filter">The filter to apply to retrieve the scalar</param>
		/// <returns>the scalar value requested</returns>
		public object GetScalar(RoomControlWidgetFieldIndex fieldIndex, IExpression expressionToExecute, AggregateFunction aggregateToApply, IPredicate filter)
		{
			return GetScalar(fieldIndex, expressionToExecute, aggregateToApply, filter, null, null);
		}

		/// <summary> Gets a scalar value, calculated with the aggregate and expression specified. the field index specified is the field the expression and aggregate are applied on.</summary>
		/// <param name="fieldIndex">Field index of field to which to apply the aggregate function and expression</param>
		/// <param name="expressionToExecute">The expression to execute. Can be null</param>
		/// <param name="aggregateToApply">Aggregate function to apply. </param>
		/// <param name="filter">The filter to apply to retrieve the scalar</param>
		/// <param name="groupByClause">The groupby clause to apply to retrieve the scalar</param>
		/// <returns>the scalar value requested</returns>
		public object GetScalar(RoomControlWidgetFieldIndex fieldIndex, IExpression expressionToExecute, AggregateFunction aggregateToApply, IPredicate filter, IGroupByCollection groupByClause)
		{
			return GetScalar(fieldIndex, expressionToExecute, aggregateToApply, filter, null, groupByClause);
		}

		/// <summary> Gets a scalar value, calculated with the aggregate and expression specified. the field index specified is the field the expression and aggregate are applied on.</summary>
		/// <param name="fieldIndex">Field index of field to which to apply the aggregate function and expression</param>
		/// <param name="expressionToExecute">The expression to execute. Can be null</param>
		/// <param name="aggregateToApply">Aggregate function to apply. </param>
		/// <param name="filter">The filter to apply to retrieve the scalar</param>
		/// <param name="relations">The relations to walk</param>
		/// <param name="groupByClause">The groupby clause to apply to retrieve the scalar</param>
		/// <returns>the scalar value requested</returns>
		public virtual object GetScalar(RoomControlWidgetFieldIndex fieldIndex, IExpression expressionToExecute, AggregateFunction aggregateToApply, IPredicate filter, IRelationCollection relations, IGroupByCollection groupByClause)
		{
			EntityFields fields = new EntityFields(1);
			fields[0] = EntityFieldFactory.Create(fieldIndex);
			if((fields[0].ExpressionToApply == null) || (expressionToExecute != null))
			{
				fields[0].ExpressionToApply = expressionToExecute;
			}
			if((fields[0].AggregateFunctionToApply == AggregateFunction.None) || (aggregateToApply != AggregateFunction.None))
			{
				fields[0].AggregateFunctionToApply = aggregateToApply;
			}
			return DAOFactory.CreateRoomControlWidgetDAO().GetScalar(fields, this.Transaction, filter, relations, groupByClause);
		}
		
		/// <summary>Creats a new DAO instance so code which is in the base class can still use the proper DAO object.</summary>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateRoomControlWidgetDAO();
		}
		
		/// <summary>Creates a new transaction object</summary>
		/// <param name="levelOfIsolation">The level of isolation.</param>
		/// <param name="name">The name.</param>
		protected override ITransaction CreateTransaction( IsolationLevel levelOfIsolation, string name )
		{
			return new Transaction(levelOfIsolation, name);
		}

		#region Custom EntityCollection code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCollectionCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion
		
		#region Included Code

		#endregion
	}
}
