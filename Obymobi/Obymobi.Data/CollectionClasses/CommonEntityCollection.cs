﻿using Dionysos;
using Obymobi.Data.DaoClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System;
using System.Data;
using System.IO;
using System.Runtime.Serialization;
using System.Text;

namespace Obymobi.Data.CollectionClasses
{
    [Serializable]
    public abstract partial class CommonEntityCollection<TEntity> : EntityCollectionBase<TEntity> where TEntity : SD.LLBLGen.Pro.ORMSupportClasses.EntityBase
    {
        private static DateTime lastSaveException = DateTime.MinValue;
        private static object lastSaveExceptionLock = new object();

        #region Constructors

        /// <summary> CTor</summary>
        /// <param name="entityFactoryToUse">The EntityFactory to use when creating entity objects during a GetMulti() call.</param>
        public CommonEntityCollection(IEntityFactory entityFactoryToUse)
            : base(entityFactoryToUse)
        {
        }

        /// <summary> Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
        protected CommonEntityCollection(SerializationInfo info, StreamingContext context)
            : base(info, context)
		{
		}

        #endregion

        #region GetMulti Overrides

        /// <summary> Retrieves in this Collection object all Entity objects which match with the specified filter, formulated in the predicate or predicate expression definition.</summary>
        /// <param name="selectFilter">A predicate or predicate expression which should be used as filter for the entities to retrieve. When set to null all entities will be retrieved (no filtering is being performed)</param>
        /// <returns>true if succeeded, false otherwise</returns>
        public new bool GetMulti(IPredicate selectFilter)
        {
            return GetMulti(selectFilter, this.MaxNumberOfItemsToReturn, this.SortClauses, null, null, null, 0, 0);
        }

        /// <summary> Retrieves in this Collection object all Entity objects which match with the specified filter, formulated in the predicate or predicate expression definition.</summary>
        /// <param name="selectFilter">A predicate or predicate expression which should be used as filter for the entities to retrieve. When set to null all entities will be retrieved (no filtering is being performed)</param>
        /// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
        /// <returns>true if succeeded, false otherwise</returns>
        public new bool GetMulti(IPredicate selectFilter, long maxNumberOfItemsToReturn)
        {
            return GetMulti(selectFilter, maxNumberOfItemsToReturn, this.SortClauses, null, null, null, 0, 0);
        }

        /// <summary> Retrieves in this Collection object all Entity objects which match with the specified filter, formulated in the predicate or predicate expression definition.</summary>
        /// <param name="selectFilter">A predicate or predicate expression which should be used as filter for the entities to retrieve. When set to null all entities will be retrieved (no filtering is being performed)</param>
        /// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
        /// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
        /// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
        /// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
        /// <returns>true if succeeded, false otherwise</returns>
        public new bool GetMulti(IPredicate selectFilter, ExcludeIncludeFieldsList excludedIncludedFields, long maxNumberOfItemsToReturn)
        {
            return GetMulti(selectFilter, maxNumberOfItemsToReturn, this.SortClauses, null, null, excludedIncludedFields, 0, 0);
        }

        /// <summary> Retrieves in this Collection object all Entity objects which match with the specified filter, formulated in the predicate or predicate expression definition.</summary>
        /// <param name="selectFilter">A predicate or predicate expression which should be used as filter for the entities to retrieve. When set to null all entities will be retrieved (no filtering is being performed)</param>
        /// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
        /// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
        /// <returns>true if succeeded, false otherwise</returns>
        public new bool GetMulti(IPredicate selectFilter, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
        {
            return GetMulti(selectFilter, maxNumberOfItemsToReturn, sortClauses, null, null, null, 0, 0);
        }

        /// <summary> Retrieves in this Collection object all Entity objects which match with the specified filter, formulated in the predicate or predicate expression definition.</summary>
        /// <param name="selectFilter">A predicate or predicate expression which should be used as filter for the entities to retrieve.</param>
        /// <param name="relations">The set of relations to walk to construct the total query.</param>
        /// <returns>true if succeeded, false otherwise</returns>
        public new bool GetMulti(IPredicate selectFilter, IRelationCollection relations)
        {
            return GetMulti(selectFilter, this.MaxNumberOfItemsToReturn, this.SortClauses, relations, null, null, 0, 0);
        }

        /// <summary> Retrieves in this Collection object all Entity objects which match with the specified filter, formulated in the predicate or predicate expression definition.</summary>
        /// <param name="selectFilter">A predicate or predicate expression which should be used as filter for the entities to retrieve.</param>
        /// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
        /// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
        /// <param name="relations">The set of relations to walk to construct the total query.</param>
        /// <returns>true if succeeded, false otherwise</returns>
        public new bool GetMulti(IPredicate selectFilter, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IRelationCollection relations)
        {
            return GetMulti(selectFilter, maxNumberOfItemsToReturn, sortClauses, relations, null, null, 0, 0);
        }

        /// <summary> Retrieves in this Collection object all Entity objects which match with the specified filter, formulated in the predicate or predicate expression definition, using the passed in relations to construct the total query.</summary>
        /// <param name="selectFilter">A predicate or predicate expression which should be used as filter for the entities to retrieve. When set to null all entities will be retrieved (no filtering is being performed)</param>
        /// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
        /// <returns>true if succeeded, false otherwise</returns>
        public new bool GetMulti(IPredicate selectFilter, IPrefetchPath prefetchPathToUse)
        {
            return GetMulti(selectFilter, this.MaxNumberOfItemsToReturn, this.SortClauses, null, prefetchPathToUse, null, 0, 0);
        }

        /// <summary> Retrieves in this Collection object all Entity objects which match with the specified filter, formulated in the predicate or predicate expression definition, using the passed in relations to construct the total query.</summary>
        /// <param name="selectFilter">A predicate or predicate expression which should be used as filter for the entities to retrieve. When set to null all entities will be retrieved (no filtering is being performed)</param>
        /// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
        /// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
        /// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
        /// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
        /// <returns>true if succeeded, false otherwise</returns>
        public new bool GetMulti(IPredicate selectFilter, ExcludeIncludeFieldsList excludedIncludedFields, IPrefetchPath prefetchPathToUse)
        {
            return GetMulti(selectFilter, this.MaxNumberOfItemsToReturn, this.SortClauses, null, prefetchPathToUse, excludedIncludedFields, 0, 0);
        }

        /// <summary> Retrieves in this Collection object all Entity objects which match with the specified filter, formulated in the predicate or predicate expression definition, using the passed in relations to construct the total query.</summary>
        /// <param name="selectFilter">A predicate or predicate expression which should be used as filter for the entities to retrieve.</param>
        /// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
        /// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
        /// <param name="relations">The set of relations to walk to construct the total query.</param>
        /// <param name="pageNumber">The page number to retrieve.</param>
        /// <param name="pageSize">The page size of the page to retrieve.</param>
        /// <returns>true if succeeded, false otherwise</returns>
        public new bool GetMulti(IPredicate selectFilter, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IRelationCollection relations,
                                 int pageNumber, int pageSize)
        {
            return GetMulti(selectFilter, maxNumberOfItemsToReturn, sortClauses, relations, null, null, pageNumber, pageSize);
        }

        /// <summary> Retrieves in this Collection object all Entity objects which match with the specified filter, formulated in the predicate or predicate expression definition, using the passed in relations to construct the total query.</summary>
        /// <param name="selectFilter">A predicate or predicate expression which should be used as filter for the entities to retrieve.</param>
        /// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
        /// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
        /// <param name="relations">The set of relations to walk to construct the total query.</param>
        /// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
        /// <returns>true if succeeded, false otherwise</returns>
        public new bool GetMulti(IPredicate selectFilter, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IRelationCollection relations,
                                 IPrefetchPath prefetchPathToUse)
        {
            return GetMulti(selectFilter, maxNumberOfItemsToReturn, sortClauses, relations, prefetchPathToUse, null, 0, 0);
        }

        /// <summary> Retrieves in this Collection object all Entity objects which match with the specified filter, formulated in the predicate or predicate expression definition, using the passed in relations to construct the total query.</summary>
        /// <param name="selectFilter">A predicate or predicate expression which should be used as filter for the entities to retrieve.</param>
        /// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
        /// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
        /// <param name="relations">The set of relations to walk to construct the total query.</param>
        /// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
        /// <param name="pageNumber">The page number to retrieve.</param>
        /// <param name="pageSize">The page size of the page to retrieve.</param>
        /// <returns>true if succeeded, false otherwise</returns>
        public new bool GetMulti(IPredicate selectFilter, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IRelationCollection relations,
                                 IPrefetchPath prefetchPathToUse, int pageNumber, int pageSize)
        {
            return GetMulti(selectFilter, maxNumberOfItemsToReturn, sortClauses, relations, prefetchPathToUse, null, pageNumber, pageSize);
        }

        /// <summary> Retrieves in this Collection object all Entity objects which match with the specified filter, formulated in the predicate or predicate expression definition, using the passed in relations to construct the total query.</summary>
        /// <param name="selectFilter">A predicate or predicate expression which should be used as filter for the entities to retrieve.</param>
        /// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
        /// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
        /// <param name="relations">The set of relations to walk to construct the total query.</param>
        /// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
        /// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
        /// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
        /// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
        /// <param name="pageNumber">The page number to retrieve.</param>
        /// <param name="pageSize">The page size of the page to retrieve.</param>
        /// <returns>true if succeeded, false otherwise</returns>
        public new bool GetMulti(IPredicate selectFilter, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IRelationCollection relations,
                                 IPrefetchPath prefetchPathToUse, ExcludeIncludeFieldsList excludedIncludedFields, int pageNumber, int pageSize)
        {
            try
            {
                return base.GetMulti(selectFilter, maxNumberOfItemsToReturn, sortClauses, relations, prefetchPathToUse, excludedIncludedFields, pageNumber, pageSize);
            }
            catch (Exception ex)
            {
                // Read stacktrace here otherwise it will be the stacktrace of the Task
                var stacktrace = System.Environment.StackTrace;

                WriteException(ex, stacktrace, "GetMulti");

                // Rethrow exception
                throw;
            }
        }

        #endregion

        #region GetDbCount

        /// <summary> Gets the amount of Entity objects in the database.</summary>
        /// <returns>the amount of objects found</returns>
        public new int GetDbCount()
        {
            return GetDbCount(null, null);
        }

        /// <summary> Gets the amount of Entity objects in the database, when taking into account the filter specified.</summary>
        /// <param name="filter">the filter to apply</param>
        /// <returns>the amount of objects found</returns>
        public new int GetDbCount(IPredicate filter)
        {
            return GetDbCount(filter, null);
        }

        /// <summary> Gets the amount of Entity objects in the database, when taking into account the filter specified and the relations specified.</summary>
        /// <param name="filter">the filter to apply</param>
        /// <param name="relations">The relations to walk</param>
        /// <returns>the amount of objects found</returns>
        public new int GetDbCount(IPredicate filter, IRelationCollection relations)
        {
            try
            {
                return base.GetDbCount(filter, relations);
            }
            catch (Exception ex)
            {
                // Read stacktrace here otherwise it will be the stacktrace of the Task
                var stacktrace = System.Environment.StackTrace;

                WriteException(ex, stacktrace, "GetDbCount");

                // Rethrow exception
                throw;
            }            
        }

        #endregion

        private void WriteException(Exception ex, string stacktrace, string methodName)
        {
            bool canRun = true;
            DateTime utcNow = DateTime.UtcNow;
            lock (lastSaveExceptionLock)
            {
                // Only run this every 10 seconds so we dont put extra pressure on the database
                if ((utcNow - lastSaveException).TotalSeconds < 10)
                    canRun = false;

                lastSaveException = utcNow;
            }

            if (canRun)
            {
                System.Threading.Tasks.Task.Factory.StartNew(() =>
                {
                    var errorMessage = new StringBuilder();
                    errorMessage.AppendLine("Time (UTC): " + utcNow);
                    errorMessage.AppendLine("Method: " + methodName);
                    errorMessage.AppendLine("Entity: " + this.ContainingEntity.LLBLGenProEntityName);
                    errorMessage.AppendLine("Transaction: " + ((this.Transaction != null) ? this.Transaction.Name : "None"));
                    errorMessage.AppendLine("Exception: " + ex.Message);
                    errorMessage.AppendLine("Stacktrace:");
                    errorMessage.AppendLine(stacktrace);
                    errorMessage.AppendLine("Stacktrace #2:");
                    errorMessage.AppendLine(ex.ProcessStackTrace(true));

                    try
                    {
                        // Write to disk
                        string path = System.Web.Hosting.HostingEnvironment.MapPath("~/App_Data/Error");
                        Directory.CreateDirectory(path);
                        string fileName = Path.Combine(path, string.Format("LLBLGen-{0}-{1}.txt", Dionysos.TimeStamp.CreateTimeStamp(utcNow), utcNow.Ticks.ToString()));
                        File.WriteAllText(fileName, errorMessage.ToString());
                    }
                    catch
                    {
                        System.Diagnostics.Debug.WriteLine(errorMessage);
                    }
                });
            }            
        }

        protected override IDao CreateDAOInstance()
        {
            throw new NotImplementedException();
        }

        protected override ITransaction CreateTransaction(System.Data.IsolationLevel levelOfIsolation, string name)
        {
            throw new NotImplementedException();
        }
    }
}
