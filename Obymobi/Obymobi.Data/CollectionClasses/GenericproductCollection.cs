﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Data;
using System.Collections.Generic;
using System.ComponentModel;
using System.Xml;
#if !CF
using System.Runtime.Serialization;
#endif

using Obymobi.Data.EntityClasses;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.CollectionClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Collection class for storing and retrieving collections of GenericproductEntity objects. </summary>
	[Serializable]
	public partial class GenericproductCollection : CommonEntityCollection<GenericproductEntity>
	{
		/// <summary> CTor</summary>
		public GenericproductCollection():base(new GenericproductEntityFactory())
		{
		}

		/// <summary> CTor</summary>
		/// <param name="initialContents">The initial contents of this collection.</param>
		public GenericproductCollection(IEnumerable<GenericproductEntity> initialContents):base(new GenericproductEntityFactory())
		{
			AddRange(initialContents);
		}

		/// <summary> CTor</summary>
		/// <param name="entityFactoryToUse">The EntityFactory to use when creating entity objects during a GetMulti() call.</param>
		public GenericproductCollection(IEntityFactory entityFactoryToUse):base(entityFactoryToUse)
		{
		}

		/// <summary> Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected GenericproductCollection(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}


		/// <summary> Retrieves in this GenericproductCollection object all GenericproductEntity objects which have data in common with the specified related Entities.
		/// If one is omitted, that entity is not used as a filter. All current elements in the collection are removed from the collection.</summary>
		/// <param name="brandEntityInstance">BrandEntity instance to use as a filter for the GenericproductEntity objects to return</param>
		/// <param name="genericcategoryEntityInstance">GenericcategoryEntity instance to use as a filter for the GenericproductEntity objects to return</param>
		/// <param name="supplierEntityInstance">SupplierEntity instance to use as a filter for the GenericproductEntity objects to return</param>
		/// <param name="vattariffEntityInstance">VattariffEntity instance to use as a filter for the GenericproductEntity objects to return</param>
		/// <returns>true if succeeded, false otherwise</returns>
		public bool GetMultiManyToOne(IEntity brandEntityInstance, IEntity genericcategoryEntityInstance, IEntity supplierEntityInstance, IEntity vattariffEntityInstance)
		{
			return GetMultiManyToOne(brandEntityInstance, genericcategoryEntityInstance, supplierEntityInstance, vattariffEntityInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, null, 0, 0);
		}

		/// <summary> Retrieves in this GenericproductCollection object all GenericproductEntity objects which have data in common with the specified related Entities.
		/// If one is omitted, that entity is not used as a filter. All current elements in the collection are removed from the collection.</summary>
		/// <param name="brandEntityInstance">BrandEntity instance to use as a filter for the GenericproductEntity objects to return</param>
		/// <param name="genericcategoryEntityInstance">GenericcategoryEntity instance to use as a filter for the GenericproductEntity objects to return</param>
		/// <param name="supplierEntityInstance">SupplierEntity instance to use as a filter for the GenericproductEntity objects to return</param>
		/// <param name="vattariffEntityInstance">VattariffEntity instance to use as a filter for the GenericproductEntity objects to return</param>
		/// <param name="filter">Extra filter to limit the resultset. Predicate expression can be null, in which case it will be ignored.</param>
		/// <returns>true if succeeded, false otherwise</returns>
		public bool GetMultiManyToOne(IEntity brandEntityInstance, IEntity genericcategoryEntityInstance, IEntity supplierEntityInstance, IEntity vattariffEntityInstance, IPredicateExpression filter)
		{
			return GetMultiManyToOne(brandEntityInstance, genericcategoryEntityInstance, supplierEntityInstance, vattariffEntityInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, filter, 0, 0);
		}

		/// <summary> Retrieves in this GenericproductCollection object all GenericproductEntity objects which have data in common with the specified related Entities.
		/// If one is omitted, that entity is not used as a filter. All current elements in the collection are removed from the collection.</summary>
		/// <param name="brandEntityInstance">BrandEntity instance to use as a filter for the GenericproductEntity objects to return</param>
		/// <param name="genericcategoryEntityInstance">GenericcategoryEntity instance to use as a filter for the GenericproductEntity objects to return</param>
		/// <param name="supplierEntityInstance">SupplierEntity instance to use as a filter for the GenericproductEntity objects to return</param>
		/// <param name="vattariffEntityInstance">VattariffEntity instance to use as a filter for the GenericproductEntity objects to return</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="filter">Extra filter to limit the resultset. Predicate expression can be null, in which case it will be ignored.</param>
		/// <returns>true if succeeded, false otherwise</returns>
		public bool GetMultiManyToOne(IEntity brandEntityInstance, IEntity genericcategoryEntityInstance, IEntity supplierEntityInstance, IEntity vattariffEntityInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPredicateExpression filter)
		{
			return GetMultiManyToOne(brandEntityInstance, genericcategoryEntityInstance, supplierEntityInstance, vattariffEntityInstance, maxNumberOfItemsToReturn, sortClauses, filter, 0, 0);
		}

		/// <summary> Retrieves in this GenericproductCollection object all GenericproductEntity objects which have data in common with the specified related Entities.
		/// If one is omitted, that entity is not used as a filter. All current elements in the collection are removed from the collection.</summary>
		/// <param name="brandEntityInstance">BrandEntity instance to use as a filter for the GenericproductEntity objects to return</param>
		/// <param name="genericcategoryEntityInstance">GenericcategoryEntity instance to use as a filter for the GenericproductEntity objects to return</param>
		/// <param name="supplierEntityInstance">SupplierEntity instance to use as a filter for the GenericproductEntity objects to return</param>
		/// <param name="vattariffEntityInstance">VattariffEntity instance to use as a filter for the GenericproductEntity objects to return</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="filter">Extra filter to limit the resultset. Predicate expression can be null, in which case it will be ignored.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToOne(IEntity brandEntityInstance, IEntity genericcategoryEntityInstance, IEntity supplierEntityInstance, IEntity vattariffEntityInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPredicateExpression filter, int pageNumber, int pageSize)
		{
			bool validParameters = false;
			validParameters |= (brandEntityInstance!=null);
			validParameters |= (genericcategoryEntityInstance!=null);
			validParameters |= (supplierEntityInstance!=null);
			validParameters |= (vattariffEntityInstance!=null);
			if(!validParameters)
			{
				return GetMulti(filter, maxNumberOfItemsToReturn, sortClauses, null, pageNumber, pageSize);
			}
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateGenericproductDAO().GetMulti(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, filter, brandEntityInstance, genericcategoryEntityInstance, supplierEntityInstance, vattariffEntityInstance, pageNumber, pageSize);
		}

		/// <summary> Deletes from the persistent storage all Genericproduct entities which have data in common with the specified related Entities. If one is omitted, that entity is not used as a filter.</summary>
		/// <remarks>Runs directly on the persistent storage. It will not delete entity objects from the current collection.</remarks>
		/// <param name="brandEntityInstance">BrandEntity instance to use as a filter for the GenericproductEntity objects to return</param>
		/// <param name="genericcategoryEntityInstance">GenericcategoryEntity instance to use as a filter for the GenericproductEntity objects to return</param>
		/// <param name="supplierEntityInstance">SupplierEntity instance to use as a filter for the GenericproductEntity objects to return</param>
		/// <param name="vattariffEntityInstance">VattariffEntity instance to use as a filter for the GenericproductEntity objects to return</param>
		/// <returns>Amount of entities affected, if the used persistent storage has rowcounting enabled.</returns>
		public int DeleteMultiManyToOne(IEntity brandEntityInstance, IEntity genericcategoryEntityInstance, IEntity supplierEntityInstance, IEntity vattariffEntityInstance)
		{
			return DAOFactory.CreateGenericproductDAO().DeleteMulti(this.Transaction, brandEntityInstance, genericcategoryEntityInstance, supplierEntityInstance, vattariffEntityInstance);
		}

		/// <summary> Updates in the persistent storage all Genericproduct entities which have data in common with the specified related Entities. If one is omitted, that entity is not used as a filter.
		/// Which fields are updated in those matching entities depends on which fields are <i>changed</i> in the passed in entity entityWithNewValues. The new values of these fields are read from entityWithNewValues. </summary>
		/// <param name="entityWithNewValues">GenericproductEntity instance which holds the new values for the matching entities to update. Only changed fields are taken into account</param>
		/// <param name="brandEntityInstance">BrandEntity instance to use as a filter for the GenericproductEntity objects to return</param>
		/// <param name="genericcategoryEntityInstance">GenericcategoryEntity instance to use as a filter for the GenericproductEntity objects to return</param>
		/// <param name="supplierEntityInstance">SupplierEntity instance to use as a filter for the GenericproductEntity objects to return</param>
		/// <param name="vattariffEntityInstance">VattariffEntity instance to use as a filter for the GenericproductEntity objects to return</param>
		/// <returns>Amount of entities affected, if the used persistent storage has rowcounting enabled.</returns>
		public int UpdateMultiManyToOne(GenericproductEntity entityWithNewValues, IEntity brandEntityInstance, IEntity genericcategoryEntityInstance, IEntity supplierEntityInstance, IEntity vattariffEntityInstance)
		{
			return DAOFactory.CreateGenericproductDAO().UpdateMulti(entityWithNewValues, this.Transaction, brandEntityInstance, genericcategoryEntityInstance, supplierEntityInstance, vattariffEntityInstance);
		}

		/// <summary> Retrieves in this GenericproductCollection object all GenericproductEntity objects which are related via a  Relation of type 'm:n' with the passed in AlterationoptionEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="alterationoptionInstance">AlterationoptionEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingAlterationoptionCollectionViaMedium(IEntity alterationoptionInstance)
		{
			return GetMultiManyToManyUsingAlterationoptionCollectionViaMedium(alterationoptionInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this GenericproductCollection object all GenericproductEntity objects which are related via a  relation of type 'm:n' with the passed in AlterationoptionEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="alterationoptionInstance">AlterationoptionEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingAlterationoptionCollectionViaMedium(IEntity alterationoptionInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingAlterationoptionCollectionViaMedium(alterationoptionInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this GenericproductCollection object all GenericproductEntity objects which are related via a Relation of type 'm:n' with the passed in AlterationoptionEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="alterationoptionInstance">AlterationoptionEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingAlterationoptionCollectionViaMedium(IEntity alterationoptionInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingAlterationoptionCollectionViaMedium(alterationoptionInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this GenericproductCollection object all GenericproductEntity objects which are related via a  relation of type 'm:n' with the passed in AlterationoptionEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="alterationoptionInstance">AlterationoptionEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingAlterationoptionCollectionViaMedium(IEntity alterationoptionInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateGenericproductDAO().GetMultiUsingAlterationoptionCollectionViaMedium(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, alterationoptionInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this GenericproductCollection object all GenericproductEntity objects which are related via a  relation of type 'm:n' with the passed in AlterationoptionEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="alterationoptionInstance">AlterationoptionEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingAlterationoptionCollectionViaMedium(IEntity alterationoptionInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateGenericproductDAO().GetMultiUsingAlterationoptionCollectionViaMedium(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, alterationoptionInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this GenericproductCollection object all GenericproductEntity objects which are related via a  Relation of type 'm:n' with the passed in CategoryEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="categoryInstance">CategoryEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCategoryCollectionViaMedium(IEntity categoryInstance)
		{
			return GetMultiManyToManyUsingCategoryCollectionViaMedium(categoryInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this GenericproductCollection object all GenericproductEntity objects which are related via a  relation of type 'm:n' with the passed in CategoryEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="categoryInstance">CategoryEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCategoryCollectionViaMedium(IEntity categoryInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingCategoryCollectionViaMedium(categoryInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this GenericproductCollection object all GenericproductEntity objects which are related via a Relation of type 'm:n' with the passed in CategoryEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="categoryInstance">CategoryEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCategoryCollectionViaMedium(IEntity categoryInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingCategoryCollectionViaMedium(categoryInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this GenericproductCollection object all GenericproductEntity objects which are related via a  relation of type 'm:n' with the passed in CategoryEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="categoryInstance">CategoryEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingCategoryCollectionViaMedium(IEntity categoryInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateGenericproductDAO().GetMultiUsingCategoryCollectionViaMedium(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, categoryInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this GenericproductCollection object all GenericproductEntity objects which are related via a  relation of type 'm:n' with the passed in CategoryEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="categoryInstance">CategoryEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCategoryCollectionViaMedium(IEntity categoryInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateGenericproductDAO().GetMultiUsingCategoryCollectionViaMedium(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, categoryInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this GenericproductCollection object all GenericproductEntity objects which are related via a  Relation of type 'm:n' with the passed in CategoryEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="categoryInstance">CategoryEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCategoryCollectionViaAdvertisement(IEntity categoryInstance)
		{
			return GetMultiManyToManyUsingCategoryCollectionViaAdvertisement(categoryInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this GenericproductCollection object all GenericproductEntity objects which are related via a  relation of type 'm:n' with the passed in CategoryEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="categoryInstance">CategoryEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCategoryCollectionViaAdvertisement(IEntity categoryInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingCategoryCollectionViaAdvertisement(categoryInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this GenericproductCollection object all GenericproductEntity objects which are related via a Relation of type 'm:n' with the passed in CategoryEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="categoryInstance">CategoryEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCategoryCollectionViaAdvertisement(IEntity categoryInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingCategoryCollectionViaAdvertisement(categoryInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this GenericproductCollection object all GenericproductEntity objects which are related via a  relation of type 'm:n' with the passed in CategoryEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="categoryInstance">CategoryEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingCategoryCollectionViaAdvertisement(IEntity categoryInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateGenericproductDAO().GetMultiUsingCategoryCollectionViaAdvertisement(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, categoryInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this GenericproductCollection object all GenericproductEntity objects which are related via a  relation of type 'm:n' with the passed in CategoryEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="categoryInstance">CategoryEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCategoryCollectionViaAdvertisement(IEntity categoryInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateGenericproductDAO().GetMultiUsingCategoryCollectionViaAdvertisement(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, categoryInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this GenericproductCollection object all GenericproductEntity objects which are related via a  Relation of type 'm:n' with the passed in DeliverypointgroupEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="deliverypointgroupInstance">DeliverypointgroupEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingDeliverypointgroupCollectionViaAdvertisement(IEntity deliverypointgroupInstance)
		{
			return GetMultiManyToManyUsingDeliverypointgroupCollectionViaAdvertisement(deliverypointgroupInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this GenericproductCollection object all GenericproductEntity objects which are related via a  relation of type 'm:n' with the passed in DeliverypointgroupEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="deliverypointgroupInstance">DeliverypointgroupEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingDeliverypointgroupCollectionViaAdvertisement(IEntity deliverypointgroupInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingDeliverypointgroupCollectionViaAdvertisement(deliverypointgroupInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this GenericproductCollection object all GenericproductEntity objects which are related via a Relation of type 'm:n' with the passed in DeliverypointgroupEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="deliverypointgroupInstance">DeliverypointgroupEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingDeliverypointgroupCollectionViaAdvertisement(IEntity deliverypointgroupInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingDeliverypointgroupCollectionViaAdvertisement(deliverypointgroupInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this GenericproductCollection object all GenericproductEntity objects which are related via a  relation of type 'm:n' with the passed in DeliverypointgroupEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="deliverypointgroupInstance">DeliverypointgroupEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingDeliverypointgroupCollectionViaAdvertisement(IEntity deliverypointgroupInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateGenericproductDAO().GetMultiUsingDeliverypointgroupCollectionViaAdvertisement(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, deliverypointgroupInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this GenericproductCollection object all GenericproductEntity objects which are related via a  relation of type 'm:n' with the passed in DeliverypointgroupEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="deliverypointgroupInstance">DeliverypointgroupEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingDeliverypointgroupCollectionViaAdvertisement(IEntity deliverypointgroupInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateGenericproductDAO().GetMultiUsingDeliverypointgroupCollectionViaAdvertisement(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, deliverypointgroupInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this GenericproductCollection object all GenericproductEntity objects which are related via a  Relation of type 'm:n' with the passed in EntertainmentEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="entertainmentInstance">EntertainmentEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingEntertainmentCollectionViaAdvertisement(IEntity entertainmentInstance)
		{
			return GetMultiManyToManyUsingEntertainmentCollectionViaAdvertisement(entertainmentInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this GenericproductCollection object all GenericproductEntity objects which are related via a  relation of type 'm:n' with the passed in EntertainmentEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="entertainmentInstance">EntertainmentEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingEntertainmentCollectionViaAdvertisement(IEntity entertainmentInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingEntertainmentCollectionViaAdvertisement(entertainmentInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this GenericproductCollection object all GenericproductEntity objects which are related via a Relation of type 'm:n' with the passed in EntertainmentEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="entertainmentInstance">EntertainmentEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingEntertainmentCollectionViaAdvertisement(IEntity entertainmentInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingEntertainmentCollectionViaAdvertisement(entertainmentInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this GenericproductCollection object all GenericproductEntity objects which are related via a  relation of type 'm:n' with the passed in EntertainmentEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="entertainmentInstance">EntertainmentEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingEntertainmentCollectionViaAdvertisement(IEntity entertainmentInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateGenericproductDAO().GetMultiUsingEntertainmentCollectionViaAdvertisement(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, entertainmentInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this GenericproductCollection object all GenericproductEntity objects which are related via a  relation of type 'm:n' with the passed in EntertainmentEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="entertainmentInstance">EntertainmentEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingEntertainmentCollectionViaAdvertisement(IEntity entertainmentInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateGenericproductDAO().GetMultiUsingEntertainmentCollectionViaAdvertisement(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, entertainmentInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this GenericproductCollection object all GenericproductEntity objects which are related via a  Relation of type 'm:n' with the passed in EntertainmentEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="entertainmentInstance">EntertainmentEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingEntertainmentCollectionViaMedium(IEntity entertainmentInstance)
		{
			return GetMultiManyToManyUsingEntertainmentCollectionViaMedium(entertainmentInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this GenericproductCollection object all GenericproductEntity objects which are related via a  relation of type 'm:n' with the passed in EntertainmentEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="entertainmentInstance">EntertainmentEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingEntertainmentCollectionViaMedium(IEntity entertainmentInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingEntertainmentCollectionViaMedium(entertainmentInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this GenericproductCollection object all GenericproductEntity objects which are related via a Relation of type 'm:n' with the passed in EntertainmentEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="entertainmentInstance">EntertainmentEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingEntertainmentCollectionViaMedium(IEntity entertainmentInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingEntertainmentCollectionViaMedium(entertainmentInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this GenericproductCollection object all GenericproductEntity objects which are related via a  relation of type 'm:n' with the passed in EntertainmentEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="entertainmentInstance">EntertainmentEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingEntertainmentCollectionViaMedium(IEntity entertainmentInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateGenericproductDAO().GetMultiUsingEntertainmentCollectionViaMedium(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, entertainmentInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this GenericproductCollection object all GenericproductEntity objects which are related via a  relation of type 'm:n' with the passed in EntertainmentEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="entertainmentInstance">EntertainmentEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingEntertainmentCollectionViaMedium(IEntity entertainmentInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateGenericproductDAO().GetMultiUsingEntertainmentCollectionViaMedium(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, entertainmentInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this GenericproductCollection object all GenericproductEntity objects which are related via a  Relation of type 'm:n' with the passed in EntertainmentcategoryEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="entertainmentcategoryInstance">EntertainmentcategoryEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingEntertainmentcategoryCollectionViaMedium(IEntity entertainmentcategoryInstance)
		{
			return GetMultiManyToManyUsingEntertainmentcategoryCollectionViaMedium(entertainmentcategoryInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this GenericproductCollection object all GenericproductEntity objects which are related via a  relation of type 'm:n' with the passed in EntertainmentcategoryEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="entertainmentcategoryInstance">EntertainmentcategoryEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingEntertainmentcategoryCollectionViaMedium(IEntity entertainmentcategoryInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingEntertainmentcategoryCollectionViaMedium(entertainmentcategoryInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this GenericproductCollection object all GenericproductEntity objects which are related via a Relation of type 'm:n' with the passed in EntertainmentcategoryEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="entertainmentcategoryInstance">EntertainmentcategoryEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingEntertainmentcategoryCollectionViaMedium(IEntity entertainmentcategoryInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingEntertainmentcategoryCollectionViaMedium(entertainmentcategoryInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this GenericproductCollection object all GenericproductEntity objects which are related via a  relation of type 'm:n' with the passed in EntertainmentcategoryEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="entertainmentcategoryInstance">EntertainmentcategoryEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingEntertainmentcategoryCollectionViaMedium(IEntity entertainmentcategoryInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateGenericproductDAO().GetMultiUsingEntertainmentcategoryCollectionViaMedium(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, entertainmentcategoryInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this GenericproductCollection object all GenericproductEntity objects which are related via a  relation of type 'm:n' with the passed in EntertainmentcategoryEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="entertainmentcategoryInstance">EntertainmentcategoryEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingEntertainmentcategoryCollectionViaMedium(IEntity entertainmentcategoryInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateGenericproductDAO().GetMultiUsingEntertainmentcategoryCollectionViaMedium(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, entertainmentcategoryInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this GenericproductCollection object all GenericproductEntity objects which are related via a  Relation of type 'm:n' with the passed in EntertainmentcategoryEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="entertainmentcategoryInstance">EntertainmentcategoryEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingEntertainmentcategoryCollectionViaAdvertisement(IEntity entertainmentcategoryInstance)
		{
			return GetMultiManyToManyUsingEntertainmentcategoryCollectionViaAdvertisement(entertainmentcategoryInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this GenericproductCollection object all GenericproductEntity objects which are related via a  relation of type 'm:n' with the passed in EntertainmentcategoryEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="entertainmentcategoryInstance">EntertainmentcategoryEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingEntertainmentcategoryCollectionViaAdvertisement(IEntity entertainmentcategoryInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingEntertainmentcategoryCollectionViaAdvertisement(entertainmentcategoryInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this GenericproductCollection object all GenericproductEntity objects which are related via a Relation of type 'm:n' with the passed in EntertainmentcategoryEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="entertainmentcategoryInstance">EntertainmentcategoryEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingEntertainmentcategoryCollectionViaAdvertisement(IEntity entertainmentcategoryInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingEntertainmentcategoryCollectionViaAdvertisement(entertainmentcategoryInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this GenericproductCollection object all GenericproductEntity objects which are related via a  relation of type 'm:n' with the passed in EntertainmentcategoryEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="entertainmentcategoryInstance">EntertainmentcategoryEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingEntertainmentcategoryCollectionViaAdvertisement(IEntity entertainmentcategoryInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateGenericproductDAO().GetMultiUsingEntertainmentcategoryCollectionViaAdvertisement(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, entertainmentcategoryInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this GenericproductCollection object all GenericproductEntity objects which are related via a  relation of type 'm:n' with the passed in EntertainmentcategoryEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="entertainmentcategoryInstance">EntertainmentcategoryEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingEntertainmentcategoryCollectionViaAdvertisement(IEntity entertainmentcategoryInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateGenericproductDAO().GetMultiUsingEntertainmentcategoryCollectionViaAdvertisement(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, entertainmentcategoryInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this GenericproductCollection object all GenericproductEntity objects which are related via a  Relation of type 'm:n' with the passed in GenericalterationEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="genericalterationInstance">GenericalterationEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingGenericalterationCollectionViaGenericproductGenericalteration(IEntity genericalterationInstance)
		{
			return GetMultiManyToManyUsingGenericalterationCollectionViaGenericproductGenericalteration(genericalterationInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this GenericproductCollection object all GenericproductEntity objects which are related via a  relation of type 'm:n' with the passed in GenericalterationEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="genericalterationInstance">GenericalterationEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingGenericalterationCollectionViaGenericproductGenericalteration(IEntity genericalterationInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingGenericalterationCollectionViaGenericproductGenericalteration(genericalterationInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this GenericproductCollection object all GenericproductEntity objects which are related via a Relation of type 'm:n' with the passed in GenericalterationEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="genericalterationInstance">GenericalterationEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingGenericalterationCollectionViaGenericproductGenericalteration(IEntity genericalterationInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingGenericalterationCollectionViaGenericproductGenericalteration(genericalterationInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this GenericproductCollection object all GenericproductEntity objects which are related via a  relation of type 'm:n' with the passed in GenericalterationEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="genericalterationInstance">GenericalterationEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingGenericalterationCollectionViaGenericproductGenericalteration(IEntity genericalterationInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateGenericproductDAO().GetMultiUsingGenericalterationCollectionViaGenericproductGenericalteration(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, genericalterationInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this GenericproductCollection object all GenericproductEntity objects which are related via a  relation of type 'm:n' with the passed in GenericalterationEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="genericalterationInstance">GenericalterationEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingGenericalterationCollectionViaGenericproductGenericalteration(IEntity genericalterationInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateGenericproductDAO().GetMultiUsingGenericalterationCollectionViaGenericproductGenericalteration(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, genericalterationInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this GenericproductCollection object all GenericproductEntity objects which are related via a  Relation of type 'm:n' with the passed in PointOfInterestEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="pointOfInterestInstance">PointOfInterestEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingPointOfInterestCollectionViaMedium(IEntity pointOfInterestInstance)
		{
			return GetMultiManyToManyUsingPointOfInterestCollectionViaMedium(pointOfInterestInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this GenericproductCollection object all GenericproductEntity objects which are related via a  relation of type 'm:n' with the passed in PointOfInterestEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="pointOfInterestInstance">PointOfInterestEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingPointOfInterestCollectionViaMedium(IEntity pointOfInterestInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingPointOfInterestCollectionViaMedium(pointOfInterestInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this GenericproductCollection object all GenericproductEntity objects which are related via a Relation of type 'm:n' with the passed in PointOfInterestEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="pointOfInterestInstance">PointOfInterestEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingPointOfInterestCollectionViaMedium(IEntity pointOfInterestInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingPointOfInterestCollectionViaMedium(pointOfInterestInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this GenericproductCollection object all GenericproductEntity objects which are related via a  relation of type 'm:n' with the passed in PointOfInterestEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="pointOfInterestInstance">PointOfInterestEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingPointOfInterestCollectionViaMedium(IEntity pointOfInterestInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateGenericproductDAO().GetMultiUsingPointOfInterestCollectionViaMedium(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, pointOfInterestInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this GenericproductCollection object all GenericproductEntity objects which are related via a  relation of type 'm:n' with the passed in PointOfInterestEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="pointOfInterestInstance">PointOfInterestEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingPointOfInterestCollectionViaMedium(IEntity pointOfInterestInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateGenericproductDAO().GetMultiUsingPointOfInterestCollectionViaMedium(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, pointOfInterestInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this GenericproductCollection object all GenericproductEntity objects which are related via a  Relation of type 'm:n' with the passed in ProductEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="productInstance">ProductEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingProductCollectionViaMedium(IEntity productInstance)
		{
			return GetMultiManyToManyUsingProductCollectionViaMedium(productInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this GenericproductCollection object all GenericproductEntity objects which are related via a  relation of type 'm:n' with the passed in ProductEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="productInstance">ProductEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingProductCollectionViaMedium(IEntity productInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingProductCollectionViaMedium(productInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this GenericproductCollection object all GenericproductEntity objects which are related via a Relation of type 'm:n' with the passed in ProductEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="productInstance">ProductEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingProductCollectionViaMedium(IEntity productInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingProductCollectionViaMedium(productInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this GenericproductCollection object all GenericproductEntity objects which are related via a  relation of type 'm:n' with the passed in ProductEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="productInstance">ProductEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingProductCollectionViaMedium(IEntity productInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateGenericproductDAO().GetMultiUsingProductCollectionViaMedium(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, productInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this GenericproductCollection object all GenericproductEntity objects which are related via a  relation of type 'm:n' with the passed in ProductEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="productInstance">ProductEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingProductCollectionViaMedium(IEntity productInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateGenericproductDAO().GetMultiUsingProductCollectionViaMedium(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, productInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this GenericproductCollection object all GenericproductEntity objects which are related via a  Relation of type 'm:n' with the passed in RouteEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="routeInstance">RouteEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingRouteCollectionViaProduct(IEntity routeInstance)
		{
			return GetMultiManyToManyUsingRouteCollectionViaProduct(routeInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this GenericproductCollection object all GenericproductEntity objects which are related via a  relation of type 'm:n' with the passed in RouteEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="routeInstance">RouteEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingRouteCollectionViaProduct(IEntity routeInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingRouteCollectionViaProduct(routeInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this GenericproductCollection object all GenericproductEntity objects which are related via a Relation of type 'm:n' with the passed in RouteEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="routeInstance">RouteEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingRouteCollectionViaProduct(IEntity routeInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingRouteCollectionViaProduct(routeInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this GenericproductCollection object all GenericproductEntity objects which are related via a  relation of type 'm:n' with the passed in RouteEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="routeInstance">RouteEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingRouteCollectionViaProduct(IEntity routeInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateGenericproductDAO().GetMultiUsingRouteCollectionViaProduct(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, routeInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this GenericproductCollection object all GenericproductEntity objects which are related via a  relation of type 'm:n' with the passed in RouteEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="routeInstance">RouteEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingRouteCollectionViaProduct(IEntity routeInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateGenericproductDAO().GetMultiUsingRouteCollectionViaProduct(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, routeInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this GenericproductCollection object all GenericproductEntity objects which are related via a  Relation of type 'm:n' with the passed in SurveyPageEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="surveyPageInstance">SurveyPageEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingSurveyPageCollectionViaMedium(IEntity surveyPageInstance)
		{
			return GetMultiManyToManyUsingSurveyPageCollectionViaMedium(surveyPageInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this GenericproductCollection object all GenericproductEntity objects which are related via a  relation of type 'm:n' with the passed in SurveyPageEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="surveyPageInstance">SurveyPageEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingSurveyPageCollectionViaMedium(IEntity surveyPageInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingSurveyPageCollectionViaMedium(surveyPageInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this GenericproductCollection object all GenericproductEntity objects which are related via a Relation of type 'm:n' with the passed in SurveyPageEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="surveyPageInstance">SurveyPageEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingSurveyPageCollectionViaMedium(IEntity surveyPageInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingSurveyPageCollectionViaMedium(surveyPageInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this GenericproductCollection object all GenericproductEntity objects which are related via a  relation of type 'm:n' with the passed in SurveyPageEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="surveyPageInstance">SurveyPageEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingSurveyPageCollectionViaMedium(IEntity surveyPageInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateGenericproductDAO().GetMultiUsingSurveyPageCollectionViaMedium(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, surveyPageInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this GenericproductCollection object all GenericproductEntity objects which are related via a  relation of type 'm:n' with the passed in SurveyPageEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="surveyPageInstance">SurveyPageEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingSurveyPageCollectionViaMedium(IEntity surveyPageInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateGenericproductDAO().GetMultiUsingSurveyPageCollectionViaMedium(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, surveyPageInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this GenericproductCollection object all GenericproductEntity objects which are related via a  Relation of type 'm:n' with the passed in VattariffEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="vattariffInstance">VattariffEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingVattariffCollectionViaProduct(IEntity vattariffInstance)
		{
			return GetMultiManyToManyUsingVattariffCollectionViaProduct(vattariffInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this GenericproductCollection object all GenericproductEntity objects which are related via a  relation of type 'm:n' with the passed in VattariffEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="vattariffInstance">VattariffEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingVattariffCollectionViaProduct(IEntity vattariffInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingVattariffCollectionViaProduct(vattariffInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this GenericproductCollection object all GenericproductEntity objects which are related via a Relation of type 'm:n' with the passed in VattariffEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="vattariffInstance">VattariffEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingVattariffCollectionViaProduct(IEntity vattariffInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingVattariffCollectionViaProduct(vattariffInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this GenericproductCollection object all GenericproductEntity objects which are related via a  relation of type 'm:n' with the passed in VattariffEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="vattariffInstance">VattariffEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingVattariffCollectionViaProduct(IEntity vattariffInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateGenericproductDAO().GetMultiUsingVattariffCollectionViaProduct(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, vattariffInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this GenericproductCollection object all GenericproductEntity objects which are related via a  relation of type 'm:n' with the passed in VattariffEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="vattariffInstance">VattariffEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingVattariffCollectionViaProduct(IEntity vattariffInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateGenericproductDAO().GetMultiUsingVattariffCollectionViaProduct(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, vattariffInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves Entity rows in a datatable which match the specified filter. It will always create a new connection to the database.</summary>
		/// <param name="selectFilter">A predicate or predicate expression which should be used as filter for the entities to retrieve.</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>DataTable with the rows requested.</returns>
		public static DataTable GetMultiAsDataTable(IPredicate selectFilter, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiAsDataTable(selectFilter, maxNumberOfItemsToReturn, sortClauses, null, 0, 0);
		}

		/// <summary> Retrieves Entity rows in a datatable which match the specified filter. It will always create a new connection to the database.</summary>
		/// <param name="selectFilter">A predicate or predicate expression which should be used as filter for the entities to retrieve.</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="relations">The set of relations to walk to construct to total query.</param>
		/// <returns>DataTable with the rows requested.</returns>
		public static DataTable GetMultiAsDataTable(IPredicate selectFilter, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IRelationCollection relations)
		{
			return GetMultiAsDataTable(selectFilter, maxNumberOfItemsToReturn, sortClauses, relations, 0, 0);
		}
		
		/// <summary> Retrieves Entity rows in a datatable which match the specified filter. It will always create a new connection to the database.</summary>
		/// <param name="selectFilter">A predicate or predicate expression which should be used as filter for the entities to retrieve.</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="relations">The set of relations to walk to construct to total query.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>DataTable with the rows requested.</returns>
		public static DataTable GetMultiAsDataTable(IPredicate selectFilter, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IRelationCollection relations, int pageNumber, int pageSize)
		{
			GenericproductDAO dao = DAOFactory.CreateGenericproductDAO();
			return dao.GetMultiAsDataTable(maxNumberOfItemsToReturn, sortClauses, selectFilter, relations, pageNumber, pageSize);
		}


		
		/// <summary> Gets a scalar value, calculated with the aggregate. the field index specified is the field the aggregate are applied on.</summary>
		/// <param name="fieldIndex">Field index of field to which to apply the aggregate function and expression</param>
		/// <param name="aggregateToApply">Aggregate function to apply. </param>
		/// <returns>the scalar value requested</returns>
		public object GetScalar(GenericproductFieldIndex fieldIndex, AggregateFunction aggregateToApply)
		{
			return GetScalar(fieldIndex, null, aggregateToApply, null, null, null);
		}

		/// <summary> Gets a scalar value, calculated with the aggregate and expression specified. the field index specified is the field the expression and aggregate are applied on.</summary>
		/// <param name="fieldIndex">Field index of field to which to apply the aggregate function and expression</param>
		/// <param name="expressionToExecute">The expression to execute. Can be null</param>
		/// <param name="aggregateToApply">Aggregate function to apply. </param>
		/// <returns>the scalar value requested</returns>
		public object GetScalar(GenericproductFieldIndex fieldIndex, IExpression expressionToExecute, AggregateFunction aggregateToApply)
		{
			return GetScalar(fieldIndex, expressionToExecute, aggregateToApply, null, null, null);
		}

		/// <summary> Gets a scalar value, calculated with the aggregate and expression specified. the field index specified is the field the expression and aggregate are applied on.</summary>
		/// <param name="fieldIndex">Field index of field to which to apply the aggregate function and expression</param>
		/// <param name="expressionToExecute">The expression to execute. Can be null</param>
		/// <param name="aggregateToApply">Aggregate function to apply. </param>
		/// <param name="filter">The filter to apply to retrieve the scalar</param>
		/// <returns>the scalar value requested</returns>
		public object GetScalar(GenericproductFieldIndex fieldIndex, IExpression expressionToExecute, AggregateFunction aggregateToApply, IPredicate filter)
		{
			return GetScalar(fieldIndex, expressionToExecute, aggregateToApply, filter, null, null);
		}

		/// <summary> Gets a scalar value, calculated with the aggregate and expression specified. the field index specified is the field the expression and aggregate are applied on.</summary>
		/// <param name="fieldIndex">Field index of field to which to apply the aggregate function and expression</param>
		/// <param name="expressionToExecute">The expression to execute. Can be null</param>
		/// <param name="aggregateToApply">Aggregate function to apply. </param>
		/// <param name="filter">The filter to apply to retrieve the scalar</param>
		/// <param name="groupByClause">The groupby clause to apply to retrieve the scalar</param>
		/// <returns>the scalar value requested</returns>
		public object GetScalar(GenericproductFieldIndex fieldIndex, IExpression expressionToExecute, AggregateFunction aggregateToApply, IPredicate filter, IGroupByCollection groupByClause)
		{
			return GetScalar(fieldIndex, expressionToExecute, aggregateToApply, filter, null, groupByClause);
		}

		/// <summary> Gets a scalar value, calculated with the aggregate and expression specified. the field index specified is the field the expression and aggregate are applied on.</summary>
		/// <param name="fieldIndex">Field index of field to which to apply the aggregate function and expression</param>
		/// <param name="expressionToExecute">The expression to execute. Can be null</param>
		/// <param name="aggregateToApply">Aggregate function to apply. </param>
		/// <param name="filter">The filter to apply to retrieve the scalar</param>
		/// <param name="relations">The relations to walk</param>
		/// <param name="groupByClause">The groupby clause to apply to retrieve the scalar</param>
		/// <returns>the scalar value requested</returns>
		public virtual object GetScalar(GenericproductFieldIndex fieldIndex, IExpression expressionToExecute, AggregateFunction aggregateToApply, IPredicate filter, IRelationCollection relations, IGroupByCollection groupByClause)
		{
			EntityFields fields = new EntityFields(1);
			fields[0] = EntityFieldFactory.Create(fieldIndex);
			if((fields[0].ExpressionToApply == null) || (expressionToExecute != null))
			{
				fields[0].ExpressionToApply = expressionToExecute;
			}
			if((fields[0].AggregateFunctionToApply == AggregateFunction.None) || (aggregateToApply != AggregateFunction.None))
			{
				fields[0].AggregateFunctionToApply = aggregateToApply;
			}
			return DAOFactory.CreateGenericproductDAO().GetScalar(fields, this.Transaction, filter, relations, groupByClause);
		}
		
		/// <summary>Creats a new DAO instance so code which is in the base class can still use the proper DAO object.</summary>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateGenericproductDAO();
		}
		
		/// <summary>Creates a new transaction object</summary>
		/// <param name="levelOfIsolation">The level of isolation.</param>
		/// <param name="name">The name.</param>
		protected override ITransaction CreateTransaction( IsolationLevel levelOfIsolation, string name )
		{
			return new Transaction(levelOfIsolation, name);
		}

		#region Custom EntityCollection code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCollectionCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion
		
		#region Included Code

		#endregion
	}
}
