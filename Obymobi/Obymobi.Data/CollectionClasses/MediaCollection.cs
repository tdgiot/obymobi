﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Data;
using System.Collections.Generic;
using System.ComponentModel;
using System.Xml;
#if !CF
using System.Runtime.Serialization;
#endif

using Obymobi.Data.EntityClasses;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.CollectionClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Collection class for storing and retrieving collections of MediaEntity objects. </summary>
	[Serializable]
	public partial class MediaCollection : CommonEntityCollection<MediaEntity>
	{
		/// <summary> CTor</summary>
		public MediaCollection():base(new MediaEntityFactory())
		{
		}

		/// <summary> CTor</summary>
		/// <param name="initialContents">The initial contents of this collection.</param>
		public MediaCollection(IEnumerable<MediaEntity> initialContents):base(new MediaEntityFactory())
		{
			AddRange(initialContents);
		}

		/// <summary> CTor</summary>
		/// <param name="entityFactoryToUse">The EntityFactory to use when creating entity objects during a GetMulti() call.</param>
		public MediaCollection(IEntityFactory entityFactoryToUse):base(entityFactoryToUse)
		{
		}

		/// <summary> Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected MediaCollection(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}


		/// <summary> Retrieves in this MediaCollection object all MediaEntity objects which have data in common with the specified related Entities.
		/// If one is omitted, that entity is not used as a filter. All current elements in the collection are removed from the collection.</summary>
		/// <param name="advertisementEntityInstance">AdvertisementEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="alterationEntityInstance">AlterationEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="alterationoptionEntityInstance">AlterationoptionEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="applicationConfigurationEntityInstance">ApplicationConfigurationEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="carouselItemEntityInstance">CarouselItemEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="landingPageEntityInstance">LandingPageEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="widgetActionBannerEntityInstance">WidgetActionBannerEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="widgetHeroEntityInstance">WidgetHeroEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="attachmentEntityInstance">AttachmentEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="actionCategoryEntityInstance">CategoryEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="categoryEntityInstance">CategoryEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="clientConfigurationEntityInstance">ClientConfigurationEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="companyEntityInstance">CompanyEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="deliverypointgroupEntityInstance">DeliverypointgroupEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="actionEntertainmentEntityInstance">EntertainmentEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="entertainmentEntityInstance">EntertainmentEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="actionEntertainmentcategoryEntityInstance">EntertainmentcategoryEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="genericcategoryEntityInstance">GenericcategoryEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="genericproductEntityInstance">GenericproductEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="mediaEntityInstance">MediaEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="pageEntityInstance">PageEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="pageEntity_Instance">PageEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="pageElementEntityInstance">PageElementEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="pageTemplateEntityInstance">PageTemplateEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="pageTemplateElementEntityInstance">PageTemplateElementEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="pointOfInterestEntityInstance">PointOfInterestEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="actionProductEntityInstance">ProductEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="productEntityInstance">ProductEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="productgroupEntityInstance">ProductgroupEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="roomControlSectionEntityInstance">RoomControlSectionEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="roomControlSectionItemEntityInstance">RoomControlSectionItemEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="routestephandlerEntityInstance">RoutestephandlerEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="siteEntityInstance">SiteEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="siteEntity_Instance">SiteEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="stationEntityInstance">StationEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="surveyEntityInstance">SurveyEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="surveyPageEntityInstance">SurveyPageEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="uIFooterItemEntityInstance">UIFooterItemEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="uIThemeEntityInstance">UIThemeEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="uIWidgetEntityInstance">UIWidgetEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <returns>true if succeeded, false otherwise</returns>
		public bool GetMultiManyToOne(IEntity advertisementEntityInstance, IEntity alterationEntityInstance, IEntity alterationoptionEntityInstance, IEntity applicationConfigurationEntityInstance, IEntity carouselItemEntityInstance, IEntity landingPageEntityInstance, IEntity widgetActionBannerEntityInstance, IEntity widgetHeroEntityInstance, IEntity attachmentEntityInstance, IEntity actionCategoryEntityInstance, IEntity categoryEntityInstance, IEntity clientConfigurationEntityInstance, IEntity companyEntityInstance, IEntity deliverypointgroupEntityInstance, IEntity actionEntertainmentEntityInstance, IEntity entertainmentEntityInstance, IEntity actionEntertainmentcategoryEntityInstance, IEntity genericcategoryEntityInstance, IEntity genericproductEntityInstance, IEntity mediaEntityInstance, IEntity pageEntityInstance, IEntity pageEntity_Instance, IEntity pageElementEntityInstance, IEntity pageTemplateEntityInstance, IEntity pageTemplateElementEntityInstance, IEntity pointOfInterestEntityInstance, IEntity actionProductEntityInstance, IEntity productEntityInstance, IEntity productgroupEntityInstance, IEntity roomControlSectionEntityInstance, IEntity roomControlSectionItemEntityInstance, IEntity routestephandlerEntityInstance, IEntity siteEntityInstance, IEntity siteEntity_Instance, IEntity stationEntityInstance, IEntity surveyEntityInstance, IEntity surveyPageEntityInstance, IEntity uIFooterItemEntityInstance, IEntity uIThemeEntityInstance, IEntity uIWidgetEntityInstance)
		{
			return GetMultiManyToOne(advertisementEntityInstance, alterationEntityInstance, alterationoptionEntityInstance, applicationConfigurationEntityInstance, carouselItemEntityInstance, landingPageEntityInstance, widgetActionBannerEntityInstance, widgetHeroEntityInstance, attachmentEntityInstance, actionCategoryEntityInstance, categoryEntityInstance, clientConfigurationEntityInstance, companyEntityInstance, deliverypointgroupEntityInstance, actionEntertainmentEntityInstance, entertainmentEntityInstance, actionEntertainmentcategoryEntityInstance, genericcategoryEntityInstance, genericproductEntityInstance, mediaEntityInstance, pageEntityInstance, pageEntity_Instance, pageElementEntityInstance, pageTemplateEntityInstance, pageTemplateElementEntityInstance, pointOfInterestEntityInstance, actionProductEntityInstance, productEntityInstance, productgroupEntityInstance, roomControlSectionEntityInstance, roomControlSectionItemEntityInstance, routestephandlerEntityInstance, siteEntityInstance, siteEntity_Instance, stationEntityInstance, surveyEntityInstance, surveyPageEntityInstance, uIFooterItemEntityInstance, uIThemeEntityInstance, uIWidgetEntityInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, null, 0, 0);
		}

		/// <summary> Retrieves in this MediaCollection object all MediaEntity objects which have data in common with the specified related Entities.
		/// If one is omitted, that entity is not used as a filter. All current elements in the collection are removed from the collection.</summary>
		/// <param name="advertisementEntityInstance">AdvertisementEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="alterationEntityInstance">AlterationEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="alterationoptionEntityInstance">AlterationoptionEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="applicationConfigurationEntityInstance">ApplicationConfigurationEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="carouselItemEntityInstance">CarouselItemEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="landingPageEntityInstance">LandingPageEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="widgetActionBannerEntityInstance">WidgetActionBannerEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="widgetHeroEntityInstance">WidgetHeroEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="attachmentEntityInstance">AttachmentEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="actionCategoryEntityInstance">CategoryEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="categoryEntityInstance">CategoryEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="clientConfigurationEntityInstance">ClientConfigurationEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="companyEntityInstance">CompanyEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="deliverypointgroupEntityInstance">DeliverypointgroupEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="actionEntertainmentEntityInstance">EntertainmentEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="entertainmentEntityInstance">EntertainmentEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="actionEntertainmentcategoryEntityInstance">EntertainmentcategoryEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="genericcategoryEntityInstance">GenericcategoryEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="genericproductEntityInstance">GenericproductEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="mediaEntityInstance">MediaEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="pageEntityInstance">PageEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="pageEntity_Instance">PageEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="pageElementEntityInstance">PageElementEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="pageTemplateEntityInstance">PageTemplateEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="pageTemplateElementEntityInstance">PageTemplateElementEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="pointOfInterestEntityInstance">PointOfInterestEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="actionProductEntityInstance">ProductEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="productEntityInstance">ProductEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="productgroupEntityInstance">ProductgroupEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="roomControlSectionEntityInstance">RoomControlSectionEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="roomControlSectionItemEntityInstance">RoomControlSectionItemEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="routestephandlerEntityInstance">RoutestephandlerEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="siteEntityInstance">SiteEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="siteEntity_Instance">SiteEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="stationEntityInstance">StationEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="surveyEntityInstance">SurveyEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="surveyPageEntityInstance">SurveyPageEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="uIFooterItemEntityInstance">UIFooterItemEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="uIThemeEntityInstance">UIThemeEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="uIWidgetEntityInstance">UIWidgetEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="filter">Extra filter to limit the resultset. Predicate expression can be null, in which case it will be ignored.</param>
		/// <returns>true if succeeded, false otherwise</returns>
		public bool GetMultiManyToOne(IEntity advertisementEntityInstance, IEntity alterationEntityInstance, IEntity alterationoptionEntityInstance, IEntity applicationConfigurationEntityInstance, IEntity carouselItemEntityInstance, IEntity landingPageEntityInstance, IEntity widgetActionBannerEntityInstance, IEntity widgetHeroEntityInstance, IEntity attachmentEntityInstance, IEntity actionCategoryEntityInstance, IEntity categoryEntityInstance, IEntity clientConfigurationEntityInstance, IEntity companyEntityInstance, IEntity deliverypointgroupEntityInstance, IEntity actionEntertainmentEntityInstance, IEntity entertainmentEntityInstance, IEntity actionEntertainmentcategoryEntityInstance, IEntity genericcategoryEntityInstance, IEntity genericproductEntityInstance, IEntity mediaEntityInstance, IEntity pageEntityInstance, IEntity pageEntity_Instance, IEntity pageElementEntityInstance, IEntity pageTemplateEntityInstance, IEntity pageTemplateElementEntityInstance, IEntity pointOfInterestEntityInstance, IEntity actionProductEntityInstance, IEntity productEntityInstance, IEntity productgroupEntityInstance, IEntity roomControlSectionEntityInstance, IEntity roomControlSectionItemEntityInstance, IEntity routestephandlerEntityInstance, IEntity siteEntityInstance, IEntity siteEntity_Instance, IEntity stationEntityInstance, IEntity surveyEntityInstance, IEntity surveyPageEntityInstance, IEntity uIFooterItemEntityInstance, IEntity uIThemeEntityInstance, IEntity uIWidgetEntityInstance, IPredicateExpression filter)
		{
			return GetMultiManyToOne(advertisementEntityInstance, alterationEntityInstance, alterationoptionEntityInstance, applicationConfigurationEntityInstance, carouselItemEntityInstance, landingPageEntityInstance, widgetActionBannerEntityInstance, widgetHeroEntityInstance, attachmentEntityInstance, actionCategoryEntityInstance, categoryEntityInstance, clientConfigurationEntityInstance, companyEntityInstance, deliverypointgroupEntityInstance, actionEntertainmentEntityInstance, entertainmentEntityInstance, actionEntertainmentcategoryEntityInstance, genericcategoryEntityInstance, genericproductEntityInstance, mediaEntityInstance, pageEntityInstance, pageEntity_Instance, pageElementEntityInstance, pageTemplateEntityInstance, pageTemplateElementEntityInstance, pointOfInterestEntityInstance, actionProductEntityInstance, productEntityInstance, productgroupEntityInstance, roomControlSectionEntityInstance, roomControlSectionItemEntityInstance, routestephandlerEntityInstance, siteEntityInstance, siteEntity_Instance, stationEntityInstance, surveyEntityInstance, surveyPageEntityInstance, uIFooterItemEntityInstance, uIThemeEntityInstance, uIWidgetEntityInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, filter, 0, 0);
		}

		/// <summary> Retrieves in this MediaCollection object all MediaEntity objects which have data in common with the specified related Entities.
		/// If one is omitted, that entity is not used as a filter. All current elements in the collection are removed from the collection.</summary>
		/// <param name="advertisementEntityInstance">AdvertisementEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="alterationEntityInstance">AlterationEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="alterationoptionEntityInstance">AlterationoptionEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="applicationConfigurationEntityInstance">ApplicationConfigurationEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="carouselItemEntityInstance">CarouselItemEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="landingPageEntityInstance">LandingPageEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="widgetActionBannerEntityInstance">WidgetActionBannerEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="widgetHeroEntityInstance">WidgetHeroEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="attachmentEntityInstance">AttachmentEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="actionCategoryEntityInstance">CategoryEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="categoryEntityInstance">CategoryEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="clientConfigurationEntityInstance">ClientConfigurationEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="companyEntityInstance">CompanyEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="deliverypointgroupEntityInstance">DeliverypointgroupEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="actionEntertainmentEntityInstance">EntertainmentEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="entertainmentEntityInstance">EntertainmentEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="actionEntertainmentcategoryEntityInstance">EntertainmentcategoryEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="genericcategoryEntityInstance">GenericcategoryEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="genericproductEntityInstance">GenericproductEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="mediaEntityInstance">MediaEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="pageEntityInstance">PageEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="pageEntity_Instance">PageEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="pageElementEntityInstance">PageElementEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="pageTemplateEntityInstance">PageTemplateEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="pageTemplateElementEntityInstance">PageTemplateElementEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="pointOfInterestEntityInstance">PointOfInterestEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="actionProductEntityInstance">ProductEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="productEntityInstance">ProductEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="productgroupEntityInstance">ProductgroupEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="roomControlSectionEntityInstance">RoomControlSectionEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="roomControlSectionItemEntityInstance">RoomControlSectionItemEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="routestephandlerEntityInstance">RoutestephandlerEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="siteEntityInstance">SiteEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="siteEntity_Instance">SiteEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="stationEntityInstance">StationEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="surveyEntityInstance">SurveyEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="surveyPageEntityInstance">SurveyPageEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="uIFooterItemEntityInstance">UIFooterItemEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="uIThemeEntityInstance">UIThemeEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="uIWidgetEntityInstance">UIWidgetEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="filter">Extra filter to limit the resultset. Predicate expression can be null, in which case it will be ignored.</param>
		/// <returns>true if succeeded, false otherwise</returns>
		public bool GetMultiManyToOne(IEntity advertisementEntityInstance, IEntity alterationEntityInstance, IEntity alterationoptionEntityInstance, IEntity applicationConfigurationEntityInstance, IEntity carouselItemEntityInstance, IEntity landingPageEntityInstance, IEntity widgetActionBannerEntityInstance, IEntity widgetHeroEntityInstance, IEntity attachmentEntityInstance, IEntity actionCategoryEntityInstance, IEntity categoryEntityInstance, IEntity clientConfigurationEntityInstance, IEntity companyEntityInstance, IEntity deliverypointgroupEntityInstance, IEntity actionEntertainmentEntityInstance, IEntity entertainmentEntityInstance, IEntity actionEntertainmentcategoryEntityInstance, IEntity genericcategoryEntityInstance, IEntity genericproductEntityInstance, IEntity mediaEntityInstance, IEntity pageEntityInstance, IEntity pageEntity_Instance, IEntity pageElementEntityInstance, IEntity pageTemplateEntityInstance, IEntity pageTemplateElementEntityInstance, IEntity pointOfInterestEntityInstance, IEntity actionProductEntityInstance, IEntity productEntityInstance, IEntity productgroupEntityInstance, IEntity roomControlSectionEntityInstance, IEntity roomControlSectionItemEntityInstance, IEntity routestephandlerEntityInstance, IEntity siteEntityInstance, IEntity siteEntity_Instance, IEntity stationEntityInstance, IEntity surveyEntityInstance, IEntity surveyPageEntityInstance, IEntity uIFooterItemEntityInstance, IEntity uIThemeEntityInstance, IEntity uIWidgetEntityInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPredicateExpression filter)
		{
			return GetMultiManyToOne(advertisementEntityInstance, alterationEntityInstance, alterationoptionEntityInstance, applicationConfigurationEntityInstance, carouselItemEntityInstance, landingPageEntityInstance, widgetActionBannerEntityInstance, widgetHeroEntityInstance, attachmentEntityInstance, actionCategoryEntityInstance, categoryEntityInstance, clientConfigurationEntityInstance, companyEntityInstance, deliverypointgroupEntityInstance, actionEntertainmentEntityInstance, entertainmentEntityInstance, actionEntertainmentcategoryEntityInstance, genericcategoryEntityInstance, genericproductEntityInstance, mediaEntityInstance, pageEntityInstance, pageEntity_Instance, pageElementEntityInstance, pageTemplateEntityInstance, pageTemplateElementEntityInstance, pointOfInterestEntityInstance, actionProductEntityInstance, productEntityInstance, productgroupEntityInstance, roomControlSectionEntityInstance, roomControlSectionItemEntityInstance, routestephandlerEntityInstance, siteEntityInstance, siteEntity_Instance, stationEntityInstance, surveyEntityInstance, surveyPageEntityInstance, uIFooterItemEntityInstance, uIThemeEntityInstance, uIWidgetEntityInstance, maxNumberOfItemsToReturn, sortClauses, filter, 0, 0);
		}

		/// <summary> Retrieves in this MediaCollection object all MediaEntity objects which have data in common with the specified related Entities.
		/// If one is omitted, that entity is not used as a filter. All current elements in the collection are removed from the collection.</summary>
		/// <param name="advertisementEntityInstance">AdvertisementEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="alterationEntityInstance">AlterationEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="alterationoptionEntityInstance">AlterationoptionEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="applicationConfigurationEntityInstance">ApplicationConfigurationEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="carouselItemEntityInstance">CarouselItemEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="landingPageEntityInstance">LandingPageEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="widgetActionBannerEntityInstance">WidgetActionBannerEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="widgetHeroEntityInstance">WidgetHeroEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="attachmentEntityInstance">AttachmentEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="actionCategoryEntityInstance">CategoryEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="categoryEntityInstance">CategoryEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="clientConfigurationEntityInstance">ClientConfigurationEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="companyEntityInstance">CompanyEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="deliverypointgroupEntityInstance">DeliverypointgroupEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="actionEntertainmentEntityInstance">EntertainmentEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="entertainmentEntityInstance">EntertainmentEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="actionEntertainmentcategoryEntityInstance">EntertainmentcategoryEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="genericcategoryEntityInstance">GenericcategoryEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="genericproductEntityInstance">GenericproductEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="mediaEntityInstance">MediaEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="pageEntityInstance">PageEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="pageEntity_Instance">PageEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="pageElementEntityInstance">PageElementEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="pageTemplateEntityInstance">PageTemplateEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="pageTemplateElementEntityInstance">PageTemplateElementEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="pointOfInterestEntityInstance">PointOfInterestEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="actionProductEntityInstance">ProductEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="productEntityInstance">ProductEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="productgroupEntityInstance">ProductgroupEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="roomControlSectionEntityInstance">RoomControlSectionEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="roomControlSectionItemEntityInstance">RoomControlSectionItemEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="routestephandlerEntityInstance">RoutestephandlerEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="siteEntityInstance">SiteEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="siteEntity_Instance">SiteEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="stationEntityInstance">StationEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="surveyEntityInstance">SurveyEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="surveyPageEntityInstance">SurveyPageEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="uIFooterItemEntityInstance">UIFooterItemEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="uIThemeEntityInstance">UIThemeEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="uIWidgetEntityInstance">UIWidgetEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="filter">Extra filter to limit the resultset. Predicate expression can be null, in which case it will be ignored.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToOne(IEntity advertisementEntityInstance, IEntity alterationEntityInstance, IEntity alterationoptionEntityInstance, IEntity applicationConfigurationEntityInstance, IEntity carouselItemEntityInstance, IEntity landingPageEntityInstance, IEntity widgetActionBannerEntityInstance, IEntity widgetHeroEntityInstance, IEntity attachmentEntityInstance, IEntity actionCategoryEntityInstance, IEntity categoryEntityInstance, IEntity clientConfigurationEntityInstance, IEntity companyEntityInstance, IEntity deliverypointgroupEntityInstance, IEntity actionEntertainmentEntityInstance, IEntity entertainmentEntityInstance, IEntity actionEntertainmentcategoryEntityInstance, IEntity genericcategoryEntityInstance, IEntity genericproductEntityInstance, IEntity mediaEntityInstance, IEntity pageEntityInstance, IEntity pageEntity_Instance, IEntity pageElementEntityInstance, IEntity pageTemplateEntityInstance, IEntity pageTemplateElementEntityInstance, IEntity pointOfInterestEntityInstance, IEntity actionProductEntityInstance, IEntity productEntityInstance, IEntity productgroupEntityInstance, IEntity roomControlSectionEntityInstance, IEntity roomControlSectionItemEntityInstance, IEntity routestephandlerEntityInstance, IEntity siteEntityInstance, IEntity siteEntity_Instance, IEntity stationEntityInstance, IEntity surveyEntityInstance, IEntity surveyPageEntityInstance, IEntity uIFooterItemEntityInstance, IEntity uIThemeEntityInstance, IEntity uIWidgetEntityInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPredicateExpression filter, int pageNumber, int pageSize)
		{
			bool validParameters = false;
			validParameters |= (advertisementEntityInstance!=null);
			validParameters |= (alterationEntityInstance!=null);
			validParameters |= (alterationoptionEntityInstance!=null);
			validParameters |= (applicationConfigurationEntityInstance!=null);
			validParameters |= (carouselItemEntityInstance!=null);
			validParameters |= (landingPageEntityInstance!=null);
			validParameters |= (widgetActionBannerEntityInstance!=null);
			validParameters |= (widgetHeroEntityInstance!=null);
			validParameters |= (attachmentEntityInstance!=null);
			validParameters |= (actionCategoryEntityInstance!=null);
			validParameters |= (categoryEntityInstance!=null);
			validParameters |= (clientConfigurationEntityInstance!=null);
			validParameters |= (companyEntityInstance!=null);
			validParameters |= (deliverypointgroupEntityInstance!=null);
			validParameters |= (actionEntertainmentEntityInstance!=null);
			validParameters |= (entertainmentEntityInstance!=null);
			validParameters |= (actionEntertainmentcategoryEntityInstance!=null);
			validParameters |= (genericcategoryEntityInstance!=null);
			validParameters |= (genericproductEntityInstance!=null);
			validParameters |= (mediaEntityInstance!=null);
			validParameters |= (pageEntityInstance!=null);
			validParameters |= (pageEntity_Instance!=null);
			validParameters |= (pageElementEntityInstance!=null);
			validParameters |= (pageTemplateEntityInstance!=null);
			validParameters |= (pageTemplateElementEntityInstance!=null);
			validParameters |= (pointOfInterestEntityInstance!=null);
			validParameters |= (actionProductEntityInstance!=null);
			validParameters |= (productEntityInstance!=null);
			validParameters |= (productgroupEntityInstance!=null);
			validParameters |= (roomControlSectionEntityInstance!=null);
			validParameters |= (roomControlSectionItemEntityInstance!=null);
			validParameters |= (routestephandlerEntityInstance!=null);
			validParameters |= (siteEntityInstance!=null);
			validParameters |= (siteEntity_Instance!=null);
			validParameters |= (stationEntityInstance!=null);
			validParameters |= (surveyEntityInstance!=null);
			validParameters |= (surveyPageEntityInstance!=null);
			validParameters |= (uIFooterItemEntityInstance!=null);
			validParameters |= (uIThemeEntityInstance!=null);
			validParameters |= (uIWidgetEntityInstance!=null);
			if(!validParameters)
			{
				return GetMulti(filter, maxNumberOfItemsToReturn, sortClauses, null, pageNumber, pageSize);
			}
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateMediaDAO().GetMulti(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, filter, advertisementEntityInstance, alterationEntityInstance, alterationoptionEntityInstance, applicationConfigurationEntityInstance, carouselItemEntityInstance, landingPageEntityInstance, widgetActionBannerEntityInstance, widgetHeroEntityInstance, attachmentEntityInstance, actionCategoryEntityInstance, categoryEntityInstance, clientConfigurationEntityInstance, companyEntityInstance, deliverypointgroupEntityInstance, actionEntertainmentEntityInstance, entertainmentEntityInstance, actionEntertainmentcategoryEntityInstance, genericcategoryEntityInstance, genericproductEntityInstance, mediaEntityInstance, pageEntityInstance, pageEntity_Instance, pageElementEntityInstance, pageTemplateEntityInstance, pageTemplateElementEntityInstance, pointOfInterestEntityInstance, actionProductEntityInstance, productEntityInstance, productgroupEntityInstance, roomControlSectionEntityInstance, roomControlSectionItemEntityInstance, routestephandlerEntityInstance, siteEntityInstance, siteEntity_Instance, stationEntityInstance, surveyEntityInstance, surveyPageEntityInstance, uIFooterItemEntityInstance, uIThemeEntityInstance, uIWidgetEntityInstance, pageNumber, pageSize);
		}

		/// <summary> Deletes from the persistent storage all Media entities which have data in common with the specified related Entities. If one is omitted, that entity is not used as a filter.</summary>
		/// <remarks>Runs directly on the persistent storage. It will not delete entity objects from the current collection.</remarks>
		/// <param name="advertisementEntityInstance">AdvertisementEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="alterationEntityInstance">AlterationEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="alterationoptionEntityInstance">AlterationoptionEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="applicationConfigurationEntityInstance">ApplicationConfigurationEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="carouselItemEntityInstance">CarouselItemEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="landingPageEntityInstance">LandingPageEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="widgetActionBannerEntityInstance">WidgetActionBannerEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="widgetHeroEntityInstance">WidgetHeroEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="attachmentEntityInstance">AttachmentEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="actionCategoryEntityInstance">CategoryEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="categoryEntityInstance">CategoryEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="clientConfigurationEntityInstance">ClientConfigurationEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="companyEntityInstance">CompanyEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="deliverypointgroupEntityInstance">DeliverypointgroupEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="actionEntertainmentEntityInstance">EntertainmentEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="entertainmentEntityInstance">EntertainmentEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="actionEntertainmentcategoryEntityInstance">EntertainmentcategoryEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="genericcategoryEntityInstance">GenericcategoryEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="genericproductEntityInstance">GenericproductEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="mediaEntityInstance">MediaEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="pageEntityInstance">PageEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="pageEntity_Instance">PageEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="pageElementEntityInstance">PageElementEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="pageTemplateEntityInstance">PageTemplateEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="pageTemplateElementEntityInstance">PageTemplateElementEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="pointOfInterestEntityInstance">PointOfInterestEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="actionProductEntityInstance">ProductEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="productEntityInstance">ProductEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="productgroupEntityInstance">ProductgroupEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="roomControlSectionEntityInstance">RoomControlSectionEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="roomControlSectionItemEntityInstance">RoomControlSectionItemEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="routestephandlerEntityInstance">RoutestephandlerEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="siteEntityInstance">SiteEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="siteEntity_Instance">SiteEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="stationEntityInstance">StationEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="surveyEntityInstance">SurveyEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="surveyPageEntityInstance">SurveyPageEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="uIFooterItemEntityInstance">UIFooterItemEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="uIThemeEntityInstance">UIThemeEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="uIWidgetEntityInstance">UIWidgetEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <returns>Amount of entities affected, if the used persistent storage has rowcounting enabled.</returns>
		public int DeleteMultiManyToOne(IEntity advertisementEntityInstance, IEntity alterationEntityInstance, IEntity alterationoptionEntityInstance, IEntity applicationConfigurationEntityInstance, IEntity carouselItemEntityInstance, IEntity landingPageEntityInstance, IEntity widgetActionBannerEntityInstance, IEntity widgetHeroEntityInstance, IEntity attachmentEntityInstance, IEntity actionCategoryEntityInstance, IEntity categoryEntityInstance, IEntity clientConfigurationEntityInstance, IEntity companyEntityInstance, IEntity deliverypointgroupEntityInstance, IEntity actionEntertainmentEntityInstance, IEntity entertainmentEntityInstance, IEntity actionEntertainmentcategoryEntityInstance, IEntity genericcategoryEntityInstance, IEntity genericproductEntityInstance, IEntity mediaEntityInstance, IEntity pageEntityInstance, IEntity pageEntity_Instance, IEntity pageElementEntityInstance, IEntity pageTemplateEntityInstance, IEntity pageTemplateElementEntityInstance, IEntity pointOfInterestEntityInstance, IEntity actionProductEntityInstance, IEntity productEntityInstance, IEntity productgroupEntityInstance, IEntity roomControlSectionEntityInstance, IEntity roomControlSectionItemEntityInstance, IEntity routestephandlerEntityInstance, IEntity siteEntityInstance, IEntity siteEntity_Instance, IEntity stationEntityInstance, IEntity surveyEntityInstance, IEntity surveyPageEntityInstance, IEntity uIFooterItemEntityInstance, IEntity uIThemeEntityInstance, IEntity uIWidgetEntityInstance)
		{
			return DAOFactory.CreateMediaDAO().DeleteMulti(this.Transaction, advertisementEntityInstance, alterationEntityInstance, alterationoptionEntityInstance, applicationConfigurationEntityInstance, carouselItemEntityInstance, landingPageEntityInstance, widgetActionBannerEntityInstance, widgetHeroEntityInstance, attachmentEntityInstance, actionCategoryEntityInstance, categoryEntityInstance, clientConfigurationEntityInstance, companyEntityInstance, deliverypointgroupEntityInstance, actionEntertainmentEntityInstance, entertainmentEntityInstance, actionEntertainmentcategoryEntityInstance, genericcategoryEntityInstance, genericproductEntityInstance, mediaEntityInstance, pageEntityInstance, pageEntity_Instance, pageElementEntityInstance, pageTemplateEntityInstance, pageTemplateElementEntityInstance, pointOfInterestEntityInstance, actionProductEntityInstance, productEntityInstance, productgroupEntityInstance, roomControlSectionEntityInstance, roomControlSectionItemEntityInstance, routestephandlerEntityInstance, siteEntityInstance, siteEntity_Instance, stationEntityInstance, surveyEntityInstance, surveyPageEntityInstance, uIFooterItemEntityInstance, uIThemeEntityInstance, uIWidgetEntityInstance);
		}

		/// <summary> Updates in the persistent storage all Media entities which have data in common with the specified related Entities. If one is omitted, that entity is not used as a filter.
		/// Which fields are updated in those matching entities depends on which fields are <i>changed</i> in the passed in entity entityWithNewValues. The new values of these fields are read from entityWithNewValues. </summary>
		/// <param name="entityWithNewValues">MediaEntity instance which holds the new values for the matching entities to update. Only changed fields are taken into account</param>
		/// <param name="advertisementEntityInstance">AdvertisementEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="alterationEntityInstance">AlterationEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="alterationoptionEntityInstance">AlterationoptionEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="applicationConfigurationEntityInstance">ApplicationConfigurationEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="carouselItemEntityInstance">CarouselItemEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="landingPageEntityInstance">LandingPageEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="widgetActionBannerEntityInstance">WidgetActionBannerEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="widgetHeroEntityInstance">WidgetHeroEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="attachmentEntityInstance">AttachmentEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="actionCategoryEntityInstance">CategoryEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="categoryEntityInstance">CategoryEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="clientConfigurationEntityInstance">ClientConfigurationEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="companyEntityInstance">CompanyEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="deliverypointgroupEntityInstance">DeliverypointgroupEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="actionEntertainmentEntityInstance">EntertainmentEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="entertainmentEntityInstance">EntertainmentEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="actionEntertainmentcategoryEntityInstance">EntertainmentcategoryEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="genericcategoryEntityInstance">GenericcategoryEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="genericproductEntityInstance">GenericproductEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="mediaEntityInstance">MediaEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="pageEntityInstance">PageEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="pageEntity_Instance">PageEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="pageElementEntityInstance">PageElementEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="pageTemplateEntityInstance">PageTemplateEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="pageTemplateElementEntityInstance">PageTemplateElementEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="pointOfInterestEntityInstance">PointOfInterestEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="actionProductEntityInstance">ProductEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="productEntityInstance">ProductEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="productgroupEntityInstance">ProductgroupEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="roomControlSectionEntityInstance">RoomControlSectionEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="roomControlSectionItemEntityInstance">RoomControlSectionItemEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="routestephandlerEntityInstance">RoutestephandlerEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="siteEntityInstance">SiteEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="siteEntity_Instance">SiteEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="stationEntityInstance">StationEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="surveyEntityInstance">SurveyEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="surveyPageEntityInstance">SurveyPageEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="uIFooterItemEntityInstance">UIFooterItemEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="uIThemeEntityInstance">UIThemeEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <param name="uIWidgetEntityInstance">UIWidgetEntity instance to use as a filter for the MediaEntity objects to return</param>
		/// <returns>Amount of entities affected, if the used persistent storage has rowcounting enabled.</returns>
		public int UpdateMultiManyToOne(MediaEntity entityWithNewValues, IEntity advertisementEntityInstance, IEntity alterationEntityInstance, IEntity alterationoptionEntityInstance, IEntity applicationConfigurationEntityInstance, IEntity carouselItemEntityInstance, IEntity landingPageEntityInstance, IEntity widgetActionBannerEntityInstance, IEntity widgetHeroEntityInstance, IEntity attachmentEntityInstance, IEntity actionCategoryEntityInstance, IEntity categoryEntityInstance, IEntity clientConfigurationEntityInstance, IEntity companyEntityInstance, IEntity deliverypointgroupEntityInstance, IEntity actionEntertainmentEntityInstance, IEntity entertainmentEntityInstance, IEntity actionEntertainmentcategoryEntityInstance, IEntity genericcategoryEntityInstance, IEntity genericproductEntityInstance, IEntity mediaEntityInstance, IEntity pageEntityInstance, IEntity pageEntity_Instance, IEntity pageElementEntityInstance, IEntity pageTemplateEntityInstance, IEntity pageTemplateElementEntityInstance, IEntity pointOfInterestEntityInstance, IEntity actionProductEntityInstance, IEntity productEntityInstance, IEntity productgroupEntityInstance, IEntity roomControlSectionEntityInstance, IEntity roomControlSectionItemEntityInstance, IEntity routestephandlerEntityInstance, IEntity siteEntityInstance, IEntity siteEntity_Instance, IEntity stationEntityInstance, IEntity surveyEntityInstance, IEntity surveyPageEntityInstance, IEntity uIFooterItemEntityInstance, IEntity uIThemeEntityInstance, IEntity uIWidgetEntityInstance)
		{
			return DAOFactory.CreateMediaDAO().UpdateMulti(entityWithNewValues, this.Transaction, advertisementEntityInstance, alterationEntityInstance, alterationoptionEntityInstance, applicationConfigurationEntityInstance, carouselItemEntityInstance, landingPageEntityInstance, widgetActionBannerEntityInstance, widgetHeroEntityInstance, attachmentEntityInstance, actionCategoryEntityInstance, categoryEntityInstance, clientConfigurationEntityInstance, companyEntityInstance, deliverypointgroupEntityInstance, actionEntertainmentEntityInstance, entertainmentEntityInstance, actionEntertainmentcategoryEntityInstance, genericcategoryEntityInstance, genericproductEntityInstance, mediaEntityInstance, pageEntityInstance, pageEntity_Instance, pageElementEntityInstance, pageTemplateEntityInstance, pageTemplateElementEntityInstance, pointOfInterestEntityInstance, actionProductEntityInstance, productEntityInstance, productgroupEntityInstance, roomControlSectionEntityInstance, roomControlSectionItemEntityInstance, routestephandlerEntityInstance, siteEntityInstance, siteEntity_Instance, stationEntityInstance, surveyEntityInstance, surveyPageEntityInstance, uIFooterItemEntityInstance, uIThemeEntityInstance, uIWidgetEntityInstance);
		}

		/// <summary> Retrieves in this MediaCollection object all MediaEntity objects which are related via a  Relation of type 'm:n' with the passed in CategoryEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="categoryInstance">CategoryEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCategoryCollectionViaAnnouncement(IEntity categoryInstance)
		{
			return GetMultiManyToManyUsingCategoryCollectionViaAnnouncement(categoryInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this MediaCollection object all MediaEntity objects which are related via a  relation of type 'm:n' with the passed in CategoryEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="categoryInstance">CategoryEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCategoryCollectionViaAnnouncement(IEntity categoryInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingCategoryCollectionViaAnnouncement(categoryInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this MediaCollection object all MediaEntity objects which are related via a Relation of type 'm:n' with the passed in CategoryEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="categoryInstance">CategoryEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCategoryCollectionViaAnnouncement(IEntity categoryInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingCategoryCollectionViaAnnouncement(categoryInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this MediaCollection object all MediaEntity objects which are related via a  relation of type 'm:n' with the passed in CategoryEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="categoryInstance">CategoryEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingCategoryCollectionViaAnnouncement(IEntity categoryInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateMediaDAO().GetMultiUsingCategoryCollectionViaAnnouncement(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, categoryInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this MediaCollection object all MediaEntity objects which are related via a  relation of type 'm:n' with the passed in CategoryEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="categoryInstance">CategoryEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCategoryCollectionViaAnnouncement(IEntity categoryInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateMediaDAO().GetMultiUsingCategoryCollectionViaAnnouncement(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, categoryInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this MediaCollection object all MediaEntity objects which are related via a  Relation of type 'm:n' with the passed in CategoryEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="categoryInstance">CategoryEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCategoryCollectionViaAnnouncement_(IEntity categoryInstance)
		{
			return GetMultiManyToManyUsingCategoryCollectionViaAnnouncement_(categoryInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this MediaCollection object all MediaEntity objects which are related via a  relation of type 'm:n' with the passed in CategoryEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="categoryInstance">CategoryEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCategoryCollectionViaAnnouncement_(IEntity categoryInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingCategoryCollectionViaAnnouncement_(categoryInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this MediaCollection object all MediaEntity objects which are related via a Relation of type 'm:n' with the passed in CategoryEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="categoryInstance">CategoryEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCategoryCollectionViaAnnouncement_(IEntity categoryInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingCategoryCollectionViaAnnouncement_(categoryInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this MediaCollection object all MediaEntity objects which are related via a  relation of type 'm:n' with the passed in CategoryEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="categoryInstance">CategoryEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingCategoryCollectionViaAnnouncement_(IEntity categoryInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateMediaDAO().GetMultiUsingCategoryCollectionViaAnnouncement_(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, categoryInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this MediaCollection object all MediaEntity objects which are related via a  relation of type 'm:n' with the passed in CategoryEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="categoryInstance">CategoryEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCategoryCollectionViaAnnouncement_(IEntity categoryInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateMediaDAO().GetMultiUsingCategoryCollectionViaAnnouncement_(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, categoryInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this MediaCollection object all MediaEntity objects which are related via a  Relation of type 'm:n' with the passed in CategoryEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="categoryInstance">CategoryEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCategoryCollectionViaMessage(IEntity categoryInstance)
		{
			return GetMultiManyToManyUsingCategoryCollectionViaMessage(categoryInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this MediaCollection object all MediaEntity objects which are related via a  relation of type 'm:n' with the passed in CategoryEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="categoryInstance">CategoryEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCategoryCollectionViaMessage(IEntity categoryInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingCategoryCollectionViaMessage(categoryInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this MediaCollection object all MediaEntity objects which are related via a Relation of type 'm:n' with the passed in CategoryEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="categoryInstance">CategoryEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCategoryCollectionViaMessage(IEntity categoryInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingCategoryCollectionViaMessage(categoryInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this MediaCollection object all MediaEntity objects which are related via a  relation of type 'm:n' with the passed in CategoryEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="categoryInstance">CategoryEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingCategoryCollectionViaMessage(IEntity categoryInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateMediaDAO().GetMultiUsingCategoryCollectionViaMessage(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, categoryInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this MediaCollection object all MediaEntity objects which are related via a  relation of type 'm:n' with the passed in CategoryEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="categoryInstance">CategoryEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCategoryCollectionViaMessage(IEntity categoryInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateMediaDAO().GetMultiUsingCategoryCollectionViaMessage(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, categoryInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this MediaCollection object all MediaEntity objects which are related via a  Relation of type 'm:n' with the passed in CategoryEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="categoryInstance">CategoryEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCategoryCollectionViaMessageTemplate(IEntity categoryInstance)
		{
			return GetMultiManyToManyUsingCategoryCollectionViaMessageTemplate(categoryInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this MediaCollection object all MediaEntity objects which are related via a  relation of type 'm:n' with the passed in CategoryEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="categoryInstance">CategoryEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCategoryCollectionViaMessageTemplate(IEntity categoryInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingCategoryCollectionViaMessageTemplate(categoryInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this MediaCollection object all MediaEntity objects which are related via a Relation of type 'm:n' with the passed in CategoryEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="categoryInstance">CategoryEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCategoryCollectionViaMessageTemplate(IEntity categoryInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingCategoryCollectionViaMessageTemplate(categoryInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this MediaCollection object all MediaEntity objects which are related via a  relation of type 'm:n' with the passed in CategoryEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="categoryInstance">CategoryEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingCategoryCollectionViaMessageTemplate(IEntity categoryInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateMediaDAO().GetMultiUsingCategoryCollectionViaMessageTemplate(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, categoryInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this MediaCollection object all MediaEntity objects which are related via a  relation of type 'm:n' with the passed in CategoryEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="categoryInstance">CategoryEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCategoryCollectionViaMessageTemplate(IEntity categoryInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateMediaDAO().GetMultiUsingCategoryCollectionViaMessageTemplate(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, categoryInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this MediaCollection object all MediaEntity objects which are related via a  Relation of type 'm:n' with the passed in ClientEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="clientInstance">ClientEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingClientCollectionViaMessage(IEntity clientInstance)
		{
			return GetMultiManyToManyUsingClientCollectionViaMessage(clientInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this MediaCollection object all MediaEntity objects which are related via a  relation of type 'm:n' with the passed in ClientEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="clientInstance">ClientEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingClientCollectionViaMessage(IEntity clientInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingClientCollectionViaMessage(clientInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this MediaCollection object all MediaEntity objects which are related via a Relation of type 'm:n' with the passed in ClientEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="clientInstance">ClientEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingClientCollectionViaMessage(IEntity clientInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingClientCollectionViaMessage(clientInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this MediaCollection object all MediaEntity objects which are related via a  relation of type 'm:n' with the passed in ClientEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="clientInstance">ClientEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingClientCollectionViaMessage(IEntity clientInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateMediaDAO().GetMultiUsingClientCollectionViaMessage(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, clientInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this MediaCollection object all MediaEntity objects which are related via a  relation of type 'm:n' with the passed in ClientEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="clientInstance">ClientEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingClientCollectionViaMessage(IEntity clientInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateMediaDAO().GetMultiUsingClientCollectionViaMessage(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, clientInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this MediaCollection object all MediaEntity objects which are related via a  Relation of type 'm:n' with the passed in CompanyEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="companyInstance">CompanyEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCompanyCollectionViaAnnouncement(IEntity companyInstance)
		{
			return GetMultiManyToManyUsingCompanyCollectionViaAnnouncement(companyInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this MediaCollection object all MediaEntity objects which are related via a  relation of type 'm:n' with the passed in CompanyEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="companyInstance">CompanyEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCompanyCollectionViaAnnouncement(IEntity companyInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingCompanyCollectionViaAnnouncement(companyInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this MediaCollection object all MediaEntity objects which are related via a Relation of type 'm:n' with the passed in CompanyEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="companyInstance">CompanyEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCompanyCollectionViaAnnouncement(IEntity companyInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingCompanyCollectionViaAnnouncement(companyInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this MediaCollection object all MediaEntity objects which are related via a  relation of type 'm:n' with the passed in CompanyEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="companyInstance">CompanyEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingCompanyCollectionViaAnnouncement(IEntity companyInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateMediaDAO().GetMultiUsingCompanyCollectionViaAnnouncement(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, companyInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this MediaCollection object all MediaEntity objects which are related via a  relation of type 'm:n' with the passed in CompanyEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="companyInstance">CompanyEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCompanyCollectionViaAnnouncement(IEntity companyInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateMediaDAO().GetMultiUsingCompanyCollectionViaAnnouncement(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, companyInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this MediaCollection object all MediaEntity objects which are related via a  Relation of type 'm:n' with the passed in CompanyEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="companyInstance">CompanyEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCompanyCollectionViaMessage(IEntity companyInstance)
		{
			return GetMultiManyToManyUsingCompanyCollectionViaMessage(companyInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this MediaCollection object all MediaEntity objects which are related via a  relation of type 'm:n' with the passed in CompanyEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="companyInstance">CompanyEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCompanyCollectionViaMessage(IEntity companyInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingCompanyCollectionViaMessage(companyInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this MediaCollection object all MediaEntity objects which are related via a Relation of type 'm:n' with the passed in CompanyEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="companyInstance">CompanyEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCompanyCollectionViaMessage(IEntity companyInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingCompanyCollectionViaMessage(companyInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this MediaCollection object all MediaEntity objects which are related via a  relation of type 'm:n' with the passed in CompanyEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="companyInstance">CompanyEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingCompanyCollectionViaMessage(IEntity companyInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateMediaDAO().GetMultiUsingCompanyCollectionViaMessage(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, companyInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this MediaCollection object all MediaEntity objects which are related via a  relation of type 'm:n' with the passed in CompanyEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="companyInstance">CompanyEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCompanyCollectionViaMessage(IEntity companyInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateMediaDAO().GetMultiUsingCompanyCollectionViaMessage(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, companyInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this MediaCollection object all MediaEntity objects which are related via a  Relation of type 'm:n' with the passed in CompanyEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="companyInstance">CompanyEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCompanyCollectionViaMessageTemplate(IEntity companyInstance)
		{
			return GetMultiManyToManyUsingCompanyCollectionViaMessageTemplate(companyInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this MediaCollection object all MediaEntity objects which are related via a  relation of type 'm:n' with the passed in CompanyEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="companyInstance">CompanyEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCompanyCollectionViaMessageTemplate(IEntity companyInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingCompanyCollectionViaMessageTemplate(companyInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this MediaCollection object all MediaEntity objects which are related via a Relation of type 'm:n' with the passed in CompanyEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="companyInstance">CompanyEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCompanyCollectionViaMessageTemplate(IEntity companyInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingCompanyCollectionViaMessageTemplate(companyInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this MediaCollection object all MediaEntity objects which are related via a  relation of type 'm:n' with the passed in CompanyEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="companyInstance">CompanyEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingCompanyCollectionViaMessageTemplate(IEntity companyInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateMediaDAO().GetMultiUsingCompanyCollectionViaMessageTemplate(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, companyInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this MediaCollection object all MediaEntity objects which are related via a  relation of type 'm:n' with the passed in CompanyEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="companyInstance">CompanyEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCompanyCollectionViaMessageTemplate(IEntity companyInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateMediaDAO().GetMultiUsingCompanyCollectionViaMessageTemplate(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, companyInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this MediaCollection object all MediaEntity objects which are related via a  Relation of type 'm:n' with the passed in CustomerEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="customerInstance">CustomerEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCustomerCollectionViaMessage(IEntity customerInstance)
		{
			return GetMultiManyToManyUsingCustomerCollectionViaMessage(customerInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this MediaCollection object all MediaEntity objects which are related via a  relation of type 'm:n' with the passed in CustomerEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="customerInstance">CustomerEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCustomerCollectionViaMessage(IEntity customerInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingCustomerCollectionViaMessage(customerInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this MediaCollection object all MediaEntity objects which are related via a Relation of type 'm:n' with the passed in CustomerEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="customerInstance">CustomerEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCustomerCollectionViaMessage(IEntity customerInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingCustomerCollectionViaMessage(customerInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this MediaCollection object all MediaEntity objects which are related via a  relation of type 'm:n' with the passed in CustomerEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="customerInstance">CustomerEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingCustomerCollectionViaMessage(IEntity customerInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateMediaDAO().GetMultiUsingCustomerCollectionViaMessage(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, customerInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this MediaCollection object all MediaEntity objects which are related via a  relation of type 'm:n' with the passed in CustomerEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="customerInstance">CustomerEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCustomerCollectionViaMessage(IEntity customerInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateMediaDAO().GetMultiUsingCustomerCollectionViaMessage(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, customerInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this MediaCollection object all MediaEntity objects which are related via a  Relation of type 'm:n' with the passed in DeliverypointEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="deliverypointInstance">DeliverypointEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingDeliverypointCollectionViaMessage(IEntity deliverypointInstance)
		{
			return GetMultiManyToManyUsingDeliverypointCollectionViaMessage(deliverypointInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this MediaCollection object all MediaEntity objects which are related via a  relation of type 'm:n' with the passed in DeliverypointEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="deliverypointInstance">DeliverypointEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingDeliverypointCollectionViaMessage(IEntity deliverypointInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingDeliverypointCollectionViaMessage(deliverypointInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this MediaCollection object all MediaEntity objects which are related via a Relation of type 'm:n' with the passed in DeliverypointEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="deliverypointInstance">DeliverypointEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingDeliverypointCollectionViaMessage(IEntity deliverypointInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingDeliverypointCollectionViaMessage(deliverypointInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this MediaCollection object all MediaEntity objects which are related via a  relation of type 'm:n' with the passed in DeliverypointEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="deliverypointInstance">DeliverypointEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingDeliverypointCollectionViaMessage(IEntity deliverypointInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateMediaDAO().GetMultiUsingDeliverypointCollectionViaMessage(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, deliverypointInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this MediaCollection object all MediaEntity objects which are related via a  relation of type 'm:n' with the passed in DeliverypointEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="deliverypointInstance">DeliverypointEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingDeliverypointCollectionViaMessage(IEntity deliverypointInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateMediaDAO().GetMultiUsingDeliverypointCollectionViaMessage(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, deliverypointInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this MediaCollection object all MediaEntity objects which are related via a  Relation of type 'm:n' with the passed in DeliverypointgroupEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="deliverypointgroupInstance">DeliverypointgroupEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingDeliverypointgroupCollectionViaAnnouncement(IEntity deliverypointgroupInstance)
		{
			return GetMultiManyToManyUsingDeliverypointgroupCollectionViaAnnouncement(deliverypointgroupInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this MediaCollection object all MediaEntity objects which are related via a  relation of type 'm:n' with the passed in DeliverypointgroupEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="deliverypointgroupInstance">DeliverypointgroupEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingDeliverypointgroupCollectionViaAnnouncement(IEntity deliverypointgroupInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingDeliverypointgroupCollectionViaAnnouncement(deliverypointgroupInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this MediaCollection object all MediaEntity objects which are related via a Relation of type 'm:n' with the passed in DeliverypointgroupEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="deliverypointgroupInstance">DeliverypointgroupEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingDeliverypointgroupCollectionViaAnnouncement(IEntity deliverypointgroupInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingDeliverypointgroupCollectionViaAnnouncement(deliverypointgroupInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this MediaCollection object all MediaEntity objects which are related via a  relation of type 'm:n' with the passed in DeliverypointgroupEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="deliverypointgroupInstance">DeliverypointgroupEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingDeliverypointgroupCollectionViaAnnouncement(IEntity deliverypointgroupInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateMediaDAO().GetMultiUsingDeliverypointgroupCollectionViaAnnouncement(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, deliverypointgroupInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this MediaCollection object all MediaEntity objects which are related via a  relation of type 'm:n' with the passed in DeliverypointgroupEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="deliverypointgroupInstance">DeliverypointgroupEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingDeliverypointgroupCollectionViaAnnouncement(IEntity deliverypointgroupInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateMediaDAO().GetMultiUsingDeliverypointgroupCollectionViaAnnouncement(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, deliverypointgroupInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this MediaCollection object all MediaEntity objects which are related via a  Relation of type 'm:n' with the passed in EntertainmentEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="entertainmentInstance">EntertainmentEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingEntertainmentCollectionViaAnnouncement(IEntity entertainmentInstance)
		{
			return GetMultiManyToManyUsingEntertainmentCollectionViaAnnouncement(entertainmentInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this MediaCollection object all MediaEntity objects which are related via a  relation of type 'm:n' with the passed in EntertainmentEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="entertainmentInstance">EntertainmentEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingEntertainmentCollectionViaAnnouncement(IEntity entertainmentInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingEntertainmentCollectionViaAnnouncement(entertainmentInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this MediaCollection object all MediaEntity objects which are related via a Relation of type 'm:n' with the passed in EntertainmentEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="entertainmentInstance">EntertainmentEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingEntertainmentCollectionViaAnnouncement(IEntity entertainmentInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingEntertainmentCollectionViaAnnouncement(entertainmentInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this MediaCollection object all MediaEntity objects which are related via a  relation of type 'm:n' with the passed in EntertainmentEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="entertainmentInstance">EntertainmentEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingEntertainmentCollectionViaAnnouncement(IEntity entertainmentInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateMediaDAO().GetMultiUsingEntertainmentCollectionViaAnnouncement(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, entertainmentInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this MediaCollection object all MediaEntity objects which are related via a  relation of type 'm:n' with the passed in EntertainmentEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="entertainmentInstance">EntertainmentEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingEntertainmentCollectionViaAnnouncement(IEntity entertainmentInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateMediaDAO().GetMultiUsingEntertainmentCollectionViaAnnouncement(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, entertainmentInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this MediaCollection object all MediaEntity objects which are related via a  Relation of type 'm:n' with the passed in EntertainmentEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="entertainmentInstance">EntertainmentEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingEntertainmentCollectionViaMessage(IEntity entertainmentInstance)
		{
			return GetMultiManyToManyUsingEntertainmentCollectionViaMessage(entertainmentInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this MediaCollection object all MediaEntity objects which are related via a  relation of type 'm:n' with the passed in EntertainmentEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="entertainmentInstance">EntertainmentEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingEntertainmentCollectionViaMessage(IEntity entertainmentInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingEntertainmentCollectionViaMessage(entertainmentInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this MediaCollection object all MediaEntity objects which are related via a Relation of type 'm:n' with the passed in EntertainmentEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="entertainmentInstance">EntertainmentEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingEntertainmentCollectionViaMessage(IEntity entertainmentInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingEntertainmentCollectionViaMessage(entertainmentInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this MediaCollection object all MediaEntity objects which are related via a  relation of type 'm:n' with the passed in EntertainmentEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="entertainmentInstance">EntertainmentEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingEntertainmentCollectionViaMessage(IEntity entertainmentInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateMediaDAO().GetMultiUsingEntertainmentCollectionViaMessage(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, entertainmentInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this MediaCollection object all MediaEntity objects which are related via a  relation of type 'm:n' with the passed in EntertainmentEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="entertainmentInstance">EntertainmentEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingEntertainmentCollectionViaMessage(IEntity entertainmentInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateMediaDAO().GetMultiUsingEntertainmentCollectionViaMessage(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, entertainmentInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this MediaCollection object all MediaEntity objects which are related via a  Relation of type 'm:n' with the passed in EntertainmentEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="entertainmentInstance">EntertainmentEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingEntertainmentCollectionViaMessageTemplate(IEntity entertainmentInstance)
		{
			return GetMultiManyToManyUsingEntertainmentCollectionViaMessageTemplate(entertainmentInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this MediaCollection object all MediaEntity objects which are related via a  relation of type 'm:n' with the passed in EntertainmentEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="entertainmentInstance">EntertainmentEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingEntertainmentCollectionViaMessageTemplate(IEntity entertainmentInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingEntertainmentCollectionViaMessageTemplate(entertainmentInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this MediaCollection object all MediaEntity objects which are related via a Relation of type 'm:n' with the passed in EntertainmentEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="entertainmentInstance">EntertainmentEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingEntertainmentCollectionViaMessageTemplate(IEntity entertainmentInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingEntertainmentCollectionViaMessageTemplate(entertainmentInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this MediaCollection object all MediaEntity objects which are related via a  relation of type 'm:n' with the passed in EntertainmentEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="entertainmentInstance">EntertainmentEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingEntertainmentCollectionViaMessageTemplate(IEntity entertainmentInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateMediaDAO().GetMultiUsingEntertainmentCollectionViaMessageTemplate(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, entertainmentInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this MediaCollection object all MediaEntity objects which are related via a  relation of type 'm:n' with the passed in EntertainmentEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="entertainmentInstance">EntertainmentEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingEntertainmentCollectionViaMessageTemplate(IEntity entertainmentInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateMediaDAO().GetMultiUsingEntertainmentCollectionViaMessageTemplate(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, entertainmentInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this MediaCollection object all MediaEntity objects which are related via a  Relation of type 'm:n' with the passed in EntertainmentcategoryEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="entertainmentcategoryInstance">EntertainmentcategoryEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingEntertainmentcategoryCollectionViaAnnouncement(IEntity entertainmentcategoryInstance)
		{
			return GetMultiManyToManyUsingEntertainmentcategoryCollectionViaAnnouncement(entertainmentcategoryInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this MediaCollection object all MediaEntity objects which are related via a  relation of type 'm:n' with the passed in EntertainmentcategoryEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="entertainmentcategoryInstance">EntertainmentcategoryEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingEntertainmentcategoryCollectionViaAnnouncement(IEntity entertainmentcategoryInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingEntertainmentcategoryCollectionViaAnnouncement(entertainmentcategoryInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this MediaCollection object all MediaEntity objects which are related via a Relation of type 'm:n' with the passed in EntertainmentcategoryEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="entertainmentcategoryInstance">EntertainmentcategoryEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingEntertainmentcategoryCollectionViaAnnouncement(IEntity entertainmentcategoryInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingEntertainmentcategoryCollectionViaAnnouncement(entertainmentcategoryInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this MediaCollection object all MediaEntity objects which are related via a  relation of type 'm:n' with the passed in EntertainmentcategoryEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="entertainmentcategoryInstance">EntertainmentcategoryEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingEntertainmentcategoryCollectionViaAnnouncement(IEntity entertainmentcategoryInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateMediaDAO().GetMultiUsingEntertainmentcategoryCollectionViaAnnouncement(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, entertainmentcategoryInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this MediaCollection object all MediaEntity objects which are related via a  relation of type 'm:n' with the passed in EntertainmentcategoryEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="entertainmentcategoryInstance">EntertainmentcategoryEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingEntertainmentcategoryCollectionViaAnnouncement(IEntity entertainmentcategoryInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateMediaDAO().GetMultiUsingEntertainmentcategoryCollectionViaAnnouncement(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, entertainmentcategoryInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this MediaCollection object all MediaEntity objects which are related via a  Relation of type 'm:n' with the passed in EntertainmentcategoryEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="entertainmentcategoryInstance">EntertainmentcategoryEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingEntertainmentcategoryCollectionViaAnnouncement_(IEntity entertainmentcategoryInstance)
		{
			return GetMultiManyToManyUsingEntertainmentcategoryCollectionViaAnnouncement_(entertainmentcategoryInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this MediaCollection object all MediaEntity objects which are related via a  relation of type 'm:n' with the passed in EntertainmentcategoryEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="entertainmentcategoryInstance">EntertainmentcategoryEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingEntertainmentcategoryCollectionViaAnnouncement_(IEntity entertainmentcategoryInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingEntertainmentcategoryCollectionViaAnnouncement_(entertainmentcategoryInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this MediaCollection object all MediaEntity objects which are related via a Relation of type 'm:n' with the passed in EntertainmentcategoryEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="entertainmentcategoryInstance">EntertainmentcategoryEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingEntertainmentcategoryCollectionViaAnnouncement_(IEntity entertainmentcategoryInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingEntertainmentcategoryCollectionViaAnnouncement_(entertainmentcategoryInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this MediaCollection object all MediaEntity objects which are related via a  relation of type 'm:n' with the passed in EntertainmentcategoryEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="entertainmentcategoryInstance">EntertainmentcategoryEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingEntertainmentcategoryCollectionViaAnnouncement_(IEntity entertainmentcategoryInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateMediaDAO().GetMultiUsingEntertainmentcategoryCollectionViaAnnouncement_(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, entertainmentcategoryInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this MediaCollection object all MediaEntity objects which are related via a  relation of type 'm:n' with the passed in EntertainmentcategoryEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="entertainmentcategoryInstance">EntertainmentcategoryEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingEntertainmentcategoryCollectionViaAnnouncement_(IEntity entertainmentcategoryInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateMediaDAO().GetMultiUsingEntertainmentcategoryCollectionViaAnnouncement_(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, entertainmentcategoryInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this MediaCollection object all MediaEntity objects which are related via a  Relation of type 'm:n' with the passed in OrderEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="orderInstance">OrderEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingOrderCollectionViaMessage(IEntity orderInstance)
		{
			return GetMultiManyToManyUsingOrderCollectionViaMessage(orderInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this MediaCollection object all MediaEntity objects which are related via a  relation of type 'm:n' with the passed in OrderEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="orderInstance">OrderEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingOrderCollectionViaMessage(IEntity orderInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingOrderCollectionViaMessage(orderInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this MediaCollection object all MediaEntity objects which are related via a Relation of type 'm:n' with the passed in OrderEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="orderInstance">OrderEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingOrderCollectionViaMessage(IEntity orderInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingOrderCollectionViaMessage(orderInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this MediaCollection object all MediaEntity objects which are related via a  relation of type 'm:n' with the passed in OrderEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="orderInstance">OrderEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingOrderCollectionViaMessage(IEntity orderInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateMediaDAO().GetMultiUsingOrderCollectionViaMessage(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, orderInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this MediaCollection object all MediaEntity objects which are related via a  relation of type 'm:n' with the passed in OrderEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="orderInstance">OrderEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingOrderCollectionViaMessage(IEntity orderInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateMediaDAO().GetMultiUsingOrderCollectionViaMessage(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, orderInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this MediaCollection object all MediaEntity objects which are related via a  Relation of type 'm:n' with the passed in ProductEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="productInstance">ProductEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingProductCollectionViaAnnouncement(IEntity productInstance)
		{
			return GetMultiManyToManyUsingProductCollectionViaAnnouncement(productInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this MediaCollection object all MediaEntity objects which are related via a  relation of type 'm:n' with the passed in ProductEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="productInstance">ProductEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingProductCollectionViaAnnouncement(IEntity productInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingProductCollectionViaAnnouncement(productInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this MediaCollection object all MediaEntity objects which are related via a Relation of type 'm:n' with the passed in ProductEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="productInstance">ProductEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingProductCollectionViaAnnouncement(IEntity productInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingProductCollectionViaAnnouncement(productInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this MediaCollection object all MediaEntity objects which are related via a  relation of type 'm:n' with the passed in ProductEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="productInstance">ProductEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingProductCollectionViaAnnouncement(IEntity productInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateMediaDAO().GetMultiUsingProductCollectionViaAnnouncement(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, productInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this MediaCollection object all MediaEntity objects which are related via a  relation of type 'm:n' with the passed in ProductEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="productInstance">ProductEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingProductCollectionViaAnnouncement(IEntity productInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateMediaDAO().GetMultiUsingProductCollectionViaAnnouncement(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, productInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this MediaCollection object all MediaEntity objects which are related via a  Relation of type 'm:n' with the passed in ProductEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="productInstance">ProductEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingProductCollectionViaMessageTemplate(IEntity productInstance)
		{
			return GetMultiManyToManyUsingProductCollectionViaMessageTemplate(productInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this MediaCollection object all MediaEntity objects which are related via a  relation of type 'm:n' with the passed in ProductEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="productInstance">ProductEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingProductCollectionViaMessageTemplate(IEntity productInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingProductCollectionViaMessageTemplate(productInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this MediaCollection object all MediaEntity objects which are related via a Relation of type 'm:n' with the passed in ProductEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="productInstance">ProductEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingProductCollectionViaMessageTemplate(IEntity productInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingProductCollectionViaMessageTemplate(productInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this MediaCollection object all MediaEntity objects which are related via a  relation of type 'm:n' with the passed in ProductEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="productInstance">ProductEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingProductCollectionViaMessageTemplate(IEntity productInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateMediaDAO().GetMultiUsingProductCollectionViaMessageTemplate(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, productInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this MediaCollection object all MediaEntity objects which are related via a  relation of type 'm:n' with the passed in ProductEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="productInstance">ProductEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingProductCollectionViaMessageTemplate(IEntity productInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateMediaDAO().GetMultiUsingProductCollectionViaMessageTemplate(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, productInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves Entity rows in a datatable which match the specified filter. It will always create a new connection to the database.</summary>
		/// <param name="selectFilter">A predicate or predicate expression which should be used as filter for the entities to retrieve.</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>DataTable with the rows requested.</returns>
		public static DataTable GetMultiAsDataTable(IPredicate selectFilter, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiAsDataTable(selectFilter, maxNumberOfItemsToReturn, sortClauses, null, 0, 0);
		}

		/// <summary> Retrieves Entity rows in a datatable which match the specified filter. It will always create a new connection to the database.</summary>
		/// <param name="selectFilter">A predicate or predicate expression which should be used as filter for the entities to retrieve.</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="relations">The set of relations to walk to construct to total query.</param>
		/// <returns>DataTable with the rows requested.</returns>
		public static DataTable GetMultiAsDataTable(IPredicate selectFilter, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IRelationCollection relations)
		{
			return GetMultiAsDataTable(selectFilter, maxNumberOfItemsToReturn, sortClauses, relations, 0, 0);
		}
		
		/// <summary> Retrieves Entity rows in a datatable which match the specified filter. It will always create a new connection to the database.</summary>
		/// <param name="selectFilter">A predicate or predicate expression which should be used as filter for the entities to retrieve.</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="relations">The set of relations to walk to construct to total query.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>DataTable with the rows requested.</returns>
		public static DataTable GetMultiAsDataTable(IPredicate selectFilter, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IRelationCollection relations, int pageNumber, int pageSize)
		{
			MediaDAO dao = DAOFactory.CreateMediaDAO();
			return dao.GetMultiAsDataTable(maxNumberOfItemsToReturn, sortClauses, selectFilter, relations, pageNumber, pageSize);
		}


		
		/// <summary> Gets a scalar value, calculated with the aggregate. the field index specified is the field the aggregate are applied on.</summary>
		/// <param name="fieldIndex">Field index of field to which to apply the aggregate function and expression</param>
		/// <param name="aggregateToApply">Aggregate function to apply. </param>
		/// <returns>the scalar value requested</returns>
		public object GetScalar(MediaFieldIndex fieldIndex, AggregateFunction aggregateToApply)
		{
			return GetScalar(fieldIndex, null, aggregateToApply, null, null, null);
		}

		/// <summary> Gets a scalar value, calculated with the aggregate and expression specified. the field index specified is the field the expression and aggregate are applied on.</summary>
		/// <param name="fieldIndex">Field index of field to which to apply the aggregate function and expression</param>
		/// <param name="expressionToExecute">The expression to execute. Can be null</param>
		/// <param name="aggregateToApply">Aggregate function to apply. </param>
		/// <returns>the scalar value requested</returns>
		public object GetScalar(MediaFieldIndex fieldIndex, IExpression expressionToExecute, AggregateFunction aggregateToApply)
		{
			return GetScalar(fieldIndex, expressionToExecute, aggregateToApply, null, null, null);
		}

		/// <summary> Gets a scalar value, calculated with the aggregate and expression specified. the field index specified is the field the expression and aggregate are applied on.</summary>
		/// <param name="fieldIndex">Field index of field to which to apply the aggregate function and expression</param>
		/// <param name="expressionToExecute">The expression to execute. Can be null</param>
		/// <param name="aggregateToApply">Aggregate function to apply. </param>
		/// <param name="filter">The filter to apply to retrieve the scalar</param>
		/// <returns>the scalar value requested</returns>
		public object GetScalar(MediaFieldIndex fieldIndex, IExpression expressionToExecute, AggregateFunction aggregateToApply, IPredicate filter)
		{
			return GetScalar(fieldIndex, expressionToExecute, aggregateToApply, filter, null, null);
		}

		/// <summary> Gets a scalar value, calculated with the aggregate and expression specified. the field index specified is the field the expression and aggregate are applied on.</summary>
		/// <param name="fieldIndex">Field index of field to which to apply the aggregate function and expression</param>
		/// <param name="expressionToExecute">The expression to execute. Can be null</param>
		/// <param name="aggregateToApply">Aggregate function to apply. </param>
		/// <param name="filter">The filter to apply to retrieve the scalar</param>
		/// <param name="groupByClause">The groupby clause to apply to retrieve the scalar</param>
		/// <returns>the scalar value requested</returns>
		public object GetScalar(MediaFieldIndex fieldIndex, IExpression expressionToExecute, AggregateFunction aggregateToApply, IPredicate filter, IGroupByCollection groupByClause)
		{
			return GetScalar(fieldIndex, expressionToExecute, aggregateToApply, filter, null, groupByClause);
		}

		/// <summary> Gets a scalar value, calculated with the aggregate and expression specified. the field index specified is the field the expression and aggregate are applied on.</summary>
		/// <param name="fieldIndex">Field index of field to which to apply the aggregate function and expression</param>
		/// <param name="expressionToExecute">The expression to execute. Can be null</param>
		/// <param name="aggregateToApply">Aggregate function to apply. </param>
		/// <param name="filter">The filter to apply to retrieve the scalar</param>
		/// <param name="relations">The relations to walk</param>
		/// <param name="groupByClause">The groupby clause to apply to retrieve the scalar</param>
		/// <returns>the scalar value requested</returns>
		public virtual object GetScalar(MediaFieldIndex fieldIndex, IExpression expressionToExecute, AggregateFunction aggregateToApply, IPredicate filter, IRelationCollection relations, IGroupByCollection groupByClause)
		{
			EntityFields fields = new EntityFields(1);
			fields[0] = EntityFieldFactory.Create(fieldIndex);
			if((fields[0].ExpressionToApply == null) || (expressionToExecute != null))
			{
				fields[0].ExpressionToApply = expressionToExecute;
			}
			if((fields[0].AggregateFunctionToApply == AggregateFunction.None) || (aggregateToApply != AggregateFunction.None))
			{
				fields[0].AggregateFunctionToApply = aggregateToApply;
			}
			return DAOFactory.CreateMediaDAO().GetScalar(fields, this.Transaction, filter, relations, groupByClause);
		}
		
		/// <summary>Creats a new DAO instance so code which is in the base class can still use the proper DAO object.</summary>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateMediaDAO();
		}
		
		/// <summary>Creates a new transaction object</summary>
		/// <param name="levelOfIsolation">The level of isolation.</param>
		/// <param name="name">The name.</param>
		protected override ITransaction CreateTransaction( IsolationLevel levelOfIsolation, string name )
		{
			return new Transaction(levelOfIsolation, name);
		}

		#region Custom EntityCollection code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCollectionCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion
		
		#region Included Code

		#endregion
	}
}
