﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Data;
using System.Collections.Generic;
using System.ComponentModel;
using System.Xml;
#if !CF
using System.Runtime.Serialization;
#endif

using Obymobi.Data.EntityClasses;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.CollectionClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Collection class for storing and retrieving collections of UIWidgetEntity objects. </summary>
	[Serializable]
	public partial class UIWidgetCollection : CommonEntityCollection<UIWidgetEntity>
	{
		/// <summary> CTor</summary>
		public UIWidgetCollection():base(new UIWidgetEntityFactory())
		{
		}

		/// <summary> CTor</summary>
		/// <param name="initialContents">The initial contents of this collection.</param>
		public UIWidgetCollection(IEnumerable<UIWidgetEntity> initialContents):base(new UIWidgetEntityFactory())
		{
			AddRange(initialContents);
		}

		/// <summary> CTor</summary>
		/// <param name="entityFactoryToUse">The EntityFactory to use when creating entity objects during a GetMulti() call.</param>
		public UIWidgetCollection(IEntityFactory entityFactoryToUse):base(entityFactoryToUse)
		{
		}

		/// <summary> Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected UIWidgetCollection(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}


		/// <summary> Retrieves in this UIWidgetCollection object all UIWidgetEntity objects which have data in common with the specified related Entities.
		/// If one is omitted, that entity is not used as a filter. All current elements in the collection are removed from the collection.</summary>
		/// <param name="advertisementEntityInstance">AdvertisementEntity instance to use as a filter for the UIWidgetEntity objects to return</param>
		/// <param name="categoryEntityInstance">CategoryEntity instance to use as a filter for the UIWidgetEntity objects to return</param>
		/// <param name="entertainmentEntityInstance">EntertainmentEntity instance to use as a filter for the UIWidgetEntity objects to return</param>
		/// <param name="entertainmentcategoryEntityInstance">EntertainmentcategoryEntity instance to use as a filter for the UIWidgetEntity objects to return</param>
		/// <param name="pageEntityInstance">PageEntity instance to use as a filter for the UIWidgetEntity objects to return</param>
		/// <param name="productCategoryEntityInstance">ProductCategoryEntity instance to use as a filter for the UIWidgetEntity objects to return</param>
		/// <param name="roomControlAreaEntityInstance">RoomControlAreaEntity instance to use as a filter for the UIWidgetEntity objects to return</param>
		/// <param name="roomControlSectionEntityInstance">RoomControlSectionEntity instance to use as a filter for the UIWidgetEntity objects to return</param>
		/// <param name="siteEntityInstance">SiteEntity instance to use as a filter for the UIWidgetEntity objects to return</param>
		/// <param name="uITabEntityInstance">UITabEntity instance to use as a filter for the UIWidgetEntity objects to return</param>
		/// <returns>true if succeeded, false otherwise</returns>
		public bool GetMultiManyToOne(IEntity advertisementEntityInstance, IEntity categoryEntityInstance, IEntity entertainmentEntityInstance, IEntity entertainmentcategoryEntityInstance, IEntity pageEntityInstance, IEntity productCategoryEntityInstance, IEntity roomControlAreaEntityInstance, IEntity roomControlSectionEntityInstance, IEntity siteEntityInstance, IEntity uITabEntityInstance)
		{
			return GetMultiManyToOne(advertisementEntityInstance, categoryEntityInstance, entertainmentEntityInstance, entertainmentcategoryEntityInstance, pageEntityInstance, productCategoryEntityInstance, roomControlAreaEntityInstance, roomControlSectionEntityInstance, siteEntityInstance, uITabEntityInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, null, 0, 0);
		}

		/// <summary> Retrieves in this UIWidgetCollection object all UIWidgetEntity objects which have data in common with the specified related Entities.
		/// If one is omitted, that entity is not used as a filter. All current elements in the collection are removed from the collection.</summary>
		/// <param name="advertisementEntityInstance">AdvertisementEntity instance to use as a filter for the UIWidgetEntity objects to return</param>
		/// <param name="categoryEntityInstance">CategoryEntity instance to use as a filter for the UIWidgetEntity objects to return</param>
		/// <param name="entertainmentEntityInstance">EntertainmentEntity instance to use as a filter for the UIWidgetEntity objects to return</param>
		/// <param name="entertainmentcategoryEntityInstance">EntertainmentcategoryEntity instance to use as a filter for the UIWidgetEntity objects to return</param>
		/// <param name="pageEntityInstance">PageEntity instance to use as a filter for the UIWidgetEntity objects to return</param>
		/// <param name="productCategoryEntityInstance">ProductCategoryEntity instance to use as a filter for the UIWidgetEntity objects to return</param>
		/// <param name="roomControlAreaEntityInstance">RoomControlAreaEntity instance to use as a filter for the UIWidgetEntity objects to return</param>
		/// <param name="roomControlSectionEntityInstance">RoomControlSectionEntity instance to use as a filter for the UIWidgetEntity objects to return</param>
		/// <param name="siteEntityInstance">SiteEntity instance to use as a filter for the UIWidgetEntity objects to return</param>
		/// <param name="uITabEntityInstance">UITabEntity instance to use as a filter for the UIWidgetEntity objects to return</param>
		/// <param name="filter">Extra filter to limit the resultset. Predicate expression can be null, in which case it will be ignored.</param>
		/// <returns>true if succeeded, false otherwise</returns>
		public bool GetMultiManyToOne(IEntity advertisementEntityInstance, IEntity categoryEntityInstance, IEntity entertainmentEntityInstance, IEntity entertainmentcategoryEntityInstance, IEntity pageEntityInstance, IEntity productCategoryEntityInstance, IEntity roomControlAreaEntityInstance, IEntity roomControlSectionEntityInstance, IEntity siteEntityInstance, IEntity uITabEntityInstance, IPredicateExpression filter)
		{
			return GetMultiManyToOne(advertisementEntityInstance, categoryEntityInstance, entertainmentEntityInstance, entertainmentcategoryEntityInstance, pageEntityInstance, productCategoryEntityInstance, roomControlAreaEntityInstance, roomControlSectionEntityInstance, siteEntityInstance, uITabEntityInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, filter, 0, 0);
		}

		/// <summary> Retrieves in this UIWidgetCollection object all UIWidgetEntity objects which have data in common with the specified related Entities.
		/// If one is omitted, that entity is not used as a filter. All current elements in the collection are removed from the collection.</summary>
		/// <param name="advertisementEntityInstance">AdvertisementEntity instance to use as a filter for the UIWidgetEntity objects to return</param>
		/// <param name="categoryEntityInstance">CategoryEntity instance to use as a filter for the UIWidgetEntity objects to return</param>
		/// <param name="entertainmentEntityInstance">EntertainmentEntity instance to use as a filter for the UIWidgetEntity objects to return</param>
		/// <param name="entertainmentcategoryEntityInstance">EntertainmentcategoryEntity instance to use as a filter for the UIWidgetEntity objects to return</param>
		/// <param name="pageEntityInstance">PageEntity instance to use as a filter for the UIWidgetEntity objects to return</param>
		/// <param name="productCategoryEntityInstance">ProductCategoryEntity instance to use as a filter for the UIWidgetEntity objects to return</param>
		/// <param name="roomControlAreaEntityInstance">RoomControlAreaEntity instance to use as a filter for the UIWidgetEntity objects to return</param>
		/// <param name="roomControlSectionEntityInstance">RoomControlSectionEntity instance to use as a filter for the UIWidgetEntity objects to return</param>
		/// <param name="siteEntityInstance">SiteEntity instance to use as a filter for the UIWidgetEntity objects to return</param>
		/// <param name="uITabEntityInstance">UITabEntity instance to use as a filter for the UIWidgetEntity objects to return</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="filter">Extra filter to limit the resultset. Predicate expression can be null, in which case it will be ignored.</param>
		/// <returns>true if succeeded, false otherwise</returns>
		public bool GetMultiManyToOne(IEntity advertisementEntityInstance, IEntity categoryEntityInstance, IEntity entertainmentEntityInstance, IEntity entertainmentcategoryEntityInstance, IEntity pageEntityInstance, IEntity productCategoryEntityInstance, IEntity roomControlAreaEntityInstance, IEntity roomControlSectionEntityInstance, IEntity siteEntityInstance, IEntity uITabEntityInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPredicateExpression filter)
		{
			return GetMultiManyToOne(advertisementEntityInstance, categoryEntityInstance, entertainmentEntityInstance, entertainmentcategoryEntityInstance, pageEntityInstance, productCategoryEntityInstance, roomControlAreaEntityInstance, roomControlSectionEntityInstance, siteEntityInstance, uITabEntityInstance, maxNumberOfItemsToReturn, sortClauses, filter, 0, 0);
		}

		/// <summary> Retrieves in this UIWidgetCollection object all UIWidgetEntity objects which have data in common with the specified related Entities.
		/// If one is omitted, that entity is not used as a filter. All current elements in the collection are removed from the collection.</summary>
		/// <param name="advertisementEntityInstance">AdvertisementEntity instance to use as a filter for the UIWidgetEntity objects to return</param>
		/// <param name="categoryEntityInstance">CategoryEntity instance to use as a filter for the UIWidgetEntity objects to return</param>
		/// <param name="entertainmentEntityInstance">EntertainmentEntity instance to use as a filter for the UIWidgetEntity objects to return</param>
		/// <param name="entertainmentcategoryEntityInstance">EntertainmentcategoryEntity instance to use as a filter for the UIWidgetEntity objects to return</param>
		/// <param name="pageEntityInstance">PageEntity instance to use as a filter for the UIWidgetEntity objects to return</param>
		/// <param name="productCategoryEntityInstance">ProductCategoryEntity instance to use as a filter for the UIWidgetEntity objects to return</param>
		/// <param name="roomControlAreaEntityInstance">RoomControlAreaEntity instance to use as a filter for the UIWidgetEntity objects to return</param>
		/// <param name="roomControlSectionEntityInstance">RoomControlSectionEntity instance to use as a filter for the UIWidgetEntity objects to return</param>
		/// <param name="siteEntityInstance">SiteEntity instance to use as a filter for the UIWidgetEntity objects to return</param>
		/// <param name="uITabEntityInstance">UITabEntity instance to use as a filter for the UIWidgetEntity objects to return</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="filter">Extra filter to limit the resultset. Predicate expression can be null, in which case it will be ignored.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToOne(IEntity advertisementEntityInstance, IEntity categoryEntityInstance, IEntity entertainmentEntityInstance, IEntity entertainmentcategoryEntityInstance, IEntity pageEntityInstance, IEntity productCategoryEntityInstance, IEntity roomControlAreaEntityInstance, IEntity roomControlSectionEntityInstance, IEntity siteEntityInstance, IEntity uITabEntityInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPredicateExpression filter, int pageNumber, int pageSize)
		{
			bool validParameters = false;
			validParameters |= (advertisementEntityInstance!=null);
			validParameters |= (categoryEntityInstance!=null);
			validParameters |= (entertainmentEntityInstance!=null);
			validParameters |= (entertainmentcategoryEntityInstance!=null);
			validParameters |= (pageEntityInstance!=null);
			validParameters |= (productCategoryEntityInstance!=null);
			validParameters |= (roomControlAreaEntityInstance!=null);
			validParameters |= (roomControlSectionEntityInstance!=null);
			validParameters |= (siteEntityInstance!=null);
			validParameters |= (uITabEntityInstance!=null);
			if(!validParameters)
			{
				return GetMulti(filter, maxNumberOfItemsToReturn, sortClauses, null, pageNumber, pageSize);
			}
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateUIWidgetDAO().GetMulti(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, filter, advertisementEntityInstance, categoryEntityInstance, entertainmentEntityInstance, entertainmentcategoryEntityInstance, pageEntityInstance, productCategoryEntityInstance, roomControlAreaEntityInstance, roomControlSectionEntityInstance, siteEntityInstance, uITabEntityInstance, pageNumber, pageSize);
		}

		/// <summary> Deletes from the persistent storage all UIWidget entities which have data in common with the specified related Entities. If one is omitted, that entity is not used as a filter.</summary>
		/// <remarks>Runs directly on the persistent storage. It will not delete entity objects from the current collection.</remarks>
		/// <param name="advertisementEntityInstance">AdvertisementEntity instance to use as a filter for the UIWidgetEntity objects to return</param>
		/// <param name="categoryEntityInstance">CategoryEntity instance to use as a filter for the UIWidgetEntity objects to return</param>
		/// <param name="entertainmentEntityInstance">EntertainmentEntity instance to use as a filter for the UIWidgetEntity objects to return</param>
		/// <param name="entertainmentcategoryEntityInstance">EntertainmentcategoryEntity instance to use as a filter for the UIWidgetEntity objects to return</param>
		/// <param name="pageEntityInstance">PageEntity instance to use as a filter for the UIWidgetEntity objects to return</param>
		/// <param name="productCategoryEntityInstance">ProductCategoryEntity instance to use as a filter for the UIWidgetEntity objects to return</param>
		/// <param name="roomControlAreaEntityInstance">RoomControlAreaEntity instance to use as a filter for the UIWidgetEntity objects to return</param>
		/// <param name="roomControlSectionEntityInstance">RoomControlSectionEntity instance to use as a filter for the UIWidgetEntity objects to return</param>
		/// <param name="siteEntityInstance">SiteEntity instance to use as a filter for the UIWidgetEntity objects to return</param>
		/// <param name="uITabEntityInstance">UITabEntity instance to use as a filter for the UIWidgetEntity objects to return</param>
		/// <returns>Amount of entities affected, if the used persistent storage has rowcounting enabled.</returns>
		public int DeleteMultiManyToOne(IEntity advertisementEntityInstance, IEntity categoryEntityInstance, IEntity entertainmentEntityInstance, IEntity entertainmentcategoryEntityInstance, IEntity pageEntityInstance, IEntity productCategoryEntityInstance, IEntity roomControlAreaEntityInstance, IEntity roomControlSectionEntityInstance, IEntity siteEntityInstance, IEntity uITabEntityInstance)
		{
			return DAOFactory.CreateUIWidgetDAO().DeleteMulti(this.Transaction, advertisementEntityInstance, categoryEntityInstance, entertainmentEntityInstance, entertainmentcategoryEntityInstance, pageEntityInstance, productCategoryEntityInstance, roomControlAreaEntityInstance, roomControlSectionEntityInstance, siteEntityInstance, uITabEntityInstance);
		}

		/// <summary> Updates in the persistent storage all UIWidget entities which have data in common with the specified related Entities. If one is omitted, that entity is not used as a filter.
		/// Which fields are updated in those matching entities depends on which fields are <i>changed</i> in the passed in entity entityWithNewValues. The new values of these fields are read from entityWithNewValues. </summary>
		/// <param name="entityWithNewValues">UIWidgetEntity instance which holds the new values for the matching entities to update. Only changed fields are taken into account</param>
		/// <param name="advertisementEntityInstance">AdvertisementEntity instance to use as a filter for the UIWidgetEntity objects to return</param>
		/// <param name="categoryEntityInstance">CategoryEntity instance to use as a filter for the UIWidgetEntity objects to return</param>
		/// <param name="entertainmentEntityInstance">EntertainmentEntity instance to use as a filter for the UIWidgetEntity objects to return</param>
		/// <param name="entertainmentcategoryEntityInstance">EntertainmentcategoryEntity instance to use as a filter for the UIWidgetEntity objects to return</param>
		/// <param name="pageEntityInstance">PageEntity instance to use as a filter for the UIWidgetEntity objects to return</param>
		/// <param name="productCategoryEntityInstance">ProductCategoryEntity instance to use as a filter for the UIWidgetEntity objects to return</param>
		/// <param name="roomControlAreaEntityInstance">RoomControlAreaEntity instance to use as a filter for the UIWidgetEntity objects to return</param>
		/// <param name="roomControlSectionEntityInstance">RoomControlSectionEntity instance to use as a filter for the UIWidgetEntity objects to return</param>
		/// <param name="siteEntityInstance">SiteEntity instance to use as a filter for the UIWidgetEntity objects to return</param>
		/// <param name="uITabEntityInstance">UITabEntity instance to use as a filter for the UIWidgetEntity objects to return</param>
		/// <returns>Amount of entities affected, if the used persistent storage has rowcounting enabled.</returns>
		public int UpdateMultiManyToOne(UIWidgetEntity entityWithNewValues, IEntity advertisementEntityInstance, IEntity categoryEntityInstance, IEntity entertainmentEntityInstance, IEntity entertainmentcategoryEntityInstance, IEntity pageEntityInstance, IEntity productCategoryEntityInstance, IEntity roomControlAreaEntityInstance, IEntity roomControlSectionEntityInstance, IEntity siteEntityInstance, IEntity uITabEntityInstance)
		{
			return DAOFactory.CreateUIWidgetDAO().UpdateMulti(entityWithNewValues, this.Transaction, advertisementEntityInstance, categoryEntityInstance, entertainmentEntityInstance, entertainmentcategoryEntityInstance, pageEntityInstance, productCategoryEntityInstance, roomControlAreaEntityInstance, roomControlSectionEntityInstance, siteEntityInstance, uITabEntityInstance);
		}


		/// <summary> Retrieves Entity rows in a datatable which match the specified filter. It will always create a new connection to the database.</summary>
		/// <param name="selectFilter">A predicate or predicate expression which should be used as filter for the entities to retrieve.</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>DataTable with the rows requested.</returns>
		public static DataTable GetMultiAsDataTable(IPredicate selectFilter, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiAsDataTable(selectFilter, maxNumberOfItemsToReturn, sortClauses, null, 0, 0);
		}

		/// <summary> Retrieves Entity rows in a datatable which match the specified filter. It will always create a new connection to the database.</summary>
		/// <param name="selectFilter">A predicate or predicate expression which should be used as filter for the entities to retrieve.</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="relations">The set of relations to walk to construct to total query.</param>
		/// <returns>DataTable with the rows requested.</returns>
		public static DataTable GetMultiAsDataTable(IPredicate selectFilter, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IRelationCollection relations)
		{
			return GetMultiAsDataTable(selectFilter, maxNumberOfItemsToReturn, sortClauses, relations, 0, 0);
		}
		
		/// <summary> Retrieves Entity rows in a datatable which match the specified filter. It will always create a new connection to the database.</summary>
		/// <param name="selectFilter">A predicate or predicate expression which should be used as filter for the entities to retrieve.</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="relations">The set of relations to walk to construct to total query.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>DataTable with the rows requested.</returns>
		public static DataTable GetMultiAsDataTable(IPredicate selectFilter, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IRelationCollection relations, int pageNumber, int pageSize)
		{
			UIWidgetDAO dao = DAOFactory.CreateUIWidgetDAO();
			return dao.GetMultiAsDataTable(maxNumberOfItemsToReturn, sortClauses, selectFilter, relations, pageNumber, pageSize);
		}


		
		/// <summary> Gets a scalar value, calculated with the aggregate. the field index specified is the field the aggregate are applied on.</summary>
		/// <param name="fieldIndex">Field index of field to which to apply the aggregate function and expression</param>
		/// <param name="aggregateToApply">Aggregate function to apply. </param>
		/// <returns>the scalar value requested</returns>
		public object GetScalar(UIWidgetFieldIndex fieldIndex, AggregateFunction aggregateToApply)
		{
			return GetScalar(fieldIndex, null, aggregateToApply, null, null, null);
		}

		/// <summary> Gets a scalar value, calculated with the aggregate and expression specified. the field index specified is the field the expression and aggregate are applied on.</summary>
		/// <param name="fieldIndex">Field index of field to which to apply the aggregate function and expression</param>
		/// <param name="expressionToExecute">The expression to execute. Can be null</param>
		/// <param name="aggregateToApply">Aggregate function to apply. </param>
		/// <returns>the scalar value requested</returns>
		public object GetScalar(UIWidgetFieldIndex fieldIndex, IExpression expressionToExecute, AggregateFunction aggregateToApply)
		{
			return GetScalar(fieldIndex, expressionToExecute, aggregateToApply, null, null, null);
		}

		/// <summary> Gets a scalar value, calculated with the aggregate and expression specified. the field index specified is the field the expression and aggregate are applied on.</summary>
		/// <param name="fieldIndex">Field index of field to which to apply the aggregate function and expression</param>
		/// <param name="expressionToExecute">The expression to execute. Can be null</param>
		/// <param name="aggregateToApply">Aggregate function to apply. </param>
		/// <param name="filter">The filter to apply to retrieve the scalar</param>
		/// <returns>the scalar value requested</returns>
		public object GetScalar(UIWidgetFieldIndex fieldIndex, IExpression expressionToExecute, AggregateFunction aggregateToApply, IPredicate filter)
		{
			return GetScalar(fieldIndex, expressionToExecute, aggregateToApply, filter, null, null);
		}

		/// <summary> Gets a scalar value, calculated with the aggregate and expression specified. the field index specified is the field the expression and aggregate are applied on.</summary>
		/// <param name="fieldIndex">Field index of field to which to apply the aggregate function and expression</param>
		/// <param name="expressionToExecute">The expression to execute. Can be null</param>
		/// <param name="aggregateToApply">Aggregate function to apply. </param>
		/// <param name="filter">The filter to apply to retrieve the scalar</param>
		/// <param name="groupByClause">The groupby clause to apply to retrieve the scalar</param>
		/// <returns>the scalar value requested</returns>
		public object GetScalar(UIWidgetFieldIndex fieldIndex, IExpression expressionToExecute, AggregateFunction aggregateToApply, IPredicate filter, IGroupByCollection groupByClause)
		{
			return GetScalar(fieldIndex, expressionToExecute, aggregateToApply, filter, null, groupByClause);
		}

		/// <summary> Gets a scalar value, calculated with the aggregate and expression specified. the field index specified is the field the expression and aggregate are applied on.</summary>
		/// <param name="fieldIndex">Field index of field to which to apply the aggregate function and expression</param>
		/// <param name="expressionToExecute">The expression to execute. Can be null</param>
		/// <param name="aggregateToApply">Aggregate function to apply. </param>
		/// <param name="filter">The filter to apply to retrieve the scalar</param>
		/// <param name="relations">The relations to walk</param>
		/// <param name="groupByClause">The groupby clause to apply to retrieve the scalar</param>
		/// <returns>the scalar value requested</returns>
		public virtual object GetScalar(UIWidgetFieldIndex fieldIndex, IExpression expressionToExecute, AggregateFunction aggregateToApply, IPredicate filter, IRelationCollection relations, IGroupByCollection groupByClause)
		{
			EntityFields fields = new EntityFields(1);
			fields[0] = EntityFieldFactory.Create(fieldIndex);
			if((fields[0].ExpressionToApply == null) || (expressionToExecute != null))
			{
				fields[0].ExpressionToApply = expressionToExecute;
			}
			if((fields[0].AggregateFunctionToApply == AggregateFunction.None) || (aggregateToApply != AggregateFunction.None))
			{
				fields[0].AggregateFunctionToApply = aggregateToApply;
			}
			return DAOFactory.CreateUIWidgetDAO().GetScalar(fields, this.Transaction, filter, relations, groupByClause);
		}
		
		/// <summary>Creats a new DAO instance so code which is in the base class can still use the proper DAO object.</summary>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateUIWidgetDAO();
		}
		
		/// <summary>Creates a new transaction object</summary>
		/// <param name="levelOfIsolation">The level of isolation.</param>
		/// <param name="name">The name.</param>
		protected override ITransaction CreateTransaction( IsolationLevel levelOfIsolation, string name )
		{
			return new Transaction(levelOfIsolation, name);
		}

		#region Custom EntityCollection code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCollectionCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion
		
		#region Included Code

		#endregion
	}
}
