﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Data;
using System.Collections.Generic;
using System.ComponentModel;
using System.Xml;
#if !CF
using System.Runtime.Serialization;
#endif

using Obymobi.Data.EntityClasses;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.CollectionClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Collection class for storing and retrieving collections of RouteEntity objects. </summary>
	[Serializable]
	public partial class RouteCollection : CommonEntityCollection<RouteEntity>
	{
		/// <summary> CTor</summary>
		public RouteCollection():base(new RouteEntityFactory())
		{
		}

		/// <summary> CTor</summary>
		/// <param name="initialContents">The initial contents of this collection.</param>
		public RouteCollection(IEnumerable<RouteEntity> initialContents):base(new RouteEntityFactory())
		{
			AddRange(initialContents);
		}

		/// <summary> CTor</summary>
		/// <param name="entityFactoryToUse">The EntityFactory to use when creating entity objects during a GetMulti() call.</param>
		public RouteCollection(IEntityFactory entityFactoryToUse):base(entityFactoryToUse)
		{
		}

		/// <summary> Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected RouteCollection(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}


		/// <summary> Retrieves in this RouteCollection object all RouteEntity objects which have data in common with the specified related Entities.
		/// If one is omitted, that entity is not used as a filter. All current elements in the collection are removed from the collection.</summary>
		/// <param name="companyEntityInstance">CompanyEntity instance to use as a filter for the RouteEntity objects to return</param>
		/// <param name="escalationRouteEntityInstance">RouteEntity instance to use as a filter for the RouteEntity objects to return</param>
		/// <returns>true if succeeded, false otherwise</returns>
		public bool GetMultiManyToOne(IEntity companyEntityInstance, IEntity escalationRouteEntityInstance)
		{
			return GetMultiManyToOne(companyEntityInstance, escalationRouteEntityInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, null, 0, 0);
		}

		/// <summary> Retrieves in this RouteCollection object all RouteEntity objects which have data in common with the specified related Entities.
		/// If one is omitted, that entity is not used as a filter. All current elements in the collection are removed from the collection.</summary>
		/// <param name="companyEntityInstance">CompanyEntity instance to use as a filter for the RouteEntity objects to return</param>
		/// <param name="escalationRouteEntityInstance">RouteEntity instance to use as a filter for the RouteEntity objects to return</param>
		/// <param name="filter">Extra filter to limit the resultset. Predicate expression can be null, in which case it will be ignored.</param>
		/// <returns>true if succeeded, false otherwise</returns>
		public bool GetMultiManyToOne(IEntity companyEntityInstance, IEntity escalationRouteEntityInstance, IPredicateExpression filter)
		{
			return GetMultiManyToOne(companyEntityInstance, escalationRouteEntityInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, filter, 0, 0);
		}

		/// <summary> Retrieves in this RouteCollection object all RouteEntity objects which have data in common with the specified related Entities.
		/// If one is omitted, that entity is not used as a filter. All current elements in the collection are removed from the collection.</summary>
		/// <param name="companyEntityInstance">CompanyEntity instance to use as a filter for the RouteEntity objects to return</param>
		/// <param name="escalationRouteEntityInstance">RouteEntity instance to use as a filter for the RouteEntity objects to return</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="filter">Extra filter to limit the resultset. Predicate expression can be null, in which case it will be ignored.</param>
		/// <returns>true if succeeded, false otherwise</returns>
		public bool GetMultiManyToOne(IEntity companyEntityInstance, IEntity escalationRouteEntityInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPredicateExpression filter)
		{
			return GetMultiManyToOne(companyEntityInstance, escalationRouteEntityInstance, maxNumberOfItemsToReturn, sortClauses, filter, 0, 0);
		}

		/// <summary> Retrieves in this RouteCollection object all RouteEntity objects which have data in common with the specified related Entities.
		/// If one is omitted, that entity is not used as a filter. All current elements in the collection are removed from the collection.</summary>
		/// <param name="companyEntityInstance">CompanyEntity instance to use as a filter for the RouteEntity objects to return</param>
		/// <param name="escalationRouteEntityInstance">RouteEntity instance to use as a filter for the RouteEntity objects to return</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="filter">Extra filter to limit the resultset. Predicate expression can be null, in which case it will be ignored.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToOne(IEntity companyEntityInstance, IEntity escalationRouteEntityInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPredicateExpression filter, int pageNumber, int pageSize)
		{
			bool validParameters = false;
			validParameters |= (companyEntityInstance!=null);
			validParameters |= (escalationRouteEntityInstance!=null);
			if(!validParameters)
			{
				return GetMulti(filter, maxNumberOfItemsToReturn, sortClauses, null, pageNumber, pageSize);
			}
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateRouteDAO().GetMulti(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, filter, companyEntityInstance, escalationRouteEntityInstance, pageNumber, pageSize);
		}

		/// <summary> Deletes from the persistent storage all Route entities which have data in common with the specified related Entities. If one is omitted, that entity is not used as a filter.</summary>
		/// <remarks>Runs directly on the persistent storage. It will not delete entity objects from the current collection.</remarks>
		/// <param name="companyEntityInstance">CompanyEntity instance to use as a filter for the RouteEntity objects to return</param>
		/// <param name="escalationRouteEntityInstance">RouteEntity instance to use as a filter for the RouteEntity objects to return</param>
		/// <returns>Amount of entities affected, if the used persistent storage has rowcounting enabled.</returns>
		public int DeleteMultiManyToOne(IEntity companyEntityInstance, IEntity escalationRouteEntityInstance)
		{
			return DAOFactory.CreateRouteDAO().DeleteMulti(this.Transaction, companyEntityInstance, escalationRouteEntityInstance);
		}

		/// <summary> Updates in the persistent storage all Route entities which have data in common with the specified related Entities. If one is omitted, that entity is not used as a filter.
		/// Which fields are updated in those matching entities depends on which fields are <i>changed</i> in the passed in entity entityWithNewValues. The new values of these fields are read from entityWithNewValues. </summary>
		/// <param name="entityWithNewValues">RouteEntity instance which holds the new values for the matching entities to update. Only changed fields are taken into account</param>
		/// <param name="companyEntityInstance">CompanyEntity instance to use as a filter for the RouteEntity objects to return</param>
		/// <param name="escalationRouteEntityInstance">RouteEntity instance to use as a filter for the RouteEntity objects to return</param>
		/// <returns>Amount of entities affected, if the used persistent storage has rowcounting enabled.</returns>
		public int UpdateMultiManyToOne(RouteEntity entityWithNewValues, IEntity companyEntityInstance, IEntity escalationRouteEntityInstance)
		{
			return DAOFactory.CreateRouteDAO().UpdateMulti(entityWithNewValues, this.Transaction, companyEntityInstance, escalationRouteEntityInstance);
		}

		/// <summary> Retrieves in this RouteCollection object all RouteEntity objects which are related via a  Relation of type 'm:n' with the passed in AnnouncementEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="announcementInstance">AnnouncementEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingAnnouncementCollectionViaDeliverypointgroup(IEntity announcementInstance)
		{
			return GetMultiManyToManyUsingAnnouncementCollectionViaDeliverypointgroup(announcementInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this RouteCollection object all RouteEntity objects which are related via a  relation of type 'm:n' with the passed in AnnouncementEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="announcementInstance">AnnouncementEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingAnnouncementCollectionViaDeliverypointgroup(IEntity announcementInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingAnnouncementCollectionViaDeliverypointgroup(announcementInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this RouteCollection object all RouteEntity objects which are related via a Relation of type 'm:n' with the passed in AnnouncementEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="announcementInstance">AnnouncementEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingAnnouncementCollectionViaDeliverypointgroup(IEntity announcementInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingAnnouncementCollectionViaDeliverypointgroup(announcementInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this RouteCollection object all RouteEntity objects which are related via a  relation of type 'm:n' with the passed in AnnouncementEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="announcementInstance">AnnouncementEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingAnnouncementCollectionViaDeliverypointgroup(IEntity announcementInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateRouteDAO().GetMultiUsingAnnouncementCollectionViaDeliverypointgroup(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, announcementInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this RouteCollection object all RouteEntity objects which are related via a  relation of type 'm:n' with the passed in AnnouncementEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="announcementInstance">AnnouncementEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingAnnouncementCollectionViaDeliverypointgroup(IEntity announcementInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateRouteDAO().GetMultiUsingAnnouncementCollectionViaDeliverypointgroup(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, announcementInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this RouteCollection object all RouteEntity objects which are related via a  Relation of type 'm:n' with the passed in AnnouncementEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="announcementInstance">AnnouncementEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingAnnouncementCollectionViaDeliverypointgroup_(IEntity announcementInstance)
		{
			return GetMultiManyToManyUsingAnnouncementCollectionViaDeliverypointgroup_(announcementInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this RouteCollection object all RouteEntity objects which are related via a  relation of type 'm:n' with the passed in AnnouncementEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="announcementInstance">AnnouncementEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingAnnouncementCollectionViaDeliverypointgroup_(IEntity announcementInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingAnnouncementCollectionViaDeliverypointgroup_(announcementInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this RouteCollection object all RouteEntity objects which are related via a Relation of type 'm:n' with the passed in AnnouncementEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="announcementInstance">AnnouncementEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingAnnouncementCollectionViaDeliverypointgroup_(IEntity announcementInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingAnnouncementCollectionViaDeliverypointgroup_(announcementInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this RouteCollection object all RouteEntity objects which are related via a  relation of type 'm:n' with the passed in AnnouncementEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="announcementInstance">AnnouncementEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingAnnouncementCollectionViaDeliverypointgroup_(IEntity announcementInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateRouteDAO().GetMultiUsingAnnouncementCollectionViaDeliverypointgroup_(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, announcementInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this RouteCollection object all RouteEntity objects which are related via a  relation of type 'm:n' with the passed in AnnouncementEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="announcementInstance">AnnouncementEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingAnnouncementCollectionViaDeliverypointgroup_(IEntity announcementInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateRouteDAO().GetMultiUsingAnnouncementCollectionViaDeliverypointgroup_(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, announcementInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this RouteCollection object all RouteEntity objects which are related via a  Relation of type 'm:n' with the passed in CategoryEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="categoryInstance">CategoryEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCategoryCollectionViaCategory(IEntity categoryInstance)
		{
			return GetMultiManyToManyUsingCategoryCollectionViaCategory(categoryInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this RouteCollection object all RouteEntity objects which are related via a  relation of type 'm:n' with the passed in CategoryEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="categoryInstance">CategoryEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCategoryCollectionViaCategory(IEntity categoryInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingCategoryCollectionViaCategory(categoryInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this RouteCollection object all RouteEntity objects which are related via a Relation of type 'm:n' with the passed in CategoryEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="categoryInstance">CategoryEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCategoryCollectionViaCategory(IEntity categoryInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingCategoryCollectionViaCategory(categoryInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this RouteCollection object all RouteEntity objects which are related via a  relation of type 'm:n' with the passed in CategoryEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="categoryInstance">CategoryEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingCategoryCollectionViaCategory(IEntity categoryInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateRouteDAO().GetMultiUsingCategoryCollectionViaCategory(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, categoryInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this RouteCollection object all RouteEntity objects which are related via a  relation of type 'm:n' with the passed in CategoryEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="categoryInstance">CategoryEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCategoryCollectionViaCategory(IEntity categoryInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateRouteDAO().GetMultiUsingCategoryCollectionViaCategory(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, categoryInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this RouteCollection object all RouteEntity objects which are related via a  Relation of type 'm:n' with the passed in CompanyEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="companyInstance">CompanyEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCompanyCollectionViaCategory(IEntity companyInstance)
		{
			return GetMultiManyToManyUsingCompanyCollectionViaCategory(companyInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this RouteCollection object all RouteEntity objects which are related via a  relation of type 'm:n' with the passed in CompanyEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="companyInstance">CompanyEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCompanyCollectionViaCategory(IEntity companyInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingCompanyCollectionViaCategory(companyInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this RouteCollection object all RouteEntity objects which are related via a Relation of type 'm:n' with the passed in CompanyEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="companyInstance">CompanyEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCompanyCollectionViaCategory(IEntity companyInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingCompanyCollectionViaCategory(companyInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this RouteCollection object all RouteEntity objects which are related via a  relation of type 'm:n' with the passed in CompanyEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="companyInstance">CompanyEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingCompanyCollectionViaCategory(IEntity companyInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateRouteDAO().GetMultiUsingCompanyCollectionViaCategory(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, companyInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this RouteCollection object all RouteEntity objects which are related via a  relation of type 'm:n' with the passed in CompanyEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="companyInstance">CompanyEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCompanyCollectionViaCategory(IEntity companyInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateRouteDAO().GetMultiUsingCompanyCollectionViaCategory(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, companyInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this RouteCollection object all RouteEntity objects which are related via a  Relation of type 'm:n' with the passed in CompanyEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="companyInstance">CompanyEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCompanyCollectionViaDeliverypointgroup(IEntity companyInstance)
		{
			return GetMultiManyToManyUsingCompanyCollectionViaDeliverypointgroup(companyInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this RouteCollection object all RouteEntity objects which are related via a  relation of type 'm:n' with the passed in CompanyEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="companyInstance">CompanyEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCompanyCollectionViaDeliverypointgroup(IEntity companyInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingCompanyCollectionViaDeliverypointgroup(companyInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this RouteCollection object all RouteEntity objects which are related via a Relation of type 'm:n' with the passed in CompanyEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="companyInstance">CompanyEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCompanyCollectionViaDeliverypointgroup(IEntity companyInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingCompanyCollectionViaDeliverypointgroup(companyInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this RouteCollection object all RouteEntity objects which are related via a  relation of type 'm:n' with the passed in CompanyEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="companyInstance">CompanyEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingCompanyCollectionViaDeliverypointgroup(IEntity companyInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateRouteDAO().GetMultiUsingCompanyCollectionViaDeliverypointgroup(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, companyInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this RouteCollection object all RouteEntity objects which are related via a  relation of type 'm:n' with the passed in CompanyEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="companyInstance">CompanyEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCompanyCollectionViaDeliverypointgroup(IEntity companyInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateRouteDAO().GetMultiUsingCompanyCollectionViaDeliverypointgroup(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, companyInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this RouteCollection object all RouteEntity objects which are related via a  Relation of type 'm:n' with the passed in CompanyEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="companyInstance">CompanyEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCompanyCollectionViaDeliverypointgroup_(IEntity companyInstance)
		{
			return GetMultiManyToManyUsingCompanyCollectionViaDeliverypointgroup_(companyInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this RouteCollection object all RouteEntity objects which are related via a  relation of type 'm:n' with the passed in CompanyEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="companyInstance">CompanyEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCompanyCollectionViaDeliverypointgroup_(IEntity companyInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingCompanyCollectionViaDeliverypointgroup_(companyInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this RouteCollection object all RouteEntity objects which are related via a Relation of type 'm:n' with the passed in CompanyEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="companyInstance">CompanyEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCompanyCollectionViaDeliverypointgroup_(IEntity companyInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingCompanyCollectionViaDeliverypointgroup_(companyInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this RouteCollection object all RouteEntity objects which are related via a  relation of type 'm:n' with the passed in CompanyEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="companyInstance">CompanyEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingCompanyCollectionViaDeliverypointgroup_(IEntity companyInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateRouteDAO().GetMultiUsingCompanyCollectionViaDeliverypointgroup_(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, companyInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this RouteCollection object all RouteEntity objects which are related via a  relation of type 'm:n' with the passed in CompanyEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="companyInstance">CompanyEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCompanyCollectionViaDeliverypointgroup_(IEntity companyInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateRouteDAO().GetMultiUsingCompanyCollectionViaDeliverypointgroup_(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, companyInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this RouteCollection object all RouteEntity objects which are related via a  Relation of type 'm:n' with the passed in CompanyEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="companyInstance">CompanyEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCompanyCollectionViaProduct(IEntity companyInstance)
		{
			return GetMultiManyToManyUsingCompanyCollectionViaProduct(companyInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this RouteCollection object all RouteEntity objects which are related via a  relation of type 'm:n' with the passed in CompanyEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="companyInstance">CompanyEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCompanyCollectionViaProduct(IEntity companyInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingCompanyCollectionViaProduct(companyInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this RouteCollection object all RouteEntity objects which are related via a Relation of type 'm:n' with the passed in CompanyEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="companyInstance">CompanyEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCompanyCollectionViaProduct(IEntity companyInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingCompanyCollectionViaProduct(companyInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this RouteCollection object all RouteEntity objects which are related via a  relation of type 'm:n' with the passed in CompanyEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="companyInstance">CompanyEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingCompanyCollectionViaProduct(IEntity companyInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateRouteDAO().GetMultiUsingCompanyCollectionViaProduct(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, companyInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this RouteCollection object all RouteEntity objects which are related via a  relation of type 'm:n' with the passed in CompanyEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="companyInstance">CompanyEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCompanyCollectionViaProduct(IEntity companyInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateRouteDAO().GetMultiUsingCompanyCollectionViaProduct(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, companyInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this RouteCollection object all RouteEntity objects which are related via a  Relation of type 'm:n' with the passed in CompanyEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="companyInstance">CompanyEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCompanyCollectionViaRoute(IEntity companyInstance)
		{
			return GetMultiManyToManyUsingCompanyCollectionViaRoute(companyInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this RouteCollection object all RouteEntity objects which are related via a  relation of type 'm:n' with the passed in CompanyEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="companyInstance">CompanyEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCompanyCollectionViaRoute(IEntity companyInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingCompanyCollectionViaRoute(companyInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this RouteCollection object all RouteEntity objects which are related via a Relation of type 'm:n' with the passed in CompanyEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="companyInstance">CompanyEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCompanyCollectionViaRoute(IEntity companyInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingCompanyCollectionViaRoute(companyInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this RouteCollection object all RouteEntity objects which are related via a  relation of type 'm:n' with the passed in CompanyEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="companyInstance">CompanyEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingCompanyCollectionViaRoute(IEntity companyInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateRouteDAO().GetMultiUsingCompanyCollectionViaRoute(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, companyInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this RouteCollection object all RouteEntity objects which are related via a  relation of type 'm:n' with the passed in CompanyEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="companyInstance">CompanyEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCompanyCollectionViaRoute(IEntity companyInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateRouteDAO().GetMultiUsingCompanyCollectionViaRoute(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, companyInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this RouteCollection object all RouteEntity objects which are related via a  Relation of type 'm:n' with the passed in CompanyOwnerEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="companyOwnerInstance">CompanyOwnerEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCompanyOwnerCollectionViaCompany(IEntity companyOwnerInstance)
		{
			return GetMultiManyToManyUsingCompanyOwnerCollectionViaCompany(companyOwnerInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this RouteCollection object all RouteEntity objects which are related via a  relation of type 'm:n' with the passed in CompanyOwnerEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="companyOwnerInstance">CompanyOwnerEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCompanyOwnerCollectionViaCompany(IEntity companyOwnerInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingCompanyOwnerCollectionViaCompany(companyOwnerInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this RouteCollection object all RouteEntity objects which are related via a Relation of type 'm:n' with the passed in CompanyOwnerEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="companyOwnerInstance">CompanyOwnerEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCompanyOwnerCollectionViaCompany(IEntity companyOwnerInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingCompanyOwnerCollectionViaCompany(companyOwnerInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this RouteCollection object all RouteEntity objects which are related via a  relation of type 'm:n' with the passed in CompanyOwnerEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="companyOwnerInstance">CompanyOwnerEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingCompanyOwnerCollectionViaCompany(IEntity companyOwnerInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateRouteDAO().GetMultiUsingCompanyOwnerCollectionViaCompany(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, companyOwnerInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this RouteCollection object all RouteEntity objects which are related via a  relation of type 'm:n' with the passed in CompanyOwnerEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="companyOwnerInstance">CompanyOwnerEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCompanyOwnerCollectionViaCompany(IEntity companyOwnerInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateRouteDAO().GetMultiUsingCompanyOwnerCollectionViaCompany(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, companyOwnerInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this RouteCollection object all RouteEntity objects which are related via a  Relation of type 'm:n' with the passed in CountryEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="countryInstance">CountryEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCountryCollectionViaCompany(IEntity countryInstance)
		{
			return GetMultiManyToManyUsingCountryCollectionViaCompany(countryInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this RouteCollection object all RouteEntity objects which are related via a  relation of type 'm:n' with the passed in CountryEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="countryInstance">CountryEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCountryCollectionViaCompany(IEntity countryInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingCountryCollectionViaCompany(countryInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this RouteCollection object all RouteEntity objects which are related via a Relation of type 'm:n' with the passed in CountryEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="countryInstance">CountryEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCountryCollectionViaCompany(IEntity countryInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingCountryCollectionViaCompany(countryInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this RouteCollection object all RouteEntity objects which are related via a  relation of type 'm:n' with the passed in CountryEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="countryInstance">CountryEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingCountryCollectionViaCompany(IEntity countryInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateRouteDAO().GetMultiUsingCountryCollectionViaCompany(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, countryInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this RouteCollection object all RouteEntity objects which are related via a  relation of type 'm:n' with the passed in CountryEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="countryInstance">CountryEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCountryCollectionViaCompany(IEntity countryInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateRouteDAO().GetMultiUsingCountryCollectionViaCompany(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, countryInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this RouteCollection object all RouteEntity objects which are related via a  Relation of type 'm:n' with the passed in CurrencyEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="currencyInstance">CurrencyEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCurrencyCollectionViaCompany(IEntity currencyInstance)
		{
			return GetMultiManyToManyUsingCurrencyCollectionViaCompany(currencyInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this RouteCollection object all RouteEntity objects which are related via a  relation of type 'm:n' with the passed in CurrencyEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="currencyInstance">CurrencyEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCurrencyCollectionViaCompany(IEntity currencyInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingCurrencyCollectionViaCompany(currencyInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this RouteCollection object all RouteEntity objects which are related via a Relation of type 'm:n' with the passed in CurrencyEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="currencyInstance">CurrencyEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCurrencyCollectionViaCompany(IEntity currencyInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingCurrencyCollectionViaCompany(currencyInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this RouteCollection object all RouteEntity objects which are related via a  relation of type 'm:n' with the passed in CurrencyEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="currencyInstance">CurrencyEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingCurrencyCollectionViaCompany(IEntity currencyInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateRouteDAO().GetMultiUsingCurrencyCollectionViaCompany(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, currencyInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this RouteCollection object all RouteEntity objects which are related via a  relation of type 'm:n' with the passed in CurrencyEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="currencyInstance">CurrencyEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCurrencyCollectionViaCompany(IEntity currencyInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateRouteDAO().GetMultiUsingCurrencyCollectionViaCompany(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, currencyInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this RouteCollection object all RouteEntity objects which are related via a  Relation of type 'm:n' with the passed in GenericcategoryEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="genericcategoryInstance">GenericcategoryEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingGenericcategoryCollectionViaCategory(IEntity genericcategoryInstance)
		{
			return GetMultiManyToManyUsingGenericcategoryCollectionViaCategory(genericcategoryInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this RouteCollection object all RouteEntity objects which are related via a  relation of type 'm:n' with the passed in GenericcategoryEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="genericcategoryInstance">GenericcategoryEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingGenericcategoryCollectionViaCategory(IEntity genericcategoryInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingGenericcategoryCollectionViaCategory(genericcategoryInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this RouteCollection object all RouteEntity objects which are related via a Relation of type 'm:n' with the passed in GenericcategoryEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="genericcategoryInstance">GenericcategoryEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingGenericcategoryCollectionViaCategory(IEntity genericcategoryInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingGenericcategoryCollectionViaCategory(genericcategoryInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this RouteCollection object all RouteEntity objects which are related via a  relation of type 'm:n' with the passed in GenericcategoryEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="genericcategoryInstance">GenericcategoryEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingGenericcategoryCollectionViaCategory(IEntity genericcategoryInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateRouteDAO().GetMultiUsingGenericcategoryCollectionViaCategory(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, genericcategoryInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this RouteCollection object all RouteEntity objects which are related via a  relation of type 'm:n' with the passed in GenericcategoryEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="genericcategoryInstance">GenericcategoryEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingGenericcategoryCollectionViaCategory(IEntity genericcategoryInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateRouteDAO().GetMultiUsingGenericcategoryCollectionViaCategory(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, genericcategoryInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this RouteCollection object all RouteEntity objects which are related via a  Relation of type 'm:n' with the passed in GenericproductEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="genericproductInstance">GenericproductEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingGenericproductCollectionViaProduct(IEntity genericproductInstance)
		{
			return GetMultiManyToManyUsingGenericproductCollectionViaProduct(genericproductInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this RouteCollection object all RouteEntity objects which are related via a  relation of type 'm:n' with the passed in GenericproductEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="genericproductInstance">GenericproductEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingGenericproductCollectionViaProduct(IEntity genericproductInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingGenericproductCollectionViaProduct(genericproductInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this RouteCollection object all RouteEntity objects which are related via a Relation of type 'm:n' with the passed in GenericproductEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="genericproductInstance">GenericproductEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingGenericproductCollectionViaProduct(IEntity genericproductInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingGenericproductCollectionViaProduct(genericproductInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this RouteCollection object all RouteEntity objects which are related via a  relation of type 'm:n' with the passed in GenericproductEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="genericproductInstance">GenericproductEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingGenericproductCollectionViaProduct(IEntity genericproductInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateRouteDAO().GetMultiUsingGenericproductCollectionViaProduct(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, genericproductInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this RouteCollection object all RouteEntity objects which are related via a  relation of type 'm:n' with the passed in GenericproductEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="genericproductInstance">GenericproductEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingGenericproductCollectionViaProduct(IEntity genericproductInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateRouteDAO().GetMultiUsingGenericproductCollectionViaProduct(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, genericproductInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this RouteCollection object all RouteEntity objects which are related via a  Relation of type 'm:n' with the passed in LanguageEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="languageInstance">LanguageEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingLanguageCollectionViaCompany(IEntity languageInstance)
		{
			return GetMultiManyToManyUsingLanguageCollectionViaCompany(languageInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this RouteCollection object all RouteEntity objects which are related via a  relation of type 'm:n' with the passed in LanguageEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="languageInstance">LanguageEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingLanguageCollectionViaCompany(IEntity languageInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingLanguageCollectionViaCompany(languageInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this RouteCollection object all RouteEntity objects which are related via a Relation of type 'm:n' with the passed in LanguageEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="languageInstance">LanguageEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingLanguageCollectionViaCompany(IEntity languageInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingLanguageCollectionViaCompany(languageInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this RouteCollection object all RouteEntity objects which are related via a  relation of type 'm:n' with the passed in LanguageEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="languageInstance">LanguageEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingLanguageCollectionViaCompany(IEntity languageInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateRouteDAO().GetMultiUsingLanguageCollectionViaCompany(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, languageInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this RouteCollection object all RouteEntity objects which are related via a  relation of type 'm:n' with the passed in LanguageEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="languageInstance">LanguageEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingLanguageCollectionViaCompany(IEntity languageInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateRouteDAO().GetMultiUsingLanguageCollectionViaCompany(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, languageInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this RouteCollection object all RouteEntity objects which are related via a  Relation of type 'm:n' with the passed in MenuEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="menuInstance">MenuEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingMenuCollectionViaCategory(IEntity menuInstance)
		{
			return GetMultiManyToManyUsingMenuCollectionViaCategory(menuInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this RouteCollection object all RouteEntity objects which are related via a  relation of type 'm:n' with the passed in MenuEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="menuInstance">MenuEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingMenuCollectionViaCategory(IEntity menuInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingMenuCollectionViaCategory(menuInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this RouteCollection object all RouteEntity objects which are related via a Relation of type 'm:n' with the passed in MenuEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="menuInstance">MenuEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingMenuCollectionViaCategory(IEntity menuInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingMenuCollectionViaCategory(menuInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this RouteCollection object all RouteEntity objects which are related via a  relation of type 'm:n' with the passed in MenuEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="menuInstance">MenuEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingMenuCollectionViaCategory(IEntity menuInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateRouteDAO().GetMultiUsingMenuCollectionViaCategory(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, menuInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this RouteCollection object all RouteEntity objects which are related via a  relation of type 'm:n' with the passed in MenuEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="menuInstance">MenuEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingMenuCollectionViaCategory(IEntity menuInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateRouteDAO().GetMultiUsingMenuCollectionViaCategory(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, menuInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this RouteCollection object all RouteEntity objects which are related via a  Relation of type 'm:n' with the passed in MenuEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="menuInstance">MenuEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingMenuCollectionViaDeliverypointgroup(IEntity menuInstance)
		{
			return GetMultiManyToManyUsingMenuCollectionViaDeliverypointgroup(menuInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this RouteCollection object all RouteEntity objects which are related via a  relation of type 'm:n' with the passed in MenuEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="menuInstance">MenuEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingMenuCollectionViaDeliverypointgroup(IEntity menuInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingMenuCollectionViaDeliverypointgroup(menuInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this RouteCollection object all RouteEntity objects which are related via a Relation of type 'm:n' with the passed in MenuEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="menuInstance">MenuEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingMenuCollectionViaDeliverypointgroup(IEntity menuInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingMenuCollectionViaDeliverypointgroup(menuInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this RouteCollection object all RouteEntity objects which are related via a  relation of type 'm:n' with the passed in MenuEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="menuInstance">MenuEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingMenuCollectionViaDeliverypointgroup(IEntity menuInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateRouteDAO().GetMultiUsingMenuCollectionViaDeliverypointgroup(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, menuInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this RouteCollection object all RouteEntity objects which are related via a  relation of type 'm:n' with the passed in MenuEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="menuInstance">MenuEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingMenuCollectionViaDeliverypointgroup(IEntity menuInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateRouteDAO().GetMultiUsingMenuCollectionViaDeliverypointgroup(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, menuInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this RouteCollection object all RouteEntity objects which are related via a  Relation of type 'm:n' with the passed in MenuEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="menuInstance">MenuEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingMenuCollectionViaDeliverypointgroup_(IEntity menuInstance)
		{
			return GetMultiManyToManyUsingMenuCollectionViaDeliverypointgroup_(menuInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this RouteCollection object all RouteEntity objects which are related via a  relation of type 'm:n' with the passed in MenuEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="menuInstance">MenuEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingMenuCollectionViaDeliverypointgroup_(IEntity menuInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingMenuCollectionViaDeliverypointgroup_(menuInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this RouteCollection object all RouteEntity objects which are related via a Relation of type 'm:n' with the passed in MenuEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="menuInstance">MenuEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingMenuCollectionViaDeliverypointgroup_(IEntity menuInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingMenuCollectionViaDeliverypointgroup_(menuInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this RouteCollection object all RouteEntity objects which are related via a  relation of type 'm:n' with the passed in MenuEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="menuInstance">MenuEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingMenuCollectionViaDeliverypointgroup_(IEntity menuInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateRouteDAO().GetMultiUsingMenuCollectionViaDeliverypointgroup_(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, menuInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this RouteCollection object all RouteEntity objects which are related via a  relation of type 'm:n' with the passed in MenuEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="menuInstance">MenuEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingMenuCollectionViaDeliverypointgroup_(IEntity menuInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateRouteDAO().GetMultiUsingMenuCollectionViaDeliverypointgroup_(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, menuInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this RouteCollection object all RouteEntity objects which are related via a  Relation of type 'm:n' with the passed in PoscategoryEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="poscategoryInstance">PoscategoryEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingPoscategoryCollectionViaCategory(IEntity poscategoryInstance)
		{
			return GetMultiManyToManyUsingPoscategoryCollectionViaCategory(poscategoryInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this RouteCollection object all RouteEntity objects which are related via a  relation of type 'm:n' with the passed in PoscategoryEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="poscategoryInstance">PoscategoryEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingPoscategoryCollectionViaCategory(IEntity poscategoryInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingPoscategoryCollectionViaCategory(poscategoryInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this RouteCollection object all RouteEntity objects which are related via a Relation of type 'm:n' with the passed in PoscategoryEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="poscategoryInstance">PoscategoryEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingPoscategoryCollectionViaCategory(IEntity poscategoryInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingPoscategoryCollectionViaCategory(poscategoryInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this RouteCollection object all RouteEntity objects which are related via a  relation of type 'm:n' with the passed in PoscategoryEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="poscategoryInstance">PoscategoryEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingPoscategoryCollectionViaCategory(IEntity poscategoryInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateRouteDAO().GetMultiUsingPoscategoryCollectionViaCategory(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, poscategoryInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this RouteCollection object all RouteEntity objects which are related via a  relation of type 'm:n' with the passed in PoscategoryEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="poscategoryInstance">PoscategoryEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingPoscategoryCollectionViaCategory(IEntity poscategoryInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateRouteDAO().GetMultiUsingPoscategoryCollectionViaCategory(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, poscategoryInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this RouteCollection object all RouteEntity objects which are related via a  Relation of type 'm:n' with the passed in PosdeliverypointgroupEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="posdeliverypointgroupInstance">PosdeliverypointgroupEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingPosdeliverypointgroupCollectionViaDeliverypointgroup(IEntity posdeliverypointgroupInstance)
		{
			return GetMultiManyToManyUsingPosdeliverypointgroupCollectionViaDeliverypointgroup(posdeliverypointgroupInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this RouteCollection object all RouteEntity objects which are related via a  relation of type 'm:n' with the passed in PosdeliverypointgroupEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="posdeliverypointgroupInstance">PosdeliverypointgroupEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingPosdeliverypointgroupCollectionViaDeliverypointgroup(IEntity posdeliverypointgroupInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingPosdeliverypointgroupCollectionViaDeliverypointgroup(posdeliverypointgroupInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this RouteCollection object all RouteEntity objects which are related via a Relation of type 'm:n' with the passed in PosdeliverypointgroupEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="posdeliverypointgroupInstance">PosdeliverypointgroupEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingPosdeliverypointgroupCollectionViaDeliverypointgroup(IEntity posdeliverypointgroupInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingPosdeliverypointgroupCollectionViaDeliverypointgroup(posdeliverypointgroupInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this RouteCollection object all RouteEntity objects which are related via a  relation of type 'm:n' with the passed in PosdeliverypointgroupEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="posdeliverypointgroupInstance">PosdeliverypointgroupEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingPosdeliverypointgroupCollectionViaDeliverypointgroup(IEntity posdeliverypointgroupInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateRouteDAO().GetMultiUsingPosdeliverypointgroupCollectionViaDeliverypointgroup(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, posdeliverypointgroupInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this RouteCollection object all RouteEntity objects which are related via a  relation of type 'm:n' with the passed in PosdeliverypointgroupEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="posdeliverypointgroupInstance">PosdeliverypointgroupEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingPosdeliverypointgroupCollectionViaDeliverypointgroup(IEntity posdeliverypointgroupInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateRouteDAO().GetMultiUsingPosdeliverypointgroupCollectionViaDeliverypointgroup(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, posdeliverypointgroupInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this RouteCollection object all RouteEntity objects which are related via a  Relation of type 'm:n' with the passed in PosdeliverypointgroupEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="posdeliverypointgroupInstance">PosdeliverypointgroupEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingPosdeliverypointgroupCollectionViaDeliverypointgroup_(IEntity posdeliverypointgroupInstance)
		{
			return GetMultiManyToManyUsingPosdeliverypointgroupCollectionViaDeliverypointgroup_(posdeliverypointgroupInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this RouteCollection object all RouteEntity objects which are related via a  relation of type 'm:n' with the passed in PosdeliverypointgroupEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="posdeliverypointgroupInstance">PosdeliverypointgroupEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingPosdeliverypointgroupCollectionViaDeliverypointgroup_(IEntity posdeliverypointgroupInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingPosdeliverypointgroupCollectionViaDeliverypointgroup_(posdeliverypointgroupInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this RouteCollection object all RouteEntity objects which are related via a Relation of type 'm:n' with the passed in PosdeliverypointgroupEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="posdeliverypointgroupInstance">PosdeliverypointgroupEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingPosdeliverypointgroupCollectionViaDeliverypointgroup_(IEntity posdeliverypointgroupInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingPosdeliverypointgroupCollectionViaDeliverypointgroup_(posdeliverypointgroupInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this RouteCollection object all RouteEntity objects which are related via a  relation of type 'm:n' with the passed in PosdeliverypointgroupEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="posdeliverypointgroupInstance">PosdeliverypointgroupEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingPosdeliverypointgroupCollectionViaDeliverypointgroup_(IEntity posdeliverypointgroupInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateRouteDAO().GetMultiUsingPosdeliverypointgroupCollectionViaDeliverypointgroup_(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, posdeliverypointgroupInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this RouteCollection object all RouteEntity objects which are related via a  relation of type 'm:n' with the passed in PosdeliverypointgroupEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="posdeliverypointgroupInstance">PosdeliverypointgroupEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingPosdeliverypointgroupCollectionViaDeliverypointgroup_(IEntity posdeliverypointgroupInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateRouteDAO().GetMultiUsingPosdeliverypointgroupCollectionViaDeliverypointgroup_(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, posdeliverypointgroupInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this RouteCollection object all RouteEntity objects which are related via a  Relation of type 'm:n' with the passed in PosproductEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="posproductInstance">PosproductEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingPosproductCollectionViaProduct(IEntity posproductInstance)
		{
			return GetMultiManyToManyUsingPosproductCollectionViaProduct(posproductInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this RouteCollection object all RouteEntity objects which are related via a  relation of type 'm:n' with the passed in PosproductEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="posproductInstance">PosproductEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingPosproductCollectionViaProduct(IEntity posproductInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingPosproductCollectionViaProduct(posproductInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this RouteCollection object all RouteEntity objects which are related via a Relation of type 'm:n' with the passed in PosproductEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="posproductInstance">PosproductEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingPosproductCollectionViaProduct(IEntity posproductInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingPosproductCollectionViaProduct(posproductInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this RouteCollection object all RouteEntity objects which are related via a  relation of type 'm:n' with the passed in PosproductEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="posproductInstance">PosproductEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingPosproductCollectionViaProduct(IEntity posproductInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateRouteDAO().GetMultiUsingPosproductCollectionViaProduct(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, posproductInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this RouteCollection object all RouteEntity objects which are related via a  relation of type 'm:n' with the passed in PosproductEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="posproductInstance">PosproductEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingPosproductCollectionViaProduct(IEntity posproductInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateRouteDAO().GetMultiUsingPosproductCollectionViaProduct(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, posproductInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this RouteCollection object all RouteEntity objects which are related via a  Relation of type 'm:n' with the passed in ProductEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="productInstance">ProductEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingProductCollectionViaCategory(IEntity productInstance)
		{
			return GetMultiManyToManyUsingProductCollectionViaCategory(productInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this RouteCollection object all RouteEntity objects which are related via a  relation of type 'm:n' with the passed in ProductEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="productInstance">ProductEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingProductCollectionViaCategory(IEntity productInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingProductCollectionViaCategory(productInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this RouteCollection object all RouteEntity objects which are related via a Relation of type 'm:n' with the passed in ProductEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="productInstance">ProductEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingProductCollectionViaCategory(IEntity productInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingProductCollectionViaCategory(productInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this RouteCollection object all RouteEntity objects which are related via a  relation of type 'm:n' with the passed in ProductEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="productInstance">ProductEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingProductCollectionViaCategory(IEntity productInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateRouteDAO().GetMultiUsingProductCollectionViaCategory(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, productInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this RouteCollection object all RouteEntity objects which are related via a  relation of type 'm:n' with the passed in ProductEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="productInstance">ProductEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingProductCollectionViaCategory(IEntity productInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateRouteDAO().GetMultiUsingProductCollectionViaCategory(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, productInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this RouteCollection object all RouteEntity objects which are related via a  Relation of type 'm:n' with the passed in RouteEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="routeInstance">RouteEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingRouteCollectionViaDeliverypointgroup(IEntity routeInstance)
		{
			return GetMultiManyToManyUsingRouteCollectionViaDeliverypointgroup(routeInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this RouteCollection object all RouteEntity objects which are related via a  relation of type 'm:n' with the passed in RouteEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="routeInstance">RouteEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingRouteCollectionViaDeliverypointgroup(IEntity routeInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingRouteCollectionViaDeliverypointgroup(routeInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this RouteCollection object all RouteEntity objects which are related via a Relation of type 'm:n' with the passed in RouteEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="routeInstance">RouteEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingRouteCollectionViaDeliverypointgroup(IEntity routeInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingRouteCollectionViaDeliverypointgroup(routeInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this RouteCollection object all RouteEntity objects which are related via a  relation of type 'm:n' with the passed in RouteEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="routeInstance">RouteEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingRouteCollectionViaDeliverypointgroup(IEntity routeInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateRouteDAO().GetMultiUsingRouteCollectionViaDeliverypointgroup(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, routeInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this RouteCollection object all RouteEntity objects which are related via a  relation of type 'm:n' with the passed in RouteEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="routeInstance">RouteEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingRouteCollectionViaDeliverypointgroup(IEntity routeInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateRouteDAO().GetMultiUsingRouteCollectionViaDeliverypointgroup(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, routeInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this RouteCollection object all RouteEntity objects which are related via a  Relation of type 'm:n' with the passed in RouteEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="routeInstance">RouteEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingRouteCollectionViaDeliverypointgroup_(IEntity routeInstance)
		{
			return GetMultiManyToManyUsingRouteCollectionViaDeliverypointgroup_(routeInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this RouteCollection object all RouteEntity objects which are related via a  relation of type 'm:n' with the passed in RouteEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="routeInstance">RouteEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingRouteCollectionViaDeliverypointgroup_(IEntity routeInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingRouteCollectionViaDeliverypointgroup_(routeInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this RouteCollection object all RouteEntity objects which are related via a Relation of type 'm:n' with the passed in RouteEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="routeInstance">RouteEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingRouteCollectionViaDeliverypointgroup_(IEntity routeInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingRouteCollectionViaDeliverypointgroup_(routeInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this RouteCollection object all RouteEntity objects which are related via a  relation of type 'm:n' with the passed in RouteEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="routeInstance">RouteEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingRouteCollectionViaDeliverypointgroup_(IEntity routeInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateRouteDAO().GetMultiUsingRouteCollectionViaDeliverypointgroup_(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, routeInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this RouteCollection object all RouteEntity objects which are related via a  relation of type 'm:n' with the passed in RouteEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="routeInstance">RouteEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingRouteCollectionViaDeliverypointgroup_(IEntity routeInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateRouteDAO().GetMultiUsingRouteCollectionViaDeliverypointgroup_(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, routeInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this RouteCollection object all RouteEntity objects which are related via a  Relation of type 'm:n' with the passed in SupportpoolEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="supportpoolInstance">SupportpoolEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingSupportpoolCollectionViaCompany(IEntity supportpoolInstance)
		{
			return GetMultiManyToManyUsingSupportpoolCollectionViaCompany(supportpoolInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this RouteCollection object all RouteEntity objects which are related via a  relation of type 'm:n' with the passed in SupportpoolEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="supportpoolInstance">SupportpoolEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingSupportpoolCollectionViaCompany(IEntity supportpoolInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingSupportpoolCollectionViaCompany(supportpoolInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this RouteCollection object all RouteEntity objects which are related via a Relation of type 'm:n' with the passed in SupportpoolEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="supportpoolInstance">SupportpoolEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingSupportpoolCollectionViaCompany(IEntity supportpoolInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingSupportpoolCollectionViaCompany(supportpoolInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this RouteCollection object all RouteEntity objects which are related via a  relation of type 'm:n' with the passed in SupportpoolEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="supportpoolInstance">SupportpoolEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingSupportpoolCollectionViaCompany(IEntity supportpoolInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateRouteDAO().GetMultiUsingSupportpoolCollectionViaCompany(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, supportpoolInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this RouteCollection object all RouteEntity objects which are related via a  relation of type 'm:n' with the passed in SupportpoolEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="supportpoolInstance">SupportpoolEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingSupportpoolCollectionViaCompany(IEntity supportpoolInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateRouteDAO().GetMultiUsingSupportpoolCollectionViaCompany(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, supportpoolInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this RouteCollection object all RouteEntity objects which are related via a  Relation of type 'm:n' with the passed in TerminalEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="terminalInstance">TerminalEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingTerminalCollectionViaDeliverypointgroup(IEntity terminalInstance)
		{
			return GetMultiManyToManyUsingTerminalCollectionViaDeliverypointgroup(terminalInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this RouteCollection object all RouteEntity objects which are related via a  relation of type 'm:n' with the passed in TerminalEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="terminalInstance">TerminalEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingTerminalCollectionViaDeliverypointgroup(IEntity terminalInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingTerminalCollectionViaDeliverypointgroup(terminalInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this RouteCollection object all RouteEntity objects which are related via a Relation of type 'm:n' with the passed in TerminalEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="terminalInstance">TerminalEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingTerminalCollectionViaDeliverypointgroup(IEntity terminalInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingTerminalCollectionViaDeliverypointgroup(terminalInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this RouteCollection object all RouteEntity objects which are related via a  relation of type 'm:n' with the passed in TerminalEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="terminalInstance">TerminalEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingTerminalCollectionViaDeliverypointgroup(IEntity terminalInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateRouteDAO().GetMultiUsingTerminalCollectionViaDeliverypointgroup(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, terminalInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this RouteCollection object all RouteEntity objects which are related via a  relation of type 'm:n' with the passed in TerminalEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="terminalInstance">TerminalEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingTerminalCollectionViaDeliverypointgroup(IEntity terminalInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateRouteDAO().GetMultiUsingTerminalCollectionViaDeliverypointgroup(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, terminalInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this RouteCollection object all RouteEntity objects which are related via a  Relation of type 'm:n' with the passed in TerminalEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="terminalInstance">TerminalEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingTerminalCollectionViaDeliverypointgroup_(IEntity terminalInstance)
		{
			return GetMultiManyToManyUsingTerminalCollectionViaDeliverypointgroup_(terminalInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this RouteCollection object all RouteEntity objects which are related via a  relation of type 'm:n' with the passed in TerminalEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="terminalInstance">TerminalEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingTerminalCollectionViaDeliverypointgroup_(IEntity terminalInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingTerminalCollectionViaDeliverypointgroup_(terminalInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this RouteCollection object all RouteEntity objects which are related via a Relation of type 'm:n' with the passed in TerminalEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="terminalInstance">TerminalEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingTerminalCollectionViaDeliverypointgroup_(IEntity terminalInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingTerminalCollectionViaDeliverypointgroup_(terminalInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this RouteCollection object all RouteEntity objects which are related via a  relation of type 'm:n' with the passed in TerminalEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="terminalInstance">TerminalEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingTerminalCollectionViaDeliverypointgroup_(IEntity terminalInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateRouteDAO().GetMultiUsingTerminalCollectionViaDeliverypointgroup_(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, terminalInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this RouteCollection object all RouteEntity objects which are related via a  relation of type 'm:n' with the passed in TerminalEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="terminalInstance">TerminalEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingTerminalCollectionViaDeliverypointgroup_(IEntity terminalInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateRouteDAO().GetMultiUsingTerminalCollectionViaDeliverypointgroup_(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, terminalInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this RouteCollection object all RouteEntity objects which are related via a  Relation of type 'm:n' with the passed in UIModeEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="uIModeInstance">UIModeEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingUIModeCollectionViaDeliverypointgroup__(IEntity uIModeInstance)
		{
			return GetMultiManyToManyUsingUIModeCollectionViaDeliverypointgroup__(uIModeInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this RouteCollection object all RouteEntity objects which are related via a  relation of type 'm:n' with the passed in UIModeEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="uIModeInstance">UIModeEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingUIModeCollectionViaDeliverypointgroup__(IEntity uIModeInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingUIModeCollectionViaDeliverypointgroup__(uIModeInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this RouteCollection object all RouteEntity objects which are related via a Relation of type 'm:n' with the passed in UIModeEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="uIModeInstance">UIModeEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingUIModeCollectionViaDeliverypointgroup__(IEntity uIModeInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingUIModeCollectionViaDeliverypointgroup__(uIModeInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this RouteCollection object all RouteEntity objects which are related via a  relation of type 'm:n' with the passed in UIModeEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="uIModeInstance">UIModeEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingUIModeCollectionViaDeliverypointgroup__(IEntity uIModeInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateRouteDAO().GetMultiUsingUIModeCollectionViaDeliverypointgroup__(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, uIModeInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this RouteCollection object all RouteEntity objects which are related via a  relation of type 'm:n' with the passed in UIModeEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="uIModeInstance">UIModeEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingUIModeCollectionViaDeliverypointgroup__(IEntity uIModeInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateRouteDAO().GetMultiUsingUIModeCollectionViaDeliverypointgroup__(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, uIModeInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this RouteCollection object all RouteEntity objects which are related via a  Relation of type 'm:n' with the passed in UIModeEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="uIModeInstance">UIModeEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingUIModeCollectionViaDeliverypointgroup___(IEntity uIModeInstance)
		{
			return GetMultiManyToManyUsingUIModeCollectionViaDeliverypointgroup___(uIModeInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this RouteCollection object all RouteEntity objects which are related via a  relation of type 'm:n' with the passed in UIModeEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="uIModeInstance">UIModeEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingUIModeCollectionViaDeliverypointgroup___(IEntity uIModeInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingUIModeCollectionViaDeliverypointgroup___(uIModeInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this RouteCollection object all RouteEntity objects which are related via a Relation of type 'm:n' with the passed in UIModeEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="uIModeInstance">UIModeEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingUIModeCollectionViaDeliverypointgroup___(IEntity uIModeInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingUIModeCollectionViaDeliverypointgroup___(uIModeInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this RouteCollection object all RouteEntity objects which are related via a  relation of type 'm:n' with the passed in UIModeEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="uIModeInstance">UIModeEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingUIModeCollectionViaDeliverypointgroup___(IEntity uIModeInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateRouteDAO().GetMultiUsingUIModeCollectionViaDeliverypointgroup___(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, uIModeInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this RouteCollection object all RouteEntity objects which are related via a  relation of type 'm:n' with the passed in UIModeEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="uIModeInstance">UIModeEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingUIModeCollectionViaDeliverypointgroup___(IEntity uIModeInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateRouteDAO().GetMultiUsingUIModeCollectionViaDeliverypointgroup___(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, uIModeInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this RouteCollection object all RouteEntity objects which are related via a  Relation of type 'm:n' with the passed in UIModeEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="uIModeInstance">UIModeEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingUIModeCollectionViaDeliverypointgroup____(IEntity uIModeInstance)
		{
			return GetMultiManyToManyUsingUIModeCollectionViaDeliverypointgroup____(uIModeInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this RouteCollection object all RouteEntity objects which are related via a  relation of type 'm:n' with the passed in UIModeEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="uIModeInstance">UIModeEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingUIModeCollectionViaDeliverypointgroup____(IEntity uIModeInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingUIModeCollectionViaDeliverypointgroup____(uIModeInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this RouteCollection object all RouteEntity objects which are related via a Relation of type 'm:n' with the passed in UIModeEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="uIModeInstance">UIModeEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingUIModeCollectionViaDeliverypointgroup____(IEntity uIModeInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingUIModeCollectionViaDeliverypointgroup____(uIModeInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this RouteCollection object all RouteEntity objects which are related via a  relation of type 'm:n' with the passed in UIModeEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="uIModeInstance">UIModeEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingUIModeCollectionViaDeliverypointgroup____(IEntity uIModeInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateRouteDAO().GetMultiUsingUIModeCollectionViaDeliverypointgroup____(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, uIModeInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this RouteCollection object all RouteEntity objects which are related via a  relation of type 'm:n' with the passed in UIModeEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="uIModeInstance">UIModeEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingUIModeCollectionViaDeliverypointgroup____(IEntity uIModeInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateRouteDAO().GetMultiUsingUIModeCollectionViaDeliverypointgroup____(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, uIModeInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this RouteCollection object all RouteEntity objects which are related via a  Relation of type 'm:n' with the passed in UIModeEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="uIModeInstance">UIModeEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingUIModeCollectionViaDeliverypointgroup_____(IEntity uIModeInstance)
		{
			return GetMultiManyToManyUsingUIModeCollectionViaDeliverypointgroup_____(uIModeInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this RouteCollection object all RouteEntity objects which are related via a  relation of type 'm:n' with the passed in UIModeEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="uIModeInstance">UIModeEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingUIModeCollectionViaDeliverypointgroup_____(IEntity uIModeInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingUIModeCollectionViaDeliverypointgroup_____(uIModeInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this RouteCollection object all RouteEntity objects which are related via a Relation of type 'm:n' with the passed in UIModeEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="uIModeInstance">UIModeEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingUIModeCollectionViaDeliverypointgroup_____(IEntity uIModeInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingUIModeCollectionViaDeliverypointgroup_____(uIModeInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this RouteCollection object all RouteEntity objects which are related via a  relation of type 'm:n' with the passed in UIModeEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="uIModeInstance">UIModeEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingUIModeCollectionViaDeliverypointgroup_____(IEntity uIModeInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateRouteDAO().GetMultiUsingUIModeCollectionViaDeliverypointgroup_____(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, uIModeInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this RouteCollection object all RouteEntity objects which are related via a  relation of type 'm:n' with the passed in UIModeEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="uIModeInstance">UIModeEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingUIModeCollectionViaDeliverypointgroup_____(IEntity uIModeInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateRouteDAO().GetMultiUsingUIModeCollectionViaDeliverypointgroup_____(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, uIModeInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this RouteCollection object all RouteEntity objects which are related via a  Relation of type 'm:n' with the passed in UIModeEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="uIModeInstance">UIModeEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingUIModeCollectionViaDeliverypointgroup(IEntity uIModeInstance)
		{
			return GetMultiManyToManyUsingUIModeCollectionViaDeliverypointgroup(uIModeInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this RouteCollection object all RouteEntity objects which are related via a  relation of type 'm:n' with the passed in UIModeEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="uIModeInstance">UIModeEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingUIModeCollectionViaDeliverypointgroup(IEntity uIModeInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingUIModeCollectionViaDeliverypointgroup(uIModeInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this RouteCollection object all RouteEntity objects which are related via a Relation of type 'm:n' with the passed in UIModeEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="uIModeInstance">UIModeEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingUIModeCollectionViaDeliverypointgroup(IEntity uIModeInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingUIModeCollectionViaDeliverypointgroup(uIModeInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this RouteCollection object all RouteEntity objects which are related via a  relation of type 'm:n' with the passed in UIModeEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="uIModeInstance">UIModeEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingUIModeCollectionViaDeliverypointgroup(IEntity uIModeInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateRouteDAO().GetMultiUsingUIModeCollectionViaDeliverypointgroup(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, uIModeInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this RouteCollection object all RouteEntity objects which are related via a  relation of type 'm:n' with the passed in UIModeEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="uIModeInstance">UIModeEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingUIModeCollectionViaDeliverypointgroup(IEntity uIModeInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateRouteDAO().GetMultiUsingUIModeCollectionViaDeliverypointgroup(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, uIModeInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this RouteCollection object all RouteEntity objects which are related via a  Relation of type 'm:n' with the passed in UIModeEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="uIModeInstance">UIModeEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingUIModeCollectionViaDeliverypointgroup_(IEntity uIModeInstance)
		{
			return GetMultiManyToManyUsingUIModeCollectionViaDeliverypointgroup_(uIModeInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this RouteCollection object all RouteEntity objects which are related via a  relation of type 'm:n' with the passed in UIModeEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="uIModeInstance">UIModeEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingUIModeCollectionViaDeliverypointgroup_(IEntity uIModeInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingUIModeCollectionViaDeliverypointgroup_(uIModeInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this RouteCollection object all RouteEntity objects which are related via a Relation of type 'm:n' with the passed in UIModeEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="uIModeInstance">UIModeEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingUIModeCollectionViaDeliverypointgroup_(IEntity uIModeInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingUIModeCollectionViaDeliverypointgroup_(uIModeInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this RouteCollection object all RouteEntity objects which are related via a  relation of type 'm:n' with the passed in UIModeEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="uIModeInstance">UIModeEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingUIModeCollectionViaDeliverypointgroup_(IEntity uIModeInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateRouteDAO().GetMultiUsingUIModeCollectionViaDeliverypointgroup_(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, uIModeInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this RouteCollection object all RouteEntity objects which are related via a  relation of type 'm:n' with the passed in UIModeEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="uIModeInstance">UIModeEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingUIModeCollectionViaDeliverypointgroup_(IEntity uIModeInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateRouteDAO().GetMultiUsingUIModeCollectionViaDeliverypointgroup_(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, uIModeInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this RouteCollection object all RouteEntity objects which are related via a  Relation of type 'm:n' with the passed in VattariffEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="vattariffInstance">VattariffEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingVattariffCollectionViaProduct(IEntity vattariffInstance)
		{
			return GetMultiManyToManyUsingVattariffCollectionViaProduct(vattariffInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this RouteCollection object all RouteEntity objects which are related via a  relation of type 'm:n' with the passed in VattariffEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="vattariffInstance">VattariffEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingVattariffCollectionViaProduct(IEntity vattariffInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingVattariffCollectionViaProduct(vattariffInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this RouteCollection object all RouteEntity objects which are related via a Relation of type 'm:n' with the passed in VattariffEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="vattariffInstance">VattariffEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingVattariffCollectionViaProduct(IEntity vattariffInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingVattariffCollectionViaProduct(vattariffInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this RouteCollection object all RouteEntity objects which are related via a  relation of type 'm:n' with the passed in VattariffEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="vattariffInstance">VattariffEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingVattariffCollectionViaProduct(IEntity vattariffInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateRouteDAO().GetMultiUsingVattariffCollectionViaProduct(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, vattariffInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this RouteCollection object all RouteEntity objects which are related via a  relation of type 'm:n' with the passed in VattariffEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="vattariffInstance">VattariffEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingVattariffCollectionViaProduct(IEntity vattariffInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateRouteDAO().GetMultiUsingVattariffCollectionViaProduct(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, vattariffInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves Entity rows in a datatable which match the specified filter. It will always create a new connection to the database.</summary>
		/// <param name="selectFilter">A predicate or predicate expression which should be used as filter for the entities to retrieve.</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>DataTable with the rows requested.</returns>
		public static DataTable GetMultiAsDataTable(IPredicate selectFilter, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiAsDataTable(selectFilter, maxNumberOfItemsToReturn, sortClauses, null, 0, 0);
		}

		/// <summary> Retrieves Entity rows in a datatable which match the specified filter. It will always create a new connection to the database.</summary>
		/// <param name="selectFilter">A predicate or predicate expression which should be used as filter for the entities to retrieve.</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="relations">The set of relations to walk to construct to total query.</param>
		/// <returns>DataTable with the rows requested.</returns>
		public static DataTable GetMultiAsDataTable(IPredicate selectFilter, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IRelationCollection relations)
		{
			return GetMultiAsDataTable(selectFilter, maxNumberOfItemsToReturn, sortClauses, relations, 0, 0);
		}
		
		/// <summary> Retrieves Entity rows in a datatable which match the specified filter. It will always create a new connection to the database.</summary>
		/// <param name="selectFilter">A predicate or predicate expression which should be used as filter for the entities to retrieve.</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="relations">The set of relations to walk to construct to total query.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>DataTable with the rows requested.</returns>
		public static DataTable GetMultiAsDataTable(IPredicate selectFilter, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IRelationCollection relations, int pageNumber, int pageSize)
		{
			RouteDAO dao = DAOFactory.CreateRouteDAO();
			return dao.GetMultiAsDataTable(maxNumberOfItemsToReturn, sortClauses, selectFilter, relations, pageNumber, pageSize);
		}


		
		/// <summary> Gets a scalar value, calculated with the aggregate. the field index specified is the field the aggregate are applied on.</summary>
		/// <param name="fieldIndex">Field index of field to which to apply the aggregate function and expression</param>
		/// <param name="aggregateToApply">Aggregate function to apply. </param>
		/// <returns>the scalar value requested</returns>
		public object GetScalar(RouteFieldIndex fieldIndex, AggregateFunction aggregateToApply)
		{
			return GetScalar(fieldIndex, null, aggregateToApply, null, null, null);
		}

		/// <summary> Gets a scalar value, calculated with the aggregate and expression specified. the field index specified is the field the expression and aggregate are applied on.</summary>
		/// <param name="fieldIndex">Field index of field to which to apply the aggregate function and expression</param>
		/// <param name="expressionToExecute">The expression to execute. Can be null</param>
		/// <param name="aggregateToApply">Aggregate function to apply. </param>
		/// <returns>the scalar value requested</returns>
		public object GetScalar(RouteFieldIndex fieldIndex, IExpression expressionToExecute, AggregateFunction aggregateToApply)
		{
			return GetScalar(fieldIndex, expressionToExecute, aggregateToApply, null, null, null);
		}

		/// <summary> Gets a scalar value, calculated with the aggregate and expression specified. the field index specified is the field the expression and aggregate are applied on.</summary>
		/// <param name="fieldIndex">Field index of field to which to apply the aggregate function and expression</param>
		/// <param name="expressionToExecute">The expression to execute. Can be null</param>
		/// <param name="aggregateToApply">Aggregate function to apply. </param>
		/// <param name="filter">The filter to apply to retrieve the scalar</param>
		/// <returns>the scalar value requested</returns>
		public object GetScalar(RouteFieldIndex fieldIndex, IExpression expressionToExecute, AggregateFunction aggregateToApply, IPredicate filter)
		{
			return GetScalar(fieldIndex, expressionToExecute, aggregateToApply, filter, null, null);
		}

		/// <summary> Gets a scalar value, calculated with the aggregate and expression specified. the field index specified is the field the expression and aggregate are applied on.</summary>
		/// <param name="fieldIndex">Field index of field to which to apply the aggregate function and expression</param>
		/// <param name="expressionToExecute">The expression to execute. Can be null</param>
		/// <param name="aggregateToApply">Aggregate function to apply. </param>
		/// <param name="filter">The filter to apply to retrieve the scalar</param>
		/// <param name="groupByClause">The groupby clause to apply to retrieve the scalar</param>
		/// <returns>the scalar value requested</returns>
		public object GetScalar(RouteFieldIndex fieldIndex, IExpression expressionToExecute, AggregateFunction aggregateToApply, IPredicate filter, IGroupByCollection groupByClause)
		{
			return GetScalar(fieldIndex, expressionToExecute, aggregateToApply, filter, null, groupByClause);
		}

		/// <summary> Gets a scalar value, calculated with the aggregate and expression specified. the field index specified is the field the expression and aggregate are applied on.</summary>
		/// <param name="fieldIndex">Field index of field to which to apply the aggregate function and expression</param>
		/// <param name="expressionToExecute">The expression to execute. Can be null</param>
		/// <param name="aggregateToApply">Aggregate function to apply. </param>
		/// <param name="filter">The filter to apply to retrieve the scalar</param>
		/// <param name="relations">The relations to walk</param>
		/// <param name="groupByClause">The groupby clause to apply to retrieve the scalar</param>
		/// <returns>the scalar value requested</returns>
		public virtual object GetScalar(RouteFieldIndex fieldIndex, IExpression expressionToExecute, AggregateFunction aggregateToApply, IPredicate filter, IRelationCollection relations, IGroupByCollection groupByClause)
		{
			EntityFields fields = new EntityFields(1);
			fields[0] = EntityFieldFactory.Create(fieldIndex);
			if((fields[0].ExpressionToApply == null) || (expressionToExecute != null))
			{
				fields[0].ExpressionToApply = expressionToExecute;
			}
			if((fields[0].AggregateFunctionToApply == AggregateFunction.None) || (aggregateToApply != AggregateFunction.None))
			{
				fields[0].AggregateFunctionToApply = aggregateToApply;
			}
			return DAOFactory.CreateRouteDAO().GetScalar(fields, this.Transaction, filter, relations, groupByClause);
		}
		
		/// <summary>Creats a new DAO instance so code which is in the base class can still use the proper DAO object.</summary>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateRouteDAO();
		}
		
		/// <summary>Creates a new transaction object</summary>
		/// <param name="levelOfIsolation">The level of isolation.</param>
		/// <param name="name">The name.</param>
		protected override ITransaction CreateTransaction( IsolationLevel levelOfIsolation, string name )
		{
			return new Transaction(levelOfIsolation, name);
		}

		#region Custom EntityCollection code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCollectionCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion
		
		#region Included Code

		#endregion
	}
}
