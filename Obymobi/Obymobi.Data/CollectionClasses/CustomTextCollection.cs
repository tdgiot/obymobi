﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Data;
using System.Collections.Generic;
using System.ComponentModel;
using System.Xml;
#if !CF
using System.Runtime.Serialization;
#endif

using Obymobi.Data.EntityClasses;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.CollectionClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Collection class for storing and retrieving collections of CustomTextEntity objects. </summary>
	[Serializable]
	public partial class CustomTextCollection : CommonEntityCollection<CustomTextEntity>
	{
		/// <summary> CTor</summary>
		public CustomTextCollection():base(new CustomTextEntityFactory())
		{
		}

		/// <summary> CTor</summary>
		/// <param name="initialContents">The initial contents of this collection.</param>
		public CustomTextCollection(IEnumerable<CustomTextEntity> initialContents):base(new CustomTextEntityFactory())
		{
			AddRange(initialContents);
		}

		/// <summary> CTor</summary>
		/// <param name="entityFactoryToUse">The EntityFactory to use when creating entity objects during a GetMulti() call.</param>
		public CustomTextCollection(IEntityFactory entityFactoryToUse):base(entityFactoryToUse)
		{
		}

		/// <summary> Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected CustomTextCollection(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}


		/// <summary> Retrieves in this CustomTextCollection object all CustomTextEntity objects which have data in common with the specified related Entities.
		/// If one is omitted, that entity is not used as a filter. All current elements in the collection are removed from the collection.</summary>
		/// <param name="actionButtonEntityInstance">ActionButtonEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="advertisementEntityInstance">AdvertisementEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="alterationEntityInstance">AlterationEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="alterationoptionEntityInstance">AlterationoptionEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="amenityEntityInstance">AmenityEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="announcementEntityInstance">AnnouncementEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="applicationConfigurationEntityInstance">ApplicationConfigurationEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="carouselItemEntityInstance">CarouselItemEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="landingPageEntityInstance">LandingPageEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="navigationMenuItemEntityInstance">NavigationMenuItemEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="widgetEntityInstance">WidgetEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="attachmentEntityInstance">AttachmentEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="availabilityEntityInstance">AvailabilityEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="categoryEntityInstance">CategoryEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="checkoutMethodEntityInstance">CheckoutMethodEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="clientConfigurationEntityInstance">ClientConfigurationEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="companyEntityInstance">CompanyEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="deliverypointgroupEntityInstance">DeliverypointgroupEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="entertainmentcategoryEntityInstance">EntertainmentcategoryEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="genericcategoryEntityInstance">GenericcategoryEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="genericproductEntityInstance">GenericproductEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="infraredCommandEntityInstance">InfraredCommandEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="languageEntityInstance">LanguageEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="outletEntityInstance">OutletEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="pageEntityInstance">PageEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="pageTemplateEntityInstance">PageTemplateEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="pointOfInterestEntityInstance">PointOfInterestEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="productEntityInstance">ProductEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="productgroupEntityInstance">ProductgroupEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="roomControlAreaEntityInstance">RoomControlAreaEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="roomControlComponentEntityInstance">RoomControlComponentEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="roomControlSectionEntityInstance">RoomControlSectionEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="roomControlSectionItemEntityInstance">RoomControlSectionItemEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="roomControlWidgetEntityInstance">RoomControlWidgetEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="routestephandlerEntityInstance">RoutestephandlerEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="scheduledMessageEntityInstance">ScheduledMessageEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="serviceMethodEntityInstance">ServiceMethodEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="siteEntityInstance">SiteEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="siteTemplateEntityInstance">SiteTemplateEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="stationEntityInstance">StationEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="uIFooterItemEntityInstance">UIFooterItemEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="uITabEntityInstance">UITabEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="uIWidgetEntityInstance">UIWidgetEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="venueCategoryEntityInstance">VenueCategoryEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <returns>true if succeeded, false otherwise</returns>
		public bool GetMultiManyToOne(IEntity actionButtonEntityInstance, IEntity advertisementEntityInstance, IEntity alterationEntityInstance, IEntity alterationoptionEntityInstance, IEntity amenityEntityInstance, IEntity announcementEntityInstance, IEntity applicationConfigurationEntityInstance, IEntity carouselItemEntityInstance, IEntity landingPageEntityInstance, IEntity navigationMenuItemEntityInstance, IEntity widgetEntityInstance, IEntity attachmentEntityInstance, IEntity availabilityEntityInstance, IEntity categoryEntityInstance, IEntity checkoutMethodEntityInstance, IEntity clientConfigurationEntityInstance, IEntity companyEntityInstance, IEntity deliverypointgroupEntityInstance, IEntity entertainmentcategoryEntityInstance, IEntity genericcategoryEntityInstance, IEntity genericproductEntityInstance, IEntity infraredCommandEntityInstance, IEntity languageEntityInstance, IEntity outletEntityInstance, IEntity pageEntityInstance, IEntity pageTemplateEntityInstance, IEntity pointOfInterestEntityInstance, IEntity productEntityInstance, IEntity productgroupEntityInstance, IEntity roomControlAreaEntityInstance, IEntity roomControlComponentEntityInstance, IEntity roomControlSectionEntityInstance, IEntity roomControlSectionItemEntityInstance, IEntity roomControlWidgetEntityInstance, IEntity routestephandlerEntityInstance, IEntity scheduledMessageEntityInstance, IEntity serviceMethodEntityInstance, IEntity siteEntityInstance, IEntity siteTemplateEntityInstance, IEntity stationEntityInstance, IEntity uIFooterItemEntityInstance, IEntity uITabEntityInstance, IEntity uIWidgetEntityInstance, IEntity venueCategoryEntityInstance)
		{
			return GetMultiManyToOne(actionButtonEntityInstance, advertisementEntityInstance, alterationEntityInstance, alterationoptionEntityInstance, amenityEntityInstance, announcementEntityInstance, applicationConfigurationEntityInstance, carouselItemEntityInstance, landingPageEntityInstance, navigationMenuItemEntityInstance, widgetEntityInstance, attachmentEntityInstance, availabilityEntityInstance, categoryEntityInstance, checkoutMethodEntityInstance, clientConfigurationEntityInstance, companyEntityInstance, deliverypointgroupEntityInstance, entertainmentcategoryEntityInstance, genericcategoryEntityInstance, genericproductEntityInstance, infraredCommandEntityInstance, languageEntityInstance, outletEntityInstance, pageEntityInstance, pageTemplateEntityInstance, pointOfInterestEntityInstance, productEntityInstance, productgroupEntityInstance, roomControlAreaEntityInstance, roomControlComponentEntityInstance, roomControlSectionEntityInstance, roomControlSectionItemEntityInstance, roomControlWidgetEntityInstance, routestephandlerEntityInstance, scheduledMessageEntityInstance, serviceMethodEntityInstance, siteEntityInstance, siteTemplateEntityInstance, stationEntityInstance, uIFooterItemEntityInstance, uITabEntityInstance, uIWidgetEntityInstance, venueCategoryEntityInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, null, 0, 0);
		}

		/// <summary> Retrieves in this CustomTextCollection object all CustomTextEntity objects which have data in common with the specified related Entities.
		/// If one is omitted, that entity is not used as a filter. All current elements in the collection are removed from the collection.</summary>
		/// <param name="actionButtonEntityInstance">ActionButtonEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="advertisementEntityInstance">AdvertisementEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="alterationEntityInstance">AlterationEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="alterationoptionEntityInstance">AlterationoptionEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="amenityEntityInstance">AmenityEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="announcementEntityInstance">AnnouncementEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="applicationConfigurationEntityInstance">ApplicationConfigurationEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="carouselItemEntityInstance">CarouselItemEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="landingPageEntityInstance">LandingPageEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="navigationMenuItemEntityInstance">NavigationMenuItemEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="widgetEntityInstance">WidgetEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="attachmentEntityInstance">AttachmentEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="availabilityEntityInstance">AvailabilityEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="categoryEntityInstance">CategoryEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="checkoutMethodEntityInstance">CheckoutMethodEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="clientConfigurationEntityInstance">ClientConfigurationEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="companyEntityInstance">CompanyEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="deliverypointgroupEntityInstance">DeliverypointgroupEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="entertainmentcategoryEntityInstance">EntertainmentcategoryEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="genericcategoryEntityInstance">GenericcategoryEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="genericproductEntityInstance">GenericproductEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="infraredCommandEntityInstance">InfraredCommandEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="languageEntityInstance">LanguageEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="outletEntityInstance">OutletEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="pageEntityInstance">PageEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="pageTemplateEntityInstance">PageTemplateEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="pointOfInterestEntityInstance">PointOfInterestEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="productEntityInstance">ProductEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="productgroupEntityInstance">ProductgroupEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="roomControlAreaEntityInstance">RoomControlAreaEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="roomControlComponentEntityInstance">RoomControlComponentEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="roomControlSectionEntityInstance">RoomControlSectionEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="roomControlSectionItemEntityInstance">RoomControlSectionItemEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="roomControlWidgetEntityInstance">RoomControlWidgetEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="routestephandlerEntityInstance">RoutestephandlerEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="scheduledMessageEntityInstance">ScheduledMessageEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="serviceMethodEntityInstance">ServiceMethodEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="siteEntityInstance">SiteEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="siteTemplateEntityInstance">SiteTemplateEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="stationEntityInstance">StationEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="uIFooterItemEntityInstance">UIFooterItemEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="uITabEntityInstance">UITabEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="uIWidgetEntityInstance">UIWidgetEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="venueCategoryEntityInstance">VenueCategoryEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="filter">Extra filter to limit the resultset. Predicate expression can be null, in which case it will be ignored.</param>
		/// <returns>true if succeeded, false otherwise</returns>
		public bool GetMultiManyToOne(IEntity actionButtonEntityInstance, IEntity advertisementEntityInstance, IEntity alterationEntityInstance, IEntity alterationoptionEntityInstance, IEntity amenityEntityInstance, IEntity announcementEntityInstance, IEntity applicationConfigurationEntityInstance, IEntity carouselItemEntityInstance, IEntity landingPageEntityInstance, IEntity navigationMenuItemEntityInstance, IEntity widgetEntityInstance, IEntity attachmentEntityInstance, IEntity availabilityEntityInstance, IEntity categoryEntityInstance, IEntity checkoutMethodEntityInstance, IEntity clientConfigurationEntityInstance, IEntity companyEntityInstance, IEntity deliverypointgroupEntityInstance, IEntity entertainmentcategoryEntityInstance, IEntity genericcategoryEntityInstance, IEntity genericproductEntityInstance, IEntity infraredCommandEntityInstance, IEntity languageEntityInstance, IEntity outletEntityInstance, IEntity pageEntityInstance, IEntity pageTemplateEntityInstance, IEntity pointOfInterestEntityInstance, IEntity productEntityInstance, IEntity productgroupEntityInstance, IEntity roomControlAreaEntityInstance, IEntity roomControlComponentEntityInstance, IEntity roomControlSectionEntityInstance, IEntity roomControlSectionItemEntityInstance, IEntity roomControlWidgetEntityInstance, IEntity routestephandlerEntityInstance, IEntity scheduledMessageEntityInstance, IEntity serviceMethodEntityInstance, IEntity siteEntityInstance, IEntity siteTemplateEntityInstance, IEntity stationEntityInstance, IEntity uIFooterItemEntityInstance, IEntity uITabEntityInstance, IEntity uIWidgetEntityInstance, IEntity venueCategoryEntityInstance, IPredicateExpression filter)
		{
			return GetMultiManyToOne(actionButtonEntityInstance, advertisementEntityInstance, alterationEntityInstance, alterationoptionEntityInstance, amenityEntityInstance, announcementEntityInstance, applicationConfigurationEntityInstance, carouselItemEntityInstance, landingPageEntityInstance, navigationMenuItemEntityInstance, widgetEntityInstance, attachmentEntityInstance, availabilityEntityInstance, categoryEntityInstance, checkoutMethodEntityInstance, clientConfigurationEntityInstance, companyEntityInstance, deliverypointgroupEntityInstance, entertainmentcategoryEntityInstance, genericcategoryEntityInstance, genericproductEntityInstance, infraredCommandEntityInstance, languageEntityInstance, outletEntityInstance, pageEntityInstance, pageTemplateEntityInstance, pointOfInterestEntityInstance, productEntityInstance, productgroupEntityInstance, roomControlAreaEntityInstance, roomControlComponentEntityInstance, roomControlSectionEntityInstance, roomControlSectionItemEntityInstance, roomControlWidgetEntityInstance, routestephandlerEntityInstance, scheduledMessageEntityInstance, serviceMethodEntityInstance, siteEntityInstance, siteTemplateEntityInstance, stationEntityInstance, uIFooterItemEntityInstance, uITabEntityInstance, uIWidgetEntityInstance, venueCategoryEntityInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, filter, 0, 0);
		}

		/// <summary> Retrieves in this CustomTextCollection object all CustomTextEntity objects which have data in common with the specified related Entities.
		/// If one is omitted, that entity is not used as a filter. All current elements in the collection are removed from the collection.</summary>
		/// <param name="actionButtonEntityInstance">ActionButtonEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="advertisementEntityInstance">AdvertisementEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="alterationEntityInstance">AlterationEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="alterationoptionEntityInstance">AlterationoptionEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="amenityEntityInstance">AmenityEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="announcementEntityInstance">AnnouncementEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="applicationConfigurationEntityInstance">ApplicationConfigurationEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="carouselItemEntityInstance">CarouselItemEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="landingPageEntityInstance">LandingPageEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="navigationMenuItemEntityInstance">NavigationMenuItemEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="widgetEntityInstance">WidgetEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="attachmentEntityInstance">AttachmentEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="availabilityEntityInstance">AvailabilityEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="categoryEntityInstance">CategoryEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="checkoutMethodEntityInstance">CheckoutMethodEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="clientConfigurationEntityInstance">ClientConfigurationEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="companyEntityInstance">CompanyEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="deliverypointgroupEntityInstance">DeliverypointgroupEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="entertainmentcategoryEntityInstance">EntertainmentcategoryEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="genericcategoryEntityInstance">GenericcategoryEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="genericproductEntityInstance">GenericproductEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="infraredCommandEntityInstance">InfraredCommandEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="languageEntityInstance">LanguageEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="outletEntityInstance">OutletEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="pageEntityInstance">PageEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="pageTemplateEntityInstance">PageTemplateEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="pointOfInterestEntityInstance">PointOfInterestEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="productEntityInstance">ProductEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="productgroupEntityInstance">ProductgroupEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="roomControlAreaEntityInstance">RoomControlAreaEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="roomControlComponentEntityInstance">RoomControlComponentEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="roomControlSectionEntityInstance">RoomControlSectionEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="roomControlSectionItemEntityInstance">RoomControlSectionItemEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="roomControlWidgetEntityInstance">RoomControlWidgetEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="routestephandlerEntityInstance">RoutestephandlerEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="scheduledMessageEntityInstance">ScheduledMessageEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="serviceMethodEntityInstance">ServiceMethodEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="siteEntityInstance">SiteEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="siteTemplateEntityInstance">SiteTemplateEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="stationEntityInstance">StationEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="uIFooterItemEntityInstance">UIFooterItemEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="uITabEntityInstance">UITabEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="uIWidgetEntityInstance">UIWidgetEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="venueCategoryEntityInstance">VenueCategoryEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="filter">Extra filter to limit the resultset. Predicate expression can be null, in which case it will be ignored.</param>
		/// <returns>true if succeeded, false otherwise</returns>
		public bool GetMultiManyToOne(IEntity actionButtonEntityInstance, IEntity advertisementEntityInstance, IEntity alterationEntityInstance, IEntity alterationoptionEntityInstance, IEntity amenityEntityInstance, IEntity announcementEntityInstance, IEntity applicationConfigurationEntityInstance, IEntity carouselItemEntityInstance, IEntity landingPageEntityInstance, IEntity navigationMenuItemEntityInstance, IEntity widgetEntityInstance, IEntity attachmentEntityInstance, IEntity availabilityEntityInstance, IEntity categoryEntityInstance, IEntity checkoutMethodEntityInstance, IEntity clientConfigurationEntityInstance, IEntity companyEntityInstance, IEntity deliverypointgroupEntityInstance, IEntity entertainmentcategoryEntityInstance, IEntity genericcategoryEntityInstance, IEntity genericproductEntityInstance, IEntity infraredCommandEntityInstance, IEntity languageEntityInstance, IEntity outletEntityInstance, IEntity pageEntityInstance, IEntity pageTemplateEntityInstance, IEntity pointOfInterestEntityInstance, IEntity productEntityInstance, IEntity productgroupEntityInstance, IEntity roomControlAreaEntityInstance, IEntity roomControlComponentEntityInstance, IEntity roomControlSectionEntityInstance, IEntity roomControlSectionItemEntityInstance, IEntity roomControlWidgetEntityInstance, IEntity routestephandlerEntityInstance, IEntity scheduledMessageEntityInstance, IEntity serviceMethodEntityInstance, IEntity siteEntityInstance, IEntity siteTemplateEntityInstance, IEntity stationEntityInstance, IEntity uIFooterItemEntityInstance, IEntity uITabEntityInstance, IEntity uIWidgetEntityInstance, IEntity venueCategoryEntityInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPredicateExpression filter)
		{
			return GetMultiManyToOne(actionButtonEntityInstance, advertisementEntityInstance, alterationEntityInstance, alterationoptionEntityInstance, amenityEntityInstance, announcementEntityInstance, applicationConfigurationEntityInstance, carouselItemEntityInstance, landingPageEntityInstance, navigationMenuItemEntityInstance, widgetEntityInstance, attachmentEntityInstance, availabilityEntityInstance, categoryEntityInstance, checkoutMethodEntityInstance, clientConfigurationEntityInstance, companyEntityInstance, deliverypointgroupEntityInstance, entertainmentcategoryEntityInstance, genericcategoryEntityInstance, genericproductEntityInstance, infraredCommandEntityInstance, languageEntityInstance, outletEntityInstance, pageEntityInstance, pageTemplateEntityInstance, pointOfInterestEntityInstance, productEntityInstance, productgroupEntityInstance, roomControlAreaEntityInstance, roomControlComponentEntityInstance, roomControlSectionEntityInstance, roomControlSectionItemEntityInstance, roomControlWidgetEntityInstance, routestephandlerEntityInstance, scheduledMessageEntityInstance, serviceMethodEntityInstance, siteEntityInstance, siteTemplateEntityInstance, stationEntityInstance, uIFooterItemEntityInstance, uITabEntityInstance, uIWidgetEntityInstance, venueCategoryEntityInstance, maxNumberOfItemsToReturn, sortClauses, filter, 0, 0);
		}

		/// <summary> Retrieves in this CustomTextCollection object all CustomTextEntity objects which have data in common with the specified related Entities.
		/// If one is omitted, that entity is not used as a filter. All current elements in the collection are removed from the collection.</summary>
		/// <param name="actionButtonEntityInstance">ActionButtonEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="advertisementEntityInstance">AdvertisementEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="alterationEntityInstance">AlterationEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="alterationoptionEntityInstance">AlterationoptionEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="amenityEntityInstance">AmenityEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="announcementEntityInstance">AnnouncementEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="applicationConfigurationEntityInstance">ApplicationConfigurationEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="carouselItemEntityInstance">CarouselItemEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="landingPageEntityInstance">LandingPageEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="navigationMenuItemEntityInstance">NavigationMenuItemEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="widgetEntityInstance">WidgetEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="attachmentEntityInstance">AttachmentEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="availabilityEntityInstance">AvailabilityEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="categoryEntityInstance">CategoryEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="checkoutMethodEntityInstance">CheckoutMethodEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="clientConfigurationEntityInstance">ClientConfigurationEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="companyEntityInstance">CompanyEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="deliverypointgroupEntityInstance">DeliverypointgroupEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="entertainmentcategoryEntityInstance">EntertainmentcategoryEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="genericcategoryEntityInstance">GenericcategoryEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="genericproductEntityInstance">GenericproductEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="infraredCommandEntityInstance">InfraredCommandEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="languageEntityInstance">LanguageEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="outletEntityInstance">OutletEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="pageEntityInstance">PageEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="pageTemplateEntityInstance">PageTemplateEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="pointOfInterestEntityInstance">PointOfInterestEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="productEntityInstance">ProductEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="productgroupEntityInstance">ProductgroupEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="roomControlAreaEntityInstance">RoomControlAreaEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="roomControlComponentEntityInstance">RoomControlComponentEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="roomControlSectionEntityInstance">RoomControlSectionEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="roomControlSectionItemEntityInstance">RoomControlSectionItemEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="roomControlWidgetEntityInstance">RoomControlWidgetEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="routestephandlerEntityInstance">RoutestephandlerEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="scheduledMessageEntityInstance">ScheduledMessageEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="serviceMethodEntityInstance">ServiceMethodEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="siteEntityInstance">SiteEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="siteTemplateEntityInstance">SiteTemplateEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="stationEntityInstance">StationEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="uIFooterItemEntityInstance">UIFooterItemEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="uITabEntityInstance">UITabEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="uIWidgetEntityInstance">UIWidgetEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="venueCategoryEntityInstance">VenueCategoryEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="filter">Extra filter to limit the resultset. Predicate expression can be null, in which case it will be ignored.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToOne(IEntity actionButtonEntityInstance, IEntity advertisementEntityInstance, IEntity alterationEntityInstance, IEntity alterationoptionEntityInstance, IEntity amenityEntityInstance, IEntity announcementEntityInstance, IEntity applicationConfigurationEntityInstance, IEntity carouselItemEntityInstance, IEntity landingPageEntityInstance, IEntity navigationMenuItemEntityInstance, IEntity widgetEntityInstance, IEntity attachmentEntityInstance, IEntity availabilityEntityInstance, IEntity categoryEntityInstance, IEntity checkoutMethodEntityInstance, IEntity clientConfigurationEntityInstance, IEntity companyEntityInstance, IEntity deliverypointgroupEntityInstance, IEntity entertainmentcategoryEntityInstance, IEntity genericcategoryEntityInstance, IEntity genericproductEntityInstance, IEntity infraredCommandEntityInstance, IEntity languageEntityInstance, IEntity outletEntityInstance, IEntity pageEntityInstance, IEntity pageTemplateEntityInstance, IEntity pointOfInterestEntityInstance, IEntity productEntityInstance, IEntity productgroupEntityInstance, IEntity roomControlAreaEntityInstance, IEntity roomControlComponentEntityInstance, IEntity roomControlSectionEntityInstance, IEntity roomControlSectionItemEntityInstance, IEntity roomControlWidgetEntityInstance, IEntity routestephandlerEntityInstance, IEntity scheduledMessageEntityInstance, IEntity serviceMethodEntityInstance, IEntity siteEntityInstance, IEntity siteTemplateEntityInstance, IEntity stationEntityInstance, IEntity uIFooterItemEntityInstance, IEntity uITabEntityInstance, IEntity uIWidgetEntityInstance, IEntity venueCategoryEntityInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPredicateExpression filter, int pageNumber, int pageSize)
		{
			bool validParameters = false;
			validParameters |= (actionButtonEntityInstance!=null);
			validParameters |= (advertisementEntityInstance!=null);
			validParameters |= (alterationEntityInstance!=null);
			validParameters |= (alterationoptionEntityInstance!=null);
			validParameters |= (amenityEntityInstance!=null);
			validParameters |= (announcementEntityInstance!=null);
			validParameters |= (applicationConfigurationEntityInstance!=null);
			validParameters |= (carouselItemEntityInstance!=null);
			validParameters |= (landingPageEntityInstance!=null);
			validParameters |= (navigationMenuItemEntityInstance!=null);
			validParameters |= (widgetEntityInstance!=null);
			validParameters |= (attachmentEntityInstance!=null);
			validParameters |= (availabilityEntityInstance!=null);
			validParameters |= (categoryEntityInstance!=null);
			validParameters |= (checkoutMethodEntityInstance!=null);
			validParameters |= (clientConfigurationEntityInstance!=null);
			validParameters |= (companyEntityInstance!=null);
			validParameters |= (deliverypointgroupEntityInstance!=null);
			validParameters |= (entertainmentcategoryEntityInstance!=null);
			validParameters |= (genericcategoryEntityInstance!=null);
			validParameters |= (genericproductEntityInstance!=null);
			validParameters |= (infraredCommandEntityInstance!=null);
			validParameters |= (languageEntityInstance!=null);
			validParameters |= (outletEntityInstance!=null);
			validParameters |= (pageEntityInstance!=null);
			validParameters |= (pageTemplateEntityInstance!=null);
			validParameters |= (pointOfInterestEntityInstance!=null);
			validParameters |= (productEntityInstance!=null);
			validParameters |= (productgroupEntityInstance!=null);
			validParameters |= (roomControlAreaEntityInstance!=null);
			validParameters |= (roomControlComponentEntityInstance!=null);
			validParameters |= (roomControlSectionEntityInstance!=null);
			validParameters |= (roomControlSectionItemEntityInstance!=null);
			validParameters |= (roomControlWidgetEntityInstance!=null);
			validParameters |= (routestephandlerEntityInstance!=null);
			validParameters |= (scheduledMessageEntityInstance!=null);
			validParameters |= (serviceMethodEntityInstance!=null);
			validParameters |= (siteEntityInstance!=null);
			validParameters |= (siteTemplateEntityInstance!=null);
			validParameters |= (stationEntityInstance!=null);
			validParameters |= (uIFooterItemEntityInstance!=null);
			validParameters |= (uITabEntityInstance!=null);
			validParameters |= (uIWidgetEntityInstance!=null);
			validParameters |= (venueCategoryEntityInstance!=null);
			if(!validParameters)
			{
				return GetMulti(filter, maxNumberOfItemsToReturn, sortClauses, null, pageNumber, pageSize);
			}
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateCustomTextDAO().GetMulti(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, filter, actionButtonEntityInstance, advertisementEntityInstance, alterationEntityInstance, alterationoptionEntityInstance, amenityEntityInstance, announcementEntityInstance, applicationConfigurationEntityInstance, carouselItemEntityInstance, landingPageEntityInstance, navigationMenuItemEntityInstance, widgetEntityInstance, attachmentEntityInstance, availabilityEntityInstance, categoryEntityInstance, checkoutMethodEntityInstance, clientConfigurationEntityInstance, companyEntityInstance, deliverypointgroupEntityInstance, entertainmentcategoryEntityInstance, genericcategoryEntityInstance, genericproductEntityInstance, infraredCommandEntityInstance, languageEntityInstance, outletEntityInstance, pageEntityInstance, pageTemplateEntityInstance, pointOfInterestEntityInstance, productEntityInstance, productgroupEntityInstance, roomControlAreaEntityInstance, roomControlComponentEntityInstance, roomControlSectionEntityInstance, roomControlSectionItemEntityInstance, roomControlWidgetEntityInstance, routestephandlerEntityInstance, scheduledMessageEntityInstance, serviceMethodEntityInstance, siteEntityInstance, siteTemplateEntityInstance, stationEntityInstance, uIFooterItemEntityInstance, uITabEntityInstance, uIWidgetEntityInstance, venueCategoryEntityInstance, pageNumber, pageSize);
		}

		/// <summary> Deletes from the persistent storage all CustomText entities which have data in common with the specified related Entities. If one is omitted, that entity is not used as a filter.</summary>
		/// <remarks>Runs directly on the persistent storage. It will not delete entity objects from the current collection.</remarks>
		/// <param name="actionButtonEntityInstance">ActionButtonEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="advertisementEntityInstance">AdvertisementEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="alterationEntityInstance">AlterationEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="alterationoptionEntityInstance">AlterationoptionEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="amenityEntityInstance">AmenityEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="announcementEntityInstance">AnnouncementEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="applicationConfigurationEntityInstance">ApplicationConfigurationEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="carouselItemEntityInstance">CarouselItemEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="landingPageEntityInstance">LandingPageEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="navigationMenuItemEntityInstance">NavigationMenuItemEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="widgetEntityInstance">WidgetEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="attachmentEntityInstance">AttachmentEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="availabilityEntityInstance">AvailabilityEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="categoryEntityInstance">CategoryEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="checkoutMethodEntityInstance">CheckoutMethodEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="clientConfigurationEntityInstance">ClientConfigurationEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="companyEntityInstance">CompanyEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="deliverypointgroupEntityInstance">DeliverypointgroupEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="entertainmentcategoryEntityInstance">EntertainmentcategoryEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="genericcategoryEntityInstance">GenericcategoryEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="genericproductEntityInstance">GenericproductEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="infraredCommandEntityInstance">InfraredCommandEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="languageEntityInstance">LanguageEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="outletEntityInstance">OutletEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="pageEntityInstance">PageEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="pageTemplateEntityInstance">PageTemplateEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="pointOfInterestEntityInstance">PointOfInterestEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="productEntityInstance">ProductEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="productgroupEntityInstance">ProductgroupEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="roomControlAreaEntityInstance">RoomControlAreaEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="roomControlComponentEntityInstance">RoomControlComponentEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="roomControlSectionEntityInstance">RoomControlSectionEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="roomControlSectionItemEntityInstance">RoomControlSectionItemEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="roomControlWidgetEntityInstance">RoomControlWidgetEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="routestephandlerEntityInstance">RoutestephandlerEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="scheduledMessageEntityInstance">ScheduledMessageEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="serviceMethodEntityInstance">ServiceMethodEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="siteEntityInstance">SiteEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="siteTemplateEntityInstance">SiteTemplateEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="stationEntityInstance">StationEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="uIFooterItemEntityInstance">UIFooterItemEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="uITabEntityInstance">UITabEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="uIWidgetEntityInstance">UIWidgetEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="venueCategoryEntityInstance">VenueCategoryEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <returns>Amount of entities affected, if the used persistent storage has rowcounting enabled.</returns>
		public int DeleteMultiManyToOne(IEntity actionButtonEntityInstance, IEntity advertisementEntityInstance, IEntity alterationEntityInstance, IEntity alterationoptionEntityInstance, IEntity amenityEntityInstance, IEntity announcementEntityInstance, IEntity applicationConfigurationEntityInstance, IEntity carouselItemEntityInstance, IEntity landingPageEntityInstance, IEntity navigationMenuItemEntityInstance, IEntity widgetEntityInstance, IEntity attachmentEntityInstance, IEntity availabilityEntityInstance, IEntity categoryEntityInstance, IEntity checkoutMethodEntityInstance, IEntity clientConfigurationEntityInstance, IEntity companyEntityInstance, IEntity deliverypointgroupEntityInstance, IEntity entertainmentcategoryEntityInstance, IEntity genericcategoryEntityInstance, IEntity genericproductEntityInstance, IEntity infraredCommandEntityInstance, IEntity languageEntityInstance, IEntity outletEntityInstance, IEntity pageEntityInstance, IEntity pageTemplateEntityInstance, IEntity pointOfInterestEntityInstance, IEntity productEntityInstance, IEntity productgroupEntityInstance, IEntity roomControlAreaEntityInstance, IEntity roomControlComponentEntityInstance, IEntity roomControlSectionEntityInstance, IEntity roomControlSectionItemEntityInstance, IEntity roomControlWidgetEntityInstance, IEntity routestephandlerEntityInstance, IEntity scheduledMessageEntityInstance, IEntity serviceMethodEntityInstance, IEntity siteEntityInstance, IEntity siteTemplateEntityInstance, IEntity stationEntityInstance, IEntity uIFooterItemEntityInstance, IEntity uITabEntityInstance, IEntity uIWidgetEntityInstance, IEntity venueCategoryEntityInstance)
		{
			return DAOFactory.CreateCustomTextDAO().DeleteMulti(this.Transaction, actionButtonEntityInstance, advertisementEntityInstance, alterationEntityInstance, alterationoptionEntityInstance, amenityEntityInstance, announcementEntityInstance, applicationConfigurationEntityInstance, carouselItemEntityInstance, landingPageEntityInstance, navigationMenuItemEntityInstance, widgetEntityInstance, attachmentEntityInstance, availabilityEntityInstance, categoryEntityInstance, checkoutMethodEntityInstance, clientConfigurationEntityInstance, companyEntityInstance, deliverypointgroupEntityInstance, entertainmentcategoryEntityInstance, genericcategoryEntityInstance, genericproductEntityInstance, infraredCommandEntityInstance, languageEntityInstance, outletEntityInstance, pageEntityInstance, pageTemplateEntityInstance, pointOfInterestEntityInstance, productEntityInstance, productgroupEntityInstance, roomControlAreaEntityInstance, roomControlComponentEntityInstance, roomControlSectionEntityInstance, roomControlSectionItemEntityInstance, roomControlWidgetEntityInstance, routestephandlerEntityInstance, scheduledMessageEntityInstance, serviceMethodEntityInstance, siteEntityInstance, siteTemplateEntityInstance, stationEntityInstance, uIFooterItemEntityInstance, uITabEntityInstance, uIWidgetEntityInstance, venueCategoryEntityInstance);
		}

		/// <summary> Updates in the persistent storage all CustomText entities which have data in common with the specified related Entities. If one is omitted, that entity is not used as a filter.
		/// Which fields are updated in those matching entities depends on which fields are <i>changed</i> in the passed in entity entityWithNewValues. The new values of these fields are read from entityWithNewValues. </summary>
		/// <param name="entityWithNewValues">CustomTextEntity instance which holds the new values for the matching entities to update. Only changed fields are taken into account</param>
		/// <param name="actionButtonEntityInstance">ActionButtonEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="advertisementEntityInstance">AdvertisementEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="alterationEntityInstance">AlterationEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="alterationoptionEntityInstance">AlterationoptionEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="amenityEntityInstance">AmenityEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="announcementEntityInstance">AnnouncementEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="applicationConfigurationEntityInstance">ApplicationConfigurationEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="carouselItemEntityInstance">CarouselItemEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="landingPageEntityInstance">LandingPageEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="navigationMenuItemEntityInstance">NavigationMenuItemEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="widgetEntityInstance">WidgetEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="attachmentEntityInstance">AttachmentEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="availabilityEntityInstance">AvailabilityEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="categoryEntityInstance">CategoryEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="checkoutMethodEntityInstance">CheckoutMethodEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="clientConfigurationEntityInstance">ClientConfigurationEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="companyEntityInstance">CompanyEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="deliverypointgroupEntityInstance">DeliverypointgroupEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="entertainmentcategoryEntityInstance">EntertainmentcategoryEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="genericcategoryEntityInstance">GenericcategoryEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="genericproductEntityInstance">GenericproductEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="infraredCommandEntityInstance">InfraredCommandEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="languageEntityInstance">LanguageEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="outletEntityInstance">OutletEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="pageEntityInstance">PageEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="pageTemplateEntityInstance">PageTemplateEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="pointOfInterestEntityInstance">PointOfInterestEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="productEntityInstance">ProductEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="productgroupEntityInstance">ProductgroupEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="roomControlAreaEntityInstance">RoomControlAreaEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="roomControlComponentEntityInstance">RoomControlComponentEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="roomControlSectionEntityInstance">RoomControlSectionEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="roomControlSectionItemEntityInstance">RoomControlSectionItemEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="roomControlWidgetEntityInstance">RoomControlWidgetEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="routestephandlerEntityInstance">RoutestephandlerEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="scheduledMessageEntityInstance">ScheduledMessageEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="serviceMethodEntityInstance">ServiceMethodEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="siteEntityInstance">SiteEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="siteTemplateEntityInstance">SiteTemplateEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="stationEntityInstance">StationEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="uIFooterItemEntityInstance">UIFooterItemEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="uITabEntityInstance">UITabEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="uIWidgetEntityInstance">UIWidgetEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <param name="venueCategoryEntityInstance">VenueCategoryEntity instance to use as a filter for the CustomTextEntity objects to return</param>
		/// <returns>Amount of entities affected, if the used persistent storage has rowcounting enabled.</returns>
		public int UpdateMultiManyToOne(CustomTextEntity entityWithNewValues, IEntity actionButtonEntityInstance, IEntity advertisementEntityInstance, IEntity alterationEntityInstance, IEntity alterationoptionEntityInstance, IEntity amenityEntityInstance, IEntity announcementEntityInstance, IEntity applicationConfigurationEntityInstance, IEntity carouselItemEntityInstance, IEntity landingPageEntityInstance, IEntity navigationMenuItemEntityInstance, IEntity widgetEntityInstance, IEntity attachmentEntityInstance, IEntity availabilityEntityInstance, IEntity categoryEntityInstance, IEntity checkoutMethodEntityInstance, IEntity clientConfigurationEntityInstance, IEntity companyEntityInstance, IEntity deliverypointgroupEntityInstance, IEntity entertainmentcategoryEntityInstance, IEntity genericcategoryEntityInstance, IEntity genericproductEntityInstance, IEntity infraredCommandEntityInstance, IEntity languageEntityInstance, IEntity outletEntityInstance, IEntity pageEntityInstance, IEntity pageTemplateEntityInstance, IEntity pointOfInterestEntityInstance, IEntity productEntityInstance, IEntity productgroupEntityInstance, IEntity roomControlAreaEntityInstance, IEntity roomControlComponentEntityInstance, IEntity roomControlSectionEntityInstance, IEntity roomControlSectionItemEntityInstance, IEntity roomControlWidgetEntityInstance, IEntity routestephandlerEntityInstance, IEntity scheduledMessageEntityInstance, IEntity serviceMethodEntityInstance, IEntity siteEntityInstance, IEntity siteTemplateEntityInstance, IEntity stationEntityInstance, IEntity uIFooterItemEntityInstance, IEntity uITabEntityInstance, IEntity uIWidgetEntityInstance, IEntity venueCategoryEntityInstance)
		{
			return DAOFactory.CreateCustomTextDAO().UpdateMulti(entityWithNewValues, this.Transaction, actionButtonEntityInstance, advertisementEntityInstance, alterationEntityInstance, alterationoptionEntityInstance, amenityEntityInstance, announcementEntityInstance, applicationConfigurationEntityInstance, carouselItemEntityInstance, landingPageEntityInstance, navigationMenuItemEntityInstance, widgetEntityInstance, attachmentEntityInstance, availabilityEntityInstance, categoryEntityInstance, checkoutMethodEntityInstance, clientConfigurationEntityInstance, companyEntityInstance, deliverypointgroupEntityInstance, entertainmentcategoryEntityInstance, genericcategoryEntityInstance, genericproductEntityInstance, infraredCommandEntityInstance, languageEntityInstance, outletEntityInstance, pageEntityInstance, pageTemplateEntityInstance, pointOfInterestEntityInstance, productEntityInstance, productgroupEntityInstance, roomControlAreaEntityInstance, roomControlComponentEntityInstance, roomControlSectionEntityInstance, roomControlSectionItemEntityInstance, roomControlWidgetEntityInstance, routestephandlerEntityInstance, scheduledMessageEntityInstance, serviceMethodEntityInstance, siteEntityInstance, siteTemplateEntityInstance, stationEntityInstance, uIFooterItemEntityInstance, uITabEntityInstance, uIWidgetEntityInstance, venueCategoryEntityInstance);
		}


		/// <summary> Retrieves Entity rows in a datatable which match the specified filter. It will always create a new connection to the database.</summary>
		/// <param name="selectFilter">A predicate or predicate expression which should be used as filter for the entities to retrieve.</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>DataTable with the rows requested.</returns>
		public static DataTable GetMultiAsDataTable(IPredicate selectFilter, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiAsDataTable(selectFilter, maxNumberOfItemsToReturn, sortClauses, null, 0, 0);
		}

		/// <summary> Retrieves Entity rows in a datatable which match the specified filter. It will always create a new connection to the database.</summary>
		/// <param name="selectFilter">A predicate or predicate expression which should be used as filter for the entities to retrieve.</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="relations">The set of relations to walk to construct to total query.</param>
		/// <returns>DataTable with the rows requested.</returns>
		public static DataTable GetMultiAsDataTable(IPredicate selectFilter, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IRelationCollection relations)
		{
			return GetMultiAsDataTable(selectFilter, maxNumberOfItemsToReturn, sortClauses, relations, 0, 0);
		}
		
		/// <summary> Retrieves Entity rows in a datatable which match the specified filter. It will always create a new connection to the database.</summary>
		/// <param name="selectFilter">A predicate or predicate expression which should be used as filter for the entities to retrieve.</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="relations">The set of relations to walk to construct to total query.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>DataTable with the rows requested.</returns>
		public static DataTable GetMultiAsDataTable(IPredicate selectFilter, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IRelationCollection relations, int pageNumber, int pageSize)
		{
			CustomTextDAO dao = DAOFactory.CreateCustomTextDAO();
			return dao.GetMultiAsDataTable(maxNumberOfItemsToReturn, sortClauses, selectFilter, relations, pageNumber, pageSize);
		}


		
		/// <summary> Gets a scalar value, calculated with the aggregate. the field index specified is the field the aggregate are applied on.</summary>
		/// <param name="fieldIndex">Field index of field to which to apply the aggregate function and expression</param>
		/// <param name="aggregateToApply">Aggregate function to apply. </param>
		/// <returns>the scalar value requested</returns>
		public object GetScalar(CustomTextFieldIndex fieldIndex, AggregateFunction aggregateToApply)
		{
			return GetScalar(fieldIndex, null, aggregateToApply, null, null, null);
		}

		/// <summary> Gets a scalar value, calculated with the aggregate and expression specified. the field index specified is the field the expression and aggregate are applied on.</summary>
		/// <param name="fieldIndex">Field index of field to which to apply the aggregate function and expression</param>
		/// <param name="expressionToExecute">The expression to execute. Can be null</param>
		/// <param name="aggregateToApply">Aggregate function to apply. </param>
		/// <returns>the scalar value requested</returns>
		public object GetScalar(CustomTextFieldIndex fieldIndex, IExpression expressionToExecute, AggregateFunction aggregateToApply)
		{
			return GetScalar(fieldIndex, expressionToExecute, aggregateToApply, null, null, null);
		}

		/// <summary> Gets a scalar value, calculated with the aggregate and expression specified. the field index specified is the field the expression and aggregate are applied on.</summary>
		/// <param name="fieldIndex">Field index of field to which to apply the aggregate function and expression</param>
		/// <param name="expressionToExecute">The expression to execute. Can be null</param>
		/// <param name="aggregateToApply">Aggregate function to apply. </param>
		/// <param name="filter">The filter to apply to retrieve the scalar</param>
		/// <returns>the scalar value requested</returns>
		public object GetScalar(CustomTextFieldIndex fieldIndex, IExpression expressionToExecute, AggregateFunction aggregateToApply, IPredicate filter)
		{
			return GetScalar(fieldIndex, expressionToExecute, aggregateToApply, filter, null, null);
		}

		/// <summary> Gets a scalar value, calculated with the aggregate and expression specified. the field index specified is the field the expression and aggregate are applied on.</summary>
		/// <param name="fieldIndex">Field index of field to which to apply the aggregate function and expression</param>
		/// <param name="expressionToExecute">The expression to execute. Can be null</param>
		/// <param name="aggregateToApply">Aggregate function to apply. </param>
		/// <param name="filter">The filter to apply to retrieve the scalar</param>
		/// <param name="groupByClause">The groupby clause to apply to retrieve the scalar</param>
		/// <returns>the scalar value requested</returns>
		public object GetScalar(CustomTextFieldIndex fieldIndex, IExpression expressionToExecute, AggregateFunction aggregateToApply, IPredicate filter, IGroupByCollection groupByClause)
		{
			return GetScalar(fieldIndex, expressionToExecute, aggregateToApply, filter, null, groupByClause);
		}

		/// <summary> Gets a scalar value, calculated with the aggregate and expression specified. the field index specified is the field the expression and aggregate are applied on.</summary>
		/// <param name="fieldIndex">Field index of field to which to apply the aggregate function and expression</param>
		/// <param name="expressionToExecute">The expression to execute. Can be null</param>
		/// <param name="aggregateToApply">Aggregate function to apply. </param>
		/// <param name="filter">The filter to apply to retrieve the scalar</param>
		/// <param name="relations">The relations to walk</param>
		/// <param name="groupByClause">The groupby clause to apply to retrieve the scalar</param>
		/// <returns>the scalar value requested</returns>
		public virtual object GetScalar(CustomTextFieldIndex fieldIndex, IExpression expressionToExecute, AggregateFunction aggregateToApply, IPredicate filter, IRelationCollection relations, IGroupByCollection groupByClause)
		{
			EntityFields fields = new EntityFields(1);
			fields[0] = EntityFieldFactory.Create(fieldIndex);
			if((fields[0].ExpressionToApply == null) || (expressionToExecute != null))
			{
				fields[0].ExpressionToApply = expressionToExecute;
			}
			if((fields[0].AggregateFunctionToApply == AggregateFunction.None) || (aggregateToApply != AggregateFunction.None))
			{
				fields[0].AggregateFunctionToApply = aggregateToApply;
			}
			return DAOFactory.CreateCustomTextDAO().GetScalar(fields, this.Transaction, filter, relations, groupByClause);
		}
		
		/// <summary>Creats a new DAO instance so code which is in the base class can still use the proper DAO object.</summary>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateCustomTextDAO();
		}
		
		/// <summary>Creates a new transaction object</summary>
		/// <param name="levelOfIsolation">The level of isolation.</param>
		/// <param name="name">The name.</param>
		protected override ITransaction CreateTransaction( IsolationLevel levelOfIsolation, string name )
		{
			return new Transaction(levelOfIsolation, name);
		}

		#region Custom EntityCollection code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCollectionCode	    
	    // __LLBLGENPRO_USER_CODE_REGION_END
		#endregion
		
		#region Included Code

		#endregion
	}
}
