﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Data;
using System.Collections.Generic;
using System.ComponentModel;
using System.Xml;
#if !CF
using System.Runtime.Serialization;
#endif

using Obymobi.Data.EntityClasses;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.CollectionClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Collection class for storing and retrieving collections of DeliverypointgroupEntity objects. </summary>
	[Serializable]
	public partial class DeliverypointgroupCollection : CommonEntityCollection<DeliverypointgroupEntity>
	{
		/// <summary> CTor</summary>
		public DeliverypointgroupCollection():base(new DeliverypointgroupEntityFactory())
		{
		}

		/// <summary> CTor</summary>
		/// <param name="initialContents">The initial contents of this collection.</param>
		public DeliverypointgroupCollection(IEnumerable<DeliverypointgroupEntity> initialContents):base(new DeliverypointgroupEntityFactory())
		{
			AddRange(initialContents);
		}

		/// <summary> CTor</summary>
		/// <param name="entityFactoryToUse">The EntityFactory to use when creating entity objects during a GetMulti() call.</param>
		public DeliverypointgroupCollection(IEntityFactory entityFactoryToUse):base(entityFactoryToUse)
		{
		}

		/// <summary> Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected DeliverypointgroupCollection(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}


		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which have data in common with the specified related Entities.
		/// If one is omitted, that entity is not used as a filter. All current elements in the collection are removed from the collection.</summary>
		/// <param name="addressEntityInstance">AddressEntity instance to use as a filter for the DeliverypointgroupEntity objects to return</param>
		/// <param name="affiliateCampaignEntityInstance">AffiliateCampaignEntity instance to use as a filter for the DeliverypointgroupEntity objects to return</param>
		/// <param name="announcementEntityInstance">AnnouncementEntity instance to use as a filter for the DeliverypointgroupEntity objects to return</param>
		/// <param name="clientConfigurationEntityInstance">ClientConfigurationEntity instance to use as a filter for the DeliverypointgroupEntity objects to return</param>
		/// <param name="companyEntityInstance">CompanyEntity instance to use as a filter for the DeliverypointgroupEntity objects to return</param>
		/// <param name="menuEntityInstance">MenuEntity instance to use as a filter for the DeliverypointgroupEntity objects to return</param>
		/// <param name="posdeliverypointgroupEntityInstance">PosdeliverypointgroupEntity instance to use as a filter for the DeliverypointgroupEntity objects to return</param>
		/// <param name="priceScheduleEntityInstance">PriceScheduleEntity instance to use as a filter for the DeliverypointgroupEntity objects to return</param>
		/// <param name="hotSOSBatteryLowProductEntityInstance">ProductEntity instance to use as a filter for the DeliverypointgroupEntity objects to return</param>
		/// <param name="emailDocumentRouteEntityInstance">RouteEntity instance to use as a filter for the DeliverypointgroupEntity objects to return</param>
		/// <param name="routeEntityInstance">RouteEntity instance to use as a filter for the DeliverypointgroupEntity objects to return</param>
		/// <param name="routeEntity_Instance">RouteEntity instance to use as a filter for the DeliverypointgroupEntity objects to return</param>
		/// <param name="systemMessageRouteEntityInstance">RouteEntity instance to use as a filter for the DeliverypointgroupEntity objects to return</param>
		/// <param name="terminalEntityInstance">TerminalEntity instance to use as a filter for the DeliverypointgroupEntity objects to return</param>
		/// <param name="mobileUIModeEntityInstance">UIModeEntity instance to use as a filter for the DeliverypointgroupEntity objects to return</param>
		/// <param name="tabletUIModeEntityInstance">UIModeEntity instance to use as a filter for the DeliverypointgroupEntity objects to return</param>
		/// <param name="uIModeEntityInstance">UIModeEntity instance to use as a filter for the DeliverypointgroupEntity objects to return</param>
		/// <param name="uIScheduleEntityInstance">UIScheduleEntity instance to use as a filter for the DeliverypointgroupEntity objects to return</param>
		/// <param name="uIThemeEntityInstance">UIThemeEntity instance to use as a filter for the DeliverypointgroupEntity objects to return</param>
		/// <returns>true if succeeded, false otherwise</returns>
		public bool GetMultiManyToOne(IEntity addressEntityInstance, IEntity affiliateCampaignEntityInstance, IEntity announcementEntityInstance, IEntity clientConfigurationEntityInstance, IEntity companyEntityInstance, IEntity menuEntityInstance, IEntity posdeliverypointgroupEntityInstance, IEntity priceScheduleEntityInstance, IEntity hotSOSBatteryLowProductEntityInstance, IEntity emailDocumentRouteEntityInstance, IEntity routeEntityInstance, IEntity routeEntity_Instance, IEntity systemMessageRouteEntityInstance, IEntity terminalEntityInstance, IEntity mobileUIModeEntityInstance, IEntity tabletUIModeEntityInstance, IEntity uIModeEntityInstance, IEntity uIScheduleEntityInstance, IEntity uIThemeEntityInstance)
		{
			return GetMultiManyToOne(addressEntityInstance, affiliateCampaignEntityInstance, announcementEntityInstance, clientConfigurationEntityInstance, companyEntityInstance, menuEntityInstance, posdeliverypointgroupEntityInstance, priceScheduleEntityInstance, hotSOSBatteryLowProductEntityInstance, emailDocumentRouteEntityInstance, routeEntityInstance, routeEntity_Instance, systemMessageRouteEntityInstance, terminalEntityInstance, mobileUIModeEntityInstance, tabletUIModeEntityInstance, uIModeEntityInstance, uIScheduleEntityInstance, uIThemeEntityInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, null, 0, 0);
		}

		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which have data in common with the specified related Entities.
		/// If one is omitted, that entity is not used as a filter. All current elements in the collection are removed from the collection.</summary>
		/// <param name="addressEntityInstance">AddressEntity instance to use as a filter for the DeliverypointgroupEntity objects to return</param>
		/// <param name="affiliateCampaignEntityInstance">AffiliateCampaignEntity instance to use as a filter for the DeliverypointgroupEntity objects to return</param>
		/// <param name="announcementEntityInstance">AnnouncementEntity instance to use as a filter for the DeliverypointgroupEntity objects to return</param>
		/// <param name="clientConfigurationEntityInstance">ClientConfigurationEntity instance to use as a filter for the DeliverypointgroupEntity objects to return</param>
		/// <param name="companyEntityInstance">CompanyEntity instance to use as a filter for the DeliverypointgroupEntity objects to return</param>
		/// <param name="menuEntityInstance">MenuEntity instance to use as a filter for the DeliverypointgroupEntity objects to return</param>
		/// <param name="posdeliverypointgroupEntityInstance">PosdeliverypointgroupEntity instance to use as a filter for the DeliverypointgroupEntity objects to return</param>
		/// <param name="priceScheduleEntityInstance">PriceScheduleEntity instance to use as a filter for the DeliverypointgroupEntity objects to return</param>
		/// <param name="hotSOSBatteryLowProductEntityInstance">ProductEntity instance to use as a filter for the DeliverypointgroupEntity objects to return</param>
		/// <param name="emailDocumentRouteEntityInstance">RouteEntity instance to use as a filter for the DeliverypointgroupEntity objects to return</param>
		/// <param name="routeEntityInstance">RouteEntity instance to use as a filter for the DeliverypointgroupEntity objects to return</param>
		/// <param name="routeEntity_Instance">RouteEntity instance to use as a filter for the DeliverypointgroupEntity objects to return</param>
		/// <param name="systemMessageRouteEntityInstance">RouteEntity instance to use as a filter for the DeliverypointgroupEntity objects to return</param>
		/// <param name="terminalEntityInstance">TerminalEntity instance to use as a filter for the DeliverypointgroupEntity objects to return</param>
		/// <param name="mobileUIModeEntityInstance">UIModeEntity instance to use as a filter for the DeliverypointgroupEntity objects to return</param>
		/// <param name="tabletUIModeEntityInstance">UIModeEntity instance to use as a filter for the DeliverypointgroupEntity objects to return</param>
		/// <param name="uIModeEntityInstance">UIModeEntity instance to use as a filter for the DeliverypointgroupEntity objects to return</param>
		/// <param name="uIScheduleEntityInstance">UIScheduleEntity instance to use as a filter for the DeliverypointgroupEntity objects to return</param>
		/// <param name="uIThemeEntityInstance">UIThemeEntity instance to use as a filter for the DeliverypointgroupEntity objects to return</param>
		/// <param name="filter">Extra filter to limit the resultset. Predicate expression can be null, in which case it will be ignored.</param>
		/// <returns>true if succeeded, false otherwise</returns>
		public bool GetMultiManyToOne(IEntity addressEntityInstance, IEntity affiliateCampaignEntityInstance, IEntity announcementEntityInstance, IEntity clientConfigurationEntityInstance, IEntity companyEntityInstance, IEntity menuEntityInstance, IEntity posdeliverypointgroupEntityInstance, IEntity priceScheduleEntityInstance, IEntity hotSOSBatteryLowProductEntityInstance, IEntity emailDocumentRouteEntityInstance, IEntity routeEntityInstance, IEntity routeEntity_Instance, IEntity systemMessageRouteEntityInstance, IEntity terminalEntityInstance, IEntity mobileUIModeEntityInstance, IEntity tabletUIModeEntityInstance, IEntity uIModeEntityInstance, IEntity uIScheduleEntityInstance, IEntity uIThemeEntityInstance, IPredicateExpression filter)
		{
			return GetMultiManyToOne(addressEntityInstance, affiliateCampaignEntityInstance, announcementEntityInstance, clientConfigurationEntityInstance, companyEntityInstance, menuEntityInstance, posdeliverypointgroupEntityInstance, priceScheduleEntityInstance, hotSOSBatteryLowProductEntityInstance, emailDocumentRouteEntityInstance, routeEntityInstance, routeEntity_Instance, systemMessageRouteEntityInstance, terminalEntityInstance, mobileUIModeEntityInstance, tabletUIModeEntityInstance, uIModeEntityInstance, uIScheduleEntityInstance, uIThemeEntityInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, filter, 0, 0);
		}

		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which have data in common with the specified related Entities.
		/// If one is omitted, that entity is not used as a filter. All current elements in the collection are removed from the collection.</summary>
		/// <param name="addressEntityInstance">AddressEntity instance to use as a filter for the DeliverypointgroupEntity objects to return</param>
		/// <param name="affiliateCampaignEntityInstance">AffiliateCampaignEntity instance to use as a filter for the DeliverypointgroupEntity objects to return</param>
		/// <param name="announcementEntityInstance">AnnouncementEntity instance to use as a filter for the DeliverypointgroupEntity objects to return</param>
		/// <param name="clientConfigurationEntityInstance">ClientConfigurationEntity instance to use as a filter for the DeliverypointgroupEntity objects to return</param>
		/// <param name="companyEntityInstance">CompanyEntity instance to use as a filter for the DeliverypointgroupEntity objects to return</param>
		/// <param name="menuEntityInstance">MenuEntity instance to use as a filter for the DeliverypointgroupEntity objects to return</param>
		/// <param name="posdeliverypointgroupEntityInstance">PosdeliverypointgroupEntity instance to use as a filter for the DeliverypointgroupEntity objects to return</param>
		/// <param name="priceScheduleEntityInstance">PriceScheduleEntity instance to use as a filter for the DeliverypointgroupEntity objects to return</param>
		/// <param name="hotSOSBatteryLowProductEntityInstance">ProductEntity instance to use as a filter for the DeliverypointgroupEntity objects to return</param>
		/// <param name="emailDocumentRouteEntityInstance">RouteEntity instance to use as a filter for the DeliverypointgroupEntity objects to return</param>
		/// <param name="routeEntityInstance">RouteEntity instance to use as a filter for the DeliverypointgroupEntity objects to return</param>
		/// <param name="routeEntity_Instance">RouteEntity instance to use as a filter for the DeliverypointgroupEntity objects to return</param>
		/// <param name="systemMessageRouteEntityInstance">RouteEntity instance to use as a filter for the DeliverypointgroupEntity objects to return</param>
		/// <param name="terminalEntityInstance">TerminalEntity instance to use as a filter for the DeliverypointgroupEntity objects to return</param>
		/// <param name="mobileUIModeEntityInstance">UIModeEntity instance to use as a filter for the DeliverypointgroupEntity objects to return</param>
		/// <param name="tabletUIModeEntityInstance">UIModeEntity instance to use as a filter for the DeliverypointgroupEntity objects to return</param>
		/// <param name="uIModeEntityInstance">UIModeEntity instance to use as a filter for the DeliverypointgroupEntity objects to return</param>
		/// <param name="uIScheduleEntityInstance">UIScheduleEntity instance to use as a filter for the DeliverypointgroupEntity objects to return</param>
		/// <param name="uIThemeEntityInstance">UIThemeEntity instance to use as a filter for the DeliverypointgroupEntity objects to return</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="filter">Extra filter to limit the resultset. Predicate expression can be null, in which case it will be ignored.</param>
		/// <returns>true if succeeded, false otherwise</returns>
		public bool GetMultiManyToOne(IEntity addressEntityInstance, IEntity affiliateCampaignEntityInstance, IEntity announcementEntityInstance, IEntity clientConfigurationEntityInstance, IEntity companyEntityInstance, IEntity menuEntityInstance, IEntity posdeliverypointgroupEntityInstance, IEntity priceScheduleEntityInstance, IEntity hotSOSBatteryLowProductEntityInstance, IEntity emailDocumentRouteEntityInstance, IEntity routeEntityInstance, IEntity routeEntity_Instance, IEntity systemMessageRouteEntityInstance, IEntity terminalEntityInstance, IEntity mobileUIModeEntityInstance, IEntity tabletUIModeEntityInstance, IEntity uIModeEntityInstance, IEntity uIScheduleEntityInstance, IEntity uIThemeEntityInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPredicateExpression filter)
		{
			return GetMultiManyToOne(addressEntityInstance, affiliateCampaignEntityInstance, announcementEntityInstance, clientConfigurationEntityInstance, companyEntityInstance, menuEntityInstance, posdeliverypointgroupEntityInstance, priceScheduleEntityInstance, hotSOSBatteryLowProductEntityInstance, emailDocumentRouteEntityInstance, routeEntityInstance, routeEntity_Instance, systemMessageRouteEntityInstance, terminalEntityInstance, mobileUIModeEntityInstance, tabletUIModeEntityInstance, uIModeEntityInstance, uIScheduleEntityInstance, uIThemeEntityInstance, maxNumberOfItemsToReturn, sortClauses, filter, 0, 0);
		}

		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which have data in common with the specified related Entities.
		/// If one is omitted, that entity is not used as a filter. All current elements in the collection are removed from the collection.</summary>
		/// <param name="addressEntityInstance">AddressEntity instance to use as a filter for the DeliverypointgroupEntity objects to return</param>
		/// <param name="affiliateCampaignEntityInstance">AffiliateCampaignEntity instance to use as a filter for the DeliverypointgroupEntity objects to return</param>
		/// <param name="announcementEntityInstance">AnnouncementEntity instance to use as a filter for the DeliverypointgroupEntity objects to return</param>
		/// <param name="clientConfigurationEntityInstance">ClientConfigurationEntity instance to use as a filter for the DeliverypointgroupEntity objects to return</param>
		/// <param name="companyEntityInstance">CompanyEntity instance to use as a filter for the DeliverypointgroupEntity objects to return</param>
		/// <param name="menuEntityInstance">MenuEntity instance to use as a filter for the DeliverypointgroupEntity objects to return</param>
		/// <param name="posdeliverypointgroupEntityInstance">PosdeliverypointgroupEntity instance to use as a filter for the DeliverypointgroupEntity objects to return</param>
		/// <param name="priceScheduleEntityInstance">PriceScheduleEntity instance to use as a filter for the DeliverypointgroupEntity objects to return</param>
		/// <param name="hotSOSBatteryLowProductEntityInstance">ProductEntity instance to use as a filter for the DeliverypointgroupEntity objects to return</param>
		/// <param name="emailDocumentRouteEntityInstance">RouteEntity instance to use as a filter for the DeliverypointgroupEntity objects to return</param>
		/// <param name="routeEntityInstance">RouteEntity instance to use as a filter for the DeliverypointgroupEntity objects to return</param>
		/// <param name="routeEntity_Instance">RouteEntity instance to use as a filter for the DeliverypointgroupEntity objects to return</param>
		/// <param name="systemMessageRouteEntityInstance">RouteEntity instance to use as a filter for the DeliverypointgroupEntity objects to return</param>
		/// <param name="terminalEntityInstance">TerminalEntity instance to use as a filter for the DeliverypointgroupEntity objects to return</param>
		/// <param name="mobileUIModeEntityInstance">UIModeEntity instance to use as a filter for the DeliverypointgroupEntity objects to return</param>
		/// <param name="tabletUIModeEntityInstance">UIModeEntity instance to use as a filter for the DeliverypointgroupEntity objects to return</param>
		/// <param name="uIModeEntityInstance">UIModeEntity instance to use as a filter for the DeliverypointgroupEntity objects to return</param>
		/// <param name="uIScheduleEntityInstance">UIScheduleEntity instance to use as a filter for the DeliverypointgroupEntity objects to return</param>
		/// <param name="uIThemeEntityInstance">UIThemeEntity instance to use as a filter for the DeliverypointgroupEntity objects to return</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="filter">Extra filter to limit the resultset. Predicate expression can be null, in which case it will be ignored.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToOne(IEntity addressEntityInstance, IEntity affiliateCampaignEntityInstance, IEntity announcementEntityInstance, IEntity clientConfigurationEntityInstance, IEntity companyEntityInstance, IEntity menuEntityInstance, IEntity posdeliverypointgroupEntityInstance, IEntity priceScheduleEntityInstance, IEntity hotSOSBatteryLowProductEntityInstance, IEntity emailDocumentRouteEntityInstance, IEntity routeEntityInstance, IEntity routeEntity_Instance, IEntity systemMessageRouteEntityInstance, IEntity terminalEntityInstance, IEntity mobileUIModeEntityInstance, IEntity tabletUIModeEntityInstance, IEntity uIModeEntityInstance, IEntity uIScheduleEntityInstance, IEntity uIThemeEntityInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPredicateExpression filter, int pageNumber, int pageSize)
		{
			bool validParameters = false;
			validParameters |= (addressEntityInstance!=null);
			validParameters |= (affiliateCampaignEntityInstance!=null);
			validParameters |= (announcementEntityInstance!=null);
			validParameters |= (clientConfigurationEntityInstance!=null);
			validParameters |= (companyEntityInstance!=null);
			validParameters |= (menuEntityInstance!=null);
			validParameters |= (posdeliverypointgroupEntityInstance!=null);
			validParameters |= (priceScheduleEntityInstance!=null);
			validParameters |= (hotSOSBatteryLowProductEntityInstance!=null);
			validParameters |= (emailDocumentRouteEntityInstance!=null);
			validParameters |= (routeEntityInstance!=null);
			validParameters |= (routeEntity_Instance!=null);
			validParameters |= (systemMessageRouteEntityInstance!=null);
			validParameters |= (terminalEntityInstance!=null);
			validParameters |= (mobileUIModeEntityInstance!=null);
			validParameters |= (tabletUIModeEntityInstance!=null);
			validParameters |= (uIModeEntityInstance!=null);
			validParameters |= (uIScheduleEntityInstance!=null);
			validParameters |= (uIThemeEntityInstance!=null);
			if(!validParameters)
			{
				return GetMulti(filter, maxNumberOfItemsToReturn, sortClauses, null, pageNumber, pageSize);
			}
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateDeliverypointgroupDAO().GetMulti(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, filter, addressEntityInstance, affiliateCampaignEntityInstance, announcementEntityInstance, clientConfigurationEntityInstance, companyEntityInstance, menuEntityInstance, posdeliverypointgroupEntityInstance, priceScheduleEntityInstance, hotSOSBatteryLowProductEntityInstance, emailDocumentRouteEntityInstance, routeEntityInstance, routeEntity_Instance, systemMessageRouteEntityInstance, terminalEntityInstance, mobileUIModeEntityInstance, tabletUIModeEntityInstance, uIModeEntityInstance, uIScheduleEntityInstance, uIThemeEntityInstance, pageNumber, pageSize);
		}

		/// <summary> Deletes from the persistent storage all Deliverypointgroup entities which have data in common with the specified related Entities. If one is omitted, that entity is not used as a filter.</summary>
		/// <remarks>Runs directly on the persistent storage. It will not delete entity objects from the current collection.</remarks>
		/// <param name="addressEntityInstance">AddressEntity instance to use as a filter for the DeliverypointgroupEntity objects to return</param>
		/// <param name="affiliateCampaignEntityInstance">AffiliateCampaignEntity instance to use as a filter for the DeliverypointgroupEntity objects to return</param>
		/// <param name="announcementEntityInstance">AnnouncementEntity instance to use as a filter for the DeliverypointgroupEntity objects to return</param>
		/// <param name="clientConfigurationEntityInstance">ClientConfigurationEntity instance to use as a filter for the DeliverypointgroupEntity objects to return</param>
		/// <param name="companyEntityInstance">CompanyEntity instance to use as a filter for the DeliverypointgroupEntity objects to return</param>
		/// <param name="menuEntityInstance">MenuEntity instance to use as a filter for the DeliverypointgroupEntity objects to return</param>
		/// <param name="posdeliverypointgroupEntityInstance">PosdeliverypointgroupEntity instance to use as a filter for the DeliverypointgroupEntity objects to return</param>
		/// <param name="priceScheduleEntityInstance">PriceScheduleEntity instance to use as a filter for the DeliverypointgroupEntity objects to return</param>
		/// <param name="hotSOSBatteryLowProductEntityInstance">ProductEntity instance to use as a filter for the DeliverypointgroupEntity objects to return</param>
		/// <param name="emailDocumentRouteEntityInstance">RouteEntity instance to use as a filter for the DeliverypointgroupEntity objects to return</param>
		/// <param name="routeEntityInstance">RouteEntity instance to use as a filter for the DeliverypointgroupEntity objects to return</param>
		/// <param name="routeEntity_Instance">RouteEntity instance to use as a filter for the DeliverypointgroupEntity objects to return</param>
		/// <param name="systemMessageRouteEntityInstance">RouteEntity instance to use as a filter for the DeliverypointgroupEntity objects to return</param>
		/// <param name="terminalEntityInstance">TerminalEntity instance to use as a filter for the DeliverypointgroupEntity objects to return</param>
		/// <param name="mobileUIModeEntityInstance">UIModeEntity instance to use as a filter for the DeliverypointgroupEntity objects to return</param>
		/// <param name="tabletUIModeEntityInstance">UIModeEntity instance to use as a filter for the DeliverypointgroupEntity objects to return</param>
		/// <param name="uIModeEntityInstance">UIModeEntity instance to use as a filter for the DeliverypointgroupEntity objects to return</param>
		/// <param name="uIScheduleEntityInstance">UIScheduleEntity instance to use as a filter for the DeliverypointgroupEntity objects to return</param>
		/// <param name="uIThemeEntityInstance">UIThemeEntity instance to use as a filter for the DeliverypointgroupEntity objects to return</param>
		/// <returns>Amount of entities affected, if the used persistent storage has rowcounting enabled.</returns>
		public int DeleteMultiManyToOne(IEntity addressEntityInstance, IEntity affiliateCampaignEntityInstance, IEntity announcementEntityInstance, IEntity clientConfigurationEntityInstance, IEntity companyEntityInstance, IEntity menuEntityInstance, IEntity posdeliverypointgroupEntityInstance, IEntity priceScheduleEntityInstance, IEntity hotSOSBatteryLowProductEntityInstance, IEntity emailDocumentRouteEntityInstance, IEntity routeEntityInstance, IEntity routeEntity_Instance, IEntity systemMessageRouteEntityInstance, IEntity terminalEntityInstance, IEntity mobileUIModeEntityInstance, IEntity tabletUIModeEntityInstance, IEntity uIModeEntityInstance, IEntity uIScheduleEntityInstance, IEntity uIThemeEntityInstance)
		{
			return DAOFactory.CreateDeliverypointgroupDAO().DeleteMulti(this.Transaction, addressEntityInstance, affiliateCampaignEntityInstance, announcementEntityInstance, clientConfigurationEntityInstance, companyEntityInstance, menuEntityInstance, posdeliverypointgroupEntityInstance, priceScheduleEntityInstance, hotSOSBatteryLowProductEntityInstance, emailDocumentRouteEntityInstance, routeEntityInstance, routeEntity_Instance, systemMessageRouteEntityInstance, terminalEntityInstance, mobileUIModeEntityInstance, tabletUIModeEntityInstance, uIModeEntityInstance, uIScheduleEntityInstance, uIThemeEntityInstance);
		}

		/// <summary> Updates in the persistent storage all Deliverypointgroup entities which have data in common with the specified related Entities. If one is omitted, that entity is not used as a filter.
		/// Which fields are updated in those matching entities depends on which fields are <i>changed</i> in the passed in entity entityWithNewValues. The new values of these fields are read from entityWithNewValues. </summary>
		/// <param name="entityWithNewValues">DeliverypointgroupEntity instance which holds the new values for the matching entities to update. Only changed fields are taken into account</param>
		/// <param name="addressEntityInstance">AddressEntity instance to use as a filter for the DeliverypointgroupEntity objects to return</param>
		/// <param name="affiliateCampaignEntityInstance">AffiliateCampaignEntity instance to use as a filter for the DeliverypointgroupEntity objects to return</param>
		/// <param name="announcementEntityInstance">AnnouncementEntity instance to use as a filter for the DeliverypointgroupEntity objects to return</param>
		/// <param name="clientConfigurationEntityInstance">ClientConfigurationEntity instance to use as a filter for the DeliverypointgroupEntity objects to return</param>
		/// <param name="companyEntityInstance">CompanyEntity instance to use as a filter for the DeliverypointgroupEntity objects to return</param>
		/// <param name="menuEntityInstance">MenuEntity instance to use as a filter for the DeliverypointgroupEntity objects to return</param>
		/// <param name="posdeliverypointgroupEntityInstance">PosdeliverypointgroupEntity instance to use as a filter for the DeliverypointgroupEntity objects to return</param>
		/// <param name="priceScheduleEntityInstance">PriceScheduleEntity instance to use as a filter for the DeliverypointgroupEntity objects to return</param>
		/// <param name="hotSOSBatteryLowProductEntityInstance">ProductEntity instance to use as a filter for the DeliverypointgroupEntity objects to return</param>
		/// <param name="emailDocumentRouteEntityInstance">RouteEntity instance to use as a filter for the DeliverypointgroupEntity objects to return</param>
		/// <param name="routeEntityInstance">RouteEntity instance to use as a filter for the DeliverypointgroupEntity objects to return</param>
		/// <param name="routeEntity_Instance">RouteEntity instance to use as a filter for the DeliverypointgroupEntity objects to return</param>
		/// <param name="systemMessageRouteEntityInstance">RouteEntity instance to use as a filter for the DeliverypointgroupEntity objects to return</param>
		/// <param name="terminalEntityInstance">TerminalEntity instance to use as a filter for the DeliverypointgroupEntity objects to return</param>
		/// <param name="mobileUIModeEntityInstance">UIModeEntity instance to use as a filter for the DeliverypointgroupEntity objects to return</param>
		/// <param name="tabletUIModeEntityInstance">UIModeEntity instance to use as a filter for the DeliverypointgroupEntity objects to return</param>
		/// <param name="uIModeEntityInstance">UIModeEntity instance to use as a filter for the DeliverypointgroupEntity objects to return</param>
		/// <param name="uIScheduleEntityInstance">UIScheduleEntity instance to use as a filter for the DeliverypointgroupEntity objects to return</param>
		/// <param name="uIThemeEntityInstance">UIThemeEntity instance to use as a filter for the DeliverypointgroupEntity objects to return</param>
		/// <returns>Amount of entities affected, if the used persistent storage has rowcounting enabled.</returns>
		public int UpdateMultiManyToOne(DeliverypointgroupEntity entityWithNewValues, IEntity addressEntityInstance, IEntity affiliateCampaignEntityInstance, IEntity announcementEntityInstance, IEntity clientConfigurationEntityInstance, IEntity companyEntityInstance, IEntity menuEntityInstance, IEntity posdeliverypointgroupEntityInstance, IEntity priceScheduleEntityInstance, IEntity hotSOSBatteryLowProductEntityInstance, IEntity emailDocumentRouteEntityInstance, IEntity routeEntityInstance, IEntity routeEntity_Instance, IEntity systemMessageRouteEntityInstance, IEntity terminalEntityInstance, IEntity mobileUIModeEntityInstance, IEntity tabletUIModeEntityInstance, IEntity uIModeEntityInstance, IEntity uIScheduleEntityInstance, IEntity uIThemeEntityInstance)
		{
			return DAOFactory.CreateDeliverypointgroupDAO().UpdateMulti(entityWithNewValues, this.Transaction, addressEntityInstance, affiliateCampaignEntityInstance, announcementEntityInstance, clientConfigurationEntityInstance, companyEntityInstance, menuEntityInstance, posdeliverypointgroupEntityInstance, priceScheduleEntityInstance, hotSOSBatteryLowProductEntityInstance, emailDocumentRouteEntityInstance, routeEntityInstance, routeEntity_Instance, systemMessageRouteEntityInstance, terminalEntityInstance, mobileUIModeEntityInstance, tabletUIModeEntityInstance, uIModeEntityInstance, uIScheduleEntityInstance, uIThemeEntityInstance);
		}

		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  Relation of type 'm:n' with the passed in AdvertisementEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="advertisementInstance">AdvertisementEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingAdvertisementCollectionViaDeliverypointgroupAdvertisement(IEntity advertisementInstance)
		{
			return GetMultiManyToManyUsingAdvertisementCollectionViaDeliverypointgroupAdvertisement(advertisementInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  relation of type 'm:n' with the passed in AdvertisementEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="advertisementInstance">AdvertisementEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingAdvertisementCollectionViaDeliverypointgroupAdvertisement(IEntity advertisementInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingAdvertisementCollectionViaDeliverypointgroupAdvertisement(advertisementInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a Relation of type 'm:n' with the passed in AdvertisementEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="advertisementInstance">AdvertisementEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingAdvertisementCollectionViaDeliverypointgroupAdvertisement(IEntity advertisementInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingAdvertisementCollectionViaDeliverypointgroupAdvertisement(advertisementInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  relation of type 'm:n' with the passed in AdvertisementEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="advertisementInstance">AdvertisementEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingAdvertisementCollectionViaDeliverypointgroupAdvertisement(IEntity advertisementInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateDeliverypointgroupDAO().GetMultiUsingAdvertisementCollectionViaDeliverypointgroupAdvertisement(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, advertisementInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  relation of type 'm:n' with the passed in AdvertisementEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="advertisementInstance">AdvertisementEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingAdvertisementCollectionViaDeliverypointgroupAdvertisement(IEntity advertisementInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateDeliverypointgroupDAO().GetMultiUsingAdvertisementCollectionViaDeliverypointgroupAdvertisement(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, advertisementInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  Relation of type 'm:n' with the passed in AlterationoptionEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="alterationoptionInstance">AlterationoptionEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingAlterationoptionCollectionViaMedium(IEntity alterationoptionInstance)
		{
			return GetMultiManyToManyUsingAlterationoptionCollectionViaMedium(alterationoptionInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  relation of type 'm:n' with the passed in AlterationoptionEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="alterationoptionInstance">AlterationoptionEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingAlterationoptionCollectionViaMedium(IEntity alterationoptionInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingAlterationoptionCollectionViaMedium(alterationoptionInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a Relation of type 'm:n' with the passed in AlterationoptionEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="alterationoptionInstance">AlterationoptionEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingAlterationoptionCollectionViaMedium(IEntity alterationoptionInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingAlterationoptionCollectionViaMedium(alterationoptionInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  relation of type 'm:n' with the passed in AlterationoptionEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="alterationoptionInstance">AlterationoptionEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingAlterationoptionCollectionViaMedium(IEntity alterationoptionInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateDeliverypointgroupDAO().GetMultiUsingAlterationoptionCollectionViaMedium(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, alterationoptionInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  relation of type 'm:n' with the passed in AlterationoptionEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="alterationoptionInstance">AlterationoptionEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingAlterationoptionCollectionViaMedium(IEntity alterationoptionInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateDeliverypointgroupDAO().GetMultiUsingAlterationoptionCollectionViaMedium(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, alterationoptionInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  Relation of type 'm:n' with the passed in CategoryEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="categoryInstance">CategoryEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCategoryCollectionViaAdvertisement(IEntity categoryInstance)
		{
			return GetMultiManyToManyUsingCategoryCollectionViaAdvertisement(categoryInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  relation of type 'm:n' with the passed in CategoryEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="categoryInstance">CategoryEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCategoryCollectionViaAdvertisement(IEntity categoryInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingCategoryCollectionViaAdvertisement(categoryInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a Relation of type 'm:n' with the passed in CategoryEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="categoryInstance">CategoryEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCategoryCollectionViaAdvertisement(IEntity categoryInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingCategoryCollectionViaAdvertisement(categoryInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  relation of type 'm:n' with the passed in CategoryEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="categoryInstance">CategoryEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingCategoryCollectionViaAdvertisement(IEntity categoryInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateDeliverypointgroupDAO().GetMultiUsingCategoryCollectionViaAdvertisement(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, categoryInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  relation of type 'm:n' with the passed in CategoryEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="categoryInstance">CategoryEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCategoryCollectionViaAdvertisement(IEntity categoryInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateDeliverypointgroupDAO().GetMultiUsingCategoryCollectionViaAdvertisement(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, categoryInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  Relation of type 'm:n' with the passed in CategoryEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="categoryInstance">CategoryEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCategoryCollectionViaAnnouncement(IEntity categoryInstance)
		{
			return GetMultiManyToManyUsingCategoryCollectionViaAnnouncement(categoryInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  relation of type 'm:n' with the passed in CategoryEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="categoryInstance">CategoryEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCategoryCollectionViaAnnouncement(IEntity categoryInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingCategoryCollectionViaAnnouncement(categoryInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a Relation of type 'm:n' with the passed in CategoryEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="categoryInstance">CategoryEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCategoryCollectionViaAnnouncement(IEntity categoryInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingCategoryCollectionViaAnnouncement(categoryInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  relation of type 'm:n' with the passed in CategoryEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="categoryInstance">CategoryEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingCategoryCollectionViaAnnouncement(IEntity categoryInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateDeliverypointgroupDAO().GetMultiUsingCategoryCollectionViaAnnouncement(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, categoryInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  relation of type 'm:n' with the passed in CategoryEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="categoryInstance">CategoryEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCategoryCollectionViaAnnouncement(IEntity categoryInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateDeliverypointgroupDAO().GetMultiUsingCategoryCollectionViaAnnouncement(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, categoryInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  Relation of type 'm:n' with the passed in CategoryEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="categoryInstance">CategoryEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCategoryCollectionViaAnnouncement_(IEntity categoryInstance)
		{
			return GetMultiManyToManyUsingCategoryCollectionViaAnnouncement_(categoryInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  relation of type 'm:n' with the passed in CategoryEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="categoryInstance">CategoryEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCategoryCollectionViaAnnouncement_(IEntity categoryInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingCategoryCollectionViaAnnouncement_(categoryInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a Relation of type 'm:n' with the passed in CategoryEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="categoryInstance">CategoryEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCategoryCollectionViaAnnouncement_(IEntity categoryInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingCategoryCollectionViaAnnouncement_(categoryInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  relation of type 'm:n' with the passed in CategoryEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="categoryInstance">CategoryEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingCategoryCollectionViaAnnouncement_(IEntity categoryInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateDeliverypointgroupDAO().GetMultiUsingCategoryCollectionViaAnnouncement_(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, categoryInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  relation of type 'm:n' with the passed in CategoryEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="categoryInstance">CategoryEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCategoryCollectionViaAnnouncement_(IEntity categoryInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateDeliverypointgroupDAO().GetMultiUsingCategoryCollectionViaAnnouncement_(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, categoryInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  Relation of type 'm:n' with the passed in CategoryEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="categoryInstance">CategoryEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCategoryCollectionViaMedium(IEntity categoryInstance)
		{
			return GetMultiManyToManyUsingCategoryCollectionViaMedium(categoryInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  relation of type 'm:n' with the passed in CategoryEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="categoryInstance">CategoryEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCategoryCollectionViaMedium(IEntity categoryInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingCategoryCollectionViaMedium(categoryInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a Relation of type 'm:n' with the passed in CategoryEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="categoryInstance">CategoryEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCategoryCollectionViaMedium(IEntity categoryInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingCategoryCollectionViaMedium(categoryInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  relation of type 'm:n' with the passed in CategoryEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="categoryInstance">CategoryEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingCategoryCollectionViaMedium(IEntity categoryInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateDeliverypointgroupDAO().GetMultiUsingCategoryCollectionViaMedium(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, categoryInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  relation of type 'm:n' with the passed in CategoryEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="categoryInstance">CategoryEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCategoryCollectionViaMedium(IEntity categoryInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateDeliverypointgroupDAO().GetMultiUsingCategoryCollectionViaMedium(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, categoryInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  Relation of type 'm:n' with the passed in ClientEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="clientInstance">ClientEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingClientCollectionViaNetmessage(IEntity clientInstance)
		{
			return GetMultiManyToManyUsingClientCollectionViaNetmessage(clientInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  relation of type 'm:n' with the passed in ClientEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="clientInstance">ClientEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingClientCollectionViaNetmessage(IEntity clientInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingClientCollectionViaNetmessage(clientInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a Relation of type 'm:n' with the passed in ClientEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="clientInstance">ClientEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingClientCollectionViaNetmessage(IEntity clientInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingClientCollectionViaNetmessage(clientInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  relation of type 'm:n' with the passed in ClientEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="clientInstance">ClientEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingClientCollectionViaNetmessage(IEntity clientInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateDeliverypointgroupDAO().GetMultiUsingClientCollectionViaNetmessage(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, clientInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  relation of type 'm:n' with the passed in ClientEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="clientInstance">ClientEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingClientCollectionViaNetmessage(IEntity clientInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateDeliverypointgroupDAO().GetMultiUsingClientCollectionViaNetmessage(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, clientInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  Relation of type 'm:n' with the passed in CompanyEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="companyInstance">CompanyEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCompanyCollectionViaAdvertisement(IEntity companyInstance)
		{
			return GetMultiManyToManyUsingCompanyCollectionViaAdvertisement(companyInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  relation of type 'm:n' with the passed in CompanyEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="companyInstance">CompanyEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCompanyCollectionViaAdvertisement(IEntity companyInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingCompanyCollectionViaAdvertisement(companyInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a Relation of type 'm:n' with the passed in CompanyEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="companyInstance">CompanyEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCompanyCollectionViaAdvertisement(IEntity companyInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingCompanyCollectionViaAdvertisement(companyInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  relation of type 'm:n' with the passed in CompanyEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="companyInstance">CompanyEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingCompanyCollectionViaAdvertisement(IEntity companyInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateDeliverypointgroupDAO().GetMultiUsingCompanyCollectionViaAdvertisement(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, companyInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  relation of type 'm:n' with the passed in CompanyEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="companyInstance">CompanyEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCompanyCollectionViaAdvertisement(IEntity companyInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateDeliverypointgroupDAO().GetMultiUsingCompanyCollectionViaAdvertisement(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, companyInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  Relation of type 'm:n' with the passed in CompanyEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="companyInstance">CompanyEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCompanyCollectionViaAnnouncement(IEntity companyInstance)
		{
			return GetMultiManyToManyUsingCompanyCollectionViaAnnouncement(companyInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  relation of type 'm:n' with the passed in CompanyEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="companyInstance">CompanyEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCompanyCollectionViaAnnouncement(IEntity companyInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingCompanyCollectionViaAnnouncement(companyInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a Relation of type 'm:n' with the passed in CompanyEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="companyInstance">CompanyEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCompanyCollectionViaAnnouncement(IEntity companyInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingCompanyCollectionViaAnnouncement(companyInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  relation of type 'm:n' with the passed in CompanyEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="companyInstance">CompanyEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingCompanyCollectionViaAnnouncement(IEntity companyInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateDeliverypointgroupDAO().GetMultiUsingCompanyCollectionViaAnnouncement(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, companyInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  relation of type 'm:n' with the passed in CompanyEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="companyInstance">CompanyEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCompanyCollectionViaAnnouncement(IEntity companyInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateDeliverypointgroupDAO().GetMultiUsingCompanyCollectionViaAnnouncement(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, companyInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  Relation of type 'm:n' with the passed in CompanyEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="companyInstance">CompanyEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCompanyCollectionViaNetmessage(IEntity companyInstance)
		{
			return GetMultiManyToManyUsingCompanyCollectionViaNetmessage(companyInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  relation of type 'm:n' with the passed in CompanyEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="companyInstance">CompanyEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCompanyCollectionViaNetmessage(IEntity companyInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingCompanyCollectionViaNetmessage(companyInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a Relation of type 'm:n' with the passed in CompanyEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="companyInstance">CompanyEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCompanyCollectionViaNetmessage(IEntity companyInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingCompanyCollectionViaNetmessage(companyInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  relation of type 'm:n' with the passed in CompanyEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="companyInstance">CompanyEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingCompanyCollectionViaNetmessage(IEntity companyInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateDeliverypointgroupDAO().GetMultiUsingCompanyCollectionViaNetmessage(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, companyInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  relation of type 'm:n' with the passed in CompanyEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="companyInstance">CompanyEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCompanyCollectionViaNetmessage(IEntity companyInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateDeliverypointgroupDAO().GetMultiUsingCompanyCollectionViaNetmessage(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, companyInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  Relation of type 'm:n' with the passed in CompanyEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="companyInstance">CompanyEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCompanyCollectionViaNetmessage_(IEntity companyInstance)
		{
			return GetMultiManyToManyUsingCompanyCollectionViaNetmessage_(companyInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  relation of type 'm:n' with the passed in CompanyEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="companyInstance">CompanyEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCompanyCollectionViaNetmessage_(IEntity companyInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingCompanyCollectionViaNetmessage_(companyInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a Relation of type 'm:n' with the passed in CompanyEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="companyInstance">CompanyEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCompanyCollectionViaNetmessage_(IEntity companyInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingCompanyCollectionViaNetmessage_(companyInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  relation of type 'm:n' with the passed in CompanyEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="companyInstance">CompanyEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingCompanyCollectionViaNetmessage_(IEntity companyInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateDeliverypointgroupDAO().GetMultiUsingCompanyCollectionViaNetmessage_(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, companyInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  relation of type 'm:n' with the passed in CompanyEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="companyInstance">CompanyEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCompanyCollectionViaNetmessage_(IEntity companyInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateDeliverypointgroupDAO().GetMultiUsingCompanyCollectionViaNetmessage_(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, companyInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  Relation of type 'm:n' with the passed in CompanyEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="companyInstance">CompanyEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCompanyCollectionViaTerminal(IEntity companyInstance)
		{
			return GetMultiManyToManyUsingCompanyCollectionViaTerminal(companyInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  relation of type 'm:n' with the passed in CompanyEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="companyInstance">CompanyEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCompanyCollectionViaTerminal(IEntity companyInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingCompanyCollectionViaTerminal(companyInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a Relation of type 'm:n' with the passed in CompanyEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="companyInstance">CompanyEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCompanyCollectionViaTerminal(IEntity companyInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingCompanyCollectionViaTerminal(companyInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  relation of type 'm:n' with the passed in CompanyEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="companyInstance">CompanyEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingCompanyCollectionViaTerminal(IEntity companyInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateDeliverypointgroupDAO().GetMultiUsingCompanyCollectionViaTerminal(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, companyInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  relation of type 'm:n' with the passed in CompanyEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="companyInstance">CompanyEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCompanyCollectionViaTerminal(IEntity companyInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateDeliverypointgroupDAO().GetMultiUsingCompanyCollectionViaTerminal(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, companyInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  Relation of type 'm:n' with the passed in CustomerEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="customerInstance">CustomerEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCustomerCollectionViaNetmessage(IEntity customerInstance)
		{
			return GetMultiManyToManyUsingCustomerCollectionViaNetmessage(customerInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  relation of type 'm:n' with the passed in CustomerEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="customerInstance">CustomerEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCustomerCollectionViaNetmessage(IEntity customerInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingCustomerCollectionViaNetmessage(customerInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a Relation of type 'm:n' with the passed in CustomerEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="customerInstance">CustomerEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCustomerCollectionViaNetmessage(IEntity customerInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingCustomerCollectionViaNetmessage(customerInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  relation of type 'm:n' with the passed in CustomerEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="customerInstance">CustomerEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingCustomerCollectionViaNetmessage(IEntity customerInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateDeliverypointgroupDAO().GetMultiUsingCustomerCollectionViaNetmessage(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, customerInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  relation of type 'm:n' with the passed in CustomerEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="customerInstance">CustomerEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCustomerCollectionViaNetmessage(IEntity customerInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateDeliverypointgroupDAO().GetMultiUsingCustomerCollectionViaNetmessage(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, customerInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  Relation of type 'm:n' with the passed in CustomerEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="customerInstance">CustomerEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCustomerCollectionViaNetmessage_(IEntity customerInstance)
		{
			return GetMultiManyToManyUsingCustomerCollectionViaNetmessage_(customerInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  relation of type 'm:n' with the passed in CustomerEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="customerInstance">CustomerEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCustomerCollectionViaNetmessage_(IEntity customerInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingCustomerCollectionViaNetmessage_(customerInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a Relation of type 'm:n' with the passed in CustomerEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="customerInstance">CustomerEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCustomerCollectionViaNetmessage_(IEntity customerInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingCustomerCollectionViaNetmessage_(customerInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  relation of type 'm:n' with the passed in CustomerEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="customerInstance">CustomerEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingCustomerCollectionViaNetmessage_(IEntity customerInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateDeliverypointgroupDAO().GetMultiUsingCustomerCollectionViaNetmessage_(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, customerInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  relation of type 'm:n' with the passed in CustomerEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="customerInstance">CustomerEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCustomerCollectionViaNetmessage_(IEntity customerInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateDeliverypointgroupDAO().GetMultiUsingCustomerCollectionViaNetmessage_(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, customerInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  Relation of type 'm:n' with the passed in DeliverypointEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="deliverypointInstance">DeliverypointEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingDeliverypointCollectionViaClient(IEntity deliverypointInstance)
		{
			return GetMultiManyToManyUsingDeliverypointCollectionViaClient(deliverypointInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  relation of type 'm:n' with the passed in DeliverypointEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="deliverypointInstance">DeliverypointEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingDeliverypointCollectionViaClient(IEntity deliverypointInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingDeliverypointCollectionViaClient(deliverypointInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a Relation of type 'm:n' with the passed in DeliverypointEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="deliverypointInstance">DeliverypointEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingDeliverypointCollectionViaClient(IEntity deliverypointInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingDeliverypointCollectionViaClient(deliverypointInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  relation of type 'm:n' with the passed in DeliverypointEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="deliverypointInstance">DeliverypointEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingDeliverypointCollectionViaClient(IEntity deliverypointInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateDeliverypointgroupDAO().GetMultiUsingDeliverypointCollectionViaClient(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, deliverypointInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  relation of type 'm:n' with the passed in DeliverypointEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="deliverypointInstance">DeliverypointEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingDeliverypointCollectionViaClient(IEntity deliverypointInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateDeliverypointgroupDAO().GetMultiUsingDeliverypointCollectionViaClient(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, deliverypointInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  Relation of type 'm:n' with the passed in DeliverypointEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="deliverypointInstance">DeliverypointEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingDeliverypointCollectionViaClient_(IEntity deliverypointInstance)
		{
			return GetMultiManyToManyUsingDeliverypointCollectionViaClient_(deliverypointInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  relation of type 'm:n' with the passed in DeliverypointEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="deliverypointInstance">DeliverypointEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingDeliverypointCollectionViaClient_(IEntity deliverypointInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingDeliverypointCollectionViaClient_(deliverypointInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a Relation of type 'm:n' with the passed in DeliverypointEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="deliverypointInstance">DeliverypointEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingDeliverypointCollectionViaClient_(IEntity deliverypointInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingDeliverypointCollectionViaClient_(deliverypointInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  relation of type 'm:n' with the passed in DeliverypointEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="deliverypointInstance">DeliverypointEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingDeliverypointCollectionViaClient_(IEntity deliverypointInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateDeliverypointgroupDAO().GetMultiUsingDeliverypointCollectionViaClient_(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, deliverypointInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  relation of type 'm:n' with the passed in DeliverypointEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="deliverypointInstance">DeliverypointEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingDeliverypointCollectionViaClient_(IEntity deliverypointInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateDeliverypointgroupDAO().GetMultiUsingDeliverypointCollectionViaClient_(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, deliverypointInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  Relation of type 'm:n' with the passed in DeliverypointEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="deliverypointInstance">DeliverypointEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingDeliverypointCollectionViaNetmessage(IEntity deliverypointInstance)
		{
			return GetMultiManyToManyUsingDeliverypointCollectionViaNetmessage(deliverypointInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  relation of type 'm:n' with the passed in DeliverypointEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="deliverypointInstance">DeliverypointEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingDeliverypointCollectionViaNetmessage(IEntity deliverypointInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingDeliverypointCollectionViaNetmessage(deliverypointInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a Relation of type 'm:n' with the passed in DeliverypointEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="deliverypointInstance">DeliverypointEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingDeliverypointCollectionViaNetmessage(IEntity deliverypointInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingDeliverypointCollectionViaNetmessage(deliverypointInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  relation of type 'm:n' with the passed in DeliverypointEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="deliverypointInstance">DeliverypointEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingDeliverypointCollectionViaNetmessage(IEntity deliverypointInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateDeliverypointgroupDAO().GetMultiUsingDeliverypointCollectionViaNetmessage(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, deliverypointInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  relation of type 'm:n' with the passed in DeliverypointEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="deliverypointInstance">DeliverypointEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingDeliverypointCollectionViaNetmessage(IEntity deliverypointInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateDeliverypointgroupDAO().GetMultiUsingDeliverypointCollectionViaNetmessage(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, deliverypointInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  Relation of type 'm:n' with the passed in DeliverypointEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="deliverypointInstance">DeliverypointEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingDeliverypointCollectionViaNetmessage_(IEntity deliverypointInstance)
		{
			return GetMultiManyToManyUsingDeliverypointCollectionViaNetmessage_(deliverypointInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  relation of type 'm:n' with the passed in DeliverypointEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="deliverypointInstance">DeliverypointEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingDeliverypointCollectionViaNetmessage_(IEntity deliverypointInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingDeliverypointCollectionViaNetmessage_(deliverypointInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a Relation of type 'm:n' with the passed in DeliverypointEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="deliverypointInstance">DeliverypointEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingDeliverypointCollectionViaNetmessage_(IEntity deliverypointInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingDeliverypointCollectionViaNetmessage_(deliverypointInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  relation of type 'm:n' with the passed in DeliverypointEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="deliverypointInstance">DeliverypointEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingDeliverypointCollectionViaNetmessage_(IEntity deliverypointInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateDeliverypointgroupDAO().GetMultiUsingDeliverypointCollectionViaNetmessage_(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, deliverypointInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  relation of type 'm:n' with the passed in DeliverypointEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="deliverypointInstance">DeliverypointEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingDeliverypointCollectionViaNetmessage_(IEntity deliverypointInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateDeliverypointgroupDAO().GetMultiUsingDeliverypointCollectionViaNetmessage_(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, deliverypointInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  Relation of type 'm:n' with the passed in DeliverypointEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="deliverypointInstance">DeliverypointEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingDeliverypointCollectionViaTerminal(IEntity deliverypointInstance)
		{
			return GetMultiManyToManyUsingDeliverypointCollectionViaTerminal(deliverypointInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  relation of type 'm:n' with the passed in DeliverypointEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="deliverypointInstance">DeliverypointEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingDeliverypointCollectionViaTerminal(IEntity deliverypointInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingDeliverypointCollectionViaTerminal(deliverypointInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a Relation of type 'm:n' with the passed in DeliverypointEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="deliverypointInstance">DeliverypointEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingDeliverypointCollectionViaTerminal(IEntity deliverypointInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingDeliverypointCollectionViaTerminal(deliverypointInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  relation of type 'm:n' with the passed in DeliverypointEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="deliverypointInstance">DeliverypointEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingDeliverypointCollectionViaTerminal(IEntity deliverypointInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateDeliverypointgroupDAO().GetMultiUsingDeliverypointCollectionViaTerminal(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, deliverypointInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  relation of type 'm:n' with the passed in DeliverypointEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="deliverypointInstance">DeliverypointEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingDeliverypointCollectionViaTerminal(IEntity deliverypointInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateDeliverypointgroupDAO().GetMultiUsingDeliverypointCollectionViaTerminal(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, deliverypointInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  Relation of type 'm:n' with the passed in DeliverypointEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="deliverypointInstance">DeliverypointEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingDeliverypointCollectionViaTerminal_(IEntity deliverypointInstance)
		{
			return GetMultiManyToManyUsingDeliverypointCollectionViaTerminal_(deliverypointInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  relation of type 'm:n' with the passed in DeliverypointEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="deliverypointInstance">DeliverypointEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingDeliverypointCollectionViaTerminal_(IEntity deliverypointInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingDeliverypointCollectionViaTerminal_(deliverypointInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a Relation of type 'm:n' with the passed in DeliverypointEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="deliverypointInstance">DeliverypointEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingDeliverypointCollectionViaTerminal_(IEntity deliverypointInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingDeliverypointCollectionViaTerminal_(deliverypointInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  relation of type 'm:n' with the passed in DeliverypointEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="deliverypointInstance">DeliverypointEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingDeliverypointCollectionViaTerminal_(IEntity deliverypointInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateDeliverypointgroupDAO().GetMultiUsingDeliverypointCollectionViaTerminal_(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, deliverypointInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  relation of type 'm:n' with the passed in DeliverypointEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="deliverypointInstance">DeliverypointEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingDeliverypointCollectionViaTerminal_(IEntity deliverypointInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateDeliverypointgroupDAO().GetMultiUsingDeliverypointCollectionViaTerminal_(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, deliverypointInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  Relation of type 'm:n' with the passed in DeviceEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="deviceInstance">DeviceEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingDeviceCollectionViaClient(IEntity deviceInstance)
		{
			return GetMultiManyToManyUsingDeviceCollectionViaClient(deviceInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  relation of type 'm:n' with the passed in DeviceEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="deviceInstance">DeviceEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingDeviceCollectionViaClient(IEntity deviceInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingDeviceCollectionViaClient(deviceInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a Relation of type 'm:n' with the passed in DeviceEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="deviceInstance">DeviceEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingDeviceCollectionViaClient(IEntity deviceInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingDeviceCollectionViaClient(deviceInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  relation of type 'm:n' with the passed in DeviceEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="deviceInstance">DeviceEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingDeviceCollectionViaClient(IEntity deviceInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateDeliverypointgroupDAO().GetMultiUsingDeviceCollectionViaClient(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, deviceInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  relation of type 'm:n' with the passed in DeviceEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="deviceInstance">DeviceEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingDeviceCollectionViaClient(IEntity deviceInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateDeliverypointgroupDAO().GetMultiUsingDeviceCollectionViaClient(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, deviceInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  Relation of type 'm:n' with the passed in DeviceEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="deviceInstance">DeviceEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingDeviceCollectionViaTerminal(IEntity deviceInstance)
		{
			return GetMultiManyToManyUsingDeviceCollectionViaTerminal(deviceInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  relation of type 'm:n' with the passed in DeviceEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="deviceInstance">DeviceEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingDeviceCollectionViaTerminal(IEntity deviceInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingDeviceCollectionViaTerminal(deviceInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a Relation of type 'm:n' with the passed in DeviceEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="deviceInstance">DeviceEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingDeviceCollectionViaTerminal(IEntity deviceInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingDeviceCollectionViaTerminal(deviceInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  relation of type 'm:n' with the passed in DeviceEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="deviceInstance">DeviceEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingDeviceCollectionViaTerminal(IEntity deviceInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateDeliverypointgroupDAO().GetMultiUsingDeviceCollectionViaTerminal(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, deviceInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  relation of type 'm:n' with the passed in DeviceEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="deviceInstance">DeviceEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingDeviceCollectionViaTerminal(IEntity deviceInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateDeliverypointgroupDAO().GetMultiUsingDeviceCollectionViaTerminal(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, deviceInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  Relation of type 'm:n' with the passed in EntertainmentEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="entertainmentInstance">EntertainmentEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingEntertainmentCollectionViaMedium(IEntity entertainmentInstance)
		{
			return GetMultiManyToManyUsingEntertainmentCollectionViaMedium(entertainmentInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  relation of type 'm:n' with the passed in EntertainmentEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="entertainmentInstance">EntertainmentEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingEntertainmentCollectionViaMedium(IEntity entertainmentInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingEntertainmentCollectionViaMedium(entertainmentInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a Relation of type 'm:n' with the passed in EntertainmentEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="entertainmentInstance">EntertainmentEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingEntertainmentCollectionViaMedium(IEntity entertainmentInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingEntertainmentCollectionViaMedium(entertainmentInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  relation of type 'm:n' with the passed in EntertainmentEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="entertainmentInstance">EntertainmentEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingEntertainmentCollectionViaMedium(IEntity entertainmentInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateDeliverypointgroupDAO().GetMultiUsingEntertainmentCollectionViaMedium(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, entertainmentInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  relation of type 'm:n' with the passed in EntertainmentEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="entertainmentInstance">EntertainmentEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingEntertainmentCollectionViaMedium(IEntity entertainmentInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateDeliverypointgroupDAO().GetMultiUsingEntertainmentCollectionViaMedium(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, entertainmentInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  Relation of type 'm:n' with the passed in EntertainmentEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="entertainmentInstance">EntertainmentEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingEntertainmentCollectionViaTerminal(IEntity entertainmentInstance)
		{
			return GetMultiManyToManyUsingEntertainmentCollectionViaTerminal(entertainmentInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  relation of type 'm:n' with the passed in EntertainmentEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="entertainmentInstance">EntertainmentEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingEntertainmentCollectionViaTerminal(IEntity entertainmentInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingEntertainmentCollectionViaTerminal(entertainmentInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a Relation of type 'm:n' with the passed in EntertainmentEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="entertainmentInstance">EntertainmentEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingEntertainmentCollectionViaTerminal(IEntity entertainmentInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingEntertainmentCollectionViaTerminal(entertainmentInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  relation of type 'm:n' with the passed in EntertainmentEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="entertainmentInstance">EntertainmentEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingEntertainmentCollectionViaTerminal(IEntity entertainmentInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateDeliverypointgroupDAO().GetMultiUsingEntertainmentCollectionViaTerminal(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, entertainmentInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  relation of type 'm:n' with the passed in EntertainmentEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="entertainmentInstance">EntertainmentEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingEntertainmentCollectionViaTerminal(IEntity entertainmentInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateDeliverypointgroupDAO().GetMultiUsingEntertainmentCollectionViaTerminal(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, entertainmentInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  Relation of type 'm:n' with the passed in EntertainmentEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="entertainmentInstance">EntertainmentEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingEntertainmentCollectionViaTerminal_(IEntity entertainmentInstance)
		{
			return GetMultiManyToManyUsingEntertainmentCollectionViaTerminal_(entertainmentInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  relation of type 'm:n' with the passed in EntertainmentEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="entertainmentInstance">EntertainmentEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingEntertainmentCollectionViaTerminal_(IEntity entertainmentInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingEntertainmentCollectionViaTerminal_(entertainmentInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a Relation of type 'm:n' with the passed in EntertainmentEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="entertainmentInstance">EntertainmentEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingEntertainmentCollectionViaTerminal_(IEntity entertainmentInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingEntertainmentCollectionViaTerminal_(entertainmentInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  relation of type 'm:n' with the passed in EntertainmentEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="entertainmentInstance">EntertainmentEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingEntertainmentCollectionViaTerminal_(IEntity entertainmentInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateDeliverypointgroupDAO().GetMultiUsingEntertainmentCollectionViaTerminal_(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, entertainmentInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  relation of type 'm:n' with the passed in EntertainmentEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="entertainmentInstance">EntertainmentEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingEntertainmentCollectionViaTerminal_(IEntity entertainmentInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateDeliverypointgroupDAO().GetMultiUsingEntertainmentCollectionViaTerminal_(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, entertainmentInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  Relation of type 'm:n' with the passed in EntertainmentEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="entertainmentInstance">EntertainmentEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingEntertainmentCollectionViaTerminal__(IEntity entertainmentInstance)
		{
			return GetMultiManyToManyUsingEntertainmentCollectionViaTerminal__(entertainmentInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  relation of type 'm:n' with the passed in EntertainmentEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="entertainmentInstance">EntertainmentEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingEntertainmentCollectionViaTerminal__(IEntity entertainmentInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingEntertainmentCollectionViaTerminal__(entertainmentInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a Relation of type 'm:n' with the passed in EntertainmentEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="entertainmentInstance">EntertainmentEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingEntertainmentCollectionViaTerminal__(IEntity entertainmentInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingEntertainmentCollectionViaTerminal__(entertainmentInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  relation of type 'm:n' with the passed in EntertainmentEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="entertainmentInstance">EntertainmentEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingEntertainmentCollectionViaTerminal__(IEntity entertainmentInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateDeliverypointgroupDAO().GetMultiUsingEntertainmentCollectionViaTerminal__(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, entertainmentInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  relation of type 'm:n' with the passed in EntertainmentEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="entertainmentInstance">EntertainmentEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingEntertainmentCollectionViaTerminal__(IEntity entertainmentInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateDeliverypointgroupDAO().GetMultiUsingEntertainmentCollectionViaTerminal__(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, entertainmentInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  Relation of type 'm:n' with the passed in EntertainmentEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="entertainmentInstance">EntertainmentEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingEntertainmentCollectionViaAdvertisement(IEntity entertainmentInstance)
		{
			return GetMultiManyToManyUsingEntertainmentCollectionViaAdvertisement(entertainmentInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  relation of type 'm:n' with the passed in EntertainmentEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="entertainmentInstance">EntertainmentEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingEntertainmentCollectionViaAdvertisement(IEntity entertainmentInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingEntertainmentCollectionViaAdvertisement(entertainmentInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a Relation of type 'm:n' with the passed in EntertainmentEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="entertainmentInstance">EntertainmentEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingEntertainmentCollectionViaAdvertisement(IEntity entertainmentInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingEntertainmentCollectionViaAdvertisement(entertainmentInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  relation of type 'm:n' with the passed in EntertainmentEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="entertainmentInstance">EntertainmentEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingEntertainmentCollectionViaAdvertisement(IEntity entertainmentInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateDeliverypointgroupDAO().GetMultiUsingEntertainmentCollectionViaAdvertisement(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, entertainmentInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  relation of type 'm:n' with the passed in EntertainmentEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="entertainmentInstance">EntertainmentEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingEntertainmentCollectionViaAdvertisement(IEntity entertainmentInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateDeliverypointgroupDAO().GetMultiUsingEntertainmentCollectionViaAdvertisement(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, entertainmentInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  Relation of type 'm:n' with the passed in EntertainmentEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="entertainmentInstance">EntertainmentEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingEntertainmentCollectionViaAdvertisement_(IEntity entertainmentInstance)
		{
			return GetMultiManyToManyUsingEntertainmentCollectionViaAdvertisement_(entertainmentInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  relation of type 'm:n' with the passed in EntertainmentEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="entertainmentInstance">EntertainmentEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingEntertainmentCollectionViaAdvertisement_(IEntity entertainmentInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingEntertainmentCollectionViaAdvertisement_(entertainmentInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a Relation of type 'm:n' with the passed in EntertainmentEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="entertainmentInstance">EntertainmentEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingEntertainmentCollectionViaAdvertisement_(IEntity entertainmentInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingEntertainmentCollectionViaAdvertisement_(entertainmentInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  relation of type 'm:n' with the passed in EntertainmentEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="entertainmentInstance">EntertainmentEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingEntertainmentCollectionViaAdvertisement_(IEntity entertainmentInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateDeliverypointgroupDAO().GetMultiUsingEntertainmentCollectionViaAdvertisement_(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, entertainmentInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  relation of type 'm:n' with the passed in EntertainmentEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="entertainmentInstance">EntertainmentEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingEntertainmentCollectionViaAdvertisement_(IEntity entertainmentInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateDeliverypointgroupDAO().GetMultiUsingEntertainmentCollectionViaAdvertisement_(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, entertainmentInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  Relation of type 'm:n' with the passed in EntertainmentEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="entertainmentInstance">EntertainmentEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingEntertainmentCollectionViaAnnouncement(IEntity entertainmentInstance)
		{
			return GetMultiManyToManyUsingEntertainmentCollectionViaAnnouncement(entertainmentInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  relation of type 'm:n' with the passed in EntertainmentEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="entertainmentInstance">EntertainmentEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingEntertainmentCollectionViaAnnouncement(IEntity entertainmentInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingEntertainmentCollectionViaAnnouncement(entertainmentInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a Relation of type 'm:n' with the passed in EntertainmentEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="entertainmentInstance">EntertainmentEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingEntertainmentCollectionViaAnnouncement(IEntity entertainmentInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingEntertainmentCollectionViaAnnouncement(entertainmentInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  relation of type 'm:n' with the passed in EntertainmentEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="entertainmentInstance">EntertainmentEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingEntertainmentCollectionViaAnnouncement(IEntity entertainmentInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateDeliverypointgroupDAO().GetMultiUsingEntertainmentCollectionViaAnnouncement(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, entertainmentInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  relation of type 'm:n' with the passed in EntertainmentEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="entertainmentInstance">EntertainmentEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingEntertainmentCollectionViaAnnouncement(IEntity entertainmentInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateDeliverypointgroupDAO().GetMultiUsingEntertainmentCollectionViaAnnouncement(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, entertainmentInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  Relation of type 'm:n' with the passed in EntertainmentEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="entertainmentInstance">EntertainmentEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingEntertainmentCollectionViaDeliverypointgroupEntertainment(IEntity entertainmentInstance)
		{
			return GetMultiManyToManyUsingEntertainmentCollectionViaDeliverypointgroupEntertainment(entertainmentInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  relation of type 'm:n' with the passed in EntertainmentEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="entertainmentInstance">EntertainmentEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingEntertainmentCollectionViaDeliverypointgroupEntertainment(IEntity entertainmentInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingEntertainmentCollectionViaDeliverypointgroupEntertainment(entertainmentInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a Relation of type 'm:n' with the passed in EntertainmentEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="entertainmentInstance">EntertainmentEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingEntertainmentCollectionViaDeliverypointgroupEntertainment(IEntity entertainmentInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingEntertainmentCollectionViaDeliverypointgroupEntertainment(entertainmentInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  relation of type 'm:n' with the passed in EntertainmentEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="entertainmentInstance">EntertainmentEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingEntertainmentCollectionViaDeliverypointgroupEntertainment(IEntity entertainmentInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateDeliverypointgroupDAO().GetMultiUsingEntertainmentCollectionViaDeliverypointgroupEntertainment(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, entertainmentInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  relation of type 'm:n' with the passed in EntertainmentEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="entertainmentInstance">EntertainmentEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingEntertainmentCollectionViaDeliverypointgroupEntertainment(IEntity entertainmentInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateDeliverypointgroupDAO().GetMultiUsingEntertainmentCollectionViaDeliverypointgroupEntertainment(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, entertainmentInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  Relation of type 'm:n' with the passed in EntertainmentcategoryEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="entertainmentcategoryInstance">EntertainmentcategoryEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingEntertainmentcategoryCollectionViaMedium(IEntity entertainmentcategoryInstance)
		{
			return GetMultiManyToManyUsingEntertainmentcategoryCollectionViaMedium(entertainmentcategoryInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  relation of type 'm:n' with the passed in EntertainmentcategoryEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="entertainmentcategoryInstance">EntertainmentcategoryEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingEntertainmentcategoryCollectionViaMedium(IEntity entertainmentcategoryInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingEntertainmentcategoryCollectionViaMedium(entertainmentcategoryInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a Relation of type 'm:n' with the passed in EntertainmentcategoryEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="entertainmentcategoryInstance">EntertainmentcategoryEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingEntertainmentcategoryCollectionViaMedium(IEntity entertainmentcategoryInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingEntertainmentcategoryCollectionViaMedium(entertainmentcategoryInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  relation of type 'm:n' with the passed in EntertainmentcategoryEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="entertainmentcategoryInstance">EntertainmentcategoryEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingEntertainmentcategoryCollectionViaMedium(IEntity entertainmentcategoryInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateDeliverypointgroupDAO().GetMultiUsingEntertainmentcategoryCollectionViaMedium(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, entertainmentcategoryInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  relation of type 'm:n' with the passed in EntertainmentcategoryEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="entertainmentcategoryInstance">EntertainmentcategoryEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingEntertainmentcategoryCollectionViaMedium(IEntity entertainmentcategoryInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateDeliverypointgroupDAO().GetMultiUsingEntertainmentcategoryCollectionViaMedium(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, entertainmentcategoryInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  Relation of type 'm:n' with the passed in EntertainmentcategoryEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="entertainmentcategoryInstance">EntertainmentcategoryEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingEntertainmentcategoryCollectionViaAdvertisement(IEntity entertainmentcategoryInstance)
		{
			return GetMultiManyToManyUsingEntertainmentcategoryCollectionViaAdvertisement(entertainmentcategoryInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  relation of type 'm:n' with the passed in EntertainmentcategoryEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="entertainmentcategoryInstance">EntertainmentcategoryEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingEntertainmentcategoryCollectionViaAdvertisement(IEntity entertainmentcategoryInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingEntertainmentcategoryCollectionViaAdvertisement(entertainmentcategoryInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a Relation of type 'm:n' with the passed in EntertainmentcategoryEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="entertainmentcategoryInstance">EntertainmentcategoryEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingEntertainmentcategoryCollectionViaAdvertisement(IEntity entertainmentcategoryInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingEntertainmentcategoryCollectionViaAdvertisement(entertainmentcategoryInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  relation of type 'm:n' with the passed in EntertainmentcategoryEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="entertainmentcategoryInstance">EntertainmentcategoryEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingEntertainmentcategoryCollectionViaAdvertisement(IEntity entertainmentcategoryInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateDeliverypointgroupDAO().GetMultiUsingEntertainmentcategoryCollectionViaAdvertisement(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, entertainmentcategoryInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  relation of type 'm:n' with the passed in EntertainmentcategoryEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="entertainmentcategoryInstance">EntertainmentcategoryEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingEntertainmentcategoryCollectionViaAdvertisement(IEntity entertainmentcategoryInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateDeliverypointgroupDAO().GetMultiUsingEntertainmentcategoryCollectionViaAdvertisement(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, entertainmentcategoryInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  Relation of type 'm:n' with the passed in EntertainmentcategoryEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="entertainmentcategoryInstance">EntertainmentcategoryEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingEntertainmentcategoryCollectionViaAnnouncement(IEntity entertainmentcategoryInstance)
		{
			return GetMultiManyToManyUsingEntertainmentcategoryCollectionViaAnnouncement(entertainmentcategoryInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  relation of type 'm:n' with the passed in EntertainmentcategoryEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="entertainmentcategoryInstance">EntertainmentcategoryEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingEntertainmentcategoryCollectionViaAnnouncement(IEntity entertainmentcategoryInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingEntertainmentcategoryCollectionViaAnnouncement(entertainmentcategoryInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a Relation of type 'm:n' with the passed in EntertainmentcategoryEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="entertainmentcategoryInstance">EntertainmentcategoryEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingEntertainmentcategoryCollectionViaAnnouncement(IEntity entertainmentcategoryInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingEntertainmentcategoryCollectionViaAnnouncement(entertainmentcategoryInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  relation of type 'm:n' with the passed in EntertainmentcategoryEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="entertainmentcategoryInstance">EntertainmentcategoryEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingEntertainmentcategoryCollectionViaAnnouncement(IEntity entertainmentcategoryInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateDeliverypointgroupDAO().GetMultiUsingEntertainmentcategoryCollectionViaAnnouncement(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, entertainmentcategoryInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  relation of type 'm:n' with the passed in EntertainmentcategoryEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="entertainmentcategoryInstance">EntertainmentcategoryEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingEntertainmentcategoryCollectionViaAnnouncement(IEntity entertainmentcategoryInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateDeliverypointgroupDAO().GetMultiUsingEntertainmentcategoryCollectionViaAnnouncement(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, entertainmentcategoryInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  Relation of type 'm:n' with the passed in EntertainmentcategoryEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="entertainmentcategoryInstance">EntertainmentcategoryEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingEntertainmentcategoryCollectionViaAnnouncement_(IEntity entertainmentcategoryInstance)
		{
			return GetMultiManyToManyUsingEntertainmentcategoryCollectionViaAnnouncement_(entertainmentcategoryInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  relation of type 'm:n' with the passed in EntertainmentcategoryEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="entertainmentcategoryInstance">EntertainmentcategoryEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingEntertainmentcategoryCollectionViaAnnouncement_(IEntity entertainmentcategoryInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingEntertainmentcategoryCollectionViaAnnouncement_(entertainmentcategoryInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a Relation of type 'm:n' with the passed in EntertainmentcategoryEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="entertainmentcategoryInstance">EntertainmentcategoryEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingEntertainmentcategoryCollectionViaAnnouncement_(IEntity entertainmentcategoryInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingEntertainmentcategoryCollectionViaAnnouncement_(entertainmentcategoryInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  relation of type 'm:n' with the passed in EntertainmentcategoryEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="entertainmentcategoryInstance">EntertainmentcategoryEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingEntertainmentcategoryCollectionViaAnnouncement_(IEntity entertainmentcategoryInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateDeliverypointgroupDAO().GetMultiUsingEntertainmentcategoryCollectionViaAnnouncement_(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, entertainmentcategoryInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  relation of type 'm:n' with the passed in EntertainmentcategoryEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="entertainmentcategoryInstance">EntertainmentcategoryEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingEntertainmentcategoryCollectionViaAnnouncement_(IEntity entertainmentcategoryInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateDeliverypointgroupDAO().GetMultiUsingEntertainmentcategoryCollectionViaAnnouncement_(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, entertainmentcategoryInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  Relation of type 'm:n' with the passed in GenericproductEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="genericproductInstance">GenericproductEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingGenericproductCollectionViaAdvertisement(IEntity genericproductInstance)
		{
			return GetMultiManyToManyUsingGenericproductCollectionViaAdvertisement(genericproductInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  relation of type 'm:n' with the passed in GenericproductEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="genericproductInstance">GenericproductEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingGenericproductCollectionViaAdvertisement(IEntity genericproductInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingGenericproductCollectionViaAdvertisement(genericproductInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a Relation of type 'm:n' with the passed in GenericproductEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="genericproductInstance">GenericproductEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingGenericproductCollectionViaAdvertisement(IEntity genericproductInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingGenericproductCollectionViaAdvertisement(genericproductInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  relation of type 'm:n' with the passed in GenericproductEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="genericproductInstance">GenericproductEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingGenericproductCollectionViaAdvertisement(IEntity genericproductInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateDeliverypointgroupDAO().GetMultiUsingGenericproductCollectionViaAdvertisement(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, genericproductInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  relation of type 'm:n' with the passed in GenericproductEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="genericproductInstance">GenericproductEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingGenericproductCollectionViaAdvertisement(IEntity genericproductInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateDeliverypointgroupDAO().GetMultiUsingGenericproductCollectionViaAdvertisement(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, genericproductInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  Relation of type 'm:n' with the passed in IcrtouchprintermappingEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="icrtouchprintermappingInstance">IcrtouchprintermappingEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingIcrtouchprintermappingCollectionViaTerminal(IEntity icrtouchprintermappingInstance)
		{
			return GetMultiManyToManyUsingIcrtouchprintermappingCollectionViaTerminal(icrtouchprintermappingInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  relation of type 'm:n' with the passed in IcrtouchprintermappingEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="icrtouchprintermappingInstance">IcrtouchprintermappingEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingIcrtouchprintermappingCollectionViaTerminal(IEntity icrtouchprintermappingInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingIcrtouchprintermappingCollectionViaTerminal(icrtouchprintermappingInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a Relation of type 'm:n' with the passed in IcrtouchprintermappingEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="icrtouchprintermappingInstance">IcrtouchprintermappingEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingIcrtouchprintermappingCollectionViaTerminal(IEntity icrtouchprintermappingInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingIcrtouchprintermappingCollectionViaTerminal(icrtouchprintermappingInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  relation of type 'm:n' with the passed in IcrtouchprintermappingEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="icrtouchprintermappingInstance">IcrtouchprintermappingEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingIcrtouchprintermappingCollectionViaTerminal(IEntity icrtouchprintermappingInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateDeliverypointgroupDAO().GetMultiUsingIcrtouchprintermappingCollectionViaTerminal(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, icrtouchprintermappingInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  relation of type 'm:n' with the passed in IcrtouchprintermappingEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="icrtouchprintermappingInstance">IcrtouchprintermappingEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingIcrtouchprintermappingCollectionViaTerminal(IEntity icrtouchprintermappingInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateDeliverypointgroupDAO().GetMultiUsingIcrtouchprintermappingCollectionViaTerminal(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, icrtouchprintermappingInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  Relation of type 'm:n' with the passed in MediaEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="mediaInstance">MediaEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingMediaCollectionViaAnnouncement(IEntity mediaInstance)
		{
			return GetMultiManyToManyUsingMediaCollectionViaAnnouncement(mediaInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  relation of type 'm:n' with the passed in MediaEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="mediaInstance">MediaEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingMediaCollectionViaAnnouncement(IEntity mediaInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingMediaCollectionViaAnnouncement(mediaInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a Relation of type 'm:n' with the passed in MediaEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="mediaInstance">MediaEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingMediaCollectionViaAnnouncement(IEntity mediaInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingMediaCollectionViaAnnouncement(mediaInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  relation of type 'm:n' with the passed in MediaEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="mediaInstance">MediaEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingMediaCollectionViaAnnouncement(IEntity mediaInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateDeliverypointgroupDAO().GetMultiUsingMediaCollectionViaAnnouncement(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, mediaInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  relation of type 'm:n' with the passed in MediaEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="mediaInstance">MediaEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingMediaCollectionViaAnnouncement(IEntity mediaInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateDeliverypointgroupDAO().GetMultiUsingMediaCollectionViaAnnouncement(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, mediaInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  Relation of type 'm:n' with the passed in PointOfInterestEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="pointOfInterestInstance">PointOfInterestEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingPointOfInterestCollectionViaMedium(IEntity pointOfInterestInstance)
		{
			return GetMultiManyToManyUsingPointOfInterestCollectionViaMedium(pointOfInterestInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  relation of type 'm:n' with the passed in PointOfInterestEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="pointOfInterestInstance">PointOfInterestEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingPointOfInterestCollectionViaMedium(IEntity pointOfInterestInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingPointOfInterestCollectionViaMedium(pointOfInterestInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a Relation of type 'm:n' with the passed in PointOfInterestEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="pointOfInterestInstance">PointOfInterestEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingPointOfInterestCollectionViaMedium(IEntity pointOfInterestInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingPointOfInterestCollectionViaMedium(pointOfInterestInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  relation of type 'm:n' with the passed in PointOfInterestEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="pointOfInterestInstance">PointOfInterestEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingPointOfInterestCollectionViaMedium(IEntity pointOfInterestInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateDeliverypointgroupDAO().GetMultiUsingPointOfInterestCollectionViaMedium(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, pointOfInterestInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  relation of type 'm:n' with the passed in PointOfInterestEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="pointOfInterestInstance">PointOfInterestEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingPointOfInterestCollectionViaMedium(IEntity pointOfInterestInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateDeliverypointgroupDAO().GetMultiUsingPointOfInterestCollectionViaMedium(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, pointOfInterestInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  Relation of type 'm:n' with the passed in ProductEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="productInstance">ProductEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingProductCollectionViaAnnouncement(IEntity productInstance)
		{
			return GetMultiManyToManyUsingProductCollectionViaAnnouncement(productInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  relation of type 'm:n' with the passed in ProductEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="productInstance">ProductEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingProductCollectionViaAnnouncement(IEntity productInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingProductCollectionViaAnnouncement(productInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a Relation of type 'm:n' with the passed in ProductEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="productInstance">ProductEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingProductCollectionViaAnnouncement(IEntity productInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingProductCollectionViaAnnouncement(productInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  relation of type 'm:n' with the passed in ProductEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="productInstance">ProductEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingProductCollectionViaAnnouncement(IEntity productInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateDeliverypointgroupDAO().GetMultiUsingProductCollectionViaAnnouncement(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, productInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  relation of type 'm:n' with the passed in ProductEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="productInstance">ProductEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingProductCollectionViaAnnouncement(IEntity productInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateDeliverypointgroupDAO().GetMultiUsingProductCollectionViaAnnouncement(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, productInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  Relation of type 'm:n' with the passed in ProductEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="productInstance">ProductEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingProductCollectionViaDeliverypointgroupProduct(IEntity productInstance)
		{
			return GetMultiManyToManyUsingProductCollectionViaDeliverypointgroupProduct(productInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  relation of type 'm:n' with the passed in ProductEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="productInstance">ProductEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingProductCollectionViaDeliverypointgroupProduct(IEntity productInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingProductCollectionViaDeliverypointgroupProduct(productInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a Relation of type 'm:n' with the passed in ProductEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="productInstance">ProductEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingProductCollectionViaDeliverypointgroupProduct(IEntity productInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingProductCollectionViaDeliverypointgroupProduct(productInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  relation of type 'm:n' with the passed in ProductEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="productInstance">ProductEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingProductCollectionViaDeliverypointgroupProduct(IEntity productInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateDeliverypointgroupDAO().GetMultiUsingProductCollectionViaDeliverypointgroupProduct(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, productInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  relation of type 'm:n' with the passed in ProductEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="productInstance">ProductEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingProductCollectionViaDeliverypointgroupProduct(IEntity productInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateDeliverypointgroupDAO().GetMultiUsingProductCollectionViaDeliverypointgroupProduct(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, productInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  Relation of type 'm:n' with the passed in ProductEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="productInstance">ProductEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingProductCollectionViaMedium(IEntity productInstance)
		{
			return GetMultiManyToManyUsingProductCollectionViaMedium(productInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  relation of type 'm:n' with the passed in ProductEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="productInstance">ProductEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingProductCollectionViaMedium(IEntity productInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingProductCollectionViaMedium(productInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a Relation of type 'm:n' with the passed in ProductEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="productInstance">ProductEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingProductCollectionViaMedium(IEntity productInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingProductCollectionViaMedium(productInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  relation of type 'm:n' with the passed in ProductEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="productInstance">ProductEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingProductCollectionViaMedium(IEntity productInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateDeliverypointgroupDAO().GetMultiUsingProductCollectionViaMedium(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, productInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  relation of type 'm:n' with the passed in ProductEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="productInstance">ProductEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingProductCollectionViaMedium(IEntity productInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateDeliverypointgroupDAO().GetMultiUsingProductCollectionViaMedium(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, productInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  Relation of type 'm:n' with the passed in ProductEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="productInstance">ProductEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingProductCollectionViaTerminal(IEntity productInstance)
		{
			return GetMultiManyToManyUsingProductCollectionViaTerminal(productInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  relation of type 'm:n' with the passed in ProductEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="productInstance">ProductEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingProductCollectionViaTerminal(IEntity productInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingProductCollectionViaTerminal(productInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a Relation of type 'm:n' with the passed in ProductEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="productInstance">ProductEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingProductCollectionViaTerminal(IEntity productInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingProductCollectionViaTerminal(productInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  relation of type 'm:n' with the passed in ProductEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="productInstance">ProductEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingProductCollectionViaTerminal(IEntity productInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateDeliverypointgroupDAO().GetMultiUsingProductCollectionViaTerminal(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, productInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  relation of type 'm:n' with the passed in ProductEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="productInstance">ProductEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingProductCollectionViaTerminal(IEntity productInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateDeliverypointgroupDAO().GetMultiUsingProductCollectionViaTerminal(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, productInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  Relation of type 'm:n' with the passed in ProductEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="productInstance">ProductEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingProductCollectionViaTerminal_(IEntity productInstance)
		{
			return GetMultiManyToManyUsingProductCollectionViaTerminal_(productInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  relation of type 'm:n' with the passed in ProductEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="productInstance">ProductEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingProductCollectionViaTerminal_(IEntity productInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingProductCollectionViaTerminal_(productInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a Relation of type 'm:n' with the passed in ProductEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="productInstance">ProductEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingProductCollectionViaTerminal_(IEntity productInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingProductCollectionViaTerminal_(productInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  relation of type 'm:n' with the passed in ProductEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="productInstance">ProductEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingProductCollectionViaTerminal_(IEntity productInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateDeliverypointgroupDAO().GetMultiUsingProductCollectionViaTerminal_(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, productInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  relation of type 'm:n' with the passed in ProductEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="productInstance">ProductEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingProductCollectionViaTerminal_(IEntity productInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateDeliverypointgroupDAO().GetMultiUsingProductCollectionViaTerminal_(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, productInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  Relation of type 'm:n' with the passed in ProductEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="productInstance">ProductEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingProductCollectionViaTerminal__(IEntity productInstance)
		{
			return GetMultiManyToManyUsingProductCollectionViaTerminal__(productInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  relation of type 'm:n' with the passed in ProductEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="productInstance">ProductEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingProductCollectionViaTerminal__(IEntity productInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingProductCollectionViaTerminal__(productInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a Relation of type 'm:n' with the passed in ProductEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="productInstance">ProductEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingProductCollectionViaTerminal__(IEntity productInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingProductCollectionViaTerminal__(productInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  relation of type 'm:n' with the passed in ProductEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="productInstance">ProductEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingProductCollectionViaTerminal__(IEntity productInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateDeliverypointgroupDAO().GetMultiUsingProductCollectionViaTerminal__(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, productInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  relation of type 'm:n' with the passed in ProductEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="productInstance">ProductEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingProductCollectionViaTerminal__(IEntity productInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateDeliverypointgroupDAO().GetMultiUsingProductCollectionViaTerminal__(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, productInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  Relation of type 'm:n' with the passed in ProductEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="productInstance">ProductEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingProductCollectionViaTerminal___(IEntity productInstance)
		{
			return GetMultiManyToManyUsingProductCollectionViaTerminal___(productInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  relation of type 'm:n' with the passed in ProductEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="productInstance">ProductEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingProductCollectionViaTerminal___(IEntity productInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingProductCollectionViaTerminal___(productInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a Relation of type 'm:n' with the passed in ProductEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="productInstance">ProductEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingProductCollectionViaTerminal___(IEntity productInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingProductCollectionViaTerminal___(productInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  relation of type 'm:n' with the passed in ProductEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="productInstance">ProductEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingProductCollectionViaTerminal___(IEntity productInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateDeliverypointgroupDAO().GetMultiUsingProductCollectionViaTerminal___(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, productInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  relation of type 'm:n' with the passed in ProductEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="productInstance">ProductEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingProductCollectionViaTerminal___(IEntity productInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateDeliverypointgroupDAO().GetMultiUsingProductCollectionViaTerminal___(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, productInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  Relation of type 'm:n' with the passed in ProductEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="productInstance">ProductEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingProductCollectionViaAdvertisement(IEntity productInstance)
		{
			return GetMultiManyToManyUsingProductCollectionViaAdvertisement(productInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  relation of type 'm:n' with the passed in ProductEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="productInstance">ProductEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingProductCollectionViaAdvertisement(IEntity productInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingProductCollectionViaAdvertisement(productInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a Relation of type 'm:n' with the passed in ProductEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="productInstance">ProductEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingProductCollectionViaAdvertisement(IEntity productInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingProductCollectionViaAdvertisement(productInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  relation of type 'm:n' with the passed in ProductEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="productInstance">ProductEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingProductCollectionViaAdvertisement(IEntity productInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateDeliverypointgroupDAO().GetMultiUsingProductCollectionViaAdvertisement(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, productInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  relation of type 'm:n' with the passed in ProductEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="productInstance">ProductEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingProductCollectionViaAdvertisement(IEntity productInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateDeliverypointgroupDAO().GetMultiUsingProductCollectionViaAdvertisement(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, productInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  Relation of type 'm:n' with the passed in SupplierEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="supplierInstance">SupplierEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingSupplierCollectionViaAdvertisement(IEntity supplierInstance)
		{
			return GetMultiManyToManyUsingSupplierCollectionViaAdvertisement(supplierInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  relation of type 'm:n' with the passed in SupplierEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="supplierInstance">SupplierEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingSupplierCollectionViaAdvertisement(IEntity supplierInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingSupplierCollectionViaAdvertisement(supplierInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a Relation of type 'm:n' with the passed in SupplierEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="supplierInstance">SupplierEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingSupplierCollectionViaAdvertisement(IEntity supplierInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingSupplierCollectionViaAdvertisement(supplierInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  relation of type 'm:n' with the passed in SupplierEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="supplierInstance">SupplierEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingSupplierCollectionViaAdvertisement(IEntity supplierInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateDeliverypointgroupDAO().GetMultiUsingSupplierCollectionViaAdvertisement(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, supplierInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  relation of type 'm:n' with the passed in SupplierEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="supplierInstance">SupplierEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingSupplierCollectionViaAdvertisement(IEntity supplierInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateDeliverypointgroupDAO().GetMultiUsingSupplierCollectionViaAdvertisement(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, supplierInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  Relation of type 'm:n' with the passed in SurveyPageEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="surveyPageInstance">SurveyPageEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingSurveyPageCollectionViaMedium(IEntity surveyPageInstance)
		{
			return GetMultiManyToManyUsingSurveyPageCollectionViaMedium(surveyPageInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  relation of type 'm:n' with the passed in SurveyPageEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="surveyPageInstance">SurveyPageEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingSurveyPageCollectionViaMedium(IEntity surveyPageInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingSurveyPageCollectionViaMedium(surveyPageInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a Relation of type 'm:n' with the passed in SurveyPageEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="surveyPageInstance">SurveyPageEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingSurveyPageCollectionViaMedium(IEntity surveyPageInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingSurveyPageCollectionViaMedium(surveyPageInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  relation of type 'm:n' with the passed in SurveyPageEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="surveyPageInstance">SurveyPageEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingSurveyPageCollectionViaMedium(IEntity surveyPageInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateDeliverypointgroupDAO().GetMultiUsingSurveyPageCollectionViaMedium(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, surveyPageInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  relation of type 'm:n' with the passed in SurveyPageEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="surveyPageInstance">SurveyPageEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingSurveyPageCollectionViaMedium(IEntity surveyPageInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateDeliverypointgroupDAO().GetMultiUsingSurveyPageCollectionViaMedium(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, surveyPageInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  Relation of type 'm:n' with the passed in TerminalEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="terminalInstance">TerminalEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingTerminalCollectionViaNetmessage(IEntity terminalInstance)
		{
			return GetMultiManyToManyUsingTerminalCollectionViaNetmessage(terminalInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  relation of type 'm:n' with the passed in TerminalEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="terminalInstance">TerminalEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingTerminalCollectionViaNetmessage(IEntity terminalInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingTerminalCollectionViaNetmessage(terminalInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a Relation of type 'm:n' with the passed in TerminalEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="terminalInstance">TerminalEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingTerminalCollectionViaNetmessage(IEntity terminalInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingTerminalCollectionViaNetmessage(terminalInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  relation of type 'm:n' with the passed in TerminalEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="terminalInstance">TerminalEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingTerminalCollectionViaNetmessage(IEntity terminalInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateDeliverypointgroupDAO().GetMultiUsingTerminalCollectionViaNetmessage(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, terminalInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  relation of type 'm:n' with the passed in TerminalEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="terminalInstance">TerminalEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingTerminalCollectionViaNetmessage(IEntity terminalInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateDeliverypointgroupDAO().GetMultiUsingTerminalCollectionViaNetmessage(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, terminalInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  Relation of type 'm:n' with the passed in TerminalEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="terminalInstance">TerminalEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingTerminalCollectionViaNetmessage_(IEntity terminalInstance)
		{
			return GetMultiManyToManyUsingTerminalCollectionViaNetmessage_(terminalInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  relation of type 'm:n' with the passed in TerminalEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="terminalInstance">TerminalEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingTerminalCollectionViaNetmessage_(IEntity terminalInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingTerminalCollectionViaNetmessage_(terminalInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a Relation of type 'm:n' with the passed in TerminalEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="terminalInstance">TerminalEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingTerminalCollectionViaNetmessage_(IEntity terminalInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingTerminalCollectionViaNetmessage_(terminalInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  relation of type 'm:n' with the passed in TerminalEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="terminalInstance">TerminalEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingTerminalCollectionViaNetmessage_(IEntity terminalInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateDeliverypointgroupDAO().GetMultiUsingTerminalCollectionViaNetmessage_(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, terminalInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  relation of type 'm:n' with the passed in TerminalEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="terminalInstance">TerminalEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingTerminalCollectionViaNetmessage_(IEntity terminalInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateDeliverypointgroupDAO().GetMultiUsingTerminalCollectionViaNetmessage_(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, terminalInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  Relation of type 'm:n' with the passed in TerminalEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="terminalInstance">TerminalEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingTerminalCollectionViaTerminal(IEntity terminalInstance)
		{
			return GetMultiManyToManyUsingTerminalCollectionViaTerminal(terminalInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  relation of type 'm:n' with the passed in TerminalEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="terminalInstance">TerminalEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingTerminalCollectionViaTerminal(IEntity terminalInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingTerminalCollectionViaTerminal(terminalInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a Relation of type 'm:n' with the passed in TerminalEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="terminalInstance">TerminalEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingTerminalCollectionViaTerminal(IEntity terminalInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingTerminalCollectionViaTerminal(terminalInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  relation of type 'm:n' with the passed in TerminalEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="terminalInstance">TerminalEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingTerminalCollectionViaTerminal(IEntity terminalInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateDeliverypointgroupDAO().GetMultiUsingTerminalCollectionViaTerminal(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, terminalInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  relation of type 'm:n' with the passed in TerminalEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="terminalInstance">TerminalEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingTerminalCollectionViaTerminal(IEntity terminalInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateDeliverypointgroupDAO().GetMultiUsingTerminalCollectionViaTerminal(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, terminalInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  Relation of type 'm:n' with the passed in UIModeEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="uIModeInstance">UIModeEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingUIModeCollectionViaTerminal(IEntity uIModeInstance)
		{
			return GetMultiManyToManyUsingUIModeCollectionViaTerminal(uIModeInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  relation of type 'm:n' with the passed in UIModeEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="uIModeInstance">UIModeEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingUIModeCollectionViaTerminal(IEntity uIModeInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingUIModeCollectionViaTerminal(uIModeInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a Relation of type 'm:n' with the passed in UIModeEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="uIModeInstance">UIModeEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingUIModeCollectionViaTerminal(IEntity uIModeInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingUIModeCollectionViaTerminal(uIModeInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  relation of type 'm:n' with the passed in UIModeEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="uIModeInstance">UIModeEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingUIModeCollectionViaTerminal(IEntity uIModeInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateDeliverypointgroupDAO().GetMultiUsingUIModeCollectionViaTerminal(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, uIModeInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  relation of type 'm:n' with the passed in UIModeEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="uIModeInstance">UIModeEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingUIModeCollectionViaTerminal(IEntity uIModeInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateDeliverypointgroupDAO().GetMultiUsingUIModeCollectionViaTerminal(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, uIModeInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  Relation of type 'm:n' with the passed in UserEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="userInstance">UserEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingUserCollectionViaTerminal(IEntity userInstance)
		{
			return GetMultiManyToManyUsingUserCollectionViaTerminal(userInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  relation of type 'm:n' with the passed in UserEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="userInstance">UserEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingUserCollectionViaTerminal(IEntity userInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingUserCollectionViaTerminal(userInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a Relation of type 'm:n' with the passed in UserEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="userInstance">UserEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingUserCollectionViaTerminal(IEntity userInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingUserCollectionViaTerminal(userInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  relation of type 'm:n' with the passed in UserEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="userInstance">UserEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingUserCollectionViaTerminal(IEntity userInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateDeliverypointgroupDAO().GetMultiUsingUserCollectionViaTerminal(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, userInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this DeliverypointgroupCollection object all DeliverypointgroupEntity objects which are related via a  relation of type 'm:n' with the passed in UserEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="userInstance">UserEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingUserCollectionViaTerminal(IEntity userInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateDeliverypointgroupDAO().GetMultiUsingUserCollectionViaTerminal(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, userInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves Entity rows in a datatable which match the specified filter. It will always create a new connection to the database.</summary>
		/// <param name="selectFilter">A predicate or predicate expression which should be used as filter for the entities to retrieve.</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>DataTable with the rows requested.</returns>
		public static DataTable GetMultiAsDataTable(IPredicate selectFilter, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiAsDataTable(selectFilter, maxNumberOfItemsToReturn, sortClauses, null, 0, 0);
		}

		/// <summary> Retrieves Entity rows in a datatable which match the specified filter. It will always create a new connection to the database.</summary>
		/// <param name="selectFilter">A predicate or predicate expression which should be used as filter for the entities to retrieve.</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="relations">The set of relations to walk to construct to total query.</param>
		/// <returns>DataTable with the rows requested.</returns>
		public static DataTable GetMultiAsDataTable(IPredicate selectFilter, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IRelationCollection relations)
		{
			return GetMultiAsDataTable(selectFilter, maxNumberOfItemsToReturn, sortClauses, relations, 0, 0);
		}
		
		/// <summary> Retrieves Entity rows in a datatable which match the specified filter. It will always create a new connection to the database.</summary>
		/// <param name="selectFilter">A predicate or predicate expression which should be used as filter for the entities to retrieve.</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="relations">The set of relations to walk to construct to total query.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>DataTable with the rows requested.</returns>
		public static DataTable GetMultiAsDataTable(IPredicate selectFilter, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IRelationCollection relations, int pageNumber, int pageSize)
		{
			DeliverypointgroupDAO dao = DAOFactory.CreateDeliverypointgroupDAO();
			return dao.GetMultiAsDataTable(maxNumberOfItemsToReturn, sortClauses, selectFilter, relations, pageNumber, pageSize);
		}


		
		/// <summary> Gets a scalar value, calculated with the aggregate. the field index specified is the field the aggregate are applied on.</summary>
		/// <param name="fieldIndex">Field index of field to which to apply the aggregate function and expression</param>
		/// <param name="aggregateToApply">Aggregate function to apply. </param>
		/// <returns>the scalar value requested</returns>
		public object GetScalar(DeliverypointgroupFieldIndex fieldIndex, AggregateFunction aggregateToApply)
		{
			return GetScalar(fieldIndex, null, aggregateToApply, null, null, null);
		}

		/// <summary> Gets a scalar value, calculated with the aggregate and expression specified. the field index specified is the field the expression and aggregate are applied on.</summary>
		/// <param name="fieldIndex">Field index of field to which to apply the aggregate function and expression</param>
		/// <param name="expressionToExecute">The expression to execute. Can be null</param>
		/// <param name="aggregateToApply">Aggregate function to apply. </param>
		/// <returns>the scalar value requested</returns>
		public object GetScalar(DeliverypointgroupFieldIndex fieldIndex, IExpression expressionToExecute, AggregateFunction aggregateToApply)
		{
			return GetScalar(fieldIndex, expressionToExecute, aggregateToApply, null, null, null);
		}

		/// <summary> Gets a scalar value, calculated with the aggregate and expression specified. the field index specified is the field the expression and aggregate are applied on.</summary>
		/// <param name="fieldIndex">Field index of field to which to apply the aggregate function and expression</param>
		/// <param name="expressionToExecute">The expression to execute. Can be null</param>
		/// <param name="aggregateToApply">Aggregate function to apply. </param>
		/// <param name="filter">The filter to apply to retrieve the scalar</param>
		/// <returns>the scalar value requested</returns>
		public object GetScalar(DeliverypointgroupFieldIndex fieldIndex, IExpression expressionToExecute, AggregateFunction aggregateToApply, IPredicate filter)
		{
			return GetScalar(fieldIndex, expressionToExecute, aggregateToApply, filter, null, null);
		}

		/// <summary> Gets a scalar value, calculated with the aggregate and expression specified. the field index specified is the field the expression and aggregate are applied on.</summary>
		/// <param name="fieldIndex">Field index of field to which to apply the aggregate function and expression</param>
		/// <param name="expressionToExecute">The expression to execute. Can be null</param>
		/// <param name="aggregateToApply">Aggregate function to apply. </param>
		/// <param name="filter">The filter to apply to retrieve the scalar</param>
		/// <param name="groupByClause">The groupby clause to apply to retrieve the scalar</param>
		/// <returns>the scalar value requested</returns>
		public object GetScalar(DeliverypointgroupFieldIndex fieldIndex, IExpression expressionToExecute, AggregateFunction aggregateToApply, IPredicate filter, IGroupByCollection groupByClause)
		{
			return GetScalar(fieldIndex, expressionToExecute, aggregateToApply, filter, null, groupByClause);
		}

		/// <summary> Gets a scalar value, calculated with the aggregate and expression specified. the field index specified is the field the expression and aggregate are applied on.</summary>
		/// <param name="fieldIndex">Field index of field to which to apply the aggregate function and expression</param>
		/// <param name="expressionToExecute">The expression to execute. Can be null</param>
		/// <param name="aggregateToApply">Aggregate function to apply. </param>
		/// <param name="filter">The filter to apply to retrieve the scalar</param>
		/// <param name="relations">The relations to walk</param>
		/// <param name="groupByClause">The groupby clause to apply to retrieve the scalar</param>
		/// <returns>the scalar value requested</returns>
		public virtual object GetScalar(DeliverypointgroupFieldIndex fieldIndex, IExpression expressionToExecute, AggregateFunction aggregateToApply, IPredicate filter, IRelationCollection relations, IGroupByCollection groupByClause)
		{
			EntityFields fields = new EntityFields(1);
			fields[0] = EntityFieldFactory.Create(fieldIndex);
			if((fields[0].ExpressionToApply == null) || (expressionToExecute != null))
			{
				fields[0].ExpressionToApply = expressionToExecute;
			}
			if((fields[0].AggregateFunctionToApply == AggregateFunction.None) || (aggregateToApply != AggregateFunction.None))
			{
				fields[0].AggregateFunctionToApply = aggregateToApply;
			}
			return DAOFactory.CreateDeliverypointgroupDAO().GetScalar(fields, this.Transaction, filter, relations, groupByClause);
		}
		
		/// <summary>Creats a new DAO instance so code which is in the base class can still use the proper DAO object.</summary>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateDeliverypointgroupDAO();
		}
		
		/// <summary>Creates a new transaction object</summary>
		/// <param name="levelOfIsolation">The level of isolation.</param>
		/// <param name="name">The name.</param>
		protected override ITransaction CreateTransaction( IsolationLevel levelOfIsolation, string name )
		{
			return new Transaction(levelOfIsolation, name);
		}

		#region Custom EntityCollection code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCollectionCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion
		
		#region Included Code

		#endregion
	}
}
