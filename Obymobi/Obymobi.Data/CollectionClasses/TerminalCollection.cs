﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Data;
using System.Collections.Generic;
using System.ComponentModel;
using System.Xml;
#if !CF
using System.Runtime.Serialization;
#endif

using Obymobi.Data.EntityClasses;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.CollectionClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Collection class for storing and retrieving collections of TerminalEntity objects. </summary>
	[Serializable]
	public partial class TerminalCollection : CommonEntityCollection<TerminalEntity>
	{
		/// <summary> CTor</summary>
		public TerminalCollection():base(new TerminalEntityFactory())
		{
		}

		/// <summary> CTor</summary>
		/// <param name="initialContents">The initial contents of this collection.</param>
		public TerminalCollection(IEnumerable<TerminalEntity> initialContents):base(new TerminalEntityFactory())
		{
			AddRange(initialContents);
		}

		/// <summary> CTor</summary>
		/// <param name="entityFactoryToUse">The EntityFactory to use when creating entity objects during a GetMulti() call.</param>
		public TerminalCollection(IEntityFactory entityFactoryToUse):base(entityFactoryToUse)
		{
		}

		/// <summary> Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected TerminalCollection(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}


		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which have data in common with the specified related Entities.
		/// If one is omitted, that entity is not used as a filter. All current elements in the collection are removed from the collection.</summary>
		/// <param name="companyEntityInstance">CompanyEntity instance to use as a filter for the TerminalEntity objects to return</param>
		/// <param name="altSystemMessagesDeliverypointEntityInstance">DeliverypointEntity instance to use as a filter for the TerminalEntity objects to return</param>
		/// <param name="systemMessagesDeliverypointEntityInstance">DeliverypointEntity instance to use as a filter for the TerminalEntity objects to return</param>
		/// <param name="deliverypointgroupEntityInstance">DeliverypointgroupEntity instance to use as a filter for the TerminalEntity objects to return</param>
		/// <param name="deviceEntityInstance">DeviceEntity instance to use as a filter for the TerminalEntity objects to return</param>
		/// <param name="browser1EntertainmentEntityInstance">EntertainmentEntity instance to use as a filter for the TerminalEntity objects to return</param>
		/// <param name="browser2EntertainmentEntityInstance">EntertainmentEntity instance to use as a filter for the TerminalEntity objects to return</param>
		/// <param name="cmsPageEntertainmentEntityInstance">EntertainmentEntity instance to use as a filter for the TerminalEntity objects to return</param>
		/// <param name="icrtouchprintermappingEntityInstance">IcrtouchprintermappingEntity instance to use as a filter for the TerminalEntity objects to return</param>
		/// <param name="batteryLowProductEntityInstance">ProductEntity instance to use as a filter for the TerminalEntity objects to return</param>
		/// <param name="clientDisconnectedProductEntityInstance">ProductEntity instance to use as a filter for the TerminalEntity objects to return</param>
		/// <param name="productEntityInstance">ProductEntity instance to use as a filter for the TerminalEntity objects to return</param>
		/// <param name="unlockDeliverypointProductEntityInstance">ProductEntity instance to use as a filter for the TerminalEntity objects to return</param>
		/// <param name="forwardToTerminalEntityInstance">TerminalEntity instance to use as a filter for the TerminalEntity objects to return</param>
		/// <param name="terminalConfigurationEntityInstance">TerminalConfigurationEntity instance to use as a filter for the TerminalEntity objects to return</param>
		/// <param name="uIModeEntityInstance">UIModeEntity instance to use as a filter for the TerminalEntity objects to return</param>
		/// <param name="automaticSignOnUserEntityInstance">UserEntity instance to use as a filter for the TerminalEntity objects to return</param>
		/// <returns>true if succeeded, false otherwise</returns>
		public bool GetMultiManyToOne(IEntity companyEntityInstance, IEntity altSystemMessagesDeliverypointEntityInstance, IEntity systemMessagesDeliverypointEntityInstance, IEntity deliverypointgroupEntityInstance, IEntity deviceEntityInstance, IEntity browser1EntertainmentEntityInstance, IEntity browser2EntertainmentEntityInstance, IEntity cmsPageEntertainmentEntityInstance, IEntity icrtouchprintermappingEntityInstance, IEntity batteryLowProductEntityInstance, IEntity clientDisconnectedProductEntityInstance, IEntity productEntityInstance, IEntity unlockDeliverypointProductEntityInstance, IEntity forwardToTerminalEntityInstance, IEntity terminalConfigurationEntityInstance, IEntity uIModeEntityInstance, IEntity automaticSignOnUserEntityInstance)
		{
			return GetMultiManyToOne(companyEntityInstance, altSystemMessagesDeliverypointEntityInstance, systemMessagesDeliverypointEntityInstance, deliverypointgroupEntityInstance, deviceEntityInstance, browser1EntertainmentEntityInstance, browser2EntertainmentEntityInstance, cmsPageEntertainmentEntityInstance, icrtouchprintermappingEntityInstance, batteryLowProductEntityInstance, clientDisconnectedProductEntityInstance, productEntityInstance, unlockDeliverypointProductEntityInstance, forwardToTerminalEntityInstance, terminalConfigurationEntityInstance, uIModeEntityInstance, automaticSignOnUserEntityInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, null, 0, 0);
		}

		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which have data in common with the specified related Entities.
		/// If one is omitted, that entity is not used as a filter. All current elements in the collection are removed from the collection.</summary>
		/// <param name="companyEntityInstance">CompanyEntity instance to use as a filter for the TerminalEntity objects to return</param>
		/// <param name="altSystemMessagesDeliverypointEntityInstance">DeliverypointEntity instance to use as a filter for the TerminalEntity objects to return</param>
		/// <param name="systemMessagesDeliverypointEntityInstance">DeliverypointEntity instance to use as a filter for the TerminalEntity objects to return</param>
		/// <param name="deliverypointgroupEntityInstance">DeliverypointgroupEntity instance to use as a filter for the TerminalEntity objects to return</param>
		/// <param name="deviceEntityInstance">DeviceEntity instance to use as a filter for the TerminalEntity objects to return</param>
		/// <param name="browser1EntertainmentEntityInstance">EntertainmentEntity instance to use as a filter for the TerminalEntity objects to return</param>
		/// <param name="browser2EntertainmentEntityInstance">EntertainmentEntity instance to use as a filter for the TerminalEntity objects to return</param>
		/// <param name="cmsPageEntertainmentEntityInstance">EntertainmentEntity instance to use as a filter for the TerminalEntity objects to return</param>
		/// <param name="icrtouchprintermappingEntityInstance">IcrtouchprintermappingEntity instance to use as a filter for the TerminalEntity objects to return</param>
		/// <param name="batteryLowProductEntityInstance">ProductEntity instance to use as a filter for the TerminalEntity objects to return</param>
		/// <param name="clientDisconnectedProductEntityInstance">ProductEntity instance to use as a filter for the TerminalEntity objects to return</param>
		/// <param name="productEntityInstance">ProductEntity instance to use as a filter for the TerminalEntity objects to return</param>
		/// <param name="unlockDeliverypointProductEntityInstance">ProductEntity instance to use as a filter for the TerminalEntity objects to return</param>
		/// <param name="forwardToTerminalEntityInstance">TerminalEntity instance to use as a filter for the TerminalEntity objects to return</param>
		/// <param name="terminalConfigurationEntityInstance">TerminalConfigurationEntity instance to use as a filter for the TerminalEntity objects to return</param>
		/// <param name="uIModeEntityInstance">UIModeEntity instance to use as a filter for the TerminalEntity objects to return</param>
		/// <param name="automaticSignOnUserEntityInstance">UserEntity instance to use as a filter for the TerminalEntity objects to return</param>
		/// <param name="filter">Extra filter to limit the resultset. Predicate expression can be null, in which case it will be ignored.</param>
		/// <returns>true if succeeded, false otherwise</returns>
		public bool GetMultiManyToOne(IEntity companyEntityInstance, IEntity altSystemMessagesDeliverypointEntityInstance, IEntity systemMessagesDeliverypointEntityInstance, IEntity deliverypointgroupEntityInstance, IEntity deviceEntityInstance, IEntity browser1EntertainmentEntityInstance, IEntity browser2EntertainmentEntityInstance, IEntity cmsPageEntertainmentEntityInstance, IEntity icrtouchprintermappingEntityInstance, IEntity batteryLowProductEntityInstance, IEntity clientDisconnectedProductEntityInstance, IEntity productEntityInstance, IEntity unlockDeliverypointProductEntityInstance, IEntity forwardToTerminalEntityInstance, IEntity terminalConfigurationEntityInstance, IEntity uIModeEntityInstance, IEntity automaticSignOnUserEntityInstance, IPredicateExpression filter)
		{
			return GetMultiManyToOne(companyEntityInstance, altSystemMessagesDeliverypointEntityInstance, systemMessagesDeliverypointEntityInstance, deliverypointgroupEntityInstance, deviceEntityInstance, browser1EntertainmentEntityInstance, browser2EntertainmentEntityInstance, cmsPageEntertainmentEntityInstance, icrtouchprintermappingEntityInstance, batteryLowProductEntityInstance, clientDisconnectedProductEntityInstance, productEntityInstance, unlockDeliverypointProductEntityInstance, forwardToTerminalEntityInstance, terminalConfigurationEntityInstance, uIModeEntityInstance, automaticSignOnUserEntityInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, filter, 0, 0);
		}

		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which have data in common with the specified related Entities.
		/// If one is omitted, that entity is not used as a filter. All current elements in the collection are removed from the collection.</summary>
		/// <param name="companyEntityInstance">CompanyEntity instance to use as a filter for the TerminalEntity objects to return</param>
		/// <param name="altSystemMessagesDeliverypointEntityInstance">DeliverypointEntity instance to use as a filter for the TerminalEntity objects to return</param>
		/// <param name="systemMessagesDeliverypointEntityInstance">DeliverypointEntity instance to use as a filter for the TerminalEntity objects to return</param>
		/// <param name="deliverypointgroupEntityInstance">DeliverypointgroupEntity instance to use as a filter for the TerminalEntity objects to return</param>
		/// <param name="deviceEntityInstance">DeviceEntity instance to use as a filter for the TerminalEntity objects to return</param>
		/// <param name="browser1EntertainmentEntityInstance">EntertainmentEntity instance to use as a filter for the TerminalEntity objects to return</param>
		/// <param name="browser2EntertainmentEntityInstance">EntertainmentEntity instance to use as a filter for the TerminalEntity objects to return</param>
		/// <param name="cmsPageEntertainmentEntityInstance">EntertainmentEntity instance to use as a filter for the TerminalEntity objects to return</param>
		/// <param name="icrtouchprintermappingEntityInstance">IcrtouchprintermappingEntity instance to use as a filter for the TerminalEntity objects to return</param>
		/// <param name="batteryLowProductEntityInstance">ProductEntity instance to use as a filter for the TerminalEntity objects to return</param>
		/// <param name="clientDisconnectedProductEntityInstance">ProductEntity instance to use as a filter for the TerminalEntity objects to return</param>
		/// <param name="productEntityInstance">ProductEntity instance to use as a filter for the TerminalEntity objects to return</param>
		/// <param name="unlockDeliverypointProductEntityInstance">ProductEntity instance to use as a filter for the TerminalEntity objects to return</param>
		/// <param name="forwardToTerminalEntityInstance">TerminalEntity instance to use as a filter for the TerminalEntity objects to return</param>
		/// <param name="terminalConfigurationEntityInstance">TerminalConfigurationEntity instance to use as a filter for the TerminalEntity objects to return</param>
		/// <param name="uIModeEntityInstance">UIModeEntity instance to use as a filter for the TerminalEntity objects to return</param>
		/// <param name="automaticSignOnUserEntityInstance">UserEntity instance to use as a filter for the TerminalEntity objects to return</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="filter">Extra filter to limit the resultset. Predicate expression can be null, in which case it will be ignored.</param>
		/// <returns>true if succeeded, false otherwise</returns>
		public bool GetMultiManyToOne(IEntity companyEntityInstance, IEntity altSystemMessagesDeliverypointEntityInstance, IEntity systemMessagesDeliverypointEntityInstance, IEntity deliverypointgroupEntityInstance, IEntity deviceEntityInstance, IEntity browser1EntertainmentEntityInstance, IEntity browser2EntertainmentEntityInstance, IEntity cmsPageEntertainmentEntityInstance, IEntity icrtouchprintermappingEntityInstance, IEntity batteryLowProductEntityInstance, IEntity clientDisconnectedProductEntityInstance, IEntity productEntityInstance, IEntity unlockDeliverypointProductEntityInstance, IEntity forwardToTerminalEntityInstance, IEntity terminalConfigurationEntityInstance, IEntity uIModeEntityInstance, IEntity automaticSignOnUserEntityInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPredicateExpression filter)
		{
			return GetMultiManyToOne(companyEntityInstance, altSystemMessagesDeliverypointEntityInstance, systemMessagesDeliverypointEntityInstance, deliverypointgroupEntityInstance, deviceEntityInstance, browser1EntertainmentEntityInstance, browser2EntertainmentEntityInstance, cmsPageEntertainmentEntityInstance, icrtouchprintermappingEntityInstance, batteryLowProductEntityInstance, clientDisconnectedProductEntityInstance, productEntityInstance, unlockDeliverypointProductEntityInstance, forwardToTerminalEntityInstance, terminalConfigurationEntityInstance, uIModeEntityInstance, automaticSignOnUserEntityInstance, maxNumberOfItemsToReturn, sortClauses, filter, 0, 0);
		}

		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which have data in common with the specified related Entities.
		/// If one is omitted, that entity is not used as a filter. All current elements in the collection are removed from the collection.</summary>
		/// <param name="companyEntityInstance">CompanyEntity instance to use as a filter for the TerminalEntity objects to return</param>
		/// <param name="altSystemMessagesDeliverypointEntityInstance">DeliverypointEntity instance to use as a filter for the TerminalEntity objects to return</param>
		/// <param name="systemMessagesDeliverypointEntityInstance">DeliverypointEntity instance to use as a filter for the TerminalEntity objects to return</param>
		/// <param name="deliverypointgroupEntityInstance">DeliverypointgroupEntity instance to use as a filter for the TerminalEntity objects to return</param>
		/// <param name="deviceEntityInstance">DeviceEntity instance to use as a filter for the TerminalEntity objects to return</param>
		/// <param name="browser1EntertainmentEntityInstance">EntertainmentEntity instance to use as a filter for the TerminalEntity objects to return</param>
		/// <param name="browser2EntertainmentEntityInstance">EntertainmentEntity instance to use as a filter for the TerminalEntity objects to return</param>
		/// <param name="cmsPageEntertainmentEntityInstance">EntertainmentEntity instance to use as a filter for the TerminalEntity objects to return</param>
		/// <param name="icrtouchprintermappingEntityInstance">IcrtouchprintermappingEntity instance to use as a filter for the TerminalEntity objects to return</param>
		/// <param name="batteryLowProductEntityInstance">ProductEntity instance to use as a filter for the TerminalEntity objects to return</param>
		/// <param name="clientDisconnectedProductEntityInstance">ProductEntity instance to use as a filter for the TerminalEntity objects to return</param>
		/// <param name="productEntityInstance">ProductEntity instance to use as a filter for the TerminalEntity objects to return</param>
		/// <param name="unlockDeliverypointProductEntityInstance">ProductEntity instance to use as a filter for the TerminalEntity objects to return</param>
		/// <param name="forwardToTerminalEntityInstance">TerminalEntity instance to use as a filter for the TerminalEntity objects to return</param>
		/// <param name="terminalConfigurationEntityInstance">TerminalConfigurationEntity instance to use as a filter for the TerminalEntity objects to return</param>
		/// <param name="uIModeEntityInstance">UIModeEntity instance to use as a filter for the TerminalEntity objects to return</param>
		/// <param name="automaticSignOnUserEntityInstance">UserEntity instance to use as a filter for the TerminalEntity objects to return</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="filter">Extra filter to limit the resultset. Predicate expression can be null, in which case it will be ignored.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToOne(IEntity companyEntityInstance, IEntity altSystemMessagesDeliverypointEntityInstance, IEntity systemMessagesDeliverypointEntityInstance, IEntity deliverypointgroupEntityInstance, IEntity deviceEntityInstance, IEntity browser1EntertainmentEntityInstance, IEntity browser2EntertainmentEntityInstance, IEntity cmsPageEntertainmentEntityInstance, IEntity icrtouchprintermappingEntityInstance, IEntity batteryLowProductEntityInstance, IEntity clientDisconnectedProductEntityInstance, IEntity productEntityInstance, IEntity unlockDeliverypointProductEntityInstance, IEntity forwardToTerminalEntityInstance, IEntity terminalConfigurationEntityInstance, IEntity uIModeEntityInstance, IEntity automaticSignOnUserEntityInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPredicateExpression filter, int pageNumber, int pageSize)
		{
			bool validParameters = false;
			validParameters |= (companyEntityInstance!=null);
			validParameters |= (altSystemMessagesDeliverypointEntityInstance!=null);
			validParameters |= (systemMessagesDeliverypointEntityInstance!=null);
			validParameters |= (deliverypointgroupEntityInstance!=null);
			validParameters |= (deviceEntityInstance!=null);
			validParameters |= (browser1EntertainmentEntityInstance!=null);
			validParameters |= (browser2EntertainmentEntityInstance!=null);
			validParameters |= (cmsPageEntertainmentEntityInstance!=null);
			validParameters |= (icrtouchprintermappingEntityInstance!=null);
			validParameters |= (batteryLowProductEntityInstance!=null);
			validParameters |= (clientDisconnectedProductEntityInstance!=null);
			validParameters |= (productEntityInstance!=null);
			validParameters |= (unlockDeliverypointProductEntityInstance!=null);
			validParameters |= (forwardToTerminalEntityInstance!=null);
			validParameters |= (terminalConfigurationEntityInstance!=null);
			validParameters |= (uIModeEntityInstance!=null);
			validParameters |= (automaticSignOnUserEntityInstance!=null);
			if(!validParameters)
			{
				return GetMulti(filter, maxNumberOfItemsToReturn, sortClauses, null, pageNumber, pageSize);
			}
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateTerminalDAO().GetMulti(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, filter, companyEntityInstance, altSystemMessagesDeliverypointEntityInstance, systemMessagesDeliverypointEntityInstance, deliverypointgroupEntityInstance, deviceEntityInstance, browser1EntertainmentEntityInstance, browser2EntertainmentEntityInstance, cmsPageEntertainmentEntityInstance, icrtouchprintermappingEntityInstance, batteryLowProductEntityInstance, clientDisconnectedProductEntityInstance, productEntityInstance, unlockDeliverypointProductEntityInstance, forwardToTerminalEntityInstance, terminalConfigurationEntityInstance, uIModeEntityInstance, automaticSignOnUserEntityInstance, pageNumber, pageSize);
		}

		/// <summary> Deletes from the persistent storage all Terminal entities which have data in common with the specified related Entities. If one is omitted, that entity is not used as a filter.</summary>
		/// <remarks>Runs directly on the persistent storage. It will not delete entity objects from the current collection.</remarks>
		/// <param name="companyEntityInstance">CompanyEntity instance to use as a filter for the TerminalEntity objects to return</param>
		/// <param name="altSystemMessagesDeliverypointEntityInstance">DeliverypointEntity instance to use as a filter for the TerminalEntity objects to return</param>
		/// <param name="systemMessagesDeliverypointEntityInstance">DeliverypointEntity instance to use as a filter for the TerminalEntity objects to return</param>
		/// <param name="deliverypointgroupEntityInstance">DeliverypointgroupEntity instance to use as a filter for the TerminalEntity objects to return</param>
		/// <param name="deviceEntityInstance">DeviceEntity instance to use as a filter for the TerminalEntity objects to return</param>
		/// <param name="browser1EntertainmentEntityInstance">EntertainmentEntity instance to use as a filter for the TerminalEntity objects to return</param>
		/// <param name="browser2EntertainmentEntityInstance">EntertainmentEntity instance to use as a filter for the TerminalEntity objects to return</param>
		/// <param name="cmsPageEntertainmentEntityInstance">EntertainmentEntity instance to use as a filter for the TerminalEntity objects to return</param>
		/// <param name="icrtouchprintermappingEntityInstance">IcrtouchprintermappingEntity instance to use as a filter for the TerminalEntity objects to return</param>
		/// <param name="batteryLowProductEntityInstance">ProductEntity instance to use as a filter for the TerminalEntity objects to return</param>
		/// <param name="clientDisconnectedProductEntityInstance">ProductEntity instance to use as a filter for the TerminalEntity objects to return</param>
		/// <param name="productEntityInstance">ProductEntity instance to use as a filter for the TerminalEntity objects to return</param>
		/// <param name="unlockDeliverypointProductEntityInstance">ProductEntity instance to use as a filter for the TerminalEntity objects to return</param>
		/// <param name="forwardToTerminalEntityInstance">TerminalEntity instance to use as a filter for the TerminalEntity objects to return</param>
		/// <param name="terminalConfigurationEntityInstance">TerminalConfigurationEntity instance to use as a filter for the TerminalEntity objects to return</param>
		/// <param name="uIModeEntityInstance">UIModeEntity instance to use as a filter for the TerminalEntity objects to return</param>
		/// <param name="automaticSignOnUserEntityInstance">UserEntity instance to use as a filter for the TerminalEntity objects to return</param>
		/// <returns>Amount of entities affected, if the used persistent storage has rowcounting enabled.</returns>
		public int DeleteMultiManyToOne(IEntity companyEntityInstance, IEntity altSystemMessagesDeliverypointEntityInstance, IEntity systemMessagesDeliverypointEntityInstance, IEntity deliverypointgroupEntityInstance, IEntity deviceEntityInstance, IEntity browser1EntertainmentEntityInstance, IEntity browser2EntertainmentEntityInstance, IEntity cmsPageEntertainmentEntityInstance, IEntity icrtouchprintermappingEntityInstance, IEntity batteryLowProductEntityInstance, IEntity clientDisconnectedProductEntityInstance, IEntity productEntityInstance, IEntity unlockDeliverypointProductEntityInstance, IEntity forwardToTerminalEntityInstance, IEntity terminalConfigurationEntityInstance, IEntity uIModeEntityInstance, IEntity automaticSignOnUserEntityInstance)
		{
			return DAOFactory.CreateTerminalDAO().DeleteMulti(this.Transaction, companyEntityInstance, altSystemMessagesDeliverypointEntityInstance, systemMessagesDeliverypointEntityInstance, deliverypointgroupEntityInstance, deviceEntityInstance, browser1EntertainmentEntityInstance, browser2EntertainmentEntityInstance, cmsPageEntertainmentEntityInstance, icrtouchprintermappingEntityInstance, batteryLowProductEntityInstance, clientDisconnectedProductEntityInstance, productEntityInstance, unlockDeliverypointProductEntityInstance, forwardToTerminalEntityInstance, terminalConfigurationEntityInstance, uIModeEntityInstance, automaticSignOnUserEntityInstance);
		}

		/// <summary> Updates in the persistent storage all Terminal entities which have data in common with the specified related Entities. If one is omitted, that entity is not used as a filter.
		/// Which fields are updated in those matching entities depends on which fields are <i>changed</i> in the passed in entity entityWithNewValues. The new values of these fields are read from entityWithNewValues. </summary>
		/// <param name="entityWithNewValues">TerminalEntity instance which holds the new values for the matching entities to update. Only changed fields are taken into account</param>
		/// <param name="companyEntityInstance">CompanyEntity instance to use as a filter for the TerminalEntity objects to return</param>
		/// <param name="altSystemMessagesDeliverypointEntityInstance">DeliverypointEntity instance to use as a filter for the TerminalEntity objects to return</param>
		/// <param name="systemMessagesDeliverypointEntityInstance">DeliverypointEntity instance to use as a filter for the TerminalEntity objects to return</param>
		/// <param name="deliverypointgroupEntityInstance">DeliverypointgroupEntity instance to use as a filter for the TerminalEntity objects to return</param>
		/// <param name="deviceEntityInstance">DeviceEntity instance to use as a filter for the TerminalEntity objects to return</param>
		/// <param name="browser1EntertainmentEntityInstance">EntertainmentEntity instance to use as a filter for the TerminalEntity objects to return</param>
		/// <param name="browser2EntertainmentEntityInstance">EntertainmentEntity instance to use as a filter for the TerminalEntity objects to return</param>
		/// <param name="cmsPageEntertainmentEntityInstance">EntertainmentEntity instance to use as a filter for the TerminalEntity objects to return</param>
		/// <param name="icrtouchprintermappingEntityInstance">IcrtouchprintermappingEntity instance to use as a filter for the TerminalEntity objects to return</param>
		/// <param name="batteryLowProductEntityInstance">ProductEntity instance to use as a filter for the TerminalEntity objects to return</param>
		/// <param name="clientDisconnectedProductEntityInstance">ProductEntity instance to use as a filter for the TerminalEntity objects to return</param>
		/// <param name="productEntityInstance">ProductEntity instance to use as a filter for the TerminalEntity objects to return</param>
		/// <param name="unlockDeliverypointProductEntityInstance">ProductEntity instance to use as a filter for the TerminalEntity objects to return</param>
		/// <param name="forwardToTerminalEntityInstance">TerminalEntity instance to use as a filter for the TerminalEntity objects to return</param>
		/// <param name="terminalConfigurationEntityInstance">TerminalConfigurationEntity instance to use as a filter for the TerminalEntity objects to return</param>
		/// <param name="uIModeEntityInstance">UIModeEntity instance to use as a filter for the TerminalEntity objects to return</param>
		/// <param name="automaticSignOnUserEntityInstance">UserEntity instance to use as a filter for the TerminalEntity objects to return</param>
		/// <returns>Amount of entities affected, if the used persistent storage has rowcounting enabled.</returns>
		public int UpdateMultiManyToOne(TerminalEntity entityWithNewValues, IEntity companyEntityInstance, IEntity altSystemMessagesDeliverypointEntityInstance, IEntity systemMessagesDeliverypointEntityInstance, IEntity deliverypointgroupEntityInstance, IEntity deviceEntityInstance, IEntity browser1EntertainmentEntityInstance, IEntity browser2EntertainmentEntityInstance, IEntity cmsPageEntertainmentEntityInstance, IEntity icrtouchprintermappingEntityInstance, IEntity batteryLowProductEntityInstance, IEntity clientDisconnectedProductEntityInstance, IEntity productEntityInstance, IEntity unlockDeliverypointProductEntityInstance, IEntity forwardToTerminalEntityInstance, IEntity terminalConfigurationEntityInstance, IEntity uIModeEntityInstance, IEntity automaticSignOnUserEntityInstance)
		{
			return DAOFactory.CreateTerminalDAO().UpdateMulti(entityWithNewValues, this.Transaction, companyEntityInstance, altSystemMessagesDeliverypointEntityInstance, systemMessagesDeliverypointEntityInstance, deliverypointgroupEntityInstance, deviceEntityInstance, browser1EntertainmentEntityInstance, browser2EntertainmentEntityInstance, cmsPageEntertainmentEntityInstance, icrtouchprintermappingEntityInstance, batteryLowProductEntityInstance, clientDisconnectedProductEntityInstance, productEntityInstance, unlockDeliverypointProductEntityInstance, forwardToTerminalEntityInstance, terminalConfigurationEntityInstance, uIModeEntityInstance, automaticSignOnUserEntityInstance);
		}

		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  Relation of type 'm:n' with the passed in AnnouncementEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="announcementInstance">AnnouncementEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingAnnouncementCollectionViaDeliverypointgroup(IEntity announcementInstance)
		{
			return GetMultiManyToManyUsingAnnouncementCollectionViaDeliverypointgroup(announcementInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  relation of type 'm:n' with the passed in AnnouncementEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="announcementInstance">AnnouncementEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingAnnouncementCollectionViaDeliverypointgroup(IEntity announcementInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingAnnouncementCollectionViaDeliverypointgroup(announcementInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a Relation of type 'm:n' with the passed in AnnouncementEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="announcementInstance">AnnouncementEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingAnnouncementCollectionViaDeliverypointgroup(IEntity announcementInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingAnnouncementCollectionViaDeliverypointgroup(announcementInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  relation of type 'm:n' with the passed in AnnouncementEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="announcementInstance">AnnouncementEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingAnnouncementCollectionViaDeliverypointgroup(IEntity announcementInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateTerminalDAO().GetMultiUsingAnnouncementCollectionViaDeliverypointgroup(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, announcementInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  relation of type 'm:n' with the passed in AnnouncementEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="announcementInstance">AnnouncementEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingAnnouncementCollectionViaDeliverypointgroup(IEntity announcementInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateTerminalDAO().GetMultiUsingAnnouncementCollectionViaDeliverypointgroup(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, announcementInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  Relation of type 'm:n' with the passed in ClientEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="clientInstance">ClientEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingClientCollectionViaNetmessage(IEntity clientInstance)
		{
			return GetMultiManyToManyUsingClientCollectionViaNetmessage(clientInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  relation of type 'm:n' with the passed in ClientEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="clientInstance">ClientEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingClientCollectionViaNetmessage(IEntity clientInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingClientCollectionViaNetmessage(clientInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a Relation of type 'm:n' with the passed in ClientEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="clientInstance">ClientEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingClientCollectionViaNetmessage(IEntity clientInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingClientCollectionViaNetmessage(clientInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  relation of type 'm:n' with the passed in ClientEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="clientInstance">ClientEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingClientCollectionViaNetmessage(IEntity clientInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateTerminalDAO().GetMultiUsingClientCollectionViaNetmessage(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, clientInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  relation of type 'm:n' with the passed in ClientEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="clientInstance">ClientEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingClientCollectionViaNetmessage(IEntity clientInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateTerminalDAO().GetMultiUsingClientCollectionViaNetmessage(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, clientInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  Relation of type 'm:n' with the passed in CompanyEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="companyInstance">CompanyEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCompanyCollectionViaDeliverypointgroup(IEntity companyInstance)
		{
			return GetMultiManyToManyUsingCompanyCollectionViaDeliverypointgroup(companyInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  relation of type 'm:n' with the passed in CompanyEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="companyInstance">CompanyEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCompanyCollectionViaDeliverypointgroup(IEntity companyInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingCompanyCollectionViaDeliverypointgroup(companyInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a Relation of type 'm:n' with the passed in CompanyEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="companyInstance">CompanyEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCompanyCollectionViaDeliverypointgroup(IEntity companyInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingCompanyCollectionViaDeliverypointgroup(companyInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  relation of type 'm:n' with the passed in CompanyEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="companyInstance">CompanyEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingCompanyCollectionViaDeliverypointgroup(IEntity companyInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateTerminalDAO().GetMultiUsingCompanyCollectionViaDeliverypointgroup(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, companyInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  relation of type 'm:n' with the passed in CompanyEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="companyInstance">CompanyEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCompanyCollectionViaDeliverypointgroup(IEntity companyInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateTerminalDAO().GetMultiUsingCompanyCollectionViaDeliverypointgroup(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, companyInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  Relation of type 'm:n' with the passed in CompanyEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="companyInstance">CompanyEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCompanyCollectionViaNetmessage(IEntity companyInstance)
		{
			return GetMultiManyToManyUsingCompanyCollectionViaNetmessage(companyInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  relation of type 'm:n' with the passed in CompanyEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="companyInstance">CompanyEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCompanyCollectionViaNetmessage(IEntity companyInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingCompanyCollectionViaNetmessage(companyInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a Relation of type 'm:n' with the passed in CompanyEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="companyInstance">CompanyEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCompanyCollectionViaNetmessage(IEntity companyInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingCompanyCollectionViaNetmessage(companyInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  relation of type 'm:n' with the passed in CompanyEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="companyInstance">CompanyEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingCompanyCollectionViaNetmessage(IEntity companyInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateTerminalDAO().GetMultiUsingCompanyCollectionViaNetmessage(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, companyInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  relation of type 'm:n' with the passed in CompanyEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="companyInstance">CompanyEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCompanyCollectionViaNetmessage(IEntity companyInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateTerminalDAO().GetMultiUsingCompanyCollectionViaNetmessage(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, companyInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  Relation of type 'm:n' with the passed in CompanyEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="companyInstance">CompanyEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCompanyCollectionViaNetmessage_(IEntity companyInstance)
		{
			return GetMultiManyToManyUsingCompanyCollectionViaNetmessage_(companyInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  relation of type 'm:n' with the passed in CompanyEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="companyInstance">CompanyEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCompanyCollectionViaNetmessage_(IEntity companyInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingCompanyCollectionViaNetmessage_(companyInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a Relation of type 'm:n' with the passed in CompanyEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="companyInstance">CompanyEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCompanyCollectionViaNetmessage_(IEntity companyInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingCompanyCollectionViaNetmessage_(companyInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  relation of type 'm:n' with the passed in CompanyEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="companyInstance">CompanyEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingCompanyCollectionViaNetmessage_(IEntity companyInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateTerminalDAO().GetMultiUsingCompanyCollectionViaNetmessage_(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, companyInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  relation of type 'm:n' with the passed in CompanyEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="companyInstance">CompanyEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCompanyCollectionViaNetmessage_(IEntity companyInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateTerminalDAO().GetMultiUsingCompanyCollectionViaNetmessage_(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, companyInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  Relation of type 'm:n' with the passed in CompanyEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="companyInstance">CompanyEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCompanyCollectionViaNetmessage__(IEntity companyInstance)
		{
			return GetMultiManyToManyUsingCompanyCollectionViaNetmessage__(companyInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  relation of type 'm:n' with the passed in CompanyEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="companyInstance">CompanyEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCompanyCollectionViaNetmessage__(IEntity companyInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingCompanyCollectionViaNetmessage__(companyInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a Relation of type 'm:n' with the passed in CompanyEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="companyInstance">CompanyEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCompanyCollectionViaNetmessage__(IEntity companyInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingCompanyCollectionViaNetmessage__(companyInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  relation of type 'm:n' with the passed in CompanyEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="companyInstance">CompanyEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingCompanyCollectionViaNetmessage__(IEntity companyInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateTerminalDAO().GetMultiUsingCompanyCollectionViaNetmessage__(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, companyInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  relation of type 'm:n' with the passed in CompanyEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="companyInstance">CompanyEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCompanyCollectionViaNetmessage__(IEntity companyInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateTerminalDAO().GetMultiUsingCompanyCollectionViaNetmessage__(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, companyInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  Relation of type 'm:n' with the passed in CompanyEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="companyInstance">CompanyEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCompanyCollectionViaNetmessage___(IEntity companyInstance)
		{
			return GetMultiManyToManyUsingCompanyCollectionViaNetmessage___(companyInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  relation of type 'm:n' with the passed in CompanyEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="companyInstance">CompanyEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCompanyCollectionViaNetmessage___(IEntity companyInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingCompanyCollectionViaNetmessage___(companyInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a Relation of type 'm:n' with the passed in CompanyEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="companyInstance">CompanyEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCompanyCollectionViaNetmessage___(IEntity companyInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingCompanyCollectionViaNetmessage___(companyInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  relation of type 'm:n' with the passed in CompanyEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="companyInstance">CompanyEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingCompanyCollectionViaNetmessage___(IEntity companyInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateTerminalDAO().GetMultiUsingCompanyCollectionViaNetmessage___(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, companyInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  relation of type 'm:n' with the passed in CompanyEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="companyInstance">CompanyEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCompanyCollectionViaNetmessage___(IEntity companyInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateTerminalDAO().GetMultiUsingCompanyCollectionViaNetmessage___(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, companyInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  Relation of type 'm:n' with the passed in CompanyEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="companyInstance">CompanyEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCompanyCollectionViaTerminal(IEntity companyInstance)
		{
			return GetMultiManyToManyUsingCompanyCollectionViaTerminal(companyInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  relation of type 'm:n' with the passed in CompanyEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="companyInstance">CompanyEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCompanyCollectionViaTerminal(IEntity companyInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingCompanyCollectionViaTerminal(companyInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a Relation of type 'm:n' with the passed in CompanyEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="companyInstance">CompanyEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCompanyCollectionViaTerminal(IEntity companyInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingCompanyCollectionViaTerminal(companyInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  relation of type 'm:n' with the passed in CompanyEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="companyInstance">CompanyEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingCompanyCollectionViaTerminal(IEntity companyInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateTerminalDAO().GetMultiUsingCompanyCollectionViaTerminal(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, companyInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  relation of type 'm:n' with the passed in CompanyEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="companyInstance">CompanyEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCompanyCollectionViaTerminal(IEntity companyInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateTerminalDAO().GetMultiUsingCompanyCollectionViaTerminal(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, companyInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  Relation of type 'm:n' with the passed in CustomerEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="customerInstance">CustomerEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCustomerCollectionViaNetmessage(IEntity customerInstance)
		{
			return GetMultiManyToManyUsingCustomerCollectionViaNetmessage(customerInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  relation of type 'm:n' with the passed in CustomerEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="customerInstance">CustomerEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCustomerCollectionViaNetmessage(IEntity customerInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingCustomerCollectionViaNetmessage(customerInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a Relation of type 'm:n' with the passed in CustomerEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="customerInstance">CustomerEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCustomerCollectionViaNetmessage(IEntity customerInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingCustomerCollectionViaNetmessage(customerInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  relation of type 'm:n' with the passed in CustomerEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="customerInstance">CustomerEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingCustomerCollectionViaNetmessage(IEntity customerInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateTerminalDAO().GetMultiUsingCustomerCollectionViaNetmessage(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, customerInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  relation of type 'm:n' with the passed in CustomerEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="customerInstance">CustomerEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCustomerCollectionViaNetmessage(IEntity customerInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateTerminalDAO().GetMultiUsingCustomerCollectionViaNetmessage(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, customerInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  Relation of type 'm:n' with the passed in CustomerEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="customerInstance">CustomerEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCustomerCollectionViaNetmessage_(IEntity customerInstance)
		{
			return GetMultiManyToManyUsingCustomerCollectionViaNetmessage_(customerInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  relation of type 'm:n' with the passed in CustomerEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="customerInstance">CustomerEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCustomerCollectionViaNetmessage_(IEntity customerInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingCustomerCollectionViaNetmessage_(customerInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a Relation of type 'm:n' with the passed in CustomerEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="customerInstance">CustomerEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCustomerCollectionViaNetmessage_(IEntity customerInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingCustomerCollectionViaNetmessage_(customerInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  relation of type 'm:n' with the passed in CustomerEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="customerInstance">CustomerEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingCustomerCollectionViaNetmessage_(IEntity customerInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateTerminalDAO().GetMultiUsingCustomerCollectionViaNetmessage_(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, customerInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  relation of type 'm:n' with the passed in CustomerEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="customerInstance">CustomerEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCustomerCollectionViaNetmessage_(IEntity customerInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateTerminalDAO().GetMultiUsingCustomerCollectionViaNetmessage_(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, customerInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  Relation of type 'm:n' with the passed in CustomerEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="customerInstance">CustomerEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCustomerCollectionViaNetmessage__(IEntity customerInstance)
		{
			return GetMultiManyToManyUsingCustomerCollectionViaNetmessage__(customerInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  relation of type 'm:n' with the passed in CustomerEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="customerInstance">CustomerEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCustomerCollectionViaNetmessage__(IEntity customerInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingCustomerCollectionViaNetmessage__(customerInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a Relation of type 'm:n' with the passed in CustomerEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="customerInstance">CustomerEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCustomerCollectionViaNetmessage__(IEntity customerInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingCustomerCollectionViaNetmessage__(customerInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  relation of type 'm:n' with the passed in CustomerEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="customerInstance">CustomerEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingCustomerCollectionViaNetmessage__(IEntity customerInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateTerminalDAO().GetMultiUsingCustomerCollectionViaNetmessage__(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, customerInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  relation of type 'm:n' with the passed in CustomerEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="customerInstance">CustomerEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCustomerCollectionViaNetmessage__(IEntity customerInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateTerminalDAO().GetMultiUsingCustomerCollectionViaNetmessage__(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, customerInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  Relation of type 'm:n' with the passed in CustomerEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="customerInstance">CustomerEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCustomerCollectionViaNetmessage___(IEntity customerInstance)
		{
			return GetMultiManyToManyUsingCustomerCollectionViaNetmessage___(customerInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  relation of type 'm:n' with the passed in CustomerEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="customerInstance">CustomerEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCustomerCollectionViaNetmessage___(IEntity customerInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingCustomerCollectionViaNetmessage___(customerInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a Relation of type 'm:n' with the passed in CustomerEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="customerInstance">CustomerEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCustomerCollectionViaNetmessage___(IEntity customerInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingCustomerCollectionViaNetmessage___(customerInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  relation of type 'm:n' with the passed in CustomerEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="customerInstance">CustomerEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingCustomerCollectionViaNetmessage___(IEntity customerInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateTerminalDAO().GetMultiUsingCustomerCollectionViaNetmessage___(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, customerInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  relation of type 'm:n' with the passed in CustomerEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="customerInstance">CustomerEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingCustomerCollectionViaNetmessage___(IEntity customerInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateTerminalDAO().GetMultiUsingCustomerCollectionViaNetmessage___(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, customerInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  Relation of type 'm:n' with the passed in DeliverypointEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="deliverypointInstance">DeliverypointEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingDeliverypointCollectionViaNetmessage(IEntity deliverypointInstance)
		{
			return GetMultiManyToManyUsingDeliverypointCollectionViaNetmessage(deliverypointInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  relation of type 'm:n' with the passed in DeliverypointEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="deliverypointInstance">DeliverypointEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingDeliverypointCollectionViaNetmessage(IEntity deliverypointInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingDeliverypointCollectionViaNetmessage(deliverypointInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a Relation of type 'm:n' with the passed in DeliverypointEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="deliverypointInstance">DeliverypointEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingDeliverypointCollectionViaNetmessage(IEntity deliverypointInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingDeliverypointCollectionViaNetmessage(deliverypointInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  relation of type 'm:n' with the passed in DeliverypointEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="deliverypointInstance">DeliverypointEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingDeliverypointCollectionViaNetmessage(IEntity deliverypointInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateTerminalDAO().GetMultiUsingDeliverypointCollectionViaNetmessage(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, deliverypointInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  relation of type 'm:n' with the passed in DeliverypointEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="deliverypointInstance">DeliverypointEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingDeliverypointCollectionViaNetmessage(IEntity deliverypointInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateTerminalDAO().GetMultiUsingDeliverypointCollectionViaNetmessage(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, deliverypointInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  Relation of type 'm:n' with the passed in DeliverypointEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="deliverypointInstance">DeliverypointEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingDeliverypointCollectionViaNetmessage_(IEntity deliverypointInstance)
		{
			return GetMultiManyToManyUsingDeliverypointCollectionViaNetmessage_(deliverypointInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  relation of type 'm:n' with the passed in DeliverypointEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="deliverypointInstance">DeliverypointEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingDeliverypointCollectionViaNetmessage_(IEntity deliverypointInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingDeliverypointCollectionViaNetmessage_(deliverypointInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a Relation of type 'm:n' with the passed in DeliverypointEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="deliverypointInstance">DeliverypointEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingDeliverypointCollectionViaNetmessage_(IEntity deliverypointInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingDeliverypointCollectionViaNetmessage_(deliverypointInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  relation of type 'm:n' with the passed in DeliverypointEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="deliverypointInstance">DeliverypointEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingDeliverypointCollectionViaNetmessage_(IEntity deliverypointInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateTerminalDAO().GetMultiUsingDeliverypointCollectionViaNetmessage_(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, deliverypointInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  relation of type 'm:n' with the passed in DeliverypointEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="deliverypointInstance">DeliverypointEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingDeliverypointCollectionViaNetmessage_(IEntity deliverypointInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateTerminalDAO().GetMultiUsingDeliverypointCollectionViaNetmessage_(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, deliverypointInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  Relation of type 'm:n' with the passed in DeliverypointEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="deliverypointInstance">DeliverypointEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingDeliverypointCollectionViaNetmessage__(IEntity deliverypointInstance)
		{
			return GetMultiManyToManyUsingDeliverypointCollectionViaNetmessage__(deliverypointInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  relation of type 'm:n' with the passed in DeliverypointEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="deliverypointInstance">DeliverypointEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingDeliverypointCollectionViaNetmessage__(IEntity deliverypointInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingDeliverypointCollectionViaNetmessage__(deliverypointInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a Relation of type 'm:n' with the passed in DeliverypointEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="deliverypointInstance">DeliverypointEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingDeliverypointCollectionViaNetmessage__(IEntity deliverypointInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingDeliverypointCollectionViaNetmessage__(deliverypointInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  relation of type 'm:n' with the passed in DeliverypointEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="deliverypointInstance">DeliverypointEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingDeliverypointCollectionViaNetmessage__(IEntity deliverypointInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateTerminalDAO().GetMultiUsingDeliverypointCollectionViaNetmessage__(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, deliverypointInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  relation of type 'm:n' with the passed in DeliverypointEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="deliverypointInstance">DeliverypointEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingDeliverypointCollectionViaNetmessage__(IEntity deliverypointInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateTerminalDAO().GetMultiUsingDeliverypointCollectionViaNetmessage__(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, deliverypointInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  Relation of type 'm:n' with the passed in DeliverypointEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="deliverypointInstance">DeliverypointEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingDeliverypointCollectionViaNetmessage___(IEntity deliverypointInstance)
		{
			return GetMultiManyToManyUsingDeliverypointCollectionViaNetmessage___(deliverypointInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  relation of type 'm:n' with the passed in DeliverypointEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="deliverypointInstance">DeliverypointEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingDeliverypointCollectionViaNetmessage___(IEntity deliverypointInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingDeliverypointCollectionViaNetmessage___(deliverypointInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a Relation of type 'm:n' with the passed in DeliverypointEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="deliverypointInstance">DeliverypointEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingDeliverypointCollectionViaNetmessage___(IEntity deliverypointInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingDeliverypointCollectionViaNetmessage___(deliverypointInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  relation of type 'm:n' with the passed in DeliverypointEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="deliverypointInstance">DeliverypointEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingDeliverypointCollectionViaNetmessage___(IEntity deliverypointInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateTerminalDAO().GetMultiUsingDeliverypointCollectionViaNetmessage___(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, deliverypointInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  relation of type 'm:n' with the passed in DeliverypointEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="deliverypointInstance">DeliverypointEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingDeliverypointCollectionViaNetmessage___(IEntity deliverypointInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateTerminalDAO().GetMultiUsingDeliverypointCollectionViaNetmessage___(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, deliverypointInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  Relation of type 'm:n' with the passed in DeliverypointEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="deliverypointInstance">DeliverypointEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingDeliverypointCollectionViaTerminal(IEntity deliverypointInstance)
		{
			return GetMultiManyToManyUsingDeliverypointCollectionViaTerminal(deliverypointInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  relation of type 'm:n' with the passed in DeliverypointEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="deliverypointInstance">DeliverypointEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingDeliverypointCollectionViaTerminal(IEntity deliverypointInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingDeliverypointCollectionViaTerminal(deliverypointInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a Relation of type 'm:n' with the passed in DeliverypointEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="deliverypointInstance">DeliverypointEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingDeliverypointCollectionViaTerminal(IEntity deliverypointInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingDeliverypointCollectionViaTerminal(deliverypointInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  relation of type 'm:n' with the passed in DeliverypointEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="deliverypointInstance">DeliverypointEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingDeliverypointCollectionViaTerminal(IEntity deliverypointInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateTerminalDAO().GetMultiUsingDeliverypointCollectionViaTerminal(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, deliverypointInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  relation of type 'm:n' with the passed in DeliverypointEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="deliverypointInstance">DeliverypointEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingDeliverypointCollectionViaTerminal(IEntity deliverypointInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateTerminalDAO().GetMultiUsingDeliverypointCollectionViaTerminal(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, deliverypointInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  Relation of type 'm:n' with the passed in DeliverypointEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="deliverypointInstance">DeliverypointEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingDeliverypointCollectionViaTerminal_(IEntity deliverypointInstance)
		{
			return GetMultiManyToManyUsingDeliverypointCollectionViaTerminal_(deliverypointInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  relation of type 'm:n' with the passed in DeliverypointEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="deliverypointInstance">DeliverypointEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingDeliverypointCollectionViaTerminal_(IEntity deliverypointInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingDeliverypointCollectionViaTerminal_(deliverypointInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a Relation of type 'm:n' with the passed in DeliverypointEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="deliverypointInstance">DeliverypointEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingDeliverypointCollectionViaTerminal_(IEntity deliverypointInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingDeliverypointCollectionViaTerminal_(deliverypointInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  relation of type 'm:n' with the passed in DeliverypointEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="deliverypointInstance">DeliverypointEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingDeliverypointCollectionViaTerminal_(IEntity deliverypointInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateTerminalDAO().GetMultiUsingDeliverypointCollectionViaTerminal_(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, deliverypointInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  relation of type 'm:n' with the passed in DeliverypointEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="deliverypointInstance">DeliverypointEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingDeliverypointCollectionViaTerminal_(IEntity deliverypointInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateTerminalDAO().GetMultiUsingDeliverypointCollectionViaTerminal_(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, deliverypointInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  Relation of type 'm:n' with the passed in DeliverypointgroupEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="deliverypointgroupInstance">DeliverypointgroupEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingDeliverypointgroupCollectionViaNetmessage(IEntity deliverypointgroupInstance)
		{
			return GetMultiManyToManyUsingDeliverypointgroupCollectionViaNetmessage(deliverypointgroupInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  relation of type 'm:n' with the passed in DeliverypointgroupEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="deliverypointgroupInstance">DeliverypointgroupEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingDeliverypointgroupCollectionViaNetmessage(IEntity deliverypointgroupInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingDeliverypointgroupCollectionViaNetmessage(deliverypointgroupInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a Relation of type 'm:n' with the passed in DeliverypointgroupEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="deliverypointgroupInstance">DeliverypointgroupEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingDeliverypointgroupCollectionViaNetmessage(IEntity deliverypointgroupInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingDeliverypointgroupCollectionViaNetmessage(deliverypointgroupInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  relation of type 'm:n' with the passed in DeliverypointgroupEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="deliverypointgroupInstance">DeliverypointgroupEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingDeliverypointgroupCollectionViaNetmessage(IEntity deliverypointgroupInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateTerminalDAO().GetMultiUsingDeliverypointgroupCollectionViaNetmessage(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, deliverypointgroupInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  relation of type 'm:n' with the passed in DeliverypointgroupEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="deliverypointgroupInstance">DeliverypointgroupEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingDeliverypointgroupCollectionViaNetmessage(IEntity deliverypointgroupInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateTerminalDAO().GetMultiUsingDeliverypointgroupCollectionViaNetmessage(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, deliverypointgroupInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  Relation of type 'm:n' with the passed in DeliverypointgroupEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="deliverypointgroupInstance">DeliverypointgroupEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingDeliverypointgroupCollectionViaNetmessage_(IEntity deliverypointgroupInstance)
		{
			return GetMultiManyToManyUsingDeliverypointgroupCollectionViaNetmessage_(deliverypointgroupInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  relation of type 'm:n' with the passed in DeliverypointgroupEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="deliverypointgroupInstance">DeliverypointgroupEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingDeliverypointgroupCollectionViaNetmessage_(IEntity deliverypointgroupInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingDeliverypointgroupCollectionViaNetmessage_(deliverypointgroupInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a Relation of type 'm:n' with the passed in DeliverypointgroupEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="deliverypointgroupInstance">DeliverypointgroupEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingDeliverypointgroupCollectionViaNetmessage_(IEntity deliverypointgroupInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingDeliverypointgroupCollectionViaNetmessage_(deliverypointgroupInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  relation of type 'm:n' with the passed in DeliverypointgroupEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="deliverypointgroupInstance">DeliverypointgroupEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingDeliverypointgroupCollectionViaNetmessage_(IEntity deliverypointgroupInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateTerminalDAO().GetMultiUsingDeliverypointgroupCollectionViaNetmessage_(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, deliverypointgroupInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  relation of type 'm:n' with the passed in DeliverypointgroupEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="deliverypointgroupInstance">DeliverypointgroupEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingDeliverypointgroupCollectionViaNetmessage_(IEntity deliverypointgroupInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateTerminalDAO().GetMultiUsingDeliverypointgroupCollectionViaNetmessage_(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, deliverypointgroupInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  Relation of type 'm:n' with the passed in DeliverypointgroupEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="deliverypointgroupInstance">DeliverypointgroupEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingDeliverypointgroupCollectionViaTerminal(IEntity deliverypointgroupInstance)
		{
			return GetMultiManyToManyUsingDeliverypointgroupCollectionViaTerminal(deliverypointgroupInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  relation of type 'm:n' with the passed in DeliverypointgroupEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="deliverypointgroupInstance">DeliverypointgroupEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingDeliverypointgroupCollectionViaTerminal(IEntity deliverypointgroupInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingDeliverypointgroupCollectionViaTerminal(deliverypointgroupInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a Relation of type 'm:n' with the passed in DeliverypointgroupEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="deliverypointgroupInstance">DeliverypointgroupEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingDeliverypointgroupCollectionViaTerminal(IEntity deliverypointgroupInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingDeliverypointgroupCollectionViaTerminal(deliverypointgroupInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  relation of type 'm:n' with the passed in DeliverypointgroupEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="deliverypointgroupInstance">DeliverypointgroupEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingDeliverypointgroupCollectionViaTerminal(IEntity deliverypointgroupInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateTerminalDAO().GetMultiUsingDeliverypointgroupCollectionViaTerminal(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, deliverypointgroupInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  relation of type 'm:n' with the passed in DeliverypointgroupEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="deliverypointgroupInstance">DeliverypointgroupEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingDeliverypointgroupCollectionViaTerminal(IEntity deliverypointgroupInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateTerminalDAO().GetMultiUsingDeliverypointgroupCollectionViaTerminal(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, deliverypointgroupInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  Relation of type 'm:n' with the passed in DeviceEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="deviceInstance">DeviceEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingDeviceCollectionViaTerminal(IEntity deviceInstance)
		{
			return GetMultiManyToManyUsingDeviceCollectionViaTerminal(deviceInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  relation of type 'm:n' with the passed in DeviceEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="deviceInstance">DeviceEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingDeviceCollectionViaTerminal(IEntity deviceInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingDeviceCollectionViaTerminal(deviceInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a Relation of type 'm:n' with the passed in DeviceEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="deviceInstance">DeviceEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingDeviceCollectionViaTerminal(IEntity deviceInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingDeviceCollectionViaTerminal(deviceInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  relation of type 'm:n' with the passed in DeviceEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="deviceInstance">DeviceEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingDeviceCollectionViaTerminal(IEntity deviceInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateTerminalDAO().GetMultiUsingDeviceCollectionViaTerminal(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, deviceInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  relation of type 'm:n' with the passed in DeviceEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="deviceInstance">DeviceEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingDeviceCollectionViaTerminal(IEntity deviceInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateTerminalDAO().GetMultiUsingDeviceCollectionViaTerminal(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, deviceInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  Relation of type 'm:n' with the passed in EntertainmentEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="entertainmentInstance">EntertainmentEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingEntertainmentCollectionViaTerminal(IEntity entertainmentInstance)
		{
			return GetMultiManyToManyUsingEntertainmentCollectionViaTerminal(entertainmentInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  relation of type 'm:n' with the passed in EntertainmentEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="entertainmentInstance">EntertainmentEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingEntertainmentCollectionViaTerminal(IEntity entertainmentInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingEntertainmentCollectionViaTerminal(entertainmentInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a Relation of type 'm:n' with the passed in EntertainmentEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="entertainmentInstance">EntertainmentEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingEntertainmentCollectionViaTerminal(IEntity entertainmentInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingEntertainmentCollectionViaTerminal(entertainmentInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  relation of type 'm:n' with the passed in EntertainmentEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="entertainmentInstance">EntertainmentEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingEntertainmentCollectionViaTerminal(IEntity entertainmentInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateTerminalDAO().GetMultiUsingEntertainmentCollectionViaTerminal(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, entertainmentInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  relation of type 'm:n' with the passed in EntertainmentEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="entertainmentInstance">EntertainmentEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingEntertainmentCollectionViaTerminal(IEntity entertainmentInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateTerminalDAO().GetMultiUsingEntertainmentCollectionViaTerminal(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, entertainmentInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  Relation of type 'm:n' with the passed in EntertainmentEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="entertainmentInstance">EntertainmentEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingEntertainmentCollectionViaTerminal_(IEntity entertainmentInstance)
		{
			return GetMultiManyToManyUsingEntertainmentCollectionViaTerminal_(entertainmentInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  relation of type 'm:n' with the passed in EntertainmentEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="entertainmentInstance">EntertainmentEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingEntertainmentCollectionViaTerminal_(IEntity entertainmentInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingEntertainmentCollectionViaTerminal_(entertainmentInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a Relation of type 'm:n' with the passed in EntertainmentEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="entertainmentInstance">EntertainmentEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingEntertainmentCollectionViaTerminal_(IEntity entertainmentInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingEntertainmentCollectionViaTerminal_(entertainmentInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  relation of type 'm:n' with the passed in EntertainmentEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="entertainmentInstance">EntertainmentEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingEntertainmentCollectionViaTerminal_(IEntity entertainmentInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateTerminalDAO().GetMultiUsingEntertainmentCollectionViaTerminal_(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, entertainmentInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  relation of type 'm:n' with the passed in EntertainmentEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="entertainmentInstance">EntertainmentEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingEntertainmentCollectionViaTerminal_(IEntity entertainmentInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateTerminalDAO().GetMultiUsingEntertainmentCollectionViaTerminal_(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, entertainmentInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  Relation of type 'm:n' with the passed in EntertainmentEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="entertainmentInstance">EntertainmentEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingEntertainmentCollectionViaTerminal__(IEntity entertainmentInstance)
		{
			return GetMultiManyToManyUsingEntertainmentCollectionViaTerminal__(entertainmentInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  relation of type 'm:n' with the passed in EntertainmentEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="entertainmentInstance">EntertainmentEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingEntertainmentCollectionViaTerminal__(IEntity entertainmentInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingEntertainmentCollectionViaTerminal__(entertainmentInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a Relation of type 'm:n' with the passed in EntertainmentEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="entertainmentInstance">EntertainmentEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingEntertainmentCollectionViaTerminal__(IEntity entertainmentInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingEntertainmentCollectionViaTerminal__(entertainmentInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  relation of type 'm:n' with the passed in EntertainmentEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="entertainmentInstance">EntertainmentEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingEntertainmentCollectionViaTerminal__(IEntity entertainmentInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateTerminalDAO().GetMultiUsingEntertainmentCollectionViaTerminal__(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, entertainmentInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  relation of type 'm:n' with the passed in EntertainmentEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="entertainmentInstance">EntertainmentEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingEntertainmentCollectionViaTerminal__(IEntity entertainmentInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateTerminalDAO().GetMultiUsingEntertainmentCollectionViaTerminal__(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, entertainmentInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  Relation of type 'm:n' with the passed in IcrtouchprintermappingEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="icrtouchprintermappingInstance">IcrtouchprintermappingEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingIcrtouchprintermappingCollectionViaTerminal(IEntity icrtouchprintermappingInstance)
		{
			return GetMultiManyToManyUsingIcrtouchprintermappingCollectionViaTerminal(icrtouchprintermappingInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  relation of type 'm:n' with the passed in IcrtouchprintermappingEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="icrtouchprintermappingInstance">IcrtouchprintermappingEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingIcrtouchprintermappingCollectionViaTerminal(IEntity icrtouchprintermappingInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingIcrtouchprintermappingCollectionViaTerminal(icrtouchprintermappingInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a Relation of type 'm:n' with the passed in IcrtouchprintermappingEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="icrtouchprintermappingInstance">IcrtouchprintermappingEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingIcrtouchprintermappingCollectionViaTerminal(IEntity icrtouchprintermappingInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingIcrtouchprintermappingCollectionViaTerminal(icrtouchprintermappingInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  relation of type 'm:n' with the passed in IcrtouchprintermappingEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="icrtouchprintermappingInstance">IcrtouchprintermappingEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingIcrtouchprintermappingCollectionViaTerminal(IEntity icrtouchprintermappingInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateTerminalDAO().GetMultiUsingIcrtouchprintermappingCollectionViaTerminal(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, icrtouchprintermappingInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  relation of type 'm:n' with the passed in IcrtouchprintermappingEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="icrtouchprintermappingInstance">IcrtouchprintermappingEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingIcrtouchprintermappingCollectionViaTerminal(IEntity icrtouchprintermappingInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateTerminalDAO().GetMultiUsingIcrtouchprintermappingCollectionViaTerminal(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, icrtouchprintermappingInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  Relation of type 'm:n' with the passed in MenuEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="menuInstance">MenuEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingMenuCollectionViaDeliverypointgroup(IEntity menuInstance)
		{
			return GetMultiManyToManyUsingMenuCollectionViaDeliverypointgroup(menuInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  relation of type 'm:n' with the passed in MenuEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="menuInstance">MenuEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingMenuCollectionViaDeliverypointgroup(IEntity menuInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingMenuCollectionViaDeliverypointgroup(menuInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a Relation of type 'm:n' with the passed in MenuEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="menuInstance">MenuEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingMenuCollectionViaDeliverypointgroup(IEntity menuInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingMenuCollectionViaDeliverypointgroup(menuInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  relation of type 'm:n' with the passed in MenuEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="menuInstance">MenuEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingMenuCollectionViaDeliverypointgroup(IEntity menuInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateTerminalDAO().GetMultiUsingMenuCollectionViaDeliverypointgroup(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, menuInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  relation of type 'm:n' with the passed in MenuEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="menuInstance">MenuEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingMenuCollectionViaDeliverypointgroup(IEntity menuInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateTerminalDAO().GetMultiUsingMenuCollectionViaDeliverypointgroup(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, menuInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  Relation of type 'm:n' with the passed in OrderEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="orderInstance">OrderEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingOrderCollectionViaOrderRoutestephandler(IEntity orderInstance)
		{
			return GetMultiManyToManyUsingOrderCollectionViaOrderRoutestephandler(orderInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  relation of type 'm:n' with the passed in OrderEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="orderInstance">OrderEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingOrderCollectionViaOrderRoutestephandler(IEntity orderInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingOrderCollectionViaOrderRoutestephandler(orderInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a Relation of type 'm:n' with the passed in OrderEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="orderInstance">OrderEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingOrderCollectionViaOrderRoutestephandler(IEntity orderInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingOrderCollectionViaOrderRoutestephandler(orderInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  relation of type 'm:n' with the passed in OrderEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="orderInstance">OrderEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingOrderCollectionViaOrderRoutestephandler(IEntity orderInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateTerminalDAO().GetMultiUsingOrderCollectionViaOrderRoutestephandler(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, orderInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  relation of type 'm:n' with the passed in OrderEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="orderInstance">OrderEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingOrderCollectionViaOrderRoutestephandler(IEntity orderInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateTerminalDAO().GetMultiUsingOrderCollectionViaOrderRoutestephandler(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, orderInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  Relation of type 'm:n' with the passed in OrderEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="orderInstance">OrderEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingOrderCollectionViaOrderRoutestephandler_(IEntity orderInstance)
		{
			return GetMultiManyToManyUsingOrderCollectionViaOrderRoutestephandler_(orderInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  relation of type 'm:n' with the passed in OrderEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="orderInstance">OrderEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingOrderCollectionViaOrderRoutestephandler_(IEntity orderInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingOrderCollectionViaOrderRoutestephandler_(orderInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a Relation of type 'm:n' with the passed in OrderEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="orderInstance">OrderEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingOrderCollectionViaOrderRoutestephandler_(IEntity orderInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingOrderCollectionViaOrderRoutestephandler_(orderInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  relation of type 'm:n' with the passed in OrderEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="orderInstance">OrderEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingOrderCollectionViaOrderRoutestephandler_(IEntity orderInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateTerminalDAO().GetMultiUsingOrderCollectionViaOrderRoutestephandler_(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, orderInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  relation of type 'm:n' with the passed in OrderEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="orderInstance">OrderEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingOrderCollectionViaOrderRoutestephandler_(IEntity orderInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateTerminalDAO().GetMultiUsingOrderCollectionViaOrderRoutestephandler_(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, orderInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  Relation of type 'm:n' with the passed in OrderEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="orderInstance">OrderEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingOrderCollectionViaOrderRoutestephandlerHistory(IEntity orderInstance)
		{
			return GetMultiManyToManyUsingOrderCollectionViaOrderRoutestephandlerHistory(orderInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  relation of type 'm:n' with the passed in OrderEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="orderInstance">OrderEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingOrderCollectionViaOrderRoutestephandlerHistory(IEntity orderInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingOrderCollectionViaOrderRoutestephandlerHistory(orderInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a Relation of type 'm:n' with the passed in OrderEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="orderInstance">OrderEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingOrderCollectionViaOrderRoutestephandlerHistory(IEntity orderInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingOrderCollectionViaOrderRoutestephandlerHistory(orderInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  relation of type 'm:n' with the passed in OrderEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="orderInstance">OrderEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingOrderCollectionViaOrderRoutestephandlerHistory(IEntity orderInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateTerminalDAO().GetMultiUsingOrderCollectionViaOrderRoutestephandlerHistory(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, orderInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  relation of type 'm:n' with the passed in OrderEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="orderInstance">OrderEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingOrderCollectionViaOrderRoutestephandlerHistory(IEntity orderInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateTerminalDAO().GetMultiUsingOrderCollectionViaOrderRoutestephandlerHistory(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, orderInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  Relation of type 'm:n' with the passed in OrderEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="orderInstance">OrderEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingOrderCollectionViaOrderRoutestephandlerHistory_(IEntity orderInstance)
		{
			return GetMultiManyToManyUsingOrderCollectionViaOrderRoutestephandlerHistory_(orderInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  relation of type 'm:n' with the passed in OrderEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="orderInstance">OrderEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingOrderCollectionViaOrderRoutestephandlerHistory_(IEntity orderInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingOrderCollectionViaOrderRoutestephandlerHistory_(orderInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a Relation of type 'm:n' with the passed in OrderEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="orderInstance">OrderEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingOrderCollectionViaOrderRoutestephandlerHistory_(IEntity orderInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingOrderCollectionViaOrderRoutestephandlerHistory_(orderInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  relation of type 'm:n' with the passed in OrderEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="orderInstance">OrderEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingOrderCollectionViaOrderRoutestephandlerHistory_(IEntity orderInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateTerminalDAO().GetMultiUsingOrderCollectionViaOrderRoutestephandlerHistory_(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, orderInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  relation of type 'm:n' with the passed in OrderEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="orderInstance">OrderEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingOrderCollectionViaOrderRoutestephandlerHistory_(IEntity orderInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateTerminalDAO().GetMultiUsingOrderCollectionViaOrderRoutestephandlerHistory_(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, orderInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  Relation of type 'm:n' with the passed in PosdeliverypointgroupEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="posdeliverypointgroupInstance">PosdeliverypointgroupEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingPosdeliverypointgroupCollectionViaDeliverypointgroup(IEntity posdeliverypointgroupInstance)
		{
			return GetMultiManyToManyUsingPosdeliverypointgroupCollectionViaDeliverypointgroup(posdeliverypointgroupInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  relation of type 'm:n' with the passed in PosdeliverypointgroupEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="posdeliverypointgroupInstance">PosdeliverypointgroupEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingPosdeliverypointgroupCollectionViaDeliverypointgroup(IEntity posdeliverypointgroupInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingPosdeliverypointgroupCollectionViaDeliverypointgroup(posdeliverypointgroupInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a Relation of type 'm:n' with the passed in PosdeliverypointgroupEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="posdeliverypointgroupInstance">PosdeliverypointgroupEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingPosdeliverypointgroupCollectionViaDeliverypointgroup(IEntity posdeliverypointgroupInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingPosdeliverypointgroupCollectionViaDeliverypointgroup(posdeliverypointgroupInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  relation of type 'm:n' with the passed in PosdeliverypointgroupEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="posdeliverypointgroupInstance">PosdeliverypointgroupEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingPosdeliverypointgroupCollectionViaDeliverypointgroup(IEntity posdeliverypointgroupInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateTerminalDAO().GetMultiUsingPosdeliverypointgroupCollectionViaDeliverypointgroup(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, posdeliverypointgroupInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  relation of type 'm:n' with the passed in PosdeliverypointgroupEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="posdeliverypointgroupInstance">PosdeliverypointgroupEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingPosdeliverypointgroupCollectionViaDeliverypointgroup(IEntity posdeliverypointgroupInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateTerminalDAO().GetMultiUsingPosdeliverypointgroupCollectionViaDeliverypointgroup(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, posdeliverypointgroupInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  Relation of type 'm:n' with the passed in ProductEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="productInstance">ProductEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingProductCollectionViaTerminal(IEntity productInstance)
		{
			return GetMultiManyToManyUsingProductCollectionViaTerminal(productInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  relation of type 'm:n' with the passed in ProductEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="productInstance">ProductEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingProductCollectionViaTerminal(IEntity productInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingProductCollectionViaTerminal(productInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a Relation of type 'm:n' with the passed in ProductEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="productInstance">ProductEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingProductCollectionViaTerminal(IEntity productInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingProductCollectionViaTerminal(productInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  relation of type 'm:n' with the passed in ProductEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="productInstance">ProductEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingProductCollectionViaTerminal(IEntity productInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateTerminalDAO().GetMultiUsingProductCollectionViaTerminal(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, productInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  relation of type 'm:n' with the passed in ProductEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="productInstance">ProductEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingProductCollectionViaTerminal(IEntity productInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateTerminalDAO().GetMultiUsingProductCollectionViaTerminal(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, productInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  Relation of type 'm:n' with the passed in ProductEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="productInstance">ProductEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingProductCollectionViaTerminal_(IEntity productInstance)
		{
			return GetMultiManyToManyUsingProductCollectionViaTerminal_(productInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  relation of type 'm:n' with the passed in ProductEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="productInstance">ProductEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingProductCollectionViaTerminal_(IEntity productInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingProductCollectionViaTerminal_(productInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a Relation of type 'm:n' with the passed in ProductEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="productInstance">ProductEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingProductCollectionViaTerminal_(IEntity productInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingProductCollectionViaTerminal_(productInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  relation of type 'm:n' with the passed in ProductEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="productInstance">ProductEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingProductCollectionViaTerminal_(IEntity productInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateTerminalDAO().GetMultiUsingProductCollectionViaTerminal_(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, productInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  relation of type 'm:n' with the passed in ProductEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="productInstance">ProductEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingProductCollectionViaTerminal_(IEntity productInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateTerminalDAO().GetMultiUsingProductCollectionViaTerminal_(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, productInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  Relation of type 'm:n' with the passed in ProductEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="productInstance">ProductEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingProductCollectionViaTerminal__(IEntity productInstance)
		{
			return GetMultiManyToManyUsingProductCollectionViaTerminal__(productInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  relation of type 'm:n' with the passed in ProductEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="productInstance">ProductEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingProductCollectionViaTerminal__(IEntity productInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingProductCollectionViaTerminal__(productInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a Relation of type 'm:n' with the passed in ProductEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="productInstance">ProductEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingProductCollectionViaTerminal__(IEntity productInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingProductCollectionViaTerminal__(productInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  relation of type 'm:n' with the passed in ProductEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="productInstance">ProductEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingProductCollectionViaTerminal__(IEntity productInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateTerminalDAO().GetMultiUsingProductCollectionViaTerminal__(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, productInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  relation of type 'm:n' with the passed in ProductEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="productInstance">ProductEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingProductCollectionViaTerminal__(IEntity productInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateTerminalDAO().GetMultiUsingProductCollectionViaTerminal__(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, productInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  Relation of type 'm:n' with the passed in ProductEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="productInstance">ProductEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingProductCollectionViaTerminal___(IEntity productInstance)
		{
			return GetMultiManyToManyUsingProductCollectionViaTerminal___(productInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  relation of type 'm:n' with the passed in ProductEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="productInstance">ProductEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingProductCollectionViaTerminal___(IEntity productInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingProductCollectionViaTerminal___(productInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a Relation of type 'm:n' with the passed in ProductEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="productInstance">ProductEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingProductCollectionViaTerminal___(IEntity productInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingProductCollectionViaTerminal___(productInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  relation of type 'm:n' with the passed in ProductEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="productInstance">ProductEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingProductCollectionViaTerminal___(IEntity productInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateTerminalDAO().GetMultiUsingProductCollectionViaTerminal___(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, productInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  relation of type 'm:n' with the passed in ProductEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="productInstance">ProductEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingProductCollectionViaTerminal___(IEntity productInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateTerminalDAO().GetMultiUsingProductCollectionViaTerminal___(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, productInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  Relation of type 'm:n' with the passed in RouteEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="routeInstance">RouteEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingRouteCollectionViaDeliverypointgroup(IEntity routeInstance)
		{
			return GetMultiManyToManyUsingRouteCollectionViaDeliverypointgroup(routeInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  relation of type 'm:n' with the passed in RouteEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="routeInstance">RouteEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingRouteCollectionViaDeliverypointgroup(IEntity routeInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingRouteCollectionViaDeliverypointgroup(routeInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a Relation of type 'm:n' with the passed in RouteEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="routeInstance">RouteEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingRouteCollectionViaDeliverypointgroup(IEntity routeInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingRouteCollectionViaDeliverypointgroup(routeInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  relation of type 'm:n' with the passed in RouteEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="routeInstance">RouteEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingRouteCollectionViaDeliverypointgroup(IEntity routeInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateTerminalDAO().GetMultiUsingRouteCollectionViaDeliverypointgroup(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, routeInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  relation of type 'm:n' with the passed in RouteEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="routeInstance">RouteEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingRouteCollectionViaDeliverypointgroup(IEntity routeInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateTerminalDAO().GetMultiUsingRouteCollectionViaDeliverypointgroup(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, routeInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  Relation of type 'm:n' with the passed in RouteEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="routeInstance">RouteEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingRouteCollectionViaDeliverypointgroup_(IEntity routeInstance)
		{
			return GetMultiManyToManyUsingRouteCollectionViaDeliverypointgroup_(routeInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  relation of type 'm:n' with the passed in RouteEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="routeInstance">RouteEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingRouteCollectionViaDeliverypointgroup_(IEntity routeInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingRouteCollectionViaDeliverypointgroup_(routeInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a Relation of type 'm:n' with the passed in RouteEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="routeInstance">RouteEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingRouteCollectionViaDeliverypointgroup_(IEntity routeInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingRouteCollectionViaDeliverypointgroup_(routeInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  relation of type 'm:n' with the passed in RouteEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="routeInstance">RouteEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingRouteCollectionViaDeliverypointgroup_(IEntity routeInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateTerminalDAO().GetMultiUsingRouteCollectionViaDeliverypointgroup_(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, routeInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  relation of type 'm:n' with the passed in RouteEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="routeInstance">RouteEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingRouteCollectionViaDeliverypointgroup_(IEntity routeInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateTerminalDAO().GetMultiUsingRouteCollectionViaDeliverypointgroup_(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, routeInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  Relation of type 'm:n' with the passed in RoutestepEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="routestepInstance">RoutestepEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingRoutestepCollectionViaRoutestephandler(IEntity routestepInstance)
		{
			return GetMultiManyToManyUsingRoutestepCollectionViaRoutestephandler(routestepInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  relation of type 'm:n' with the passed in RoutestepEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="routestepInstance">RoutestepEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingRoutestepCollectionViaRoutestephandler(IEntity routestepInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingRoutestepCollectionViaRoutestephandler(routestepInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a Relation of type 'm:n' with the passed in RoutestepEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="routestepInstance">RoutestepEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingRoutestepCollectionViaRoutestephandler(IEntity routestepInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingRoutestepCollectionViaRoutestephandler(routestepInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  relation of type 'm:n' with the passed in RoutestepEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="routestepInstance">RoutestepEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingRoutestepCollectionViaRoutestephandler(IEntity routestepInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateTerminalDAO().GetMultiUsingRoutestepCollectionViaRoutestephandler(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, routestepInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  relation of type 'm:n' with the passed in RoutestepEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="routestepInstance">RoutestepEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingRoutestepCollectionViaRoutestephandler(IEntity routestepInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateTerminalDAO().GetMultiUsingRoutestepCollectionViaRoutestephandler(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, routestepInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  Relation of type 'm:n' with the passed in SupportpoolEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="supportpoolInstance">SupportpoolEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingSupportpoolCollectionViaOrderRoutestephandler(IEntity supportpoolInstance)
		{
			return GetMultiManyToManyUsingSupportpoolCollectionViaOrderRoutestephandler(supportpoolInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  relation of type 'm:n' with the passed in SupportpoolEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="supportpoolInstance">SupportpoolEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingSupportpoolCollectionViaOrderRoutestephandler(IEntity supportpoolInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingSupportpoolCollectionViaOrderRoutestephandler(supportpoolInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a Relation of type 'm:n' with the passed in SupportpoolEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="supportpoolInstance">SupportpoolEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingSupportpoolCollectionViaOrderRoutestephandler(IEntity supportpoolInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingSupportpoolCollectionViaOrderRoutestephandler(supportpoolInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  relation of type 'm:n' with the passed in SupportpoolEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="supportpoolInstance">SupportpoolEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingSupportpoolCollectionViaOrderRoutestephandler(IEntity supportpoolInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateTerminalDAO().GetMultiUsingSupportpoolCollectionViaOrderRoutestephandler(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, supportpoolInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  relation of type 'm:n' with the passed in SupportpoolEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="supportpoolInstance">SupportpoolEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingSupportpoolCollectionViaOrderRoutestephandler(IEntity supportpoolInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateTerminalDAO().GetMultiUsingSupportpoolCollectionViaOrderRoutestephandler(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, supportpoolInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  Relation of type 'm:n' with the passed in SupportpoolEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="supportpoolInstance">SupportpoolEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingSupportpoolCollectionViaOrderRoutestephandler_(IEntity supportpoolInstance)
		{
			return GetMultiManyToManyUsingSupportpoolCollectionViaOrderRoutestephandler_(supportpoolInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  relation of type 'm:n' with the passed in SupportpoolEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="supportpoolInstance">SupportpoolEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingSupportpoolCollectionViaOrderRoutestephandler_(IEntity supportpoolInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingSupportpoolCollectionViaOrderRoutestephandler_(supportpoolInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a Relation of type 'm:n' with the passed in SupportpoolEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="supportpoolInstance">SupportpoolEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingSupportpoolCollectionViaOrderRoutestephandler_(IEntity supportpoolInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingSupportpoolCollectionViaOrderRoutestephandler_(supportpoolInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  relation of type 'm:n' with the passed in SupportpoolEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="supportpoolInstance">SupportpoolEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingSupportpoolCollectionViaOrderRoutestephandler_(IEntity supportpoolInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateTerminalDAO().GetMultiUsingSupportpoolCollectionViaOrderRoutestephandler_(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, supportpoolInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  relation of type 'm:n' with the passed in SupportpoolEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="supportpoolInstance">SupportpoolEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingSupportpoolCollectionViaOrderRoutestephandler_(IEntity supportpoolInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateTerminalDAO().GetMultiUsingSupportpoolCollectionViaOrderRoutestephandler_(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, supportpoolInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  Relation of type 'm:n' with the passed in SupportpoolEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="supportpoolInstance">SupportpoolEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingSupportpoolCollectionViaOrderRoutestephandlerHistory(IEntity supportpoolInstance)
		{
			return GetMultiManyToManyUsingSupportpoolCollectionViaOrderRoutestephandlerHistory(supportpoolInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  relation of type 'm:n' with the passed in SupportpoolEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="supportpoolInstance">SupportpoolEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingSupportpoolCollectionViaOrderRoutestephandlerHistory(IEntity supportpoolInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingSupportpoolCollectionViaOrderRoutestephandlerHistory(supportpoolInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a Relation of type 'm:n' with the passed in SupportpoolEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="supportpoolInstance">SupportpoolEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingSupportpoolCollectionViaOrderRoutestephandlerHistory(IEntity supportpoolInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingSupportpoolCollectionViaOrderRoutestephandlerHistory(supportpoolInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  relation of type 'm:n' with the passed in SupportpoolEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="supportpoolInstance">SupportpoolEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingSupportpoolCollectionViaOrderRoutestephandlerHistory(IEntity supportpoolInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateTerminalDAO().GetMultiUsingSupportpoolCollectionViaOrderRoutestephandlerHistory(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, supportpoolInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  relation of type 'm:n' with the passed in SupportpoolEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="supportpoolInstance">SupportpoolEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingSupportpoolCollectionViaOrderRoutestephandlerHistory(IEntity supportpoolInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateTerminalDAO().GetMultiUsingSupportpoolCollectionViaOrderRoutestephandlerHistory(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, supportpoolInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  Relation of type 'm:n' with the passed in SupportpoolEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="supportpoolInstance">SupportpoolEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingSupportpoolCollectionViaOrderRoutestephandlerHistory_(IEntity supportpoolInstance)
		{
			return GetMultiManyToManyUsingSupportpoolCollectionViaOrderRoutestephandlerHistory_(supportpoolInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  relation of type 'm:n' with the passed in SupportpoolEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="supportpoolInstance">SupportpoolEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingSupportpoolCollectionViaOrderRoutestephandlerHistory_(IEntity supportpoolInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingSupportpoolCollectionViaOrderRoutestephandlerHistory_(supportpoolInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a Relation of type 'm:n' with the passed in SupportpoolEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="supportpoolInstance">SupportpoolEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingSupportpoolCollectionViaOrderRoutestephandlerHistory_(IEntity supportpoolInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingSupportpoolCollectionViaOrderRoutestephandlerHistory_(supportpoolInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  relation of type 'm:n' with the passed in SupportpoolEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="supportpoolInstance">SupportpoolEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingSupportpoolCollectionViaOrderRoutestephandlerHistory_(IEntity supportpoolInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateTerminalDAO().GetMultiUsingSupportpoolCollectionViaOrderRoutestephandlerHistory_(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, supportpoolInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  relation of type 'm:n' with the passed in SupportpoolEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="supportpoolInstance">SupportpoolEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingSupportpoolCollectionViaOrderRoutestephandlerHistory_(IEntity supportpoolInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateTerminalDAO().GetMultiUsingSupportpoolCollectionViaOrderRoutestephandlerHistory_(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, supportpoolInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  Relation of type 'm:n' with the passed in SupportpoolEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="supportpoolInstance">SupportpoolEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingSupportpoolCollectionViaRoutestephandler(IEntity supportpoolInstance)
		{
			return GetMultiManyToManyUsingSupportpoolCollectionViaRoutestephandler(supportpoolInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  relation of type 'm:n' with the passed in SupportpoolEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="supportpoolInstance">SupportpoolEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingSupportpoolCollectionViaRoutestephandler(IEntity supportpoolInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingSupportpoolCollectionViaRoutestephandler(supportpoolInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a Relation of type 'm:n' with the passed in SupportpoolEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="supportpoolInstance">SupportpoolEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingSupportpoolCollectionViaRoutestephandler(IEntity supportpoolInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingSupportpoolCollectionViaRoutestephandler(supportpoolInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  relation of type 'm:n' with the passed in SupportpoolEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="supportpoolInstance">SupportpoolEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingSupportpoolCollectionViaRoutestephandler(IEntity supportpoolInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateTerminalDAO().GetMultiUsingSupportpoolCollectionViaRoutestephandler(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, supportpoolInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  relation of type 'm:n' with the passed in SupportpoolEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="supportpoolInstance">SupportpoolEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingSupportpoolCollectionViaRoutestephandler(IEntity supportpoolInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateTerminalDAO().GetMultiUsingSupportpoolCollectionViaRoutestephandler(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, supportpoolInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  Relation of type 'm:n' with the passed in TerminalEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="terminalInstance">TerminalEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingTerminalCollectionViaNetmessage(IEntity terminalInstance)
		{
			return GetMultiManyToManyUsingTerminalCollectionViaNetmessage(terminalInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  relation of type 'm:n' with the passed in TerminalEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="terminalInstance">TerminalEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingTerminalCollectionViaNetmessage(IEntity terminalInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingTerminalCollectionViaNetmessage(terminalInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a Relation of type 'm:n' with the passed in TerminalEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="terminalInstance">TerminalEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingTerminalCollectionViaNetmessage(IEntity terminalInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingTerminalCollectionViaNetmessage(terminalInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  relation of type 'm:n' with the passed in TerminalEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="terminalInstance">TerminalEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingTerminalCollectionViaNetmessage(IEntity terminalInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateTerminalDAO().GetMultiUsingTerminalCollectionViaNetmessage(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, terminalInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  relation of type 'm:n' with the passed in TerminalEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="terminalInstance">TerminalEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingTerminalCollectionViaNetmessage(IEntity terminalInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateTerminalDAO().GetMultiUsingTerminalCollectionViaNetmessage(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, terminalInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  Relation of type 'm:n' with the passed in TerminalEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="terminalInstance">TerminalEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingTerminalCollectionViaNetmessage_(IEntity terminalInstance)
		{
			return GetMultiManyToManyUsingTerminalCollectionViaNetmessage_(terminalInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  relation of type 'm:n' with the passed in TerminalEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="terminalInstance">TerminalEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingTerminalCollectionViaNetmessage_(IEntity terminalInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingTerminalCollectionViaNetmessage_(terminalInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a Relation of type 'm:n' with the passed in TerminalEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="terminalInstance">TerminalEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingTerminalCollectionViaNetmessage_(IEntity terminalInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingTerminalCollectionViaNetmessage_(terminalInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  relation of type 'm:n' with the passed in TerminalEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="terminalInstance">TerminalEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingTerminalCollectionViaNetmessage_(IEntity terminalInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateTerminalDAO().GetMultiUsingTerminalCollectionViaNetmessage_(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, terminalInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  relation of type 'm:n' with the passed in TerminalEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="terminalInstance">TerminalEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingTerminalCollectionViaNetmessage_(IEntity terminalInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateTerminalDAO().GetMultiUsingTerminalCollectionViaNetmessage_(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, terminalInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  Relation of type 'm:n' with the passed in TerminalEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="terminalInstance">TerminalEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingTerminalCollectionViaOrderRoutestephandler(IEntity terminalInstance)
		{
			return GetMultiManyToManyUsingTerminalCollectionViaOrderRoutestephandler(terminalInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  relation of type 'm:n' with the passed in TerminalEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="terminalInstance">TerminalEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingTerminalCollectionViaOrderRoutestephandler(IEntity terminalInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingTerminalCollectionViaOrderRoutestephandler(terminalInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a Relation of type 'm:n' with the passed in TerminalEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="terminalInstance">TerminalEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingTerminalCollectionViaOrderRoutestephandler(IEntity terminalInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingTerminalCollectionViaOrderRoutestephandler(terminalInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  relation of type 'm:n' with the passed in TerminalEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="terminalInstance">TerminalEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingTerminalCollectionViaOrderRoutestephandler(IEntity terminalInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateTerminalDAO().GetMultiUsingTerminalCollectionViaOrderRoutestephandler(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, terminalInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  relation of type 'm:n' with the passed in TerminalEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="terminalInstance">TerminalEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingTerminalCollectionViaOrderRoutestephandler(IEntity terminalInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateTerminalDAO().GetMultiUsingTerminalCollectionViaOrderRoutestephandler(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, terminalInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  Relation of type 'm:n' with the passed in TerminalEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="terminalInstance">TerminalEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingTerminalCollectionViaOrderRoutestephandler_(IEntity terminalInstance)
		{
			return GetMultiManyToManyUsingTerminalCollectionViaOrderRoutestephandler_(terminalInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  relation of type 'm:n' with the passed in TerminalEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="terminalInstance">TerminalEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingTerminalCollectionViaOrderRoutestephandler_(IEntity terminalInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingTerminalCollectionViaOrderRoutestephandler_(terminalInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a Relation of type 'm:n' with the passed in TerminalEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="terminalInstance">TerminalEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingTerminalCollectionViaOrderRoutestephandler_(IEntity terminalInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingTerminalCollectionViaOrderRoutestephandler_(terminalInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  relation of type 'm:n' with the passed in TerminalEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="terminalInstance">TerminalEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingTerminalCollectionViaOrderRoutestephandler_(IEntity terminalInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateTerminalDAO().GetMultiUsingTerminalCollectionViaOrderRoutestephandler_(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, terminalInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  relation of type 'm:n' with the passed in TerminalEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="terminalInstance">TerminalEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingTerminalCollectionViaOrderRoutestephandler_(IEntity terminalInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateTerminalDAO().GetMultiUsingTerminalCollectionViaOrderRoutestephandler_(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, terminalInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  Relation of type 'm:n' with the passed in TerminalEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="terminalInstance">TerminalEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingTerminalCollectionViaOrderRoutestephandlerHistory(IEntity terminalInstance)
		{
			return GetMultiManyToManyUsingTerminalCollectionViaOrderRoutestephandlerHistory(terminalInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  relation of type 'm:n' with the passed in TerminalEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="terminalInstance">TerminalEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingTerminalCollectionViaOrderRoutestephandlerHistory(IEntity terminalInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingTerminalCollectionViaOrderRoutestephandlerHistory(terminalInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a Relation of type 'm:n' with the passed in TerminalEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="terminalInstance">TerminalEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingTerminalCollectionViaOrderRoutestephandlerHistory(IEntity terminalInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingTerminalCollectionViaOrderRoutestephandlerHistory(terminalInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  relation of type 'm:n' with the passed in TerminalEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="terminalInstance">TerminalEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingTerminalCollectionViaOrderRoutestephandlerHistory(IEntity terminalInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateTerminalDAO().GetMultiUsingTerminalCollectionViaOrderRoutestephandlerHistory(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, terminalInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  relation of type 'm:n' with the passed in TerminalEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="terminalInstance">TerminalEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingTerminalCollectionViaOrderRoutestephandlerHistory(IEntity terminalInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateTerminalDAO().GetMultiUsingTerminalCollectionViaOrderRoutestephandlerHistory(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, terminalInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  Relation of type 'm:n' with the passed in TerminalEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="terminalInstance">TerminalEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingTerminalCollectionViaOrderRoutestephandlerHistory_(IEntity terminalInstance)
		{
			return GetMultiManyToManyUsingTerminalCollectionViaOrderRoutestephandlerHistory_(terminalInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  relation of type 'm:n' with the passed in TerminalEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="terminalInstance">TerminalEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingTerminalCollectionViaOrderRoutestephandlerHistory_(IEntity terminalInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingTerminalCollectionViaOrderRoutestephandlerHistory_(terminalInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a Relation of type 'm:n' with the passed in TerminalEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="terminalInstance">TerminalEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingTerminalCollectionViaOrderRoutestephandlerHistory_(IEntity terminalInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingTerminalCollectionViaOrderRoutestephandlerHistory_(terminalInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  relation of type 'm:n' with the passed in TerminalEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="terminalInstance">TerminalEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingTerminalCollectionViaOrderRoutestephandlerHistory_(IEntity terminalInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateTerminalDAO().GetMultiUsingTerminalCollectionViaOrderRoutestephandlerHistory_(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, terminalInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  relation of type 'm:n' with the passed in TerminalEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="terminalInstance">TerminalEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingTerminalCollectionViaOrderRoutestephandlerHistory_(IEntity terminalInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateTerminalDAO().GetMultiUsingTerminalCollectionViaOrderRoutestephandlerHistory_(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, terminalInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  Relation of type 'm:n' with the passed in TerminalLogFileEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="terminalLogFileInstance">TerminalLogFileEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingTerminalLogFileCollectionViaTerminalLog(IEntity terminalLogFileInstance)
		{
			return GetMultiManyToManyUsingTerminalLogFileCollectionViaTerminalLog(terminalLogFileInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  relation of type 'm:n' with the passed in TerminalLogFileEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="terminalLogFileInstance">TerminalLogFileEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingTerminalLogFileCollectionViaTerminalLog(IEntity terminalLogFileInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingTerminalLogFileCollectionViaTerminalLog(terminalLogFileInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a Relation of type 'm:n' with the passed in TerminalLogFileEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="terminalLogFileInstance">TerminalLogFileEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingTerminalLogFileCollectionViaTerminalLog(IEntity terminalLogFileInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingTerminalLogFileCollectionViaTerminalLog(terminalLogFileInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  relation of type 'm:n' with the passed in TerminalLogFileEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="terminalLogFileInstance">TerminalLogFileEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingTerminalLogFileCollectionViaTerminalLog(IEntity terminalLogFileInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateTerminalDAO().GetMultiUsingTerminalLogFileCollectionViaTerminalLog(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, terminalLogFileInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  relation of type 'm:n' with the passed in TerminalLogFileEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="terminalLogFileInstance">TerminalLogFileEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingTerminalLogFileCollectionViaTerminalLog(IEntity terminalLogFileInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateTerminalDAO().GetMultiUsingTerminalLogFileCollectionViaTerminalLog(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, terminalLogFileInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  Relation of type 'm:n' with the passed in UIModeEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="uIModeInstance">UIModeEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingUIModeCollectionViaDeliverypointgroup_(IEntity uIModeInstance)
		{
			return GetMultiManyToManyUsingUIModeCollectionViaDeliverypointgroup_(uIModeInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  relation of type 'm:n' with the passed in UIModeEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="uIModeInstance">UIModeEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingUIModeCollectionViaDeliverypointgroup_(IEntity uIModeInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingUIModeCollectionViaDeliverypointgroup_(uIModeInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a Relation of type 'm:n' with the passed in UIModeEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="uIModeInstance">UIModeEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingUIModeCollectionViaDeliverypointgroup_(IEntity uIModeInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingUIModeCollectionViaDeliverypointgroup_(uIModeInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  relation of type 'm:n' with the passed in UIModeEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="uIModeInstance">UIModeEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingUIModeCollectionViaDeliverypointgroup_(IEntity uIModeInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateTerminalDAO().GetMultiUsingUIModeCollectionViaDeliverypointgroup_(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, uIModeInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  relation of type 'm:n' with the passed in UIModeEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="uIModeInstance">UIModeEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingUIModeCollectionViaDeliverypointgroup_(IEntity uIModeInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateTerminalDAO().GetMultiUsingUIModeCollectionViaDeliverypointgroup_(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, uIModeInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  Relation of type 'm:n' with the passed in UIModeEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="uIModeInstance">UIModeEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingUIModeCollectionViaDeliverypointgroup__(IEntity uIModeInstance)
		{
			return GetMultiManyToManyUsingUIModeCollectionViaDeliverypointgroup__(uIModeInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  relation of type 'm:n' with the passed in UIModeEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="uIModeInstance">UIModeEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingUIModeCollectionViaDeliverypointgroup__(IEntity uIModeInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingUIModeCollectionViaDeliverypointgroup__(uIModeInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a Relation of type 'm:n' with the passed in UIModeEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="uIModeInstance">UIModeEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingUIModeCollectionViaDeliverypointgroup__(IEntity uIModeInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingUIModeCollectionViaDeliverypointgroup__(uIModeInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  relation of type 'm:n' with the passed in UIModeEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="uIModeInstance">UIModeEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingUIModeCollectionViaDeliverypointgroup__(IEntity uIModeInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateTerminalDAO().GetMultiUsingUIModeCollectionViaDeliverypointgroup__(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, uIModeInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  relation of type 'm:n' with the passed in UIModeEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="uIModeInstance">UIModeEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingUIModeCollectionViaDeliverypointgroup__(IEntity uIModeInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateTerminalDAO().GetMultiUsingUIModeCollectionViaDeliverypointgroup__(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, uIModeInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  Relation of type 'm:n' with the passed in UIModeEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="uIModeInstance">UIModeEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingUIModeCollectionViaTerminal(IEntity uIModeInstance)
		{
			return GetMultiManyToManyUsingUIModeCollectionViaTerminal(uIModeInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  relation of type 'm:n' with the passed in UIModeEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="uIModeInstance">UIModeEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingUIModeCollectionViaTerminal(IEntity uIModeInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingUIModeCollectionViaTerminal(uIModeInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a Relation of type 'm:n' with the passed in UIModeEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="uIModeInstance">UIModeEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingUIModeCollectionViaTerminal(IEntity uIModeInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingUIModeCollectionViaTerminal(uIModeInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  relation of type 'm:n' with the passed in UIModeEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="uIModeInstance">UIModeEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingUIModeCollectionViaTerminal(IEntity uIModeInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateTerminalDAO().GetMultiUsingUIModeCollectionViaTerminal(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, uIModeInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  relation of type 'm:n' with the passed in UIModeEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="uIModeInstance">UIModeEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingUIModeCollectionViaTerminal(IEntity uIModeInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateTerminalDAO().GetMultiUsingUIModeCollectionViaTerminal(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, uIModeInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  Relation of type 'm:n' with the passed in UIModeEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="uIModeInstance">UIModeEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingUIModeCollectionViaDeliverypointgroup(IEntity uIModeInstance)
		{
			return GetMultiManyToManyUsingUIModeCollectionViaDeliverypointgroup(uIModeInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  relation of type 'm:n' with the passed in UIModeEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="uIModeInstance">UIModeEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingUIModeCollectionViaDeliverypointgroup(IEntity uIModeInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingUIModeCollectionViaDeliverypointgroup(uIModeInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a Relation of type 'm:n' with the passed in UIModeEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="uIModeInstance">UIModeEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingUIModeCollectionViaDeliverypointgroup(IEntity uIModeInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingUIModeCollectionViaDeliverypointgroup(uIModeInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  relation of type 'm:n' with the passed in UIModeEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="uIModeInstance">UIModeEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingUIModeCollectionViaDeliverypointgroup(IEntity uIModeInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateTerminalDAO().GetMultiUsingUIModeCollectionViaDeliverypointgroup(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, uIModeInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  relation of type 'm:n' with the passed in UIModeEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="uIModeInstance">UIModeEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingUIModeCollectionViaDeliverypointgroup(IEntity uIModeInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateTerminalDAO().GetMultiUsingUIModeCollectionViaDeliverypointgroup(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, uIModeInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  Relation of type 'm:n' with the passed in UserEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="userInstance">UserEntity object to be used as a filter in the m:n relation</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingAutomaticSignOnUserCollectionViaTerminal(IEntity userInstance)
		{
			return GetMultiManyToManyUsingAutomaticSignOnUserCollectionViaTerminal(userInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, 0, 0);
		}
		
		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  relation of type 'm:n' with the passed in UserEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="userInstance">UserEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingAutomaticSignOnUserCollectionViaTerminal(IEntity userInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiManyToManyUsingAutomaticSignOnUserCollectionViaTerminal(userInstance, maxNumberOfItemsToReturn, sortClauses, 0, 0);
		}

		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a Relation of type 'm:n' with the passed in UserEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="userInstance">UserEntity object to be used as a filter in the m:n relation</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingAutomaticSignOnUserCollectionViaTerminal(IEntity userInstance, IPrefetchPath prefetchPathToUse)
		{
			return GetMultiManyToManyUsingAutomaticSignOnUserCollectionViaTerminal(userInstance, this.MaxNumberOfItemsToReturn, this.SortClauses, prefetchPathToUse);
		}
		
		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  relation of type 'm:n' with the passed in UserEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="userInstance">UserEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public virtual bool GetMultiManyToManyUsingAutomaticSignOnUserCollectionViaTerminal(IEntity userInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, int pageNumber, int pageSize)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateTerminalDAO().GetMultiUsingAutomaticSignOnUserCollectionViaTerminal(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, userInstance, null, pageNumber, pageSize);
		}

		/// <summary> Retrieves in this TerminalCollection object all TerminalEntity objects which are related via a  relation of type 'm:n' with the passed in UserEntity. All current elements in the collection are removed from the collection.</summary>
		/// <param name="userInstance">UserEntity object to be used as a filter in the m:n relation</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch.</param>
		/// <returns>true if the retrieval succeeded, false otherwise</returns>
		public bool GetMultiManyToManyUsingAutomaticSignOnUserCollectionViaTerminal(IEntity userInstance, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IPrefetchPath prefetchPathToUse)
		{
			if(!this.SuppressClearInGetMulti)
			{
				this.Clear();
			}
			return DAOFactory.CreateTerminalDAO().GetMultiUsingAutomaticSignOnUserCollectionViaTerminal(this.Transaction, this, maxNumberOfItemsToReturn, sortClauses, this.EntityFactoryToUse, userInstance, prefetchPathToUse, 0, 0);
		}

		/// <summary> Retrieves Entity rows in a datatable which match the specified filter. It will always create a new connection to the database.</summary>
		/// <param name="selectFilter">A predicate or predicate expression which should be used as filter for the entities to retrieve.</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <returns>DataTable with the rows requested.</returns>
		public static DataTable GetMultiAsDataTable(IPredicate selectFilter, long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			return GetMultiAsDataTable(selectFilter, maxNumberOfItemsToReturn, sortClauses, null, 0, 0);
		}

		/// <summary> Retrieves Entity rows in a datatable which match the specified filter. It will always create a new connection to the database.</summary>
		/// <param name="selectFilter">A predicate or predicate expression which should be used as filter for the entities to retrieve.</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="relations">The set of relations to walk to construct to total query.</param>
		/// <returns>DataTable with the rows requested.</returns>
		public static DataTable GetMultiAsDataTable(IPredicate selectFilter, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IRelationCollection relations)
		{
			return GetMultiAsDataTable(selectFilter, maxNumberOfItemsToReturn, sortClauses, relations, 0, 0);
		}
		
		/// <summary> Retrieves Entity rows in a datatable which match the specified filter. It will always create a new connection to the database.</summary>
		/// <param name="selectFilter">A predicate or predicate expression which should be used as filter for the entities to retrieve.</param>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return with this retrieval query.</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified, no sorting is applied.</param>
		/// <param name="relations">The set of relations to walk to construct to total query.</param>
		/// <param name="pageNumber">The page number to retrieve.</param>
		/// <param name="pageSize">The page size of the page to retrieve.</param>
		/// <returns>DataTable with the rows requested.</returns>
		public static DataTable GetMultiAsDataTable(IPredicate selectFilter, long maxNumberOfItemsToReturn, ISortExpression sortClauses, IRelationCollection relations, int pageNumber, int pageSize)
		{
			TerminalDAO dao = DAOFactory.CreateTerminalDAO();
			return dao.GetMultiAsDataTable(maxNumberOfItemsToReturn, sortClauses, selectFilter, relations, pageNumber, pageSize);
		}


		
		/// <summary> Gets a scalar value, calculated with the aggregate. the field index specified is the field the aggregate are applied on.</summary>
		/// <param name="fieldIndex">Field index of field to which to apply the aggregate function and expression</param>
		/// <param name="aggregateToApply">Aggregate function to apply. </param>
		/// <returns>the scalar value requested</returns>
		public object GetScalar(TerminalFieldIndex fieldIndex, AggregateFunction aggregateToApply)
		{
			return GetScalar(fieldIndex, null, aggregateToApply, null, null, null);
		}

		/// <summary> Gets a scalar value, calculated with the aggregate and expression specified. the field index specified is the field the expression and aggregate are applied on.</summary>
		/// <param name="fieldIndex">Field index of field to which to apply the aggregate function and expression</param>
		/// <param name="expressionToExecute">The expression to execute. Can be null</param>
		/// <param name="aggregateToApply">Aggregate function to apply. </param>
		/// <returns>the scalar value requested</returns>
		public object GetScalar(TerminalFieldIndex fieldIndex, IExpression expressionToExecute, AggregateFunction aggregateToApply)
		{
			return GetScalar(fieldIndex, expressionToExecute, aggregateToApply, null, null, null);
		}

		/// <summary> Gets a scalar value, calculated with the aggregate and expression specified. the field index specified is the field the expression and aggregate are applied on.</summary>
		/// <param name="fieldIndex">Field index of field to which to apply the aggregate function and expression</param>
		/// <param name="expressionToExecute">The expression to execute. Can be null</param>
		/// <param name="aggregateToApply">Aggregate function to apply. </param>
		/// <param name="filter">The filter to apply to retrieve the scalar</param>
		/// <returns>the scalar value requested</returns>
		public object GetScalar(TerminalFieldIndex fieldIndex, IExpression expressionToExecute, AggregateFunction aggregateToApply, IPredicate filter)
		{
			return GetScalar(fieldIndex, expressionToExecute, aggregateToApply, filter, null, null);
		}

		/// <summary> Gets a scalar value, calculated with the aggregate and expression specified. the field index specified is the field the expression and aggregate are applied on.</summary>
		/// <param name="fieldIndex">Field index of field to which to apply the aggregate function and expression</param>
		/// <param name="expressionToExecute">The expression to execute. Can be null</param>
		/// <param name="aggregateToApply">Aggregate function to apply. </param>
		/// <param name="filter">The filter to apply to retrieve the scalar</param>
		/// <param name="groupByClause">The groupby clause to apply to retrieve the scalar</param>
		/// <returns>the scalar value requested</returns>
		public object GetScalar(TerminalFieldIndex fieldIndex, IExpression expressionToExecute, AggregateFunction aggregateToApply, IPredicate filter, IGroupByCollection groupByClause)
		{
			return GetScalar(fieldIndex, expressionToExecute, aggregateToApply, filter, null, groupByClause);
		}

		/// <summary> Gets a scalar value, calculated with the aggregate and expression specified. the field index specified is the field the expression and aggregate are applied on.</summary>
		/// <param name="fieldIndex">Field index of field to which to apply the aggregate function and expression</param>
		/// <param name="expressionToExecute">The expression to execute. Can be null</param>
		/// <param name="aggregateToApply">Aggregate function to apply. </param>
		/// <param name="filter">The filter to apply to retrieve the scalar</param>
		/// <param name="relations">The relations to walk</param>
		/// <param name="groupByClause">The groupby clause to apply to retrieve the scalar</param>
		/// <returns>the scalar value requested</returns>
		public virtual object GetScalar(TerminalFieldIndex fieldIndex, IExpression expressionToExecute, AggregateFunction aggregateToApply, IPredicate filter, IRelationCollection relations, IGroupByCollection groupByClause)
		{
			EntityFields fields = new EntityFields(1);
			fields[0] = EntityFieldFactory.Create(fieldIndex);
			if((fields[0].ExpressionToApply == null) || (expressionToExecute != null))
			{
				fields[0].ExpressionToApply = expressionToExecute;
			}
			if((fields[0].AggregateFunctionToApply == AggregateFunction.None) || (aggregateToApply != AggregateFunction.None))
			{
				fields[0].AggregateFunctionToApply = aggregateToApply;
			}
			return DAOFactory.CreateTerminalDAO().GetScalar(fields, this.Transaction, filter, relations, groupByClause);
		}
		
		/// <summary>Creats a new DAO instance so code which is in the base class can still use the proper DAO object.</summary>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateTerminalDAO();
		}
		
		/// <summary>Creates a new transaction object</summary>
		/// <param name="levelOfIsolation">The level of isolation.</param>
		/// <param name="name">The name.</param>
		protected override ITransaction CreateTransaction( IsolationLevel levelOfIsolation, string name )
		{
			return new Transaction(levelOfIsolation, name);
		}

		#region Custom EntityCollection code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCollectionCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion
		
		#region Included Code

		#endregion
	}
}
