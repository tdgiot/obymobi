﻿using Obymobi.Data.EntityClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.Data
{
    public interface INullableCompanyRelatedChildEntity
    {
        int? ParentCompanyId { get; set; }

        CommonEntityBase Parent { get; }
    }
}
