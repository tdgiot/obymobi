﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
using System.Runtime.Serialization;
using System.Data;
using System.Xml.Serialization;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity base class which represents the base class for the entity 'Currency'.<br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public abstract partial class CurrencyEntityBase : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		public new string LLBLGenProEntityName {
			get { return "CurrencyEntity"; }
		}
	
		#region Class Member Declarations
		private Obymobi.Data.CollectionClasses.CompanyCollection	_companyCollection;
		private bool	_alwaysFetchCompanyCollection, _alreadyFetchedCompanyCollection;
		private Obymobi.Data.CollectionClasses.CountryCollection	_countryCollection;
		private bool	_alwaysFetchCountryCollection, _alreadyFetchedCountryCollection;
		private Obymobi.Data.CollectionClasses.OrderCollection	_orderCollection;
		private bool	_alwaysFetchOrderCollection, _alreadyFetchedOrderCollection;
		private Obymobi.Data.CollectionClasses.PointOfInterestCollection	_pointOfInterestCollection;
		private bool	_alwaysFetchPointOfInterestCollection, _alreadyFetchedPointOfInterestCollection;
		private Obymobi.Data.CollectionClasses.CompanyOwnerCollection _companyOwnerCollectionViaCompany;
		private bool	_alwaysFetchCompanyOwnerCollectionViaCompany, _alreadyFetchedCompanyOwnerCollectionViaCompany;
		private Obymobi.Data.CollectionClasses.CountryCollection _countryCollectionViaCompany;
		private bool	_alwaysFetchCountryCollectionViaCompany, _alreadyFetchedCountryCollectionViaCompany;
		private Obymobi.Data.CollectionClasses.OrderCollection _orderCollectionViaOrder;
		private bool	_alwaysFetchOrderCollectionViaOrder, _alreadyFetchedOrderCollectionViaOrder;
		private Obymobi.Data.CollectionClasses.RouteCollection _routeCollectionViaCompany;
		private bool	_alwaysFetchRouteCollectionViaCompany, _alreadyFetchedRouteCollectionViaCompany;
		private Obymobi.Data.CollectionClasses.SupportpoolCollection _supportpoolCollectionViaCompany;
		private bool	_alwaysFetchSupportpoolCollectionViaCompany, _alreadyFetchedSupportpoolCollectionViaCompany;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name CompanyCollection</summary>
			public static readonly string CompanyCollection = "CompanyCollection";
			/// <summary>Member name CountryCollection</summary>
			public static readonly string CountryCollection = "CountryCollection";
			/// <summary>Member name OrderCollection</summary>
			public static readonly string OrderCollection = "OrderCollection";
			/// <summary>Member name PointOfInterestCollection</summary>
			public static readonly string PointOfInterestCollection = "PointOfInterestCollection";
			/// <summary>Member name CompanyOwnerCollectionViaCompany</summary>
			public static readonly string CompanyOwnerCollectionViaCompany = "CompanyOwnerCollectionViaCompany";
			/// <summary>Member name CountryCollectionViaCompany</summary>
			public static readonly string CountryCollectionViaCompany = "CountryCollectionViaCompany";
			/// <summary>Member name OrderCollectionViaOrder</summary>
			public static readonly string OrderCollectionViaOrder = "OrderCollectionViaOrder";
			/// <summary>Member name RouteCollectionViaCompany</summary>
			public static readonly string RouteCollectionViaCompany = "RouteCollectionViaCompany";
			/// <summary>Member name SupportpoolCollectionViaCompany</summary>
			public static readonly string SupportpoolCollectionViaCompany = "SupportpoolCollectionViaCompany";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static CurrencyEntityBase()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		protected CurrencyEntityBase() :base("CurrencyEntity")
		{
			InitClassEmpty(null);
		}

		/// <summary>CTor</summary>
		/// <param name="currencyId">PK value for Currency which data should be fetched into this Currency object</param>
		protected CurrencyEntityBase(System.Int32 currencyId):base("CurrencyEntity")
		{
			InitClassFetch(currencyId, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="currencyId">PK value for Currency which data should be fetched into this Currency object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		protected CurrencyEntityBase(System.Int32 currencyId, IPrefetchPath prefetchPathToUse): base("CurrencyEntity")
		{
			InitClassFetch(currencyId, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="currencyId">PK value for Currency which data should be fetched into this Currency object</param>
		/// <param name="validator">The custom validator object for this CurrencyEntity</param>
		protected CurrencyEntityBase(System.Int32 currencyId, IValidator validator):base("CurrencyEntity")
		{
			InitClassFetch(currencyId, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected CurrencyEntityBase(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_companyCollection = (Obymobi.Data.CollectionClasses.CompanyCollection)info.GetValue("_companyCollection", typeof(Obymobi.Data.CollectionClasses.CompanyCollection));
			_alwaysFetchCompanyCollection = info.GetBoolean("_alwaysFetchCompanyCollection");
			_alreadyFetchedCompanyCollection = info.GetBoolean("_alreadyFetchedCompanyCollection");

			_countryCollection = (Obymobi.Data.CollectionClasses.CountryCollection)info.GetValue("_countryCollection", typeof(Obymobi.Data.CollectionClasses.CountryCollection));
			_alwaysFetchCountryCollection = info.GetBoolean("_alwaysFetchCountryCollection");
			_alreadyFetchedCountryCollection = info.GetBoolean("_alreadyFetchedCountryCollection");

			_orderCollection = (Obymobi.Data.CollectionClasses.OrderCollection)info.GetValue("_orderCollection", typeof(Obymobi.Data.CollectionClasses.OrderCollection));
			_alwaysFetchOrderCollection = info.GetBoolean("_alwaysFetchOrderCollection");
			_alreadyFetchedOrderCollection = info.GetBoolean("_alreadyFetchedOrderCollection");

			_pointOfInterestCollection = (Obymobi.Data.CollectionClasses.PointOfInterestCollection)info.GetValue("_pointOfInterestCollection", typeof(Obymobi.Data.CollectionClasses.PointOfInterestCollection));
			_alwaysFetchPointOfInterestCollection = info.GetBoolean("_alwaysFetchPointOfInterestCollection");
			_alreadyFetchedPointOfInterestCollection = info.GetBoolean("_alreadyFetchedPointOfInterestCollection");
			_companyOwnerCollectionViaCompany = (Obymobi.Data.CollectionClasses.CompanyOwnerCollection)info.GetValue("_companyOwnerCollectionViaCompany", typeof(Obymobi.Data.CollectionClasses.CompanyOwnerCollection));
			_alwaysFetchCompanyOwnerCollectionViaCompany = info.GetBoolean("_alwaysFetchCompanyOwnerCollectionViaCompany");
			_alreadyFetchedCompanyOwnerCollectionViaCompany = info.GetBoolean("_alreadyFetchedCompanyOwnerCollectionViaCompany");

			_countryCollectionViaCompany = (Obymobi.Data.CollectionClasses.CountryCollection)info.GetValue("_countryCollectionViaCompany", typeof(Obymobi.Data.CollectionClasses.CountryCollection));
			_alwaysFetchCountryCollectionViaCompany = info.GetBoolean("_alwaysFetchCountryCollectionViaCompany");
			_alreadyFetchedCountryCollectionViaCompany = info.GetBoolean("_alreadyFetchedCountryCollectionViaCompany");

			_orderCollectionViaOrder = (Obymobi.Data.CollectionClasses.OrderCollection)info.GetValue("_orderCollectionViaOrder", typeof(Obymobi.Data.CollectionClasses.OrderCollection));
			_alwaysFetchOrderCollectionViaOrder = info.GetBoolean("_alwaysFetchOrderCollectionViaOrder");
			_alreadyFetchedOrderCollectionViaOrder = info.GetBoolean("_alreadyFetchedOrderCollectionViaOrder");

			_routeCollectionViaCompany = (Obymobi.Data.CollectionClasses.RouteCollection)info.GetValue("_routeCollectionViaCompany", typeof(Obymobi.Data.CollectionClasses.RouteCollection));
			_alwaysFetchRouteCollectionViaCompany = info.GetBoolean("_alwaysFetchRouteCollectionViaCompany");
			_alreadyFetchedRouteCollectionViaCompany = info.GetBoolean("_alreadyFetchedRouteCollectionViaCompany");

			_supportpoolCollectionViaCompany = (Obymobi.Data.CollectionClasses.SupportpoolCollection)info.GetValue("_supportpoolCollectionViaCompany", typeof(Obymobi.Data.CollectionClasses.SupportpoolCollection));
			_alwaysFetchSupportpoolCollectionViaCompany = info.GetBoolean("_alwaysFetchSupportpoolCollectionViaCompany");
			_alreadyFetchedSupportpoolCollectionViaCompany = info.GetBoolean("_alreadyFetchedSupportpoolCollectionViaCompany");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}	

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedCompanyCollection = (_companyCollection.Count > 0);
			_alreadyFetchedCountryCollection = (_countryCollection.Count > 0);
			_alreadyFetchedOrderCollection = (_orderCollection.Count > 0);
			_alreadyFetchedPointOfInterestCollection = (_pointOfInterestCollection.Count > 0);
			_alreadyFetchedCompanyOwnerCollectionViaCompany = (_companyOwnerCollectionViaCompany.Count > 0);
			_alreadyFetchedCountryCollectionViaCompany = (_countryCollectionViaCompany.Count > 0);
			_alreadyFetchedOrderCollectionViaOrder = (_orderCollectionViaOrder.Count > 0);
			_alreadyFetchedRouteCollectionViaCompany = (_routeCollectionViaCompany.Count > 0);
			_alreadyFetchedSupportpoolCollectionViaCompany = (_supportpoolCollectionViaCompany.Count > 0);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "CompanyCollection":
					toReturn.Add(Relations.CompanyEntityUsingCurrencyId);
					break;
				case "CountryCollection":
					toReturn.Add(Relations.CountryEntityUsingCurrencyId);
					break;
				case "OrderCollection":
					toReturn.Add(Relations.OrderEntityUsingCurrencyId);
					break;
				case "PointOfInterestCollection":
					toReturn.Add(Relations.PointOfInterestEntityUsingCurrencyId);
					break;
				case "CompanyOwnerCollectionViaCompany":
					toReturn.Add(Relations.CompanyEntityUsingCurrencyId, "CurrencyEntity__", "Company_", JoinHint.None);
					toReturn.Add(CompanyEntity.Relations.CompanyOwnerEntityUsingCompanyOwnerId, "Company_", string.Empty, JoinHint.None);
					break;
				case "CountryCollectionViaCompany":
					toReturn.Add(Relations.CompanyEntityUsingCurrencyId, "CurrencyEntity__", "Company_", JoinHint.None);
					toReturn.Add(CompanyEntity.Relations.CountryEntityUsingCountryId, "Company_", string.Empty, JoinHint.None);
					break;
				case "OrderCollectionViaOrder":
					toReturn.Add(Relations.OrderEntityUsingCurrencyId, "CurrencyEntity__", "Order_", JoinHint.None);
					toReturn.Add(OrderEntity.Relations.OrderEntityUsingMasterOrderId, "Order_", string.Empty, JoinHint.None);
					break;
				case "RouteCollectionViaCompany":
					toReturn.Add(Relations.CompanyEntityUsingCurrencyId, "CurrencyEntity__", "Company_", JoinHint.None);
					toReturn.Add(CompanyEntity.Relations.RouteEntityUsingRouteId, "Company_", string.Empty, JoinHint.None);
					break;
				case "SupportpoolCollectionViaCompany":
					toReturn.Add(Relations.CompanyEntityUsingCurrencyId, "CurrencyEntity__", "Company_", JoinHint.None);
					toReturn.Add(CompanyEntity.Relations.SupportpoolEntityUsingSupportpoolId, "Company_", string.Empty, JoinHint.None);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_companyCollection", (!this.MarkedForDeletion?_companyCollection:null));
			info.AddValue("_alwaysFetchCompanyCollection", _alwaysFetchCompanyCollection);
			info.AddValue("_alreadyFetchedCompanyCollection", _alreadyFetchedCompanyCollection);
			info.AddValue("_countryCollection", (!this.MarkedForDeletion?_countryCollection:null));
			info.AddValue("_alwaysFetchCountryCollection", _alwaysFetchCountryCollection);
			info.AddValue("_alreadyFetchedCountryCollection", _alreadyFetchedCountryCollection);
			info.AddValue("_orderCollection", (!this.MarkedForDeletion?_orderCollection:null));
			info.AddValue("_alwaysFetchOrderCollection", _alwaysFetchOrderCollection);
			info.AddValue("_alreadyFetchedOrderCollection", _alreadyFetchedOrderCollection);
			info.AddValue("_pointOfInterestCollection", (!this.MarkedForDeletion?_pointOfInterestCollection:null));
			info.AddValue("_alwaysFetchPointOfInterestCollection", _alwaysFetchPointOfInterestCollection);
			info.AddValue("_alreadyFetchedPointOfInterestCollection", _alreadyFetchedPointOfInterestCollection);
			info.AddValue("_companyOwnerCollectionViaCompany", (!this.MarkedForDeletion?_companyOwnerCollectionViaCompany:null));
			info.AddValue("_alwaysFetchCompanyOwnerCollectionViaCompany", _alwaysFetchCompanyOwnerCollectionViaCompany);
			info.AddValue("_alreadyFetchedCompanyOwnerCollectionViaCompany", _alreadyFetchedCompanyOwnerCollectionViaCompany);
			info.AddValue("_countryCollectionViaCompany", (!this.MarkedForDeletion?_countryCollectionViaCompany:null));
			info.AddValue("_alwaysFetchCountryCollectionViaCompany", _alwaysFetchCountryCollectionViaCompany);
			info.AddValue("_alreadyFetchedCountryCollectionViaCompany", _alreadyFetchedCountryCollectionViaCompany);
			info.AddValue("_orderCollectionViaOrder", (!this.MarkedForDeletion?_orderCollectionViaOrder:null));
			info.AddValue("_alwaysFetchOrderCollectionViaOrder", _alwaysFetchOrderCollectionViaOrder);
			info.AddValue("_alreadyFetchedOrderCollectionViaOrder", _alreadyFetchedOrderCollectionViaOrder);
			info.AddValue("_routeCollectionViaCompany", (!this.MarkedForDeletion?_routeCollectionViaCompany:null));
			info.AddValue("_alwaysFetchRouteCollectionViaCompany", _alwaysFetchRouteCollectionViaCompany);
			info.AddValue("_alreadyFetchedRouteCollectionViaCompany", _alreadyFetchedRouteCollectionViaCompany);
			info.AddValue("_supportpoolCollectionViaCompany", (!this.MarkedForDeletion?_supportpoolCollectionViaCompany:null));
			info.AddValue("_alwaysFetchSupportpoolCollectionViaCompany", _alwaysFetchSupportpoolCollectionViaCompany);
			info.AddValue("_alreadyFetchedSupportpoolCollectionViaCompany", _alreadyFetchedSupportpoolCollectionViaCompany);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "CompanyCollection":
					_alreadyFetchedCompanyCollection = true;
					if(entity!=null)
					{
						this.CompanyCollection.Add((CompanyEntity)entity);
					}
					break;
				case "CountryCollection":
					_alreadyFetchedCountryCollection = true;
					if(entity!=null)
					{
						this.CountryCollection.Add((CountryEntity)entity);
					}
					break;
				case "OrderCollection":
					_alreadyFetchedOrderCollection = true;
					if(entity!=null)
					{
						this.OrderCollection.Add((OrderEntity)entity);
					}
					break;
				case "PointOfInterestCollection":
					_alreadyFetchedPointOfInterestCollection = true;
					if(entity!=null)
					{
						this.PointOfInterestCollection.Add((PointOfInterestEntity)entity);
					}
					break;
				case "CompanyOwnerCollectionViaCompany":
					_alreadyFetchedCompanyOwnerCollectionViaCompany = true;
					if(entity!=null)
					{
						this.CompanyOwnerCollectionViaCompany.Add((CompanyOwnerEntity)entity);
					}
					break;
				case "CountryCollectionViaCompany":
					_alreadyFetchedCountryCollectionViaCompany = true;
					if(entity!=null)
					{
						this.CountryCollectionViaCompany.Add((CountryEntity)entity);
					}
					break;
				case "OrderCollectionViaOrder":
					_alreadyFetchedOrderCollectionViaOrder = true;
					if(entity!=null)
					{
						this.OrderCollectionViaOrder.Add((OrderEntity)entity);
					}
					break;
				case "RouteCollectionViaCompany":
					_alreadyFetchedRouteCollectionViaCompany = true;
					if(entity!=null)
					{
						this.RouteCollectionViaCompany.Add((RouteEntity)entity);
					}
					break;
				case "SupportpoolCollectionViaCompany":
					_alreadyFetchedSupportpoolCollectionViaCompany = true;
					if(entity!=null)
					{
						this.SupportpoolCollectionViaCompany.Add((SupportpoolEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "CompanyCollection":
					_companyCollection.Add((CompanyEntity)relatedEntity);
					break;
				case "CountryCollection":
					_countryCollection.Add((CountryEntity)relatedEntity);
					break;
				case "OrderCollection":
					_orderCollection.Add((OrderEntity)relatedEntity);
					break;
				case "PointOfInterestCollection":
					_pointOfInterestCollection.Add((PointOfInterestEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "CompanyCollection":
					this.PerformRelatedEntityRemoval(_companyCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "CountryCollection":
					this.PerformRelatedEntityRemoval(_countryCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "OrderCollection":
					this.PerformRelatedEntityRemoval(_orderCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "PointOfInterestCollection":
					this.PerformRelatedEntityRemoval(_pointOfInterestCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_companyCollection);
			toReturn.Add(_countryCollection);
			toReturn.Add(_orderCollection);
			toReturn.Add(_pointOfInterestCollection);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="currencyId">PK value for Currency which data should be fetched into this Currency object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 currencyId)
		{
			return FetchUsingPK(currencyId, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="currencyId">PK value for Currency which data should be fetched into this Currency object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 currencyId, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(currencyId, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="currencyId">PK value for Currency which data should be fetched into this Currency object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 currencyId, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(currencyId, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="currencyId">PK value for Currency which data should be fetched into this Currency object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 currencyId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(currencyId, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.CurrencyId, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new CurrencyRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'CompanyEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CompanyEntity'</returns>
		public Obymobi.Data.CollectionClasses.CompanyCollection GetMultiCompanyCollection(bool forceFetch)
		{
			return GetMultiCompanyCollection(forceFetch, _companyCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CompanyEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'CompanyEntity'</returns>
		public Obymobi.Data.CollectionClasses.CompanyCollection GetMultiCompanyCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiCompanyCollection(forceFetch, _companyCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'CompanyEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.CompanyCollection GetMultiCompanyCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiCompanyCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CompanyEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.CompanyCollection GetMultiCompanyCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedCompanyCollection || forceFetch || _alwaysFetchCompanyCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_companyCollection);
				_companyCollection.SuppressClearInGetMulti=!forceFetch;
				_companyCollection.EntityFactoryToUse = entityFactoryToUse;
				_companyCollection.GetMultiManyToOne(null, null, null, null, this, null, null, null, null, filter);
				_companyCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedCompanyCollection = true;
			}
			return _companyCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'CompanyCollection'. These settings will be taken into account
		/// when the property CompanyCollection is requested or GetMultiCompanyCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCompanyCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_companyCollection.SortClauses=sortClauses;
			_companyCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'CountryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CountryEntity'</returns>
		public Obymobi.Data.CollectionClasses.CountryCollection GetMultiCountryCollection(bool forceFetch)
		{
			return GetMultiCountryCollection(forceFetch, _countryCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CountryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'CountryEntity'</returns>
		public Obymobi.Data.CollectionClasses.CountryCollection GetMultiCountryCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiCountryCollection(forceFetch, _countryCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'CountryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.CountryCollection GetMultiCountryCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiCountryCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CountryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.CountryCollection GetMultiCountryCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedCountryCollection || forceFetch || _alwaysFetchCountryCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_countryCollection);
				_countryCollection.SuppressClearInGetMulti=!forceFetch;
				_countryCollection.EntityFactoryToUse = entityFactoryToUse;
				_countryCollection.GetMultiManyToOne(this, filter);
				_countryCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedCountryCollection = true;
			}
			return _countryCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'CountryCollection'. These settings will be taken into account
		/// when the property CountryCollection is requested or GetMultiCountryCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCountryCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_countryCollection.SortClauses=sortClauses;
			_countryCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'OrderEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'OrderEntity'</returns>
		public Obymobi.Data.CollectionClasses.OrderCollection GetMultiOrderCollection(bool forceFetch)
		{
			return GetMultiOrderCollection(forceFetch, _orderCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'OrderEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'OrderEntity'</returns>
		public Obymobi.Data.CollectionClasses.OrderCollection GetMultiOrderCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiOrderCollection(forceFetch, _orderCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'OrderEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.OrderCollection GetMultiOrderCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiOrderCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'OrderEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.OrderCollection GetMultiOrderCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedOrderCollection || forceFetch || _alwaysFetchOrderCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_orderCollection);
				_orderCollection.SuppressClearInGetMulti=!forceFetch;
				_orderCollection.EntityFactoryToUse = entityFactoryToUse;
				_orderCollection.GetMultiManyToOne(null, null, null, this, null, null, null, null, null, null, null, filter);
				_orderCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedOrderCollection = true;
			}
			return _orderCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'OrderCollection'. These settings will be taken into account
		/// when the property OrderCollection is requested or GetMultiOrderCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersOrderCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_orderCollection.SortClauses=sortClauses;
			_orderCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'PointOfInterestEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'PointOfInterestEntity'</returns>
		public Obymobi.Data.CollectionClasses.PointOfInterestCollection GetMultiPointOfInterestCollection(bool forceFetch)
		{
			return GetMultiPointOfInterestCollection(forceFetch, _pointOfInterestCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'PointOfInterestEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'PointOfInterestEntity'</returns>
		public Obymobi.Data.CollectionClasses.PointOfInterestCollection GetMultiPointOfInterestCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiPointOfInterestCollection(forceFetch, _pointOfInterestCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'PointOfInterestEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.PointOfInterestCollection GetMultiPointOfInterestCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiPointOfInterestCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'PointOfInterestEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.PointOfInterestCollection GetMultiPointOfInterestCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedPointOfInterestCollection || forceFetch || _alwaysFetchPointOfInterestCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_pointOfInterestCollection);
				_pointOfInterestCollection.SuppressClearInGetMulti=!forceFetch;
				_pointOfInterestCollection.EntityFactoryToUse = entityFactoryToUse;
				_pointOfInterestCollection.GetMultiManyToOne(null, null, this, null, filter);
				_pointOfInterestCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedPointOfInterestCollection = true;
			}
			return _pointOfInterestCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'PointOfInterestCollection'. These settings will be taken into account
		/// when the property PointOfInterestCollection is requested or GetMultiPointOfInterestCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersPointOfInterestCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_pointOfInterestCollection.SortClauses=sortClauses;
			_pointOfInterestCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'CompanyOwnerEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CompanyOwnerEntity'</returns>
		public Obymobi.Data.CollectionClasses.CompanyOwnerCollection GetMultiCompanyOwnerCollectionViaCompany(bool forceFetch)
		{
			return GetMultiCompanyOwnerCollectionViaCompany(forceFetch, _companyOwnerCollectionViaCompany.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'CompanyOwnerEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.CompanyOwnerCollection GetMultiCompanyOwnerCollectionViaCompany(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedCompanyOwnerCollectionViaCompany || forceFetch || _alwaysFetchCompanyOwnerCollectionViaCompany) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_companyOwnerCollectionViaCompany);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(CurrencyFields.CurrencyId, ComparisonOperator.Equal, this.CurrencyId, "CurrencyEntity__"));
				_companyOwnerCollectionViaCompany.SuppressClearInGetMulti=!forceFetch;
				_companyOwnerCollectionViaCompany.EntityFactoryToUse = entityFactoryToUse;
				_companyOwnerCollectionViaCompany.GetMulti(filter, GetRelationsForField("CompanyOwnerCollectionViaCompany"));
				_companyOwnerCollectionViaCompany.SuppressClearInGetMulti=false;
				_alreadyFetchedCompanyOwnerCollectionViaCompany = true;
			}
			return _companyOwnerCollectionViaCompany;
		}

		/// <summary> Sets the collection parameters for the collection for 'CompanyOwnerCollectionViaCompany'. These settings will be taken into account
		/// when the property CompanyOwnerCollectionViaCompany is requested or GetMultiCompanyOwnerCollectionViaCompany is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCompanyOwnerCollectionViaCompany(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_companyOwnerCollectionViaCompany.SortClauses=sortClauses;
			_companyOwnerCollectionViaCompany.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'CountryEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CountryEntity'</returns>
		public Obymobi.Data.CollectionClasses.CountryCollection GetMultiCountryCollectionViaCompany(bool forceFetch)
		{
			return GetMultiCountryCollectionViaCompany(forceFetch, _countryCollectionViaCompany.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'CountryEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.CountryCollection GetMultiCountryCollectionViaCompany(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedCountryCollectionViaCompany || forceFetch || _alwaysFetchCountryCollectionViaCompany) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_countryCollectionViaCompany);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(CurrencyFields.CurrencyId, ComparisonOperator.Equal, this.CurrencyId, "CurrencyEntity__"));
				_countryCollectionViaCompany.SuppressClearInGetMulti=!forceFetch;
				_countryCollectionViaCompany.EntityFactoryToUse = entityFactoryToUse;
				_countryCollectionViaCompany.GetMulti(filter, GetRelationsForField("CountryCollectionViaCompany"));
				_countryCollectionViaCompany.SuppressClearInGetMulti=false;
				_alreadyFetchedCountryCollectionViaCompany = true;
			}
			return _countryCollectionViaCompany;
		}

		/// <summary> Sets the collection parameters for the collection for 'CountryCollectionViaCompany'. These settings will be taken into account
		/// when the property CountryCollectionViaCompany is requested or GetMultiCountryCollectionViaCompany is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCountryCollectionViaCompany(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_countryCollectionViaCompany.SortClauses=sortClauses;
			_countryCollectionViaCompany.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'OrderEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'OrderEntity'</returns>
		public Obymobi.Data.CollectionClasses.OrderCollection GetMultiOrderCollectionViaOrder(bool forceFetch)
		{
			return GetMultiOrderCollectionViaOrder(forceFetch, _orderCollectionViaOrder.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'OrderEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.OrderCollection GetMultiOrderCollectionViaOrder(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedOrderCollectionViaOrder || forceFetch || _alwaysFetchOrderCollectionViaOrder) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_orderCollectionViaOrder);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(CurrencyFields.CurrencyId, ComparisonOperator.Equal, this.CurrencyId, "CurrencyEntity__"));
				_orderCollectionViaOrder.SuppressClearInGetMulti=!forceFetch;
				_orderCollectionViaOrder.EntityFactoryToUse = entityFactoryToUse;
				_orderCollectionViaOrder.GetMulti(filter, GetRelationsForField("OrderCollectionViaOrder"));
				_orderCollectionViaOrder.SuppressClearInGetMulti=false;
				_alreadyFetchedOrderCollectionViaOrder = true;
			}
			return _orderCollectionViaOrder;
		}

		/// <summary> Sets the collection parameters for the collection for 'OrderCollectionViaOrder'. These settings will be taken into account
		/// when the property OrderCollectionViaOrder is requested or GetMultiOrderCollectionViaOrder is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersOrderCollectionViaOrder(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_orderCollectionViaOrder.SortClauses=sortClauses;
			_orderCollectionViaOrder.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'RouteEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'RouteEntity'</returns>
		public Obymobi.Data.CollectionClasses.RouteCollection GetMultiRouteCollectionViaCompany(bool forceFetch)
		{
			return GetMultiRouteCollectionViaCompany(forceFetch, _routeCollectionViaCompany.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'RouteEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.RouteCollection GetMultiRouteCollectionViaCompany(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedRouteCollectionViaCompany || forceFetch || _alwaysFetchRouteCollectionViaCompany) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_routeCollectionViaCompany);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(CurrencyFields.CurrencyId, ComparisonOperator.Equal, this.CurrencyId, "CurrencyEntity__"));
				_routeCollectionViaCompany.SuppressClearInGetMulti=!forceFetch;
				_routeCollectionViaCompany.EntityFactoryToUse = entityFactoryToUse;
				_routeCollectionViaCompany.GetMulti(filter, GetRelationsForField("RouteCollectionViaCompany"));
				_routeCollectionViaCompany.SuppressClearInGetMulti=false;
				_alreadyFetchedRouteCollectionViaCompany = true;
			}
			return _routeCollectionViaCompany;
		}

		/// <summary> Sets the collection parameters for the collection for 'RouteCollectionViaCompany'. These settings will be taken into account
		/// when the property RouteCollectionViaCompany is requested or GetMultiRouteCollectionViaCompany is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersRouteCollectionViaCompany(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_routeCollectionViaCompany.SortClauses=sortClauses;
			_routeCollectionViaCompany.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'SupportpoolEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'SupportpoolEntity'</returns>
		public Obymobi.Data.CollectionClasses.SupportpoolCollection GetMultiSupportpoolCollectionViaCompany(bool forceFetch)
		{
			return GetMultiSupportpoolCollectionViaCompany(forceFetch, _supportpoolCollectionViaCompany.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'SupportpoolEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.SupportpoolCollection GetMultiSupportpoolCollectionViaCompany(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedSupportpoolCollectionViaCompany || forceFetch || _alwaysFetchSupportpoolCollectionViaCompany) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_supportpoolCollectionViaCompany);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(CurrencyFields.CurrencyId, ComparisonOperator.Equal, this.CurrencyId, "CurrencyEntity__"));
				_supportpoolCollectionViaCompany.SuppressClearInGetMulti=!forceFetch;
				_supportpoolCollectionViaCompany.EntityFactoryToUse = entityFactoryToUse;
				_supportpoolCollectionViaCompany.GetMulti(filter, GetRelationsForField("SupportpoolCollectionViaCompany"));
				_supportpoolCollectionViaCompany.SuppressClearInGetMulti=false;
				_alreadyFetchedSupportpoolCollectionViaCompany = true;
			}
			return _supportpoolCollectionViaCompany;
		}

		/// <summary> Sets the collection parameters for the collection for 'SupportpoolCollectionViaCompany'. These settings will be taken into account
		/// when the property SupportpoolCollectionViaCompany is requested or GetMultiSupportpoolCollectionViaCompany is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersSupportpoolCollectionViaCompany(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_supportpoolCollectionViaCompany.SortClauses=sortClauses;
			_supportpoolCollectionViaCompany.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("CompanyCollection", _companyCollection);
			toReturn.Add("CountryCollection", _countryCollection);
			toReturn.Add("OrderCollection", _orderCollection);
			toReturn.Add("PointOfInterestCollection", _pointOfInterestCollection);
			toReturn.Add("CompanyOwnerCollectionViaCompany", _companyOwnerCollectionViaCompany);
			toReturn.Add("CountryCollectionViaCompany", _countryCollectionViaCompany);
			toReturn.Add("OrderCollectionViaOrder", _orderCollectionViaOrder);
			toReturn.Add("RouteCollectionViaCompany", _routeCollectionViaCompany);
			toReturn.Add("SupportpoolCollectionViaCompany", _supportpoolCollectionViaCompany);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="currencyId">PK value for Currency which data should be fetched into this Currency object</param>
		/// <param name="validator">The validator object for this CurrencyEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int32 currencyId, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(currencyId, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_companyCollection = new Obymobi.Data.CollectionClasses.CompanyCollection();
			_companyCollection.SetContainingEntityInfo(this, "CurrencyEntity");

			_countryCollection = new Obymobi.Data.CollectionClasses.CountryCollection();
			_countryCollection.SetContainingEntityInfo(this, "CurrencyEntity");

			_orderCollection = new Obymobi.Data.CollectionClasses.OrderCollection();
			_orderCollection.SetContainingEntityInfo(this, "CurrencyEntity");

			_pointOfInterestCollection = new Obymobi.Data.CollectionClasses.PointOfInterestCollection();
			_pointOfInterestCollection.SetContainingEntityInfo(this, "CurrencyEntity");
			_companyOwnerCollectionViaCompany = new Obymobi.Data.CollectionClasses.CompanyOwnerCollection();
			_countryCollectionViaCompany = new Obymobi.Data.CollectionClasses.CountryCollection();
			_orderCollectionViaOrder = new Obymobi.Data.CollectionClasses.OrderCollection();
			_routeCollectionViaCompany = new Obymobi.Data.CollectionClasses.RouteCollection();
			_supportpoolCollectionViaCompany = new Obymobi.Data.CollectionClasses.SupportpoolCollection();
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CurrencyId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Name", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Symbol", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CodeIso4217", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedUTC", fieldHashtable);
		}
		#endregion

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="currencyId">PK value for Currency which data should be fetched into this Currency object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int32 currencyId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)CurrencyFieldIndex.CurrencyId].ForcedCurrentValueWrite(currencyId);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateCurrencyDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new CurrencyEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static CurrencyRelations Relations
		{
			get	{ return new CurrencyRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Company' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCompanyCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CompanyCollection(), (IEntityRelation)GetRelationsForField("CompanyCollection")[0], (int)Obymobi.Data.EntityType.CurrencyEntity, (int)Obymobi.Data.EntityType.CompanyEntity, 0, null, null, null, "CompanyCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Country' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCountryCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CountryCollection(), (IEntityRelation)GetRelationsForField("CountryCollection")[0], (int)Obymobi.Data.EntityType.CurrencyEntity, (int)Obymobi.Data.EntityType.CountryEntity, 0, null, null, null, "CountryCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Order' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathOrderCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.OrderCollection(), (IEntityRelation)GetRelationsForField("OrderCollection")[0], (int)Obymobi.Data.EntityType.CurrencyEntity, (int)Obymobi.Data.EntityType.OrderEntity, 0, null, null, null, "OrderCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'PointOfInterest' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPointOfInterestCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.PointOfInterestCollection(), (IEntityRelation)GetRelationsForField("PointOfInterestCollection")[0], (int)Obymobi.Data.EntityType.CurrencyEntity, (int)Obymobi.Data.EntityType.PointOfInterestEntity, 0, null, null, null, "PointOfInterestCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'CompanyOwner'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCompanyOwnerCollectionViaCompany
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.CompanyEntityUsingCurrencyId;
				intermediateRelation.SetAliases(string.Empty, "Company_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CompanyOwnerCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.CurrencyEntity, (int)Obymobi.Data.EntityType.CompanyOwnerEntity, 0, null, null, GetRelationsForField("CompanyOwnerCollectionViaCompany"), "CompanyOwnerCollectionViaCompany", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Country'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCountryCollectionViaCompany
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.CompanyEntityUsingCurrencyId;
				intermediateRelation.SetAliases(string.Empty, "Company_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CountryCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.CurrencyEntity, (int)Obymobi.Data.EntityType.CountryEntity, 0, null, null, GetRelationsForField("CountryCollectionViaCompany"), "CountryCollectionViaCompany", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Order'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathOrderCollectionViaOrder
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.OrderEntityUsingCurrencyId;
				intermediateRelation.SetAliases(string.Empty, "Order_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.OrderCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.CurrencyEntity, (int)Obymobi.Data.EntityType.OrderEntity, 0, null, null, GetRelationsForField("OrderCollectionViaOrder"), "OrderCollectionViaOrder", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Route'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathRouteCollectionViaCompany
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.CompanyEntityUsingCurrencyId;
				intermediateRelation.SetAliases(string.Empty, "Company_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.RouteCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.CurrencyEntity, (int)Obymobi.Data.EntityType.RouteEntity, 0, null, null, GetRelationsForField("RouteCollectionViaCompany"), "RouteCollectionViaCompany", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Supportpool'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathSupportpoolCollectionViaCompany
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.CompanyEntityUsingCurrencyId;
				intermediateRelation.SetAliases(string.Empty, "Company_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.SupportpoolCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.CurrencyEntity, (int)Obymobi.Data.EntityType.SupportpoolEntity, 0, null, null, GetRelationsForField("SupportpoolCollectionViaCompany"), "SupportpoolCollectionViaCompany", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The CurrencyId property of the Entity Currency<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Currency"."CurrencyId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 CurrencyId
		{
			get { return (System.Int32)GetValue((int)CurrencyFieldIndex.CurrencyId, true); }
			set	{ SetValue((int)CurrencyFieldIndex.CurrencyId, value, true); }
		}

		/// <summary> The Name property of the Entity Currency<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Currency"."Name"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Name
		{
			get { return (System.String)GetValue((int)CurrencyFieldIndex.Name, true); }
			set	{ SetValue((int)CurrencyFieldIndex.Name, value, true); }
		}

		/// <summary> The Symbol property of the Entity Currency<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Currency"."Symbol"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Symbol
		{
			get { return (System.String)GetValue((int)CurrencyFieldIndex.Symbol, true); }
			set	{ SetValue((int)CurrencyFieldIndex.Symbol, value, true); }
		}

		/// <summary> The CodeIso4217 property of the Entity Currency<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Currency"."CodeIso4217"<br/>
		/// Table field type characteristics (type, precision, scale, length): Char, 0, 0, 3<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String CodeIso4217
		{
			get { return (System.String)GetValue((int)CurrencyFieldIndex.CodeIso4217, true); }
			set	{ SetValue((int)CurrencyFieldIndex.CodeIso4217, value, true); }
		}

		/// <summary> The CreatedBy property of the Entity Currency<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Currency"."CreatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CreatedBy
		{
			get { return (System.Int32)GetValue((int)CurrencyFieldIndex.CreatedBy, true); }
			set	{ SetValue((int)CurrencyFieldIndex.CreatedBy, value, true); }
		}

		/// <summary> The UpdatedBy property of the Entity Currency<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Currency"."UpdatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 UpdatedBy
		{
			get { return (System.Int32)GetValue((int)CurrencyFieldIndex.UpdatedBy, true); }
			set	{ SetValue((int)CurrencyFieldIndex.UpdatedBy, value, true); }
		}

		/// <summary> The CreatedUTC property of the Entity Currency<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Currency"."CreatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CreatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)CurrencyFieldIndex.CreatedUTC, false); }
			set	{ SetValue((int)CurrencyFieldIndex.CreatedUTC, value, true); }
		}

		/// <summary> The UpdatedUTC property of the Entity Currency<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Currency"."UpdatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)CurrencyFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)CurrencyFieldIndex.UpdatedUTC, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'CompanyEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCompanyCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.CompanyCollection CompanyCollection
		{
			get	{ return GetMultiCompanyCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CompanyCollection. When set to true, CompanyCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CompanyCollection is accessed. You can always execute/ a forced fetch by calling GetMultiCompanyCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCompanyCollection
		{
			get	{ return _alwaysFetchCompanyCollection; }
			set	{ _alwaysFetchCompanyCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property CompanyCollection already has been fetched. Setting this property to false when CompanyCollection has been fetched
		/// will clear the CompanyCollection collection well. Setting this property to true while CompanyCollection hasn't been fetched disables lazy loading for CompanyCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCompanyCollection
		{
			get { return _alreadyFetchedCompanyCollection;}
			set 
			{
				if(_alreadyFetchedCompanyCollection && !value && (_companyCollection != null))
				{
					_companyCollection.Clear();
				}
				_alreadyFetchedCompanyCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'CountryEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCountryCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.CountryCollection CountryCollection
		{
			get	{ return GetMultiCountryCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CountryCollection. When set to true, CountryCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CountryCollection is accessed. You can always execute/ a forced fetch by calling GetMultiCountryCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCountryCollection
		{
			get	{ return _alwaysFetchCountryCollection; }
			set	{ _alwaysFetchCountryCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property CountryCollection already has been fetched. Setting this property to false when CountryCollection has been fetched
		/// will clear the CountryCollection collection well. Setting this property to true while CountryCollection hasn't been fetched disables lazy loading for CountryCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCountryCollection
		{
			get { return _alreadyFetchedCountryCollection;}
			set 
			{
				if(_alreadyFetchedCountryCollection && !value && (_countryCollection != null))
				{
					_countryCollection.Clear();
				}
				_alreadyFetchedCountryCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'OrderEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiOrderCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.OrderCollection OrderCollection
		{
			get	{ return GetMultiOrderCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for OrderCollection. When set to true, OrderCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time OrderCollection is accessed. You can always execute/ a forced fetch by calling GetMultiOrderCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchOrderCollection
		{
			get	{ return _alwaysFetchOrderCollection; }
			set	{ _alwaysFetchOrderCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property OrderCollection already has been fetched. Setting this property to false when OrderCollection has been fetched
		/// will clear the OrderCollection collection well. Setting this property to true while OrderCollection hasn't been fetched disables lazy loading for OrderCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedOrderCollection
		{
			get { return _alreadyFetchedOrderCollection;}
			set 
			{
				if(_alreadyFetchedOrderCollection && !value && (_orderCollection != null))
				{
					_orderCollection.Clear();
				}
				_alreadyFetchedOrderCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'PointOfInterestEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiPointOfInterestCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.PointOfInterestCollection PointOfInterestCollection
		{
			get	{ return GetMultiPointOfInterestCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for PointOfInterestCollection. When set to true, PointOfInterestCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time PointOfInterestCollection is accessed. You can always execute/ a forced fetch by calling GetMultiPointOfInterestCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPointOfInterestCollection
		{
			get	{ return _alwaysFetchPointOfInterestCollection; }
			set	{ _alwaysFetchPointOfInterestCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property PointOfInterestCollection already has been fetched. Setting this property to false when PointOfInterestCollection has been fetched
		/// will clear the PointOfInterestCollection collection well. Setting this property to true while PointOfInterestCollection hasn't been fetched disables lazy loading for PointOfInterestCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPointOfInterestCollection
		{
			get { return _alreadyFetchedPointOfInterestCollection;}
			set 
			{
				if(_alreadyFetchedPointOfInterestCollection && !value && (_pointOfInterestCollection != null))
				{
					_pointOfInterestCollection.Clear();
				}
				_alreadyFetchedPointOfInterestCollection = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'CompanyOwnerEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCompanyOwnerCollectionViaCompany()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.CompanyOwnerCollection CompanyOwnerCollectionViaCompany
		{
			get { return GetMultiCompanyOwnerCollectionViaCompany(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CompanyOwnerCollectionViaCompany. When set to true, CompanyOwnerCollectionViaCompany is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CompanyOwnerCollectionViaCompany is accessed. You can always execute a forced fetch by calling GetMultiCompanyOwnerCollectionViaCompany(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCompanyOwnerCollectionViaCompany
		{
			get	{ return _alwaysFetchCompanyOwnerCollectionViaCompany; }
			set	{ _alwaysFetchCompanyOwnerCollectionViaCompany = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CompanyOwnerCollectionViaCompany already has been fetched. Setting this property to false when CompanyOwnerCollectionViaCompany has been fetched
		/// will clear the CompanyOwnerCollectionViaCompany collection well. Setting this property to true while CompanyOwnerCollectionViaCompany hasn't been fetched disables lazy loading for CompanyOwnerCollectionViaCompany</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCompanyOwnerCollectionViaCompany
		{
			get { return _alreadyFetchedCompanyOwnerCollectionViaCompany;}
			set 
			{
				if(_alreadyFetchedCompanyOwnerCollectionViaCompany && !value && (_companyOwnerCollectionViaCompany != null))
				{
					_companyOwnerCollectionViaCompany.Clear();
				}
				_alreadyFetchedCompanyOwnerCollectionViaCompany = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'CountryEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCountryCollectionViaCompany()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.CountryCollection CountryCollectionViaCompany
		{
			get { return GetMultiCountryCollectionViaCompany(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CountryCollectionViaCompany. When set to true, CountryCollectionViaCompany is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CountryCollectionViaCompany is accessed. You can always execute a forced fetch by calling GetMultiCountryCollectionViaCompany(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCountryCollectionViaCompany
		{
			get	{ return _alwaysFetchCountryCollectionViaCompany; }
			set	{ _alwaysFetchCountryCollectionViaCompany = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CountryCollectionViaCompany already has been fetched. Setting this property to false when CountryCollectionViaCompany has been fetched
		/// will clear the CountryCollectionViaCompany collection well. Setting this property to true while CountryCollectionViaCompany hasn't been fetched disables lazy loading for CountryCollectionViaCompany</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCountryCollectionViaCompany
		{
			get { return _alreadyFetchedCountryCollectionViaCompany;}
			set 
			{
				if(_alreadyFetchedCountryCollectionViaCompany && !value && (_countryCollectionViaCompany != null))
				{
					_countryCollectionViaCompany.Clear();
				}
				_alreadyFetchedCountryCollectionViaCompany = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'OrderEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiOrderCollectionViaOrder()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.OrderCollection OrderCollectionViaOrder
		{
			get { return GetMultiOrderCollectionViaOrder(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for OrderCollectionViaOrder. When set to true, OrderCollectionViaOrder is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time OrderCollectionViaOrder is accessed. You can always execute a forced fetch by calling GetMultiOrderCollectionViaOrder(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchOrderCollectionViaOrder
		{
			get	{ return _alwaysFetchOrderCollectionViaOrder; }
			set	{ _alwaysFetchOrderCollectionViaOrder = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property OrderCollectionViaOrder already has been fetched. Setting this property to false when OrderCollectionViaOrder has been fetched
		/// will clear the OrderCollectionViaOrder collection well. Setting this property to true while OrderCollectionViaOrder hasn't been fetched disables lazy loading for OrderCollectionViaOrder</summary>
		[Browsable(false)]
		public bool AlreadyFetchedOrderCollectionViaOrder
		{
			get { return _alreadyFetchedOrderCollectionViaOrder;}
			set 
			{
				if(_alreadyFetchedOrderCollectionViaOrder && !value && (_orderCollectionViaOrder != null))
				{
					_orderCollectionViaOrder.Clear();
				}
				_alreadyFetchedOrderCollectionViaOrder = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'RouteEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiRouteCollectionViaCompany()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.RouteCollection RouteCollectionViaCompany
		{
			get { return GetMultiRouteCollectionViaCompany(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for RouteCollectionViaCompany. When set to true, RouteCollectionViaCompany is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time RouteCollectionViaCompany is accessed. You can always execute a forced fetch by calling GetMultiRouteCollectionViaCompany(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchRouteCollectionViaCompany
		{
			get	{ return _alwaysFetchRouteCollectionViaCompany; }
			set	{ _alwaysFetchRouteCollectionViaCompany = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property RouteCollectionViaCompany already has been fetched. Setting this property to false when RouteCollectionViaCompany has been fetched
		/// will clear the RouteCollectionViaCompany collection well. Setting this property to true while RouteCollectionViaCompany hasn't been fetched disables lazy loading for RouteCollectionViaCompany</summary>
		[Browsable(false)]
		public bool AlreadyFetchedRouteCollectionViaCompany
		{
			get { return _alreadyFetchedRouteCollectionViaCompany;}
			set 
			{
				if(_alreadyFetchedRouteCollectionViaCompany && !value && (_routeCollectionViaCompany != null))
				{
					_routeCollectionViaCompany.Clear();
				}
				_alreadyFetchedRouteCollectionViaCompany = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'SupportpoolEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiSupportpoolCollectionViaCompany()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.SupportpoolCollection SupportpoolCollectionViaCompany
		{
			get { return GetMultiSupportpoolCollectionViaCompany(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for SupportpoolCollectionViaCompany. When set to true, SupportpoolCollectionViaCompany is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time SupportpoolCollectionViaCompany is accessed. You can always execute a forced fetch by calling GetMultiSupportpoolCollectionViaCompany(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchSupportpoolCollectionViaCompany
		{
			get	{ return _alwaysFetchSupportpoolCollectionViaCompany; }
			set	{ _alwaysFetchSupportpoolCollectionViaCompany = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property SupportpoolCollectionViaCompany already has been fetched. Setting this property to false when SupportpoolCollectionViaCompany has been fetched
		/// will clear the SupportpoolCollectionViaCompany collection well. Setting this property to true while SupportpoolCollectionViaCompany hasn't been fetched disables lazy loading for SupportpoolCollectionViaCompany</summary>
		[Browsable(false)]
		public bool AlreadyFetchedSupportpoolCollectionViaCompany
		{
			get { return _alreadyFetchedSupportpoolCollectionViaCompany;}
			set 
			{
				if(_alreadyFetchedSupportpoolCollectionViaCompany && !value && (_supportpoolCollectionViaCompany != null))
				{
					_supportpoolCollectionViaCompany.Clear();
				}
				_alreadyFetchedSupportpoolCollectionViaCompany = value;
			}
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the Obymobi.Data.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)Obymobi.Data.EntityType.CurrencyEntity; }
		}

		#endregion

		
		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
