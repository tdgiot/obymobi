﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
using System.Runtime.Serialization;
using System.Data;
using System.Xml.Serialization;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity base class which represents the base class for the entity 'Amenity'.<br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public abstract partial class AmenityEntityBase : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		public new string LLBLGenProEntityName {
			get { return "AmenityEntity"; }
		}
	
		#region Class Member Declarations
		private Obymobi.Data.CollectionClasses.AmenityLanguageCollection	_amenityLanguageCollection;
		private bool	_alwaysFetchAmenityLanguageCollection, _alreadyFetchedAmenityLanguageCollection;
		private Obymobi.Data.CollectionClasses.CompanyAmenityCollection	_companyAmenityCollection;
		private bool	_alwaysFetchCompanyAmenityCollection, _alreadyFetchedCompanyAmenityCollection;
		private Obymobi.Data.CollectionClasses.CustomTextCollection	_customTextCollection;
		private bool	_alwaysFetchCustomTextCollection, _alreadyFetchedCustomTextCollection;
		private Obymobi.Data.CollectionClasses.PointOfInterestAmenityCollection	_pointOfInterestAmenityCollection;
		private bool	_alwaysFetchPointOfInterestAmenityCollection, _alreadyFetchedPointOfInterestAmenityCollection;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name AmenityLanguageCollection</summary>
			public static readonly string AmenityLanguageCollection = "AmenityLanguageCollection";
			/// <summary>Member name CompanyAmenityCollection</summary>
			public static readonly string CompanyAmenityCollection = "CompanyAmenityCollection";
			/// <summary>Member name CustomTextCollection</summary>
			public static readonly string CustomTextCollection = "CustomTextCollection";
			/// <summary>Member name PointOfInterestAmenityCollection</summary>
			public static readonly string PointOfInterestAmenityCollection = "PointOfInterestAmenityCollection";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static AmenityEntityBase()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		protected AmenityEntityBase() :base("AmenityEntity")
		{
			InitClassEmpty(null);
		}

		/// <summary>CTor</summary>
		/// <param name="amenityId">PK value for Amenity which data should be fetched into this Amenity object</param>
		protected AmenityEntityBase(System.Int32 amenityId):base("AmenityEntity")
		{
			InitClassFetch(amenityId, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="amenityId">PK value for Amenity which data should be fetched into this Amenity object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		protected AmenityEntityBase(System.Int32 amenityId, IPrefetchPath prefetchPathToUse): base("AmenityEntity")
		{
			InitClassFetch(amenityId, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="amenityId">PK value for Amenity which data should be fetched into this Amenity object</param>
		/// <param name="validator">The custom validator object for this AmenityEntity</param>
		protected AmenityEntityBase(System.Int32 amenityId, IValidator validator):base("AmenityEntity")
		{
			InitClassFetch(amenityId, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected AmenityEntityBase(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_amenityLanguageCollection = (Obymobi.Data.CollectionClasses.AmenityLanguageCollection)info.GetValue("_amenityLanguageCollection", typeof(Obymobi.Data.CollectionClasses.AmenityLanguageCollection));
			_alwaysFetchAmenityLanguageCollection = info.GetBoolean("_alwaysFetchAmenityLanguageCollection");
			_alreadyFetchedAmenityLanguageCollection = info.GetBoolean("_alreadyFetchedAmenityLanguageCollection");

			_companyAmenityCollection = (Obymobi.Data.CollectionClasses.CompanyAmenityCollection)info.GetValue("_companyAmenityCollection", typeof(Obymobi.Data.CollectionClasses.CompanyAmenityCollection));
			_alwaysFetchCompanyAmenityCollection = info.GetBoolean("_alwaysFetchCompanyAmenityCollection");
			_alreadyFetchedCompanyAmenityCollection = info.GetBoolean("_alreadyFetchedCompanyAmenityCollection");

			_customTextCollection = (Obymobi.Data.CollectionClasses.CustomTextCollection)info.GetValue("_customTextCollection", typeof(Obymobi.Data.CollectionClasses.CustomTextCollection));
			_alwaysFetchCustomTextCollection = info.GetBoolean("_alwaysFetchCustomTextCollection");
			_alreadyFetchedCustomTextCollection = info.GetBoolean("_alreadyFetchedCustomTextCollection");

			_pointOfInterestAmenityCollection = (Obymobi.Data.CollectionClasses.PointOfInterestAmenityCollection)info.GetValue("_pointOfInterestAmenityCollection", typeof(Obymobi.Data.CollectionClasses.PointOfInterestAmenityCollection));
			_alwaysFetchPointOfInterestAmenityCollection = info.GetBoolean("_alwaysFetchPointOfInterestAmenityCollection");
			_alreadyFetchedPointOfInterestAmenityCollection = info.GetBoolean("_alreadyFetchedPointOfInterestAmenityCollection");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}	

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedAmenityLanguageCollection = (_amenityLanguageCollection.Count > 0);
			_alreadyFetchedCompanyAmenityCollection = (_companyAmenityCollection.Count > 0);
			_alreadyFetchedCustomTextCollection = (_customTextCollection.Count > 0);
			_alreadyFetchedPointOfInterestAmenityCollection = (_pointOfInterestAmenityCollection.Count > 0);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "AmenityLanguageCollection":
					toReturn.Add(Relations.AmenityLanguageEntityUsingAmenityId);
					break;
				case "CompanyAmenityCollection":
					toReturn.Add(Relations.CompanyAmenityEntityUsingAmenityId);
					break;
				case "CustomTextCollection":
					toReturn.Add(Relations.CustomTextEntityUsingAmenityId);
					break;
				case "PointOfInterestAmenityCollection":
					toReturn.Add(Relations.PointOfInterestAmenityEntityUsingAmenityId);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_amenityLanguageCollection", (!this.MarkedForDeletion?_amenityLanguageCollection:null));
			info.AddValue("_alwaysFetchAmenityLanguageCollection", _alwaysFetchAmenityLanguageCollection);
			info.AddValue("_alreadyFetchedAmenityLanguageCollection", _alreadyFetchedAmenityLanguageCollection);
			info.AddValue("_companyAmenityCollection", (!this.MarkedForDeletion?_companyAmenityCollection:null));
			info.AddValue("_alwaysFetchCompanyAmenityCollection", _alwaysFetchCompanyAmenityCollection);
			info.AddValue("_alreadyFetchedCompanyAmenityCollection", _alreadyFetchedCompanyAmenityCollection);
			info.AddValue("_customTextCollection", (!this.MarkedForDeletion?_customTextCollection:null));
			info.AddValue("_alwaysFetchCustomTextCollection", _alwaysFetchCustomTextCollection);
			info.AddValue("_alreadyFetchedCustomTextCollection", _alreadyFetchedCustomTextCollection);
			info.AddValue("_pointOfInterestAmenityCollection", (!this.MarkedForDeletion?_pointOfInterestAmenityCollection:null));
			info.AddValue("_alwaysFetchPointOfInterestAmenityCollection", _alwaysFetchPointOfInterestAmenityCollection);
			info.AddValue("_alreadyFetchedPointOfInterestAmenityCollection", _alreadyFetchedPointOfInterestAmenityCollection);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "AmenityLanguageCollection":
					_alreadyFetchedAmenityLanguageCollection = true;
					if(entity!=null)
					{
						this.AmenityLanguageCollection.Add((AmenityLanguageEntity)entity);
					}
					break;
				case "CompanyAmenityCollection":
					_alreadyFetchedCompanyAmenityCollection = true;
					if(entity!=null)
					{
						this.CompanyAmenityCollection.Add((CompanyAmenityEntity)entity);
					}
					break;
				case "CustomTextCollection":
					_alreadyFetchedCustomTextCollection = true;
					if(entity!=null)
					{
						this.CustomTextCollection.Add((CustomTextEntity)entity);
					}
					break;
				case "PointOfInterestAmenityCollection":
					_alreadyFetchedPointOfInterestAmenityCollection = true;
					if(entity!=null)
					{
						this.PointOfInterestAmenityCollection.Add((PointOfInterestAmenityEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "AmenityLanguageCollection":
					_amenityLanguageCollection.Add((AmenityLanguageEntity)relatedEntity);
					break;
				case "CompanyAmenityCollection":
					_companyAmenityCollection.Add((CompanyAmenityEntity)relatedEntity);
					break;
				case "CustomTextCollection":
					_customTextCollection.Add((CustomTextEntity)relatedEntity);
					break;
				case "PointOfInterestAmenityCollection":
					_pointOfInterestAmenityCollection.Add((PointOfInterestAmenityEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "AmenityLanguageCollection":
					this.PerformRelatedEntityRemoval(_amenityLanguageCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "CompanyAmenityCollection":
					this.PerformRelatedEntityRemoval(_companyAmenityCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "CustomTextCollection":
					this.PerformRelatedEntityRemoval(_customTextCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "PointOfInterestAmenityCollection":
					this.PerformRelatedEntityRemoval(_pointOfInterestAmenityCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_amenityLanguageCollection);
			toReturn.Add(_companyAmenityCollection);
			toReturn.Add(_customTextCollection);
			toReturn.Add(_pointOfInterestAmenityCollection);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="amenityId">PK value for Amenity which data should be fetched into this Amenity object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 amenityId)
		{
			return FetchUsingPK(amenityId, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="amenityId">PK value for Amenity which data should be fetched into this Amenity object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 amenityId, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(amenityId, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="amenityId">PK value for Amenity which data should be fetched into this Amenity object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 amenityId, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(amenityId, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="amenityId">PK value for Amenity which data should be fetched into this Amenity object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 amenityId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(amenityId, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.AmenityId, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new AmenityRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'AmenityLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'AmenityLanguageEntity'</returns>
		public Obymobi.Data.CollectionClasses.AmenityLanguageCollection GetMultiAmenityLanguageCollection(bool forceFetch)
		{
			return GetMultiAmenityLanguageCollection(forceFetch, _amenityLanguageCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'AmenityLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'AmenityLanguageEntity'</returns>
		public Obymobi.Data.CollectionClasses.AmenityLanguageCollection GetMultiAmenityLanguageCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiAmenityLanguageCollection(forceFetch, _amenityLanguageCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'AmenityLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.AmenityLanguageCollection GetMultiAmenityLanguageCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiAmenityLanguageCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'AmenityLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.AmenityLanguageCollection GetMultiAmenityLanguageCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedAmenityLanguageCollection || forceFetch || _alwaysFetchAmenityLanguageCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_amenityLanguageCollection);
				_amenityLanguageCollection.SuppressClearInGetMulti=!forceFetch;
				_amenityLanguageCollection.EntityFactoryToUse = entityFactoryToUse;
				_amenityLanguageCollection.GetMultiManyToOne(this, null, filter);
				_amenityLanguageCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedAmenityLanguageCollection = true;
			}
			return _amenityLanguageCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'AmenityLanguageCollection'. These settings will be taken into account
		/// when the property AmenityLanguageCollection is requested or GetMultiAmenityLanguageCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersAmenityLanguageCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_amenityLanguageCollection.SortClauses=sortClauses;
			_amenityLanguageCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'CompanyAmenityEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CompanyAmenityEntity'</returns>
		public Obymobi.Data.CollectionClasses.CompanyAmenityCollection GetMultiCompanyAmenityCollection(bool forceFetch)
		{
			return GetMultiCompanyAmenityCollection(forceFetch, _companyAmenityCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CompanyAmenityEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'CompanyAmenityEntity'</returns>
		public Obymobi.Data.CollectionClasses.CompanyAmenityCollection GetMultiCompanyAmenityCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiCompanyAmenityCollection(forceFetch, _companyAmenityCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'CompanyAmenityEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.CompanyAmenityCollection GetMultiCompanyAmenityCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiCompanyAmenityCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CompanyAmenityEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.CompanyAmenityCollection GetMultiCompanyAmenityCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedCompanyAmenityCollection || forceFetch || _alwaysFetchCompanyAmenityCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_companyAmenityCollection);
				_companyAmenityCollection.SuppressClearInGetMulti=!forceFetch;
				_companyAmenityCollection.EntityFactoryToUse = entityFactoryToUse;
				_companyAmenityCollection.GetMultiManyToOne(this, null, filter);
				_companyAmenityCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedCompanyAmenityCollection = true;
			}
			return _companyAmenityCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'CompanyAmenityCollection'. These settings will be taken into account
		/// when the property CompanyAmenityCollection is requested or GetMultiCompanyAmenityCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCompanyAmenityCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_companyAmenityCollection.SortClauses=sortClauses;
			_companyAmenityCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'CustomTextEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CustomTextEntity'</returns>
		public Obymobi.Data.CollectionClasses.CustomTextCollection GetMultiCustomTextCollection(bool forceFetch)
		{
			return GetMultiCustomTextCollection(forceFetch, _customTextCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CustomTextEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'CustomTextEntity'</returns>
		public Obymobi.Data.CollectionClasses.CustomTextCollection GetMultiCustomTextCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiCustomTextCollection(forceFetch, _customTextCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'CustomTextEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.CustomTextCollection GetMultiCustomTextCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiCustomTextCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CustomTextEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.CustomTextCollection GetMultiCustomTextCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedCustomTextCollection || forceFetch || _alwaysFetchCustomTextCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_customTextCollection);
				_customTextCollection.SuppressClearInGetMulti=!forceFetch;
				_customTextCollection.EntityFactoryToUse = entityFactoryToUse;
				_customTextCollection.GetMultiManyToOne(null, null, null, null, this, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, filter);
				_customTextCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedCustomTextCollection = true;
			}
			return _customTextCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'CustomTextCollection'. These settings will be taken into account
		/// when the property CustomTextCollection is requested or GetMultiCustomTextCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCustomTextCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_customTextCollection.SortClauses=sortClauses;
			_customTextCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'PointOfInterestAmenityEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'PointOfInterestAmenityEntity'</returns>
		public Obymobi.Data.CollectionClasses.PointOfInterestAmenityCollection GetMultiPointOfInterestAmenityCollection(bool forceFetch)
		{
			return GetMultiPointOfInterestAmenityCollection(forceFetch, _pointOfInterestAmenityCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'PointOfInterestAmenityEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'PointOfInterestAmenityEntity'</returns>
		public Obymobi.Data.CollectionClasses.PointOfInterestAmenityCollection GetMultiPointOfInterestAmenityCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiPointOfInterestAmenityCollection(forceFetch, _pointOfInterestAmenityCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'PointOfInterestAmenityEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.PointOfInterestAmenityCollection GetMultiPointOfInterestAmenityCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiPointOfInterestAmenityCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'PointOfInterestAmenityEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.PointOfInterestAmenityCollection GetMultiPointOfInterestAmenityCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedPointOfInterestAmenityCollection || forceFetch || _alwaysFetchPointOfInterestAmenityCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_pointOfInterestAmenityCollection);
				_pointOfInterestAmenityCollection.SuppressClearInGetMulti=!forceFetch;
				_pointOfInterestAmenityCollection.EntityFactoryToUse = entityFactoryToUse;
				_pointOfInterestAmenityCollection.GetMultiManyToOne(this, null, filter);
				_pointOfInterestAmenityCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedPointOfInterestAmenityCollection = true;
			}
			return _pointOfInterestAmenityCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'PointOfInterestAmenityCollection'. These settings will be taken into account
		/// when the property PointOfInterestAmenityCollection is requested or GetMultiPointOfInterestAmenityCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersPointOfInterestAmenityCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_pointOfInterestAmenityCollection.SortClauses=sortClauses;
			_pointOfInterestAmenityCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("AmenityLanguageCollection", _amenityLanguageCollection);
			toReturn.Add("CompanyAmenityCollection", _companyAmenityCollection);
			toReturn.Add("CustomTextCollection", _customTextCollection);
			toReturn.Add("PointOfInterestAmenityCollection", _pointOfInterestAmenityCollection);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="amenityId">PK value for Amenity which data should be fetched into this Amenity object</param>
		/// <param name="validator">The validator object for this AmenityEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int32 amenityId, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(amenityId, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_amenityLanguageCollection = new Obymobi.Data.CollectionClasses.AmenityLanguageCollection();
			_amenityLanguageCollection.SetContainingEntityInfo(this, "AmenityEntity");

			_companyAmenityCollection = new Obymobi.Data.CollectionClasses.CompanyAmenityCollection();
			_companyAmenityCollection.SetContainingEntityInfo(this, "AmenityEntity");

			_customTextCollection = new Obymobi.Data.CollectionClasses.CustomTextCollection();
			_customTextCollection.SetContainingEntityInfo(this, "AmenityEntity");

			_pointOfInterestAmenityCollection = new Obymobi.Data.CollectionClasses.PointOfInterestAmenityCollection();
			_pointOfInterestAmenityCollection.SetContainingEntityInfo(this, "AmenityEntity");
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AmenityId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Name", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastModifiedUTC", fieldHashtable);
		}
		#endregion

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="amenityId">PK value for Amenity which data should be fetched into this Amenity object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int32 amenityId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)AmenityFieldIndex.AmenityId].ForcedCurrentValueWrite(amenityId);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateAmenityDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new AmenityEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static AmenityRelations Relations
		{
			get	{ return new AmenityRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'AmenityLanguage' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAmenityLanguageCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.AmenityLanguageCollection(), (IEntityRelation)GetRelationsForField("AmenityLanguageCollection")[0], (int)Obymobi.Data.EntityType.AmenityEntity, (int)Obymobi.Data.EntityType.AmenityLanguageEntity, 0, null, null, null, "AmenityLanguageCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'CompanyAmenity' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCompanyAmenityCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CompanyAmenityCollection(), (IEntityRelation)GetRelationsForField("CompanyAmenityCollection")[0], (int)Obymobi.Data.EntityType.AmenityEntity, (int)Obymobi.Data.EntityType.CompanyAmenityEntity, 0, null, null, null, "CompanyAmenityCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'CustomText' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCustomTextCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CustomTextCollection(), (IEntityRelation)GetRelationsForField("CustomTextCollection")[0], (int)Obymobi.Data.EntityType.AmenityEntity, (int)Obymobi.Data.EntityType.CustomTextEntity, 0, null, null, null, "CustomTextCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'PointOfInterestAmenity' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPointOfInterestAmenityCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.PointOfInterestAmenityCollection(), (IEntityRelation)GetRelationsForField("PointOfInterestAmenityCollection")[0], (int)Obymobi.Data.EntityType.AmenityEntity, (int)Obymobi.Data.EntityType.PointOfInterestAmenityEntity, 0, null, null, null, "PointOfInterestAmenityCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The AmenityId property of the Entity Amenity<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Amenity"."AmenityId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 AmenityId
		{
			get { return (System.Int32)GetValue((int)AmenityFieldIndex.AmenityId, true); }
			set	{ SetValue((int)AmenityFieldIndex.AmenityId, value, true); }
		}

		/// <summary> The Name property of the Entity Amenity<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Amenity"."Name"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 255<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String Name
		{
			get { return (System.String)GetValue((int)AmenityFieldIndex.Name, true); }
			set	{ SetValue((int)AmenityFieldIndex.Name, value, true); }
		}

		/// <summary> The UpdatedBy property of the Entity Amenity<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Amenity"."UpdatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 UpdatedBy
		{
			get { return (System.Int32)GetValue((int)AmenityFieldIndex.UpdatedBy, true); }
			set	{ SetValue((int)AmenityFieldIndex.UpdatedBy, value, true); }
		}

		/// <summary> The CreatedBy property of the Entity Amenity<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Amenity"."CreatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CreatedBy
		{
			get { return (System.Int32)GetValue((int)AmenityFieldIndex.CreatedBy, true); }
			set	{ SetValue((int)AmenityFieldIndex.CreatedBy, value, true); }
		}

		/// <summary> The CreatedUTC property of the Entity Amenity<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Amenity"."CreatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CreatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)AmenityFieldIndex.CreatedUTC, false); }
			set	{ SetValue((int)AmenityFieldIndex.CreatedUTC, value, true); }
		}

		/// <summary> The UpdatedUTC property of the Entity Amenity<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Amenity"."UpdatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)AmenityFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)AmenityFieldIndex.UpdatedUTC, value, true); }
		}

		/// <summary> The LastModifiedUTC property of the Entity Amenity<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Amenity"."LastModifiedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime LastModifiedUTC
		{
			get { return (System.DateTime)GetValue((int)AmenityFieldIndex.LastModifiedUTC, true); }
			set	{ SetValue((int)AmenityFieldIndex.LastModifiedUTC, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'AmenityLanguageEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiAmenityLanguageCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.AmenityLanguageCollection AmenityLanguageCollection
		{
			get	{ return GetMultiAmenityLanguageCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for AmenityLanguageCollection. When set to true, AmenityLanguageCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time AmenityLanguageCollection is accessed. You can always execute/ a forced fetch by calling GetMultiAmenityLanguageCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAmenityLanguageCollection
		{
			get	{ return _alwaysFetchAmenityLanguageCollection; }
			set	{ _alwaysFetchAmenityLanguageCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property AmenityLanguageCollection already has been fetched. Setting this property to false when AmenityLanguageCollection has been fetched
		/// will clear the AmenityLanguageCollection collection well. Setting this property to true while AmenityLanguageCollection hasn't been fetched disables lazy loading for AmenityLanguageCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAmenityLanguageCollection
		{
			get { return _alreadyFetchedAmenityLanguageCollection;}
			set 
			{
				if(_alreadyFetchedAmenityLanguageCollection && !value && (_amenityLanguageCollection != null))
				{
					_amenityLanguageCollection.Clear();
				}
				_alreadyFetchedAmenityLanguageCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'CompanyAmenityEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCompanyAmenityCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.CompanyAmenityCollection CompanyAmenityCollection
		{
			get	{ return GetMultiCompanyAmenityCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CompanyAmenityCollection. When set to true, CompanyAmenityCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CompanyAmenityCollection is accessed. You can always execute/ a forced fetch by calling GetMultiCompanyAmenityCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCompanyAmenityCollection
		{
			get	{ return _alwaysFetchCompanyAmenityCollection; }
			set	{ _alwaysFetchCompanyAmenityCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property CompanyAmenityCollection already has been fetched. Setting this property to false when CompanyAmenityCollection has been fetched
		/// will clear the CompanyAmenityCollection collection well. Setting this property to true while CompanyAmenityCollection hasn't been fetched disables lazy loading for CompanyAmenityCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCompanyAmenityCollection
		{
			get { return _alreadyFetchedCompanyAmenityCollection;}
			set 
			{
				if(_alreadyFetchedCompanyAmenityCollection && !value && (_companyAmenityCollection != null))
				{
					_companyAmenityCollection.Clear();
				}
				_alreadyFetchedCompanyAmenityCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'CustomTextEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCustomTextCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.CustomTextCollection CustomTextCollection
		{
			get	{ return GetMultiCustomTextCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CustomTextCollection. When set to true, CustomTextCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CustomTextCollection is accessed. You can always execute/ a forced fetch by calling GetMultiCustomTextCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCustomTextCollection
		{
			get	{ return _alwaysFetchCustomTextCollection; }
			set	{ _alwaysFetchCustomTextCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property CustomTextCollection already has been fetched. Setting this property to false when CustomTextCollection has been fetched
		/// will clear the CustomTextCollection collection well. Setting this property to true while CustomTextCollection hasn't been fetched disables lazy loading for CustomTextCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCustomTextCollection
		{
			get { return _alreadyFetchedCustomTextCollection;}
			set 
			{
				if(_alreadyFetchedCustomTextCollection && !value && (_customTextCollection != null))
				{
					_customTextCollection.Clear();
				}
				_alreadyFetchedCustomTextCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'PointOfInterestAmenityEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiPointOfInterestAmenityCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.PointOfInterestAmenityCollection PointOfInterestAmenityCollection
		{
			get	{ return GetMultiPointOfInterestAmenityCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for PointOfInterestAmenityCollection. When set to true, PointOfInterestAmenityCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time PointOfInterestAmenityCollection is accessed. You can always execute/ a forced fetch by calling GetMultiPointOfInterestAmenityCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPointOfInterestAmenityCollection
		{
			get	{ return _alwaysFetchPointOfInterestAmenityCollection; }
			set	{ _alwaysFetchPointOfInterestAmenityCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property PointOfInterestAmenityCollection already has been fetched. Setting this property to false when PointOfInterestAmenityCollection has been fetched
		/// will clear the PointOfInterestAmenityCollection collection well. Setting this property to true while PointOfInterestAmenityCollection hasn't been fetched disables lazy loading for PointOfInterestAmenityCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPointOfInterestAmenityCollection
		{
			get { return _alreadyFetchedPointOfInterestAmenityCollection;}
			set 
			{
				if(_alreadyFetchedPointOfInterestAmenityCollection && !value && (_pointOfInterestAmenityCollection != null))
				{
					_pointOfInterestAmenityCollection.Clear();
				}
				_alreadyFetchedPointOfInterestAmenityCollection = value;
			}
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the Obymobi.Data.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)Obymobi.Data.EntityType.AmenityEntity; }
		}

		#endregion

		
		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
