﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
using System.Runtime.Serialization;
using System.Data;
using System.Xml.Serialization;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity base class which represents the base class for the entity 'EntertainmentConfigurationEntertainment'.<br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public abstract partial class EntertainmentConfigurationEntertainmentEntityBase : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		public new string LLBLGenProEntityName {
			get { return "EntertainmentConfigurationEntertainmentEntity"; }
		}
	
		#region Class Member Declarations
		private EntertainmentEntity _entertainmentEntity;
		private bool	_alwaysFetchEntertainmentEntity, _alreadyFetchedEntertainmentEntity, _entertainmentEntityReturnsNewIfNotFound;
		private EntertainmentConfigurationEntity _entertainmentConfigurationEntity;
		private bool	_alwaysFetchEntertainmentConfigurationEntity, _alreadyFetchedEntertainmentConfigurationEntity, _entertainmentConfigurationEntityReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name EntertainmentEntity</summary>
			public static readonly string EntertainmentEntity = "EntertainmentEntity";
			/// <summary>Member name EntertainmentConfigurationEntity</summary>
			public static readonly string EntertainmentConfigurationEntity = "EntertainmentConfigurationEntity";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static EntertainmentConfigurationEntertainmentEntityBase()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		protected EntertainmentConfigurationEntertainmentEntityBase() :base("EntertainmentConfigurationEntertainmentEntity")
		{
			InitClassEmpty(null);
		}

		/// <summary>CTor</summary>
		/// <param name="entertainmentConfigurationEntertainmentId">PK value for EntertainmentConfigurationEntertainment which data should be fetched into this EntertainmentConfigurationEntertainment object</param>
		protected EntertainmentConfigurationEntertainmentEntityBase(System.Int32 entertainmentConfigurationEntertainmentId):base("EntertainmentConfigurationEntertainmentEntity")
		{
			InitClassFetch(entertainmentConfigurationEntertainmentId, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="entertainmentConfigurationEntertainmentId">PK value for EntertainmentConfigurationEntertainment which data should be fetched into this EntertainmentConfigurationEntertainment object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		protected EntertainmentConfigurationEntertainmentEntityBase(System.Int32 entertainmentConfigurationEntertainmentId, IPrefetchPath prefetchPathToUse): base("EntertainmentConfigurationEntertainmentEntity")
		{
			InitClassFetch(entertainmentConfigurationEntertainmentId, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="entertainmentConfigurationEntertainmentId">PK value for EntertainmentConfigurationEntertainment which data should be fetched into this EntertainmentConfigurationEntertainment object</param>
		/// <param name="validator">The custom validator object for this EntertainmentConfigurationEntertainmentEntity</param>
		protected EntertainmentConfigurationEntertainmentEntityBase(System.Int32 entertainmentConfigurationEntertainmentId, IValidator validator):base("EntertainmentConfigurationEntertainmentEntity")
		{
			InitClassFetch(entertainmentConfigurationEntertainmentId, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected EntertainmentConfigurationEntertainmentEntityBase(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_entertainmentEntity = (EntertainmentEntity)info.GetValue("_entertainmentEntity", typeof(EntertainmentEntity));
			if(_entertainmentEntity!=null)
			{
				_entertainmentEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_entertainmentEntityReturnsNewIfNotFound = info.GetBoolean("_entertainmentEntityReturnsNewIfNotFound");
			_alwaysFetchEntertainmentEntity = info.GetBoolean("_alwaysFetchEntertainmentEntity");
			_alreadyFetchedEntertainmentEntity = info.GetBoolean("_alreadyFetchedEntertainmentEntity");

			_entertainmentConfigurationEntity = (EntertainmentConfigurationEntity)info.GetValue("_entertainmentConfigurationEntity", typeof(EntertainmentConfigurationEntity));
			if(_entertainmentConfigurationEntity!=null)
			{
				_entertainmentConfigurationEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_entertainmentConfigurationEntityReturnsNewIfNotFound = info.GetBoolean("_entertainmentConfigurationEntityReturnsNewIfNotFound");
			_alwaysFetchEntertainmentConfigurationEntity = info.GetBoolean("_alwaysFetchEntertainmentConfigurationEntity");
			_alreadyFetchedEntertainmentConfigurationEntity = info.GetBoolean("_alreadyFetchedEntertainmentConfigurationEntity");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}	
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((EntertainmentConfigurationEntertainmentFieldIndex)fieldIndex)
			{
				case EntertainmentConfigurationEntertainmentFieldIndex.EntertainmentConfigurationId:
					DesetupSyncEntertainmentConfigurationEntity(true, false);
					_alreadyFetchedEntertainmentConfigurationEntity = false;
					break;
				case EntertainmentConfigurationEntertainmentFieldIndex.EntertainmentId:
					DesetupSyncEntertainmentEntity(true, false);
					_alreadyFetchedEntertainmentEntity = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedEntertainmentEntity = (_entertainmentEntity != null);
			_alreadyFetchedEntertainmentConfigurationEntity = (_entertainmentConfigurationEntity != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "EntertainmentEntity":
					toReturn.Add(Relations.EntertainmentEntityUsingEntertainmentId);
					break;
				case "EntertainmentConfigurationEntity":
					toReturn.Add(Relations.EntertainmentConfigurationEntityUsingEntertainmentConfigurationId);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_entertainmentEntity", (!this.MarkedForDeletion?_entertainmentEntity:null));
			info.AddValue("_entertainmentEntityReturnsNewIfNotFound", _entertainmentEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchEntertainmentEntity", _alwaysFetchEntertainmentEntity);
			info.AddValue("_alreadyFetchedEntertainmentEntity", _alreadyFetchedEntertainmentEntity);
			info.AddValue("_entertainmentConfigurationEntity", (!this.MarkedForDeletion?_entertainmentConfigurationEntity:null));
			info.AddValue("_entertainmentConfigurationEntityReturnsNewIfNotFound", _entertainmentConfigurationEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchEntertainmentConfigurationEntity", _alwaysFetchEntertainmentConfigurationEntity);
			info.AddValue("_alreadyFetchedEntertainmentConfigurationEntity", _alreadyFetchedEntertainmentConfigurationEntity);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "EntertainmentEntity":
					_alreadyFetchedEntertainmentEntity = true;
					this.EntertainmentEntity = (EntertainmentEntity)entity;
					break;
				case "EntertainmentConfigurationEntity":
					_alreadyFetchedEntertainmentConfigurationEntity = true;
					this.EntertainmentConfigurationEntity = (EntertainmentConfigurationEntity)entity;
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "EntertainmentEntity":
					SetupSyncEntertainmentEntity(relatedEntity);
					break;
				case "EntertainmentConfigurationEntity":
					SetupSyncEntertainmentConfigurationEntity(relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "EntertainmentEntity":
					DesetupSyncEntertainmentEntity(false, true);
					break;
				case "EntertainmentConfigurationEntity":
					DesetupSyncEntertainmentConfigurationEntity(false, true);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_entertainmentEntity!=null)
			{
				toReturn.Add(_entertainmentEntity);
			}
			if(_entertainmentConfigurationEntity!=null)
			{
				toReturn.Add(_entertainmentConfigurationEntity);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();


			return toReturn;
		}

		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="entertainmentConfigurationId">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="entertainmentId">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCEntertainmentConfigurationIdEntertainmentId(System.Int32 entertainmentConfigurationId, System.Int32 entertainmentId)
		{
			return FetchUsingUCEntertainmentConfigurationIdEntertainmentId( entertainmentConfigurationId,  entertainmentId, null, null, null);
		}

		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="entertainmentConfigurationId">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="entertainmentId">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCEntertainmentConfigurationIdEntertainmentId(System.Int32 entertainmentConfigurationId, System.Int32 entertainmentId, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingUCEntertainmentConfigurationIdEntertainmentId( entertainmentConfigurationId,  entertainmentId, prefetchPathToUse, null, null);
		}
	
		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="entertainmentConfigurationId">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="entertainmentId">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCEntertainmentConfigurationIdEntertainmentId(System.Int32 entertainmentConfigurationId, System.Int32 entertainmentId, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingUCEntertainmentConfigurationIdEntertainmentId( entertainmentConfigurationId,  entertainmentId, prefetchPathToUse, contextToUse, null);
		}
	
		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="entertainmentConfigurationId">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="entertainmentId">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCEntertainmentConfigurationIdEntertainmentId(System.Int32 entertainmentConfigurationId, System.Int32 entertainmentId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				((EntertainmentConfigurationEntertainmentDAO)CreateDAOInstance()).FetchEntertainmentConfigurationEntertainmentUsingUCEntertainmentConfigurationIdEntertainmentId(this, this.Transaction, entertainmentConfigurationId, entertainmentId, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="entertainmentConfigurationEntertainmentId">PK value for EntertainmentConfigurationEntertainment which data should be fetched into this EntertainmentConfigurationEntertainment object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 entertainmentConfigurationEntertainmentId)
		{
			return FetchUsingPK(entertainmentConfigurationEntertainmentId, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="entertainmentConfigurationEntertainmentId">PK value for EntertainmentConfigurationEntertainment which data should be fetched into this EntertainmentConfigurationEntertainment object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 entertainmentConfigurationEntertainmentId, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(entertainmentConfigurationEntertainmentId, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="entertainmentConfigurationEntertainmentId">PK value for EntertainmentConfigurationEntertainment which data should be fetched into this EntertainmentConfigurationEntertainment object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 entertainmentConfigurationEntertainmentId, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(entertainmentConfigurationEntertainmentId, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="entertainmentConfigurationEntertainmentId">PK value for EntertainmentConfigurationEntertainment which data should be fetched into this EntertainmentConfigurationEntertainment object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 entertainmentConfigurationEntertainmentId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(entertainmentConfigurationEntertainmentId, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.EntertainmentConfigurationEntertainmentId, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new EntertainmentConfigurationEntertainmentRelations().GetAllRelations();
		}

		/// <summary> Retrieves the related entity of type 'EntertainmentEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'EntertainmentEntity' which is related to this entity.</returns>
		public EntertainmentEntity GetSingleEntertainmentEntity()
		{
			return GetSingleEntertainmentEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'EntertainmentEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'EntertainmentEntity' which is related to this entity.</returns>
		public virtual EntertainmentEntity GetSingleEntertainmentEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedEntertainmentEntity || forceFetch || _alwaysFetchEntertainmentEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.EntertainmentEntityUsingEntertainmentId);
				EntertainmentEntity newEntity = new EntertainmentEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.EntertainmentId);
				}
				if(fetchResult)
				{
					newEntity = (EntertainmentEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_entertainmentEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.EntertainmentEntity = newEntity;
				_alreadyFetchedEntertainmentEntity = fetchResult;
			}
			return _entertainmentEntity;
		}


		/// <summary> Retrieves the related entity of type 'EntertainmentConfigurationEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'EntertainmentConfigurationEntity' which is related to this entity.</returns>
		public EntertainmentConfigurationEntity GetSingleEntertainmentConfigurationEntity()
		{
			return GetSingleEntertainmentConfigurationEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'EntertainmentConfigurationEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'EntertainmentConfigurationEntity' which is related to this entity.</returns>
		public virtual EntertainmentConfigurationEntity GetSingleEntertainmentConfigurationEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedEntertainmentConfigurationEntity || forceFetch || _alwaysFetchEntertainmentConfigurationEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.EntertainmentConfigurationEntityUsingEntertainmentConfigurationId);
				EntertainmentConfigurationEntity newEntity = new EntertainmentConfigurationEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.EntertainmentConfigurationId);
				}
				if(fetchResult)
				{
					newEntity = (EntertainmentConfigurationEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_entertainmentConfigurationEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.EntertainmentConfigurationEntity = newEntity;
				_alreadyFetchedEntertainmentConfigurationEntity = fetchResult;
			}
			return _entertainmentConfigurationEntity;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("EntertainmentEntity", _entertainmentEntity);
			toReturn.Add("EntertainmentConfigurationEntity", _entertainmentConfigurationEntity);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="entertainmentConfigurationEntertainmentId">PK value for EntertainmentConfigurationEntertainment which data should be fetched into this EntertainmentConfigurationEntertainment object</param>
		/// <param name="validator">The validator object for this EntertainmentConfigurationEntertainmentEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int32 entertainmentConfigurationEntertainmentId, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(entertainmentConfigurationEntertainmentId, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{
			_entertainmentEntityReturnsNewIfNotFound = true;
			_entertainmentConfigurationEntityReturnsNewIfNotFound = true;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("EntertainmentConfigurationEntertainmentId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ParentCompanyId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("EntertainmentConfigurationId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("EntertainmentId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SortOrder", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _entertainmentEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncEntertainmentEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _entertainmentEntity, new PropertyChangedEventHandler( OnEntertainmentEntityPropertyChanged ), "EntertainmentEntity", Obymobi.Data.RelationClasses.StaticEntertainmentConfigurationEntertainmentRelations.EntertainmentEntityUsingEntertainmentIdStatic, true, signalRelatedEntity, "EntertainmentConfigurationEntertainmentCollection", resetFKFields, new int[] { (int)EntertainmentConfigurationEntertainmentFieldIndex.EntertainmentId } );		
			_entertainmentEntity = null;
		}
		
		/// <summary> setups the sync logic for member _entertainmentEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncEntertainmentEntity(IEntityCore relatedEntity)
		{
			if(_entertainmentEntity!=relatedEntity)
			{		
				DesetupSyncEntertainmentEntity(true, true);
				_entertainmentEntity = (EntertainmentEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _entertainmentEntity, new PropertyChangedEventHandler( OnEntertainmentEntityPropertyChanged ), "EntertainmentEntity", Obymobi.Data.RelationClasses.StaticEntertainmentConfigurationEntertainmentRelations.EntertainmentEntityUsingEntertainmentIdStatic, true, ref _alreadyFetchedEntertainmentEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnEntertainmentEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _entertainmentConfigurationEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncEntertainmentConfigurationEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _entertainmentConfigurationEntity, new PropertyChangedEventHandler( OnEntertainmentConfigurationEntityPropertyChanged ), "EntertainmentConfigurationEntity", Obymobi.Data.RelationClasses.StaticEntertainmentConfigurationEntertainmentRelations.EntertainmentConfigurationEntityUsingEntertainmentConfigurationIdStatic, true, signalRelatedEntity, "EntertainmentConfigurationEntertainmentCollection", resetFKFields, new int[] { (int)EntertainmentConfigurationEntertainmentFieldIndex.EntertainmentConfigurationId } );		
			_entertainmentConfigurationEntity = null;
		}
		
		/// <summary> setups the sync logic for member _entertainmentConfigurationEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncEntertainmentConfigurationEntity(IEntityCore relatedEntity)
		{
			if(_entertainmentConfigurationEntity!=relatedEntity)
			{		
				DesetupSyncEntertainmentConfigurationEntity(true, true);
				_entertainmentConfigurationEntity = (EntertainmentConfigurationEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _entertainmentConfigurationEntity, new PropertyChangedEventHandler( OnEntertainmentConfigurationEntityPropertyChanged ), "EntertainmentConfigurationEntity", Obymobi.Data.RelationClasses.StaticEntertainmentConfigurationEntertainmentRelations.EntertainmentConfigurationEntityUsingEntertainmentConfigurationIdStatic, true, ref _alreadyFetchedEntertainmentConfigurationEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnEntertainmentConfigurationEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="entertainmentConfigurationEntertainmentId">PK value for EntertainmentConfigurationEntertainment which data should be fetched into this EntertainmentConfigurationEntertainment object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int32 entertainmentConfigurationEntertainmentId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)EntertainmentConfigurationEntertainmentFieldIndex.EntertainmentConfigurationEntertainmentId].ForcedCurrentValueWrite(entertainmentConfigurationEntertainmentId);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateEntertainmentConfigurationEntertainmentDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new EntertainmentConfigurationEntertainmentEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static EntertainmentConfigurationEntertainmentRelations Relations
		{
			get	{ return new EntertainmentConfigurationEntertainmentRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Entertainment'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathEntertainmentEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.EntertainmentCollection(), (IEntityRelation)GetRelationsForField("EntertainmentEntity")[0], (int)Obymobi.Data.EntityType.EntertainmentConfigurationEntertainmentEntity, (int)Obymobi.Data.EntityType.EntertainmentEntity, 0, null, null, null, "EntertainmentEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'EntertainmentConfiguration'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathEntertainmentConfigurationEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.EntertainmentConfigurationCollection(), (IEntityRelation)GetRelationsForField("EntertainmentConfigurationEntity")[0], (int)Obymobi.Data.EntityType.EntertainmentConfigurationEntertainmentEntity, (int)Obymobi.Data.EntityType.EntertainmentConfigurationEntity, 0, null, null, null, "EntertainmentConfigurationEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The EntertainmentConfigurationEntertainmentId property of the Entity EntertainmentConfigurationEntertainment<br/><br/></summary>
		/// <remarks>Mapped on  table field: "EntertainmentConfigurationEntertainment"."EntertainmentConfigurationEntertainmentId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 EntertainmentConfigurationEntertainmentId
		{
			get { return (System.Int32)GetValue((int)EntertainmentConfigurationEntertainmentFieldIndex.EntertainmentConfigurationEntertainmentId, true); }
			set	{ SetValue((int)EntertainmentConfigurationEntertainmentFieldIndex.EntertainmentConfigurationEntertainmentId, value, true); }
		}

		/// <summary> The ParentCompanyId property of the Entity EntertainmentConfigurationEntertainment<br/><br/></summary>
		/// <remarks>Mapped on  table field: "EntertainmentConfigurationEntertainment"."ParentCompanyId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 ParentCompanyId
		{
			get { return (System.Int32)GetValue((int)EntertainmentConfigurationEntertainmentFieldIndex.ParentCompanyId, true); }
			set	{ SetValue((int)EntertainmentConfigurationEntertainmentFieldIndex.ParentCompanyId, value, true); }
		}

		/// <summary> The EntertainmentConfigurationId property of the Entity EntertainmentConfigurationEntertainment<br/><br/></summary>
		/// <remarks>Mapped on  table field: "EntertainmentConfigurationEntertainment"."EntertainmentConfigurationId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 EntertainmentConfigurationId
		{
			get { return (System.Int32)GetValue((int)EntertainmentConfigurationEntertainmentFieldIndex.EntertainmentConfigurationId, true); }
			set	{ SetValue((int)EntertainmentConfigurationEntertainmentFieldIndex.EntertainmentConfigurationId, value, true); }
		}

		/// <summary> The EntertainmentId property of the Entity EntertainmentConfigurationEntertainment<br/><br/></summary>
		/// <remarks>Mapped on  table field: "EntertainmentConfigurationEntertainment"."EntertainmentId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 EntertainmentId
		{
			get { return (System.Int32)GetValue((int)EntertainmentConfigurationEntertainmentFieldIndex.EntertainmentId, true); }
			set	{ SetValue((int)EntertainmentConfigurationEntertainmentFieldIndex.EntertainmentId, value, true); }
		}

		/// <summary> The CreatedUTC property of the Entity EntertainmentConfigurationEntertainment<br/><br/></summary>
		/// <remarks>Mapped on  table field: "EntertainmentConfigurationEntertainment"."CreatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CreatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)EntertainmentConfigurationEntertainmentFieldIndex.CreatedUTC, false); }
			set	{ SetValue((int)EntertainmentConfigurationEntertainmentFieldIndex.CreatedUTC, value, true); }
		}

		/// <summary> The CreatedBy property of the Entity EntertainmentConfigurationEntertainment<br/><br/></summary>
		/// <remarks>Mapped on  table field: "EntertainmentConfigurationEntertainment"."CreatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> CreatedBy
		{
			get { return (Nullable<System.Int32>)GetValue((int)EntertainmentConfigurationEntertainmentFieldIndex.CreatedBy, false); }
			set	{ SetValue((int)EntertainmentConfigurationEntertainmentFieldIndex.CreatedBy, value, true); }
		}

		/// <summary> The UpdatedUTC property of the Entity EntertainmentConfigurationEntertainment<br/><br/></summary>
		/// <remarks>Mapped on  table field: "EntertainmentConfigurationEntertainment"."UpdatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)EntertainmentConfigurationEntertainmentFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)EntertainmentConfigurationEntertainmentFieldIndex.UpdatedUTC, value, true); }
		}

		/// <summary> The UpdatedBy property of the Entity EntertainmentConfigurationEntertainment<br/><br/></summary>
		/// <remarks>Mapped on  table field: "EntertainmentConfigurationEntertainment"."UpdatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> UpdatedBy
		{
			get { return (Nullable<System.Int32>)GetValue((int)EntertainmentConfigurationEntertainmentFieldIndex.UpdatedBy, false); }
			set	{ SetValue((int)EntertainmentConfigurationEntertainmentFieldIndex.UpdatedBy, value, true); }
		}

		/// <summary> The SortOrder property of the Entity EntertainmentConfigurationEntertainment<br/><br/></summary>
		/// <remarks>Mapped on  table field: "EntertainmentConfigurationEntertainment"."SortOrder"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 SortOrder
		{
			get { return (System.Int32)GetValue((int)EntertainmentConfigurationEntertainmentFieldIndex.SortOrder, true); }
			set	{ SetValue((int)EntertainmentConfigurationEntertainmentFieldIndex.SortOrder, value, true); }
		}


		/// <summary> Gets / sets related entity of type 'EntertainmentEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleEntertainmentEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual EntertainmentEntity EntertainmentEntity
		{
			get	{ return GetSingleEntertainmentEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncEntertainmentEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "EntertainmentConfigurationEntertainmentCollection", "EntertainmentEntity", _entertainmentEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for EntertainmentEntity. When set to true, EntertainmentEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time EntertainmentEntity is accessed. You can always execute a forced fetch by calling GetSingleEntertainmentEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchEntertainmentEntity
		{
			get	{ return _alwaysFetchEntertainmentEntity; }
			set	{ _alwaysFetchEntertainmentEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property EntertainmentEntity already has been fetched. Setting this property to false when EntertainmentEntity has been fetched
		/// will set EntertainmentEntity to null as well. Setting this property to true while EntertainmentEntity hasn't been fetched disables lazy loading for EntertainmentEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedEntertainmentEntity
		{
			get { return _alreadyFetchedEntertainmentEntity;}
			set 
			{
				if(_alreadyFetchedEntertainmentEntity && !value)
				{
					this.EntertainmentEntity = null;
				}
				_alreadyFetchedEntertainmentEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property EntertainmentEntity is not found
		/// in the database. When set to true, EntertainmentEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool EntertainmentEntityReturnsNewIfNotFound
		{
			get	{ return _entertainmentEntityReturnsNewIfNotFound; }
			set { _entertainmentEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'EntertainmentConfigurationEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleEntertainmentConfigurationEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual EntertainmentConfigurationEntity EntertainmentConfigurationEntity
		{
			get	{ return GetSingleEntertainmentConfigurationEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncEntertainmentConfigurationEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "EntertainmentConfigurationEntertainmentCollection", "EntertainmentConfigurationEntity", _entertainmentConfigurationEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for EntertainmentConfigurationEntity. When set to true, EntertainmentConfigurationEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time EntertainmentConfigurationEntity is accessed. You can always execute a forced fetch by calling GetSingleEntertainmentConfigurationEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchEntertainmentConfigurationEntity
		{
			get	{ return _alwaysFetchEntertainmentConfigurationEntity; }
			set	{ _alwaysFetchEntertainmentConfigurationEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property EntertainmentConfigurationEntity already has been fetched. Setting this property to false when EntertainmentConfigurationEntity has been fetched
		/// will set EntertainmentConfigurationEntity to null as well. Setting this property to true while EntertainmentConfigurationEntity hasn't been fetched disables lazy loading for EntertainmentConfigurationEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedEntertainmentConfigurationEntity
		{
			get { return _alreadyFetchedEntertainmentConfigurationEntity;}
			set 
			{
				if(_alreadyFetchedEntertainmentConfigurationEntity && !value)
				{
					this.EntertainmentConfigurationEntity = null;
				}
				_alreadyFetchedEntertainmentConfigurationEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property EntertainmentConfigurationEntity is not found
		/// in the database. When set to true, EntertainmentConfigurationEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool EntertainmentConfigurationEntityReturnsNewIfNotFound
		{
			get	{ return _entertainmentConfigurationEntityReturnsNewIfNotFound; }
			set { _entertainmentConfigurationEntityReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the Obymobi.Data.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)Obymobi.Data.EntityType.EntertainmentConfigurationEntertainmentEntity; }
		}

		#endregion

		
		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
