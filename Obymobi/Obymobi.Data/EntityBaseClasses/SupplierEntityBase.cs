﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
using System.Runtime.Serialization;
using System.Data;
using System.Xml.Serialization;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity base class which represents the base class for the entity 'Supplier'.<br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public abstract partial class SupplierEntityBase : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		public new string LLBLGenProEntityName {
			get { return "SupplierEntity"; }
		}
	
		#region Class Member Declarations
		private Obymobi.Data.CollectionClasses.AdvertisementCollection	_advertisementCollection;
		private bool	_alwaysFetchAdvertisementCollection, _alreadyFetchedAdvertisementCollection;
		private Obymobi.Data.CollectionClasses.GenericproductCollection	_genericproductCollection;
		private bool	_alwaysFetchGenericproductCollection, _alreadyFetchedGenericproductCollection;
		private Obymobi.Data.CollectionClasses.CategoryCollection _categoryCollectionViaAdvertisement;
		private bool	_alwaysFetchCategoryCollectionViaAdvertisement, _alreadyFetchedCategoryCollectionViaAdvertisement;
		private Obymobi.Data.CollectionClasses.DeliverypointgroupCollection _deliverypointgroupCollectionViaAdvertisement;
		private bool	_alwaysFetchDeliverypointgroupCollectionViaAdvertisement, _alreadyFetchedDeliverypointgroupCollectionViaAdvertisement;
		private Obymobi.Data.CollectionClasses.EntertainmentCollection _entertainmentCollectionViaAdvertisement;
		private bool	_alwaysFetchEntertainmentCollectionViaAdvertisement, _alreadyFetchedEntertainmentCollectionViaAdvertisement;
		private Obymobi.Data.CollectionClasses.EntertainmentcategoryCollection _entertainmentcategoryCollectionViaAdvertisement;
		private bool	_alwaysFetchEntertainmentcategoryCollectionViaAdvertisement, _alreadyFetchedEntertainmentcategoryCollectionViaAdvertisement;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name AdvertisementCollection</summary>
			public static readonly string AdvertisementCollection = "AdvertisementCollection";
			/// <summary>Member name GenericproductCollection</summary>
			public static readonly string GenericproductCollection = "GenericproductCollection";
			/// <summary>Member name CategoryCollectionViaAdvertisement</summary>
			public static readonly string CategoryCollectionViaAdvertisement = "CategoryCollectionViaAdvertisement";
			/// <summary>Member name DeliverypointgroupCollectionViaAdvertisement</summary>
			public static readonly string DeliverypointgroupCollectionViaAdvertisement = "DeliverypointgroupCollectionViaAdvertisement";
			/// <summary>Member name EntertainmentCollectionViaAdvertisement</summary>
			public static readonly string EntertainmentCollectionViaAdvertisement = "EntertainmentCollectionViaAdvertisement";
			/// <summary>Member name EntertainmentcategoryCollectionViaAdvertisement</summary>
			public static readonly string EntertainmentcategoryCollectionViaAdvertisement = "EntertainmentcategoryCollectionViaAdvertisement";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static SupplierEntityBase()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		protected SupplierEntityBase() :base("SupplierEntity")
		{
			InitClassEmpty(null);
		}

		/// <summary>CTor</summary>
		/// <param name="supplierId">PK value for Supplier which data should be fetched into this Supplier object</param>
		protected SupplierEntityBase(System.Int32 supplierId):base("SupplierEntity")
		{
			InitClassFetch(supplierId, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="supplierId">PK value for Supplier which data should be fetched into this Supplier object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		protected SupplierEntityBase(System.Int32 supplierId, IPrefetchPath prefetchPathToUse): base("SupplierEntity")
		{
			InitClassFetch(supplierId, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="supplierId">PK value for Supplier which data should be fetched into this Supplier object</param>
		/// <param name="validator">The custom validator object for this SupplierEntity</param>
		protected SupplierEntityBase(System.Int32 supplierId, IValidator validator):base("SupplierEntity")
		{
			InitClassFetch(supplierId, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected SupplierEntityBase(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_advertisementCollection = (Obymobi.Data.CollectionClasses.AdvertisementCollection)info.GetValue("_advertisementCollection", typeof(Obymobi.Data.CollectionClasses.AdvertisementCollection));
			_alwaysFetchAdvertisementCollection = info.GetBoolean("_alwaysFetchAdvertisementCollection");
			_alreadyFetchedAdvertisementCollection = info.GetBoolean("_alreadyFetchedAdvertisementCollection");

			_genericproductCollection = (Obymobi.Data.CollectionClasses.GenericproductCollection)info.GetValue("_genericproductCollection", typeof(Obymobi.Data.CollectionClasses.GenericproductCollection));
			_alwaysFetchGenericproductCollection = info.GetBoolean("_alwaysFetchGenericproductCollection");
			_alreadyFetchedGenericproductCollection = info.GetBoolean("_alreadyFetchedGenericproductCollection");
			_categoryCollectionViaAdvertisement = (Obymobi.Data.CollectionClasses.CategoryCollection)info.GetValue("_categoryCollectionViaAdvertisement", typeof(Obymobi.Data.CollectionClasses.CategoryCollection));
			_alwaysFetchCategoryCollectionViaAdvertisement = info.GetBoolean("_alwaysFetchCategoryCollectionViaAdvertisement");
			_alreadyFetchedCategoryCollectionViaAdvertisement = info.GetBoolean("_alreadyFetchedCategoryCollectionViaAdvertisement");

			_deliverypointgroupCollectionViaAdvertisement = (Obymobi.Data.CollectionClasses.DeliverypointgroupCollection)info.GetValue("_deliverypointgroupCollectionViaAdvertisement", typeof(Obymobi.Data.CollectionClasses.DeliverypointgroupCollection));
			_alwaysFetchDeliverypointgroupCollectionViaAdvertisement = info.GetBoolean("_alwaysFetchDeliverypointgroupCollectionViaAdvertisement");
			_alreadyFetchedDeliverypointgroupCollectionViaAdvertisement = info.GetBoolean("_alreadyFetchedDeliverypointgroupCollectionViaAdvertisement");

			_entertainmentCollectionViaAdvertisement = (Obymobi.Data.CollectionClasses.EntertainmentCollection)info.GetValue("_entertainmentCollectionViaAdvertisement", typeof(Obymobi.Data.CollectionClasses.EntertainmentCollection));
			_alwaysFetchEntertainmentCollectionViaAdvertisement = info.GetBoolean("_alwaysFetchEntertainmentCollectionViaAdvertisement");
			_alreadyFetchedEntertainmentCollectionViaAdvertisement = info.GetBoolean("_alreadyFetchedEntertainmentCollectionViaAdvertisement");

			_entertainmentcategoryCollectionViaAdvertisement = (Obymobi.Data.CollectionClasses.EntertainmentcategoryCollection)info.GetValue("_entertainmentcategoryCollectionViaAdvertisement", typeof(Obymobi.Data.CollectionClasses.EntertainmentcategoryCollection));
			_alwaysFetchEntertainmentcategoryCollectionViaAdvertisement = info.GetBoolean("_alwaysFetchEntertainmentcategoryCollectionViaAdvertisement");
			_alreadyFetchedEntertainmentcategoryCollectionViaAdvertisement = info.GetBoolean("_alreadyFetchedEntertainmentcategoryCollectionViaAdvertisement");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}	

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedAdvertisementCollection = (_advertisementCollection.Count > 0);
			_alreadyFetchedGenericproductCollection = (_genericproductCollection.Count > 0);
			_alreadyFetchedCategoryCollectionViaAdvertisement = (_categoryCollectionViaAdvertisement.Count > 0);
			_alreadyFetchedDeliverypointgroupCollectionViaAdvertisement = (_deliverypointgroupCollectionViaAdvertisement.Count > 0);
			_alreadyFetchedEntertainmentCollectionViaAdvertisement = (_entertainmentCollectionViaAdvertisement.Count > 0);
			_alreadyFetchedEntertainmentcategoryCollectionViaAdvertisement = (_entertainmentcategoryCollectionViaAdvertisement.Count > 0);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "AdvertisementCollection":
					toReturn.Add(Relations.AdvertisementEntityUsingSupplierId);
					break;
				case "GenericproductCollection":
					toReturn.Add(Relations.GenericproductEntityUsingSupplierId);
					break;
				case "CategoryCollectionViaAdvertisement":
					toReturn.Add(Relations.AdvertisementEntityUsingSupplierId, "SupplierEntity__", "Advertisement_", JoinHint.None);
					toReturn.Add(AdvertisementEntity.Relations.CategoryEntityUsingActionCategoryId, "Advertisement_", string.Empty, JoinHint.None);
					break;
				case "DeliverypointgroupCollectionViaAdvertisement":
					toReturn.Add(Relations.AdvertisementEntityUsingSupplierId, "SupplierEntity__", "Advertisement_", JoinHint.None);
					toReturn.Add(AdvertisementEntity.Relations.DeliverypointgroupEntityUsingDeliverypointgroupId, "Advertisement_", string.Empty, JoinHint.None);
					break;
				case "EntertainmentCollectionViaAdvertisement":
					toReturn.Add(Relations.AdvertisementEntityUsingSupplierId, "SupplierEntity__", "Advertisement_", JoinHint.None);
					toReturn.Add(AdvertisementEntity.Relations.EntertainmentEntityUsingActionEntertainmentId, "Advertisement_", string.Empty, JoinHint.None);
					break;
				case "EntertainmentcategoryCollectionViaAdvertisement":
					toReturn.Add(Relations.AdvertisementEntityUsingSupplierId, "SupplierEntity__", "Advertisement_", JoinHint.None);
					toReturn.Add(AdvertisementEntity.Relations.EntertainmentcategoryEntityUsingActionEntertainmentCategoryId, "Advertisement_", string.Empty, JoinHint.None);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_advertisementCollection", (!this.MarkedForDeletion?_advertisementCollection:null));
			info.AddValue("_alwaysFetchAdvertisementCollection", _alwaysFetchAdvertisementCollection);
			info.AddValue("_alreadyFetchedAdvertisementCollection", _alreadyFetchedAdvertisementCollection);
			info.AddValue("_genericproductCollection", (!this.MarkedForDeletion?_genericproductCollection:null));
			info.AddValue("_alwaysFetchGenericproductCollection", _alwaysFetchGenericproductCollection);
			info.AddValue("_alreadyFetchedGenericproductCollection", _alreadyFetchedGenericproductCollection);
			info.AddValue("_categoryCollectionViaAdvertisement", (!this.MarkedForDeletion?_categoryCollectionViaAdvertisement:null));
			info.AddValue("_alwaysFetchCategoryCollectionViaAdvertisement", _alwaysFetchCategoryCollectionViaAdvertisement);
			info.AddValue("_alreadyFetchedCategoryCollectionViaAdvertisement", _alreadyFetchedCategoryCollectionViaAdvertisement);
			info.AddValue("_deliverypointgroupCollectionViaAdvertisement", (!this.MarkedForDeletion?_deliverypointgroupCollectionViaAdvertisement:null));
			info.AddValue("_alwaysFetchDeliverypointgroupCollectionViaAdvertisement", _alwaysFetchDeliverypointgroupCollectionViaAdvertisement);
			info.AddValue("_alreadyFetchedDeliverypointgroupCollectionViaAdvertisement", _alreadyFetchedDeliverypointgroupCollectionViaAdvertisement);
			info.AddValue("_entertainmentCollectionViaAdvertisement", (!this.MarkedForDeletion?_entertainmentCollectionViaAdvertisement:null));
			info.AddValue("_alwaysFetchEntertainmentCollectionViaAdvertisement", _alwaysFetchEntertainmentCollectionViaAdvertisement);
			info.AddValue("_alreadyFetchedEntertainmentCollectionViaAdvertisement", _alreadyFetchedEntertainmentCollectionViaAdvertisement);
			info.AddValue("_entertainmentcategoryCollectionViaAdvertisement", (!this.MarkedForDeletion?_entertainmentcategoryCollectionViaAdvertisement:null));
			info.AddValue("_alwaysFetchEntertainmentcategoryCollectionViaAdvertisement", _alwaysFetchEntertainmentcategoryCollectionViaAdvertisement);
			info.AddValue("_alreadyFetchedEntertainmentcategoryCollectionViaAdvertisement", _alreadyFetchedEntertainmentcategoryCollectionViaAdvertisement);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "AdvertisementCollection":
					_alreadyFetchedAdvertisementCollection = true;
					if(entity!=null)
					{
						this.AdvertisementCollection.Add((AdvertisementEntity)entity);
					}
					break;
				case "GenericproductCollection":
					_alreadyFetchedGenericproductCollection = true;
					if(entity!=null)
					{
						this.GenericproductCollection.Add((GenericproductEntity)entity);
					}
					break;
				case "CategoryCollectionViaAdvertisement":
					_alreadyFetchedCategoryCollectionViaAdvertisement = true;
					if(entity!=null)
					{
						this.CategoryCollectionViaAdvertisement.Add((CategoryEntity)entity);
					}
					break;
				case "DeliverypointgroupCollectionViaAdvertisement":
					_alreadyFetchedDeliverypointgroupCollectionViaAdvertisement = true;
					if(entity!=null)
					{
						this.DeliverypointgroupCollectionViaAdvertisement.Add((DeliverypointgroupEntity)entity);
					}
					break;
				case "EntertainmentCollectionViaAdvertisement":
					_alreadyFetchedEntertainmentCollectionViaAdvertisement = true;
					if(entity!=null)
					{
						this.EntertainmentCollectionViaAdvertisement.Add((EntertainmentEntity)entity);
					}
					break;
				case "EntertainmentcategoryCollectionViaAdvertisement":
					_alreadyFetchedEntertainmentcategoryCollectionViaAdvertisement = true;
					if(entity!=null)
					{
						this.EntertainmentcategoryCollectionViaAdvertisement.Add((EntertainmentcategoryEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "AdvertisementCollection":
					_advertisementCollection.Add((AdvertisementEntity)relatedEntity);
					break;
				case "GenericproductCollection":
					_genericproductCollection.Add((GenericproductEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "AdvertisementCollection":
					this.PerformRelatedEntityRemoval(_advertisementCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "GenericproductCollection":
					this.PerformRelatedEntityRemoval(_genericproductCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_advertisementCollection);
			toReturn.Add(_genericproductCollection);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="supplierId">PK value for Supplier which data should be fetched into this Supplier object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 supplierId)
		{
			return FetchUsingPK(supplierId, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="supplierId">PK value for Supplier which data should be fetched into this Supplier object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 supplierId, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(supplierId, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="supplierId">PK value for Supplier which data should be fetched into this Supplier object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 supplierId, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(supplierId, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="supplierId">PK value for Supplier which data should be fetched into this Supplier object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 supplierId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(supplierId, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.SupplierId, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new SupplierRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'AdvertisementEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'AdvertisementEntity'</returns>
		public Obymobi.Data.CollectionClasses.AdvertisementCollection GetMultiAdvertisementCollection(bool forceFetch)
		{
			return GetMultiAdvertisementCollection(forceFetch, _advertisementCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'AdvertisementEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'AdvertisementEntity'</returns>
		public Obymobi.Data.CollectionClasses.AdvertisementCollection GetMultiAdvertisementCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiAdvertisementCollection(forceFetch, _advertisementCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'AdvertisementEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.AdvertisementCollection GetMultiAdvertisementCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiAdvertisementCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'AdvertisementEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.AdvertisementCollection GetMultiAdvertisementCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedAdvertisementCollection || forceFetch || _alwaysFetchAdvertisementCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_advertisementCollection);
				_advertisementCollection.SuppressClearInGetMulti=!forceFetch;
				_advertisementCollection.EntityFactoryToUse = entityFactoryToUse;
				_advertisementCollection.GetMultiManyToOne(null, null, null, null, null, null, null, null, null, null, null, this, filter);
				_advertisementCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedAdvertisementCollection = true;
			}
			return _advertisementCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'AdvertisementCollection'. These settings will be taken into account
		/// when the property AdvertisementCollection is requested or GetMultiAdvertisementCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersAdvertisementCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_advertisementCollection.SortClauses=sortClauses;
			_advertisementCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'GenericproductEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'GenericproductEntity'</returns>
		public Obymobi.Data.CollectionClasses.GenericproductCollection GetMultiGenericproductCollection(bool forceFetch)
		{
			return GetMultiGenericproductCollection(forceFetch, _genericproductCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'GenericproductEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'GenericproductEntity'</returns>
		public Obymobi.Data.CollectionClasses.GenericproductCollection GetMultiGenericproductCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiGenericproductCollection(forceFetch, _genericproductCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'GenericproductEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.GenericproductCollection GetMultiGenericproductCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiGenericproductCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'GenericproductEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.GenericproductCollection GetMultiGenericproductCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedGenericproductCollection || forceFetch || _alwaysFetchGenericproductCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_genericproductCollection);
				_genericproductCollection.SuppressClearInGetMulti=!forceFetch;
				_genericproductCollection.EntityFactoryToUse = entityFactoryToUse;
				_genericproductCollection.GetMultiManyToOne(null, null, this, null, filter);
				_genericproductCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedGenericproductCollection = true;
			}
			return _genericproductCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'GenericproductCollection'. These settings will be taken into account
		/// when the property GenericproductCollection is requested or GetMultiGenericproductCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersGenericproductCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_genericproductCollection.SortClauses=sortClauses;
			_genericproductCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'CategoryEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CategoryEntity'</returns>
		public Obymobi.Data.CollectionClasses.CategoryCollection GetMultiCategoryCollectionViaAdvertisement(bool forceFetch)
		{
			return GetMultiCategoryCollectionViaAdvertisement(forceFetch, _categoryCollectionViaAdvertisement.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'CategoryEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.CategoryCollection GetMultiCategoryCollectionViaAdvertisement(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedCategoryCollectionViaAdvertisement || forceFetch || _alwaysFetchCategoryCollectionViaAdvertisement) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_categoryCollectionViaAdvertisement);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(SupplierFields.SupplierId, ComparisonOperator.Equal, this.SupplierId, "SupplierEntity__"));
				_categoryCollectionViaAdvertisement.SuppressClearInGetMulti=!forceFetch;
				_categoryCollectionViaAdvertisement.EntityFactoryToUse = entityFactoryToUse;
				_categoryCollectionViaAdvertisement.GetMulti(filter, GetRelationsForField("CategoryCollectionViaAdvertisement"));
				_categoryCollectionViaAdvertisement.SuppressClearInGetMulti=false;
				_alreadyFetchedCategoryCollectionViaAdvertisement = true;
			}
			return _categoryCollectionViaAdvertisement;
		}

		/// <summary> Sets the collection parameters for the collection for 'CategoryCollectionViaAdvertisement'. These settings will be taken into account
		/// when the property CategoryCollectionViaAdvertisement is requested or GetMultiCategoryCollectionViaAdvertisement is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCategoryCollectionViaAdvertisement(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_categoryCollectionViaAdvertisement.SortClauses=sortClauses;
			_categoryCollectionViaAdvertisement.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointgroupEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'DeliverypointgroupEntity'</returns>
		public Obymobi.Data.CollectionClasses.DeliverypointgroupCollection GetMultiDeliverypointgroupCollectionViaAdvertisement(bool forceFetch)
		{
			return GetMultiDeliverypointgroupCollectionViaAdvertisement(forceFetch, _deliverypointgroupCollectionViaAdvertisement.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointgroupEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.DeliverypointgroupCollection GetMultiDeliverypointgroupCollectionViaAdvertisement(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedDeliverypointgroupCollectionViaAdvertisement || forceFetch || _alwaysFetchDeliverypointgroupCollectionViaAdvertisement) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_deliverypointgroupCollectionViaAdvertisement);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(SupplierFields.SupplierId, ComparisonOperator.Equal, this.SupplierId, "SupplierEntity__"));
				_deliverypointgroupCollectionViaAdvertisement.SuppressClearInGetMulti=!forceFetch;
				_deliverypointgroupCollectionViaAdvertisement.EntityFactoryToUse = entityFactoryToUse;
				_deliverypointgroupCollectionViaAdvertisement.GetMulti(filter, GetRelationsForField("DeliverypointgroupCollectionViaAdvertisement"));
				_deliverypointgroupCollectionViaAdvertisement.SuppressClearInGetMulti=false;
				_alreadyFetchedDeliverypointgroupCollectionViaAdvertisement = true;
			}
			return _deliverypointgroupCollectionViaAdvertisement;
		}

		/// <summary> Sets the collection parameters for the collection for 'DeliverypointgroupCollectionViaAdvertisement'. These settings will be taken into account
		/// when the property DeliverypointgroupCollectionViaAdvertisement is requested or GetMultiDeliverypointgroupCollectionViaAdvertisement is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersDeliverypointgroupCollectionViaAdvertisement(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_deliverypointgroupCollectionViaAdvertisement.SortClauses=sortClauses;
			_deliverypointgroupCollectionViaAdvertisement.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'EntertainmentEntity'</returns>
		public Obymobi.Data.CollectionClasses.EntertainmentCollection GetMultiEntertainmentCollectionViaAdvertisement(bool forceFetch)
		{
			return GetMultiEntertainmentCollectionViaAdvertisement(forceFetch, _entertainmentCollectionViaAdvertisement.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.EntertainmentCollection GetMultiEntertainmentCollectionViaAdvertisement(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedEntertainmentCollectionViaAdvertisement || forceFetch || _alwaysFetchEntertainmentCollectionViaAdvertisement) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_entertainmentCollectionViaAdvertisement);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(SupplierFields.SupplierId, ComparisonOperator.Equal, this.SupplierId, "SupplierEntity__"));
				_entertainmentCollectionViaAdvertisement.SuppressClearInGetMulti=!forceFetch;
				_entertainmentCollectionViaAdvertisement.EntityFactoryToUse = entityFactoryToUse;
				_entertainmentCollectionViaAdvertisement.GetMulti(filter, GetRelationsForField("EntertainmentCollectionViaAdvertisement"));
				_entertainmentCollectionViaAdvertisement.SuppressClearInGetMulti=false;
				_alreadyFetchedEntertainmentCollectionViaAdvertisement = true;
			}
			return _entertainmentCollectionViaAdvertisement;
		}

		/// <summary> Sets the collection parameters for the collection for 'EntertainmentCollectionViaAdvertisement'. These settings will be taken into account
		/// when the property EntertainmentCollectionViaAdvertisement is requested or GetMultiEntertainmentCollectionViaAdvertisement is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersEntertainmentCollectionViaAdvertisement(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_entertainmentCollectionViaAdvertisement.SortClauses=sortClauses;
			_entertainmentCollectionViaAdvertisement.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentcategoryEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'EntertainmentcategoryEntity'</returns>
		public Obymobi.Data.CollectionClasses.EntertainmentcategoryCollection GetMultiEntertainmentcategoryCollectionViaAdvertisement(bool forceFetch)
		{
			return GetMultiEntertainmentcategoryCollectionViaAdvertisement(forceFetch, _entertainmentcategoryCollectionViaAdvertisement.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentcategoryEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.EntertainmentcategoryCollection GetMultiEntertainmentcategoryCollectionViaAdvertisement(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedEntertainmentcategoryCollectionViaAdvertisement || forceFetch || _alwaysFetchEntertainmentcategoryCollectionViaAdvertisement) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_entertainmentcategoryCollectionViaAdvertisement);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(SupplierFields.SupplierId, ComparisonOperator.Equal, this.SupplierId, "SupplierEntity__"));
				_entertainmentcategoryCollectionViaAdvertisement.SuppressClearInGetMulti=!forceFetch;
				_entertainmentcategoryCollectionViaAdvertisement.EntityFactoryToUse = entityFactoryToUse;
				_entertainmentcategoryCollectionViaAdvertisement.GetMulti(filter, GetRelationsForField("EntertainmentcategoryCollectionViaAdvertisement"));
				_entertainmentcategoryCollectionViaAdvertisement.SuppressClearInGetMulti=false;
				_alreadyFetchedEntertainmentcategoryCollectionViaAdvertisement = true;
			}
			return _entertainmentcategoryCollectionViaAdvertisement;
		}

		/// <summary> Sets the collection parameters for the collection for 'EntertainmentcategoryCollectionViaAdvertisement'. These settings will be taken into account
		/// when the property EntertainmentcategoryCollectionViaAdvertisement is requested or GetMultiEntertainmentcategoryCollectionViaAdvertisement is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersEntertainmentcategoryCollectionViaAdvertisement(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_entertainmentcategoryCollectionViaAdvertisement.SortClauses=sortClauses;
			_entertainmentcategoryCollectionViaAdvertisement.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("AdvertisementCollection", _advertisementCollection);
			toReturn.Add("GenericproductCollection", _genericproductCollection);
			toReturn.Add("CategoryCollectionViaAdvertisement", _categoryCollectionViaAdvertisement);
			toReturn.Add("DeliverypointgroupCollectionViaAdvertisement", _deliverypointgroupCollectionViaAdvertisement);
			toReturn.Add("EntertainmentCollectionViaAdvertisement", _entertainmentCollectionViaAdvertisement);
			toReturn.Add("EntertainmentcategoryCollectionViaAdvertisement", _entertainmentcategoryCollectionViaAdvertisement);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="supplierId">PK value for Supplier which data should be fetched into this Supplier object</param>
		/// <param name="validator">The validator object for this SupplierEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int32 supplierId, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(supplierId, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_advertisementCollection = new Obymobi.Data.CollectionClasses.AdvertisementCollection();
			_advertisementCollection.SetContainingEntityInfo(this, "SupplierEntity");

			_genericproductCollection = new Obymobi.Data.CollectionClasses.GenericproductCollection();
			_genericproductCollection.SetContainingEntityInfo(this, "SupplierEntity");
			_categoryCollectionViaAdvertisement = new Obymobi.Data.CollectionClasses.CategoryCollection();
			_deliverypointgroupCollectionViaAdvertisement = new Obymobi.Data.CollectionClasses.DeliverypointgroupCollection();
			_entertainmentCollectionViaAdvertisement = new Obymobi.Data.CollectionClasses.EntertainmentCollection();
			_entertainmentcategoryCollectionViaAdvertisement = new Obymobi.Data.CollectionClasses.EntertainmentcategoryCollection();
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SupplierId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Name", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Addressline1", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Addressline2", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Addressline3", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Addressline4", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Telephone1", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Telephone2", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Fax", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Email", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Website", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedUTC", fieldHashtable);
		}
		#endregion

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="supplierId">PK value for Supplier which data should be fetched into this Supplier object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int32 supplierId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)SupplierFieldIndex.SupplierId].ForcedCurrentValueWrite(supplierId);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateSupplierDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new SupplierEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static SupplierRelations Relations
		{
			get	{ return new SupplierRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Advertisement' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAdvertisementCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.AdvertisementCollection(), (IEntityRelation)GetRelationsForField("AdvertisementCollection")[0], (int)Obymobi.Data.EntityType.SupplierEntity, (int)Obymobi.Data.EntityType.AdvertisementEntity, 0, null, null, null, "AdvertisementCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Genericproduct' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathGenericproductCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.GenericproductCollection(), (IEntityRelation)GetRelationsForField("GenericproductCollection")[0], (int)Obymobi.Data.EntityType.SupplierEntity, (int)Obymobi.Data.EntityType.GenericproductEntity, 0, null, null, null, "GenericproductCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Category'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCategoryCollectionViaAdvertisement
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.AdvertisementEntityUsingSupplierId;
				intermediateRelation.SetAliases(string.Empty, "Advertisement_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CategoryCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.SupplierEntity, (int)Obymobi.Data.EntityType.CategoryEntity, 0, null, null, GetRelationsForField("CategoryCollectionViaAdvertisement"), "CategoryCollectionViaAdvertisement", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Deliverypointgroup'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathDeliverypointgroupCollectionViaAdvertisement
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.AdvertisementEntityUsingSupplierId;
				intermediateRelation.SetAliases(string.Empty, "Advertisement_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.DeliverypointgroupCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.SupplierEntity, (int)Obymobi.Data.EntityType.DeliverypointgroupEntity, 0, null, null, GetRelationsForField("DeliverypointgroupCollectionViaAdvertisement"), "DeliverypointgroupCollectionViaAdvertisement", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Entertainment'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathEntertainmentCollectionViaAdvertisement
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.AdvertisementEntityUsingSupplierId;
				intermediateRelation.SetAliases(string.Empty, "Advertisement_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.EntertainmentCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.SupplierEntity, (int)Obymobi.Data.EntityType.EntertainmentEntity, 0, null, null, GetRelationsForField("EntertainmentCollectionViaAdvertisement"), "EntertainmentCollectionViaAdvertisement", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Entertainmentcategory'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathEntertainmentcategoryCollectionViaAdvertisement
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.AdvertisementEntityUsingSupplierId;
				intermediateRelation.SetAliases(string.Empty, "Advertisement_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.EntertainmentcategoryCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.SupplierEntity, (int)Obymobi.Data.EntityType.EntertainmentcategoryEntity, 0, null, null, GetRelationsForField("EntertainmentcategoryCollectionViaAdvertisement"), "EntertainmentcategoryCollectionViaAdvertisement", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The SupplierId property of the Entity Supplier<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Supplier"."SupplierId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 SupplierId
		{
			get { return (System.Int32)GetValue((int)SupplierFieldIndex.SupplierId, true); }
			set	{ SetValue((int)SupplierFieldIndex.SupplierId, value, true); }
		}

		/// <summary> The Name property of the Entity Supplier<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Supplier"."Name"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 200<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Name
		{
			get { return (System.String)GetValue((int)SupplierFieldIndex.Name, true); }
			set	{ SetValue((int)SupplierFieldIndex.Name, value, true); }
		}

		/// <summary> The Addressline1 property of the Entity Supplier<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Supplier"."Addressline1"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 250<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Addressline1
		{
			get { return (System.String)GetValue((int)SupplierFieldIndex.Addressline1, true); }
			set	{ SetValue((int)SupplierFieldIndex.Addressline1, value, true); }
		}

		/// <summary> The Addressline2 property of the Entity Supplier<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Supplier"."Addressline2"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 250<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Addressline2
		{
			get { return (System.String)GetValue((int)SupplierFieldIndex.Addressline2, true); }
			set	{ SetValue((int)SupplierFieldIndex.Addressline2, value, true); }
		}

		/// <summary> The Addressline3 property of the Entity Supplier<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Supplier"."Addressline3"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 250<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Addressline3
		{
			get { return (System.String)GetValue((int)SupplierFieldIndex.Addressline3, true); }
			set	{ SetValue((int)SupplierFieldIndex.Addressline3, value, true); }
		}

		/// <summary> The Addressline4 property of the Entity Supplier<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Supplier"."Addressline4"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 250<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Addressline4
		{
			get { return (System.String)GetValue((int)SupplierFieldIndex.Addressline4, true); }
			set	{ SetValue((int)SupplierFieldIndex.Addressline4, value, true); }
		}

		/// <summary> The Telephone1 property of the Entity Supplier<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Supplier"."Telephone1"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Telephone1
		{
			get { return (System.String)GetValue((int)SupplierFieldIndex.Telephone1, true); }
			set	{ SetValue((int)SupplierFieldIndex.Telephone1, value, true); }
		}

		/// <summary> The Telephone2 property of the Entity Supplier<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Supplier"."Telephone2"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Telephone2
		{
			get { return (System.String)GetValue((int)SupplierFieldIndex.Telephone2, true); }
			set	{ SetValue((int)SupplierFieldIndex.Telephone2, value, true); }
		}

		/// <summary> The Fax property of the Entity Supplier<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Supplier"."Fax"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Fax
		{
			get { return (System.String)GetValue((int)SupplierFieldIndex.Fax, true); }
			set	{ SetValue((int)SupplierFieldIndex.Fax, value, true); }
		}

		/// <summary> The Email property of the Entity Supplier<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Supplier"."Email"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 100<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Email
		{
			get { return (System.String)GetValue((int)SupplierFieldIndex.Email, true); }
			set	{ SetValue((int)SupplierFieldIndex.Email, value, true); }
		}

		/// <summary> The Website property of the Entity Supplier<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Supplier"."Website"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 100<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Website
		{
			get { return (System.String)GetValue((int)SupplierFieldIndex.Website, true); }
			set	{ SetValue((int)SupplierFieldIndex.Website, value, true); }
		}

		/// <summary> The CreatedBy property of the Entity Supplier<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Supplier"."CreatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CreatedBy
		{
			get { return (System.Int32)GetValue((int)SupplierFieldIndex.CreatedBy, true); }
			set	{ SetValue((int)SupplierFieldIndex.CreatedBy, value, true); }
		}

		/// <summary> The UpdatedBy property of the Entity Supplier<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Supplier"."UpdatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 UpdatedBy
		{
			get { return (System.Int32)GetValue((int)SupplierFieldIndex.UpdatedBy, true); }
			set	{ SetValue((int)SupplierFieldIndex.UpdatedBy, value, true); }
		}

		/// <summary> The CreatedUTC property of the Entity Supplier<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Supplier"."CreatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CreatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)SupplierFieldIndex.CreatedUTC, false); }
			set	{ SetValue((int)SupplierFieldIndex.CreatedUTC, value, true); }
		}

		/// <summary> The UpdatedUTC property of the Entity Supplier<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Supplier"."UpdatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)SupplierFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)SupplierFieldIndex.UpdatedUTC, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'AdvertisementEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiAdvertisementCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.AdvertisementCollection AdvertisementCollection
		{
			get	{ return GetMultiAdvertisementCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for AdvertisementCollection. When set to true, AdvertisementCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time AdvertisementCollection is accessed. You can always execute/ a forced fetch by calling GetMultiAdvertisementCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAdvertisementCollection
		{
			get	{ return _alwaysFetchAdvertisementCollection; }
			set	{ _alwaysFetchAdvertisementCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property AdvertisementCollection already has been fetched. Setting this property to false when AdvertisementCollection has been fetched
		/// will clear the AdvertisementCollection collection well. Setting this property to true while AdvertisementCollection hasn't been fetched disables lazy loading for AdvertisementCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAdvertisementCollection
		{
			get { return _alreadyFetchedAdvertisementCollection;}
			set 
			{
				if(_alreadyFetchedAdvertisementCollection && !value && (_advertisementCollection != null))
				{
					_advertisementCollection.Clear();
				}
				_alreadyFetchedAdvertisementCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'GenericproductEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiGenericproductCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.GenericproductCollection GenericproductCollection
		{
			get	{ return GetMultiGenericproductCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for GenericproductCollection. When set to true, GenericproductCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time GenericproductCollection is accessed. You can always execute/ a forced fetch by calling GetMultiGenericproductCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchGenericproductCollection
		{
			get	{ return _alwaysFetchGenericproductCollection; }
			set	{ _alwaysFetchGenericproductCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property GenericproductCollection already has been fetched. Setting this property to false when GenericproductCollection has been fetched
		/// will clear the GenericproductCollection collection well. Setting this property to true while GenericproductCollection hasn't been fetched disables lazy loading for GenericproductCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedGenericproductCollection
		{
			get { return _alreadyFetchedGenericproductCollection;}
			set 
			{
				if(_alreadyFetchedGenericproductCollection && !value && (_genericproductCollection != null))
				{
					_genericproductCollection.Clear();
				}
				_alreadyFetchedGenericproductCollection = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'CategoryEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCategoryCollectionViaAdvertisement()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.CategoryCollection CategoryCollectionViaAdvertisement
		{
			get { return GetMultiCategoryCollectionViaAdvertisement(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CategoryCollectionViaAdvertisement. When set to true, CategoryCollectionViaAdvertisement is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CategoryCollectionViaAdvertisement is accessed. You can always execute a forced fetch by calling GetMultiCategoryCollectionViaAdvertisement(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCategoryCollectionViaAdvertisement
		{
			get	{ return _alwaysFetchCategoryCollectionViaAdvertisement; }
			set	{ _alwaysFetchCategoryCollectionViaAdvertisement = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CategoryCollectionViaAdvertisement already has been fetched. Setting this property to false when CategoryCollectionViaAdvertisement has been fetched
		/// will clear the CategoryCollectionViaAdvertisement collection well. Setting this property to true while CategoryCollectionViaAdvertisement hasn't been fetched disables lazy loading for CategoryCollectionViaAdvertisement</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCategoryCollectionViaAdvertisement
		{
			get { return _alreadyFetchedCategoryCollectionViaAdvertisement;}
			set 
			{
				if(_alreadyFetchedCategoryCollectionViaAdvertisement && !value && (_categoryCollectionViaAdvertisement != null))
				{
					_categoryCollectionViaAdvertisement.Clear();
				}
				_alreadyFetchedCategoryCollectionViaAdvertisement = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointgroupEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiDeliverypointgroupCollectionViaAdvertisement()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.DeliverypointgroupCollection DeliverypointgroupCollectionViaAdvertisement
		{
			get { return GetMultiDeliverypointgroupCollectionViaAdvertisement(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for DeliverypointgroupCollectionViaAdvertisement. When set to true, DeliverypointgroupCollectionViaAdvertisement is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time DeliverypointgroupCollectionViaAdvertisement is accessed. You can always execute a forced fetch by calling GetMultiDeliverypointgroupCollectionViaAdvertisement(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchDeliverypointgroupCollectionViaAdvertisement
		{
			get	{ return _alwaysFetchDeliverypointgroupCollectionViaAdvertisement; }
			set	{ _alwaysFetchDeliverypointgroupCollectionViaAdvertisement = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property DeliverypointgroupCollectionViaAdvertisement already has been fetched. Setting this property to false when DeliverypointgroupCollectionViaAdvertisement has been fetched
		/// will clear the DeliverypointgroupCollectionViaAdvertisement collection well. Setting this property to true while DeliverypointgroupCollectionViaAdvertisement hasn't been fetched disables lazy loading for DeliverypointgroupCollectionViaAdvertisement</summary>
		[Browsable(false)]
		public bool AlreadyFetchedDeliverypointgroupCollectionViaAdvertisement
		{
			get { return _alreadyFetchedDeliverypointgroupCollectionViaAdvertisement;}
			set 
			{
				if(_alreadyFetchedDeliverypointgroupCollectionViaAdvertisement && !value && (_deliverypointgroupCollectionViaAdvertisement != null))
				{
					_deliverypointgroupCollectionViaAdvertisement.Clear();
				}
				_alreadyFetchedDeliverypointgroupCollectionViaAdvertisement = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiEntertainmentCollectionViaAdvertisement()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.EntertainmentCollection EntertainmentCollectionViaAdvertisement
		{
			get { return GetMultiEntertainmentCollectionViaAdvertisement(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for EntertainmentCollectionViaAdvertisement. When set to true, EntertainmentCollectionViaAdvertisement is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time EntertainmentCollectionViaAdvertisement is accessed. You can always execute a forced fetch by calling GetMultiEntertainmentCollectionViaAdvertisement(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchEntertainmentCollectionViaAdvertisement
		{
			get	{ return _alwaysFetchEntertainmentCollectionViaAdvertisement; }
			set	{ _alwaysFetchEntertainmentCollectionViaAdvertisement = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property EntertainmentCollectionViaAdvertisement already has been fetched. Setting this property to false when EntertainmentCollectionViaAdvertisement has been fetched
		/// will clear the EntertainmentCollectionViaAdvertisement collection well. Setting this property to true while EntertainmentCollectionViaAdvertisement hasn't been fetched disables lazy loading for EntertainmentCollectionViaAdvertisement</summary>
		[Browsable(false)]
		public bool AlreadyFetchedEntertainmentCollectionViaAdvertisement
		{
			get { return _alreadyFetchedEntertainmentCollectionViaAdvertisement;}
			set 
			{
				if(_alreadyFetchedEntertainmentCollectionViaAdvertisement && !value && (_entertainmentCollectionViaAdvertisement != null))
				{
					_entertainmentCollectionViaAdvertisement.Clear();
				}
				_alreadyFetchedEntertainmentCollectionViaAdvertisement = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentcategoryEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiEntertainmentcategoryCollectionViaAdvertisement()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.EntertainmentcategoryCollection EntertainmentcategoryCollectionViaAdvertisement
		{
			get { return GetMultiEntertainmentcategoryCollectionViaAdvertisement(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for EntertainmentcategoryCollectionViaAdvertisement. When set to true, EntertainmentcategoryCollectionViaAdvertisement is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time EntertainmentcategoryCollectionViaAdvertisement is accessed. You can always execute a forced fetch by calling GetMultiEntertainmentcategoryCollectionViaAdvertisement(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchEntertainmentcategoryCollectionViaAdvertisement
		{
			get	{ return _alwaysFetchEntertainmentcategoryCollectionViaAdvertisement; }
			set	{ _alwaysFetchEntertainmentcategoryCollectionViaAdvertisement = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property EntertainmentcategoryCollectionViaAdvertisement already has been fetched. Setting this property to false when EntertainmentcategoryCollectionViaAdvertisement has been fetched
		/// will clear the EntertainmentcategoryCollectionViaAdvertisement collection well. Setting this property to true while EntertainmentcategoryCollectionViaAdvertisement hasn't been fetched disables lazy loading for EntertainmentcategoryCollectionViaAdvertisement</summary>
		[Browsable(false)]
		public bool AlreadyFetchedEntertainmentcategoryCollectionViaAdvertisement
		{
			get { return _alreadyFetchedEntertainmentcategoryCollectionViaAdvertisement;}
			set 
			{
				if(_alreadyFetchedEntertainmentcategoryCollectionViaAdvertisement && !value && (_entertainmentcategoryCollectionViaAdvertisement != null))
				{
					_entertainmentcategoryCollectionViaAdvertisement.Clear();
				}
				_alreadyFetchedEntertainmentcategoryCollectionViaAdvertisement = value;
			}
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the Obymobi.Data.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)Obymobi.Data.EntityType.SupplierEntity; }
		}

		#endregion

		
		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
