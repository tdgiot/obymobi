﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
using System.Runtime.Serialization;
using System.Data;
using System.Xml.Serialization;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity base class which represents the base class for the entity 'SurveyQuestion'.<br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public abstract partial class SurveyQuestionEntityBase : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		public new string LLBLGenProEntityName {
			get { return "SurveyQuestionEntity"; }
		}
	
		#region Class Member Declarations
		private Obymobi.Data.CollectionClasses.SurveyAnswerCollection	_surveyAnswerCollection;
		private bool	_alwaysFetchSurveyAnswerCollection, _alreadyFetchedSurveyAnswerCollection;
		private Obymobi.Data.CollectionClasses.SurveyAnswerCollection	_surveyAnswerTargetCollection;
		private bool	_alwaysFetchSurveyAnswerTargetCollection, _alreadyFetchedSurveyAnswerTargetCollection;
		private Obymobi.Data.CollectionClasses.SurveyQuestionCollection	_surveyQuestionCollection;
		private bool	_alwaysFetchSurveyQuestionCollection, _alreadyFetchedSurveyQuestionCollection;
		private Obymobi.Data.CollectionClasses.SurveyQuestionLanguageCollection	_surveyQuestionLanguageCollection;
		private bool	_alwaysFetchSurveyQuestionLanguageCollection, _alreadyFetchedSurveyQuestionLanguageCollection;
		private Obymobi.Data.CollectionClasses.SurveyResultCollection	_surveyResultCollection;
		private bool	_alwaysFetchSurveyResultCollection, _alreadyFetchedSurveyResultCollection;
		private Obymobi.Data.CollectionClasses.SurveyPageCollection _surveyPageCollectionViaSurveyQuestion;
		private bool	_alwaysFetchSurveyPageCollectionViaSurveyQuestion, _alreadyFetchedSurveyPageCollectionViaSurveyQuestion;
		private Obymobi.Data.CollectionClasses.SurveyQuestionCollection _surveyQuestionCollectionViaSurveyAnswer;
		private bool	_alwaysFetchSurveyQuestionCollectionViaSurveyAnswer, _alreadyFetchedSurveyQuestionCollectionViaSurveyAnswer;
		private Obymobi.Data.CollectionClasses.SurveyQuestionCollection _surveyQuestionCollectionViaSurveyAnswer_;
		private bool	_alwaysFetchSurveyQuestionCollectionViaSurveyAnswer_, _alreadyFetchedSurveyQuestionCollectionViaSurveyAnswer_;
		private SurveyPageEntity _surveyPageEntity;
		private bool	_alwaysFetchSurveyPageEntity, _alreadyFetchedSurveyPageEntity, _surveyPageEntityReturnsNewIfNotFound;
		private SurveyQuestionEntity _parentSurveyQuestionEntity;
		private bool	_alwaysFetchParentSurveyQuestionEntity, _alreadyFetchedParentSurveyQuestionEntity, _parentSurveyQuestionEntityReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name SurveyPageEntity</summary>
			public static readonly string SurveyPageEntity = "SurveyPageEntity";
			/// <summary>Member name ParentSurveyQuestionEntity</summary>
			public static readonly string ParentSurveyQuestionEntity = "ParentSurveyQuestionEntity";
			/// <summary>Member name SurveyAnswerCollection</summary>
			public static readonly string SurveyAnswerCollection = "SurveyAnswerCollection";
			/// <summary>Member name SurveyAnswerTargetCollection</summary>
			public static readonly string SurveyAnswerTargetCollection = "SurveyAnswerTargetCollection";
			/// <summary>Member name SurveyQuestionCollection</summary>
			public static readonly string SurveyQuestionCollection = "SurveyQuestionCollection";
			/// <summary>Member name SurveyQuestionLanguageCollection</summary>
			public static readonly string SurveyQuestionLanguageCollection = "SurveyQuestionLanguageCollection";
			/// <summary>Member name SurveyResultCollection</summary>
			public static readonly string SurveyResultCollection = "SurveyResultCollection";
			/// <summary>Member name SurveyPageCollectionViaSurveyQuestion</summary>
			public static readonly string SurveyPageCollectionViaSurveyQuestion = "SurveyPageCollectionViaSurveyQuestion";
			/// <summary>Member name SurveyQuestionCollectionViaSurveyAnswer</summary>
			public static readonly string SurveyQuestionCollectionViaSurveyAnswer = "SurveyQuestionCollectionViaSurveyAnswer";
			/// <summary>Member name SurveyQuestionCollectionViaSurveyAnswer_</summary>
			public static readonly string SurveyQuestionCollectionViaSurveyAnswer_ = "SurveyQuestionCollectionViaSurveyAnswer_";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static SurveyQuestionEntityBase()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		protected SurveyQuestionEntityBase() :base("SurveyQuestionEntity")
		{
			InitClassEmpty(null);
		}

		/// <summary>CTor</summary>
		/// <param name="surveyQuestionId">PK value for SurveyQuestion which data should be fetched into this SurveyQuestion object</param>
		protected SurveyQuestionEntityBase(System.Int32 surveyQuestionId):base("SurveyQuestionEntity")
		{
			InitClassFetch(surveyQuestionId, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="surveyQuestionId">PK value for SurveyQuestion which data should be fetched into this SurveyQuestion object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		protected SurveyQuestionEntityBase(System.Int32 surveyQuestionId, IPrefetchPath prefetchPathToUse): base("SurveyQuestionEntity")
		{
			InitClassFetch(surveyQuestionId, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="surveyQuestionId">PK value for SurveyQuestion which data should be fetched into this SurveyQuestion object</param>
		/// <param name="validator">The custom validator object for this SurveyQuestionEntity</param>
		protected SurveyQuestionEntityBase(System.Int32 surveyQuestionId, IValidator validator):base("SurveyQuestionEntity")
		{
			InitClassFetch(surveyQuestionId, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected SurveyQuestionEntityBase(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_surveyAnswerCollection = (Obymobi.Data.CollectionClasses.SurveyAnswerCollection)info.GetValue("_surveyAnswerCollection", typeof(Obymobi.Data.CollectionClasses.SurveyAnswerCollection));
			_alwaysFetchSurveyAnswerCollection = info.GetBoolean("_alwaysFetchSurveyAnswerCollection");
			_alreadyFetchedSurveyAnswerCollection = info.GetBoolean("_alreadyFetchedSurveyAnswerCollection");

			_surveyAnswerTargetCollection = (Obymobi.Data.CollectionClasses.SurveyAnswerCollection)info.GetValue("_surveyAnswerTargetCollection", typeof(Obymobi.Data.CollectionClasses.SurveyAnswerCollection));
			_alwaysFetchSurveyAnswerTargetCollection = info.GetBoolean("_alwaysFetchSurveyAnswerTargetCollection");
			_alreadyFetchedSurveyAnswerTargetCollection = info.GetBoolean("_alreadyFetchedSurveyAnswerTargetCollection");

			_surveyQuestionCollection = (Obymobi.Data.CollectionClasses.SurveyQuestionCollection)info.GetValue("_surveyQuestionCollection", typeof(Obymobi.Data.CollectionClasses.SurveyQuestionCollection));
			_alwaysFetchSurveyQuestionCollection = info.GetBoolean("_alwaysFetchSurveyQuestionCollection");
			_alreadyFetchedSurveyQuestionCollection = info.GetBoolean("_alreadyFetchedSurveyQuestionCollection");

			_surveyQuestionLanguageCollection = (Obymobi.Data.CollectionClasses.SurveyQuestionLanguageCollection)info.GetValue("_surveyQuestionLanguageCollection", typeof(Obymobi.Data.CollectionClasses.SurveyQuestionLanguageCollection));
			_alwaysFetchSurveyQuestionLanguageCollection = info.GetBoolean("_alwaysFetchSurveyQuestionLanguageCollection");
			_alreadyFetchedSurveyQuestionLanguageCollection = info.GetBoolean("_alreadyFetchedSurveyQuestionLanguageCollection");

			_surveyResultCollection = (Obymobi.Data.CollectionClasses.SurveyResultCollection)info.GetValue("_surveyResultCollection", typeof(Obymobi.Data.CollectionClasses.SurveyResultCollection));
			_alwaysFetchSurveyResultCollection = info.GetBoolean("_alwaysFetchSurveyResultCollection");
			_alreadyFetchedSurveyResultCollection = info.GetBoolean("_alreadyFetchedSurveyResultCollection");
			_surveyPageCollectionViaSurveyQuestion = (Obymobi.Data.CollectionClasses.SurveyPageCollection)info.GetValue("_surveyPageCollectionViaSurveyQuestion", typeof(Obymobi.Data.CollectionClasses.SurveyPageCollection));
			_alwaysFetchSurveyPageCollectionViaSurveyQuestion = info.GetBoolean("_alwaysFetchSurveyPageCollectionViaSurveyQuestion");
			_alreadyFetchedSurveyPageCollectionViaSurveyQuestion = info.GetBoolean("_alreadyFetchedSurveyPageCollectionViaSurveyQuestion");

			_surveyQuestionCollectionViaSurveyAnswer = (Obymobi.Data.CollectionClasses.SurveyQuestionCollection)info.GetValue("_surveyQuestionCollectionViaSurveyAnswer", typeof(Obymobi.Data.CollectionClasses.SurveyQuestionCollection));
			_alwaysFetchSurveyQuestionCollectionViaSurveyAnswer = info.GetBoolean("_alwaysFetchSurveyQuestionCollectionViaSurveyAnswer");
			_alreadyFetchedSurveyQuestionCollectionViaSurveyAnswer = info.GetBoolean("_alreadyFetchedSurveyQuestionCollectionViaSurveyAnswer");

			_surveyQuestionCollectionViaSurveyAnswer_ = (Obymobi.Data.CollectionClasses.SurveyQuestionCollection)info.GetValue("_surveyQuestionCollectionViaSurveyAnswer_", typeof(Obymobi.Data.CollectionClasses.SurveyQuestionCollection));
			_alwaysFetchSurveyQuestionCollectionViaSurveyAnswer_ = info.GetBoolean("_alwaysFetchSurveyQuestionCollectionViaSurveyAnswer_");
			_alreadyFetchedSurveyQuestionCollectionViaSurveyAnswer_ = info.GetBoolean("_alreadyFetchedSurveyQuestionCollectionViaSurveyAnswer_");
			_surveyPageEntity = (SurveyPageEntity)info.GetValue("_surveyPageEntity", typeof(SurveyPageEntity));
			if(_surveyPageEntity!=null)
			{
				_surveyPageEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_surveyPageEntityReturnsNewIfNotFound = info.GetBoolean("_surveyPageEntityReturnsNewIfNotFound");
			_alwaysFetchSurveyPageEntity = info.GetBoolean("_alwaysFetchSurveyPageEntity");
			_alreadyFetchedSurveyPageEntity = info.GetBoolean("_alreadyFetchedSurveyPageEntity");

			_parentSurveyQuestionEntity = (SurveyQuestionEntity)info.GetValue("_parentSurveyQuestionEntity", typeof(SurveyQuestionEntity));
			if(_parentSurveyQuestionEntity!=null)
			{
				_parentSurveyQuestionEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_parentSurveyQuestionEntityReturnsNewIfNotFound = info.GetBoolean("_parentSurveyQuestionEntityReturnsNewIfNotFound");
			_alwaysFetchParentSurveyQuestionEntity = info.GetBoolean("_alwaysFetchParentSurveyQuestionEntity");
			_alreadyFetchedParentSurveyQuestionEntity = info.GetBoolean("_alreadyFetchedParentSurveyQuestionEntity");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}	
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((SurveyQuestionFieldIndex)fieldIndex)
			{
				case SurveyQuestionFieldIndex.ParentQuestionId:
					DesetupSyncParentSurveyQuestionEntity(true, false);
					_alreadyFetchedParentSurveyQuestionEntity = false;
					break;
				case SurveyQuestionFieldIndex.SurveyPageId:
					DesetupSyncSurveyPageEntity(true, false);
					_alreadyFetchedSurveyPageEntity = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedSurveyAnswerCollection = (_surveyAnswerCollection.Count > 0);
			_alreadyFetchedSurveyAnswerTargetCollection = (_surveyAnswerTargetCollection.Count > 0);
			_alreadyFetchedSurveyQuestionCollection = (_surveyQuestionCollection.Count > 0);
			_alreadyFetchedSurveyQuestionLanguageCollection = (_surveyQuestionLanguageCollection.Count > 0);
			_alreadyFetchedSurveyResultCollection = (_surveyResultCollection.Count > 0);
			_alreadyFetchedSurveyPageCollectionViaSurveyQuestion = (_surveyPageCollectionViaSurveyQuestion.Count > 0);
			_alreadyFetchedSurveyQuestionCollectionViaSurveyAnswer = (_surveyQuestionCollectionViaSurveyAnswer.Count > 0);
			_alreadyFetchedSurveyQuestionCollectionViaSurveyAnswer_ = (_surveyQuestionCollectionViaSurveyAnswer_.Count > 0);
			_alreadyFetchedSurveyPageEntity = (_surveyPageEntity != null);
			_alreadyFetchedParentSurveyQuestionEntity = (_parentSurveyQuestionEntity != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "SurveyPageEntity":
					toReturn.Add(Relations.SurveyPageEntityUsingSurveyPageId);
					break;
				case "ParentSurveyQuestionEntity":
					toReturn.Add(Relations.SurveyQuestionEntityUsingSurveyQuestionIdParentQuestionId);
					break;
				case "SurveyAnswerCollection":
					toReturn.Add(Relations.SurveyAnswerEntityUsingSurveyQuestionId);
					break;
				case "SurveyAnswerTargetCollection":
					toReturn.Add(Relations.SurveyAnswerEntityUsingTargetSurveyQuestionId);
					break;
				case "SurveyQuestionCollection":
					toReturn.Add(Relations.SurveyQuestionEntityUsingParentQuestionId);
					break;
				case "SurveyQuestionLanguageCollection":
					toReturn.Add(Relations.SurveyQuestionLanguageEntityUsingSurveyQuestionId);
					break;
				case "SurveyResultCollection":
					toReturn.Add(Relations.SurveyResultEntityUsingSurveyQuestionId);
					break;
				case "SurveyPageCollectionViaSurveyQuestion":
					toReturn.Add(Relations.SurveyQuestionEntityUsingParentQuestionId, "SurveyQuestionEntity__", "SurveyQuestion_", JoinHint.None);
					toReturn.Add(SurveyQuestionEntity.Relations.SurveyPageEntityUsingSurveyPageId, "SurveyQuestion_", string.Empty, JoinHint.None);
					break;
				case "SurveyQuestionCollectionViaSurveyAnswer":
					toReturn.Add(Relations.SurveyAnswerEntityUsingSurveyQuestionId, "SurveyQuestionEntity__", "SurveyAnswer_", JoinHint.None);
					toReturn.Add(SurveyAnswerEntity.Relations.SurveyQuestionEntityUsingTargetSurveyQuestionId, "SurveyAnswer_", string.Empty, JoinHint.None);
					break;
				case "SurveyQuestionCollectionViaSurveyAnswer_":
					toReturn.Add(Relations.SurveyAnswerEntityUsingSurveyQuestionId, "SurveyQuestionEntity__", "SurveyAnswer_", JoinHint.None);
					toReturn.Add(SurveyAnswerEntity.Relations.SurveyQuestionEntityUsingTargetSurveyQuestionId, "SurveyAnswer_", string.Empty, JoinHint.None);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_surveyAnswerCollection", (!this.MarkedForDeletion?_surveyAnswerCollection:null));
			info.AddValue("_alwaysFetchSurveyAnswerCollection", _alwaysFetchSurveyAnswerCollection);
			info.AddValue("_alreadyFetchedSurveyAnswerCollection", _alreadyFetchedSurveyAnswerCollection);
			info.AddValue("_surveyAnswerTargetCollection", (!this.MarkedForDeletion?_surveyAnswerTargetCollection:null));
			info.AddValue("_alwaysFetchSurveyAnswerTargetCollection", _alwaysFetchSurveyAnswerTargetCollection);
			info.AddValue("_alreadyFetchedSurveyAnswerTargetCollection", _alreadyFetchedSurveyAnswerTargetCollection);
			info.AddValue("_surveyQuestionCollection", (!this.MarkedForDeletion?_surveyQuestionCollection:null));
			info.AddValue("_alwaysFetchSurveyQuestionCollection", _alwaysFetchSurveyQuestionCollection);
			info.AddValue("_alreadyFetchedSurveyQuestionCollection", _alreadyFetchedSurveyQuestionCollection);
			info.AddValue("_surveyQuestionLanguageCollection", (!this.MarkedForDeletion?_surveyQuestionLanguageCollection:null));
			info.AddValue("_alwaysFetchSurveyQuestionLanguageCollection", _alwaysFetchSurveyQuestionLanguageCollection);
			info.AddValue("_alreadyFetchedSurveyQuestionLanguageCollection", _alreadyFetchedSurveyQuestionLanguageCollection);
			info.AddValue("_surveyResultCollection", (!this.MarkedForDeletion?_surveyResultCollection:null));
			info.AddValue("_alwaysFetchSurveyResultCollection", _alwaysFetchSurveyResultCollection);
			info.AddValue("_alreadyFetchedSurveyResultCollection", _alreadyFetchedSurveyResultCollection);
			info.AddValue("_surveyPageCollectionViaSurveyQuestion", (!this.MarkedForDeletion?_surveyPageCollectionViaSurveyQuestion:null));
			info.AddValue("_alwaysFetchSurveyPageCollectionViaSurveyQuestion", _alwaysFetchSurveyPageCollectionViaSurveyQuestion);
			info.AddValue("_alreadyFetchedSurveyPageCollectionViaSurveyQuestion", _alreadyFetchedSurveyPageCollectionViaSurveyQuestion);
			info.AddValue("_surveyQuestionCollectionViaSurveyAnswer", (!this.MarkedForDeletion?_surveyQuestionCollectionViaSurveyAnswer:null));
			info.AddValue("_alwaysFetchSurveyQuestionCollectionViaSurveyAnswer", _alwaysFetchSurveyQuestionCollectionViaSurveyAnswer);
			info.AddValue("_alreadyFetchedSurveyQuestionCollectionViaSurveyAnswer", _alreadyFetchedSurveyQuestionCollectionViaSurveyAnswer);
			info.AddValue("_surveyQuestionCollectionViaSurveyAnswer_", (!this.MarkedForDeletion?_surveyQuestionCollectionViaSurveyAnswer_:null));
			info.AddValue("_alwaysFetchSurveyQuestionCollectionViaSurveyAnswer_", _alwaysFetchSurveyQuestionCollectionViaSurveyAnswer_);
			info.AddValue("_alreadyFetchedSurveyQuestionCollectionViaSurveyAnswer_", _alreadyFetchedSurveyQuestionCollectionViaSurveyAnswer_);
			info.AddValue("_surveyPageEntity", (!this.MarkedForDeletion?_surveyPageEntity:null));
			info.AddValue("_surveyPageEntityReturnsNewIfNotFound", _surveyPageEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchSurveyPageEntity", _alwaysFetchSurveyPageEntity);
			info.AddValue("_alreadyFetchedSurveyPageEntity", _alreadyFetchedSurveyPageEntity);
			info.AddValue("_parentSurveyQuestionEntity", (!this.MarkedForDeletion?_parentSurveyQuestionEntity:null));
			info.AddValue("_parentSurveyQuestionEntityReturnsNewIfNotFound", _parentSurveyQuestionEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchParentSurveyQuestionEntity", _alwaysFetchParentSurveyQuestionEntity);
			info.AddValue("_alreadyFetchedParentSurveyQuestionEntity", _alreadyFetchedParentSurveyQuestionEntity);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "SurveyPageEntity":
					_alreadyFetchedSurveyPageEntity = true;
					this.SurveyPageEntity = (SurveyPageEntity)entity;
					break;
				case "ParentSurveyQuestionEntity":
					_alreadyFetchedParentSurveyQuestionEntity = true;
					this.ParentSurveyQuestionEntity = (SurveyQuestionEntity)entity;
					break;
				case "SurveyAnswerCollection":
					_alreadyFetchedSurveyAnswerCollection = true;
					if(entity!=null)
					{
						this.SurveyAnswerCollection.Add((SurveyAnswerEntity)entity);
					}
					break;
				case "SurveyAnswerTargetCollection":
					_alreadyFetchedSurveyAnswerTargetCollection = true;
					if(entity!=null)
					{
						this.SurveyAnswerTargetCollection.Add((SurveyAnswerEntity)entity);
					}
					break;
				case "SurveyQuestionCollection":
					_alreadyFetchedSurveyQuestionCollection = true;
					if(entity!=null)
					{
						this.SurveyQuestionCollection.Add((SurveyQuestionEntity)entity);
					}
					break;
				case "SurveyQuestionLanguageCollection":
					_alreadyFetchedSurveyQuestionLanguageCollection = true;
					if(entity!=null)
					{
						this.SurveyQuestionLanguageCollection.Add((SurveyQuestionLanguageEntity)entity);
					}
					break;
				case "SurveyResultCollection":
					_alreadyFetchedSurveyResultCollection = true;
					if(entity!=null)
					{
						this.SurveyResultCollection.Add((SurveyResultEntity)entity);
					}
					break;
				case "SurveyPageCollectionViaSurveyQuestion":
					_alreadyFetchedSurveyPageCollectionViaSurveyQuestion = true;
					if(entity!=null)
					{
						this.SurveyPageCollectionViaSurveyQuestion.Add((SurveyPageEntity)entity);
					}
					break;
				case "SurveyQuestionCollectionViaSurveyAnswer":
					_alreadyFetchedSurveyQuestionCollectionViaSurveyAnswer = true;
					if(entity!=null)
					{
						this.SurveyQuestionCollectionViaSurveyAnswer.Add((SurveyQuestionEntity)entity);
					}
					break;
				case "SurveyQuestionCollectionViaSurveyAnswer_":
					_alreadyFetchedSurveyQuestionCollectionViaSurveyAnswer_ = true;
					if(entity!=null)
					{
						this.SurveyQuestionCollectionViaSurveyAnswer_.Add((SurveyQuestionEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "SurveyPageEntity":
					SetupSyncSurveyPageEntity(relatedEntity);
					break;
				case "ParentSurveyQuestionEntity":
					SetupSyncParentSurveyQuestionEntity(relatedEntity);
					break;
				case "SurveyAnswerCollection":
					_surveyAnswerCollection.Add((SurveyAnswerEntity)relatedEntity);
					break;
				case "SurveyAnswerTargetCollection":
					_surveyAnswerTargetCollection.Add((SurveyAnswerEntity)relatedEntity);
					break;
				case "SurveyQuestionCollection":
					_surveyQuestionCollection.Add((SurveyQuestionEntity)relatedEntity);
					break;
				case "SurveyQuestionLanguageCollection":
					_surveyQuestionLanguageCollection.Add((SurveyQuestionLanguageEntity)relatedEntity);
					break;
				case "SurveyResultCollection":
					_surveyResultCollection.Add((SurveyResultEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "SurveyPageEntity":
					DesetupSyncSurveyPageEntity(false, true);
					break;
				case "ParentSurveyQuestionEntity":
					DesetupSyncParentSurveyQuestionEntity(false, true);
					break;
				case "SurveyAnswerCollection":
					this.PerformRelatedEntityRemoval(_surveyAnswerCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "SurveyAnswerTargetCollection":
					this.PerformRelatedEntityRemoval(_surveyAnswerTargetCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "SurveyQuestionCollection":
					this.PerformRelatedEntityRemoval(_surveyQuestionCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "SurveyQuestionLanguageCollection":
					this.PerformRelatedEntityRemoval(_surveyQuestionLanguageCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "SurveyResultCollection":
					this.PerformRelatedEntityRemoval(_surveyResultCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_surveyPageEntity!=null)
			{
				toReturn.Add(_surveyPageEntity);
			}
			if(_parentSurveyQuestionEntity!=null)
			{
				toReturn.Add(_parentSurveyQuestionEntity);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_surveyAnswerCollection);
			toReturn.Add(_surveyAnswerTargetCollection);
			toReturn.Add(_surveyQuestionCollection);
			toReturn.Add(_surveyQuestionLanguageCollection);
			toReturn.Add(_surveyResultCollection);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="surveyQuestionId">PK value for SurveyQuestion which data should be fetched into this SurveyQuestion object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 surveyQuestionId)
		{
			return FetchUsingPK(surveyQuestionId, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="surveyQuestionId">PK value for SurveyQuestion which data should be fetched into this SurveyQuestion object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 surveyQuestionId, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(surveyQuestionId, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="surveyQuestionId">PK value for SurveyQuestion which data should be fetched into this SurveyQuestion object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 surveyQuestionId, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(surveyQuestionId, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="surveyQuestionId">PK value for SurveyQuestion which data should be fetched into this SurveyQuestion object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 surveyQuestionId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(surveyQuestionId, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.SurveyQuestionId, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new SurveyQuestionRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'SurveyAnswerEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'SurveyAnswerEntity'</returns>
		public Obymobi.Data.CollectionClasses.SurveyAnswerCollection GetMultiSurveyAnswerCollection(bool forceFetch)
		{
			return GetMultiSurveyAnswerCollection(forceFetch, _surveyAnswerCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'SurveyAnswerEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'SurveyAnswerEntity'</returns>
		public Obymobi.Data.CollectionClasses.SurveyAnswerCollection GetMultiSurveyAnswerCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiSurveyAnswerCollection(forceFetch, _surveyAnswerCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'SurveyAnswerEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.SurveyAnswerCollection GetMultiSurveyAnswerCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiSurveyAnswerCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'SurveyAnswerEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.SurveyAnswerCollection GetMultiSurveyAnswerCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedSurveyAnswerCollection || forceFetch || _alwaysFetchSurveyAnswerCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_surveyAnswerCollection);
				_surveyAnswerCollection.SuppressClearInGetMulti=!forceFetch;
				_surveyAnswerCollection.EntityFactoryToUse = entityFactoryToUse;
				_surveyAnswerCollection.GetMultiManyToOne(this, null, filter);
				_surveyAnswerCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedSurveyAnswerCollection = true;
			}
			return _surveyAnswerCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'SurveyAnswerCollection'. These settings will be taken into account
		/// when the property SurveyAnswerCollection is requested or GetMultiSurveyAnswerCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersSurveyAnswerCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_surveyAnswerCollection.SortClauses=sortClauses;
			_surveyAnswerCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'SurveyAnswerEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'SurveyAnswerEntity'</returns>
		public Obymobi.Data.CollectionClasses.SurveyAnswerCollection GetMultiSurveyAnswerTargetCollection(bool forceFetch)
		{
			return GetMultiSurveyAnswerTargetCollection(forceFetch, _surveyAnswerTargetCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'SurveyAnswerEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'SurveyAnswerEntity'</returns>
		public Obymobi.Data.CollectionClasses.SurveyAnswerCollection GetMultiSurveyAnswerTargetCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiSurveyAnswerTargetCollection(forceFetch, _surveyAnswerTargetCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'SurveyAnswerEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.SurveyAnswerCollection GetMultiSurveyAnswerTargetCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiSurveyAnswerTargetCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'SurveyAnswerEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.SurveyAnswerCollection GetMultiSurveyAnswerTargetCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedSurveyAnswerTargetCollection || forceFetch || _alwaysFetchSurveyAnswerTargetCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_surveyAnswerTargetCollection);
				_surveyAnswerTargetCollection.SuppressClearInGetMulti=!forceFetch;
				_surveyAnswerTargetCollection.EntityFactoryToUse = entityFactoryToUse;
				_surveyAnswerTargetCollection.GetMultiManyToOne(null, this, filter);
				_surveyAnswerTargetCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedSurveyAnswerTargetCollection = true;
			}
			return _surveyAnswerTargetCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'SurveyAnswerTargetCollection'. These settings will be taken into account
		/// when the property SurveyAnswerTargetCollection is requested or GetMultiSurveyAnswerTargetCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersSurveyAnswerTargetCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_surveyAnswerTargetCollection.SortClauses=sortClauses;
			_surveyAnswerTargetCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'SurveyQuestionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'SurveyQuestionEntity'</returns>
		public Obymobi.Data.CollectionClasses.SurveyQuestionCollection GetMultiSurveyQuestionCollection(bool forceFetch)
		{
			return GetMultiSurveyQuestionCollection(forceFetch, _surveyQuestionCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'SurveyQuestionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'SurveyQuestionEntity'</returns>
		public Obymobi.Data.CollectionClasses.SurveyQuestionCollection GetMultiSurveyQuestionCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiSurveyQuestionCollection(forceFetch, _surveyQuestionCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'SurveyQuestionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.SurveyQuestionCollection GetMultiSurveyQuestionCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiSurveyQuestionCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'SurveyQuestionEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.SurveyQuestionCollection GetMultiSurveyQuestionCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedSurveyQuestionCollection || forceFetch || _alwaysFetchSurveyQuestionCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_surveyQuestionCollection);
				_surveyQuestionCollection.SuppressClearInGetMulti=!forceFetch;
				_surveyQuestionCollection.EntityFactoryToUse = entityFactoryToUse;
				_surveyQuestionCollection.GetMultiManyToOne(null, this, filter);
				_surveyQuestionCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedSurveyQuestionCollection = true;
			}
			return _surveyQuestionCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'SurveyQuestionCollection'. These settings will be taken into account
		/// when the property SurveyQuestionCollection is requested or GetMultiSurveyQuestionCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersSurveyQuestionCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_surveyQuestionCollection.SortClauses=sortClauses;
			_surveyQuestionCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'SurveyQuestionLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'SurveyQuestionLanguageEntity'</returns>
		public Obymobi.Data.CollectionClasses.SurveyQuestionLanguageCollection GetMultiSurveyQuestionLanguageCollection(bool forceFetch)
		{
			return GetMultiSurveyQuestionLanguageCollection(forceFetch, _surveyQuestionLanguageCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'SurveyQuestionLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'SurveyQuestionLanguageEntity'</returns>
		public Obymobi.Data.CollectionClasses.SurveyQuestionLanguageCollection GetMultiSurveyQuestionLanguageCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiSurveyQuestionLanguageCollection(forceFetch, _surveyQuestionLanguageCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'SurveyQuestionLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.SurveyQuestionLanguageCollection GetMultiSurveyQuestionLanguageCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiSurveyQuestionLanguageCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'SurveyQuestionLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.SurveyQuestionLanguageCollection GetMultiSurveyQuestionLanguageCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedSurveyQuestionLanguageCollection || forceFetch || _alwaysFetchSurveyQuestionLanguageCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_surveyQuestionLanguageCollection);
				_surveyQuestionLanguageCollection.SuppressClearInGetMulti=!forceFetch;
				_surveyQuestionLanguageCollection.EntityFactoryToUse = entityFactoryToUse;
				_surveyQuestionLanguageCollection.GetMultiManyToOne(null, this, filter);
				_surveyQuestionLanguageCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedSurveyQuestionLanguageCollection = true;
			}
			return _surveyQuestionLanguageCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'SurveyQuestionLanguageCollection'. These settings will be taken into account
		/// when the property SurveyQuestionLanguageCollection is requested or GetMultiSurveyQuestionLanguageCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersSurveyQuestionLanguageCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_surveyQuestionLanguageCollection.SortClauses=sortClauses;
			_surveyQuestionLanguageCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'SurveyResultEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'SurveyResultEntity'</returns>
		public Obymobi.Data.CollectionClasses.SurveyResultCollection GetMultiSurveyResultCollection(bool forceFetch)
		{
			return GetMultiSurveyResultCollection(forceFetch, _surveyResultCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'SurveyResultEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'SurveyResultEntity'</returns>
		public Obymobi.Data.CollectionClasses.SurveyResultCollection GetMultiSurveyResultCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiSurveyResultCollection(forceFetch, _surveyResultCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'SurveyResultEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.SurveyResultCollection GetMultiSurveyResultCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiSurveyResultCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'SurveyResultEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.SurveyResultCollection GetMultiSurveyResultCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedSurveyResultCollection || forceFetch || _alwaysFetchSurveyResultCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_surveyResultCollection);
				_surveyResultCollection.SuppressClearInGetMulti=!forceFetch;
				_surveyResultCollection.EntityFactoryToUse = entityFactoryToUse;
				_surveyResultCollection.GetMultiManyToOne(null, null, null, this, filter);
				_surveyResultCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedSurveyResultCollection = true;
			}
			return _surveyResultCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'SurveyResultCollection'. These settings will be taken into account
		/// when the property SurveyResultCollection is requested or GetMultiSurveyResultCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersSurveyResultCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_surveyResultCollection.SortClauses=sortClauses;
			_surveyResultCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'SurveyPageEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'SurveyPageEntity'</returns>
		public Obymobi.Data.CollectionClasses.SurveyPageCollection GetMultiSurveyPageCollectionViaSurveyQuestion(bool forceFetch)
		{
			return GetMultiSurveyPageCollectionViaSurveyQuestion(forceFetch, _surveyPageCollectionViaSurveyQuestion.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'SurveyPageEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.SurveyPageCollection GetMultiSurveyPageCollectionViaSurveyQuestion(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedSurveyPageCollectionViaSurveyQuestion || forceFetch || _alwaysFetchSurveyPageCollectionViaSurveyQuestion) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_surveyPageCollectionViaSurveyQuestion);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(SurveyQuestionFields.SurveyQuestionId, ComparisonOperator.Equal, this.SurveyQuestionId, "SurveyQuestionEntity__"));
				_surveyPageCollectionViaSurveyQuestion.SuppressClearInGetMulti=!forceFetch;
				_surveyPageCollectionViaSurveyQuestion.EntityFactoryToUse = entityFactoryToUse;
				_surveyPageCollectionViaSurveyQuestion.GetMulti(filter, GetRelationsForField("SurveyPageCollectionViaSurveyQuestion"));
				_surveyPageCollectionViaSurveyQuestion.SuppressClearInGetMulti=false;
				_alreadyFetchedSurveyPageCollectionViaSurveyQuestion = true;
			}
			return _surveyPageCollectionViaSurveyQuestion;
		}

		/// <summary> Sets the collection parameters for the collection for 'SurveyPageCollectionViaSurveyQuestion'. These settings will be taken into account
		/// when the property SurveyPageCollectionViaSurveyQuestion is requested or GetMultiSurveyPageCollectionViaSurveyQuestion is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersSurveyPageCollectionViaSurveyQuestion(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_surveyPageCollectionViaSurveyQuestion.SortClauses=sortClauses;
			_surveyPageCollectionViaSurveyQuestion.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'SurveyQuestionEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'SurveyQuestionEntity'</returns>
		public Obymobi.Data.CollectionClasses.SurveyQuestionCollection GetMultiSurveyQuestionCollectionViaSurveyAnswer(bool forceFetch)
		{
			return GetMultiSurveyQuestionCollectionViaSurveyAnswer(forceFetch, _surveyQuestionCollectionViaSurveyAnswer.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'SurveyQuestionEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.SurveyQuestionCollection GetMultiSurveyQuestionCollectionViaSurveyAnswer(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedSurveyQuestionCollectionViaSurveyAnswer || forceFetch || _alwaysFetchSurveyQuestionCollectionViaSurveyAnswer) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_surveyQuestionCollectionViaSurveyAnswer);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(SurveyQuestionFields.SurveyQuestionId, ComparisonOperator.Equal, this.SurveyQuestionId, "SurveyQuestionEntity__"));
				_surveyQuestionCollectionViaSurveyAnswer.SuppressClearInGetMulti=!forceFetch;
				_surveyQuestionCollectionViaSurveyAnswer.EntityFactoryToUse = entityFactoryToUse;
				_surveyQuestionCollectionViaSurveyAnswer.GetMulti(filter, GetRelationsForField("SurveyQuestionCollectionViaSurveyAnswer"));
				_surveyQuestionCollectionViaSurveyAnswer.SuppressClearInGetMulti=false;
				_alreadyFetchedSurveyQuestionCollectionViaSurveyAnswer = true;
			}
			return _surveyQuestionCollectionViaSurveyAnswer;
		}

		/// <summary> Sets the collection parameters for the collection for 'SurveyQuestionCollectionViaSurveyAnswer'. These settings will be taken into account
		/// when the property SurveyQuestionCollectionViaSurveyAnswer is requested or GetMultiSurveyQuestionCollectionViaSurveyAnswer is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersSurveyQuestionCollectionViaSurveyAnswer(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_surveyQuestionCollectionViaSurveyAnswer.SortClauses=sortClauses;
			_surveyQuestionCollectionViaSurveyAnswer.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'SurveyQuestionEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'SurveyQuestionEntity'</returns>
		public Obymobi.Data.CollectionClasses.SurveyQuestionCollection GetMultiSurveyQuestionCollectionViaSurveyAnswer_(bool forceFetch)
		{
			return GetMultiSurveyQuestionCollectionViaSurveyAnswer_(forceFetch, _surveyQuestionCollectionViaSurveyAnswer_.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'SurveyQuestionEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.SurveyQuestionCollection GetMultiSurveyQuestionCollectionViaSurveyAnswer_(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedSurveyQuestionCollectionViaSurveyAnswer_ || forceFetch || _alwaysFetchSurveyQuestionCollectionViaSurveyAnswer_) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_surveyQuestionCollectionViaSurveyAnswer_);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(SurveyQuestionFields.SurveyQuestionId, ComparisonOperator.Equal, this.SurveyQuestionId, "SurveyQuestionEntity__"));
				_surveyQuestionCollectionViaSurveyAnswer_.SuppressClearInGetMulti=!forceFetch;
				_surveyQuestionCollectionViaSurveyAnswer_.EntityFactoryToUse = entityFactoryToUse;
				_surveyQuestionCollectionViaSurveyAnswer_.GetMulti(filter, GetRelationsForField("SurveyQuestionCollectionViaSurveyAnswer_"));
				_surveyQuestionCollectionViaSurveyAnswer_.SuppressClearInGetMulti=false;
				_alreadyFetchedSurveyQuestionCollectionViaSurveyAnswer_ = true;
			}
			return _surveyQuestionCollectionViaSurveyAnswer_;
		}

		/// <summary> Sets the collection parameters for the collection for 'SurveyQuestionCollectionViaSurveyAnswer_'. These settings will be taken into account
		/// when the property SurveyQuestionCollectionViaSurveyAnswer_ is requested or GetMultiSurveyQuestionCollectionViaSurveyAnswer_ is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersSurveyQuestionCollectionViaSurveyAnswer_(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_surveyQuestionCollectionViaSurveyAnswer_.SortClauses=sortClauses;
			_surveyQuestionCollectionViaSurveyAnswer_.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves the related entity of type 'SurveyPageEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'SurveyPageEntity' which is related to this entity.</returns>
		public SurveyPageEntity GetSingleSurveyPageEntity()
		{
			return GetSingleSurveyPageEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'SurveyPageEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'SurveyPageEntity' which is related to this entity.</returns>
		public virtual SurveyPageEntity GetSingleSurveyPageEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedSurveyPageEntity || forceFetch || _alwaysFetchSurveyPageEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.SurveyPageEntityUsingSurveyPageId);
				SurveyPageEntity newEntity = new SurveyPageEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.SurveyPageId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (SurveyPageEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_surveyPageEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.SurveyPageEntity = newEntity;
				_alreadyFetchedSurveyPageEntity = fetchResult;
			}
			return _surveyPageEntity;
		}


		/// <summary> Retrieves the related entity of type 'SurveyQuestionEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'SurveyQuestionEntity' which is related to this entity.</returns>
		public SurveyQuestionEntity GetSingleParentSurveyQuestionEntity()
		{
			return GetSingleParentSurveyQuestionEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'SurveyQuestionEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'SurveyQuestionEntity' which is related to this entity.</returns>
		public virtual SurveyQuestionEntity GetSingleParentSurveyQuestionEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedParentSurveyQuestionEntity || forceFetch || _alwaysFetchParentSurveyQuestionEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.SurveyQuestionEntityUsingSurveyQuestionIdParentQuestionId);
				SurveyQuestionEntity newEntity = new SurveyQuestionEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ParentQuestionId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (SurveyQuestionEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_parentSurveyQuestionEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.ParentSurveyQuestionEntity = newEntity;
				_alreadyFetchedParentSurveyQuestionEntity = fetchResult;
			}
			return _parentSurveyQuestionEntity;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("SurveyPageEntity", _surveyPageEntity);
			toReturn.Add("ParentSurveyQuestionEntity", _parentSurveyQuestionEntity);
			toReturn.Add("SurveyAnswerCollection", _surveyAnswerCollection);
			toReturn.Add("SurveyAnswerTargetCollection", _surveyAnswerTargetCollection);
			toReturn.Add("SurveyQuestionCollection", _surveyQuestionCollection);
			toReturn.Add("SurveyQuestionLanguageCollection", _surveyQuestionLanguageCollection);
			toReturn.Add("SurveyResultCollection", _surveyResultCollection);
			toReturn.Add("SurveyPageCollectionViaSurveyQuestion", _surveyPageCollectionViaSurveyQuestion);
			toReturn.Add("SurveyQuestionCollectionViaSurveyAnswer", _surveyQuestionCollectionViaSurveyAnswer);
			toReturn.Add("SurveyQuestionCollectionViaSurveyAnswer_", _surveyQuestionCollectionViaSurveyAnswer_);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="surveyQuestionId">PK value for SurveyQuestion which data should be fetched into this SurveyQuestion object</param>
		/// <param name="validator">The validator object for this SurveyQuestionEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int32 surveyQuestionId, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(surveyQuestionId, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_surveyAnswerCollection = new Obymobi.Data.CollectionClasses.SurveyAnswerCollection();
			_surveyAnswerCollection.SetContainingEntityInfo(this, "SurveyQuestionEntity");

			_surveyAnswerTargetCollection = new Obymobi.Data.CollectionClasses.SurveyAnswerCollection();
			_surveyAnswerTargetCollection.SetContainingEntityInfo(this, "TargetSurveyQuestionEntity");

			_surveyQuestionCollection = new Obymobi.Data.CollectionClasses.SurveyQuestionCollection();
			_surveyQuestionCollection.SetContainingEntityInfo(this, "ParentSurveyQuestionEntity");

			_surveyQuestionLanguageCollection = new Obymobi.Data.CollectionClasses.SurveyQuestionLanguageCollection();
			_surveyQuestionLanguageCollection.SetContainingEntityInfo(this, "SurveyQuestionEntity");

			_surveyResultCollection = new Obymobi.Data.CollectionClasses.SurveyResultCollection();
			_surveyResultCollection.SetContainingEntityInfo(this, "SurveyQuestionEntity");
			_surveyPageCollectionViaSurveyQuestion = new Obymobi.Data.CollectionClasses.SurveyPageCollection();
			_surveyQuestionCollectionViaSurveyAnswer = new Obymobi.Data.CollectionClasses.SurveyQuestionCollection();
			_surveyQuestionCollectionViaSurveyAnswer_ = new Obymobi.Data.CollectionClasses.SurveyQuestionCollection();
			_surveyPageEntityReturnsNewIfNotFound = true;
			_parentSurveyQuestionEntityReturnsNewIfNotFound = true;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SurveyQuestionId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Question", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Type", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Required", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SortOrder", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FieldValue1", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FieldValue10", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FieldValue2", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FieldValue3", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FieldValue4", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FieldValue5", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FieldValue6", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FieldValue7", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FieldValue8", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FieldValue9", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ParentQuestionId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SurveyPageId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("NotesEnabled", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ParentCompanyId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedUTC", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _surveyPageEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncSurveyPageEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _surveyPageEntity, new PropertyChangedEventHandler( OnSurveyPageEntityPropertyChanged ), "SurveyPageEntity", Obymobi.Data.RelationClasses.StaticSurveyQuestionRelations.SurveyPageEntityUsingSurveyPageIdStatic, true, signalRelatedEntity, "SurveyQuestionCollection", resetFKFields, new int[] { (int)SurveyQuestionFieldIndex.SurveyPageId } );		
			_surveyPageEntity = null;
		}
		
		/// <summary> setups the sync logic for member _surveyPageEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncSurveyPageEntity(IEntityCore relatedEntity)
		{
			if(_surveyPageEntity!=relatedEntity)
			{		
				DesetupSyncSurveyPageEntity(true, true);
				_surveyPageEntity = (SurveyPageEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _surveyPageEntity, new PropertyChangedEventHandler( OnSurveyPageEntityPropertyChanged ), "SurveyPageEntity", Obymobi.Data.RelationClasses.StaticSurveyQuestionRelations.SurveyPageEntityUsingSurveyPageIdStatic, true, ref _alreadyFetchedSurveyPageEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnSurveyPageEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _parentSurveyQuestionEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncParentSurveyQuestionEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _parentSurveyQuestionEntity, new PropertyChangedEventHandler( OnParentSurveyQuestionEntityPropertyChanged ), "ParentSurveyQuestionEntity", Obymobi.Data.RelationClasses.StaticSurveyQuestionRelations.SurveyQuestionEntityUsingSurveyQuestionIdParentQuestionIdStatic, true, signalRelatedEntity, "SurveyQuestionCollection", resetFKFields, new int[] { (int)SurveyQuestionFieldIndex.ParentQuestionId } );		
			_parentSurveyQuestionEntity = null;
		}
		
		/// <summary> setups the sync logic for member _parentSurveyQuestionEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncParentSurveyQuestionEntity(IEntityCore relatedEntity)
		{
			if(_parentSurveyQuestionEntity!=relatedEntity)
			{		
				DesetupSyncParentSurveyQuestionEntity(true, true);
				_parentSurveyQuestionEntity = (SurveyQuestionEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _parentSurveyQuestionEntity, new PropertyChangedEventHandler( OnParentSurveyQuestionEntityPropertyChanged ), "ParentSurveyQuestionEntity", Obymobi.Data.RelationClasses.StaticSurveyQuestionRelations.SurveyQuestionEntityUsingSurveyQuestionIdParentQuestionIdStatic, true, ref _alreadyFetchedParentSurveyQuestionEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnParentSurveyQuestionEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="surveyQuestionId">PK value for SurveyQuestion which data should be fetched into this SurveyQuestion object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int32 surveyQuestionId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)SurveyQuestionFieldIndex.SurveyQuestionId].ForcedCurrentValueWrite(surveyQuestionId);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateSurveyQuestionDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new SurveyQuestionEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static SurveyQuestionRelations Relations
		{
			get	{ return new SurveyQuestionRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'SurveyAnswer' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathSurveyAnswerCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.SurveyAnswerCollection(), (IEntityRelation)GetRelationsForField("SurveyAnswerCollection")[0], (int)Obymobi.Data.EntityType.SurveyQuestionEntity, (int)Obymobi.Data.EntityType.SurveyAnswerEntity, 0, null, null, null, "SurveyAnswerCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'SurveyAnswer' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathSurveyAnswerTargetCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.SurveyAnswerCollection(), (IEntityRelation)GetRelationsForField("SurveyAnswerTargetCollection")[0], (int)Obymobi.Data.EntityType.SurveyQuestionEntity, (int)Obymobi.Data.EntityType.SurveyAnswerEntity, 0, null, null, null, "SurveyAnswerTargetCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'SurveyQuestion' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathSurveyQuestionCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.SurveyQuestionCollection(), (IEntityRelation)GetRelationsForField("SurveyQuestionCollection")[0], (int)Obymobi.Data.EntityType.SurveyQuestionEntity, (int)Obymobi.Data.EntityType.SurveyQuestionEntity, 0, null, null, null, "SurveyQuestionCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'SurveyQuestionLanguage' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathSurveyQuestionLanguageCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.SurveyQuestionLanguageCollection(), (IEntityRelation)GetRelationsForField("SurveyQuestionLanguageCollection")[0], (int)Obymobi.Data.EntityType.SurveyQuestionEntity, (int)Obymobi.Data.EntityType.SurveyQuestionLanguageEntity, 0, null, null, null, "SurveyQuestionLanguageCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'SurveyResult' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathSurveyResultCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.SurveyResultCollection(), (IEntityRelation)GetRelationsForField("SurveyResultCollection")[0], (int)Obymobi.Data.EntityType.SurveyQuestionEntity, (int)Obymobi.Data.EntityType.SurveyResultEntity, 0, null, null, null, "SurveyResultCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'SurveyPage'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathSurveyPageCollectionViaSurveyQuestion
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.SurveyQuestionEntityUsingParentQuestionId;
				intermediateRelation.SetAliases(string.Empty, "SurveyQuestion_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.SurveyPageCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.SurveyQuestionEntity, (int)Obymobi.Data.EntityType.SurveyPageEntity, 0, null, null, GetRelationsForField("SurveyPageCollectionViaSurveyQuestion"), "SurveyPageCollectionViaSurveyQuestion", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'SurveyQuestion'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathSurveyQuestionCollectionViaSurveyAnswer
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.SurveyAnswerEntityUsingSurveyQuestionId;
				intermediateRelation.SetAliases(string.Empty, "SurveyAnswer_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.SurveyQuestionCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.SurveyQuestionEntity, (int)Obymobi.Data.EntityType.SurveyQuestionEntity, 0, null, null, GetRelationsForField("SurveyQuestionCollectionViaSurveyAnswer"), "SurveyQuestionCollectionViaSurveyAnswer", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'SurveyQuestion'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathSurveyQuestionCollectionViaSurveyAnswer_
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.SurveyAnswerEntityUsingSurveyQuestionId;
				intermediateRelation.SetAliases(string.Empty, "SurveyAnswer_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.SurveyQuestionCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.SurveyQuestionEntity, (int)Obymobi.Data.EntityType.SurveyQuestionEntity, 0, null, null, GetRelationsForField("SurveyQuestionCollectionViaSurveyAnswer_"), "SurveyQuestionCollectionViaSurveyAnswer_", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'SurveyPage'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathSurveyPageEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.SurveyPageCollection(), (IEntityRelation)GetRelationsForField("SurveyPageEntity")[0], (int)Obymobi.Data.EntityType.SurveyQuestionEntity, (int)Obymobi.Data.EntityType.SurveyPageEntity, 0, null, null, null, "SurveyPageEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'SurveyQuestion'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathParentSurveyQuestionEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.SurveyQuestionCollection(), (IEntityRelation)GetRelationsForField("ParentSurveyQuestionEntity")[0], (int)Obymobi.Data.EntityType.SurveyQuestionEntity, (int)Obymobi.Data.EntityType.SurveyQuestionEntity, 0, null, null, null, "ParentSurveyQuestionEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The SurveyQuestionId property of the Entity SurveyQuestion<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SurveyQuestion"."SurveyQuestionId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 SurveyQuestionId
		{
			get { return (System.Int32)GetValue((int)SurveyQuestionFieldIndex.SurveyQuestionId, true); }
			set	{ SetValue((int)SurveyQuestionFieldIndex.SurveyQuestionId, value, true); }
		}

		/// <summary> The Question property of the Entity SurveyQuestion<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SurveyQuestion"."Question"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 255<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Question
		{
			get { return (System.String)GetValue((int)SurveyQuestionFieldIndex.Question, true); }
			set	{ SetValue((int)SurveyQuestionFieldIndex.Question, value, true); }
		}

		/// <summary> The Type property of the Entity SurveyQuestion<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SurveyQuestion"."Type"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 Type
		{
			get { return (System.Int32)GetValue((int)SurveyQuestionFieldIndex.Type, true); }
			set	{ SetValue((int)SurveyQuestionFieldIndex.Type, value, true); }
		}

		/// <summary> The CreatedBy property of the Entity SurveyQuestion<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SurveyQuestion"."CreatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CreatedBy
		{
			get { return (System.Int32)GetValue((int)SurveyQuestionFieldIndex.CreatedBy, true); }
			set	{ SetValue((int)SurveyQuestionFieldIndex.CreatedBy, value, true); }
		}

		/// <summary> The UpdatedBy property of the Entity SurveyQuestion<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SurveyQuestion"."UpdatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 UpdatedBy
		{
			get { return (System.Int32)GetValue((int)SurveyQuestionFieldIndex.UpdatedBy, true); }
			set	{ SetValue((int)SurveyQuestionFieldIndex.UpdatedBy, value, true); }
		}

		/// <summary> The Required property of the Entity SurveyQuestion<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SurveyQuestion"."Required"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean Required
		{
			get { return (System.Boolean)GetValue((int)SurveyQuestionFieldIndex.Required, true); }
			set	{ SetValue((int)SurveyQuestionFieldIndex.Required, value, true); }
		}

		/// <summary> The SortOrder property of the Entity SurveyQuestion<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SurveyQuestion"."SortOrder"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 SortOrder
		{
			get { return (System.Int32)GetValue((int)SurveyQuestionFieldIndex.SortOrder, true); }
			set	{ SetValue((int)SurveyQuestionFieldIndex.SortOrder, value, true); }
		}

		/// <summary> The FieldValue1 property of the Entity SurveyQuestion<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SurveyQuestion"."FieldValue1"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FieldValue1
		{
			get { return (System.String)GetValue((int)SurveyQuestionFieldIndex.FieldValue1, true); }
			set	{ SetValue((int)SurveyQuestionFieldIndex.FieldValue1, value, true); }
		}

		/// <summary> The FieldValue10 property of the Entity SurveyQuestion<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SurveyQuestion"."FieldValue10"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FieldValue10
		{
			get { return (System.String)GetValue((int)SurveyQuestionFieldIndex.FieldValue10, true); }
			set	{ SetValue((int)SurveyQuestionFieldIndex.FieldValue10, value, true); }
		}

		/// <summary> The FieldValue2 property of the Entity SurveyQuestion<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SurveyQuestion"."FieldValue2"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FieldValue2
		{
			get { return (System.String)GetValue((int)SurveyQuestionFieldIndex.FieldValue2, true); }
			set	{ SetValue((int)SurveyQuestionFieldIndex.FieldValue2, value, true); }
		}

		/// <summary> The FieldValue3 property of the Entity SurveyQuestion<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SurveyQuestion"."FieldValue3"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FieldValue3
		{
			get { return (System.String)GetValue((int)SurveyQuestionFieldIndex.FieldValue3, true); }
			set	{ SetValue((int)SurveyQuestionFieldIndex.FieldValue3, value, true); }
		}

		/// <summary> The FieldValue4 property of the Entity SurveyQuestion<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SurveyQuestion"."FieldValue4"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FieldValue4
		{
			get { return (System.String)GetValue((int)SurveyQuestionFieldIndex.FieldValue4, true); }
			set	{ SetValue((int)SurveyQuestionFieldIndex.FieldValue4, value, true); }
		}

		/// <summary> The FieldValue5 property of the Entity SurveyQuestion<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SurveyQuestion"."FieldValue5"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FieldValue5
		{
			get { return (System.String)GetValue((int)SurveyQuestionFieldIndex.FieldValue5, true); }
			set	{ SetValue((int)SurveyQuestionFieldIndex.FieldValue5, value, true); }
		}

		/// <summary> The FieldValue6 property of the Entity SurveyQuestion<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SurveyQuestion"."FieldValue6"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FieldValue6
		{
			get { return (System.String)GetValue((int)SurveyQuestionFieldIndex.FieldValue6, true); }
			set	{ SetValue((int)SurveyQuestionFieldIndex.FieldValue6, value, true); }
		}

		/// <summary> The FieldValue7 property of the Entity SurveyQuestion<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SurveyQuestion"."FieldValue7"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FieldValue7
		{
			get { return (System.String)GetValue((int)SurveyQuestionFieldIndex.FieldValue7, true); }
			set	{ SetValue((int)SurveyQuestionFieldIndex.FieldValue7, value, true); }
		}

		/// <summary> The FieldValue8 property of the Entity SurveyQuestion<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SurveyQuestion"."FieldValue8"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FieldValue8
		{
			get { return (System.String)GetValue((int)SurveyQuestionFieldIndex.FieldValue8, true); }
			set	{ SetValue((int)SurveyQuestionFieldIndex.FieldValue8, value, true); }
		}

		/// <summary> The FieldValue9 property of the Entity SurveyQuestion<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SurveyQuestion"."FieldValue9"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FieldValue9
		{
			get { return (System.String)GetValue((int)SurveyQuestionFieldIndex.FieldValue9, true); }
			set	{ SetValue((int)SurveyQuestionFieldIndex.FieldValue9, value, true); }
		}

		/// <summary> The ParentQuestionId property of the Entity SurveyQuestion<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SurveyQuestion"."ParentSurveyQuestionId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ParentQuestionId
		{
			get { return (Nullable<System.Int32>)GetValue((int)SurveyQuestionFieldIndex.ParentQuestionId, false); }
			set	{ SetValue((int)SurveyQuestionFieldIndex.ParentQuestionId, value, true); }
		}

		/// <summary> The SurveyPageId property of the Entity SurveyQuestion<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SurveyQuestion"."SurveyPageId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> SurveyPageId
		{
			get { return (Nullable<System.Int32>)GetValue((int)SurveyQuestionFieldIndex.SurveyPageId, false); }
			set	{ SetValue((int)SurveyQuestionFieldIndex.SurveyPageId, value, true); }
		}

		/// <summary> The NotesEnabled property of the Entity SurveyQuestion<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SurveyQuestion"."NotesEnabled"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean NotesEnabled
		{
			get { return (System.Boolean)GetValue((int)SurveyQuestionFieldIndex.NotesEnabled, true); }
			set	{ SetValue((int)SurveyQuestionFieldIndex.NotesEnabled, value, true); }
		}

		/// <summary> The ParentCompanyId property of the Entity SurveyQuestion<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SurveyQuestion"."ParentCompanyId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ParentCompanyId
		{
			get { return (Nullable<System.Int32>)GetValue((int)SurveyQuestionFieldIndex.ParentCompanyId, false); }
			set	{ SetValue((int)SurveyQuestionFieldIndex.ParentCompanyId, value, true); }
		}

		/// <summary> The CreatedUTC property of the Entity SurveyQuestion<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SurveyQuestion"."CreatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CreatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)SurveyQuestionFieldIndex.CreatedUTC, false); }
			set	{ SetValue((int)SurveyQuestionFieldIndex.CreatedUTC, value, true); }
		}

		/// <summary> The UpdatedUTC property of the Entity SurveyQuestion<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SurveyQuestion"."UpdatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)SurveyQuestionFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)SurveyQuestionFieldIndex.UpdatedUTC, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'SurveyAnswerEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiSurveyAnswerCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.SurveyAnswerCollection SurveyAnswerCollection
		{
			get	{ return GetMultiSurveyAnswerCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for SurveyAnswerCollection. When set to true, SurveyAnswerCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time SurveyAnswerCollection is accessed. You can always execute/ a forced fetch by calling GetMultiSurveyAnswerCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchSurveyAnswerCollection
		{
			get	{ return _alwaysFetchSurveyAnswerCollection; }
			set	{ _alwaysFetchSurveyAnswerCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property SurveyAnswerCollection already has been fetched. Setting this property to false when SurveyAnswerCollection has been fetched
		/// will clear the SurveyAnswerCollection collection well. Setting this property to true while SurveyAnswerCollection hasn't been fetched disables lazy loading for SurveyAnswerCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedSurveyAnswerCollection
		{
			get { return _alreadyFetchedSurveyAnswerCollection;}
			set 
			{
				if(_alreadyFetchedSurveyAnswerCollection && !value && (_surveyAnswerCollection != null))
				{
					_surveyAnswerCollection.Clear();
				}
				_alreadyFetchedSurveyAnswerCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'SurveyAnswerEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiSurveyAnswerTargetCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.SurveyAnswerCollection SurveyAnswerTargetCollection
		{
			get	{ return GetMultiSurveyAnswerTargetCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for SurveyAnswerTargetCollection. When set to true, SurveyAnswerTargetCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time SurveyAnswerTargetCollection is accessed. You can always execute/ a forced fetch by calling GetMultiSurveyAnswerTargetCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchSurveyAnswerTargetCollection
		{
			get	{ return _alwaysFetchSurveyAnswerTargetCollection; }
			set	{ _alwaysFetchSurveyAnswerTargetCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property SurveyAnswerTargetCollection already has been fetched. Setting this property to false when SurveyAnswerTargetCollection has been fetched
		/// will clear the SurveyAnswerTargetCollection collection well. Setting this property to true while SurveyAnswerTargetCollection hasn't been fetched disables lazy loading for SurveyAnswerTargetCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedSurveyAnswerTargetCollection
		{
			get { return _alreadyFetchedSurveyAnswerTargetCollection;}
			set 
			{
				if(_alreadyFetchedSurveyAnswerTargetCollection && !value && (_surveyAnswerTargetCollection != null))
				{
					_surveyAnswerTargetCollection.Clear();
				}
				_alreadyFetchedSurveyAnswerTargetCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'SurveyQuestionEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiSurveyQuestionCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.SurveyQuestionCollection SurveyQuestionCollection
		{
			get	{ return GetMultiSurveyQuestionCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for SurveyQuestionCollection. When set to true, SurveyQuestionCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time SurveyQuestionCollection is accessed. You can always execute/ a forced fetch by calling GetMultiSurveyQuestionCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchSurveyQuestionCollection
		{
			get	{ return _alwaysFetchSurveyQuestionCollection; }
			set	{ _alwaysFetchSurveyQuestionCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property SurveyQuestionCollection already has been fetched. Setting this property to false when SurveyQuestionCollection has been fetched
		/// will clear the SurveyQuestionCollection collection well. Setting this property to true while SurveyQuestionCollection hasn't been fetched disables lazy loading for SurveyQuestionCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedSurveyQuestionCollection
		{
			get { return _alreadyFetchedSurveyQuestionCollection;}
			set 
			{
				if(_alreadyFetchedSurveyQuestionCollection && !value && (_surveyQuestionCollection != null))
				{
					_surveyQuestionCollection.Clear();
				}
				_alreadyFetchedSurveyQuestionCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'SurveyQuestionLanguageEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiSurveyQuestionLanguageCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.SurveyQuestionLanguageCollection SurveyQuestionLanguageCollection
		{
			get	{ return GetMultiSurveyQuestionLanguageCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for SurveyQuestionLanguageCollection. When set to true, SurveyQuestionLanguageCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time SurveyQuestionLanguageCollection is accessed. You can always execute/ a forced fetch by calling GetMultiSurveyQuestionLanguageCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchSurveyQuestionLanguageCollection
		{
			get	{ return _alwaysFetchSurveyQuestionLanguageCollection; }
			set	{ _alwaysFetchSurveyQuestionLanguageCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property SurveyQuestionLanguageCollection already has been fetched. Setting this property to false when SurveyQuestionLanguageCollection has been fetched
		/// will clear the SurveyQuestionLanguageCollection collection well. Setting this property to true while SurveyQuestionLanguageCollection hasn't been fetched disables lazy loading for SurveyQuestionLanguageCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedSurveyQuestionLanguageCollection
		{
			get { return _alreadyFetchedSurveyQuestionLanguageCollection;}
			set 
			{
				if(_alreadyFetchedSurveyQuestionLanguageCollection && !value && (_surveyQuestionLanguageCollection != null))
				{
					_surveyQuestionLanguageCollection.Clear();
				}
				_alreadyFetchedSurveyQuestionLanguageCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'SurveyResultEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiSurveyResultCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.SurveyResultCollection SurveyResultCollection
		{
			get	{ return GetMultiSurveyResultCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for SurveyResultCollection. When set to true, SurveyResultCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time SurveyResultCollection is accessed. You can always execute/ a forced fetch by calling GetMultiSurveyResultCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchSurveyResultCollection
		{
			get	{ return _alwaysFetchSurveyResultCollection; }
			set	{ _alwaysFetchSurveyResultCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property SurveyResultCollection already has been fetched. Setting this property to false when SurveyResultCollection has been fetched
		/// will clear the SurveyResultCollection collection well. Setting this property to true while SurveyResultCollection hasn't been fetched disables lazy loading for SurveyResultCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedSurveyResultCollection
		{
			get { return _alreadyFetchedSurveyResultCollection;}
			set 
			{
				if(_alreadyFetchedSurveyResultCollection && !value && (_surveyResultCollection != null))
				{
					_surveyResultCollection.Clear();
				}
				_alreadyFetchedSurveyResultCollection = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'SurveyPageEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiSurveyPageCollectionViaSurveyQuestion()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.SurveyPageCollection SurveyPageCollectionViaSurveyQuestion
		{
			get { return GetMultiSurveyPageCollectionViaSurveyQuestion(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for SurveyPageCollectionViaSurveyQuestion. When set to true, SurveyPageCollectionViaSurveyQuestion is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time SurveyPageCollectionViaSurveyQuestion is accessed. You can always execute a forced fetch by calling GetMultiSurveyPageCollectionViaSurveyQuestion(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchSurveyPageCollectionViaSurveyQuestion
		{
			get	{ return _alwaysFetchSurveyPageCollectionViaSurveyQuestion; }
			set	{ _alwaysFetchSurveyPageCollectionViaSurveyQuestion = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property SurveyPageCollectionViaSurveyQuestion already has been fetched. Setting this property to false when SurveyPageCollectionViaSurveyQuestion has been fetched
		/// will clear the SurveyPageCollectionViaSurveyQuestion collection well. Setting this property to true while SurveyPageCollectionViaSurveyQuestion hasn't been fetched disables lazy loading for SurveyPageCollectionViaSurveyQuestion</summary>
		[Browsable(false)]
		public bool AlreadyFetchedSurveyPageCollectionViaSurveyQuestion
		{
			get { return _alreadyFetchedSurveyPageCollectionViaSurveyQuestion;}
			set 
			{
				if(_alreadyFetchedSurveyPageCollectionViaSurveyQuestion && !value && (_surveyPageCollectionViaSurveyQuestion != null))
				{
					_surveyPageCollectionViaSurveyQuestion.Clear();
				}
				_alreadyFetchedSurveyPageCollectionViaSurveyQuestion = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'SurveyQuestionEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiSurveyQuestionCollectionViaSurveyAnswer()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.SurveyQuestionCollection SurveyQuestionCollectionViaSurveyAnswer
		{
			get { return GetMultiSurveyQuestionCollectionViaSurveyAnswer(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for SurveyQuestionCollectionViaSurveyAnswer. When set to true, SurveyQuestionCollectionViaSurveyAnswer is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time SurveyQuestionCollectionViaSurveyAnswer is accessed. You can always execute a forced fetch by calling GetMultiSurveyQuestionCollectionViaSurveyAnswer(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchSurveyQuestionCollectionViaSurveyAnswer
		{
			get	{ return _alwaysFetchSurveyQuestionCollectionViaSurveyAnswer; }
			set	{ _alwaysFetchSurveyQuestionCollectionViaSurveyAnswer = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property SurveyQuestionCollectionViaSurveyAnswer already has been fetched. Setting this property to false when SurveyQuestionCollectionViaSurveyAnswer has been fetched
		/// will clear the SurveyQuestionCollectionViaSurveyAnswer collection well. Setting this property to true while SurveyQuestionCollectionViaSurveyAnswer hasn't been fetched disables lazy loading for SurveyQuestionCollectionViaSurveyAnswer</summary>
		[Browsable(false)]
		public bool AlreadyFetchedSurveyQuestionCollectionViaSurveyAnswer
		{
			get { return _alreadyFetchedSurveyQuestionCollectionViaSurveyAnswer;}
			set 
			{
				if(_alreadyFetchedSurveyQuestionCollectionViaSurveyAnswer && !value && (_surveyQuestionCollectionViaSurveyAnswer != null))
				{
					_surveyQuestionCollectionViaSurveyAnswer.Clear();
				}
				_alreadyFetchedSurveyQuestionCollectionViaSurveyAnswer = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'SurveyQuestionEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiSurveyQuestionCollectionViaSurveyAnswer_()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.SurveyQuestionCollection SurveyQuestionCollectionViaSurveyAnswer_
		{
			get { return GetMultiSurveyQuestionCollectionViaSurveyAnswer_(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for SurveyQuestionCollectionViaSurveyAnswer_. When set to true, SurveyQuestionCollectionViaSurveyAnswer_ is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time SurveyQuestionCollectionViaSurveyAnswer_ is accessed. You can always execute a forced fetch by calling GetMultiSurveyQuestionCollectionViaSurveyAnswer_(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchSurveyQuestionCollectionViaSurveyAnswer_
		{
			get	{ return _alwaysFetchSurveyQuestionCollectionViaSurveyAnswer_; }
			set	{ _alwaysFetchSurveyQuestionCollectionViaSurveyAnswer_ = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property SurveyQuestionCollectionViaSurveyAnswer_ already has been fetched. Setting this property to false when SurveyQuestionCollectionViaSurveyAnswer_ has been fetched
		/// will clear the SurveyQuestionCollectionViaSurveyAnswer_ collection well. Setting this property to true while SurveyQuestionCollectionViaSurveyAnswer_ hasn't been fetched disables lazy loading for SurveyQuestionCollectionViaSurveyAnswer_</summary>
		[Browsable(false)]
		public bool AlreadyFetchedSurveyQuestionCollectionViaSurveyAnswer_
		{
			get { return _alreadyFetchedSurveyQuestionCollectionViaSurveyAnswer_;}
			set 
			{
				if(_alreadyFetchedSurveyQuestionCollectionViaSurveyAnswer_ && !value && (_surveyQuestionCollectionViaSurveyAnswer_ != null))
				{
					_surveyQuestionCollectionViaSurveyAnswer_.Clear();
				}
				_alreadyFetchedSurveyQuestionCollectionViaSurveyAnswer_ = value;
			}
		}

		/// <summary> Gets / sets related entity of type 'SurveyPageEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleSurveyPageEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual SurveyPageEntity SurveyPageEntity
		{
			get	{ return GetSingleSurveyPageEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncSurveyPageEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "SurveyQuestionCollection", "SurveyPageEntity", _surveyPageEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for SurveyPageEntity. When set to true, SurveyPageEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time SurveyPageEntity is accessed. You can always execute a forced fetch by calling GetSingleSurveyPageEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchSurveyPageEntity
		{
			get	{ return _alwaysFetchSurveyPageEntity; }
			set	{ _alwaysFetchSurveyPageEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property SurveyPageEntity already has been fetched. Setting this property to false when SurveyPageEntity has been fetched
		/// will set SurveyPageEntity to null as well. Setting this property to true while SurveyPageEntity hasn't been fetched disables lazy loading for SurveyPageEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedSurveyPageEntity
		{
			get { return _alreadyFetchedSurveyPageEntity;}
			set 
			{
				if(_alreadyFetchedSurveyPageEntity && !value)
				{
					this.SurveyPageEntity = null;
				}
				_alreadyFetchedSurveyPageEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property SurveyPageEntity is not found
		/// in the database. When set to true, SurveyPageEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool SurveyPageEntityReturnsNewIfNotFound
		{
			get	{ return _surveyPageEntityReturnsNewIfNotFound; }
			set { _surveyPageEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'SurveyQuestionEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleParentSurveyQuestionEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual SurveyQuestionEntity ParentSurveyQuestionEntity
		{
			get	{ return GetSingleParentSurveyQuestionEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncParentSurveyQuestionEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "SurveyQuestionCollection", "ParentSurveyQuestionEntity", _parentSurveyQuestionEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for ParentSurveyQuestionEntity. When set to true, ParentSurveyQuestionEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ParentSurveyQuestionEntity is accessed. You can always execute a forced fetch by calling GetSingleParentSurveyQuestionEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchParentSurveyQuestionEntity
		{
			get	{ return _alwaysFetchParentSurveyQuestionEntity; }
			set	{ _alwaysFetchParentSurveyQuestionEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ParentSurveyQuestionEntity already has been fetched. Setting this property to false when ParentSurveyQuestionEntity has been fetched
		/// will set ParentSurveyQuestionEntity to null as well. Setting this property to true while ParentSurveyQuestionEntity hasn't been fetched disables lazy loading for ParentSurveyQuestionEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedParentSurveyQuestionEntity
		{
			get { return _alreadyFetchedParentSurveyQuestionEntity;}
			set 
			{
				if(_alreadyFetchedParentSurveyQuestionEntity && !value)
				{
					this.ParentSurveyQuestionEntity = null;
				}
				_alreadyFetchedParentSurveyQuestionEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property ParentSurveyQuestionEntity is not found
		/// in the database. When set to true, ParentSurveyQuestionEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool ParentSurveyQuestionEntityReturnsNewIfNotFound
		{
			get	{ return _parentSurveyQuestionEntityReturnsNewIfNotFound; }
			set { _parentSurveyQuestionEntityReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the Obymobi.Data.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)Obymobi.Data.EntityType.SurveyQuestionEntity; }
		}

		#endregion

		
		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
