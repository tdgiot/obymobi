﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
using System.Runtime.Serialization;
using System.Data;
using System.Xml.Serialization;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity base class which represents the base class for the entity 'UITheme'.<br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public abstract partial class UIThemeEntityBase : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		public new string LLBLGenProEntityName {
			get { return "UIThemeEntity"; }
		}
	
		#region Class Member Declarations
		private Obymobi.Data.CollectionClasses.ClientConfigurationCollection	_clientConfigurationCollection;
		private bool	_alwaysFetchClientConfigurationCollection, _alreadyFetchedClientConfigurationCollection;
		private Obymobi.Data.CollectionClasses.DeliverypointgroupCollection	_deliverypointgroupCollection;
		private bool	_alwaysFetchDeliverypointgroupCollection, _alreadyFetchedDeliverypointgroupCollection;
		private Obymobi.Data.CollectionClasses.MediaCollection	_mediaCollection;
		private bool	_alwaysFetchMediaCollection, _alreadyFetchedMediaCollection;
		private Obymobi.Data.CollectionClasses.TimestampCollection	_timestampCollection;
		private bool	_alwaysFetchTimestampCollection, _alreadyFetchedTimestampCollection;
		private Obymobi.Data.CollectionClasses.UIThemeColorCollection	_uIThemeColorCollection;
		private bool	_alwaysFetchUIThemeColorCollection, _alreadyFetchedUIThemeColorCollection;
		private Obymobi.Data.CollectionClasses.UIThemeTextSizeCollection	_uIThemeTextSizeCollection;
		private bool	_alwaysFetchUIThemeTextSizeCollection, _alreadyFetchedUIThemeTextSizeCollection;
		private CompanyEntity _companyEntity;
		private bool	_alwaysFetchCompanyEntity, _alreadyFetchedCompanyEntity, _companyEntityReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name CompanyEntity</summary>
			public static readonly string CompanyEntity = "CompanyEntity";
			/// <summary>Member name ClientConfigurationCollection</summary>
			public static readonly string ClientConfigurationCollection = "ClientConfigurationCollection";
			/// <summary>Member name DeliverypointgroupCollection</summary>
			public static readonly string DeliverypointgroupCollection = "DeliverypointgroupCollection";
			/// <summary>Member name MediaCollection</summary>
			public static readonly string MediaCollection = "MediaCollection";
			/// <summary>Member name TimestampCollection</summary>
			public static readonly string TimestampCollection = "TimestampCollection";
			/// <summary>Member name UIThemeColorCollection</summary>
			public static readonly string UIThemeColorCollection = "UIThemeColorCollection";
			/// <summary>Member name UIThemeTextSizeCollection</summary>
			public static readonly string UIThemeTextSizeCollection = "UIThemeTextSizeCollection";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static UIThemeEntityBase()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		protected UIThemeEntityBase() :base("UIThemeEntity")
		{
			InitClassEmpty(null);
		}

		/// <summary>CTor</summary>
		/// <param name="uIThemeId">PK value for UITheme which data should be fetched into this UITheme object</param>
		protected UIThemeEntityBase(System.Int32 uIThemeId):base("UIThemeEntity")
		{
			InitClassFetch(uIThemeId, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="uIThemeId">PK value for UITheme which data should be fetched into this UITheme object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		protected UIThemeEntityBase(System.Int32 uIThemeId, IPrefetchPath prefetchPathToUse): base("UIThemeEntity")
		{
			InitClassFetch(uIThemeId, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="uIThemeId">PK value for UITheme which data should be fetched into this UITheme object</param>
		/// <param name="validator">The custom validator object for this UIThemeEntity</param>
		protected UIThemeEntityBase(System.Int32 uIThemeId, IValidator validator):base("UIThemeEntity")
		{
			InitClassFetch(uIThemeId, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected UIThemeEntityBase(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_clientConfigurationCollection = (Obymobi.Data.CollectionClasses.ClientConfigurationCollection)info.GetValue("_clientConfigurationCollection", typeof(Obymobi.Data.CollectionClasses.ClientConfigurationCollection));
			_alwaysFetchClientConfigurationCollection = info.GetBoolean("_alwaysFetchClientConfigurationCollection");
			_alreadyFetchedClientConfigurationCollection = info.GetBoolean("_alreadyFetchedClientConfigurationCollection");

			_deliverypointgroupCollection = (Obymobi.Data.CollectionClasses.DeliverypointgroupCollection)info.GetValue("_deliverypointgroupCollection", typeof(Obymobi.Data.CollectionClasses.DeliverypointgroupCollection));
			_alwaysFetchDeliverypointgroupCollection = info.GetBoolean("_alwaysFetchDeliverypointgroupCollection");
			_alreadyFetchedDeliverypointgroupCollection = info.GetBoolean("_alreadyFetchedDeliverypointgroupCollection");

			_mediaCollection = (Obymobi.Data.CollectionClasses.MediaCollection)info.GetValue("_mediaCollection", typeof(Obymobi.Data.CollectionClasses.MediaCollection));
			_alwaysFetchMediaCollection = info.GetBoolean("_alwaysFetchMediaCollection");
			_alreadyFetchedMediaCollection = info.GetBoolean("_alreadyFetchedMediaCollection");

			_timestampCollection = (Obymobi.Data.CollectionClasses.TimestampCollection)info.GetValue("_timestampCollection", typeof(Obymobi.Data.CollectionClasses.TimestampCollection));
			_alwaysFetchTimestampCollection = info.GetBoolean("_alwaysFetchTimestampCollection");
			_alreadyFetchedTimestampCollection = info.GetBoolean("_alreadyFetchedTimestampCollection");

			_uIThemeColorCollection = (Obymobi.Data.CollectionClasses.UIThemeColorCollection)info.GetValue("_uIThemeColorCollection", typeof(Obymobi.Data.CollectionClasses.UIThemeColorCollection));
			_alwaysFetchUIThemeColorCollection = info.GetBoolean("_alwaysFetchUIThemeColorCollection");
			_alreadyFetchedUIThemeColorCollection = info.GetBoolean("_alreadyFetchedUIThemeColorCollection");

			_uIThemeTextSizeCollection = (Obymobi.Data.CollectionClasses.UIThemeTextSizeCollection)info.GetValue("_uIThemeTextSizeCollection", typeof(Obymobi.Data.CollectionClasses.UIThemeTextSizeCollection));
			_alwaysFetchUIThemeTextSizeCollection = info.GetBoolean("_alwaysFetchUIThemeTextSizeCollection");
			_alreadyFetchedUIThemeTextSizeCollection = info.GetBoolean("_alreadyFetchedUIThemeTextSizeCollection");
			_companyEntity = (CompanyEntity)info.GetValue("_companyEntity", typeof(CompanyEntity));
			if(_companyEntity!=null)
			{
				_companyEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_companyEntityReturnsNewIfNotFound = info.GetBoolean("_companyEntityReturnsNewIfNotFound");
			_alwaysFetchCompanyEntity = info.GetBoolean("_alwaysFetchCompanyEntity");
			_alreadyFetchedCompanyEntity = info.GetBoolean("_alreadyFetchedCompanyEntity");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}	
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((UIThemeFieldIndex)fieldIndex)
			{
				case UIThemeFieldIndex.CompanyId:
					DesetupSyncCompanyEntity(true, false);
					_alreadyFetchedCompanyEntity = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedClientConfigurationCollection = (_clientConfigurationCollection.Count > 0);
			_alreadyFetchedDeliverypointgroupCollection = (_deliverypointgroupCollection.Count > 0);
			_alreadyFetchedMediaCollection = (_mediaCollection.Count > 0);
			_alreadyFetchedTimestampCollection = (_timestampCollection.Count > 0);
			_alreadyFetchedUIThemeColorCollection = (_uIThemeColorCollection.Count > 0);
			_alreadyFetchedUIThemeTextSizeCollection = (_uIThemeTextSizeCollection.Count > 0);
			_alreadyFetchedCompanyEntity = (_companyEntity != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "CompanyEntity":
					toReturn.Add(Relations.CompanyEntityUsingCompanyId);
					break;
				case "ClientConfigurationCollection":
					toReturn.Add(Relations.ClientConfigurationEntityUsingUIThemeId);
					break;
				case "DeliverypointgroupCollection":
					toReturn.Add(Relations.DeliverypointgroupEntityUsingUIThemeId);
					break;
				case "MediaCollection":
					toReturn.Add(Relations.MediaEntityUsingUIThemeId);
					break;
				case "TimestampCollection":
					toReturn.Add(Relations.TimestampEntityUsingUIThemeId);
					break;
				case "UIThemeColorCollection":
					toReturn.Add(Relations.UIThemeColorEntityUsingUIThemeId);
					break;
				case "UIThemeTextSizeCollection":
					toReturn.Add(Relations.UIThemeTextSizeEntityUsingUIThemeId);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_clientConfigurationCollection", (!this.MarkedForDeletion?_clientConfigurationCollection:null));
			info.AddValue("_alwaysFetchClientConfigurationCollection", _alwaysFetchClientConfigurationCollection);
			info.AddValue("_alreadyFetchedClientConfigurationCollection", _alreadyFetchedClientConfigurationCollection);
			info.AddValue("_deliverypointgroupCollection", (!this.MarkedForDeletion?_deliverypointgroupCollection:null));
			info.AddValue("_alwaysFetchDeliverypointgroupCollection", _alwaysFetchDeliverypointgroupCollection);
			info.AddValue("_alreadyFetchedDeliverypointgroupCollection", _alreadyFetchedDeliverypointgroupCollection);
			info.AddValue("_mediaCollection", (!this.MarkedForDeletion?_mediaCollection:null));
			info.AddValue("_alwaysFetchMediaCollection", _alwaysFetchMediaCollection);
			info.AddValue("_alreadyFetchedMediaCollection", _alreadyFetchedMediaCollection);
			info.AddValue("_timestampCollection", (!this.MarkedForDeletion?_timestampCollection:null));
			info.AddValue("_alwaysFetchTimestampCollection", _alwaysFetchTimestampCollection);
			info.AddValue("_alreadyFetchedTimestampCollection", _alreadyFetchedTimestampCollection);
			info.AddValue("_uIThemeColorCollection", (!this.MarkedForDeletion?_uIThemeColorCollection:null));
			info.AddValue("_alwaysFetchUIThemeColorCollection", _alwaysFetchUIThemeColorCollection);
			info.AddValue("_alreadyFetchedUIThemeColorCollection", _alreadyFetchedUIThemeColorCollection);
			info.AddValue("_uIThemeTextSizeCollection", (!this.MarkedForDeletion?_uIThemeTextSizeCollection:null));
			info.AddValue("_alwaysFetchUIThemeTextSizeCollection", _alwaysFetchUIThemeTextSizeCollection);
			info.AddValue("_alreadyFetchedUIThemeTextSizeCollection", _alreadyFetchedUIThemeTextSizeCollection);
			info.AddValue("_companyEntity", (!this.MarkedForDeletion?_companyEntity:null));
			info.AddValue("_companyEntityReturnsNewIfNotFound", _companyEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchCompanyEntity", _alwaysFetchCompanyEntity);
			info.AddValue("_alreadyFetchedCompanyEntity", _alreadyFetchedCompanyEntity);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "CompanyEntity":
					_alreadyFetchedCompanyEntity = true;
					this.CompanyEntity = (CompanyEntity)entity;
					break;
				case "ClientConfigurationCollection":
					_alreadyFetchedClientConfigurationCollection = true;
					if(entity!=null)
					{
						this.ClientConfigurationCollection.Add((ClientConfigurationEntity)entity);
					}
					break;
				case "DeliverypointgroupCollection":
					_alreadyFetchedDeliverypointgroupCollection = true;
					if(entity!=null)
					{
						this.DeliverypointgroupCollection.Add((DeliverypointgroupEntity)entity);
					}
					break;
				case "MediaCollection":
					_alreadyFetchedMediaCollection = true;
					if(entity!=null)
					{
						this.MediaCollection.Add((MediaEntity)entity);
					}
					break;
				case "TimestampCollection":
					_alreadyFetchedTimestampCollection = true;
					if(entity!=null)
					{
						this.TimestampCollection.Add((TimestampEntity)entity);
					}
					break;
				case "UIThemeColorCollection":
					_alreadyFetchedUIThemeColorCollection = true;
					if(entity!=null)
					{
						this.UIThemeColorCollection.Add((UIThemeColorEntity)entity);
					}
					break;
				case "UIThemeTextSizeCollection":
					_alreadyFetchedUIThemeTextSizeCollection = true;
					if(entity!=null)
					{
						this.UIThemeTextSizeCollection.Add((UIThemeTextSizeEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "CompanyEntity":
					SetupSyncCompanyEntity(relatedEntity);
					break;
				case "ClientConfigurationCollection":
					_clientConfigurationCollection.Add((ClientConfigurationEntity)relatedEntity);
					break;
				case "DeliverypointgroupCollection":
					_deliverypointgroupCollection.Add((DeliverypointgroupEntity)relatedEntity);
					break;
				case "MediaCollection":
					_mediaCollection.Add((MediaEntity)relatedEntity);
					break;
				case "TimestampCollection":
					_timestampCollection.Add((TimestampEntity)relatedEntity);
					break;
				case "UIThemeColorCollection":
					_uIThemeColorCollection.Add((UIThemeColorEntity)relatedEntity);
					break;
				case "UIThemeTextSizeCollection":
					_uIThemeTextSizeCollection.Add((UIThemeTextSizeEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "CompanyEntity":
					DesetupSyncCompanyEntity(false, true);
					break;
				case "ClientConfigurationCollection":
					this.PerformRelatedEntityRemoval(_clientConfigurationCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "DeliverypointgroupCollection":
					this.PerformRelatedEntityRemoval(_deliverypointgroupCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "MediaCollection":
					this.PerformRelatedEntityRemoval(_mediaCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "TimestampCollection":
					this.PerformRelatedEntityRemoval(_timestampCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "UIThemeColorCollection":
					this.PerformRelatedEntityRemoval(_uIThemeColorCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "UIThemeTextSizeCollection":
					this.PerformRelatedEntityRemoval(_uIThemeTextSizeCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_companyEntity!=null)
			{
				toReturn.Add(_companyEntity);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_clientConfigurationCollection);
			toReturn.Add(_deliverypointgroupCollection);
			toReturn.Add(_mediaCollection);
			toReturn.Add(_timestampCollection);
			toReturn.Add(_uIThemeColorCollection);
			toReturn.Add(_uIThemeTextSizeCollection);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="uIThemeId">PK value for UITheme which data should be fetched into this UITheme object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 uIThemeId)
		{
			return FetchUsingPK(uIThemeId, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="uIThemeId">PK value for UITheme which data should be fetched into this UITheme object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 uIThemeId, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(uIThemeId, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="uIThemeId">PK value for UITheme which data should be fetched into this UITheme object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 uIThemeId, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(uIThemeId, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="uIThemeId">PK value for UITheme which data should be fetched into this UITheme object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 uIThemeId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(uIThemeId, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.UIThemeId, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new UIThemeRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'ClientConfigurationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ClientConfigurationEntity'</returns>
		public Obymobi.Data.CollectionClasses.ClientConfigurationCollection GetMultiClientConfigurationCollection(bool forceFetch)
		{
			return GetMultiClientConfigurationCollection(forceFetch, _clientConfigurationCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ClientConfigurationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ClientConfigurationEntity'</returns>
		public Obymobi.Data.CollectionClasses.ClientConfigurationCollection GetMultiClientConfigurationCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiClientConfigurationCollection(forceFetch, _clientConfigurationCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ClientConfigurationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.ClientConfigurationCollection GetMultiClientConfigurationCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiClientConfigurationCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ClientConfigurationEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.ClientConfigurationCollection GetMultiClientConfigurationCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedClientConfigurationCollection || forceFetch || _alwaysFetchClientConfigurationCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_clientConfigurationCollection);
				_clientConfigurationCollection.SuppressClearInGetMulti=!forceFetch;
				_clientConfigurationCollection.EntityFactoryToUse = entityFactoryToUse;
				_clientConfigurationCollection.GetMultiManyToOne(null, null, null, null, null, null, null, null, null, this, null, filter);
				_clientConfigurationCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedClientConfigurationCollection = true;
			}
			return _clientConfigurationCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'ClientConfigurationCollection'. These settings will be taken into account
		/// when the property ClientConfigurationCollection is requested or GetMultiClientConfigurationCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersClientConfigurationCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_clientConfigurationCollection.SortClauses=sortClauses;
			_clientConfigurationCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointgroupEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'DeliverypointgroupEntity'</returns>
		public Obymobi.Data.CollectionClasses.DeliverypointgroupCollection GetMultiDeliverypointgroupCollection(bool forceFetch)
		{
			return GetMultiDeliverypointgroupCollection(forceFetch, _deliverypointgroupCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointgroupEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'DeliverypointgroupEntity'</returns>
		public Obymobi.Data.CollectionClasses.DeliverypointgroupCollection GetMultiDeliverypointgroupCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiDeliverypointgroupCollection(forceFetch, _deliverypointgroupCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointgroupEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.DeliverypointgroupCollection GetMultiDeliverypointgroupCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiDeliverypointgroupCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointgroupEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.DeliverypointgroupCollection GetMultiDeliverypointgroupCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedDeliverypointgroupCollection || forceFetch || _alwaysFetchDeliverypointgroupCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_deliverypointgroupCollection);
				_deliverypointgroupCollection.SuppressClearInGetMulti=!forceFetch;
				_deliverypointgroupCollection.EntityFactoryToUse = entityFactoryToUse;
				_deliverypointgroupCollection.GetMultiManyToOne(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, this, filter);
				_deliverypointgroupCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedDeliverypointgroupCollection = true;
			}
			return _deliverypointgroupCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'DeliverypointgroupCollection'. These settings will be taken into account
		/// when the property DeliverypointgroupCollection is requested or GetMultiDeliverypointgroupCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersDeliverypointgroupCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_deliverypointgroupCollection.SortClauses=sortClauses;
			_deliverypointgroupCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'MediaEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'MediaEntity'</returns>
		public Obymobi.Data.CollectionClasses.MediaCollection GetMultiMediaCollection(bool forceFetch)
		{
			return GetMultiMediaCollection(forceFetch, _mediaCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'MediaEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'MediaEntity'</returns>
		public Obymobi.Data.CollectionClasses.MediaCollection GetMultiMediaCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiMediaCollection(forceFetch, _mediaCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'MediaEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.MediaCollection GetMultiMediaCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiMediaCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'MediaEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.MediaCollection GetMultiMediaCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedMediaCollection || forceFetch || _alwaysFetchMediaCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_mediaCollection);
				_mediaCollection.SuppressClearInGetMulti=!forceFetch;
				_mediaCollection.EntityFactoryToUse = entityFactoryToUse;
				_mediaCollection.GetMultiManyToOne(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, this, null, filter);
				_mediaCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedMediaCollection = true;
			}
			return _mediaCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'MediaCollection'. These settings will be taken into account
		/// when the property MediaCollection is requested or GetMultiMediaCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersMediaCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_mediaCollection.SortClauses=sortClauses;
			_mediaCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'TimestampEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'TimestampEntity'</returns>
		public Obymobi.Data.CollectionClasses.TimestampCollection GetMultiTimestampCollection(bool forceFetch)
		{
			return GetMultiTimestampCollection(forceFetch, _timestampCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TimestampEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'TimestampEntity'</returns>
		public Obymobi.Data.CollectionClasses.TimestampCollection GetMultiTimestampCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiTimestampCollection(forceFetch, _timestampCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'TimestampEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.TimestampCollection GetMultiTimestampCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiTimestampCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TimestampEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.TimestampCollection GetMultiTimestampCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedTimestampCollection || forceFetch || _alwaysFetchTimestampCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_timestampCollection);
				_timestampCollection.SuppressClearInGetMulti=!forceFetch;
				_timestampCollection.EntityFactoryToUse = entityFactoryToUse;
				_timestampCollection.GetMultiManyToOne(null, null, null, this, filter);
				_timestampCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedTimestampCollection = true;
			}
			return _timestampCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'TimestampCollection'. These settings will be taken into account
		/// when the property TimestampCollection is requested or GetMultiTimestampCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersTimestampCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_timestampCollection.SortClauses=sortClauses;
			_timestampCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'UIThemeColorEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'UIThemeColorEntity'</returns>
		public Obymobi.Data.CollectionClasses.UIThemeColorCollection GetMultiUIThemeColorCollection(bool forceFetch)
		{
			return GetMultiUIThemeColorCollection(forceFetch, _uIThemeColorCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'UIThemeColorEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'UIThemeColorEntity'</returns>
		public Obymobi.Data.CollectionClasses.UIThemeColorCollection GetMultiUIThemeColorCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiUIThemeColorCollection(forceFetch, _uIThemeColorCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'UIThemeColorEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.UIThemeColorCollection GetMultiUIThemeColorCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiUIThemeColorCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'UIThemeColorEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.UIThemeColorCollection GetMultiUIThemeColorCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedUIThemeColorCollection || forceFetch || _alwaysFetchUIThemeColorCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_uIThemeColorCollection);
				_uIThemeColorCollection.SuppressClearInGetMulti=!forceFetch;
				_uIThemeColorCollection.EntityFactoryToUse = entityFactoryToUse;
				_uIThemeColorCollection.GetMultiManyToOne(this, filter);
				_uIThemeColorCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedUIThemeColorCollection = true;
			}
			return _uIThemeColorCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'UIThemeColorCollection'. These settings will be taken into account
		/// when the property UIThemeColorCollection is requested or GetMultiUIThemeColorCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersUIThemeColorCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_uIThemeColorCollection.SortClauses=sortClauses;
			_uIThemeColorCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'UIThemeTextSizeEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'UIThemeTextSizeEntity'</returns>
		public Obymobi.Data.CollectionClasses.UIThemeTextSizeCollection GetMultiUIThemeTextSizeCollection(bool forceFetch)
		{
			return GetMultiUIThemeTextSizeCollection(forceFetch, _uIThemeTextSizeCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'UIThemeTextSizeEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'UIThemeTextSizeEntity'</returns>
		public Obymobi.Data.CollectionClasses.UIThemeTextSizeCollection GetMultiUIThemeTextSizeCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiUIThemeTextSizeCollection(forceFetch, _uIThemeTextSizeCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'UIThemeTextSizeEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.UIThemeTextSizeCollection GetMultiUIThemeTextSizeCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiUIThemeTextSizeCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'UIThemeTextSizeEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.UIThemeTextSizeCollection GetMultiUIThemeTextSizeCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedUIThemeTextSizeCollection || forceFetch || _alwaysFetchUIThemeTextSizeCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_uIThemeTextSizeCollection);
				_uIThemeTextSizeCollection.SuppressClearInGetMulti=!forceFetch;
				_uIThemeTextSizeCollection.EntityFactoryToUse = entityFactoryToUse;
				_uIThemeTextSizeCollection.GetMultiManyToOne(this, filter);
				_uIThemeTextSizeCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedUIThemeTextSizeCollection = true;
			}
			return _uIThemeTextSizeCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'UIThemeTextSizeCollection'. These settings will be taken into account
		/// when the property UIThemeTextSizeCollection is requested or GetMultiUIThemeTextSizeCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersUIThemeTextSizeCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_uIThemeTextSizeCollection.SortClauses=sortClauses;
			_uIThemeTextSizeCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves the related entity of type 'CompanyEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'CompanyEntity' which is related to this entity.</returns>
		public CompanyEntity GetSingleCompanyEntity()
		{
			return GetSingleCompanyEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'CompanyEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'CompanyEntity' which is related to this entity.</returns>
		public virtual CompanyEntity GetSingleCompanyEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedCompanyEntity || forceFetch || _alwaysFetchCompanyEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.CompanyEntityUsingCompanyId);
				CompanyEntity newEntity = new CompanyEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.CompanyId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (CompanyEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_companyEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.CompanyEntity = newEntity;
				_alreadyFetchedCompanyEntity = fetchResult;
			}
			return _companyEntity;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("CompanyEntity", _companyEntity);
			toReturn.Add("ClientConfigurationCollection", _clientConfigurationCollection);
			toReturn.Add("DeliverypointgroupCollection", _deliverypointgroupCollection);
			toReturn.Add("MediaCollection", _mediaCollection);
			toReturn.Add("TimestampCollection", _timestampCollection);
			toReturn.Add("UIThemeColorCollection", _uIThemeColorCollection);
			toReturn.Add("UIThemeTextSizeCollection", _uIThemeTextSizeCollection);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="uIThemeId">PK value for UITheme which data should be fetched into this UITheme object</param>
		/// <param name="validator">The validator object for this UIThemeEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int32 uIThemeId, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(uIThemeId, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_clientConfigurationCollection = new Obymobi.Data.CollectionClasses.ClientConfigurationCollection();
			_clientConfigurationCollection.SetContainingEntityInfo(this, "UIThemeEntity");

			_deliverypointgroupCollection = new Obymobi.Data.CollectionClasses.DeliverypointgroupCollection();
			_deliverypointgroupCollection.SetContainingEntityInfo(this, "UIThemeEntity");

			_mediaCollection = new Obymobi.Data.CollectionClasses.MediaCollection();
			_mediaCollection.SetContainingEntityInfo(this, "UIThemeEntity");

			_timestampCollection = new Obymobi.Data.CollectionClasses.TimestampCollection();
			_timestampCollection.SetContainingEntityInfo(this, "UIThemeEntity");

			_uIThemeColorCollection = new Obymobi.Data.CollectionClasses.UIThemeColorCollection();
			_uIThemeColorCollection.SetContainingEntityInfo(this, "UIThemeEntity");

			_uIThemeTextSizeCollection = new Obymobi.Data.CollectionClasses.UIThemeTextSizeCollection();
			_uIThemeTextSizeCollection.SetContainingEntityInfo(this, "UIThemeEntity");
			_companyEntityReturnsNewIfNotFound = true;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UIThemeId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ListCategoryBackgroundColor", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ListCategoryDividerColor", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ListCategoryTextColor", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ListCategorySelectedBackgroundColor", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ListCategorySelectedDividerColor", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ListCategorySelectedTextColor", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ListItemBackgroundColor", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ListItemDividerColor", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ListItemTextColor", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ListItemSelectedBackgroundColor", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ListItemSelectedDividerColor", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ListItemSelectedTextColor", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("WidgetBackgroundColor", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("WidgetBorderColor", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Name", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("WidgetTextColor", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("HeaderColor", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TabBackgroundBottomColor", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TabBackgroundTopColor", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TabDividerColor", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TabBorderColor", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TabActiveTextColor", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TabInactiveTextColor", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PageBackgroundColor", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PageBorderColor", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PageTitleTextColor", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PageDescriptionTextColor", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PageDividerTopColor", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PageDividerBottomColor", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FooterBackgroundBottomColor", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FooterBackgroundTopColor", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FooterTextColor", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PageErrorTextColor", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FooterDividerColor", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PagePriceTextColor", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PageFooterColor", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ButtonBackgroundTopColor", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ButtonBackgroundBottomColor", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ButtonBorderColor", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ButtonTextColor", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ButtonPositiveBackgroundTopColor", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ButtonPositiveBackgroundBottomColor", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ButtonPositiveBorderColor", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ButtonPositiveTextColor", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ButtonDisabledBackgroundTopColor", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ButtonDisabledBackgroundBottomColor", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ButtonDisabledBorderColor", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ButtonDisabledTextColor", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DialogBackgroundColor", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DialogBorderColor", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DialogPanelBackgroundColor", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DialogPanelBorderColor", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DialogPrimaryTextColor", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DialogSecondaryTextColor", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TextboxBackgroundColor", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TextboxBorderColor", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TextboxTextColor", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TextboxCursorColor", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PageInstructionsTextColor", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DialogTitleTextColor", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SpinnerBackgroundColor", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SpinnerBorderColor", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SpinnerTextColor", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CompanyId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RoomControlButtonOuterBorderColor", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RoomControlButtonInnerBorderTopColor", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RoomControlButtonInnerBorderBottomColor", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RoomControlButtonBackgroundTopColor", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RoomControlButtonBackgroundBottomColor", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RoomControlDividerTopColor", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RoomControlDividerBottomColor", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RoomControlButtonTextColor", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RoomControlTitleColor", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RoomControlTextColor", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RoomControlPageBackgroundColor", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RoomControlSpinnerBorderColor", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RoomControlSpinnerBackgroundColor", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RoomControlSpinnerTextColor", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RoomControlButtonIndicatorColor", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RoomControlButtonIndicatorBackgroundColor", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RoomControlHeaderButtonBackgroundColor", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RoomControlHeaderButtonSelectorColor", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RoomControlHeaderButtonTextColor", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RoomControlStationNumberColor", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RoomControlSliderMinColor", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RoomControlSliderMaxColor", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RoomControlToggleOnTextColor", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RoomControlToggleOffTextColor", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RoomControlToggleBackgroundColor", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RoomControlToggleBorderColor", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RoomControlThermostatComponentColor", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FooterButtonBackgroundTopColor", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FooterButtonBackgroundBottomColor", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FooterButtonBorderColor", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FooterButtonTextColor", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FooterButtonDisabledBackgroundTopColor", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FooterButtonDisabledBackgroundBottomColor", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FooterButtonDisabledBorderColor", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FooterButtonDisabledTextColor", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ListCategoryL2BackgroundColor", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ListCategoryL3BackgroundColor", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ListCategoryL4BackgroundColor", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ListCategoryL5BackgroundColor", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ListItemPriceTextColor", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ListItemSelectedPriceTextColor", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AlterationDialogBackgroundColor", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AlterationDialogBorderColor", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AlterationDialogPanelBackgroundColor", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AlterationDialogListItemBackgroundColor", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AlterationDialogListItemSelectedBackgroundColor", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AlterationDialogListItemSelectedBorderColor", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AlterationDialogPrimaryTextColor", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AlterationDialogSecondaryTextColor", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AlterationDialogInputBackgroundColor", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AlterationDialogInputBorderColor", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AlterationDialogInputTextColor", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AlterationDialogInputHintTextColor", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AlterationDialogPanelBorderColor", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AlterationDialogRadioButtonColor", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AlterationDialogCheckBoxColor", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AlterationDialogCloseButtonColor", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AlterationDialogListViewArrowColor", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AlterationDialogListViewDividerColor", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AreaTabActiveText", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AreaTabInactiveText", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _companyEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncCompanyEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _companyEntity, new PropertyChangedEventHandler( OnCompanyEntityPropertyChanged ), "CompanyEntity", Obymobi.Data.RelationClasses.StaticUIThemeRelations.CompanyEntityUsingCompanyIdStatic, true, signalRelatedEntity, "UIThemeCollection", resetFKFields, new int[] { (int)UIThemeFieldIndex.CompanyId } );		
			_companyEntity = null;
		}
		
		/// <summary> setups the sync logic for member _companyEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncCompanyEntity(IEntityCore relatedEntity)
		{
			if(_companyEntity!=relatedEntity)
			{		
				DesetupSyncCompanyEntity(true, true);
				_companyEntity = (CompanyEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _companyEntity, new PropertyChangedEventHandler( OnCompanyEntityPropertyChanged ), "CompanyEntity", Obymobi.Data.RelationClasses.StaticUIThemeRelations.CompanyEntityUsingCompanyIdStatic, true, ref _alreadyFetchedCompanyEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnCompanyEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="uIThemeId">PK value for UITheme which data should be fetched into this UITheme object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int32 uIThemeId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)UIThemeFieldIndex.UIThemeId].ForcedCurrentValueWrite(uIThemeId);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateUIThemeDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new UIThemeEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static UIThemeRelations Relations
		{
			get	{ return new UIThemeRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ClientConfiguration' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathClientConfigurationCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ClientConfigurationCollection(), (IEntityRelation)GetRelationsForField("ClientConfigurationCollection")[0], (int)Obymobi.Data.EntityType.UIThemeEntity, (int)Obymobi.Data.EntityType.ClientConfigurationEntity, 0, null, null, null, "ClientConfigurationCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Deliverypointgroup' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathDeliverypointgroupCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.DeliverypointgroupCollection(), (IEntityRelation)GetRelationsForField("DeliverypointgroupCollection")[0], (int)Obymobi.Data.EntityType.UIThemeEntity, (int)Obymobi.Data.EntityType.DeliverypointgroupEntity, 0, null, null, null, "DeliverypointgroupCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Media' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathMediaCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.MediaCollection(), (IEntityRelation)GetRelationsForField("MediaCollection")[0], (int)Obymobi.Data.EntityType.UIThemeEntity, (int)Obymobi.Data.EntityType.MediaEntity, 0, null, null, null, "MediaCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Timestamp' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTimestampCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.TimestampCollection(), (IEntityRelation)GetRelationsForField("TimestampCollection")[0], (int)Obymobi.Data.EntityType.UIThemeEntity, (int)Obymobi.Data.EntityType.TimestampEntity, 0, null, null, null, "TimestampCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'UIThemeColor' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathUIThemeColorCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.UIThemeColorCollection(), (IEntityRelation)GetRelationsForField("UIThemeColorCollection")[0], (int)Obymobi.Data.EntityType.UIThemeEntity, (int)Obymobi.Data.EntityType.UIThemeColorEntity, 0, null, null, null, "UIThemeColorCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'UIThemeTextSize' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathUIThemeTextSizeCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.UIThemeTextSizeCollection(), (IEntityRelation)GetRelationsForField("UIThemeTextSizeCollection")[0], (int)Obymobi.Data.EntityType.UIThemeEntity, (int)Obymobi.Data.EntityType.UIThemeTextSizeEntity, 0, null, null, null, "UIThemeTextSizeCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Company'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCompanyEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CompanyCollection(), (IEntityRelation)GetRelationsForField("CompanyEntity")[0], (int)Obymobi.Data.EntityType.UIThemeEntity, (int)Obymobi.Data.EntityType.CompanyEntity, 0, null, null, null, "CompanyEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The UIThemeId property of the Entity UITheme<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UITheme"."UIThemeId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 UIThemeId
		{
			get { return (System.Int32)GetValue((int)UIThemeFieldIndex.UIThemeId, true); }
			set	{ SetValue((int)UIThemeFieldIndex.UIThemeId, value, true); }
		}

		/// <summary> The ListCategoryBackgroundColor property of the Entity UITheme<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UITheme"."ListCategoryBackgroundColor"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 ListCategoryBackgroundColor
		{
			get { return (System.Int32)GetValue((int)UIThemeFieldIndex.ListCategoryBackgroundColor, true); }
			set	{ SetValue((int)UIThemeFieldIndex.ListCategoryBackgroundColor, value, true); }
		}

		/// <summary> The ListCategoryDividerColor property of the Entity UITheme<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UITheme"."ListCategoryDividerColor"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 ListCategoryDividerColor
		{
			get { return (System.Int32)GetValue((int)UIThemeFieldIndex.ListCategoryDividerColor, true); }
			set	{ SetValue((int)UIThemeFieldIndex.ListCategoryDividerColor, value, true); }
		}

		/// <summary> The ListCategoryTextColor property of the Entity UITheme<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UITheme"."ListCategoryTextColor"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 ListCategoryTextColor
		{
			get { return (System.Int32)GetValue((int)UIThemeFieldIndex.ListCategoryTextColor, true); }
			set	{ SetValue((int)UIThemeFieldIndex.ListCategoryTextColor, value, true); }
		}

		/// <summary> The ListCategorySelectedBackgroundColor property of the Entity UITheme<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UITheme"."ListCategorySelectedBackgroundColor"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 ListCategorySelectedBackgroundColor
		{
			get { return (System.Int32)GetValue((int)UIThemeFieldIndex.ListCategorySelectedBackgroundColor, true); }
			set	{ SetValue((int)UIThemeFieldIndex.ListCategorySelectedBackgroundColor, value, true); }
		}

		/// <summary> The ListCategorySelectedDividerColor property of the Entity UITheme<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UITheme"."ListCategorySelectedDividerColor"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 ListCategorySelectedDividerColor
		{
			get { return (System.Int32)GetValue((int)UIThemeFieldIndex.ListCategorySelectedDividerColor, true); }
			set	{ SetValue((int)UIThemeFieldIndex.ListCategorySelectedDividerColor, value, true); }
		}

		/// <summary> The ListCategorySelectedTextColor property of the Entity UITheme<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UITheme"."ListCategorySelectedTextColor"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 ListCategorySelectedTextColor
		{
			get { return (System.Int32)GetValue((int)UIThemeFieldIndex.ListCategorySelectedTextColor, true); }
			set	{ SetValue((int)UIThemeFieldIndex.ListCategorySelectedTextColor, value, true); }
		}

		/// <summary> The ListItemBackgroundColor property of the Entity UITheme<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UITheme"."ListItemBackgroundColor"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 ListItemBackgroundColor
		{
			get { return (System.Int32)GetValue((int)UIThemeFieldIndex.ListItemBackgroundColor, true); }
			set	{ SetValue((int)UIThemeFieldIndex.ListItemBackgroundColor, value, true); }
		}

		/// <summary> The ListItemDividerColor property of the Entity UITheme<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UITheme"."ListItemDividerColor"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 ListItemDividerColor
		{
			get { return (System.Int32)GetValue((int)UIThemeFieldIndex.ListItemDividerColor, true); }
			set	{ SetValue((int)UIThemeFieldIndex.ListItemDividerColor, value, true); }
		}

		/// <summary> The ListItemTextColor property of the Entity UITheme<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UITheme"."ListItemTextColor"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 ListItemTextColor
		{
			get { return (System.Int32)GetValue((int)UIThemeFieldIndex.ListItemTextColor, true); }
			set	{ SetValue((int)UIThemeFieldIndex.ListItemTextColor, value, true); }
		}

		/// <summary> The ListItemSelectedBackgroundColor property of the Entity UITheme<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UITheme"."ListItemSelectedBackgroundColor"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 ListItemSelectedBackgroundColor
		{
			get { return (System.Int32)GetValue((int)UIThemeFieldIndex.ListItemSelectedBackgroundColor, true); }
			set	{ SetValue((int)UIThemeFieldIndex.ListItemSelectedBackgroundColor, value, true); }
		}

		/// <summary> The ListItemSelectedDividerColor property of the Entity UITheme<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UITheme"."ListItemSelectedDividerColor"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 ListItemSelectedDividerColor
		{
			get { return (System.Int32)GetValue((int)UIThemeFieldIndex.ListItemSelectedDividerColor, true); }
			set	{ SetValue((int)UIThemeFieldIndex.ListItemSelectedDividerColor, value, true); }
		}

		/// <summary> The ListItemSelectedTextColor property of the Entity UITheme<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UITheme"."ListItemSelectedTextColor"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 ListItemSelectedTextColor
		{
			get { return (System.Int32)GetValue((int)UIThemeFieldIndex.ListItemSelectedTextColor, true); }
			set	{ SetValue((int)UIThemeFieldIndex.ListItemSelectedTextColor, value, true); }
		}

		/// <summary> The WidgetBackgroundColor property of the Entity UITheme<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UITheme"."WidgetBackgroundColor"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 WidgetBackgroundColor
		{
			get { return (System.Int32)GetValue((int)UIThemeFieldIndex.WidgetBackgroundColor, true); }
			set	{ SetValue((int)UIThemeFieldIndex.WidgetBackgroundColor, value, true); }
		}

		/// <summary> The WidgetBorderColor property of the Entity UITheme<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UITheme"."WidgetBorderColor"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 WidgetBorderColor
		{
			get { return (System.Int32)GetValue((int)UIThemeFieldIndex.WidgetBorderColor, true); }
			set	{ SetValue((int)UIThemeFieldIndex.WidgetBorderColor, value, true); }
		}

		/// <summary> The CreatedUTC property of the Entity UITheme<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UITheme"."CreatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CreatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)UIThemeFieldIndex.CreatedUTC, false); }
			set	{ SetValue((int)UIThemeFieldIndex.CreatedUTC, value, true); }
		}

		/// <summary> The CreatedBy property of the Entity UITheme<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UITheme"."CreatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> CreatedBy
		{
			get { return (Nullable<System.Int32>)GetValue((int)UIThemeFieldIndex.CreatedBy, false); }
			set	{ SetValue((int)UIThemeFieldIndex.CreatedBy, value, true); }
		}

		/// <summary> The UpdatedUTC property of the Entity UITheme<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UITheme"."UpdatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)UIThemeFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)UIThemeFieldIndex.UpdatedUTC, value, true); }
		}

		/// <summary> The UpdatedBy property of the Entity UITheme<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UITheme"."UpdatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> UpdatedBy
		{
			get { return (Nullable<System.Int32>)GetValue((int)UIThemeFieldIndex.UpdatedBy, false); }
			set	{ SetValue((int)UIThemeFieldIndex.UpdatedBy, value, true); }
		}

		/// <summary> The Name property of the Entity UITheme<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UITheme"."Name"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 255<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String Name
		{
			get { return (System.String)GetValue((int)UIThemeFieldIndex.Name, true); }
			set	{ SetValue((int)UIThemeFieldIndex.Name, value, true); }
		}

		/// <summary> The WidgetTextColor property of the Entity UITheme<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UITheme"."WidgetTextColor"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 WidgetTextColor
		{
			get { return (System.Int32)GetValue((int)UIThemeFieldIndex.WidgetTextColor, true); }
			set	{ SetValue((int)UIThemeFieldIndex.WidgetTextColor, value, true); }
		}

		/// <summary> The HeaderColor property of the Entity UITheme<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UITheme"."HeaderColor"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 HeaderColor
		{
			get { return (System.Int32)GetValue((int)UIThemeFieldIndex.HeaderColor, true); }
			set	{ SetValue((int)UIThemeFieldIndex.HeaderColor, value, true); }
		}

		/// <summary> The TabBackgroundBottomColor property of the Entity UITheme<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UITheme"."TabBackgroundBottomColor"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 TabBackgroundBottomColor
		{
			get { return (System.Int32)GetValue((int)UIThemeFieldIndex.TabBackgroundBottomColor, true); }
			set	{ SetValue((int)UIThemeFieldIndex.TabBackgroundBottomColor, value, true); }
		}

		/// <summary> The TabBackgroundTopColor property of the Entity UITheme<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UITheme"."TabBackgroundTopColor"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 TabBackgroundTopColor
		{
			get { return (System.Int32)GetValue((int)UIThemeFieldIndex.TabBackgroundTopColor, true); }
			set	{ SetValue((int)UIThemeFieldIndex.TabBackgroundTopColor, value, true); }
		}

		/// <summary> The TabDividerColor property of the Entity UITheme<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UITheme"."TabDividerColor"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 TabDividerColor
		{
			get { return (System.Int32)GetValue((int)UIThemeFieldIndex.TabDividerColor, true); }
			set	{ SetValue((int)UIThemeFieldIndex.TabDividerColor, value, true); }
		}

		/// <summary> The TabBorderColor property of the Entity UITheme<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UITheme"."TabBorderColor"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 TabBorderColor
		{
			get { return (System.Int32)GetValue((int)UIThemeFieldIndex.TabBorderColor, true); }
			set	{ SetValue((int)UIThemeFieldIndex.TabBorderColor, value, true); }
		}

		/// <summary> The TabActiveTextColor property of the Entity UITheme<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UITheme"."TabActiveTextColor"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 TabActiveTextColor
		{
			get { return (System.Int32)GetValue((int)UIThemeFieldIndex.TabActiveTextColor, true); }
			set	{ SetValue((int)UIThemeFieldIndex.TabActiveTextColor, value, true); }
		}

		/// <summary> The TabInactiveTextColor property of the Entity UITheme<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UITheme"."TabInactiveTextColor"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 TabInactiveTextColor
		{
			get { return (System.Int32)GetValue((int)UIThemeFieldIndex.TabInactiveTextColor, true); }
			set	{ SetValue((int)UIThemeFieldIndex.TabInactiveTextColor, value, true); }
		}

		/// <summary> The PageBackgroundColor property of the Entity UITheme<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UITheme"."PageBackgroundColor"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 PageBackgroundColor
		{
			get { return (System.Int32)GetValue((int)UIThemeFieldIndex.PageBackgroundColor, true); }
			set	{ SetValue((int)UIThemeFieldIndex.PageBackgroundColor, value, true); }
		}

		/// <summary> The PageBorderColor property of the Entity UITheme<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UITheme"."PageBorderColor"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 PageBorderColor
		{
			get { return (System.Int32)GetValue((int)UIThemeFieldIndex.PageBorderColor, true); }
			set	{ SetValue((int)UIThemeFieldIndex.PageBorderColor, value, true); }
		}

		/// <summary> The PageTitleTextColor property of the Entity UITheme<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UITheme"."PageTitleTextColor"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 PageTitleTextColor
		{
			get { return (System.Int32)GetValue((int)UIThemeFieldIndex.PageTitleTextColor, true); }
			set	{ SetValue((int)UIThemeFieldIndex.PageTitleTextColor, value, true); }
		}

		/// <summary> The PageDescriptionTextColor property of the Entity UITheme<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UITheme"."PageDescriptionTextColor"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 PageDescriptionTextColor
		{
			get { return (System.Int32)GetValue((int)UIThemeFieldIndex.PageDescriptionTextColor, true); }
			set	{ SetValue((int)UIThemeFieldIndex.PageDescriptionTextColor, value, true); }
		}

		/// <summary> The PageDividerTopColor property of the Entity UITheme<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UITheme"."PageDividerTopColor"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 PageDividerTopColor
		{
			get { return (System.Int32)GetValue((int)UIThemeFieldIndex.PageDividerTopColor, true); }
			set	{ SetValue((int)UIThemeFieldIndex.PageDividerTopColor, value, true); }
		}

		/// <summary> The PageDividerBottomColor property of the Entity UITheme<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UITheme"."PageDividerBottomColor"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 PageDividerBottomColor
		{
			get { return (System.Int32)GetValue((int)UIThemeFieldIndex.PageDividerBottomColor, true); }
			set	{ SetValue((int)UIThemeFieldIndex.PageDividerBottomColor, value, true); }
		}

		/// <summary> The FooterBackgroundBottomColor property of the Entity UITheme<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UITheme"."FooterBackgroundBottomColor"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 FooterBackgroundBottomColor
		{
			get { return (System.Int32)GetValue((int)UIThemeFieldIndex.FooterBackgroundBottomColor, true); }
			set	{ SetValue((int)UIThemeFieldIndex.FooterBackgroundBottomColor, value, true); }
		}

		/// <summary> The FooterBackgroundTopColor property of the Entity UITheme<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UITheme"."FooterBackgroundTopColor"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 FooterBackgroundTopColor
		{
			get { return (System.Int32)GetValue((int)UIThemeFieldIndex.FooterBackgroundTopColor, true); }
			set	{ SetValue((int)UIThemeFieldIndex.FooterBackgroundTopColor, value, true); }
		}

		/// <summary> The FooterTextColor property of the Entity UITheme<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UITheme"."FooterTextColor"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 FooterTextColor
		{
			get { return (System.Int32)GetValue((int)UIThemeFieldIndex.FooterTextColor, true); }
			set	{ SetValue((int)UIThemeFieldIndex.FooterTextColor, value, true); }
		}

		/// <summary> The PageErrorTextColor property of the Entity UITheme<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UITheme"."PageErrorTextColor"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 PageErrorTextColor
		{
			get { return (System.Int32)GetValue((int)UIThemeFieldIndex.PageErrorTextColor, true); }
			set	{ SetValue((int)UIThemeFieldIndex.PageErrorTextColor, value, true); }
		}

		/// <summary> The FooterDividerColor property of the Entity UITheme<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UITheme"."FooterDividerColor"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 FooterDividerColor
		{
			get { return (System.Int32)GetValue((int)UIThemeFieldIndex.FooterDividerColor, true); }
			set	{ SetValue((int)UIThemeFieldIndex.FooterDividerColor, value, true); }
		}

		/// <summary> The PagePriceTextColor property of the Entity UITheme<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UITheme"."PagePriceTextColor"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 PagePriceTextColor
		{
			get { return (System.Int32)GetValue((int)UIThemeFieldIndex.PagePriceTextColor, true); }
			set	{ SetValue((int)UIThemeFieldIndex.PagePriceTextColor, value, true); }
		}

		/// <summary> The PageFooterColor property of the Entity UITheme<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UITheme"."PageFooterColor"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 PageFooterColor
		{
			get { return (System.Int32)GetValue((int)UIThemeFieldIndex.PageFooterColor, true); }
			set	{ SetValue((int)UIThemeFieldIndex.PageFooterColor, value, true); }
		}

		/// <summary> The ButtonBackgroundTopColor property of the Entity UITheme<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UITheme"."ButtonBackgroundTopColor"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 ButtonBackgroundTopColor
		{
			get { return (System.Int32)GetValue((int)UIThemeFieldIndex.ButtonBackgroundTopColor, true); }
			set	{ SetValue((int)UIThemeFieldIndex.ButtonBackgroundTopColor, value, true); }
		}

		/// <summary> The ButtonBackgroundBottomColor property of the Entity UITheme<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UITheme"."ButtonBackgroundBottomColor"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 ButtonBackgroundBottomColor
		{
			get { return (System.Int32)GetValue((int)UIThemeFieldIndex.ButtonBackgroundBottomColor, true); }
			set	{ SetValue((int)UIThemeFieldIndex.ButtonBackgroundBottomColor, value, true); }
		}

		/// <summary> The ButtonBorderColor property of the Entity UITheme<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UITheme"."ButtonBorderColor"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 ButtonBorderColor
		{
			get { return (System.Int32)GetValue((int)UIThemeFieldIndex.ButtonBorderColor, true); }
			set	{ SetValue((int)UIThemeFieldIndex.ButtonBorderColor, value, true); }
		}

		/// <summary> The ButtonTextColor property of the Entity UITheme<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UITheme"."ButtonTextColor"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 ButtonTextColor
		{
			get { return (System.Int32)GetValue((int)UIThemeFieldIndex.ButtonTextColor, true); }
			set	{ SetValue((int)UIThemeFieldIndex.ButtonTextColor, value, true); }
		}

		/// <summary> The ButtonPositiveBackgroundTopColor property of the Entity UITheme<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UITheme"."ButtonPositiveBackgroundTopColor"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 ButtonPositiveBackgroundTopColor
		{
			get { return (System.Int32)GetValue((int)UIThemeFieldIndex.ButtonPositiveBackgroundTopColor, true); }
			set	{ SetValue((int)UIThemeFieldIndex.ButtonPositiveBackgroundTopColor, value, true); }
		}

		/// <summary> The ButtonPositiveBackgroundBottomColor property of the Entity UITheme<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UITheme"."ButtonPositiveBackgroundBottomColor"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 ButtonPositiveBackgroundBottomColor
		{
			get { return (System.Int32)GetValue((int)UIThemeFieldIndex.ButtonPositiveBackgroundBottomColor, true); }
			set	{ SetValue((int)UIThemeFieldIndex.ButtonPositiveBackgroundBottomColor, value, true); }
		}

		/// <summary> The ButtonPositiveBorderColor property of the Entity UITheme<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UITheme"."ButtonPositiveBorderColor"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 ButtonPositiveBorderColor
		{
			get { return (System.Int32)GetValue((int)UIThemeFieldIndex.ButtonPositiveBorderColor, true); }
			set	{ SetValue((int)UIThemeFieldIndex.ButtonPositiveBorderColor, value, true); }
		}

		/// <summary> The ButtonPositiveTextColor property of the Entity UITheme<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UITheme"."ButtonPositiveTextColor"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 ButtonPositiveTextColor
		{
			get { return (System.Int32)GetValue((int)UIThemeFieldIndex.ButtonPositiveTextColor, true); }
			set	{ SetValue((int)UIThemeFieldIndex.ButtonPositiveTextColor, value, true); }
		}

		/// <summary> The ButtonDisabledBackgroundTopColor property of the Entity UITheme<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UITheme"."ButtonDisabledBackgroundTopColor"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 ButtonDisabledBackgroundTopColor
		{
			get { return (System.Int32)GetValue((int)UIThemeFieldIndex.ButtonDisabledBackgroundTopColor, true); }
			set	{ SetValue((int)UIThemeFieldIndex.ButtonDisabledBackgroundTopColor, value, true); }
		}

		/// <summary> The ButtonDisabledBackgroundBottomColor property of the Entity UITheme<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UITheme"."ButtonDisabledBackgroundBottomColor"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 ButtonDisabledBackgroundBottomColor
		{
			get { return (System.Int32)GetValue((int)UIThemeFieldIndex.ButtonDisabledBackgroundBottomColor, true); }
			set	{ SetValue((int)UIThemeFieldIndex.ButtonDisabledBackgroundBottomColor, value, true); }
		}

		/// <summary> The ButtonDisabledBorderColor property of the Entity UITheme<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UITheme"."ButtonDisabledBorderColor"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 ButtonDisabledBorderColor
		{
			get { return (System.Int32)GetValue((int)UIThemeFieldIndex.ButtonDisabledBorderColor, true); }
			set	{ SetValue((int)UIThemeFieldIndex.ButtonDisabledBorderColor, value, true); }
		}

		/// <summary> The ButtonDisabledTextColor property of the Entity UITheme<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UITheme"."ButtonDisabledTextColor"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 ButtonDisabledTextColor
		{
			get { return (System.Int32)GetValue((int)UIThemeFieldIndex.ButtonDisabledTextColor, true); }
			set	{ SetValue((int)UIThemeFieldIndex.ButtonDisabledTextColor, value, true); }
		}

		/// <summary> The DialogBackgroundColor property of the Entity UITheme<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UITheme"."DialogBackgroundColor"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 DialogBackgroundColor
		{
			get { return (System.Int32)GetValue((int)UIThemeFieldIndex.DialogBackgroundColor, true); }
			set	{ SetValue((int)UIThemeFieldIndex.DialogBackgroundColor, value, true); }
		}

		/// <summary> The DialogBorderColor property of the Entity UITheme<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UITheme"."DialogBorderColor"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 DialogBorderColor
		{
			get { return (System.Int32)GetValue((int)UIThemeFieldIndex.DialogBorderColor, true); }
			set	{ SetValue((int)UIThemeFieldIndex.DialogBorderColor, value, true); }
		}

		/// <summary> The DialogPanelBackgroundColor property of the Entity UITheme<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UITheme"."DialogPanelBackgroundColor"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 DialogPanelBackgroundColor
		{
			get { return (System.Int32)GetValue((int)UIThemeFieldIndex.DialogPanelBackgroundColor, true); }
			set	{ SetValue((int)UIThemeFieldIndex.DialogPanelBackgroundColor, value, true); }
		}

		/// <summary> The DialogPanelBorderColor property of the Entity UITheme<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UITheme"."DialogPanelBorderColor"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 DialogPanelBorderColor
		{
			get { return (System.Int32)GetValue((int)UIThemeFieldIndex.DialogPanelBorderColor, true); }
			set	{ SetValue((int)UIThemeFieldIndex.DialogPanelBorderColor, value, true); }
		}

		/// <summary> The DialogPrimaryTextColor property of the Entity UITheme<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UITheme"."DialogPrimaryTextColor"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 DialogPrimaryTextColor
		{
			get { return (System.Int32)GetValue((int)UIThemeFieldIndex.DialogPrimaryTextColor, true); }
			set	{ SetValue((int)UIThemeFieldIndex.DialogPrimaryTextColor, value, true); }
		}

		/// <summary> The DialogSecondaryTextColor property of the Entity UITheme<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UITheme"."DialogSecondaryTextColor"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 DialogSecondaryTextColor
		{
			get { return (System.Int32)GetValue((int)UIThemeFieldIndex.DialogSecondaryTextColor, true); }
			set	{ SetValue((int)UIThemeFieldIndex.DialogSecondaryTextColor, value, true); }
		}

		/// <summary> The TextboxBackgroundColor property of the Entity UITheme<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UITheme"."TextboxBackgroundColor"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 TextboxBackgroundColor
		{
			get { return (System.Int32)GetValue((int)UIThemeFieldIndex.TextboxBackgroundColor, true); }
			set	{ SetValue((int)UIThemeFieldIndex.TextboxBackgroundColor, value, true); }
		}

		/// <summary> The TextboxBorderColor property of the Entity UITheme<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UITheme"."TextboxBorderColor"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 TextboxBorderColor
		{
			get { return (System.Int32)GetValue((int)UIThemeFieldIndex.TextboxBorderColor, true); }
			set	{ SetValue((int)UIThemeFieldIndex.TextboxBorderColor, value, true); }
		}

		/// <summary> The TextboxTextColor property of the Entity UITheme<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UITheme"."TextboxTextColor"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 TextboxTextColor
		{
			get { return (System.Int32)GetValue((int)UIThemeFieldIndex.TextboxTextColor, true); }
			set	{ SetValue((int)UIThemeFieldIndex.TextboxTextColor, value, true); }
		}

		/// <summary> The TextboxCursorColor property of the Entity UITheme<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UITheme"."TextboxCursorColor"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 TextboxCursorColor
		{
			get { return (System.Int32)GetValue((int)UIThemeFieldIndex.TextboxCursorColor, true); }
			set	{ SetValue((int)UIThemeFieldIndex.TextboxCursorColor, value, true); }
		}

		/// <summary> The PageInstructionsTextColor property of the Entity UITheme<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UITheme"."PageInstructionsTextColor"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 PageInstructionsTextColor
		{
			get { return (System.Int32)GetValue((int)UIThemeFieldIndex.PageInstructionsTextColor, true); }
			set	{ SetValue((int)UIThemeFieldIndex.PageInstructionsTextColor, value, true); }
		}

		/// <summary> The DialogTitleTextColor property of the Entity UITheme<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UITheme"."DialogTitleTextColor"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 DialogTitleTextColor
		{
			get { return (System.Int32)GetValue((int)UIThemeFieldIndex.DialogTitleTextColor, true); }
			set	{ SetValue((int)UIThemeFieldIndex.DialogTitleTextColor, value, true); }
		}

		/// <summary> The SpinnerBackgroundColor property of the Entity UITheme<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UITheme"."SpinnerBackgroundColor"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 SpinnerBackgroundColor
		{
			get { return (System.Int32)GetValue((int)UIThemeFieldIndex.SpinnerBackgroundColor, true); }
			set	{ SetValue((int)UIThemeFieldIndex.SpinnerBackgroundColor, value, true); }
		}

		/// <summary> The SpinnerBorderColor property of the Entity UITheme<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UITheme"."SpinnerBorderColor"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 SpinnerBorderColor
		{
			get { return (System.Int32)GetValue((int)UIThemeFieldIndex.SpinnerBorderColor, true); }
			set	{ SetValue((int)UIThemeFieldIndex.SpinnerBorderColor, value, true); }
		}

		/// <summary> The SpinnerTextColor property of the Entity UITheme<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UITheme"."SpinnerTextColor"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 SpinnerTextColor
		{
			get { return (System.Int32)GetValue((int)UIThemeFieldIndex.SpinnerTextColor, true); }
			set	{ SetValue((int)UIThemeFieldIndex.SpinnerTextColor, value, true); }
		}

		/// <summary> The CompanyId property of the Entity UITheme<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UITheme"."CompanyId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> CompanyId
		{
			get { return (Nullable<System.Int32>)GetValue((int)UIThemeFieldIndex.CompanyId, false); }
			set	{ SetValue((int)UIThemeFieldIndex.CompanyId, value, true); }
		}

		/// <summary> The RoomControlButtonOuterBorderColor property of the Entity UITheme<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UITheme"."RoomControlButtonOuterBorderColor"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 RoomControlButtonOuterBorderColor
		{
			get { return (System.Int32)GetValue((int)UIThemeFieldIndex.RoomControlButtonOuterBorderColor, true); }
			set	{ SetValue((int)UIThemeFieldIndex.RoomControlButtonOuterBorderColor, value, true); }
		}

		/// <summary> The RoomControlButtonInnerBorderTopColor property of the Entity UITheme<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UITheme"."RoomControlButtonInnerBorderTopColor"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 RoomControlButtonInnerBorderTopColor
		{
			get { return (System.Int32)GetValue((int)UIThemeFieldIndex.RoomControlButtonInnerBorderTopColor, true); }
			set	{ SetValue((int)UIThemeFieldIndex.RoomControlButtonInnerBorderTopColor, value, true); }
		}

		/// <summary> The RoomControlButtonInnerBorderBottomColor property of the Entity UITheme<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UITheme"."RoomControlButtonInnerBorderBottomColor"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 RoomControlButtonInnerBorderBottomColor
		{
			get { return (System.Int32)GetValue((int)UIThemeFieldIndex.RoomControlButtonInnerBorderBottomColor, true); }
			set	{ SetValue((int)UIThemeFieldIndex.RoomControlButtonInnerBorderBottomColor, value, true); }
		}

		/// <summary> The RoomControlButtonBackgroundTopColor property of the Entity UITheme<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UITheme"."RoomControlButtonBackgroundTopColor"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 RoomControlButtonBackgroundTopColor
		{
			get { return (System.Int32)GetValue((int)UIThemeFieldIndex.RoomControlButtonBackgroundTopColor, true); }
			set	{ SetValue((int)UIThemeFieldIndex.RoomControlButtonBackgroundTopColor, value, true); }
		}

		/// <summary> The RoomControlButtonBackgroundBottomColor property of the Entity UITheme<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UITheme"."RoomControlButtonBackgroundBottomColor"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 RoomControlButtonBackgroundBottomColor
		{
			get { return (System.Int32)GetValue((int)UIThemeFieldIndex.RoomControlButtonBackgroundBottomColor, true); }
			set	{ SetValue((int)UIThemeFieldIndex.RoomControlButtonBackgroundBottomColor, value, true); }
		}

		/// <summary> The RoomControlDividerTopColor property of the Entity UITheme<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UITheme"."RoomControlDividerTopColor"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 RoomControlDividerTopColor
		{
			get { return (System.Int32)GetValue((int)UIThemeFieldIndex.RoomControlDividerTopColor, true); }
			set	{ SetValue((int)UIThemeFieldIndex.RoomControlDividerTopColor, value, true); }
		}

		/// <summary> The RoomControlDividerBottomColor property of the Entity UITheme<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UITheme"."RoomControlDividerBottomColor"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 RoomControlDividerBottomColor
		{
			get { return (System.Int32)GetValue((int)UIThemeFieldIndex.RoomControlDividerBottomColor, true); }
			set	{ SetValue((int)UIThemeFieldIndex.RoomControlDividerBottomColor, value, true); }
		}

		/// <summary> The RoomControlButtonTextColor property of the Entity UITheme<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UITheme"."RoomControlButtonTextColor"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 RoomControlButtonTextColor
		{
			get { return (System.Int32)GetValue((int)UIThemeFieldIndex.RoomControlButtonTextColor, true); }
			set	{ SetValue((int)UIThemeFieldIndex.RoomControlButtonTextColor, value, true); }
		}

		/// <summary> The RoomControlTitleColor property of the Entity UITheme<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UITheme"."RoomControlTitleColor"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 RoomControlTitleColor
		{
			get { return (System.Int32)GetValue((int)UIThemeFieldIndex.RoomControlTitleColor, true); }
			set	{ SetValue((int)UIThemeFieldIndex.RoomControlTitleColor, value, true); }
		}

		/// <summary> The RoomControlTextColor property of the Entity UITheme<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UITheme"."RoomControlTextColor"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 RoomControlTextColor
		{
			get { return (System.Int32)GetValue((int)UIThemeFieldIndex.RoomControlTextColor, true); }
			set	{ SetValue((int)UIThemeFieldIndex.RoomControlTextColor, value, true); }
		}

		/// <summary> The RoomControlPageBackgroundColor property of the Entity UITheme<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UITheme"."RoomControlPageBackgroundColor"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 RoomControlPageBackgroundColor
		{
			get { return (System.Int32)GetValue((int)UIThemeFieldIndex.RoomControlPageBackgroundColor, true); }
			set	{ SetValue((int)UIThemeFieldIndex.RoomControlPageBackgroundColor, value, true); }
		}

		/// <summary> The RoomControlSpinnerBorderColor property of the Entity UITheme<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UITheme"."RoomControlSpinnerBorderColor"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 RoomControlSpinnerBorderColor
		{
			get { return (System.Int32)GetValue((int)UIThemeFieldIndex.RoomControlSpinnerBorderColor, true); }
			set	{ SetValue((int)UIThemeFieldIndex.RoomControlSpinnerBorderColor, value, true); }
		}

		/// <summary> The RoomControlSpinnerBackgroundColor property of the Entity UITheme<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UITheme"."RoomControlSpinnerBackgroundColor"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 RoomControlSpinnerBackgroundColor
		{
			get { return (System.Int32)GetValue((int)UIThemeFieldIndex.RoomControlSpinnerBackgroundColor, true); }
			set	{ SetValue((int)UIThemeFieldIndex.RoomControlSpinnerBackgroundColor, value, true); }
		}

		/// <summary> The RoomControlSpinnerTextColor property of the Entity UITheme<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UITheme"."RoomControlSpinnerTextColor"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 RoomControlSpinnerTextColor
		{
			get { return (System.Int32)GetValue((int)UIThemeFieldIndex.RoomControlSpinnerTextColor, true); }
			set	{ SetValue((int)UIThemeFieldIndex.RoomControlSpinnerTextColor, value, true); }
		}

		/// <summary> The RoomControlButtonIndicatorColor property of the Entity UITheme<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UITheme"."RoomControlButtonIndicatorColor"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 RoomControlButtonIndicatorColor
		{
			get { return (System.Int32)GetValue((int)UIThemeFieldIndex.RoomControlButtonIndicatorColor, true); }
			set	{ SetValue((int)UIThemeFieldIndex.RoomControlButtonIndicatorColor, value, true); }
		}

		/// <summary> The RoomControlButtonIndicatorBackgroundColor property of the Entity UITheme<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UITheme"."RoomControlButtonIndicatorBackgroundColor"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 RoomControlButtonIndicatorBackgroundColor
		{
			get { return (System.Int32)GetValue((int)UIThemeFieldIndex.RoomControlButtonIndicatorBackgroundColor, true); }
			set	{ SetValue((int)UIThemeFieldIndex.RoomControlButtonIndicatorBackgroundColor, value, true); }
		}

		/// <summary> The RoomControlHeaderButtonBackgroundColor property of the Entity UITheme<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UITheme"."RoomControlHeaderButtonBackgroundColor"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 RoomControlHeaderButtonBackgroundColor
		{
			get { return (System.Int32)GetValue((int)UIThemeFieldIndex.RoomControlHeaderButtonBackgroundColor, true); }
			set	{ SetValue((int)UIThemeFieldIndex.RoomControlHeaderButtonBackgroundColor, value, true); }
		}

		/// <summary> The RoomControlHeaderButtonSelectorColor property of the Entity UITheme<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UITheme"."RoomControlHeaderButtonSelectorColor"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 RoomControlHeaderButtonSelectorColor
		{
			get { return (System.Int32)GetValue((int)UIThemeFieldIndex.RoomControlHeaderButtonSelectorColor, true); }
			set	{ SetValue((int)UIThemeFieldIndex.RoomControlHeaderButtonSelectorColor, value, true); }
		}

		/// <summary> The RoomControlHeaderButtonTextColor property of the Entity UITheme<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UITheme"."RoomControlHeaderButtonTextColor"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 RoomControlHeaderButtonTextColor
		{
			get { return (System.Int32)GetValue((int)UIThemeFieldIndex.RoomControlHeaderButtonTextColor, true); }
			set	{ SetValue((int)UIThemeFieldIndex.RoomControlHeaderButtonTextColor, value, true); }
		}

		/// <summary> The RoomControlStationNumberColor property of the Entity UITheme<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UITheme"."RoomControlStationNumberColor"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 RoomControlStationNumberColor
		{
			get { return (System.Int32)GetValue((int)UIThemeFieldIndex.RoomControlStationNumberColor, true); }
			set	{ SetValue((int)UIThemeFieldIndex.RoomControlStationNumberColor, value, true); }
		}

		/// <summary> The RoomControlSliderMinColor property of the Entity UITheme<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UITheme"."RoomControlSliderMinColor"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 RoomControlSliderMinColor
		{
			get { return (System.Int32)GetValue((int)UIThemeFieldIndex.RoomControlSliderMinColor, true); }
			set	{ SetValue((int)UIThemeFieldIndex.RoomControlSliderMinColor, value, true); }
		}

		/// <summary> The RoomControlSliderMaxColor property of the Entity UITheme<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UITheme"."RoomControlSliderMaxColor"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 RoomControlSliderMaxColor
		{
			get { return (System.Int32)GetValue((int)UIThemeFieldIndex.RoomControlSliderMaxColor, true); }
			set	{ SetValue((int)UIThemeFieldIndex.RoomControlSliderMaxColor, value, true); }
		}

		/// <summary> The RoomControlToggleOnTextColor property of the Entity UITheme<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UITheme"."RoomControlToggleOnTextColor"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 RoomControlToggleOnTextColor
		{
			get { return (System.Int32)GetValue((int)UIThemeFieldIndex.RoomControlToggleOnTextColor, true); }
			set	{ SetValue((int)UIThemeFieldIndex.RoomControlToggleOnTextColor, value, true); }
		}

		/// <summary> The RoomControlToggleOffTextColor property of the Entity UITheme<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UITheme"."RoomControlToggleOffTextColor"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 RoomControlToggleOffTextColor
		{
			get { return (System.Int32)GetValue((int)UIThemeFieldIndex.RoomControlToggleOffTextColor, true); }
			set	{ SetValue((int)UIThemeFieldIndex.RoomControlToggleOffTextColor, value, true); }
		}

		/// <summary> The RoomControlToggleBackgroundColor property of the Entity UITheme<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UITheme"."RoomControlToggleBackgroundColor"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 RoomControlToggleBackgroundColor
		{
			get { return (System.Int32)GetValue((int)UIThemeFieldIndex.RoomControlToggleBackgroundColor, true); }
			set	{ SetValue((int)UIThemeFieldIndex.RoomControlToggleBackgroundColor, value, true); }
		}

		/// <summary> The RoomControlToggleBorderColor property of the Entity UITheme<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UITheme"."RoomControlToggleBorderColor"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 RoomControlToggleBorderColor
		{
			get { return (System.Int32)GetValue((int)UIThemeFieldIndex.RoomControlToggleBorderColor, true); }
			set	{ SetValue((int)UIThemeFieldIndex.RoomControlToggleBorderColor, value, true); }
		}

		/// <summary> The RoomControlThermostatComponentColor property of the Entity UITheme<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UITheme"."RoomControlThermostatComponentColor"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 RoomControlThermostatComponentColor
		{
			get { return (System.Int32)GetValue((int)UIThemeFieldIndex.RoomControlThermostatComponentColor, true); }
			set	{ SetValue((int)UIThemeFieldIndex.RoomControlThermostatComponentColor, value, true); }
		}

		/// <summary> The FooterButtonBackgroundTopColor property of the Entity UITheme<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UITheme"."FooterButtonBackgroundTopColor"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 FooterButtonBackgroundTopColor
		{
			get { return (System.Int32)GetValue((int)UIThemeFieldIndex.FooterButtonBackgroundTopColor, true); }
			set	{ SetValue((int)UIThemeFieldIndex.FooterButtonBackgroundTopColor, value, true); }
		}

		/// <summary> The FooterButtonBackgroundBottomColor property of the Entity UITheme<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UITheme"."FooterButtonBackgroundBottomColor"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 FooterButtonBackgroundBottomColor
		{
			get { return (System.Int32)GetValue((int)UIThemeFieldIndex.FooterButtonBackgroundBottomColor, true); }
			set	{ SetValue((int)UIThemeFieldIndex.FooterButtonBackgroundBottomColor, value, true); }
		}

		/// <summary> The FooterButtonBorderColor property of the Entity UITheme<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UITheme"."FooterButtonBorderColor"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 FooterButtonBorderColor
		{
			get { return (System.Int32)GetValue((int)UIThemeFieldIndex.FooterButtonBorderColor, true); }
			set	{ SetValue((int)UIThemeFieldIndex.FooterButtonBorderColor, value, true); }
		}

		/// <summary> The FooterButtonTextColor property of the Entity UITheme<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UITheme"."FooterButtonTextColor"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 FooterButtonTextColor
		{
			get { return (System.Int32)GetValue((int)UIThemeFieldIndex.FooterButtonTextColor, true); }
			set	{ SetValue((int)UIThemeFieldIndex.FooterButtonTextColor, value, true); }
		}

		/// <summary> The FooterButtonDisabledBackgroundTopColor property of the Entity UITheme<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UITheme"."FooterButtonDisabledBackgroundTopColor"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 FooterButtonDisabledBackgroundTopColor
		{
			get { return (System.Int32)GetValue((int)UIThemeFieldIndex.FooterButtonDisabledBackgroundTopColor, true); }
			set	{ SetValue((int)UIThemeFieldIndex.FooterButtonDisabledBackgroundTopColor, value, true); }
		}

		/// <summary> The FooterButtonDisabledBackgroundBottomColor property of the Entity UITheme<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UITheme"."FooterButtonDisabledBackgroundBottomColor"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 FooterButtonDisabledBackgroundBottomColor
		{
			get { return (System.Int32)GetValue((int)UIThemeFieldIndex.FooterButtonDisabledBackgroundBottomColor, true); }
			set	{ SetValue((int)UIThemeFieldIndex.FooterButtonDisabledBackgroundBottomColor, value, true); }
		}

		/// <summary> The FooterButtonDisabledBorderColor property of the Entity UITheme<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UITheme"."FooterButtonDisabledBorderColor"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 FooterButtonDisabledBorderColor
		{
			get { return (System.Int32)GetValue((int)UIThemeFieldIndex.FooterButtonDisabledBorderColor, true); }
			set	{ SetValue((int)UIThemeFieldIndex.FooterButtonDisabledBorderColor, value, true); }
		}

		/// <summary> The FooterButtonDisabledTextColor property of the Entity UITheme<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UITheme"."FooterButtonDisabledTextColor"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 FooterButtonDisabledTextColor
		{
			get { return (System.Int32)GetValue((int)UIThemeFieldIndex.FooterButtonDisabledTextColor, true); }
			set	{ SetValue((int)UIThemeFieldIndex.FooterButtonDisabledTextColor, value, true); }
		}

		/// <summary> The ListCategoryL2BackgroundColor property of the Entity UITheme<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UITheme"."ListCategoryL2BackgroundColor"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 ListCategoryL2BackgroundColor
		{
			get { return (System.Int32)GetValue((int)UIThemeFieldIndex.ListCategoryL2BackgroundColor, true); }
			set	{ SetValue((int)UIThemeFieldIndex.ListCategoryL2BackgroundColor, value, true); }
		}

		/// <summary> The ListCategoryL3BackgroundColor property of the Entity UITheme<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UITheme"."ListCategoryL3BackgroundColor"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 ListCategoryL3BackgroundColor
		{
			get { return (System.Int32)GetValue((int)UIThemeFieldIndex.ListCategoryL3BackgroundColor, true); }
			set	{ SetValue((int)UIThemeFieldIndex.ListCategoryL3BackgroundColor, value, true); }
		}

		/// <summary> The ListCategoryL4BackgroundColor property of the Entity UITheme<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UITheme"."ListCategoryL4BackgroundColor"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 ListCategoryL4BackgroundColor
		{
			get { return (System.Int32)GetValue((int)UIThemeFieldIndex.ListCategoryL4BackgroundColor, true); }
			set	{ SetValue((int)UIThemeFieldIndex.ListCategoryL4BackgroundColor, value, true); }
		}

		/// <summary> The ListCategoryL5BackgroundColor property of the Entity UITheme<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UITheme"."ListCategoryL5BackgroundColor"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 ListCategoryL5BackgroundColor
		{
			get { return (System.Int32)GetValue((int)UIThemeFieldIndex.ListCategoryL5BackgroundColor, true); }
			set	{ SetValue((int)UIThemeFieldIndex.ListCategoryL5BackgroundColor, value, true); }
		}

		/// <summary> The ListItemPriceTextColor property of the Entity UITheme<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UITheme"."ListItemPriceTextColor"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 ListItemPriceTextColor
		{
			get { return (System.Int32)GetValue((int)UIThemeFieldIndex.ListItemPriceTextColor, true); }
			set	{ SetValue((int)UIThemeFieldIndex.ListItemPriceTextColor, value, true); }
		}

		/// <summary> The ListItemSelectedPriceTextColor property of the Entity UITheme<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UITheme"."ListItemSelectedPriceTextColor"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 ListItemSelectedPriceTextColor
		{
			get { return (System.Int32)GetValue((int)UIThemeFieldIndex.ListItemSelectedPriceTextColor, true); }
			set	{ SetValue((int)UIThemeFieldIndex.ListItemSelectedPriceTextColor, value, true); }
		}

		/// <summary> The AlterationDialogBackgroundColor property of the Entity UITheme<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UITheme"."AlterationDialogBackgroundColor"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 AlterationDialogBackgroundColor
		{
			get { return (System.Int32)GetValue((int)UIThemeFieldIndex.AlterationDialogBackgroundColor, true); }
			set	{ SetValue((int)UIThemeFieldIndex.AlterationDialogBackgroundColor, value, true); }
		}

		/// <summary> The AlterationDialogBorderColor property of the Entity UITheme<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UITheme"."AlterationDialogBorderColor"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 AlterationDialogBorderColor
		{
			get { return (System.Int32)GetValue((int)UIThemeFieldIndex.AlterationDialogBorderColor, true); }
			set	{ SetValue((int)UIThemeFieldIndex.AlterationDialogBorderColor, value, true); }
		}

		/// <summary> The AlterationDialogPanelBackgroundColor property of the Entity UITheme<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UITheme"."AlterationDialogPanelBackgroundColor"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 AlterationDialogPanelBackgroundColor
		{
			get { return (System.Int32)GetValue((int)UIThemeFieldIndex.AlterationDialogPanelBackgroundColor, true); }
			set	{ SetValue((int)UIThemeFieldIndex.AlterationDialogPanelBackgroundColor, value, true); }
		}

		/// <summary> The AlterationDialogListItemBackgroundColor property of the Entity UITheme<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UITheme"."AlterationDialogListItemBackgroundColor"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 AlterationDialogListItemBackgroundColor
		{
			get { return (System.Int32)GetValue((int)UIThemeFieldIndex.AlterationDialogListItemBackgroundColor, true); }
			set	{ SetValue((int)UIThemeFieldIndex.AlterationDialogListItemBackgroundColor, value, true); }
		}

		/// <summary> The AlterationDialogListItemSelectedBackgroundColor property of the Entity UITheme<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UITheme"."AlterationDialogListItemSelectedBackgroundColor"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 AlterationDialogListItemSelectedBackgroundColor
		{
			get { return (System.Int32)GetValue((int)UIThemeFieldIndex.AlterationDialogListItemSelectedBackgroundColor, true); }
			set	{ SetValue((int)UIThemeFieldIndex.AlterationDialogListItemSelectedBackgroundColor, value, true); }
		}

		/// <summary> The AlterationDialogListItemSelectedBorderColor property of the Entity UITheme<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UITheme"."AlterationDialogListItemSelectedBorderColor"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 AlterationDialogListItemSelectedBorderColor
		{
			get { return (System.Int32)GetValue((int)UIThemeFieldIndex.AlterationDialogListItemSelectedBorderColor, true); }
			set	{ SetValue((int)UIThemeFieldIndex.AlterationDialogListItemSelectedBorderColor, value, true); }
		}

		/// <summary> The AlterationDialogPrimaryTextColor property of the Entity UITheme<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UITheme"."AlterationDialogPrimaryTextColor"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 AlterationDialogPrimaryTextColor
		{
			get { return (System.Int32)GetValue((int)UIThemeFieldIndex.AlterationDialogPrimaryTextColor, true); }
			set	{ SetValue((int)UIThemeFieldIndex.AlterationDialogPrimaryTextColor, value, true); }
		}

		/// <summary> The AlterationDialogSecondaryTextColor property of the Entity UITheme<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UITheme"."AlterationDialogSecondaryTextColor"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 AlterationDialogSecondaryTextColor
		{
			get { return (System.Int32)GetValue((int)UIThemeFieldIndex.AlterationDialogSecondaryTextColor, true); }
			set	{ SetValue((int)UIThemeFieldIndex.AlterationDialogSecondaryTextColor, value, true); }
		}

		/// <summary> The AlterationDialogInputBackgroundColor property of the Entity UITheme<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UITheme"."AlterationDialogInputBackgroundColor"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 AlterationDialogInputBackgroundColor
		{
			get { return (System.Int32)GetValue((int)UIThemeFieldIndex.AlterationDialogInputBackgroundColor, true); }
			set	{ SetValue((int)UIThemeFieldIndex.AlterationDialogInputBackgroundColor, value, true); }
		}

		/// <summary> The AlterationDialogInputBorderColor property of the Entity UITheme<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UITheme"."AlterationDialogInputBorderColor"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 AlterationDialogInputBorderColor
		{
			get { return (System.Int32)GetValue((int)UIThemeFieldIndex.AlterationDialogInputBorderColor, true); }
			set	{ SetValue((int)UIThemeFieldIndex.AlterationDialogInputBorderColor, value, true); }
		}

		/// <summary> The AlterationDialogInputTextColor property of the Entity UITheme<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UITheme"."AlterationDialogInputTextColor"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 AlterationDialogInputTextColor
		{
			get { return (System.Int32)GetValue((int)UIThemeFieldIndex.AlterationDialogInputTextColor, true); }
			set	{ SetValue((int)UIThemeFieldIndex.AlterationDialogInputTextColor, value, true); }
		}

		/// <summary> The AlterationDialogInputHintTextColor property of the Entity UITheme<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UITheme"."AlterationDialogInputHintTextColor"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 AlterationDialogInputHintTextColor
		{
			get { return (System.Int32)GetValue((int)UIThemeFieldIndex.AlterationDialogInputHintTextColor, true); }
			set	{ SetValue((int)UIThemeFieldIndex.AlterationDialogInputHintTextColor, value, true); }
		}

		/// <summary> The AlterationDialogPanelBorderColor property of the Entity UITheme<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UITheme"."AlterationDialogPanelBorderColor"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 AlterationDialogPanelBorderColor
		{
			get { return (System.Int32)GetValue((int)UIThemeFieldIndex.AlterationDialogPanelBorderColor, true); }
			set	{ SetValue((int)UIThemeFieldIndex.AlterationDialogPanelBorderColor, value, true); }
		}

		/// <summary> The AlterationDialogRadioButtonColor property of the Entity UITheme<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UITheme"."AlterationDialogRadioButtonColor"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 AlterationDialogRadioButtonColor
		{
			get { return (System.Int32)GetValue((int)UIThemeFieldIndex.AlterationDialogRadioButtonColor, true); }
			set	{ SetValue((int)UIThemeFieldIndex.AlterationDialogRadioButtonColor, value, true); }
		}

		/// <summary> The AlterationDialogCheckBoxColor property of the Entity UITheme<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UITheme"."AlterationDialogCheckBoxColor"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 AlterationDialogCheckBoxColor
		{
			get { return (System.Int32)GetValue((int)UIThemeFieldIndex.AlterationDialogCheckBoxColor, true); }
			set	{ SetValue((int)UIThemeFieldIndex.AlterationDialogCheckBoxColor, value, true); }
		}

		/// <summary> The AlterationDialogCloseButtonColor property of the Entity UITheme<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UITheme"."AlterationDialogCloseButtonColor"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 AlterationDialogCloseButtonColor
		{
			get { return (System.Int32)GetValue((int)UIThemeFieldIndex.AlterationDialogCloseButtonColor, true); }
			set	{ SetValue((int)UIThemeFieldIndex.AlterationDialogCloseButtonColor, value, true); }
		}

		/// <summary> The AlterationDialogListViewArrowColor property of the Entity UITheme<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UITheme"."AlterationDialogListViewArrowColor"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 AlterationDialogListViewArrowColor
		{
			get { return (System.Int32)GetValue((int)UIThemeFieldIndex.AlterationDialogListViewArrowColor, true); }
			set	{ SetValue((int)UIThemeFieldIndex.AlterationDialogListViewArrowColor, value, true); }
		}

		/// <summary> The AlterationDialogListViewDividerColor property of the Entity UITheme<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UITheme"."AlterationDialogListViewDividerColor"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 AlterationDialogListViewDividerColor
		{
			get { return (System.Int32)GetValue((int)UIThemeFieldIndex.AlterationDialogListViewDividerColor, true); }
			set	{ SetValue((int)UIThemeFieldIndex.AlterationDialogListViewDividerColor, value, true); }
		}

		/// <summary> The AreaTabActiveText property of the Entity UITheme<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UITheme"."AreaTabActiveText"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 AreaTabActiveText
		{
			get { return (System.Int32)GetValue((int)UIThemeFieldIndex.AreaTabActiveText, true); }
			set	{ SetValue((int)UIThemeFieldIndex.AreaTabActiveText, value, true); }
		}

		/// <summary> The AreaTabInactiveText property of the Entity UITheme<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UITheme"."AreaTabInactiveText"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 AreaTabInactiveText
		{
			get { return (System.Int32)GetValue((int)UIThemeFieldIndex.AreaTabInactiveText, true); }
			set	{ SetValue((int)UIThemeFieldIndex.AreaTabInactiveText, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'ClientConfigurationEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiClientConfigurationCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.ClientConfigurationCollection ClientConfigurationCollection
		{
			get	{ return GetMultiClientConfigurationCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ClientConfigurationCollection. When set to true, ClientConfigurationCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ClientConfigurationCollection is accessed. You can always execute/ a forced fetch by calling GetMultiClientConfigurationCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchClientConfigurationCollection
		{
			get	{ return _alwaysFetchClientConfigurationCollection; }
			set	{ _alwaysFetchClientConfigurationCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property ClientConfigurationCollection already has been fetched. Setting this property to false when ClientConfigurationCollection has been fetched
		/// will clear the ClientConfigurationCollection collection well. Setting this property to true while ClientConfigurationCollection hasn't been fetched disables lazy loading for ClientConfigurationCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedClientConfigurationCollection
		{
			get { return _alreadyFetchedClientConfigurationCollection;}
			set 
			{
				if(_alreadyFetchedClientConfigurationCollection && !value && (_clientConfigurationCollection != null))
				{
					_clientConfigurationCollection.Clear();
				}
				_alreadyFetchedClientConfigurationCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'DeliverypointgroupEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiDeliverypointgroupCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.DeliverypointgroupCollection DeliverypointgroupCollection
		{
			get	{ return GetMultiDeliverypointgroupCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for DeliverypointgroupCollection. When set to true, DeliverypointgroupCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time DeliverypointgroupCollection is accessed. You can always execute/ a forced fetch by calling GetMultiDeliverypointgroupCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchDeliverypointgroupCollection
		{
			get	{ return _alwaysFetchDeliverypointgroupCollection; }
			set	{ _alwaysFetchDeliverypointgroupCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property DeliverypointgroupCollection already has been fetched. Setting this property to false when DeliverypointgroupCollection has been fetched
		/// will clear the DeliverypointgroupCollection collection well. Setting this property to true while DeliverypointgroupCollection hasn't been fetched disables lazy loading for DeliverypointgroupCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedDeliverypointgroupCollection
		{
			get { return _alreadyFetchedDeliverypointgroupCollection;}
			set 
			{
				if(_alreadyFetchedDeliverypointgroupCollection && !value && (_deliverypointgroupCollection != null))
				{
					_deliverypointgroupCollection.Clear();
				}
				_alreadyFetchedDeliverypointgroupCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'MediaEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiMediaCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.MediaCollection MediaCollection
		{
			get	{ return GetMultiMediaCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for MediaCollection. When set to true, MediaCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time MediaCollection is accessed. You can always execute/ a forced fetch by calling GetMultiMediaCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchMediaCollection
		{
			get	{ return _alwaysFetchMediaCollection; }
			set	{ _alwaysFetchMediaCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property MediaCollection already has been fetched. Setting this property to false when MediaCollection has been fetched
		/// will clear the MediaCollection collection well. Setting this property to true while MediaCollection hasn't been fetched disables lazy loading for MediaCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedMediaCollection
		{
			get { return _alreadyFetchedMediaCollection;}
			set 
			{
				if(_alreadyFetchedMediaCollection && !value && (_mediaCollection != null))
				{
					_mediaCollection.Clear();
				}
				_alreadyFetchedMediaCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'TimestampEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiTimestampCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.TimestampCollection TimestampCollection
		{
			get	{ return GetMultiTimestampCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for TimestampCollection. When set to true, TimestampCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TimestampCollection is accessed. You can always execute/ a forced fetch by calling GetMultiTimestampCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTimestampCollection
		{
			get	{ return _alwaysFetchTimestampCollection; }
			set	{ _alwaysFetchTimestampCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property TimestampCollection already has been fetched. Setting this property to false when TimestampCollection has been fetched
		/// will clear the TimestampCollection collection well. Setting this property to true while TimestampCollection hasn't been fetched disables lazy loading for TimestampCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTimestampCollection
		{
			get { return _alreadyFetchedTimestampCollection;}
			set 
			{
				if(_alreadyFetchedTimestampCollection && !value && (_timestampCollection != null))
				{
					_timestampCollection.Clear();
				}
				_alreadyFetchedTimestampCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'UIThemeColorEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiUIThemeColorCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.UIThemeColorCollection UIThemeColorCollection
		{
			get	{ return GetMultiUIThemeColorCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for UIThemeColorCollection. When set to true, UIThemeColorCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time UIThemeColorCollection is accessed. You can always execute/ a forced fetch by calling GetMultiUIThemeColorCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchUIThemeColorCollection
		{
			get	{ return _alwaysFetchUIThemeColorCollection; }
			set	{ _alwaysFetchUIThemeColorCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property UIThemeColorCollection already has been fetched. Setting this property to false when UIThemeColorCollection has been fetched
		/// will clear the UIThemeColorCollection collection well. Setting this property to true while UIThemeColorCollection hasn't been fetched disables lazy loading for UIThemeColorCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedUIThemeColorCollection
		{
			get { return _alreadyFetchedUIThemeColorCollection;}
			set 
			{
				if(_alreadyFetchedUIThemeColorCollection && !value && (_uIThemeColorCollection != null))
				{
					_uIThemeColorCollection.Clear();
				}
				_alreadyFetchedUIThemeColorCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'UIThemeTextSizeEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiUIThemeTextSizeCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.UIThemeTextSizeCollection UIThemeTextSizeCollection
		{
			get	{ return GetMultiUIThemeTextSizeCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for UIThemeTextSizeCollection. When set to true, UIThemeTextSizeCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time UIThemeTextSizeCollection is accessed. You can always execute/ a forced fetch by calling GetMultiUIThemeTextSizeCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchUIThemeTextSizeCollection
		{
			get	{ return _alwaysFetchUIThemeTextSizeCollection; }
			set	{ _alwaysFetchUIThemeTextSizeCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property UIThemeTextSizeCollection already has been fetched. Setting this property to false when UIThemeTextSizeCollection has been fetched
		/// will clear the UIThemeTextSizeCollection collection well. Setting this property to true while UIThemeTextSizeCollection hasn't been fetched disables lazy loading for UIThemeTextSizeCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedUIThemeTextSizeCollection
		{
			get { return _alreadyFetchedUIThemeTextSizeCollection;}
			set 
			{
				if(_alreadyFetchedUIThemeTextSizeCollection && !value && (_uIThemeTextSizeCollection != null))
				{
					_uIThemeTextSizeCollection.Clear();
				}
				_alreadyFetchedUIThemeTextSizeCollection = value;
			}
		}

		/// <summary> Gets / sets related entity of type 'CompanyEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleCompanyEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual CompanyEntity CompanyEntity
		{
			get	{ return GetSingleCompanyEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncCompanyEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "UIThemeCollection", "CompanyEntity", _companyEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for CompanyEntity. When set to true, CompanyEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CompanyEntity is accessed. You can always execute a forced fetch by calling GetSingleCompanyEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCompanyEntity
		{
			get	{ return _alwaysFetchCompanyEntity; }
			set	{ _alwaysFetchCompanyEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CompanyEntity already has been fetched. Setting this property to false when CompanyEntity has been fetched
		/// will set CompanyEntity to null as well. Setting this property to true while CompanyEntity hasn't been fetched disables lazy loading for CompanyEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCompanyEntity
		{
			get { return _alreadyFetchedCompanyEntity;}
			set 
			{
				if(_alreadyFetchedCompanyEntity && !value)
				{
					this.CompanyEntity = null;
				}
				_alreadyFetchedCompanyEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property CompanyEntity is not found
		/// in the database. When set to true, CompanyEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool CompanyEntityReturnsNewIfNotFound
		{
			get	{ return _companyEntityReturnsNewIfNotFound; }
			set { _companyEntityReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the Obymobi.Data.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)Obymobi.Data.EntityType.UIThemeEntity; }
		}

		#endregion

		
		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
