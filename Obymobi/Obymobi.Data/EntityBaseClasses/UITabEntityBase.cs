﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
using System.Runtime.Serialization;
using System.Data;
using System.Xml.Serialization;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity base class which represents the base class for the entity 'UITab'.<br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public abstract partial class UITabEntityBase : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		public new string LLBLGenProEntityName {
			get { return "UITabEntity"; }
		}
	
		#region Class Member Declarations
		private Obymobi.Data.CollectionClasses.CustomTextCollection	_customTextCollection;
		private bool	_alwaysFetchCustomTextCollection, _alreadyFetchedCustomTextCollection;
		private Obymobi.Data.CollectionClasses.UIModeCollection	_uIModeCollection;
		private bool	_alwaysFetchUIModeCollection, _alreadyFetchedUIModeCollection;
		private Obymobi.Data.CollectionClasses.UITabLanguageCollection	_uITabLanguageCollection;
		private bool	_alwaysFetchUITabLanguageCollection, _alreadyFetchedUITabLanguageCollection;
		private Obymobi.Data.CollectionClasses.UIWidgetCollection	_uIWidgetCollection;
		private bool	_alwaysFetchUIWidgetCollection, _alreadyFetchedUIWidgetCollection;
		private Obymobi.Data.CollectionClasses.CompanyCollection _companyCollectionViaUIMode;
		private bool	_alwaysFetchCompanyCollectionViaUIMode, _alreadyFetchedCompanyCollectionViaUIMode;
		private Obymobi.Data.CollectionClasses.LanguageCollection _languageCollectionViaUITabLanguage;
		private bool	_alwaysFetchLanguageCollectionViaUITabLanguage, _alreadyFetchedLanguageCollectionViaUITabLanguage;
		private CategoryEntity _categoryEntity;
		private bool	_alwaysFetchCategoryEntity, _alreadyFetchedCategoryEntity, _categoryEntityReturnsNewIfNotFound;
		private EntertainmentEntity _entertainmentEntity;
		private bool	_alwaysFetchEntertainmentEntity, _alreadyFetchedEntertainmentEntity, _entertainmentEntityReturnsNewIfNotFound;
		private MapEntity _mapEntity;
		private bool	_alwaysFetchMapEntity, _alreadyFetchedMapEntity, _mapEntityReturnsNewIfNotFound;
		private SiteEntity _siteEntity;
		private bool	_alwaysFetchSiteEntity, _alreadyFetchedSiteEntity, _siteEntityReturnsNewIfNotFound;
		private UIModeEntity _uIModeEntity;
		private bool	_alwaysFetchUIModeEntity, _alreadyFetchedUIModeEntity, _uIModeEntityReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name CategoryEntity</summary>
			public static readonly string CategoryEntity = "CategoryEntity";
			/// <summary>Member name EntertainmentEntity</summary>
			public static readonly string EntertainmentEntity = "EntertainmentEntity";
			/// <summary>Member name MapEntity</summary>
			public static readonly string MapEntity = "MapEntity";
			/// <summary>Member name SiteEntity</summary>
			public static readonly string SiteEntity = "SiteEntity";
			/// <summary>Member name UIModeEntity</summary>
			public static readonly string UIModeEntity = "UIModeEntity";
			/// <summary>Member name CustomTextCollection</summary>
			public static readonly string CustomTextCollection = "CustomTextCollection";
			/// <summary>Member name UIModeCollection</summary>
			public static readonly string UIModeCollection = "UIModeCollection";
			/// <summary>Member name UITabLanguageCollection</summary>
			public static readonly string UITabLanguageCollection = "UITabLanguageCollection";
			/// <summary>Member name UIWidgetCollection</summary>
			public static readonly string UIWidgetCollection = "UIWidgetCollection";
			/// <summary>Member name CompanyCollectionViaUIMode</summary>
			public static readonly string CompanyCollectionViaUIMode = "CompanyCollectionViaUIMode";
			/// <summary>Member name LanguageCollectionViaUITabLanguage</summary>
			public static readonly string LanguageCollectionViaUITabLanguage = "LanguageCollectionViaUITabLanguage";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static UITabEntityBase()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		protected UITabEntityBase() :base("UITabEntity")
		{
			InitClassEmpty(null);
		}

		/// <summary>CTor</summary>
		/// <param name="uITabId">PK value for UITab which data should be fetched into this UITab object</param>
		protected UITabEntityBase(System.Int32 uITabId):base("UITabEntity")
		{
			InitClassFetch(uITabId, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="uITabId">PK value for UITab which data should be fetched into this UITab object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		protected UITabEntityBase(System.Int32 uITabId, IPrefetchPath prefetchPathToUse): base("UITabEntity")
		{
			InitClassFetch(uITabId, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="uITabId">PK value for UITab which data should be fetched into this UITab object</param>
		/// <param name="validator">The custom validator object for this UITabEntity</param>
		protected UITabEntityBase(System.Int32 uITabId, IValidator validator):base("UITabEntity")
		{
			InitClassFetch(uITabId, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected UITabEntityBase(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_customTextCollection = (Obymobi.Data.CollectionClasses.CustomTextCollection)info.GetValue("_customTextCollection", typeof(Obymobi.Data.CollectionClasses.CustomTextCollection));
			_alwaysFetchCustomTextCollection = info.GetBoolean("_alwaysFetchCustomTextCollection");
			_alreadyFetchedCustomTextCollection = info.GetBoolean("_alreadyFetchedCustomTextCollection");

			_uIModeCollection = (Obymobi.Data.CollectionClasses.UIModeCollection)info.GetValue("_uIModeCollection", typeof(Obymobi.Data.CollectionClasses.UIModeCollection));
			_alwaysFetchUIModeCollection = info.GetBoolean("_alwaysFetchUIModeCollection");
			_alreadyFetchedUIModeCollection = info.GetBoolean("_alreadyFetchedUIModeCollection");

			_uITabLanguageCollection = (Obymobi.Data.CollectionClasses.UITabLanguageCollection)info.GetValue("_uITabLanguageCollection", typeof(Obymobi.Data.CollectionClasses.UITabLanguageCollection));
			_alwaysFetchUITabLanguageCollection = info.GetBoolean("_alwaysFetchUITabLanguageCollection");
			_alreadyFetchedUITabLanguageCollection = info.GetBoolean("_alreadyFetchedUITabLanguageCollection");

			_uIWidgetCollection = (Obymobi.Data.CollectionClasses.UIWidgetCollection)info.GetValue("_uIWidgetCollection", typeof(Obymobi.Data.CollectionClasses.UIWidgetCollection));
			_alwaysFetchUIWidgetCollection = info.GetBoolean("_alwaysFetchUIWidgetCollection");
			_alreadyFetchedUIWidgetCollection = info.GetBoolean("_alreadyFetchedUIWidgetCollection");
			_companyCollectionViaUIMode = (Obymobi.Data.CollectionClasses.CompanyCollection)info.GetValue("_companyCollectionViaUIMode", typeof(Obymobi.Data.CollectionClasses.CompanyCollection));
			_alwaysFetchCompanyCollectionViaUIMode = info.GetBoolean("_alwaysFetchCompanyCollectionViaUIMode");
			_alreadyFetchedCompanyCollectionViaUIMode = info.GetBoolean("_alreadyFetchedCompanyCollectionViaUIMode");

			_languageCollectionViaUITabLanguage = (Obymobi.Data.CollectionClasses.LanguageCollection)info.GetValue("_languageCollectionViaUITabLanguage", typeof(Obymobi.Data.CollectionClasses.LanguageCollection));
			_alwaysFetchLanguageCollectionViaUITabLanguage = info.GetBoolean("_alwaysFetchLanguageCollectionViaUITabLanguage");
			_alreadyFetchedLanguageCollectionViaUITabLanguage = info.GetBoolean("_alreadyFetchedLanguageCollectionViaUITabLanguage");
			_categoryEntity = (CategoryEntity)info.GetValue("_categoryEntity", typeof(CategoryEntity));
			if(_categoryEntity!=null)
			{
				_categoryEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_categoryEntityReturnsNewIfNotFound = info.GetBoolean("_categoryEntityReturnsNewIfNotFound");
			_alwaysFetchCategoryEntity = info.GetBoolean("_alwaysFetchCategoryEntity");
			_alreadyFetchedCategoryEntity = info.GetBoolean("_alreadyFetchedCategoryEntity");

			_entertainmentEntity = (EntertainmentEntity)info.GetValue("_entertainmentEntity", typeof(EntertainmentEntity));
			if(_entertainmentEntity!=null)
			{
				_entertainmentEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_entertainmentEntityReturnsNewIfNotFound = info.GetBoolean("_entertainmentEntityReturnsNewIfNotFound");
			_alwaysFetchEntertainmentEntity = info.GetBoolean("_alwaysFetchEntertainmentEntity");
			_alreadyFetchedEntertainmentEntity = info.GetBoolean("_alreadyFetchedEntertainmentEntity");

			_mapEntity = (MapEntity)info.GetValue("_mapEntity", typeof(MapEntity));
			if(_mapEntity!=null)
			{
				_mapEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_mapEntityReturnsNewIfNotFound = info.GetBoolean("_mapEntityReturnsNewIfNotFound");
			_alwaysFetchMapEntity = info.GetBoolean("_alwaysFetchMapEntity");
			_alreadyFetchedMapEntity = info.GetBoolean("_alreadyFetchedMapEntity");

			_siteEntity = (SiteEntity)info.GetValue("_siteEntity", typeof(SiteEntity));
			if(_siteEntity!=null)
			{
				_siteEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_siteEntityReturnsNewIfNotFound = info.GetBoolean("_siteEntityReturnsNewIfNotFound");
			_alwaysFetchSiteEntity = info.GetBoolean("_alwaysFetchSiteEntity");
			_alreadyFetchedSiteEntity = info.GetBoolean("_alreadyFetchedSiteEntity");

			_uIModeEntity = (UIModeEntity)info.GetValue("_uIModeEntity", typeof(UIModeEntity));
			if(_uIModeEntity!=null)
			{
				_uIModeEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_uIModeEntityReturnsNewIfNotFound = info.GetBoolean("_uIModeEntityReturnsNewIfNotFound");
			_alwaysFetchUIModeEntity = info.GetBoolean("_alwaysFetchUIModeEntity");
			_alreadyFetchedUIModeEntity = info.GetBoolean("_alreadyFetchedUIModeEntity");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}	
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((UITabFieldIndex)fieldIndex)
			{
				case UITabFieldIndex.UIModeId:
					DesetupSyncUIModeEntity(true, false);
					_alreadyFetchedUIModeEntity = false;
					break;
				case UITabFieldIndex.CategoryId:
					DesetupSyncCategoryEntity(true, false);
					_alreadyFetchedCategoryEntity = false;
					break;
				case UITabFieldIndex.EntertainmentId:
					DesetupSyncEntertainmentEntity(true, false);
					_alreadyFetchedEntertainmentEntity = false;
					break;
				case UITabFieldIndex.SiteId:
					DesetupSyncSiteEntity(true, false);
					_alreadyFetchedSiteEntity = false;
					break;
				case UITabFieldIndex.MapId:
					DesetupSyncMapEntity(true, false);
					_alreadyFetchedMapEntity = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedCustomTextCollection = (_customTextCollection.Count > 0);
			_alreadyFetchedUIModeCollection = (_uIModeCollection.Count > 0);
			_alreadyFetchedUITabLanguageCollection = (_uITabLanguageCollection.Count > 0);
			_alreadyFetchedUIWidgetCollection = (_uIWidgetCollection.Count > 0);
			_alreadyFetchedCompanyCollectionViaUIMode = (_companyCollectionViaUIMode.Count > 0);
			_alreadyFetchedLanguageCollectionViaUITabLanguage = (_languageCollectionViaUITabLanguage.Count > 0);
			_alreadyFetchedCategoryEntity = (_categoryEntity != null);
			_alreadyFetchedEntertainmentEntity = (_entertainmentEntity != null);
			_alreadyFetchedMapEntity = (_mapEntity != null);
			_alreadyFetchedSiteEntity = (_siteEntity != null);
			_alreadyFetchedUIModeEntity = (_uIModeEntity != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "CategoryEntity":
					toReturn.Add(Relations.CategoryEntityUsingCategoryId);
					break;
				case "EntertainmentEntity":
					toReturn.Add(Relations.EntertainmentEntityUsingEntertainmentId);
					break;
				case "MapEntity":
					toReturn.Add(Relations.MapEntityUsingMapId);
					break;
				case "SiteEntity":
					toReturn.Add(Relations.SiteEntityUsingSiteId);
					break;
				case "UIModeEntity":
					toReturn.Add(Relations.UIModeEntityUsingUIModeId);
					break;
				case "CustomTextCollection":
					toReturn.Add(Relations.CustomTextEntityUsingUITabId);
					break;
				case "UIModeCollection":
					toReturn.Add(Relations.UIModeEntityUsingDefaultUITabId);
					break;
				case "UITabLanguageCollection":
					toReturn.Add(Relations.UITabLanguageEntityUsingUITabId);
					break;
				case "UIWidgetCollection":
					toReturn.Add(Relations.UIWidgetEntityUsingUITabId);
					break;
				case "CompanyCollectionViaUIMode":
					toReturn.Add(Relations.UIModeEntityUsingDefaultUITabId, "UITabEntity__", "UIMode_", JoinHint.None);
					toReturn.Add(UIModeEntity.Relations.CompanyEntityUsingCompanyId, "UIMode_", string.Empty, JoinHint.None);
					break;
				case "LanguageCollectionViaUITabLanguage":
					toReturn.Add(Relations.UITabLanguageEntityUsingUITabId, "UITabEntity__", "UITabLanguage_", JoinHint.None);
					toReturn.Add(UITabLanguageEntity.Relations.LanguageEntityUsingLanguageId, "UITabLanguage_", string.Empty, JoinHint.None);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_customTextCollection", (!this.MarkedForDeletion?_customTextCollection:null));
			info.AddValue("_alwaysFetchCustomTextCollection", _alwaysFetchCustomTextCollection);
			info.AddValue("_alreadyFetchedCustomTextCollection", _alreadyFetchedCustomTextCollection);
			info.AddValue("_uIModeCollection", (!this.MarkedForDeletion?_uIModeCollection:null));
			info.AddValue("_alwaysFetchUIModeCollection", _alwaysFetchUIModeCollection);
			info.AddValue("_alreadyFetchedUIModeCollection", _alreadyFetchedUIModeCollection);
			info.AddValue("_uITabLanguageCollection", (!this.MarkedForDeletion?_uITabLanguageCollection:null));
			info.AddValue("_alwaysFetchUITabLanguageCollection", _alwaysFetchUITabLanguageCollection);
			info.AddValue("_alreadyFetchedUITabLanguageCollection", _alreadyFetchedUITabLanguageCollection);
			info.AddValue("_uIWidgetCollection", (!this.MarkedForDeletion?_uIWidgetCollection:null));
			info.AddValue("_alwaysFetchUIWidgetCollection", _alwaysFetchUIWidgetCollection);
			info.AddValue("_alreadyFetchedUIWidgetCollection", _alreadyFetchedUIWidgetCollection);
			info.AddValue("_companyCollectionViaUIMode", (!this.MarkedForDeletion?_companyCollectionViaUIMode:null));
			info.AddValue("_alwaysFetchCompanyCollectionViaUIMode", _alwaysFetchCompanyCollectionViaUIMode);
			info.AddValue("_alreadyFetchedCompanyCollectionViaUIMode", _alreadyFetchedCompanyCollectionViaUIMode);
			info.AddValue("_languageCollectionViaUITabLanguage", (!this.MarkedForDeletion?_languageCollectionViaUITabLanguage:null));
			info.AddValue("_alwaysFetchLanguageCollectionViaUITabLanguage", _alwaysFetchLanguageCollectionViaUITabLanguage);
			info.AddValue("_alreadyFetchedLanguageCollectionViaUITabLanguage", _alreadyFetchedLanguageCollectionViaUITabLanguage);
			info.AddValue("_categoryEntity", (!this.MarkedForDeletion?_categoryEntity:null));
			info.AddValue("_categoryEntityReturnsNewIfNotFound", _categoryEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchCategoryEntity", _alwaysFetchCategoryEntity);
			info.AddValue("_alreadyFetchedCategoryEntity", _alreadyFetchedCategoryEntity);
			info.AddValue("_entertainmentEntity", (!this.MarkedForDeletion?_entertainmentEntity:null));
			info.AddValue("_entertainmentEntityReturnsNewIfNotFound", _entertainmentEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchEntertainmentEntity", _alwaysFetchEntertainmentEntity);
			info.AddValue("_alreadyFetchedEntertainmentEntity", _alreadyFetchedEntertainmentEntity);
			info.AddValue("_mapEntity", (!this.MarkedForDeletion?_mapEntity:null));
			info.AddValue("_mapEntityReturnsNewIfNotFound", _mapEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchMapEntity", _alwaysFetchMapEntity);
			info.AddValue("_alreadyFetchedMapEntity", _alreadyFetchedMapEntity);
			info.AddValue("_siteEntity", (!this.MarkedForDeletion?_siteEntity:null));
			info.AddValue("_siteEntityReturnsNewIfNotFound", _siteEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchSiteEntity", _alwaysFetchSiteEntity);
			info.AddValue("_alreadyFetchedSiteEntity", _alreadyFetchedSiteEntity);
			info.AddValue("_uIModeEntity", (!this.MarkedForDeletion?_uIModeEntity:null));
			info.AddValue("_uIModeEntityReturnsNewIfNotFound", _uIModeEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchUIModeEntity", _alwaysFetchUIModeEntity);
			info.AddValue("_alreadyFetchedUIModeEntity", _alreadyFetchedUIModeEntity);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "CategoryEntity":
					_alreadyFetchedCategoryEntity = true;
					this.CategoryEntity = (CategoryEntity)entity;
					break;
				case "EntertainmentEntity":
					_alreadyFetchedEntertainmentEntity = true;
					this.EntertainmentEntity = (EntertainmentEntity)entity;
					break;
				case "MapEntity":
					_alreadyFetchedMapEntity = true;
					this.MapEntity = (MapEntity)entity;
					break;
				case "SiteEntity":
					_alreadyFetchedSiteEntity = true;
					this.SiteEntity = (SiteEntity)entity;
					break;
				case "UIModeEntity":
					_alreadyFetchedUIModeEntity = true;
					this.UIModeEntity = (UIModeEntity)entity;
					break;
				case "CustomTextCollection":
					_alreadyFetchedCustomTextCollection = true;
					if(entity!=null)
					{
						this.CustomTextCollection.Add((CustomTextEntity)entity);
					}
					break;
				case "UIModeCollection":
					_alreadyFetchedUIModeCollection = true;
					if(entity!=null)
					{
						this.UIModeCollection.Add((UIModeEntity)entity);
					}
					break;
				case "UITabLanguageCollection":
					_alreadyFetchedUITabLanguageCollection = true;
					if(entity!=null)
					{
						this.UITabLanguageCollection.Add((UITabLanguageEntity)entity);
					}
					break;
				case "UIWidgetCollection":
					_alreadyFetchedUIWidgetCollection = true;
					if(entity!=null)
					{
						this.UIWidgetCollection.Add((UIWidgetEntity)entity);
					}
					break;
				case "CompanyCollectionViaUIMode":
					_alreadyFetchedCompanyCollectionViaUIMode = true;
					if(entity!=null)
					{
						this.CompanyCollectionViaUIMode.Add((CompanyEntity)entity);
					}
					break;
				case "LanguageCollectionViaUITabLanguage":
					_alreadyFetchedLanguageCollectionViaUITabLanguage = true;
					if(entity!=null)
					{
						this.LanguageCollectionViaUITabLanguage.Add((LanguageEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "CategoryEntity":
					SetupSyncCategoryEntity(relatedEntity);
					break;
				case "EntertainmentEntity":
					SetupSyncEntertainmentEntity(relatedEntity);
					break;
				case "MapEntity":
					SetupSyncMapEntity(relatedEntity);
					break;
				case "SiteEntity":
					SetupSyncSiteEntity(relatedEntity);
					break;
				case "UIModeEntity":
					SetupSyncUIModeEntity(relatedEntity);
					break;
				case "CustomTextCollection":
					_customTextCollection.Add((CustomTextEntity)relatedEntity);
					break;
				case "UIModeCollection":
					_uIModeCollection.Add((UIModeEntity)relatedEntity);
					break;
				case "UITabLanguageCollection":
					_uITabLanguageCollection.Add((UITabLanguageEntity)relatedEntity);
					break;
				case "UIWidgetCollection":
					_uIWidgetCollection.Add((UIWidgetEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "CategoryEntity":
					DesetupSyncCategoryEntity(false, true);
					break;
				case "EntertainmentEntity":
					DesetupSyncEntertainmentEntity(false, true);
					break;
				case "MapEntity":
					DesetupSyncMapEntity(false, true);
					break;
				case "SiteEntity":
					DesetupSyncSiteEntity(false, true);
					break;
				case "UIModeEntity":
					DesetupSyncUIModeEntity(false, true);
					break;
				case "CustomTextCollection":
					this.PerformRelatedEntityRemoval(_customTextCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "UIModeCollection":
					this.PerformRelatedEntityRemoval(_uIModeCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "UITabLanguageCollection":
					this.PerformRelatedEntityRemoval(_uITabLanguageCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "UIWidgetCollection":
					this.PerformRelatedEntityRemoval(_uIWidgetCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_categoryEntity!=null)
			{
				toReturn.Add(_categoryEntity);
			}
			if(_entertainmentEntity!=null)
			{
				toReturn.Add(_entertainmentEntity);
			}
			if(_mapEntity!=null)
			{
				toReturn.Add(_mapEntity);
			}
			if(_siteEntity!=null)
			{
				toReturn.Add(_siteEntity);
			}
			if(_uIModeEntity!=null)
			{
				toReturn.Add(_uIModeEntity);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_customTextCollection);
			toReturn.Add(_uIModeCollection);
			toReturn.Add(_uITabLanguageCollection);
			toReturn.Add(_uIWidgetCollection);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="uITabId">PK value for UITab which data should be fetched into this UITab object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 uITabId)
		{
			return FetchUsingPK(uITabId, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="uITabId">PK value for UITab which data should be fetched into this UITab object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 uITabId, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(uITabId, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="uITabId">PK value for UITab which data should be fetched into this UITab object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 uITabId, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(uITabId, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="uITabId">PK value for UITab which data should be fetched into this UITab object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 uITabId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(uITabId, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.UITabId, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new UITabRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'CustomTextEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CustomTextEntity'</returns>
		public Obymobi.Data.CollectionClasses.CustomTextCollection GetMultiCustomTextCollection(bool forceFetch)
		{
			return GetMultiCustomTextCollection(forceFetch, _customTextCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CustomTextEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'CustomTextEntity'</returns>
		public Obymobi.Data.CollectionClasses.CustomTextCollection GetMultiCustomTextCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiCustomTextCollection(forceFetch, _customTextCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'CustomTextEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.CustomTextCollection GetMultiCustomTextCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiCustomTextCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'CustomTextEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.CustomTextCollection GetMultiCustomTextCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedCustomTextCollection || forceFetch || _alwaysFetchCustomTextCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_customTextCollection);
				_customTextCollection.SuppressClearInGetMulti=!forceFetch;
				_customTextCollection.EntityFactoryToUse = entityFactoryToUse;
				_customTextCollection.GetMultiManyToOne(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, this, null, null, filter);
				_customTextCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedCustomTextCollection = true;
			}
			return _customTextCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'CustomTextCollection'. These settings will be taken into account
		/// when the property CustomTextCollection is requested or GetMultiCustomTextCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCustomTextCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_customTextCollection.SortClauses=sortClauses;
			_customTextCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'UIModeEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'UIModeEntity'</returns>
		public Obymobi.Data.CollectionClasses.UIModeCollection GetMultiUIModeCollection(bool forceFetch)
		{
			return GetMultiUIModeCollection(forceFetch, _uIModeCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'UIModeEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'UIModeEntity'</returns>
		public Obymobi.Data.CollectionClasses.UIModeCollection GetMultiUIModeCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiUIModeCollection(forceFetch, _uIModeCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'UIModeEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.UIModeCollection GetMultiUIModeCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiUIModeCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'UIModeEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.UIModeCollection GetMultiUIModeCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedUIModeCollection || forceFetch || _alwaysFetchUIModeCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_uIModeCollection);
				_uIModeCollection.SuppressClearInGetMulti=!forceFetch;
				_uIModeCollection.EntityFactoryToUse = entityFactoryToUse;
				_uIModeCollection.GetMultiManyToOne(null, null, this, filter);
				_uIModeCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedUIModeCollection = true;
			}
			return _uIModeCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'UIModeCollection'. These settings will be taken into account
		/// when the property UIModeCollection is requested or GetMultiUIModeCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersUIModeCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_uIModeCollection.SortClauses=sortClauses;
			_uIModeCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'UITabLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'UITabLanguageEntity'</returns>
		public Obymobi.Data.CollectionClasses.UITabLanguageCollection GetMultiUITabLanguageCollection(bool forceFetch)
		{
			return GetMultiUITabLanguageCollection(forceFetch, _uITabLanguageCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'UITabLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'UITabLanguageEntity'</returns>
		public Obymobi.Data.CollectionClasses.UITabLanguageCollection GetMultiUITabLanguageCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiUITabLanguageCollection(forceFetch, _uITabLanguageCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'UITabLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.UITabLanguageCollection GetMultiUITabLanguageCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiUITabLanguageCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'UITabLanguageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.UITabLanguageCollection GetMultiUITabLanguageCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedUITabLanguageCollection || forceFetch || _alwaysFetchUITabLanguageCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_uITabLanguageCollection);
				_uITabLanguageCollection.SuppressClearInGetMulti=!forceFetch;
				_uITabLanguageCollection.EntityFactoryToUse = entityFactoryToUse;
				_uITabLanguageCollection.GetMultiManyToOne(null, this, filter);
				_uITabLanguageCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedUITabLanguageCollection = true;
			}
			return _uITabLanguageCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'UITabLanguageCollection'. These settings will be taken into account
		/// when the property UITabLanguageCollection is requested or GetMultiUITabLanguageCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersUITabLanguageCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_uITabLanguageCollection.SortClauses=sortClauses;
			_uITabLanguageCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'UIWidgetEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'UIWidgetEntity'</returns>
		public Obymobi.Data.CollectionClasses.UIWidgetCollection GetMultiUIWidgetCollection(bool forceFetch)
		{
			return GetMultiUIWidgetCollection(forceFetch, _uIWidgetCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'UIWidgetEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'UIWidgetEntity'</returns>
		public Obymobi.Data.CollectionClasses.UIWidgetCollection GetMultiUIWidgetCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiUIWidgetCollection(forceFetch, _uIWidgetCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'UIWidgetEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.UIWidgetCollection GetMultiUIWidgetCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiUIWidgetCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'UIWidgetEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.UIWidgetCollection GetMultiUIWidgetCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedUIWidgetCollection || forceFetch || _alwaysFetchUIWidgetCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_uIWidgetCollection);
				_uIWidgetCollection.SuppressClearInGetMulti=!forceFetch;
				_uIWidgetCollection.EntityFactoryToUse = entityFactoryToUse;
				_uIWidgetCollection.GetMultiManyToOne(null, null, null, null, null, null, null, null, null, this, filter);
				_uIWidgetCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedUIWidgetCollection = true;
			}
			return _uIWidgetCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'UIWidgetCollection'. These settings will be taken into account
		/// when the property UIWidgetCollection is requested or GetMultiUIWidgetCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersUIWidgetCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_uIWidgetCollection.SortClauses=sortClauses;
			_uIWidgetCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'CompanyEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CompanyEntity'</returns>
		public Obymobi.Data.CollectionClasses.CompanyCollection GetMultiCompanyCollectionViaUIMode(bool forceFetch)
		{
			return GetMultiCompanyCollectionViaUIMode(forceFetch, _companyCollectionViaUIMode.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'CompanyEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.CompanyCollection GetMultiCompanyCollectionViaUIMode(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedCompanyCollectionViaUIMode || forceFetch || _alwaysFetchCompanyCollectionViaUIMode) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_companyCollectionViaUIMode);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(UITabFields.UITabId, ComparisonOperator.Equal, this.UITabId, "UITabEntity__"));
				_companyCollectionViaUIMode.SuppressClearInGetMulti=!forceFetch;
				_companyCollectionViaUIMode.EntityFactoryToUse = entityFactoryToUse;
				_companyCollectionViaUIMode.GetMulti(filter, GetRelationsForField("CompanyCollectionViaUIMode"));
				_companyCollectionViaUIMode.SuppressClearInGetMulti=false;
				_alreadyFetchedCompanyCollectionViaUIMode = true;
			}
			return _companyCollectionViaUIMode;
		}

		/// <summary> Sets the collection parameters for the collection for 'CompanyCollectionViaUIMode'. These settings will be taken into account
		/// when the property CompanyCollectionViaUIMode is requested or GetMultiCompanyCollectionViaUIMode is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCompanyCollectionViaUIMode(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_companyCollectionViaUIMode.SortClauses=sortClauses;
			_companyCollectionViaUIMode.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'LanguageEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'LanguageEntity'</returns>
		public Obymobi.Data.CollectionClasses.LanguageCollection GetMultiLanguageCollectionViaUITabLanguage(bool forceFetch)
		{
			return GetMultiLanguageCollectionViaUITabLanguage(forceFetch, _languageCollectionViaUITabLanguage.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'LanguageEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.LanguageCollection GetMultiLanguageCollectionViaUITabLanguage(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedLanguageCollectionViaUITabLanguage || forceFetch || _alwaysFetchLanguageCollectionViaUITabLanguage) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_languageCollectionViaUITabLanguage);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(UITabFields.UITabId, ComparisonOperator.Equal, this.UITabId, "UITabEntity__"));
				_languageCollectionViaUITabLanguage.SuppressClearInGetMulti=!forceFetch;
				_languageCollectionViaUITabLanguage.EntityFactoryToUse = entityFactoryToUse;
				_languageCollectionViaUITabLanguage.GetMulti(filter, GetRelationsForField("LanguageCollectionViaUITabLanguage"));
				_languageCollectionViaUITabLanguage.SuppressClearInGetMulti=false;
				_alreadyFetchedLanguageCollectionViaUITabLanguage = true;
			}
			return _languageCollectionViaUITabLanguage;
		}

		/// <summary> Sets the collection parameters for the collection for 'LanguageCollectionViaUITabLanguage'. These settings will be taken into account
		/// when the property LanguageCollectionViaUITabLanguage is requested or GetMultiLanguageCollectionViaUITabLanguage is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersLanguageCollectionViaUITabLanguage(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_languageCollectionViaUITabLanguage.SortClauses=sortClauses;
			_languageCollectionViaUITabLanguage.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves the related entity of type 'CategoryEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'CategoryEntity' which is related to this entity.</returns>
		public CategoryEntity GetSingleCategoryEntity()
		{
			return GetSingleCategoryEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'CategoryEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'CategoryEntity' which is related to this entity.</returns>
		public virtual CategoryEntity GetSingleCategoryEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedCategoryEntity || forceFetch || _alwaysFetchCategoryEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.CategoryEntityUsingCategoryId);
				CategoryEntity newEntity = new CategoryEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.CategoryId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (CategoryEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_categoryEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.CategoryEntity = newEntity;
				_alreadyFetchedCategoryEntity = fetchResult;
			}
			return _categoryEntity;
		}


		/// <summary> Retrieves the related entity of type 'EntertainmentEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'EntertainmentEntity' which is related to this entity.</returns>
		public EntertainmentEntity GetSingleEntertainmentEntity()
		{
			return GetSingleEntertainmentEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'EntertainmentEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'EntertainmentEntity' which is related to this entity.</returns>
		public virtual EntertainmentEntity GetSingleEntertainmentEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedEntertainmentEntity || forceFetch || _alwaysFetchEntertainmentEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.EntertainmentEntityUsingEntertainmentId);
				EntertainmentEntity newEntity = new EntertainmentEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.EntertainmentId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (EntertainmentEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_entertainmentEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.EntertainmentEntity = newEntity;
				_alreadyFetchedEntertainmentEntity = fetchResult;
			}
			return _entertainmentEntity;
		}


		/// <summary> Retrieves the related entity of type 'MapEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'MapEntity' which is related to this entity.</returns>
		public MapEntity GetSingleMapEntity()
		{
			return GetSingleMapEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'MapEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'MapEntity' which is related to this entity.</returns>
		public virtual MapEntity GetSingleMapEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedMapEntity || forceFetch || _alwaysFetchMapEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.MapEntityUsingMapId);
				MapEntity newEntity = new MapEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.MapId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (MapEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_mapEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.MapEntity = newEntity;
				_alreadyFetchedMapEntity = fetchResult;
			}
			return _mapEntity;
		}


		/// <summary> Retrieves the related entity of type 'SiteEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'SiteEntity' which is related to this entity.</returns>
		public SiteEntity GetSingleSiteEntity()
		{
			return GetSingleSiteEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'SiteEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'SiteEntity' which is related to this entity.</returns>
		public virtual SiteEntity GetSingleSiteEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedSiteEntity || forceFetch || _alwaysFetchSiteEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.SiteEntityUsingSiteId);
				SiteEntity newEntity = new SiteEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.SiteId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (SiteEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_siteEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.SiteEntity = newEntity;
				_alreadyFetchedSiteEntity = fetchResult;
			}
			return _siteEntity;
		}


		/// <summary> Retrieves the related entity of type 'UIModeEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'UIModeEntity' which is related to this entity.</returns>
		public UIModeEntity GetSingleUIModeEntity()
		{
			return GetSingleUIModeEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'UIModeEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'UIModeEntity' which is related to this entity.</returns>
		public virtual UIModeEntity GetSingleUIModeEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedUIModeEntity || forceFetch || _alwaysFetchUIModeEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.UIModeEntityUsingUIModeId);
				UIModeEntity newEntity = new UIModeEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.UIModeId);
				}
				if(fetchResult)
				{
					newEntity = (UIModeEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_uIModeEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.UIModeEntity = newEntity;
				_alreadyFetchedUIModeEntity = fetchResult;
			}
			return _uIModeEntity;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("CategoryEntity", _categoryEntity);
			toReturn.Add("EntertainmentEntity", _entertainmentEntity);
			toReturn.Add("MapEntity", _mapEntity);
			toReturn.Add("SiteEntity", _siteEntity);
			toReturn.Add("UIModeEntity", _uIModeEntity);
			toReturn.Add("CustomTextCollection", _customTextCollection);
			toReturn.Add("UIModeCollection", _uIModeCollection);
			toReturn.Add("UITabLanguageCollection", _uITabLanguageCollection);
			toReturn.Add("UIWidgetCollection", _uIWidgetCollection);
			toReturn.Add("CompanyCollectionViaUIMode", _companyCollectionViaUIMode);
			toReturn.Add("LanguageCollectionViaUITabLanguage", _languageCollectionViaUITabLanguage);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="uITabId">PK value for UITab which data should be fetched into this UITab object</param>
		/// <param name="validator">The validator object for this UITabEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int32 uITabId, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(uITabId, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_customTextCollection = new Obymobi.Data.CollectionClasses.CustomTextCollection();
			_customTextCollection.SetContainingEntityInfo(this, "UITabEntity");

			_uIModeCollection = new Obymobi.Data.CollectionClasses.UIModeCollection();
			_uIModeCollection.SetContainingEntityInfo(this, "DefaultUITabEntity");

			_uITabLanguageCollection = new Obymobi.Data.CollectionClasses.UITabLanguageCollection();
			_uITabLanguageCollection.SetContainingEntityInfo(this, "UITabEntity");

			_uIWidgetCollection = new Obymobi.Data.CollectionClasses.UIWidgetCollection();
			_uIWidgetCollection.SetContainingEntityInfo(this, "UITabEntity");
			_companyCollectionViaUIMode = new Obymobi.Data.CollectionClasses.CompanyCollection();
			_languageCollectionViaUITabLanguage = new Obymobi.Data.CollectionClasses.LanguageCollection();
			_categoryEntityReturnsNewIfNotFound = true;
			_entertainmentEntityReturnsNewIfNotFound = true;
			_mapEntityReturnsNewIfNotFound = true;
			_siteEntityReturnsNewIfNotFound = true;
			_uIModeEntityReturnsNewIfNotFound = true;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UITabId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UIModeId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Caption", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Type", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CategoryId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("EntertainmentId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("URL", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Width", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SortOrder", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Zoom", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Visible", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SiteId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RestrictedAccess", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ParentCompanyId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AllCategoryVisible", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ShowPmsMessagegroups", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("MapId", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _categoryEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncCategoryEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _categoryEntity, new PropertyChangedEventHandler( OnCategoryEntityPropertyChanged ), "CategoryEntity", Obymobi.Data.RelationClasses.StaticUITabRelations.CategoryEntityUsingCategoryIdStatic, true, signalRelatedEntity, "UITabCollection", resetFKFields, new int[] { (int)UITabFieldIndex.CategoryId } );		
			_categoryEntity = null;
		}
		
		/// <summary> setups the sync logic for member _categoryEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncCategoryEntity(IEntityCore relatedEntity)
		{
			if(_categoryEntity!=relatedEntity)
			{		
				DesetupSyncCategoryEntity(true, true);
				_categoryEntity = (CategoryEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _categoryEntity, new PropertyChangedEventHandler( OnCategoryEntityPropertyChanged ), "CategoryEntity", Obymobi.Data.RelationClasses.StaticUITabRelations.CategoryEntityUsingCategoryIdStatic, true, ref _alreadyFetchedCategoryEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnCategoryEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _entertainmentEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncEntertainmentEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _entertainmentEntity, new PropertyChangedEventHandler( OnEntertainmentEntityPropertyChanged ), "EntertainmentEntity", Obymobi.Data.RelationClasses.StaticUITabRelations.EntertainmentEntityUsingEntertainmentIdStatic, true, signalRelatedEntity, "UITabCollection", resetFKFields, new int[] { (int)UITabFieldIndex.EntertainmentId } );		
			_entertainmentEntity = null;
		}
		
		/// <summary> setups the sync logic for member _entertainmentEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncEntertainmentEntity(IEntityCore relatedEntity)
		{
			if(_entertainmentEntity!=relatedEntity)
			{		
				DesetupSyncEntertainmentEntity(true, true);
				_entertainmentEntity = (EntertainmentEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _entertainmentEntity, new PropertyChangedEventHandler( OnEntertainmentEntityPropertyChanged ), "EntertainmentEntity", Obymobi.Data.RelationClasses.StaticUITabRelations.EntertainmentEntityUsingEntertainmentIdStatic, true, ref _alreadyFetchedEntertainmentEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnEntertainmentEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _mapEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncMapEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _mapEntity, new PropertyChangedEventHandler( OnMapEntityPropertyChanged ), "MapEntity", Obymobi.Data.RelationClasses.StaticUITabRelations.MapEntityUsingMapIdStatic, true, signalRelatedEntity, "UITabCollection", resetFKFields, new int[] { (int)UITabFieldIndex.MapId } );		
			_mapEntity = null;
		}
		
		/// <summary> setups the sync logic for member _mapEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncMapEntity(IEntityCore relatedEntity)
		{
			if(_mapEntity!=relatedEntity)
			{		
				DesetupSyncMapEntity(true, true);
				_mapEntity = (MapEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _mapEntity, new PropertyChangedEventHandler( OnMapEntityPropertyChanged ), "MapEntity", Obymobi.Data.RelationClasses.StaticUITabRelations.MapEntityUsingMapIdStatic, true, ref _alreadyFetchedMapEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnMapEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _siteEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncSiteEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _siteEntity, new PropertyChangedEventHandler( OnSiteEntityPropertyChanged ), "SiteEntity", Obymobi.Data.RelationClasses.StaticUITabRelations.SiteEntityUsingSiteIdStatic, true, signalRelatedEntity, "UITabCollection", resetFKFields, new int[] { (int)UITabFieldIndex.SiteId } );		
			_siteEntity = null;
		}
		
		/// <summary> setups the sync logic for member _siteEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncSiteEntity(IEntityCore relatedEntity)
		{
			if(_siteEntity!=relatedEntity)
			{		
				DesetupSyncSiteEntity(true, true);
				_siteEntity = (SiteEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _siteEntity, new PropertyChangedEventHandler( OnSiteEntityPropertyChanged ), "SiteEntity", Obymobi.Data.RelationClasses.StaticUITabRelations.SiteEntityUsingSiteIdStatic, true, ref _alreadyFetchedSiteEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnSiteEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _uIModeEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncUIModeEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _uIModeEntity, new PropertyChangedEventHandler( OnUIModeEntityPropertyChanged ), "UIModeEntity", Obymobi.Data.RelationClasses.StaticUITabRelations.UIModeEntityUsingUIModeIdStatic, true, signalRelatedEntity, "UITabCollection", resetFKFields, new int[] { (int)UITabFieldIndex.UIModeId } );		
			_uIModeEntity = null;
		}
		
		/// <summary> setups the sync logic for member _uIModeEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncUIModeEntity(IEntityCore relatedEntity)
		{
			if(_uIModeEntity!=relatedEntity)
			{		
				DesetupSyncUIModeEntity(true, true);
				_uIModeEntity = (UIModeEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _uIModeEntity, new PropertyChangedEventHandler( OnUIModeEntityPropertyChanged ), "UIModeEntity", Obymobi.Data.RelationClasses.StaticUITabRelations.UIModeEntityUsingUIModeIdStatic, true, ref _alreadyFetchedUIModeEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnUIModeEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="uITabId">PK value for UITab which data should be fetched into this UITab object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int32 uITabId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)UITabFieldIndex.UITabId].ForcedCurrentValueWrite(uITabId);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateUITabDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new UITabEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static UITabRelations Relations
		{
			get	{ return new UITabRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'CustomText' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCustomTextCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CustomTextCollection(), (IEntityRelation)GetRelationsForField("CustomTextCollection")[0], (int)Obymobi.Data.EntityType.UITabEntity, (int)Obymobi.Data.EntityType.CustomTextEntity, 0, null, null, null, "CustomTextCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'UIMode' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathUIModeCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.UIModeCollection(), (IEntityRelation)GetRelationsForField("UIModeCollection")[0], (int)Obymobi.Data.EntityType.UITabEntity, (int)Obymobi.Data.EntityType.UIModeEntity, 0, null, null, null, "UIModeCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'UITabLanguage' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathUITabLanguageCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.UITabLanguageCollection(), (IEntityRelation)GetRelationsForField("UITabLanguageCollection")[0], (int)Obymobi.Data.EntityType.UITabEntity, (int)Obymobi.Data.EntityType.UITabLanguageEntity, 0, null, null, null, "UITabLanguageCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'UIWidget' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathUIWidgetCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.UIWidgetCollection(), (IEntityRelation)GetRelationsForField("UIWidgetCollection")[0], (int)Obymobi.Data.EntityType.UITabEntity, (int)Obymobi.Data.EntityType.UIWidgetEntity, 0, null, null, null, "UIWidgetCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Company'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCompanyCollectionViaUIMode
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.UIModeEntityUsingDefaultUITabId;
				intermediateRelation.SetAliases(string.Empty, "UIMode_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CompanyCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.UITabEntity, (int)Obymobi.Data.EntityType.CompanyEntity, 0, null, null, GetRelationsForField("CompanyCollectionViaUIMode"), "CompanyCollectionViaUIMode", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Language'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathLanguageCollectionViaUITabLanguage
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.UITabLanguageEntityUsingUITabId;
				intermediateRelation.SetAliases(string.Empty, "UITabLanguage_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.LanguageCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.UITabEntity, (int)Obymobi.Data.EntityType.LanguageEntity, 0, null, null, GetRelationsForField("LanguageCollectionViaUITabLanguage"), "LanguageCollectionViaUITabLanguage", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Category'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCategoryEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CategoryCollection(), (IEntityRelation)GetRelationsForField("CategoryEntity")[0], (int)Obymobi.Data.EntityType.UITabEntity, (int)Obymobi.Data.EntityType.CategoryEntity, 0, null, null, null, "CategoryEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Entertainment'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathEntertainmentEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.EntertainmentCollection(), (IEntityRelation)GetRelationsForField("EntertainmentEntity")[0], (int)Obymobi.Data.EntityType.UITabEntity, (int)Obymobi.Data.EntityType.EntertainmentEntity, 0, null, null, null, "EntertainmentEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Map'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathMapEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.MapCollection(), (IEntityRelation)GetRelationsForField("MapEntity")[0], (int)Obymobi.Data.EntityType.UITabEntity, (int)Obymobi.Data.EntityType.MapEntity, 0, null, null, null, "MapEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Site'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathSiteEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.SiteCollection(), (IEntityRelation)GetRelationsForField("SiteEntity")[0], (int)Obymobi.Data.EntityType.UITabEntity, (int)Obymobi.Data.EntityType.SiteEntity, 0, null, null, null, "SiteEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'UIMode'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathUIModeEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.UIModeCollection(), (IEntityRelation)GetRelationsForField("UIModeEntity")[0], (int)Obymobi.Data.EntityType.UITabEntity, (int)Obymobi.Data.EntityType.UIModeEntity, 0, null, null, null, "UIModeEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The UITabId property of the Entity UITab<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UITab"."UITabId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 UITabId
		{
			get { return (System.Int32)GetValue((int)UITabFieldIndex.UITabId, true); }
			set	{ SetValue((int)UITabFieldIndex.UITabId, value, true); }
		}

		/// <summary> The UIModeId property of the Entity UITab<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UITab"."UIModeId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 UIModeId
		{
			get { return (System.Int32)GetValue((int)UITabFieldIndex.UIModeId, true); }
			set	{ SetValue((int)UITabFieldIndex.UIModeId, value, true); }
		}

		/// <summary> The Caption property of the Entity UITab<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UITab"."Caption"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Caption
		{
			get { return (System.String)GetValue((int)UITabFieldIndex.Caption, true); }
			set	{ SetValue((int)UITabFieldIndex.Caption, value, true); }
		}

		/// <summary> The Type property of the Entity UITab<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UITab"."Type"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 Type
		{
			get { return (System.Int32)GetValue((int)UITabFieldIndex.Type, true); }
			set	{ SetValue((int)UITabFieldIndex.Type, value, true); }
		}

		/// <summary> The CategoryId property of the Entity UITab<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UITab"."CategoryId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> CategoryId
		{
			get { return (Nullable<System.Int32>)GetValue((int)UITabFieldIndex.CategoryId, false); }
			set	{ SetValue((int)UITabFieldIndex.CategoryId, value, true); }
		}

		/// <summary> The EntertainmentId property of the Entity UITab<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UITab"."EntertainmentId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> EntertainmentId
		{
			get { return (Nullable<System.Int32>)GetValue((int)UITabFieldIndex.EntertainmentId, false); }
			set	{ SetValue((int)UITabFieldIndex.EntertainmentId, value, true); }
		}

		/// <summary> The URL property of the Entity UITab<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UITab"."URL"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String URL
		{
			get { return (System.String)GetValue((int)UITabFieldIndex.URL, true); }
			set	{ SetValue((int)UITabFieldIndex.URL, value, true); }
		}

		/// <summary> The Width property of the Entity UITab<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UITab"."Width"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 Width
		{
			get { return (System.Int32)GetValue((int)UITabFieldIndex.Width, true); }
			set	{ SetValue((int)UITabFieldIndex.Width, value, true); }
		}

		/// <summary> The CreatedBy property of the Entity UITab<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UITab"."CreatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CreatedBy
		{
			get { return (System.Int32)GetValue((int)UITabFieldIndex.CreatedBy, true); }
			set	{ SetValue((int)UITabFieldIndex.CreatedBy, value, true); }
		}

		/// <summary> The UpdatedBy property of the Entity UITab<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UITab"."UpdatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 UpdatedBy
		{
			get { return (System.Int32)GetValue((int)UITabFieldIndex.UpdatedBy, true); }
			set	{ SetValue((int)UITabFieldIndex.UpdatedBy, value, true); }
		}

		/// <summary> The SortOrder property of the Entity UITab<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UITab"."SortOrder"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 SortOrder
		{
			get { return (System.Int32)GetValue((int)UITabFieldIndex.SortOrder, true); }
			set	{ SetValue((int)UITabFieldIndex.SortOrder, value, true); }
		}

		/// <summary> The Zoom property of the Entity UITab<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UITab"."Zoom"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 Zoom
		{
			get { return (System.Int32)GetValue((int)UITabFieldIndex.Zoom, true); }
			set	{ SetValue((int)UITabFieldIndex.Zoom, value, true); }
		}

		/// <summary> The Visible property of the Entity UITab<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UITab"."Visible"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean Visible
		{
			get { return (System.Boolean)GetValue((int)UITabFieldIndex.Visible, true); }
			set	{ SetValue((int)UITabFieldIndex.Visible, value, true); }
		}

		/// <summary> The SiteId property of the Entity UITab<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UITab"."SiteId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> SiteId
		{
			get { return (Nullable<System.Int32>)GetValue((int)UITabFieldIndex.SiteId, false); }
			set	{ SetValue((int)UITabFieldIndex.SiteId, value, true); }
		}

		/// <summary> The RestrictedAccess property of the Entity UITab<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UITab"."RestrictedAccess"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean RestrictedAccess
		{
			get { return (System.Boolean)GetValue((int)UITabFieldIndex.RestrictedAccess, true); }
			set	{ SetValue((int)UITabFieldIndex.RestrictedAccess, value, true); }
		}

		/// <summary> The ParentCompanyId property of the Entity UITab<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UITab"."ParentCompanyId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ParentCompanyId
		{
			get { return (Nullable<System.Int32>)GetValue((int)UITabFieldIndex.ParentCompanyId, false); }
			set	{ SetValue((int)UITabFieldIndex.ParentCompanyId, value, true); }
		}

		/// <summary> The AllCategoryVisible property of the Entity UITab<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UITab"."AllCategoryVisible"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean AllCategoryVisible
		{
			get { return (System.Boolean)GetValue((int)UITabFieldIndex.AllCategoryVisible, true); }
			set	{ SetValue((int)UITabFieldIndex.AllCategoryVisible, value, true); }
		}

		/// <summary> The CreatedUTC property of the Entity UITab<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UITab"."CreatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CreatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)UITabFieldIndex.CreatedUTC, false); }
			set	{ SetValue((int)UITabFieldIndex.CreatedUTC, value, true); }
		}

		/// <summary> The UpdatedUTC property of the Entity UITab<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UITab"."UpdatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)UITabFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)UITabFieldIndex.UpdatedUTC, value, true); }
		}

		/// <summary> The ShowPmsMessagegroups property of the Entity UITab<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UITab"."ShowPmsMessagegroups"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean ShowPmsMessagegroups
		{
			get { return (System.Boolean)GetValue((int)UITabFieldIndex.ShowPmsMessagegroups, true); }
			set	{ SetValue((int)UITabFieldIndex.ShowPmsMessagegroups, value, true); }
		}

		/// <summary> The MapId property of the Entity UITab<br/><br/></summary>
		/// <remarks>Mapped on  table field: "UITab"."MapId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> MapId
		{
			get { return (Nullable<System.Int32>)GetValue((int)UITabFieldIndex.MapId, false); }
			set	{ SetValue((int)UITabFieldIndex.MapId, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'CustomTextEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCustomTextCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.CustomTextCollection CustomTextCollection
		{
			get	{ return GetMultiCustomTextCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CustomTextCollection. When set to true, CustomTextCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CustomTextCollection is accessed. You can always execute/ a forced fetch by calling GetMultiCustomTextCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCustomTextCollection
		{
			get	{ return _alwaysFetchCustomTextCollection; }
			set	{ _alwaysFetchCustomTextCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property CustomTextCollection already has been fetched. Setting this property to false when CustomTextCollection has been fetched
		/// will clear the CustomTextCollection collection well. Setting this property to true while CustomTextCollection hasn't been fetched disables lazy loading for CustomTextCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCustomTextCollection
		{
			get { return _alreadyFetchedCustomTextCollection;}
			set 
			{
				if(_alreadyFetchedCustomTextCollection && !value && (_customTextCollection != null))
				{
					_customTextCollection.Clear();
				}
				_alreadyFetchedCustomTextCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'UIModeEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiUIModeCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.UIModeCollection UIModeCollection
		{
			get	{ return GetMultiUIModeCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for UIModeCollection. When set to true, UIModeCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time UIModeCollection is accessed. You can always execute/ a forced fetch by calling GetMultiUIModeCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchUIModeCollection
		{
			get	{ return _alwaysFetchUIModeCollection; }
			set	{ _alwaysFetchUIModeCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property UIModeCollection already has been fetched. Setting this property to false when UIModeCollection has been fetched
		/// will clear the UIModeCollection collection well. Setting this property to true while UIModeCollection hasn't been fetched disables lazy loading for UIModeCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedUIModeCollection
		{
			get { return _alreadyFetchedUIModeCollection;}
			set 
			{
				if(_alreadyFetchedUIModeCollection && !value && (_uIModeCollection != null))
				{
					_uIModeCollection.Clear();
				}
				_alreadyFetchedUIModeCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'UITabLanguageEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiUITabLanguageCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.UITabLanguageCollection UITabLanguageCollection
		{
			get	{ return GetMultiUITabLanguageCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for UITabLanguageCollection. When set to true, UITabLanguageCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time UITabLanguageCollection is accessed. You can always execute/ a forced fetch by calling GetMultiUITabLanguageCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchUITabLanguageCollection
		{
			get	{ return _alwaysFetchUITabLanguageCollection; }
			set	{ _alwaysFetchUITabLanguageCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property UITabLanguageCollection already has been fetched. Setting this property to false when UITabLanguageCollection has been fetched
		/// will clear the UITabLanguageCollection collection well. Setting this property to true while UITabLanguageCollection hasn't been fetched disables lazy loading for UITabLanguageCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedUITabLanguageCollection
		{
			get { return _alreadyFetchedUITabLanguageCollection;}
			set 
			{
				if(_alreadyFetchedUITabLanguageCollection && !value && (_uITabLanguageCollection != null))
				{
					_uITabLanguageCollection.Clear();
				}
				_alreadyFetchedUITabLanguageCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'UIWidgetEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiUIWidgetCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.UIWidgetCollection UIWidgetCollection
		{
			get	{ return GetMultiUIWidgetCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for UIWidgetCollection. When set to true, UIWidgetCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time UIWidgetCollection is accessed. You can always execute/ a forced fetch by calling GetMultiUIWidgetCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchUIWidgetCollection
		{
			get	{ return _alwaysFetchUIWidgetCollection; }
			set	{ _alwaysFetchUIWidgetCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property UIWidgetCollection already has been fetched. Setting this property to false when UIWidgetCollection has been fetched
		/// will clear the UIWidgetCollection collection well. Setting this property to true while UIWidgetCollection hasn't been fetched disables lazy loading for UIWidgetCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedUIWidgetCollection
		{
			get { return _alreadyFetchedUIWidgetCollection;}
			set 
			{
				if(_alreadyFetchedUIWidgetCollection && !value && (_uIWidgetCollection != null))
				{
					_uIWidgetCollection.Clear();
				}
				_alreadyFetchedUIWidgetCollection = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'CompanyEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCompanyCollectionViaUIMode()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.CompanyCollection CompanyCollectionViaUIMode
		{
			get { return GetMultiCompanyCollectionViaUIMode(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CompanyCollectionViaUIMode. When set to true, CompanyCollectionViaUIMode is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CompanyCollectionViaUIMode is accessed. You can always execute a forced fetch by calling GetMultiCompanyCollectionViaUIMode(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCompanyCollectionViaUIMode
		{
			get	{ return _alwaysFetchCompanyCollectionViaUIMode; }
			set	{ _alwaysFetchCompanyCollectionViaUIMode = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CompanyCollectionViaUIMode already has been fetched. Setting this property to false when CompanyCollectionViaUIMode has been fetched
		/// will clear the CompanyCollectionViaUIMode collection well. Setting this property to true while CompanyCollectionViaUIMode hasn't been fetched disables lazy loading for CompanyCollectionViaUIMode</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCompanyCollectionViaUIMode
		{
			get { return _alreadyFetchedCompanyCollectionViaUIMode;}
			set 
			{
				if(_alreadyFetchedCompanyCollectionViaUIMode && !value && (_companyCollectionViaUIMode != null))
				{
					_companyCollectionViaUIMode.Clear();
				}
				_alreadyFetchedCompanyCollectionViaUIMode = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'LanguageEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiLanguageCollectionViaUITabLanguage()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.LanguageCollection LanguageCollectionViaUITabLanguage
		{
			get { return GetMultiLanguageCollectionViaUITabLanguage(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for LanguageCollectionViaUITabLanguage. When set to true, LanguageCollectionViaUITabLanguage is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time LanguageCollectionViaUITabLanguage is accessed. You can always execute a forced fetch by calling GetMultiLanguageCollectionViaUITabLanguage(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchLanguageCollectionViaUITabLanguage
		{
			get	{ return _alwaysFetchLanguageCollectionViaUITabLanguage; }
			set	{ _alwaysFetchLanguageCollectionViaUITabLanguage = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property LanguageCollectionViaUITabLanguage already has been fetched. Setting this property to false when LanguageCollectionViaUITabLanguage has been fetched
		/// will clear the LanguageCollectionViaUITabLanguage collection well. Setting this property to true while LanguageCollectionViaUITabLanguage hasn't been fetched disables lazy loading for LanguageCollectionViaUITabLanguage</summary>
		[Browsable(false)]
		public bool AlreadyFetchedLanguageCollectionViaUITabLanguage
		{
			get { return _alreadyFetchedLanguageCollectionViaUITabLanguage;}
			set 
			{
				if(_alreadyFetchedLanguageCollectionViaUITabLanguage && !value && (_languageCollectionViaUITabLanguage != null))
				{
					_languageCollectionViaUITabLanguage.Clear();
				}
				_alreadyFetchedLanguageCollectionViaUITabLanguage = value;
			}
		}

		/// <summary> Gets / sets related entity of type 'CategoryEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleCategoryEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual CategoryEntity CategoryEntity
		{
			get	{ return GetSingleCategoryEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncCategoryEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "UITabCollection", "CategoryEntity", _categoryEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for CategoryEntity. When set to true, CategoryEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CategoryEntity is accessed. You can always execute a forced fetch by calling GetSingleCategoryEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCategoryEntity
		{
			get	{ return _alwaysFetchCategoryEntity; }
			set	{ _alwaysFetchCategoryEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CategoryEntity already has been fetched. Setting this property to false when CategoryEntity has been fetched
		/// will set CategoryEntity to null as well. Setting this property to true while CategoryEntity hasn't been fetched disables lazy loading for CategoryEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCategoryEntity
		{
			get { return _alreadyFetchedCategoryEntity;}
			set 
			{
				if(_alreadyFetchedCategoryEntity && !value)
				{
					this.CategoryEntity = null;
				}
				_alreadyFetchedCategoryEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property CategoryEntity is not found
		/// in the database. When set to true, CategoryEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool CategoryEntityReturnsNewIfNotFound
		{
			get	{ return _categoryEntityReturnsNewIfNotFound; }
			set { _categoryEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'EntertainmentEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleEntertainmentEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual EntertainmentEntity EntertainmentEntity
		{
			get	{ return GetSingleEntertainmentEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncEntertainmentEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "UITabCollection", "EntertainmentEntity", _entertainmentEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for EntertainmentEntity. When set to true, EntertainmentEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time EntertainmentEntity is accessed. You can always execute a forced fetch by calling GetSingleEntertainmentEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchEntertainmentEntity
		{
			get	{ return _alwaysFetchEntertainmentEntity; }
			set	{ _alwaysFetchEntertainmentEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property EntertainmentEntity already has been fetched. Setting this property to false when EntertainmentEntity has been fetched
		/// will set EntertainmentEntity to null as well. Setting this property to true while EntertainmentEntity hasn't been fetched disables lazy loading for EntertainmentEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedEntertainmentEntity
		{
			get { return _alreadyFetchedEntertainmentEntity;}
			set 
			{
				if(_alreadyFetchedEntertainmentEntity && !value)
				{
					this.EntertainmentEntity = null;
				}
				_alreadyFetchedEntertainmentEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property EntertainmentEntity is not found
		/// in the database. When set to true, EntertainmentEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool EntertainmentEntityReturnsNewIfNotFound
		{
			get	{ return _entertainmentEntityReturnsNewIfNotFound; }
			set { _entertainmentEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'MapEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleMapEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual MapEntity MapEntity
		{
			get	{ return GetSingleMapEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncMapEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "UITabCollection", "MapEntity", _mapEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for MapEntity. When set to true, MapEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time MapEntity is accessed. You can always execute a forced fetch by calling GetSingleMapEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchMapEntity
		{
			get	{ return _alwaysFetchMapEntity; }
			set	{ _alwaysFetchMapEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property MapEntity already has been fetched. Setting this property to false when MapEntity has been fetched
		/// will set MapEntity to null as well. Setting this property to true while MapEntity hasn't been fetched disables lazy loading for MapEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedMapEntity
		{
			get { return _alreadyFetchedMapEntity;}
			set 
			{
				if(_alreadyFetchedMapEntity && !value)
				{
					this.MapEntity = null;
				}
				_alreadyFetchedMapEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property MapEntity is not found
		/// in the database. When set to true, MapEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool MapEntityReturnsNewIfNotFound
		{
			get	{ return _mapEntityReturnsNewIfNotFound; }
			set { _mapEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'SiteEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleSiteEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual SiteEntity SiteEntity
		{
			get	{ return GetSingleSiteEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncSiteEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "UITabCollection", "SiteEntity", _siteEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for SiteEntity. When set to true, SiteEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time SiteEntity is accessed. You can always execute a forced fetch by calling GetSingleSiteEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchSiteEntity
		{
			get	{ return _alwaysFetchSiteEntity; }
			set	{ _alwaysFetchSiteEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property SiteEntity already has been fetched. Setting this property to false when SiteEntity has been fetched
		/// will set SiteEntity to null as well. Setting this property to true while SiteEntity hasn't been fetched disables lazy loading for SiteEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedSiteEntity
		{
			get { return _alreadyFetchedSiteEntity;}
			set 
			{
				if(_alreadyFetchedSiteEntity && !value)
				{
					this.SiteEntity = null;
				}
				_alreadyFetchedSiteEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property SiteEntity is not found
		/// in the database. When set to true, SiteEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool SiteEntityReturnsNewIfNotFound
		{
			get	{ return _siteEntityReturnsNewIfNotFound; }
			set { _siteEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'UIModeEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleUIModeEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual UIModeEntity UIModeEntity
		{
			get	{ return GetSingleUIModeEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncUIModeEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "UITabCollection", "UIModeEntity", _uIModeEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for UIModeEntity. When set to true, UIModeEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time UIModeEntity is accessed. You can always execute a forced fetch by calling GetSingleUIModeEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchUIModeEntity
		{
			get	{ return _alwaysFetchUIModeEntity; }
			set	{ _alwaysFetchUIModeEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property UIModeEntity already has been fetched. Setting this property to false when UIModeEntity has been fetched
		/// will set UIModeEntity to null as well. Setting this property to true while UIModeEntity hasn't been fetched disables lazy loading for UIModeEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedUIModeEntity
		{
			get { return _alreadyFetchedUIModeEntity;}
			set 
			{
				if(_alreadyFetchedUIModeEntity && !value)
				{
					this.UIModeEntity = null;
				}
				_alreadyFetchedUIModeEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property UIModeEntity is not found
		/// in the database. When set to true, UIModeEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool UIModeEntityReturnsNewIfNotFound
		{
			get	{ return _uIModeEntityReturnsNewIfNotFound; }
			set { _uIModeEntityReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the Obymobi.Data.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)Obymobi.Data.EntityType.UITabEntity; }
		}

		#endregion

		
		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
