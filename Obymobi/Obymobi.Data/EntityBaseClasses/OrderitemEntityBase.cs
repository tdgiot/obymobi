﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
using System.Runtime.Serialization;
using System.Data;
using System.Xml.Serialization;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
    // __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity base class which represents the base class for the entity 'Orderitem'.<br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public abstract partial class OrderitemEntityBase : CommonEntityBase
    // __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
    // __LLBLGENPRO_USER_CODE_REGION_END	
	{
		public new string LLBLGenProEntityName {
			get { return "OrderitemEntity"; }
		}
	
		#region Class Member Declarations
		private Obymobi.Data.CollectionClasses.OrderitemAlterationitemCollection	_orderitemAlterationitemCollection;
		private bool	_alwaysFetchOrderitemAlterationitemCollection, _alreadyFetchedOrderitemAlterationitemCollection;
		private Obymobi.Data.CollectionClasses.OrderitemTagCollection	_orderitemTagCollection;
		private bool	_alwaysFetchOrderitemTagCollection, _alreadyFetchedOrderitemTagCollection;
		private CategoryEntity _categoryEntity;
		private bool	_alwaysFetchCategoryEntity, _alreadyFetchedCategoryEntity, _categoryEntityReturnsNewIfNotFound;
		private OrderEntity _orderEntity;
		private bool	_alwaysFetchOrderEntity, _alreadyFetchedOrderEntity, _orderEntityReturnsNewIfNotFound;
		private PriceLevelItemEntity _priceLevelItemEntity;
		private bool	_alwaysFetchPriceLevelItemEntity, _alreadyFetchedPriceLevelItemEntity, _priceLevelItemEntityReturnsNewIfNotFound;
		private ProductEntity _productEntity;
		private bool	_alwaysFetchProductEntity, _alreadyFetchedProductEntity, _productEntityReturnsNewIfNotFound;
		private TaxTariffEntity _taxTariffEntity;
		private bool	_alwaysFetchTaxTariffEntity, _alreadyFetchedTaxTariffEntity, _taxTariffEntityReturnsNewIfNotFound;

        // __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
        // __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name CategoryEntity</summary>
			public static readonly string CategoryEntity = "CategoryEntity";
			/// <summary>Member name OrderEntity</summary>
			public static readonly string OrderEntity = "OrderEntity";
			/// <summary>Member name PriceLevelItemEntity</summary>
			public static readonly string PriceLevelItemEntity = "PriceLevelItemEntity";
			/// <summary>Member name ProductEntity</summary>
			public static readonly string ProductEntity = "ProductEntity";
			/// <summary>Member name TaxTariffEntity</summary>
			public static readonly string TaxTariffEntity = "TaxTariffEntity";
			/// <summary>Member name OrderitemAlterationitemCollection</summary>
			public static readonly string OrderitemAlterationitemCollection = "OrderitemAlterationitemCollection";
			/// <summary>Member name OrderitemTagCollection</summary>
			public static readonly string OrderitemTagCollection = "OrderitemTagCollection";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static OrderitemEntityBase()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		protected OrderitemEntityBase() :base("OrderitemEntity")
		{
			InitClassEmpty(null);
		}

		/// <summary>CTor</summary>
		/// <param name="orderitemId">PK value for Orderitem which data should be fetched into this Orderitem object</param>
		protected OrderitemEntityBase(System.Int32 orderitemId):base("OrderitemEntity")
		{
			InitClassFetch(orderitemId, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="orderitemId">PK value for Orderitem which data should be fetched into this Orderitem object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		protected OrderitemEntityBase(System.Int32 orderitemId, IPrefetchPath prefetchPathToUse): base("OrderitemEntity")
		{
			InitClassFetch(orderitemId, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="orderitemId">PK value for Orderitem which data should be fetched into this Orderitem object</param>
		/// <param name="validator">The custom validator object for this OrderitemEntity</param>
		protected OrderitemEntityBase(System.Int32 orderitemId, IValidator validator):base("OrderitemEntity")
		{
			InitClassFetch(orderitemId, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected OrderitemEntityBase(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_orderitemAlterationitemCollection = (Obymobi.Data.CollectionClasses.OrderitemAlterationitemCollection)info.GetValue("_orderitemAlterationitemCollection", typeof(Obymobi.Data.CollectionClasses.OrderitemAlterationitemCollection));
			_alwaysFetchOrderitemAlterationitemCollection = info.GetBoolean("_alwaysFetchOrderitemAlterationitemCollection");
			_alreadyFetchedOrderitemAlterationitemCollection = info.GetBoolean("_alreadyFetchedOrderitemAlterationitemCollection");

			_orderitemTagCollection = (Obymobi.Data.CollectionClasses.OrderitemTagCollection)info.GetValue("_orderitemTagCollection", typeof(Obymobi.Data.CollectionClasses.OrderitemTagCollection));
			_alwaysFetchOrderitemTagCollection = info.GetBoolean("_alwaysFetchOrderitemTagCollection");
			_alreadyFetchedOrderitemTagCollection = info.GetBoolean("_alreadyFetchedOrderitemTagCollection");
			_categoryEntity = (CategoryEntity)info.GetValue("_categoryEntity", typeof(CategoryEntity));
			if(_categoryEntity!=null)
			{
				_categoryEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_categoryEntityReturnsNewIfNotFound = info.GetBoolean("_categoryEntityReturnsNewIfNotFound");
			_alwaysFetchCategoryEntity = info.GetBoolean("_alwaysFetchCategoryEntity");
			_alreadyFetchedCategoryEntity = info.GetBoolean("_alreadyFetchedCategoryEntity");

			_orderEntity = (OrderEntity)info.GetValue("_orderEntity", typeof(OrderEntity));
			if(_orderEntity!=null)
			{
				_orderEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_orderEntityReturnsNewIfNotFound = info.GetBoolean("_orderEntityReturnsNewIfNotFound");
			_alwaysFetchOrderEntity = info.GetBoolean("_alwaysFetchOrderEntity");
			_alreadyFetchedOrderEntity = info.GetBoolean("_alreadyFetchedOrderEntity");

			_priceLevelItemEntity = (PriceLevelItemEntity)info.GetValue("_priceLevelItemEntity", typeof(PriceLevelItemEntity));
			if(_priceLevelItemEntity!=null)
			{
				_priceLevelItemEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_priceLevelItemEntityReturnsNewIfNotFound = info.GetBoolean("_priceLevelItemEntityReturnsNewIfNotFound");
			_alwaysFetchPriceLevelItemEntity = info.GetBoolean("_alwaysFetchPriceLevelItemEntity");
			_alreadyFetchedPriceLevelItemEntity = info.GetBoolean("_alreadyFetchedPriceLevelItemEntity");

			_productEntity = (ProductEntity)info.GetValue("_productEntity", typeof(ProductEntity));
			if(_productEntity!=null)
			{
				_productEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_productEntityReturnsNewIfNotFound = info.GetBoolean("_productEntityReturnsNewIfNotFound");
			_alwaysFetchProductEntity = info.GetBoolean("_alwaysFetchProductEntity");
			_alreadyFetchedProductEntity = info.GetBoolean("_alreadyFetchedProductEntity");

			_taxTariffEntity = (TaxTariffEntity)info.GetValue("_taxTariffEntity", typeof(TaxTariffEntity));
			if(_taxTariffEntity!=null)
			{
				_taxTariffEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_taxTariffEntityReturnsNewIfNotFound = info.GetBoolean("_taxTariffEntityReturnsNewIfNotFound");
			_alwaysFetchTaxTariffEntity = info.GetBoolean("_alwaysFetchTaxTariffEntity");
			_alreadyFetchedTaxTariffEntity = info.GetBoolean("_alreadyFetchedTaxTariffEntity");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
            // __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
            // __LLBLGENPRO_USER_CODE_REGION_END
		}	
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((OrderitemFieldIndex)fieldIndex)
			{
				case OrderitemFieldIndex.OrderId:
					DesetupSyncOrderEntity(true, false);
					_alreadyFetchedOrderEntity = false;
					break;
				case OrderitemFieldIndex.ProductId:
					DesetupSyncProductEntity(true, false);
					_alreadyFetchedProductEntity = false;
					break;
				case OrderitemFieldIndex.CategoryId:
					DesetupSyncCategoryEntity(true, false);
					_alreadyFetchedCategoryEntity = false;
					break;
				case OrderitemFieldIndex.PriceLevelItemId:
					DesetupSyncPriceLevelItemEntity(true, false);
					_alreadyFetchedPriceLevelItemEntity = false;
					break;
				case OrderitemFieldIndex.TaxTariffId:
					DesetupSyncTaxTariffEntity(true, false);
					_alreadyFetchedTaxTariffEntity = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedOrderitemAlterationitemCollection = (_orderitemAlterationitemCollection.Count > 0);
			_alreadyFetchedOrderitemTagCollection = (_orderitemTagCollection.Count > 0);
			_alreadyFetchedCategoryEntity = (_categoryEntity != null);
			_alreadyFetchedOrderEntity = (_orderEntity != null);
			_alreadyFetchedPriceLevelItemEntity = (_priceLevelItemEntity != null);
			_alreadyFetchedProductEntity = (_productEntity != null);
			_alreadyFetchedTaxTariffEntity = (_taxTariffEntity != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "CategoryEntity":
					toReturn.Add(Relations.CategoryEntityUsingCategoryId);
					break;
				case "OrderEntity":
					toReturn.Add(Relations.OrderEntityUsingOrderId);
					break;
				case "PriceLevelItemEntity":
					toReturn.Add(Relations.PriceLevelItemEntityUsingPriceLevelItemId);
					break;
				case "ProductEntity":
					toReturn.Add(Relations.ProductEntityUsingProductId);
					break;
				case "TaxTariffEntity":
					toReturn.Add(Relations.TaxTariffEntityUsingTaxTariffId);
					break;
				case "OrderitemAlterationitemCollection":
					toReturn.Add(Relations.OrderitemAlterationitemEntityUsingOrderitemId);
					break;
				case "OrderitemTagCollection":
					toReturn.Add(Relations.OrderitemTagEntityUsingOrderitemId);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_orderitemAlterationitemCollection", (!this.MarkedForDeletion?_orderitemAlterationitemCollection:null));
			info.AddValue("_alwaysFetchOrderitemAlterationitemCollection", _alwaysFetchOrderitemAlterationitemCollection);
			info.AddValue("_alreadyFetchedOrderitemAlterationitemCollection", _alreadyFetchedOrderitemAlterationitemCollection);
			info.AddValue("_orderitemTagCollection", (!this.MarkedForDeletion?_orderitemTagCollection:null));
			info.AddValue("_alwaysFetchOrderitemTagCollection", _alwaysFetchOrderitemTagCollection);
			info.AddValue("_alreadyFetchedOrderitemTagCollection", _alreadyFetchedOrderitemTagCollection);
			info.AddValue("_categoryEntity", (!this.MarkedForDeletion?_categoryEntity:null));
			info.AddValue("_categoryEntityReturnsNewIfNotFound", _categoryEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchCategoryEntity", _alwaysFetchCategoryEntity);
			info.AddValue("_alreadyFetchedCategoryEntity", _alreadyFetchedCategoryEntity);
			info.AddValue("_orderEntity", (!this.MarkedForDeletion?_orderEntity:null));
			info.AddValue("_orderEntityReturnsNewIfNotFound", _orderEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchOrderEntity", _alwaysFetchOrderEntity);
			info.AddValue("_alreadyFetchedOrderEntity", _alreadyFetchedOrderEntity);
			info.AddValue("_priceLevelItemEntity", (!this.MarkedForDeletion?_priceLevelItemEntity:null));
			info.AddValue("_priceLevelItemEntityReturnsNewIfNotFound", _priceLevelItemEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchPriceLevelItemEntity", _alwaysFetchPriceLevelItemEntity);
			info.AddValue("_alreadyFetchedPriceLevelItemEntity", _alreadyFetchedPriceLevelItemEntity);
			info.AddValue("_productEntity", (!this.MarkedForDeletion?_productEntity:null));
			info.AddValue("_productEntityReturnsNewIfNotFound", _productEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchProductEntity", _alwaysFetchProductEntity);
			info.AddValue("_alreadyFetchedProductEntity", _alreadyFetchedProductEntity);
			info.AddValue("_taxTariffEntity", (!this.MarkedForDeletion?_taxTariffEntity:null));
			info.AddValue("_taxTariffEntityReturnsNewIfNotFound", _taxTariffEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchTaxTariffEntity", _alwaysFetchTaxTariffEntity);
			info.AddValue("_alreadyFetchedTaxTariffEntity", _alreadyFetchedTaxTariffEntity);

            // __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
            // __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "CategoryEntity":
					_alreadyFetchedCategoryEntity = true;
					this.CategoryEntity = (CategoryEntity)entity;
					break;
				case "OrderEntity":
					_alreadyFetchedOrderEntity = true;
					this.OrderEntity = (OrderEntity)entity;
					break;
				case "PriceLevelItemEntity":
					_alreadyFetchedPriceLevelItemEntity = true;
					this.PriceLevelItemEntity = (PriceLevelItemEntity)entity;
					break;
				case "ProductEntity":
					_alreadyFetchedProductEntity = true;
					this.ProductEntity = (ProductEntity)entity;
					break;
				case "TaxTariffEntity":
					_alreadyFetchedTaxTariffEntity = true;
					this.TaxTariffEntity = (TaxTariffEntity)entity;
					break;
				case "OrderitemAlterationitemCollection":
					_alreadyFetchedOrderitemAlterationitemCollection = true;
					if(entity!=null)
					{
						this.OrderitemAlterationitemCollection.Add((OrderitemAlterationitemEntity)entity);
					}
					break;
				case "OrderitemTagCollection":
					_alreadyFetchedOrderitemTagCollection = true;
					if(entity!=null)
					{
						this.OrderitemTagCollection.Add((OrderitemTagEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "CategoryEntity":
					SetupSyncCategoryEntity(relatedEntity);
					break;
				case "OrderEntity":
					SetupSyncOrderEntity(relatedEntity);
					break;
				case "PriceLevelItemEntity":
					SetupSyncPriceLevelItemEntity(relatedEntity);
					break;
				case "ProductEntity":
					SetupSyncProductEntity(relatedEntity);
					break;
				case "TaxTariffEntity":
					SetupSyncTaxTariffEntity(relatedEntity);
					break;
				case "OrderitemAlterationitemCollection":
					_orderitemAlterationitemCollection.Add((OrderitemAlterationitemEntity)relatedEntity);
					break;
				case "OrderitemTagCollection":
					_orderitemTagCollection.Add((OrderitemTagEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "CategoryEntity":
					DesetupSyncCategoryEntity(false, true);
					break;
				case "OrderEntity":
					DesetupSyncOrderEntity(false, true);
					break;
				case "PriceLevelItemEntity":
					DesetupSyncPriceLevelItemEntity(false, true);
					break;
				case "ProductEntity":
					DesetupSyncProductEntity(false, true);
					break;
				case "TaxTariffEntity":
					DesetupSyncTaxTariffEntity(false, true);
					break;
				case "OrderitemAlterationitemCollection":
					this.PerformRelatedEntityRemoval(_orderitemAlterationitemCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "OrderitemTagCollection":
					this.PerformRelatedEntityRemoval(_orderitemTagCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_categoryEntity!=null)
			{
				toReturn.Add(_categoryEntity);
			}
			if(_orderEntity!=null)
			{
				toReturn.Add(_orderEntity);
			}
			if(_priceLevelItemEntity!=null)
			{
				toReturn.Add(_priceLevelItemEntity);
			}
			if(_productEntity!=null)
			{
				toReturn.Add(_productEntity);
			}
			if(_taxTariffEntity!=null)
			{
				toReturn.Add(_taxTariffEntity);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_orderitemAlterationitemCollection);
			toReturn.Add(_orderitemTagCollection);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="orderitemId">PK value for Orderitem which data should be fetched into this Orderitem object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 orderitemId)
		{
			return FetchUsingPK(orderitemId, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="orderitemId">PK value for Orderitem which data should be fetched into this Orderitem object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 orderitemId, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(orderitemId, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="orderitemId">PK value for Orderitem which data should be fetched into this Orderitem object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 orderitemId, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(orderitemId, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="orderitemId">PK value for Orderitem which data should be fetched into this Orderitem object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 orderitemId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(orderitemId, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.OrderitemId, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new OrderitemRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'OrderitemAlterationitemEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'OrderitemAlterationitemEntity'</returns>
		public Obymobi.Data.CollectionClasses.OrderitemAlterationitemCollection GetMultiOrderitemAlterationitemCollection(bool forceFetch)
		{
			return GetMultiOrderitemAlterationitemCollection(forceFetch, _orderitemAlterationitemCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'OrderitemAlterationitemEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'OrderitemAlterationitemEntity'</returns>
		public Obymobi.Data.CollectionClasses.OrderitemAlterationitemCollection GetMultiOrderitemAlterationitemCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiOrderitemAlterationitemCollection(forceFetch, _orderitemAlterationitemCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'OrderitemAlterationitemEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.OrderitemAlterationitemCollection GetMultiOrderitemAlterationitemCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiOrderitemAlterationitemCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'OrderitemAlterationitemEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.OrderitemAlterationitemCollection GetMultiOrderitemAlterationitemCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedOrderitemAlterationitemCollection || forceFetch || _alwaysFetchOrderitemAlterationitemCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_orderitemAlterationitemCollection);
				_orderitemAlterationitemCollection.SuppressClearInGetMulti=!forceFetch;
				_orderitemAlterationitemCollection.EntityFactoryToUse = entityFactoryToUse;
				_orderitemAlterationitemCollection.GetMultiManyToOne(null, null, this, null, null, null, filter);
				_orderitemAlterationitemCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedOrderitemAlterationitemCollection = true;
			}
			return _orderitemAlterationitemCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'OrderitemAlterationitemCollection'. These settings will be taken into account
		/// when the property OrderitemAlterationitemCollection is requested or GetMultiOrderitemAlterationitemCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersOrderitemAlterationitemCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_orderitemAlterationitemCollection.SortClauses=sortClauses;
			_orderitemAlterationitemCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'OrderitemTagEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'OrderitemTagEntity'</returns>
		public Obymobi.Data.CollectionClasses.OrderitemTagCollection GetMultiOrderitemTagCollection(bool forceFetch)
		{
			return GetMultiOrderitemTagCollection(forceFetch, _orderitemTagCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'OrderitemTagEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'OrderitemTagEntity'</returns>
		public Obymobi.Data.CollectionClasses.OrderitemTagCollection GetMultiOrderitemTagCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiOrderitemTagCollection(forceFetch, _orderitemTagCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'OrderitemTagEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.OrderitemTagCollection GetMultiOrderitemTagCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiOrderitemTagCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'OrderitemTagEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.OrderitemTagCollection GetMultiOrderitemTagCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedOrderitemTagCollection || forceFetch || _alwaysFetchOrderitemTagCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_orderitemTagCollection);
				_orderitemTagCollection.SuppressClearInGetMulti=!forceFetch;
				_orderitemTagCollection.EntityFactoryToUse = entityFactoryToUse;
				_orderitemTagCollection.GetMultiManyToOne(this, null, null, null, filter);
				_orderitemTagCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedOrderitemTagCollection = true;
			}
			return _orderitemTagCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'OrderitemTagCollection'. These settings will be taken into account
		/// when the property OrderitemTagCollection is requested or GetMultiOrderitemTagCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersOrderitemTagCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_orderitemTagCollection.SortClauses=sortClauses;
			_orderitemTagCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves the related entity of type 'CategoryEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'CategoryEntity' which is related to this entity.</returns>
		public CategoryEntity GetSingleCategoryEntity()
		{
			return GetSingleCategoryEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'CategoryEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'CategoryEntity' which is related to this entity.</returns>
		public virtual CategoryEntity GetSingleCategoryEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedCategoryEntity || forceFetch || _alwaysFetchCategoryEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.CategoryEntityUsingCategoryId);
				CategoryEntity newEntity = new CategoryEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.CategoryId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (CategoryEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_categoryEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.CategoryEntity = newEntity;
				_alreadyFetchedCategoryEntity = fetchResult;
			}
			return _categoryEntity;
		}


		/// <summary> Retrieves the related entity of type 'OrderEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'OrderEntity' which is related to this entity.</returns>
		public OrderEntity GetSingleOrderEntity()
		{
			return GetSingleOrderEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'OrderEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'OrderEntity' which is related to this entity.</returns>
		public virtual OrderEntity GetSingleOrderEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedOrderEntity || forceFetch || _alwaysFetchOrderEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.OrderEntityUsingOrderId);
				OrderEntity newEntity = new OrderEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.OrderId);
				}
				if(fetchResult)
				{
					newEntity = (OrderEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_orderEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.OrderEntity = newEntity;
				_alreadyFetchedOrderEntity = fetchResult;
			}
			return _orderEntity;
		}


		/// <summary> Retrieves the related entity of type 'PriceLevelItemEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'PriceLevelItemEntity' which is related to this entity.</returns>
		public PriceLevelItemEntity GetSinglePriceLevelItemEntity()
		{
			return GetSinglePriceLevelItemEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'PriceLevelItemEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'PriceLevelItemEntity' which is related to this entity.</returns>
		public virtual PriceLevelItemEntity GetSinglePriceLevelItemEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedPriceLevelItemEntity || forceFetch || _alwaysFetchPriceLevelItemEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.PriceLevelItemEntityUsingPriceLevelItemId);
				PriceLevelItemEntity newEntity = new PriceLevelItemEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.PriceLevelItemId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (PriceLevelItemEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_priceLevelItemEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.PriceLevelItemEntity = newEntity;
				_alreadyFetchedPriceLevelItemEntity = fetchResult;
			}
			return _priceLevelItemEntity;
		}


		/// <summary> Retrieves the related entity of type 'ProductEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ProductEntity' which is related to this entity.</returns>
		public ProductEntity GetSingleProductEntity()
		{
			return GetSingleProductEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'ProductEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ProductEntity' which is related to this entity.</returns>
		public virtual ProductEntity GetSingleProductEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedProductEntity || forceFetch || _alwaysFetchProductEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ProductEntityUsingProductId);
				ProductEntity newEntity = new ProductEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ProductId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (ProductEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_productEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.ProductEntity = newEntity;
				_alreadyFetchedProductEntity = fetchResult;
			}
			return _productEntity;
		}


		/// <summary> Retrieves the related entity of type 'TaxTariffEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'TaxTariffEntity' which is related to this entity.</returns>
		public TaxTariffEntity GetSingleTaxTariffEntity()
		{
			return GetSingleTaxTariffEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'TaxTariffEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'TaxTariffEntity' which is related to this entity.</returns>
		public virtual TaxTariffEntity GetSingleTaxTariffEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedTaxTariffEntity || forceFetch || _alwaysFetchTaxTariffEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.TaxTariffEntityUsingTaxTariffId);
				TaxTariffEntity newEntity = new TaxTariffEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.TaxTariffId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (TaxTariffEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_taxTariffEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.TaxTariffEntity = newEntity;
				_alreadyFetchedTaxTariffEntity = fetchResult;
			}
			return _taxTariffEntity;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("CategoryEntity", _categoryEntity);
			toReturn.Add("OrderEntity", _orderEntity);
			toReturn.Add("PriceLevelItemEntity", _priceLevelItemEntity);
			toReturn.Add("ProductEntity", _productEntity);
			toReturn.Add("TaxTariffEntity", _taxTariffEntity);
			toReturn.Add("OrderitemAlterationitemCollection", _orderitemAlterationitemCollection);
			toReturn.Add("OrderitemTagCollection", _orderitemTagCollection);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

            // __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
            // __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="orderitemId">PK value for Orderitem which data should be fetched into this Orderitem object</param>
		/// <param name="validator">The validator object for this OrderitemEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int32 orderitemId, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(orderitemId, prefetchPathToUse, null, null);

            // __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
            // __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_orderitemAlterationitemCollection = new Obymobi.Data.CollectionClasses.OrderitemAlterationitemCollection();
			_orderitemAlterationitemCollection.SetContainingEntityInfo(this, "OrderitemEntity");

			_orderitemTagCollection = new Obymobi.Data.CollectionClasses.OrderitemTagCollection();
			_orderitemTagCollection.SetContainingEntityInfo(this, "OrderitemEntity");
			_categoryEntityReturnsNewIfNotFound = true;
			_orderEntityReturnsNewIfNotFound = true;
			_priceLevelItemEntityReturnsNewIfNotFound = true;
			_productEntityReturnsNewIfNotFound = true;
			_taxTariffEntityReturnsNewIfNotFound = true;
			PerformDependencyInjection();

            // __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
            // __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("OrderitemId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("OrderId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ProductId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("XProductDescription", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ProductName", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ProductPriceIn", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Quantity", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("XPriceIn", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("VatPercentage", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Notes", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Guid", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CategoryId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Color", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CategoryName", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("OrderSource", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ParentCompanyId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CategoryPath", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PriceLevelItemId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PriceLevelLog", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Type", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PriceInTax", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PriceExTax", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PriceTotalInTax", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PriceTotalExTax", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TaxPercentage", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Tax", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TaxTariffId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TaxTotal", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TaxName", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ExternalIdentifier", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _categoryEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncCategoryEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _categoryEntity, new PropertyChangedEventHandler( OnCategoryEntityPropertyChanged ), "CategoryEntity", Obymobi.Data.RelationClasses.StaticOrderitemRelations.CategoryEntityUsingCategoryIdStatic, true, signalRelatedEntity, "OrderitemCollection", resetFKFields, new int[] { (int)OrderitemFieldIndex.CategoryId } );		
			_categoryEntity = null;
		}
		
		/// <summary> setups the sync logic for member _categoryEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncCategoryEntity(IEntityCore relatedEntity)
		{
			if(_categoryEntity!=relatedEntity)
			{		
				DesetupSyncCategoryEntity(true, true);
				_categoryEntity = (CategoryEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _categoryEntity, new PropertyChangedEventHandler( OnCategoryEntityPropertyChanged ), "CategoryEntity", Obymobi.Data.RelationClasses.StaticOrderitemRelations.CategoryEntityUsingCategoryIdStatic, true, ref _alreadyFetchedCategoryEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnCategoryEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _orderEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncOrderEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _orderEntity, new PropertyChangedEventHandler( OnOrderEntityPropertyChanged ), "OrderEntity", Obymobi.Data.RelationClasses.StaticOrderitemRelations.OrderEntityUsingOrderIdStatic, true, signalRelatedEntity, "OrderitemCollection", resetFKFields, new int[] { (int)OrderitemFieldIndex.OrderId } );		
			_orderEntity = null;
		}
		
		/// <summary> setups the sync logic for member _orderEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncOrderEntity(IEntityCore relatedEntity)
		{
			if(_orderEntity!=relatedEntity)
			{		
				DesetupSyncOrderEntity(true, true);
				_orderEntity = (OrderEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _orderEntity, new PropertyChangedEventHandler( OnOrderEntityPropertyChanged ), "OrderEntity", Obymobi.Data.RelationClasses.StaticOrderitemRelations.OrderEntityUsingOrderIdStatic, true, ref _alreadyFetchedOrderEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnOrderEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _priceLevelItemEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncPriceLevelItemEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _priceLevelItemEntity, new PropertyChangedEventHandler( OnPriceLevelItemEntityPropertyChanged ), "PriceLevelItemEntity", Obymobi.Data.RelationClasses.StaticOrderitemRelations.PriceLevelItemEntityUsingPriceLevelItemIdStatic, true, signalRelatedEntity, "OrderitemCollection", resetFKFields, new int[] { (int)OrderitemFieldIndex.PriceLevelItemId } );		
			_priceLevelItemEntity = null;
		}
		
		/// <summary> setups the sync logic for member _priceLevelItemEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncPriceLevelItemEntity(IEntityCore relatedEntity)
		{
			if(_priceLevelItemEntity!=relatedEntity)
			{		
				DesetupSyncPriceLevelItemEntity(true, true);
				_priceLevelItemEntity = (PriceLevelItemEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _priceLevelItemEntity, new PropertyChangedEventHandler( OnPriceLevelItemEntityPropertyChanged ), "PriceLevelItemEntity", Obymobi.Data.RelationClasses.StaticOrderitemRelations.PriceLevelItemEntityUsingPriceLevelItemIdStatic, true, ref _alreadyFetchedPriceLevelItemEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnPriceLevelItemEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _productEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncProductEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _productEntity, new PropertyChangedEventHandler( OnProductEntityPropertyChanged ), "ProductEntity", Obymobi.Data.RelationClasses.StaticOrderitemRelations.ProductEntityUsingProductIdStatic, true, signalRelatedEntity, "OrderitemCollection", resetFKFields, new int[] { (int)OrderitemFieldIndex.ProductId } );		
			_productEntity = null;
		}
		
		/// <summary> setups the sync logic for member _productEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncProductEntity(IEntityCore relatedEntity)
		{
			if(_productEntity!=relatedEntity)
			{		
				DesetupSyncProductEntity(true, true);
				_productEntity = (ProductEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _productEntity, new PropertyChangedEventHandler( OnProductEntityPropertyChanged ), "ProductEntity", Obymobi.Data.RelationClasses.StaticOrderitemRelations.ProductEntityUsingProductIdStatic, true, ref _alreadyFetchedProductEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnProductEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _taxTariffEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncTaxTariffEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _taxTariffEntity, new PropertyChangedEventHandler( OnTaxTariffEntityPropertyChanged ), "TaxTariffEntity", Obymobi.Data.RelationClasses.StaticOrderitemRelations.TaxTariffEntityUsingTaxTariffIdStatic, true, signalRelatedEntity, "OrderitemCollection", resetFKFields, new int[] { (int)OrderitemFieldIndex.TaxTariffId } );		
			_taxTariffEntity = null;
		}
		
		/// <summary> setups the sync logic for member _taxTariffEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncTaxTariffEntity(IEntityCore relatedEntity)
		{
			if(_taxTariffEntity!=relatedEntity)
			{		
				DesetupSyncTaxTariffEntity(true, true);
				_taxTariffEntity = (TaxTariffEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _taxTariffEntity, new PropertyChangedEventHandler( OnTaxTariffEntityPropertyChanged ), "TaxTariffEntity", Obymobi.Data.RelationClasses.StaticOrderitemRelations.TaxTariffEntityUsingTaxTariffIdStatic, true, ref _alreadyFetchedTaxTariffEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnTaxTariffEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="orderitemId">PK value for Orderitem which data should be fetched into this Orderitem object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int32 orderitemId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)OrderitemFieldIndex.OrderitemId].ForcedCurrentValueWrite(orderitemId);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateOrderitemDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new OrderitemEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static OrderitemRelations Relations
		{
			get	{ return new OrderitemRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'OrderitemAlterationitem' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathOrderitemAlterationitemCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.OrderitemAlterationitemCollection(), (IEntityRelation)GetRelationsForField("OrderitemAlterationitemCollection")[0], (int)Obymobi.Data.EntityType.OrderitemEntity, (int)Obymobi.Data.EntityType.OrderitemAlterationitemEntity, 0, null, null, null, "OrderitemAlterationitemCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'OrderitemTag' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathOrderitemTagCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.OrderitemTagCollection(), (IEntityRelation)GetRelationsForField("OrderitemTagCollection")[0], (int)Obymobi.Data.EntityType.OrderitemEntity, (int)Obymobi.Data.EntityType.OrderitemTagEntity, 0, null, null, null, "OrderitemTagCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Category'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCategoryEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CategoryCollection(), (IEntityRelation)GetRelationsForField("CategoryEntity")[0], (int)Obymobi.Data.EntityType.OrderitemEntity, (int)Obymobi.Data.EntityType.CategoryEntity, 0, null, null, null, "CategoryEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Order'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathOrderEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.OrderCollection(), (IEntityRelation)GetRelationsForField("OrderEntity")[0], (int)Obymobi.Data.EntityType.OrderitemEntity, (int)Obymobi.Data.EntityType.OrderEntity, 0, null, null, null, "OrderEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'PriceLevelItem'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPriceLevelItemEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.PriceLevelItemCollection(), (IEntityRelation)GetRelationsForField("PriceLevelItemEntity")[0], (int)Obymobi.Data.EntityType.OrderitemEntity, (int)Obymobi.Data.EntityType.PriceLevelItemEntity, 0, null, null, null, "PriceLevelItemEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Product'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathProductEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ProductCollection(), (IEntityRelation)GetRelationsForField("ProductEntity")[0], (int)Obymobi.Data.EntityType.OrderitemEntity, (int)Obymobi.Data.EntityType.ProductEntity, 0, null, null, null, "ProductEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'TaxTariff'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTaxTariffEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.TaxTariffCollection(), (IEntityRelation)GetRelationsForField("TaxTariffEntity")[0], (int)Obymobi.Data.EntityType.OrderitemEntity, (int)Obymobi.Data.EntityType.TaxTariffEntity, 0, null, null, null, "TaxTariffEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The OrderitemId property of the Entity Orderitem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Orderitem"."OrderitemId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 OrderitemId
		{
			get { return (System.Int32)GetValue((int)OrderitemFieldIndex.OrderitemId, true); }
			set	{ SetValue((int)OrderitemFieldIndex.OrderitemId, value, true); }
		}

		/// <summary> The OrderId property of the Entity Orderitem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Orderitem"."OrderId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 OrderId
		{
			get { return (System.Int32)GetValue((int)OrderitemFieldIndex.OrderId, true); }
			set	{ SetValue((int)OrderitemFieldIndex.OrderId, value, true); }
		}

		/// <summary> The ProductId property of the Entity Orderitem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Orderitem"."ProductId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ProductId
		{
			get { return (Nullable<System.Int32>)GetValue((int)OrderitemFieldIndex.ProductId, false); }
			set	{ SetValue((int)OrderitemFieldIndex.ProductId, value, true); }
		}

		/// <summary> The XProductDescription property of the Entity Orderitem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Orderitem"."ProductDescription"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 255<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String XProductDescription
		{
			get { return (System.String)GetValue((int)OrderitemFieldIndex.XProductDescription, true); }
			set	{ SetValue((int)OrderitemFieldIndex.XProductDescription, value, true); }
		}

		/// <summary> The ProductName property of the Entity Orderitem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Orderitem"."ProductName"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 255<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ProductName
		{
			get { return (System.String)GetValue((int)OrderitemFieldIndex.ProductName, true); }
			set	{ SetValue((int)OrderitemFieldIndex.ProductName, value, true); }
		}

		/// <summary> The ProductPriceIn property of the Entity Orderitem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Orderitem"."ProductPriceIn"<br/>
		/// Table field type characteristics (type, precision, scale, length): Money, 19, 4, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Decimal ProductPriceIn
		{
			get { return (System.Decimal)GetValue((int)OrderitemFieldIndex.ProductPriceIn, true); }
			set	{ SetValue((int)OrderitemFieldIndex.ProductPriceIn, value, true); }
		}

		/// <summary> The Quantity property of the Entity Orderitem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Orderitem"."Quantity"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 Quantity
		{
			get { return (System.Int32)GetValue((int)OrderitemFieldIndex.Quantity, true); }
			set	{ SetValue((int)OrderitemFieldIndex.Quantity, value, true); }
		}

		/// <summary> The XPriceIn property of the Entity Orderitem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Orderitem"."PriceIn"<br/>
		/// Table field type characteristics (type, precision, scale, length): Money, 19, 4, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Decimal> XPriceIn
		{
			get { return (Nullable<System.Decimal>)GetValue((int)OrderitemFieldIndex.XPriceIn, false); }
			set	{ SetValue((int)OrderitemFieldIndex.XPriceIn, value, true); }
		}

		/// <summary> The VatPercentage property of the Entity Orderitem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Orderitem"."VatPercentage"<br/>
		/// Table field type characteristics (type, precision, scale, length): Float, 38, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Double VatPercentage
		{
			get { return (System.Double)GetValue((int)OrderitemFieldIndex.VatPercentage, true); }
			set	{ SetValue((int)OrderitemFieldIndex.VatPercentage, value, true); }
		}

		/// <summary> The Notes property of the Entity Orderitem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Orderitem"."Notes"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Notes
		{
			get { return (System.String)GetValue((int)OrderitemFieldIndex.Notes, true); }
			set	{ SetValue((int)OrderitemFieldIndex.Notes, value, true); }
		}

		/// <summary> The CreatedBy property of the Entity Orderitem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Orderitem"."CreatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CreatedBy
		{
			get { return (System.Int32)GetValue((int)OrderitemFieldIndex.CreatedBy, true); }
			set	{ SetValue((int)OrderitemFieldIndex.CreatedBy, value, true); }
		}

		/// <summary> The UpdatedBy property of the Entity Orderitem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Orderitem"."UpdatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 UpdatedBy
		{
			get { return (System.Int32)GetValue((int)OrderitemFieldIndex.UpdatedBy, true); }
			set	{ SetValue((int)OrderitemFieldIndex.UpdatedBy, value, true); }
		}

		/// <summary> The Guid property of the Entity Orderitem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Orderitem"."Guid"<br/>
		/// Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 128<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Guid
		{
			get { return (System.String)GetValue((int)OrderitemFieldIndex.Guid, true); }
			set	{ SetValue((int)OrderitemFieldIndex.Guid, value, true); }
		}

		/// <summary> The CategoryId property of the Entity Orderitem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Orderitem"."CategoryId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> CategoryId
		{
			get { return (Nullable<System.Int32>)GetValue((int)OrderitemFieldIndex.CategoryId, false); }
			set	{ SetValue((int)OrderitemFieldIndex.CategoryId, value, true); }
		}

		/// <summary> The Color property of the Entity Orderitem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Orderitem"."Color"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 Color
		{
			get { return (System.Int32)GetValue((int)OrderitemFieldIndex.Color, true); }
			set	{ SetValue((int)OrderitemFieldIndex.Color, value, true); }
		}

		/// <summary> The CategoryName property of the Entity Orderitem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Orderitem"."CategoryName"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 500<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String CategoryName
		{
			get { return (System.String)GetValue((int)OrderitemFieldIndex.CategoryName, true); }
			set	{ SetValue((int)OrderitemFieldIndex.CategoryName, value, true); }
		}

		/// <summary> The OrderSource property of the Entity Orderitem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Orderitem"."OrderSource"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 OrderSource
		{
			get { return (System.Int32)GetValue((int)OrderitemFieldIndex.OrderSource, true); }
			set	{ SetValue((int)OrderitemFieldIndex.OrderSource, value, true); }
		}

		/// <summary> The ParentCompanyId property of the Entity Orderitem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Orderitem"."ParentCompanyId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 ParentCompanyId
		{
			get { return (System.Int32)GetValue((int)OrderitemFieldIndex.ParentCompanyId, true); }
			set	{ SetValue((int)OrderitemFieldIndex.ParentCompanyId, value, true); }
		}

		/// <summary> The CreatedUTC property of the Entity Orderitem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Orderitem"."CreatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CreatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)OrderitemFieldIndex.CreatedUTC, false); }
			set	{ SetValue((int)OrderitemFieldIndex.CreatedUTC, value, true); }
		}

		/// <summary> The UpdatedUTC property of the Entity Orderitem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Orderitem"."UpdatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)OrderitemFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)OrderitemFieldIndex.UpdatedUTC, value, true); }
		}

		/// <summary> The CategoryPath property of the Entity Orderitem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Orderitem"."CategoryPath"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String CategoryPath
		{
			get { return (System.String)GetValue((int)OrderitemFieldIndex.CategoryPath, true); }
			set	{ SetValue((int)OrderitemFieldIndex.CategoryPath, value, true); }
		}

		/// <summary> The PriceLevelItemId property of the Entity Orderitem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Orderitem"."PriceLevelItemId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> PriceLevelItemId
		{
			get { return (Nullable<System.Int32>)GetValue((int)OrderitemFieldIndex.PriceLevelItemId, false); }
			set	{ SetValue((int)OrderitemFieldIndex.PriceLevelItemId, value, true); }
		}

		/// <summary> The PriceLevelLog property of the Entity Orderitem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Orderitem"."PriceLevelLog"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String PriceLevelLog
		{
			get { return (System.String)GetValue((int)OrderitemFieldIndex.PriceLevelLog, true); }
			set	{ SetValue((int)OrderitemFieldIndex.PriceLevelLog, value, true); }
		}

		/// <summary> The Type property of the Entity Orderitem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Orderitem"."Type"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual Obymobi.Enums.OrderitemType Type
		{
			get { return (Obymobi.Enums.OrderitemType)GetValue((int)OrderitemFieldIndex.Type, true); }
			set	{ SetValue((int)OrderitemFieldIndex.Type, value, true); }
		}

		/// <summary> The PriceInTax property of the Entity Orderitem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Orderitem"."PriceInTax"<br/>
		/// Table field type characteristics (type, precision, scale, length): Money, 19, 4, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Decimal PriceInTax
		{
			get { return (System.Decimal)GetValue((int)OrderitemFieldIndex.PriceInTax, true); }
			set	{ SetValue((int)OrderitemFieldIndex.PriceInTax, value, true); }
		}

		/// <summary> The PriceExTax property of the Entity Orderitem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Orderitem"."PriceExTax"<br/>
		/// Table field type characteristics (type, precision, scale, length): Money, 19, 4, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Decimal PriceExTax
		{
			get { return (System.Decimal)GetValue((int)OrderitemFieldIndex.PriceExTax, true); }
			set	{ SetValue((int)OrderitemFieldIndex.PriceExTax, value, true); }
		}

		/// <summary> The PriceTotalInTax property of the Entity Orderitem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Orderitem"."PriceTotalInTax"<br/>
		/// Table field type characteristics (type, precision, scale, length): Money, 19, 4, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Decimal PriceTotalInTax
		{
			get { return (System.Decimal)GetValue((int)OrderitemFieldIndex.PriceTotalInTax, true); }
			set	{ SetValue((int)OrderitemFieldIndex.PriceTotalInTax, value, true); }
		}

		/// <summary> The PriceTotalExTax property of the Entity Orderitem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Orderitem"."PriceTotalExTax"<br/>
		/// Table field type characteristics (type, precision, scale, length): Money, 19, 4, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Decimal PriceTotalExTax
		{
			get { return (System.Decimal)GetValue((int)OrderitemFieldIndex.PriceTotalExTax, true); }
			set	{ SetValue((int)OrderitemFieldIndex.PriceTotalExTax, value, true); }
		}

		/// <summary> The TaxPercentage property of the Entity Orderitem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Orderitem"."TaxPercentage"<br/>
		/// Table field type characteristics (type, precision, scale, length): Float, 38, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Double TaxPercentage
		{
			get { return (System.Double)GetValue((int)OrderitemFieldIndex.TaxPercentage, true); }
			set	{ SetValue((int)OrderitemFieldIndex.TaxPercentage, value, true); }
		}

		/// <summary> The Tax property of the Entity Orderitem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Orderitem"."Tax"<br/>
		/// Table field type characteristics (type, precision, scale, length): Money, 19, 4, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Decimal Tax
		{
			get { return (System.Decimal)GetValue((int)OrderitemFieldIndex.Tax, true); }
			set	{ SetValue((int)OrderitemFieldIndex.Tax, value, true); }
		}

		/// <summary> The TaxTariffId property of the Entity Orderitem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Orderitem"."TaxTariffId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> TaxTariffId
		{
			get { return (Nullable<System.Int32>)GetValue((int)OrderitemFieldIndex.TaxTariffId, false); }
			set	{ SetValue((int)OrderitemFieldIndex.TaxTariffId, value, true); }
		}

		/// <summary> The TaxTotal property of the Entity Orderitem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Orderitem"."TaxTotal"<br/>
		/// Table field type characteristics (type, precision, scale, length): Money, 19, 4, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Decimal TaxTotal
		{
			get { return (System.Decimal)GetValue((int)OrderitemFieldIndex.TaxTotal, true); }
			set	{ SetValue((int)OrderitemFieldIndex.TaxTotal, value, true); }
		}

		/// <summary> The TaxName property of the Entity Orderitem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Orderitem"."TaxName"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 200<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String TaxName
		{
			get { return (System.String)GetValue((int)OrderitemFieldIndex.TaxName, true); }
			set	{ SetValue((int)OrderitemFieldIndex.TaxName, value, true); }
		}

		/// <summary> The ExternalIdentifier property of the Entity Orderitem<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Orderitem"."ExternalIdentifier"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 255<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ExternalIdentifier
		{
			get { return (System.String)GetValue((int)OrderitemFieldIndex.ExternalIdentifier, true); }
			set	{ SetValue((int)OrderitemFieldIndex.ExternalIdentifier, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'OrderitemAlterationitemEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiOrderitemAlterationitemCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.OrderitemAlterationitemCollection OrderitemAlterationitemCollection
		{
			get	{ return GetMultiOrderitemAlterationitemCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for OrderitemAlterationitemCollection. When set to true, OrderitemAlterationitemCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time OrderitemAlterationitemCollection is accessed. You can always execute/ a forced fetch by calling GetMultiOrderitemAlterationitemCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchOrderitemAlterationitemCollection
		{
			get	{ return _alwaysFetchOrderitemAlterationitemCollection; }
			set	{ _alwaysFetchOrderitemAlterationitemCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property OrderitemAlterationitemCollection already has been fetched. Setting this property to false when OrderitemAlterationitemCollection has been fetched
		/// will clear the OrderitemAlterationitemCollection collection well. Setting this property to true while OrderitemAlterationitemCollection hasn't been fetched disables lazy loading for OrderitemAlterationitemCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedOrderitemAlterationitemCollection
		{
			get { return _alreadyFetchedOrderitemAlterationitemCollection;}
			set 
			{
				if(_alreadyFetchedOrderitemAlterationitemCollection && !value && (_orderitemAlterationitemCollection != null))
				{
					_orderitemAlterationitemCollection.Clear();
				}
				_alreadyFetchedOrderitemAlterationitemCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'OrderitemTagEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiOrderitemTagCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.OrderitemTagCollection OrderitemTagCollection
		{
			get	{ return GetMultiOrderitemTagCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for OrderitemTagCollection. When set to true, OrderitemTagCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time OrderitemTagCollection is accessed. You can always execute/ a forced fetch by calling GetMultiOrderitemTagCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchOrderitemTagCollection
		{
			get	{ return _alwaysFetchOrderitemTagCollection; }
			set	{ _alwaysFetchOrderitemTagCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property OrderitemTagCollection already has been fetched. Setting this property to false when OrderitemTagCollection has been fetched
		/// will clear the OrderitemTagCollection collection well. Setting this property to true while OrderitemTagCollection hasn't been fetched disables lazy loading for OrderitemTagCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedOrderitemTagCollection
		{
			get { return _alreadyFetchedOrderitemTagCollection;}
			set 
			{
				if(_alreadyFetchedOrderitemTagCollection && !value && (_orderitemTagCollection != null))
				{
					_orderitemTagCollection.Clear();
				}
				_alreadyFetchedOrderitemTagCollection = value;
			}
		}

		/// <summary> Gets / sets related entity of type 'CategoryEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleCategoryEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual CategoryEntity CategoryEntity
		{
			get	{ return GetSingleCategoryEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncCategoryEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "OrderitemCollection", "CategoryEntity", _categoryEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for CategoryEntity. When set to true, CategoryEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CategoryEntity is accessed. You can always execute a forced fetch by calling GetSingleCategoryEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCategoryEntity
		{
			get	{ return _alwaysFetchCategoryEntity; }
			set	{ _alwaysFetchCategoryEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CategoryEntity already has been fetched. Setting this property to false when CategoryEntity has been fetched
		/// will set CategoryEntity to null as well. Setting this property to true while CategoryEntity hasn't been fetched disables lazy loading for CategoryEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCategoryEntity
		{
			get { return _alreadyFetchedCategoryEntity;}
			set 
			{
				if(_alreadyFetchedCategoryEntity && !value)
				{
					this.CategoryEntity = null;
				}
				_alreadyFetchedCategoryEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property CategoryEntity is not found
		/// in the database. When set to true, CategoryEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool CategoryEntityReturnsNewIfNotFound
		{
			get	{ return _categoryEntityReturnsNewIfNotFound; }
			set { _categoryEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'OrderEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleOrderEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual OrderEntity OrderEntity
		{
			get	{ return GetSingleOrderEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncOrderEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "OrderitemCollection", "OrderEntity", _orderEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for OrderEntity. When set to true, OrderEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time OrderEntity is accessed. You can always execute a forced fetch by calling GetSingleOrderEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchOrderEntity
		{
			get	{ return _alwaysFetchOrderEntity; }
			set	{ _alwaysFetchOrderEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property OrderEntity already has been fetched. Setting this property to false when OrderEntity has been fetched
		/// will set OrderEntity to null as well. Setting this property to true while OrderEntity hasn't been fetched disables lazy loading for OrderEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedOrderEntity
		{
			get { return _alreadyFetchedOrderEntity;}
			set 
			{
				if(_alreadyFetchedOrderEntity && !value)
				{
					this.OrderEntity = null;
				}
				_alreadyFetchedOrderEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property OrderEntity is not found
		/// in the database. When set to true, OrderEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool OrderEntityReturnsNewIfNotFound
		{
			get	{ return _orderEntityReturnsNewIfNotFound; }
			set { _orderEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'PriceLevelItemEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSinglePriceLevelItemEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual PriceLevelItemEntity PriceLevelItemEntity
		{
			get	{ return GetSinglePriceLevelItemEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncPriceLevelItemEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "OrderitemCollection", "PriceLevelItemEntity", _priceLevelItemEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for PriceLevelItemEntity. When set to true, PriceLevelItemEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time PriceLevelItemEntity is accessed. You can always execute a forced fetch by calling GetSinglePriceLevelItemEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPriceLevelItemEntity
		{
			get	{ return _alwaysFetchPriceLevelItemEntity; }
			set	{ _alwaysFetchPriceLevelItemEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property PriceLevelItemEntity already has been fetched. Setting this property to false when PriceLevelItemEntity has been fetched
		/// will set PriceLevelItemEntity to null as well. Setting this property to true while PriceLevelItemEntity hasn't been fetched disables lazy loading for PriceLevelItemEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPriceLevelItemEntity
		{
			get { return _alreadyFetchedPriceLevelItemEntity;}
			set 
			{
				if(_alreadyFetchedPriceLevelItemEntity && !value)
				{
					this.PriceLevelItemEntity = null;
				}
				_alreadyFetchedPriceLevelItemEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property PriceLevelItemEntity is not found
		/// in the database. When set to true, PriceLevelItemEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool PriceLevelItemEntityReturnsNewIfNotFound
		{
			get	{ return _priceLevelItemEntityReturnsNewIfNotFound; }
			set { _priceLevelItemEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'ProductEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleProductEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual ProductEntity ProductEntity
		{
			get	{ return GetSingleProductEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncProductEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "OrderitemCollection", "ProductEntity", _productEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for ProductEntity. When set to true, ProductEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ProductEntity is accessed. You can always execute a forced fetch by calling GetSingleProductEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchProductEntity
		{
			get	{ return _alwaysFetchProductEntity; }
			set	{ _alwaysFetchProductEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ProductEntity already has been fetched. Setting this property to false when ProductEntity has been fetched
		/// will set ProductEntity to null as well. Setting this property to true while ProductEntity hasn't been fetched disables lazy loading for ProductEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedProductEntity
		{
			get { return _alreadyFetchedProductEntity;}
			set 
			{
				if(_alreadyFetchedProductEntity && !value)
				{
					this.ProductEntity = null;
				}
				_alreadyFetchedProductEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property ProductEntity is not found
		/// in the database. When set to true, ProductEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool ProductEntityReturnsNewIfNotFound
		{
			get	{ return _productEntityReturnsNewIfNotFound; }
			set { _productEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'TaxTariffEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleTaxTariffEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual TaxTariffEntity TaxTariffEntity
		{
			get	{ return GetSingleTaxTariffEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncTaxTariffEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "OrderitemCollection", "TaxTariffEntity", _taxTariffEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for TaxTariffEntity. When set to true, TaxTariffEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TaxTariffEntity is accessed. You can always execute a forced fetch by calling GetSingleTaxTariffEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTaxTariffEntity
		{
			get	{ return _alwaysFetchTaxTariffEntity; }
			set	{ _alwaysFetchTaxTariffEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property TaxTariffEntity already has been fetched. Setting this property to false when TaxTariffEntity has been fetched
		/// will set TaxTariffEntity to null as well. Setting this property to true while TaxTariffEntity hasn't been fetched disables lazy loading for TaxTariffEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTaxTariffEntity
		{
			get { return _alreadyFetchedTaxTariffEntity;}
			set 
			{
				if(_alreadyFetchedTaxTariffEntity && !value)
				{
					this.TaxTariffEntity = null;
				}
				_alreadyFetchedTaxTariffEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property TaxTariffEntity is not found
		/// in the database. When set to true, TaxTariffEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool TaxTariffEntityReturnsNewIfNotFound
		{
			get	{ return _taxTariffEntityReturnsNewIfNotFound; }
			set { _taxTariffEntityReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the Obymobi.Data.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)Obymobi.Data.EntityType.OrderitemEntity; }
		}

		#endregion

		
		#region Custom Entity code
		
        // __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
        // __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
