﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
using System.Runtime.Serialization;
using System.Data;
using System.Xml.Serialization;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity base class which represents the base class for the entity 'Device'.<br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public abstract partial class DeviceEntityBase : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		public new string LLBLGenProEntityName {
			get { return "DeviceEntity"; }
		}
	
		#region Class Member Declarations
		private Obymobi.Data.CollectionClasses.ClientCollection	_clientCollection;
		private bool	_alwaysFetchClientCollection, _alreadyFetchedClientCollection;
		private Obymobi.Data.CollectionClasses.DeliverypointCollection	_deliverypointCollection;
		private bool	_alwaysFetchDeliverypointCollection, _alreadyFetchedDeliverypointCollection;
		private Obymobi.Data.CollectionClasses.DevicemediaCollection	_devicemediumCollection;
		private bool	_alwaysFetchDevicemediumCollection, _alreadyFetchedDevicemediumCollection;
		private Obymobi.Data.CollectionClasses.DeviceTokenHistoryCollection	_deviceTokenHistoryCollection;
		private bool	_alwaysFetchDeviceTokenHistoryCollection, _alreadyFetchedDeviceTokenHistoryCollection;
		private Obymobi.Data.CollectionClasses.DeviceTokenTaskCollection	_deviceTokenTaskCollection;
		private bool	_alwaysFetchDeviceTokenTaskCollection, _alreadyFetchedDeviceTokenTaskCollection;
		private Obymobi.Data.CollectionClasses.TerminalCollection	_terminalCollection;
		private bool	_alwaysFetchTerminalCollection, _alreadyFetchedTerminalCollection;
		private Obymobi.Data.CollectionClasses.CompanyCollection _companyCollectionViaClient;
		private bool	_alwaysFetchCompanyCollectionViaClient, _alreadyFetchedCompanyCollectionViaClient;
		private Obymobi.Data.CollectionClasses.CompanyCollection _companyCollectionViaTerminal;
		private bool	_alwaysFetchCompanyCollectionViaTerminal, _alreadyFetchedCompanyCollectionViaTerminal;
		private Obymobi.Data.CollectionClasses.DeliverypointCollection _deliverypointCollectionViaClient;
		private bool	_alwaysFetchDeliverypointCollectionViaClient, _alreadyFetchedDeliverypointCollectionViaClient;
		private Obymobi.Data.CollectionClasses.DeliverypointCollection _deliverypointCollectionViaClient_;
		private bool	_alwaysFetchDeliverypointCollectionViaClient_, _alreadyFetchedDeliverypointCollectionViaClient_;
		private Obymobi.Data.CollectionClasses.DeliverypointCollection _deliverypointCollectionViaTerminal;
		private bool	_alwaysFetchDeliverypointCollectionViaTerminal, _alreadyFetchedDeliverypointCollectionViaTerminal;
		private Obymobi.Data.CollectionClasses.DeliverypointCollection _deliverypointCollectionViaTerminal_;
		private bool	_alwaysFetchDeliverypointCollectionViaTerminal_, _alreadyFetchedDeliverypointCollectionViaTerminal_;
		private Obymobi.Data.CollectionClasses.DeliverypointgroupCollection _deliverypointgroupCollectionViaClient;
		private bool	_alwaysFetchDeliverypointgroupCollectionViaClient, _alreadyFetchedDeliverypointgroupCollectionViaClient;
		private Obymobi.Data.CollectionClasses.DeliverypointgroupCollection _deliverypointgroupCollectionViaTerminal;
		private bool	_alwaysFetchDeliverypointgroupCollectionViaTerminal, _alreadyFetchedDeliverypointgroupCollectionViaTerminal;
		private Obymobi.Data.CollectionClasses.EntertainmentCollection _entertainmentCollectionViaTerminal;
		private bool	_alwaysFetchEntertainmentCollectionViaTerminal, _alreadyFetchedEntertainmentCollectionViaTerminal;
		private Obymobi.Data.CollectionClasses.EntertainmentCollection _entertainmentCollectionViaTerminal_;
		private bool	_alwaysFetchEntertainmentCollectionViaTerminal_, _alreadyFetchedEntertainmentCollectionViaTerminal_;
		private Obymobi.Data.CollectionClasses.EntertainmentCollection _entertainmentCollectionViaTerminal__;
		private bool	_alwaysFetchEntertainmentCollectionViaTerminal__, _alreadyFetchedEntertainmentCollectionViaTerminal__;
		private Obymobi.Data.CollectionClasses.IcrtouchprintermappingCollection _icrtouchprintermappingCollectionViaTerminal;
		private bool	_alwaysFetchIcrtouchprintermappingCollectionViaTerminal, _alreadyFetchedIcrtouchprintermappingCollectionViaTerminal;
		private Obymobi.Data.CollectionClasses.ProductCollection _productCollectionViaTerminal;
		private bool	_alwaysFetchProductCollectionViaTerminal, _alreadyFetchedProductCollectionViaTerminal;
		private Obymobi.Data.CollectionClasses.ProductCollection _productCollectionViaTerminal_;
		private bool	_alwaysFetchProductCollectionViaTerminal_, _alreadyFetchedProductCollectionViaTerminal_;
		private Obymobi.Data.CollectionClasses.ProductCollection _productCollectionViaTerminal__;
		private bool	_alwaysFetchProductCollectionViaTerminal__, _alreadyFetchedProductCollectionViaTerminal__;
		private Obymobi.Data.CollectionClasses.ProductCollection _productCollectionViaTerminal___;
		private bool	_alwaysFetchProductCollectionViaTerminal___, _alreadyFetchedProductCollectionViaTerminal___;
		private Obymobi.Data.CollectionClasses.TerminalCollection _terminalCollectionViaTerminal;
		private bool	_alwaysFetchTerminalCollectionViaTerminal, _alreadyFetchedTerminalCollectionViaTerminal;
		private Obymobi.Data.CollectionClasses.UIModeCollection _uIModeCollectionViaTerminal;
		private bool	_alwaysFetchUIModeCollectionViaTerminal, _alreadyFetchedUIModeCollectionViaTerminal;
		private Obymobi.Data.CollectionClasses.UserCollection _userCollectionViaTerminal;
		private bool	_alwaysFetchUserCollectionViaTerminal, _alreadyFetchedUserCollectionViaTerminal;
		private CustomerEntity _customerEntity;
		private bool	_alwaysFetchCustomerEntity, _alreadyFetchedCustomerEntity, _customerEntityReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name CustomerEntity</summary>
			public static readonly string CustomerEntity = "CustomerEntity";
			/// <summary>Member name ClientCollection</summary>
			public static readonly string ClientCollection = "ClientCollection";
			/// <summary>Member name DeliverypointCollection</summary>
			public static readonly string DeliverypointCollection = "DeliverypointCollection";
			/// <summary>Member name DevicemediumCollection</summary>
			public static readonly string DevicemediumCollection = "DevicemediumCollection";
			/// <summary>Member name DeviceTokenHistoryCollection</summary>
			public static readonly string DeviceTokenHistoryCollection = "DeviceTokenHistoryCollection";
			/// <summary>Member name DeviceTokenTaskCollection</summary>
			public static readonly string DeviceTokenTaskCollection = "DeviceTokenTaskCollection";
			/// <summary>Member name TerminalCollection</summary>
			public static readonly string TerminalCollection = "TerminalCollection";
			/// <summary>Member name CompanyCollectionViaClient</summary>
			public static readonly string CompanyCollectionViaClient = "CompanyCollectionViaClient";
			/// <summary>Member name CompanyCollectionViaTerminal</summary>
			public static readonly string CompanyCollectionViaTerminal = "CompanyCollectionViaTerminal";
			/// <summary>Member name DeliverypointCollectionViaClient</summary>
			public static readonly string DeliverypointCollectionViaClient = "DeliverypointCollectionViaClient";
			/// <summary>Member name DeliverypointCollectionViaClient_</summary>
			public static readonly string DeliverypointCollectionViaClient_ = "DeliverypointCollectionViaClient_";
			/// <summary>Member name DeliverypointCollectionViaTerminal</summary>
			public static readonly string DeliverypointCollectionViaTerminal = "DeliverypointCollectionViaTerminal";
			/// <summary>Member name DeliverypointCollectionViaTerminal_</summary>
			public static readonly string DeliverypointCollectionViaTerminal_ = "DeliverypointCollectionViaTerminal_";
			/// <summary>Member name DeliverypointgroupCollectionViaClient</summary>
			public static readonly string DeliverypointgroupCollectionViaClient = "DeliverypointgroupCollectionViaClient";
			/// <summary>Member name DeliverypointgroupCollectionViaTerminal</summary>
			public static readonly string DeliverypointgroupCollectionViaTerminal = "DeliverypointgroupCollectionViaTerminal";
			/// <summary>Member name EntertainmentCollectionViaTerminal</summary>
			public static readonly string EntertainmentCollectionViaTerminal = "EntertainmentCollectionViaTerminal";
			/// <summary>Member name EntertainmentCollectionViaTerminal_</summary>
			public static readonly string EntertainmentCollectionViaTerminal_ = "EntertainmentCollectionViaTerminal_";
			/// <summary>Member name EntertainmentCollectionViaTerminal__</summary>
			public static readonly string EntertainmentCollectionViaTerminal__ = "EntertainmentCollectionViaTerminal__";
			/// <summary>Member name IcrtouchprintermappingCollectionViaTerminal</summary>
			public static readonly string IcrtouchprintermappingCollectionViaTerminal = "IcrtouchprintermappingCollectionViaTerminal";
			/// <summary>Member name ProductCollectionViaTerminal</summary>
			public static readonly string ProductCollectionViaTerminal = "ProductCollectionViaTerminal";
			/// <summary>Member name ProductCollectionViaTerminal_</summary>
			public static readonly string ProductCollectionViaTerminal_ = "ProductCollectionViaTerminal_";
			/// <summary>Member name ProductCollectionViaTerminal__</summary>
			public static readonly string ProductCollectionViaTerminal__ = "ProductCollectionViaTerminal__";
			/// <summary>Member name ProductCollectionViaTerminal___</summary>
			public static readonly string ProductCollectionViaTerminal___ = "ProductCollectionViaTerminal___";
			/// <summary>Member name TerminalCollectionViaTerminal</summary>
			public static readonly string TerminalCollectionViaTerminal = "TerminalCollectionViaTerminal";
			/// <summary>Member name UIModeCollectionViaTerminal</summary>
			public static readonly string UIModeCollectionViaTerminal = "UIModeCollectionViaTerminal";
			/// <summary>Member name UserCollectionViaTerminal</summary>
			public static readonly string UserCollectionViaTerminal = "UserCollectionViaTerminal";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static DeviceEntityBase()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		protected DeviceEntityBase() :base("DeviceEntity")
		{
			InitClassEmpty(null);
		}

		/// <summary>CTor</summary>
		/// <param name="deviceId">PK value for Device which data should be fetched into this Device object</param>
		protected DeviceEntityBase(System.Int32 deviceId):base("DeviceEntity")
		{
			InitClassFetch(deviceId, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="deviceId">PK value for Device which data should be fetched into this Device object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		protected DeviceEntityBase(System.Int32 deviceId, IPrefetchPath prefetchPathToUse): base("DeviceEntity")
		{
			InitClassFetch(deviceId, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="deviceId">PK value for Device which data should be fetched into this Device object</param>
		/// <param name="validator">The custom validator object for this DeviceEntity</param>
		protected DeviceEntityBase(System.Int32 deviceId, IValidator validator):base("DeviceEntity")
		{
			InitClassFetch(deviceId, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected DeviceEntityBase(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_clientCollection = (Obymobi.Data.CollectionClasses.ClientCollection)info.GetValue("_clientCollection", typeof(Obymobi.Data.CollectionClasses.ClientCollection));
			_alwaysFetchClientCollection = info.GetBoolean("_alwaysFetchClientCollection");
			_alreadyFetchedClientCollection = info.GetBoolean("_alreadyFetchedClientCollection");

			_deliverypointCollection = (Obymobi.Data.CollectionClasses.DeliverypointCollection)info.GetValue("_deliverypointCollection", typeof(Obymobi.Data.CollectionClasses.DeliverypointCollection));
			_alwaysFetchDeliverypointCollection = info.GetBoolean("_alwaysFetchDeliverypointCollection");
			_alreadyFetchedDeliverypointCollection = info.GetBoolean("_alreadyFetchedDeliverypointCollection");

			_devicemediumCollection = (Obymobi.Data.CollectionClasses.DevicemediaCollection)info.GetValue("_devicemediumCollection", typeof(Obymobi.Data.CollectionClasses.DevicemediaCollection));
			_alwaysFetchDevicemediumCollection = info.GetBoolean("_alwaysFetchDevicemediumCollection");
			_alreadyFetchedDevicemediumCollection = info.GetBoolean("_alreadyFetchedDevicemediumCollection");

			_deviceTokenHistoryCollection = (Obymobi.Data.CollectionClasses.DeviceTokenHistoryCollection)info.GetValue("_deviceTokenHistoryCollection", typeof(Obymobi.Data.CollectionClasses.DeviceTokenHistoryCollection));
			_alwaysFetchDeviceTokenHistoryCollection = info.GetBoolean("_alwaysFetchDeviceTokenHistoryCollection");
			_alreadyFetchedDeviceTokenHistoryCollection = info.GetBoolean("_alreadyFetchedDeviceTokenHistoryCollection");

			_deviceTokenTaskCollection = (Obymobi.Data.CollectionClasses.DeviceTokenTaskCollection)info.GetValue("_deviceTokenTaskCollection", typeof(Obymobi.Data.CollectionClasses.DeviceTokenTaskCollection));
			_alwaysFetchDeviceTokenTaskCollection = info.GetBoolean("_alwaysFetchDeviceTokenTaskCollection");
			_alreadyFetchedDeviceTokenTaskCollection = info.GetBoolean("_alreadyFetchedDeviceTokenTaskCollection");

			_terminalCollection = (Obymobi.Data.CollectionClasses.TerminalCollection)info.GetValue("_terminalCollection", typeof(Obymobi.Data.CollectionClasses.TerminalCollection));
			_alwaysFetchTerminalCollection = info.GetBoolean("_alwaysFetchTerminalCollection");
			_alreadyFetchedTerminalCollection = info.GetBoolean("_alreadyFetchedTerminalCollection");
			_companyCollectionViaClient = (Obymobi.Data.CollectionClasses.CompanyCollection)info.GetValue("_companyCollectionViaClient", typeof(Obymobi.Data.CollectionClasses.CompanyCollection));
			_alwaysFetchCompanyCollectionViaClient = info.GetBoolean("_alwaysFetchCompanyCollectionViaClient");
			_alreadyFetchedCompanyCollectionViaClient = info.GetBoolean("_alreadyFetchedCompanyCollectionViaClient");

			_companyCollectionViaTerminal = (Obymobi.Data.CollectionClasses.CompanyCollection)info.GetValue("_companyCollectionViaTerminal", typeof(Obymobi.Data.CollectionClasses.CompanyCollection));
			_alwaysFetchCompanyCollectionViaTerminal = info.GetBoolean("_alwaysFetchCompanyCollectionViaTerminal");
			_alreadyFetchedCompanyCollectionViaTerminal = info.GetBoolean("_alreadyFetchedCompanyCollectionViaTerminal");

			_deliverypointCollectionViaClient = (Obymobi.Data.CollectionClasses.DeliverypointCollection)info.GetValue("_deliverypointCollectionViaClient", typeof(Obymobi.Data.CollectionClasses.DeliverypointCollection));
			_alwaysFetchDeliverypointCollectionViaClient = info.GetBoolean("_alwaysFetchDeliverypointCollectionViaClient");
			_alreadyFetchedDeliverypointCollectionViaClient = info.GetBoolean("_alreadyFetchedDeliverypointCollectionViaClient");

			_deliverypointCollectionViaClient_ = (Obymobi.Data.CollectionClasses.DeliverypointCollection)info.GetValue("_deliverypointCollectionViaClient_", typeof(Obymobi.Data.CollectionClasses.DeliverypointCollection));
			_alwaysFetchDeliverypointCollectionViaClient_ = info.GetBoolean("_alwaysFetchDeliverypointCollectionViaClient_");
			_alreadyFetchedDeliverypointCollectionViaClient_ = info.GetBoolean("_alreadyFetchedDeliverypointCollectionViaClient_");

			_deliverypointCollectionViaTerminal = (Obymobi.Data.CollectionClasses.DeliverypointCollection)info.GetValue("_deliverypointCollectionViaTerminal", typeof(Obymobi.Data.CollectionClasses.DeliverypointCollection));
			_alwaysFetchDeliverypointCollectionViaTerminal = info.GetBoolean("_alwaysFetchDeliverypointCollectionViaTerminal");
			_alreadyFetchedDeliverypointCollectionViaTerminal = info.GetBoolean("_alreadyFetchedDeliverypointCollectionViaTerminal");

			_deliverypointCollectionViaTerminal_ = (Obymobi.Data.CollectionClasses.DeliverypointCollection)info.GetValue("_deliverypointCollectionViaTerminal_", typeof(Obymobi.Data.CollectionClasses.DeliverypointCollection));
			_alwaysFetchDeliverypointCollectionViaTerminal_ = info.GetBoolean("_alwaysFetchDeliverypointCollectionViaTerminal_");
			_alreadyFetchedDeliverypointCollectionViaTerminal_ = info.GetBoolean("_alreadyFetchedDeliverypointCollectionViaTerminal_");

			_deliverypointgroupCollectionViaClient = (Obymobi.Data.CollectionClasses.DeliverypointgroupCollection)info.GetValue("_deliverypointgroupCollectionViaClient", typeof(Obymobi.Data.CollectionClasses.DeliverypointgroupCollection));
			_alwaysFetchDeliverypointgroupCollectionViaClient = info.GetBoolean("_alwaysFetchDeliverypointgroupCollectionViaClient");
			_alreadyFetchedDeliverypointgroupCollectionViaClient = info.GetBoolean("_alreadyFetchedDeliverypointgroupCollectionViaClient");

			_deliverypointgroupCollectionViaTerminal = (Obymobi.Data.CollectionClasses.DeliverypointgroupCollection)info.GetValue("_deliverypointgroupCollectionViaTerminal", typeof(Obymobi.Data.CollectionClasses.DeliverypointgroupCollection));
			_alwaysFetchDeliverypointgroupCollectionViaTerminal = info.GetBoolean("_alwaysFetchDeliverypointgroupCollectionViaTerminal");
			_alreadyFetchedDeliverypointgroupCollectionViaTerminal = info.GetBoolean("_alreadyFetchedDeliverypointgroupCollectionViaTerminal");

			_entertainmentCollectionViaTerminal = (Obymobi.Data.CollectionClasses.EntertainmentCollection)info.GetValue("_entertainmentCollectionViaTerminal", typeof(Obymobi.Data.CollectionClasses.EntertainmentCollection));
			_alwaysFetchEntertainmentCollectionViaTerminal = info.GetBoolean("_alwaysFetchEntertainmentCollectionViaTerminal");
			_alreadyFetchedEntertainmentCollectionViaTerminal = info.GetBoolean("_alreadyFetchedEntertainmentCollectionViaTerminal");

			_entertainmentCollectionViaTerminal_ = (Obymobi.Data.CollectionClasses.EntertainmentCollection)info.GetValue("_entertainmentCollectionViaTerminal_", typeof(Obymobi.Data.CollectionClasses.EntertainmentCollection));
			_alwaysFetchEntertainmentCollectionViaTerminal_ = info.GetBoolean("_alwaysFetchEntertainmentCollectionViaTerminal_");
			_alreadyFetchedEntertainmentCollectionViaTerminal_ = info.GetBoolean("_alreadyFetchedEntertainmentCollectionViaTerminal_");

			_entertainmentCollectionViaTerminal__ = (Obymobi.Data.CollectionClasses.EntertainmentCollection)info.GetValue("_entertainmentCollectionViaTerminal__", typeof(Obymobi.Data.CollectionClasses.EntertainmentCollection));
			_alwaysFetchEntertainmentCollectionViaTerminal__ = info.GetBoolean("_alwaysFetchEntertainmentCollectionViaTerminal__");
			_alreadyFetchedEntertainmentCollectionViaTerminal__ = info.GetBoolean("_alreadyFetchedEntertainmentCollectionViaTerminal__");

			_icrtouchprintermappingCollectionViaTerminal = (Obymobi.Data.CollectionClasses.IcrtouchprintermappingCollection)info.GetValue("_icrtouchprintermappingCollectionViaTerminal", typeof(Obymobi.Data.CollectionClasses.IcrtouchprintermappingCollection));
			_alwaysFetchIcrtouchprintermappingCollectionViaTerminal = info.GetBoolean("_alwaysFetchIcrtouchprintermappingCollectionViaTerminal");
			_alreadyFetchedIcrtouchprintermappingCollectionViaTerminal = info.GetBoolean("_alreadyFetchedIcrtouchprintermappingCollectionViaTerminal");

			_productCollectionViaTerminal = (Obymobi.Data.CollectionClasses.ProductCollection)info.GetValue("_productCollectionViaTerminal", typeof(Obymobi.Data.CollectionClasses.ProductCollection));
			_alwaysFetchProductCollectionViaTerminal = info.GetBoolean("_alwaysFetchProductCollectionViaTerminal");
			_alreadyFetchedProductCollectionViaTerminal = info.GetBoolean("_alreadyFetchedProductCollectionViaTerminal");

			_productCollectionViaTerminal_ = (Obymobi.Data.CollectionClasses.ProductCollection)info.GetValue("_productCollectionViaTerminal_", typeof(Obymobi.Data.CollectionClasses.ProductCollection));
			_alwaysFetchProductCollectionViaTerminal_ = info.GetBoolean("_alwaysFetchProductCollectionViaTerminal_");
			_alreadyFetchedProductCollectionViaTerminal_ = info.GetBoolean("_alreadyFetchedProductCollectionViaTerminal_");

			_productCollectionViaTerminal__ = (Obymobi.Data.CollectionClasses.ProductCollection)info.GetValue("_productCollectionViaTerminal__", typeof(Obymobi.Data.CollectionClasses.ProductCollection));
			_alwaysFetchProductCollectionViaTerminal__ = info.GetBoolean("_alwaysFetchProductCollectionViaTerminal__");
			_alreadyFetchedProductCollectionViaTerminal__ = info.GetBoolean("_alreadyFetchedProductCollectionViaTerminal__");

			_productCollectionViaTerminal___ = (Obymobi.Data.CollectionClasses.ProductCollection)info.GetValue("_productCollectionViaTerminal___", typeof(Obymobi.Data.CollectionClasses.ProductCollection));
			_alwaysFetchProductCollectionViaTerminal___ = info.GetBoolean("_alwaysFetchProductCollectionViaTerminal___");
			_alreadyFetchedProductCollectionViaTerminal___ = info.GetBoolean("_alreadyFetchedProductCollectionViaTerminal___");

			_terminalCollectionViaTerminal = (Obymobi.Data.CollectionClasses.TerminalCollection)info.GetValue("_terminalCollectionViaTerminal", typeof(Obymobi.Data.CollectionClasses.TerminalCollection));
			_alwaysFetchTerminalCollectionViaTerminal = info.GetBoolean("_alwaysFetchTerminalCollectionViaTerminal");
			_alreadyFetchedTerminalCollectionViaTerminal = info.GetBoolean("_alreadyFetchedTerminalCollectionViaTerminal");

			_uIModeCollectionViaTerminal = (Obymobi.Data.CollectionClasses.UIModeCollection)info.GetValue("_uIModeCollectionViaTerminal", typeof(Obymobi.Data.CollectionClasses.UIModeCollection));
			_alwaysFetchUIModeCollectionViaTerminal = info.GetBoolean("_alwaysFetchUIModeCollectionViaTerminal");
			_alreadyFetchedUIModeCollectionViaTerminal = info.GetBoolean("_alreadyFetchedUIModeCollectionViaTerminal");

			_userCollectionViaTerminal = (Obymobi.Data.CollectionClasses.UserCollection)info.GetValue("_userCollectionViaTerminal", typeof(Obymobi.Data.CollectionClasses.UserCollection));
			_alwaysFetchUserCollectionViaTerminal = info.GetBoolean("_alwaysFetchUserCollectionViaTerminal");
			_alreadyFetchedUserCollectionViaTerminal = info.GetBoolean("_alreadyFetchedUserCollectionViaTerminal");
			_customerEntity = (CustomerEntity)info.GetValue("_customerEntity", typeof(CustomerEntity));
			if(_customerEntity!=null)
			{
				_customerEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_customerEntityReturnsNewIfNotFound = info.GetBoolean("_customerEntityReturnsNewIfNotFound");
			_alwaysFetchCustomerEntity = info.GetBoolean("_alwaysFetchCustomerEntity");
			_alreadyFetchedCustomerEntity = info.GetBoolean("_alreadyFetchedCustomerEntity");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}	
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((DeviceFieldIndex)fieldIndex)
			{
				case DeviceFieldIndex.CustomerId:
					DesetupSyncCustomerEntity(true, false);
					_alreadyFetchedCustomerEntity = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedClientCollection = (_clientCollection.Count > 0);
			_alreadyFetchedDeliverypointCollection = (_deliverypointCollection.Count > 0);
			_alreadyFetchedDevicemediumCollection = (_devicemediumCollection.Count > 0);
			_alreadyFetchedDeviceTokenHistoryCollection = (_deviceTokenHistoryCollection.Count > 0);
			_alreadyFetchedDeviceTokenTaskCollection = (_deviceTokenTaskCollection.Count > 0);
			_alreadyFetchedTerminalCollection = (_terminalCollection.Count > 0);
			_alreadyFetchedCompanyCollectionViaClient = (_companyCollectionViaClient.Count > 0);
			_alreadyFetchedCompanyCollectionViaTerminal = (_companyCollectionViaTerminal.Count > 0);
			_alreadyFetchedDeliverypointCollectionViaClient = (_deliverypointCollectionViaClient.Count > 0);
			_alreadyFetchedDeliverypointCollectionViaClient_ = (_deliverypointCollectionViaClient_.Count > 0);
			_alreadyFetchedDeliverypointCollectionViaTerminal = (_deliverypointCollectionViaTerminal.Count > 0);
			_alreadyFetchedDeliverypointCollectionViaTerminal_ = (_deliverypointCollectionViaTerminal_.Count > 0);
			_alreadyFetchedDeliverypointgroupCollectionViaClient = (_deliverypointgroupCollectionViaClient.Count > 0);
			_alreadyFetchedDeliverypointgroupCollectionViaTerminal = (_deliverypointgroupCollectionViaTerminal.Count > 0);
			_alreadyFetchedEntertainmentCollectionViaTerminal = (_entertainmentCollectionViaTerminal.Count > 0);
			_alreadyFetchedEntertainmentCollectionViaTerminal_ = (_entertainmentCollectionViaTerminal_.Count > 0);
			_alreadyFetchedEntertainmentCollectionViaTerminal__ = (_entertainmentCollectionViaTerminal__.Count > 0);
			_alreadyFetchedIcrtouchprintermappingCollectionViaTerminal = (_icrtouchprintermappingCollectionViaTerminal.Count > 0);
			_alreadyFetchedProductCollectionViaTerminal = (_productCollectionViaTerminal.Count > 0);
			_alreadyFetchedProductCollectionViaTerminal_ = (_productCollectionViaTerminal_.Count > 0);
			_alreadyFetchedProductCollectionViaTerminal__ = (_productCollectionViaTerminal__.Count > 0);
			_alreadyFetchedProductCollectionViaTerminal___ = (_productCollectionViaTerminal___.Count > 0);
			_alreadyFetchedTerminalCollectionViaTerminal = (_terminalCollectionViaTerminal.Count > 0);
			_alreadyFetchedUIModeCollectionViaTerminal = (_uIModeCollectionViaTerminal.Count > 0);
			_alreadyFetchedUserCollectionViaTerminal = (_userCollectionViaTerminal.Count > 0);
			_alreadyFetchedCustomerEntity = (_customerEntity != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "CustomerEntity":
					toReturn.Add(Relations.CustomerEntityUsingCustomerId);
					break;
				case "ClientCollection":
					toReturn.Add(Relations.ClientEntityUsingDeviceId);
					break;
				case "DeliverypointCollection":
					toReturn.Add(Relations.DeliverypointEntityUsingDeviceId);
					break;
				case "DevicemediumCollection":
					toReturn.Add(Relations.DevicemediaEntityUsingDeviceId);
					break;
				case "DeviceTokenHistoryCollection":
					toReturn.Add(Relations.DeviceTokenHistoryEntityUsingDeviceId);
					break;
				case "DeviceTokenTaskCollection":
					toReturn.Add(Relations.DeviceTokenTaskEntityUsingDeviceId);
					break;
				case "TerminalCollection":
					toReturn.Add(Relations.TerminalEntityUsingDeviceId);
					break;
				case "CompanyCollectionViaClient":
					toReturn.Add(Relations.ClientEntityUsingDeviceId, "DeviceEntity__", "Client_", JoinHint.None);
					toReturn.Add(ClientEntity.Relations.CompanyEntityUsingCompanyId, "Client_", string.Empty, JoinHint.None);
					break;
				case "CompanyCollectionViaTerminal":
					toReturn.Add(Relations.TerminalEntityUsingDeviceId, "DeviceEntity__", "Terminal_", JoinHint.None);
					toReturn.Add(TerminalEntity.Relations.CompanyEntityUsingCompanyId, "Terminal_", string.Empty, JoinHint.None);
					break;
				case "DeliverypointCollectionViaClient":
					toReturn.Add(Relations.ClientEntityUsingDeviceId, "DeviceEntity__", "Client_", JoinHint.None);
					toReturn.Add(ClientEntity.Relations.DeliverypointEntityUsingDeliverypointId, "Client_", string.Empty, JoinHint.None);
					break;
				case "DeliverypointCollectionViaClient_":
					toReturn.Add(Relations.ClientEntityUsingDeviceId, "DeviceEntity__", "Client_", JoinHint.None);
					toReturn.Add(ClientEntity.Relations.DeliverypointEntityUsingLastDeliverypointId, "Client_", string.Empty, JoinHint.None);
					break;
				case "DeliverypointCollectionViaTerminal":
					toReturn.Add(Relations.TerminalEntityUsingDeviceId, "DeviceEntity__", "Terminal_", JoinHint.None);
					toReturn.Add(TerminalEntity.Relations.DeliverypointEntityUsingAltSystemMessagesDeliverypointId, "Terminal_", string.Empty, JoinHint.None);
					break;
				case "DeliverypointCollectionViaTerminal_":
					toReturn.Add(Relations.TerminalEntityUsingDeviceId, "DeviceEntity__", "Terminal_", JoinHint.None);
					toReturn.Add(TerminalEntity.Relations.DeliverypointEntityUsingSystemMessagesDeliverypointId, "Terminal_", string.Empty, JoinHint.None);
					break;
				case "DeliverypointgroupCollectionViaClient":
					toReturn.Add(Relations.ClientEntityUsingDeviceId, "DeviceEntity__", "Client_", JoinHint.None);
					toReturn.Add(ClientEntity.Relations.DeliverypointgroupEntityUsingDeliverypointGroupId, "Client_", string.Empty, JoinHint.None);
					break;
				case "DeliverypointgroupCollectionViaTerminal":
					toReturn.Add(Relations.TerminalEntityUsingDeviceId, "DeviceEntity__", "Terminal_", JoinHint.None);
					toReturn.Add(TerminalEntity.Relations.DeliverypointgroupEntityUsingDeliverypointgroupId, "Terminal_", string.Empty, JoinHint.None);
					break;
				case "EntertainmentCollectionViaTerminal":
					toReturn.Add(Relations.TerminalEntityUsingDeviceId, "DeviceEntity__", "Terminal_", JoinHint.None);
					toReturn.Add(TerminalEntity.Relations.EntertainmentEntityUsingBrowser1, "Terminal_", string.Empty, JoinHint.None);
					break;
				case "EntertainmentCollectionViaTerminal_":
					toReturn.Add(Relations.TerminalEntityUsingDeviceId, "DeviceEntity__", "Terminal_", JoinHint.None);
					toReturn.Add(TerminalEntity.Relations.EntertainmentEntityUsingBrowser2, "Terminal_", string.Empty, JoinHint.None);
					break;
				case "EntertainmentCollectionViaTerminal__":
					toReturn.Add(Relations.TerminalEntityUsingDeviceId, "DeviceEntity__", "Terminal_", JoinHint.None);
					toReturn.Add(TerminalEntity.Relations.EntertainmentEntityUsingCmsPage, "Terminal_", string.Empty, JoinHint.None);
					break;
				case "IcrtouchprintermappingCollectionViaTerminal":
					toReturn.Add(Relations.TerminalEntityUsingDeviceId, "DeviceEntity__", "Terminal_", JoinHint.None);
					toReturn.Add(TerminalEntity.Relations.IcrtouchprintermappingEntityUsingIcrtouchprintermappingId, "Terminal_", string.Empty, JoinHint.None);
					break;
				case "ProductCollectionViaTerminal":
					toReturn.Add(Relations.TerminalEntityUsingDeviceId, "DeviceEntity__", "Terminal_", JoinHint.None);
					toReturn.Add(TerminalEntity.Relations.ProductEntityUsingBatteryLowProductId, "Terminal_", string.Empty, JoinHint.None);
					break;
				case "ProductCollectionViaTerminal_":
					toReturn.Add(Relations.TerminalEntityUsingDeviceId, "DeviceEntity__", "Terminal_", JoinHint.None);
					toReturn.Add(TerminalEntity.Relations.ProductEntityUsingClientDisconnectedProductId, "Terminal_", string.Empty, JoinHint.None);
					break;
				case "ProductCollectionViaTerminal__":
					toReturn.Add(Relations.TerminalEntityUsingDeviceId, "DeviceEntity__", "Terminal_", JoinHint.None);
					toReturn.Add(TerminalEntity.Relations.ProductEntityUsingOrderFailedProductId, "Terminal_", string.Empty, JoinHint.None);
					break;
				case "ProductCollectionViaTerminal___":
					toReturn.Add(Relations.TerminalEntityUsingDeviceId, "DeviceEntity__", "Terminal_", JoinHint.None);
					toReturn.Add(TerminalEntity.Relations.ProductEntityUsingUnlockDeliverypointProductId, "Terminal_", string.Empty, JoinHint.None);
					break;
				case "TerminalCollectionViaTerminal":
					toReturn.Add(Relations.TerminalEntityUsingDeviceId, "DeviceEntity__", "Terminal_", JoinHint.None);
					toReturn.Add(TerminalEntity.Relations.TerminalEntityUsingForwardToTerminalId, "Terminal_", string.Empty, JoinHint.None);
					break;
				case "UIModeCollectionViaTerminal":
					toReturn.Add(Relations.TerminalEntityUsingDeviceId, "DeviceEntity__", "Terminal_", JoinHint.None);
					toReturn.Add(TerminalEntity.Relations.UIModeEntityUsingUIModeId, "Terminal_", string.Empty, JoinHint.None);
					break;
				case "UserCollectionViaTerminal":
					toReturn.Add(Relations.TerminalEntityUsingDeviceId, "DeviceEntity__", "Terminal_", JoinHint.None);
					toReturn.Add(TerminalEntity.Relations.UserEntityUsingAutomaticSignOnUserId, "Terminal_", string.Empty, JoinHint.None);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_clientCollection", (!this.MarkedForDeletion?_clientCollection:null));
			info.AddValue("_alwaysFetchClientCollection", _alwaysFetchClientCollection);
			info.AddValue("_alreadyFetchedClientCollection", _alreadyFetchedClientCollection);
			info.AddValue("_deliverypointCollection", (!this.MarkedForDeletion?_deliverypointCollection:null));
			info.AddValue("_alwaysFetchDeliverypointCollection", _alwaysFetchDeliverypointCollection);
			info.AddValue("_alreadyFetchedDeliverypointCollection", _alreadyFetchedDeliverypointCollection);
			info.AddValue("_devicemediumCollection", (!this.MarkedForDeletion?_devicemediumCollection:null));
			info.AddValue("_alwaysFetchDevicemediumCollection", _alwaysFetchDevicemediumCollection);
			info.AddValue("_alreadyFetchedDevicemediumCollection", _alreadyFetchedDevicemediumCollection);
			info.AddValue("_deviceTokenHistoryCollection", (!this.MarkedForDeletion?_deviceTokenHistoryCollection:null));
			info.AddValue("_alwaysFetchDeviceTokenHistoryCollection", _alwaysFetchDeviceTokenHistoryCollection);
			info.AddValue("_alreadyFetchedDeviceTokenHistoryCollection", _alreadyFetchedDeviceTokenHistoryCollection);
			info.AddValue("_deviceTokenTaskCollection", (!this.MarkedForDeletion?_deviceTokenTaskCollection:null));
			info.AddValue("_alwaysFetchDeviceTokenTaskCollection", _alwaysFetchDeviceTokenTaskCollection);
			info.AddValue("_alreadyFetchedDeviceTokenTaskCollection", _alreadyFetchedDeviceTokenTaskCollection);
			info.AddValue("_terminalCollection", (!this.MarkedForDeletion?_terminalCollection:null));
			info.AddValue("_alwaysFetchTerminalCollection", _alwaysFetchTerminalCollection);
			info.AddValue("_alreadyFetchedTerminalCollection", _alreadyFetchedTerminalCollection);
			info.AddValue("_companyCollectionViaClient", (!this.MarkedForDeletion?_companyCollectionViaClient:null));
			info.AddValue("_alwaysFetchCompanyCollectionViaClient", _alwaysFetchCompanyCollectionViaClient);
			info.AddValue("_alreadyFetchedCompanyCollectionViaClient", _alreadyFetchedCompanyCollectionViaClient);
			info.AddValue("_companyCollectionViaTerminal", (!this.MarkedForDeletion?_companyCollectionViaTerminal:null));
			info.AddValue("_alwaysFetchCompanyCollectionViaTerminal", _alwaysFetchCompanyCollectionViaTerminal);
			info.AddValue("_alreadyFetchedCompanyCollectionViaTerminal", _alreadyFetchedCompanyCollectionViaTerminal);
			info.AddValue("_deliverypointCollectionViaClient", (!this.MarkedForDeletion?_deliverypointCollectionViaClient:null));
			info.AddValue("_alwaysFetchDeliverypointCollectionViaClient", _alwaysFetchDeliverypointCollectionViaClient);
			info.AddValue("_alreadyFetchedDeliverypointCollectionViaClient", _alreadyFetchedDeliverypointCollectionViaClient);
			info.AddValue("_deliverypointCollectionViaClient_", (!this.MarkedForDeletion?_deliverypointCollectionViaClient_:null));
			info.AddValue("_alwaysFetchDeliverypointCollectionViaClient_", _alwaysFetchDeliverypointCollectionViaClient_);
			info.AddValue("_alreadyFetchedDeliverypointCollectionViaClient_", _alreadyFetchedDeliverypointCollectionViaClient_);
			info.AddValue("_deliverypointCollectionViaTerminal", (!this.MarkedForDeletion?_deliverypointCollectionViaTerminal:null));
			info.AddValue("_alwaysFetchDeliverypointCollectionViaTerminal", _alwaysFetchDeliverypointCollectionViaTerminal);
			info.AddValue("_alreadyFetchedDeliverypointCollectionViaTerminal", _alreadyFetchedDeliverypointCollectionViaTerminal);
			info.AddValue("_deliverypointCollectionViaTerminal_", (!this.MarkedForDeletion?_deliverypointCollectionViaTerminal_:null));
			info.AddValue("_alwaysFetchDeliverypointCollectionViaTerminal_", _alwaysFetchDeliverypointCollectionViaTerminal_);
			info.AddValue("_alreadyFetchedDeliverypointCollectionViaTerminal_", _alreadyFetchedDeliverypointCollectionViaTerminal_);
			info.AddValue("_deliverypointgroupCollectionViaClient", (!this.MarkedForDeletion?_deliverypointgroupCollectionViaClient:null));
			info.AddValue("_alwaysFetchDeliverypointgroupCollectionViaClient", _alwaysFetchDeliverypointgroupCollectionViaClient);
			info.AddValue("_alreadyFetchedDeliverypointgroupCollectionViaClient", _alreadyFetchedDeliverypointgroupCollectionViaClient);
			info.AddValue("_deliverypointgroupCollectionViaTerminal", (!this.MarkedForDeletion?_deliverypointgroupCollectionViaTerminal:null));
			info.AddValue("_alwaysFetchDeliverypointgroupCollectionViaTerminal", _alwaysFetchDeliverypointgroupCollectionViaTerminal);
			info.AddValue("_alreadyFetchedDeliverypointgroupCollectionViaTerminal", _alreadyFetchedDeliverypointgroupCollectionViaTerminal);
			info.AddValue("_entertainmentCollectionViaTerminal", (!this.MarkedForDeletion?_entertainmentCollectionViaTerminal:null));
			info.AddValue("_alwaysFetchEntertainmentCollectionViaTerminal", _alwaysFetchEntertainmentCollectionViaTerminal);
			info.AddValue("_alreadyFetchedEntertainmentCollectionViaTerminal", _alreadyFetchedEntertainmentCollectionViaTerminal);
			info.AddValue("_entertainmentCollectionViaTerminal_", (!this.MarkedForDeletion?_entertainmentCollectionViaTerminal_:null));
			info.AddValue("_alwaysFetchEntertainmentCollectionViaTerminal_", _alwaysFetchEntertainmentCollectionViaTerminal_);
			info.AddValue("_alreadyFetchedEntertainmentCollectionViaTerminal_", _alreadyFetchedEntertainmentCollectionViaTerminal_);
			info.AddValue("_entertainmentCollectionViaTerminal__", (!this.MarkedForDeletion?_entertainmentCollectionViaTerminal__:null));
			info.AddValue("_alwaysFetchEntertainmentCollectionViaTerminal__", _alwaysFetchEntertainmentCollectionViaTerminal__);
			info.AddValue("_alreadyFetchedEntertainmentCollectionViaTerminal__", _alreadyFetchedEntertainmentCollectionViaTerminal__);
			info.AddValue("_icrtouchprintermappingCollectionViaTerminal", (!this.MarkedForDeletion?_icrtouchprintermappingCollectionViaTerminal:null));
			info.AddValue("_alwaysFetchIcrtouchprintermappingCollectionViaTerminal", _alwaysFetchIcrtouchprintermappingCollectionViaTerminal);
			info.AddValue("_alreadyFetchedIcrtouchprintermappingCollectionViaTerminal", _alreadyFetchedIcrtouchprintermappingCollectionViaTerminal);
			info.AddValue("_productCollectionViaTerminal", (!this.MarkedForDeletion?_productCollectionViaTerminal:null));
			info.AddValue("_alwaysFetchProductCollectionViaTerminal", _alwaysFetchProductCollectionViaTerminal);
			info.AddValue("_alreadyFetchedProductCollectionViaTerminal", _alreadyFetchedProductCollectionViaTerminal);
			info.AddValue("_productCollectionViaTerminal_", (!this.MarkedForDeletion?_productCollectionViaTerminal_:null));
			info.AddValue("_alwaysFetchProductCollectionViaTerminal_", _alwaysFetchProductCollectionViaTerminal_);
			info.AddValue("_alreadyFetchedProductCollectionViaTerminal_", _alreadyFetchedProductCollectionViaTerminal_);
			info.AddValue("_productCollectionViaTerminal__", (!this.MarkedForDeletion?_productCollectionViaTerminal__:null));
			info.AddValue("_alwaysFetchProductCollectionViaTerminal__", _alwaysFetchProductCollectionViaTerminal__);
			info.AddValue("_alreadyFetchedProductCollectionViaTerminal__", _alreadyFetchedProductCollectionViaTerminal__);
			info.AddValue("_productCollectionViaTerminal___", (!this.MarkedForDeletion?_productCollectionViaTerminal___:null));
			info.AddValue("_alwaysFetchProductCollectionViaTerminal___", _alwaysFetchProductCollectionViaTerminal___);
			info.AddValue("_alreadyFetchedProductCollectionViaTerminal___", _alreadyFetchedProductCollectionViaTerminal___);
			info.AddValue("_terminalCollectionViaTerminal", (!this.MarkedForDeletion?_terminalCollectionViaTerminal:null));
			info.AddValue("_alwaysFetchTerminalCollectionViaTerminal", _alwaysFetchTerminalCollectionViaTerminal);
			info.AddValue("_alreadyFetchedTerminalCollectionViaTerminal", _alreadyFetchedTerminalCollectionViaTerminal);
			info.AddValue("_uIModeCollectionViaTerminal", (!this.MarkedForDeletion?_uIModeCollectionViaTerminal:null));
			info.AddValue("_alwaysFetchUIModeCollectionViaTerminal", _alwaysFetchUIModeCollectionViaTerminal);
			info.AddValue("_alreadyFetchedUIModeCollectionViaTerminal", _alreadyFetchedUIModeCollectionViaTerminal);
			info.AddValue("_userCollectionViaTerminal", (!this.MarkedForDeletion?_userCollectionViaTerminal:null));
			info.AddValue("_alwaysFetchUserCollectionViaTerminal", _alwaysFetchUserCollectionViaTerminal);
			info.AddValue("_alreadyFetchedUserCollectionViaTerminal", _alreadyFetchedUserCollectionViaTerminal);
			info.AddValue("_customerEntity", (!this.MarkedForDeletion?_customerEntity:null));
			info.AddValue("_customerEntityReturnsNewIfNotFound", _customerEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchCustomerEntity", _alwaysFetchCustomerEntity);
			info.AddValue("_alreadyFetchedCustomerEntity", _alreadyFetchedCustomerEntity);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "CustomerEntity":
					_alreadyFetchedCustomerEntity = true;
					this.CustomerEntity = (CustomerEntity)entity;
					break;
				case "ClientCollection":
					_alreadyFetchedClientCollection = true;
					if(entity!=null)
					{
						this.ClientCollection.Add((ClientEntity)entity);
					}
					break;
				case "DeliverypointCollection":
					_alreadyFetchedDeliverypointCollection = true;
					if(entity!=null)
					{
						this.DeliverypointCollection.Add((DeliverypointEntity)entity);
					}
					break;
				case "DevicemediumCollection":
					_alreadyFetchedDevicemediumCollection = true;
					if(entity!=null)
					{
						this.DevicemediumCollection.Add((DevicemediaEntity)entity);
					}
					break;
				case "DeviceTokenHistoryCollection":
					_alreadyFetchedDeviceTokenHistoryCollection = true;
					if(entity!=null)
					{
						this.DeviceTokenHistoryCollection.Add((DeviceTokenHistoryEntity)entity);
					}
					break;
				case "DeviceTokenTaskCollection":
					_alreadyFetchedDeviceTokenTaskCollection = true;
					if(entity!=null)
					{
						this.DeviceTokenTaskCollection.Add((DeviceTokenTaskEntity)entity);
					}
					break;
				case "TerminalCollection":
					_alreadyFetchedTerminalCollection = true;
					if(entity!=null)
					{
						this.TerminalCollection.Add((TerminalEntity)entity);
					}
					break;
				case "CompanyCollectionViaClient":
					_alreadyFetchedCompanyCollectionViaClient = true;
					if(entity!=null)
					{
						this.CompanyCollectionViaClient.Add((CompanyEntity)entity);
					}
					break;
				case "CompanyCollectionViaTerminal":
					_alreadyFetchedCompanyCollectionViaTerminal = true;
					if(entity!=null)
					{
						this.CompanyCollectionViaTerminal.Add((CompanyEntity)entity);
					}
					break;
				case "DeliverypointCollectionViaClient":
					_alreadyFetchedDeliverypointCollectionViaClient = true;
					if(entity!=null)
					{
						this.DeliverypointCollectionViaClient.Add((DeliverypointEntity)entity);
					}
					break;
				case "DeliverypointCollectionViaClient_":
					_alreadyFetchedDeliverypointCollectionViaClient_ = true;
					if(entity!=null)
					{
						this.DeliverypointCollectionViaClient_.Add((DeliverypointEntity)entity);
					}
					break;
				case "DeliverypointCollectionViaTerminal":
					_alreadyFetchedDeliverypointCollectionViaTerminal = true;
					if(entity!=null)
					{
						this.DeliverypointCollectionViaTerminal.Add((DeliverypointEntity)entity);
					}
					break;
				case "DeliverypointCollectionViaTerminal_":
					_alreadyFetchedDeliverypointCollectionViaTerminal_ = true;
					if(entity!=null)
					{
						this.DeliverypointCollectionViaTerminal_.Add((DeliverypointEntity)entity);
					}
					break;
				case "DeliverypointgroupCollectionViaClient":
					_alreadyFetchedDeliverypointgroupCollectionViaClient = true;
					if(entity!=null)
					{
						this.DeliverypointgroupCollectionViaClient.Add((DeliverypointgroupEntity)entity);
					}
					break;
				case "DeliverypointgroupCollectionViaTerminal":
					_alreadyFetchedDeliverypointgroupCollectionViaTerminal = true;
					if(entity!=null)
					{
						this.DeliverypointgroupCollectionViaTerminal.Add((DeliverypointgroupEntity)entity);
					}
					break;
				case "EntertainmentCollectionViaTerminal":
					_alreadyFetchedEntertainmentCollectionViaTerminal = true;
					if(entity!=null)
					{
						this.EntertainmentCollectionViaTerminal.Add((EntertainmentEntity)entity);
					}
					break;
				case "EntertainmentCollectionViaTerminal_":
					_alreadyFetchedEntertainmentCollectionViaTerminal_ = true;
					if(entity!=null)
					{
						this.EntertainmentCollectionViaTerminal_.Add((EntertainmentEntity)entity);
					}
					break;
				case "EntertainmentCollectionViaTerminal__":
					_alreadyFetchedEntertainmentCollectionViaTerminal__ = true;
					if(entity!=null)
					{
						this.EntertainmentCollectionViaTerminal__.Add((EntertainmentEntity)entity);
					}
					break;
				case "IcrtouchprintermappingCollectionViaTerminal":
					_alreadyFetchedIcrtouchprintermappingCollectionViaTerminal = true;
					if(entity!=null)
					{
						this.IcrtouchprintermappingCollectionViaTerminal.Add((IcrtouchprintermappingEntity)entity);
					}
					break;
				case "ProductCollectionViaTerminal":
					_alreadyFetchedProductCollectionViaTerminal = true;
					if(entity!=null)
					{
						this.ProductCollectionViaTerminal.Add((ProductEntity)entity);
					}
					break;
				case "ProductCollectionViaTerminal_":
					_alreadyFetchedProductCollectionViaTerminal_ = true;
					if(entity!=null)
					{
						this.ProductCollectionViaTerminal_.Add((ProductEntity)entity);
					}
					break;
				case "ProductCollectionViaTerminal__":
					_alreadyFetchedProductCollectionViaTerminal__ = true;
					if(entity!=null)
					{
						this.ProductCollectionViaTerminal__.Add((ProductEntity)entity);
					}
					break;
				case "ProductCollectionViaTerminal___":
					_alreadyFetchedProductCollectionViaTerminal___ = true;
					if(entity!=null)
					{
						this.ProductCollectionViaTerminal___.Add((ProductEntity)entity);
					}
					break;
				case "TerminalCollectionViaTerminal":
					_alreadyFetchedTerminalCollectionViaTerminal = true;
					if(entity!=null)
					{
						this.TerminalCollectionViaTerminal.Add((TerminalEntity)entity);
					}
					break;
				case "UIModeCollectionViaTerminal":
					_alreadyFetchedUIModeCollectionViaTerminal = true;
					if(entity!=null)
					{
						this.UIModeCollectionViaTerminal.Add((UIModeEntity)entity);
					}
					break;
				case "UserCollectionViaTerminal":
					_alreadyFetchedUserCollectionViaTerminal = true;
					if(entity!=null)
					{
						this.UserCollectionViaTerminal.Add((UserEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "CustomerEntity":
					SetupSyncCustomerEntity(relatedEntity);
					break;
				case "ClientCollection":
					_clientCollection.Add((ClientEntity)relatedEntity);
					break;
				case "DeliverypointCollection":
					_deliverypointCollection.Add((DeliverypointEntity)relatedEntity);
					break;
				case "DevicemediumCollection":
					_devicemediumCollection.Add((DevicemediaEntity)relatedEntity);
					break;
				case "DeviceTokenHistoryCollection":
					_deviceTokenHistoryCollection.Add((DeviceTokenHistoryEntity)relatedEntity);
					break;
				case "DeviceTokenTaskCollection":
					_deviceTokenTaskCollection.Add((DeviceTokenTaskEntity)relatedEntity);
					break;
				case "TerminalCollection":
					_terminalCollection.Add((TerminalEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "CustomerEntity":
					DesetupSyncCustomerEntity(false, true);
					break;
				case "ClientCollection":
					this.PerformRelatedEntityRemoval(_clientCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "DeliverypointCollection":
					this.PerformRelatedEntityRemoval(_deliverypointCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "DevicemediumCollection":
					this.PerformRelatedEntityRemoval(_devicemediumCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "DeviceTokenHistoryCollection":
					this.PerformRelatedEntityRemoval(_deviceTokenHistoryCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "DeviceTokenTaskCollection":
					this.PerformRelatedEntityRemoval(_deviceTokenTaskCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "TerminalCollection":
					this.PerformRelatedEntityRemoval(_terminalCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_customerEntity!=null)
			{
				toReturn.Add(_customerEntity);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_clientCollection);
			toReturn.Add(_deliverypointCollection);
			toReturn.Add(_devicemediumCollection);
			toReturn.Add(_deviceTokenHistoryCollection);
			toReturn.Add(_deviceTokenTaskCollection);
			toReturn.Add(_terminalCollection);

			return toReturn;
		}

		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="identifier">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCIdentifier(System.String identifier)
		{
			return FetchUsingUCIdentifier( identifier, null, null, null);
		}

		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="identifier">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCIdentifier(System.String identifier, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingUCIdentifier( identifier, prefetchPathToUse, null, null);
		}
	
		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="identifier">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCIdentifier(System.String identifier, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingUCIdentifier( identifier, prefetchPathToUse, contextToUse, null);
		}
	
		/// <summary> Method which will try to fetch the contents for this entity using a unique constraint. </summary>
		/// <remarks>All contents of the entity is lost.</remarks>
		/// <param name="identifier">Value for a field in the UniqueConstraint, which is used to retrieve the contents.</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public bool FetchUsingUCIdentifier(System.String identifier, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				((DeviceDAO)CreateDAOInstance()).FetchDeviceUsingUCIdentifier(this, this.Transaction, identifier, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="deviceId">PK value for Device which data should be fetched into this Device object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 deviceId)
		{
			return FetchUsingPK(deviceId, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="deviceId">PK value for Device which data should be fetched into this Device object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 deviceId, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(deviceId, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="deviceId">PK value for Device which data should be fetched into this Device object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 deviceId, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(deviceId, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="deviceId">PK value for Device which data should be fetched into this Device object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 deviceId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(deviceId, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.DeviceId, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new DeviceRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'ClientEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ClientEntity'</returns>
		public Obymobi.Data.CollectionClasses.ClientCollection GetMultiClientCollection(bool forceFetch)
		{
			return GetMultiClientCollection(forceFetch, _clientCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ClientEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ClientEntity'</returns>
		public Obymobi.Data.CollectionClasses.ClientCollection GetMultiClientCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiClientCollection(forceFetch, _clientCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ClientEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.ClientCollection GetMultiClientCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiClientCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ClientEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.ClientCollection GetMultiClientCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedClientCollection || forceFetch || _alwaysFetchClientCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_clientCollection);
				_clientCollection.SuppressClearInGetMulti=!forceFetch;
				_clientCollection.EntityFactoryToUse = entityFactoryToUse;
				_clientCollection.GetMultiManyToOne(null, null, null, null, this, null, filter);
				_clientCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedClientCollection = true;
			}
			return _clientCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'ClientCollection'. These settings will be taken into account
		/// when the property ClientCollection is requested or GetMultiClientCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersClientCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_clientCollection.SortClauses=sortClauses;
			_clientCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'DeliverypointEntity'</returns>
		public Obymobi.Data.CollectionClasses.DeliverypointCollection GetMultiDeliverypointCollection(bool forceFetch)
		{
			return GetMultiDeliverypointCollection(forceFetch, _deliverypointCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'DeliverypointEntity'</returns>
		public Obymobi.Data.CollectionClasses.DeliverypointCollection GetMultiDeliverypointCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiDeliverypointCollection(forceFetch, _deliverypointCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.DeliverypointCollection GetMultiDeliverypointCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiDeliverypointCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.DeliverypointCollection GetMultiDeliverypointCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedDeliverypointCollection || forceFetch || _alwaysFetchDeliverypointCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_deliverypointCollection);
				_deliverypointCollection.SuppressClearInGetMulti=!forceFetch;
				_deliverypointCollection.EntityFactoryToUse = entityFactoryToUse;
				_deliverypointCollection.GetMultiManyToOne(null, null, null, this, null, null, null, filter);
				_deliverypointCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedDeliverypointCollection = true;
			}
			return _deliverypointCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'DeliverypointCollection'. These settings will be taken into account
		/// when the property DeliverypointCollection is requested or GetMultiDeliverypointCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersDeliverypointCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_deliverypointCollection.SortClauses=sortClauses;
			_deliverypointCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'DevicemediaEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'DevicemediaEntity'</returns>
		public Obymobi.Data.CollectionClasses.DevicemediaCollection GetMultiDevicemediumCollection(bool forceFetch)
		{
			return GetMultiDevicemediumCollection(forceFetch, _devicemediumCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'DevicemediaEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'DevicemediaEntity'</returns>
		public Obymobi.Data.CollectionClasses.DevicemediaCollection GetMultiDevicemediumCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiDevicemediumCollection(forceFetch, _devicemediumCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'DevicemediaEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.DevicemediaCollection GetMultiDevicemediumCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiDevicemediumCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'DevicemediaEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.DevicemediaCollection GetMultiDevicemediumCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedDevicemediumCollection || forceFetch || _alwaysFetchDevicemediumCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_devicemediumCollection);
				_devicemediumCollection.SuppressClearInGetMulti=!forceFetch;
				_devicemediumCollection.EntityFactoryToUse = entityFactoryToUse;
				_devicemediumCollection.GetMultiManyToOne(this, filter);
				_devicemediumCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedDevicemediumCollection = true;
			}
			return _devicemediumCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'DevicemediumCollection'. These settings will be taken into account
		/// when the property DevicemediumCollection is requested or GetMultiDevicemediumCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersDevicemediumCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_devicemediumCollection.SortClauses=sortClauses;
			_devicemediumCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'DeviceTokenHistoryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'DeviceTokenHistoryEntity'</returns>
		public Obymobi.Data.CollectionClasses.DeviceTokenHistoryCollection GetMultiDeviceTokenHistoryCollection(bool forceFetch)
		{
			return GetMultiDeviceTokenHistoryCollection(forceFetch, _deviceTokenHistoryCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'DeviceTokenHistoryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'DeviceTokenHistoryEntity'</returns>
		public Obymobi.Data.CollectionClasses.DeviceTokenHistoryCollection GetMultiDeviceTokenHistoryCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiDeviceTokenHistoryCollection(forceFetch, _deviceTokenHistoryCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'DeviceTokenHistoryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.DeviceTokenHistoryCollection GetMultiDeviceTokenHistoryCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiDeviceTokenHistoryCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'DeviceTokenHistoryEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.DeviceTokenHistoryCollection GetMultiDeviceTokenHistoryCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedDeviceTokenHistoryCollection || forceFetch || _alwaysFetchDeviceTokenHistoryCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_deviceTokenHistoryCollection);
				_deviceTokenHistoryCollection.SuppressClearInGetMulti=!forceFetch;
				_deviceTokenHistoryCollection.EntityFactoryToUse = entityFactoryToUse;
				_deviceTokenHistoryCollection.GetMultiManyToOne(this, null, filter);
				_deviceTokenHistoryCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedDeviceTokenHistoryCollection = true;
			}
			return _deviceTokenHistoryCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'DeviceTokenHistoryCollection'. These settings will be taken into account
		/// when the property DeviceTokenHistoryCollection is requested or GetMultiDeviceTokenHistoryCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersDeviceTokenHistoryCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_deviceTokenHistoryCollection.SortClauses=sortClauses;
			_deviceTokenHistoryCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'DeviceTokenTaskEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'DeviceTokenTaskEntity'</returns>
		public Obymobi.Data.CollectionClasses.DeviceTokenTaskCollection GetMultiDeviceTokenTaskCollection(bool forceFetch)
		{
			return GetMultiDeviceTokenTaskCollection(forceFetch, _deviceTokenTaskCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'DeviceTokenTaskEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'DeviceTokenTaskEntity'</returns>
		public Obymobi.Data.CollectionClasses.DeviceTokenTaskCollection GetMultiDeviceTokenTaskCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiDeviceTokenTaskCollection(forceFetch, _deviceTokenTaskCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'DeviceTokenTaskEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.DeviceTokenTaskCollection GetMultiDeviceTokenTaskCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiDeviceTokenTaskCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'DeviceTokenTaskEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.DeviceTokenTaskCollection GetMultiDeviceTokenTaskCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedDeviceTokenTaskCollection || forceFetch || _alwaysFetchDeviceTokenTaskCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_deviceTokenTaskCollection);
				_deviceTokenTaskCollection.SuppressClearInGetMulti=!forceFetch;
				_deviceTokenTaskCollection.EntityFactoryToUse = entityFactoryToUse;
				_deviceTokenTaskCollection.GetMultiManyToOne(this, null, filter);
				_deviceTokenTaskCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedDeviceTokenTaskCollection = true;
			}
			return _deviceTokenTaskCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'DeviceTokenTaskCollection'. These settings will be taken into account
		/// when the property DeviceTokenTaskCollection is requested or GetMultiDeviceTokenTaskCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersDeviceTokenTaskCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_deviceTokenTaskCollection.SortClauses=sortClauses;
			_deviceTokenTaskCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'TerminalEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'TerminalEntity'</returns>
		public Obymobi.Data.CollectionClasses.TerminalCollection GetMultiTerminalCollection(bool forceFetch)
		{
			return GetMultiTerminalCollection(forceFetch, _terminalCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TerminalEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'TerminalEntity'</returns>
		public Obymobi.Data.CollectionClasses.TerminalCollection GetMultiTerminalCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiTerminalCollection(forceFetch, _terminalCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'TerminalEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.TerminalCollection GetMultiTerminalCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiTerminalCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'TerminalEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.TerminalCollection GetMultiTerminalCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedTerminalCollection || forceFetch || _alwaysFetchTerminalCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_terminalCollection);
				_terminalCollection.SuppressClearInGetMulti=!forceFetch;
				_terminalCollection.EntityFactoryToUse = entityFactoryToUse;
				_terminalCollection.GetMultiManyToOne(null, null, null, null, this, null, null, null, null, null, null, null, null, null, null, null, null, filter);
				_terminalCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedTerminalCollection = true;
			}
			return _terminalCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'TerminalCollection'. These settings will be taken into account
		/// when the property TerminalCollection is requested or GetMultiTerminalCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersTerminalCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_terminalCollection.SortClauses=sortClauses;
			_terminalCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'CompanyEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CompanyEntity'</returns>
		public Obymobi.Data.CollectionClasses.CompanyCollection GetMultiCompanyCollectionViaClient(bool forceFetch)
		{
			return GetMultiCompanyCollectionViaClient(forceFetch, _companyCollectionViaClient.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'CompanyEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.CompanyCollection GetMultiCompanyCollectionViaClient(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedCompanyCollectionViaClient || forceFetch || _alwaysFetchCompanyCollectionViaClient) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_companyCollectionViaClient);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(DeviceFields.DeviceId, ComparisonOperator.Equal, this.DeviceId, "DeviceEntity__"));
				_companyCollectionViaClient.SuppressClearInGetMulti=!forceFetch;
				_companyCollectionViaClient.EntityFactoryToUse = entityFactoryToUse;
				_companyCollectionViaClient.GetMulti(filter, GetRelationsForField("CompanyCollectionViaClient"));
				_companyCollectionViaClient.SuppressClearInGetMulti=false;
				_alreadyFetchedCompanyCollectionViaClient = true;
			}
			return _companyCollectionViaClient;
		}

		/// <summary> Sets the collection parameters for the collection for 'CompanyCollectionViaClient'. These settings will be taken into account
		/// when the property CompanyCollectionViaClient is requested or GetMultiCompanyCollectionViaClient is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCompanyCollectionViaClient(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_companyCollectionViaClient.SortClauses=sortClauses;
			_companyCollectionViaClient.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'CompanyEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'CompanyEntity'</returns>
		public Obymobi.Data.CollectionClasses.CompanyCollection GetMultiCompanyCollectionViaTerminal(bool forceFetch)
		{
			return GetMultiCompanyCollectionViaTerminal(forceFetch, _companyCollectionViaTerminal.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'CompanyEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.CompanyCollection GetMultiCompanyCollectionViaTerminal(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedCompanyCollectionViaTerminal || forceFetch || _alwaysFetchCompanyCollectionViaTerminal) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_companyCollectionViaTerminal);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(DeviceFields.DeviceId, ComparisonOperator.Equal, this.DeviceId, "DeviceEntity__"));
				_companyCollectionViaTerminal.SuppressClearInGetMulti=!forceFetch;
				_companyCollectionViaTerminal.EntityFactoryToUse = entityFactoryToUse;
				_companyCollectionViaTerminal.GetMulti(filter, GetRelationsForField("CompanyCollectionViaTerminal"));
				_companyCollectionViaTerminal.SuppressClearInGetMulti=false;
				_alreadyFetchedCompanyCollectionViaTerminal = true;
			}
			return _companyCollectionViaTerminal;
		}

		/// <summary> Sets the collection parameters for the collection for 'CompanyCollectionViaTerminal'. These settings will be taken into account
		/// when the property CompanyCollectionViaTerminal is requested or GetMultiCompanyCollectionViaTerminal is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersCompanyCollectionViaTerminal(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_companyCollectionViaTerminal.SortClauses=sortClauses;
			_companyCollectionViaTerminal.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'DeliverypointEntity'</returns>
		public Obymobi.Data.CollectionClasses.DeliverypointCollection GetMultiDeliverypointCollectionViaClient(bool forceFetch)
		{
			return GetMultiDeliverypointCollectionViaClient(forceFetch, _deliverypointCollectionViaClient.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.DeliverypointCollection GetMultiDeliverypointCollectionViaClient(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedDeliverypointCollectionViaClient || forceFetch || _alwaysFetchDeliverypointCollectionViaClient) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_deliverypointCollectionViaClient);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(DeviceFields.DeviceId, ComparisonOperator.Equal, this.DeviceId, "DeviceEntity__"));
				_deliverypointCollectionViaClient.SuppressClearInGetMulti=!forceFetch;
				_deliverypointCollectionViaClient.EntityFactoryToUse = entityFactoryToUse;
				_deliverypointCollectionViaClient.GetMulti(filter, GetRelationsForField("DeliverypointCollectionViaClient"));
				_deliverypointCollectionViaClient.SuppressClearInGetMulti=false;
				_alreadyFetchedDeliverypointCollectionViaClient = true;
			}
			return _deliverypointCollectionViaClient;
		}

		/// <summary> Sets the collection parameters for the collection for 'DeliverypointCollectionViaClient'. These settings will be taken into account
		/// when the property DeliverypointCollectionViaClient is requested or GetMultiDeliverypointCollectionViaClient is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersDeliverypointCollectionViaClient(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_deliverypointCollectionViaClient.SortClauses=sortClauses;
			_deliverypointCollectionViaClient.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'DeliverypointEntity'</returns>
		public Obymobi.Data.CollectionClasses.DeliverypointCollection GetMultiDeliverypointCollectionViaClient_(bool forceFetch)
		{
			return GetMultiDeliverypointCollectionViaClient_(forceFetch, _deliverypointCollectionViaClient_.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.DeliverypointCollection GetMultiDeliverypointCollectionViaClient_(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedDeliverypointCollectionViaClient_ || forceFetch || _alwaysFetchDeliverypointCollectionViaClient_) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_deliverypointCollectionViaClient_);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(DeviceFields.DeviceId, ComparisonOperator.Equal, this.DeviceId, "DeviceEntity__"));
				_deliverypointCollectionViaClient_.SuppressClearInGetMulti=!forceFetch;
				_deliverypointCollectionViaClient_.EntityFactoryToUse = entityFactoryToUse;
				_deliverypointCollectionViaClient_.GetMulti(filter, GetRelationsForField("DeliverypointCollectionViaClient_"));
				_deliverypointCollectionViaClient_.SuppressClearInGetMulti=false;
				_alreadyFetchedDeliverypointCollectionViaClient_ = true;
			}
			return _deliverypointCollectionViaClient_;
		}

		/// <summary> Sets the collection parameters for the collection for 'DeliverypointCollectionViaClient_'. These settings will be taken into account
		/// when the property DeliverypointCollectionViaClient_ is requested or GetMultiDeliverypointCollectionViaClient_ is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersDeliverypointCollectionViaClient_(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_deliverypointCollectionViaClient_.SortClauses=sortClauses;
			_deliverypointCollectionViaClient_.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'DeliverypointEntity'</returns>
		public Obymobi.Data.CollectionClasses.DeliverypointCollection GetMultiDeliverypointCollectionViaTerminal(bool forceFetch)
		{
			return GetMultiDeliverypointCollectionViaTerminal(forceFetch, _deliverypointCollectionViaTerminal.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.DeliverypointCollection GetMultiDeliverypointCollectionViaTerminal(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedDeliverypointCollectionViaTerminal || forceFetch || _alwaysFetchDeliverypointCollectionViaTerminal) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_deliverypointCollectionViaTerminal);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(DeviceFields.DeviceId, ComparisonOperator.Equal, this.DeviceId, "DeviceEntity__"));
				_deliverypointCollectionViaTerminal.SuppressClearInGetMulti=!forceFetch;
				_deliverypointCollectionViaTerminal.EntityFactoryToUse = entityFactoryToUse;
				_deliverypointCollectionViaTerminal.GetMulti(filter, GetRelationsForField("DeliverypointCollectionViaTerminal"));
				_deliverypointCollectionViaTerminal.SuppressClearInGetMulti=false;
				_alreadyFetchedDeliverypointCollectionViaTerminal = true;
			}
			return _deliverypointCollectionViaTerminal;
		}

		/// <summary> Sets the collection parameters for the collection for 'DeliverypointCollectionViaTerminal'. These settings will be taken into account
		/// when the property DeliverypointCollectionViaTerminal is requested or GetMultiDeliverypointCollectionViaTerminal is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersDeliverypointCollectionViaTerminal(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_deliverypointCollectionViaTerminal.SortClauses=sortClauses;
			_deliverypointCollectionViaTerminal.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'DeliverypointEntity'</returns>
		public Obymobi.Data.CollectionClasses.DeliverypointCollection GetMultiDeliverypointCollectionViaTerminal_(bool forceFetch)
		{
			return GetMultiDeliverypointCollectionViaTerminal_(forceFetch, _deliverypointCollectionViaTerminal_.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.DeliverypointCollection GetMultiDeliverypointCollectionViaTerminal_(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedDeliverypointCollectionViaTerminal_ || forceFetch || _alwaysFetchDeliverypointCollectionViaTerminal_) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_deliverypointCollectionViaTerminal_);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(DeviceFields.DeviceId, ComparisonOperator.Equal, this.DeviceId, "DeviceEntity__"));
				_deliverypointCollectionViaTerminal_.SuppressClearInGetMulti=!forceFetch;
				_deliverypointCollectionViaTerminal_.EntityFactoryToUse = entityFactoryToUse;
				_deliverypointCollectionViaTerminal_.GetMulti(filter, GetRelationsForField("DeliverypointCollectionViaTerminal_"));
				_deliverypointCollectionViaTerminal_.SuppressClearInGetMulti=false;
				_alreadyFetchedDeliverypointCollectionViaTerminal_ = true;
			}
			return _deliverypointCollectionViaTerminal_;
		}

		/// <summary> Sets the collection parameters for the collection for 'DeliverypointCollectionViaTerminal_'. These settings will be taken into account
		/// when the property DeliverypointCollectionViaTerminal_ is requested or GetMultiDeliverypointCollectionViaTerminal_ is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersDeliverypointCollectionViaTerminal_(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_deliverypointCollectionViaTerminal_.SortClauses=sortClauses;
			_deliverypointCollectionViaTerminal_.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointgroupEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'DeliverypointgroupEntity'</returns>
		public Obymobi.Data.CollectionClasses.DeliverypointgroupCollection GetMultiDeliverypointgroupCollectionViaClient(bool forceFetch)
		{
			return GetMultiDeliverypointgroupCollectionViaClient(forceFetch, _deliverypointgroupCollectionViaClient.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointgroupEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.DeliverypointgroupCollection GetMultiDeliverypointgroupCollectionViaClient(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedDeliverypointgroupCollectionViaClient || forceFetch || _alwaysFetchDeliverypointgroupCollectionViaClient) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_deliverypointgroupCollectionViaClient);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(DeviceFields.DeviceId, ComparisonOperator.Equal, this.DeviceId, "DeviceEntity__"));
				_deliverypointgroupCollectionViaClient.SuppressClearInGetMulti=!forceFetch;
				_deliverypointgroupCollectionViaClient.EntityFactoryToUse = entityFactoryToUse;
				_deliverypointgroupCollectionViaClient.GetMulti(filter, GetRelationsForField("DeliverypointgroupCollectionViaClient"));
				_deliverypointgroupCollectionViaClient.SuppressClearInGetMulti=false;
				_alreadyFetchedDeliverypointgroupCollectionViaClient = true;
			}
			return _deliverypointgroupCollectionViaClient;
		}

		/// <summary> Sets the collection parameters for the collection for 'DeliverypointgroupCollectionViaClient'. These settings will be taken into account
		/// when the property DeliverypointgroupCollectionViaClient is requested or GetMultiDeliverypointgroupCollectionViaClient is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersDeliverypointgroupCollectionViaClient(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_deliverypointgroupCollectionViaClient.SortClauses=sortClauses;
			_deliverypointgroupCollectionViaClient.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointgroupEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'DeliverypointgroupEntity'</returns>
		public Obymobi.Data.CollectionClasses.DeliverypointgroupCollection GetMultiDeliverypointgroupCollectionViaTerminal(bool forceFetch)
		{
			return GetMultiDeliverypointgroupCollectionViaTerminal(forceFetch, _deliverypointgroupCollectionViaTerminal.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointgroupEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.DeliverypointgroupCollection GetMultiDeliverypointgroupCollectionViaTerminal(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedDeliverypointgroupCollectionViaTerminal || forceFetch || _alwaysFetchDeliverypointgroupCollectionViaTerminal) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_deliverypointgroupCollectionViaTerminal);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(DeviceFields.DeviceId, ComparisonOperator.Equal, this.DeviceId, "DeviceEntity__"));
				_deliverypointgroupCollectionViaTerminal.SuppressClearInGetMulti=!forceFetch;
				_deliverypointgroupCollectionViaTerminal.EntityFactoryToUse = entityFactoryToUse;
				_deliverypointgroupCollectionViaTerminal.GetMulti(filter, GetRelationsForField("DeliverypointgroupCollectionViaTerminal"));
				_deliverypointgroupCollectionViaTerminal.SuppressClearInGetMulti=false;
				_alreadyFetchedDeliverypointgroupCollectionViaTerminal = true;
			}
			return _deliverypointgroupCollectionViaTerminal;
		}

		/// <summary> Sets the collection parameters for the collection for 'DeliverypointgroupCollectionViaTerminal'. These settings will be taken into account
		/// when the property DeliverypointgroupCollectionViaTerminal is requested or GetMultiDeliverypointgroupCollectionViaTerminal is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersDeliverypointgroupCollectionViaTerminal(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_deliverypointgroupCollectionViaTerminal.SortClauses=sortClauses;
			_deliverypointgroupCollectionViaTerminal.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'EntertainmentEntity'</returns>
		public Obymobi.Data.CollectionClasses.EntertainmentCollection GetMultiEntertainmentCollectionViaTerminal(bool forceFetch)
		{
			return GetMultiEntertainmentCollectionViaTerminal(forceFetch, _entertainmentCollectionViaTerminal.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.EntertainmentCollection GetMultiEntertainmentCollectionViaTerminal(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedEntertainmentCollectionViaTerminal || forceFetch || _alwaysFetchEntertainmentCollectionViaTerminal) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_entertainmentCollectionViaTerminal);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(DeviceFields.DeviceId, ComparisonOperator.Equal, this.DeviceId, "DeviceEntity__"));
				_entertainmentCollectionViaTerminal.SuppressClearInGetMulti=!forceFetch;
				_entertainmentCollectionViaTerminal.EntityFactoryToUse = entityFactoryToUse;
				_entertainmentCollectionViaTerminal.GetMulti(filter, GetRelationsForField("EntertainmentCollectionViaTerminal"));
				_entertainmentCollectionViaTerminal.SuppressClearInGetMulti=false;
				_alreadyFetchedEntertainmentCollectionViaTerminal = true;
			}
			return _entertainmentCollectionViaTerminal;
		}

		/// <summary> Sets the collection parameters for the collection for 'EntertainmentCollectionViaTerminal'. These settings will be taken into account
		/// when the property EntertainmentCollectionViaTerminal is requested or GetMultiEntertainmentCollectionViaTerminal is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersEntertainmentCollectionViaTerminal(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_entertainmentCollectionViaTerminal.SortClauses=sortClauses;
			_entertainmentCollectionViaTerminal.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'EntertainmentEntity'</returns>
		public Obymobi.Data.CollectionClasses.EntertainmentCollection GetMultiEntertainmentCollectionViaTerminal_(bool forceFetch)
		{
			return GetMultiEntertainmentCollectionViaTerminal_(forceFetch, _entertainmentCollectionViaTerminal_.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.EntertainmentCollection GetMultiEntertainmentCollectionViaTerminal_(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedEntertainmentCollectionViaTerminal_ || forceFetch || _alwaysFetchEntertainmentCollectionViaTerminal_) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_entertainmentCollectionViaTerminal_);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(DeviceFields.DeviceId, ComparisonOperator.Equal, this.DeviceId, "DeviceEntity__"));
				_entertainmentCollectionViaTerminal_.SuppressClearInGetMulti=!forceFetch;
				_entertainmentCollectionViaTerminal_.EntityFactoryToUse = entityFactoryToUse;
				_entertainmentCollectionViaTerminal_.GetMulti(filter, GetRelationsForField("EntertainmentCollectionViaTerminal_"));
				_entertainmentCollectionViaTerminal_.SuppressClearInGetMulti=false;
				_alreadyFetchedEntertainmentCollectionViaTerminal_ = true;
			}
			return _entertainmentCollectionViaTerminal_;
		}

		/// <summary> Sets the collection parameters for the collection for 'EntertainmentCollectionViaTerminal_'. These settings will be taken into account
		/// when the property EntertainmentCollectionViaTerminal_ is requested or GetMultiEntertainmentCollectionViaTerminal_ is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersEntertainmentCollectionViaTerminal_(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_entertainmentCollectionViaTerminal_.SortClauses=sortClauses;
			_entertainmentCollectionViaTerminal_.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'EntertainmentEntity'</returns>
		public Obymobi.Data.CollectionClasses.EntertainmentCollection GetMultiEntertainmentCollectionViaTerminal__(bool forceFetch)
		{
			return GetMultiEntertainmentCollectionViaTerminal__(forceFetch, _entertainmentCollectionViaTerminal__.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.EntertainmentCollection GetMultiEntertainmentCollectionViaTerminal__(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedEntertainmentCollectionViaTerminal__ || forceFetch || _alwaysFetchEntertainmentCollectionViaTerminal__) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_entertainmentCollectionViaTerminal__);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(DeviceFields.DeviceId, ComparisonOperator.Equal, this.DeviceId, "DeviceEntity__"));
				_entertainmentCollectionViaTerminal__.SuppressClearInGetMulti=!forceFetch;
				_entertainmentCollectionViaTerminal__.EntityFactoryToUse = entityFactoryToUse;
				_entertainmentCollectionViaTerminal__.GetMulti(filter, GetRelationsForField("EntertainmentCollectionViaTerminal__"));
				_entertainmentCollectionViaTerminal__.SuppressClearInGetMulti=false;
				_alreadyFetchedEntertainmentCollectionViaTerminal__ = true;
			}
			return _entertainmentCollectionViaTerminal__;
		}

		/// <summary> Sets the collection parameters for the collection for 'EntertainmentCollectionViaTerminal__'. These settings will be taken into account
		/// when the property EntertainmentCollectionViaTerminal__ is requested or GetMultiEntertainmentCollectionViaTerminal__ is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersEntertainmentCollectionViaTerminal__(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_entertainmentCollectionViaTerminal__.SortClauses=sortClauses;
			_entertainmentCollectionViaTerminal__.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'IcrtouchprintermappingEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'IcrtouchprintermappingEntity'</returns>
		public Obymobi.Data.CollectionClasses.IcrtouchprintermappingCollection GetMultiIcrtouchprintermappingCollectionViaTerminal(bool forceFetch)
		{
			return GetMultiIcrtouchprintermappingCollectionViaTerminal(forceFetch, _icrtouchprintermappingCollectionViaTerminal.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'IcrtouchprintermappingEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.IcrtouchprintermappingCollection GetMultiIcrtouchprintermappingCollectionViaTerminal(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedIcrtouchprintermappingCollectionViaTerminal || forceFetch || _alwaysFetchIcrtouchprintermappingCollectionViaTerminal) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_icrtouchprintermappingCollectionViaTerminal);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(DeviceFields.DeviceId, ComparisonOperator.Equal, this.DeviceId, "DeviceEntity__"));
				_icrtouchprintermappingCollectionViaTerminal.SuppressClearInGetMulti=!forceFetch;
				_icrtouchprintermappingCollectionViaTerminal.EntityFactoryToUse = entityFactoryToUse;
				_icrtouchprintermappingCollectionViaTerminal.GetMulti(filter, GetRelationsForField("IcrtouchprintermappingCollectionViaTerminal"));
				_icrtouchprintermappingCollectionViaTerminal.SuppressClearInGetMulti=false;
				_alreadyFetchedIcrtouchprintermappingCollectionViaTerminal = true;
			}
			return _icrtouchprintermappingCollectionViaTerminal;
		}

		/// <summary> Sets the collection parameters for the collection for 'IcrtouchprintermappingCollectionViaTerminal'. These settings will be taken into account
		/// when the property IcrtouchprintermappingCollectionViaTerminal is requested or GetMultiIcrtouchprintermappingCollectionViaTerminal is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersIcrtouchprintermappingCollectionViaTerminal(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_icrtouchprintermappingCollectionViaTerminal.SortClauses=sortClauses;
			_icrtouchprintermappingCollectionViaTerminal.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ProductEntity'</returns>
		public Obymobi.Data.CollectionClasses.ProductCollection GetMultiProductCollectionViaTerminal(bool forceFetch)
		{
			return GetMultiProductCollectionViaTerminal(forceFetch, _productCollectionViaTerminal.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.ProductCollection GetMultiProductCollectionViaTerminal(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedProductCollectionViaTerminal || forceFetch || _alwaysFetchProductCollectionViaTerminal) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_productCollectionViaTerminal);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(DeviceFields.DeviceId, ComparisonOperator.Equal, this.DeviceId, "DeviceEntity__"));
				_productCollectionViaTerminal.SuppressClearInGetMulti=!forceFetch;
				_productCollectionViaTerminal.EntityFactoryToUse = entityFactoryToUse;
				_productCollectionViaTerminal.GetMulti(filter, GetRelationsForField("ProductCollectionViaTerminal"));
				_productCollectionViaTerminal.SuppressClearInGetMulti=false;
				_alreadyFetchedProductCollectionViaTerminal = true;
			}
			return _productCollectionViaTerminal;
		}

		/// <summary> Sets the collection parameters for the collection for 'ProductCollectionViaTerminal'. These settings will be taken into account
		/// when the property ProductCollectionViaTerminal is requested or GetMultiProductCollectionViaTerminal is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersProductCollectionViaTerminal(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_productCollectionViaTerminal.SortClauses=sortClauses;
			_productCollectionViaTerminal.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ProductEntity'</returns>
		public Obymobi.Data.CollectionClasses.ProductCollection GetMultiProductCollectionViaTerminal_(bool forceFetch)
		{
			return GetMultiProductCollectionViaTerminal_(forceFetch, _productCollectionViaTerminal_.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.ProductCollection GetMultiProductCollectionViaTerminal_(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedProductCollectionViaTerminal_ || forceFetch || _alwaysFetchProductCollectionViaTerminal_) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_productCollectionViaTerminal_);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(DeviceFields.DeviceId, ComparisonOperator.Equal, this.DeviceId, "DeviceEntity__"));
				_productCollectionViaTerminal_.SuppressClearInGetMulti=!forceFetch;
				_productCollectionViaTerminal_.EntityFactoryToUse = entityFactoryToUse;
				_productCollectionViaTerminal_.GetMulti(filter, GetRelationsForField("ProductCollectionViaTerminal_"));
				_productCollectionViaTerminal_.SuppressClearInGetMulti=false;
				_alreadyFetchedProductCollectionViaTerminal_ = true;
			}
			return _productCollectionViaTerminal_;
		}

		/// <summary> Sets the collection parameters for the collection for 'ProductCollectionViaTerminal_'. These settings will be taken into account
		/// when the property ProductCollectionViaTerminal_ is requested or GetMultiProductCollectionViaTerminal_ is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersProductCollectionViaTerminal_(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_productCollectionViaTerminal_.SortClauses=sortClauses;
			_productCollectionViaTerminal_.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ProductEntity'</returns>
		public Obymobi.Data.CollectionClasses.ProductCollection GetMultiProductCollectionViaTerminal__(bool forceFetch)
		{
			return GetMultiProductCollectionViaTerminal__(forceFetch, _productCollectionViaTerminal__.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.ProductCollection GetMultiProductCollectionViaTerminal__(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedProductCollectionViaTerminal__ || forceFetch || _alwaysFetchProductCollectionViaTerminal__) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_productCollectionViaTerminal__);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(DeviceFields.DeviceId, ComparisonOperator.Equal, this.DeviceId, "DeviceEntity__"));
				_productCollectionViaTerminal__.SuppressClearInGetMulti=!forceFetch;
				_productCollectionViaTerminal__.EntityFactoryToUse = entityFactoryToUse;
				_productCollectionViaTerminal__.GetMulti(filter, GetRelationsForField("ProductCollectionViaTerminal__"));
				_productCollectionViaTerminal__.SuppressClearInGetMulti=false;
				_alreadyFetchedProductCollectionViaTerminal__ = true;
			}
			return _productCollectionViaTerminal__;
		}

		/// <summary> Sets the collection parameters for the collection for 'ProductCollectionViaTerminal__'. These settings will be taken into account
		/// when the property ProductCollectionViaTerminal__ is requested or GetMultiProductCollectionViaTerminal__ is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersProductCollectionViaTerminal__(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_productCollectionViaTerminal__.SortClauses=sortClauses;
			_productCollectionViaTerminal__.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ProductEntity'</returns>
		public Obymobi.Data.CollectionClasses.ProductCollection GetMultiProductCollectionViaTerminal___(bool forceFetch)
		{
			return GetMultiProductCollectionViaTerminal___(forceFetch, _productCollectionViaTerminal___.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.ProductCollection GetMultiProductCollectionViaTerminal___(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedProductCollectionViaTerminal___ || forceFetch || _alwaysFetchProductCollectionViaTerminal___) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_productCollectionViaTerminal___);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(DeviceFields.DeviceId, ComparisonOperator.Equal, this.DeviceId, "DeviceEntity__"));
				_productCollectionViaTerminal___.SuppressClearInGetMulti=!forceFetch;
				_productCollectionViaTerminal___.EntityFactoryToUse = entityFactoryToUse;
				_productCollectionViaTerminal___.GetMulti(filter, GetRelationsForField("ProductCollectionViaTerminal___"));
				_productCollectionViaTerminal___.SuppressClearInGetMulti=false;
				_alreadyFetchedProductCollectionViaTerminal___ = true;
			}
			return _productCollectionViaTerminal___;
		}

		/// <summary> Sets the collection parameters for the collection for 'ProductCollectionViaTerminal___'. These settings will be taken into account
		/// when the property ProductCollectionViaTerminal___ is requested or GetMultiProductCollectionViaTerminal___ is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersProductCollectionViaTerminal___(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_productCollectionViaTerminal___.SortClauses=sortClauses;
			_productCollectionViaTerminal___.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'TerminalEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'TerminalEntity'</returns>
		public Obymobi.Data.CollectionClasses.TerminalCollection GetMultiTerminalCollectionViaTerminal(bool forceFetch)
		{
			return GetMultiTerminalCollectionViaTerminal(forceFetch, _terminalCollectionViaTerminal.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'TerminalEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.TerminalCollection GetMultiTerminalCollectionViaTerminal(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedTerminalCollectionViaTerminal || forceFetch || _alwaysFetchTerminalCollectionViaTerminal) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_terminalCollectionViaTerminal);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(DeviceFields.DeviceId, ComparisonOperator.Equal, this.DeviceId, "DeviceEntity__"));
				_terminalCollectionViaTerminal.SuppressClearInGetMulti=!forceFetch;
				_terminalCollectionViaTerminal.EntityFactoryToUse = entityFactoryToUse;
				_terminalCollectionViaTerminal.GetMulti(filter, GetRelationsForField("TerminalCollectionViaTerminal"));
				_terminalCollectionViaTerminal.SuppressClearInGetMulti=false;
				_alreadyFetchedTerminalCollectionViaTerminal = true;
			}
			return _terminalCollectionViaTerminal;
		}

		/// <summary> Sets the collection parameters for the collection for 'TerminalCollectionViaTerminal'. These settings will be taken into account
		/// when the property TerminalCollectionViaTerminal is requested or GetMultiTerminalCollectionViaTerminal is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersTerminalCollectionViaTerminal(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_terminalCollectionViaTerminal.SortClauses=sortClauses;
			_terminalCollectionViaTerminal.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'UIModeEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'UIModeEntity'</returns>
		public Obymobi.Data.CollectionClasses.UIModeCollection GetMultiUIModeCollectionViaTerminal(bool forceFetch)
		{
			return GetMultiUIModeCollectionViaTerminal(forceFetch, _uIModeCollectionViaTerminal.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'UIModeEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.UIModeCollection GetMultiUIModeCollectionViaTerminal(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedUIModeCollectionViaTerminal || forceFetch || _alwaysFetchUIModeCollectionViaTerminal) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_uIModeCollectionViaTerminal);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(DeviceFields.DeviceId, ComparisonOperator.Equal, this.DeviceId, "DeviceEntity__"));
				_uIModeCollectionViaTerminal.SuppressClearInGetMulti=!forceFetch;
				_uIModeCollectionViaTerminal.EntityFactoryToUse = entityFactoryToUse;
				_uIModeCollectionViaTerminal.GetMulti(filter, GetRelationsForField("UIModeCollectionViaTerminal"));
				_uIModeCollectionViaTerminal.SuppressClearInGetMulti=false;
				_alreadyFetchedUIModeCollectionViaTerminal = true;
			}
			return _uIModeCollectionViaTerminal;
		}

		/// <summary> Sets the collection parameters for the collection for 'UIModeCollectionViaTerminal'. These settings will be taken into account
		/// when the property UIModeCollectionViaTerminal is requested or GetMultiUIModeCollectionViaTerminal is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersUIModeCollectionViaTerminal(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_uIModeCollectionViaTerminal.SortClauses=sortClauses;
			_uIModeCollectionViaTerminal.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'UserEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'UserEntity'</returns>
		public Obymobi.Data.CollectionClasses.UserCollection GetMultiUserCollectionViaTerminal(bool forceFetch)
		{
			return GetMultiUserCollectionViaTerminal(forceFetch, _userCollectionViaTerminal.EntityFactoryToUse);
		}

		/// <summary> Retrieves all related entities of type 'UserEntity' using a relation of type 'm:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToMany() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.UserCollection GetMultiUserCollectionViaTerminal(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
 			if( ( !_alreadyFetchedUserCollectionViaTerminal || forceFetch || _alwaysFetchUserCollectionViaTerminal) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_userCollectionViaTerminal);
				IPredicateExpression filter = new PredicateExpression();
				filter.Add(new FieldCompareValuePredicate(DeviceFields.DeviceId, ComparisonOperator.Equal, this.DeviceId, "DeviceEntity__"));
				_userCollectionViaTerminal.SuppressClearInGetMulti=!forceFetch;
				_userCollectionViaTerminal.EntityFactoryToUse = entityFactoryToUse;
				_userCollectionViaTerminal.GetMulti(filter, GetRelationsForField("UserCollectionViaTerminal"));
				_userCollectionViaTerminal.SuppressClearInGetMulti=false;
				_alreadyFetchedUserCollectionViaTerminal = true;
			}
			return _userCollectionViaTerminal;
		}

		/// <summary> Sets the collection parameters for the collection for 'UserCollectionViaTerminal'. These settings will be taken into account
		/// when the property UserCollectionViaTerminal is requested or GetMultiUserCollectionViaTerminal is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersUserCollectionViaTerminal(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_userCollectionViaTerminal.SortClauses=sortClauses;
			_userCollectionViaTerminal.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves the related entity of type 'CustomerEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'CustomerEntity' which is related to this entity.</returns>
		public CustomerEntity GetSingleCustomerEntity()
		{
			return GetSingleCustomerEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'CustomerEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'CustomerEntity' which is related to this entity.</returns>
		public virtual CustomerEntity GetSingleCustomerEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedCustomerEntity || forceFetch || _alwaysFetchCustomerEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.CustomerEntityUsingCustomerId);
				CustomerEntity newEntity = new CustomerEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.CustomerId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (CustomerEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_customerEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.CustomerEntity = newEntity;
				_alreadyFetchedCustomerEntity = fetchResult;
			}
			return _customerEntity;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("CustomerEntity", _customerEntity);
			toReturn.Add("ClientCollection", _clientCollection);
			toReturn.Add("DeliverypointCollection", _deliverypointCollection);
			toReturn.Add("DevicemediumCollection", _devicemediumCollection);
			toReturn.Add("DeviceTokenHistoryCollection", _deviceTokenHistoryCollection);
			toReturn.Add("DeviceTokenTaskCollection", _deviceTokenTaskCollection);
			toReturn.Add("TerminalCollection", _terminalCollection);
			toReturn.Add("CompanyCollectionViaClient", _companyCollectionViaClient);
			toReturn.Add("CompanyCollectionViaTerminal", _companyCollectionViaTerminal);
			toReturn.Add("DeliverypointCollectionViaClient", _deliverypointCollectionViaClient);
			toReturn.Add("DeliverypointCollectionViaClient_", _deliverypointCollectionViaClient_);
			toReturn.Add("DeliverypointCollectionViaTerminal", _deliverypointCollectionViaTerminal);
			toReturn.Add("DeliverypointCollectionViaTerminal_", _deliverypointCollectionViaTerminal_);
			toReturn.Add("DeliverypointgroupCollectionViaClient", _deliverypointgroupCollectionViaClient);
			toReturn.Add("DeliverypointgroupCollectionViaTerminal", _deliverypointgroupCollectionViaTerminal);
			toReturn.Add("EntertainmentCollectionViaTerminal", _entertainmentCollectionViaTerminal);
			toReturn.Add("EntertainmentCollectionViaTerminal_", _entertainmentCollectionViaTerminal_);
			toReturn.Add("EntertainmentCollectionViaTerminal__", _entertainmentCollectionViaTerminal__);
			toReturn.Add("IcrtouchprintermappingCollectionViaTerminal", _icrtouchprintermappingCollectionViaTerminal);
			toReturn.Add("ProductCollectionViaTerminal", _productCollectionViaTerminal);
			toReturn.Add("ProductCollectionViaTerminal_", _productCollectionViaTerminal_);
			toReturn.Add("ProductCollectionViaTerminal__", _productCollectionViaTerminal__);
			toReturn.Add("ProductCollectionViaTerminal___", _productCollectionViaTerminal___);
			toReturn.Add("TerminalCollectionViaTerminal", _terminalCollectionViaTerminal);
			toReturn.Add("UIModeCollectionViaTerminal", _uIModeCollectionViaTerminal);
			toReturn.Add("UserCollectionViaTerminal", _userCollectionViaTerminal);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="deviceId">PK value for Device which data should be fetched into this Device object</param>
		/// <param name="validator">The validator object for this DeviceEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int32 deviceId, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(deviceId, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_clientCollection = new Obymobi.Data.CollectionClasses.ClientCollection();
			_clientCollection.SetContainingEntityInfo(this, "DeviceEntity");

			_deliverypointCollection = new Obymobi.Data.CollectionClasses.DeliverypointCollection();
			_deliverypointCollection.SetContainingEntityInfo(this, "DeviceEntity");

			_devicemediumCollection = new Obymobi.Data.CollectionClasses.DevicemediaCollection();
			_devicemediumCollection.SetContainingEntityInfo(this, "DeviceEntity");

			_deviceTokenHistoryCollection = new Obymobi.Data.CollectionClasses.DeviceTokenHistoryCollection();
			_deviceTokenHistoryCollection.SetContainingEntityInfo(this, "DeviceEntity");

			_deviceTokenTaskCollection = new Obymobi.Data.CollectionClasses.DeviceTokenTaskCollection();
			_deviceTokenTaskCollection.SetContainingEntityInfo(this, "DeviceEntity");

			_terminalCollection = new Obymobi.Data.CollectionClasses.TerminalCollection();
			_terminalCollection.SetContainingEntityInfo(this, "DeviceEntity");
			_companyCollectionViaClient = new Obymobi.Data.CollectionClasses.CompanyCollection();
			_companyCollectionViaTerminal = new Obymobi.Data.CollectionClasses.CompanyCollection();
			_deliverypointCollectionViaClient = new Obymobi.Data.CollectionClasses.DeliverypointCollection();
			_deliverypointCollectionViaClient_ = new Obymobi.Data.CollectionClasses.DeliverypointCollection();
			_deliverypointCollectionViaTerminal = new Obymobi.Data.CollectionClasses.DeliverypointCollection();
			_deliverypointCollectionViaTerminal_ = new Obymobi.Data.CollectionClasses.DeliverypointCollection();
			_deliverypointgroupCollectionViaClient = new Obymobi.Data.CollectionClasses.DeliverypointgroupCollection();
			_deliverypointgroupCollectionViaTerminal = new Obymobi.Data.CollectionClasses.DeliverypointgroupCollection();
			_entertainmentCollectionViaTerminal = new Obymobi.Data.CollectionClasses.EntertainmentCollection();
			_entertainmentCollectionViaTerminal_ = new Obymobi.Data.CollectionClasses.EntertainmentCollection();
			_entertainmentCollectionViaTerminal__ = new Obymobi.Data.CollectionClasses.EntertainmentCollection();
			_icrtouchprintermappingCollectionViaTerminal = new Obymobi.Data.CollectionClasses.IcrtouchprintermappingCollection();
			_productCollectionViaTerminal = new Obymobi.Data.CollectionClasses.ProductCollection();
			_productCollectionViaTerminal_ = new Obymobi.Data.CollectionClasses.ProductCollection();
			_productCollectionViaTerminal__ = new Obymobi.Data.CollectionClasses.ProductCollection();
			_productCollectionViaTerminal___ = new Obymobi.Data.CollectionClasses.ProductCollection();
			_terminalCollectionViaTerminal = new Obymobi.Data.CollectionClasses.TerminalCollection();
			_uIModeCollectionViaTerminal = new Obymobi.Data.CollectionClasses.UIModeCollection();
			_userCollectionViaTerminal = new Obymobi.Data.CollectionClasses.UserCollection();
			_customerEntityReturnsNewIfNotFound = true;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DeviceId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Identifier", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Type", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Notes", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdateStatus", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CustomerId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DevicePlatform", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IsDevelopment", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DeviceModel", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdateStatusChangedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastRequestUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastRequestNotifiedBySmsUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastSupportToolsRequestUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IsCharging", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("BatteryLevel", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PublicIpAddress", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PrivateIpAddresses", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ApplicationVersion", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AgentRunning", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AgentVersion", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SupportToolsRunning", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SupportToolsVersion", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("OsVersion", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("WifiStrength", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DeviceInfoUpdatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdateEmenuDownloaded", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdateAgentDownloaded", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdateSupportToolsDownloaded", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdateOSDownloaded", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CloudEnvironment", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CommunicationMethod", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdateConsoleDownloaded", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastUserInteraction", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Token", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SandboxMode", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("MessagingServiceRunning", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("MessagingServiceVersion", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastMessagingServiceRequest", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastMessagingServiceConnectionType", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastMessagingServiceConnectionStatus", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LastMessagingServiceSuccessfulConnection", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("MessagingServiceConnectingCount24h", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("MessagingServiceConnectedCount24h", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("MessagingServiceReconnectingCount24h", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("MessagingServiceConnectionLostCount24h", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _customerEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncCustomerEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _customerEntity, new PropertyChangedEventHandler( OnCustomerEntityPropertyChanged ), "CustomerEntity", Obymobi.Data.RelationClasses.StaticDeviceRelations.CustomerEntityUsingCustomerIdStatic, true, signalRelatedEntity, "DeviceCollection", resetFKFields, new int[] { (int)DeviceFieldIndex.CustomerId } );		
			_customerEntity = null;
		}
		
		/// <summary> setups the sync logic for member _customerEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncCustomerEntity(IEntityCore relatedEntity)
		{
			if(_customerEntity!=relatedEntity)
			{		
				DesetupSyncCustomerEntity(true, true);
				_customerEntity = (CustomerEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _customerEntity, new PropertyChangedEventHandler( OnCustomerEntityPropertyChanged ), "CustomerEntity", Obymobi.Data.RelationClasses.StaticDeviceRelations.CustomerEntityUsingCustomerIdStatic, true, ref _alreadyFetchedCustomerEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnCustomerEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="deviceId">PK value for Device which data should be fetched into this Device object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int32 deviceId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)DeviceFieldIndex.DeviceId].ForcedCurrentValueWrite(deviceId);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateDeviceDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new DeviceEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static DeviceRelations Relations
		{
			get	{ return new DeviceRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Client' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathClientCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ClientCollection(), (IEntityRelation)GetRelationsForField("ClientCollection")[0], (int)Obymobi.Data.EntityType.DeviceEntity, (int)Obymobi.Data.EntityType.ClientEntity, 0, null, null, null, "ClientCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Deliverypoint' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathDeliverypointCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.DeliverypointCollection(), (IEntityRelation)GetRelationsForField("DeliverypointCollection")[0], (int)Obymobi.Data.EntityType.DeviceEntity, (int)Obymobi.Data.EntityType.DeliverypointEntity, 0, null, null, null, "DeliverypointCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Devicemedia' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathDevicemediumCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.DevicemediaCollection(), (IEntityRelation)GetRelationsForField("DevicemediumCollection")[0], (int)Obymobi.Data.EntityType.DeviceEntity, (int)Obymobi.Data.EntityType.DevicemediaEntity, 0, null, null, null, "DevicemediumCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'DeviceTokenHistory' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathDeviceTokenHistoryCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.DeviceTokenHistoryCollection(), (IEntityRelation)GetRelationsForField("DeviceTokenHistoryCollection")[0], (int)Obymobi.Data.EntityType.DeviceEntity, (int)Obymobi.Data.EntityType.DeviceTokenHistoryEntity, 0, null, null, null, "DeviceTokenHistoryCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'DeviceTokenTask' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathDeviceTokenTaskCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.DeviceTokenTaskCollection(), (IEntityRelation)GetRelationsForField("DeviceTokenTaskCollection")[0], (int)Obymobi.Data.EntityType.DeviceEntity, (int)Obymobi.Data.EntityType.DeviceTokenTaskEntity, 0, null, null, null, "DeviceTokenTaskCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Terminal' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTerminalCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.TerminalCollection(), (IEntityRelation)GetRelationsForField("TerminalCollection")[0], (int)Obymobi.Data.EntityType.DeviceEntity, (int)Obymobi.Data.EntityType.TerminalEntity, 0, null, null, null, "TerminalCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Company'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCompanyCollectionViaClient
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.ClientEntityUsingDeviceId;
				intermediateRelation.SetAliases(string.Empty, "Client_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CompanyCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.DeviceEntity, (int)Obymobi.Data.EntityType.CompanyEntity, 0, null, null, GetRelationsForField("CompanyCollectionViaClient"), "CompanyCollectionViaClient", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Company'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCompanyCollectionViaTerminal
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.TerminalEntityUsingDeviceId;
				intermediateRelation.SetAliases(string.Empty, "Terminal_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CompanyCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.DeviceEntity, (int)Obymobi.Data.EntityType.CompanyEntity, 0, null, null, GetRelationsForField("CompanyCollectionViaTerminal"), "CompanyCollectionViaTerminal", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Deliverypoint'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathDeliverypointCollectionViaClient
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.ClientEntityUsingDeviceId;
				intermediateRelation.SetAliases(string.Empty, "Client_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.DeliverypointCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.DeviceEntity, (int)Obymobi.Data.EntityType.DeliverypointEntity, 0, null, null, GetRelationsForField("DeliverypointCollectionViaClient"), "DeliverypointCollectionViaClient", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Deliverypoint'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathDeliverypointCollectionViaClient_
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.ClientEntityUsingDeviceId;
				intermediateRelation.SetAliases(string.Empty, "Client_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.DeliverypointCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.DeviceEntity, (int)Obymobi.Data.EntityType.DeliverypointEntity, 0, null, null, GetRelationsForField("DeliverypointCollectionViaClient_"), "DeliverypointCollectionViaClient_", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Deliverypoint'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathDeliverypointCollectionViaTerminal
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.TerminalEntityUsingDeviceId;
				intermediateRelation.SetAliases(string.Empty, "Terminal_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.DeliverypointCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.DeviceEntity, (int)Obymobi.Data.EntityType.DeliverypointEntity, 0, null, null, GetRelationsForField("DeliverypointCollectionViaTerminal"), "DeliverypointCollectionViaTerminal", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Deliverypoint'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathDeliverypointCollectionViaTerminal_
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.TerminalEntityUsingDeviceId;
				intermediateRelation.SetAliases(string.Empty, "Terminal_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.DeliverypointCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.DeviceEntity, (int)Obymobi.Data.EntityType.DeliverypointEntity, 0, null, null, GetRelationsForField("DeliverypointCollectionViaTerminal_"), "DeliverypointCollectionViaTerminal_", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Deliverypointgroup'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathDeliverypointgroupCollectionViaClient
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.ClientEntityUsingDeviceId;
				intermediateRelation.SetAliases(string.Empty, "Client_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.DeliverypointgroupCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.DeviceEntity, (int)Obymobi.Data.EntityType.DeliverypointgroupEntity, 0, null, null, GetRelationsForField("DeliverypointgroupCollectionViaClient"), "DeliverypointgroupCollectionViaClient", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Deliverypointgroup'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathDeliverypointgroupCollectionViaTerminal
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.TerminalEntityUsingDeviceId;
				intermediateRelation.SetAliases(string.Empty, "Terminal_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.DeliverypointgroupCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.DeviceEntity, (int)Obymobi.Data.EntityType.DeliverypointgroupEntity, 0, null, null, GetRelationsForField("DeliverypointgroupCollectionViaTerminal"), "DeliverypointgroupCollectionViaTerminal", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Entertainment'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathEntertainmentCollectionViaTerminal
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.TerminalEntityUsingDeviceId;
				intermediateRelation.SetAliases(string.Empty, "Terminal_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.EntertainmentCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.DeviceEntity, (int)Obymobi.Data.EntityType.EntertainmentEntity, 0, null, null, GetRelationsForField("EntertainmentCollectionViaTerminal"), "EntertainmentCollectionViaTerminal", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Entertainment'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathEntertainmentCollectionViaTerminal_
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.TerminalEntityUsingDeviceId;
				intermediateRelation.SetAliases(string.Empty, "Terminal_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.EntertainmentCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.DeviceEntity, (int)Obymobi.Data.EntityType.EntertainmentEntity, 0, null, null, GetRelationsForField("EntertainmentCollectionViaTerminal_"), "EntertainmentCollectionViaTerminal_", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Entertainment'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathEntertainmentCollectionViaTerminal__
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.TerminalEntityUsingDeviceId;
				intermediateRelation.SetAliases(string.Empty, "Terminal_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.EntertainmentCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.DeviceEntity, (int)Obymobi.Data.EntityType.EntertainmentEntity, 0, null, null, GetRelationsForField("EntertainmentCollectionViaTerminal__"), "EntertainmentCollectionViaTerminal__", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Icrtouchprintermapping'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathIcrtouchprintermappingCollectionViaTerminal
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.TerminalEntityUsingDeviceId;
				intermediateRelation.SetAliases(string.Empty, "Terminal_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.IcrtouchprintermappingCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.DeviceEntity, (int)Obymobi.Data.EntityType.IcrtouchprintermappingEntity, 0, null, null, GetRelationsForField("IcrtouchprintermappingCollectionViaTerminal"), "IcrtouchprintermappingCollectionViaTerminal", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Product'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathProductCollectionViaTerminal
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.TerminalEntityUsingDeviceId;
				intermediateRelation.SetAliases(string.Empty, "Terminal_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ProductCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.DeviceEntity, (int)Obymobi.Data.EntityType.ProductEntity, 0, null, null, GetRelationsForField("ProductCollectionViaTerminal"), "ProductCollectionViaTerminal", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Product'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathProductCollectionViaTerminal_
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.TerminalEntityUsingDeviceId;
				intermediateRelation.SetAliases(string.Empty, "Terminal_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ProductCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.DeviceEntity, (int)Obymobi.Data.EntityType.ProductEntity, 0, null, null, GetRelationsForField("ProductCollectionViaTerminal_"), "ProductCollectionViaTerminal_", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Product'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathProductCollectionViaTerminal__
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.TerminalEntityUsingDeviceId;
				intermediateRelation.SetAliases(string.Empty, "Terminal_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ProductCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.DeviceEntity, (int)Obymobi.Data.EntityType.ProductEntity, 0, null, null, GetRelationsForField("ProductCollectionViaTerminal__"), "ProductCollectionViaTerminal__", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Product'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathProductCollectionViaTerminal___
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.TerminalEntityUsingDeviceId;
				intermediateRelation.SetAliases(string.Empty, "Terminal_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ProductCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.DeviceEntity, (int)Obymobi.Data.EntityType.ProductEntity, 0, null, null, GetRelationsForField("ProductCollectionViaTerminal___"), "ProductCollectionViaTerminal___", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Terminal'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathTerminalCollectionViaTerminal
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.TerminalEntityUsingDeviceId;
				intermediateRelation.SetAliases(string.Empty, "Terminal_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.TerminalCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.DeviceEntity, (int)Obymobi.Data.EntityType.TerminalEntity, 0, null, null, GetRelationsForField("TerminalCollectionViaTerminal"), "TerminalCollectionViaTerminal", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'UIMode'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathUIModeCollectionViaTerminal
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.TerminalEntityUsingDeviceId;
				intermediateRelation.SetAliases(string.Empty, "Terminal_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.UIModeCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.DeviceEntity, (int)Obymobi.Data.EntityType.UIModeEntity, 0, null, null, GetRelationsForField("UIModeCollectionViaTerminal"), "UIModeCollectionViaTerminal", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'User'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathUserCollectionViaTerminal
		{
			get
			{
				IEntityRelation intermediateRelation = Relations.TerminalEntityUsingDeviceId;
				intermediateRelation.SetAliases(string.Empty, "Terminal_");
				return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.UserCollection(), intermediateRelation,	(int)Obymobi.Data.EntityType.DeviceEntity, (int)Obymobi.Data.EntityType.UserEntity, 0, null, null, GetRelationsForField("UserCollectionViaTerminal"), "UserCollectionViaTerminal", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToMany);
			}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Customer'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCustomerEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CustomerCollection(), (IEntityRelation)GetRelationsForField("CustomerEntity")[0], (int)Obymobi.Data.EntityType.DeviceEntity, (int)Obymobi.Data.EntityType.CustomerEntity, 0, null, null, null, "CustomerEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The DeviceId property of the Entity Device<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Device"."DeviceId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 DeviceId
		{
			get { return (System.Int32)GetValue((int)DeviceFieldIndex.DeviceId, true); }
			set	{ SetValue((int)DeviceFieldIndex.DeviceId, value, true); }
		}

		/// <summary> The Identifier property of the Entity Device<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Device"."Identifier"<br/>
		/// Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 255<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String Identifier
		{
			get { return (System.String)GetValue((int)DeviceFieldIndex.Identifier, true); }
			set	{ SetValue((int)DeviceFieldIndex.Identifier, value, true); }
		}

		/// <summary> The Type property of the Entity Device<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Device"."Type"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual Obymobi.Enums.DeviceType Type
		{
			get { return (Obymobi.Enums.DeviceType)GetValue((int)DeviceFieldIndex.Type, true); }
			set	{ SetValue((int)DeviceFieldIndex.Type, value, true); }
		}

		/// <summary> The Notes property of the Entity Device<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Device"."Notes"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Notes
		{
			get { return (System.String)GetValue((int)DeviceFieldIndex.Notes, true); }
			set	{ SetValue((int)DeviceFieldIndex.Notes, value, true); }
		}

		/// <summary> The CreatedBy property of the Entity Device<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Device"."CreatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CreatedBy
		{
			get { return (System.Int32)GetValue((int)DeviceFieldIndex.CreatedBy, true); }
			set	{ SetValue((int)DeviceFieldIndex.CreatedBy, value, true); }
		}

		/// <summary> The UpdatedBy property of the Entity Device<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Device"."UpdatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 UpdatedBy
		{
			get { return (System.Int32)GetValue((int)DeviceFieldIndex.UpdatedBy, true); }
			set	{ SetValue((int)DeviceFieldIndex.UpdatedBy, value, true); }
		}

		/// <summary> The UpdateStatus property of the Entity Device<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Device"."UpdateStatus"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 UpdateStatus
		{
			get { return (System.Int32)GetValue((int)DeviceFieldIndex.UpdateStatus, true); }
			set	{ SetValue((int)DeviceFieldIndex.UpdateStatus, value, true); }
		}

		/// <summary> The CustomerId property of the Entity Device<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Device"."CustomerId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> CustomerId
		{
			get { return (Nullable<System.Int32>)GetValue((int)DeviceFieldIndex.CustomerId, false); }
			set	{ SetValue((int)DeviceFieldIndex.CustomerId, value, true); }
		}

		/// <summary> The DevicePlatform property of the Entity Device<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Device"."DevicePlatform"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 DevicePlatform
		{
			get { return (System.Int32)GetValue((int)DeviceFieldIndex.DevicePlatform, true); }
			set	{ SetValue((int)DeviceFieldIndex.DevicePlatform, value, true); }
		}

		/// <summary> The IsDevelopment property of the Entity Device<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Device"."IsDevelopment"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean IsDevelopment
		{
			get { return (System.Boolean)GetValue((int)DeviceFieldIndex.IsDevelopment, true); }
			set	{ SetValue((int)DeviceFieldIndex.IsDevelopment, value, true); }
		}

		/// <summary> The DeviceModel property of the Entity Device<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Device"."DeviceModel"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<Obymobi.Enums.DeviceModel> DeviceModel
		{
			get { return (Nullable<Obymobi.Enums.DeviceModel>)GetValue((int)DeviceFieldIndex.DeviceModel, false); }
			set	{ SetValue((int)DeviceFieldIndex.DeviceModel, value, true); }
		}

		/// <summary> The CreatedUTC property of the Entity Device<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Device"."CreatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CreatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)DeviceFieldIndex.CreatedUTC, false); }
			set	{ SetValue((int)DeviceFieldIndex.CreatedUTC, value, true); }
		}

		/// <summary> The UpdatedUTC property of the Entity Device<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Device"."UpdatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)DeviceFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)DeviceFieldIndex.UpdatedUTC, value, true); }
		}

		/// <summary> The UpdateStatusChangedUTC property of the Entity Device<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Device"."UpdateStatusChangedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime UpdateStatusChangedUTC
		{
			get { return (System.DateTime)GetValue((int)DeviceFieldIndex.UpdateStatusChangedUTC, true); }
			set	{ SetValue((int)DeviceFieldIndex.UpdateStatusChangedUTC, value, true); }
		}

		/// <summary> The LastRequestUTC property of the Entity Device<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Device"."LastRequestUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> LastRequestUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)DeviceFieldIndex.LastRequestUTC, false); }
			set	{ SetValue((int)DeviceFieldIndex.LastRequestUTC, value, true); }
		}

		/// <summary> The LastRequestNotifiedBySmsUTC property of the Entity Device<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Device"."LastRequestNotifiedBySmsUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> LastRequestNotifiedBySmsUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)DeviceFieldIndex.LastRequestNotifiedBySmsUTC, false); }
			set	{ SetValue((int)DeviceFieldIndex.LastRequestNotifiedBySmsUTC, value, true); }
		}

		/// <summary> The LastSupportToolsRequestUTC property of the Entity Device<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Device"."LastSupportToolsRequestUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> LastSupportToolsRequestUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)DeviceFieldIndex.LastSupportToolsRequestUTC, false); }
			set	{ SetValue((int)DeviceFieldIndex.LastSupportToolsRequestUTC, value, true); }
		}

		/// <summary> The IsCharging property of the Entity Device<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Device"."IsCharging"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean IsCharging
		{
			get { return (System.Boolean)GetValue((int)DeviceFieldIndex.IsCharging, true); }
			set	{ SetValue((int)DeviceFieldIndex.IsCharging, value, true); }
		}

		/// <summary> The BatteryLevel property of the Entity Device<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Device"."BatteryLevel"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> BatteryLevel
		{
			get { return (Nullable<System.Int32>)GetValue((int)DeviceFieldIndex.BatteryLevel, false); }
			set	{ SetValue((int)DeviceFieldIndex.BatteryLevel, value, true); }
		}

		/// <summary> The PublicIpAddress property of the Entity Device<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Device"."PublicIpAddress"<br/>
		/// Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 255<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String PublicIpAddress
		{
			get { return (System.String)GetValue((int)DeviceFieldIndex.PublicIpAddress, true); }
			set	{ SetValue((int)DeviceFieldIndex.PublicIpAddress, value, true); }
		}

		/// <summary> The PrivateIpAddresses property of the Entity Device<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Device"."PrivateIpAddresses"<br/>
		/// Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 255<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String PrivateIpAddresses
		{
			get { return (System.String)GetValue((int)DeviceFieldIndex.PrivateIpAddresses, true); }
			set	{ SetValue((int)DeviceFieldIndex.PrivateIpAddresses, value, true); }
		}

		/// <summary> The ApplicationVersion property of the Entity Device<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Device"."ApplicationVersion"<br/>
		/// Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ApplicationVersion
		{
			get { return (System.String)GetValue((int)DeviceFieldIndex.ApplicationVersion, true); }
			set	{ SetValue((int)DeviceFieldIndex.ApplicationVersion, value, true); }
		}

		/// <summary> The AgentRunning property of the Entity Device<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Device"."AgentRunning"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean AgentRunning
		{
			get { return (System.Boolean)GetValue((int)DeviceFieldIndex.AgentRunning, true); }
			set	{ SetValue((int)DeviceFieldIndex.AgentRunning, value, true); }
		}

		/// <summary> The AgentVersion property of the Entity Device<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Device"."AgentVersion"<br/>
		/// Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String AgentVersion
		{
			get { return (System.String)GetValue((int)DeviceFieldIndex.AgentVersion, true); }
			set	{ SetValue((int)DeviceFieldIndex.AgentVersion, value, true); }
		}

		/// <summary> The SupportToolsRunning property of the Entity Device<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Device"."SupportToolsRunning"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean SupportToolsRunning
		{
			get { return (System.Boolean)GetValue((int)DeviceFieldIndex.SupportToolsRunning, true); }
			set	{ SetValue((int)DeviceFieldIndex.SupportToolsRunning, value, true); }
		}

		/// <summary> The SupportToolsVersion property of the Entity Device<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Device"."SupportToolsVersion"<br/>
		/// Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String SupportToolsVersion
		{
			get { return (System.String)GetValue((int)DeviceFieldIndex.SupportToolsVersion, true); }
			set	{ SetValue((int)DeviceFieldIndex.SupportToolsVersion, value, true); }
		}

		/// <summary> The OsVersion property of the Entity Device<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Device"."OsVersion"<br/>
		/// Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String OsVersion
		{
			get { return (System.String)GetValue((int)DeviceFieldIndex.OsVersion, true); }
			set	{ SetValue((int)DeviceFieldIndex.OsVersion, value, true); }
		}

		/// <summary> The WifiStrength property of the Entity Device<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Device"."WifiStrength"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> WifiStrength
		{
			get { return (Nullable<System.Int32>)GetValue((int)DeviceFieldIndex.WifiStrength, false); }
			set	{ SetValue((int)DeviceFieldIndex.WifiStrength, value, true); }
		}

		/// <summary> The DeviceInfoUpdatedUTC property of the Entity Device<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Device"."DeviceInfoUpdatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> DeviceInfoUpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)DeviceFieldIndex.DeviceInfoUpdatedUTC, false); }
			set	{ SetValue((int)DeviceFieldIndex.DeviceInfoUpdatedUTC, value, true); }
		}

		/// <summary> The UpdateEmenuDownloaded property of the Entity Device<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Device"."UpdateEmenuDownloaded"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean UpdateEmenuDownloaded
		{
			get { return (System.Boolean)GetValue((int)DeviceFieldIndex.UpdateEmenuDownloaded, true); }
			set	{ SetValue((int)DeviceFieldIndex.UpdateEmenuDownloaded, value, true); }
		}

		/// <summary> The UpdateAgentDownloaded property of the Entity Device<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Device"."UpdateAgentDownloaded"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean UpdateAgentDownloaded
		{
			get { return (System.Boolean)GetValue((int)DeviceFieldIndex.UpdateAgentDownloaded, true); }
			set	{ SetValue((int)DeviceFieldIndex.UpdateAgentDownloaded, value, true); }
		}

		/// <summary> The UpdateSupportToolsDownloaded property of the Entity Device<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Device"."UpdateSupportToolsDownloaded"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean UpdateSupportToolsDownloaded
		{
			get { return (System.Boolean)GetValue((int)DeviceFieldIndex.UpdateSupportToolsDownloaded, true); }
			set	{ SetValue((int)DeviceFieldIndex.UpdateSupportToolsDownloaded, value, true); }
		}

		/// <summary> The UpdateOSDownloaded property of the Entity Device<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Device"."UpdateOSDownloaded"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean UpdateOSDownloaded
		{
			get { return (System.Boolean)GetValue((int)DeviceFieldIndex.UpdateOSDownloaded, true); }
			set	{ SetValue((int)DeviceFieldIndex.UpdateOSDownloaded, value, true); }
		}

		/// <summary> The CloudEnvironment property of the Entity Device<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Device"."CloudEnvironment"<br/>
		/// Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String CloudEnvironment
		{
			get { return (System.String)GetValue((int)DeviceFieldIndex.CloudEnvironment, true); }
			set	{ SetValue((int)DeviceFieldIndex.CloudEnvironment, value, true); }
		}

		/// <summary> The CommunicationMethod property of the Entity Device<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Device"."CommunicationMethod"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CommunicationMethod
		{
			get { return (System.Int32)GetValue((int)DeviceFieldIndex.CommunicationMethod, true); }
			set	{ SetValue((int)DeviceFieldIndex.CommunicationMethod, value, true); }
		}

		/// <summary> The UpdateConsoleDownloaded property of the Entity Device<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Device"."UpdateConsoleDownloaded"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean UpdateConsoleDownloaded
		{
			get { return (System.Boolean)GetValue((int)DeviceFieldIndex.UpdateConsoleDownloaded, true); }
			set	{ SetValue((int)DeviceFieldIndex.UpdateConsoleDownloaded, value, true); }
		}

		/// <summary> The LastUserInteraction property of the Entity Device<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Device"."LastUserInteraction"<br/>
		/// Table field type characteristics (type, precision, scale, length): BigInt, 19, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int64> LastUserInteraction
		{
			get { return (Nullable<System.Int64>)GetValue((int)DeviceFieldIndex.LastUserInteraction, false); }
			set	{ SetValue((int)DeviceFieldIndex.LastUserInteraction, value, true); }
		}

		/// <summary> The Token property of the Entity Device<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Device"."Token"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Token
		{
			get { return (System.String)GetValue((int)DeviceFieldIndex.Token, true); }
			set	{ SetValue((int)DeviceFieldIndex.Token, value, true); }
		}

		/// <summary> The SandboxMode property of the Entity Device<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Device"."SandboxMode"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean SandboxMode
		{
			get { return (System.Boolean)GetValue((int)DeviceFieldIndex.SandboxMode, true); }
			set	{ SetValue((int)DeviceFieldIndex.SandboxMode, value, true); }
		}

		/// <summary> The MessagingServiceRunning property of the Entity Device<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Device"."MessagingServiceRunning"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean MessagingServiceRunning
		{
			get { return (System.Boolean)GetValue((int)DeviceFieldIndex.MessagingServiceRunning, true); }
			set	{ SetValue((int)DeviceFieldIndex.MessagingServiceRunning, value, true); }
		}

		/// <summary> The MessagingServiceVersion property of the Entity Device<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Device"."MessagingServiceVersion"<br/>
		/// Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String MessagingServiceVersion
		{
			get { return (System.String)GetValue((int)DeviceFieldIndex.MessagingServiceVersion, true); }
			set	{ SetValue((int)DeviceFieldIndex.MessagingServiceVersion, value, true); }
		}

		/// <summary> The LastMessagingServiceRequest property of the Entity Device<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Device"."LastMessagingServiceRequest"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> LastMessagingServiceRequest
		{
			get { return (Nullable<System.DateTime>)GetValue((int)DeviceFieldIndex.LastMessagingServiceRequest, false); }
			set	{ SetValue((int)DeviceFieldIndex.LastMessagingServiceRequest, value, true); }
		}

		/// <summary> The LastMessagingServiceConnectionType property of the Entity Device<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Device"."LastMessagingServiceConnectionType"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 LastMessagingServiceConnectionType
		{
			get { return (System.Int32)GetValue((int)DeviceFieldIndex.LastMessagingServiceConnectionType, true); }
			set	{ SetValue((int)DeviceFieldIndex.LastMessagingServiceConnectionType, value, true); }
		}

		/// <summary> The LastMessagingServiceConnectionStatus property of the Entity Device<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Device"."LastMessagingServiceConnectionStatus"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 LastMessagingServiceConnectionStatus
		{
			get { return (System.Int32)GetValue((int)DeviceFieldIndex.LastMessagingServiceConnectionStatus, true); }
			set	{ SetValue((int)DeviceFieldIndex.LastMessagingServiceConnectionStatus, value, true); }
		}

		/// <summary> The LastMessagingServiceSuccessfulConnection property of the Entity Device<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Device"."LastMessagingServiceSuccessfulConnection"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> LastMessagingServiceSuccessfulConnection
		{
			get { return (Nullable<System.DateTime>)GetValue((int)DeviceFieldIndex.LastMessagingServiceSuccessfulConnection, false); }
			set	{ SetValue((int)DeviceFieldIndex.LastMessagingServiceSuccessfulConnection, value, true); }
		}

		/// <summary> The MessagingServiceConnectingCount24h property of the Entity Device<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Device"."MessagingServiceConnectingCount24h"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 MessagingServiceConnectingCount24h
		{
			get { return (System.Int32)GetValue((int)DeviceFieldIndex.MessagingServiceConnectingCount24h, true); }
			set	{ SetValue((int)DeviceFieldIndex.MessagingServiceConnectingCount24h, value, true); }
		}

		/// <summary> The MessagingServiceConnectedCount24h property of the Entity Device<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Device"."MessagingServiceConnectedCount24h"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 MessagingServiceConnectedCount24h
		{
			get { return (System.Int32)GetValue((int)DeviceFieldIndex.MessagingServiceConnectedCount24h, true); }
			set	{ SetValue((int)DeviceFieldIndex.MessagingServiceConnectedCount24h, value, true); }
		}

		/// <summary> The MessagingServiceReconnectingCount24h property of the Entity Device<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Device"."MessagingServiceReconnectingCount24h"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 MessagingServiceReconnectingCount24h
		{
			get { return (System.Int32)GetValue((int)DeviceFieldIndex.MessagingServiceReconnectingCount24h, true); }
			set	{ SetValue((int)DeviceFieldIndex.MessagingServiceReconnectingCount24h, value, true); }
		}

		/// <summary> The MessagingServiceConnectionLostCount24h property of the Entity Device<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Device"."MessagingServiceConnectionLostCount24h"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 MessagingServiceConnectionLostCount24h
		{
			get { return (System.Int32)GetValue((int)DeviceFieldIndex.MessagingServiceConnectionLostCount24h, true); }
			set	{ SetValue((int)DeviceFieldIndex.MessagingServiceConnectionLostCount24h, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'ClientEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiClientCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.ClientCollection ClientCollection
		{
			get	{ return GetMultiClientCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ClientCollection. When set to true, ClientCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ClientCollection is accessed. You can always execute/ a forced fetch by calling GetMultiClientCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchClientCollection
		{
			get	{ return _alwaysFetchClientCollection; }
			set	{ _alwaysFetchClientCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property ClientCollection already has been fetched. Setting this property to false when ClientCollection has been fetched
		/// will clear the ClientCollection collection well. Setting this property to true while ClientCollection hasn't been fetched disables lazy loading for ClientCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedClientCollection
		{
			get { return _alreadyFetchedClientCollection;}
			set 
			{
				if(_alreadyFetchedClientCollection && !value && (_clientCollection != null))
				{
					_clientCollection.Clear();
				}
				_alreadyFetchedClientCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'DeliverypointEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiDeliverypointCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.DeliverypointCollection DeliverypointCollection
		{
			get	{ return GetMultiDeliverypointCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for DeliverypointCollection. When set to true, DeliverypointCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time DeliverypointCollection is accessed. You can always execute/ a forced fetch by calling GetMultiDeliverypointCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchDeliverypointCollection
		{
			get	{ return _alwaysFetchDeliverypointCollection; }
			set	{ _alwaysFetchDeliverypointCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property DeliverypointCollection already has been fetched. Setting this property to false when DeliverypointCollection has been fetched
		/// will clear the DeliverypointCollection collection well. Setting this property to true while DeliverypointCollection hasn't been fetched disables lazy loading for DeliverypointCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedDeliverypointCollection
		{
			get { return _alreadyFetchedDeliverypointCollection;}
			set 
			{
				if(_alreadyFetchedDeliverypointCollection && !value && (_deliverypointCollection != null))
				{
					_deliverypointCollection.Clear();
				}
				_alreadyFetchedDeliverypointCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'DevicemediaEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiDevicemediumCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.DevicemediaCollection DevicemediumCollection
		{
			get	{ return GetMultiDevicemediumCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for DevicemediumCollection. When set to true, DevicemediumCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time DevicemediumCollection is accessed. You can always execute/ a forced fetch by calling GetMultiDevicemediumCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchDevicemediumCollection
		{
			get	{ return _alwaysFetchDevicemediumCollection; }
			set	{ _alwaysFetchDevicemediumCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property DevicemediumCollection already has been fetched. Setting this property to false when DevicemediumCollection has been fetched
		/// will clear the DevicemediumCollection collection well. Setting this property to true while DevicemediumCollection hasn't been fetched disables lazy loading for DevicemediumCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedDevicemediumCollection
		{
			get { return _alreadyFetchedDevicemediumCollection;}
			set 
			{
				if(_alreadyFetchedDevicemediumCollection && !value && (_devicemediumCollection != null))
				{
					_devicemediumCollection.Clear();
				}
				_alreadyFetchedDevicemediumCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'DeviceTokenHistoryEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiDeviceTokenHistoryCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.DeviceTokenHistoryCollection DeviceTokenHistoryCollection
		{
			get	{ return GetMultiDeviceTokenHistoryCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for DeviceTokenHistoryCollection. When set to true, DeviceTokenHistoryCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time DeviceTokenHistoryCollection is accessed. You can always execute/ a forced fetch by calling GetMultiDeviceTokenHistoryCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchDeviceTokenHistoryCollection
		{
			get	{ return _alwaysFetchDeviceTokenHistoryCollection; }
			set	{ _alwaysFetchDeviceTokenHistoryCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property DeviceTokenHistoryCollection already has been fetched. Setting this property to false when DeviceTokenHistoryCollection has been fetched
		/// will clear the DeviceTokenHistoryCollection collection well. Setting this property to true while DeviceTokenHistoryCollection hasn't been fetched disables lazy loading for DeviceTokenHistoryCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedDeviceTokenHistoryCollection
		{
			get { return _alreadyFetchedDeviceTokenHistoryCollection;}
			set 
			{
				if(_alreadyFetchedDeviceTokenHistoryCollection && !value && (_deviceTokenHistoryCollection != null))
				{
					_deviceTokenHistoryCollection.Clear();
				}
				_alreadyFetchedDeviceTokenHistoryCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'DeviceTokenTaskEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiDeviceTokenTaskCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.DeviceTokenTaskCollection DeviceTokenTaskCollection
		{
			get	{ return GetMultiDeviceTokenTaskCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for DeviceTokenTaskCollection. When set to true, DeviceTokenTaskCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time DeviceTokenTaskCollection is accessed. You can always execute/ a forced fetch by calling GetMultiDeviceTokenTaskCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchDeviceTokenTaskCollection
		{
			get	{ return _alwaysFetchDeviceTokenTaskCollection; }
			set	{ _alwaysFetchDeviceTokenTaskCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property DeviceTokenTaskCollection already has been fetched. Setting this property to false when DeviceTokenTaskCollection has been fetched
		/// will clear the DeviceTokenTaskCollection collection well. Setting this property to true while DeviceTokenTaskCollection hasn't been fetched disables lazy loading for DeviceTokenTaskCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedDeviceTokenTaskCollection
		{
			get { return _alreadyFetchedDeviceTokenTaskCollection;}
			set 
			{
				if(_alreadyFetchedDeviceTokenTaskCollection && !value && (_deviceTokenTaskCollection != null))
				{
					_deviceTokenTaskCollection.Clear();
				}
				_alreadyFetchedDeviceTokenTaskCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'TerminalEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiTerminalCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.TerminalCollection TerminalCollection
		{
			get	{ return GetMultiTerminalCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for TerminalCollection. When set to true, TerminalCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TerminalCollection is accessed. You can always execute/ a forced fetch by calling GetMultiTerminalCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTerminalCollection
		{
			get	{ return _alwaysFetchTerminalCollection; }
			set	{ _alwaysFetchTerminalCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property TerminalCollection already has been fetched. Setting this property to false when TerminalCollection has been fetched
		/// will clear the TerminalCollection collection well. Setting this property to true while TerminalCollection hasn't been fetched disables lazy loading for TerminalCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTerminalCollection
		{
			get { return _alreadyFetchedTerminalCollection;}
			set 
			{
				if(_alreadyFetchedTerminalCollection && !value && (_terminalCollection != null))
				{
					_terminalCollection.Clear();
				}
				_alreadyFetchedTerminalCollection = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'CompanyEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCompanyCollectionViaClient()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.CompanyCollection CompanyCollectionViaClient
		{
			get { return GetMultiCompanyCollectionViaClient(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CompanyCollectionViaClient. When set to true, CompanyCollectionViaClient is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CompanyCollectionViaClient is accessed. You can always execute a forced fetch by calling GetMultiCompanyCollectionViaClient(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCompanyCollectionViaClient
		{
			get	{ return _alwaysFetchCompanyCollectionViaClient; }
			set	{ _alwaysFetchCompanyCollectionViaClient = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CompanyCollectionViaClient already has been fetched. Setting this property to false when CompanyCollectionViaClient has been fetched
		/// will clear the CompanyCollectionViaClient collection well. Setting this property to true while CompanyCollectionViaClient hasn't been fetched disables lazy loading for CompanyCollectionViaClient</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCompanyCollectionViaClient
		{
			get { return _alreadyFetchedCompanyCollectionViaClient;}
			set 
			{
				if(_alreadyFetchedCompanyCollectionViaClient && !value && (_companyCollectionViaClient != null))
				{
					_companyCollectionViaClient.Clear();
				}
				_alreadyFetchedCompanyCollectionViaClient = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'CompanyEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiCompanyCollectionViaTerminal()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.CompanyCollection CompanyCollectionViaTerminal
		{
			get { return GetMultiCompanyCollectionViaTerminal(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for CompanyCollectionViaTerminal. When set to true, CompanyCollectionViaTerminal is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CompanyCollectionViaTerminal is accessed. You can always execute a forced fetch by calling GetMultiCompanyCollectionViaTerminal(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCompanyCollectionViaTerminal
		{
			get	{ return _alwaysFetchCompanyCollectionViaTerminal; }
			set	{ _alwaysFetchCompanyCollectionViaTerminal = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CompanyCollectionViaTerminal already has been fetched. Setting this property to false when CompanyCollectionViaTerminal has been fetched
		/// will clear the CompanyCollectionViaTerminal collection well. Setting this property to true while CompanyCollectionViaTerminal hasn't been fetched disables lazy loading for CompanyCollectionViaTerminal</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCompanyCollectionViaTerminal
		{
			get { return _alreadyFetchedCompanyCollectionViaTerminal;}
			set 
			{
				if(_alreadyFetchedCompanyCollectionViaTerminal && !value && (_companyCollectionViaTerminal != null))
				{
					_companyCollectionViaTerminal.Clear();
				}
				_alreadyFetchedCompanyCollectionViaTerminal = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiDeliverypointCollectionViaClient()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.DeliverypointCollection DeliverypointCollectionViaClient
		{
			get { return GetMultiDeliverypointCollectionViaClient(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for DeliverypointCollectionViaClient. When set to true, DeliverypointCollectionViaClient is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time DeliverypointCollectionViaClient is accessed. You can always execute a forced fetch by calling GetMultiDeliverypointCollectionViaClient(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchDeliverypointCollectionViaClient
		{
			get	{ return _alwaysFetchDeliverypointCollectionViaClient; }
			set	{ _alwaysFetchDeliverypointCollectionViaClient = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property DeliverypointCollectionViaClient already has been fetched. Setting this property to false when DeliverypointCollectionViaClient has been fetched
		/// will clear the DeliverypointCollectionViaClient collection well. Setting this property to true while DeliverypointCollectionViaClient hasn't been fetched disables lazy loading for DeliverypointCollectionViaClient</summary>
		[Browsable(false)]
		public bool AlreadyFetchedDeliverypointCollectionViaClient
		{
			get { return _alreadyFetchedDeliverypointCollectionViaClient;}
			set 
			{
				if(_alreadyFetchedDeliverypointCollectionViaClient && !value && (_deliverypointCollectionViaClient != null))
				{
					_deliverypointCollectionViaClient.Clear();
				}
				_alreadyFetchedDeliverypointCollectionViaClient = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiDeliverypointCollectionViaClient_()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.DeliverypointCollection DeliverypointCollectionViaClient_
		{
			get { return GetMultiDeliverypointCollectionViaClient_(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for DeliverypointCollectionViaClient_. When set to true, DeliverypointCollectionViaClient_ is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time DeliverypointCollectionViaClient_ is accessed. You can always execute a forced fetch by calling GetMultiDeliverypointCollectionViaClient_(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchDeliverypointCollectionViaClient_
		{
			get	{ return _alwaysFetchDeliverypointCollectionViaClient_; }
			set	{ _alwaysFetchDeliverypointCollectionViaClient_ = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property DeliverypointCollectionViaClient_ already has been fetched. Setting this property to false when DeliverypointCollectionViaClient_ has been fetched
		/// will clear the DeliverypointCollectionViaClient_ collection well. Setting this property to true while DeliverypointCollectionViaClient_ hasn't been fetched disables lazy loading for DeliverypointCollectionViaClient_</summary>
		[Browsable(false)]
		public bool AlreadyFetchedDeliverypointCollectionViaClient_
		{
			get { return _alreadyFetchedDeliverypointCollectionViaClient_;}
			set 
			{
				if(_alreadyFetchedDeliverypointCollectionViaClient_ && !value && (_deliverypointCollectionViaClient_ != null))
				{
					_deliverypointCollectionViaClient_.Clear();
				}
				_alreadyFetchedDeliverypointCollectionViaClient_ = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiDeliverypointCollectionViaTerminal()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.DeliverypointCollection DeliverypointCollectionViaTerminal
		{
			get { return GetMultiDeliverypointCollectionViaTerminal(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for DeliverypointCollectionViaTerminal. When set to true, DeliverypointCollectionViaTerminal is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time DeliverypointCollectionViaTerminal is accessed. You can always execute a forced fetch by calling GetMultiDeliverypointCollectionViaTerminal(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchDeliverypointCollectionViaTerminal
		{
			get	{ return _alwaysFetchDeliverypointCollectionViaTerminal; }
			set	{ _alwaysFetchDeliverypointCollectionViaTerminal = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property DeliverypointCollectionViaTerminal already has been fetched. Setting this property to false when DeliverypointCollectionViaTerminal has been fetched
		/// will clear the DeliverypointCollectionViaTerminal collection well. Setting this property to true while DeliverypointCollectionViaTerminal hasn't been fetched disables lazy loading for DeliverypointCollectionViaTerminal</summary>
		[Browsable(false)]
		public bool AlreadyFetchedDeliverypointCollectionViaTerminal
		{
			get { return _alreadyFetchedDeliverypointCollectionViaTerminal;}
			set 
			{
				if(_alreadyFetchedDeliverypointCollectionViaTerminal && !value && (_deliverypointCollectionViaTerminal != null))
				{
					_deliverypointCollectionViaTerminal.Clear();
				}
				_alreadyFetchedDeliverypointCollectionViaTerminal = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiDeliverypointCollectionViaTerminal_()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.DeliverypointCollection DeliverypointCollectionViaTerminal_
		{
			get { return GetMultiDeliverypointCollectionViaTerminal_(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for DeliverypointCollectionViaTerminal_. When set to true, DeliverypointCollectionViaTerminal_ is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time DeliverypointCollectionViaTerminal_ is accessed. You can always execute a forced fetch by calling GetMultiDeliverypointCollectionViaTerminal_(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchDeliverypointCollectionViaTerminal_
		{
			get	{ return _alwaysFetchDeliverypointCollectionViaTerminal_; }
			set	{ _alwaysFetchDeliverypointCollectionViaTerminal_ = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property DeliverypointCollectionViaTerminal_ already has been fetched. Setting this property to false when DeliverypointCollectionViaTerminal_ has been fetched
		/// will clear the DeliverypointCollectionViaTerminal_ collection well. Setting this property to true while DeliverypointCollectionViaTerminal_ hasn't been fetched disables lazy loading for DeliverypointCollectionViaTerminal_</summary>
		[Browsable(false)]
		public bool AlreadyFetchedDeliverypointCollectionViaTerminal_
		{
			get { return _alreadyFetchedDeliverypointCollectionViaTerminal_;}
			set 
			{
				if(_alreadyFetchedDeliverypointCollectionViaTerminal_ && !value && (_deliverypointCollectionViaTerminal_ != null))
				{
					_deliverypointCollectionViaTerminal_.Clear();
				}
				_alreadyFetchedDeliverypointCollectionViaTerminal_ = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointgroupEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiDeliverypointgroupCollectionViaClient()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.DeliverypointgroupCollection DeliverypointgroupCollectionViaClient
		{
			get { return GetMultiDeliverypointgroupCollectionViaClient(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for DeliverypointgroupCollectionViaClient. When set to true, DeliverypointgroupCollectionViaClient is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time DeliverypointgroupCollectionViaClient is accessed. You can always execute a forced fetch by calling GetMultiDeliverypointgroupCollectionViaClient(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchDeliverypointgroupCollectionViaClient
		{
			get	{ return _alwaysFetchDeliverypointgroupCollectionViaClient; }
			set	{ _alwaysFetchDeliverypointgroupCollectionViaClient = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property DeliverypointgroupCollectionViaClient already has been fetched. Setting this property to false when DeliverypointgroupCollectionViaClient has been fetched
		/// will clear the DeliverypointgroupCollectionViaClient collection well. Setting this property to true while DeliverypointgroupCollectionViaClient hasn't been fetched disables lazy loading for DeliverypointgroupCollectionViaClient</summary>
		[Browsable(false)]
		public bool AlreadyFetchedDeliverypointgroupCollectionViaClient
		{
			get { return _alreadyFetchedDeliverypointgroupCollectionViaClient;}
			set 
			{
				if(_alreadyFetchedDeliverypointgroupCollectionViaClient && !value && (_deliverypointgroupCollectionViaClient != null))
				{
					_deliverypointgroupCollectionViaClient.Clear();
				}
				_alreadyFetchedDeliverypointgroupCollectionViaClient = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'DeliverypointgroupEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiDeliverypointgroupCollectionViaTerminal()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.DeliverypointgroupCollection DeliverypointgroupCollectionViaTerminal
		{
			get { return GetMultiDeliverypointgroupCollectionViaTerminal(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for DeliverypointgroupCollectionViaTerminal. When set to true, DeliverypointgroupCollectionViaTerminal is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time DeliverypointgroupCollectionViaTerminal is accessed. You can always execute a forced fetch by calling GetMultiDeliverypointgroupCollectionViaTerminal(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchDeliverypointgroupCollectionViaTerminal
		{
			get	{ return _alwaysFetchDeliverypointgroupCollectionViaTerminal; }
			set	{ _alwaysFetchDeliverypointgroupCollectionViaTerminal = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property DeliverypointgroupCollectionViaTerminal already has been fetched. Setting this property to false when DeliverypointgroupCollectionViaTerminal has been fetched
		/// will clear the DeliverypointgroupCollectionViaTerminal collection well. Setting this property to true while DeliverypointgroupCollectionViaTerminal hasn't been fetched disables lazy loading for DeliverypointgroupCollectionViaTerminal</summary>
		[Browsable(false)]
		public bool AlreadyFetchedDeliverypointgroupCollectionViaTerminal
		{
			get { return _alreadyFetchedDeliverypointgroupCollectionViaTerminal;}
			set 
			{
				if(_alreadyFetchedDeliverypointgroupCollectionViaTerminal && !value && (_deliverypointgroupCollectionViaTerminal != null))
				{
					_deliverypointgroupCollectionViaTerminal.Clear();
				}
				_alreadyFetchedDeliverypointgroupCollectionViaTerminal = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiEntertainmentCollectionViaTerminal()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.EntertainmentCollection EntertainmentCollectionViaTerminal
		{
			get { return GetMultiEntertainmentCollectionViaTerminal(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for EntertainmentCollectionViaTerminal. When set to true, EntertainmentCollectionViaTerminal is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time EntertainmentCollectionViaTerminal is accessed. You can always execute a forced fetch by calling GetMultiEntertainmentCollectionViaTerminal(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchEntertainmentCollectionViaTerminal
		{
			get	{ return _alwaysFetchEntertainmentCollectionViaTerminal; }
			set	{ _alwaysFetchEntertainmentCollectionViaTerminal = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property EntertainmentCollectionViaTerminal already has been fetched. Setting this property to false when EntertainmentCollectionViaTerminal has been fetched
		/// will clear the EntertainmentCollectionViaTerminal collection well. Setting this property to true while EntertainmentCollectionViaTerminal hasn't been fetched disables lazy loading for EntertainmentCollectionViaTerminal</summary>
		[Browsable(false)]
		public bool AlreadyFetchedEntertainmentCollectionViaTerminal
		{
			get { return _alreadyFetchedEntertainmentCollectionViaTerminal;}
			set 
			{
				if(_alreadyFetchedEntertainmentCollectionViaTerminal && !value && (_entertainmentCollectionViaTerminal != null))
				{
					_entertainmentCollectionViaTerminal.Clear();
				}
				_alreadyFetchedEntertainmentCollectionViaTerminal = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiEntertainmentCollectionViaTerminal_()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.EntertainmentCollection EntertainmentCollectionViaTerminal_
		{
			get { return GetMultiEntertainmentCollectionViaTerminal_(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for EntertainmentCollectionViaTerminal_. When set to true, EntertainmentCollectionViaTerminal_ is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time EntertainmentCollectionViaTerminal_ is accessed. You can always execute a forced fetch by calling GetMultiEntertainmentCollectionViaTerminal_(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchEntertainmentCollectionViaTerminal_
		{
			get	{ return _alwaysFetchEntertainmentCollectionViaTerminal_; }
			set	{ _alwaysFetchEntertainmentCollectionViaTerminal_ = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property EntertainmentCollectionViaTerminal_ already has been fetched. Setting this property to false when EntertainmentCollectionViaTerminal_ has been fetched
		/// will clear the EntertainmentCollectionViaTerminal_ collection well. Setting this property to true while EntertainmentCollectionViaTerminal_ hasn't been fetched disables lazy loading for EntertainmentCollectionViaTerminal_</summary>
		[Browsable(false)]
		public bool AlreadyFetchedEntertainmentCollectionViaTerminal_
		{
			get { return _alreadyFetchedEntertainmentCollectionViaTerminal_;}
			set 
			{
				if(_alreadyFetchedEntertainmentCollectionViaTerminal_ && !value && (_entertainmentCollectionViaTerminal_ != null))
				{
					_entertainmentCollectionViaTerminal_.Clear();
				}
				_alreadyFetchedEntertainmentCollectionViaTerminal_ = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'EntertainmentEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiEntertainmentCollectionViaTerminal__()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.EntertainmentCollection EntertainmentCollectionViaTerminal__
		{
			get { return GetMultiEntertainmentCollectionViaTerminal__(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for EntertainmentCollectionViaTerminal__. When set to true, EntertainmentCollectionViaTerminal__ is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time EntertainmentCollectionViaTerminal__ is accessed. You can always execute a forced fetch by calling GetMultiEntertainmentCollectionViaTerminal__(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchEntertainmentCollectionViaTerminal__
		{
			get	{ return _alwaysFetchEntertainmentCollectionViaTerminal__; }
			set	{ _alwaysFetchEntertainmentCollectionViaTerminal__ = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property EntertainmentCollectionViaTerminal__ already has been fetched. Setting this property to false when EntertainmentCollectionViaTerminal__ has been fetched
		/// will clear the EntertainmentCollectionViaTerminal__ collection well. Setting this property to true while EntertainmentCollectionViaTerminal__ hasn't been fetched disables lazy loading for EntertainmentCollectionViaTerminal__</summary>
		[Browsable(false)]
		public bool AlreadyFetchedEntertainmentCollectionViaTerminal__
		{
			get { return _alreadyFetchedEntertainmentCollectionViaTerminal__;}
			set 
			{
				if(_alreadyFetchedEntertainmentCollectionViaTerminal__ && !value && (_entertainmentCollectionViaTerminal__ != null))
				{
					_entertainmentCollectionViaTerminal__.Clear();
				}
				_alreadyFetchedEntertainmentCollectionViaTerminal__ = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'IcrtouchprintermappingEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiIcrtouchprintermappingCollectionViaTerminal()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.IcrtouchprintermappingCollection IcrtouchprintermappingCollectionViaTerminal
		{
			get { return GetMultiIcrtouchprintermappingCollectionViaTerminal(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for IcrtouchprintermappingCollectionViaTerminal. When set to true, IcrtouchprintermappingCollectionViaTerminal is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time IcrtouchprintermappingCollectionViaTerminal is accessed. You can always execute a forced fetch by calling GetMultiIcrtouchprintermappingCollectionViaTerminal(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchIcrtouchprintermappingCollectionViaTerminal
		{
			get	{ return _alwaysFetchIcrtouchprintermappingCollectionViaTerminal; }
			set	{ _alwaysFetchIcrtouchprintermappingCollectionViaTerminal = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property IcrtouchprintermappingCollectionViaTerminal already has been fetched. Setting this property to false when IcrtouchprintermappingCollectionViaTerminal has been fetched
		/// will clear the IcrtouchprintermappingCollectionViaTerminal collection well. Setting this property to true while IcrtouchprintermappingCollectionViaTerminal hasn't been fetched disables lazy loading for IcrtouchprintermappingCollectionViaTerminal</summary>
		[Browsable(false)]
		public bool AlreadyFetchedIcrtouchprintermappingCollectionViaTerminal
		{
			get { return _alreadyFetchedIcrtouchprintermappingCollectionViaTerminal;}
			set 
			{
				if(_alreadyFetchedIcrtouchprintermappingCollectionViaTerminal && !value && (_icrtouchprintermappingCollectionViaTerminal != null))
				{
					_icrtouchprintermappingCollectionViaTerminal.Clear();
				}
				_alreadyFetchedIcrtouchprintermappingCollectionViaTerminal = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiProductCollectionViaTerminal()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.ProductCollection ProductCollectionViaTerminal
		{
			get { return GetMultiProductCollectionViaTerminal(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ProductCollectionViaTerminal. When set to true, ProductCollectionViaTerminal is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ProductCollectionViaTerminal is accessed. You can always execute a forced fetch by calling GetMultiProductCollectionViaTerminal(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchProductCollectionViaTerminal
		{
			get	{ return _alwaysFetchProductCollectionViaTerminal; }
			set	{ _alwaysFetchProductCollectionViaTerminal = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ProductCollectionViaTerminal already has been fetched. Setting this property to false when ProductCollectionViaTerminal has been fetched
		/// will clear the ProductCollectionViaTerminal collection well. Setting this property to true while ProductCollectionViaTerminal hasn't been fetched disables lazy loading for ProductCollectionViaTerminal</summary>
		[Browsable(false)]
		public bool AlreadyFetchedProductCollectionViaTerminal
		{
			get { return _alreadyFetchedProductCollectionViaTerminal;}
			set 
			{
				if(_alreadyFetchedProductCollectionViaTerminal && !value && (_productCollectionViaTerminal != null))
				{
					_productCollectionViaTerminal.Clear();
				}
				_alreadyFetchedProductCollectionViaTerminal = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiProductCollectionViaTerminal_()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.ProductCollection ProductCollectionViaTerminal_
		{
			get { return GetMultiProductCollectionViaTerminal_(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ProductCollectionViaTerminal_. When set to true, ProductCollectionViaTerminal_ is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ProductCollectionViaTerminal_ is accessed. You can always execute a forced fetch by calling GetMultiProductCollectionViaTerminal_(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchProductCollectionViaTerminal_
		{
			get	{ return _alwaysFetchProductCollectionViaTerminal_; }
			set	{ _alwaysFetchProductCollectionViaTerminal_ = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ProductCollectionViaTerminal_ already has been fetched. Setting this property to false when ProductCollectionViaTerminal_ has been fetched
		/// will clear the ProductCollectionViaTerminal_ collection well. Setting this property to true while ProductCollectionViaTerminal_ hasn't been fetched disables lazy loading for ProductCollectionViaTerminal_</summary>
		[Browsable(false)]
		public bool AlreadyFetchedProductCollectionViaTerminal_
		{
			get { return _alreadyFetchedProductCollectionViaTerminal_;}
			set 
			{
				if(_alreadyFetchedProductCollectionViaTerminal_ && !value && (_productCollectionViaTerminal_ != null))
				{
					_productCollectionViaTerminal_.Clear();
				}
				_alreadyFetchedProductCollectionViaTerminal_ = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiProductCollectionViaTerminal__()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.ProductCollection ProductCollectionViaTerminal__
		{
			get { return GetMultiProductCollectionViaTerminal__(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ProductCollectionViaTerminal__. When set to true, ProductCollectionViaTerminal__ is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ProductCollectionViaTerminal__ is accessed. You can always execute a forced fetch by calling GetMultiProductCollectionViaTerminal__(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchProductCollectionViaTerminal__
		{
			get	{ return _alwaysFetchProductCollectionViaTerminal__; }
			set	{ _alwaysFetchProductCollectionViaTerminal__ = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ProductCollectionViaTerminal__ already has been fetched. Setting this property to false when ProductCollectionViaTerminal__ has been fetched
		/// will clear the ProductCollectionViaTerminal__ collection well. Setting this property to true while ProductCollectionViaTerminal__ hasn't been fetched disables lazy loading for ProductCollectionViaTerminal__</summary>
		[Browsable(false)]
		public bool AlreadyFetchedProductCollectionViaTerminal__
		{
			get { return _alreadyFetchedProductCollectionViaTerminal__;}
			set 
			{
				if(_alreadyFetchedProductCollectionViaTerminal__ && !value && (_productCollectionViaTerminal__ != null))
				{
					_productCollectionViaTerminal__.Clear();
				}
				_alreadyFetchedProductCollectionViaTerminal__ = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'ProductEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiProductCollectionViaTerminal___()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.ProductCollection ProductCollectionViaTerminal___
		{
			get { return GetMultiProductCollectionViaTerminal___(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ProductCollectionViaTerminal___. When set to true, ProductCollectionViaTerminal___ is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ProductCollectionViaTerminal___ is accessed. You can always execute a forced fetch by calling GetMultiProductCollectionViaTerminal___(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchProductCollectionViaTerminal___
		{
			get	{ return _alwaysFetchProductCollectionViaTerminal___; }
			set	{ _alwaysFetchProductCollectionViaTerminal___ = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ProductCollectionViaTerminal___ already has been fetched. Setting this property to false when ProductCollectionViaTerminal___ has been fetched
		/// will clear the ProductCollectionViaTerminal___ collection well. Setting this property to true while ProductCollectionViaTerminal___ hasn't been fetched disables lazy loading for ProductCollectionViaTerminal___</summary>
		[Browsable(false)]
		public bool AlreadyFetchedProductCollectionViaTerminal___
		{
			get { return _alreadyFetchedProductCollectionViaTerminal___;}
			set 
			{
				if(_alreadyFetchedProductCollectionViaTerminal___ && !value && (_productCollectionViaTerminal___ != null))
				{
					_productCollectionViaTerminal___.Clear();
				}
				_alreadyFetchedProductCollectionViaTerminal___ = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'TerminalEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiTerminalCollectionViaTerminal()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.TerminalCollection TerminalCollectionViaTerminal
		{
			get { return GetMultiTerminalCollectionViaTerminal(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for TerminalCollectionViaTerminal. When set to true, TerminalCollectionViaTerminal is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time TerminalCollectionViaTerminal is accessed. You can always execute a forced fetch by calling GetMultiTerminalCollectionViaTerminal(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchTerminalCollectionViaTerminal
		{
			get	{ return _alwaysFetchTerminalCollectionViaTerminal; }
			set	{ _alwaysFetchTerminalCollectionViaTerminal = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property TerminalCollectionViaTerminal already has been fetched. Setting this property to false when TerminalCollectionViaTerminal has been fetched
		/// will clear the TerminalCollectionViaTerminal collection well. Setting this property to true while TerminalCollectionViaTerminal hasn't been fetched disables lazy loading for TerminalCollectionViaTerminal</summary>
		[Browsable(false)]
		public bool AlreadyFetchedTerminalCollectionViaTerminal
		{
			get { return _alreadyFetchedTerminalCollectionViaTerminal;}
			set 
			{
				if(_alreadyFetchedTerminalCollectionViaTerminal && !value && (_terminalCollectionViaTerminal != null))
				{
					_terminalCollectionViaTerminal.Clear();
				}
				_alreadyFetchedTerminalCollectionViaTerminal = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'UIModeEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiUIModeCollectionViaTerminal()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.UIModeCollection UIModeCollectionViaTerminal
		{
			get { return GetMultiUIModeCollectionViaTerminal(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for UIModeCollectionViaTerminal. When set to true, UIModeCollectionViaTerminal is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time UIModeCollectionViaTerminal is accessed. You can always execute a forced fetch by calling GetMultiUIModeCollectionViaTerminal(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchUIModeCollectionViaTerminal
		{
			get	{ return _alwaysFetchUIModeCollectionViaTerminal; }
			set	{ _alwaysFetchUIModeCollectionViaTerminal = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property UIModeCollectionViaTerminal already has been fetched. Setting this property to false when UIModeCollectionViaTerminal has been fetched
		/// will clear the UIModeCollectionViaTerminal collection well. Setting this property to true while UIModeCollectionViaTerminal hasn't been fetched disables lazy loading for UIModeCollectionViaTerminal</summary>
		[Browsable(false)]
		public bool AlreadyFetchedUIModeCollectionViaTerminal
		{
			get { return _alreadyFetchedUIModeCollectionViaTerminal;}
			set 
			{
				if(_alreadyFetchedUIModeCollectionViaTerminal && !value && (_uIModeCollectionViaTerminal != null))
				{
					_uIModeCollectionViaTerminal.Clear();
				}
				_alreadyFetchedUIModeCollectionViaTerminal = value;
			}
		}

		/// <summary> Retrieves all related entities of type 'UserEntity' using a relation of type 'm:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiUserCollectionViaTerminal()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.UserCollection UserCollectionViaTerminal
		{
			get { return GetMultiUserCollectionViaTerminal(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for UserCollectionViaTerminal. When set to true, UserCollectionViaTerminal is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time UserCollectionViaTerminal is accessed. You can always execute a forced fetch by calling GetMultiUserCollectionViaTerminal(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchUserCollectionViaTerminal
		{
			get	{ return _alwaysFetchUserCollectionViaTerminal; }
			set	{ _alwaysFetchUserCollectionViaTerminal = value; }
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property UserCollectionViaTerminal already has been fetched. Setting this property to false when UserCollectionViaTerminal has been fetched
		/// will clear the UserCollectionViaTerminal collection well. Setting this property to true while UserCollectionViaTerminal hasn't been fetched disables lazy loading for UserCollectionViaTerminal</summary>
		[Browsable(false)]
		public bool AlreadyFetchedUserCollectionViaTerminal
		{
			get { return _alreadyFetchedUserCollectionViaTerminal;}
			set 
			{
				if(_alreadyFetchedUserCollectionViaTerminal && !value && (_userCollectionViaTerminal != null))
				{
					_userCollectionViaTerminal.Clear();
				}
				_alreadyFetchedUserCollectionViaTerminal = value;
			}
		}

		/// <summary> Gets / sets related entity of type 'CustomerEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleCustomerEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual CustomerEntity CustomerEntity
		{
			get	{ return GetSingleCustomerEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncCustomerEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "DeviceCollection", "CustomerEntity", _customerEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for CustomerEntity. When set to true, CustomerEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CustomerEntity is accessed. You can always execute a forced fetch by calling GetSingleCustomerEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCustomerEntity
		{
			get	{ return _alwaysFetchCustomerEntity; }
			set	{ _alwaysFetchCustomerEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CustomerEntity already has been fetched. Setting this property to false when CustomerEntity has been fetched
		/// will set CustomerEntity to null as well. Setting this property to true while CustomerEntity hasn't been fetched disables lazy loading for CustomerEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCustomerEntity
		{
			get { return _alreadyFetchedCustomerEntity;}
			set 
			{
				if(_alreadyFetchedCustomerEntity && !value)
				{
					this.CustomerEntity = null;
				}
				_alreadyFetchedCustomerEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property CustomerEntity is not found
		/// in the database. When set to true, CustomerEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool CustomerEntityReturnsNewIfNotFound
		{
			get	{ return _customerEntityReturnsNewIfNotFound; }
			set { _customerEntityReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the Obymobi.Data.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)Obymobi.Data.EntityType.DeviceEntity; }
		}

		#endregion

		
		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
