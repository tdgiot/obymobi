﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
using System.Runtime.Serialization;
using System.Data;
using System.Xml.Serialization;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity base class which represents the base class for the entity 'MessageTemplate'.<br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public abstract partial class MessageTemplateEntityBase : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		public new string LLBLGenProEntityName {
			get { return "MessageTemplateEntity"; }
		}
	
		#region Class Member Declarations
		private Obymobi.Data.CollectionClasses.MessageTemplateCategoryMessageTemplateCollection	_messageTemplateCategoryMessageTemplateCollection;
		private bool	_alwaysFetchMessageTemplateCategoryMessageTemplateCollection, _alreadyFetchedMessageTemplateCategoryMessageTemplateCollection;
		private Obymobi.Data.CollectionClasses.ScheduledMessageCollection	_scheduledMessageCollection;
		private bool	_alwaysFetchScheduledMessageCollection, _alreadyFetchedScheduledMessageCollection;
		private CategoryEntity _categoryEntity;
		private bool	_alwaysFetchCategoryEntity, _alreadyFetchedCategoryEntity, _categoryEntityReturnsNewIfNotFound;
		private CompanyEntity _companyEntity;
		private bool	_alwaysFetchCompanyEntity, _alreadyFetchedCompanyEntity, _companyEntityReturnsNewIfNotFound;
		private EntertainmentEntity _entertainmentEntity;
		private bool	_alwaysFetchEntertainmentEntity, _alreadyFetchedEntertainmentEntity, _entertainmentEntityReturnsNewIfNotFound;
		private MediaEntity _mediaEntity;
		private bool	_alwaysFetchMediaEntity, _alreadyFetchedMediaEntity, _mediaEntityReturnsNewIfNotFound;
		private PageEntity _pageEntity;
		private bool	_alwaysFetchPageEntity, _alreadyFetchedPageEntity, _pageEntityReturnsNewIfNotFound;
		private ProductEntity _productEntity;
		private bool	_alwaysFetchProductEntity, _alreadyFetchedProductEntity, _productEntityReturnsNewIfNotFound;
		private ProductCategoryEntity _productCategoryEntity;
		private bool	_alwaysFetchProductCategoryEntity, _alreadyFetchedProductCategoryEntity, _productCategoryEntityReturnsNewIfNotFound;
		private SiteEntity _siteEntity;
		private bool	_alwaysFetchSiteEntity, _alreadyFetchedSiteEntity, _siteEntityReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name CategoryEntity</summary>
			public static readonly string CategoryEntity = "CategoryEntity";
			/// <summary>Member name CompanyEntity</summary>
			public static readonly string CompanyEntity = "CompanyEntity";
			/// <summary>Member name EntertainmentEntity</summary>
			public static readonly string EntertainmentEntity = "EntertainmentEntity";
			/// <summary>Member name MediaEntity</summary>
			public static readonly string MediaEntity = "MediaEntity";
			/// <summary>Member name PageEntity</summary>
			public static readonly string PageEntity = "PageEntity";
			/// <summary>Member name ProductEntity</summary>
			public static readonly string ProductEntity = "ProductEntity";
			/// <summary>Member name ProductCategoryEntity</summary>
			public static readonly string ProductCategoryEntity = "ProductCategoryEntity";
			/// <summary>Member name SiteEntity</summary>
			public static readonly string SiteEntity = "SiteEntity";
			/// <summary>Member name MessageTemplateCategoryMessageTemplateCollection</summary>
			public static readonly string MessageTemplateCategoryMessageTemplateCollection = "MessageTemplateCategoryMessageTemplateCollection";
			/// <summary>Member name ScheduledMessageCollection</summary>
			public static readonly string ScheduledMessageCollection = "ScheduledMessageCollection";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static MessageTemplateEntityBase()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		protected MessageTemplateEntityBase() :base("MessageTemplateEntity")
		{
			InitClassEmpty(null);
		}

		/// <summary>CTor</summary>
		/// <param name="messageTemplateId">PK value for MessageTemplate which data should be fetched into this MessageTemplate object</param>
		protected MessageTemplateEntityBase(System.Int32 messageTemplateId):base("MessageTemplateEntity")
		{
			InitClassFetch(messageTemplateId, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="messageTemplateId">PK value for MessageTemplate which data should be fetched into this MessageTemplate object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		protected MessageTemplateEntityBase(System.Int32 messageTemplateId, IPrefetchPath prefetchPathToUse): base("MessageTemplateEntity")
		{
			InitClassFetch(messageTemplateId, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="messageTemplateId">PK value for MessageTemplate which data should be fetched into this MessageTemplate object</param>
		/// <param name="validator">The custom validator object for this MessageTemplateEntity</param>
		protected MessageTemplateEntityBase(System.Int32 messageTemplateId, IValidator validator):base("MessageTemplateEntity")
		{
			InitClassFetch(messageTemplateId, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected MessageTemplateEntityBase(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_messageTemplateCategoryMessageTemplateCollection = (Obymobi.Data.CollectionClasses.MessageTemplateCategoryMessageTemplateCollection)info.GetValue("_messageTemplateCategoryMessageTemplateCollection", typeof(Obymobi.Data.CollectionClasses.MessageTemplateCategoryMessageTemplateCollection));
			_alwaysFetchMessageTemplateCategoryMessageTemplateCollection = info.GetBoolean("_alwaysFetchMessageTemplateCategoryMessageTemplateCollection");
			_alreadyFetchedMessageTemplateCategoryMessageTemplateCollection = info.GetBoolean("_alreadyFetchedMessageTemplateCategoryMessageTemplateCollection");

			_scheduledMessageCollection = (Obymobi.Data.CollectionClasses.ScheduledMessageCollection)info.GetValue("_scheduledMessageCollection", typeof(Obymobi.Data.CollectionClasses.ScheduledMessageCollection));
			_alwaysFetchScheduledMessageCollection = info.GetBoolean("_alwaysFetchScheduledMessageCollection");
			_alreadyFetchedScheduledMessageCollection = info.GetBoolean("_alreadyFetchedScheduledMessageCollection");
			_categoryEntity = (CategoryEntity)info.GetValue("_categoryEntity", typeof(CategoryEntity));
			if(_categoryEntity!=null)
			{
				_categoryEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_categoryEntityReturnsNewIfNotFound = info.GetBoolean("_categoryEntityReturnsNewIfNotFound");
			_alwaysFetchCategoryEntity = info.GetBoolean("_alwaysFetchCategoryEntity");
			_alreadyFetchedCategoryEntity = info.GetBoolean("_alreadyFetchedCategoryEntity");

			_companyEntity = (CompanyEntity)info.GetValue("_companyEntity", typeof(CompanyEntity));
			if(_companyEntity!=null)
			{
				_companyEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_companyEntityReturnsNewIfNotFound = info.GetBoolean("_companyEntityReturnsNewIfNotFound");
			_alwaysFetchCompanyEntity = info.GetBoolean("_alwaysFetchCompanyEntity");
			_alreadyFetchedCompanyEntity = info.GetBoolean("_alreadyFetchedCompanyEntity");

			_entertainmentEntity = (EntertainmentEntity)info.GetValue("_entertainmentEntity", typeof(EntertainmentEntity));
			if(_entertainmentEntity!=null)
			{
				_entertainmentEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_entertainmentEntityReturnsNewIfNotFound = info.GetBoolean("_entertainmentEntityReturnsNewIfNotFound");
			_alwaysFetchEntertainmentEntity = info.GetBoolean("_alwaysFetchEntertainmentEntity");
			_alreadyFetchedEntertainmentEntity = info.GetBoolean("_alreadyFetchedEntertainmentEntity");

			_mediaEntity = (MediaEntity)info.GetValue("_mediaEntity", typeof(MediaEntity));
			if(_mediaEntity!=null)
			{
				_mediaEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_mediaEntityReturnsNewIfNotFound = info.GetBoolean("_mediaEntityReturnsNewIfNotFound");
			_alwaysFetchMediaEntity = info.GetBoolean("_alwaysFetchMediaEntity");
			_alreadyFetchedMediaEntity = info.GetBoolean("_alreadyFetchedMediaEntity");

			_pageEntity = (PageEntity)info.GetValue("_pageEntity", typeof(PageEntity));
			if(_pageEntity!=null)
			{
				_pageEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_pageEntityReturnsNewIfNotFound = info.GetBoolean("_pageEntityReturnsNewIfNotFound");
			_alwaysFetchPageEntity = info.GetBoolean("_alwaysFetchPageEntity");
			_alreadyFetchedPageEntity = info.GetBoolean("_alreadyFetchedPageEntity");

			_productEntity = (ProductEntity)info.GetValue("_productEntity", typeof(ProductEntity));
			if(_productEntity!=null)
			{
				_productEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_productEntityReturnsNewIfNotFound = info.GetBoolean("_productEntityReturnsNewIfNotFound");
			_alwaysFetchProductEntity = info.GetBoolean("_alwaysFetchProductEntity");
			_alreadyFetchedProductEntity = info.GetBoolean("_alreadyFetchedProductEntity");

			_productCategoryEntity = (ProductCategoryEntity)info.GetValue("_productCategoryEntity", typeof(ProductCategoryEntity));
			if(_productCategoryEntity!=null)
			{
				_productCategoryEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_productCategoryEntityReturnsNewIfNotFound = info.GetBoolean("_productCategoryEntityReturnsNewIfNotFound");
			_alwaysFetchProductCategoryEntity = info.GetBoolean("_alwaysFetchProductCategoryEntity");
			_alreadyFetchedProductCategoryEntity = info.GetBoolean("_alreadyFetchedProductCategoryEntity");

			_siteEntity = (SiteEntity)info.GetValue("_siteEntity", typeof(SiteEntity));
			if(_siteEntity!=null)
			{
				_siteEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_siteEntityReturnsNewIfNotFound = info.GetBoolean("_siteEntityReturnsNewIfNotFound");
			_alwaysFetchSiteEntity = info.GetBoolean("_alwaysFetchSiteEntity");
			_alreadyFetchedSiteEntity = info.GetBoolean("_alreadyFetchedSiteEntity");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}	
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((MessageTemplateFieldIndex)fieldIndex)
			{
				case MessageTemplateFieldIndex.CompanyId:
					DesetupSyncCompanyEntity(true, false);
					_alreadyFetchedCompanyEntity = false;
					break;
				case MessageTemplateFieldIndex.EntertainmentId:
					DesetupSyncEntertainmentEntity(true, false);
					_alreadyFetchedEntertainmentEntity = false;
					break;
				case MessageTemplateFieldIndex.MediaId:
					DesetupSyncMediaEntity(true, false);
					_alreadyFetchedMediaEntity = false;
					break;
				case MessageTemplateFieldIndex.CategoryId:
					DesetupSyncCategoryEntity(true, false);
					_alreadyFetchedCategoryEntity = false;
					break;
				case MessageTemplateFieldIndex.ProductId:
					DesetupSyncProductEntity(true, false);
					_alreadyFetchedProductEntity = false;
					break;
				case MessageTemplateFieldIndex.PageId:
					DesetupSyncPageEntity(true, false);
					_alreadyFetchedPageEntity = false;
					break;
				case MessageTemplateFieldIndex.SiteId:
					DesetupSyncSiteEntity(true, false);
					_alreadyFetchedSiteEntity = false;
					break;
				case MessageTemplateFieldIndex.ProductCategoryId:
					DesetupSyncProductCategoryEntity(true, false);
					_alreadyFetchedProductCategoryEntity = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedMessageTemplateCategoryMessageTemplateCollection = (_messageTemplateCategoryMessageTemplateCollection.Count > 0);
			_alreadyFetchedScheduledMessageCollection = (_scheduledMessageCollection.Count > 0);
			_alreadyFetchedCategoryEntity = (_categoryEntity != null);
			_alreadyFetchedCompanyEntity = (_companyEntity != null);
			_alreadyFetchedEntertainmentEntity = (_entertainmentEntity != null);
			_alreadyFetchedMediaEntity = (_mediaEntity != null);
			_alreadyFetchedPageEntity = (_pageEntity != null);
			_alreadyFetchedProductEntity = (_productEntity != null);
			_alreadyFetchedProductCategoryEntity = (_productCategoryEntity != null);
			_alreadyFetchedSiteEntity = (_siteEntity != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "CategoryEntity":
					toReturn.Add(Relations.CategoryEntityUsingCategoryId);
					break;
				case "CompanyEntity":
					toReturn.Add(Relations.CompanyEntityUsingCompanyId);
					break;
				case "EntertainmentEntity":
					toReturn.Add(Relations.EntertainmentEntityUsingEntertainmentId);
					break;
				case "MediaEntity":
					toReturn.Add(Relations.MediaEntityUsingMediaId);
					break;
				case "PageEntity":
					toReturn.Add(Relations.PageEntityUsingPageId);
					break;
				case "ProductEntity":
					toReturn.Add(Relations.ProductEntityUsingProductId);
					break;
				case "ProductCategoryEntity":
					toReturn.Add(Relations.ProductCategoryEntityUsingProductCategoryId);
					break;
				case "SiteEntity":
					toReturn.Add(Relations.SiteEntityUsingSiteId);
					break;
				case "MessageTemplateCategoryMessageTemplateCollection":
					toReturn.Add(Relations.MessageTemplateCategoryMessageTemplateEntityUsingMessageTemplateId);
					break;
				case "ScheduledMessageCollection":
					toReturn.Add(Relations.ScheduledMessageEntityUsingMessageTemplateId);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_messageTemplateCategoryMessageTemplateCollection", (!this.MarkedForDeletion?_messageTemplateCategoryMessageTemplateCollection:null));
			info.AddValue("_alwaysFetchMessageTemplateCategoryMessageTemplateCollection", _alwaysFetchMessageTemplateCategoryMessageTemplateCollection);
			info.AddValue("_alreadyFetchedMessageTemplateCategoryMessageTemplateCollection", _alreadyFetchedMessageTemplateCategoryMessageTemplateCollection);
			info.AddValue("_scheduledMessageCollection", (!this.MarkedForDeletion?_scheduledMessageCollection:null));
			info.AddValue("_alwaysFetchScheduledMessageCollection", _alwaysFetchScheduledMessageCollection);
			info.AddValue("_alreadyFetchedScheduledMessageCollection", _alreadyFetchedScheduledMessageCollection);
			info.AddValue("_categoryEntity", (!this.MarkedForDeletion?_categoryEntity:null));
			info.AddValue("_categoryEntityReturnsNewIfNotFound", _categoryEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchCategoryEntity", _alwaysFetchCategoryEntity);
			info.AddValue("_alreadyFetchedCategoryEntity", _alreadyFetchedCategoryEntity);
			info.AddValue("_companyEntity", (!this.MarkedForDeletion?_companyEntity:null));
			info.AddValue("_companyEntityReturnsNewIfNotFound", _companyEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchCompanyEntity", _alwaysFetchCompanyEntity);
			info.AddValue("_alreadyFetchedCompanyEntity", _alreadyFetchedCompanyEntity);
			info.AddValue("_entertainmentEntity", (!this.MarkedForDeletion?_entertainmentEntity:null));
			info.AddValue("_entertainmentEntityReturnsNewIfNotFound", _entertainmentEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchEntertainmentEntity", _alwaysFetchEntertainmentEntity);
			info.AddValue("_alreadyFetchedEntertainmentEntity", _alreadyFetchedEntertainmentEntity);
			info.AddValue("_mediaEntity", (!this.MarkedForDeletion?_mediaEntity:null));
			info.AddValue("_mediaEntityReturnsNewIfNotFound", _mediaEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchMediaEntity", _alwaysFetchMediaEntity);
			info.AddValue("_alreadyFetchedMediaEntity", _alreadyFetchedMediaEntity);
			info.AddValue("_pageEntity", (!this.MarkedForDeletion?_pageEntity:null));
			info.AddValue("_pageEntityReturnsNewIfNotFound", _pageEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchPageEntity", _alwaysFetchPageEntity);
			info.AddValue("_alreadyFetchedPageEntity", _alreadyFetchedPageEntity);
			info.AddValue("_productEntity", (!this.MarkedForDeletion?_productEntity:null));
			info.AddValue("_productEntityReturnsNewIfNotFound", _productEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchProductEntity", _alwaysFetchProductEntity);
			info.AddValue("_alreadyFetchedProductEntity", _alreadyFetchedProductEntity);
			info.AddValue("_productCategoryEntity", (!this.MarkedForDeletion?_productCategoryEntity:null));
			info.AddValue("_productCategoryEntityReturnsNewIfNotFound", _productCategoryEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchProductCategoryEntity", _alwaysFetchProductCategoryEntity);
			info.AddValue("_alreadyFetchedProductCategoryEntity", _alreadyFetchedProductCategoryEntity);
			info.AddValue("_siteEntity", (!this.MarkedForDeletion?_siteEntity:null));
			info.AddValue("_siteEntityReturnsNewIfNotFound", _siteEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchSiteEntity", _alwaysFetchSiteEntity);
			info.AddValue("_alreadyFetchedSiteEntity", _alreadyFetchedSiteEntity);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "CategoryEntity":
					_alreadyFetchedCategoryEntity = true;
					this.CategoryEntity = (CategoryEntity)entity;
					break;
				case "CompanyEntity":
					_alreadyFetchedCompanyEntity = true;
					this.CompanyEntity = (CompanyEntity)entity;
					break;
				case "EntertainmentEntity":
					_alreadyFetchedEntertainmentEntity = true;
					this.EntertainmentEntity = (EntertainmentEntity)entity;
					break;
				case "MediaEntity":
					_alreadyFetchedMediaEntity = true;
					this.MediaEntity = (MediaEntity)entity;
					break;
				case "PageEntity":
					_alreadyFetchedPageEntity = true;
					this.PageEntity = (PageEntity)entity;
					break;
				case "ProductEntity":
					_alreadyFetchedProductEntity = true;
					this.ProductEntity = (ProductEntity)entity;
					break;
				case "ProductCategoryEntity":
					_alreadyFetchedProductCategoryEntity = true;
					this.ProductCategoryEntity = (ProductCategoryEntity)entity;
					break;
				case "SiteEntity":
					_alreadyFetchedSiteEntity = true;
					this.SiteEntity = (SiteEntity)entity;
					break;
				case "MessageTemplateCategoryMessageTemplateCollection":
					_alreadyFetchedMessageTemplateCategoryMessageTemplateCollection = true;
					if(entity!=null)
					{
						this.MessageTemplateCategoryMessageTemplateCollection.Add((MessageTemplateCategoryMessageTemplateEntity)entity);
					}
					break;
				case "ScheduledMessageCollection":
					_alreadyFetchedScheduledMessageCollection = true;
					if(entity!=null)
					{
						this.ScheduledMessageCollection.Add((ScheduledMessageEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "CategoryEntity":
					SetupSyncCategoryEntity(relatedEntity);
					break;
				case "CompanyEntity":
					SetupSyncCompanyEntity(relatedEntity);
					break;
				case "EntertainmentEntity":
					SetupSyncEntertainmentEntity(relatedEntity);
					break;
				case "MediaEntity":
					SetupSyncMediaEntity(relatedEntity);
					break;
				case "PageEntity":
					SetupSyncPageEntity(relatedEntity);
					break;
				case "ProductEntity":
					SetupSyncProductEntity(relatedEntity);
					break;
				case "ProductCategoryEntity":
					SetupSyncProductCategoryEntity(relatedEntity);
					break;
				case "SiteEntity":
					SetupSyncSiteEntity(relatedEntity);
					break;
				case "MessageTemplateCategoryMessageTemplateCollection":
					_messageTemplateCategoryMessageTemplateCollection.Add((MessageTemplateCategoryMessageTemplateEntity)relatedEntity);
					break;
				case "ScheduledMessageCollection":
					_scheduledMessageCollection.Add((ScheduledMessageEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "CategoryEntity":
					DesetupSyncCategoryEntity(false, true);
					break;
				case "CompanyEntity":
					DesetupSyncCompanyEntity(false, true);
					break;
				case "EntertainmentEntity":
					DesetupSyncEntertainmentEntity(false, true);
					break;
				case "MediaEntity":
					DesetupSyncMediaEntity(false, true);
					break;
				case "PageEntity":
					DesetupSyncPageEntity(false, true);
					break;
				case "ProductEntity":
					DesetupSyncProductEntity(false, true);
					break;
				case "ProductCategoryEntity":
					DesetupSyncProductCategoryEntity(false, true);
					break;
				case "SiteEntity":
					DesetupSyncSiteEntity(false, true);
					break;
				case "MessageTemplateCategoryMessageTemplateCollection":
					this.PerformRelatedEntityRemoval(_messageTemplateCategoryMessageTemplateCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "ScheduledMessageCollection":
					this.PerformRelatedEntityRemoval(_scheduledMessageCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_categoryEntity!=null)
			{
				toReturn.Add(_categoryEntity);
			}
			if(_companyEntity!=null)
			{
				toReturn.Add(_companyEntity);
			}
			if(_entertainmentEntity!=null)
			{
				toReturn.Add(_entertainmentEntity);
			}
			if(_mediaEntity!=null)
			{
				toReturn.Add(_mediaEntity);
			}
			if(_pageEntity!=null)
			{
				toReturn.Add(_pageEntity);
			}
			if(_productEntity!=null)
			{
				toReturn.Add(_productEntity);
			}
			if(_productCategoryEntity!=null)
			{
				toReturn.Add(_productCategoryEntity);
			}
			if(_siteEntity!=null)
			{
				toReturn.Add(_siteEntity);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_messageTemplateCategoryMessageTemplateCollection);
			toReturn.Add(_scheduledMessageCollection);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="messageTemplateId">PK value for MessageTemplate which data should be fetched into this MessageTemplate object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 messageTemplateId)
		{
			return FetchUsingPK(messageTemplateId, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="messageTemplateId">PK value for MessageTemplate which data should be fetched into this MessageTemplate object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 messageTemplateId, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(messageTemplateId, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="messageTemplateId">PK value for MessageTemplate which data should be fetched into this MessageTemplate object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 messageTemplateId, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(messageTemplateId, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="messageTemplateId">PK value for MessageTemplate which data should be fetched into this MessageTemplate object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 messageTemplateId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(messageTemplateId, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.MessageTemplateId, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new MessageTemplateRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'MessageTemplateCategoryMessageTemplateEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'MessageTemplateCategoryMessageTemplateEntity'</returns>
		public Obymobi.Data.CollectionClasses.MessageTemplateCategoryMessageTemplateCollection GetMultiMessageTemplateCategoryMessageTemplateCollection(bool forceFetch)
		{
			return GetMultiMessageTemplateCategoryMessageTemplateCollection(forceFetch, _messageTemplateCategoryMessageTemplateCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'MessageTemplateCategoryMessageTemplateEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'MessageTemplateCategoryMessageTemplateEntity'</returns>
		public Obymobi.Data.CollectionClasses.MessageTemplateCategoryMessageTemplateCollection GetMultiMessageTemplateCategoryMessageTemplateCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiMessageTemplateCategoryMessageTemplateCollection(forceFetch, _messageTemplateCategoryMessageTemplateCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'MessageTemplateCategoryMessageTemplateEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.MessageTemplateCategoryMessageTemplateCollection GetMultiMessageTemplateCategoryMessageTemplateCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiMessageTemplateCategoryMessageTemplateCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'MessageTemplateCategoryMessageTemplateEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.MessageTemplateCategoryMessageTemplateCollection GetMultiMessageTemplateCategoryMessageTemplateCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedMessageTemplateCategoryMessageTemplateCollection || forceFetch || _alwaysFetchMessageTemplateCategoryMessageTemplateCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_messageTemplateCategoryMessageTemplateCollection);
				_messageTemplateCategoryMessageTemplateCollection.SuppressClearInGetMulti=!forceFetch;
				_messageTemplateCategoryMessageTemplateCollection.EntityFactoryToUse = entityFactoryToUse;
				_messageTemplateCategoryMessageTemplateCollection.GetMultiManyToOne(this, null, filter);
				_messageTemplateCategoryMessageTemplateCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedMessageTemplateCategoryMessageTemplateCollection = true;
			}
			return _messageTemplateCategoryMessageTemplateCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'MessageTemplateCategoryMessageTemplateCollection'. These settings will be taken into account
		/// when the property MessageTemplateCategoryMessageTemplateCollection is requested or GetMultiMessageTemplateCategoryMessageTemplateCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersMessageTemplateCategoryMessageTemplateCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_messageTemplateCategoryMessageTemplateCollection.SortClauses=sortClauses;
			_messageTemplateCategoryMessageTemplateCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'ScheduledMessageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'ScheduledMessageEntity'</returns>
		public Obymobi.Data.CollectionClasses.ScheduledMessageCollection GetMultiScheduledMessageCollection(bool forceFetch)
		{
			return GetMultiScheduledMessageCollection(forceFetch, _scheduledMessageCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ScheduledMessageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'ScheduledMessageEntity'</returns>
		public Obymobi.Data.CollectionClasses.ScheduledMessageCollection GetMultiScheduledMessageCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiScheduledMessageCollection(forceFetch, _scheduledMessageCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'ScheduledMessageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.ScheduledMessageCollection GetMultiScheduledMessageCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiScheduledMessageCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'ScheduledMessageEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.ScheduledMessageCollection GetMultiScheduledMessageCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedScheduledMessageCollection || forceFetch || _alwaysFetchScheduledMessageCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_scheduledMessageCollection);
				_scheduledMessageCollection.SuppressClearInGetMulti=!forceFetch;
				_scheduledMessageCollection.EntityFactoryToUse = entityFactoryToUse;
				_scheduledMessageCollection.GetMultiManyToOne(null, null, null, null, this, null, null, null, filter);
				_scheduledMessageCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedScheduledMessageCollection = true;
			}
			return _scheduledMessageCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'ScheduledMessageCollection'. These settings will be taken into account
		/// when the property ScheduledMessageCollection is requested or GetMultiScheduledMessageCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersScheduledMessageCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_scheduledMessageCollection.SortClauses=sortClauses;
			_scheduledMessageCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves the related entity of type 'CategoryEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'CategoryEntity' which is related to this entity.</returns>
		public CategoryEntity GetSingleCategoryEntity()
		{
			return GetSingleCategoryEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'CategoryEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'CategoryEntity' which is related to this entity.</returns>
		public virtual CategoryEntity GetSingleCategoryEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedCategoryEntity || forceFetch || _alwaysFetchCategoryEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.CategoryEntityUsingCategoryId);
				CategoryEntity newEntity = new CategoryEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.CategoryId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (CategoryEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_categoryEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.CategoryEntity = newEntity;
				_alreadyFetchedCategoryEntity = fetchResult;
			}
			return _categoryEntity;
		}


		/// <summary> Retrieves the related entity of type 'CompanyEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'CompanyEntity' which is related to this entity.</returns>
		public CompanyEntity GetSingleCompanyEntity()
		{
			return GetSingleCompanyEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'CompanyEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'CompanyEntity' which is related to this entity.</returns>
		public virtual CompanyEntity GetSingleCompanyEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedCompanyEntity || forceFetch || _alwaysFetchCompanyEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.CompanyEntityUsingCompanyId);
				CompanyEntity newEntity = new CompanyEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.CompanyId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (CompanyEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_companyEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.CompanyEntity = newEntity;
				_alreadyFetchedCompanyEntity = fetchResult;
			}
			return _companyEntity;
		}


		/// <summary> Retrieves the related entity of type 'EntertainmentEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'EntertainmentEntity' which is related to this entity.</returns>
		public EntertainmentEntity GetSingleEntertainmentEntity()
		{
			return GetSingleEntertainmentEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'EntertainmentEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'EntertainmentEntity' which is related to this entity.</returns>
		public virtual EntertainmentEntity GetSingleEntertainmentEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedEntertainmentEntity || forceFetch || _alwaysFetchEntertainmentEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.EntertainmentEntityUsingEntertainmentId);
				EntertainmentEntity newEntity = new EntertainmentEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.EntertainmentId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (EntertainmentEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_entertainmentEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.EntertainmentEntity = newEntity;
				_alreadyFetchedEntertainmentEntity = fetchResult;
			}
			return _entertainmentEntity;
		}


		/// <summary> Retrieves the related entity of type 'MediaEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'MediaEntity' which is related to this entity.</returns>
		public MediaEntity GetSingleMediaEntity()
		{
			return GetSingleMediaEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'MediaEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'MediaEntity' which is related to this entity.</returns>
		public virtual MediaEntity GetSingleMediaEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedMediaEntity || forceFetch || _alwaysFetchMediaEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.MediaEntityUsingMediaId);
				MediaEntity newEntity = new MediaEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.MediaId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (MediaEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_mediaEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.MediaEntity = newEntity;
				_alreadyFetchedMediaEntity = fetchResult;
			}
			return _mediaEntity;
		}


		/// <summary> Retrieves the related entity of type 'PageEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'PageEntity' which is related to this entity.</returns>
		public PageEntity GetSinglePageEntity()
		{
			return GetSinglePageEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'PageEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'PageEntity' which is related to this entity.</returns>
		public virtual PageEntity GetSinglePageEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedPageEntity || forceFetch || _alwaysFetchPageEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.PageEntityUsingPageId);
				PageEntity newEntity = new PageEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.PageId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (PageEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_pageEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.PageEntity = newEntity;
				_alreadyFetchedPageEntity = fetchResult;
			}
			return _pageEntity;
		}


		/// <summary> Retrieves the related entity of type 'ProductEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ProductEntity' which is related to this entity.</returns>
		public ProductEntity GetSingleProductEntity()
		{
			return GetSingleProductEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'ProductEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ProductEntity' which is related to this entity.</returns>
		public virtual ProductEntity GetSingleProductEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedProductEntity || forceFetch || _alwaysFetchProductEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ProductEntityUsingProductId);
				ProductEntity newEntity = new ProductEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ProductId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (ProductEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_productEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.ProductEntity = newEntity;
				_alreadyFetchedProductEntity = fetchResult;
			}
			return _productEntity;
		}


		/// <summary> Retrieves the related entity of type 'ProductCategoryEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ProductCategoryEntity' which is related to this entity.</returns>
		public ProductCategoryEntity GetSingleProductCategoryEntity()
		{
			return GetSingleProductCategoryEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'ProductCategoryEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ProductCategoryEntity' which is related to this entity.</returns>
		public virtual ProductCategoryEntity GetSingleProductCategoryEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedProductCategoryEntity || forceFetch || _alwaysFetchProductCategoryEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ProductCategoryEntityUsingProductCategoryId);
				ProductCategoryEntity newEntity = new ProductCategoryEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ProductCategoryId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (ProductCategoryEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_productCategoryEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.ProductCategoryEntity = newEntity;
				_alreadyFetchedProductCategoryEntity = fetchResult;
			}
			return _productCategoryEntity;
		}


		/// <summary> Retrieves the related entity of type 'SiteEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'SiteEntity' which is related to this entity.</returns>
		public SiteEntity GetSingleSiteEntity()
		{
			return GetSingleSiteEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'SiteEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'SiteEntity' which is related to this entity.</returns>
		public virtual SiteEntity GetSingleSiteEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedSiteEntity || forceFetch || _alwaysFetchSiteEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.SiteEntityUsingSiteId);
				SiteEntity newEntity = new SiteEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.SiteId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (SiteEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_siteEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.SiteEntity = newEntity;
				_alreadyFetchedSiteEntity = fetchResult;
			}
			return _siteEntity;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("CategoryEntity", _categoryEntity);
			toReturn.Add("CompanyEntity", _companyEntity);
			toReturn.Add("EntertainmentEntity", _entertainmentEntity);
			toReturn.Add("MediaEntity", _mediaEntity);
			toReturn.Add("PageEntity", _pageEntity);
			toReturn.Add("ProductEntity", _productEntity);
			toReturn.Add("ProductCategoryEntity", _productCategoryEntity);
			toReturn.Add("SiteEntity", _siteEntity);
			toReturn.Add("MessageTemplateCategoryMessageTemplateCollection", _messageTemplateCategoryMessageTemplateCollection);
			toReturn.Add("ScheduledMessageCollection", _scheduledMessageCollection);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="messageTemplateId">PK value for MessageTemplate which data should be fetched into this MessageTemplate object</param>
		/// <param name="validator">The validator object for this MessageTemplateEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int32 messageTemplateId, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(messageTemplateId, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_messageTemplateCategoryMessageTemplateCollection = new Obymobi.Data.CollectionClasses.MessageTemplateCategoryMessageTemplateCollection();
			_messageTemplateCategoryMessageTemplateCollection.SetContainingEntityInfo(this, "MessageTemplateEntity");

			_scheduledMessageCollection = new Obymobi.Data.CollectionClasses.ScheduledMessageCollection();
			_scheduledMessageCollection.SetContainingEntityInfo(this, "MessageTemplateEntity");
			_categoryEntityReturnsNewIfNotFound = true;
			_companyEntityReturnsNewIfNotFound = true;
			_entertainmentEntityReturnsNewIfNotFound = true;
			_mediaEntityReturnsNewIfNotFound = true;
			_pageEntityReturnsNewIfNotFound = true;
			_productEntityReturnsNewIfNotFound = true;
			_productCategoryEntityReturnsNewIfNotFound = true;
			_siteEntityReturnsNewIfNotFound = true;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("MessageTemplateId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Title", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Message", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CompanyId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Name", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Duration", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("EntertainmentId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("MediaId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CategoryId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ProductId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("NotifyOnYes", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Url", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PageId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SiteId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ProductCategoryId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("MessageLayoutType", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _categoryEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncCategoryEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _categoryEntity, new PropertyChangedEventHandler( OnCategoryEntityPropertyChanged ), "CategoryEntity", Obymobi.Data.RelationClasses.StaticMessageTemplateRelations.CategoryEntityUsingCategoryIdStatic, true, signalRelatedEntity, "MessageTemplateCollection", resetFKFields, new int[] { (int)MessageTemplateFieldIndex.CategoryId } );		
			_categoryEntity = null;
		}
		
		/// <summary> setups the sync logic for member _categoryEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncCategoryEntity(IEntityCore relatedEntity)
		{
			if(_categoryEntity!=relatedEntity)
			{		
				DesetupSyncCategoryEntity(true, true);
				_categoryEntity = (CategoryEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _categoryEntity, new PropertyChangedEventHandler( OnCategoryEntityPropertyChanged ), "CategoryEntity", Obymobi.Data.RelationClasses.StaticMessageTemplateRelations.CategoryEntityUsingCategoryIdStatic, true, ref _alreadyFetchedCategoryEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnCategoryEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _companyEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncCompanyEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _companyEntity, new PropertyChangedEventHandler( OnCompanyEntityPropertyChanged ), "CompanyEntity", Obymobi.Data.RelationClasses.StaticMessageTemplateRelations.CompanyEntityUsingCompanyIdStatic, true, signalRelatedEntity, "MessageTemplateCollection", resetFKFields, new int[] { (int)MessageTemplateFieldIndex.CompanyId } );		
			_companyEntity = null;
		}
		
		/// <summary> setups the sync logic for member _companyEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncCompanyEntity(IEntityCore relatedEntity)
		{
			if(_companyEntity!=relatedEntity)
			{		
				DesetupSyncCompanyEntity(true, true);
				_companyEntity = (CompanyEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _companyEntity, new PropertyChangedEventHandler( OnCompanyEntityPropertyChanged ), "CompanyEntity", Obymobi.Data.RelationClasses.StaticMessageTemplateRelations.CompanyEntityUsingCompanyIdStatic, true, ref _alreadyFetchedCompanyEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnCompanyEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _entertainmentEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncEntertainmentEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _entertainmentEntity, new PropertyChangedEventHandler( OnEntertainmentEntityPropertyChanged ), "EntertainmentEntity", Obymobi.Data.RelationClasses.StaticMessageTemplateRelations.EntertainmentEntityUsingEntertainmentIdStatic, true, signalRelatedEntity, "MessageTemplateCollection", resetFKFields, new int[] { (int)MessageTemplateFieldIndex.EntertainmentId } );		
			_entertainmentEntity = null;
		}
		
		/// <summary> setups the sync logic for member _entertainmentEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncEntertainmentEntity(IEntityCore relatedEntity)
		{
			if(_entertainmentEntity!=relatedEntity)
			{		
				DesetupSyncEntertainmentEntity(true, true);
				_entertainmentEntity = (EntertainmentEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _entertainmentEntity, new PropertyChangedEventHandler( OnEntertainmentEntityPropertyChanged ), "EntertainmentEntity", Obymobi.Data.RelationClasses.StaticMessageTemplateRelations.EntertainmentEntityUsingEntertainmentIdStatic, true, ref _alreadyFetchedEntertainmentEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnEntertainmentEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _mediaEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncMediaEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _mediaEntity, new PropertyChangedEventHandler( OnMediaEntityPropertyChanged ), "MediaEntity", Obymobi.Data.RelationClasses.StaticMessageTemplateRelations.MediaEntityUsingMediaIdStatic, true, signalRelatedEntity, "MessageTemplateCollection", resetFKFields, new int[] { (int)MessageTemplateFieldIndex.MediaId } );		
			_mediaEntity = null;
		}
		
		/// <summary> setups the sync logic for member _mediaEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncMediaEntity(IEntityCore relatedEntity)
		{
			if(_mediaEntity!=relatedEntity)
			{		
				DesetupSyncMediaEntity(true, true);
				_mediaEntity = (MediaEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _mediaEntity, new PropertyChangedEventHandler( OnMediaEntityPropertyChanged ), "MediaEntity", Obymobi.Data.RelationClasses.StaticMessageTemplateRelations.MediaEntityUsingMediaIdStatic, true, ref _alreadyFetchedMediaEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnMediaEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _pageEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncPageEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _pageEntity, new PropertyChangedEventHandler( OnPageEntityPropertyChanged ), "PageEntity", Obymobi.Data.RelationClasses.StaticMessageTemplateRelations.PageEntityUsingPageIdStatic, true, signalRelatedEntity, "MessageTemplateCollection", resetFKFields, new int[] { (int)MessageTemplateFieldIndex.PageId } );		
			_pageEntity = null;
		}
		
		/// <summary> setups the sync logic for member _pageEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncPageEntity(IEntityCore relatedEntity)
		{
			if(_pageEntity!=relatedEntity)
			{		
				DesetupSyncPageEntity(true, true);
				_pageEntity = (PageEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _pageEntity, new PropertyChangedEventHandler( OnPageEntityPropertyChanged ), "PageEntity", Obymobi.Data.RelationClasses.StaticMessageTemplateRelations.PageEntityUsingPageIdStatic, true, ref _alreadyFetchedPageEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnPageEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _productEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncProductEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _productEntity, new PropertyChangedEventHandler( OnProductEntityPropertyChanged ), "ProductEntity", Obymobi.Data.RelationClasses.StaticMessageTemplateRelations.ProductEntityUsingProductIdStatic, true, signalRelatedEntity, "MessageTemplateCollection", resetFKFields, new int[] { (int)MessageTemplateFieldIndex.ProductId } );		
			_productEntity = null;
		}
		
		/// <summary> setups the sync logic for member _productEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncProductEntity(IEntityCore relatedEntity)
		{
			if(_productEntity!=relatedEntity)
			{		
				DesetupSyncProductEntity(true, true);
				_productEntity = (ProductEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _productEntity, new PropertyChangedEventHandler( OnProductEntityPropertyChanged ), "ProductEntity", Obymobi.Data.RelationClasses.StaticMessageTemplateRelations.ProductEntityUsingProductIdStatic, true, ref _alreadyFetchedProductEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnProductEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _productCategoryEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncProductCategoryEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _productCategoryEntity, new PropertyChangedEventHandler( OnProductCategoryEntityPropertyChanged ), "ProductCategoryEntity", Obymobi.Data.RelationClasses.StaticMessageTemplateRelations.ProductCategoryEntityUsingProductCategoryIdStatic, true, signalRelatedEntity, "MessageTemplateCollection", resetFKFields, new int[] { (int)MessageTemplateFieldIndex.ProductCategoryId } );		
			_productCategoryEntity = null;
		}
		
		/// <summary> setups the sync logic for member _productCategoryEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncProductCategoryEntity(IEntityCore relatedEntity)
		{
			if(_productCategoryEntity!=relatedEntity)
			{		
				DesetupSyncProductCategoryEntity(true, true);
				_productCategoryEntity = (ProductCategoryEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _productCategoryEntity, new PropertyChangedEventHandler( OnProductCategoryEntityPropertyChanged ), "ProductCategoryEntity", Obymobi.Data.RelationClasses.StaticMessageTemplateRelations.ProductCategoryEntityUsingProductCategoryIdStatic, true, ref _alreadyFetchedProductCategoryEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnProductCategoryEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _siteEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncSiteEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _siteEntity, new PropertyChangedEventHandler( OnSiteEntityPropertyChanged ), "SiteEntity", Obymobi.Data.RelationClasses.StaticMessageTemplateRelations.SiteEntityUsingSiteIdStatic, true, signalRelatedEntity, "MessageTemplateCollection", resetFKFields, new int[] { (int)MessageTemplateFieldIndex.SiteId } );		
			_siteEntity = null;
		}
		
		/// <summary> setups the sync logic for member _siteEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncSiteEntity(IEntityCore relatedEntity)
		{
			if(_siteEntity!=relatedEntity)
			{		
				DesetupSyncSiteEntity(true, true);
				_siteEntity = (SiteEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _siteEntity, new PropertyChangedEventHandler( OnSiteEntityPropertyChanged ), "SiteEntity", Obymobi.Data.RelationClasses.StaticMessageTemplateRelations.SiteEntityUsingSiteIdStatic, true, ref _alreadyFetchedSiteEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnSiteEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="messageTemplateId">PK value for MessageTemplate which data should be fetched into this MessageTemplate object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int32 messageTemplateId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)MessageTemplateFieldIndex.MessageTemplateId].ForcedCurrentValueWrite(messageTemplateId);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateMessageTemplateDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new MessageTemplateEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static MessageTemplateRelations Relations
		{
			get	{ return new MessageTemplateRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'MessageTemplateCategoryMessageTemplate' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathMessageTemplateCategoryMessageTemplateCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.MessageTemplateCategoryMessageTemplateCollection(), (IEntityRelation)GetRelationsForField("MessageTemplateCategoryMessageTemplateCollection")[0], (int)Obymobi.Data.EntityType.MessageTemplateEntity, (int)Obymobi.Data.EntityType.MessageTemplateCategoryMessageTemplateEntity, 0, null, null, null, "MessageTemplateCategoryMessageTemplateCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ScheduledMessage' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathScheduledMessageCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ScheduledMessageCollection(), (IEntityRelation)GetRelationsForField("ScheduledMessageCollection")[0], (int)Obymobi.Data.EntityType.MessageTemplateEntity, (int)Obymobi.Data.EntityType.ScheduledMessageEntity, 0, null, null, null, "ScheduledMessageCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Category'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCategoryEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CategoryCollection(), (IEntityRelation)GetRelationsForField("CategoryEntity")[0], (int)Obymobi.Data.EntityType.MessageTemplateEntity, (int)Obymobi.Data.EntityType.CategoryEntity, 0, null, null, null, "CategoryEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Company'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCompanyEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CompanyCollection(), (IEntityRelation)GetRelationsForField("CompanyEntity")[0], (int)Obymobi.Data.EntityType.MessageTemplateEntity, (int)Obymobi.Data.EntityType.CompanyEntity, 0, null, null, null, "CompanyEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Entertainment'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathEntertainmentEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.EntertainmentCollection(), (IEntityRelation)GetRelationsForField("EntertainmentEntity")[0], (int)Obymobi.Data.EntityType.MessageTemplateEntity, (int)Obymobi.Data.EntityType.EntertainmentEntity, 0, null, null, null, "EntertainmentEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Media'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathMediaEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.MediaCollection(), (IEntityRelation)GetRelationsForField("MediaEntity")[0], (int)Obymobi.Data.EntityType.MessageTemplateEntity, (int)Obymobi.Data.EntityType.MediaEntity, 0, null, null, null, "MediaEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Page'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathPageEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.PageCollection(), (IEntityRelation)GetRelationsForField("PageEntity")[0], (int)Obymobi.Data.EntityType.MessageTemplateEntity, (int)Obymobi.Data.EntityType.PageEntity, 0, null, null, null, "PageEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Product'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathProductEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ProductCollection(), (IEntityRelation)GetRelationsForField("ProductEntity")[0], (int)Obymobi.Data.EntityType.MessageTemplateEntity, (int)Obymobi.Data.EntityType.ProductEntity, 0, null, null, null, "ProductEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ProductCategory'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathProductCategoryEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ProductCategoryCollection(), (IEntityRelation)GetRelationsForField("ProductCategoryEntity")[0], (int)Obymobi.Data.EntityType.MessageTemplateEntity, (int)Obymobi.Data.EntityType.ProductCategoryEntity, 0, null, null, null, "ProductCategoryEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Site'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathSiteEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.SiteCollection(), (IEntityRelation)GetRelationsForField("SiteEntity")[0], (int)Obymobi.Data.EntityType.MessageTemplateEntity, (int)Obymobi.Data.EntityType.SiteEntity, 0, null, null, null, "SiteEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The MessageTemplateId property of the Entity MessageTemplate<br/><br/></summary>
		/// <remarks>Mapped on  table field: "MessageTemplate"."MessageTemplateId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 MessageTemplateId
		{
			get { return (System.Int32)GetValue((int)MessageTemplateFieldIndex.MessageTemplateId, true); }
			set	{ SetValue((int)MessageTemplateFieldIndex.MessageTemplateId, value, true); }
		}

		/// <summary> The Title property of the Entity MessageTemplate<br/><br/></summary>
		/// <remarks>Mapped on  table field: "MessageTemplate"."Title"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 255<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Title
		{
			get { return (System.String)GetValue((int)MessageTemplateFieldIndex.Title, true); }
			set	{ SetValue((int)MessageTemplateFieldIndex.Title, value, true); }
		}

		/// <summary> The Message property of the Entity MessageTemplate<br/><br/></summary>
		/// <remarks>Mapped on  table field: "MessageTemplate"."Message"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Message
		{
			get { return (System.String)GetValue((int)MessageTemplateFieldIndex.Message, true); }
			set	{ SetValue((int)MessageTemplateFieldIndex.Message, value, true); }
		}

		/// <summary> The CompanyId property of the Entity MessageTemplate<br/><br/></summary>
		/// <remarks>Mapped on  table field: "MessageTemplate"."CompanyId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> CompanyId
		{
			get { return (Nullable<System.Int32>)GetValue((int)MessageTemplateFieldIndex.CompanyId, false); }
			set	{ SetValue((int)MessageTemplateFieldIndex.CompanyId, value, true); }
		}

		/// <summary> The CreatedBy property of the Entity MessageTemplate<br/><br/></summary>
		/// <remarks>Mapped on  table field: "MessageTemplate"."CreatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CreatedBy
		{
			get { return (System.Int32)GetValue((int)MessageTemplateFieldIndex.CreatedBy, true); }
			set	{ SetValue((int)MessageTemplateFieldIndex.CreatedBy, value, true); }
		}

		/// <summary> The UpdatedBy property of the Entity MessageTemplate<br/><br/></summary>
		/// <remarks>Mapped on  table field: "MessageTemplate"."UpdatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 UpdatedBy
		{
			get { return (System.Int32)GetValue((int)MessageTemplateFieldIndex.UpdatedBy, true); }
			set	{ SetValue((int)MessageTemplateFieldIndex.UpdatedBy, value, true); }
		}

		/// <summary> The Name property of the Entity MessageTemplate<br/><br/></summary>
		/// <remarks>Mapped on  table field: "MessageTemplate"."Name"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Name
		{
			get { return (System.String)GetValue((int)MessageTemplateFieldIndex.Name, true); }
			set	{ SetValue((int)MessageTemplateFieldIndex.Name, value, true); }
		}

		/// <summary> The Duration property of the Entity MessageTemplate<br/><br/></summary>
		/// <remarks>Mapped on  table field: "MessageTemplate"."Duration"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 Duration
		{
			get { return (System.Int32)GetValue((int)MessageTemplateFieldIndex.Duration, true); }
			set	{ SetValue((int)MessageTemplateFieldIndex.Duration, value, true); }
		}

		/// <summary> The EntertainmentId property of the Entity MessageTemplate<br/><br/></summary>
		/// <remarks>Mapped on  table field: "MessageTemplate"."EntertainmentId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> EntertainmentId
		{
			get { return (Nullable<System.Int32>)GetValue((int)MessageTemplateFieldIndex.EntertainmentId, false); }
			set	{ SetValue((int)MessageTemplateFieldIndex.EntertainmentId, value, true); }
		}

		/// <summary> The MediaId property of the Entity MessageTemplate<br/><br/></summary>
		/// <remarks>Mapped on  table field: "MessageTemplate"."MediaId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> MediaId
		{
			get { return (Nullable<System.Int32>)GetValue((int)MessageTemplateFieldIndex.MediaId, false); }
			set	{ SetValue((int)MessageTemplateFieldIndex.MediaId, value, true); }
		}

		/// <summary> The CategoryId property of the Entity MessageTemplate<br/><br/></summary>
		/// <remarks>Mapped on  table field: "MessageTemplate"."CategoryId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> CategoryId
		{
			get { return (Nullable<System.Int32>)GetValue((int)MessageTemplateFieldIndex.CategoryId, false); }
			set	{ SetValue((int)MessageTemplateFieldIndex.CategoryId, value, true); }
		}

		/// <summary> The ProductId property of the Entity MessageTemplate<br/><br/></summary>
		/// <remarks>Mapped on  table field: "MessageTemplate"."ProductId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ProductId
		{
			get { return (Nullable<System.Int32>)GetValue((int)MessageTemplateFieldIndex.ProductId, false); }
			set	{ SetValue((int)MessageTemplateFieldIndex.ProductId, value, true); }
		}

		/// <summary> The NotifyOnYes property of the Entity MessageTemplate<br/><br/></summary>
		/// <remarks>Mapped on  table field: "MessageTemplate"."NotifyOnYes"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Boolean> NotifyOnYes
		{
			get { return (Nullable<System.Boolean>)GetValue((int)MessageTemplateFieldIndex.NotifyOnYes, false); }
			set	{ SetValue((int)MessageTemplateFieldIndex.NotifyOnYes, value, true); }
		}

		/// <summary> The CreatedUTC property of the Entity MessageTemplate<br/><br/></summary>
		/// <remarks>Mapped on  table field: "MessageTemplate"."CreatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CreatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)MessageTemplateFieldIndex.CreatedUTC, false); }
			set	{ SetValue((int)MessageTemplateFieldIndex.CreatedUTC, value, true); }
		}

		/// <summary> The UpdatedUTC property of the Entity MessageTemplate<br/><br/></summary>
		/// <remarks>Mapped on  table field: "MessageTemplate"."UpdatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)MessageTemplateFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)MessageTemplateFieldIndex.UpdatedUTC, value, true); }
		}

		/// <summary> The Url property of the Entity MessageTemplate<br/><br/></summary>
		/// <remarks>Mapped on  table field: "MessageTemplate"."Url"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Url
		{
			get { return (System.String)GetValue((int)MessageTemplateFieldIndex.Url, true); }
			set	{ SetValue((int)MessageTemplateFieldIndex.Url, value, true); }
		}

		/// <summary> The PageId property of the Entity MessageTemplate<br/><br/></summary>
		/// <remarks>Mapped on  table field: "MessageTemplate"."PageId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> PageId
		{
			get { return (Nullable<System.Int32>)GetValue((int)MessageTemplateFieldIndex.PageId, false); }
			set	{ SetValue((int)MessageTemplateFieldIndex.PageId, value, true); }
		}

		/// <summary> The SiteId property of the Entity MessageTemplate<br/><br/></summary>
		/// <remarks>Mapped on  table field: "MessageTemplate"."SiteId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> SiteId
		{
			get { return (Nullable<System.Int32>)GetValue((int)MessageTemplateFieldIndex.SiteId, false); }
			set	{ SetValue((int)MessageTemplateFieldIndex.SiteId, value, true); }
		}

		/// <summary> The ProductCategoryId property of the Entity MessageTemplate<br/><br/></summary>
		/// <remarks>Mapped on  table field: "MessageTemplate"."ProductCategoryId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ProductCategoryId
		{
			get { return (Nullable<System.Int32>)GetValue((int)MessageTemplateFieldIndex.ProductCategoryId, false); }
			set	{ SetValue((int)MessageTemplateFieldIndex.ProductCategoryId, value, true); }
		}

		/// <summary> The MessageLayoutType property of the Entity MessageTemplate<br/><br/></summary>
		/// <remarks>Mapped on  table field: "MessageTemplate"."MessageLayoutType"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual Obymobi.Enums.MessageLayoutType MessageLayoutType
		{
			get { return (Obymobi.Enums.MessageLayoutType)GetValue((int)MessageTemplateFieldIndex.MessageLayoutType, true); }
			set	{ SetValue((int)MessageTemplateFieldIndex.MessageLayoutType, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'MessageTemplateCategoryMessageTemplateEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiMessageTemplateCategoryMessageTemplateCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.MessageTemplateCategoryMessageTemplateCollection MessageTemplateCategoryMessageTemplateCollection
		{
			get	{ return GetMultiMessageTemplateCategoryMessageTemplateCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for MessageTemplateCategoryMessageTemplateCollection. When set to true, MessageTemplateCategoryMessageTemplateCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time MessageTemplateCategoryMessageTemplateCollection is accessed. You can always execute/ a forced fetch by calling GetMultiMessageTemplateCategoryMessageTemplateCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchMessageTemplateCategoryMessageTemplateCollection
		{
			get	{ return _alwaysFetchMessageTemplateCategoryMessageTemplateCollection; }
			set	{ _alwaysFetchMessageTemplateCategoryMessageTemplateCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property MessageTemplateCategoryMessageTemplateCollection already has been fetched. Setting this property to false when MessageTemplateCategoryMessageTemplateCollection has been fetched
		/// will clear the MessageTemplateCategoryMessageTemplateCollection collection well. Setting this property to true while MessageTemplateCategoryMessageTemplateCollection hasn't been fetched disables lazy loading for MessageTemplateCategoryMessageTemplateCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedMessageTemplateCategoryMessageTemplateCollection
		{
			get { return _alreadyFetchedMessageTemplateCategoryMessageTemplateCollection;}
			set 
			{
				if(_alreadyFetchedMessageTemplateCategoryMessageTemplateCollection && !value && (_messageTemplateCategoryMessageTemplateCollection != null))
				{
					_messageTemplateCategoryMessageTemplateCollection.Clear();
				}
				_alreadyFetchedMessageTemplateCategoryMessageTemplateCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'ScheduledMessageEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiScheduledMessageCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.ScheduledMessageCollection ScheduledMessageCollection
		{
			get	{ return GetMultiScheduledMessageCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for ScheduledMessageCollection. When set to true, ScheduledMessageCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ScheduledMessageCollection is accessed. You can always execute/ a forced fetch by calling GetMultiScheduledMessageCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchScheduledMessageCollection
		{
			get	{ return _alwaysFetchScheduledMessageCollection; }
			set	{ _alwaysFetchScheduledMessageCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property ScheduledMessageCollection already has been fetched. Setting this property to false when ScheduledMessageCollection has been fetched
		/// will clear the ScheduledMessageCollection collection well. Setting this property to true while ScheduledMessageCollection hasn't been fetched disables lazy loading for ScheduledMessageCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedScheduledMessageCollection
		{
			get { return _alreadyFetchedScheduledMessageCollection;}
			set 
			{
				if(_alreadyFetchedScheduledMessageCollection && !value && (_scheduledMessageCollection != null))
				{
					_scheduledMessageCollection.Clear();
				}
				_alreadyFetchedScheduledMessageCollection = value;
			}
		}

		/// <summary> Gets / sets related entity of type 'CategoryEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleCategoryEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual CategoryEntity CategoryEntity
		{
			get	{ return GetSingleCategoryEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncCategoryEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "MessageTemplateCollection", "CategoryEntity", _categoryEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for CategoryEntity. When set to true, CategoryEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CategoryEntity is accessed. You can always execute a forced fetch by calling GetSingleCategoryEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCategoryEntity
		{
			get	{ return _alwaysFetchCategoryEntity; }
			set	{ _alwaysFetchCategoryEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CategoryEntity already has been fetched. Setting this property to false when CategoryEntity has been fetched
		/// will set CategoryEntity to null as well. Setting this property to true while CategoryEntity hasn't been fetched disables lazy loading for CategoryEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCategoryEntity
		{
			get { return _alreadyFetchedCategoryEntity;}
			set 
			{
				if(_alreadyFetchedCategoryEntity && !value)
				{
					this.CategoryEntity = null;
				}
				_alreadyFetchedCategoryEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property CategoryEntity is not found
		/// in the database. When set to true, CategoryEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool CategoryEntityReturnsNewIfNotFound
		{
			get	{ return _categoryEntityReturnsNewIfNotFound; }
			set { _categoryEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'CompanyEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleCompanyEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual CompanyEntity CompanyEntity
		{
			get	{ return GetSingleCompanyEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncCompanyEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "MessageTemplateCollection", "CompanyEntity", _companyEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for CompanyEntity. When set to true, CompanyEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CompanyEntity is accessed. You can always execute a forced fetch by calling GetSingleCompanyEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCompanyEntity
		{
			get	{ return _alwaysFetchCompanyEntity; }
			set	{ _alwaysFetchCompanyEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CompanyEntity already has been fetched. Setting this property to false when CompanyEntity has been fetched
		/// will set CompanyEntity to null as well. Setting this property to true while CompanyEntity hasn't been fetched disables lazy loading for CompanyEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCompanyEntity
		{
			get { return _alreadyFetchedCompanyEntity;}
			set 
			{
				if(_alreadyFetchedCompanyEntity && !value)
				{
					this.CompanyEntity = null;
				}
				_alreadyFetchedCompanyEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property CompanyEntity is not found
		/// in the database. When set to true, CompanyEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool CompanyEntityReturnsNewIfNotFound
		{
			get	{ return _companyEntityReturnsNewIfNotFound; }
			set { _companyEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'EntertainmentEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleEntertainmentEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual EntertainmentEntity EntertainmentEntity
		{
			get	{ return GetSingleEntertainmentEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncEntertainmentEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "MessageTemplateCollection", "EntertainmentEntity", _entertainmentEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for EntertainmentEntity. When set to true, EntertainmentEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time EntertainmentEntity is accessed. You can always execute a forced fetch by calling GetSingleEntertainmentEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchEntertainmentEntity
		{
			get	{ return _alwaysFetchEntertainmentEntity; }
			set	{ _alwaysFetchEntertainmentEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property EntertainmentEntity already has been fetched. Setting this property to false when EntertainmentEntity has been fetched
		/// will set EntertainmentEntity to null as well. Setting this property to true while EntertainmentEntity hasn't been fetched disables lazy loading for EntertainmentEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedEntertainmentEntity
		{
			get { return _alreadyFetchedEntertainmentEntity;}
			set 
			{
				if(_alreadyFetchedEntertainmentEntity && !value)
				{
					this.EntertainmentEntity = null;
				}
				_alreadyFetchedEntertainmentEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property EntertainmentEntity is not found
		/// in the database. When set to true, EntertainmentEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool EntertainmentEntityReturnsNewIfNotFound
		{
			get	{ return _entertainmentEntityReturnsNewIfNotFound; }
			set { _entertainmentEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'MediaEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleMediaEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual MediaEntity MediaEntity
		{
			get	{ return GetSingleMediaEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncMediaEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "MessageTemplateCollection", "MediaEntity", _mediaEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for MediaEntity. When set to true, MediaEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time MediaEntity is accessed. You can always execute a forced fetch by calling GetSingleMediaEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchMediaEntity
		{
			get	{ return _alwaysFetchMediaEntity; }
			set	{ _alwaysFetchMediaEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property MediaEntity already has been fetched. Setting this property to false when MediaEntity has been fetched
		/// will set MediaEntity to null as well. Setting this property to true while MediaEntity hasn't been fetched disables lazy loading for MediaEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedMediaEntity
		{
			get { return _alreadyFetchedMediaEntity;}
			set 
			{
				if(_alreadyFetchedMediaEntity && !value)
				{
					this.MediaEntity = null;
				}
				_alreadyFetchedMediaEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property MediaEntity is not found
		/// in the database. When set to true, MediaEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool MediaEntityReturnsNewIfNotFound
		{
			get	{ return _mediaEntityReturnsNewIfNotFound; }
			set { _mediaEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'PageEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSinglePageEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual PageEntity PageEntity
		{
			get	{ return GetSinglePageEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncPageEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "MessageTemplateCollection", "PageEntity", _pageEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for PageEntity. When set to true, PageEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time PageEntity is accessed. You can always execute a forced fetch by calling GetSinglePageEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchPageEntity
		{
			get	{ return _alwaysFetchPageEntity; }
			set	{ _alwaysFetchPageEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property PageEntity already has been fetched. Setting this property to false when PageEntity has been fetched
		/// will set PageEntity to null as well. Setting this property to true while PageEntity hasn't been fetched disables lazy loading for PageEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedPageEntity
		{
			get { return _alreadyFetchedPageEntity;}
			set 
			{
				if(_alreadyFetchedPageEntity && !value)
				{
					this.PageEntity = null;
				}
				_alreadyFetchedPageEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property PageEntity is not found
		/// in the database. When set to true, PageEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool PageEntityReturnsNewIfNotFound
		{
			get	{ return _pageEntityReturnsNewIfNotFound; }
			set { _pageEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'ProductEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleProductEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual ProductEntity ProductEntity
		{
			get	{ return GetSingleProductEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncProductEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "MessageTemplateCollection", "ProductEntity", _productEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for ProductEntity. When set to true, ProductEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ProductEntity is accessed. You can always execute a forced fetch by calling GetSingleProductEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchProductEntity
		{
			get	{ return _alwaysFetchProductEntity; }
			set	{ _alwaysFetchProductEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ProductEntity already has been fetched. Setting this property to false when ProductEntity has been fetched
		/// will set ProductEntity to null as well. Setting this property to true while ProductEntity hasn't been fetched disables lazy loading for ProductEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedProductEntity
		{
			get { return _alreadyFetchedProductEntity;}
			set 
			{
				if(_alreadyFetchedProductEntity && !value)
				{
					this.ProductEntity = null;
				}
				_alreadyFetchedProductEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property ProductEntity is not found
		/// in the database. When set to true, ProductEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool ProductEntityReturnsNewIfNotFound
		{
			get	{ return _productEntityReturnsNewIfNotFound; }
			set { _productEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'ProductCategoryEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleProductCategoryEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual ProductCategoryEntity ProductCategoryEntity
		{
			get	{ return GetSingleProductCategoryEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncProductCategoryEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "MessageTemplateCollection", "ProductCategoryEntity", _productCategoryEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for ProductCategoryEntity. When set to true, ProductCategoryEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ProductCategoryEntity is accessed. You can always execute a forced fetch by calling GetSingleProductCategoryEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchProductCategoryEntity
		{
			get	{ return _alwaysFetchProductCategoryEntity; }
			set	{ _alwaysFetchProductCategoryEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ProductCategoryEntity already has been fetched. Setting this property to false when ProductCategoryEntity has been fetched
		/// will set ProductCategoryEntity to null as well. Setting this property to true while ProductCategoryEntity hasn't been fetched disables lazy loading for ProductCategoryEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedProductCategoryEntity
		{
			get { return _alreadyFetchedProductCategoryEntity;}
			set 
			{
				if(_alreadyFetchedProductCategoryEntity && !value)
				{
					this.ProductCategoryEntity = null;
				}
				_alreadyFetchedProductCategoryEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property ProductCategoryEntity is not found
		/// in the database. When set to true, ProductCategoryEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool ProductCategoryEntityReturnsNewIfNotFound
		{
			get	{ return _productCategoryEntityReturnsNewIfNotFound; }
			set { _productCategoryEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'SiteEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleSiteEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual SiteEntity SiteEntity
		{
			get	{ return GetSingleSiteEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncSiteEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "MessageTemplateCollection", "SiteEntity", _siteEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for SiteEntity. When set to true, SiteEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time SiteEntity is accessed. You can always execute a forced fetch by calling GetSingleSiteEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchSiteEntity
		{
			get	{ return _alwaysFetchSiteEntity; }
			set	{ _alwaysFetchSiteEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property SiteEntity already has been fetched. Setting this property to false when SiteEntity has been fetched
		/// will set SiteEntity to null as well. Setting this property to true while SiteEntity hasn't been fetched disables lazy loading for SiteEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedSiteEntity
		{
			get { return _alreadyFetchedSiteEntity;}
			set 
			{
				if(_alreadyFetchedSiteEntity && !value)
				{
					this.SiteEntity = null;
				}
				_alreadyFetchedSiteEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property SiteEntity is not found
		/// in the database. When set to true, SiteEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool SiteEntityReturnsNewIfNotFound
		{
			get	{ return _siteEntityReturnsNewIfNotFound; }
			set { _siteEntityReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the Obymobi.Data.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)Obymobi.Data.EntityType.MessageTemplateEntity; }
		}

		#endregion

		
		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
