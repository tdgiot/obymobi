﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
using System.Runtime.Serialization;
using System.Data;
using System.Xml.Serialization;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity base class which represents the base class for the entity 'SurveyResult'.<br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public abstract partial class SurveyResultEntityBase : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		public new string LLBLGenProEntityName {
			get { return "SurveyResultEntity"; }
		}
	
		#region Class Member Declarations
		private ClientEntity _clientEntity;
		private bool	_alwaysFetchClientEntity, _alreadyFetchedClientEntity, _clientEntityReturnsNewIfNotFound;
		private CustomerEntity _customerEntity;
		private bool	_alwaysFetchCustomerEntity, _alreadyFetchedCustomerEntity, _customerEntityReturnsNewIfNotFound;
		private SurveyAnswerEntity _surveyAnswerEntity;
		private bool	_alwaysFetchSurveyAnswerEntity, _alreadyFetchedSurveyAnswerEntity, _surveyAnswerEntityReturnsNewIfNotFound;
		private SurveyQuestionEntity _surveyQuestionEntity;
		private bool	_alwaysFetchSurveyQuestionEntity, _alreadyFetchedSurveyQuestionEntity, _surveyQuestionEntityReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name ClientEntity</summary>
			public static readonly string ClientEntity = "ClientEntity";
			/// <summary>Member name CustomerEntity</summary>
			public static readonly string CustomerEntity = "CustomerEntity";
			/// <summary>Member name SurveyAnswerEntity</summary>
			public static readonly string SurveyAnswerEntity = "SurveyAnswerEntity";
			/// <summary>Member name SurveyQuestionEntity</summary>
			public static readonly string SurveyQuestionEntity = "SurveyQuestionEntity";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static SurveyResultEntityBase()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		protected SurveyResultEntityBase() :base("SurveyResultEntity")
		{
			InitClassEmpty(null);
		}

		/// <summary>CTor</summary>
		/// <param name="surveyResultId">PK value for SurveyResult which data should be fetched into this SurveyResult object</param>
		protected SurveyResultEntityBase(System.Int32 surveyResultId):base("SurveyResultEntity")
		{
			InitClassFetch(surveyResultId, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="surveyResultId">PK value for SurveyResult which data should be fetched into this SurveyResult object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		protected SurveyResultEntityBase(System.Int32 surveyResultId, IPrefetchPath prefetchPathToUse): base("SurveyResultEntity")
		{
			InitClassFetch(surveyResultId, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="surveyResultId">PK value for SurveyResult which data should be fetched into this SurveyResult object</param>
		/// <param name="validator">The custom validator object for this SurveyResultEntity</param>
		protected SurveyResultEntityBase(System.Int32 surveyResultId, IValidator validator):base("SurveyResultEntity")
		{
			InitClassFetch(surveyResultId, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected SurveyResultEntityBase(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_clientEntity = (ClientEntity)info.GetValue("_clientEntity", typeof(ClientEntity));
			if(_clientEntity!=null)
			{
				_clientEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_clientEntityReturnsNewIfNotFound = info.GetBoolean("_clientEntityReturnsNewIfNotFound");
			_alwaysFetchClientEntity = info.GetBoolean("_alwaysFetchClientEntity");
			_alreadyFetchedClientEntity = info.GetBoolean("_alreadyFetchedClientEntity");

			_customerEntity = (CustomerEntity)info.GetValue("_customerEntity", typeof(CustomerEntity));
			if(_customerEntity!=null)
			{
				_customerEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_customerEntityReturnsNewIfNotFound = info.GetBoolean("_customerEntityReturnsNewIfNotFound");
			_alwaysFetchCustomerEntity = info.GetBoolean("_alwaysFetchCustomerEntity");
			_alreadyFetchedCustomerEntity = info.GetBoolean("_alreadyFetchedCustomerEntity");

			_surveyAnswerEntity = (SurveyAnswerEntity)info.GetValue("_surveyAnswerEntity", typeof(SurveyAnswerEntity));
			if(_surveyAnswerEntity!=null)
			{
				_surveyAnswerEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_surveyAnswerEntityReturnsNewIfNotFound = info.GetBoolean("_surveyAnswerEntityReturnsNewIfNotFound");
			_alwaysFetchSurveyAnswerEntity = info.GetBoolean("_alwaysFetchSurveyAnswerEntity");
			_alreadyFetchedSurveyAnswerEntity = info.GetBoolean("_alreadyFetchedSurveyAnswerEntity");

			_surveyQuestionEntity = (SurveyQuestionEntity)info.GetValue("_surveyQuestionEntity", typeof(SurveyQuestionEntity));
			if(_surveyQuestionEntity!=null)
			{
				_surveyQuestionEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_surveyQuestionEntityReturnsNewIfNotFound = info.GetBoolean("_surveyQuestionEntityReturnsNewIfNotFound");
			_alwaysFetchSurveyQuestionEntity = info.GetBoolean("_alwaysFetchSurveyQuestionEntity");
			_alreadyFetchedSurveyQuestionEntity = info.GetBoolean("_alreadyFetchedSurveyQuestionEntity");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}	
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((SurveyResultFieldIndex)fieldIndex)
			{
				case SurveyResultFieldIndex.SurveyAnswerId:
					DesetupSyncSurveyAnswerEntity(true, false);
					_alreadyFetchedSurveyAnswerEntity = false;
					break;
				case SurveyResultFieldIndex.SurveyQuestionId:
					DesetupSyncSurveyQuestionEntity(true, false);
					_alreadyFetchedSurveyQuestionEntity = false;
					break;
				case SurveyResultFieldIndex.CustomerId:
					DesetupSyncCustomerEntity(true, false);
					_alreadyFetchedCustomerEntity = false;
					break;
				case SurveyResultFieldIndex.ClientId:
					DesetupSyncClientEntity(true, false);
					_alreadyFetchedClientEntity = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedClientEntity = (_clientEntity != null);
			_alreadyFetchedCustomerEntity = (_customerEntity != null);
			_alreadyFetchedSurveyAnswerEntity = (_surveyAnswerEntity != null);
			_alreadyFetchedSurveyQuestionEntity = (_surveyQuestionEntity != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "ClientEntity":
					toReturn.Add(Relations.ClientEntityUsingClientId);
					break;
				case "CustomerEntity":
					toReturn.Add(Relations.CustomerEntityUsingCustomerId);
					break;
				case "SurveyAnswerEntity":
					toReturn.Add(Relations.SurveyAnswerEntityUsingSurveyAnswerId);
					break;
				case "SurveyQuestionEntity":
					toReturn.Add(Relations.SurveyQuestionEntityUsingSurveyQuestionId);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_clientEntity", (!this.MarkedForDeletion?_clientEntity:null));
			info.AddValue("_clientEntityReturnsNewIfNotFound", _clientEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchClientEntity", _alwaysFetchClientEntity);
			info.AddValue("_alreadyFetchedClientEntity", _alreadyFetchedClientEntity);
			info.AddValue("_customerEntity", (!this.MarkedForDeletion?_customerEntity:null));
			info.AddValue("_customerEntityReturnsNewIfNotFound", _customerEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchCustomerEntity", _alwaysFetchCustomerEntity);
			info.AddValue("_alreadyFetchedCustomerEntity", _alreadyFetchedCustomerEntity);
			info.AddValue("_surveyAnswerEntity", (!this.MarkedForDeletion?_surveyAnswerEntity:null));
			info.AddValue("_surveyAnswerEntityReturnsNewIfNotFound", _surveyAnswerEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchSurveyAnswerEntity", _alwaysFetchSurveyAnswerEntity);
			info.AddValue("_alreadyFetchedSurveyAnswerEntity", _alreadyFetchedSurveyAnswerEntity);
			info.AddValue("_surveyQuestionEntity", (!this.MarkedForDeletion?_surveyQuestionEntity:null));
			info.AddValue("_surveyQuestionEntityReturnsNewIfNotFound", _surveyQuestionEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchSurveyQuestionEntity", _alwaysFetchSurveyQuestionEntity);
			info.AddValue("_alreadyFetchedSurveyQuestionEntity", _alreadyFetchedSurveyQuestionEntity);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "ClientEntity":
					_alreadyFetchedClientEntity = true;
					this.ClientEntity = (ClientEntity)entity;
					break;
				case "CustomerEntity":
					_alreadyFetchedCustomerEntity = true;
					this.CustomerEntity = (CustomerEntity)entity;
					break;
				case "SurveyAnswerEntity":
					_alreadyFetchedSurveyAnswerEntity = true;
					this.SurveyAnswerEntity = (SurveyAnswerEntity)entity;
					break;
				case "SurveyQuestionEntity":
					_alreadyFetchedSurveyQuestionEntity = true;
					this.SurveyQuestionEntity = (SurveyQuestionEntity)entity;
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "ClientEntity":
					SetupSyncClientEntity(relatedEntity);
					break;
				case "CustomerEntity":
					SetupSyncCustomerEntity(relatedEntity);
					break;
				case "SurveyAnswerEntity":
					SetupSyncSurveyAnswerEntity(relatedEntity);
					break;
				case "SurveyQuestionEntity":
					SetupSyncSurveyQuestionEntity(relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "ClientEntity":
					DesetupSyncClientEntity(false, true);
					break;
				case "CustomerEntity":
					DesetupSyncCustomerEntity(false, true);
					break;
				case "SurveyAnswerEntity":
					DesetupSyncSurveyAnswerEntity(false, true);
					break;
				case "SurveyQuestionEntity":
					DesetupSyncSurveyQuestionEntity(false, true);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_clientEntity!=null)
			{
				toReturn.Add(_clientEntity);
			}
			if(_customerEntity!=null)
			{
				toReturn.Add(_customerEntity);
			}
			if(_surveyAnswerEntity!=null)
			{
				toReturn.Add(_surveyAnswerEntity);
			}
			if(_surveyQuestionEntity!=null)
			{
				toReturn.Add(_surveyQuestionEntity);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();


			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="surveyResultId">PK value for SurveyResult which data should be fetched into this SurveyResult object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 surveyResultId)
		{
			return FetchUsingPK(surveyResultId, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="surveyResultId">PK value for SurveyResult which data should be fetched into this SurveyResult object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 surveyResultId, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(surveyResultId, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="surveyResultId">PK value for SurveyResult which data should be fetched into this SurveyResult object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 surveyResultId, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(surveyResultId, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="surveyResultId">PK value for SurveyResult which data should be fetched into this SurveyResult object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 surveyResultId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(surveyResultId, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.SurveyResultId, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new SurveyResultRelations().GetAllRelations();
		}

		/// <summary> Retrieves the related entity of type 'ClientEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ClientEntity' which is related to this entity.</returns>
		public ClientEntity GetSingleClientEntity()
		{
			return GetSingleClientEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'ClientEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ClientEntity' which is related to this entity.</returns>
		public virtual ClientEntity GetSingleClientEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedClientEntity || forceFetch || _alwaysFetchClientEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ClientEntityUsingClientId);
				ClientEntity newEntity = new ClientEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ClientId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (ClientEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_clientEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.ClientEntity = newEntity;
				_alreadyFetchedClientEntity = fetchResult;
			}
			return _clientEntity;
		}


		/// <summary> Retrieves the related entity of type 'CustomerEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'CustomerEntity' which is related to this entity.</returns>
		public CustomerEntity GetSingleCustomerEntity()
		{
			return GetSingleCustomerEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'CustomerEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'CustomerEntity' which is related to this entity.</returns>
		public virtual CustomerEntity GetSingleCustomerEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedCustomerEntity || forceFetch || _alwaysFetchCustomerEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.CustomerEntityUsingCustomerId);
				CustomerEntity newEntity = new CustomerEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.CustomerId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (CustomerEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_customerEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.CustomerEntity = newEntity;
				_alreadyFetchedCustomerEntity = fetchResult;
			}
			return _customerEntity;
		}


		/// <summary> Retrieves the related entity of type 'SurveyAnswerEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'SurveyAnswerEntity' which is related to this entity.</returns>
		public SurveyAnswerEntity GetSingleSurveyAnswerEntity()
		{
			return GetSingleSurveyAnswerEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'SurveyAnswerEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'SurveyAnswerEntity' which is related to this entity.</returns>
		public virtual SurveyAnswerEntity GetSingleSurveyAnswerEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedSurveyAnswerEntity || forceFetch || _alwaysFetchSurveyAnswerEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.SurveyAnswerEntityUsingSurveyAnswerId);
				SurveyAnswerEntity newEntity = new SurveyAnswerEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.SurveyAnswerId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (SurveyAnswerEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_surveyAnswerEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.SurveyAnswerEntity = newEntity;
				_alreadyFetchedSurveyAnswerEntity = fetchResult;
			}
			return _surveyAnswerEntity;
		}


		/// <summary> Retrieves the related entity of type 'SurveyQuestionEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'SurveyQuestionEntity' which is related to this entity.</returns>
		public SurveyQuestionEntity GetSingleSurveyQuestionEntity()
		{
			return GetSingleSurveyQuestionEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'SurveyQuestionEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'SurveyQuestionEntity' which is related to this entity.</returns>
		public virtual SurveyQuestionEntity GetSingleSurveyQuestionEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedSurveyQuestionEntity || forceFetch || _alwaysFetchSurveyQuestionEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.SurveyQuestionEntityUsingSurveyQuestionId);
				SurveyQuestionEntity newEntity = new SurveyQuestionEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.SurveyQuestionId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (SurveyQuestionEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_surveyQuestionEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.SurveyQuestionEntity = newEntity;
				_alreadyFetchedSurveyQuestionEntity = fetchResult;
			}
			return _surveyQuestionEntity;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("ClientEntity", _clientEntity);
			toReturn.Add("CustomerEntity", _customerEntity);
			toReturn.Add("SurveyAnswerEntity", _surveyAnswerEntity);
			toReturn.Add("SurveyQuestionEntity", _surveyQuestionEntity);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="surveyResultId">PK value for SurveyResult which data should be fetched into this SurveyResult object</param>
		/// <param name="validator">The validator object for this SurveyResultEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int32 surveyResultId, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(surveyResultId, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{
			_clientEntityReturnsNewIfNotFound = true;
			_customerEntityReturnsNewIfNotFound = true;
			_surveyAnswerEntityReturnsNewIfNotFound = true;
			_surveyQuestionEntityReturnsNewIfNotFound = true;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SurveyResultId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SurveyAnswerId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SurveyQuestionId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CustomerId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ClientId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Comment", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ParentCompanyId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SubmittedUTC", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _clientEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncClientEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _clientEntity, new PropertyChangedEventHandler( OnClientEntityPropertyChanged ), "ClientEntity", Obymobi.Data.RelationClasses.StaticSurveyResultRelations.ClientEntityUsingClientIdStatic, true, signalRelatedEntity, "SurveyResultCollection", resetFKFields, new int[] { (int)SurveyResultFieldIndex.ClientId } );		
			_clientEntity = null;
		}
		
		/// <summary> setups the sync logic for member _clientEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncClientEntity(IEntityCore relatedEntity)
		{
			if(_clientEntity!=relatedEntity)
			{		
				DesetupSyncClientEntity(true, true);
				_clientEntity = (ClientEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _clientEntity, new PropertyChangedEventHandler( OnClientEntityPropertyChanged ), "ClientEntity", Obymobi.Data.RelationClasses.StaticSurveyResultRelations.ClientEntityUsingClientIdStatic, true, ref _alreadyFetchedClientEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnClientEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _customerEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncCustomerEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _customerEntity, new PropertyChangedEventHandler( OnCustomerEntityPropertyChanged ), "CustomerEntity", Obymobi.Data.RelationClasses.StaticSurveyResultRelations.CustomerEntityUsingCustomerIdStatic, true, signalRelatedEntity, "SurveyResultCollection", resetFKFields, new int[] { (int)SurveyResultFieldIndex.CustomerId } );		
			_customerEntity = null;
		}
		
		/// <summary> setups the sync logic for member _customerEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncCustomerEntity(IEntityCore relatedEntity)
		{
			if(_customerEntity!=relatedEntity)
			{		
				DesetupSyncCustomerEntity(true, true);
				_customerEntity = (CustomerEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _customerEntity, new PropertyChangedEventHandler( OnCustomerEntityPropertyChanged ), "CustomerEntity", Obymobi.Data.RelationClasses.StaticSurveyResultRelations.CustomerEntityUsingCustomerIdStatic, true, ref _alreadyFetchedCustomerEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnCustomerEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _surveyAnswerEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncSurveyAnswerEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _surveyAnswerEntity, new PropertyChangedEventHandler( OnSurveyAnswerEntityPropertyChanged ), "SurveyAnswerEntity", Obymobi.Data.RelationClasses.StaticSurveyResultRelations.SurveyAnswerEntityUsingSurveyAnswerIdStatic, true, signalRelatedEntity, "SurveyResultCollection", resetFKFields, new int[] { (int)SurveyResultFieldIndex.SurveyAnswerId } );		
			_surveyAnswerEntity = null;
		}
		
		/// <summary> setups the sync logic for member _surveyAnswerEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncSurveyAnswerEntity(IEntityCore relatedEntity)
		{
			if(_surveyAnswerEntity!=relatedEntity)
			{		
				DesetupSyncSurveyAnswerEntity(true, true);
				_surveyAnswerEntity = (SurveyAnswerEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _surveyAnswerEntity, new PropertyChangedEventHandler( OnSurveyAnswerEntityPropertyChanged ), "SurveyAnswerEntity", Obymobi.Data.RelationClasses.StaticSurveyResultRelations.SurveyAnswerEntityUsingSurveyAnswerIdStatic, true, ref _alreadyFetchedSurveyAnswerEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnSurveyAnswerEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _surveyQuestionEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncSurveyQuestionEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _surveyQuestionEntity, new PropertyChangedEventHandler( OnSurveyQuestionEntityPropertyChanged ), "SurveyQuestionEntity", Obymobi.Data.RelationClasses.StaticSurveyResultRelations.SurveyQuestionEntityUsingSurveyQuestionIdStatic, true, signalRelatedEntity, "SurveyResultCollection", resetFKFields, new int[] { (int)SurveyResultFieldIndex.SurveyQuestionId } );		
			_surveyQuestionEntity = null;
		}
		
		/// <summary> setups the sync logic for member _surveyQuestionEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncSurveyQuestionEntity(IEntityCore relatedEntity)
		{
			if(_surveyQuestionEntity!=relatedEntity)
			{		
				DesetupSyncSurveyQuestionEntity(true, true);
				_surveyQuestionEntity = (SurveyQuestionEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _surveyQuestionEntity, new PropertyChangedEventHandler( OnSurveyQuestionEntityPropertyChanged ), "SurveyQuestionEntity", Obymobi.Data.RelationClasses.StaticSurveyResultRelations.SurveyQuestionEntityUsingSurveyQuestionIdStatic, true, ref _alreadyFetchedSurveyQuestionEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnSurveyQuestionEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="surveyResultId">PK value for SurveyResult which data should be fetched into this SurveyResult object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int32 surveyResultId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)SurveyResultFieldIndex.SurveyResultId].ForcedCurrentValueWrite(surveyResultId);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateSurveyResultDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new SurveyResultEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static SurveyResultRelations Relations
		{
			get	{ return new SurveyResultRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Client'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathClientEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ClientCollection(), (IEntityRelation)GetRelationsForField("ClientEntity")[0], (int)Obymobi.Data.EntityType.SurveyResultEntity, (int)Obymobi.Data.EntityType.ClientEntity, 0, null, null, null, "ClientEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Customer'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCustomerEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CustomerCollection(), (IEntityRelation)GetRelationsForField("CustomerEntity")[0], (int)Obymobi.Data.EntityType.SurveyResultEntity, (int)Obymobi.Data.EntityType.CustomerEntity, 0, null, null, null, "CustomerEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'SurveyAnswer'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathSurveyAnswerEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.SurveyAnswerCollection(), (IEntityRelation)GetRelationsForField("SurveyAnswerEntity")[0], (int)Obymobi.Data.EntityType.SurveyResultEntity, (int)Obymobi.Data.EntityType.SurveyAnswerEntity, 0, null, null, null, "SurveyAnswerEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'SurveyQuestion'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathSurveyQuestionEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.SurveyQuestionCollection(), (IEntityRelation)GetRelationsForField("SurveyQuestionEntity")[0], (int)Obymobi.Data.EntityType.SurveyResultEntity, (int)Obymobi.Data.EntityType.SurveyQuestionEntity, 0, null, null, null, "SurveyQuestionEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The SurveyResultId property of the Entity SurveyResult<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SurveyResult"."SurveyResultId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 SurveyResultId
		{
			get { return (System.Int32)GetValue((int)SurveyResultFieldIndex.SurveyResultId, true); }
			set	{ SetValue((int)SurveyResultFieldIndex.SurveyResultId, value, true); }
		}

		/// <summary> The SurveyAnswerId property of the Entity SurveyResult<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SurveyResult"."SurveyAnswerId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> SurveyAnswerId
		{
			get { return (Nullable<System.Int32>)GetValue((int)SurveyResultFieldIndex.SurveyAnswerId, false); }
			set	{ SetValue((int)SurveyResultFieldIndex.SurveyAnswerId, value, true); }
		}

		/// <summary> The SurveyQuestionId property of the Entity SurveyResult<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SurveyResult"."SurveyQuestionId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> SurveyQuestionId
		{
			get { return (Nullable<System.Int32>)GetValue((int)SurveyResultFieldIndex.SurveyQuestionId, false); }
			set	{ SetValue((int)SurveyResultFieldIndex.SurveyQuestionId, value, true); }
		}

		/// <summary> The CustomerId property of the Entity SurveyResult<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SurveyResult"."CustomerId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> CustomerId
		{
			get { return (Nullable<System.Int32>)GetValue((int)SurveyResultFieldIndex.CustomerId, false); }
			set	{ SetValue((int)SurveyResultFieldIndex.CustomerId, value, true); }
		}

		/// <summary> The ClientId property of the Entity SurveyResult<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SurveyResult"."ClientId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ClientId
		{
			get { return (Nullable<System.Int32>)GetValue((int)SurveyResultFieldIndex.ClientId, false); }
			set	{ SetValue((int)SurveyResultFieldIndex.ClientId, value, true); }
		}

		/// <summary> The Comment property of the Entity SurveyResult<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SurveyResult"."Comment"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Comment
		{
			get { return (System.String)GetValue((int)SurveyResultFieldIndex.Comment, true); }
			set	{ SetValue((int)SurveyResultFieldIndex.Comment, value, true); }
		}

		/// <summary> The CreatedBy property of the Entity SurveyResult<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SurveyResult"."CreatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CreatedBy
		{
			get { return (System.Int32)GetValue((int)SurveyResultFieldIndex.CreatedBy, true); }
			set	{ SetValue((int)SurveyResultFieldIndex.CreatedBy, value, true); }
		}

		/// <summary> The UpdatedBy property of the Entity SurveyResult<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SurveyResult"."UpdatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 UpdatedBy
		{
			get { return (System.Int32)GetValue((int)SurveyResultFieldIndex.UpdatedBy, true); }
			set	{ SetValue((int)SurveyResultFieldIndex.UpdatedBy, value, true); }
		}

		/// <summary> The ParentCompanyId property of the Entity SurveyResult<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SurveyResult"."ParentCompanyId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 ParentCompanyId
		{
			get { return (System.Int32)GetValue((int)SurveyResultFieldIndex.ParentCompanyId, true); }
			set	{ SetValue((int)SurveyResultFieldIndex.ParentCompanyId, value, true); }
		}

		/// <summary> The CreatedUTC property of the Entity SurveyResult<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SurveyResult"."CreatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CreatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)SurveyResultFieldIndex.CreatedUTC, false); }
			set	{ SetValue((int)SurveyResultFieldIndex.CreatedUTC, value, true); }
		}

		/// <summary> The UpdatedUTC property of the Entity SurveyResult<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SurveyResult"."UpdatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)SurveyResultFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)SurveyResultFieldIndex.UpdatedUTC, value, true); }
		}

		/// <summary> The SubmittedUTC property of the Entity SurveyResult<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SurveyResult"."SubmittedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> SubmittedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)SurveyResultFieldIndex.SubmittedUTC, false); }
			set	{ SetValue((int)SurveyResultFieldIndex.SubmittedUTC, value, true); }
		}


		/// <summary> Gets / sets related entity of type 'ClientEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleClientEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual ClientEntity ClientEntity
		{
			get	{ return GetSingleClientEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncClientEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "SurveyResultCollection", "ClientEntity", _clientEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for ClientEntity. When set to true, ClientEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ClientEntity is accessed. You can always execute a forced fetch by calling GetSingleClientEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchClientEntity
		{
			get	{ return _alwaysFetchClientEntity; }
			set	{ _alwaysFetchClientEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ClientEntity already has been fetched. Setting this property to false when ClientEntity has been fetched
		/// will set ClientEntity to null as well. Setting this property to true while ClientEntity hasn't been fetched disables lazy loading for ClientEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedClientEntity
		{
			get { return _alreadyFetchedClientEntity;}
			set 
			{
				if(_alreadyFetchedClientEntity && !value)
				{
					this.ClientEntity = null;
				}
				_alreadyFetchedClientEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property ClientEntity is not found
		/// in the database. When set to true, ClientEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool ClientEntityReturnsNewIfNotFound
		{
			get	{ return _clientEntityReturnsNewIfNotFound; }
			set { _clientEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'CustomerEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleCustomerEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual CustomerEntity CustomerEntity
		{
			get	{ return GetSingleCustomerEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncCustomerEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "SurveyResultCollection", "CustomerEntity", _customerEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for CustomerEntity. When set to true, CustomerEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CustomerEntity is accessed. You can always execute a forced fetch by calling GetSingleCustomerEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCustomerEntity
		{
			get	{ return _alwaysFetchCustomerEntity; }
			set	{ _alwaysFetchCustomerEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CustomerEntity already has been fetched. Setting this property to false when CustomerEntity has been fetched
		/// will set CustomerEntity to null as well. Setting this property to true while CustomerEntity hasn't been fetched disables lazy loading for CustomerEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCustomerEntity
		{
			get { return _alreadyFetchedCustomerEntity;}
			set 
			{
				if(_alreadyFetchedCustomerEntity && !value)
				{
					this.CustomerEntity = null;
				}
				_alreadyFetchedCustomerEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property CustomerEntity is not found
		/// in the database. When set to true, CustomerEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool CustomerEntityReturnsNewIfNotFound
		{
			get	{ return _customerEntityReturnsNewIfNotFound; }
			set { _customerEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'SurveyAnswerEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleSurveyAnswerEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual SurveyAnswerEntity SurveyAnswerEntity
		{
			get	{ return GetSingleSurveyAnswerEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncSurveyAnswerEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "SurveyResultCollection", "SurveyAnswerEntity", _surveyAnswerEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for SurveyAnswerEntity. When set to true, SurveyAnswerEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time SurveyAnswerEntity is accessed. You can always execute a forced fetch by calling GetSingleSurveyAnswerEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchSurveyAnswerEntity
		{
			get	{ return _alwaysFetchSurveyAnswerEntity; }
			set	{ _alwaysFetchSurveyAnswerEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property SurveyAnswerEntity already has been fetched. Setting this property to false when SurveyAnswerEntity has been fetched
		/// will set SurveyAnswerEntity to null as well. Setting this property to true while SurveyAnswerEntity hasn't been fetched disables lazy loading for SurveyAnswerEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedSurveyAnswerEntity
		{
			get { return _alreadyFetchedSurveyAnswerEntity;}
			set 
			{
				if(_alreadyFetchedSurveyAnswerEntity && !value)
				{
					this.SurveyAnswerEntity = null;
				}
				_alreadyFetchedSurveyAnswerEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property SurveyAnswerEntity is not found
		/// in the database. When set to true, SurveyAnswerEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool SurveyAnswerEntityReturnsNewIfNotFound
		{
			get	{ return _surveyAnswerEntityReturnsNewIfNotFound; }
			set { _surveyAnswerEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'SurveyQuestionEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleSurveyQuestionEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual SurveyQuestionEntity SurveyQuestionEntity
		{
			get	{ return GetSingleSurveyQuestionEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncSurveyQuestionEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "SurveyResultCollection", "SurveyQuestionEntity", _surveyQuestionEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for SurveyQuestionEntity. When set to true, SurveyQuestionEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time SurveyQuestionEntity is accessed. You can always execute a forced fetch by calling GetSingleSurveyQuestionEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchSurveyQuestionEntity
		{
			get	{ return _alwaysFetchSurveyQuestionEntity; }
			set	{ _alwaysFetchSurveyQuestionEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property SurveyQuestionEntity already has been fetched. Setting this property to false when SurveyQuestionEntity has been fetched
		/// will set SurveyQuestionEntity to null as well. Setting this property to true while SurveyQuestionEntity hasn't been fetched disables lazy loading for SurveyQuestionEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedSurveyQuestionEntity
		{
			get { return _alreadyFetchedSurveyQuestionEntity;}
			set 
			{
				if(_alreadyFetchedSurveyQuestionEntity && !value)
				{
					this.SurveyQuestionEntity = null;
				}
				_alreadyFetchedSurveyQuestionEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property SurveyQuestionEntity is not found
		/// in the database. When set to true, SurveyQuestionEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool SurveyQuestionEntityReturnsNewIfNotFound
		{
			get	{ return _surveyQuestionEntityReturnsNewIfNotFound; }
			set { _surveyQuestionEntityReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the Obymobi.Data.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)Obymobi.Data.EntityType.SurveyResultEntity; }
		}

		#endregion

		
		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
