﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
using System.Runtime.Serialization;
using System.Data;
using System.Xml.Serialization;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity base class which represents the base class for the entity 'SupportpoolNotificationRecipient'.<br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public abstract partial class SupportpoolNotificationRecipientEntityBase : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		public new string LLBLGenProEntityName {
			get { return "SupportpoolNotificationRecipientEntity"; }
		}
	
		#region Class Member Declarations
		private SupportpoolEntity _supportpoolEntity;
		private bool	_alwaysFetchSupportpoolEntity, _alreadyFetchedSupportpoolEntity, _supportpoolEntityReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name SupportpoolEntity</summary>
			public static readonly string SupportpoolEntity = "SupportpoolEntity";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static SupportpoolNotificationRecipientEntityBase()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		protected SupportpoolNotificationRecipientEntityBase() :base("SupportpoolNotificationRecipientEntity")
		{
			InitClassEmpty(null);
		}

		/// <summary>CTor</summary>
		/// <param name="supportpoolNotificationRecipientId">PK value for SupportpoolNotificationRecipient which data should be fetched into this SupportpoolNotificationRecipient object</param>
		protected SupportpoolNotificationRecipientEntityBase(System.Int32 supportpoolNotificationRecipientId):base("SupportpoolNotificationRecipientEntity")
		{
			InitClassFetch(supportpoolNotificationRecipientId, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="supportpoolNotificationRecipientId">PK value for SupportpoolNotificationRecipient which data should be fetched into this SupportpoolNotificationRecipient object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		protected SupportpoolNotificationRecipientEntityBase(System.Int32 supportpoolNotificationRecipientId, IPrefetchPath prefetchPathToUse): base("SupportpoolNotificationRecipientEntity")
		{
			InitClassFetch(supportpoolNotificationRecipientId, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="supportpoolNotificationRecipientId">PK value for SupportpoolNotificationRecipient which data should be fetched into this SupportpoolNotificationRecipient object</param>
		/// <param name="validator">The custom validator object for this SupportpoolNotificationRecipientEntity</param>
		protected SupportpoolNotificationRecipientEntityBase(System.Int32 supportpoolNotificationRecipientId, IValidator validator):base("SupportpoolNotificationRecipientEntity")
		{
			InitClassFetch(supportpoolNotificationRecipientId, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected SupportpoolNotificationRecipientEntityBase(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_supportpoolEntity = (SupportpoolEntity)info.GetValue("_supportpoolEntity", typeof(SupportpoolEntity));
			if(_supportpoolEntity!=null)
			{
				_supportpoolEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_supportpoolEntityReturnsNewIfNotFound = info.GetBoolean("_supportpoolEntityReturnsNewIfNotFound");
			_alwaysFetchSupportpoolEntity = info.GetBoolean("_alwaysFetchSupportpoolEntity");
			_alreadyFetchedSupportpoolEntity = info.GetBoolean("_alreadyFetchedSupportpoolEntity");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}	
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((SupportpoolNotificationRecipientFieldIndex)fieldIndex)
			{
				case SupportpoolNotificationRecipientFieldIndex.SupportpoolId:
					DesetupSyncSupportpoolEntity(true, false);
					_alreadyFetchedSupportpoolEntity = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedSupportpoolEntity = (_supportpoolEntity != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "SupportpoolEntity":
					toReturn.Add(Relations.SupportpoolEntityUsingSupportpoolId);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_supportpoolEntity", (!this.MarkedForDeletion?_supportpoolEntity:null));
			info.AddValue("_supportpoolEntityReturnsNewIfNotFound", _supportpoolEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchSupportpoolEntity", _alwaysFetchSupportpoolEntity);
			info.AddValue("_alreadyFetchedSupportpoolEntity", _alreadyFetchedSupportpoolEntity);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "SupportpoolEntity":
					_alreadyFetchedSupportpoolEntity = true;
					this.SupportpoolEntity = (SupportpoolEntity)entity;
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "SupportpoolEntity":
					SetupSyncSupportpoolEntity(relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "SupportpoolEntity":
					DesetupSyncSupportpoolEntity(false, true);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_supportpoolEntity!=null)
			{
				toReturn.Add(_supportpoolEntity);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();


			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="supportpoolNotificationRecipientId">PK value for SupportpoolNotificationRecipient which data should be fetched into this SupportpoolNotificationRecipient object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 supportpoolNotificationRecipientId)
		{
			return FetchUsingPK(supportpoolNotificationRecipientId, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="supportpoolNotificationRecipientId">PK value for SupportpoolNotificationRecipient which data should be fetched into this SupportpoolNotificationRecipient object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 supportpoolNotificationRecipientId, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(supportpoolNotificationRecipientId, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="supportpoolNotificationRecipientId">PK value for SupportpoolNotificationRecipient which data should be fetched into this SupportpoolNotificationRecipient object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 supportpoolNotificationRecipientId, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(supportpoolNotificationRecipientId, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="supportpoolNotificationRecipientId">PK value for SupportpoolNotificationRecipient which data should be fetched into this SupportpoolNotificationRecipient object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 supportpoolNotificationRecipientId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(supportpoolNotificationRecipientId, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.SupportpoolNotificationRecipientId, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new SupportpoolNotificationRecipientRelations().GetAllRelations();
		}

		/// <summary> Retrieves the related entity of type 'SupportpoolEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'SupportpoolEntity' which is related to this entity.</returns>
		public SupportpoolEntity GetSingleSupportpoolEntity()
		{
			return GetSingleSupportpoolEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'SupportpoolEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'SupportpoolEntity' which is related to this entity.</returns>
		public virtual SupportpoolEntity GetSingleSupportpoolEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedSupportpoolEntity || forceFetch || _alwaysFetchSupportpoolEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.SupportpoolEntityUsingSupportpoolId);
				SupportpoolEntity newEntity = new SupportpoolEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.SupportpoolId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (SupportpoolEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_supportpoolEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.SupportpoolEntity = newEntity;
				_alreadyFetchedSupportpoolEntity = fetchResult;
			}
			return _supportpoolEntity;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("SupportpoolEntity", _supportpoolEntity);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="supportpoolNotificationRecipientId">PK value for SupportpoolNotificationRecipient which data should be fetched into this SupportpoolNotificationRecipient object</param>
		/// <param name="validator">The validator object for this SupportpoolNotificationRecipientEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int32 supportpoolNotificationRecipientId, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(supportpoolNotificationRecipientId, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{
			_supportpoolEntityReturnsNewIfNotFound = true;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SupportpoolNotificationRecipientId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SupportpoolId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Name", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Phonenumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Email", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("NotifyOfflineTerminals", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("NotifyOfflineClients", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("NotifyUnprocessableOrders", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("NotifyExpiredSteps", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("NotifyTooMuchOfflineClientsJump", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("NotifyBouncedEmails", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedBy", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _supportpoolEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncSupportpoolEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _supportpoolEntity, new PropertyChangedEventHandler( OnSupportpoolEntityPropertyChanged ), "SupportpoolEntity", Obymobi.Data.RelationClasses.StaticSupportpoolNotificationRecipientRelations.SupportpoolEntityUsingSupportpoolIdStatic, true, signalRelatedEntity, "SupportpoolNotificationRecipientCollection", resetFKFields, new int[] { (int)SupportpoolNotificationRecipientFieldIndex.SupportpoolId } );		
			_supportpoolEntity = null;
		}
		
		/// <summary> setups the sync logic for member _supportpoolEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncSupportpoolEntity(IEntityCore relatedEntity)
		{
			if(_supportpoolEntity!=relatedEntity)
			{		
				DesetupSyncSupportpoolEntity(true, true);
				_supportpoolEntity = (SupportpoolEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _supportpoolEntity, new PropertyChangedEventHandler( OnSupportpoolEntityPropertyChanged ), "SupportpoolEntity", Obymobi.Data.RelationClasses.StaticSupportpoolNotificationRecipientRelations.SupportpoolEntityUsingSupportpoolIdStatic, true, ref _alreadyFetchedSupportpoolEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnSupportpoolEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="supportpoolNotificationRecipientId">PK value for SupportpoolNotificationRecipient which data should be fetched into this SupportpoolNotificationRecipient object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int32 supportpoolNotificationRecipientId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)SupportpoolNotificationRecipientFieldIndex.SupportpoolNotificationRecipientId].ForcedCurrentValueWrite(supportpoolNotificationRecipientId);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateSupportpoolNotificationRecipientDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new SupportpoolNotificationRecipientEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static SupportpoolNotificationRecipientRelations Relations
		{
			get	{ return new SupportpoolNotificationRecipientRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Supportpool'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathSupportpoolEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.SupportpoolCollection(), (IEntityRelation)GetRelationsForField("SupportpoolEntity")[0], (int)Obymobi.Data.EntityType.SupportpoolNotificationRecipientEntity, (int)Obymobi.Data.EntityType.SupportpoolEntity, 0, null, null, null, "SupportpoolEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The SupportpoolNotificationRecipientId property of the Entity SupportpoolNotificationRecipient<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SupportpoolNotificationRecipient"."SupportpoolNotificationRecipientId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 SupportpoolNotificationRecipientId
		{
			get { return (System.Int32)GetValue((int)SupportpoolNotificationRecipientFieldIndex.SupportpoolNotificationRecipientId, true); }
			set	{ SetValue((int)SupportpoolNotificationRecipientFieldIndex.SupportpoolNotificationRecipientId, value, true); }
		}

		/// <summary> The SupportpoolId property of the Entity SupportpoolNotificationRecipient<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SupportpoolNotificationRecipient"."SupportpoolId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> SupportpoolId
		{
			get { return (Nullable<System.Int32>)GetValue((int)SupportpoolNotificationRecipientFieldIndex.SupportpoolId, false); }
			set	{ SetValue((int)SupportpoolNotificationRecipientFieldIndex.SupportpoolId, value, true); }
		}

		/// <summary> The Name property of the Entity SupportpoolNotificationRecipient<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SupportpoolNotificationRecipient"."Name"<br/>
		/// Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 255<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Name
		{
			get { return (System.String)GetValue((int)SupportpoolNotificationRecipientFieldIndex.Name, true); }
			set	{ SetValue((int)SupportpoolNotificationRecipientFieldIndex.Name, value, true); }
		}

		/// <summary> The Phonenumber property of the Entity SupportpoolNotificationRecipient<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SupportpoolNotificationRecipient"."Phonenumber"<br/>
		/// Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 255<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Phonenumber
		{
			get { return (System.String)GetValue((int)SupportpoolNotificationRecipientFieldIndex.Phonenumber, true); }
			set	{ SetValue((int)SupportpoolNotificationRecipientFieldIndex.Phonenumber, value, true); }
		}

		/// <summary> The Email property of the Entity SupportpoolNotificationRecipient<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SupportpoolNotificationRecipient"."Email"<br/>
		/// Table field type characteristics (type, precision, scale, length): VarChar, 0, 0, 255<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Email
		{
			get { return (System.String)GetValue((int)SupportpoolNotificationRecipientFieldIndex.Email, true); }
			set	{ SetValue((int)SupportpoolNotificationRecipientFieldIndex.Email, value, true); }
		}

		/// <summary> The NotifyOfflineTerminals property of the Entity SupportpoolNotificationRecipient<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SupportpoolNotificationRecipient"."NotifyOfflineTerminals"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean NotifyOfflineTerminals
		{
			get { return (System.Boolean)GetValue((int)SupportpoolNotificationRecipientFieldIndex.NotifyOfflineTerminals, true); }
			set	{ SetValue((int)SupportpoolNotificationRecipientFieldIndex.NotifyOfflineTerminals, value, true); }
		}

		/// <summary> The NotifyOfflineClients property of the Entity SupportpoolNotificationRecipient<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SupportpoolNotificationRecipient"."NotifyOfflineClients"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean NotifyOfflineClients
		{
			get { return (System.Boolean)GetValue((int)SupportpoolNotificationRecipientFieldIndex.NotifyOfflineClients, true); }
			set	{ SetValue((int)SupportpoolNotificationRecipientFieldIndex.NotifyOfflineClients, value, true); }
		}

		/// <summary> The NotifyUnprocessableOrders property of the Entity SupportpoolNotificationRecipient<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SupportpoolNotificationRecipient"."NotifyUnprocessableOrders"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean NotifyUnprocessableOrders
		{
			get { return (System.Boolean)GetValue((int)SupportpoolNotificationRecipientFieldIndex.NotifyUnprocessableOrders, true); }
			set	{ SetValue((int)SupportpoolNotificationRecipientFieldIndex.NotifyUnprocessableOrders, value, true); }
		}

		/// <summary> The NotifyExpiredSteps property of the Entity SupportpoolNotificationRecipient<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SupportpoolNotificationRecipient"."NotifyExpiredSteps"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean NotifyExpiredSteps
		{
			get { return (System.Boolean)GetValue((int)SupportpoolNotificationRecipientFieldIndex.NotifyExpiredSteps, true); }
			set	{ SetValue((int)SupportpoolNotificationRecipientFieldIndex.NotifyExpiredSteps, value, true); }
		}

		/// <summary> The NotifyTooMuchOfflineClientsJump property of the Entity SupportpoolNotificationRecipient<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SupportpoolNotificationRecipient"."NotifyTooMuchOfflineClientsJump"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean NotifyTooMuchOfflineClientsJump
		{
			get { return (System.Boolean)GetValue((int)SupportpoolNotificationRecipientFieldIndex.NotifyTooMuchOfflineClientsJump, true); }
			set	{ SetValue((int)SupportpoolNotificationRecipientFieldIndex.NotifyTooMuchOfflineClientsJump, value, true); }
		}

		/// <summary> The NotifyBouncedEmails property of the Entity SupportpoolNotificationRecipient<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SupportpoolNotificationRecipient"."NotifyBouncedEmails"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Boolean NotifyBouncedEmails
		{
			get { return (System.Boolean)GetValue((int)SupportpoolNotificationRecipientFieldIndex.NotifyBouncedEmails, true); }
			set	{ SetValue((int)SupportpoolNotificationRecipientFieldIndex.NotifyBouncedEmails, value, true); }
		}

		/// <summary> The CreatedUTC property of the Entity SupportpoolNotificationRecipient<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SupportpoolNotificationRecipient"."CreatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CreatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)SupportpoolNotificationRecipientFieldIndex.CreatedUTC, false); }
			set	{ SetValue((int)SupportpoolNotificationRecipientFieldIndex.CreatedUTC, value, true); }
		}

		/// <summary> The CreatedBy property of the Entity SupportpoolNotificationRecipient<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SupportpoolNotificationRecipient"."CreatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> CreatedBy
		{
			get { return (Nullable<System.Int32>)GetValue((int)SupportpoolNotificationRecipientFieldIndex.CreatedBy, false); }
			set	{ SetValue((int)SupportpoolNotificationRecipientFieldIndex.CreatedBy, value, true); }
		}

		/// <summary> The UpdatedUTC property of the Entity SupportpoolNotificationRecipient<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SupportpoolNotificationRecipient"."UpdatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)SupportpoolNotificationRecipientFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)SupportpoolNotificationRecipientFieldIndex.UpdatedUTC, value, true); }
		}

		/// <summary> The UpdatedBy property of the Entity SupportpoolNotificationRecipient<br/><br/></summary>
		/// <remarks>Mapped on  table field: "SupportpoolNotificationRecipient"."UpdatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> UpdatedBy
		{
			get { return (Nullable<System.Int32>)GetValue((int)SupportpoolNotificationRecipientFieldIndex.UpdatedBy, false); }
			set	{ SetValue((int)SupportpoolNotificationRecipientFieldIndex.UpdatedBy, value, true); }
		}


		/// <summary> Gets / sets related entity of type 'SupportpoolEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleSupportpoolEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual SupportpoolEntity SupportpoolEntity
		{
			get	{ return GetSingleSupportpoolEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncSupportpoolEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "SupportpoolNotificationRecipientCollection", "SupportpoolEntity", _supportpoolEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for SupportpoolEntity. When set to true, SupportpoolEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time SupportpoolEntity is accessed. You can always execute a forced fetch by calling GetSingleSupportpoolEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchSupportpoolEntity
		{
			get	{ return _alwaysFetchSupportpoolEntity; }
			set	{ _alwaysFetchSupportpoolEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property SupportpoolEntity already has been fetched. Setting this property to false when SupportpoolEntity has been fetched
		/// will set SupportpoolEntity to null as well. Setting this property to true while SupportpoolEntity hasn't been fetched disables lazy loading for SupportpoolEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedSupportpoolEntity
		{
			get { return _alreadyFetchedSupportpoolEntity;}
			set 
			{
				if(_alreadyFetchedSupportpoolEntity && !value)
				{
					this.SupportpoolEntity = null;
				}
				_alreadyFetchedSupportpoolEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property SupportpoolEntity is not found
		/// in the database. When set to true, SupportpoolEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool SupportpoolEntityReturnsNewIfNotFound
		{
			get	{ return _supportpoolEntityReturnsNewIfNotFound; }
			set { _supportpoolEntityReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the Obymobi.Data.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)Obymobi.Data.EntityType.SupportpoolNotificationRecipientEntity; }
		}

		#endregion

		
		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
