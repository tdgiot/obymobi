﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
using System.Runtime.Serialization;
using System.Data;
using System.Xml.Serialization;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity base class which represents the base class for the entity 'AdvertisementTagEntertainment'.<br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public abstract partial class AdvertisementTagEntertainmentEntityBase : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		public new string LLBLGenProEntityName {
			get { return "AdvertisementTagEntertainmentEntity"; }
		}
	
		#region Class Member Declarations
		private AdvertisementTagEntity _advertisementTagEntity;
		private bool	_alwaysFetchAdvertisementTagEntity, _alreadyFetchedAdvertisementTagEntity, _advertisementTagEntityReturnsNewIfNotFound;
		private EntertainmentEntity _entertainmentEntity;
		private bool	_alwaysFetchEntertainmentEntity, _alreadyFetchedEntertainmentEntity, _entertainmentEntityReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name AdvertisementTagEntity</summary>
			public static readonly string AdvertisementTagEntity = "AdvertisementTagEntity";
			/// <summary>Member name EntertainmentEntity</summary>
			public static readonly string EntertainmentEntity = "EntertainmentEntity";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static AdvertisementTagEntertainmentEntityBase()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		protected AdvertisementTagEntertainmentEntityBase() :base("AdvertisementTagEntertainmentEntity")
		{
			InitClassEmpty(null);
		}

		/// <summary>CTor</summary>
		/// <param name="advertisementTagEntertainmentId">PK value for AdvertisementTagEntertainment which data should be fetched into this AdvertisementTagEntertainment object</param>
		protected AdvertisementTagEntertainmentEntityBase(System.Int32 advertisementTagEntertainmentId):base("AdvertisementTagEntertainmentEntity")
		{
			InitClassFetch(advertisementTagEntertainmentId, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="advertisementTagEntertainmentId">PK value for AdvertisementTagEntertainment which data should be fetched into this AdvertisementTagEntertainment object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		protected AdvertisementTagEntertainmentEntityBase(System.Int32 advertisementTagEntertainmentId, IPrefetchPath prefetchPathToUse): base("AdvertisementTagEntertainmentEntity")
		{
			InitClassFetch(advertisementTagEntertainmentId, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="advertisementTagEntertainmentId">PK value for AdvertisementTagEntertainment which data should be fetched into this AdvertisementTagEntertainment object</param>
		/// <param name="validator">The custom validator object for this AdvertisementTagEntertainmentEntity</param>
		protected AdvertisementTagEntertainmentEntityBase(System.Int32 advertisementTagEntertainmentId, IValidator validator):base("AdvertisementTagEntertainmentEntity")
		{
			InitClassFetch(advertisementTagEntertainmentId, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected AdvertisementTagEntertainmentEntityBase(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_advertisementTagEntity = (AdvertisementTagEntity)info.GetValue("_advertisementTagEntity", typeof(AdvertisementTagEntity));
			if(_advertisementTagEntity!=null)
			{
				_advertisementTagEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_advertisementTagEntityReturnsNewIfNotFound = info.GetBoolean("_advertisementTagEntityReturnsNewIfNotFound");
			_alwaysFetchAdvertisementTagEntity = info.GetBoolean("_alwaysFetchAdvertisementTagEntity");
			_alreadyFetchedAdvertisementTagEntity = info.GetBoolean("_alreadyFetchedAdvertisementTagEntity");

			_entertainmentEntity = (EntertainmentEntity)info.GetValue("_entertainmentEntity", typeof(EntertainmentEntity));
			if(_entertainmentEntity!=null)
			{
				_entertainmentEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_entertainmentEntityReturnsNewIfNotFound = info.GetBoolean("_entertainmentEntityReturnsNewIfNotFound");
			_alwaysFetchEntertainmentEntity = info.GetBoolean("_alwaysFetchEntertainmentEntity");
			_alreadyFetchedEntertainmentEntity = info.GetBoolean("_alreadyFetchedEntertainmentEntity");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}	
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((AdvertisementTagEntertainmentFieldIndex)fieldIndex)
			{
				case AdvertisementTagEntertainmentFieldIndex.EntertainmentId:
					DesetupSyncEntertainmentEntity(true, false);
					_alreadyFetchedEntertainmentEntity = false;
					break;
				case AdvertisementTagEntertainmentFieldIndex.AdvertisementTagId:
					DesetupSyncAdvertisementTagEntity(true, false);
					_alreadyFetchedAdvertisementTagEntity = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedAdvertisementTagEntity = (_advertisementTagEntity != null);
			_alreadyFetchedEntertainmentEntity = (_entertainmentEntity != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "AdvertisementTagEntity":
					toReturn.Add(Relations.AdvertisementTagEntityUsingAdvertisementTagId);
					break;
				case "EntertainmentEntity":
					toReturn.Add(Relations.EntertainmentEntityUsingEntertainmentId);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_advertisementTagEntity", (!this.MarkedForDeletion?_advertisementTagEntity:null));
			info.AddValue("_advertisementTagEntityReturnsNewIfNotFound", _advertisementTagEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchAdvertisementTagEntity", _alwaysFetchAdvertisementTagEntity);
			info.AddValue("_alreadyFetchedAdvertisementTagEntity", _alreadyFetchedAdvertisementTagEntity);
			info.AddValue("_entertainmentEntity", (!this.MarkedForDeletion?_entertainmentEntity:null));
			info.AddValue("_entertainmentEntityReturnsNewIfNotFound", _entertainmentEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchEntertainmentEntity", _alwaysFetchEntertainmentEntity);
			info.AddValue("_alreadyFetchedEntertainmentEntity", _alreadyFetchedEntertainmentEntity);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "AdvertisementTagEntity":
					_alreadyFetchedAdvertisementTagEntity = true;
					this.AdvertisementTagEntity = (AdvertisementTagEntity)entity;
					break;
				case "EntertainmentEntity":
					_alreadyFetchedEntertainmentEntity = true;
					this.EntertainmentEntity = (EntertainmentEntity)entity;
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "AdvertisementTagEntity":
					SetupSyncAdvertisementTagEntity(relatedEntity);
					break;
				case "EntertainmentEntity":
					SetupSyncEntertainmentEntity(relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "AdvertisementTagEntity":
					DesetupSyncAdvertisementTagEntity(false, true);
					break;
				case "EntertainmentEntity":
					DesetupSyncEntertainmentEntity(false, true);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_advertisementTagEntity!=null)
			{
				toReturn.Add(_advertisementTagEntity);
			}
			if(_entertainmentEntity!=null)
			{
				toReturn.Add(_entertainmentEntity);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();


			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="advertisementTagEntertainmentId">PK value for AdvertisementTagEntertainment which data should be fetched into this AdvertisementTagEntertainment object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 advertisementTagEntertainmentId)
		{
			return FetchUsingPK(advertisementTagEntertainmentId, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="advertisementTagEntertainmentId">PK value for AdvertisementTagEntertainment which data should be fetched into this AdvertisementTagEntertainment object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 advertisementTagEntertainmentId, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(advertisementTagEntertainmentId, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="advertisementTagEntertainmentId">PK value for AdvertisementTagEntertainment which data should be fetched into this AdvertisementTagEntertainment object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 advertisementTagEntertainmentId, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(advertisementTagEntertainmentId, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="advertisementTagEntertainmentId">PK value for AdvertisementTagEntertainment which data should be fetched into this AdvertisementTagEntertainment object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 advertisementTagEntertainmentId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(advertisementTagEntertainmentId, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.AdvertisementTagEntertainmentId, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new AdvertisementTagEntertainmentRelations().GetAllRelations();
		}

		/// <summary> Retrieves the related entity of type 'AdvertisementTagEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'AdvertisementTagEntity' which is related to this entity.</returns>
		public AdvertisementTagEntity GetSingleAdvertisementTagEntity()
		{
			return GetSingleAdvertisementTagEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'AdvertisementTagEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'AdvertisementTagEntity' which is related to this entity.</returns>
		public virtual AdvertisementTagEntity GetSingleAdvertisementTagEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedAdvertisementTagEntity || forceFetch || _alwaysFetchAdvertisementTagEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.AdvertisementTagEntityUsingAdvertisementTagId);
				AdvertisementTagEntity newEntity = new AdvertisementTagEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.AdvertisementTagId);
				}
				if(fetchResult)
				{
					newEntity = (AdvertisementTagEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_advertisementTagEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.AdvertisementTagEntity = newEntity;
				_alreadyFetchedAdvertisementTagEntity = fetchResult;
			}
			return _advertisementTagEntity;
		}


		/// <summary> Retrieves the related entity of type 'EntertainmentEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'EntertainmentEntity' which is related to this entity.</returns>
		public EntertainmentEntity GetSingleEntertainmentEntity()
		{
			return GetSingleEntertainmentEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'EntertainmentEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'EntertainmentEntity' which is related to this entity.</returns>
		public virtual EntertainmentEntity GetSingleEntertainmentEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedEntertainmentEntity || forceFetch || _alwaysFetchEntertainmentEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.EntertainmentEntityUsingEntertainmentId);
				EntertainmentEntity newEntity = new EntertainmentEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.EntertainmentId);
				}
				if(fetchResult)
				{
					newEntity = (EntertainmentEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_entertainmentEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.EntertainmentEntity = newEntity;
				_alreadyFetchedEntertainmentEntity = fetchResult;
			}
			return _entertainmentEntity;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("AdvertisementTagEntity", _advertisementTagEntity);
			toReturn.Add("EntertainmentEntity", _entertainmentEntity);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="advertisementTagEntertainmentId">PK value for AdvertisementTagEntertainment which data should be fetched into this AdvertisementTagEntertainment object</param>
		/// <param name="validator">The validator object for this AdvertisementTagEntertainmentEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int32 advertisementTagEntertainmentId, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(advertisementTagEntertainmentId, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{
			_advertisementTagEntityReturnsNewIfNotFound = true;
			_entertainmentEntityReturnsNewIfNotFound = true;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AdvertisementTagEntertainmentId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("EntertainmentId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AdvertisementTagId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ParentCompanyId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedUTC", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _advertisementTagEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncAdvertisementTagEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _advertisementTagEntity, new PropertyChangedEventHandler( OnAdvertisementTagEntityPropertyChanged ), "AdvertisementTagEntity", Obymobi.Data.RelationClasses.StaticAdvertisementTagEntertainmentRelations.AdvertisementTagEntityUsingAdvertisementTagIdStatic, true, signalRelatedEntity, "AdvertisementTagEntertainmentCollection", resetFKFields, new int[] { (int)AdvertisementTagEntertainmentFieldIndex.AdvertisementTagId } );		
			_advertisementTagEntity = null;
		}
		
		/// <summary> setups the sync logic for member _advertisementTagEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncAdvertisementTagEntity(IEntityCore relatedEntity)
		{
			if(_advertisementTagEntity!=relatedEntity)
			{		
				DesetupSyncAdvertisementTagEntity(true, true);
				_advertisementTagEntity = (AdvertisementTagEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _advertisementTagEntity, new PropertyChangedEventHandler( OnAdvertisementTagEntityPropertyChanged ), "AdvertisementTagEntity", Obymobi.Data.RelationClasses.StaticAdvertisementTagEntertainmentRelations.AdvertisementTagEntityUsingAdvertisementTagIdStatic, true, ref _alreadyFetchedAdvertisementTagEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnAdvertisementTagEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _entertainmentEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncEntertainmentEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _entertainmentEntity, new PropertyChangedEventHandler( OnEntertainmentEntityPropertyChanged ), "EntertainmentEntity", Obymobi.Data.RelationClasses.StaticAdvertisementTagEntertainmentRelations.EntertainmentEntityUsingEntertainmentIdStatic, true, signalRelatedEntity, "AdvertisementTagEntertainmentCollection", resetFKFields, new int[] { (int)AdvertisementTagEntertainmentFieldIndex.EntertainmentId } );		
			_entertainmentEntity = null;
		}
		
		/// <summary> setups the sync logic for member _entertainmentEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncEntertainmentEntity(IEntityCore relatedEntity)
		{
			if(_entertainmentEntity!=relatedEntity)
			{		
				DesetupSyncEntertainmentEntity(true, true);
				_entertainmentEntity = (EntertainmentEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _entertainmentEntity, new PropertyChangedEventHandler( OnEntertainmentEntityPropertyChanged ), "EntertainmentEntity", Obymobi.Data.RelationClasses.StaticAdvertisementTagEntertainmentRelations.EntertainmentEntityUsingEntertainmentIdStatic, true, ref _alreadyFetchedEntertainmentEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnEntertainmentEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="advertisementTagEntertainmentId">PK value for AdvertisementTagEntertainment which data should be fetched into this AdvertisementTagEntertainment object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int32 advertisementTagEntertainmentId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)AdvertisementTagEntertainmentFieldIndex.AdvertisementTagEntertainmentId].ForcedCurrentValueWrite(advertisementTagEntertainmentId);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateAdvertisementTagEntertainmentDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new AdvertisementTagEntertainmentEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static AdvertisementTagEntertainmentRelations Relations
		{
			get	{ return new AdvertisementTagEntertainmentRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'AdvertisementTag'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathAdvertisementTagEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.AdvertisementTagCollection(), (IEntityRelation)GetRelationsForField("AdvertisementTagEntity")[0], (int)Obymobi.Data.EntityType.AdvertisementTagEntertainmentEntity, (int)Obymobi.Data.EntityType.AdvertisementTagEntity, 0, null, null, null, "AdvertisementTagEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Entertainment'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathEntertainmentEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.EntertainmentCollection(), (IEntityRelation)GetRelationsForField("EntertainmentEntity")[0], (int)Obymobi.Data.EntityType.AdvertisementTagEntertainmentEntity, (int)Obymobi.Data.EntityType.EntertainmentEntity, 0, null, null, null, "EntertainmentEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The AdvertisementTagEntertainmentId property of the Entity AdvertisementTagEntertainment<br/><br/></summary>
		/// <remarks>Mapped on  table field: "AdvertisementTagEntertainment"."AdvertisementTagEntertainmentId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 AdvertisementTagEntertainmentId
		{
			get { return (System.Int32)GetValue((int)AdvertisementTagEntertainmentFieldIndex.AdvertisementTagEntertainmentId, true); }
			set	{ SetValue((int)AdvertisementTagEntertainmentFieldIndex.AdvertisementTagEntertainmentId, value, true); }
		}

		/// <summary> The EntertainmentId property of the Entity AdvertisementTagEntertainment<br/><br/></summary>
		/// <remarks>Mapped on  table field: "AdvertisementTagEntertainment"."EntertainmentId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 EntertainmentId
		{
			get { return (System.Int32)GetValue((int)AdvertisementTagEntertainmentFieldIndex.EntertainmentId, true); }
			set	{ SetValue((int)AdvertisementTagEntertainmentFieldIndex.EntertainmentId, value, true); }
		}

		/// <summary> The AdvertisementTagId property of the Entity AdvertisementTagEntertainment<br/><br/></summary>
		/// <remarks>Mapped on  table field: "AdvertisementTagEntertainment"."AdvertisementTagId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 AdvertisementTagId
		{
			get { return (System.Int32)GetValue((int)AdvertisementTagEntertainmentFieldIndex.AdvertisementTagId, true); }
			set	{ SetValue((int)AdvertisementTagEntertainmentFieldIndex.AdvertisementTagId, value, true); }
		}

		/// <summary> The CreatedBy property of the Entity AdvertisementTagEntertainment<br/><br/></summary>
		/// <remarks>Mapped on  table field: "AdvertisementTagEntertainment"."CreatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> CreatedBy
		{
			get { return (Nullable<System.Int32>)GetValue((int)AdvertisementTagEntertainmentFieldIndex.CreatedBy, false); }
			set	{ SetValue((int)AdvertisementTagEntertainmentFieldIndex.CreatedBy, value, true); }
		}

		/// <summary> The UpdatedBy property of the Entity AdvertisementTagEntertainment<br/><br/></summary>
		/// <remarks>Mapped on  table field: "AdvertisementTagEntertainment"."UpdatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> UpdatedBy
		{
			get { return (Nullable<System.Int32>)GetValue((int)AdvertisementTagEntertainmentFieldIndex.UpdatedBy, false); }
			set	{ SetValue((int)AdvertisementTagEntertainmentFieldIndex.UpdatedBy, value, true); }
		}

		/// <summary> The ParentCompanyId property of the Entity AdvertisementTagEntertainment<br/><br/></summary>
		/// <remarks>Mapped on  table field: "AdvertisementTagEntertainment"."ParentCompanyId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ParentCompanyId
		{
			get { return (Nullable<System.Int32>)GetValue((int)AdvertisementTagEntertainmentFieldIndex.ParentCompanyId, false); }
			set	{ SetValue((int)AdvertisementTagEntertainmentFieldIndex.ParentCompanyId, value, true); }
		}

		/// <summary> The CreatedUTC property of the Entity AdvertisementTagEntertainment<br/><br/></summary>
		/// <remarks>Mapped on  table field: "AdvertisementTagEntertainment"."CreatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CreatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)AdvertisementTagEntertainmentFieldIndex.CreatedUTC, false); }
			set	{ SetValue((int)AdvertisementTagEntertainmentFieldIndex.CreatedUTC, value, true); }
		}

		/// <summary> The UpdatedUTC property of the Entity AdvertisementTagEntertainment<br/><br/></summary>
		/// <remarks>Mapped on  table field: "AdvertisementTagEntertainment"."UpdatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)AdvertisementTagEntertainmentFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)AdvertisementTagEntertainmentFieldIndex.UpdatedUTC, value, true); }
		}


		/// <summary> Gets / sets related entity of type 'AdvertisementTagEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleAdvertisementTagEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual AdvertisementTagEntity AdvertisementTagEntity
		{
			get	{ return GetSingleAdvertisementTagEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncAdvertisementTagEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "AdvertisementTagEntertainmentCollection", "AdvertisementTagEntity", _advertisementTagEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for AdvertisementTagEntity. When set to true, AdvertisementTagEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time AdvertisementTagEntity is accessed. You can always execute a forced fetch by calling GetSingleAdvertisementTagEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchAdvertisementTagEntity
		{
			get	{ return _alwaysFetchAdvertisementTagEntity; }
			set	{ _alwaysFetchAdvertisementTagEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property AdvertisementTagEntity already has been fetched. Setting this property to false when AdvertisementTagEntity has been fetched
		/// will set AdvertisementTagEntity to null as well. Setting this property to true while AdvertisementTagEntity hasn't been fetched disables lazy loading for AdvertisementTagEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedAdvertisementTagEntity
		{
			get { return _alreadyFetchedAdvertisementTagEntity;}
			set 
			{
				if(_alreadyFetchedAdvertisementTagEntity && !value)
				{
					this.AdvertisementTagEntity = null;
				}
				_alreadyFetchedAdvertisementTagEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property AdvertisementTagEntity is not found
		/// in the database. When set to true, AdvertisementTagEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool AdvertisementTagEntityReturnsNewIfNotFound
		{
			get	{ return _advertisementTagEntityReturnsNewIfNotFound; }
			set { _advertisementTagEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'EntertainmentEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleEntertainmentEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual EntertainmentEntity EntertainmentEntity
		{
			get	{ return GetSingleEntertainmentEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncEntertainmentEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "AdvertisementTagEntertainmentCollection", "EntertainmentEntity", _entertainmentEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for EntertainmentEntity. When set to true, EntertainmentEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time EntertainmentEntity is accessed. You can always execute a forced fetch by calling GetSingleEntertainmentEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchEntertainmentEntity
		{
			get	{ return _alwaysFetchEntertainmentEntity; }
			set	{ _alwaysFetchEntertainmentEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property EntertainmentEntity already has been fetched. Setting this property to false when EntertainmentEntity has been fetched
		/// will set EntertainmentEntity to null as well. Setting this property to true while EntertainmentEntity hasn't been fetched disables lazy loading for EntertainmentEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedEntertainmentEntity
		{
			get { return _alreadyFetchedEntertainmentEntity;}
			set 
			{
				if(_alreadyFetchedEntertainmentEntity && !value)
				{
					this.EntertainmentEntity = null;
				}
				_alreadyFetchedEntertainmentEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property EntertainmentEntity is not found
		/// in the database. When set to true, EntertainmentEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool EntertainmentEntityReturnsNewIfNotFound
		{
			get	{ return _entertainmentEntityReturnsNewIfNotFound; }
			set { _entertainmentEntityReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the Obymobi.Data.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)Obymobi.Data.EntityType.AdvertisementTagEntertainmentEntity; }
		}

		#endregion

		
		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
