﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
using System.Runtime.Serialization;
using System.Data;
using System.Xml.Serialization;
using Obymobi.Data;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.RelationClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Dionysos.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity base class which represents the base class for the entity 'ReportProcessingTaskTemplate'.<br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public abstract partial class ReportProcessingTaskTemplateEntityBase : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		public new string LLBLGenProEntityName {
			get { return "ReportProcessingTaskTemplateEntity"; }
		}
	
		#region Class Member Declarations
		private Obymobi.Data.CollectionClasses.UIScheduleItemCollection	_uIScheduleItemCollection;
		private bool	_alwaysFetchUIScheduleItemCollection, _alreadyFetchedUIScheduleItemCollection;
		private Obymobi.Data.CollectionClasses.UIScheduleItemOccurrenceCollection	_uIScheduleItemOccurrenceCollection;
		private bool	_alwaysFetchUIScheduleItemOccurrenceCollection, _alreadyFetchedUIScheduleItemOccurrenceCollection;
		private CompanyEntity _companyEntity;
		private bool	_alwaysFetchCompanyEntity, _alreadyFetchedCompanyEntity, _companyEntityReturnsNewIfNotFound;
		private ReportProcessingTaskEntity _reportProcessingTaskEntity;
		private bool	_alwaysFetchReportProcessingTaskEntity, _alreadyFetchedReportProcessingTaskEntity, _reportProcessingTaskEntityReturnsNewIfNotFound;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name CompanyEntity</summary>
			public static readonly string CompanyEntity = "CompanyEntity";
			/// <summary>Member name ReportProcessingTaskEntity</summary>
			public static readonly string ReportProcessingTaskEntity = "ReportProcessingTaskEntity";
			/// <summary>Member name UIScheduleItemCollection</summary>
			public static readonly string UIScheduleItemCollection = "UIScheduleItemCollection";
			/// <summary>Member name UIScheduleItemOccurrenceCollection</summary>
			public static readonly string UIScheduleItemOccurrenceCollection = "UIScheduleItemOccurrenceCollection";
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static ReportProcessingTaskTemplateEntityBase()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		protected ReportProcessingTaskTemplateEntityBase() :base("ReportProcessingTaskTemplateEntity")
		{
			InitClassEmpty(null);
		}

		/// <summary>CTor</summary>
		/// <param name="reportProcessingTaskTemplateId">PK value for ReportProcessingTaskTemplate which data should be fetched into this ReportProcessingTaskTemplate object</param>
		protected ReportProcessingTaskTemplateEntityBase(System.Int32 reportProcessingTaskTemplateId):base("ReportProcessingTaskTemplateEntity")
		{
			InitClassFetch(reportProcessingTaskTemplateId, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="reportProcessingTaskTemplateId">PK value for ReportProcessingTaskTemplate which data should be fetched into this ReportProcessingTaskTemplate object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		protected ReportProcessingTaskTemplateEntityBase(System.Int32 reportProcessingTaskTemplateId, IPrefetchPath prefetchPathToUse): base("ReportProcessingTaskTemplateEntity")
		{
			InitClassFetch(reportProcessingTaskTemplateId, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="reportProcessingTaskTemplateId">PK value for ReportProcessingTaskTemplate which data should be fetched into this ReportProcessingTaskTemplate object</param>
		/// <param name="validator">The custom validator object for this ReportProcessingTaskTemplateEntity</param>
		protected ReportProcessingTaskTemplateEntityBase(System.Int32 reportProcessingTaskTemplateId, IValidator validator):base("ReportProcessingTaskTemplateEntity")
		{
			InitClassFetch(reportProcessingTaskTemplateId, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected ReportProcessingTaskTemplateEntityBase(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			_uIScheduleItemCollection = (Obymobi.Data.CollectionClasses.UIScheduleItemCollection)info.GetValue("_uIScheduleItemCollection", typeof(Obymobi.Data.CollectionClasses.UIScheduleItemCollection));
			_alwaysFetchUIScheduleItemCollection = info.GetBoolean("_alwaysFetchUIScheduleItemCollection");
			_alreadyFetchedUIScheduleItemCollection = info.GetBoolean("_alreadyFetchedUIScheduleItemCollection");

			_uIScheduleItemOccurrenceCollection = (Obymobi.Data.CollectionClasses.UIScheduleItemOccurrenceCollection)info.GetValue("_uIScheduleItemOccurrenceCollection", typeof(Obymobi.Data.CollectionClasses.UIScheduleItemOccurrenceCollection));
			_alwaysFetchUIScheduleItemOccurrenceCollection = info.GetBoolean("_alwaysFetchUIScheduleItemOccurrenceCollection");
			_alreadyFetchedUIScheduleItemOccurrenceCollection = info.GetBoolean("_alreadyFetchedUIScheduleItemOccurrenceCollection");
			_companyEntity = (CompanyEntity)info.GetValue("_companyEntity", typeof(CompanyEntity));
			if(_companyEntity!=null)
			{
				_companyEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_companyEntityReturnsNewIfNotFound = info.GetBoolean("_companyEntityReturnsNewIfNotFound");
			_alwaysFetchCompanyEntity = info.GetBoolean("_alwaysFetchCompanyEntity");
			_alreadyFetchedCompanyEntity = info.GetBoolean("_alreadyFetchedCompanyEntity");

			_reportProcessingTaskEntity = (ReportProcessingTaskEntity)info.GetValue("_reportProcessingTaskEntity", typeof(ReportProcessingTaskEntity));
			if(_reportProcessingTaskEntity!=null)
			{
				_reportProcessingTaskEntity.AfterSave+=new EventHandler(OnEntityAfterSave);
			}
			_reportProcessingTaskEntityReturnsNewIfNotFound = info.GetBoolean("_reportProcessingTaskEntityReturnsNewIfNotFound");
			_alwaysFetchReportProcessingTaskEntity = info.GetBoolean("_alwaysFetchReportProcessingTaskEntity");
			_alreadyFetchedReportProcessingTaskEntity = info.GetBoolean("_alreadyFetchedReportProcessingTaskEntity");
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}	
		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((ReportProcessingTaskTemplateFieldIndex)fieldIndex)
			{
				case ReportProcessingTaskTemplateFieldIndex.ReportProcessingTaskId:
					DesetupSyncReportProcessingTaskEntity(true, false);
					_alreadyFetchedReportProcessingTaskEntity = false;
					break;
				case ReportProcessingTaskTemplateFieldIndex.CompanyId:
					DesetupSyncCompanyEntity(true, false);
					_alreadyFetchedCompanyEntity = false;
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
			_alreadyFetchedUIScheduleItemCollection = (_uIScheduleItemCollection.Count > 0);
			_alreadyFetchedUIScheduleItemOccurrenceCollection = (_uIScheduleItemOccurrenceCollection.Count > 0);
			_alreadyFetchedCompanyEntity = (_companyEntity != null);
			_alreadyFetchedReportProcessingTaskEntity = (_reportProcessingTaskEntity != null);
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "CompanyEntity":
					toReturn.Add(Relations.CompanyEntityUsingCompanyId);
					break;
				case "ReportProcessingTaskEntity":
					toReturn.Add(Relations.ReportProcessingTaskEntityUsingReportProcessingTaskId);
					break;
				case "UIScheduleItemCollection":
					toReturn.Add(Relations.UIScheduleItemEntityUsingReportProcessingTaskTemplateId);
					break;
				case "UIScheduleItemOccurrenceCollection":
					toReturn.Add(Relations.UIScheduleItemOccurrenceEntityUsingReportProcessingTaskTemplateId);
					break;
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_uIScheduleItemCollection", (!this.MarkedForDeletion?_uIScheduleItemCollection:null));
			info.AddValue("_alwaysFetchUIScheduleItemCollection", _alwaysFetchUIScheduleItemCollection);
			info.AddValue("_alreadyFetchedUIScheduleItemCollection", _alreadyFetchedUIScheduleItemCollection);
			info.AddValue("_uIScheduleItemOccurrenceCollection", (!this.MarkedForDeletion?_uIScheduleItemOccurrenceCollection:null));
			info.AddValue("_alwaysFetchUIScheduleItemOccurrenceCollection", _alwaysFetchUIScheduleItemOccurrenceCollection);
			info.AddValue("_alreadyFetchedUIScheduleItemOccurrenceCollection", _alreadyFetchedUIScheduleItemOccurrenceCollection);
			info.AddValue("_companyEntity", (!this.MarkedForDeletion?_companyEntity:null));
			info.AddValue("_companyEntityReturnsNewIfNotFound", _companyEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchCompanyEntity", _alwaysFetchCompanyEntity);
			info.AddValue("_alreadyFetchedCompanyEntity", _alreadyFetchedCompanyEntity);
			info.AddValue("_reportProcessingTaskEntity", (!this.MarkedForDeletion?_reportProcessingTaskEntity:null));
			info.AddValue("_reportProcessingTaskEntityReturnsNewIfNotFound", _reportProcessingTaskEntityReturnsNewIfNotFound);
			info.AddValue("_alwaysFetchReportProcessingTaskEntity", _alwaysFetchReportProcessingTaskEntity);
			info.AddValue("_alreadyFetchedReportProcessingTaskEntity", _alreadyFetchedReportProcessingTaskEntity);

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "CompanyEntity":
					_alreadyFetchedCompanyEntity = true;
					this.CompanyEntity = (CompanyEntity)entity;
					break;
				case "ReportProcessingTaskEntity":
					_alreadyFetchedReportProcessingTaskEntity = true;
					this.ReportProcessingTaskEntity = (ReportProcessingTaskEntity)entity;
					break;
				case "UIScheduleItemCollection":
					_alreadyFetchedUIScheduleItemCollection = true;
					if(entity!=null)
					{
						this.UIScheduleItemCollection.Add((UIScheduleItemEntity)entity);
					}
					break;
				case "UIScheduleItemOccurrenceCollection":
					_alreadyFetchedUIScheduleItemOccurrenceCollection = true;
					if(entity!=null)
					{
						this.UIScheduleItemOccurrenceCollection.Add((UIScheduleItemOccurrenceEntity)entity);
					}
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "CompanyEntity":
					SetupSyncCompanyEntity(relatedEntity);
					break;
				case "ReportProcessingTaskEntity":
					SetupSyncReportProcessingTaskEntity(relatedEntity);
					break;
				case "UIScheduleItemCollection":
					_uIScheduleItemCollection.Add((UIScheduleItemEntity)relatedEntity);
					break;
				case "UIScheduleItemOccurrenceCollection":
					_uIScheduleItemOccurrenceCollection.Add((UIScheduleItemOccurrenceEntity)relatedEntity);
					break;
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "CompanyEntity":
					DesetupSyncCompanyEntity(false, true);
					break;
				case "ReportProcessingTaskEntity":
					DesetupSyncReportProcessingTaskEntity(false, true);
					break;
				case "UIScheduleItemCollection":
					this.PerformRelatedEntityRemoval(_uIScheduleItemCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "UIScheduleItemOccurrenceCollection":
					this.PerformRelatedEntityRemoval(_uIScheduleItemOccurrenceCollection, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			if(_companyEntity!=null)
			{
				toReturn.Add(_companyEntity);
			}
			if(_reportProcessingTaskEntity!=null)
			{
				toReturn.Add(_reportProcessingTaskEntity);
			}
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();
			toReturn.Add(_uIScheduleItemCollection);
			toReturn.Add(_uIScheduleItemOccurrenceCollection);

			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="reportProcessingTaskTemplateId">PK value for ReportProcessingTaskTemplate which data should be fetched into this ReportProcessingTaskTemplate object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 reportProcessingTaskTemplateId)
		{
			return FetchUsingPK(reportProcessingTaskTemplateId, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="reportProcessingTaskTemplateId">PK value for ReportProcessingTaskTemplate which data should be fetched into this ReportProcessingTaskTemplate object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 reportProcessingTaskTemplateId, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(reportProcessingTaskTemplateId, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="reportProcessingTaskTemplateId">PK value for ReportProcessingTaskTemplate which data should be fetched into this ReportProcessingTaskTemplate object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 reportProcessingTaskTemplateId, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(reportProcessingTaskTemplateId, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="reportProcessingTaskTemplateId">PK value for ReportProcessingTaskTemplate which data should be fetched into this ReportProcessingTaskTemplate object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int32 reportProcessingTaskTemplateId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(reportProcessingTaskTemplateId, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.ReportProcessingTaskTemplateId, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new ReportProcessingTaskTemplateRelations().GetAllRelations();
		}

		/// <summary> Retrieves all related entities of type 'UIScheduleItemEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'UIScheduleItemEntity'</returns>
		public Obymobi.Data.CollectionClasses.UIScheduleItemCollection GetMultiUIScheduleItemCollection(bool forceFetch)
		{
			return GetMultiUIScheduleItemCollection(forceFetch, _uIScheduleItemCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'UIScheduleItemEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'UIScheduleItemEntity'</returns>
		public Obymobi.Data.CollectionClasses.UIScheduleItemCollection GetMultiUIScheduleItemCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiUIScheduleItemCollection(forceFetch, _uIScheduleItemCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'UIScheduleItemEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.UIScheduleItemCollection GetMultiUIScheduleItemCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiUIScheduleItemCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'UIScheduleItemEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.UIScheduleItemCollection GetMultiUIScheduleItemCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedUIScheduleItemCollection || forceFetch || _alwaysFetchUIScheduleItemCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_uIScheduleItemCollection);
				_uIScheduleItemCollection.SuppressClearInGetMulti=!forceFetch;
				_uIScheduleItemCollection.EntityFactoryToUse = entityFactoryToUse;
				_uIScheduleItemCollection.GetMultiManyToOne(null, this, null, null, null, filter);
				_uIScheduleItemCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedUIScheduleItemCollection = true;
			}
			return _uIScheduleItemCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'UIScheduleItemCollection'. These settings will be taken into account
		/// when the property UIScheduleItemCollection is requested or GetMultiUIScheduleItemCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersUIScheduleItemCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_uIScheduleItemCollection.SortClauses=sortClauses;
			_uIScheduleItemCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves all related entities of type 'UIScheduleItemOccurrenceEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <returns>Filled collection with all related entities of type 'UIScheduleItemOccurrenceEntity'</returns>
		public Obymobi.Data.CollectionClasses.UIScheduleItemOccurrenceCollection GetMultiUIScheduleItemOccurrenceCollection(bool forceFetch)
		{
			return GetMultiUIScheduleItemOccurrenceCollection(forceFetch, _uIScheduleItemOccurrenceCollection.EntityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'UIScheduleItemOccurrenceEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of type 'UIScheduleItemOccurrenceEntity'</returns>
		public Obymobi.Data.CollectionClasses.UIScheduleItemOccurrenceCollection GetMultiUIScheduleItemOccurrenceCollection(bool forceFetch, IPredicateExpression filter)
		{
			return GetMultiUIScheduleItemOccurrenceCollection(forceFetch, _uIScheduleItemOccurrenceCollection.EntityFactoryToUse, filter);
		}

		/// <summary> Retrieves all related entities of type 'UIScheduleItemOccurrenceEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public Obymobi.Data.CollectionClasses.UIScheduleItemOccurrenceCollection GetMultiUIScheduleItemOccurrenceCollection(bool forceFetch, IEntityFactory entityFactoryToUse)
		{
			return GetMultiUIScheduleItemOccurrenceCollection(forceFetch, entityFactoryToUse, null);
		}

		/// <summary> Retrieves all related entities of type 'UIScheduleItemOccurrenceEntity' using a relation of type '1:n'.</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the collection and will rerun the complete query instead</param>
		/// <param name="entityFactoryToUse">The entity factory to use for the GetMultiManyToOne() routine.</param>
		/// <param name="filter">Extra filter to limit the resultset.</param>
		/// <returns>Filled collection with all related entities of the type constructed by the passed in entity factory</returns>
		public virtual Obymobi.Data.CollectionClasses.UIScheduleItemOccurrenceCollection GetMultiUIScheduleItemOccurrenceCollection(bool forceFetch, IEntityFactory entityFactoryToUse, IPredicateExpression filter)
		{
 			if( ( !_alreadyFetchedUIScheduleItemOccurrenceCollection || forceFetch || _alwaysFetchUIScheduleItemOccurrenceCollection) && !this.IsSerializing && !this.IsDeserializing && !this.InDesignMode)
			{
				AddToTransactionIfNecessary(_uIScheduleItemOccurrenceCollection);
				_uIScheduleItemOccurrenceCollection.SuppressClearInGetMulti=!forceFetch;
				_uIScheduleItemOccurrenceCollection.EntityFactoryToUse = entityFactoryToUse;
				_uIScheduleItemOccurrenceCollection.GetMultiManyToOne(null, this, null, null, null, filter);
				_uIScheduleItemOccurrenceCollection.SuppressClearInGetMulti=false;
				_alreadyFetchedUIScheduleItemOccurrenceCollection = true;
			}
			return _uIScheduleItemOccurrenceCollection;
		}

		/// <summary> Sets the collection parameters for the collection for 'UIScheduleItemOccurrenceCollection'. These settings will be taken into account
		/// when the property UIScheduleItemOccurrenceCollection is requested or GetMultiUIScheduleItemOccurrenceCollection is called.</summary>
		/// <param name="maxNumberOfItemsToReturn"> The maximum number of items to return. When set to 0, this parameter is ignored</param>
		/// <param name="sortClauses">The order by specifications for the sorting of the resultset. When not specified (null), no sorting is applied.</param>
		public virtual void SetCollectionParametersUIScheduleItemOccurrenceCollection(long maxNumberOfItemsToReturn, ISortExpression sortClauses)
		{
			_uIScheduleItemOccurrenceCollection.SortClauses=sortClauses;
			_uIScheduleItemOccurrenceCollection.MaxNumberOfItemsToReturn=maxNumberOfItemsToReturn;
		}

		/// <summary> Retrieves the related entity of type 'CompanyEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'CompanyEntity' which is related to this entity.</returns>
		public CompanyEntity GetSingleCompanyEntity()
		{
			return GetSingleCompanyEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'CompanyEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'CompanyEntity' which is related to this entity.</returns>
		public virtual CompanyEntity GetSingleCompanyEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedCompanyEntity || forceFetch || _alwaysFetchCompanyEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.CompanyEntityUsingCompanyId);
				CompanyEntity newEntity = new CompanyEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.CompanyId);
				}
				if(fetchResult)
				{
					newEntity = (CompanyEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_companyEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.CompanyEntity = newEntity;
				_alreadyFetchedCompanyEntity = fetchResult;
			}
			return _companyEntity;
		}


		/// <summary> Retrieves the related entity of type 'ReportProcessingTaskEntity', using a relation of type 'n:1'</summary>
		/// <returns>A fetched entity of type 'ReportProcessingTaskEntity' which is related to this entity.</returns>
		public ReportProcessingTaskEntity GetSingleReportProcessingTaskEntity()
		{
			return GetSingleReportProcessingTaskEntity(false);
		}

		/// <summary> Retrieves the related entity of type 'ReportProcessingTaskEntity', using a relation of type 'n:1'</summary>
		/// <param name="forceFetch">if true, it will discard any changes currently in the currently loaded related entity and will refetch the entity from the persistent storage</param>
		/// <returns>A fetched entity of type 'ReportProcessingTaskEntity' which is related to this entity.</returns>
		public virtual ReportProcessingTaskEntity GetSingleReportProcessingTaskEntity(bool forceFetch)
		{
			if( ( !_alreadyFetchedReportProcessingTaskEntity || forceFetch || _alwaysFetchReportProcessingTaskEntity) && !this.IsSerializing && !this.IsDeserializing  && !this.InDesignMode)			
			{
				bool performLazyLoading = this.CheckIfLazyLoadingShouldOccur(Relations.ReportProcessingTaskEntityUsingReportProcessingTaskId);
				ReportProcessingTaskEntity newEntity = new ReportProcessingTaskEntity();
				bool fetchResult = false;
				if(performLazyLoading)
				{
					AddToTransactionIfNecessary(newEntity);
					fetchResult = newEntity.FetchUsingPK(this.ReportProcessingTaskId.GetValueOrDefault());
				}
				if(fetchResult)
				{
					newEntity = (ReportProcessingTaskEntity)GetFromActiveContext(newEntity);
				}
				else
				{
					if(!_reportProcessingTaskEntityReturnsNewIfNotFound)
					{
						RemoveFromTransactionIfNecessary(newEntity);
						newEntity = null;
					}
				}
				this.ReportProcessingTaskEntity = newEntity;
				_alreadyFetchedReportProcessingTaskEntity = fetchResult;
			}
			return _reportProcessingTaskEntity;
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("CompanyEntity", _companyEntity);
			toReturn.Add("ReportProcessingTaskEntity", _reportProcessingTaskEntity);
			toReturn.Add("UIScheduleItemCollection", _uIScheduleItemCollection);
			toReturn.Add("UIScheduleItemOccurrenceCollection", _uIScheduleItemOccurrenceCollection);
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="reportProcessingTaskTemplateId">PK value for ReportProcessingTaskTemplate which data should be fetched into this ReportProcessingTaskTemplate object</param>
		/// <param name="validator">The validator object for this ReportProcessingTaskTemplateEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int32 reportProcessingTaskTemplateId, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(reportProcessingTaskTemplateId, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{

			_uIScheduleItemCollection = new Obymobi.Data.CollectionClasses.UIScheduleItemCollection();
			_uIScheduleItemCollection.SetContainingEntityInfo(this, "ReportProcessingTaskTemplateEntity");

			_uIScheduleItemOccurrenceCollection = new Obymobi.Data.CollectionClasses.UIScheduleItemOccurrenceCollection();
			_uIScheduleItemOccurrenceCollection.SetContainingEntityInfo(this, "ReportProcessingTaskTemplateEntity");
			_companyEntityReturnsNewIfNotFound = true;
			_reportProcessingTaskEntityReturnsNewIfNotFound = true;
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ReportProcessingTaskTemplateId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ReportProcessingTaskId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TemplateFile", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedBy", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CreatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("UpdatedUTC", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CompanyId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Filter", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ReportingPeriod", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("FriendlyName", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Email", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ReportType", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _companyEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncCompanyEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _companyEntity, new PropertyChangedEventHandler( OnCompanyEntityPropertyChanged ), "CompanyEntity", Obymobi.Data.RelationClasses.StaticReportProcessingTaskTemplateRelations.CompanyEntityUsingCompanyIdStatic, true, signalRelatedEntity, "ReportProcessingTaskTemplateCollection", resetFKFields, new int[] { (int)ReportProcessingTaskTemplateFieldIndex.CompanyId } );		
			_companyEntity = null;
		}
		
		/// <summary> setups the sync logic for member _companyEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncCompanyEntity(IEntityCore relatedEntity)
		{
			if(_companyEntity!=relatedEntity)
			{		
				DesetupSyncCompanyEntity(true, true);
				_companyEntity = (CompanyEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _companyEntity, new PropertyChangedEventHandler( OnCompanyEntityPropertyChanged ), "CompanyEntity", Obymobi.Data.RelationClasses.StaticReportProcessingTaskTemplateRelations.CompanyEntityUsingCompanyIdStatic, true, ref _alreadyFetchedCompanyEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnCompanyEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _reportProcessingTaskEntity</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncReportProcessingTaskEntity(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _reportProcessingTaskEntity, new PropertyChangedEventHandler( OnReportProcessingTaskEntityPropertyChanged ), "ReportProcessingTaskEntity", Obymobi.Data.RelationClasses.StaticReportProcessingTaskTemplateRelations.ReportProcessingTaskEntityUsingReportProcessingTaskIdStatic, true, signalRelatedEntity, "ReportProcessingTaskTemplateCollection", resetFKFields, new int[] { (int)ReportProcessingTaskTemplateFieldIndex.ReportProcessingTaskId } );		
			_reportProcessingTaskEntity = null;
		}
		
		/// <summary> setups the sync logic for member _reportProcessingTaskEntity</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncReportProcessingTaskEntity(IEntityCore relatedEntity)
		{
			if(_reportProcessingTaskEntity!=relatedEntity)
			{		
				DesetupSyncReportProcessingTaskEntity(true, true);
				_reportProcessingTaskEntity = (ReportProcessingTaskEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _reportProcessingTaskEntity, new PropertyChangedEventHandler( OnReportProcessingTaskEntityPropertyChanged ), "ReportProcessingTaskEntity", Obymobi.Data.RelationClasses.StaticReportProcessingTaskTemplateRelations.ReportProcessingTaskEntityUsingReportProcessingTaskIdStatic, true, ref _alreadyFetchedReportProcessingTaskEntity, new string[] {  } );
			}
		}

		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnReportProcessingTaskEntityPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="reportProcessingTaskTemplateId">PK value for ReportProcessingTaskTemplate which data should be fetched into this ReportProcessingTaskTemplate object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int32 reportProcessingTaskTemplateId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)ReportProcessingTaskTemplateFieldIndex.ReportProcessingTaskTemplateId].ForcedCurrentValueWrite(reportProcessingTaskTemplateId);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateReportProcessingTaskTemplateDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new ReportProcessingTaskTemplateEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static ReportProcessingTaskTemplateRelations Relations
		{
			get	{ return new ReportProcessingTaskTemplateRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'UIScheduleItem' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathUIScheduleItemCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.UIScheduleItemCollection(), (IEntityRelation)GetRelationsForField("UIScheduleItemCollection")[0], (int)Obymobi.Data.EntityType.ReportProcessingTaskTemplateEntity, (int)Obymobi.Data.EntityType.UIScheduleItemEntity, 0, null, null, null, "UIScheduleItemCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'UIScheduleItemOccurrence' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathUIScheduleItemOccurrenceCollection
		{
			get { return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.UIScheduleItemOccurrenceCollection(), (IEntityRelation)GetRelationsForField("UIScheduleItemOccurrenceCollection")[0], (int)Obymobi.Data.EntityType.ReportProcessingTaskTemplateEntity, (int)Obymobi.Data.EntityType.UIScheduleItemOccurrenceEntity, 0, null, null, null, "UIScheduleItemOccurrenceCollection", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'Company'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathCompanyEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.CompanyCollection(), (IEntityRelation)GetRelationsForField("CompanyEntity")[0], (int)Obymobi.Data.EntityType.ReportProcessingTaskTemplateEntity, (int)Obymobi.Data.EntityType.CompanyEntity, 0, null, null, null, "CompanyEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement object which contains all the information to prefetch the related entities of type 'ReportProcessingTask'  for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement implementation.</returns>
		public static IPrefetchPathElement PrefetchPathReportProcessingTaskEntity
		{
			get	{ return new PrefetchPathElement(new Obymobi.Data.CollectionClasses.ReportProcessingTaskCollection(), (IEntityRelation)GetRelationsForField("ReportProcessingTaskEntity")[0], (int)Obymobi.Data.EntityType.ReportProcessingTaskTemplateEntity, (int)Obymobi.Data.EntityType.ReportProcessingTaskEntity, 0, null, null, null, "ReportProcessingTaskEntity", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The ReportProcessingTaskTemplateId property of the Entity ReportProcessingTaskTemplate<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ReportProcessingTaskTemplate"."ReportProcessingTaskTemplateId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 ReportProcessingTaskTemplateId
		{
			get { return (System.Int32)GetValue((int)ReportProcessingTaskTemplateFieldIndex.ReportProcessingTaskTemplateId, true); }
			set	{ SetValue((int)ReportProcessingTaskTemplateFieldIndex.ReportProcessingTaskTemplateId, value, true); }
		}

		/// <summary> The ReportProcessingTaskId property of the Entity ReportProcessingTaskTemplate<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ReportProcessingTaskTemplate"."ReportProcessingTaskId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ReportProcessingTaskId
		{
			get { return (Nullable<System.Int32>)GetValue((int)ReportProcessingTaskTemplateFieldIndex.ReportProcessingTaskId, false); }
			set	{ SetValue((int)ReportProcessingTaskTemplateFieldIndex.ReportProcessingTaskId, value, true); }
		}

		/// <summary> The TemplateFile property of the Entity ReportProcessingTaskTemplate<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ReportProcessingTaskTemplate"."TemplateFile"<br/>
		/// Table field type characteristics (type, precision, scale, length): VarBinary, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.Byte[] TemplateFile
		{
			get { return (System.Byte[])GetValue((int)ReportProcessingTaskTemplateFieldIndex.TemplateFile, true); }
			set	{ SetValue((int)ReportProcessingTaskTemplateFieldIndex.TemplateFile, value, true); }
		}

		/// <summary> The CreatedBy property of the Entity ReportProcessingTaskTemplate<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ReportProcessingTaskTemplate"."CreatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CreatedBy
		{
			get { return (System.Int32)GetValue((int)ReportProcessingTaskTemplateFieldIndex.CreatedBy, true); }
			set	{ SetValue((int)ReportProcessingTaskTemplateFieldIndex.CreatedBy, value, true); }
		}

		/// <summary> The UpdatedBy property of the Entity ReportProcessingTaskTemplate<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ReportProcessingTaskTemplate"."UpdatedBy"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 UpdatedBy
		{
			get { return (System.Int32)GetValue((int)ReportProcessingTaskTemplateFieldIndex.UpdatedBy, true); }
			set	{ SetValue((int)ReportProcessingTaskTemplateFieldIndex.UpdatedBy, value, true); }
		}

		/// <summary> The CreatedUTC property of the Entity ReportProcessingTaskTemplate<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ReportProcessingTaskTemplate"."CreatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> CreatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)ReportProcessingTaskTemplateFieldIndex.CreatedUTC, false); }
			set	{ SetValue((int)ReportProcessingTaskTemplateFieldIndex.CreatedUTC, value, true); }
		}

		/// <summary> The UpdatedUTC property of the Entity ReportProcessingTaskTemplate<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ReportProcessingTaskTemplate"."UpdatedUTC"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime2, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.DateTime> UpdatedUTC
		{
			get { return (Nullable<System.DateTime>)GetValue((int)ReportProcessingTaskTemplateFieldIndex.UpdatedUTC, false); }
			set	{ SetValue((int)ReportProcessingTaskTemplateFieldIndex.UpdatedUTC, value, true); }
		}

		/// <summary> The CompanyId property of the Entity ReportProcessingTaskTemplate<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ReportProcessingTaskTemplate"."CompanyId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 CompanyId
		{
			get { return (System.Int32)GetValue((int)ReportProcessingTaskTemplateFieldIndex.CompanyId, true); }
			set	{ SetValue((int)ReportProcessingTaskTemplateFieldIndex.CompanyId, value, true); }
		}

		/// <summary> The Filter property of the Entity ReportProcessingTaskTemplate<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ReportProcessingTaskTemplate"."Filter"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2147483647<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Filter
		{
			get { return (System.String)GetValue((int)ReportProcessingTaskTemplateFieldIndex.Filter, true); }
			set	{ SetValue((int)ReportProcessingTaskTemplateFieldIndex.Filter, value, true); }
		}

		/// <summary> The ReportingPeriod property of the Entity ReportProcessingTaskTemplate<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ReportProcessingTaskTemplate"."ReportingPeriod"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual Obymobi.Enums.ReportingPeriod ReportingPeriod
		{
			get { return (Obymobi.Enums.ReportingPeriod)GetValue((int)ReportProcessingTaskTemplateFieldIndex.ReportingPeriod, true); }
			set	{ SetValue((int)ReportProcessingTaskTemplateFieldIndex.ReportingPeriod, value, true); }
		}

		/// <summary> The FriendlyName property of the Entity ReportProcessingTaskTemplate<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ReportProcessingTaskTemplate"."FriendlyName"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 255<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String FriendlyName
		{
			get { return (System.String)GetValue((int)ReportProcessingTaskTemplateFieldIndex.FriendlyName, true); }
			set	{ SetValue((int)ReportProcessingTaskTemplateFieldIndex.FriendlyName, value, true); }
		}

		/// <summary> The Email property of the Entity ReportProcessingTaskTemplate<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ReportProcessingTaskTemplate"."Email"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 2024<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Email
		{
			get { return (System.String)GetValue((int)ReportProcessingTaskTemplateFieldIndex.Email, true); }
			set	{ SetValue((int)ReportProcessingTaskTemplateFieldIndex.Email, value, true); }
		}

		/// <summary> The ReportType property of the Entity ReportProcessingTaskTemplate<br/><br/></summary>
		/// <remarks>Mapped on  table field: "ReportProcessingTaskTemplate"."ReportType"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual Obymobi.Enums.AnalyticsReportType ReportType
		{
			get { return (Obymobi.Enums.AnalyticsReportType)GetValue((int)ReportProcessingTaskTemplateFieldIndex.ReportType, true); }
			set	{ SetValue((int)ReportProcessingTaskTemplateFieldIndex.ReportType, value, true); }
		}

		/// <summary> Retrieves all related entities of type 'UIScheduleItemEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiUIScheduleItemCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.UIScheduleItemCollection UIScheduleItemCollection
		{
			get	{ return GetMultiUIScheduleItemCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for UIScheduleItemCollection. When set to true, UIScheduleItemCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time UIScheduleItemCollection is accessed. You can always execute/ a forced fetch by calling GetMultiUIScheduleItemCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchUIScheduleItemCollection
		{
			get	{ return _alwaysFetchUIScheduleItemCollection; }
			set	{ _alwaysFetchUIScheduleItemCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property UIScheduleItemCollection already has been fetched. Setting this property to false when UIScheduleItemCollection has been fetched
		/// will clear the UIScheduleItemCollection collection well. Setting this property to true while UIScheduleItemCollection hasn't been fetched disables lazy loading for UIScheduleItemCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedUIScheduleItemCollection
		{
			get { return _alreadyFetchedUIScheduleItemCollection;}
			set 
			{
				if(_alreadyFetchedUIScheduleItemCollection && !value && (_uIScheduleItemCollection != null))
				{
					_uIScheduleItemCollection.Clear();
				}
				_alreadyFetchedUIScheduleItemCollection = value;
			}
		}
		/// <summary> Retrieves all related entities of type 'UIScheduleItemOccurrenceEntity' using a relation of type '1:n'.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for databinding conveniance, however it is recommeded to use the method 'GetMultiUIScheduleItemOccurrenceCollection()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the same scope.</remarks>
		public virtual Obymobi.Data.CollectionClasses.UIScheduleItemOccurrenceCollection UIScheduleItemOccurrenceCollection
		{
			get	{ return GetMultiUIScheduleItemOccurrenceCollection(false); }
		}

		/// <summary> Gets / sets the lazy loading flag for UIScheduleItemOccurrenceCollection. When set to true, UIScheduleItemOccurrenceCollection is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time UIScheduleItemOccurrenceCollection is accessed. You can always execute/ a forced fetch by calling GetMultiUIScheduleItemOccurrenceCollection(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchUIScheduleItemOccurrenceCollection
		{
			get	{ return _alwaysFetchUIScheduleItemOccurrenceCollection; }
			set	{ _alwaysFetchUIScheduleItemOccurrenceCollection = value; }	
		}		
				
		/// <summary>Gets / Sets the lazy loading flag if the property UIScheduleItemOccurrenceCollection already has been fetched. Setting this property to false when UIScheduleItemOccurrenceCollection has been fetched
		/// will clear the UIScheduleItemOccurrenceCollection collection well. Setting this property to true while UIScheduleItemOccurrenceCollection hasn't been fetched disables lazy loading for UIScheduleItemOccurrenceCollection</summary>
		[Browsable(false)]
		public bool AlreadyFetchedUIScheduleItemOccurrenceCollection
		{
			get { return _alreadyFetchedUIScheduleItemOccurrenceCollection;}
			set 
			{
				if(_alreadyFetchedUIScheduleItemOccurrenceCollection && !value && (_uIScheduleItemOccurrenceCollection != null))
				{
					_uIScheduleItemOccurrenceCollection.Clear();
				}
				_alreadyFetchedUIScheduleItemOccurrenceCollection = value;
			}
		}

		/// <summary> Gets / sets related entity of type 'CompanyEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleCompanyEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual CompanyEntity CompanyEntity
		{
			get	{ return GetSingleCompanyEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncCompanyEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "ReportProcessingTaskTemplateCollection", "CompanyEntity", _companyEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for CompanyEntity. When set to true, CompanyEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time CompanyEntity is accessed. You can always execute a forced fetch by calling GetSingleCompanyEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchCompanyEntity
		{
			get	{ return _alwaysFetchCompanyEntity; }
			set	{ _alwaysFetchCompanyEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property CompanyEntity already has been fetched. Setting this property to false when CompanyEntity has been fetched
		/// will set CompanyEntity to null as well. Setting this property to true while CompanyEntity hasn't been fetched disables lazy loading for CompanyEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedCompanyEntity
		{
			get { return _alreadyFetchedCompanyEntity;}
			set 
			{
				if(_alreadyFetchedCompanyEntity && !value)
				{
					this.CompanyEntity = null;
				}
				_alreadyFetchedCompanyEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property CompanyEntity is not found
		/// in the database. When set to true, CompanyEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool CompanyEntityReturnsNewIfNotFound
		{
			get	{ return _companyEntityReturnsNewIfNotFound; }
			set { _companyEntityReturnsNewIfNotFound = value; }	
		}

		/// <summary> Gets / sets related entity of type 'ReportProcessingTaskEntity'. This property is not visible in databound grids.
		/// Setting this property to a new object will make the load-on-demand feature to stop fetching data from the database, until you set this
		/// property to null. Setting this property to an entity will make sure that FK-PK relations are synchronized when appropriate.<br/><br/>
		/// </summary>
		/// <remarks>This property is added for conveniance, however it is recommeded to use the method 'GetSingleReportProcessingTaskEntity()', because 
		/// this property is rather expensive and a method tells the user to cache the result when it has to be used more than once in the
		/// same scope. The property is marked non-browsable to make it hidden in bound controls, f.e. datagrids.</remarks>
		[Browsable(true)]
		public virtual ReportProcessingTaskEntity ReportProcessingTaskEntity
		{
			get	{ return GetSingleReportProcessingTaskEntity(false); }
			set 
			{ 
				if(this.IsDeserializing)
				{
					SetupSyncReportProcessingTaskEntity(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "ReportProcessingTaskTemplateCollection", "ReportProcessingTaskEntity", _reportProcessingTaskEntity, true); 
				}
			}
		}

		/// <summary> Gets / sets the lazy loading flag for ReportProcessingTaskEntity. When set to true, ReportProcessingTaskEntity is always refetched from the 
		/// persistent storage. When set to false, the data is only fetched the first time ReportProcessingTaskEntity is accessed. You can always execute a forced fetch by calling GetSingleReportProcessingTaskEntity(true).</summary>
		[Browsable(false)]
		public bool AlwaysFetchReportProcessingTaskEntity
		{
			get	{ return _alwaysFetchReportProcessingTaskEntity; }
			set	{ _alwaysFetchReportProcessingTaskEntity = value; }	
		}
				
		/// <summary>Gets / Sets the lazy loading flag if the property ReportProcessingTaskEntity already has been fetched. Setting this property to false when ReportProcessingTaskEntity has been fetched
		/// will set ReportProcessingTaskEntity to null as well. Setting this property to true while ReportProcessingTaskEntity hasn't been fetched disables lazy loading for ReportProcessingTaskEntity</summary>
		[Browsable(false)]
		public bool AlreadyFetchedReportProcessingTaskEntity
		{
			get { return _alreadyFetchedReportProcessingTaskEntity;}
			set 
			{
				if(_alreadyFetchedReportProcessingTaskEntity && !value)
				{
					this.ReportProcessingTaskEntity = null;
				}
				_alreadyFetchedReportProcessingTaskEntity = value;
			}
		}

		/// <summary> Gets / sets the flag for what to do if the related entity available through the property ReportProcessingTaskEntity is not found
		/// in the database. When set to true, ReportProcessingTaskEntity will return a new entity instance if the related entity is not found, otherwise 
		/// null be returned if the related entity is not found. Default: true.</summary>
		[Browsable(false)]
		public bool ReportProcessingTaskEntityReturnsNewIfNotFound
		{
			get	{ return _reportProcessingTaskEntityReturnsNewIfNotFound; }
			set { _reportProcessingTaskEntityReturnsNewIfNotFound = value; }	
		}


		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the Obymobi.Data.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)Obymobi.Data.EntityType.ReportProcessingTaskTemplateEntity; }
		}

		#endregion

		
		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
